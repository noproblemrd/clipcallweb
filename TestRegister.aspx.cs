﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TestRegister : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Header.DataBind();
    }
    protected string GetPath
    {
        get { return ResolveUrl("~") + "api/WebServiceLogIn.asmx/NewUser"; }
    }
}