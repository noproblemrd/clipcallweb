﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPAbout.master" AutoEventWireup="true" CodeFile="contact.aspx.cs" Inherits="contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<div id="wrapadv">

	 	<div class="subtext_about"><p>There are many ways to find us and stay in touch:</p></div>

		

		


<h3 class="subclass">Support</h3>



      <p>We are  committed to providing the best pay-per-call experience. If you have any questions at all, or some feedback, please say <a href="mailto:hi@noproblemppc.com" class="ContactLinks">hi@noproblemppc.com</a> and we promise to get back to you.      
<p></p> 

      <br /><br /><h3 class="subclass">Reach out</h3>


 <p>Wether you are a <a href="partners-marketing.html" class="ContactLinks">marketing agency</a> looking to add an exciting and performing media for your clients, or interested in  becoming a <br>
   <a href="partners-franchisees.html" class="ContactLinks">franchisee</a> in your country, drop us a line.
 <p></p> 

<div id="formContact" class="formContactUs" style="display:block;">
    <div id="formContactCategories">
        <div class="formText">You are a...</div>
        <div class="inputform">
            <div style="width:448px;float:left;">
                <div style="display:inline;">
                <!-- Need to fix by Shai  -->
                    <input type="radio" name="categoryContactUs"   value="Marketing Agency" class="radio" onclick="validationContactUs('click','category','<#form1.clientID %>')" />
                <!-- Need to fix by Shai  -->
                    <div class="radioText" >Marketing Agency</div>
                </div>

                <div style="padding-left:20px;display:inline;"></div>
            
                <div style="display:inline;">
                    <input type="radio" name="categoryContactUs"  value="Franchisee" class="radio" onclick="validationContactUs('click','category')"/>
                    <div class="radioText">Franchisee</div>
                </div>
            </div>

             <div class="validation" id="validationContactUsCategory" style="display:none;margin-top: 0px;">
                    <div id="validationIconContactUsCategory" class="validationIcon"><img src="images/wizard/OrangeSmileyInstructor.png" alt="error"/></div>
                    <div id="validationTextContactUsCategory" >Enter category</div>
              </div>
        </div>
        
    </div>

    <div id="formContactEmail" class="email">
        <div class="formText">Your email address</div>
        <div>
            <div style="float:left" class="inputform">
                <input type="text" name="email" id="emailContactUs" value="e.g. me@awesomesite.com" class="place-holder" onblur="validationContactUs('blur','email');" onkeyup="validationContactUs('keyup','email');" onfocus="EmailFocus(this.value);"/>
            </div>
              <div class="validation" id="validationContactUs" style="display:none;">
                    <div id="validationIconContactUs" class="validationIcon"><img src="images/wizard/OrangeSmileyInstructor.png" alt="error"/></div>
                    <div id="validationTextContactUs" >Enter valid email address</div>
              </div>
        </div>
    </div>

    <div style="clear:both"></div>

    <div id="formContactSubject" class="subject">
        <div class="formText">Subject</div>
        <div style="float:left" class="inputform"><input type="text" name="subject" id="subjectContactUs" onBlur="validationContactUs('blur','subject');" onKeyUp="validationContactUs('keyup','subject');"/> </div>
        <div class="validation" id="validationContactUsSubject" style="display:none;">
            <div id="validationIconContactUsSubject" class="validationIcon"><img src="images/wizard/OrangeSmileyInstructor.png" alt="error"/></div>
            <div id="validationTextContactUsSubject">Enter subject</div>
        </div>
       
    </div>

    <div style="clear:both"></div>

    <div id="formContactMessage" class="message">
        <div class="formText">Message</div>
     

        <div style="float:left" class="inputform"><textarea name="message" id="messageContactUs" class="textareaContactUs" onblur="validationContactUs('blur','message');" onkeyup="validationContactUs('keyup','message');" onfocus="MessageFocus(this.value);">Tell us how can we work together.</textarea> </div>
        
        <div class="validation" id="validationContactUsMessage" style="display:none;padding-top:0px;">
            <div id="validationIconContactUsMessage" class="validationIcon"><img src="images/wizard/OrangeSmileyInstructor.png" alt="error"/></div>
            <div id="validationTextContactUsMessage">Enter message</div>
            
        </div>
       
       
    </div>


    <div style="clear:both;display:block;"></div>

    <div id="formContactSend" class="send">
        <img src="Formal/images/contactUs/send.png" onClick="sendContactUs();" onMouseOver="javascript:this.src=imgHover.src;"  onmouseout="javascript:this.src=imgSend.src;" onMouseDown="javascript:this.src=imgClick.src;"/>
    </div>

    <div id="formContactError" style="display:none;">
        <h3 class="subclass" id="formContactErrorTitle">Thank you!</h3>
        
        <span id="formContactErrorText">Your message has beed sent. we will get in touch with you.</span>
    </div>
    <!--[Tell us how can we work together.]-->
</div> 


<br /><br /><h3 class="subclass">You can also find us here...</h3>

<br />
<a class="wpsmall" target="_blank"  href="http://blog.noproblemppc.com/"></a> 
<a class="facebooksmall" href="https://www.facebook.com/pages/No-Problem-Local-Search-Monetized/149061931780318" target="_blank"></a>
<a class="twittersmall" href="http://twitter.com/#!/noproblemppc" target="_blank"></a> 
<a class="youtubesmall" href="http://www.youtube.com/user/NOPROBLEMPPCall" target="_blank"></a>

				

	</div> <!-- End wrapadv -->
</asp:Content>

