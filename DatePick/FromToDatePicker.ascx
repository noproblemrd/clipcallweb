﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FromToDatePicker.ascx.cs" Inherits="DatePick_FromToDatePicker" %>
<script type="text/javascript">
    var WrongFormatDate = "<%# GetWrongFormatDate %>";
    
    
   
</script>
<div class="div_DatePicker" runat="server" id="div_DatePicker">
      <div class="dates2">
        <div class="fromdate">    
            <asp:Label ID="lbl_from" runat="server" CssClass="label" Text="From"></asp:Label>
            <asp:TextBox ID="txt_from" runat="server" CssClass="_datepicker _from label_DatePicker form-textcal"></asp:TextBox> 

        </div>
      </div>  
      <div class="dates2">
            <div class="fromdate">    
                <asp:Label ID="lbl_to"  runat="server" CssClass="label" Text="To"></asp:Label>
                <asp:TextBox ID="txt_to" runat="server" CssClass="_datepicker _to label_DatePicker form-textcal"></asp:TextBox> 
                 
            </div>  
            <input id="hf_format" type="hidden" runat="server" class="hf_format" />
            <asp:Label ID="lbl_WrongFormat" runat="server" Text="Wrong date format. The right one is" Visible="false"></asp:Label>
        </div>
</div>
    

