﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class DatePick_FromToDatePicker : System.Web.UI.UserControl
{
    protected void Page_Init(object sender, EventArgs e)
    {
        HtmlGenericControl script = new HtmlGenericControl("script");
        script.Attributes.Add("language", "javascript");
        script.Attributes.Add("type", "text/javascript");
        script.ID = "Date_Picker";
        script.Attributes.Add("src", ResolveUrl("~") + @"DatePick/DatePicker.js");
        bool IsExsits = false;
        foreach (Control c in Page.Header.Controls)
        {
            if (c.ID == script.ID)
            {
                IsExsits = true;
                break;
            }
        }
        if(!IsExsits)
            Page.Header.Controls.Add(script);
        
        IsExsits = false;
        script = new HtmlGenericControl("script");
        script.Attributes.Add("language", "javascript");
        script.Attributes.Add("type", "text/javascript");
        script.ID = "_general";
        script.Attributes.Add("src", ResolveUrl("~") + @"general.js");
        
        foreach (Control c in Page.Header.Controls)
        {
            if (c.GetType() == typeof(DataBoundLiteralControl))
            {
                if (((DataBoundLiteralControl)c).Text.Contains("general.js"))
                {
                    IsExsits = true;
                    break;
                }
            }
            else if (c.GetType() == typeof(HtmlGenericControl))
            {
                HtmlGenericControl hgc = (HtmlGenericControl)c;
                if (hgc.Attributes["src"].Contains("general.js"))
                {
                    IsExsits = true;
                    break;
                }
            }
        }
        if (!IsExsits)
            Page.Header.Controls.Add(script);
        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PageSetting ps = (PageSetting)Page;
            getTranslate(ps.siteSetting.siteLangId);
            hf_format.Value = ConvertToDateTime.GetDateFormatDisplay(ps.siteSetting.DateFormat);    
        }
        this.DataBind();
    }
    private void getTranslate(int siteLangId)
    {
        string pagename = "FromToDatePicker.ascx";
        DBConnection.LoadTranslateToControl(this, pagename, siteLangId);
    }
    protected string GetWrongFormatDate
    {
        get { return Server.HtmlEncode(lbl_WrongFormat.Text + @""" " + hf_format.Value); }
    }
    public void SetDefaultDates(string _format)
    {
        if (ScriptManager.GetCurrent(Page) != null)        
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDefaultDates",
                "SetDefaultDates('" + div_DatePicker.ClientID + "','" + _format + "');", true);
        
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SetDefaultDates",
                "SetDefaultDates('" + div_DatePicker.ClientID + "','" + _format + "');", true);
    }
    public void SetDefaultDates(string _format, int _days)
    {
       
        if (ScriptManager.GetCurrent(Page) != null)  
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDefaultDates",
                "SetDefaultDatesByDays('" + div_DatePicker.ClientID + "',"+_days+");", true);
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SetDefaultDates",
                "SetDefaultDatesByDays('" + div_DatePicker.ClientID + "'," + _days + ");", true);
    }
     public void SetDefaultDatesByMonth(int months)
    {
        if (ScriptManager.GetCurrent(Page) != null)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDefaultDates",
                "SetDefaultDatesByMonths('" + div_DatePicker.ClientID + "'," + months + ");", true);
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SetDefaultDates",
                "SetDefaultDatesByMonths('" + div_DatePicker.ClientID + "'," + months + ");", true);
    }
    public void SetDefaultDatesMonthRound(string _format)
    {
        DateTime dt_Now = DateTime.Now;
        //DateTime dt_First = new DateTime(dt_Now.Year, dt_Now.Month, 1);
        SetDefaultDates(_format, dt_Now.Day - 1);
    }
    public void SetDefaultDates(string Javascriptformat, string SeverFormat, int _days)
    {
        DateTime date = DateTime.Now;
        txt_to.Text = string.Format(SeverFormat, date);
        txt_from.Text = string.Format(SeverFormat, date.AddDays(_days * -1));
        if (ScriptManager.GetCurrent(Page) != null)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDefaultDates",
                "SetDefaultDatesByDays('" + div_DatePicker.ClientID + "'," + _days + ");", true);
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SetDefaultDates",
                "SetDefaultDatesByDays('" + div_DatePicker.ClientID + "'," + _days + ");", true);
    }
    public void SetIntervalDateServer(int _days)
    {
        PageSetting _page = (PageSetting)Page;
        DateTime _date = DateTime.Now;
        txt_to.Text = string.Format(_page.siteSetting.DateFormat, _date);
        _date = _date.AddDays(-1 * _days);
        txt_from.Text = string.Format(_page.siteSetting.DateFormat, _date);
    }
    public TextBox GetTextBoxFrom()
    {
        return txt_from;
    }
    public TextBox GetTextBoxTo()
    {
        return txt_to;
    }
    public string GetParentDivId()
    {
        return div_DatePicker.ClientID;
    }
    public DateTime GetDateFrom
    {
        get
        {

            if (string.IsNullOrEmpty(txt_from.Text))
                return DateTime.MinValue;
            return ConvertToDateTime.CalanderToDateTime(txt_from.Text, ((PageSetting)Page).siteSetting.DateFormat);
        }
    }
    public DateTime GetDateTo
    {
        get
        {
            if (string.IsNullOrEmpty(txt_to.Text))
                return DateTime.MinValue;
            return ConvertToDateTime.CalanderToDateTime(txt_to.Text, ((PageSetting)Page).siteSetting.DateFormat);
        }
    }
    public void SetFromDate(string _date)
    {
        txt_from.Text = _date;
    }
    public void SetToDate(string _date)
    {
        txt_to.Text = _date;
    }
    public void SetFromDate(DateTime _date)
    {
        txt_from.Text = string.Format(((PageSetting)Page).siteSetting.DateFormat, _date);
    }
    public void SetToDate(DateTime _date)
    {
        txt_to.Text = string.Format(((PageSetting)Page).siteSetting.DateFormat, _date);
    }
    public string SetFromSentence
    {
        set { lbl_from.Text = value; }
    }
}
