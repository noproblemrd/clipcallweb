﻿$(function() {

	$('._datepicker').datepicker({
       onClose: function(dateText, inst) { OnBlurString(this); },
       dateFormat: 'mm/dd/yy'});
});

function OnBlurString(elm)
{    
    var elm_to;
    var elm_from;
    var elm_parent = getParentByClassName(elm, "div_DatePicker");
    var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;
    
    if(!validate_Date(elm.value, _format))
    {
        
        try{ alert(WrongFormatDate);}catch(ex){}
        elm.focus();
        elm.select();
        return false;
    }   
    if(elm.className.indexOf("_to") == -1)
    {
        elm_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
        elm_from = elm;
    }
    else
    {
        elm_from = getElementsWithMultiClassesByClass("_from", elm_parent, "input")[0];
        elm_to = elm;
    }
    var _end = GetDateFormat(elm_to.value, _format);
    var _start = GetDateFormat(elm_from.value, _format);
 //   alert("start= "+_start+" end= "+_end);
    if(_start>_end)
    {    
        if(elm_from.id == elm.id)
        {
            elm_to.value = GetDateString(new Date(_start), _format);
            
        }
        else
        {
            elm_from.value = GetDateString(new Date(_end), _format);
        }
        return true;
    }
    return true;
    
}



function SetDateTimeFormat(_format)
{
    $('._datepicker').datepicker({ dateFormat: _format });
}
function SetDefaultDates(elmId, _format)
{
    var elm = document.getElementById(elmId);
    var elm_to = getElementsWithMultiClassesByClass("_to", elm, "input")[0];
    var elm_from = getElementsWithMultiClassesByClass("_from", elm, "input")[0];
    var to_date = new Date();
    var from_date = new Date();
    from_date.addMonth(-1); 
    elm_to.value = GetDateString(new Date(to_date), _format);
    elm_from.value = GetDateString(new Date(from_date), _format);
}
function SetDefaultDatesByDays(elmId, _days)
{
  //  alert(_days);
    var elm = document.getElementById(elmId);
    var _format = (getElementsWithMultiClassesByClass("hf_format", elm, "input")[0]).value;
    var elm_to = getElementsWithMultiClassesByClass("_to", elm, "input")[0];
    var elm_from = getElementsWithMultiClassesByClass("_from", elm, "input")[0];
    var to_date = new Date();
    var from_date = new Date();    
    from_date.addDays(_days*-1); 
    elm_to.value = GetDateString(new Date(to_date), _format);
    elm_from.value = GetDateString(new Date(from_date), _format);
}
function SetDefaultDatesByMonths(elmId, months) {
    //  alert(_days);
    var elm = document.getElementById(elmId);
    var _format = (getElementsWithMultiClassesByClass("hf_format", elm, "input")[0]).value;
    var elm_to = getElementsWithMultiClassesByClass("_to", elm, "input")[0];
    var elm_from = getElementsWithMultiClassesByClass("_from", elm, "input")[0];
    var to_date = new Date();
    var from_date = new Date();
    from_date.addMonth(months * -1);
    elm_to.value = GetDateString(new Date(to_date), _format);
    elm_from.value = GetDateString(new Date(from_date), _format);
}
function SetDefaultDates_ByDays(elmId, _days)
{
  //  alert(_days);
    var elm = document.getElementById(elmId);
    var _format = (getElementsWithMultiClassesByClass("hf_format", elm, "input")[0]).value;
    var elm_to = getElementsWithMultiClassesByClass("_to", elm, "input")[0];
    var elm_from = getElementsWithMultiClassesByClass("_from", elm, "input")[0];
    var to_date = new Date(GetDateFormat(elm_to.value, _format));
    var from_date = new Date(GetDateFormat(elm_to.value, _format));
    if(_days == 29)
        from_date.addMonth(-1);
    else if(_days == 89)
        from_date.addMonth(-3);
    else if(_days == 364)
        from_date.addFullYear(-1);
    else
        from_date.addDays(_days*-1); 
    elm_to.value = GetDateString(new Date(to_date), _format);
    elm_from.value = GetDateString(new Date(from_date), _format);
}
function Check_DateValidation(elm)
{    
    var _format = (getElementsWithMultiClassesByClass("hf_format", elm, "input")[0]).value;
    var elm_to = getElementsWithMultiClassesByClass("_to", elm, "input")[0];
    var elm_from = getElementsWithMultiClassesByClass("_from", elm, "input")[0];
    if(elm_from.value.length==0 || !validate_Date(elm_from.value, _format))
    {
 //       try{ alert(WrongFormatDate);}catch(ex){}
        elm_from.focus();
        elm_from.select();
        return false;
    }   
    if(elm_to.value.length==0 || !validate_Date(elm_to.value, _format))
    {
//        try{ alert(WrongFormatDate);}catch(ex){}
        elm_to.focus();
        elm_to.select();
        return false;
    }   
    
    var _end = GetDateFormat(elm_to.value, _format);
    var _start = GetDateFormat(elm_from.value, _format);
    if(_start>_end)
    {
        elm_from.focus();
        elm_from.select();
        return false;
    }
    return true;
}

function GetRangeDays(elm_parentId)
{
    var elm_parent = document.getElementById(elm_parentId);
    var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;
    var elm_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
    var elm_from = getElementsWithMultiClassesByClass("_from", elm_parent, "input")[0];
    var _end = GetDateFormat(elm_to.value, _format);
    var _start = GetDateFormat(elm_from.value, _format);
    var diff = Math.round((_end - _start)/(1000 * 60 * 60 * 24));
    return diff;    
}



