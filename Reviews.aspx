﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Reviews.aspx.cs" Inherits="Reviews" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Untitled Page</title>
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />

    <script type="text/javascript" language="javascript">
    
    var REVIEW_CHAR = 1000;
    /*
    var NAME_CHAR = 50;
    */
    function check_All()
    {
        var like_error = document.getElementById("<%# lbl_LikeMissing.ClientID %>");
        like_error.style.display = "none";
        if(!Page_ClientValidate('review'))
        {
   
            var validators = Page_Validators;  
            for (var i = 0; i < validators.length; i++) { 
                var _elm = document.getElementById(validators[i].controltovalidate)
     
                if(!validators[i].isvalid)
                {

                    alert("<%# GetValidationMissing %>");
                    _elm.focus();  
                    break;
                             
                }  
                     
            }
           return false;
        }
        var text_elm = document.getElementById("<%# txt_yourReview.ClientID %>");
        if(text_elm.value.length < 5)
        {
            alert("<%# GetReviewLength %>");
            text_elm.select();
            return false;
        }
        if(document.getElementById("<%# hf_IsLike.ClientID %>").value.length == 0)
        {
            like_error.style.display = "inline";
            return false;
        }
        return true;
    }
    
    function CountLetter(elm, e)
    {   
        if(e)
        {
            var code = e.keyCode ? e.keyCode : e.which; 
            if(code == 13)
            {        
                return false;
            }
        }
       var _length = elm.value.length;
       if(elm.value.length > REVIEW_CHAR)
            elm.value = elm.value.substring(0, REVIEW_CHAR);
        document.getElementById("<%# lbl_count.ClientID %>").innerHTML=elm.value.length;
        
        return true;
    }
    function LikeClick(elem)
    {
        document.getElementById("<%# hf_IsLike.ClientID %>").value = "Like";
        elem.className = "likedisable";//"like";
        document.getElementById("a_dislike").className = "dislike";//"dislikedisable"
    }
    function DislikeClick(elem)
    {
        document.getElementById("<%# hf_IsLike.ClientID %>").value = "Dislike";
        elem.className = "dislikedisable";//"dislike";
        document.getElementById("a_like").className = "like";//"likedisable"
    }
    function ClearLike_Dislike()
    {
        document.getElementById("a_like").className = "like";
        document.getElementById("a_dislike").className = "dislike";
    }
   
    
    </script>

</head>
<body style="background:#fff;">
    <form id="form1" runat="server">
    
    <asp:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </asp:ToolkitScriptManager>

    <div>
        <div class="wraprev">
	    <div class="header"></div>
	        <div class="title">
	            <asp:Label ID="lbl_AddReview" runat="server" Text="Add new review"></asp:Label>
	        </div>
                <div class="bg">

           	    	<div class="info">
           	    	    <div class="txt" style="width:200px">
           	    	        <asp:Label ID="lbl_name" runat="server" Text="Name"></asp:Label>:
           	    	    </div>
                         <div class="clear"></div>
           	    	    <div class="fieldph" >
           	    	        <asp:TextBox ID="txt_name" runat="server" MaxLength="50" CssClass="fieldph" ></asp:TextBox>
           	    	        <asp:RequiredFieldValidator ID="RequiredFieldValidator_name" runat="server" CssClass="error-msg" 
                            ErrorMessage="*" ValidationGroup="review" Display="Static" ControlToValidate="txt_name"></asp:RequiredFieldValidator>
           	    	    </div>
                         <div class="clear"></div>
           	    	    <div class="txt" style="width:200px">
           	    	        <asp:Label ID="lbl_phone" runat="server" Text="Phone number"></asp:Label>:
           	    	    </div>
                        <div class="clear"></div>
           	    	    <div class="fieldph" >
                      	    <asp:TextBox ID="txt_phone" runat="server" CssClass="fieldph"></asp:TextBox>
                      	    
                      	    <asp:RequiredFieldValidator ID="RequiredFieldValidator_phone" runat="server" CssClass="error-msg" 
                            ErrorMessage="*" ValidationGroup="review" Display="Static" ControlToValidate="txt_phone"></asp:RequiredFieldValidator>
                            
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator_phone" CssClass="error-msg" Display="Static"
                            runat="server" ErrorMessage="Invalid phone number" ControlToValidate="txt_phone" ValidationExpression="^\d+$"
                            ValidationGroup="review"></asp:RegularExpressionValidator>

                        </div>
                        <div class="clear"></div>
                         <div class="txt" style="width:200px">
                            <asp:Label ID="lbl_yourReview" runat="server" Text="Your review"></asp:Label>:
                        </div>
                           <div class="clear"></div>
                        <div class="fieldrev" >
                      	    <asp:TextBox ID="txt_yourReview" runat="server" onkeyup="return CountLetter(this, event);" onblur="CountLetter(this);" 
                                TextMode="MultiLine"  CssClass="fieldrev"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator_yourReview" runat="server" CssClass="error-msg" 
                                ErrorMessage="*" ValidationGroup="review" Display="Static" ControlToValidate="txt_yourReview"></asp:RequiredFieldValidator>
                            <span class="count">1000/<asp:Label ID="lbl_count" runat="server" Text="0" ></asp:Label></span>
                      	</div>
                      	   <div class="clear"></div>
                        <div class="rating"> 
                            <div class="txt" style="width:100px">
                                <asp:Label ID="lbl_YourRating" runat="server" Text="Your rating" ></asp:Label>:
                            </div>
                        		<div class="rating1">
                        		    <a id="a_like" href="javascript:void(0);" onclick="javascript:LikeClick(this)" class="likedisable">
                        		        &nbsp;
                        	
                        		    </a>
                        		    <a id="a_dislike" href="javascript:void(0);" onclick="javascript:DislikeClick(this)" class="dislike">
                        		        &nbsp;
                        	
                        		    </a>
                        		
                        		 
                            <asp:Label ID="lbl_LikeMissing" runat="server" Text="*" CssClass="error-msg" style="display:none;"></asp:Label>
                                <asp:HiddenField ID="hf_IsLike" runat="server" />   
                        		</div>
                        </div>
                    </div>
                    <div class="info2">
                        <div class="capcha2">
                    <telerik:RadCaptcha ID="_RadCaptcha" runat="server"  EnableRefreshImage="true" 
                    ValidationGroup="review" ProtectionMode="Captcha"  CaptchaImage-ImageCssClass="captchapict2"  
                    CaptchaTextBoxCssClass="form-textcaptcha" CaptchaTextBoxLabelCssClass="captchalabel2">
                    </telerik:RadCaptcha>
                </div>
                  <div class="clear"></div>
                      	<div style="background:none;">
                   
                      	    <asp:Button ID="btn_send" runat="server"  onclick="btn_send_Click" ValidationGroup="review"
                      	     OnClientClick="return check_All();"  Text="Send review" CssClass="send" />
                      	    
                      	    <input id="btn_closed" runat="server" type="button" value="Close" onclick="javascript:window.close();" style="display:none;" />
                      	</div>
                    </div>
                </div>
              
                <div class="title">
                    <asp:Label ID="lbl_oldReview" runat="server" Text="Past Reviews"></asp:Label>
                </div>
                
                <asp:Repeater ID="_Repeater" runat="server">
                <ItemTemplate>
                
                    <div class="strip">
        	            <div class="field">
        		            <div class="txt" style="width:50px;">
        		                <asp:Label ID="lblrating" runat="server" Text="<%# lbl_rating.Text %>"></asp:Label>:
        		            </div>
        		            <asp:Image ID="img_rating" runat="server" ImageUrl="<%# Bind('Like') %>" Width="25" Height="21" /> 
                        </div>
                        <div class="date">
                            <div class="txt" style="width:70px;">
                                <asp:Label ID="lblcreateOn" runat="server" Text="<%# lbl_createOn.Text %>"></asp:Label>:
                            </div>
                      	    <div class="txt2">
                      	        <asp:Label ID="lbl_CreatedOn" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
                      	    </div>
                        </div> 
                        <div class="by">
                      		<div class="txt" style="width:20px;">
                      		    <asp:Label ID="lblby" runat="server" Text="<%# lbl_by.Text %>"></asp:Label>:
                      		</div>
                      		<div class="txt2">
                      		    <asp:Label ID="lbl_CreateBy" runat="server" Text="<%# Bind('Name') %>"></asp:Label>
                      		</div>
                     	</div>
                    </div>
                    <div class="review">
                        <asp:Label ID="lbl_review" runat="server" Text="<%# Bind('Description') %>"></asp:Label>
                    </div>
                </ItemTemplate>
                </asp:Repeater>
             
        </div>
    </div>
    <asp:Label ID="lbl_rating" runat="server" Text="Rating" Visible="false"></asp:Label>      
    <asp:Label ID="lbl_createOn" runat="server" Text="Created on" Visible="false"></asp:Label>   
    <asp:Label ID="lbl_by" runat="server" Text="By" Visible="false"></asp:Label>
    
                    
    <asp:Label ID="lbl_errorClose" runat="server" Text="Sorry, An error occurred, the window will be closed." Visible="false"></asp:Label>
    <asp:Label ID="lbl_error" runat="server" Text="Sorry, An error occurred, please try again later." Visible="false"></asp:Label>
    <asp:Label ID="lbl_ReviewReceive" runat="server" Text="Thank you for sharing your review. Your review will be displayed in few minutes." Visible="false"></asp:Label>
    <asp:Label ID="lbl_AllreadyExists" runat="server" Text="Thank you for your review. how ever the system allready got review from this phone number." Visible="false"></asp:Label>
    <asp:Label ID="lbl_NoCaseFound" runat="server" Text="There was no match for this phone number. Please write the phone number that was connected with the advertiser." Visible="false"></asp:Label>

    <asp:Label ID="hf_validation" runat="server" Text="Few of the mandatory fields are missing. Please fill in the mandatory fields and send again" Visible="false"></asp:Label>
    <asp:Label ID="hf_reviewLength" runat="server" Text="The review must be from 5 chars to 1000 chars" Visible="false"></asp:Label>
     <asp:Label ID="lbl_MissingCaptcha" runat="server" Visible="false" Text="Incorrect access code. Please try again"></asp:Label>	

    </form>
</body>
</html>
