﻿<%@ WebHandler Language="C#" Class="MobileService" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Script.Serialization;

public class MobileService : IHttpHandler {
 //   delegate void AsyncCreateExposure(Guid ID, string IP, string browser, eExposureType ExposureType);
    SliderData sd;
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();

        context.Response.ContentType = "application/javascript; charset=UTF-8;";
        context.Response.ContentEncoding = System.Text.Encoding.UTF8;
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

        
        string keyword = context.Request.QueryString["keyword"];
        string _ip = context.Request.QueryString["ip"];
        string user_agent = context.Request.QueryString["useragent"];
        PpcSite _cache = PpcSite.GetCurrent();
        GeoLocation gl = SalesUtility.GetGeoLocation(_ip);
        if (gl == null)
        {
            ResultRequest<MobileServiceResult> result = new ResultRequest<MobileServiceResult>(new List<string>(new string[] { "Invalid IP" }));
            context.Response.Write(new JavaScriptSerializer().Serialize(result));
            context.ApplicationInstance.CompleteRequest();
            return;
        }
   //     sd = new SliderData(user_agent, keyword, context.Request.QueryString["OriginID"], gl, _ip);
        if (string.IsNullOrEmpty(sd.ExpertiseCode))
        {
            ResultRequest<MobileServiceResult> result = new ResultRequest<MobileServiceResult>(new List<string>(new string[] { "No matching keyword" }));
            context.Response.Write(new JavaScriptSerializer().Serialize(result));
            context.ApplicationInstance.CompleteRequest();
            return;
        }
        
        HeadingCode hc = _cache.headings[sd.ExpertiseCode];
        if (hc.DidPhone == null)
        {
            ResultRequest<MobileServiceResult> result = new ResultRequest<MobileServiceResult>(new List<string>(new string[] { "Expertise without direct phone" }));
            context.Response.Write(new JavaScriptSerializer().Serialize(result));
            context.ApplicationInstance.CompleteRequest();
            return;
        }
        string tel = hc.DidPhone.GetTel(_cache.StartAppOriginId, gl.Country);
        if (string.IsNullOrEmpty(tel))
        {
            ResultRequest<MobileServiceResult> result = new ResultRequest<MobileServiceResult>(new List<string>(new string[] { "Origin without direct phone" }));
            context.Response.Write(new JavaScriptSerializer().Serialize(result));
            context.ApplicationInstance.CompleteRequest();
            return;
        }
     //   Guid _id = Guid.NewGuid();
  //      AsyncCreateExposure caller = new AsyncCreateExposure(this.CreateExposure);
//        IAsyncResult iresult = null;
   //     iresult = caller.BeginInvoke(_id, _ip, sd.Browser, sd.Type, null, null);
        sd.InsertExposure();
        string title = hc.MobileDeviceTitle.Replace("{0}", gl.City);
        ResultRequest<MobileServiceResult> _result = new ResultRequest<MobileServiceResult>(new MobileServiceResult(title, hc.MobileDeviceDescription, tel));
        context.Response.Write(new JavaScriptSerializer().Serialize(_result));        
        context.Response.StatusCode = 200;
        context.ApplicationInstance.CompleteRequest();
//        if (iresult != null)
 //           caller.EndInvoke(iresult);
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
    /*
    public void CreateExposure(Guid ID,  string IP, string browser,  eExposureType ExposureType)
    {

     
        DataExposure.AddExposure(ID, sd, ExposureType, true, eWelcomeExposureEvent.none);


    }
     * */

}