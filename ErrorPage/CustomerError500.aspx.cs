﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ErrorPage_CustomerError500 : PageSetting
{
    string _path;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (npsb_error)
        {
            npsb_error = false;
            Response.Redirect(ResolveUrl("~/npsb/edestination.aspx"));
        }
        string _siteId = null;
        bool ShowAlert = true;
        string _host = Request.ServerVariables["SERVER_NAME"];
        if (_host == "www.noproblem.me")
        {
            _path = ResolveUrl("http://www.noproblem.me");
            ShowAlert = false;
        }
        else if (string.IsNullOrEmpty(siteSetting.GetSiteID))
            _siteId = DBConnection.GetSiteId(_host);
        else
            _siteId = siteSetting.GetSiteID;
        if(!string.IsNullOrEmpty(_siteId))
        {            
            if (userManagement == null || !userManagement.IsAuthenticated())
            {
                if (_siteId == PpcSite.GetCurrent().SiteId)
                    _path = ResolveUrl("http://www.noproblem.me");
                else
                    _path = ResolveUrl("~") + "Publisher/LogOut.aspx";
            }
            else if (userManagement.IsPublisher())
                _path = ResolveUrl("~") + "Publisher/LogOut.aspx";
            else if (userManagement.IsAffiliate())
                _path = ResolveUrl("~") + "Affiliate/LogOut.aspx";
            else
                _path = ResolveUrl("~") + "Management/LogOut.aspx";
        }
        Header.DataBind();
        ClientScript.RegisterStartupScript(this.GetType(), "_onload", "_onload(" + ShowAlert.ToString().ToLower() + ");", true);
    }
    protected string GetLocation
    {
        get { return _path; }
    }
    protected string GetErrorMessage
    {
        get { return HttpUtility.JavaScriptStringEncode("Error occur"); }
    }
    bool npsb_error
    {
        get 
        { 
            HttpCookie cookie = Request.Cookies["npsb_error"];
            if(cookie == null)
                return false;
            return cookie.Value == "true";
        }
        set 
        {
            Response.Cookies.Add(new HttpCookie("npsb_error", value.ToString().ToLower()));
        }
    }
}