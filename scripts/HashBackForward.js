﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />

function HashBackForward(StartFunc) {
    this._interval;
    this.hash = window.location.hash;
    this.funcs = [];
    this.SetStep('', StartFunc);
}
function StepFunc(step, func){
    this.step = step;
    this.func = func;
}
HashBackForward.prototype.SetStep = function (step, func) {
    step = '#' + step;
    this.funcs.push(new StepFunc(step, func));
    if (step == '#')
        this.funcs.push(new StepFunc('', func));
}
HashBackForward.prototype.start = function () {
    var _this = this;
    this._interval = setInterval(function () {
        var _hash = window.location.hash;
        if (_hash != _this.hash) {
            for (var i = 0; i < _this.funcs.length; i++) {
                if (_this.funcs[i].step == _hash) {
                    _this.funcs[i].func();
                    break;
                }
            }
            _this.hash = _hash;
        }
    }, 500);
}