﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />

function np_email() {
    this.inputs = [];
    this.OnTheWay = SetRegExGroup(["^\\w+[-+.']?$",
                    "^\\w+([-+.']\\w+)*[-+.']?$",
                    "^\\w+([-+.']\\w+)*@$",
                    "^\\w+([-+.']\\w+)*@\\w+[-.]?$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*[-.]?$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{1,2}$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{2,}[-.]?$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{2,}([-.]\\w+)*[-.]?$"]);
    this.reg = new RegExp("^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{2,}([-.]\\w+)*$");
}

np_email.prototype.init = function () {
    var _this = this;
    $(function () {
        $('input[text-mode|="email"]').each(function () {
            var InComment = $(this).attr('comment-text');
            if (!InComment)
                InComment = "e.g. you@superman.com";
            var $div_parent = $(this).closest('div');
            $div_parent.addClass('inputParent');
            var $div_text = $('<div>');
            $div_text.addClass('inputSun');

            $(this).remove();
            $div_text.append($(this));
            $div_parent.append($div_text);
            $div_parent.append($('<div>').addClass('inputValid').css('display', 'none'));
            $div_parent.append($('<div>').addClass('inputError').css('display', 'none'));

            var $div_comment = $('<div>').addClass('inputComment').css('display', 'none');
            $div_comment.html(InComment);
            $div_comment.append($('<div>').addClass('chat-bubble-arrow-border'));
            $div_comment.append($('<div>').addClass('chat-bubble-arrow'));
            $div_parent.append($div_comment);

            $(this).keyup(function () {
                $(this).removeClass('textValid');
                $(this).removeClass('textError');
                var _status = _this.checkEmailStatus(this);
                switch (_status) {
                    case ("empty"):
                        $(this).closest('.inputParent').find('.inputValid').hide();
                        $(this).closest('.inputParent').find('.inputError').show();
                        $(this).addClass('textValid');
                        _this.SlideComment($(this).closest('.inputParent').find('.inputComment').get(0), true);
                        break;
                    case ("match"):
                        $(this).closest('.inputParent').find('.inputValid').show();
                        $(this).closest('.inputParent').find('.inputError').hide();
                        $(this).addClass('textValid');
                        _this.SlideComment($(this).closest('.inputParent').find('.inputComment').get(0), false);
                        break;
                    case ("OnTheWay"):
                        $(this).closest('.inputParent').find('.inputValid').hide();
                        $(this).closest('.inputParent').find('.inputError').hide();
                        _this.SlideComment($(this).closest('.inputParent').find('.inputComment').get(0), true);
                        break;
                    case ("faild"):
                        $(this).closest('.inputParent').find('.inputValid').hide();
                        $(this).closest('.inputParent').find('.inputError').show();
                        $(this).addClass('textError');
                        _this.SlideComment($(this).closest('.inputParent').find('.inputComment').get(0), true);
                        break;
                }
            });
            $(this).blur(function () {
                $(this).removeClass('textValid');
                $(this).removeClass('textError');
                var _status = _this.checkEmailStatus(this);
                switch (_status) {
                    case ("empty"):
                        $(this).closest('.inputParent').find('.inputValid').hide();
                        $(this).closest('.inputParent').find('.inputError').hide();
                        //     $(this).addClass('textValid');
                        _this.SlideComment($(this).closest('.inputParent').find('.inputComment').get(0), false);
                        break;
                    case ("match"):
                        $(this).closest('.inputParent').find('.inputValid').show();
                        $(this).closest('.inputParent').find('.inputError').hide();
                        $(this).addClass('textValid');
                        _this.SlideComment($(this).closest('.inputParent').find('.inputComment').get(0), false);
                        break;
                    case ("OnTheWay"):
                    case ("faild"):
                        $(this).closest('.inputParent').find('.inputValid').hide();
                        $(this).closest('.inputParent').find('.inputError').show();
                        $(this).addClass('textError');
                        _this.SlideComment($(this).closest('.inputParent').find('.inputComment').get(0), true);
                        break;
                }
            });
            $(this).focus(function () {
                $(this).removeClass('textValid');
                $(this).removeClass('textError');
                $(this).closest('.inputParent').find('.inputValid').hide();
                $(this).closest('.inputParent').find('.inputError').hide();
                //   _this.SlideComment($(this).closest('.inputParent').find('.inputComment').get(0), false);
            });
            _this.inputs.push(this);
        });
    });
}

np_email.prototype.GetAllStatus = function () {
    var result = [];
    for (var i = 0; i < this.inputs.length; i++) {
        var _id = this.inputs[i].id;
        var _response = this.checkEmailStatus(this.inputs[i]);
        
        $(this.inputs[i]).removeClass('textValid');
        $(this.inputs[i]).removeClass('textError');
        switch (_response) {
            case ("empty"):
                $(this.inputs[i]).closest('.inputParent').find('.inputValid').hide();
                $(this.inputs[i]).closest('.inputParent').find('.inputError').show();
             //   $(this.inputs[i]).addClass('textValid');
                this.SlideComment($(this.inputs[i]).closest('.inputParent').find('.inputComment').get(0), true);
                result.push([_id, _response]);
                break;
            case ("match"):
                $(this.inputs[i]).closest('.inputParent').find('.inputValid').show();
                $(this.inputs[i]).closest('.inputParent').find('.inputError').hide();
                $(this.inputs[i]).addClass('textValid');
                this.SlideComment($(this.inputs[i]).closest('.inputParent').find('.inputComment').get(0), false);
                break;
            case ("OnTheWay"):
            case ("faild"):
                $(this.inputs[i]).closest('.inputParent').find('.inputValid').hide();
                $(this.inputs[i]).closest('.inputParent').find('.inputError').show();
                $(this.inputs[i]).addClass('textError');
                this.SlideComment($(this.inputs[i]).closest('.inputParent').find('.inputComment').get(0), true);
                result.push([_id, _response]);
                break;
        }
    }
    return result;
}
np_email.prototype.SetStatus = function (email_element) {
    if (!email_element) {
        if (this.inputs == 0)
            return true;
        email_element = this.inputs[0];
    }

    var _response = this.checkEmailStatus(email_element);

    $(email_element).removeClass('textValid');
    $(email_element).removeClass('textError');
    switch (_response) {
        case ("empty"):
            $(email_element).closest('.inputParent').find('.inputValid').hide();
            $(email_element).closest('.inputParent').find('.inputError').show();
            $(email_element).addClass('textError');
            this.SlideComment($(email_element).closest('.inputParent').find('.inputComment').get(0), true);

            return false;
        case ("match"):
            $(email_element).closest('.inputParent').find('.inputValid').show();
            $(email_element).closest('.inputParent').find('.inputError').hide();
            $(email_element).addClass('textValid');
            this.SlideComment($(email_element).closest('.inputParent').find('.inputComment').get(0), false);
            return true;
        case ("OnTheWay"):
        case ("faild"):
            $(email_element).closest('.inputParent').find('.inputValid').hide();
            $(email_element).closest('.inputParent').find('.inputError').show();
            $(email_element).addClass('textError');
            this.SlideComment($(email_element).closest('.inputParent').find('.inputComment').get(0), true);
            return false;
    }
    return false;

}
np_email.prototype.ClearComments = function (email_element) {
    if (!email_element) {
        if (this.inputs == 0)
            return true;
        email_element = this.inputs[0];
    }
    $(email_element).removeClass('textValid');
    $(email_element).removeClass('textError');
    $(email_element).closest('.inputParent').find('.inputValid').hide();
    $(email_element).closest('.inputParent').find('.inputError').hide();
    this.SlideComment($(email_element).closest('.inputParent').find('.inputComment').get(0), false);
}
np_email.prototype.GetEmail = function (email_element) {
    if (!email_element) {
        if (this.inputs == 0)
            return '';
        email_element = this.inputs[0];
    }
    return $(email_element).val();
}
np_email.prototype.checkEmailStatus = function(text_email){
    var email = $.trim($(text_email).val());
    $(text_email).val(email);
    if (email.length == 0)
        return "empty";
    else if(this.reg.test(email))
       return "match";
    for(var j=0; j<this.OnTheWay.length; j++){
        if(this.OnTheWay[j].test(email))
            return "OnTheWay";
    }
    return "faild";
}

np_email.prototype.SlideComment = function (_element, ToSlide) {
    if (ToSlide == $(_element).data('IsHidden'))
        return;
    if (!ToSlide && $(_element).data('IsHidden') == null)
        return;
    if (ToSlide) {
        $(_element).show("slide", { direction: "down" }, 200);
        $(_element).data('IsHidden', true);
    }
    else {
        $(_element).hide("slide", { direction: "down" }, 200);
        $(_element).data('IsHidden', false);
    }
}
function SetRegExGroup(list) {
    var _list = [];
    for (var i = 0; i < list.length; i++) {
        _list.push(new RegExp(list[i]));
    }
    return _list;
}