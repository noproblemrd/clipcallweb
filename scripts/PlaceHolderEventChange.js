﻿/*
attributes:
place_holder="text in place holder"
HolderClass="css class of place holder style"

*/
/// <reference path="http://code.jquery.com/jquery-latest.js" />

$(function () {

    $("input[type=text],input[type=password], textarea").filter(function () {
        return $(this).attr("place_holder") !== undefined;
    }).each(function (index) {
        var _place_holde = $(this).attr('place_holder');
        var _elem;
        if ($(this).is("textarea")) {
            _elem = ($("<textarea>")//.offset({ top: this.offsetTop, left: this.offsetLeft })
            //         .height(this.offsetHeight)
            //         .width(this.offsetWidth)
                .attr('id', "pt" + index)
                .css({ 'display': 'inline' }).get(0));
            /*
            var attrs = this.attributes;
            for (var i = 0; i < attrs.length; i++) {
                switch (attrs[i].name) {
                    case ('class'):
                    case ('type'):
                    case ('id'):
                    case ('autocomplete'):
                    case ('HolderClass'):
                    case ('place_holder'):
                    case ('name'):
                        continue;

                }
                
                $(_elem).attr(attrs[i].name, $(this).attr(attrs[i].nodeValue));
            }
            */
            if ($(this).attr('cols') != undefined)
                $(_elem).attr('cols', $(this).attr('cols'));
            if ($(this).attr('rows') != undefined)
                $(_elem).attr('rows', $(this).attr('rows'));
            
        }
        else {
            _elem = ($("<input>").attr("type", "text")//.offset({ top: this.offsetTop, left: this.offsetLeft })
            //             .height(this.offsetHeight)
            //             .width(this.offsetWidth)

                .attr('id', "pt" + index)
                .css({ 'display': 'inline' }).get(0));
        }
        if ($(this).attr('type') == 'password' && $(this).attr('autocomplete') != 'off')
            $(this).attr('autocomplete', 'off');
        //     if ($(this).attr('holder-position') == "none")
        //         $(_elem).css('position', '');
        if ($(this).attr('class')) {
            //   $(_elem).attr('class', $(this).attr('class'));
            var _classes = $(this).attr('class').split(' ');
            for (var i = 0; i < _classes.length; i++) {
                var str = $.trim(_classes[i]);
                if (str.length > 0 && str.charAt(0) != '-')
                    $(_elem).addClass(str);
            }
        }
        $(_elem).val(_place_holde);
        //    $(_elem).appendTo($(this).parent());
        $(this).parent().get(0).insertBefore(_elem, this)
        if ($(this).val().length > 0)
            $(_elem).hide();
        else
            $(this).hide();
        if ($(this).attr('HolderClass') != undefined)
            $(_elem).addClass($(this).attr('HolderClass'));
        else
            $(_elem).css({ 'color': '#CCCCCC', 'fontStyle': 'italic', 'fontWeight': 'lighter' });
        var _inputId = this;
        $(_elem).focus(function () {
            $(this).hide();
            $(_inputId).show();
            $(_inputId).focus();
        });
        $(this).blur(function () {
            if ($(this).val().length > 0)
                return;
            $(this).hide();
            $(_elem).show();
            $(_elem).blur();

        });

        var _holder = $(_elem).val();
        var _intr = setInterval(
            function () {
                if ($(_elem).val() != $(_inputId).attr("place_holder")) {
                    $(_elem).val($(_inputId).attr("place_holder"));
                }
                if ($(_elem).css('display') != 'none' && $(_inputId).val().length > 0) {
                    $(_elem).hide();
                    $(_inputId).show();
                    $(_inputId).blur();
                }
                var AllClases = $(_inputId).attr('class').split(' ');
                var AllClasesHolder = $(_elem).attr('class').split(' ');
                RemoveClassFromArray($(_inputId).attr('HolderClass'), AllClasesHolder);
                for (var i = 0; i < AllClases.length; i++) {
                    if (AllClases[i].length < 2 || AllClases[i].substring(0, 1) == '-')
                        continue;
                    RemoveClassFromArray(AllClases[i], AllClasesHolder);
                    if (!$(_elem).hasClass(AllClases[i]))
                        $(_elem).addClass(AllClases[i]);
                }
                for (var i = 0; i < AllClasesHolder.length; i++) {
                    $(_elem).removeClass(AllClasesHolder[i]);
                }
                if ($(_inputId).is(":visible")) {
                    if (_inputId != document.activeElement && $(_inputId).val().length == 0) {
                        $(_inputId).hide();
                        $(_elem).show();
                        $(_elem).blur();
                    }
                }
                if ($(_inputId).css('width') != $(_elem).css('width')) {
                    $(_elem).css('width', $(_inputId).css('width'));
                }
            }, 500);

    });
    function RemoveClassFromArray(cssclass, _arr) {
        for (var i = _arr.length - 1; i > -1; i--) {
            if (cssclass == _arr[i]) {
                _arr.splice(i, 1);
            }
        }
    }

});

