﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />
/*
attributes:
text-mode="phone"
a[button-mode="phone"]

*/

function np_phone(_RegExp) {
    this.inputs = [];
    this.buttons = [];
    this.OnTheWay = SetRegExGroup(["^[+]?[01]?[- .]?[(]?$",
                                "^[+]?[01]?[- .]?[(]?[2-9]$",
                                "^[+]?[01]?[- .]?[(]?[2-9]\\d{1,2}[)]?[- .]?$",
                                "^[+]?[01]?[- .]?(\\([2-9]\\d{2}\\)|[2-9]\\d{2})[- .]?\\d{1,3}[- .]?$",
                                "^[+]?[01]?[- .]?(\\([2-9]\\d{2}\\)|[2-9]\\d{2})[- .]?\\d{3}[- .]?\\d{1,3}$"]);
    this.reg = new RegExp(_RegExp);
}
np_phone.prototype.init = function () {
    var _this = this;
    $(function () {
        $('input[text-mode="phone"]').each(function () {

            $(this).keyup(function (e) {
                var keynum;
                if (window.event) // IE            
                    keynum = e.keyCode
                else if (e.which) // Netscape/Firefox/Opera            
                    keynum = e.which
                if (keynum == 13) {
                    $(this).blur();
                    _this.onclick();
                    return;
                }
                var _status = _this.checkPhoneStatus(this);
                switch (_status) {
                    case ("empty"):
                        if (_this.OnKeyUpEmpty)
                            _this.OnKeyUpEmpty(this.id);
                        break;
                    case ("match"):
                        if (_this.OnMatch)
                            _this.OnMatch(this.id);
                        break;
                    case ("OnTheWay"):
                        if (_this.OnKeyUpOnTheWay)
                            _this.OnKeyUpOnTheWay(this.id);
                        break;
                    case ("faild"):
                        if (_this.OnFaild)
                            _this.OnFaild(this.id);
                        break;
                }
            });
            $(this).blur(function () {
                var _status = _this.checkPhoneStatus(this);
                switch (_status) {
                    case ("empty"):
                        if (_this.OnKeyUpEmpty)
                            _this.OnKeyUpEmpty(this.id);
                        break;
                    case ("match"):
                        if (_this.OnMatch)
                            _this.OnMatch(this.id);
                        break;
                    case ("OnTheWay"):
                    case ("faild"):
                        if (_this.OnFaild)
                            _this.OnFaild(this.id);
                        break;
                }
            });
            $(this).focus(function () {
                if (_this.OnFocus)
                    _this.OnFocus(this.id);
            });
            _this.inputs.push(this);
        });
        $('a[button-mode="phone"]').each(function () {
            _this.buttons.push(this);
            var thisButton = this;
            $(this).click(function () {
                _this.onclick();
            });
        });
    });
}
np_phone.prototype.onclick = function () {
    for (var i = 0; i < this.inputs.length; i++) {
        var txt = this.inputs[i];
        var _status = this.checkPhoneStatus(txt);
        var retVal = true;
        switch (_status) {
            case ("match"):
                if (this.OnClickMatch)
                    retVal = this.OnClickMatch(txt.id);
                break;
            case ("empty"):
            case ("OnTheWay"):
            case ("faild"):
                if (this.OnClickFaild)
                    retVal = this.OnClickFaild(txt.id);
                break;
        }
        return retVal;
    }
}
np_phone.prototype.check_phone = function () {
    var IsValid = false;
    for (var i = 0; i < this.inputs.length; i++) {
        var txt = this.inputs[i];
        var _status = this.checkPhoneStatus(txt);
        switch (_status) {
            case ("match"):
                IsValid = true;
                break;
            case ("empty"):
            case ("OnTheWay"):
            case ("faild"):
                return false;
                break;
        }
    }
    return IsValid;
}
np_phone.prototype.checkPhoneStatus = function (_text) {
    var _txt = $.trim($(_text).val());
    $(_text).val(_txt);
    if (_txt.length == 0)
        return "empty";
    else if (this.reg.test(_txt))
        return "match";
    for (var j = 0; j < this.OnTheWay.length; j++) {
        if (this.OnTheWay[j].test(_txt))
            return "OnTheWay";
    }
    return "faild";
}

function SetRegExGroup(list) {
    var _list = [];
    for (var i = 0; i < list.length; i++) {
        _list.push(new RegExp(list[i]));
    }
    return _list;
}