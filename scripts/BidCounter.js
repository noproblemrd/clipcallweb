﻿function Counter() {
    this.nums = [];
    this._divs = [];
    this.format="background";

    this.className = '';

}
//var __counter;
Counter.prototype = {
    constructor: Counter,
    Init: function (div_counterId, className) {
        //    __counter = this;
        this._div = $('.' + div_counterId);

        for (var i = 0; i < 7; i++) {
            var _div = $("<div>");
            if (className)
                _div.addClass(className);
            _div.appendTo(this._div);
            this._divs.push(_div);
            if (i == 0 || i == 3) {
                $("<div>").addClass("div_comma").html(',').appendTo(this._div);
            }
        }
        //    $("<div>").addClass("counterBox").addClass("counter_text").html("Phone leads so far").appendTo(this._div);
        //  this.SetNumInDivs(num);
        this.SetNum();
        clearInterval(IntervalCounter);
        IntervalCounter = setTimeout(function () {
            SetNum();
        }, 60000);

    },
    SetNumInDivs: function (num) {

        this.nums.length = 0;
        for (var i = num.length - 1; i > -1; i--) {
            this.nums.push(parseInt(num.charAt(i)));
        }
        for (var i = 0; i < this.nums.length; i++) {
            var _number = (this.nums[i] == 0) ? 9 : this.nums[i] - 1;
            var _length = this._divs.length - 1;
            $(this._divs[_length - i]).css('backgroundPosition', (_number * -35) + 'px 0');
        }

    },

    SetNumInDivs2: function (num) {

        this.nums.length = 0;
        for (var i = num.length - 1; i > -1; i--) {
            this.nums.push(parseInt(num.charAt(i)));
        }
        for (var i = 0; i < this.nums.length; i++) {
            var _number = this.nums[i];
            var _length = this._divs.length - 1;

            $(this._divs[_length - i]).text(_number);

        }

        $('.div_comma').show();
    },
    SetNum: function () {

        var _this = this;
        $.ajax({
            url: ROOT + "Consumer/SalesConsumer.asmx/GetTotalCaseCount",
            data: "{}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {

                if (data.d == "faild")
                    return;
                //alert(_counter.format);
                if (_counter.format == "regular")
                    _this.SetNumInDivs2(data.d);
                else
                    _this.SetNumInDivs(data.d);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //alert(textStatus + " " + errorThrown);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
            }

        });

    }

};
function SetNum() {
    clearInterval(IntervalCounter);


    _counter.SetNum();
    IntervalCounter = setTimeout(function () {
        SetNum();
    }, 60000);
}
var _counter = new Counter();
var IntervalCounter;
$(function () {

    _counter.Init('counterBox', 'div_counter');
});

