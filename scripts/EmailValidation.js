﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />

function np_email() {
    this.inputs = [];
    this.buttons = [];
    this.OnTheWay = SetRegExGroup(["^\\w+[-+.']?$",
                    "^\\w+([-+.']\\w+)*[-+.']?$",
                    "^\\w+([-+.']\\w+)*@$",
                    "^\\w+([-+.']\\w+)*@\\w+[-.]?$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*[-.]?$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{1,2}$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{2,}[-.]?$",
                    "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{2,}([-.]\\w+)*[-.]?$"]);
    this.reg = new RegExp("^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w{2,}([-.]\\w+)*$");
}
np_email.prototype.init = function () {
    var _this = this;
    $(function () {
        $('input[text-mode="email"]').each(function () {

            $(this).keyup(function (e) {
                var keynum;
                if (window.event) // IE            
                    keynum = e.keyCode
                else if (e.which) // Netscape/Firefox/Opera            
                    keynum = e.which
                if (keynum == 13) {
                    $(this).blur();
                    _this.onclick();
                    return;
                }
                var _status = _this.checkEmailStatus(this);
                switch (_status) {
                    case ("empty"):
                        if (_this.OnKeyUpEmpty)
                            _this.OnKeyUpEmpty(this.id);
                        break;
                    case ("match"):
                        if (_this.OnMatch)
                            _this.OnMatch(this.id);
                        break;
                    case ("OnTheWay"):
                        if (_this.OnKeyUpOnTheWay)
                            _this.OnKeyUpOnTheWay(this.id);
                        break;
                    case ("faild"):
                        if (_this.OnFaild)
                            _this.OnFaild(this.id);
                        break;
                }
            });
            $(this).blur(function () {
                var _status = _this.checkEmailStatus(this);
                switch (_status) {
                    case ("empty"):
                        if (_this.OnKeyUpEmpty)
                            _this.OnKeyUpEmpty(this.id);
                        break;
                    case ("match"):
                        if (_this.OnMatch)
                            _this.OnMatch(this.id);
                        break;
                    case ("OnTheWay"):
                    case ("faild"):
                        if (_this.OnFaild)
                            _this.OnFaild(this.id);
                        break;
                }
            });
            $(this).focus(function () {
                if (_this.OnFocus)
                    _this.OnFocus(this.id);
            });
            _this.inputs.push(this);
        });
        $('a[button-mode="email"]').each(function () {
            _this.buttons.push(this);
            var thisButton = this;
            $(this).click(function () {
                _this.onclick();
            });
        });
    });
}
np_email.prototype.onclick = function () {
    for (var i = 0; i < this.inputs.length; i++) {
        var txt = this.inputs[i];
        var _status = this.checkEmailStatus(txt);
        switch (_status) {
            case ("match"):
                if (this.OnClickMatch)
                    this.OnClickMatch(txt.id);
                break;
            case ("empty"):
            case ("OnTheWay"):
            case ("faild"):
                if (this.OnClickFaild)
                    this.OnClickFaild(txt.id, _status);
                else if (this.OnFaild)
                    this.OnFaild(txt.id);
                break;
        }
    }
}
/*
np_email.prototype.GetAllStatus = function () {
    var result = [];
    for (var i = 0; i < this.inputs.length; i++) {
        var _id = this.inputs[i].id;
        var _response = this.checkEmailStatus(this.inputs[i]);
        switch (_response) {
            case ("empty"):
                if (_this.OnEmpty)
                    _this.OnEmpty(this.id);
                
                break;
            case ("match"):
                if (_this.OnMatch)
                    _this.OnMatch(this.id);
                break;
            case ("OnTheWay"):
            case ("faild"):
                
                if (_this.OnFaild)
                    _this.OnFaild(this.id);
                break;
        }
        result.push([_id, _response]);
    }
    return result;
}
*/
np_email.prototype.checkEmailStatus = function (text_email) {
    var email = $.trim($(text_email).val());
    $(text_email).val(email);
    if (email.length == 0)
        return "empty";
    else if (this.reg.test(email))
        return "match";
    for (var j = 0; j < this.OnTheWay.length; j++) {
        if (this.OnTheWay[j].test(email))
            return "OnTheWay";
    }
    return "faild";
}

function SetRegExGroup(list) {
    var _list = [];
    for (var i = 0; i < list.length; i++) {
        _list.push(new RegExp(list[i]));
    }
    return _list;
}