﻿function GetStringForJson(str) {
    var new_str = '';
    for (var i = 0; i < str.length; i++) {
        if (str.charAt(i) == "'")
            new_str += "\'";
        else if (str.charAt(i) == '"')
            new_str += '\\"';
        else
            new_str += str.charAt(i);
    }
    return escape(new_str);
}