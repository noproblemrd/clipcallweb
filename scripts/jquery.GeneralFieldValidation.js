﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />

function np_inputs() {
    this.inputs = [];

}
var __inputs;
$(function () {
    __inputs = new np_inputs();
    $('[text-mode|="FieldEmpty"]').each(function () {
        var $div_parent = $(this).closest('div');
        $div_parent.addClass('inputParent');
        var $div_text = $('<div>');
        $div_text.addClass('inputSun');

        $(this).remove();
        $div_text.append($(this));
        $div_parent.append($div_text);
        $div_parent.append($('<div>').addClass('inputValid').css('display', 'none'));
        $div_parent.append($('<div>').addClass('inputError').css('display', 'none'));

        $(this).blur(function () {
            var _txt = $.trim($(this).val());
            $(this).val(_txt);
            $(this).removeClass('textValid');
            $(this).removeClass('textError');
            if (_txt == 0) {
                $(this).closest('.inputParent').find('.inputValid').hide();
                $(this).closest('.inputParent').find('.inputError').hide();

            }
            else {
                $(this).closest('.inputParent').find('.inputValid').show();
                $(this).closest('.inputParent').find('.inputError').hide();
                $(this).addClass('textValid');
            }
        });
        $(this).focus(function () {
            $(this).removeClass('textValid');
            $(this).removeClass('textError');
            $(this).closest('.inputParent').find('.inputValid').hide();
            $(this).closest('.inputParent').find('.inputError').hide();
        });
        __inputs.inputs.push(this);
    });
});
np_inputs.prototype.CheckAll = function () {
    var NotValid = [];
    for (var i = 0; i < this.inputs.length; i++) {
        var _txt = $.trim($(this.inputs[i]).val());
        $(this.inputs[i]).val(_txt);
        $(this.inputs[i]).removeClass('textValid');
        $(this.inputs[i]).removeClass('textError');
        if (_txt == 0) {
            $(this.inputs[i]).closest('.inputParent').find('.inputValid').hide();
            $(this.inputs[i]).closest('.inputParent').find('.inputError').show();
            NotValid.push(this.inputs[i].id);
        }
        else {
            $(this.inputs[i]).closest('.inputParent').find('.inputValid').show();
            $(this.inputs[i]).closest('.inputParent').find('.inputError').hide();
            $(this.inputs[i]).addClass('textValid');
        }
    }
    return NotValid;
}