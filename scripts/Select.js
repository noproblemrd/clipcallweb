﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />
function NewSelection() {
    this.elems = [];
}
function SelectItem() {
    this.span1 = {};
    this.span2 = {};
    this.a = {};
    this.$element = {};
    this.Width = 250;
   
    this.WidthSelect = function () {
        return this.Width - 44;
    };
    this.WidthUl = function () {
        return this.Width;
    };
}
$(function () {
    var selections = new NewSelection();
    $('select').each(function () {
        var _this = this;
        var _item = new SelectItem();
        var _width = $(this).attr('width');
        if (_width && _width.length > 0 && !isNaN(_width))
            _item.Width = parseInt(_width);
        var _radius = $(this).attr('radius');
        if (!_radius || _radius.length == 0  || isNaN(_radius))
            _radius = null;
        var _MaxHeight = $(this).attr('mheight');
        if (!_MaxHeight || _MaxHeight.length == 0 || isNaN(_MaxHeight))
            _MaxHeight = null;
       

        var $element = $('<div style="display:relative;"><a class="selectBox selectBox-dropdown" style="display: inline-block; -moz-user-select: none; width: ' + _item.WidthSelect() + 'px;" title="" tabindex="0">' +
                        '<span  class="selectBox-label"></span>' +
                        '<span class="selectBox-arrow"></span></a>' +
                        '<ul class="selectBox-dropdown-menu selectBox-options" style="-moz-user-select: none; width: ' + _item.WidthUl() + 'px;display:none;">');
        var $spabn_select = $element.find('span.selectBox-label');
        var $ul_container = $element.find('ul.selectBox-dropdown-menu');
        if (_MaxHeight)
            $ul_container.css('maxHeight', _MaxHeight + 'px');
        if (_radius)
            $element.find('a.selectBox').css('borderRadius', _radius + 'px');
        _item.a = $element.find('a.selectBox-dropdown');
        _item.span1 = $spabn_select;
        _item.span2 = $element.find('span.selectBox-arrow');
        _item.$element = $element;
        $element.find('a.selectBox-dropdown').click(function () {
            if ($ul_container.is(":visible"))
                $ul_container.hide();
            else
                $ul_container.show();
        });

        for (var i = 0; i < this.length; i++) {
            var _optionV = this.options[i].value;
            var _optionT = this.options[i].text;
            var _class = ''
            if (this.options[i].selected) {
                $spabn_select.html(_optionT);
                _class = 'selectBox-selected';
            }
            var $optionLi = $('<li>');
            if (_class.length > 0)
                $optionLi.addClass(_class);
            var $optionA = $('<a href="javascript:void(0);" val="' + _optionV + '">' + _optionT + '</a>');
            $optionA.click(function () {
                //         var _index = i;
                var _text = $(this).html();
                var _val = $(this).attr('val');
                $spabn_select.html(_text);
                $ul_container.find('li').each(function () {
                    $(this).removeClass('selectBox-selected');
                });
                $(this).parents('li').addClass('selectBox-selected');
                //       if (OnSelectClick)
                //           OnSelectClick(_val);
                for (var j = 0; j < _this.length; j++) {
                    if (_this.options[j].value == _val) {
                        _this.selectedIndex = j;
                        break;
                    }
                }
                //        _this.selectedIndex = _index;
            });
            $optionLi.append($optionA);
            $ul_container.append($optionLi);

        }
        selections.elems.push(_item);
        $(this).hide();
        $element.insertBefore($(this));
    });
    $(document).bind('click', function (ev) {
        for (var i = 0; i < selections.elems.length; i++) {
            if (ev.target != selections.elems[i].span1.get(0) && ev.target != selections.elems[i].span2.get(0) && ev.target != selections.elems[i].a.get(0)) {
                selections.elems[i].$element.find('ul').hide();
            }
        }


    });
});