﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />

function ValidationComment(comment, id, class_valid, class_error, ShowValidFace) {
    //alert("ValidationComment");
    this.comment = comment;
    this.id = id;
    this.CommentVisible = false;
    this.class_valid = class_valid;
    this.class_error = class_error;     
    this.load(ShowValidFace);
   
}
ValidationComment.prototype.load = function (ShowValidFace) {
    //alert("load");
    var _this = this;
    $(function () {
        _this.$valid = (ShowValidFace) ? $('<div class="inputValid" style="display:none;">') : $();
        _this.$error = (ShowValidFace) ? $('<div class="inputError" style="display:none;">') : $();
        _this.$comment = $('<div class="inputComment" style="display:none;">');
        _this.$comment.append($('<span>').html(_this.comment));
        _this.$comment.append($('<div class="chat-bubble-arrow">'));
        var txt = document.getElementById(_this.id);
        if(_this.$valid.length > 0)
            (txt).parentNode.insertBefore(_this.$valid.get(0), txt);
        if (_this.$error.length > 0)
            (txt).parentNode.insertBefore(_this.$error.get(0), txt);
        (txt).parentNode.insertBefore(_this.$comment.get(0), txt);
        $(txt).blur(function () {
            if ($(txt).val().length == 0)
                _this.RemoveAllValidations();
        });
        $(txt).focus(function () {            
            _this.RemoveAllValidations();
        });
    });
}
ValidationComment.prototype.error = function () {
    //alert("there is error");
    if (this.CommentVisible == false) {
        this.CommentVisible = true;       
        this.$comment.show("slide", { direction: "down" }, 250);
    }
    this.$error.show();
    this.$valid.hide();
    $('#' + this.id).addClass(this.class_error);
    $('#' + this.id).removeClass(this.class_valid);
}
ValidationComment.prototype.RemoveAllValidations = function () {
    //alert("2 there is error");
    if (this.CommentVisible == true) {
        this.$comment.hide("slide", { direction: "down" }, 250);
        this.CommentVisible = false;
    }
    this.$error.hide();
    this.$valid.hide();
    $('#' + this.id).removeClass(this.class_error);
    $('#' + this.id).removeClass(this.class_valid);
    
}
ValidationComment.prototype.valid = function () {
    this.RemoveAllValidations();
    this.$valid.show();
    $('#' + this.id).addClass(this.class_valid);
}