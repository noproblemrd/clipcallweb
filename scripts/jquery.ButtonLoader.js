﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />
$(function () {
    $('[button-loader|="true"]').each(function () {
        var $div = $('<div>').css({ width: (this.offsetWidth + 100) + "px",
            height: (this.scrollHeight + 2) + "px",
            backgroundImage: "url('images/ajax-loader.gif')",
            backgroundPosition: (this.offsetWidth + 15) + "px center",
            backgroundRepeat: 'no-repeat',
            position: 'absolute',
            display: 'none'
        });
        $(this).parent().append($div);
        this.ServerModeShow = function () {
            $div.css({ top: (this.offsetTop - 1) + "px",
                left: (this.offsetLeft - 1) + "px"
            });
            $div.show();
        }
        this.ServerModeHide = function () {
            $div.hide();
        }
    });
});