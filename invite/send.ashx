﻿<%@ WebHandler Language="C#" Class="send" %>

using System;
using System.Web;

public class send : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();
        HttpRequest Request = context.Request;
        HttpResponse Response = context.Response;

        context.Response.ContentType = "application/x-javascript; charset=UTF-8;"; //"application/json; charset=UTF-8;";
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

        string ii = Request.QueryString["invitationId"];
        string phone = Request.QueryString["phone"];
        Guid invitationId;
        if (!Guid.TryParse(ii, out invitationId))
            invitationId = Guid.Empty;
        InviteService _is = new InviteService();
        var _response = _is.send(invitationId, phone);
        _response = "responseSendInvitation(" + _response + ")";
        Response.StatusCode = 200;
        Response.Write(_response);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}