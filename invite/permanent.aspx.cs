﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class invite_permanent : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Guid supplierId;
        if(!Guid.TryParse(Request.QueryString["supplierid"], out supplierId))
        {
            GeneralRedirect("QueryString: " + Request.Url.Query);
            //Response.Redirect(R"")DownloadApp.aspx
            return;
        }
        WebReferenceSite.Site  _site = WebServiceConfig.GetSiteReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSite.ResultOfString result = null;
        try
        {
            result = _site.CreateVideoLeadInvitation(supplierId);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            GeneralRedirect(exc.Message);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure || string.IsNullOrEmpty(result.Value))
        {
            GeneralRedirect(string.Format("Failur, SupplierId: {0}, Message: {1}, Value: {2}", supplierId, result.Messages, result.Value));
            return;
        }
        Response.Redirect(result.Value);
    }
    private void GeneralRedirect(string str)
    {

        dbug_log.ExceptionLog(new Exception(str));
        Response.Redirect(ResolveUrl("~") + "tm/DownloadApp.aspx");
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "al", "alert('" + str + "');", true);
    }
}