﻿<%@ WebHandler Language="C#" Class="create" %>

using System;
using System.Web;

public class create : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();
        HttpRequest Request = context.Request;
        HttpResponse Response = context.Response;

        context.Response.ContentType = "application/x-javascript; charset=UTF-8;"; //"application/json; charset=UTF-8;";
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

        string si = Request.QueryString["supplierId"];
        string url = Request.QueryString["url"];
        Guid supplierId;
        if (!Guid.TryParse(si, out supplierId))
            supplierId = Guid.Empty;
        InviteService _is = new InviteService();
        var _response = _is.create(supplierId, url);
        _response = "responseCreateInvitation(" + _response + ")";
        Response.StatusCode = 200;
        Response.Write(_response);
            
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}