﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="host.aspx.cs" Inherits="invite_host" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="host.css" rel="stylesheet" type="text/css" />
    <title></title>
    <script type="text/javascript" src="invite.ashx"></script>
    <script type="text/javascript">
        var clipcallSetting = {
            elementId: "clipcall",
            SupplierId: '<%# defaultAccountId %>'
        }
        clipcall.set(clipcallSetting);
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="title">
            <h2>ClipCall widget  – Try me!</h2>
        </div>
        <div id="clipcall"></div>
        <div class="sample">
            <h3>Web site script for registered professionals only!</h3>
            <div class="code">
                <code>
&#60;head&#62;<br />
&#60;script type&#61;&#34;text&#47;javascript&#34; src&#61;&#34;https&#58;&#47;&#47;nps&#46;clipcall&#46;it&#47;invite&#47;invite&#46;ashx&#34;&#62;&#60;&#47;script&#62;<br />
&#60;script type&#61;&#34;text&#47;javascript&#34;&#62;<br />
&nbsp;&nbsp;&nbsp;&nbsp;&#9;var clipcallSetting &#61; 
&#123;<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9;&#9;elementId&#58; &#39;clipcall&#39;&#44;<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9;&#9;SupplierId&#58; &#39;&#91;to get your ID&#44; 
contact us support&#64;clipcall&#46;it&#93;&#39;<br />
&nbsp;&nbsp;&nbsp;&nbsp;&#9;&#125;&#59;<br />
&nbsp;&nbsp;&nbsp;&nbsp;&#9;clipcall&#46;set&#40;clipcallSetting&#41;&#59;<br />
&#60;&#47;script&#62;<br />
&#60;&#47;head&#62;<br />
&#60;body&#62;<br />    
&nbsp;&nbsp;&nbsp;&nbsp;&#60;div id&#61;&#34;clipcall&#34;&#62;&#60;&#47;div&#62;<br />
&#60;&#47;body&#62;                    
                </code>                
            </div>
        </div>
    </div>
    </form>
</body>
</html>
