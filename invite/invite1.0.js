﻿(function () {
    window.clipcall = {
        _elementId: null,
        _element: null,
        setLoadComplete: function (setObject) {

            if (!setObject || !setObject.elementId) {
                clipcall.NoElementFound('');
                return;
            }
            clipcall._elementId = setObject.elementId;
            clipcall._element = document.getElementById(setObject.elementId);
            if (!clipcall._element) {
                clipcall.NoElementFound(setObject.elementId);
                return;
            }
            if (!setObject.SupplierId || setObject.SupplierId.length == 0) {
                console.log("supplierId bad argument - ", setObject.SupplierId);
                return;
            }
            var _iframe = document.createElement("iframe");
            _iframe.setAttribute("height", "70px");//clipcall._element.offsetHeight);
            _iframe.setAttribute('scrolling', 'no');
            _iframe.setAttribute('frameBorder', '0');
            _iframe.setAttribute('allowTransparency', 'true');
            _iframe.setAttribute('style', 'display: inline !important;');
            _iframe.setAttribute("width", clipcall._element.offsetWidth);
            _iframe.src = "{domain}/invite/inv.html?si=" + setObject.SupplierId;
            clipcall._element.appendChild(_iframe);
        },
        set: function (setObject) {
            if (document.readyState == 'complete')
                clipcall.setLoadComplete(setObject);
            else if (window.addEventListener) {
                window.addEventListener('load', function () {
                    clipcall.setLoadComplete(setObject);
                }
                    , false); //W3C
            }
            else {
                window.attachEvent('onload', function () {
                    clipcall.setLoadComplete(setObject);
                }); //IE
            }
        },
        NoElementFound: function (elementId) {
            console.log("Element not founded - ", elementId);
        }
    }
})();