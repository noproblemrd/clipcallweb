﻿<%@ WebHandler Language="C#" Class="invite" %>

using System;
using System.Web;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.IO.Compression;

public class invite : IHttpHandler {

    private const string GZIP = "gzip";
    private const string DEFLATE = "deflate";
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();
        HttpRequest Request = context.Request;
        HttpResponse Response = context.Response;

        context.Response.ContentType = "application/x-javascript; charset=UTF-8;"; //"application/json; charset=UTF-8;";
        PpcSite _cache = PpcSite.GetCurrent();
       
        TimeSpan tm = new TimeSpan(12, 0, 0);//0.5 Day
        DateTime _expire = DateTime.UtcNow.AddTicks(tm.Ticks);


        Response.Cache.SetCacheability(HttpCacheability.Public);
        Response.Cache.SetExpires(_expire);

        Response.Cache.SetMaxAge(tm);
        //     Response.Cache.SetLastModified(_cache.DateModified);
        Response.Cache.SetLastModified(_cache.DateModified);

        Response.Cache.SetSlidingExpiration(true);


        //   string newEtag = (!IsFromToday) ? SetUser(context) : _etag;
        //    Response.Cache.SetETag(newEtag);
        context.Response.Filter = new GZipStream(context.Response.Filter, CompressionMode.Compress);
        context.Response.AppendHeader("Content-encoding", GZIP);
        Response.StatusCode = 200;
        Response.Write(_cache.ClipCallWebInvite);

    }
    void Send304(HttpResponse Response, DateTime DateModified)
    {
        DateTime _expire = DateTime.UtcNow.AddHours(1);
        Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
        Response.Cache.SetExpires(_expire);
        Response.Cache.SetOmitVaryStar(true);

        Response.Cache.SetMaxAge(new TimeSpan(1, 0, 0));
        //       Response.Cache.AppendCacheExtension("must-revalidate, proxy-revalidate");

        Response.Cache.SetLastModified(DateModified);
        Response.StatusCode = 304; // not changed
        Response.SuppressContent = true;
        Response.End();
    }
    string SetUser(HttpContext context)
    {
        /*
        string _OriginId = context.Request.QueryString["OriginId"];
        Guid OriginId;
        if (!Guid.TryParse(_OriginId, out OriginId))
            OriginId = Guid.Empty;
        string command = "dbo.InsertAddonClient";// @OriginId, @IP, @ID output\r\n SELECT @ID";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        SqlParameter Origin_Id = new SqlParameter("@OriginId", OriginId);
        Origin_Id.Direction = System.Data.ParameterDirection.Input;
        cmd.Parameters.Add(Origin_Id);
        SqlParameter _ip = new SqlParameter("@IP", Utilities.GetIP(context.Request));
        _ip.Direction = System.Data.ParameterDirection.Input;
        cmd.Parameters.Add(_ip);
        SqlParameter _id = new SqlParameter("@ID",  System.Data.SqlDbType.UniqueIdentifier);
        _id.Direction = System.Data.ParameterDirection.Output;
        cmd.Parameters.Add(_id);
        cmd.ExecuteNonQuery();
        Guid ID = (Guid)_id.Value;       
        conn.Close();
        return ID.ToString() + ":" + SalesUtility.GetEtagDateString(DateTime.UtcNow);
         * */
        return "\"" + Guid.NewGuid().ToString() + ":" + SalesUtility.GetEtagDateString(DateTime.UtcNow) + "\"";

    }

    private void CompressResponse(HttpContext context)
    {
        try
        {
            if (context.Request.Headers["Accept-encoding"] != null && context.Request.Headers["Accept-encoding"].Contains(DEFLATE)
            || context.Request.ServerVariables["HTTP_ACCEPT_ENCODING"] != null && context.Request.ServerVariables["HTTP_ACCEPT_ENCODING"].Contains(DEFLATE))
            { // prefer Deflate. It doesn't require checksums, and is therefore smaller and faster.
                context.Response.Filter = new DeflateStream(context.Response.Filter, CompressionMode.Compress);
                context.Response.AppendHeader("Content-encoding", DEFLATE);
            }
            else if (context.Request.Headers["Accept-encoding"] != null && context.Request.Headers["Accept-encoding"].Contains(GZIP)
          || context.Request.ServerVariables["HTTP_ACCEPT_ENCODING"] != null && context.Request.ServerVariables["HTTP_ACCEPT_ENCODING"].Contains(GZIP))
            {
                context.Response.Filter = new GZipStream(context.Response.Filter, CompressionMode.Compress);
                context.Response.AppendHeader("Content-encoding", GZIP);
            }
        }
        catch { }

    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}