﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class invite_host : System.Web.UI.Page
{
    private Guid accountId;
    protected string defaultAccountId
    {
        get { return accountId.ToString(); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        accountId = PpcSite.GetCurrent().InviteDefaultAccountId;
        Header.DataBind();
    }
}