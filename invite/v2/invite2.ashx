﻿<%@ WebHandler Language="C#" Class="invite2" %>

using System;
using System.Web;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.IO.Compression;

public class invite2 : IHttpHandler {
    private const string GZIP = "gzip";
    private const string DEFLATE = "deflate";
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();
        HttpRequest Request = context.Request;
        HttpResponse Response = context.Response;

        context.Response.ContentType = "application/x-javascript; charset=UTF-8;"; //"application/json; charset=UTF-8;";
        PpcSite _cache = PpcSite.GetCurrent();

        TimeSpan tm = new TimeSpan(12, 0, 0);//0.5 Day
        DateTime _expire = DateTime.UtcNow.AddTicks(tm.Ticks);


        Response.Cache.SetCacheability(HttpCacheability.Public);
        Response.Cache.SetExpires(_expire);

        Response.Cache.SetMaxAge(tm);
        //     Response.Cache.SetLastModified(_cache.DateModified);
        Response.Cache.SetLastModified(_cache.DateModified);

        Response.Cache.SetSlidingExpiration(true);


        //   string newEtag = (!IsFromToday) ? SetUser(context) : _etag;
        //    Response.Cache.SetETag(newEtag);
        context.Response.Filter = new GZipStream(context.Response.Filter, CompressionMode.Compress);
        context.Response.AppendHeader("Content-encoding", GZIP);
        Response.StatusCode = 200;
        Response.Write(_cache.ClipCallWebInvite2);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}