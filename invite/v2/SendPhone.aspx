﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendPhone.aspx.cs" Inherits="invite_v2_SendPhone" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style.css" rel="stylesheet" type="text/css" />
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript">
        var invitationId = '<%# InvitationId %>';
        var domainPath;
        $(function () {
            domainPath = GetDomainPath();
            $('#send').click(function () {
                sendSms();
            });
            $('.phone').focus(function () {
                $(this).removeClass('notvalid');
            });
            $('.phone').blur(function () {
                if (!checkValidation())
                    $(this).addClass('notvalid');
            });
            $('.phone').keypress(function () {
                var code;
                if (!e) var e = window.event;
                if (e.keyCode) code = e.keyCode;
                else if (e.which) code = e.which;
                if (code == 13) {
                    sendSms();
                    return false;
                }
            });
        });
        function sendSms() {
            if (!invitationId || invitationId.length == 0) {
                $('#step1').show();
                $('#step2').hide();
                $('#divLoader').hide();
                return;
            }
            if (!checkValidation())
                return;
            $('.divLoader').show();
            var phone = $('.phone').val();
            var _url = domainPath + "send.ashx?invitationId=" + invitationId + "&phone=" + phone;
            CreateRequest(_url);
        }
        function GetDomainPath() {
            var result = window.location.protocol + '//';
            var domain = window.location.host.split('.');
            for (var i = 0; i < domain.length; i++) {
                result += (i == 0 && domain[i] == 'nps') ? 'app.' : (domain[i] + '.');
            }
            result = result.substr(0, (result.length - 1));
            result += "/invite/";
            return result;
        }
        function responseSendInvitation(response) {
            $('.divLoader').hide();
            $('#step1').hide();
            $('#step2').show();
        }
        function CreateRequest(url) {
            var _script = document.createElement('script');
            _script.type = "text/javascript";
            _script.src = url;
            _script.async = true;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(_script, s);
        }
        function checkValidation() {
            var $phone = $('.phone');
            var phone = $phone.val();
            var reg = new RegExp(/^[0-9]{10}$/);
            if (!reg.test(phone)) {
                $phone.addClass('notvalid');
                return false;
            }
            return true;
        }
        /*
        function GetSupplierId() {
            var _query = window.location.search.toLowerCase();
            if (_query.length == 0)
                return null;
            if (_query.charAt(0) == '?')
                _query = _query.substring(1, _query.length);
            var qs = _query.split('&');
            for (var i = 0; i < qs.length; i++) {
                var keyvalue = qs[i].split('=');
                if (keyvalue.length != 2)
                    continue;
                if (keyvalue[0] == "si")
                    return keyvalue[1];
            }
            return null;
        }*/
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="step1" >
            <div class="row  row-top">
                <h2>Shoot a video of the service needed</h2>
            </div>
            <div class="row row-mar25">
                <div class="col">
                    <div class="div_v div_v_left">
                        <img src="../img/icon-v.png" alt="V" class="icon-v" />
                        <span>Better estimation</span>
                    </div>
                    <div class="div_v">
                        <img src="../img/icon-v.png" alt="V" class="icon-v" />
                        <span>More accurate quote</span>
                    </div>
                </div>
            </div>
            <div class="row div_mid">
                
                <div class="col"  >
                    <h3 style="text-align:left; margin-bottom:10px;">Get the app:</h3>
                    <div class="content">
                        <input class="phone" type="text" placeholder="Enter mobile phone" />
                        <a href="javascript:void(0);" id="send">Send</a>
                    </div>
                    <div style="text-align:left;"><span class="wont-share">we won’t share your number</span></div>
                </div>
                <!--
                <div style="margin-left:110px;">
                    <img src="../img/widget-1.png" alt="mobile" />
                </div>
                <div class="mar25">
                     <img src="../img/widget-next.png" alt=">" />
                </div>
                <div class="mar25">
                    <img src="../img/widget-2.png" alt="video" />
                </div>
                <div class="mar25">
                    <img src="../img/widget-next.png" alt=">" />
                </div>
                <div class="mar25">
                    <img src="../img/widget-3.png" alt="phone" />
                </div>
                -->
            </div>
            <!--
            <div class="row row-mar25">            
                
            </div>
           -->
            <div class="row div_bottom">
                <div class="widget123"></div>
            </div>
              <div class="row row-titles">
                <span style="margin-left:40px;">Install the<br />ClipCall app</span>
                <span style="margin-left:106px;">Shoot a video<br />of your project</span>
                <span style="margin-left:66px;">We will review it and<br />contact you ASAP</span>
            </div>
        </div>
       <div id="step2" style="display:none;">
            <div class="row" style="margin-top:130px;" >
                <img src="../img/widget-v.png" alt="V" class="icon-v" style="margin: 0 auto;" />
            </div>
            <div class="row row-mar25">
                <h2>Thank you!</h2>
            </div>
           <div class="row row-mar25">
                <div class="col"><span class="span-last-mid">Check your phone, we are texting you<br />an exclusive installation link.</span></div>
            </div>
           <div class="row row-mar25">
               <div class="col"><span class="span-last-bottom">We are waiting for your video.<br />Make sure you give the most accurate description possible<br />and get a tailor made quote back ASAP.</span></div>
            </div>
        </div>
        <div class="divLoader">
            <table>
                <tr>
                    <td><img src="../../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
