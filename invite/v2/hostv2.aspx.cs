﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class invite_v2_hostv2 : System.Web.UI.Page
{
    private Guid accountId;
    protected string defaultAccountId
    {
        get { return accountId.ToString(); }
    }
    protected string invite2JS { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        accountId = PpcSite.GetCurrent().InviteDefaultAccountId;
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        string domain = xdoc.Element("Sites").Element("clipcall").Element("WebInvite").Element("domain").Value;
        invite2JS = domain + "/invite/v2/invite2.ashx";
        Header.DataBind();
    }
}