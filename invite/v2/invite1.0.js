﻿(function () {
    window.clipcall = {
        _elementId: null,
        _element: null,
        cdnDomain: '{domain}',
        appDomain: null,
        divBackground: null,
        divIframe: null,
        xClose: null,
        iframeHeight: 520,
        iframeWidth: 680,
        setLoadComplete: function (setObject) {

            if (!setObject || !setObject.elementId) {
                clipcall.NoElementFound('');
                return;
            }
            clipcall._elementId = setObject.elementId;
            clipcall._element = document.getElementById(setObject.elementId);
            if (!clipcall._element) {
                clipcall.NoElementFound(setObject.elementId);
                return;
            }
            if (!setObject.SupplierId || setObject.SupplierId.length == 0) {
                console.log("supplierId bad argument - ", setObject.SupplierId);
                return;
            }
            appDomain = clipcall.GetDomainPath();
            clipcall.LoadCss();
            clipcall.LoadBackgroundDiv();

            var divContent = document.createElement('div');
            divContent.className = 'cc_content';

            var divSpanContent = document.createElement('div');
            divSpanContent.className = 'cc_DivSpanContent';
            var spanContentTop = document.createElement('span');
            spanContentTop.className = 'cc_span_content_top';
            spanContentTop.textContent = '*** Get 10% off ***';
            divSpanContent.appendChild(spanContentTop);

            var spanContent = document.createElement('span');
            spanContent.className = 'cc_span_create';
            spanContent.textContent = 'Show what you need';
            divSpanContent.appendChild(spanContent);

            divContent.appendChild(divSpanContent);

            var aCreate = document.createElement('a');
            aCreate.className = 'cc_a_create';
            aCreate.href = 'javascript:void(0);';
            aCreate.textContent = 'Shoot a video';
            divContent.appendChild(aCreate);

            var divCol = document.createElement('div');
            divCol.className = 'cc_col';
            divCol.appendChild(divContent);

            var divRow = document.createElement('div');
            divRow.className = 'cc_row';
            divRow.appendChild(divCol);
            
            if (aCreate.addEventListener) {
                aCreate.addEventListener('click', function () {
                    clipcall.ClickShootVideo(setObject.SupplierId);
                }, false);
            }
            else {
                aCreate.attachEvent('onclick', function () {
                    clipcall.ClickShootVideo(setObject.SupplierId);
                });
            }
            clipcall._element.appendChild(divRow);
        },
        set: function (setObject) {
            if (document.readyState == 'complete')
                clipcall.setLoadComplete(setObject);
            else if (window.addEventListener) {
                window.addEventListener('load', function () {
                    clipcall.setLoadComplete(setObject);
                }
                    , false); //W3C
            }
            else {
                window.attachEvent('onload', function () {
                    clipcall.setLoadComplete(setObject);
                }); //IE
            }
        },
        NoElementFound: function (elementId) {
            console.log("Element not founded - ", elementId);
        },
        LoadCss: function () {
            var css = document.createElement('link');//<link href="style.css" rel="stylesheet" type="text/css"/>
            css.href = clipcall.cdnDomain + '/invite/v2/cc_style.css';
            css.setAttribute('rel', 'stylesheet');
            css.setAttribute('type', 'text/css');
            (document.getElementsByTagName('head')[0]).appendChild(css);
        },
        ClickShootVideo: function (supplierId) {
            clipcall.ShowBackgroundDiv();
            var requestUrl = appDomain + 'v2/create2.ashx?supplierId=' + supplierId + '&url=' + encodeURIComponent(document.location.href);
            clipcall.CreateRequest(requestUrl);
           
        },      
        responseCreateInvitation: function (responseObj) {
         //   $('.divLoader').hide();
            if (!responseObj || !responseObj.invitationId || responseObj.invitationId.length == 0) {
                clipcall.HideBackgroundDiv();
                return;
            }
            clipcall.LoadIframe(responseObj.invitationId);            
            clipcall.LoadXClose();

         //   invitationId = response.invitationId;
        //    $('#step1').hide();
        //    $('#step2').show();
        },
        CreateRequest: function (url) {
            var _script = document.createElement('script');
            _script.type = "text/javascript";
            _script.src = url;
            _script.async = true;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(_script, s);
        },
        LoadIframe: function (invitationId) {
            var _iframe = document.createElement("iframe");
            _iframe.setAttribute("height", clipcall.iframeHeight + "px");//clipcall._element.offsetHeight);
            _iframe.setAttribute('scrolling', 'no');
            _iframe.setAttribute('frameBorder', '0');
            _iframe.setAttribute('allowTransparency', 'true');
            _iframe.setAttribute('style', 'display: inline !important;');
            _iframe.setAttribute("width", clipcall.iframeWidth + "px");
            _iframe.src = appDomain + 'v2/SendPhone.aspx?invitationid=' + invitationId;
  //          return _iframe;
            var div = document.createElement('div');
            div.className = 'cc_div_iframe';
            div.style.width = clipcall.iframeWidth + "px";
            div.style.height = clipcall.iframeHeight + "px";
            div.style.marginTop = (-1*(clipcall.iframeHeight/2)) + 'px';
            div.style.marginLeft = (-1*(clipcall.iframeWidth/2)) + 'px';
            div.appendChild(_iframe);
            document.body.appendChild(div);
            clipcall.divIframe = div;
        },
        LoadXClose: function () {
            var x = document.createElement('a');
            x.className = 'cc_close';
            x.href = "javascript:void(0);";
            x.style.marginTop = ((-1 * (clipcall.iframeHeight / 2)) + 30) + 'px';
            x.style.marginLeft = (((clipcall.iframeWidth / 2)) - 60) + 'px';
            if (x.addEventListener) {
                x.addEventListener('click', function () {
                    clipcall.CloseIframe();
                }, false);
            }
            else {
                x.attachEvent('onclick', function () {
                    clipcall.CloseIframe();
                });
            }
            document.body.appendChild(x);
            clipcall.xClose = x;
        },
        LoadBackgroundDiv: function () {
            
            var imgLoader = document.createElement('img');
            imgLoader.src = clipcall.cdnDomain + '/UpdateProgress/ajax-loader.gif';
            imgLoader.setAttribute('alt', 'Loading...');
            var tableLoader = document.createElement('table');
            var tr = document.createElement('tr');
            var td = document.createElement('td');
            
            var div = document.createElement('div');
            div.className = 'cc_divLoader';
            div.style.display = 'none';
            
            td.appendChild(imgLoader);
            tr.appendChild(td);
            tableLoader.appendChild(tr);
            div.appendChild(tableLoader);
            
            
            (document.getElementsByTagName('body')[0]).appendChild(div);
            clipcall.divBackground = div;
        },
        HideBackgroundDiv: function () {
            clipcall.divBackground.style.display = 'none';
        },
        ShowBackgroundDiv: function () {
            clipcall.divBackground.style.display = 'block';
        },
        GetDomainPath: function () {
            /*
            var result = window.location.protocol + '//';
            var domain = window.location.host.split('.');
            for (var i = 0; i < domain.length; i++) {
                result += (i == 0 && domain[i] == 'nps') ? 'app.' : (domain[i] + '.');
            }
            result = result.substr(0, (result.length - 1));
            */
            var result = clipcall.cdnDomain.replace('nps.', 'app.');
            result += "/invite/";
            return result;
        },
        CloseIframe: function () {
            if (clipcall.divIframe) {
                document.body.removeChild(clipcall.divIframe);
                clipcall.divIframe = null;
            }
            if (clipcall.xClose) {
                document.body.removeChild(clipcall.xClose);
                clipcall.xClose = null;
            }
            clipcall.HideBackgroundDiv();
        }
    }
})();