﻿(function (window) {
    function ClipCallItem(elemId, supplierId, cssclass, type){
        this.elementId = elemId;
        this.supplierId = supplierId;
        this.cssclass = cssclass;
        this.type = type;
    };
    ClipCallItem.prototype.Set = function () {
        var _this = this;
        if (document.readyState == 'complete')
            _this.OnLoad();
        else if (window.addEventListener) {
            window.addEventListener('load', function () {
                _this.OnLoad();
            }
                , false); //W3C
        }
        else {
            window.attachEvent('onload', function () {
                _this.OnLoad();
            }); //IE
        }
    };
    ClipCallItem.prototype.OnLoad = function () {
        this.element = document.getElementById(this.elementId);
        if (!this.element) {
            clipcall.NoElementFound(this.elementId);
            return;
        }
        if (this.type == 'img') {
            if (this.element.getAttribute("cc") == "1")
                return;
            this.element.setAttribute("cc", "1");
            this.element.style.cursor = 'pointer';
            this.click(this.element);            
            return;
        }
        this.element.textContent = '';
        var divContent = document.createElement('div');
        divContent.className = 'cc_content';

        var divSpanContent = document.createElement('div');
        divSpanContent.className = 'cc_DivSpanContent';
        var spanContentTop = document.createElement('span');
        spanContentTop.className = 'cc_span_content_top';
        spanContentTop.textContent = '*** Get 10% off ***';
        divSpanContent.appendChild(spanContentTop);

        var spanContent = document.createElement('span');
        spanContent.className = 'cc_span_create';
        spanContent.textContent = 'Show what you need';
        divSpanContent.appendChild(spanContent);

        divContent.appendChild(divSpanContent);

        var aCreate = document.createElement('a');
        aCreate.className = 'cc_a_create';
        aCreate.href = 'javascript:void(0);';
        aCreate.textContent = 'Shoot a video';
        divContent.appendChild(aCreate);

        var divCol = document.createElement('div');
        divCol.className = 'cc_col';
        divCol.appendChild(divContent);

        var divRow = document.createElement('div');
        var cc_row_classes = 'cc_row' + ((this.cssclass && this.cssclass.length > 0) ? ' ' + this.cssclass : '');
        divRow.className = cc_row_classes;
        divRow.appendChild(divCol);

        
        /*
        if (aCreate.addEventListener) {
            aCreate.addEventListener('click', function () {
                clipcall.ClickShootVideo(_this.supplierId);
            }, false);
        }
        else {
            aCreate.attachEvent('onclick', function () {
                clipcall.ClickShootVideo(_this.supplierId);
            });
        }
        */
        this.click(aCreate);
        this.element.appendChild(divRow);
    };
    ClipCallItem.prototype.click = function (elem) {
        var _this = this;
        if (elem.addEventListener) {
            elem.addEventListener('click', function () {
                clipcall.ClickShootVideo(_this.supplierId);
            }, false);
           
        }
        else {
            elem.attachEvent('onclick', function () {
                clipcall.ClickShootVideo(_this.supplierId);
            });
        }
    }
    window.clipcall = {
        
     //   _elementId: null,
    //    _element: null,
        cdnDomain: '{domain}',
        appDomain: null,
        divBackground: null,
        divIframe: null,
        xClose: null,
        iframeHeight: 520,
        iframeWidth: 680,
        HasLoadlibrary: false,
        set: function (setObject) {
            if (!setObject || !setObject.elementId) {
                clipcall.NoElementFound('');
                return;
            }
      
            if (!setObject.SupplierId || setObject.SupplierId.length == 0) {
                console.log("supplierId bad argument - ", setObject.SupplierId);
                return;
            }
            var ccitem = new ClipCallItem(setObject.elementId, setObject.SupplierId, (setObject.cssclass ? setObject.cssclass : null),
                (setObject.type ? setObject.type : null));
            ccitem.Set();
            if (document.readyState == 'complete')
                clipcall.setOnLoad();
            else if (window.addEventListener) {
                window.addEventListener('load', function () {
                    clipcall.setOnLoad();
                }
                    , false); //W3C
            }
            else {
                window.attachEvent('onload', function () {
                    clipcall.setOnLoad();
                }); //IE
            }
           
            
        },
        setOnLoad: function(){
            if (!clipcall.HasLoadlibrary) {
                clipcall.appDomain = clipcall.GetDomainPath();
                clipcall.LoadCss();
                clipcall.LoadBackgroundDiv();
                clipcall.HasLoadlibrary = true;
            }
        },
        NoElementFound: function (elementId) {
            console.log("Element not founded - ", elementId);
        },
        LoadCss: function () {
            var css = document.createElement('link');//<link href="style.css" rel="stylesheet" type="text/css"/>
            css.href = clipcall.cdnDomain + '/invite/v2/cc_style.css';
            css.setAttribute('rel', 'stylesheet');
            css.setAttribute('type', 'text/css');
            (document.getElementsByTagName('head')[0]).appendChild(css);
        },
        ClickShootVideo: function (supplierId) {
            clipcall.ShowBackgroundDiv();
            var requestUrl = clipcall.appDomain + 'v2/create2.ashx?supplierId=' + supplierId + '&url=' + encodeURIComponent(document.location.href);
            clipcall.CreateRequest(requestUrl);

        },
        responseCreateInvitation: function (responseObj) {
            //   $('.divLoader').hide();
            if (!responseObj || !responseObj.invitationId || responseObj.invitationId.length == 0) {
                clipcall.HideBackgroundDiv();
                return;
            }
            clipcall.LoadIframe(responseObj.invitationId);
            clipcall.LoadXClose();

            //   invitationId = response.invitationId;
            //    $('#step1').hide();
            //    $('#step2').show();
        },
        CreateRequest: function (url) {
            var _script = document.createElement('script');
            _script.type = "text/javascript";
            _script.src = url;
            _script.async = true;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(_script, s);
        },
        LoadIframe: function (invitationId) {
            var _iframe = document.createElement("iframe");
            _iframe.setAttribute("height", clipcall.iframeHeight + "px");//clipcall._element.offsetHeight);
            _iframe.setAttribute('scrolling', 'no');
            _iframe.setAttribute('frameBorder', '0');
            _iframe.setAttribute('allowTransparency', 'true');
            _iframe.setAttribute('style', 'display: inline !important;');
            _iframe.setAttribute("width", clipcall.iframeWidth + "px");
            _iframe.src = clipcall.appDomain + 'v2/SendPhone.aspx?invitationid=' + invitationId;
            //          return _iframe;
            var div = document.createElement('div');
            div.className = 'cc_div_iframe';
            div.style.width = clipcall.iframeWidth + "px";
            div.style.height = clipcall.iframeHeight + "px";
            div.style.marginTop = (-1 * (clipcall.iframeHeight / 2)) + 'px';
            div.style.marginLeft = (-1 * (clipcall.iframeWidth / 2)) + 'px';
            div.appendChild(_iframe);
            document.body.appendChild(div);
            clipcall.divIframe = div;
        },
        LoadXClose: function () {
            var x = document.createElement('a');
            x.className = 'cc_close';
            x.href = "javascript:void(0);";
            x.style.marginTop = ((-1 * (clipcall.iframeHeight / 2)) + 30) + 'px';
            x.style.marginLeft = (((clipcall.iframeWidth / 2)) - 60) + 'px';
            if (x.addEventListener) {
                x.addEventListener('click', function () {
                    clipcall.CloseIframe();
                }, false);
            }
            else {
                x.attachEvent('onclick', function () {
                    clipcall.CloseIframe();
                });
            }
            document.body.appendChild(x);
            clipcall.xClose = x;
        },
        LoadBackgroundDiv: function () {

            var imgLoader = document.createElement('img');
            imgLoader.src = clipcall.cdnDomain + '/UpdateProgress/ajax-loader.gif';
            imgLoader.setAttribute('alt', 'Loading...');
            var tableLoader = document.createElement('table');
            var tr = document.createElement('tr');
            var td = document.createElement('td');

            var div = document.createElement('div');
            div.className = 'cc_divLoader';
            div.style.display = 'none';

            td.appendChild(imgLoader);
            tr.appendChild(td);
            tableLoader.appendChild(tr);
            div.appendChild(tableLoader);


            (document.getElementsByTagName('body')[0]).appendChild(div);
            clipcall.divBackground = div;
        },
        HideBackgroundDiv: function () {
            clipcall.divBackground.style.display = 'none';
        },
        ShowBackgroundDiv: function () {
            clipcall.divBackground.style.display = 'block';
        },
        GetDomainPath: function () {
            /*
            var result = window.location.protocol + '//';
            var domain = window.location.host.split('.');
            for (var i = 0; i < domain.length; i++) {
                result += (i == 0 && domain[i] == 'nps') ? 'app.' : (domain[i] + '.');
            }
            result = result.substr(0, (result.length - 1));
            */
            var result = clipcall.cdnDomain.replace('nps.', 'app.');
            result += "/invite/";
            return result;
        },
        CloseIframe: function () {
            if (clipcall.divIframe) {
                document.body.removeChild(clipcall.divIframe);
                clipcall.divIframe = null;
            }
            if (clipcall.xClose) {
                document.body.removeChild(clipcall.xClose);
                clipcall.xClose = null;
            }
            clipcall.HideBackgroundDiv();
        }
    }
})(window);