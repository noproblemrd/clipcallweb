﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class invite_v2_SendPhone : System.Web.UI.Page
{
    protected Guid InvitationId { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        Guid _id;
        InvitationId = (!Guid.TryParse(Request.QueryString["invitationid"], out _id)) ?
            InvitationId = Guid.Empty : InvitationId = _id;
        Header.DataBind();

    }
}