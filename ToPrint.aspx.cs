﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ToPrint : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["string_print"] != null)
        {
            string string_print = (string)Session["string_print"];
            if (Session["PrintStyle"] != null)
            {
                string _style = (string)Session["PrintStyle"];
                HtmlGenericControl hgc = new HtmlGenericControl("link");
                hgc.Attributes["rel"] = "stylesheet";
                hgc.Attributes["type"] = "text/css";
                hgc.Attributes["href"] = _style;
                Header.Controls.Add(hgc);
            }
            LoadStringPrint(string_print);
            return;
        }
        if (Session["grid_print"] == null)// || Session["data_print"].GetType() != typeof(DataTable))
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "closeWin", "self.close()", true);
            return;
        }
        object control = Session["grid_print"];
        Type control_type = control.GetType();
        if(Session["data_print"] == null || Session["data_print"].GetType() != typeof(DataTable))
        {
            if(control_type == typeof(GridView))
                PrintHelper.PrintWebControl((GridView)control);
            else if (control_type == typeof(Repeater))
                PrintHelper.PrintWebControl((Repeater)control);
            else if (control_type == typeof(string))
            {
                LiteralControl lc = new LiteralControl((string)control);
                PrintHelper.PrintWebControl(lc);
            }
        }
        else
        {
            DataTable data = (DataTable)Session["data_print"];        
            if(control_type == typeof(GridView))
                PrintHelper.PrintWebControl((GridView)control, data);
            else if (control_type == typeof(Repeater))
                PrintHelper.PrintWebControl((Repeater)control, data);
            
        }
         
        /*
        if (PreviousPage != null)
        {
            Control _ctr = PreviousPage.CrossPageControl;
            if (_ctr.GetType() == typeof(GridView))
                PrintHelper.PrintWebControl((GridView)_ctr);
            else if (_ctr.GetType() == typeof(Repeater))
                PrintHelper.PrintWebControl((Repeater)_ctr);
        }
         * */
    }

    private void LoadStringPrint(string string_print)
    {
        Response.Write(string_print);
        string _script = "<script type='text/javascript'>" +
            "window.onload = function() {" +
            "window.print();setTimeout('self.close()', 10000);}" +
            "</script>";
        Response.Write(_script);
        Response.Flush();
        ApplicationInstance.CompleteRequest();
    }
   

}
