using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Management_MasterPagePreAdvertiser : System.Web.UI.MasterPage
{
    public Management_MasterPagePreAdvertiser()
        : base()
    {
        this.ID = PagesNames.PreAdvertiser.ToString();
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PageSetting ps = (PageSetting)Page;
            ps.ClearExceptSiteSetting();
            if (string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, ps))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");

                if (Request.Cookies["language"] != null)
                {
                    int SiteLangId = int.Parse(Request.Cookies["language"].Value);
                    if (DBConnection.IfSiteLangIdExists(SiteLangId, ps.siteSetting.GetSiteID)) 
                        ps.siteSetting.siteLangId = SiteLangId;
                }
            }
            if (ps.siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
            {
                Response.Redirect(ResolveUrl("~") + "PPC/PpcLogin.aspx");
                //Response.Redirect(ResolveUrl("~") + "PPC/login.aspx");
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        PageSetting _page = (PageSetting)Page;
        if (!IsPostBack)
        {
           
            setLogo(_page.siteSetting.LogoPath);           
            if (!string.IsNullOrEmpty(_page.siteSetting.GetSiteID))
                getTranslate(_page.siteSetting.siteLangId);
            _page.LoadMasterSiteLinks(this);
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + _page.siteSetting.GetStyle));
        }
        SiteSetting _site = ((PageSetting)Page).siteSetting;
        if (!string.IsNullOrEmpty(_site.GetGoogleAnalytics))
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GoogleAnalytics", _site.GetGoogleAnalytics, true);
        if (!string.IsNullOrEmpty(_site.GetGoogleCampain))
            Page.Form.Controls.Add(new LiteralControl(_site.GetGoogleCampain));
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm == null || !sm.IsInAsyncPostBack)
            _page.SetNoIframe();
    }
    private void setLogo(string name)
    {
        string LogoServerPath = ConfigurationManager.AppSettings["PublisherLogo"] + @"aspjpeg\" + name;
        if (File.Exists(LogoServerPath))
            ImageLogo.ImageUrl = ConfigurationManager.AppSettings["PublisherLogoWeb"] + @"aspjpeg/" + name;
    }
    private void getTranslate(int SiteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "MasterPagePreAdvertiser.master", SiteLangId);
    }
}
