﻿<%@ WebHandler Language="C#" Class="setStep" %>

using System;
using System.Web;

public class setStep : PageSetting, IHttpHandler, System.Web.SessionState.IRequiresSessionState 
{
    
    public void ProcessRequest (HttpContext context) {
       context.Response.ContentType = "text/plain";

       string siteId = context.Request["siteId"];
       string supplierGuid = context.Request["supplierGuid"];
       string step = context.Request["step"];      
        
       WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, siteId);
       WebReferenceSupplier.GetStartedTaskType getStartedTaskType= WebReferenceSupplier.GetStartedTaskType.register;
        
       switch (step)
       {
           case "1":
             getStartedTaskType= WebReferenceSupplier.GetStartedTaskType.register; 
             break;
               
           case "2":
             //getStartedTaskType= WebReferenceSupplier.GetStartedTaskType.watch_video; 
             break;
           
           case "3":
             getStartedTaskType= WebReferenceSupplier.GetStartedTaskType.work_hours; 
             break;
               
           case "4":
             getStartedTaskType= WebReferenceSupplier.GetStartedTaskType.honor_code; 
             break; 
               
           case "5":
             getStartedTaskType= WebReferenceSupplier.GetStartedTaskType.call_test; 
             break; 
               
           case "6":
             getStartedTaskType= WebReferenceSupplier.GetStartedTaskType.listen_recording; 
             break; 
               
           default:
               break;          
       }

       WebReferenceSupplier.Result result= new WebReferenceSupplier.Result();
       WebReferenceSupplier.ResultOfCrossOutGetStartedTaskResponse resultOfCrossOutGetStartedTaskResponse = new WebReferenceSupplier.ResultOfCrossOutGetStartedTaskResponse();
       resultOfCrossOutGetStartedTaskResponse = supplier.CrossOutGetStartedTask(new Guid(supplierGuid), getStartedTaskType);

       string resultType = "";

       bool ifAllTasksDone = resultOfCrossOutGetStartedTaskResponse.Value.AllTasksDone;
       int taskleft = resultOfCrossOutGetStartedTaskResponse.Value.TasksLeft;
          
       if (resultOfCrossOutGetStartedTaskResponse.Type == WebReferenceSupplier.eResultType.Success)
       {
           Decimal balance = resultOfCrossOutGetStartedTaskResponse.Value.Balance;
           string strBalance = string.Format(NUMBER_FORMAT, balance);
           context.Session["balance"] = strBalance;

           resultType = WebReferenceSupplier.eResultType.Success.ToString().ToLower() + "," + ifAllTasksDone + "," + taskleft.ToString();
       }

       else
       {
           resultType = WebReferenceSupplier.eResultType.Failure.ToString() + "," + ifAllTasksDone + "," + taskleft.ToString();
       }      
        
       context.Response.Write(resultType);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}