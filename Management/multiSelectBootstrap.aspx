﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="multiSelectBootstrap.aspx.cs" Inherits="Management_multiSelectBootstrap"  ClientIDMode="Static" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  
		<title>Bootstrap Multiselect</title>
		<meta content="noindex, nofollow" name="robots">
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
		<meta content="David Stutz" name="copyright">

        <!-- Include Twitter Bootstrap and jQuery: -->
		<link type="text/css" href="../MultipleSelectBootstrap/css/bootstrap-3.1.1.min.css" rel="stylesheet">
      
		<link type="text/css" href="../MultipleSelectBootstrap/css/bootstrap-multiselect.css" rel="stylesheet">
		<link type="text/css" href="../MultipleSelectBootstrap/css/prettify.css" rel="stylesheet">

        <!-- Include the plugin's CSS and JS: -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet"><script
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
		<script src="../MultipleSelectBootstrap/js/bootstrap-3.1.1.min.js" type="text/javascript"></script>
		<script src="../MultipleSelectBootstrap/js/bootstrap-multiselect.js" type="text/javascript"></script>
		<script src="../MultipleSelectBootstrap/js/prettify.js" type="text/javascript"></script>
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>

        <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
        <script type="text/javascript">        try { Typekit.load(); } catch (e) { }</script>
        <script src="../PPC/ppc.js" type="text/javascript"></script>

        <link href="../PPC/samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />

        <style>
            .btn
            {
                font-size:9px !important;
            }
            
        </style>

          <!-- Initialize the plugin: -->
        <script type="text/javascript">
             
            var containerWorkingHoursObject=new Object();
            var listWorkingHoursUnit=new Array();


            $(document).ready(function () {
                $('.multiselect').multiselect({

                    buttonText: function (options, select) {
                        //alert(options.length);
                        if (options.length == 0) {
                            return this.nonSelectedText + ' <b class="caret"></b>';
                        }
                        else {
                            if (options.length > this.numberDisplayed) {
                                return options.length + ' ' + this.nSelectedText + ' <b class="caret"></b>';
                            }
                            else {
                                var selected = '';
                                options.each(function () {
                                    var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();

                                    selected += label + ', ';
                                });
                                return selected.substr(0, selected.length - 2) + ' <b class="caret"></b>';
                            }
                        }
                    },

                    buttonWidth: '160px',
                    numberDisplayed: 7,
                    includeSelectAllOption: true

                });


                $("#lnk_Vacation").click(function () {
                    $(".containerVacation").show();
                    $(this).hide();
                }
                );

                $("#minusDatePicker").click(function () {
                    $(".containerVacation").hide();
                    $('#lnk_Vacation').show();
                    return false;
                }
                );

                /*************** date picker *************/

                var initFromDate = '<%#vacationFrom%>';
                var currentDate;
                var futureDate;
                if (initFromDate == "") {
                    currentDate = new Date();
                    futureDate = new Date();
                    futureDate.setDate(currentDate.getDate() + 1);
                }
                else {
                    currentDate = new Date('<%#vacationFrom%>');
                    futureDate = new Date('<%#vacationTo%>');
                }

                var pickerOpts = {
                    showOn: "both", // will open clcik the input and click the icon
                    buttonImageOnly: true,
                    buttonImage: "images/calendar.png",
                    minDate: currentDate,
                    //defaultDate: new Date(),
                    buttonText: "",

                    onSelect: function (dateStr) { $("#formDate").css({ 'color': 'black', 'borderColor': '' }); }
                };

                $("#txt_formDate").datepicker(pickerOpts); // from http://jqueryui.com/datepicker/
                $("#txt_formDate").datepicker("setDate", currentDate);



                $("#txt_toDate").datepicker(pickerOpts); // to  http://jqueryui.com/datepicker/
                $("#txt_toDate").datepicker("setDate", futureDate);


                /*
                var currentDate = new Date();
                //currentDate.setDate(currentDate.getDate() + 4);

                var pickerOpts = {
                showOn: "both", // will open clcik the input and click the icon
                buttonImageOnly: true,
                buttonImage: "images/calendar.png",
                minDate: currentDate,
                //defaultDate: new Date(),
                buttonText: "",

                onSelect: function (dateStr) { $("#formDate").css({ 'color': 'black', 'borderColor': '' }); }
                };


                $("#txt_formDate").datepicker(pickerOpts); // from http://jqueryui.com/datepicker/
                //$("#txt_formDate").datepicker("setDate", new Date());
                //$("#txt_formDate").datepicker("setDate", currentDate.setDate(currentDate.getDate() + 1));
                //$("#txt_formDate").datepicker("setDate", '12/15/2013');
                */

            });
                     


            var index2=0;


            function getMultiSelect() {
                //alert("getMultiSelect");
                getDetails();

                index2 = 0;

                $('.multiselect-container').each(function (index) {
                    ////alert("index: " + index);
                    $(this).prev().css('width', '160px'); // this is the top button

                    var WorkingHoursUnit = new Object();
                    var daysOfWeek = new Array();

                    //alert($(this).parent().prev('select').attr('id'));
                    //$($(this).parent().prev('select')).multiselect({ buttonWidth: '160px', numberDisplayed: 4 });
                    //$($(this).parent().prev('select')).multiselect('rebuild');

                    //alert(containerWorkingHoursObject.ListWorkingHoursUnit[index2]);
                    if (containerWorkingHoursObject.ListWorkingHoursUnit[index2] && containerWorkingHoursObject.ListWorkingHoursUnit[index2].DaysOfWeek.length > 0) // just for multiselect at least one checked
                    {
                       
                        $($(this).parent().parent().parent().children('.ddlFromTime')).prop('disabled', containerWorkingHoursObject.ListWorkingHoursUnit[index2].timeDisabled);
                        $($(this).parent().parent().parent().children('.ddlToTime')).prop('disabled', containerWorkingHoursObject.ListWorkingHoursUnit[index2].timeDisabled);

                        $(this).find('li').each(function (index) {

                            for (var i = 0; i < containerWorkingHoursObject.ListWorkingHoursUnit[index2].DaysOfWeek.length; i++) {
                                //alert(containerWorkingHoursObject.ListWorkingHoursUnit[index2].DaysOfWeek[i]);
                                if ($(this).find('input[type="checkbox"]').prop("value") == containerWorkingHoursObject.ListWorkingHoursUnit[index2].DaysOfWeek[i]) {
                                    //alert($(this).find('input[type="checkbox"]').prop("value"));
                                    //$(this).find('input[type="checkbox"]').prop("checked", true);
                                    //$(this).addClass('active');                                    


                                    $($(this).parent().parent().prev('select')).multiselect('select', $(this).find('input[type="checkbox"]').prop("value"));

                                }

                            }


                        }
                        )



                    }



                    index2++;
                }
                )
                
            }

            function setEventClick() {
                alert("setEventClick");
                
            }


            function convertDayIntToDayName(dayInt) {
                var dayName;
                switch (dayInt) {
                    case 0:
                        dayName = 'Sun';
                        break;
                    case 1:
                        dayName = 'Mon';
                        break;
                    case 2:
                        dayName = 'Tue';
                        break;
                    case 3:
                        dayName = 'Wed';
                        break;
                    case 4:
                        dayName = 'Thu';
                        break;
                    case 5:
                        dayName = 'Fri';
                        break;
                    case 6:
                        dayName = 'Sat';
                        break;
                    default:
                        break;                        
                }

                return dayName;   
            }

            function setRuntimeDates(order, dayInt) {

                $('#selectDate' + order).multiselect('select', convertDayIntToDayName(dayInt));
                //$('#selectDate1').multiselect('select', 'Mon');
            }

            function initDefaultDays() {
                //alert(objUnits);
                var objUnitsEval = eval("(" + objUnits + ")");
                for (var i = 0; i < objUnitsEval.Units.length; i++) { // there at least one unit pre populated by the system
                    //alert("Order:" + objUnitsEval.Units[i].Order);
                    for (var y = 0; y < objUnitsEval.Units[i].DaysOfWeek.length; y++) {
                        setRuntimeDates(objUnitsEval.Units[i].Order, objUnitsEval.Units[i].DaysOfWeek[y]);
                        //alert(objUnitsEval.Units[i].DaysOfWeek[y]);
                    }

                }               
               
            }

            function initDefaultDateVacation() {
                $("#txt_formDate").datepicker(pickerOpts); // from http://jqueryui.com/datepicker/
                $("#txt_formDate").datepicker("setDate", currentDate);

                $("#txt_toDate").datepicker(pickerOpts); // to  http://jqueryui.com/datepicker/
                $("#txt_toDate").datepicker("setDate", currentDate.setDate(currentDate.getDate() + 1));
            }

            function init() {
                //setEventClick();
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndHandler);

                initDefaultDays();
                //initDefaultDateVacation();
                             
            }
            function startRequest(sender, e) {
                //alert("baaa start");
            }
            function EndHandler(sender, e) {               
                getMultiSelect();
                //setEventClick();
                //alert("before");

                //$('.multiselect').multiselect('destroy');
                //getDetails();
                //$('.multiselect').multiselect('refresh');
                
            }   

            window.onload = init;

            function getDetails()
            {
                
                $('.multiselect').multiselect({
                    buttonText: function (options, select) {
                        //alert(options.length);
                        if (options.length == 0) {
                            return this.nonSelectedText + ' <b class="caret"></b>';
                        }
                        else {
                            if (options.length > this.numberDisplayed) {
                                return options.length + ' ' + this.nSelectedText + ' <b class="caret"></b>';
                            }
                            else {
                                var selected = '';
                                options.each(function () {
                                    var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();

                                    selected += label + ', ';
                                });
                                return selected.substr(0, selected.length - 2) + ' <b class="caret"></b>';
                            }
                        }
                    },
                    buttonWidth: '160px',
                    numberDisplayed: 7,
                    includeSelectAllOption: true
                });


                
            }


           

            function setSelectBox(minusObj) {                
                         
                outerIndex=0;
                innerIndex=0;


                $('.multiselect-container').each(function (index) {
                    //alert(index + " id:" + $(this).attr('id'));
                    var WorkingHoursUnit = new Object();
                    var daysOfWeek = new Array();



                    $(this).find('li').each(function (index) {
                        //alert($(this).attr('class'));

                        //alert($($(this).parent().parent().prev('select')).attr('id'));
                        if ($(this).attr('class') && $(this).attr('class') == "active") {
                            //alert(daysOfWeek[innerIndex]);
                            daysOfWeek[innerIndex++] = $(this).find('input[type="checkbox"]').attr('value');
                            //alert($(this).find('input[type="checkbox"]').attr('value'));                           

                        }
                    }
                    )

                    //if(daysOfWeek.length>0)  
                    //{ 
                    //alert($($(this).parent().prev('select')).attr('id'));
                    if (minusObj != $($(this).parent().prev('select')).attr('id')) {
                        WorkingHoursUnit.DaysOfWeek = daysOfWeek;
                        WorkingHoursUnit.Order = outerIndex;
                        listWorkingHoursUnit[outerIndex++] = WorkingHoursUnit;
                        containerWorkingHoursObject.ListWorkingHoursUnit = listWorkingHoursUnit;
                        WorkingHoursUnit.timeDisabled = $("#selectFromTime" + $($(this).parent().prev('select')).attr('id').replace('selectDate', '')).prop('disabled');
                        //alert(WorkingHoursUnit.timeDisabled);
                    }


                    // alert(containerWorkingHoursObject.ListWorkingHoursUnit[0].DaysOfWeek[0]);

                    //}



                    innerIndex = 0;
                }
                )
            }

            function setTimeEnabled(objNumber) {
                //alert(objNumber);

                //alert($("#chk24Hours" + objNumber).prop('checked'));

                if ($("#chk24Hours" + objNumber).prop('checked')) {
                    $("#selectToTime" + objNumber).prop('disabled', true);
                    $("#selectFromTime" + objNumber).prop('disabled', true);
                }

                else {
                    $("#selectToTime" + objNumber).prop('disabled', false);
                    $("#selectFromTime" + objNumber).prop('disabled', false);
                }
            }

            function validation() {
                

                setSelectBox();
                //alert($.isEmptyObject(containerWorkingHoursObject));
                if ($.isEmptyObject(containerWorkingHoursObject || containerWorkingHoursObject.ListWorkingHoursUnit.length==0))
                    return false;
                else
                    return true;

                //alert(containerWorkingHoursObject.ListWorkingHoursUnit[0].DaysOfWeek[0]);
            }

            //$('#ddlCoverArea' + index).prop('disabled', false);
            
        </script>

	

</head>
<body class="bodyDashboard bodyDates">
    <form id="form1" runat="server">
    <div class="datesDashboard dashboardIframe">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div class="titleFirst">
            <span>Availability</span>
        </div>   

        <div class="titleSecond titleWorkingHours">
            <span>Working hours</span>
        </div>   

        <div class="datesDescription">
            Please tell us when you’d like to receive calls from customers by setting your             <br />
            working hours below.
        </div>
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlDates">
                    
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <div class="newCategory">
            <asp:LinkButton ID="lbAddButton" runat="server" OnClientClick="return validation();" onclick="lbAddButton_Click" >+ Add working days</asp:LinkButton>
        </div> 

        <div class="datesCategorySeperator"></div>

        <div class="titleSecond titleVacationSettings">
            <span>Vacation settings</span>
        </div>   

        <a id="lnk_Vacation" href="javascript:void(0);" runat="server">Go on vacation</a>
        
        <div class="containerVacation" id="containerVacation" runat="server">
            <div class="fromdateVacation">
                <div class="titleDateVacation ">From</div>
                <div class="dateVacation">
                    <asp:TextBox  ID="txt_formDate" runat="server" CssClass="textActive"  ReadOnly="true"></asp:TextBox>
                </div>
            </div>

            <div class="containerSeperator" id="containerSeperator1"><div class="seperatorDate" id="seperator1"></div></div>

            <div class="todateVacation">
                <div class="titleDateVacation">To</div>
                <div class="dateVacation">
                    <asp:TextBox  ID="txt_toDate" runat="server" CssClass="textActive"  ReadOnly="true"></asp:TextBox>
                </div>
            </div>

            <div class="todateVacation">
                <div class="titleDateVacation">&nbsp;</div>
                <div class="dateVacationMinus">
                    <a id="minusDatePicker" href="">
                        <i class="fa fa-times fa-2"></i>
                    </a>
                </div>            
            </div>

        </div>
        
        <div class="clear"></div>
        
    </div>
    </form>
</body>
</html>
