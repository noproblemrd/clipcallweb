using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Linq;

public partial class Professional_professionalDates : PageSetting
{
    bool HasSuspend = false;
    const string UNVAILABILITY_LIMIT = "UnavailabilityLimit";
    protected void Page_Load(object sender, EventArgs e)
    {
        ToolkitScriptManager1.RegisterAsyncPostBackControl(sndBtn);
        btn_VirtualControl.Attributes.Add("style", "display:none");
        Page.Header.DataBind();

        if (!IsPostBack)
        {
            lbl_page.Text = " 6";
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {

                Response.Redirect(ResolveUrl("~") + @"Management/LogOut.aspx");
                return;
            }
            LoadReason();

            LoadUnavailabilityLimit();
            chk_suspend.Attributes.Add("onclick", "showHideZminut();");

            if (!string.IsNullOrEmpty(siteSetting.DateFormat))
            {
                string _dateFormat = ConvertToDateTime.GetCalenderFormat(siteSetting.DateFormat);
                CalendarExtender1.Format = _dateFormat;
                CalendarExtender2.Format = _dateFormat;
                Hidden_dateFormat.Value = _dateFormat;
                _WaterMarkFrom.WatermarkText = _dateFormat;
                _WaterMarkTo.WatermarkText = _dateFormat;
                //     txtFromDate.
            }
            getAvailability();
            RemoveServerCommentPPC();
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
            //clientIdBalance = this.Master.FindControl("lbl_Balance").ClientID;
        }
       
        
    }

    private void LoadUnavailabilityLimit()
    {
        if (userManagement.IsPublisher())
        {
            hf_MilliTime.Value = "";
            return;
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = _site.GetConfigurationSettings();// GetUserTimeZone(userManagement.GetGuid);
        }
        catch (Exception ex)
        {

            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["Configurations"] == null || xdd["Configurations"]["Error"] != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        string _value = "";
        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == UNVAILABILITY_LIMIT)
            {
                _value = node["Value"].InnerText;
                break;
            }
        }
        long _val;
        if (long.TryParse(_value, out _val))
        {
            _val = _val * 24 * 60 * 60 * 1000;
            hf_MilliTime.Value = _val.ToString();
        }
        else
            hf_MilliTime.Value = "";

    }
    private void LoadTimeFormat(DropDownList ddl)
    {
        if (string.IsNullOrEmpty(siteSetting.TimeFormat))
            return;
        foreach (ListItem li in ddl.Items)
        {
            li.Text = string.Format(siteSetting.TimeFormat,
                ConvertToDateTime.GetDateTimeByTimeValue(li.Value));
        }
    }
    private void EnableUnavailability()
    {
        CalendarExtender1.PopupButtonID = "btn_VirtualControl";
        CalendarExtender2.PopupButtonID = "btn_VirtualControl";
        txtFromDate.Attributes.Remove("onclick");
        txtFromDate.Attributes.Remove("onblur");
        txtFromDate.Attributes.Add("readonly", "readonly");
        txtFromDate.Enabled = false;
        txtToDate.Attributes.Remove("onclick");
        txtToDate.Attributes.Remove("onblur");
        txtToDate.Attributes.Add("readonly", "readonly");
        txtToDate.Enabled = false;
        ddlReason.Enabled = false;
        if (userManagement.IsPublisher())
            cb_UnlimitedTime.Enabled = false;
        else
            cb_UnlimitedTime.Visible = false;
        chk_suspend.Enabled = false;
        sndBtn.Visible = false;
        chk_suspend.Attributes.Remove("onclick");
        foreach (RepeaterItem item in _reapeter.Items)
        {
            ((HtmlAnchor)item.FindControl("a_Edit")).Attributes.Remove("href");
        }
    }


    private void LoadReason()
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        string result = string.Empty;
        try
        {

            result = supplier.GetUnavailabilityReasons();

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["UnavailabilityResons"] == null || xdd["UnavailabilityResons"]["Error"] != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlNodeList xnl = xdd["UnavailabilityResons"].ChildNodes;
        ddlReason.Items.Clear();
        //      ddlReason.Items.Add(new ListItem("*** Reason ***", "0"));
        foreach (XmlNode node in xnl)
        {
            string reason = node.InnerText;
            string reasonId = node.Attributes["ID"].InnerText;
            ListItem _li = new ListItem(reason, reasonId);
            string DisplayLevel = node.Attributes["DisplayLevel"].InnerText;
            if (DisplayLevel == "2" || (DisplayLevel == "1" && userManagement.IsSupplier()))
            {
                _li.Enabled = false;
                _li.Attributes.Add("CantTouch", "True");
            }
            string inactive = node.Attributes["Inactive"].InnerText;
            if (inactive == "1")
            {
                _li.Attributes.Add("Inactive", "True");
                _li.Enabled = false;
            }
            ddlReason.Items.Add(_li);
        }

    }

    protected void sndBtn_Click(object sender, EventArgs e)
    {

        if (updateWebServiceGeneral())
        {

            if (Session["firstUpdate"] != null)
            {
                if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
                {
                    WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
                    WebReferenceSupplier.ResultOfGetStartedTasksStatusResponse resultOfGetStartedTasksStatusResponse = supplier.RetrieveGetStartedTasksStatus(new Guid(GetGuidSetting()));
                    WebReferenceSupplier.GetStartedTasksStatusResponse getStartedTasksStatusResponse = resultOfGetStartedTasksStatusResponse.Value;
                    int bonusAmount = getStartedTasksStatusResponse.BonusAmount;
                    int tasksLeft = getStartedTasksStatusResponse.TasksLeft;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "FirstUpdate", "top.FirstUpdate('Thank you for indicating the hours you�re available to receive calls. You are only $link$ from getting $" +
                        bonusAmount + " worth of leads.','" + ResolveUrl("~/Management/overview.aspx") + "','" + tasksLeft + " steps away');", true);
                                                                                                           
                }

                Session.Remove("firstUpdate");
            }


            else if (!(userManagement.IsSupplier() && siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId && HasSuspend))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);




            HasSuspend = false;
            if (sndBtn.Text == lbl_NEXT.Text)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "setStep5", "setStep5();", true);
            else
            {
                if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
                {
                    //before ScriptManager.RegisterStartupScript(this, this.GetType(), "Header_Update", "Header_Update();", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "status_Update", "status_Update();", true);
                }
                getAvailability();

            }

        }

        else // didn't change anything
        {
            if (Session["firstUpdate"] != null)
            {
                if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
                {
                    WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
                    WebReferenceSupplier.ResultOfGetStartedTasksStatusResponse resultOfGetStartedTasksStatusResponse = supplier.RetrieveGetStartedTasksStatus(new Guid(GetGuidSetting()));
                    WebReferenceSupplier.GetStartedTasksStatusResponse getStartedTasksStatusResponse = resultOfGetStartedTasksStatusResponse.Value;
                    int bonusAmount = getStartedTasksStatusResponse.BonusAmount;
                    int tasksLeft = getStartedTasksStatusResponse.TasksLeft;
                    
                    if (tasksLeft > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "FirstUpdate", "top.FirstUpdate('Thank you for indicating the hours you�re available to receive calls. You are only $link$ from getting $" +
                            bonusAmount + " worth of leads.','" + ResolveUrl("~/Management/overview.aspx") + "','" + tasksLeft + " steps away');", true);
                    }

                    else
                    {                       
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "FirstUpdate", "top.FirstUpdate2('Hero You righteously earned $" + bonusAmount + " to spend getting phone leads!');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "allTasksDone", "allTasksDone(" + bonusAmount + ");", true);
                    }
                }

                Session.Remove("firstUpdate");
            }
        }

        _UpdatePanelMessage.Update();

        /*
        if (!(userManagement.IsSupplier() && siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId && HasSuspend))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        HasSuspend = false;
        if (sndBtn.Text == lbl_NEXT.Text)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setStep5", "setStep5();", true);
        else
        {
            if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "status_Update", "status_Update();", true);
            else
                getAvailability();

        }
            


    }
    _UpdatePanelMessage.Update();   
    */

    }


    int FindIndx(ListItemCollection lic, string val)
    {
        for (int i = 0; i < lic.Count; i++)
        {
            if (val == lic[i].Value)
                return i;
        }
        return -1;
    }
    void notEdited(RadioButtonList rbi, DropDownList ddlFrom, DropDownList ddlTo, Label lbl)
    {
        rbi.SelectedIndex = 1;
        lbl.Text = ddlFrom.SelectedValue + " - " + ddlTo.SelectedValue;
    }
    string GetTimeFormat(DateTime dt)
    {
        string hour = "" + dt.Hour;
        string minute = "" + dt.Minute;
        hour = (hour.Length == 1) ? "0" + hour : hour;
        minute = (minute.Length == 1) ? "0" + minute : minute;
        return hour + ":" + minute;
    }
    void SetDefaultRow(DataTable data)
    {
        string command = "EXEC dbo.GetDefaultWorkingTimes @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string from, until;
                DataRow rowita = data.NewRow();

                bool IsSuspend = (bool)reader["IsSuspend"];
                if (IsSuspend)
                {
                    from = "1:00";
                    until = "1:01";
                }
                else
                {
                    from = GetTimeFormat((DateTime)reader["StartHour"]);
                    until = GetTimeFormat((DateTime)reader["EndHour"]);
                }
                string DayName = (string)reader["DayName"];
                rowita["from"] = from;
                rowita["until"] = until;
                rowita["day"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "DayOfWeek", DayName);
                rowita["status"] = (IsSuspend) ? lblSuspend.Value : from + " - " + until;
                rowita["OrgDay"] = DayName;
                data.Rows.Add(rowita);
            }
            conn.Close();
        }
    }
    private void getAvailability()
    {
        string from, until;
        WebReferenceSupplier.Supplier supplire = WebServiceConfig.GetSupplierReference(this);
        string result = string.Empty;
        try
        {
            result = supplire.GetSupplierAvailability(siteSetting.GetSiteID, GetGuidSetting());
        }
        catch (Exception ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        XDocument xdoc = XDocument.Parse(result);
        //   XmlDataDocument doc = new XmlDataDocument();
        //    doc.LoadXml(result);

        /**************  RegistrationStage Influences ****************/
        if (xdoc.Element("SupplierAvailability") == null || xdoc.Element("SupplierAvailability").Element("Error") != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        string RegistrationStage = xdoc.Element("SupplierAvailability").Attribute("RegistrationStage").Value;

        string csnameStep = "setStep";
        Type cstypeStep = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager csStep = this.Page.ClientScript;


        if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
        {
            string csTextStep = "witchAlreadyStep(" + RegistrationStage + ");";
            csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
        }

        bool IsFirstTime = false;
        Dictionary<string, DateTime> dic = new Dictionary<string, DateTime>();
        if (Convert.ToInt32(RegistrationStage) >= 4)
        {
            sndBtn.Text = lbl_UPDATE.Text;
            pSteps.Visible = false;
        }
        else
        {
            sndBtn.Text = lbl_NEXT.Text;
            pSteps.Visible = true;
            IsFirstTime = true;
        }
        Dictionary<string, string> dicValue = new Dictionary<string, string>();

        /**************  End RegistrationStage Influences ****************/
        DataTable data = new DataTable();
        data.Columns.Add("from");
        data.Columns.Add("until");
        data.Columns.Add("day");
        data.Columns.Add("OrgDay");
        data.Columns.Add("status");
        data.Columns.Add("IsSuspend", typeof(bool));
        //       data.Columns.Add("index", typeof(int));
        //     DataRow[] row = new DataRow[7];

        if (IsFirstTime)
        {
            SetDefaultRow(data);
        }
        else
        {
            for (int i = 1; i < 8; i++)
            {
                XElement xelem = (from x in xdoc.Element("SupplierAvailability").Element("Availability").Elements("Day")
                                  where x.Attribute("Name").Value == ((DayOfWeek)i).ToString()
                                  select x).FirstOrDefault();
                if (i == 1 && string.IsNullOrEmpty(xelem.Element("FromTime").Value))
                {
                    SetDefaultRow(data);
                    IsFirstTime = true;
                    break;
                }
                bool IsSuspend, IsEdited;
                string _value;
                string _day = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "DayOfWeek", ((DayOfWeek)i).ToString());
                DataRow rowita = data.NewRow();
                from = xelem.Element("FromTime").Value;
                until = xelem.Element("TillTime").Value;

                rowita["from"] = from;
                rowita["until"] = until;
                IsSuspend = ((from == "1:00" && until == "1:01") || (from == until));
                IsEdited = !(string.IsNullOrEmpty(from) || string.IsNullOrEmpty(until));
                IsSuspend = IsSuspend && IsEdited;
                rowita["IsSuspend"] = IsSuspend;
                _value = (IsSuspend) ? lblSuspend.Value : from + " - " + until;
                rowita["status"] = _value;
                rowita["OrgDay"] = ((DayOfWeek)i).ToString();
                rowita["day"] = _day;
                dicValue.Add(_day, _value);
                data.Rows.Add(rowita);
            }
        }

        _reapeter.DataSource = data;
        _reapeter.DataBind();

        dicValueV = dicValue;
        IsFirstTimeV = IsFirstTime;

        span_ToDate.Style[HtmlTextWriterStyle.Display] = "inline";
        span_UnlimitedTime.Style[HtmlTextWriterStyle.Display] = "none";

        XElement nodeSupplier = xdoc.Element("SupplierAvailability");
        string _valSuspend = string.Empty;
        string startDate = nodeSupplier.Element("Unavailability").Element("StartDate").Value;
        if (!string.IsNullOrEmpty(startDate))
        {
            DateTime Start_Date = ConvertToDateTime.CalanderToDateTime(startDate, siteSetting.CrmDateFormat);
            txtFromDate.Text = (string.IsNullOrEmpty(siteSetting.DateFormat)) ?
                string.Format(siteSetting.DateFormat, Start_Date) : string.Format(siteSetting.DateFormat, Start_Date);
            string _from_date = string.Format(siteSetting.DateFormat, Start_Date);
            string _to_date = "", _reason = "";
            string endDate = nodeSupplier.Element("Unavailability").Element("EndDate").Value;
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime End_Date = ConvertToDateTime.CalanderToDateTime(endDate, siteSetting.CrmDateFormat);
                txtToDate.Text = (string.IsNullOrEmpty(siteSetting.DateFormat)) ?
                    string.Format(siteSetting.DateFormat, End_Date) : string.Format(siteSetting.DateFormat, End_Date);
                _to_date = string.Format(siteSetting.DateFormat, End_Date);
                cb_UnlimitedTime.Checked = false;
            }
            else
            {
                cb_UnlimitedTime.Checked = true;
                span_ToDate.Style[HtmlTextWriterStyle.Display] = "none";
                span_UnlimitedTime.Style[HtmlTextWriterStyle.Display] = "inline";
            }

            string ReasonId = nodeSupplier.Element("Unavailability").Element("Reason").Attribute("ID").Value;
            if (!string.IsNullOrEmpty(ReasonId))
            {
                for (int i = 0; i < ddlReason.Items.Count; i++)
                {
                    if (ddlReason.Items[i].Value == ReasonId)
                    {
                        bool isInactive = ddlReason.Items[i].Attributes["Inactive"] != null;
                        bool isCantTouch = ddlReason.Items[i].Attributes["CantTouch"] != null;
                        if (isCantTouch)
                        {
                            ddlReason.Items[i].Enabled = true;
                            EnableUnavailability();
                        }
                        else
                        {
                            cb_UnlimitedTime.Visible = userManagement.IsPublisher();
                            if (isInactive)
                            {
                                ddlReason.Items[i].Enabled = true;
                            }
                        }
                        ddlReason.SelectedIndex = i;
                        _reason = ddlReason.SelectedItem.Text;
                        break;
                    }
                }
            }
            chk_suspend.Checked = true;
            string script = "showZminut();";
            string scriptName = "showZminut";
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), scriptName))
            {
                ClientScript.RegisterStartupScript(this.GetType(), scriptName, script, true);
            }
            _valSuspend = GetSuspendValue(_reason, _from_date, _to_date);
        }
        else
        {
            chk_suspend.Checked = false;
            cb_UnlimitedTime.Visible = userManagement.IsPublisher();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDateToday", "SetDateToday();", true);
            //   txtFromDate.Text = "";
            //  txtToDate.Text = "";
            _UpdatePanelToDate.Update();
        }
        SuspendV = _valSuspend;

    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.AlternatingItem && e.Item.ItemType != ListItemType.Item)
            return;
        RepeaterItem _item = e.Item;
        Label labelStatus = (Label)_item.FindControl("lblTo1");
        string _from = ((Label)_item.FindControl("lbl_fromTime")).Text;
        string _to = ((Label)_item.FindControl("lbl_toTime")).Text;
        DropDownList ddl_from = (DropDownList)_item.FindControl("ddl_From");
        LoadTimeFormat(ddl_from);
        DropDownList ddl_to = (DropDownList)_item.FindControl("ddl_To");
        LoadTimeFormat(ddl_to);
        if (string.IsNullOrEmpty(_from))
        {
            _from = "06:00";
            _to = "18:00";
            string _day = ((Label)_item.FindControl("lbl_day")).Text;
            if (_day == DayOfWeek.Sunday.ToString())
                ((Label)_item.FindControl("lblTo1")).Text = lblSuspend.Value;
        }
        ddl_from.SelectedIndex = FindIndx(ddl_from.Items, _from);
        ddl_to.SelectedIndex = FindIndx(ddl_to.Items, _to);
        ddl_to.Attributes.Add("onChange", "checkTimes('" + ddl_from.ClientID +
            "', '" + ddl_to.ClientID + "','" + labelStatus.ClientID + "');");
        ddl_from.Attributes.Add("onChange", "checkTimes('" + ddl_from.ClientID +
            "', '" + ddl_to.ClientID + "', '" + labelStatus.ClientID + "');");

        RadioButtonList rb = (RadioButtonList)_item.FindControl("rb_Mode");
        rb.Items.Add(new ListItem(lbl_SuspendCalls.Text, "cancel"));
        rb.Items.Add(new ListItem(lbl_ActiveDuring.Text, "operate"));
        HtmlGenericControl div_times = (HtmlGenericControl)_item.FindControl("div_times");

        if (labelStatus.Text == lblSuspend.Value)
        {
            rb.SelectedIndex = 0;
            div_times.Attributes.Add("style", "visibility:hidden");
        }
        else
        {
            rb.SelectedIndex = 1;
            labelStatus.Text = ddl_from.SelectedItem.Text + " - " + ddl_to.SelectedItem.Text;
            div_times.Attributes.Add("style", "visibility:visible");
        }

        rb.Attributes.Add("onclick", "SetTimeDiv(event, '" + div_times.ClientID + "', '" + ddl_from.ClientID + "', '" +
        ddl_to.ClientID + "', '" + labelStatus.ClientID + "');");

        HtmlAnchor ha = (HtmlAnchor)_item.FindControl("a_Edit");
        HtmlGenericControl hgc = (HtmlGenericControl)_item.FindControl("div_title");
        HtmlGenericControl hgcUp = (HtmlGenericControl)_item.FindControl("div_update");
        ha.Attributes.Add("href", "javascript:SetEditLink('" + hgc.ClientID + "', '" + hgcUp.ClientID + "');");
    }

    string GetSuspendValue(string _reason, string _from_date, string _to_date)
    {
        return _reason + " " + _from_date + " - " + _to_date;
    }

    protected bool updateWebServiceGeneral()
    {

        lbl_InvalidFromDate.Visible = false;
        lbl_InvalidToDate.Visible = false;
        lbl_ResonsMessage.Visible = false;
        string startDate = txtFromDate.Text;
        string endDate = txtToDate.Text;
        string errorMessage = string.Empty;
        string page_name = System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        string page_translate = EditTranslateDB.GetPageNameTranslate(page_name, siteSetting.siteLangId);
        string reasonId = string.Empty;
        if (ddlReason.SelectedIndex > -1)
            reasonId = ddlReason.Items[ddlReason.SelectedIndex].Value;

        StringBuilder str = new StringBuilder(@"<SupplierAvailability SiteId=""" + siteSetting.GetSiteID +
                @""" SupplierId=""" + GetGuidSetting() + @""">");

        List<WebReferenceSupplier.AuditEntry> list = new List<WebReferenceSupplier.AuditEntry>();
        str.Append("<Availability>");
        bool IsAllSuspend = true;
        foreach (RepeaterItem _item in _reapeter.Items)
        {
            WebReferenceSupplier.AuditEntry ae = new WebReferenceSupplier.AuditEntry();
            ae.PageId = page_name;
            ae.PageName = page_translate;
            string xmlDay;
            if (!updateWebServiceDay(_item, ref ae, out xmlDay))
                IsAllSuspend = false;
            //     string xmlDay = updateWebServiceDay(_item, ref ae);
            str.Append(xmlDay);
            if (ae != null)
                list.Add(ae);
        }
        if (IsAllSuspend)
        {
            string _script = "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AvailableAtLeastOneDay.Text) + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AvailableAtLeastOneDay", _script, true);
            return false;
        }

        str.Append("</Availability>");
        if (chk_suspend.Checked)
        {
            DateTime _startDate = DateTime.MinValue;
            DateTime _endDate = DateTime.MinValue;
            bool isOk = true;
            if (string.IsNullOrEmpty(startDate))
            {
                errorMessage = lbl_InvalidFromDate.Text;
                isOk = false;
            }
            else
            {
                _startDate = ConvertToDateTime.CalanderToDateTime(startDate, siteSetting.DateFormat);
                if (DateTime.Now.Date > _startDate)
                {
                    errorMessage = lbl_InvalidFromDate.Text;
                    isOk = false;
                }
                if (cb_UnlimitedTime.Checked)
                    _endDate = DateTime.MinValue;
                else if (string.IsNullOrEmpty(endDate))
                {
                    errorMessage = lbl_InvalidToDate.Text;
                    isOk = false;
                }
                else
                {
                    _endDate = ConvertToDateTime.CalanderToDateTime(endDate, siteSetting.DateFormat);
                    if (_endDate < _startDate)
                    {
                        _endDate = _startDate;
                        txtToDate.Text = string.Format(siteSetting.DateFormat, _endDate);
                        _UpdatePanelToDate.Update();
                    }
                }
            }

            if (ddlReason.SelectedIndex < 0)
            {
                isOk = false;
                errorMessage += lbl_ResonsMessage.Text;
            }
            if (!isOk)
            {
                _UpdatePanelToDate.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showZminut", "showZminut();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ErrorDate", "alert('" + errorMessage + "');", true);
                return false;
            }
            HasSuspend = true;
            startDate = string.Format(siteSetting.CrmDateFormat, _startDate);
            endDate = (_endDate == DateTime.MinValue) ? string.Empty : string.Format(siteSetting.CrmDateFormat, _endDate);

        }
        else
        {
            startDate = string.Empty;
            endDate = string.Empty;
            reasonId = string.Empty;
        }
        str.Append("<Unavailability>");
        str.Append("<StartDate>" + startDate + "</StartDate>");
        str.Append("<EndDate>" + endDate + "</EndDate>");

        str.Append(@"<ReasonId>" + reasonId + "</ReasonId>");
        str.Append("</Unavailability>");

        str.Append(@"</SupplierAvailability>");
        string _valueSuspend = (startDate != string.Empty) ?
            GetSuspendValue(ddlReason.SelectedItem.Text, startDate, endDate) :
            string.Empty;

        if (_valueSuspend != SuspendV)
        {
            UserMangement um = GetUserSetting();
            WebReferenceSupplier.AuditEntry _audit = new WebReferenceSupplier.AuditEntry();
            _audit.FieldId = lbl_suspend.ID;// reasonId;
            _audit.FieldName = lbl_suspend.Text;// ddlReason.SelectedItem.Text;
            _audit.NewValue = _valueSuspend;
            _audit.OldValue = SuspendV;
            _audit.PageId = page_name;
            _audit.PageName = page_translate;
            _audit.AdvertiseId = new Guid(um.GetGuid);
            _audit.AdvertiserName = um.User_Name;
            _audit.AuditStatus = (IsFirstTimeV) ? WebReferenceSupplier.Status.Insert : WebReferenceSupplier.Status.Update;
            _audit.UserId = new Guid(userManagement.GetGuid);
            _audit.UserName = userManagement.User_Name;
            list.Add(_audit);
        }

        if (list.Count == 0) // didn't update anything
        {
            _UpdatePanelToDate.Update();

            if (Session["firstUpdate"] == null && siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
            {
                WebReferenceSupplier.Supplier supplierTask = WebServiceConfig.GetSupplierReference(this);

                WebReferenceSupplier.ResultOfGetStartedTasksStatusResponse resultOfGetStartedTasksStatusResponse = null;
                try
                {
                    resultOfGetStartedTasksStatusResponse = supplierTask.RetrieveGetStartedTasksStatus(new Guid(GetGuidSetting()));
                }
                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc, siteSetting);
                }
                if (resultOfGetStartedTasksStatusResponse != null && resultOfGetStartedTasksStatusResponse.Type == WebReferenceSupplier.eResultType.Success && resultOfGetStartedTasksStatusResponse.Value != null)
                {

                    WebReferenceSupplier.GetStartedTasksStatusResponse getStartedTasksStatusResponse = resultOfGetStartedTasksStatusResponse.Value;
                    WebReferenceSupplier.GetStartedTasksStatus workHours = getStartedTasksStatusResponse.WorkHours;
                    if (!workHours.Done)
                    {
                        WebReferenceSupplier.GetStartedTaskType getStartedTaskType = WebReferenceSupplier.GetStartedTaskType.work_hours;
                        WebReferenceSupplier.ResultOfCrossOutGetStartedTaskResponse resultOfCrossOutGetStartedTaskResponse = new WebReferenceSupplier.ResultOfCrossOutGetStartedTaskResponse();

                        try
                        {
                            resultOfCrossOutGetStartedTaskResponse = supplierTask.CrossOutGetStartedTask(new Guid(GetGuidSetting()), getStartedTaskType);
                            if (resultOfCrossOutGetStartedTaskResponse.Type == WebReferenceSupplier.eResultType.Failure)
                                dbug_log.ExceptionLog(new Exception("site id: " + siteSetting.GetSiteID + " failed step work hours for supplier id: " + GetGuidSetting()), siteSetting.GetSiteID);
                            else // true
                            {
                                Session["firstUpdate"] = "1";

                                decimal balance = resultOfCrossOutGetStartedTaskResponse.Value.Balance;
                                string strBalance = string.Format(NUMBER_FORMAT, balance);
                                Session["balance"] = strBalance;
                            }
                        }
                        catch (Exception ex)
                        {
                            dbug_log.ExceptionLog(ex, siteSetting);
                        }
                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "noNewData", "top.NoNewData();", true);
            return false;
        }


        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.SetAvailabilityRequest _request = new WebReferenceSupplier.SetAvailabilityRequest();
        _request.AuditOn = true;
        _request.Request = str.ToString();
        _request.AuditEntries = list.ToArray();

        if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
        {
            if (userManagement.IsPublisher())
            {

            }
            else
            {
                WebReferenceSupplier.ResultOfGetStartedTasksStatusResponse  resultOfGetStartedTasksStatusResponse = supplier.RetrieveGetStartedTasksStatus(new Guid(GetGuidSetting()));
                WebReferenceSupplier.GetStartedTasksStatusResponse getStartedTasksStatusResponse = resultOfGetStartedTasksStatusResponse.Value;
                if (getStartedTasksStatusResponse.WorkHours == null || !getStartedTasksStatusResponse.WorkHours.Done) // if this is first time
                {                  
                    _request.SupplierId = new Guid(GetGuidSetting());
                    _request.IsFromNewAdvertiserPortal = true;
                    Session["firstUpdate"] = "1";
                }
            }
        }
        string result = string.Empty;
        try
        {
            result = supplier.SetAvailability(_request);
        }
        catch (Exception ex)
        {
            _UpdatePanelToDate.Update();
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return false;
        }

        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["SupplierAvailability"] == null || xdd["SupplierAvailability"]["Error"] != null)
        {
            _UpdatePanelToDate.Update();

            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return false;
        }
        //Refresh the search advertiser parent page
        if (userManagement.IsPublisher())
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "run_report", "try{top.run_report();}catch(ex){}", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Set_Status", "parent.SetAccountStatus();", true);
        }
        setListAudit(list);
        return true;
    }

    private void setListAudit(List<WebReferenceSupplier.AuditEntry> list)
    {
        Dictionary<string, string> dic = dicValueV;
        foreach (WebReferenceSupplier.AuditEntry ae in list)
        {
            if (!dic.ContainsKey(ae.FieldName))// || dic[ae.FieldName] != ae.NewValue)
                dic.Add(ae.FieldName, "");
            if (dic[ae.FieldName] != ae.NewValue)
                dic[ae.FieldName] = ae.NewValue;
        }
        dicValueV = dic;
    }

    protected bool updateWebServiceDay(RepeaterItem _item, ref WebReferenceSupplier.AuditEntry ae, out string _xml)
    {
        string day = ((Label)_item.FindControl("lbl_OrgDay")).Text;
        string dayT = ((Label)_item.FindControl("lbl_day")).Text;
        bool IsSuspend = false;
        StringBuilder str = new StringBuilder(@"<Day Name=""" + day.ToLower() + @""">");
        string _value = string.Empty;

        RadioButtonList ddlMode = (RadioButtonList)_item.FindControl("rb_Mode");
        string strMode = ddlMode.SelectedItem.Value;

        string strFrom = "";
        string strTo = "";

        if (strMode == "cancel")
        {
            strFrom = "1:00";
            strTo = "1:01";
            _value = lblSuspend.Value;
            IsSuspend = true;
        }
        else
        {
            DropDownList ddlDaysFrom = (DropDownList)_item.FindControl("ddl_From");
            DropDownList ddlDaysTo = (DropDownList)_item.FindControl("ddl_To");

            strFrom = ddlDaysFrom.SelectedValue;
            strTo = ddlDaysTo.SelectedValue;
            _value = strFrom + " - " + strTo;
        }
        Dictionary<string, string> dic = dicValueV;
        if (!(dic.ContainsKey(dayT) && dic[dayT] == _value && !IsFirstTimeV))
        {
            UserMangement um = GetUserSetting();
            ae.AdvertiseId = new Guid(um.GetGuid);
            ae.AdvertiserName = um.User_Name;
            ae.AuditStatus = (IsFirstTimeV) ? WebReferenceSupplier.Status.Insert : WebReferenceSupplier.Status.Update;
            ae.FieldId = day;
            ae.FieldName = dayT;
            ae.NewValue = _value;
            ae.OldValue = (IsFirstTimeV) ? "" : dicValueV[dayT];
            ae.UserId = new Guid(userManagement.GetGuid);
            ae.UserName = userManagement.User_Name;
        }
        else
            ae = null;
        str.Append(@"<FromTime>" + strFrom + "</FromTime>");
        str.Append(@"<TillTime>" + strTo + "</TillTime>");
        str.Append(@"</Day>");

        _xml = str.ToString();
        return IsSuspend;
    }
    Dictionary<string, string> dicValueV
    {
        get { return (ViewState["dicValue"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["dicValue"]; }
        set { ViewState["dicValue"] = value; }
    }
    bool IsFirstTimeV
    {
        get { return (ViewState["IsFirstTime"] == null) ? false : (bool)ViewState["IsFirstTime"]; }
        set { ViewState["IsFirstTime"] = value; }
    }
    string SuspendV
    {
        get { return (ViewState["Suspend"] == null) ? "" : (string)ViewState["Suspend"]; }
        set { ViewState["Suspend"] = value; }
    }

    protected string clientIdBalance
    {
        get { return (ViewState["clientIdBalance"] == null ? "" : (string)ViewState["clientIdBalance"]); }
        set { ViewState["clientIdBalance"] = value; }
    }

}