using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;

public partial class Management_regions : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {
                string csnameLogin = "setLogin";
                Type cstypeLogin = this.GetType();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager csLogin = this.Page.ClientScript;

                if (!csLogin.IsStartupScriptRegistered(cstypeLogin, csnameLogin))
                {
                    string csTextLogin = "redirect();";
                    csLogin.RegisterStartupScript(cstypeLogin, csnameLogin, csTextLogin, true);
                }
            }

            int level = Convert.ToInt16(Request["level"]);
            string startWith = Request["startWith"];

            
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            string xmlProfessions = supplier.HandleAdvertiserRegions(siteSetting.GetSiteID, level, GetGuidSetting(), startWith, "");
            //Response.Write(xmlProfessions);

            
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlProfessions);

            StringBuilder sb = new StringBuilder();
            XmlNode root = xml.DocumentElement; // Regions

            string ParentName="";

            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++) // element primary
                {
                    ParentName = root.ChildNodes[i].Attributes["ParentName"].InnerText;
                    if (ParentName!="")
                    {
                        sb.Append(ParentName +  " >> " + root.ChildNodes[i].Attributes["Name"].InnerText  + 
                             "=" + root.ChildNodes[i].Attributes["ID"].InnerText + "*" +
                             root.ChildNodes[i].Attributes["HasChildRegions"].InnerText
                            + "&");
                    }
                    else
                    {
                        sb.Append(root.ChildNodes[i].Attributes["Name"].InnerText + "=" +
                            root.ChildNodes[i].Attributes["ID"].InnerText + "*" + root.ChildNodes[i].Attributes["HasChildRegions"].InnerText
                            + "&");
                    }
                }

                Response.Write(sb.ToString().Substring(0, sb.ToString().Length - 1));
            }

            else
                Response.Write("empty");
            
            


            /*
            
            if (level == "1")
            {
                if (startWith == "")
                {
                    level = "TelAviv1=9A5777FB-EA8F-DF11-9C13-0003FF727321*0&beit shemesh=8*1&lud=9*1&golan=10*0&yavne=11*0&nazeret=67*1&beer sheva=8*0&bat yam=7*1&natanya=9A5777FB-EA8F-DF11-9C13-0003FF727321*1";
                    //level = "TelAviv1=9A5777FB-EA8F-DF11-9C13-0003FF727321*0&Jerusalem=9A5777FB-EA8F-DF11-9C13-0003FF727321*0&natanya=9A5777FB-EA8F-DF11-9C13-0003FF727321*1";
                }

                else
                {
                    level = "beit shemesh=8*1&beer sheva=8*0&bat yam=7*1";

                }
            }
            else if (level == "2")
                level = "petach tikva=5*1&kfarsaba=6*1&nahariya=7*0";
            else if (level == "3")
                level = "beit shemesh=8*1&lud=9*1&golan=10*0";
            else if (level == "8")
                level = "beit shemesh=8*1&lud=9*1&golan=10*0&yavne=11*0&nazeret=67*1&beer sheva=8*0&bat yam=7*1&natanya=9A5777FB-EA8F-DF11-9C13-0003FF727321*1";
            Response.Write(level);
           */



        }


    }
}
