using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Globalization;
using System.Xml.Linq;
using System.Linq;
using System.Web;

public partial class Management_Mycalls2 : PageSetting, ICallbackEventHandler
{
    const string SCRAMBEL = "simsim";
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    private const string RECORD_KEY = "RecordCalls";
    const string REPORT_NAME = "CallsReport";
    protected string PAGE_NAME = PagesNames.MiniSite.ToString();
    string CallBackVal = "";
    
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);  
        if (userManagement.IsPublisher())
            this.MasterPageFile = "../Publisher/MasterPageFrame.master";
        else if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
            this.MasterPageFile = ResolveUrl("~") + "Controls/MasterPagePPC.master";
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (!string.IsNullOrEmpty(xmlV))
        {
            if (!ClientScript.IsStartupScriptRegistered("charts"))
            {
                string script = setWaiting(xmlV);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "charts", script, true);
            }
        }        
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        FromToDate1.ReportExec+=new EventHandler(FromToDate1_ReportExec);
        
        Button _button = (Button)FromToDate1.FindControl("btnSubmit");
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(_button);
        SetupClient();
        SetToolboxEvents();
        if (!IsPostBack)
        {
           
            foreach (ListItem li in lb_status.Items)
            {
                if (li.Value.ToLower() == "all")
                {
                    li.Selected = true;
                    break;
                }
            }
            SetToolbox();
            LoadRefundStatus();
            LoadAllowedToRecord();
        }
        else
        {
            LoadTRInline();
            Dictionary<int, string> dic = PageListV;
            foreach (KeyValuePair<int, string> kvp in dic)
                PlaceHolderPages.Controls.Add(MakeCell(kvp.Value, kvp.Key));            
        }
        
        
    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lblTitleAnalytics.Text);
    }
    void SetToolboxEvents()
    {
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
    }

    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordToPrint) + "');", true);
            return;
        }


        Session["data_print"] = dataV;
        Session["grid_print"] = _reapeter;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordExport) + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, REPORT_NAME);
        if (!te.ExecExcel(GetExcelTable(dataV)))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    DataTable GetExcelTable(DataTable data)
    {
        Dictionary<eYesNo, string> dic = new Dictionary<eYesNo,string>();
        foreach(eYesNo s in Enum.GetValues(typeof(eYesNo)))
        {
            string tran = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", s.ToString());
            dic.Add(s, tran);
        }
        DataTable NewData = data.Copy();
        NewData.Columns.Remove("Recording");
  //      NewData.Columns.Remove("HasRecord");
        NewData.Columns.Remove("InRefundStatus");
   //     NewData.Columns.Remove("StatusPic");
        NewData.Columns.Add("Charged");
        NewData.Columns.Remove("CallId");
        NewData.Columns.Remove("RefundStatus");
        foreach (DataRow row in NewData.Rows)
        {
            row["Win"] = (row["Win"].ToString() == ResolveUrl("~") + "Publisher/images/icon-v.png") ? dic[eYesNo.Yes] : dic[eYesNo.No];
            row["Charged"] = (row["StatusPic"].ToString() == ResolveUrl("~") + "Publisher/images/icon-v.png") ? dic[eYesNo.Yes] :
                ((row["StatusPic"].ToString() == ResolveUrl("~") + "Publisher/images/icon-watch.png") ? RefundStatusV[WebReferenceSite.RefundSatus.Pending.ToString()] : dic[eYesNo.No]);
        }
        NewData.Columns.Remove("StatusPic");
        return NewData;
            
    }
    private void LoadRefundStatus()
    {
        Dictionary<string, string> dic = new Dictionary<string,string>();
 	    foreach(string s in Enum.GetNames(typeof(WebReferenceSupplier.RefundSatus)))
        {
            dic.Add(s, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "RefundSatus", s));
        }
        RefundStatusV = dic;
    }

    private void LoadAllowedToRecord()
    {
        /*
        if (userManagement.IsPublisher())
        {
            AllowedToRecord = false;
            return;
        }
         * */
        IAsyncResult arConfigSite;
        string result = string.Empty;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        try
        {
            bool PublishCanHear = true;
            arConfigSite = _site.BeginGetConfigurationSettings(null, null);
            if (userManagement.IsPublisher())
            {
                PublishCanHear = PublisherAllowToRecord();
            }
            result = _site.EndGetConfigurationSettings(arConfigSite);
            if (!PublishCanHear)
            {
                AllowedToRecord = false;
                return;
            }
            XDocument xdd = XDocument.Parse(result);
            if (xdd.Element("Configurations") == null || xdd.Element("Configurations").Element("Error") != null)
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }

            AllowedToRecord = ((from p in xdd.Element("Configurations").Elements()
                                where p.Element("Key").Value == RECORD_KEY
                                select p).FirstOrDefault().Element("Value").Value) == "1";
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        
    }
    bool PublisherAllowToRecord()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfBoolean result = null;
        result = _site.IsCanHearAllRecordings(userManagement.Get_Guid);
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return false;
        }
        return result.Value;
    }
    protected void FromToDate1_ReportExec(Object sender, EventArgs e)
    {
        
        placeHolderCalls.Controls.Clear();
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        DataTable data = new DataTable();
        data.Columns.Add("Recording");
        data.Columns.Add("HasRecord", typeof(bool));
        data.Columns.Add("PrimaryExpertise");
        data.Columns.Add("Title");
        data.Columns.Add("TicketNumber");
        
        data.Columns.Add("CreatedOn");
        data.Columns.Add("Created_On");
        data.Columns.Add("Win");
        data.Columns.Add("RefundStatus");
        
        data.Columns.Add("InRefundStatus", typeof(bool));
        data.Columns.Add("StatusPic");
        data.Columns.Add("CallId");

     //   Dictionary<int, string> dic = new Dictionary<int, string>();
        DateTime to_date = FromToDate1.GetDateTo;
        DateTime from_date = FromToDate1.GetDateFrom;
        if (to_date == DateTime.MinValue)
        {
            to_date = DateTime.Now;
        }
        if (from_date == DateTime.MinValue)
        {
            from_date = to_date.AddMonths(-1);
        }
        else
        {            
            if (from_date > to_date)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + HttpUtility.JavaScriptStringEncode(FromToDate1.GetDateError) + "');", true);
                
                return;
            }
        }
        

        string publisherId = (userManagement.IsPublisher()) ? userManagement.GetGuid : string.Empty;
        WebReferenceSupplier.IncidentWinStatus iws = WebReferenceSupplier.IncidentWinStatus.ALL;
        foreach(WebReferenceSupplier.IncidentWinStatus _iws in 
            Enum.GetValues(typeof(WebReferenceSupplier.IncidentWinStatus)))
        {
            if (_iws.ToString().ToLower() == lb_status.SelectedValue.ToLower())
            {
                iws = _iws;
                break;
            }
        }

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);// WebServiceConfig.GetSupplierReference(this);
        string result="";
        try
        {
            result = supplier.GetSuppliersIncidents(publisherId, GetGuidSetting(), from_date, to_date, "", iws);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);
            /*
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
             * */
        if (xdd.Element("Incidents") == null || xdd.Element("Incidents").Element("Error") != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        /*
        if (!xdd["Incidents"].HasChildNodes)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "crmNotResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
            CleanPager();           
            return;
        }
    */
        foreach (XElement node in xdd.Element("Incidents").Elements())
        {
            
            DataRow row = data.NewRow();
            /*
            int _index;
            do
            {
                _index = new Random().Next();
            }
            while (dic.ContainsKey(_index));
             * */
            string _path = (node.Attribute("RecordLocation") == null) ? string.Empty : node.Attribute("RecordLocation").Value;
            if (!string.IsNullOrEmpty(_path))
            {
                row["HasRecord"] = true;
                _path = EncryptString.Encrypt_String(_path, SCRAMBEL);
                row["Recording"] = "javascript:OpenRecord('" + _path + "');";
            }
            else
                row["HasRecord"] = false;
            
         //   dic.Add(_index, _path);
          //  row["HasRecord"] = !string.IsNullOrEmpty(_path);
            bool IsWin = !(node.Attribute("Win") == null || node.Attribute("Win").Value == "0" || node.Attribute("Win").Value == "False");
            if (!IsWin)
                row["Win"] = ResolveUrl("~") + "Publisher/images/icon-x.png";
            else
                row["Win"] = ResolveUrl("~") + "Publisher/images/icon-v.png";
            
            row["PrimaryExpertise"] = (node.Attribute("PrimaryExpertise") == null) ? string.Empty : node.Attribute("PrimaryExpertise").Value;
            row["Title"] = (node.Attribute("Title") == null) ? string.Empty : node.Attribute("Title").Value;
            row["TicketNumber"] = (node.Attribute("TicketNumber") == null) ? string.Empty : node.Attribute("TicketNumber").Value;
            string CreatedOn = (node.Attribute("CreatedOn") == null) ? string.Empty : node.Attribute("CreatedOn").Value;
            CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");
            DateTime dt = DateTime.Parse(CreatedOn, ci);
            row["CreatedOn"] = string.Format(siteSetting.DateFormat, dt);
            row["Created_On"] = string.Format(siteSetting.DateFormat, dt) + ", " + string.Format(siteSetting.TimeFormat, dt);
            row["CallId"] = node.Attribute("CallId").Value;
            string refundStatus = (node.Attribute("RefundStatus") == null) ? string.Empty : node.Attribute("RefundStatus").Value;
            if (!string.IsNullOrEmpty(refundStatus))
            {        
                if (refundStatus == "false")
                    row["StatusPic"] = ResolveUrl("~") + @"Publisher/images/icon-x.png";//@"images/icon-thumbs-down.png";                
                else if (refundStatus == WebReferenceSite.RefundSatus.Pending.ToString())
                {
      
                    row["StatusPic"] = ResolveUrl("~") + @"Publisher/images/icon-watch.png";
                    row["RefundStatus"] = RefundStatusV[WebReferenceSite.RefundSatus.Pending.ToString()];
                }
                else// if (refundStatus == "true")
                    row["StatusPic"] = ResolveUrl("~") + @"Publisher/images/icon-v.png";//@"images/icon-thumbs-up.png";
            }
                       

            row["InRefundStatus"] = (!string.IsNullOrEmpty(refundStatus));
            data.Rows.Add(row);
        }
        
        if (data.Rows.Count > 0)
        {
            CurrentPage = 0;
            LoadCallUsing(data, from_date, to_date);
            dataV = data;
    //        RecordListV = dic;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "crmNotResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
            CleanPager();
      //      return;
        }
        lbl_RecordMached.Text = lblRecordMached.Text + " " + data.Rows.Count;
        _uPanel.Update();
    }
    protected void BindData(DataTable data)
    {
        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;
        objPDS.PageSize = ITEM_PAGE;
        objPDS.DataSource = data.DefaultView;

        if (data.Rows.Count > 0)
        {
            objPDS.CurrentPageIndex = CurrentPage;
            lnkPreviousPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkNextPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            LoadPages(data.Rows.Count);
            _reapeter.DataSource = objPDS;
            _reapeter.DataBind();
            ((HtmlTableCell)_reapeter.Controls[0].Controls[0].FindControl("th_records")).Visible = AllowedToRecord;
            int classindx = 0;
            foreach (RepeaterItem  _item in _reapeter.Items)
            {                            
                HtmlTableRow trDetails = (HtmlTableRow)_item.FindControl("tr_openClose");
                HtmlTableRow tr = (HtmlTableRow)_item.FindControl("tr_show");
                HtmlAnchor a_open = (HtmlAnchor)_item.FindControl("a_open");
                HtmlTableCell td = (HtmlTableCell)_item.FindControl("td_openClose");                
                tr.Attributes["class"] = (classindx++ % 2 == 0) ? "odd" : "even";
                string _CallId = (((Label)_item.FindControl("lbl_CallId")).Text);
           //     HtmlGenericControl _iframe = (HtmlGenericControl)_item.FindControl("iframe_details");
                Control _iframe = _item.FindControl("iframe_details");
                a_open.Attributes["href"] = "javascript:OpenCloseDetail('" + trDetails.ClientID + "','" + td.ClientID +
                    "', '" + _iframe.ClientID + "', 'MyCallDetails.aspx?CallId=" + _CallId + "');";

                ((HtmlTableCell)_item.FindControl("td_records")).Visible = AllowedToRecord;
                ((HtmlTableCell)_item.FindControl("td_details")).ColSpan = (AllowedToRecord) ? 8 : 7;
                
      
             }           
            div_paging.Visible = true;
        }
        else
        {
            CleanPager();           
        }
        _uPanel.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ResetWin", "ResetWin();", true);
    }
    
    void CleanPager()
    {
        xmlV = string.Empty;
        PlaceHolderPages.Controls.Clear();       
        div_paging.Visible = false;
        lbl_RecordMached.Text = string.Empty;
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        _uPanel.Update();
    }
    private void LoadCallUsing(DataTable data, DateTime fromDate, DateTime toDate)
    {
        DateTime fromDate2 = fromDate;
        DateTime toDate2 = toDate.AddDays(1);
       //// to check
        if (toDate2 - fromDate2  <= TimeSpan.FromDays(-1))
            return;
  //      ////
        StringBuilder strchart = new StringBuilder();
        strchart.Append(@"<chart caption='"+lbl_UnitCalls.Text +"' ");
        strchart.Append(@"xAxisName='"+lbl_Days.Text+"' ");
        strchart.Append(@"yAxisName='"+lbl_Calls.Text+"' ");
        strchart.Append(@"showNames='1' showValues='0'  showColumnShadow='1'  slantLabels='1'");//rotateNames=""1""
        strchart.Append(@" animation='1' showAlternateHGridColor='1' AlternateHGridColor='ff5904'");
        strchart.Append(@" divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' labelDisplay='ROTATE'");
        strchart.Append(@" canvasBorderColor='666666' baseFontColor='666666' lineColor='FF5904' lineAlpha='85'>");
        string dateFormat = (!string.IsNullOrEmpty(siteSetting.DateFormat))? 
            siteSetting.DateFormat : "{0:MM/dd/yyyy}";

        int _days = (int)((toDate2 - fromDate2).TotalDays);
        int modulu = _days / 25;
      //  if (modulu == 0)
         modulu++;
        int indx = 0;
        string dateStr = "";
        for (; fromDate2.CompareTo(toDate2) != 0; fromDate2 = fromDate2.AddDays(1.0))
        {
       //     dateStr = fromDate2.ToShortDateString();// + "/" + fromDate2.Day + "/" + fromDate2.Year;
            if (indx++ % modulu == 0)
            {
                dateStr = String.Format(dateFormat, fromDate2);
                strchart.Append("<set label='" + dateStr + "' value='" + NumOfCallInOneDay(data, fromDate2) + "' />");
            }
            else
                strchart.Append("<set value='" + NumOfCallInOneDay(data, fromDate2) + "' />");
        }
        strchart.Append("</chart>");
       
        xmlV = strchart.ToString(); 
        BindData(data);       
    }
    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        //if there is only one page
        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListV = null;
            return;
        }

        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListV = dic;
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell(kvp.Value, kvp.Key));
    }
    
    private string GetHtmlObject(string _path)
    {
      
        StringBuilder sb = new StringBuilder();
       sb.Append(@"<object data=""../play/dewplayer.swf"" width=""200"" height=""20"" name=""dewplayer"" id=""dewplayer"" type=""application/x-shockwave-flash"">");
       sb.Append(@"<param name=""movie"" value=""../play/dewplayer.swf"" />");
       sb.Append(@"<param name=""flashvars"" value=""mp3=" + _path + @"&amp;autostart=1"" />");
       sb.Append(@"<param name=""wmode"" value=""transparent"" />");
 //      sb.Append(@"<embed wmode=""transparent"" src=""flash/home.swf" width="220" height="271" /> 
       sb.Append(@"</object>");
       return sb.ToString();
        
    }
    private string setWaiting(string _xml)
    {
        StringBuilder sb = new StringBuilder();
    
        sb.Append(@"<div id=""chartdiv2"" align=""center""></div>");
 
        string script = @"LoadChart(""" + _xml + @""",""chartdiv2"", """ + 
            ResolveUrl("~") + @"Management/FusionCharts/Charts/Line.swf"");";

        LiteralControl litcon = new LiteralControl(sb.ToString());
        placeHolderCalls.Controls.Add(litcon);
        return script;



    }
    HtmlTableCell MakeCell(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();
        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }
    int NumOfCallInOneDay(DataTable data, DateTime date)
    {
        int index = 0;
        for (int i = 0; i < data.Rows.Count; i++)
        {
            DateTime dt = ConvertToDateTime.CalanderToDateTime(data.Rows[i]["createdon"].ToString(), siteSetting.DateFormat);  
            if (dt.Date.CompareTo(date.Date) == 0)
                index++;
        }
        return index;
    }
    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        return CallBackVal;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        if (eventArgument.StartsWith("[0]"))
            SetListTR(eventArgument);
        else if (eventArgument.StartsWith("[1]"))
            SetChart();
        
    }
    void SetListTR(string eventArgument)
    {
        eventArgument = eventArgument.Replace("[0]", "");
        List<string> list = listTR;
        string[] args = eventArgument.Split(';');
        if (args[1] == "1")
            list.Add(args[0]);
        else
            list.Remove(args[0]);
        listTR = list;
        CallBackVal = string.Empty;
    }
    void SetChart()
    {

    }

    #endregion
    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }
    private void LoadTRInline()
    {
        List<string> list = listTR;
        
        foreach (RepeaterItem item in _reapeter.Items)
        {
            HtmlTableRow trDetails = (HtmlTableRow)item.FindControl("tr_openClose");
            HtmlTableCell td_open = (HtmlTableCell)item.FindControl("td_openClose");
            if (list.Contains(trDetails.ClientID))
            {
                trDetails.Attributes.Remove("style");
                td_open.Attributes["class"] = "open first";
            }
            else
            {
                trDetails.Attributes["style"] = "display:none";
                td_open.Attributes["class"] = "close first";
            }
        }
        
    }
    protected void lnkNextPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;
        listTR = new List<string>();
        BindData(dataV);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;
        listTR = new List<string>();
        BindData(dataV);
    }
    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
        listTR = new List<string>();
        BindData(dataV);
    }
    
    DataTable dataV
    {
        get { return (Session["data"] == null) ? null : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    List<string> listTR
    {
        get { return (Session["list_TR"] == null) ? new List<string>() : (List<string>)Session["list_TR"]; }
        set { Session["list_TR"] = value; }
    }
    string xmlV
    {
        get { return (ViewState["chartXml"] == null) ? string.Empty : (string)ViewState["chartXml"]; }
        set { ViewState["chartXml"] = value; }
    }
    int CurrentPage
    {
        get
        {
            object o = this.ViewState["_CurrentPageAdvertiser"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["_CurrentPageAdvertiser"] = value;
        }
    }
    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }
    bool AllowedToRecord
    {
        get { return (ViewState["AllowedToRecord"] == null) ? false : (bool)ViewState["AllowedToRecord"]; }
        set { ViewState["AllowedToRecord"] = value; }
    }
    /*
    Dictionary<int, string> RecordListV
    {
        get { return (Session["RecordList"] == null) ? new Dictionary<int, string>() : (Dictionary<int, string>)Session["RecordList"]; }
        set { Session["RecordList"] = value; }
    }
     * */
    protected string GetAudioChrome
    {
        get { return ResolveUrl("~") + "Management/AudioChrome.aspx"; }
    }
    protected Dictionary<string, string> RefundStatusV
    {
        get { return (ViewState["RefundStatus"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["RefundStatus"]; }
        set { ViewState["RefundStatus"] = value; }
    }
    protected string record_path
    {
        get { return ResolveUrl("~/Publisher/DpzControls/ZapRecord.aspx"); }
    }
    
}
