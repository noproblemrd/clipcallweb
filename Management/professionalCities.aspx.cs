using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web.Services.Protocols;


public partial class Management_professionalCities : PageSetting
{
    public string source = "";
    public string lat = "";
    public string lot = "";
    public string address = "";
    protected string root = "";
    protected string RegistrationStage;
    protected string xmlCities = "";
    protected HiddenField hidden; 

    protected void Page_Unload(object sender, EventArgs e)
    {
        //Session["streetOrCityChange"] = null;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {           

            Session["ifUpdateCities"] = "false";           

            //Response.Write("session:" + Session["streetOrCityChange"] + " source:" + source);
            //Response.Write("<br>Session['ifUpdateCities']:" + Session["ifUpdateCities"]);            
            
            root = ResolveUrl("~");

            //Response.Write("streetOrCityChange:" + Session["streetOrCityChange"].ToString());
            lbl_page.Text = " 6";

            Session["myGuid"] = GetGuidSetting();
            //Response.Write("session: " + Session["myGuid"].ToString());
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {
                string csnameLogin = "setLogin";
                
                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager csLogin = this.Page.ClientScript;

                if (!csLogin.IsClientScriptBlockRegistered(this.GetType(), csnameLogin))
                {
                    string csTextLogin = "top.location='" + ResolveUrl("~") + @"Management/ProfessionLogin.aspx';";
                    csLogin.RegisterClientScriptBlock(this.GetType(), csnameLogin, csTextLogin, true);
                }
                return;
            }
                     

            //Response.Write("country:" + Session["country"]);
            //Response.Write("siteSetting.GetSiteID:" + siteSetting.GetSiteID + "<br>");
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            try
            {
                xmlCities = supplier.GetWorkArea(siteSetting.GetSiteID, GetGuidSetting());
                
                //Response.Write("xmlCities:" + xmlCities);
            }
            catch (SoapException ex)
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
            
            
            //Response.Write(xmlCities);
             

            /* 
             * <Areas RegistrationStage="3" Longitude="117.48" Latitude="30.54" Radius="100">
               <Area RegionId="00657fd7-5a87-df11-9c13-0003ff727321" Name="宁化县" />
             * <Area RegionId="c75f6d0b-8587-df11-9c13-0003ff727321" Name="余江县" />
             * <Area RegionId="89c69f9f-6a87-df11-9c13-0003ff727321" Name="宜阳县" />
             * <Area RegionId="0369504e-5a87-df11-9c13-0003ff727321" Name="光泽县" />
             * <Area RegionId="f9f25046-5b87-df11-9c13-0003ff727321" Name="武夷山市" />
             * <Area RegionId="9870aab2-8287-df11-9c13-0003ff727321" Name="东乡县" />
             * <Area RegionId="b016e623-8587-df11-9c13-0003ff727321" Name="资溪县" />
             * </Areas>
             * 
            */

            /*
             <Areas RegistrationStage="6" Longitude="0" Latitude="0" Radius="0">
              <SelectedAreas />
              <NotSelectedAreas />
             </Areas>
            */

            /*
            xmlCities="<Areas RegistrationStage=\"6\" Longitude=\"0\" Latitude=\"0\" Radius=\"0\">";  
            xmlCities+="<SelectedAreas>";            
            xmlCities+="<Area RegionId=\"7\" Name=\"yyy\" />";
            xmlCities+="<Area RegionId=\"9\" Name=\"jjj\" />";
            xmlCities += "<Area RegionId=\"9\" Name=\"luftanza\" />";
            xmlCities += "<Area RegionId=\"9\" Name=\"leben\" />";
            xmlCities += "<Area RegionId=\"9\" Name=\"natanya\" />";
            xmlCities += "<Area RegionId=\"9\" Name=\"yafo\" />";
            xmlCities += "<Area RegionId=\"9\" Name=\"tel aviv\" />";
            xmlCities += "<Area RegionId=\"9\" Name=\"yavne\" />";
            xmlCities += "<Area RegionId=\"9\" Name=\"lod\" />";
            xmlCities += "<Area RegionId=\"9\" Name=\"luxemburg\" />";
            xmlCities += "<Area RegionId=\"9\" Name=\"lemech\" />";
            xmlCities+="</SelectedAreas>";
                  
            xmlCities+="<NotSelectedAreas>";
            //xmlCities+="<Area RegionId=\"6\" Name=\"ooo\" />";
            //xmlCities+="<Area RegionId=\"44\" Name=\"hhh\" />";
            //xmlCities += "<Area RegionId=\"34\" Name=\"jerusalem\" />";
            //xmlCities += "<Area RegionId=\"54\" Name=\"telaviv\" />";
            //xmlCities += "<Area RegionId=\"64\" Name=\"haifa\" />";
            xmlCities+="</NotSelectedAreas>";
            xmlCities+="</Areas>";
            */


            //Response.Write("xmlCities:" + xmlCities);
            
            XmlDataDocument xml = new XmlDataDocument();
            xml.LoadXml(xmlCities);
            
            
            XmlNode xmlroot = xml.DocumentElement; // Areas

            /**************  RegistrationStage Influences ****************/

            RegistrationStage = xmlroot.Attributes["RegistrationStage"].InnerText;
            //RegistrationStage = "1";

            string csnameStep = "setStep";
            Type cstypeStep = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager csStep = this.Page.ClientScript;


            if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
            {
                string csTextStep = "witchAlreadyStep(" + RegistrationStage + ");";
                csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
            }

            ClientScriptManager csUpdateRegistration = this.Page.ClientScript;

            string csnameUpdateRegistration = "updateRegistrationStage";
            string csTextUpdateRegistration = "";

            string mode="";

            if (int.Parse(RegistrationStage) >= 3)
            {
                if (!csUpdateRegistration.IsStartupScriptRegistered(cstypeStep, csnameUpdateRegistration))
                {
                    csTextUpdateRegistration = "updateSendButton('UPDATE','" + Hidden_Update.Value + "');";
                    csUpdateRegistration.RegisterStartupScript(cstypeStep, csnameUpdateRegistration, csTextUpdateRegistration, true);
                    mode="UPDATE";
                }
            }
            else
            {
                if (!csUpdateRegistration.IsStartupScriptRegistered(cstypeStep, csnameUpdateRegistration))
                {
                    csTextUpdateRegistration = "updateSendButton('NEXT','" + Hidden_Next.Value + "');";
                    csUpdateRegistration.RegisterStartupScript(cstypeStep, csnameUpdateRegistration, csTextUpdateRegistration, true);
                    mode="NEXT";
                }
            }
             
            /**************  End RegistrationStage Influences ****************/


            string radiusAttribute = xmlroot.Attributes["Radius"].InnerText;

            lat = xmlroot.Attributes["Latitude"].InnerText;

            lot = xmlroot.Attributes["Longitude"].InnerText;           

            /************* set radius *****************/
            if (radiusAttribute != "" && radiusAttribute !="0")
            {
                for (int i = 0; i < radius.Items.Count; i++)
                {
                    if (radius.Items[i].Value == radiusAttribute)
                        radius.Items[i].Selected = true;
                }
            }

            else
            {
                ListItem item10 = radius.Items.FindByText("10");
                item10.Selected = true;
            }
            /***************** end set radius ******************/

            /***************** set sessions ********************/
            if(mode=="NEXT") // new supplier
                source = "address";
            else if (Session["streetOrCityChange"] != null && Session["streetOrCityChange"].ToString() == "true")
                source = "address";
            else if (Session["streetOrCityChange"] != null && Session["streetOrCityChange"].ToString() == "false")
                source = "latLot";
            else if (lat != "" && lot != "")
                source = "latLot";
            else
                source = "address";

            //Response.Write("source:" + source);
            
            
            /***************** end set sessions ********************/

            /************* set cities *****************/

            if (source=="latLot")
            {
                XmlNode xmlNodeSelectedAreas = xmlroot.SelectSingleNode("SelectedAreas");

                if (xmlNodeSelectedAreas.HasChildNodes)
                {
                    for (int i = 0; i < xmlNodeSelectedAreas.ChildNodes.Count; i++)
                    {
                        ListItem listItem = new ListItem(xmlNodeSelectedAreas.ChildNodes[i].Attributes["Name"].InnerText, xmlNodeSelectedAreas.ChildNodes[i].Attributes["RegionId"].InnerText);
                        selectCities.Items.Add(listItem);
                    }
                }


                XmlNode xmlNodeNotSelectedAreas = xmlroot.SelectSingleNode("NotSelectedAreas");

                if (xmlNodeNotSelectedAreas != null && xmlNodeNotSelectedAreas.HasChildNodes)
                {
                    for (int i = 0; i < xmlNodeNotSelectedAreas.ChildNodes.Count; i++)
                    {
                        ListItem listItem = new ListItem(xmlNodeNotSelectedAreas.ChildNodes[i].Attributes["Name"].InnerText, xmlNodeNotSelectedAreas.ChildNodes[i].Attributes["RegionId"].InnerText);
                        selectNotCities.Items.Add(listItem);
                    }
                }
            }
            /***************** end set cities ******************/

            /************* set lat lot or address *****************/

            address = Session["address"].ToString();
            //Response.Write("session address:" + address);
            //中国 // china
            //Response.Write("address:" + address);
            //address = "20 XUANWU RD, WEIYANG,CHINA";

            //address = "中国陕西西安未央区玄武路20号";  // correct
            //address = "玄武研发 20,维扬,中国";
            //address = "维扬,中国"; // good
            //address = "维扬,中国";
            //address = "玄武研发街 20号,维扬,中国";
            //address = "中国,维扬"; 
            //address ="中国上海爱辉路30号";
            //address = "白河路30号,上海,中国"; // good

            //address ="中国上海白河路30号"; //BaiHe Rd. 30, Shanghai 
            //address = "中国上海白河路30号";
            //中国 china
            // 上海  Shanghai
            // 白河路    路 is road
            // 街 is street
            // 30号  号 is street number

            //address ="中国上海北海路30号"; // BeiHai Rd. 30, Shanghai 

            //address = "中国广州市海珠区新港东路2号";
            //No.2, Xingang East Road, Haizhu District, Guangzhou, China

            //address = "中国辽宁省沈阳市沈河区北京街21号";
            //No.21, Beijing Street, Shenhe District, Shenyang, Liaoning, China
            //address = "modiin, israel";
            //address = "白河路30号,上海,中国";
            /*
             文正街66号, 南阳市, 河南省, 473000, 中国
            
            But the above is actually wrong:

            It should be: 
            中国 = country
            河南省 = province
            南阳市 473000 = city with postal code
            文正街66号 = address
            [Recipient's name or c/o name if there is any will be put on this line]

            In English: 
            [Recipient's name or c/o name here]
            66 Wenzheng Road 
            Nanyang, Henan [The province name could be omitted, but can be put here for clarity when you put it in English] 473000 
            China 

            473000 is Nanyang's postal code.

            街 literally means "street", but for "Wenzheng Road", it's correct. They just translated it into English differently.

            In English you do not need the word "number" after the building's number / house number, but in Chinese you MUST put 号 after the number.

            */
            //address = "4th street,Sandton, Gauteng, South Africa";
            //Response.Write("<br>real address:" + address);

            
            //Response.Write(Session["streetChange"].ToString());
            /***************** end set lat lot or address ******************/
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
        }
        if (radius.Items.FindByValue("-2") == null)
            radius.Items.Add(new ListItem(lbl_allArea.Text, "-2"));
        Page.Header.DataBind();
    }
    

    protected void sndBtn_Click(object sender, EventArgs e)
    {  
        string csname3 = "setStep3";
        Type cstype3 = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs2 = this.Page.ClientScript;

        if (!cs2.IsStartupScriptRegistered(cstype3, csname3))
        {
            string cstext3 = "updateCities();";
            cs2.RegisterStartupScript(cstype3, csname3, cstext3, true);
        }
        
    }

   
}
