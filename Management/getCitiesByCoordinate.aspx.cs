using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Management_getCitiesByCoordinate : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string lat = Request["lat"];
        string lng = Request["lng"];
        string radius = Request["radius"];

     //   SiteSetting ss = (SiteSetting)Session["Site"];       
        /*
        string xmlCoordinates = "";
        xmlCoordinates += "<WorkArea SiteId=\"" + ss.GetSiteID + "\" SupplierId=\"" + Session["myGuid"].ToString() + "\">";
        xmlCoordinates += "<Details>";
        xmlCoordinates += "<Radius>" + radius + "</Radius>";
        xmlCoordinates += "<Latitude>" + lat + "</Latitude>";
        xmlCoordinates += "<Longitude>" + lng + "</Longitude>";
        xmlCoordinates += "</Details>";
        xmlCoordinates += "</WorkArea>";
        //Response.Write(xmlCoordinates);
        */
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(siteSetting.GetUrlWebReference);

        string result = supplier.GetCitiesByCoordinate(userManagement.Get_Guid, decimal.Parse(radius), decimal.Parse(lng), decimal.Parse(lat));
        Response.Write(result);
        
    }
}
