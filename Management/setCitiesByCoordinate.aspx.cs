﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Management_setCitiesByCoordinate : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Charset = "utf-8";
        Response.ContentType = "text/xml";

        /*
        <Areas Latitude="1" Longitude="2" Radius="3">
          <AreaNotSelected RegionId="BD57BC65-E99E-DF11-9C13-0003FF727321" /> 
          <AreaNotSelected RegionId="BE57BC65-E99E-DF11-9C13-0003FF727321" /> 
          <Area RegionId="BF57BC65-E99E-DF11-9C13-0003FF727321" /> 
        </Areas>
        */

        //string RegistrationStage = Request["RegistrationStage"].ToString();
        string Longitude = Request["Longitude"].ToString();
        string Latitude = Request["Latitude"].ToString();
        string Radius = Request["Radius"].ToString();
        string strCitiesSelected = Request["strCitiesSelected"].ToString();
        string strCitiesNotSelected = Request["strCitiesNotSelected"].ToString();

        string xmlCoordinatesCities="";

        SiteSetting ss = (SiteSetting)Session["Site"];

        xmlCoordinatesCities += "<Areas Latitude=\"" + Latitude  + "\" Longitude=\""
            + Longitude + "\" Radius=\"" + Radius + "\">";

        string[] splitCitiesNotSelected;

        if (strCitiesNotSelected.Length > 0)
        {
            splitCitiesNotSelected = strCitiesNotSelected.Split(',');
            for (int i = 0; i < splitCitiesNotSelected.Length; i++)
            {
                xmlCoordinatesCities += "<AreaNotSelected RegionId=\"" + splitCitiesNotSelected[i] + "\" />";
            }
        }

        string[] splitCitiesSelected;

        if (strCitiesSelected.Length > 0)
        {
            splitCitiesSelected = strCitiesSelected.Split(',');
            for (int i = 0; i < splitCitiesSelected.Length; i++)
            {
                xmlCoordinatesCities += "<Area RegionId=\"" + splitCitiesSelected[i] + "\" />";
            }
        }

        xmlCoordinatesCities+= "</Areas>";
        /*
        string xmlCoordinates = "";
        xmlCoordinates += "<WorkArea SiteId=\"" + siteSetting.GetSiteID + "\" SupplierId=\"" + GetGuidSetting() + "\">";
        xmlCoordinates += "<Details>";
        xmlCoordinates += "<Radius>" + Radius + "</Radius>";
        xmlCoordinates += "<Latitude>" + Latitude + "</Latitude>";
        xmlCoordinates += "<Longitude>" + Longitude + "</Longitude>";
        xmlCoordinates += "</Details>";
        xmlCoordinates += "</WorkArea>";
        */

        /*
        string xml = "";
        xml="<Areas RegistrationStage=\"3\" Longitude=\""
            + Longitude+ "\" Latitude=\"" + Latitude + "\" Radius=\"" + Radius + "\">";

        
        string[] AreasChilds = Request["strAreasChilds"].ToString().Split(',');

        string RegionId = "";
        string Name = "";

        for (int i = 0; i < AreasChilds.Length; i++)
        {
            xml = xml + "<Area RegionId=\"" + AreasChilds[i] + "\" />";
        }
        

        xml = xml + "</Areas>";
        */


        //Response.Write(xml);

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(ss.GetUrlWebReference);
        string result = "";// supplier.SetSupplierWorkAreas(Session["myGuid"].ToString(), xmlCoordinatesCities);
        //string result = supplier.SetCitiesByCoordinate(xmlCoordinates);

        Response.Write(result);
        //Response.Write(Session["myGuid"].ToString() + " " + ss.GetUrlWebReference);

        Session["streetOrCityChange"]="false";
        
    }
}
