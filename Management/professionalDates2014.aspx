﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professionalDates2014.aspx.cs" Inherits="Management_professionalDates2014" ClientIDMode="Static" EnableEventValidation="false"%>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">





<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  
		<title>Bootstrap Multiselect</title>
		<meta content="noindex, nofollow" name="robots">
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
		<meta content="David Stutz" name="copyright">

        <!-- Include Twitter Bootstrap and jQuery: -->
		<link type="text/css" href="../MultipleSelectBootstrap/css/bootstrap-3.1.1.min.css" rel="stylesheet"/>
      
		<link type="text/css" href="../MultipleSelectBootstrap/css/bootstrap-multiselect.css" rel="stylesheet"/>
		<link type="text/css" href="../MultipleSelectBootstrap/css/prettify.css" rel="stylesheet"/>

        <!-- Include the plugin's CSS and JS: -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet"/>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
		<script src="../MultipleSelectBootstrap/js/bootstrap-3.1.1.min.js" type="text/javascript"></script>
		<script src="../MultipleSelectBootstrap/js/bootstrap-multiselect.js" type="text/javascript"></script>
		<script src="../MultipleSelectBootstrap/js/prettify.js" type="text/javascript"></script>
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>

        <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
        <script type="text/javascript">            try { Typekit.load(); } catch (e) { }</script>
        <script src="../PPC/ppc.js" type="text/javascript"></script>

        <link href="../PPC/samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />

        <link href="../Ladda-master/dist/ladda.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../Ladda-master/dist/spin.min.js"></script>
        <script type="text/javascript" src="../Ladda-master/dist/ladda.min.js"></script>

        <style>
            
            
        </style>

          <!-- Initialize the plugin: -->
        <script type="text/javascript">

            function UpdateSuccess() {
                SetServerComment3("<%#UpdateSuccess%>");
            }

            function UpdateFailed() {
                SetServerComment3("<%#UpdateFaild%>");

            }          


            var containerWorkingHoursObject;
            var Units = new Array();


            $(document).ready(function () {
                $('.multiselect').multiselect({

                    buttnText: function (options, select) {                        
                        if (options.length == 0) {
                            return 'Select dates <b class="caret"></b>';
                        }
                        else {
                            if (options.length > this.numberDisplayed) {
                                return options.length + ' ' + this.nSelectedText + ' <b class="caret"></b>';
                            }
                            else {
                                var selected = '';
                                options.each(function () {
                                    var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();

                                    selected += label + ', ';
                                });
                                return selected.substr(0, selected.length - 2) + ' <b class="caret"></b>';
                            }
                        }
                    },

                    buttonWidth: '204px',
                    numberDisplayed: 7,
                    includeSelectAllOption: true,                  



                });


                $("#lnk_Vacation").click(function () {
                    $(".containerVacation").show();
                    $(this).hide();
                }
                );

                $("#minusDatePicker").click(function () {
                    $(".containerVacation").hide();
                    $('#lnk_Vacation').show();
                    return false;
                }
                );

                /*************** date picker *************/

                var initFromDate = '<%#vacationFrom%>';
                var currentDate;
                var futureDate;
                if (initFromDate == "") {

                    currentDate = new Date();
                    futureDate = new Date();
                    futureDate.setDate(currentDate.getDate() + 1);

                    var pickerOpts = {
                        showOn: "both", // will open clcik the input and click the icon
                        buttonImageOnly: true,
                        buttonImage: "images/calendar.png",
                        minDate: currentDate,
                        //defaultDate: new Date(),
                        buttonText: "",

                        onSelect: function (dateStr) { $("#formDate").css({ 'color': 'black', 'borderColor': '' }); }
                    };

                }
                else {                   
                    currentDate = new Date('<%#vacationFrom%>');
                    futureDate = new Date('<%#vacationTo%>');

                    var pickerOpts = {
                        showOn: "both", // will open clcik the input and click the icon
                        buttonImageOnly: true,
                        buttonImage: "images/calendar.png",
                        //defaultDate: new Date(),
                        buttonText: "",

                        onSelect: function (dateStr) { $("#formDate").css({ 'color': 'black', 'borderColor': '' }); }
                    };
                }



                $("#txt_fromDate").datepicker(pickerOpts); // from http://jqueryui.com/datepicker/
                $("#txt_fromDate").datepicker("setDate", currentDate);



                $("#txt_toDate").datepicker(pickerOpts); // to  http://jqueryui.com/datepicker/
                $("#txt_toDate").datepicker("setDate", futureDate);


                /*
                var currentDate = new Date();
                //currentDate.setDate(currentDate.getDate() + 4);

                var pickerOpts = {
                showOn: "both", // will open clcik the input and click the icon
                buttonImageOnly: true,
                buttonImage: "images/calendar.png",
                minDate: currentDate,
                //defaultDate: new Date(),
                buttonText: "",

                onSelect: function (dateStr) { $("#formDate").css({ 'color': 'black', 'borderColor': '' }); }
                };


                $("#txt_fromDate").datepicker(pickerOpts); // from http://jqueryui.com/datepicker/
                //$("#txt_fromDate").datepicker("setDate", new Date());
                //$("#txt_fromDate").datepicker("setDate", currentDate.setDate(currentDate.getDate() + 1));
                //$("#txt_fromDate").datepicker("setDate", '12/15/2013');
                */

            });

             function getDetails() {
               
                $('.multiselect').multiselect({
                    buttonText: function (options, select) {                        
                        if (options.length == 0) {
                            return  'Select dates <b class="caret"></b>';
                        }
                        else {
                            if (options.length > this.numberDisplayed) {
                                return options.length + ' ' + this.nSelectedText + ' <b class="caret"></b>';
                            }
                            else {
                                var selected = '';
                                options.each(function () {
                                    var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();

                                    selected += label + ', ';
                                });
                                return selected.substr(0, selected.length - 2) + ' <b class="caret"></b>';
                            }
                        }
                    },
                    buttonWidth: '500px',
                    numberDisplayed: 7,
                    includeSelectAllOption: true
                });



            }

            var index2 = 0;


            function getMultiSelect() {
                //alert("getMultiSelect");
                getDetails();

                index2 = 0;

                $('.multiselect-container').each(function (index) {
                    ////alert("index: " + index);
                    $(this).prev().css('width', '160px'); // this is the top button

                    var WorkingHoursUnit = new Object();
                    var daysOfWeek = new Array();
                    var daysOfWeekLong = new Array();

                    //alert($(this).parent().prev('select').attr('id'));
                    //$($(this).parent().prev('select')).multiselect({ buttonWidth: '160px', numberDisplayed: 4 });
                    //$($(this).parent().prev('select')).multiselect('rebuild');

                    //alert(containerWorkingHoursObject.Units[index2]);
                    if (containerWorkingHoursObject.Units[index2] && containerWorkingHoursObject.Units[index2].DaysOfWeek.length > 0) // just for multiselect at least one checked
                    {

                        $($(this).parent().parent().parent().children('.ddlFromTime')).prop('disabled', containerWorkingHoursObject.Units[index2].timeDisabled);
                        $($(this).parent().parent().parent().children('.ddlToTime')).prop('disabled', containerWorkingHoursObject.Units[index2].timeDisabled);

                        $(this).find('li').each(function (index) {

                            for (var i = 0; i < containerWorkingHoursObject.Units[index2].DaysOfWeek.length; i++) {
                                //alert(containerWorkingHoursObject.Units[index2].DaysOfWeek[i]);
                                if ($(this).find('input[type="checkbox"]').prop("value") == containerWorkingHoursObject.Units[index2].DaysOfWeek[i]) {
                                    //alert($(this).find('input[type="checkbox"]').prop("value"));
                                    //$(this).find('input[type="checkbox"]').prop("checked", true);
                                    //$(this).addClass('active');                                    


                                    $($(this).parent().parent().prev('select')).multiselect('select', $(this).find('input[type="checkbox"]').prop("value"));

                                }

                            }


                        }
                        )



                    }



                    index2++;
                }
                )

            }

            function setEventClick() {
                alert("setEventClick");

            }


            function convertDayIntToDayName(dayInt) {
                var dayName;
                switch (dayInt) {
                    case 0:
                        dayName = 'Sun';
                        break;
                    case 1:
                        dayName = 'Mon';
                        break;
                    case 2:
                        dayName = 'Tue';
                        break;
                    case 3:
                        dayName = 'Wed';
                        break;
                    case 4:
                        dayName = 'Thu';
                        break;
                    case 5:
                        dayName = 'Fri';
                        break;
                    case 6:
                        dayName = 'Sat';
                        break;
                    default:
                        break;
                }

                return dayName;
            }

            function setRuntimeDates(order, dayInt) {

                $('#selectDate' + order).multiselect('select', convertDayIntToDayName(dayInt));
                //$('#selectDate1').multiselect('select', 'Mon');
            }


            function setAvailability(listObj) {

                $.ajax({
                    type: 'POST',
                    url: '<%#UrlAvailability%>',
                    data: "{workingHoursRequest:" + JSON.stringify(listObj) + "}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    dataFilter: function (data) { // This boils the response string down 
                        //  into a proper JavaScript Object().
                        var msg = eval('(' + data + ')');

                        /*
                        If you aren’t familiar with the “.d” I’m referring to, it is simply a security feature that Microsoft added in ASP.NET 3.5’s version
                        of ASP.NET AJAX. By encapsulating the JSON response within a parent object,
                        the framework helps protect against a particularly nasty XSS vulnerability.

                        */

                        // If the response has a ".d" top-level property,
                        //  return what's below that instead.
                        if (msg.hasOwnProperty('d'))
                            return msg.d;
                        else
                            return msg;
                    },
                    success: function (data) {


                        if (data.status == "failed") {
                            
                            SendFaild();
                            //SetServerComment(comment);
                            

                           
                        }
                        else {                         
                                                     
                            SendSucceed();
                            
                        }

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //alert('faild ' + errorThrown);
                        /*
                        SetServerComment(comment);
                        */
                        if (window.console)
                            console.log("failed " + textStatus + " " + errorThrown);

                    }
                });

            }



            function initDefaultDays() {
                //alert(objUnits);
                var objUnitsEval = eval("(" + objUnits + ")");
                for (var i = 0; i < objUnitsEval.Units.length; i++) { // there at least one unit pre populated by the system
                    //alert("Order:" + objUnitsEval.Units[i].Order);
                    for (var y = 0; y < objUnitsEval.Units[i].DaysOfWeek.length; y++) {
                        setRuntimeDates(objUnitsEval.Units[i].Order, objUnitsEval.Units[i].DaysOfWeek[y]);
                        //alert(objUnitsEval.Units[i].DaysOfWeek[y]);
                    }

                }

            }

            function initDefaultDateVacation() {
                $("#txt_fromDate").datepicker(pickerOpts); // from http://jqueryui.com/datepicker/
                $("#txt_fromDate").datepicker("setDate", currentDate);

                $("#txt_toDate").datepicker(pickerOpts); // to  http://jqueryui.com/datepicker/
                $("#txt_toDate").datepicker("setDate", currentDate.setDate(currentDate.getDate() + 1));
            }

            function init() {
                //setEventClick();
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndHandler);

                initDefaultDays();
                parent.window.setLinkStepActive("linkStep4");
                parent.hideDiv();
                //initDefaultDateVacation();

            }
            function startRequest(sender, e) {
                //alert("baaa start");
            }
            function EndHandler(sender, e) {
                getMultiSelect();
                //setEventClick();
                //alert("before");

                //$('.multiselect').multiselect('destroy');
                //getDetails();
                //$('.multiselect').multiselect('refresh');

            }

            window.onload = init;

           


            function shortDayToLongDay(shortDay) {
                var longDate;

                switch(shortDay.toLowerCase())
                {
                    case "sun":
                        longDate="Sunday";
                        break;

                    case "mon":
                        longDate="Monday";
                        break;

                    case "tue":
                        longDate="Tuesday";
                        break;

                    case "wed":
                        longDate="Wednesday";
                        break;

                    case "thu":
                        longDate="Thursday";
                        break;

                    case "fri":
                        longDate="Friday";
                        break;

                    case "sat":
                        longDate="Saturday";
                        break;

                    default:
                        break;
                }

                return longDate;
                
            }




            function setSelectBox(minusObj) {

                var status = "ok";

                containerWorkingHoursObject = new Object();
                Units.length=0;

                //alert("Units.length: " + Units.length);

                outerIndex = 0;
                innerIndex = 0;

                

                containerWorkingHoursObject.SupplierId = "<%#SupplierId%>";
                //alert(containerWorkingHoursObject.SupplierId);

                if($(".containerVacation").is(":visible"))
                {
                    containerWorkingHoursObject.VacationFrom = $("#txt_fromDate").val();
                    //alert(containerWorkingHoursObject.VacationFrom);
                    containerWorkingHoursObject.VacationTo = $("#txt_toDate").val();

                    if (containerWorkingHoursObject.VacationFrom > containerWorkingHoursObject.VacationTo) {
                        status = "Vacation toDate not later than Vacation fromDate";
                    }    
                }

                else
                {
                    containerWorkingHoursObject.VacationFrom = null;
                    //alert(containerWorkingHoursObject.VacationFrom);
                    containerWorkingHoursObject.VacationTo = null;
                }
                //alert(containerWorkingHoursObject.VacationTo);


                $('.multiselect-container').each(function (index) {
                    //alert(index + " id:" + $(this).attr('id'));
                    var WorkingHoursUnit = new Object();
                    var daysOfWeek = new Array();



                    $(this).find('li').each(function (index) {
                        //alert($(this).attr('class'));

                        //alert($($(this).parent().parent().prev('select')).attr('id'));
                        if ($(this).attr('class') && $(this).attr('class') == "active") {
                            //alert(daysOfWeek[innerIndex]);
                            if ($(this).find('input[type="checkbox"]').attr('value') != "multiselect-all")
                                daysOfWeek[innerIndex++] = $(this).find('input[type="checkbox"]').attr('value');
                            //alert($(this).find('input[type="checkbox"]').attr('value'));
                            //alert($(this).find('input[type="checkbox"]').attr('value'));                           

                        }
                    }
                    )

                    if(daysOfWeek.length==0)
                        status="not selected";

                    //alert(daysOfWeek.length);
                    //if(daysOfWeek.length>0)  
                    //{ 
                    //alert($($(this).parent().prev('select')).attr('id'));
                    if (minusObj != $($(this).parent().prev('select')).attr('id')) {
                        WorkingHoursUnit.DaysOfWeek = daysOfWeek;
                        WorkingHoursUnit.Order = (outerIndex + 1);
                        //alert(WorkingHoursUnit.Order);
                        WorkingHoursUnit.timeDisabled = $("#selectFromTime" + $($(this).parent().prev('select')).attr('id').replace('selectDate', '')).prop('disabled');

                        /*
                        public int FromHour { get; set; }
                        public int FromMinute { get; set; }
                        public int ToHour { get; set; }
                        public int ToMinute { get; set; }
                        public bool AllDay { get; set; }
                        */

                        var fromTime = $("#selectFromTime" + $($(this).parent().prev('select')).attr('id').replace('selectDate', '')).val();

                        var fromHour = fromTime.split(':')[0];
                        WorkingHoursUnit.FromHour = fromHour;


                        var fromMinute = fromTime.split(':')[1];
                        WorkingHoursUnit.FromMinute = fromMinute;

                        var toTime = $("#selectToTime" + $($(this).parent().prev('select')).attr('id').replace('selectDate', '')).val();


                        var toHour = toTime.split(':')[0];
                        WorkingHoursUnit.ToHour = toHour;

                        var toMinute = toTime.split(':')[1];
                        WorkingHoursUnit.ToMinute = toMinute;


                        if (checkToDateToLaterFromDate(fromHour, fromMinute, toHour, toMinute)) {

                        }

                        else {
                            status = "toDate not later than fromDate";
                        }


                        var chk24Hours = $("#chk24Hours" + $($(this).parent().prev('select')).attr('id').replace('selectDate', '')).prop('checked');
                        //alert(chk24Hours);
                        WorkingHoursUnit.AllDay = chk24Hours;

                        Units[outerIndex++] = WorkingHoursUnit;                        


                        //alert(outerIndex);
                        //alert(WorkingHoursUnit.timeDisabled);
                    }

                    containerWorkingHoursObject.Units = Units;
                    // alert(containerWorkingHoursObject.Units[0].DaysOfWeek[0]);

                    //}



                    innerIndex = 0;
                }
                )

                return status;
                //alert("after setSelectBox: " + containerWorkingHoursObject.Units.length);
            }

            function setTimeEnabled(objNumber) {
                //alert(objNumber);

                //alert($("#chk24Hours" + objNumber).prop('checked'));

                if ($("#chk24Hours" + objNumber).prop('checked')) {
                    $("#selectToTime" + objNumber).prop('disabled', true);
                    $("#selectFromTime" + objNumber).prop('disabled', true);
                }

                else {
                    $("#selectToTime" + objNumber).prop('disabled', false);
                    $("#selectFromTime" + objNumber).prop('disabled', false);
                }
            }

            function validation() {
                setSelectBox();

                
                //alert($.isEmptyObject(containerWorkingHoursObject));
                if ($.isEmptyObject(containerWorkingHoursObject || containerWorkingHoursObject.Units.length == 0))
                    return false;
                else {
                    return true;
                }


                //alert(containerWorkingHoursObject.Units[0].DaysOfWeek[0]);
            }

            function SendSucceed() {
                stopActiveButtonAnim();
                UpdateSuccess();
            }

            function SendFaild() {
                stopActiveButtonAnim();
                UpdateFailed();
            }


            function setWorkingHour(updateBtn) {
                $("#div_VacationComment").hide();
                var status = setSelectBox();
                
                if (status == "ok")
                {                    
                    
                }

                else if (status == "toDate not later than fromDate") {
                    SetServerComment3("<%#errorTime%>");                    
                    return false;
                }

                else if (status == "Vacation toDate not later than Vacation fromDate")
                {
                    SetServerComment3("<%#errorVacationTime%>");  
                    $("#div_VacationComment").show();                  
                    return false;                    
                }

                else if (status == "not selected")
                {
                    SetServerComment3("<%#errorNotSelected%>");                    
                    return false;                    
                }
                
                var containerWorkingHoursObjectLongDays = JSON.parse(JSON.stringify(containerWorkingHoursObject)); // copy object by value

                for (i = 0; i < containerWorkingHoursObjectLongDays.Units.length; i++) {
                    for (y = 0; y < containerWorkingHoursObjectLongDays.Units[i].DaysOfWeek.length; y++) {
                        //alert(containerWorkingHoursObjectLongDays.Units[i].DaysOfWeek[y]);
                        containerWorkingHoursObjectLongDays.Units[i].DaysOfWeek[y] = shortDayToLongDay(containerWorkingHoursObjectLongDays.Units[i].DaysOfWeek[y]);
                        //alert(containerWorkingHoursObjectLongDays.Units[i].DaysOfWeek[y]);
                    }
                }


                //alert("containerWorkingHoursObject.Units.length: " + containerWorkingHoursObject.Units.length);
                //alert("containerWorkingHoursObjectLongDays.Units.length: " + containerWorkingHoursObjectLongDays.Units.length);
                setAvailability(containerWorkingHoursObjectLongDays);

                activeButtonAnim(updateBtn, 200);
                /*
                public int FromHour { get; set; }
                public int FromMinute { get; set; }
                public int ToHour { get; set; }
                public int ToMinute { get; set; }
                public bool AllDay { get; set; }
                public List<DayOfWeek> DaysOfWeek { get; set; }
                public int Order { get; set; }
                */                
            }

            //$('#ddlCoverArea' + index).prop('disabled', false);
            
        </script>

	    <script type="text/javascript">
	        var _gaq = _gaq || [];
	        _gaq.push(['_setAccount', 'UA-21441721-1']);
	        _gaq.push(['_setDomainName', 'noproblemppc.com']);
	        _gaq.push(['_trackPageview']);

	        (function () {
	            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	        })();
    </script>

</head>
<body class="bodyDashboard bodyDates">
    <form id="form2" runat="server">
    <div class="datesDashboard dashboardIframe">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <uc1:PpcComment runat="server" ID="PpcComment1" /> 

        <div class="titleFirst">
            <span>Availability</span>

            <div id="containerNextTop" class="containerNextTop">
                <section class="progress-demo">       

                 <a id="linkSetDetailsTop" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" onclick="javascript:return setWorkingHour('#linkSetDetailsTop');">
                  <span class="ladda-label">Update</span><span class="ladda-spinner">
                  </span><div class="ladda-progress" ></div>
                 </a>

                </section>      
            </div>  

        </div>   

        <div class="titleSecond titleWorkingHours">
            <span>Working hours</span>
        </div>   

        <div class="datesDescription">
            Please tell us when you’d like to receive calls from customers by setting your             <br />
            working hours below.
        </div>
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlDates">
                    
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <div class="newCategory">
            <asp:LinkButton ID="lbAddButton" runat="server" OnClientClick="return validation();" onclick="lbAddButton_Click" >+ Add working days</asp:LinkButton>
        </div> 

        <div class="datesCategorySeperator"></div>

        <div class="titleSecond titleVacationSettings">
            <span>Vacation settings</span>
        </div>   

        <a id="lnk_Vacation" href="javascript:void(0);" runat="server">Go on vacation</a>
        
        <div class="containerVacation" id="containerVacation" runat="server">
            <div class="fromdateVacation">
                <div class="titleDateVacation ">From</div>
                <div class="dateVacation">
                    <asp:TextBox  ID="txt_fromDate" runat="server" CssClass="textActive"  ReadOnly="true"></asp:TextBox>
                </div>
            </div>

            <div class="containerSeperator" id="containerSeperator1"><div class="seperatorDate" id="seperator1"></div></div>

            <div class="todateVacation">
                <div class="titleDateVacation">To</div>
                <div class="inputParent">
                    <div class="dateVacation">
                        <asp:TextBox  ID="txt_toDate" runat="server" CssClass="textActive"  ReadOnly="true"></asp:TextBox>
                    </div>                                   

                    <div id="div_VacationComment" style="display:none;" class="inputComment"><%#errorVacationTime2%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                </div>

            </div>

            <div class="todateVacation">
                <div class="titleDateVacation">&nbsp;</div>
                <div class="dateVacationMinus">
                    <a id="minusDatePicker" href="">
                        <i class="fa fa-times fa-2"></i>
                    </a>
                </div>            
            </div>

        </div>
        
        <div class="clear"></div>

        <div class="datesCategorySeperator" style="margin-top:14px;"></div>

        <div id="containerNext" class="containerNext">
                <section class="progress-demo">       

                 <a id="linkSetDetails" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" onclick="javascript:return setWorkingHour('#linkSetDetails');">
                  <span class="ladda-label">Update</span><span class="ladda-spinner">
                  </span><div class="ladda-progress" ></div>
                 </a>

                </section>      
         </div>  

                 
    </div>
    </form>
</body>
</html>


