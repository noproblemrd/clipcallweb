using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Management_LogOut : PageSetting
{
    protected bool ifHasUrlLogout = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        string strUrlLogout = "";
        //Clear cookie login
        if (Request.Cookies["professionalLogin"] != null)
            Response.Cookies["professionalLogin"].Values.Clear();
        if(Request.Cookies["rememberme"] != null)
            Response.Cookies["rememberme"].Values.Clear();

        if (siteSetting != null && siteSetting.GetSiteID != null)
        {
            string command = "SELECT  dbo.GetLinkLogOutBySiteNameId (@SiteId)";
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteId", siteSetting.GetSiteID);
                strUrlLogout = (string)cmd.ExecuteScalar();
                conn.Close();
            }
        }
        


        
        PageSetting ps = (PageSetting)Page;
        ps.ClearUserSite();
        FormsAuthentication.SignOut();

        string _reason = Request.QueryString["reason"];

        if (_reason == eReasonLogout.SessionEnd.ToString())
        {
            Session["ReasonLogOut"] = _reason;
        }
        string UrlReferrer = Request.QueryString["referrer"];
        if (!string.IsNullOrEmpty(UrlReferrer))
            Response.Cookies.Add(new HttpCookie("referrer", UrlReferrer));

        if (String.IsNullOrEmpty(strUrlLogout))
            Page.ClientScript.RegisterStartupScript(this.GetType(), "logout", "window.top.location='ProfessionLogin.aspx'", true);
        else
            Page.ClientScript.RegisterStartupScript(this.GetType(), "logout", "window.top.location='" + strUrlLogout  + "'", true);
        //       Session.Abandon(); v";
        //     Page.ClientScript.RegisterStartupScript(this.GetType(), "logout", "disableBackButton();", true);
        
    }
    
}
