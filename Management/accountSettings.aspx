﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="accountSettings.aspx.cs" Inherits="Management_accountSettings"  ClientIDMode="Static"%>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script src="//code.jquery.com/jquery-1.7.1.min.js" type="text/javascript"></script>
    <link href="../PPC/samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>
    <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
    <script type="text/javascript">        try { Typekit.load(); } catch (e) { }</script>
    <script src="../PPC/ppc.js" type="text/javascript"></script>
    <script type="text/javascript">

        function UpdateSuccess() {
            SetServerComment2("<%#UpdateSuccess%>");
        }

        function UpdateFailed() {
            SetServerComment2("<%#UpdateFaild%>");

        }

        function init() {           
            parent.window.setLinkStepActive("linkStep2");
            parent.hideDiv();
        }


        window.onload = init;
    </script>
</head>
<body class="bodyDashboard">
    <form id="form1" runat="server">
    <div class="accountSettings dashboardIframe">
        <uc1:PpcComment runat="server" ID="PpcComment1" />

        <div class="titleFirst">
            <span>Account settings</span>
        </div>   
   
        <div class="accountSettingsContent">
            <div class="accountSettingsSubTitle">
                Login details
            </div>
            <div class="accountSettingsDetails">
                <div class="accountSettingsDetailsRow">
                    <div class="accountSettingsDetailsCell accountSettingsDetailsCellLeft">
                        Email (User ID): 
                    </div>

                    <div class="accountSettingsDetailsCell accountSettingsDetailsCellMiddle">
                        <asp:TextBox class="textActive" runat="server" ID="txt_email"></asp:TextBox>
                        
                       
                    </div>

                    <div class="accountSettingsDetailsCell accountSettingsDetailsCellRight">
                        <asp:LinkButton runat="server" ID="lb_change_email" 
                            onclick="lb_change_email_Click">Change</asp:LinkButton>
                    </div>
                </div>
               

            </div>
                        <div class="clear"></div>
            <div class="accountSettingsDetails">
                <div class="accountSettingsDetailsRow">
                    <div class="accountSettingsDetailsCell accountSettingsDetailsCellLeft">
                       Password: 
                    </div>

                    <div class="accountSettingsDetailsCell inputParent accountSettingsDetailsCellMiddle ">
                        <asp:TextBox class="textActive" runat="server" ID="txt_password" TextMode="Password"></asp:TextBox>

                        <div id="div_PasswordSmile" class="inputValid" style="display:none;"> </div>
                        <div id="div_PasswordError" class="inputError" style=""> </div>

                        <div class="inputValid" style="display:none;"  id="div2"></div>

                        <div id="div_PasswordComment" style="display:none;" class="inputComment"><%#PasswordError%>                    
                            <div class="chat-bubble-arrow-border"></div>
                            <div class="chat-bubble-arrow"></div>
                        </div>

                    </div>

                    <div class="accountSettingsDetailsCell accountSettingsDetailsCellRight">
                        <asp:LinkButton runat="server" ID="lb_change_password" 
                            onclick="lb_change_password_Click">Change</asp:LinkButton>
                    </div>
                </div> 

            </div>

            <div class="clear"></div>

        </div>
    </div>
    </form>
</body>
</html>
