using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Management_setCities : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string lat=Request["lat"]; 
        string lng=Request["lng"]; 
        string radius=Request["radius"];
        string cities=Request["cities"];
        string country=Request["country"];

        string[] arrCities=cities.Split(',');
        string strCities="";
        for (int i = 0; i < arrCities.Length; i++)
        {
            strCities += "<City>" + arrCities[i] + "</City>";
        }

        string xmlCities = "";
        xmlCities += "<WorkArea SiteId=\"0\" SupplierId=\"" + GetGuidSetting() + "\">";
        xmlCities += "<Details>";
        xmlCities += "<Radius>" + radius  + "</Radius>";
        xmlCities += "<Latitude>" + lat  + "</Latitude>";
        xmlCities += "<Longitude>" + lng + "</Longitude>";
        xmlCities += "</Details>";
        xmlCities += "<Countries>";
        //xmlCities += "<Country Name=\"Israel\">";
        xmlCities += "<Country Name=\"" + country + "\">";
        
        //xmlCities += "<City>Haifa</City>";
        //xmlCities += "<City>Tel Aviv</City>";
        xmlCities += strCities;
        xmlCities += "</Country>";
        xmlCities += "</Countries>";
        xmlCities += "</WorkArea>";
        Response.Write(xmlCities);

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);

        string result = "";// supplier.SetSupplierWorkAreas(GetGuidSetting(), xmlCities);
        //Response.Write(result);

    }
}
