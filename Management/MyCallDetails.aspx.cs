﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;

public partial class Management_MyCallDetails : PageSetting//, IPostBackEventHandler
{
    const string NUMBER_FORMAT = @"{0:0.##}";
    const string RUN_COMPLAINTS_REPORT = "RanComplaintsReport";
    const string CONFIRM_REFUN = "CONFIRM_REFUN";

    int DaysRefundOver;
//    int PercentRefundOver;
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(btn_Submit);
        ScriptManager1.RegisterAsyncPostBackControl(btn_RefundSubmit);
        ScriptManager1.RegisterAsyncPostBackControl(btn_complaints_virtual);
        //     ScriptManager1.RegisterAsyncPostBackControl(lb_Refund);

        if (!IsPostBack)
        {
            string CallId = Request.QueryString["CallId"];
            if (!(userManagement.IsPublisher() || userManagement.IsSupplier()))
            {
                Response.Redirect("LogOut.aspx");
                return;
            }
            if (!Utilities.IsGUID(CallId))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ErrorTicket", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_ErrorTicket.Text) + "');", true);
                return;
            }
            Guid _CallId = new Guid(CallId);
            
            LoadPageLinks();
            LoadWorkDoneOptions();
            LoadRefundReasons();
            LoadRefundStatus();
            LoadHelpDeskTypes();

            LoadTicketDetails(_CallId);
            LoadComplaints(_CallId);
            this.DataBind();
            CallIdV = _CallId;
            SetToolBox_complaints();
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "HideParentDiv", "parent.hideDiv();", true);
            SetTab();
        }
        else
        {
            if (Page.Request["__EVENTARGUMENT"] == CONFIRM_REFUN)
                SetConfirmRefund();
        }
        SetToolBoxEvents_complaints();
        Header.DataBind();

    }

    private void SetTab()
    {
        string _tab = Request.QueryString["Tab"];
        if (string.IsNullOrEmpty(_tab) || _tab == "1")
            return;
        if (_tab == "2")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "RefundTab2", "RefundTab(document.getElementById('" + a_Refund.ClientID + "'));", true);
            return;
        }
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    private void LoadRefundStatus()
    {
        ddl_RefundStatus.Items.Clear();
        //     ddl_RefundStatus.Items.Add(new ListItem("", ""));
        foreach (string name in Enum.GetNames(typeof(WebReferenceSupplier.RefundSatus)))
        {
            ListItem li = new ListItem(
                EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "RefundSatus", name), name);
            //Remove OnHold status
            if (li.Value != WebReferenceSupplier.RefundSatus.OnHold.ToString())
                ddl_RefundStatus.Items.Add(li);
        }
        ddl_RefundStatus.SelectedIndex = 0;
    }
    private void LoadHelpDeskTypes()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = new WebReferenceSite.ResultOfListOfGuidStringPair();
        try
        {
            result = _site.GetAllHelpDeskTypes();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            if (gsp.Name == eHelpDeskType.Complaint.ToString())
            {
                HelpDeskTypeV = gsp.Id;
                break;
            }
        }
    }
    private void LoadRefundReasons()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllRefundReasonsResponse result = null;
        try
        {
            result = _site.GetAllRefundReasons();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_RefundReasons.Items.Clear();
        ddl_RefundReasons.Items.Add(new ListItem("---" + lbl_ChooseItem.Text + "---", "-1"));
        foreach (WebReferenceSite.RefundReasonData rrd in result.Value.RefundReasons)
        {
            ListItem li = new ListItem(rrd.Name, "" + rrd.Code);
            li.Enabled = (rrd.Inactive == false);
            ddl_RefundReasons.Items.Add(li);
        }
        ddl_RefundReasons.SelectedIndex = 0;
    }
    private void LoadWorkDoneOptions()
    {
        ddl_WorkDone.Items.Clear();
        ddl_WorkDone.Items.Add(new ListItem("", ""));
        foreach (string _s in Enum.GetNames(typeof(eYesNo)))
        {
            ddl_WorkDone.Items.Add(new ListItem(
                EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", _s), _s));
        }
        ddl_WorkDone.SelectedIndex = 0;
    }

    private void LoadTicketDetails(Guid CallId)
    {
        hf_UserType.Value = userManagement.IsSupplier() ? "supplier" : "publisher";
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfSupplierIncidentData result = null;
        try
        {
            result = supplier.GetSupplierIncidentData(CallId);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
     //   result.Value.refundProcessState == WebReferenceSupplier.RefundProcessState.
        WebReferenceSupplier.SupplierIncidentData sid = result.Value;
        DaysRefundOver = sid.RefundDaysBackLimit;
        TicketNumV = sid.TicketNumber;
        IncidentIdV = sid.IncidentId;
        txt_CustomerPhone.Text = sid.CustomerPhoneNumber;
        txt_Price.Text = string.Format(NUMBER_FORMAT, sid.Price);
        txt_DescriptionCall.Text = sid.Description;
        txt_WinningPrice.Text = string.Format(NUMBER_FORMAT, sid.WinningPrice);
        txt_RefundDescription.Text = sid.RefundNoteForAdvertiser;
        txt_RefundReplay.Text = sid.RefundNote;
        
        //   IncidentIdV = new Guid(node.Attributes["IncidentId"].InnerText);
        //  string IsJobDone = sid.IsJobDone;
        if (!sid.Won)
            li_Refund.Visible = false;
        else
        {           
            div_WinningPrice.Visible = false;
            if (sid.RefundStatus == null && !sid.ToCharge)
            {
                li_Refund.Visible = false;
            }
            else
            {
                if (sid.RefundStatus != null)
                {
                    foreach (ListItem li in ddl_RefundStatus.Items)
                    {
                        if (li.Value == sid.RefundStatus.Value.ToString())
                        {
                            li.Selected = true;
                            txt_RefundStatus.Text = li.Text;
                        }
                        else
                            li.Selected = false;
                    }

                }

                if (sid.RefundReason != null)
                {
                    foreach (ListItem li in ddl_RefundReasons.Items)
                    {
                        if (int.Parse(li.Value) == sid.RefundReason)
                        {
                            li.Selected = true;
                            txt_RefundReasons.Text = li.Text;
                        }
                        else
                            li.Selected = false;
                    }
                }
            }
             

        }
        if (userManagement.IsPublisher() && sid.Won)
        {
            lbl_RefundThisCall.Text = lblRefundThisCall.Text;
            div_WorkDone.Visible = true;
            div_RefundCondition.Visible = false;
            switch (sid.IsJobDone)
            {
                case (null): SetWorkDoneOption(string.Empty);
                    break;
                case (false): SetWorkDoneOption(eYesNo.No.ToString());
                    break;
                case (true): SetWorkDoneOption(eYesNo.Yes.ToString());
                    break;
            }
            if (sid.RefundStatus == null)
            {
                div_RefundThisCall.Visible = true;
                div_RefundDescription.Visible = false;
                div_RefundStatus.Visible = false;
                txt_RefundReasons.Visible = false;
                ddl_RefundStatus.SelectedIndex =
                    ddl_RefundStatus.Items.IndexOf(
                    ddl_RefundStatus.Items.FindByValue(WebReferenceSupplier.RefundSatus.Approved.ToString()));
                //             txt_RefundStatus.Visible = false;
            }
            else
            {
                div_RefundThis.Style.Add(HtmlTextWriterStyle.Visibility, "visible");
                cb_RefundThisCall.Checked = true;
                cb_RefundThisCall.Enabled = false;
                txt_RefundDescription.ReadOnly = true;
                txt_RefundDescription.CssClass += " Label_ro";
                RequiredFieldValidator_RefundDescription.Visible = false;

                if (sid.RefundStatus == WebReferenceSupplier.RefundSatus.Pending)
                {
                    txt_RefundStatus.Visible = false;
                    //                   ddl_RefundStatus.Items.Remove(WebReferenceSupplier.RefundSatus.Pending.ToString());
                    ddl_RefundStatus.SelectedIndex =
                        ddl_RefundStatus.Items.IndexOf(
                        ddl_RefundStatus.Items.FindByValue(WebReferenceSupplier.RefundSatus.Pending.ToString()));
                    txt_RefundReasons.Visible = false;
                }
                else
                {
                    div_RefundSubmit.Visible = false;
                    ddl_RefundStatus.Visible = false;
                    ddl_RefundReasons.Visible = false;
                    cb_BlackList.Enabled = false;
                    txt_RefundReplay.ReadOnly = true;
                    txt_RefundReplay.CssClass += " Label_ro";
                    txt_RefundReasons.Visible = true;
                    txt_RefundStatus.Visible = true;

                }
            }
        }
        if (userManagement.IsSupplier() || !sid.Won)
        {
            div_WorkDone.Visible = false;
            div_submitWorDone.Visible = false;
        }
        if (userManagement.IsSupplier() && sid.Won)
        {
            ddl_RefundStatus.Visible = false;
            div_BlackList.Visible = false;
            txt_RefundReplay.ReadOnly = true;
            txt_RefundReplay.CssClass += " Label_ro";
            RequiredFieldValidator_RefundReplay.Visible = false;
            if (sid.RefundStatus != null)
            {
                div_RefundThisCall.Visible = false;
                div_RefundSubmit.Visible = false;
                txt_RefundDescription.ReadOnly = true;
                txt_RefundDescription.CssClass += " Label_ro";
                div_RefundThis.Style.Add(HtmlTextWriterStyle.Visibility, "visible");
                cb_RefundThisCall.Checked = true;
                cb_RefundThisCall.Enabled = false;
                ddl_RefundReasons.Visible = false;
                div_RefundCondition.Visible = false;
                
                txt_RefundReasons.Visible = true;
            }
            else
            {
                txt_RefundReasons.Visible = false;
                switch (sid.refundProcessState)
                {
                    case (WebReferenceSupplier.RefundProcessState.NoPhoneConnection):
                        div_NoPhoneConnection.Visible = true;
                        goto default;
                    case (WebReferenceSupplier.RefundProcessState.NoRecording):
                        div_NoRecord.Visible = true;
                        if (!sid.RecordsCalls)
                            div_AllowToRecord.Visible = true;
                        goto default;
                    case (WebReferenceSupplier.RefundProcessState.OverDaysLimit):
                        div_RefundOver.Visible = true;
                        goto default;
                    case (WebReferenceSupplier.RefundProcessState.OverPercentageLimit):
                        div_RefundExceeded.Visible = true;
                        goto default;
                    case(WebReferenceSupplier.RefundProcessState.NotAvailable):
                        div_AccountShouldBeAvailable.Visible = true;
                        goto default;
                    case (WebReferenceSupplier.RefundProcessState.OK):
                        div_RefundThisCall.Visible = true;
                        break;
                    default:
                        div_RefundThis.Visible = false;
                        break;
                }
            }
            
            
        }
        if (userManagement.IsSupplier())
            li_Complaints.Visible = false;

        CustomerIdV = sid.CustomerId;

        /*
              string _script = "javascript:parent.OpenIframe('" + ResolveUrl("~") + "Publisher/Ticket.aspx?HelpDesk=" + HelpDeskTypeV.ToString() +
                      "&Consumer=" + sid.CustomerId.ToString() + "&Call=" + CallId.ToString() + "');";
              a_Complaints.Attributes["onclick"] = _script;
         * */
    }
    void SetWorkDoneOption(string _option)
    {
        foreach (ListItem li in ddl_WorkDone.Items)
            li.Selected = (li.Value == _option);

    }
    protected void btn_AllowToRecord_Click(object sender, EventArgs e)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.TurnOnRecordings(new Guid(GetGuidSetting()));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        /*
        Response.Redirect("MyCallDetails.aspx?CallId=" + CallIdV.ToString() + "&Tab=2");
         * */
        string _path = ResolveUrl("~") + "Management/MyCallDetails.aspx?CallId=" + CallIdV.ToString() + "&Tab=2";
        string _script = "alert('" + HttpUtility.JavaScriptStringEncode(lbl_RecoededFromNow.Text) + "'); window.location='" + _path + "';";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CampaignOnHold", _script, true);

    }
    #region DETAIL
    protected void btn_WorkWasDone_click(object sender, EventArgs e)
    {
        div_Details.Style.Add(HtmlTextWriterStyle.Display, "block");
        div_Refund.Style.Add(HtmlTextWriterStyle.Display, "none");
        div_complaints.Style.Add(HtmlTextWriterStyle.Display, "none");
        a_Complaints.Attributes.Remove("class");
        a_Refund.Attributes.Remove("class");
        a_details.Attributes["class"] = "selected";

        string JobDone = ddl_WorkDone.SelectedValue;
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.UpdateJobDoneRequest _request = new WebReferenceSupplier.UpdateJobDoneRequest();
        _request.CallId = CallIdV;
        if (JobDone == eYesNo.Yes.ToString())
            _request.IsJobDone = true;
        else if (JobDone == eYesNo.No.ToString())
            _request.IsJobDone = false;
        else
            _request.IsJobDone = null;
        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.UpdateJobDone(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
    }
    #endregion

    #region REFUND
    protected void btn_RefundSubmit_click(object sender, EventArgs e)
    {
        div_Details.Style.Add(HtmlTextWriterStyle.Display, "none");
        div_Refund.Style.Add(HtmlTextWriterStyle.Display, "block");
        div_complaints.Style.Add(HtmlTextWriterStyle.Display, "none");
        a_Complaints.Attributes.Remove("class");
        a_details.Attributes.Remove("class");

        a_Refund.Attributes["class"] = "selected";

        div_RefundThis.Style.Add(HtmlTextWriterStyle.Visibility, "visible");

        WebReferenceSite.UpsertRefundRequest urr = new WebReferenceSite.UpsertRefundRequest();
        urr.SupplierId = new Guid(GetGuidSetting());
        urr.RefundReasonCode = int.Parse(ddl_RefundReasons.SelectedValue);
        //    urr.RefundNote = txt_RefundRequestDesc.Text;
        if (userManagement.IsSupplier())
        {
            urr.RefundNoteForAdvertiser = txt_RefundDescription.Text;
            urr.RefundStatus = WebReferenceSite.RefundSatus.Pending;
        }
        else
        {
            urr.RefundNote = txt_RefundReplay.Text;
            object obj = class_enum.GetValue(typeof(WebReferenceSupplier.RefundSatus), ddl_RefundStatus.SelectedValue);
            urr.RefundStatus = (WebReferenceSite.RefundSatus)obj;
        }
        urr.IsBlockApproved = false;
        urr.IncidentId = IncidentIdV;
        WebReferenceSite.ResultOfUpsertRefundResponse result = ExecRefund(urr);
        if (result == null)
            return;
        UpsertRefundRequestV = urr;
        //    bool IsUpsert = true;
        
        
        /*
        if (userManagement.IsPublisher())
            SetRefundAudit();
         * */
        switch (result.Value.Status)
        {
                
            case(WebReferenceSite.UpsertRefundStatus.CallWasNotCharged):
            case(WebReferenceSite.UpsertRefundStatus.OK):
                break;
                 
            case(WebReferenceSite.UpsertRefundStatus.BlockApprovalNeeded):
            //default:
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ConfirmedRefund", "ConfirmedRefund();", true);
                return;
        }
        Response.Redirect("MyCallDetails.aspx?CallId=" + CallIdV.ToString() + "&Tab=2");
        /*
        LoadTicketDetails(CallIdV);
        _UpdatePanel_Complaints.Update();
         * */
    }
    WebReferenceSite.ResultOfUpsertRefundResponse ExecRefund(WebReferenceSite.UpsertRefundRequest _request)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfUpsertRefundResponse result = null;
        //    bool IsUpsert = true;
        try
        {
            result = _site.UpsertRefund(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
            //          return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
            ///       return;
        }
        return result;
    }
    /*
    void SetRefundAudit()
    {
        OneCallUtilities.OneCallUtilities ocu = WebServiceConfig.GetOneCallUtilitiesReference(this);
        OneCallUtilities.AuditRequest _request = new OneCallUtilities.AuditRequest();
        OneCallUtilities.AuditEntry _audit = new OneCallUtilities.AuditEntry();
        string pagename =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        string page_translate = EditTranslateDB.GetPageNameTranslate(pagename, siteSetting.siteLangId);
        _audit.AdvertiseId = new Guid(GetGuidSetting());
        _audit.AdvertiserName = GetUserNameSetting();
        _audit.AuditStatus = OneCallUtilities.Status.Insert;
        _audit.FieldId = lblRefundThisCall.ID;
        _audit.FieldName = lblRefundThisCall.Text;
        _audit.OldValue = string.Empty;
        _audit.UserId = new Guid(userManagement.GetGuid);
        _audit.UserName = userManagement.User_Name;
        _audit.NewValue = lbl_TicketNumber.Text + "-" + TicketNumV;
        _audit.PageName = page_translate;
        _audit.PageId = pagename;
        _request.AuditOn = true;
        _request.AuditEntries = new OneCallUtilities.AuditEntry[] { _audit };
        OneCallUtilities.Result result = null;
        try
        {
            result = ocu.CreateAudit(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
        }
    }
     * */
    #endregion
    #region COMPLAINTS
    protected void LoadComplaints(Guid _CallId)
    {
        //Load status
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfSeverityAndStatusContainer _result = new WebReferenceSite.ResultOfSeverityAndStatusContainer();
        try
        {
            _result = _site.GetAllHelpDeskStatusesAndSeverities(HelpDeskTypeV);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (_result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Dictionary<Guid, string> dic = new Dictionary<Guid, string>();

        foreach (WebReferenceSite.HdStatusData hsd in _result.Value.StatusList)
        {
            dic.Add(hsd.Id, hsd.Name);
        }
        //Load complaints
        //    WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.GetHdEntriesRequest _request = new WebReferenceSite.GetHdEntriesRequest();

        _request.HdTypeId = HelpDeskTypeV;
        _request.RegardingObjectId = _CallId;

        WebReferenceSite.ResultOfListOfHdEntryData result = new WebReferenceSite.ResultOfListOfHdEntryData();
        try
        {
            result = _site.GetHelpDeskEntries(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        DataTable data = new DataTable();
        data.Columns.Add("ID", typeof(string));
        data.Columns.Add("guid", typeof(Guid));
        data.Columns.Add("CreatedOn", typeof(string));
        data.Columns.Add("Title", typeof(string));
        data.Columns.Add("Status", typeof(string));
        data.Columns.Add("StatusId", typeof(Guid));
        data.Columns.Add("FollowUp", typeof(string));
        data.Columns.Add("Advertiser", typeof(string));
        data.Columns.Add("ScriptComplaint", typeof(string));
        foreach (WebReferenceSite.HdEntryData hed in result.Value)
        {
            DataRow row = data.NewRow();
            row["ID"] = hed.TicketNumber;
            row["guid"] = hed.Id;
            row["CreatedOn"] = string.Format(siteSetting.DateFormat, hed.CreatedOn);
            row["Title"] = hed.Title;
            row["Advertiser"] = hed.SecondaryRegardingObject;
            row["StatusId"] = hed.StatusId;

            row["Status"] = dic[hed.StatusId];
            if (hed.FollowUp != null)
                row["FollowUp"] = string.Format(siteSetting.DateFormat, hed.FollowUp);

            string _script = "parent.OpenIframe('" + ResolveUrl("~") + "Publisher/Ticket.aspx?Ticket=" + hed.TicketNumber /*hed.Id.ToString()*/ +
                "&HelpDesk=" + HelpDeskTypeV.ToString() + "'); return false;";
            row["ScriptComplaint"] = _script;
            data.Rows.Add(row);
        }
        dataV = data;

        SetGridView(data);
        //       _UpdatePanel_Complaints.Update();
    }
    void SetGridView(DataTable data)
    {
        if (data == null || data.Rows == null || data.Rows.Count == 0)
        {
            _GridView.DataSource = null;
            _GridView.DataBind();
            _UpdatePanel_Complaints.Update();
            return;
        }
        _GridView.DataSource = data;
        _GridView.DataBind();
        StringBuilder sb = new StringBuilder();
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";
        foreach (GridViewRow _row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)_row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }
        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        _UpdatePanel_Complaints.Update();

    }
    private void SetToolBox_complaints()
    {
        _Toolbox.RemoveEdit();
        _Toolbox.RemoveAttach();
        _Toolbox.RemoveSave();
        string _script = "javascript:parent.OpenIframe('" + ResolveUrl("~") + "Publisher/Ticket.aspx?HelpDesk=" + HelpDeskTypeV.ToString() +
                "&Consumer=" + CustomerIdV.ToString() + "&Call=" + CallIdV.ToString() + "');";
        string script = _script + " return false;";
        _Toolbox.SetClientScriptToAdd(script);
    }
    private void SetToolBoxEvents_complaints()
    {
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
        _Toolbox.DeleteExec += new EventHandler(_Toolbox_DeleteExec);
        ScriptManager1.RegisterAsyncPostBackControl(_Toolbox.GetDeleteControl());
        //  _Toolbox.AddExec += new EventHandler(_Toolbox_AddExec);

    }
    void _Toolbox_DeleteExec(object sender, EventArgs e)
    {
        SetComplainTab();
        //       bool IsSelect = false;
        //       int indx = -1;
        List<Guid> list = new List<Guid>();
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
                list.Add(new Guid(((Label)row.FindControl("lbl_guid")).Text));
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.Result result = null;
        // WebReferenceSite.HelpDeskEntriesIds
        try
        {
            result = _site.DeleteHelpDeskEntries(list.ToArray());
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        LoadComplaints(CallIdV);
    }



    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        SetComplainTab();
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordDisplay + "');", true);
            return;
        }
        //     PrintHelper.PrintWebControl(_GridView, dataV);
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        SetComplainTab();
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }

        ToExcel to_excel = new ToExcel(this, "WizardReport");
        if (!to_excel.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        SetComplainTab();
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView(dataV);
    }
    void SetComplainTab()
    {
        div_Details.Style.Add(HtmlTextWriterStyle.Display, "none");
        div_Refund.Style.Add(HtmlTextWriterStyle.Display, "none");
        div_complaints.Style.Add(HtmlTextWriterStyle.Display, "block");
        a_Refund.Attributes.Remove("class");
        a_details.Attributes.Remove("class");
        a_Complaints.Attributes["class"] = "selected";
    }
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
    #region IPostBackEventHandler Members
    /*
    public void RaisePostBackEvent(string eventArgument)
    {
        if (eventArgument == RUN_COMPLAINTS_REPORT)
            LoadComplaints(CallIdV);
        else if (eventArgument == CONFIRM_REFUN)
            GetConfirmRefund();
    }
     * */

    private void SetConfirmRefund()
    {
        WebReferenceSite.UpsertRefundRequest _request = UpsertRefundRequestV;
        _request.IsBlockApproved = true;
        WebReferenceSite.ResultOfUpsertRefundResponse result = ExecRefund(_request);
        if (result == null)
            return;
        /*

        string _path = ResolveUrl("~") + "Management/MyCallDetails.aspx?CallId=" + CallIdV.ToString() + "&Tab=2";
     //   ScriptManager.RegisterStartupScript(this, this.GetType(), "ReLoadMyCallDetails", _script, true);
        Response.Redirect(_path);
         * */
        string _path = ResolveUrl("~") + "Management/MyCallDetails.aspx?CallId=" + CallIdV.ToString() + "&Tab=2";
        string _script = "alert('" + HttpUtility.JavaScriptStringEncode(lbl_CampaignOnHold.Text) + "'); window.location='" + _path + "';";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CampaignOnHold", _script, true);
    }
    protected override void Render(HtmlTextWriter writer)
    {
        Page.ClientScript.RegisterForEventValidation(btn_complaints_virtual.UniqueID, RUN_COMPLAINTS_REPORT);
        Page.ClientScript.RegisterForEventValidation(this.UniqueID, CONFIRM_REFUN);
        base.Render(writer);

    }
    protected string confirm_refund()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(this, CONFIRM_REFUN);
        myPostBackOptions.PerformValidation = false;

        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
    }
    protected string Get_run_report()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(btn_complaints_virtual, RUN_COMPLAINTS_REPORT);
        myPostBackOptions.PerformValidation = false;

        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
    }

    #endregion
    #endregion

    string TicketNumV
    {
        get { return (ViewState["TicketNum"] == null) ? string.Empty : (string)ViewState["TicketNum"]; }
        set { ViewState["TicketNum"] = value; }
    }

    Guid IncidentIdV
    {
        get { return (ViewState["IncidentId"] == null) ? Guid.Empty : (Guid)ViewState["IncidentId"]; }
        set { ViewState["IncidentId"] = value; }
    }
    Guid CustomerIdV
    {
        get { return (ViewState["ConsumerId"] == null) ? Guid.Empty : (Guid)ViewState["ConsumerId"]; }
        set { ViewState["ConsumerId"] = value; }
    }
    Guid CallIdV
    {
        get { return (ViewState["CallId"] == null) ? Guid.Empty : (Guid)ViewState["CallId"]; }
        set { ViewState["CallId"] = value; }
    }
    Guid HelpDeskTypeV
    {
        get { return (ViewState["HelpDeskType"] == null) ? new Guid() : (Guid)ViewState["HelpDeskType"]; }
        set { ViewState["HelpDeskType"] = value; }
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? null : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    WebReferenceSite.UpsertRefundRequest UpsertRefundRequestV
    {
        get { return (ViewState["UpsertRefundRequest"] == null) ? null : (WebReferenceSite.UpsertRefundRequest)ViewState["UpsertRefundRequest"]; }
        set { ViewState["UpsertRefundRequest"] = value; }
    }
    protected string GetConfirmRefundText
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_ConfirmRefund.Text); }
    }
    protected string GetAllowToRecord
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_CheckAllowToRecprd.Text); }
    }
    protected string GetRefundOver()
    {
        return string.Format(lbl_RefundOver.Text, DaysRefundOver.ToString()); 
    }
}
