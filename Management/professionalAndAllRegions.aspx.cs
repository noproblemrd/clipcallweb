using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;

public partial class Management_professionalAndAllRegions : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {
                string csnameLogin = "setLogin";
                Type cstypeLogin = this.GetType();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager csLogin = this.Page.ClientScript;

                if (!csLogin.IsStartupScriptRegistered(cstypeLogin, csnameLogin))
                {
                    string csTextLogin = "redirect();";
                    csLogin.RegisterStartupScript(cstypeLogin, csnameLogin, csTextLogin, true);
                }
            }

            int level = Convert.ToInt16(Request["level"]);

            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            string xmlProfessions = supplier.HandleAdvertiserRegions(siteSetting.GetSiteID, level, GetGuidSetting(), "", "");
            //Response.Write(xmlProfessions);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlProfessions);

            StringBuilder sbRegions = new StringBuilder();
            StringBuilder sbProfessionalRegions = new StringBuilder();

            XmlNode root = xml.DocumentElement; // Regions

            string ParentName = "";

            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++) // element primary
                {
                    ParentName = root.ChildNodes[i].Attributes["ParentName"].InnerText;

                    if (ParentName != "")
                    {
                        sbRegions.Append(ParentName + " >> " + root.ChildNodes[i].Attributes["Name"].InnerText +
                             "=" + root.ChildNodes[i].Attributes["ID"].InnerText + "*" +
                             root.ChildNodes[i].Attributes["HasChildRegions"].InnerText
                            + "&");
                    }
                    else
                    {
                        sbRegions.Append(root.ChildNodes[i].Attributes["Name"].InnerText + "=" +
                            root.ChildNodes[i].Attributes["ID"].InnerText + "*" + root.ChildNodes[i].Attributes["HasChildRegions"].InnerText
                            + "&");
                    }

                    //Response.Write(sbRegions.ToString().Substring(0, sbRegions.ToString().Length - 1));

                    if (root.ChildNodes[i].Attributes["ConnectedToAdvertiser"].InnerText == "True")
                    {
                        if (ParentName != "")
                            sbProfessionalRegions.Append(ParentName + " >> " + root.ChildNodes[i].Attributes["Name"].InnerText + "&");
                        else
                            sbProfessionalRegions.Append(root.ChildNodes[i].Attributes["Name"].InnerText + "&");
                    }
                }

                string strRegions = "";
                strRegions = sbRegions.ToString().Substring(0, sbRegions.ToString().Length - 1);

                string strProfessionalRegions = "";

                if (sbProfessionalRegions.ToString().Length > 0)
                {
                    strProfessionalRegions = sbProfessionalRegions.ToString().Substring(0, sbProfessionalRegions.ToString().Length - 1); 
                    //Response.Write(sbProfessionalRegions.ToString().Substring(0, sbProfessionalRegions.ToString().Length - 1));
                }
                else
                {
                    //sbProfessionalRegions.Append("empty");
                    strProfessionalRegions = "empty";
                    //Response.Write(sbProfessionalRegions.ToString());
                }

                Response.Write(strRegions + "*****" + strProfessionalRegions);
            }

            else
                Response.Write("empty*****empty");


            /*
            if (level == "1")
                level = "TelAviv1";
            else if (level == "2")
                level = "petach tikva";
            else if (level == "3")
                level = "beit shemesh&lud";
            Response.Write(level);
            */

            /*
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            string xmlProfessions = supplier.HandleAdvertiserRegions(siteSetting.GetSiteID, level, GetGuidSetting(), startWith, "");
            //Response.Write(xmlProfessions);


            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlProfessions);

            StringBuilder sb = new StringBuilder();
            XmlNode root = xml.DocumentElement; // Regions

            string ParentName = "";

            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++) // element primary
                {
                    ParentName = root.ChildNodes[i].Attributes["ParentName"].InnerText;
                    if (ParentName != "")
                    {
                        sb.Append(ParentName + " >> " + root.ChildNodes[i].Attributes["Name"].InnerText +
                             "=" + root.ChildNodes[i].Attributes["ID"].InnerText + "*" +
                             root.ChildNodes[i].Attributes["HasChildRegions"].InnerText
                            + "&");
                    }
                    else
                    {
                        sb.Append(root.ChildNodes[i].Attributes["Name"].InnerText + "=" +
                            root.ChildNodes[i].Attributes["ID"].InnerText + "*" + root.ChildNodes[i].Attributes["HasChildRegions"].InnerText
                            + "&");
                    }
                }

                Response.Write(sb.ToString().Substring(0, sb.ToString().Length - 1));
            }

            else
                Response.Write("empty");
            
            

            */

        }
    }
}
