<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professionChooseProfessions.aspx.cs" Inherits="Professional_professionChooseProfessions" enableEventValidation="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
    
  
    
    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript" language="javascript">
    
    var arrProfessions=new Array();
    var arrProfessionsPlace=new Array();
    var arrProfNameGuid=new Array();
    var splitProf;
    var strAllProfessionalRegions="";
    var strProfessionalExpertisesNow="";
    var strProfessionalExpertisesNow2="";
    
    function witchAlreadyStep(level)
    {   
        parent.window.witchAlreadyStep(level);        
        parent.window.setLinkStepActive('linkStep2');   
        
    }

    function sendWebService()
    {    
        
        parent.showDiv();
        
        if(document.getElementById("<%#selectProfessions2.ClientID%>").options.length==0)
        {
          alert("<%#lblHiddenUpdate.Value%>");
          parent.hideDiv();
          return false;
        }
        
        var certificateName;
        var certificateNode;
        var strCertificates="";
        var pos;
        var tbody = document.getElementById("<%#tblCertificates.ClientID%>").tBodies[0];
        
        for(indexCertificates=0;indexCertificates<tbody.childNodes.length;indexCertificates++)
        {
            certificateNode=tbody.childNodes[indexCertificates];
            certificateName=tbody.childNodes[indexCertificates].id;
            
            
            if(certificateName!="" && certificateName!=undefined)
            {        
                
                pos=certificateName.indexOf('_');
                
                certificteName=certificateName.substring(pos+1,certificateName.length);               
                for(y=0;y<certificateNode.childNodes.length;y++)
                {                  
                    
                    if(certificateNode.childNodes[y].childNodes[0]!=undefined)
                    {
                        if(certificateNode.childNodes[y].childNodes[0].checked==true)
                            strCertificates+=certificteName + ",";
                    }                        
                }       
                
            }            
            
        }   
        
        strCertificates=strCertificates.substring(0,strCertificates.length-1);
        //alert("strCertificates:" + strCertificates);
        
        var str="";
        var strAdd="";
        var strRemove="";
        
        for(i=0;i<document.getElementById("<%#selectProfessions2.ClientID%>").options.length;i++)
        {
            //alert(document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text );
            

            for(y=0;y<arrProfNameGuid.length;y++)
            {

                
                //alert(arrProfNameGuid[y][0]);
               
                if(document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text==arrProfNameGuid[y][0])
                {    
                    //alert(arrProfNameGuid[y][0]);
                   str+=arrProfNameGuid[y][0].replace("=","shaveshave") + "=" + arrProfNameGuid[y][1] + ","; 
                }  
                
                
            }
            
            // check if add professions to the init
            if(strAllProfessionalRegions.indexOf("&" + document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text + "&")==-1) // the profession is not in professional regions
            {
               //alert("not find " +  document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text);              
               strAdd=strAdd + document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text + ",";
            }
            
           
        }     
        
        //alert(strAllProfessionalRegions);
        if(strProfessionalExpertisesNow.length>0)
        {
            strProfessionalExpertisesNow2=strProfessionalExpertisesNow.substring(1,strProfessionalExpertisesNow.length); // delete the first &
            strProfessionalExpertisesNow2=strProfessionalExpertisesNow2.substring(0,strProfessionalExpertisesNow2.length-1); // delete the last &
            //alert(strProfessionalExpertisesNow2);
        }
        //alert("strAllProfessionalRegions: " + strAllProfessionalRegions);
        
        splitAllProf=strProfessionalExpertisesNow2.split('&');
        
        var ifFindExprtise=false;
        /************ check if remove professions from the init  ****************/
        for(i=0;i<splitAllProf.length;i++)
        {
            ifFindExprtise=false;
            //alert(i + " " + splitAllProf[i]);
            for(n=0;n<document.getElementById("<%#selectProfessions2.ClientID%>").options.length;n++)
            {
                
                if(splitAllProf[i]==document.getElementById("<%#selectProfessions2.ClientID%>").options[n].text)
                {
                    ifFindExprtise=true;
                    break;
                }              
                
            }
            
            if(ifFindExprtise==false)
            {
                //alert("not find: " + splitAllProf[i]);
                strRemove=strRemove + splitAllProf[i] + ",";
            }
            
            //alert(strRemove);
        }
        /************ end check if remove professions from the init  ****************/
        
        str=str.substring(0,str.length-1);
        if(strAdd.length>0)
            strAdd=strAdd.substring(0,strAdd.length-1);
        //alert("add:" + strAdd); 
         
        if(strRemove.length>0)
            strRemove=strRemove.substring(0,strRemove.length-1);
        //alert("remove:" + strRemove);      
        //alert("str:" + str);
        var url;
        //alert(escape(document.formProfessions.PrimaryExpertise.options[document.formProfessions.PrimaryExpertise.selectedIndex].text));
        //url="<#ResolveUrl("~")%>Management/professionalSetExpertises.aspx?expertises=" + escape(str) + "&certificates=" + escape(strCertificates);
        url="<%#ResolveUrl("~")%>Management/professionalSetExpertises.aspx";

        //alert(url);        
      
        var params="";
        params="expertises=" + str + "&certificates=" +
         escape(strCertificates) + "&addExpertises=" + strAdd + "&removeExpertises=" + strRemove + 
         "&userName=<%#Server.UrlEncode(userName)%>&userId=<%#userId%>&advertiseId=<%#advertiseId%>&advertiserName=<%#Server.UrlEncode(advertiserName)%>&fieldId=<%#fieldId%>&fieldName=<%#Server.UrlEncode(fieldName)%>&pageId=<%#pageId%>&isFirstTimeV=<%#isFirstTimeV%>";
         
        //alert(params);
        
       
               
        var objHttp;
        var ua = navigator.userAgent.toLowerCase(); 
        
        if(ua.indexOf("msie")!= -1)
        {           
            objHttp =  new ActiveXObject("Msxml2.XMLHTTP");            
        }
        else
        {           
            objHttp=new XMLHttpRequest();
        }        
        
       
        objHttp.open("POST", url, true);
        
        objHttp.onreadystatechange=function()
        {	  
            //alert("objHttp.readyState "+objHttp.readyState);          
	        if (objHttp.readyState==4)
	        {       
		       //alert(objHttp.responseText);
		       var index=0;
		       if(objHttp.responseText!="empty")
		       {
		            
		            var xmlDoc=objHttp.responseXML.documentElement;	            
                    var status = xmlDoc.getElementsByTagName("Status");                  
                    status=xmlDoc.childNodes[0].childNodes[0].nodeValue; 

                   
                         
                        //alert(status);
                        if(status.toLowerCase()=="success")
                        {
                            top.UpdateSuccess();
                            if(document.getElementById("<%#btnSendWebService.ClientID%>").title=="Next")
                                parent.window.step3();
                            else
                            {
                               
                               location.href='professionChooseProfessions.aspx?comment=true'; // must reload here for audit target. we need to send what expertises removed from the iniitialize
                               
                            }
                        }
                        else
                        {
                            splitMessageFailed=status.split(';');
                            //alert(splitMessageFailed.length);
                            if(splitMessageFailed.length>1)
                            {
                                /*                   
                                  Failure;BadExpertise=Air condition;DefaultPrice=5000;RechargePrice=1500 
                                */
                                var expertiseFailed=splitMessageFailed[1].split('=')[1];
                                var defaultPrice=splitMessageFailed[2].split('=')[1];
                                var rechargePrice=splitMessageFailed[3].split('=')[1];
                                alert("Failure with the expertise " + expertiseFailed + " the recharge price (which is now " + rechargePrice + ") must be greater than the default price "
                                 + defaultPrice + "\nPlease go to tab 6 to change the recharge price");
                            }

                            else
                                top.UpdateFailed();
                        }
                                       
                                          
		                    
		            
		       }
		        else
                     top.UpdateFailed(); 
		       
		       parent.hideDiv(); 
	
		       
	        }
         }        
         
         objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
         objHttp.setRequestHeader("Content-Length", params.length);
         objHttp.setRequestHeader("Connection", "close");
         objHttp.send(params);
         
         //objHttp.send(null);
         
        
        
    }
    
    function setStep3()
    {  
        //alert("bbbbb");
        parent.window.step3();
    }    
    
    function updateHiddenField()
    {
        document.getElementById("<%#hidden_professions.ClientID%>").value="";
        //alert(document.getElementById("<%#hidden_professions.ClientID%>").value);
        for(i=0;i<document.getElementById("<%#selectProfessions2.ClientID%>").options.length;i++)
        {                
            document.getElementById("<%#hidden_professions.ClientID%>").value+=document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text ;
            if(i<document.getElementById("<%#selectProfessions2.ClientID%>").options.length-1)
                document.getElementById("<%#hidden_professions.ClientID%>").value+="&";
        }
        
        
    }
    
    var arrCertificates=new Array();
    var indexCertificates=-1;
    
    function addCertificates(cerfiticate,value)
    {
        //alert(cerfiticate + " " + value);
        /******* check if can show it ***********/
        var strShowingCertificates='<%#strShowingCertificates%>';
        //alert(strShowingCertificates);
        arrShowingCertificates=strShowingCertificates.split(',');
        var indexShowingCertificates;
        var ifToShow=false;
        
        for(indexShowingCertificates=0;indexShowingCertificates<arrShowingCertificates.length;indexShowingCertificates++)
        {
            //alert(arrShowingCertificates[indexShowingCertificates]);
            if(value.indexOf('*')!=-1) // this value includes sub segment like xxxxxx*xxxxxxx
            {
                value=value.substring(0,value.indexOf('*'));               
            }
            if(value==arrShowingCertificates[indexShowingCertificates])
            {
                ifToShow=true;                
            }
        }
        
        if(!ifToShow)
        {           
            return false;
        }
        
        
        /******* end: check if can show it ***********/ 
           
        indexCertificates++;
        arrCertificates[indexCertificates]=cerfiticate;        
        
        //alert("len:" + document.getElementById("<%#tblCertificates.ClientID%>").tBodies.length);
        var tbody = document.getElementById("<%#tblCertificates.ClientID%>").tBodies[0];
        //alert("tbody: " + tbody);
        //var tbody = document.getElementById("<%#tblCertificates.ClientID%>");

        //alert("cerfiticate:" + cerfiticate + " tbody: " + tbody);
           
        var tr = document.createElement("TR"); 
         
        //tr.setAttribute("id","trCertificates_" + cerfiticate);
        tr.setAttribute("id","trCertificates_" + cerfiticate);
        tbody.appendChild(tr); 
       
        var td = document.createElement("TD"); 
        
        var chk = document.createElement("INPUT"); 
        
        chk.setAttribute("type","checkbox"); 
        chk.setAttribute("id","chkCertificates_" + cerfiticate);
        chk.setAttribute("name","chkCertificates_" + cerfiticate);
        
        
        
        td.appendChild(chk);
        chk.checked=false;
        
        var span = document.createElement("SPAN");         
        //span.innerHTML="&nbsp;I am a certified " + cerfiticate;
        span.innerHTML="&nbsp;" + document.getElementById("<%#lblHideSpan.ClientID%>").value + " " + cerfiticate;
        td.appendChild(span);
        tr.appendChild(td);
       
        //Show "Your Certification" if not exists segment      
        if(tbody.getElementsByTagName('TR').length>1)
        {
            $get("<%# pan_certification.ClientID %>").style.visibility="visible";
        }
    }
    
   
   
    var arrAlreadyExist=new Array(); 
    
    
    var indexAlready=-1;
    
    var arrCertificatesStr='<%#arrCertificatesStr%>';
    
    
    
    //alert("arrAlreadyExist:" + arrAlreadyExist.length);
    
    if(arrCertificatesStr!="")
    {
        arrCertificatesStr=arrCertificatesStr.substring(0,arrCertificatesStr.length-1);
        //alert("arrCertificatesStr:" + arrCertificatesStr);
        
        var arrCertificatesStrSplit=arrCertificatesStr.split(',');
        
        for(indexCertificateAlready=0;indexCertificateAlready<arrCertificatesStrSplit.length;indexCertificateAlready++)
        {
            //alert("indexAlready:" + indexAlready);
            indexAlready=indexAlready+1;
            arrAlreadyExist[indexAlready]=arrCertificatesStrSplit[indexCertificateAlready];
        }
    }
    
    //alert("arrAlreadyExist:" + arrAlreadyExist.length);
    
    var indexArr=-1;
    var howMachTheSame=0;    
    
    function removeCertificates(cerfiticate)
    {  
        //alert("cerfiticate:" + cerfiticate + " " + cerfiticate.length);
        indexCertificates--;
        arrCertificates[indexCertificates]=cerfiticate;
        
        
        var tbody = document.getElementById("<%#tblCertificates.ClientID%>").tBodies[0];
        //alert(tbody);
        
        splitArrowCertificate=cerfiticate.split(" >> ");
        cerfiticateClear=splitArrowCertificate[0];
        //alert("cerfiticateClear:" + cerfiticateClear + " " + cerfiticateClear.length); 
        
        
        var certificateExist;
        
        
        for(i=0;i<document.getElementById("<%#selectProfessions2.ClientID%>").options.length;i++)
        {
        
            certificateExist=document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text.split(" >> ")[0];
            //alert(certificateExist + " " + certificateExist);
            if(certificateExist==cerfiticateClear)
            {
                //alert(howMachTheSame);
                howMachTheSame++;
            }
         
        }
         
       //alert("howMachTheSame: " + howMachTheSame);
        
        //alert(tbody.childNodes.length);
        if(howMachTheSame==0)
        {
            //alert("inside");
            //alert("arrAlreadyExist.length:" + arrAlreadyExist.length);
            for(indexAlready=0;indexAlready<arrAlreadyExist.length;indexAlready++)
            {
               //alert("certificateExist:" + certificateExist + " arrAlreadyExist[indexAlready]: " + arrAlreadyExist[indexAlready]);
               if(arrAlreadyExist[indexAlready]==cerfiticateClear)
               {
                    //alert("cerfiticateClear: " + cerfiticateClear);
                    arrAlreadyExist.splice(indexAlready,1);
                    break;
               }
                
            }
            
            //alert("arrAlreadyExist.length:" + arrAlreadyExist.length);
            for(indexAlready=0;indexAlready<arrAlreadyExist.length;indexAlready++)
            {
               
               //alert(arrAlreadyExist[indexAlready]);
               
                
            }
            //alert(tbody.childNodes.length);
            for(indexCertificates=0;indexCertificates<tbody.childNodes.length;indexCertificates++)
            {
                
                 //alert(tbody.childNodes[indexCertificates].id);
                 //alert(tbody.childNodes[indexCertificates]);
                 if(tbody.childNodes[indexCertificates].id!=undefined)
                 {
                    //alert("found1");
                    //alert(tbody.childNodes[indexCertificates].id + " " + cerfiticateClear + " " + cerfiticateClear.length );
                    //alert("certificate: " + tbody.childNodes[indexCertificates].id);
                     if(tbody.childNodes[indexCertificates].id.indexOf("_" + cerfiticateClear)>=0)
                     {
                        //alert("found2");
                        
                        tbody.removeChild(tbody.childNodes[indexCertificates]);
                        
                        //alert("found3");
                     }  
                 } 
            }        
        }
        
        howMachTheSame=0;  
        //Hide "Your Certification" if not exists segment      
        if(tbody.getElementsByTagName('TR').length==1)
        {
            $get("<%# pan_certification.ClientID %>").style.visibility="hidden";
        }
    }
    
    function addProfession()
    {
        //alert("addProfession()");
        if(document.getElementById("<%#selectProfessions.ClientID%>").selectedIndex>=0)
        {
            //alert("addProfession2()");
            var arrDelete=new Array();      
            
            var strProfession;
            
            var ifExists=false;
            
            for(i=0;i<document.getElementById("<%#selectProfessions.ClientID%>").options.length;i++)
            {
                if(document.getElementById("<%#selectProfessions.ClientID%>").options[i].selected)
                {
                    //alert("selected" + i);
                    indexArr++;
                    
                    //alert("indexAlready: " + indexAlready);
                    //alert("arrAlreadyExist.length: " + arrAlreadyExist.length);
                    arrDelete[indexArr]=i;
                    
                    ////arrAlreadyExist[indexArr]=document.getElementById("<%#selectProfessions.ClientID%>").options[i].text;
                    
                    splitArrow=document.getElementById("<%#selectProfessions.ClientID%>").options[i].text.split(" >> ");
                    //alert("splitArrow:" + splitArrow.length);
                    
                    strProfession=splitArrow[0];
                    //alert("length:" + arrAlreadyExist.length);
                    if(arrAlreadyExist.length>0)
                    {
                        for(y=0;y<arrAlreadyExist.length;y++)
                        {
                           //alert(arrAlreadyExist[y]);
                           if(arrAlreadyExist[y]==strProfession)
                            ifExists=true;            
                            
                        }
                        
                        if(ifExists==true)
                        {
                            //alert("exist");
                        }
                        else
                        {
                        //alert(document.getElementById("<%#selectProfessions.ClientID%>").options[i].value);
                            addCertificates(strProfession,document.getElementById("<%#selectProfessions.ClientID%>").options[i].value);
                            indexAlready++;
                            arrAlreadyExist[indexAlready]=strProfession;
                        }                            
                        
                    }
                    
                    else
                    {
                        //alert("0000");  
                        //alert("arrAlreadyExist.length:" + arrAlreadyExist.length);
                        indexAlready++;                     
                        arrAlreadyExist[indexAlready]=strProfession;
                        //alert("indexAlready:" + indexAlready);
                        //alert("arrAlreadyExist.length:" + arrAlreadyExist.length);
                        //alert("strProfession:" + strProfession);
                        addCertificates(strProfession,document.getElementById("<%#selectProfessions.ClientID%>").options[i].value);

                    }
                    // add
                    //alert("beforeAdd");
                    document.getElementById("<%#selectProfessions2.ClientID%>").options[document.getElementById("<%#selectProfessions2.ClientID%>").options.length]=new Option(document.getElementById("<%#selectProfessions.ClientID%>").options[i].text,document.getElementById("<%#selectProfessions.ClientID%>").options[i].value);
                    //alert("afterAdd");
                    
                    
                    ifExists=false;
                    //alert("arrProfessions.length:" + arrProfessions.length);
                    /*
                    alert("before" + arrProfessions.length);
                    arrProfessions[arrProfessions.length]=new Array(2);
                     alert("after");
                    arrProfessions[arrProfessions.length-1][0]=document.getElementById("<%#selectProfessions.ClientID%>").options[i].text;
                    alert("after2");
                    
                    arrProfessions[arrProfessions.length-1][1]=arrProfessions[arrProfessions.length-2][1]*1+1;
                    alert("after3");
                    */
                    
                    //alert(document.getElementById("<%#selectProfessions.ClientID%>").options[i].text);
                    /*
                    alert("len " + arrProfNameGuid.length);
                    for(index=0;index<arrProfNameGuid.length;index++)
                    {
                        //alert(arrProfessions[index][0] + " " + arrProfessions[index][1]);
                        if(arrProfNameGuid[index][0]==document.getElementById("<%#selectProfessions.ClientID%>").options[i].text)
                        {
                            
                            alert(arrProfNameGuid[index][0]);
                            arrProfNameGuid.splice(index,1);
                            break;
                        }
                       
                    }
                    
                    for(index=0;index<arrProfNameGuid.length;index++)
                    {                        
                            alert(arrProfNameGuid[index][0]);                       
                       
                    }  
                     */
                       
                }
                   
            }
            
           
            index=0;
            
            for(i=0;i<arrDelete.length;i++)
            {             
               //alert(arrDelete[i]);
                document.getElementById("<%#selectProfessions.ClientID%>").remove(arrDelete[i]-index);
                index++;      
                   
            }          
            
            indexArr=-1;
            
            updateHiddenField();        
          
            return true;
        }
        
        else
        {
            //alert('���� ���� ����� ������ ������');
            alert(document.getElementById("<%# Hidden_Choose.ClientID %>").value);
            return false;
        }
    }
    
    function removeProfession()
    {
        //alert("removeProfession");
        if(document.getElementById("<%#selectProfessions2.ClientID%>").selectedIndex>=0)
        {
            var arrDelete=new Array();
            var indexArr=-1;
            var optionNew;
            var arrDeleteNames=new Array();
            
            for(i=0;i<document.getElementById("<%#selectProfessions2.ClientID%>").options.length;i++)
            {
                //alert(document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text);
                
                if(document.getElementById("<%#selectProfessions2.ClientID%>").options[i].selected)
                {                    
               
                    //alert(document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text);
                    
                    indexArr++;
                    arrDelete[indexArr]=i;
                    arrDeleteNames[indexArr]=document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text;
                    // add
                    //document.getElementById("<%#selectProfessions.ClientID%>").options[document.getElementById("<%#selectProfessions.ClientID%>").options.length]=new Option(document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text,document.getElementById("<%#selectProfessions2.ClientID%>").options[i].value);
                    
                    optionNew = document.createElement('option');
                    optionNew.text = document.getElementById("<%#selectProfessions2.ClientID%>").options[i].text;
                    optionNew.value = document.getElementById("<%#selectProfessions2.ClientID%>").options[i].value;
                    
                    
                    var optionOld = document.getElementById("<%#selectProfessions.ClientID%>").options[optionNew.value];  
                    //alert(optionNew.value);
                    
                       
                    try
                    {
                       
                       document.getElementById("<%#selectProfessions.ClientID%>").add(optionNew, optionOld);                            
                     
          
                    }
                    catch(ex)  // ex
                    {
                        document.getElementById("<%#selectProfessions.ClientID%>").add(optionNew, optionNew.value); // IE only
                        //alert("after");
                    }
                    
                    
                     
                    for(index=0;index<arrProfessions.length;index++)
                    {
                        //alert(optionNew.text + " " + arrProfessions[index][0]);
                        if(optionNew.text==arrProfessions[index][0])
                        {  
                            //alert("find");
                            arrProfessions.splice(index,1);
                            break;  
                            
                        }
                    } 
                           
                    
                }
                
                
                   
            }
            
           
            
            index=0;
            
            for(i=0;i<arrDelete.length;i++)
            {               
                //alert(arrDelete[i]);
                document.getElementById("<%#selectProfessions2.ClientID%>").remove(arrDelete[i]-index);
                index++;      
                   
            }
           
            for(indexDeleteCertificate=0;indexDeleteCertificate<arrDeleteNames.length;indexDeleteCertificate++)
            {
                removeCertificates(arrDeleteNames[indexDeleteCertificate]);
            }
            
            updateHiddenField();
            
            return true;
        }
        
        else
        {
            //alert('���� ���� ����� ������ �����');
            alert(document.getElementById("<%# Hidden_Choose.ClientID %>").value);
            return false;
        }
    }
    
    
    
    function ifProfessionInDb(profession,place)
    {    
        //alert("ifProfessionInDb");    
        var index;
        var strReturn=false;
        
        for(index=0;index<arrProfessions.length;index++)
        {
            //alert(profession + " " + arrProfessions[index][0]);
            
            if(profession==arrProfessions[index][0])
            {     
                              
                arrProfessions[index][1]=place;    
                strReturn=true;
                break;
            }
            
        }
        
        return strReturn;
    } 
    
    function ifProfessionInChosenSelectedBox(profession,place)
    {
        //alert(profession + " " + place + " " + arrProfessions.length);
        var index;
        var strReturn=false;
        
        for(index=0;index<document.getElementById("<%#selectProfessions2.ClientID%>").options.length;index++)
        {
            //alert(profession + " " + arrProfessions[index][0]);
            
            if(profession==document.getElementById("<%#selectProfessions2.ClientID%>").options[index].text)
            {     
                             
                //arrProfessions[index][1]=place;    
                strReturn=true;
                break;
                
            }
            
        }
        
        return strReturn;
    }
    
    function setProfessionalProfessions()
    {
        document.getElementById("<%#selectProfessions2.ClientID%>").options.length=0;
		            
        for(i=0;i<arrProfessions.length;i++)
        {                     
           document.getElementById("<%#selectProfessions2.ClientID%>").options[document.getElementById("<%#selectProfessions2.ClientID%>").options.length]= new Option(arrProfessions[i][0],arrProfessions[i][1]);
        }	
        
        updateHiddenField();
    }
    
    function rereshProfessions()
    {   
             
        var url;
        //alert(escape(document.formProfessions.PrimaryExpertise.options[document.formProfessions.PrimaryExpertise.selectedIndex].text));
        url="<%#ResolveUrl("~")%>Management/professionalStartWithWebService.aspx?startWith=" + escape(document.getElementById("txtFind").value);
        //alert(url);
        var objHttp;
        var ua = navigator.userAgent.toLowerCase(); 
        
        if(ua.indexOf("msie")!= -1)
        {           
            objHttp =  new ActiveXObject("Msxml2.XMLHTTP");            
        }
        else
        {           
            objHttp=new XMLHttpRequest();
        }        
        
        objHttp.open("GET", url, true);
        
        objHttp.onreadystatechange=function()
        {	  
            //alert("objHttp.readyState "+objHttp.readyState);          
	        if (objHttp.readyState==4)
	        {	       
		       //alert("general" + objHttp.responseText);
		       var index=0;
		       if(objHttp.responseText!="empty")
		       {
		            //alert(objHttp.responseText);
		            splitProf=objHttp.responseText.split("&");
		            		            
		            document.getElementById("<%#selectProfessions.ClientID%>").options.length=0;
		            
		            for(i=0;i<splitProf.length;i++)
		            { 
		               splitProfGuid=splitProf[i].split("=");              
		               
		               strProfName=splitProfGuid[0];
		               //alert("strProfName:" + strProfName);
		               strProfGuid=splitProfGuid[1];
		               
		               arrProfNameGuid[i]=new Array(2);
		               arrProfNameGuid[i][0]=strProfName;
		               arrProfNameGuid[i][1]=strProfGuid;
		               
		               if(!ifProfessionInDb(strProfName,index))
		               {
		               		//alert("strProfName:" + strProfName);              
		                    document.getElementById("<%#selectProfessions.ClientID%>").options[document.getElementById("<%#selectProfessions.ClientID%>").options.length]= new Option(strProfName,index);		                
		                    index++;
		               }
		            }
		            
		            setProfessionalProfessions();		           
		          
		       }
		       
		       else
		       {
		            document.getElementById("<%#selectProfessions.ClientID%>").options.length=0;
		            document.getElementById("txtFind").value=document.getElementById("txtFind").value.substring(0,document.getElementById("txtFind").value.length-1);
		            rereshProfessions();
		       }
		       
		       
	        }
         }            
            
         objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        
         objHttp.send(null);
         
         	 
    }
    
    
    function searchProfessions()
    {
        strFind=document.getElementById("txtFind").value;
        strFindLength=strFind.length;
        
        //alert("strFind " + strFind);
        var index=0;
        document.getElementById("<%#selectProfessions.ClientID%>").options.length=0;
        
        for(i=0;i<splitProf.length;i++)
		{ 		               
           splitProfGuid=splitProf[i].split("=");              
           
           strProfName=splitProfGuid[0];
           strProfGuid=splitProfGuid[1];         
           
           if(strProfName.toLowerCase().indexOf(strFind.toLowerCase())!=-1)
           {
               if(!ifProfessionInChosenSelectedBox(strProfName,index))
               {
               		              
                    document.getElementById("<%#selectProfessions.ClientID%>").options[document.getElementById("<%#selectProfessions.ClientID%>").options.length]= new Option(strProfName,index);
                     		                
                    index++;
               }
           }
		               
		 }
		              
    }
    
    function searchProfessionsOld()
    {           
        var url;
        //alert(escape(document.formProfessions.PrimaryExpertise.options[document.formProfessions.PrimaryExpertise.selectedIndex].text));
        url="<%#ResolveUrl("~")%>Management/professionalStartWithWebService.aspx?startWith=" + escape(document.getElementById("txtFind").value);
       //alert(url);
        var objHttp;
        var ua = navigator.userAgent.toLowerCase(); 
        
        if(ua.indexOf("msie")!= -1)
        {           
            objHttp =  new ActiveXObject("Msxml2.XMLHTTP");            
        }
        else
        {           
            objHttp=new XMLHttpRequest();
        }        
        
        objHttp.open("GET", url, true);
        
        objHttp.onreadystatechange=function()
        {	  
            //alert("objHttp.readyState "+objHttp.readyState);          
	        if (objHttp.readyState==4)
	        {	       
		       //alert(objHttp.responseText);
		       var index=0;
		       if(objHttp.responseText!="empty")
		       {
		            //alert(objHttp.responseText);
		            splitProf=objHttp.responseText.split("&");
		            		            
		            document.getElementById("<%#selectProfessions.ClientID%>").options.length=0;
		            
		            for(i=0;i<splitProf.length;i++)
		            { 
		               
		               splitProfGuid=splitProf[i].split("=");              
		               
		               strProfName=splitProfGuid[0];
		               strProfGuid=splitProfGuid[1];
		               
		               /*
		               arrProfNameGuid[i]=new Array(2);
		               arrProfNameGuid[i][0]=strProfName;
		               arrProfNameGuid[i][1]=strProfGuid;
		               */
		              
                       /*
		               if(!ifProfessionInDb(strProfName,index))
		               {
		               		              
		                    document.getElementById("<%#selectProfessions.ClientID%>").options[document.getElementById("<%#selectProfessions.ClientID%>").options.length]= new Option(strProfName,index);
		                     		                
		                    index++;
		               }
		               */
		               
		               if(!ifProfessionInChosenSelectedBox(strProfName,index))
		               {
		               		              
		                    document.getElementById("<%#selectProfessions.ClientID%>").options[document.getElementById("<%#selectProfessions.ClientID%>").options.length]= new Option(strProfName,index);
		                     		                
		                    index++;
		               }
		               
		            }
		            
		            //setProfessionalProfessions();		           
		          
		       }
		       
		       else
		       {
		            document.getElementById("<%#selectProfessions.ClientID%>").options.length=0;
		            document.getElementById("txtFind").value=document.getElementById("txtFind").value.substring(0,document.getElementById("txtFind").value.length-1);
		           
		            searchProfessions();
		       }
		       
		       
	        }
         }            
            
         objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        
         objHttp.send(null);
         
         	 
    }
    
    function rereshProfessions2()
    {           
        var url;
        //alert(escape(document.formProfessions.PrimaryExpertise.options[document.formProfessions.PrimaryExpertise.selectedIndex].text));
                
        if(location.hostname=="localhost")
            url="http://localhost:" + location.port 
                + "<%#ResolveUrl("~")%>Management/professionalProfessions.aspx";
        else
            url="http://" + location.hostname + ":" + location.port + "/management/professionalProfessions.aspx";
        
        
        //alert(url);
        var objHttp;
        var ua = navigator.userAgent.toLowerCase(); 
        
        if(ua.indexOf("msie")!= -1)
        {           
            objHttp =  new ActiveXObject("Msxml2.XMLHTTP");            
        }
        else
        {           
            objHttp=new XMLHttpRequest();
        }        
        
        objHttp.open("GET", url, true);
        
        objHttp.onreadystatechange=function()
        {	  
            //alert("objHttp.readyState "+objHttp.readyState);          
	        if (objHttp.readyState==4)
	        {   
		       if(objHttp.responseText!="empty")
		       {
		            //alert("professionalProfessions " + objHttp.responseText);
		            splitSubProf=objHttp.responseText.split("&");
		            		            
		            //document.getElementById("<%#selectProfessions2.ClientID%>").options.length=0;
		            
		            for(i=0;i<splitSubProf.length;i++)
		            {	
		                
		               arrProfessions[i]=new Array(2); 
		               arrProfessions[i][0]=splitSubProf[i];		               		               
		               arrProfessions[i][1]=0;              
		               //document.getElementById("<%#selectProfessions2.ClientID%>").options[document.getElementById("<%#selectProfessions2.ClientID%>").options.length]= new Option(splitSubProf[i],i);
		            }		            
		            	            	           
		          
		       }
		       
		       else
		       {
		            document.getElementById("<%#selectProfessions2.ClientID%>").options.length=0;
		           
		       }
		       
		       rereshProfessions();
		       //y=ifProfessionInDb("���");
               //alert("y" + y);
	        }
	       
         }            
            
         objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        
         objHttp.send(null);
         
         
         
         	 
    }
    
    function getProfessionalRegionsAndAllRegionsInitialize()
    {  
           
       var strExpertisesAllAndExpertiseSpecific="";
       
       
       strExpertisesAllAndExpertiseSpecific='<%#strExpertisesAllAndExpertiseSpecific%>';
       //alert(strExpertisesAllAndExpertiseSpecific);   
       splitExpertisesAllExpertiseSpecific=strExpertisesAllAndExpertiseSpecific.split("*****");
       
       //splitRegionAllProfessions=objHttp.responseText.split("*****");
       //alert(splitRegionAllProfessions[0]);
       //alert(splitRegionAllProfessions[1]);
       //if(objHttp.responseText!="empty")
       if(splitExpertisesAllExpertiseSpecific[1]!="empty") // professional expertises
       {           
            //alert("getProfessionalRegions " + objHttp.responseText);
            splitSubProf=splitExpertisesAllExpertiseSpecific[1].split("&");          
            
            for(i=0;i<splitSubProf.length;i++)
            {                
               arrProfessions[i]=new Array(2); 
               arrProfessions[i][0]=splitSubProf[i];
               strAllProfessionalRegions=strAllProfessionalRegions + "&" + arrProfessions[i][0];               			               	               		               		               		               
               arrProfessions[i][1]=0;              
               //document.getElementById("<%#selectProfessions2.ClientID%>").options[document.getElementById("<%#selectProfessions2.ClientID%>").options.length]= new Option(splitSubProf[i],i);
            }		                  	           
          
       }
       
       else
       {
            document.getElementById("<%#selectProfessions2.ClientID%>").options.length=0;
           
       }       
       
       //alert("strAllProfessionalRegions:" + strAllProfessionalRegions);
       
       strAllProfessionalRegions=strAllProfessionalRegions + "&";
       //alert(strAllProfessionalRegions);
       //alert("2");
       //getRegions(level);
       
       var index=0;
       //var strAllRegions="";
       var ifHasAnyoneChild=false;
       
       if(splitExpertisesAllExpertiseSpecific[0]!="empty") // all regions
       {
            splitProf=splitExpertisesAllExpertiseSpecific[0].split("&");
		            		            
            document.getElementById("<%#selectProfessions.ClientID%>").options.length=0;
            
            for(i=0;i<splitProf.length;i++)
            { 
               splitProfGuid=splitProf[i].split("=");              
               
               strProfName=splitProfGuid[0];
               //alert(strProfName);
               strProfName=strProfName.replace("shaveshave","="); // = is special character i use it
               //alert(strProfName);
               //alert("strProfName:" + strProfName);
               strProfGuid=splitProfGuid[1];
               
               arrProfNameGuid[i]=new Array(2);
               arrProfNameGuid[i][0]=strProfName;
               arrProfNameGuid[i][1]=strProfGuid;
               
               if(strAllProfessionalRegions.indexOf("&" + strProfName + "&")==-1) // the profession is not in professional regions
               {
                    //alert("strRegionGuid a: " + strRegionGuid);
               		//alert("strRegionName:" + strRegionName);              
                    document.getElementById("<%#selectProfessions.ClientID%>").options[document.getElementById("<%#selectProfessions.ClientID%>").options.length]= new Option(strProfName,strProfGuid);		                

                    index++;
               }
               
               else
               {
                    document.getElementById("<%#selectProfessions2.ClientID%>").options[document.getElementById("<%#selectProfessions2.ClientID%>").options.length]= new Option(strProfName,strProfGuid);
                    //alert("strRegionGuid b: " + strRegionGuid);
                    strProfessionalExpertisesNow+=strProfName + "&";  // not including expertises in the past that not relevant today or disabled by the publisher                   
               }
            }
            
            if(strProfessionalExpertisesNow.length>0)
                strProfessionalExpertisesNow="&" + strProfessionalExpertisesNow;
                
            document.getElementById("<%#selectProfessions.ClientID%>").focus();            
            
            parent.hideDiv();
            
            
            
            //alert("3");
            //ifRegionInDbArray(arrChilds);
            //alert("beforeSet " + indexLevel);
            //if(indexLevel!=3)
                //setProfessionalRegions();		                
                
           
            
           
            // else
             //{
                //alert("baaa");
                //parent.hideDiv();
             //}
            //alert("afterbeforeSet");		           
          
       }
       
       else
       {
            document.getElementById("<%#selectProfessions.ClientID%>").options.length=0;
            
       }      
  
         	 
    }

    function initialize()
    {
        
        //rereshProfessions2();
        parent.goUp();
        getProfessionalRegionsAndAllRegionsInitialize();   
        document.getElementById("<%#selectProfessions.ClientID%>").focus();
      
         
    }
    
 //   window.onload=initialize;
     function pageLoad(sender, args) { 
        try{
            parent.hideDiv();
            }catch(ex){}
            initialize();
        }
    
</script>
</head>
<body class="iframe-inner step2" style="background-color:transparent;">
<form id="form2" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
    </asp:ScriptManager>



<p>
	<label for="form-sigments" runat="server" id="lblMySegments">Search for</label>
	<input id="txtFind" class="form-text" onkeyup="searchProfessions();" onkeydown="if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) return false;" type="text" />
	<asp:Image ID="img_MySegments" runat="server" ImageUrl="../images/icon-q.png" />
</p>

<div class="lists-select clearfix">
    <p class="list-select all">
	    <label for="form-all" runat="server" id="lblAllList">Heading List</label>
	    <select id="selectProfessions" multiple="true" class="form-select" runat="server" ondblclick="javascript:addProfession();"></select>			
	    <a href="#" class="btn" runat="server" id="btnAdd" onclick="javascript:return addProfession();">Add</a>
    </p>
    <p class="list-select choice">
	    <label for="form-choice" runat="server" id="lblYourChoice">Your choice</label>
	    <select id="selectProfessions2"  multiple="true" class="form-select" runat="server" ondblclick="javascript:removeProfession();"></select>
        <input type="hidden" id="hidden_professions" class="form-select" runat="server" />			
	    <a href="#" runat="server" id="a_Remove" class="btn" onclick="javascript:removeProfession();">Remove</a>
    </p>
</div>
		
<asp:Panel runat="server" ID="pan_certification">

<h3>
    <asp:Label ID="lblCertification" runat="server" Text="Your Certification"></asp:Label>
    <asp:Image ID="img_YourCertification" runat="server" ImageUrl="../images/icon-q.png" />
</h3>
</asp:Panel>
	
		
<asp:Table runat="server" ID="tblCertificates" >
   <asp:TableRow></asp:TableRow>              
</asp:Table>
      
        
        
       <input type="button" class="form-submit" runat="server" id="btnSendWebService" onclick="return sendWebService();"   />
	  	 
<p class="steps" runat="server" id="pSteps">
    <asp:Label ID="lbl_Step2" runat="server" Text="STEP 2 OUT"></asp:Label>
    <asp:Label ID="lbl_page" runat="server" ></asp:Label>    
    <img src="<%=ResolveUrl("~")%>Management/images/bullet.gif" alt="bullet" />
</p>


    <asp:HiddenField ID="lblHideSpan" runat="server" Value="I am a certified in" />
    <asp:HiddenField ID="Hidden_Choose" runat="server" Value="Choose profession" />
    <asp:HiddenField ID="lblHiddenUpdate" runat="server" Value="No selection  was made, please select at least one heading" />
    <asp:Label ID="lbl_btn_Update" runat="server" Text="UPDATE" Visible="false"></asp:Label>
    <asp:Label ID="lbl_btn_Next" runat="server" Text="NEXT" Visible="false"></asp:Label>    
   
</form>
</body>
</html>
