using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;


public partial class Management_frame : PageSetting
{
    
    protected RegionTab result;
    

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        
        
    }
   
    
 
    protected void Page_Load(object sender, EventArgs e)
    {      
        
        if (!IsPostBack)
        {        
       
             whatCityPage(siteSetting.GetSiteID);
            /*
             //if (siteSetting.CharchingCompany=="plimus")
                // runWest = true;
 */
             string ToBalance = Request.QueryString["ToBalance"];
             if (ToBalance == "true")
             {
                 iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalPayment.aspx?mode=init");
                 iframe.Attributes.Add("height", @"860px");
             }
             else
             {
                 iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalDetails.aspx?mode=init");
                 iframe.Attributes.Add("height", @"950px");
             }
            ClientScript.RegisterStartupScript(this.GetType(), "show_div", "showDiv();", true);
            /*
            if (_NeedIframeHeigth == "true")
                div_height.Style.Add(HtmlTextWriterStyle.Height, "900px");
            */
        }
        Page.Header.DataBind();        
    //    iframe.Attributes.Add("onload", "OnIframeLoad();");
    }


    private void whatCityPage(string SiteId)
    {
        result = (RegionTab)DBConnection.GetRegionTab(SiteId);
        
    }


    protected string cityPage
    {
        get
        {
            if (result == RegionTab.Map)
                return "professionalCities.aspx";
            else if (result == RegionTab.Tree)
                return "professionalCities3.aspx";
            else
                return "professionalCities3.aspx";

        }


    }
    protected string GetPaymentInfo
    {
        get { return Server.HtmlEncode(Hidden_PaymentInfo.Value); }
    }
    protected string GetSegmentsArea
    {
        get { return Server.HtmlEncode(Hidden_SegmentsArea.Value); }
    }
    protected string GetCities
    {
        get { return Server.HtmlEncode(Hidden_Cities.Value); }
    }
    protected string Get_Time
    {
        get { return Server.HtmlEncode(Hidden_Time.Value); }
    }
    protected string Get_MyCredits
    {
        get { return Server.HtmlEncode(Hidden_MyCredits.Value); }
    }   
    
    protected string Get_GeneralInfo
    {
        get { return Server.HtmlEncode(Hidden_GeneralInfo.Value); }
    }
    /*
    protected string NeedIframeHeigth
    {
        get { return _NeedIframeHeigth; }
    }
   */
}
