using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;



public partial class Management_professionalSetExpertises : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(Request["expertises"]);
        Response.ContentType = "text/xml";
        
        string xmlExpertises = Request["expertises"].ToString();
        string addExpertises = Request["addExpertises"].ToString();
        string removeExpertises = Request["removeExpertises"].ToString();
        
        string userName = Request["userName"].ToString();
        Guid userId = new Guid(Request["userId"].ToString());
        Guid advertiseId = new Guid(Request["advertiseId"].ToString());
        string advertiserName = Request["advertiserName"].ToString();
        string fieldId = Request["fieldId"].ToString();
        string fieldName = Request["fieldName"].ToString();
        string pageId = Request["pageId"].ToString();
        bool isFirstTimeV = Convert.ToBoolean(Request["isFirstTimeV"].ToString());
        
        SiteSetting ss = (SiteSetting)Session["Site"];        

        StringBuilder sb = new StringBuilder();
        sb.Append("<SupplierExpertise SiteId=\"" + ss.GetSiteID + "\" SupplierId=\"" + Session["myGuid"].ToString() + "\">");
        string[] splitExpertiseNameGuid;
        string[] splitFirstSecondExpertise;
        string[] splitFirstSecondExpertiseValue;

        if (xmlExpertises != "")
        {
            string[] splitExpertises = xmlExpertises.Split(',');            

            for (int i = 0; i < splitExpertises.Length; i++)
            {
                splitExpertiseNameGuid = splitExpertises[i].Split(new String[] { "=" }, StringSplitOptions.None);
                splitFirstSecondExpertise = splitExpertiseNameGuid[0].Split(new String[] { " >> " }, StringSplitOptions.None);
                //Response.Write("<br>" + splitFirsrtSecond.Length);
                if (splitFirstSecondExpertise.Length == 1)
                {
                    sb.Append("<PrimaryExpertise ID=\"" + splitExpertiseNameGuid[1] + "\" Certificate=\"" + checkCertificate(splitFirstSecondExpertise[0]) + "\">");
                }

                else
                {
                    splitFirstSecondExpertiseValue = splitExpertiseNameGuid[1].Split('*');
                    sb.Append("<PrimaryExpertise ID=\"" + splitFirstSecondExpertiseValue[0] 
                        + "\" Certificate=\"" + 
                        checkCertificate(splitFirstSecondExpertise[0].Trim()) + "\">");
                    sb.Append("<SecondaryExpertise>" + splitFirstSecondExpertiseValue[1] + "</SecondaryExpertise>");
                }

                sb.Append("</PrimaryExpertise>");

            }

            

            //Response.Write(sb.ToString());
            /*
            <SupplierExpertise SiteId="" SupplierId="">

            <PrimaryExpertise ID="">

            <SecondaryExpertise>ID</SecondaryExpertise>

            <SecondaryExpertise>ID</SecondaryExpertise>

            </PrimaryExpertise>

            <PrimaryExpertise ID="">

            <SecondaryExpertise>ID</SecondaryExpertise>

            </PrimaryExpertise>

            </SupplierExpertise>
            */

           
        }

        sb.Append("</SupplierExpertise>");

        //Response.Write(sb.ToString());
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(ss.GetUrlWebReference);
        WebReferenceSupplier.CreateSupplierExpertiseRequest request = new WebReferenceSupplier.CreateSupplierExpertiseRequest();
        request.Request = sb.ToString();

        string pagename="professionChooseProfessions.aspx";
        string page_translate = EditTranslateDB.GetPageNameTranslate(pagename, ss.siteLangId);

        List<WebReferenceSupplier.AuditEntry> liAudit = new List<WebReferenceSupplier.AuditEntry>();

        if(addExpertises.Length>0)
        {
            string[] addExpertisesSplit=addExpertises.Split(',');
            int indexAdd;
            for (indexAdd = 0; indexAdd<addExpertisesSplit.Length; indexAdd++)
            {
                WebReferenceSupplier.AuditEntry ae=new WebReferenceSupplier.AuditEntry();
                ae.NewValue = addExpertisesSplit[indexAdd];
                ae.OldValue = "Add";
                ae.UserName = userName;
                ae.UserId = userId;
                ae.AdvertiseId = advertiseId;
                ae.AdvertiserName = advertiserName;
                ae.FieldId = fieldId;
                ae.FieldName = fieldName;
                ae.PageId = pageId;
                ae.AuditStatus = (isFirstTimeV) ? WebReferenceSupplier.Status.Insert :
                    WebReferenceSupplier.Status.Update;
                ae.PageName = page_translate;
                liAudit.Add(ae);
            }
            
        }

        if (removeExpertises.Length > 0)
        {
            string[] removeExpertisesSplit = removeExpertises.Split(',');
            int indexRemove;
            for (indexRemove = 0; indexRemove < removeExpertisesSplit.Length; indexRemove++)
            {
                WebReferenceSupplier.AuditEntry ae = new WebReferenceSupplier.AuditEntry();
                ae.NewValue = removeExpertisesSplit[indexRemove];
                ae.OldValue = "Remove";
                ae.UserName = userName;
                ae.UserId = userId;
                ae.AdvertiseId = advertiseId;
                ae.AdvertiserName = advertiserName;
                ae.FieldId = fieldId;
                ae.FieldName = fieldName;
                ae.PageId = pageId;
                ae.AuditStatus = (isFirstTimeV) ? WebReferenceSupplier.Status.Insert :
                    WebReferenceSupplier.Status.Update;
                ae.PageName = page_translate;
                liAudit.Add(ae);
            }

        }

        request.AuditEntries = liAudit.ToArray();
        request.AuditOn = true;
        request.UserId = userId;
        string result = supplier.CreateSupplierExpertise(request);
       
      
        //Response.Write("result:" + result + "  " + sb.ToString());
        Page.DataBind();
        Response.Write(result);
    }

    private string checkCertificate(string expertise)
    {
        string ifCertificate = "False";
        string xmlCertificates = Request["certificates"];
        
        string[] splitCertificates = xmlCertificates.Split(',');

        for (int i = 0;i < splitCertificates.Length;i++)
        {

            if (expertise == splitCertificates[i].Trim())
            {
                ifCertificate = "True";
                break;
            }
        }
        
        return ifCertificate;
    }
}
