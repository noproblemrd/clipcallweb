﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professionalDetails.aspx.cs"
 Inherits="Professional_professionalDetails"  EnableEventValidation="false" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asps" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../AutoCompleteCity/StyleAutoComplete.css" rel="stylesheet" type="text/css"/>
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
    <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .PanelDateOfBirth img
        { 
            vertical-align: middle;
        }
    </style>
    
    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript" src=" //ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
   
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <!--script type="text/javascript" language="javascript" src="../AutoCompleteCity/AutoCompleteCity.js"></script>-->
    
    <script type="text/javascript">    
    var level_txtId=new Array();
    var TheLevel;
    
    
    var _flag;
    var sentTxtBox;
    var lblMessage;
    
    var txt_id;
    function Set_level_txtId(arg)
    {
        args=arg.split(',');
        for(var i=0;i<args.length;i++)
        {
            level_txtId[i]=args[i];
        }
    }
    function witchAlreadyStep(level,mode)
    {    
        //alert(level + " " + mode); 
        //level="0";   
        parent.window.witchAlreadyStep(level);
        level=level*1;
        if(mode=="init")
        {   if(level!=6 && level!=0)
                parent.window.setInitRegistrationStep(level);
            else
                parent.window.setLinkStepActive('linkStep1');
            
        }
        else
            parent.window.setLinkStepActive('linkStep1');        
    }
    
    function SetDisableTxtbox(txtId)
    {       
        $get(txtId).readOnly = true;
        TurnOffOnValidation(txtId, false);
    }
   //not in used
  function checkPhone(sender, args)
  {
    //alert(args.Value);
    var ifNumber=true;
    
    for(i=0;i<args.Value.length;i++)
    {        
       if(isNaN(args.Value.charAt(i)) )
       {    
           if (args.Value.charAt(i)=="-" || args.Value.charAt(i)=="+") 
           {
            //alert(args.Value.charAt(i));
           }
           else
           {       
               ifNumber=false;
               break;
           }        
       }
    }
    
    //alert(ifNumber);
    
    if(ifNumber)
    {
        ////parent.window.step2();
        args.IsValid = true;
    }
    else
        args.IsValid = false;
   
  }
  
  
  function setStep2()
  {       
    
     parent.window.step2();
  }
  
  function SetUserNameSetting(name)
  {
    parent.window.setUserNameSetting(name);
  }
  function TurnOffErrorCity()
  {
  
    var _elm=document.getElementById("<%# lbl_errorCity.ClientID %>");
   
    try{
        _elm.style.display="none";
    }catch(ex){}
  }
  //not in used
  function ValidControl(control, lbl, isOK)
  {
        if(control.value.length==0)
        {
            control.className="txt_focus";
            lbl.style.display="inline";
            control.focus();
            return false;
        }
        
        if(isOK)
            control.className="form-text";
        lbl.style.display="none";
        return true;
        
  }
 
   function OnCompleteChk(arg)
    {
   //     if(arg.length>0)
 //             RegionService.SetList(TheLevel, arg, OnCompleteGetList, OnErrorEx, OnTimeOutEx);
        if(arg != "false")
            Set_Children(txt_id);
        else
        {
            $get(txt_id).readOnly = true;
            TurnOffOnValidation(txt_id, false);
        }
        parent.hideDiv();
 //       alert("arg="+arg);
    }
    function TurnOffOnValidation(_txt_id, _on)
    {
   //     alert(_txt_id);
   //     var _detail=GetDetailsOfChildAutoComplete(_txt_id);
  //      var requ = _detail.split(";");
        var reqId = GetRequiredFieldValidatorId(_txt_id);
        var validators = Page_Validators;  
        for (var i = 0;i < validators.length; i++)
        {
            if(validators[i].id==reqId)
            {
                ValidatorEnable(validators[i], _on);
                break;
            }
        }
    }
    function OnTimeOutEx(arg)
    {  
	    alert("timeOut has occured");
    }
    function OnErrorEx(arg)
    {  
//	    alert("Error has occured");
    }
  function chkOneBox(control)
  {
        if(GetValidatorOfControl(control))        
            control.className="txt_focus";        
        else    
            control.className="form-text";
            /*
        var _detail=GetDetailsOfChildAutoComplete(control);          
        if(_detail==null)
            return;
        */
        
  }
  function SetChild(sender, args )
  {
        Set_Children(sender._element.id);
      /*
        var _detail=GetDetailsOfChildAutoComplete(sender._element);
  //       alert('1');           
        if(_detail==null)
            return;
//            alert('2');
        parent.showDiv();
        var details=_detail.split(";");
        $get(details[0]).readOnly = false;
        var name="";
        TheLevel=parseInt(details[1]);
        if(details[2].length!=0)
        {
   //         alert('po1');
            name=$get(details[2]).value;  
            var parentLevel=parseInt(GetLevelOfTxtBox(details[2]));
            RegionService.GetGuidRegion(parentLevel, name, TheLevel, OnCompleteChk, OnErrorEx, OnTimeOutEx)
        }
 //       alert(args.get_value() ); 
//    alert(sender._element.id  );
*/
   }
   function Set_Children(txtId)
   {
         var _detail=GetDetailsOfChildAutoComplete(txtId);
   //      alert( "id="+txtId+";;;"+  _detail);        
        if(_detail==null)
            return;
//            alert('2');
        parent.showDiv();
        var details=_detail.split(";");
        $get(details[0]).readOnly = false;
        $get(details[0]).value="";
        var name=$get(details[2]).value;
        TheLevel=parseInt(details[1]);
    //    alert(name);
        if(name.length==0)
        {
           $get(details[0]).readOnly = true;
           TurnOffOnValidation(details[0], true);
        }
            
        txt_id= details[0];
        var parentLevel=parseInt(GetLevelOfTxtBox(details[2]));            
        RegionService.GetGuidRegion(parentLevel, name, TheLevel, OnCompleteChk, OnErrorEx, OnTimeOutEx)
        
        
   }
  function GetLevelOfTxtBox(txtId)
  {
        for(var i=0;i<level_txtId.length;i++)
        {
            var elements=level_txtId[i].split(";");
            if(elements[0]==txtId)
                return elements[1];
        }
        return null;
  }
  function GetRequiredFieldValidatorId(txtId)
  {
        for(var i=0;i<level_txtId.length;i++)
        {
            var elements=level_txtId[i].split(";");
            if(elements[0]==txtId)
                return elements[3];
        }
        return null;
  }
  function GetDetailsOfChildAutoComplete(control_id)
  {
        
        for(var i=0;i<level_txtId.length;i++)
        {
            var elements=level_txtId[i].split(";");
            if(elements[2]==control_id)
                return level_txtId[i];
        }
        return null;
  }
  function GetValidatorOfControl(control)
  {
    //    var IsInvalid=false;
        var validators = Page_Validators;  
        for (var i = 0;i < validators.length; i++)
        {
            if(control==document.getElementById(validators[i].controltovalidate))
            {
                if(!validators[i].isvalid)
                    return true;               
            }
        }
        return false;
  }
  
  function chkAll()
  {
       
        var IfEnd=_flag;
        var cityName=$get("<%# cityName.ClientID %>");
        
        var lbl_errorCity=$get("<%# lbl_errorCity.ClientID %>");
   
        if(IfEnd)
        {
            setTimeout("chkAll()",100);
            return false;
         }
         
         if(cityName && cityName.value.length==0 && lbl_errorCity.style.display=="inline")           
            return false;
            
            
       
        if(!Page_ClientValidate('detail'))
        {
            var MyArray=new Array();
            var validators = Page_Validators;  
            for (var i = validators.length-1; i > -1; i--) { 
                var _elm = document.getElementById(validators[i].controltovalidate)
        //       alert(validators[i].isvalid);
                if(!validators[i].isvalid)
                {
                    _elm.className="txt_focus";       
                    _elm.focus();  
                    MyArray.unshift(validators[i].controltovalidate);                        
                }  
                 else
                 {
                    if(!IfContain(validators[i].controltovalidate, MyArray))                   
                        _elm.className="form-text";
                    
                }          
            }
           return false;
            
        } 
   //     document.getElementById("<%# hf_HasChanges.ClientID %>").value = "false";
        window.onbeforeunload = null
        parent.showDiv();
   //     return true;
        <%# PostBackStr %>;
  }
  
 
  function IfContain(element, _array)
  {
        
        for(var i=0;i<_array.length;i++)
        {
            if(_array[i] == element)
                return true;
        }
        return false;
  }
   //autoComplete
  function setPosition(sender, args)
  {
  
  //  alert("s="+sender.value +";;ar="+args.value);
    var panel = document.getElementById("<%#Panel_showCity.ClientID %>");
    var elm=document.getElementById('<%#cityName.ClientID %>');
    panel.style.top=findTop(elm);
    panel.style.left=findLeft(elm);
    panel.style.position="absolute";
    
  }
   //autoComplete
  function setPositionState(sender, args)
  {
    
    var panel = document.getElementById("<%#Panel_state.ClientID %>");
    var elm=document.getElementById('<%#stateName.ClientID %>');
    panel.style.top=findTop(elm);
    panel.style.left=findLeft(elm);
    panel.style.position="absolute";
  }
  function setPositionDistrict()
  {
 
    var panel = document.getElementById("<%#Panel_district.ClientID %>");
    var elm=document.getElementById('<%#txt_District.ClientID %>');
    panel.style.top=findTop(elm);
    panel.style.left=findLeft(elm);
    panel.style.position="absolute";
  }
   function setPositionStreet()
  {
 
    var panel = document.getElementById("<%#Panel_street.ClientID %>");
    var elm=document.getElementById('<%#streetName.ClientID %>');
    panel.style.top=findTop(elm);
    panel.style.left=findLeft(elm);
    panel.style.position="absolute";
  }
  
  function GetLevelOfTxtbox(txt_box)
  {
        for(var i=0;i<level_txtId.length;i++)
        {
            var details=level_txtId[i].split(";");
            if(txt_box.id==details[0])
                return details[1];
        }
        return "";
  }
  function CheckMatch(txt_box, _lbl, RequiredFieldValidator)
  {
    
        lblMessage = _lbl;
        sentTxtBox = txt_box;
       
        var sent = txt_box.value;
        if(sent.length==0)
        {
            if(RequiredFieldValidator)
                sentTxtBox.className="txt_focus";
            return;
        }
        _flag=true;
         RegionService.chkMatcReturnCorrect(sent, GetLevelOfTxtbox(txt_box), OnComleteIfMatch, OnErrorEx, OnTimeOutEx);
    }
    function OnComleteIfMatch(arg)
    {
      //  var city=sentTxtBox.value.toLowerCase();
        if(arg)
        {
            lblMessage.style.display="none";
            sentTxtBox.className="form-text";            
        }
        else
        {
            
            lblMessage.style.display="inline";
            sentTxtBox.className="txt_focus";
            sentTxtBox.value="";
            Set_Children(sentTxtBox.id);
        }
        
        _flag=false;
    }
  function init()
  {
        parent.goUp();
        /*
        var input_elements = document.getElementsByTagName("input");
        for(var i=0; i<input_elements.length; i++)
        {
            if(input_elements[i].type != "text")
                continue;
            input_elements[i].onfocus = input_onfocus;
        }
        document.getElementById("<# hf_HasChanges.ClientID %>").value = "false";
        */
  }
  function input_onfocus()
  {
    document.getElementById("<%# hf_HasChanges.ClientID %>").value = "true";
  }
    
  function GetLblCityError()
  {
    return $get('<%# lbl_errorCity.ClientID %>');
  }
  function GetFieldValidatorCity()
  {
    return $get('<%# RequiredFieldValidatorAddress.ClientID %>');
  }
  
  function GetLblStateError()
  {
    return $get('<%# lbl_errorState.ClientID %>');
  }
  function GetFieldValidatorState()
  {
    return $get('<%# RequiredFieldValidatorState.ClientID %>');
  }
  
  function GetLblDistrictError()
  {
    return $get('<%# lbl_errorDistrict.ClientID %>');
  }
  function GetFieldValidatorDistrict()
  {
    return $get('<%# RequiredFieldValidatorDistrict.ClientID %>');
  }
  
  function GetLblStreetError()
  {
    return $get('<%# lbl_errorStreet.ClientID %>');
  }
  function GetFieldValidatorStreet()
  {
    return $get('<%# RequiredFieldValidatorStreet.ClientID %>');
  }
  //window.onload=init;
       
  function pageLoad(sender, args)
  {
   
    try{
        parent.hideDiv();
    }catch(ex){}
  }
  $(function(){
      /******** set date of birth ********/
      var currentDate = new Date();
      var minDate = new Date();
      var relevantYear = new Date();

    //  currentDate.setFullYear(currentDate.getFullYear() - 18);

      minDate.setFullYear(minDate.getFullYear() - 100);

      var pickerOpts = {
          showOn: "both", // will open click the input and click the icon
          buttonImageOnly: true,
          buttonImage: "../images/calendar.png",
          maxDate: currentDate,
          minDate: minDate,
          yearRange: minDate.getFullYear() + ":" + currentDate.getFullYear(),
          buttonText: "",
          changeMonth: true,
          changeYear: true,

          onSelect: function (dateStr) {
              $('#<%# hf_DateOfBirth.ClientID %>').val(dateStr);
          }
      };


      $("#<%# txt_DateOfBirth.ClientID %>").datepicker(pickerOpts); // from http://jqueryui.com/datepicker/
   //   $("#<# txt_DateOfBirth.ClientID %>").datepicker("setDate", new Date());
      /***************************** end set date of birth *********************************/
  });
  
  
</script>
</head>

<body class="iframe-inner step1" runat="server"  style="background-color:transparent;">
<form id="form1"  runat="server">

<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
    <Services>
        <asp:ServiceReference Path="../RegionService.asmx" />
    </Services>
</cc1:ToolkitScriptManager>

    	
<h3><asp:Label ID="lbl_welcome" runat="server" Text="Welcome!"></asp:Label></h3>
<div class="intro">
    <p>                                       
        <asp:Label ID="lbl_title1" runat="server" Text="Thank you for choosing our unique advertising solution!"></asp:Label>
        <asp:Label ID="lbl_title2" runat="server" Text="To start receiving calls and generate revenues, just follow these 5 simple steps"></asp:Label>
        <asp:Label ID="lbl_title3" runat="server" Text="Please fill in the required information."></asp:Label>
    </p>
    <p>
        <asp:Label ID="lbl_title4" runat="server" Text="We will never share your private data with anyone."></asp:Label>        
        <asp:Label ID="lbl_title5" runat="server" Text="Learn more about our Privacy Policy."></asp:Label>
    </p>
</div>
	
<asp:Panel ID="_PanelOsekId" runat="server">
	<p>	        
		<label for="osekId" runat="server" id="lbl_companyNum">Company Reg. No'</label>			
		<asp:TextBox runat="server" ID="osekId" CssClass="form-text"  TabIndex="1" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_companyNum" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
        <asp:RegularExpressionValidator ID="RegularExpressionValidatorCompanyNum" CssClass="error-msg" ControlToValidate="osekId"
        runat="server" ErrorMessage="*" Display="Dynamic" ValidationGroup="detail" ValidationExpression="\d+">Invalid</asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="osekId" Display="Dynamic"
		CssClass="error-msg" ID="RequiredFieldValidatorCompanyNum" ValidationGroup="detail" ErrorMessage="*" >Missing</asp:RequiredFieldValidator>
	</p>
</asp:Panel> 
<asp:Panel ID="_PanelFirstName" runat="server" >       	       
    <p>
        <label for="firstName" runat="server" id="lbl_FName">First Name</label>			
        <asp:TextBox runat="server" ID="firstName" onblur="javascript:chkOneBox(this);" CssClass="form-text" TabIndex="2" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_FirstName" runat="server" ImageUrl="../images/icon-q.png" CssClass="icon-q" />
        <asp:RequiredFieldValidator runat="server" ControlToValidate="firstName" Display="Dynamic" CssClass="error-msg" 
        ID="RequiredFieldValidatorFirstName" ValidationGroup="detail" ErrorMessage="*" >Missing</asp:RequiredFieldValidator>
    </p>
</asp:Panel>
<asp:Panel ID="_PanelSecondName" runat="server" > 
	<p>
		<label for="secondName" runat="server" id="lbl_LName">Second Name</label>			
		<asp:TextBox runat="server" ID="secondName" CssClass="form-text" TabIndex="3" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
		<asp:Image ID="img_LName" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
		
		<asp:RequiredFieldValidator runat="server" ControlToValidate="secondName" Display="Dynamic"
		CssClass="error-msg" ID="RequiredFieldValidatorSecondName" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
	</p>
</asp:Panel>
<asp:Panel ID="_PanelCompanyName" runat="server" CssClass="must" > 	           
    <p>
       <label for="companyName" runat="server" id="lbl_company">Company Name</label> 
       <asp:TextBox runat="server" ID="companyName" CssClass="form-text" TabIndex="4" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
       <asp:Image ID="img_CompanyName" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
       
       <asp:RequiredFieldValidator runat="server" ControlToValidate="companyName" Display="Dynamic"
       CssClass="error-msg" ID="RequiredFieldValidatorCompanyName" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
    </p>   
</asp:Panel>
<asp:Panel ID="_PanelcontactPhone" runat="server" CssClass="must" >  
    <p>
		<label for="contactPhone" runat="server" id="lbl_contactPerson">Main Phone</label>
		<asp:TextBox runat="server" ID="contactPhone" CssClass="form-text" TabIndex="5" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
		<asp:Image ID="img_ContactPersonPhone" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="contactPhone" Display="Dynamic"
		CssClass="error-msg" ID="RequiredFieldValidatorPhone"  ValidationGroup="detail">Missing</asp:RequiredFieldValidator>
		
		<asp:RegularExpressionValidator ID="CustomValidatorPhone" CssClass="error-msg"
        runat="server" ErrorMessage="The correct format is" Display="Dynamic"
        ValidationGroup="detail" ControlToValidate="contactPhone"
        ValidationExpression="\d+"></asp:RegularExpressionValidator> 
    </p>  
</asp:Panel>
<asp:Panel ID="_PanelFax" runat="server" > 			
	<p>
		<label for="fax" runat="server" id="lbl_fax">Fax</label>
        <asp:TextBox runat="server" ID="fax" CssClass="form-text" TabIndex="6" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_fax" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
            
        <asp:RegularExpressionValidator ID="RegularExpressionValidatorFax" CssClass="error-msg"
        runat="server" ErrorMessage="Invalid" Display="Dynamic"
        ValidationGroup="detail" ControlToValidate="fax"
        ValidationExpression="\d+"></asp:RegularExpressionValidator>
               
        <asp:RequiredFieldValidator runat="server" ControlToValidate="fax" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidatorFax" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>        
	</p>
</asp:Panel>
<asp:Panel ID="_PanelSmsNumber" runat="server" > 			
	<p>
		<label for="txt_SmsNumber" runat="server" id="lbl_SmsNumber">Sms number</label>
        <asp:TextBox runat="server" ID="txt_SmsNumber" CssClass="form-text" TabIndex="6" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_SmsNumber" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
            
        <asp:RegularExpressionValidator ID="RegularExpressionValidator_SmsNumber" CssClass="error-msg"
            runat="server" ErrorMessage="Invalid" Display="Dynamic"
            ValidationGroup="detail" ControlToValidate="txt_SmsNumber"
            ValidationExpression="\d+"></asp:RegularExpressionValidator>
               
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_SmsNumber" Display="Dynamic"
            CssClass="error-msg" ID="RequiredFieldValidator_SmsNumber" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>        
	</p>
</asp:Panel>
<asp:Panel ID="_PanelEmail" runat="server" CssClass="must" > 	
	<p>
		<label for="email" runat="server" id="lbl_email">E-mail</label>
		<asp:TextBox runat="server" ID="email" CssClass="form-text" TabIndex="7" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
		<asp:Image ID="img_email" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="email" Display="Dynamic" CssClass="error-msg" ID="RequiredFieldValidatorEmail"  ValidationGroup="detail">Missing</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator runat="server" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ID="RegularExpressionValidatorEmail" ControlToValidate="email" CssClass="error-msg" ValidationGroup="detail">Invalid</asp:RegularExpressionValidator>			
	</p>
</asp:Panel>
<asp:Panel ID="_PanelTimeZone" runat="server" Visible="false" >
	<p>
		<label for="ddl_TimeZone"  runat="server" id="lbl_TimeZone">Time zone</label>
        <asp:DropDownList ID="ddl_TimeZone" runat="server" CssClass="form-select" TabIndex="8"></asp:DropDownList>
		<asp:Image ID="img_TimeZone" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
	</p>		
</asp:Panel>
<asp:Panel ID="_PanelCountry" runat="server" Visible="false"> 
    <p>
		<label for="countryName" runat="server" id="lblCountry">Country</label>
	    <select id="countryName" name="countryName2" class="form-select" runat="server"  tabindex="9">
	        <option value="" >*** Choose Country ***</option> 
            <option value="United States" >United States</option> 
            <option value="Canada">Canada</option> 
            <option value="United Kingdom" >United Kingdom</option>
            <option value="Ireland" >Ireland</option>
            <option value="Australia" >Australia</option>
            <option value="New Zealand" >New Zealand</option>
            <option value="null" >-------------------</option>
            <option value="Afghanistan">Afghanistan</option> 
            <option value="Albania">Albania</option> 
            <option value="Algeria">Algeria</option> 
            <option value="American Samoa">American Samoa</option> 
            <option value="Andorra">Andorra</option> 
            <option value="Angola">Angola</option> 
            <option value="Anguilla">Anguilla</option> 
            <option value="Antarctica">Antarctica</option> 
            <option value="Antigua and Barbuda">Antigua and Barbuda</option> 
            <option value="Argentina">Argentina</option> 
            <option value="Armenia">Armenia</option> 
            <option value="Aruba">Aruba</option> 
            <option value="Australia">Australia</option> 
            <option value="Austria">Austria</option> 
            <option value="Azerbaijan">Azerbaijan</option> 
            <option value="Bahamas">Bahamas</option> 
            <option value="Bahrain">Bahrain</option> 
            <option value="Bangladesh">Bangladesh</option> 
            <option value="Barbados">Barbados</option> 
            <option value="Belarus">Belarus</option> 
            <option value="Belgium">Belgium</option> 
            <option value="Belize">Belize</option> 
            <option value="Benin">Benin</option> 
            <option value="Bermuda">Bermuda</option> 
            <option value="Bhutan">Bhutan</option> 
            <option value="Bolivia">Bolivia</option> 
            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
            <option value="Botswana">Botswana</option> 
            <option value="Bouvet Island">Bouvet Island</option> 
            <option value="Brazil">Brazil</option> 
            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
            <option value="Brunei Darussalam">Brunei Darussalam</option> 
            <option value="Bulgaria">Bulgaria</option> 
            <option value="Burkina Faso">Burkina Faso</option> 
            <option value="Burundi">Burundi</option> 
            <option value="Cambodia">Cambodia</option> 
            <option value="Cameroon">Cameroon</option> 
            <option value="Canada">Canada</option> 
            <option value="Cape Verde">Cape Verde</option> 
            <option value="Cayman Islands">Cayman Islands</option> 
            <option value="Central African Republic">Central African Republic</option> 
            <option value="Chad">Chad</option> 
            <option value="Chile">Chile</option> 
            <option value="中国">China</option> 
            <option value="Christmas Island">Christmas Island</option> 
            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
            <option value="Colombia">Colombia</option> 
            <option value="Comoros">Comoros</option> 
            <option value="Congo">Congo</option> 
            <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
            <option value="Cook Islands">Cook Islands</option> 
            <option value="Costa Rica">Costa Rica</option> 
            <option value="Cote D'ivoire">Cote D'ivoire</option> 
            <option value="Croatia">Croatia</option> 
            <option value="Cuba">Cuba</option> 
            <option value="Cyprus">Cyprus</option> 
            <option value="Czech Republic">Czech Republic</option> 
            <option value="Denmark">Denmark</option> 
            <option value="Djibouti">Djibouti</option> 
            <option value="Dominica">Dominica</option> 
            <option value="Dominican Republic">Dominican Republic</option> 
            <option value="Ecuador">Ecuador</option> 
            <option value="Egypt">Egypt</option> 
            <option value="El Salvador">El Salvador</option> 
            <option value="Equatorial Guinea">Equatorial Guinea</option> 
            <option value="Eritrea">Eritrea</option> 
            <option value="Estonia">Estonia</option> 
            <option value="Ethiopia">Ethiopia</option> 
            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
            <option value="Faroe Islands">Faroe Islands</option> 
            <option value="Fiji">Fiji</option> 
            <option value="Finland">Finland</option> 
            <option value="France">France</option> 
            <option value="French Guiana">French Guiana</option> 
            <option value="French Polynesia">French Polynesia</option> 
            <option value="French Southern Territories">French Southern Territories</option> 
            <option value="Gabon">Gabon</option> 
            <option value="Gambia">Gambia</option> 
            <option value="Georgia">Georgia</option> 
            <option value="Germany">Germany</option> 
            <option value="Ghana">Ghana</option> 
            <option value="Gibraltar">Gibraltar</option> 
            <option value="Greece">Greece</option> 
            <option value="Greenland">Greenland</option> 
            <option value="Grenada">Grenada</option> 
            <option value="Guadeloupe">Guadeloupe</option> 
            <option value="Guam">Guam</option> 
            <option value="Guatemala">Guatemala</option> 
            <option value="Guinea">Guinea</option> 
            <option value="Guinea-bissau">Guinea-bissau</option> 
            <option value="Guyana">Guyana</option> 
            <option value="Haiti">Haiti</option> 
            <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
            <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
            <option value="Honduras">Honduras</option> 
            <option value="Hong Kong">Hong Kong</option> 
            <option value="Hungary">Hungary</option> 
            <option value="Iceland">Iceland</option> 
            <option value="India">India</option> 
            <option value="Indonesia">Indonesia</option> 
            <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
            <option value="Iraq">Iraq</option> 
            <option value="Ireland">Ireland</option> 
            <option value="Israel">Israel</option> 
            <option value="Italy">Italy</option> 
            <option value="Jamaica">Jamaica</option> 
            <option value="Japan">Japan</option> 
            <option value="Jordan">Jordan</option> 
            <option value="Kazakhstan">Kazakhstan</option> 
            <option value="Kenya">Kenya</option> 
            <option value="Kiribati">Kiribati</option> 
            <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
            <option value="Korea, Republic of">Korea, Republic of</option> 
            <option value="Kuwait">Kuwait</option> 
            <option value="Kyrgyzstan">Kyrgyzstan</option> 
            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
            <option value="Latvia">Latvia</option> 
            <option value="Lebanon">Lebanon</option> 
            <option value="Lesotho">Lesotho</option> 
            <option value="Liberia">Liberia</option> 
            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
            <option value="Liechtenstein">Liechtenstein</option> 
            <option value="Lithuania">Lithuania</option> 
            <option value="Luxembourg">Luxembourg</option> 
            <option value="Macao">Macao</option> 
            <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
            <option value="Madagascar">Madagascar</option> 
            <option value="Malawi">Malawi</option> 
            <option value="Malaysia">Malaysia</option> 
            <option value="Maldives">Maldives</option> 
            <option value="Mali">Mali</option> 
            <option value="Malta">Malta</option> 
            <option value="Marshall Islands">Marshall Islands</option> 
            <option value="Martinique">Martinique</option> 
            <option value="Mauritania">Mauritania</option> 
            <option value="Mauritius">Mauritius</option> 
            <option value="Mayotte">Mayotte</option> 
            <option value="Mexico">Mexico</option> 
            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
            <option value="Moldova, Republic of">Moldova, Republic of</option> 
            <option value="Monaco">Monaco</option> 
            <option value="Mongolia">Mongolia</option> 
            <option value="Montserrat">Montserrat</option> 
            <option value="Morocco">Morocco</option> 
            <option value="Mozambique">Mozambique</option> 
            <option value="Myanmar">Myanmar</option> 
            <option value="Namibia">Namibia</option> 
            <option value="Nauru">Nauru</option> 
            <option value="Nepal">Nepal</option> 
            <option value="Netherlands">Netherlands</option> 
            <option value="Netherlands Antilles">Netherlands Antilles</option> 
            <option value="New Caledonia">New Caledonia</option> 
            <option value="New Zealand">New Zealand</option> 
            <option value="Nicaragua">Nicaragua</option> 
            <option value="Niger">Niger</option> 
            <option value="Nigeria">Nigeria</option> 
            <option value="Niue">Niue</option> 
            <option value="Norfolk Island">Norfolk Island</option> 
            <option value="Northern Mariana Islands">Northern Mariana Islands</option> 
            <option value="Norway">Norway</option> 
            <option value="Oman">Oman</option> 
            <option value="Pakistan">Pakistan</option> 
            <option value="Palau">Palau</option> 
            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
            <option value="Panama">Panama</option> 
            <option value="Papua New Guinea">Papua New Guinea</option> 
            <option value="Paraguay">Paraguay</option> 
            <option value="Peru">Peru</option> 
            <option value="Philippines">Philippines</option> 
            <option value="Pitcairn">Pitcairn</option> 
            <option value="Poland">Poland</option> 
            <option value="Portugal">Portugal</option> 
            <option value="Puerto Rico">Puerto Rico</option> 
            <option value="Qatar">Qatar</option> 
            <option value="Reunion">Reunion</option> 
            <option value="Romania">Romania</option> 
            <option value="Russian Federation">Russian Federation</option> 
            <option value="Rwanda">Rwanda</option> 
            <option value="Saint Helena">Saint Helena</option> 
            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
            <option value="Saint Lucia">Saint Lucia</option> 
            <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
            <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
            <option value="Samoa">Samoa</option> 
            <option value="San Marino">San Marino</option> 
            <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
            <option value="Saudi Arabia">Saudi Arabia</option> 
            <option value="Senegal">Senegal</option> 
            <option value="Serbia and Montenegro">Serbia and Montenegro</option> 
            <option value="Seychelles">Seychelles</option> 
            <option value="Sierra Leone">Sierra Leone</option> 
            <option value="Singapore">Singapore</option> 
            <option value="Slovakia">Slovakia</option> 
            <option value="Slovenia">Slovenia</option> 
            <option value="Solomon Islands">Solomon Islands</option> 
            <option value="Somalia">Somalia</option> 
            <option value="South Africa">South Africa</option> 
            <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
            <option value="Spain">Spain</option> 
            <option value="Sri Lanka">Sri Lanka</option> 
            <option value="Sudan">Sudan</option> 
            <option value="Suriname">Suriname</option> 
            <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
            <option value="Swaziland">Swaziland</option> 
            <option value="Sweden">Sweden</option> 
            <option value="Switzerland">Switzerland</option> 
            <option value="Syrian Arab Republic">Syrian Arab Republic</option> 
            <option value="Taiwan, Province of China">Taiwan, Province of China</option> 
            <option value="Tajikistan">Tajikistan</option> 
            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
            <option value="Thailand">Thailand</option> 
            <option value="Timor-leste">Timor-leste</option> 
            <option value="Togo">Togo</option> 
            <option value="Tokelau">Tokelau</option> 
            <option value="Tonga">Tonga</option> 
            <option value="Trinidad and Tobago">Trinidad and Tobago</option> 
            <option value="Tunisia">Tunisia</option> 
            <option value="Turkey">Turkey</option> 
            <option value="Turkmenistan">Turkmenistan</option> 
            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
            <option value="Tuvalu">Tuvalu</option> 
            <option value="Uganda">Uganda</option> 
            <option value="Ukraine">Ukraine</option> 
            <option value="United Arab Emirates">United Arab Emirates</option> 
            <option value="United Kingdom">United Kingdom</option> 
            <option value="United States">United States</option> 
            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
            <option value="Uruguay">Uruguay</option> 
            <option value="Uzbekistan">Uzbekistan</option> 
            <option value="Vanuatu">Vanuatu</option> 
            <option value="Venezuela">Venezuela</option> 
            <option value="Viet Nam">Viet Nam</option> 
            <option value="Virgin Islands, British">Virgin Islands, British</option> 
            <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
            <option value="Wallis and Futuna">Wallis and Futuna</option> 
            <option value="Western Sahara">Western Sahara</option> 
            <option value="Yemen">Yemen</option> 
            <option value="Zambia">Zambia</option> 
            <option value="Zimbabwe">Zimbabwe</option>
        </select>
        <asp:Image ID="img_Country" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
    </p>
</asp:Panel>
<asp:Panel ID="_PanelState" runat="server" Visible="false" >	
	<p>
        <cc1:AutoCompleteExtender ID="AutoCompleteExtenderState"                                   
        TargetControlID="stateName"
        ServicePath="../RegionService.asmx"
        FirstRowSelected="false"
        OnClientShown="setPositionState"   
        OnClientItemSelected="SetChild"                                           
        ServiceMethod="GetSuggestions" 
        MinimumPrefixLength="1"
        CompletionSetCount="10"
        EnableCaching="true"
        CompletionListElementID="Panel_state"
        CompletionInterval="10"
        CompletionListCssClass="completionListElement"
        CompletionListHighlightedItemCssClass="itemHighlighted"                                            
        CompletionListItemCssClass="listItem"   
        UseContextKey="true"
        runat="server">                                            
        </cc1:AutoCompleteExtender>
		<label for="stateName" runat="server" id="lblState">State</label>
		<asp:TextBox runat="server" ID="stateName" CssClass="form-text" TabIndex="10" autocomplete="off" onblur="javascript:CheckMatch(this, GetLblStateError(), GetFieldValidatorState());"></asp:TextBox>
		<asp:Image ID="img_State" runat="server" ImageUrl="../images/icon-q.png" CssClass="icon-q" />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="stateName" Display="Dynamic" CssClass="error-msg"
        ID="RequiredFieldValidatorState" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
        <asp:Label ID="lbl_errorState" runat="server" style="display:none;" Text="Choose state from the list" CssClass="error-msg"></asp:Label>
        <asp:HiddenField ID="hf_stateGuid" runat="server" />
    </p>
</asp:Panel>
<asp:Panel ID="_PanelCity" runat="server" >
    <asps:UpdatePanel ID="_UpdatePanelCity" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
		    <p>
                <cc1:AutoCompleteExtender ID="AutoCompleteExtenderCity" 
                TargetControlID="cityName"
                ServicePath="../RegionService.asmx"
                FirstRowSelected="false"
                OnClientShown="setPosition" 
                OnClientItemSelected="SetChild"
                UseContextKey="true"
                ServiceMethod="GetSuggestions" 
                MinimumPrefixLength="1"
                CompletionSetCount="10"
                EnableCaching="false"
                CompletionListElementID="Panel_showCity"
                CompletionInterval="10"
                CompletionListCssClass="completionListElement"
                CompletionListHighlightedItemCssClass="itemHighlighted"                                            
                CompletionListItemCssClass="listItem"   
                runat="server">                                            
                </cc1:AutoCompleteExtender>
			    <label for="cityName" runat="server" id="lbl_city">City</label>	
			    <asp:TextBox runat="server" ID="cityName" CssClass="form-text" TabIndex="11"
			    autocomplete="off" onblur="javascript:CheckMatch(this, GetLblCityError(), GetFieldValidatorCity());"></asp:TextBox>
			    <asp:Image ID="img_City" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
			    <%/*onblur="javascript:SendRequestMessage(this, GetLblCityError());" onblur="javascript:CheckIfmatch(this, GetLblCityError(), GetFieldValidatorCity());"	   */ %>
			    <asp:RequiredFieldValidator runat="server" ControlToValidate="cityName" Display="Dynamic" CssClass="error-msg" 
			    ID="RequiredFieldValidatorAddress" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>	
                <asp:Label ID="lbl_errorCity" runat="server" style="display:none;" Text="Choose city from the list" CssClass="error-msg"></asp:Label>
                <asp:HiddenField ID="hf_cityGuid" runat="server" />
		    </p>
		</ContentTemplate>
    </asps:UpdatePanel>
</asp:Panel>
<asp:Panel ID="Panel_showCity" runat="server">
</asp:Panel>
<asp:Panel ID="Panel_state" runat="server">
</asp:Panel>
<asp:Panel ID="Panel_district" runat="server">
</asp:Panel>
<asp:Panel ID="Panel_street" runat="server">
</asp:Panel>
<asp:Panel ID="_PanelDistrict" runat="server" >
    <p>
		<label for="form-street" runat="server" id="lbl_district">District</label>
		<asp:TextBox runat="server" ID="txt_District" CssClass="form-text" TabIndex="12" onblur="javascript:CheckMatch(this, GetLblDistrictError(), GetFieldValidatorDistrict());"
		autocomplete="off"></asp:TextBox>
		<asp:Image ID="img_District" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="txt_District" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidatorDistrict" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>	
        <asp:Label ID="lbl_errorDistrict" runat="server" style="display:none;" Text="Choose district from the list" CssClass="error-msg"></asp:Label>
	</p>
	
    <cc1:AutoCompleteExtender ID="AutoCompleteExtenderDistrict" 
    UseContextKey="true"
    TargetControlID="txt_District"
    ServicePath="../RegionService.asmx"
    ServiceMethod="GetSuggestions" 
    OnClientShown="setPositionDistrict"
    OnClientItemSelected="SetChild" 
    MinimumPrefixLength="1"
    CompletionSetCount="10"
    CompletionListElementID="Panel_district"
    EnableCaching="false"
    CompletionInterval="10"
    CompletionListCssClass="completionList"
    CompletionListHighlightedItemCssClass="itemHighlighted"
    CompletionListItemCssClass="listItem"      
    runat="server">
    </cc1:AutoCompleteExtender>
    
    <asp:HiddenField ID="hf_districtGuid" runat="server" />
</asp:Panel>

<asp:Panel ID="_PanelStreet" runat="server" >
    <p>
		<label for="streetName" runat="server" id="lbl_street">Street</label>
		<asp:TextBox runat="server" ID="streetName" CssClass="form-text" TabIndex="13" onblur="javascript:CheckMatch(this, GetLblStreetError(), GetFieldValidatorStreet());"
		autocomplete="off"></asp:TextBox>
		<asp:Image ID="img_Street" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="streetName" Display="Dynamic"
         CssClass="error-msg" ID="RequiredFieldValidatorStreet" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>			
        <asp:Label ID="lbl_errorStreet" runat="server" style="display:none;" Text="Choose street from the list" CssClass="error-msg"></asp:Label>
        <cc1:AutoCompleteExtender ID="AutoCompleteExtenderStreet" 
        UseContextKey="true"
        TargetControlID="streetName"
        ServicePath="../RegionService.asmx"
        ServiceMethod="GetSuggestions" 
        OnClientShown="setPositionStreet" 
        OnClientItemSelected="SetChild"
        MinimumPrefixLength="1"
        CompletionSetCount="10"
        CompletionListElementID="Panel_street"
        EnableCaching="false"
        CompletionInterval="10"
        CompletionListCssClass="completionList"
        CompletionListHighlightedItemCssClass="itemHighlighted"
        CompletionListItemCssClass="listItem"      
        runat="server">

        </cc1:AutoCompleteExtender>
        <asp:HiddenField ID="hf_StreetGuid" runat="server" />
	</p>	
</asp:Panel>
<asp:Panel ID="_PanelHouseNum" runat="server" >       
    <p>
		<label for="houseNum" runat="server" id="lbl_house">House Number</label>
		<asp:TextBox runat="server" ID="houseNum" CssClass="form-text" TabIndex="14" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
		<asp:Image ID="img_HouseNum" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
        <asp:RequiredFieldValidator runat="server" ControlToValidate="houseNum" Display="Dynamic" CssClass="error-msg" ID="RequiredFieldValidator2" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>			
	</p>	
</asp:Panel>
<asp:Panel ID="_PanelZipCode" runat="server" >              
	<p>
		<label for="zip"  runat="server" id="lbl_zip">Zip Code</label>
		<asp:TextBox runat="server" ID="zip" CssClass="form-text" TabIndex="15" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
		<asp:Image ID="img_ZipCode" runat="server" ImageUrl="../images/icon-q.png" CssClass="icon-q" />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="zip" Display="Dynamic" CssClass="error-msg" ID="RequiredFieldValidatorZip" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidatorZipCode" Display="Dynamic" runat="server" ValidationGroup="detail" ControlToValidate="zip" ValidationExpression="[0-9]+">Invalid</asp:RegularExpressionValidator>
    </p>		
</asp:Panel>
<asp:Panel ID="_PanelWebSite" runat="server" >      	       
    <p>
        <label for="form-webSite" runat="server" id="lbl_website">Web Site</label>
        <asp:TextBox runat="server" ID="site" CssClass="form-text" TabIndex="16" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_Website" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
        <asp:RegularExpressionValidator ID="RegularExpressionValidatorWesSite" CssClass="error-msg" Display="Dynamic" runat="server" ValidationGroup="detail" ControlToValidate="site" 
        ValidationExpression="^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$">Invalid</asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="site" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidatorWebSite" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
    </p>        
</asp:Panel>
    <asp:Panel ID="_licenseNumber" runat="server" >      
    <p>
		<label for="txt_licenseNumber" runat="server" id="lbl_licenseNumber">License Number</label>			
		<asp:TextBox runat="server" ID="txt_licenseNumber" CssClass="form-text" AutoPostBack="false" TabIndex="18" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
		<asp:Image ID="img_licenseNumber" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
			
               
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_licenseNumber" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_licenseNumber" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>   
	</p>
</asp:Panel>
     <asp:Panel ID="_PanelReferralCode" runat="server" >      
    <p>
		<label for="txt_ReferralCode" runat="server" id="lbl_ReferralCode">Referral Code</label>			
		<asp:TextBox runat="server" ID="txt_ReferralCode" CssClass="form-text" AutoPostBack="false"
             TabIndex="19" onblur="javascript:chkOneBox(this);" autocomplete="off" ReadOnly="true"></asp:TextBox>
		<asp:Image ID="img_ReferralCode" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
			
               
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_licenseNumber" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_ReferralCode" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>   
	</p>
</asp:Panel>
<asp:Panel ID="_PanelPhone" runat="server" >      
    <p>
		<label for="phone" runat="server" id="lbl_phone">Other phone</label>			
		<asp:TextBox runat="server" ID="phone" CssClass="form-text" AutoPostBack="false" TabIndex="20" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
		<asp:Image ID="img_phone" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
			
		<asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" CssClass="error-msg"
        runat="server" ErrorMessage="The correct format is" Display="Dynamic"
        ValidationGroup="detail" ControlToValidate="phone"
        ValidationExpression="\d+"></asp:RegularExpressionValidator>
               
        <asp:RequiredFieldValidator runat="server" ControlToValidate="phone" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidatorPhone2" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>   
	</p>
</asp:Panel>
<asp:Panel ID="_PanelEmployees" runat="server" >          
    <p>
        <label for="txt_employee" runat="server" id="lbl_Employee">Number of employees</label>
        <asp:TextBox runat="server" ID="txt_employee" CssClass="form-text" TabIndex="21" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_NumEmployees" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
            
        <asp:RegularExpressionValidator ID="RegularExpressionValidatorEnployees" CssClass="error-msg"
        runat="server" ErrorMessage="Invalid" Display="Dynamic"
        ValidationGroup="detail" ControlToValidate="txt_employee"
        ValidationExpression="\d+"></asp:RegularExpressionValidator>

        <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_employee" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidatorEmployee" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
    </p>       
</asp:Panel>   
<asp:Panel ID="_PanelRecord" runat="server" >  
    <p>
        <label for="form-webSite" runat="server" id="lbl_Record">Please record my calls</label>
        <asp:CheckBox ID="_CheckBoxRecord" runat="server" />
        <asp:Image ID="img_Record" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
    </p>
</asp:Panel>
<asp:Panel ID="_PanelSendSms" runat="server" >  
    <p>
        <label for="form-webSite" runat="server" id="lbl_SendSms">To Send SMS</label>
        <asp:CheckBox ID="cb_SendSms" runat="server" />
        <asp:Image ID="img_SendSms" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
    </p>
</asp:Panel>
   
<asp:Panel ID="_PanelMoveToPPC" runat="server" Visible="false" >          
    <p>
        <label runat="server" id="lbl_MoveTo">Move to</label>
        <asp:DropDownList ID="ddl_MoveTo" runat="server" CssClass="form-select">
        </asp:DropDownList>
    </p>
</asp:Panel>
     
<asp:Panel ID="Panel_Ranking" runat="server" CssClass="must" >          
    <p>
        <label for="form-webSite" runat="server" id="lbl_Ranking">Publisher ranking</label>
        <asp:TextBox runat="server" ID="txt_Ranking" CssClass="form-text" TabIndex="22"
        onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
            
        <asp:RangeValidator ID="RangeValidatorRanking" CssClass="error-msg" runat="server" ErrorMessage="Range 0-100" 
        MaximumValue="100" MinimumValue="0" Type="Integer" ControlToValidate="txt_Ranking"
        ValidationGroup="detail" Display="Dynamic"></asp:RangeValidator>           
               
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Ranking" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidatorRanking" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
    </p>
</asp:Panel> 
 
<asp:Panel ID="_PanelCustom1" runat="server" >          
    <p>
        <label for="txt_Custom1" runat="server" id="lbl_Custom1">Custom1</label>
        <asp:TextBox runat="server" ID="txt_Custom1" CssClass="form-text" TabIndex="23" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_Custom1" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
        
         <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Custom1" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_Custom1" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
    </p>
</asp:Panel>

<asp:Panel ID="_PanelCustom2" runat="server" >          
    <p>
        <label for="txt_Custom2" runat="server" id="lbl_Custom2">Custom2</label>
        <asp:TextBox runat="server" ID="txt_Custom2" CssClass="form-text" TabIndex="24" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_Custom2" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
        
         <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Custom2" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_Custom2" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
    </p>
</asp:Panel>

<asp:Panel ID="_PanelCustom3" runat="server" >          
    <p>
        <label for="txt_Custom3" runat="server" id="lbl_Custom3">Custom3</label>
        <asp:TextBox runat="server" ID="txt_Custom3" CssClass="form-text" TabIndex="25" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_Custom3" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
        
         <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Custom3" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_Custom3" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
    </p>
</asp:Panel>

<asp:Panel ID="_PanelCustom4" runat="server" >          
    <p>
        <label for="txt_Custom4" runat="server" id="lbl_Custom4">Custom4</label>
        <asp:TextBox runat="server" ID="txt_Custom4" CssClass="form-text" TabIndex="26" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_Custom4" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
        
         <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Custom4" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_Custom4" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
    </p>
</asp:Panel>

<asp:Panel ID="_PanelCustom5" runat="server" >          
    <p>
        <label for="txt_Custom5" runat="server" id="lbl_Custom5">Custom5</label>
        <asp:TextBox runat="server" ID="txt_Custom5" CssClass="form-text" TabIndex="27" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
        <asp:Image ID="img_Custom5" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
        
         <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Custom5" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_Custom5" ValidationGroup="detail" >Missing</asp:RequiredFieldValidator>
    </p>
</asp:Panel> 
<asp:Panel ID="_PanelBizId" runat="server" >
    <p>
        <label for="txt_BizId" runat="server" id="lbl_BizId">Biz Id</label>
        <asp:TextBox runat="server" ID="txt_BizId" CssClass="form-text" TabIndex="28"></asp:TextBox>   
        
         
    </p>          
</asp:Panel>

<asp:Panel ID="_PanelPassword" runat="server" >          
    <p>
        <label for="txt_password" runat="server" id="lbl_password">Password</label>
        <asp:TextBox runat="server" ID="txt_password" CssClass="form-text" TabIndex="29" 
         ReadOnly="true"></asp:TextBox>   
        
         
    </p>
</asp:Panel>

<asp:Panel ID="_PanelStatus" runat="server" >          
    <p>
        <label for="txt_Status" runat="server" id="lbl_Status">Status</label>
        <asp:TextBox runat="server" ID="txt_Status" CssClass="form-text" 
         ReadOnly="true"></asp:TextBox>   
        
         
    </p>
</asp:Panel>
   
<asp:Panel ID="_PanelFundsStatus" runat="server" Visible="false">          
    <p>
        <label for="ddl_FundsStatus" runat="server" id="lbl_FundsStatus">Funds status</label>
        <asp:DropDownList ID="ddl_FundsStatus" runat="server" CssClass="form-select">
        </asp:DropDownList>         
         
    </p>
</asp:Panel>


<asp:Panel ID="_PanelAccountManager" runat="server" >          
    <p>
        <label for="ddl_AccountManager" id="lblAccountManager">
            <asp:Label ID="lbl_AccountManager" runat="server" Text="Account Manager"></asp:Label>
        </label>
        <asp:DropDownList ID="ddl_AccountManager" runat="server" TabIndex="30" CssClass="form-select">
        </asp:DropDownList>
        <asp:Label ID="lbl_MustSelectAccountManager" runat="server" Text="Missing" style="display:none;" CssClass="error-msg"></asp:Label>    
    </p>
</asp:Panel>

<asp:Panel ID="_PanelDateOfBirth" CssClass="PanelDateOfBirth" runat="server" >          
    <p>
        <label for="txt_DateOfBirth" runat="server" id="lbl_DateOfBirth">Date of birth</label>
        <asp:TextBox runat="server" ID="txt_DateOfBirth" CssClass="form-text" TabIndex="31" onblur="javascript:chkOneBox(this);" 
            autocomplete="off" placeholder="MM/DD/YYYY" ReadOnly="true"></asp:TextBox>
        <asp:Image ID="img_DateOfBirth" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
        
         <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_DateOfBirth" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_DateOfBirth" ValidationGroup="detail" Visible="false">Missing</asp:RequiredFieldValidator>
        <asp:HiddenField ID="hf_DateOfBirth" runat="server" />
    </p>
</asp:Panel>
<div>
    <asp:Panel ID="_PanelBusinessType" runat="server" >      	       
    <p>
        <label for="ddl_BusinessType" runat="server" id="lbl_BusinessType">Business type</label>
         <asp:DropDownList ID="ddl_BusinessType" runat="server" TabIndex="51" CssClass="form-select">
        </asp:DropDownList>
        <asp:Image ID="img_BusinessType" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
        
    </p>        
</asp:Panel>
    <asp:Panel ID="_PanelEin" runat="server" >          
    <p>
        <label for="txt_ein" runat="server" id="lbl_ein">EIN</label>
        <asp:TextBox runat="server" ID="txt_ein" CssClass="form-text" TabIndex="52" onblur="javascript:chkOneBox(this);" 
            autocomplete="off" MaxLength="9" placeholder="123456789"></asp:TextBox>
        <asp:Image ID="img_ein" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
            
        <asp:RegularExpressionValidator ID="RegularExpressionValidator_ein" CssClass="error-msg"
        runat="server" ErrorMessage="Invalid" Display="Dynamic"
        ValidationGroup="detail" ControlToValidate="txt_ein"
        ValidationExpression="^[1-9]\d{8}$"></asp:RegularExpressionValidator>

        <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_ein" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_ein" ValidationGroup="detail" Visible="false">Missing</asp:RequiredFieldValidator>
    </p>       
</asp:Panel>   
     <asp:Panel ID="_PanelSocialSecurityNumber" runat="server" >          
    <p>
        <label for="txt_SocialSecurityNumber" runat="server" id="lbl_SocialSecurityNumber">Social Security num (last 4 digits)</label>
        <asp:TextBox runat="server" ID="txt_SocialSecurityNumber" CssClass="form-text" placeholder="9999"
            TabIndex="53" onblur="javascript:chkOneBox(this);" autocomplete="off" MaxLength="4"></asp:TextBox>
        <asp:Image ID="img_SocialSecurityNumber" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
            
        <asp:RegularExpressionValidator ID="RegularExpressionValidator_SocialSecurityNumber" CssClass="error-msg"
        runat="server" ErrorMessage="Invalid" Display="Dynamic"
        ValidationGroup="detail" ControlToValidate="txt_SocialSecurityNumber"
        ValidationExpression="^\d{4}$"></asp:RegularExpressionValidator>

        <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_SocialSecurityNumber" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_SocialSecurityNumber" ValidationGroup="detail" Visible="false">Missing</asp:RequiredFieldValidator>
    </p>       
</asp:Panel>   
     <asp:Panel ID="_PanelBankRoutingNumber" runat="server" >          
    <p>
        <label for="txt_BankRoutingNumbe" runat="server" id="lbl_BankRoutingNumbe">Bank Routing number</label>
        <asp:TextBox runat="server" ID="txt_BankRoutingNumbe" CssClass="form-text"  placeholder="111000000"
            TabIndex="54" onblur="javascript:chkOneBox(this);" autocomplete="off" MaxLength="9"></asp:TextBox>
        <asp:Image ID="img_BankRoutingNumbe" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
            
        <asp:RegularExpressionValidator ID="RegularExpressionValidator_BankRoutingNumbe" CssClass="error-msg"
        runat="server" ErrorMessage="Invalid" Display="Dynamic"
        ValidationGroup="detail" ControlToValidate="txt_BankRoutingNumbe"
        ValidationExpression="^\d{9}$"></asp:RegularExpressionValidator>

        <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_BankRoutingNumbe" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_BankRoutingNumbe" ValidationGroup="detail" Visible="false">Missing</asp:RequiredFieldValidator>
    </p>       
</asp:Panel>   
    <asp:Panel ID="PanelBankAccountNumber" runat="server" >          
    <p>
        <label for="txt_BankAccountNumber" runat="server" id="lbl_BankAccountNumber">Bank Account number</label>
        <asp:TextBox runat="server" ID="txt_BankAccountNumber" CssClass="form-text" 
            TabIndex="55" onblur="javascript:chkOneBox(this);" autocomplete="off" MaxLength="17"></asp:TextBox>
        <asp:Image ID="img_BankAccountNumber" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q"  />
            
        <asp:RegularExpressionValidator ID="RegularExpressionValidator_BankAccountNumber" CssClass="error-msg"
        runat="server" ErrorMessage="Invalid" Display="Dynamic"
        ValidationGroup="detail" ControlToValidate="txt_BankAccountNumber"
        ValidationExpression="^\d{4,17}$"></asp:RegularExpressionValidator>
        
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_BankAccountNumber" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_BankAccountNumber" ValidationGroup="detail" Visible="false" >Missing</asp:RequiredFieldValidator>
    </p>       
</asp:Panel>   
    
</div>


<asp:Button runat="server" CssClass="form-submit" ID="btnSend"  
    ValidationGroup="detail" OnClientClick="chkAll(); return false;" TabIndex="31" 
      />        

<p class="steps"  runat="server" id="pSteps">
    <asp:Label ID="lbl_step" runat="server" Text="STEP 1 OUT"></asp:Label>
    <asp:Label ID="lbl_page" runat="server" ></asp:Label>
    <img src="../images/bullet.gif" alt="bullet" />
</p>

<div id="unvisibleControls">
    <asp:Label ID="lbl_emailExist" runat="server" Text="The email already exist" Visible="false"></asp:Label>
    <asp:Label ID="lbl_phoneExists" runat="server" Text="The phone already exist" Visible="false"></asp:Label>
</div>

<asp:Label ID="lbl_NEXT" runat="server" Text="NEXT" Visible="false"></asp:Label>
<asp:Label ID="lbl_UPDATE" runat="server" Text="UPDATE" Visible="false"></asp:Label>    





<asp:HiddenField ID="hf_HasChanges" runat="server"/>


</form>
</body>
</html>
