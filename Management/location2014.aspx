﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="location2014.aspx.cs" enableEventValidation="false" Inherits="Management_location2014" %>
<%@ Register Src="~/Controls/MapControlMulti.ascx" TagName="map" TagPrefix="ma" %>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="//code.jquery.com/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../PPC/script/jquery-ui-1.8.16.custom.effect.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../PlaceHolder.js"></script>
    <script type="text/javascript" src="../ppc/script/jquery-ui-1.8.16.custom.autocomplete.min.js"></script>

     <script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&amp;sensor=false&language=en"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyByICNsdF00OXlUGTnzGsIfNmk0Mo7xqcw&sensor=false&libraries=places&language=en"></script>
    <script type="text/javascript" src="../ppc/script/JSGeneral.js"></script>
    <script type="text/javascript" src="../ppc/ppc.js"></script>
    <script type="text/javascript" src="../CallService.js"></script>
    <script type="text/javascript" src="../general.js"></script>
    <link href="../PPC/samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>
    <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
    <script type="text/javascript">    try { Typekit.load(); } catch (e) { }</script>
    <link href="../Ladda-master/dist/ladda.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Ladda-master/dist/spin.min.js"></script>
    <script type="text/javascript" src="../Ladda-master/dist/ladda.min.js"></script>

    <script type="text/javascript">
        function UpdateSuccess() {            
            SetServerComment2("<%#UpdateSuccess%>");
        }

        function UpdateFailed() {
            SetServerComment2("<%#UpdateFaild%>");

        }
        
        function CheckLocationDetails() {
            var index;
            var boolValid = true;

            $('input[id^="searchTextField"]').each(function () {

                index = $(this).attr('id').charAt($(this).attr('id').length - 1);

                if ($(this).siblings('.inputValid').css('display') == 'none') {
                    $(this).siblings('.inputComment').css('display', 'block');
                    SetServerComment("<%# _map.SelectFromList %>");
                    boolValid = false;
                }
            }
            )


            if (!boolValid)
                return false;

            activeButtonAnim('.location #linkSetDetails', 200);

            GetAllDetailsMulti('<%#urlSuccees%>', "<%#GetServerError%>",false);

            /*
            var LPlace = GetSelectLargeAreaId();
            var FullAddress = GetFullAddress();
            if (FullAddress.length == 0 || LPlace == "false") {
            var _comment = (!LPlace) ? "<# _map.ChooseLargerArea %>" : "<# _map.SelectFromList %>";
            SetServerComment(_comment);
            SetInvalidLocation();
            return false;
            }
            */

            /*
            var _details = GetAllDetailsMulti();
            if (_details.length == 0) {
            SetServerComment("<# _map.SelectFromList %>");
            ////SetInvalidLocation();
            return false;
            }

            document.getElementById("<# hf_lat.ClientID %>").value = _details.lat_lng.lat();
            document.getElementById("<# hf_lng.ClientID %>").value = _details.lat_lng.lng();
            document.getElementById("<# hf_radius.ClientID %>").value = (_details.radius * 1.6);
            document.getElementById("<# hf_LargePlace.ClientID %>").value = _details.AreaId;
            document.getElementById("<# hf_FullAddress.ClientID %>").value = _details.FullAddress;
            */


            ////parent.showDiv();
            return true;
        }

        function SendSucceed() {
            stopActiveButtonAnim();
            UpdateSuccess();
        }

        function SendFaild() {
            UpdateFailed();
            stopActiveButtonAnim();
        }

        
        function witchAlreadyStep(level, mode) {
            //alert(level + " " + mode); 
            //level="0";   
            parent.window.witchAlreadyStep(level);
            level = level * 1;
            if (mode == "init") {
                if (level != 6 && level != 0)
                    parent.window.setInitRegistrationStep(level);
                else
                    parent.window.setLinkStepActive('linkStep1');

            }
            else
                parent.window.setLinkStepActive('linkStep1');
        }

        function init(sender, args) {
            try {
                //alert("init");
                parent.window.setLinkStepActive("linkStep3");
                parent.hideDiv();               
            } catch (ex) { }
        }       

        window.onload = init;
    </script>

    <script type="text/javascript">
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-21441721-1']);
          _gaq.push(['_setDomainName', 'noproblemppc.com']);
          _gaq.push(['_trackPageview']);

          (function () {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
    </script>

</head>
<body class="bodyDashboard">
    <form id="form1" runat="server">
    <div class="location locationDashboard dashboardIframe">
        <uc1:PpcComment runat="server" ID="PpcComment1" /> 
        <div class="titleFirst">
            <span>Cover area</span>
        </div> 
        <uc1:PpcComment runat="server" ID="_comment" />

        <ma:map ID="_map" runat="server" />
        
        <div class="clear"></div>

        <div id="containerNext" class="containerNext">
                <section class="progress-demo">               

                 <a id="linkSetDetails" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" onclick="javascript:return CheckLocationDetails();">
                  <span class="ladda-label">Update</span><span class="ladda-spinner">
                  </span><div class="ladda-progress" style="width: 138px;"></div>
                 </a>

                </section>      
         </div>  
         
         
        <asp:HiddenField ID="hf_lat" runat="server" />
        <asp:HiddenField ID="hf_lng" runat="server" />
        <asp:HiddenField ID="hf_radius" runat="server" />
        <asp:HiddenField ID="hf_LargePlace" runat="server" />
        <asp:HiddenField ID="hf_FullAddress" runat="server" /> 

        <asp:HiddenField ID="HiddenPlaceDetails" runat="server" /> 
        

    </div>
    </form>
</body>
</html>
