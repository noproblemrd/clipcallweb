﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Management_MyBalance : PageSetting
{
    const string MY_BALANCE = "MyBalance";
    const string REPORT_NAME = "Balance";
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);       
        if (userManagement.IsPublisher())
            this.MasterPageFile = "../Publisher/MasterPageFrame.master";
        else if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
            this.MasterPageFile = ResolveUrl("~") + "Controls/MasterPagePPC.master";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager ScriptManager1 = ScriptManager.GetCurrent(this);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        SetToolboxEvents();
        if (!IsPostBack)
        {
            dataViewV = null;
            SetToolbox();
            LoadAuctions();
        }
    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lblTitleMyBallance.Text);
    }
    void SetToolboxEvents()
    {
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
    }

    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordToPrint) + "');", true);
            return;
        }


        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordExport) + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, REPORT_NAME);
        if (!te.ExecExcel(GetExcelTable(dataV)))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    DataTable GetExcelTable(DataTable data)
    {
        Dictionary<eYesNo, string> dic = new Dictionary<eYesNo, string>();
        foreach (eYesNo s in Enum.GetValues(typeof(eYesNo)))
        {
            string tran = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", s.ToString());
            dic.Add(s, tran);
        }
        DataTable NewData = data.Copy();
        NewData.Columns.Remove("_Balance");
        NewData.Columns.Remove("_Amount");
        NewData.Columns.Remove("css_amount");
        NewData.Columns.Remove("css_balance");
        
        return NewData;

    }
    private void LoadAuctions()
    {
        ddl_auction.Items.Add(new ListItem(lbl_all.Text, "All")); 
        foreach (string s in Enum.GetNames(typeof(WebReferenceReports.Action)))
        {
            string tran = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "Action", s);
            ddl_auction.Items.Add(new ListItem(tran, s));
        }
    }

    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        DateTime dt_from = FromToDate1.GetDateFrom;
        DateTime dt_to = FromToDate1.GetDateTo;
        if (dt_from == DateTime.MinValue || dt_to == DateTime.MinValue)
        {
            dt_from = DateTime.Now.AddMonths(-1);
            dt_to = DateTime.Now;
        }

        DataTable data = new DataTable();
        data.Columns.Add("Action", typeof(string));
        data.Columns.Add("Date", typeof(DateTime));
        data.Columns.Add("Amount", typeof(string));
        data.Columns.Add("_Amount", typeof(decimal));
        data.Columns.Add("Balance", typeof(string));
        data.Columns.Add("_Balance", typeof(decimal));
        data.Columns.Add("CreatedOn", typeof(string));
        data.Columns.Add("css_amount", typeof(string));
        data.Columns.Add("css_balance", typeof(string));
        //for sorting
        Dictionary<string, bool> dic = new Dictionary<string, bool>();
        dic.Add("Action", true);
        dic.Add("_Amount", true);
        dic.Add("_Balance", true);
        dic.Add("Date", false);
        listAsc = dic;

        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.BalanceReportRequest _request = new WebReferenceReports.BalanceReportRequest();
        _request.FromDate = dt_from;
        _request.ToDate = dt_to;
        _request.SupplierId = new Guid(GetGuidSetting());
        WebReferenceReports.ResultOfBalanceReportResponse _result = null;
        try
        {
            _result = _reports.BalanceReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (_result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
   //     string page_name = System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        /*
_result.Value.BalanceRows[0].Action
_result.Value.BalanceRows[0].Amount
_result.Value.BalanceRows[0].Balance
_result.Value.BalanceRows[0].CreatedOn
         * */
        foreach (WebReferenceReports.BalanceRowData brd in _result.Value.BalanceRows)
        {

            DataRow row = data.NewRow();
            row["Action"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "Action", brd.Action.ToString());
            double num = (double)brd.Amount;
            row["css_amount"] = (num == 0) ? "balance_num" : ((num > 0) ? "deposive_num" : "negative_num");
            row["Amount"] = siteSetting.CurrencySymbol + String.Format("{0:0.##}", brd.Amount);
            row["_Amount"] = brd.Amount;
            row["Date"] = brd.CreatedOn;
            row["CreatedOn"] = string.Format(siteSetting.DateFormat, brd.CreatedOn) +
                " " + string.Format(siteSetting.TimeFormat, brd.CreatedOn);
            num = (double)brd.Balance;
            row["Balance"] = siteSetting.CurrencySymbol + String.Format("{0:0.##}", brd.Balance);
            row["_Balance"] = brd.Balance;
            row["css_balance"] = (num == 0) ? "balance_num" : ((num > 0) ? "deposive_num" : "negative_num");

            data.Rows.Add(row);            
        }
        _GridView.PageIndex = 0;
        data.TableName = MY_BALANCE;
        

        if (ddl_auction.SelectedValue != "All")
        {
            EnumerableRowCollection<DataRow> query = from MyBalance in data.AsEnumerable()
                                                     where MyBalance.Field<string>("Action") == ddl_auction.SelectedItem.Text
                                                  //      ddl_auction.SelectedValue                                                     
                                                     select MyBalance;
            dataViewV = (query.Count() == 0) ? null : query.CopyToDataTable();
            data = dataViewV;
        }
        else
            dataViewV = data;
        dataV = data;
        _GridView.DataSource = dataViewV;
        _GridView.DataBind();
        _uPanel.Update();
        if (_GridView.Rows.Count == 0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + lbl_NoResult.Text + "');", true);
    }
    protected void _GridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable data = dataV;
      //  DataView d;
        
        bool IsAscending = listAsc[e.SortExpression];
        listAsc[e.SortExpression] = (!listAsc[e.SortExpression]);
        e.SortDirection = (IsAscending) ? SortDirection.Descending : SortDirection.Ascending;        
        EnumerableRowCollection<DataRow> query;
        if(IsAscending)
            query = from MyBalance in data.AsEnumerable()
                    orderby MyBalance.Field<object>(e.SortExpression)
                    select MyBalance;
        else
            query = from MyBalance in data.AsEnumerable()
                    orderby MyBalance.Field<object>(e.SortExpression)
                    descending
                    select MyBalance;

        _GridView.DataSource = (query.Count() == 0) ? null : query.AsDataView();
        _GridView.DataBind();
        _uPanel.Update();
        dataViewV = (query.Count() == 0) ? null : query.CopyToDataTable();      
         
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataSource = dataViewV;
        _GridView.DataBind();
        _uPanel.Update();
    }
    
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    DataTable dataViewV
    {
        get { return (Session["data_view"] == null) ? new DataTable() : (DataTable)Session["data_view"]; }
        set { Session["data_view"] = value; }
    }
    Dictionary<string, bool> listAsc
    {
        get { return (ViewState["listAsc"] == null) ? new Dictionary<string, bool>() : (Dictionary<string, bool>)ViewState["listAsc"]; }
        set { ViewState["listAsc"] = value; }
    }

    
}
