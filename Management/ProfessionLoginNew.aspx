<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Management/MasterPagePreAdvertiser.master"
CodeFile="ProfessionLoginNew.aspx.cs" Inherits="Management_ProfessionLoginNew" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
 <script type="text/javascript" src="../general.js"></script>
 <script type="text/javascript" language="javascript">
     function SendPassword(e)
     {
     
        if(e.keyCode == 13)
        {           
            if(SendPasswordBtn())
                <%# Get_Password %>;
            return false;
        }       
         return true;
     }
     function SendPasswordBtn()
     {
  //      return true;
        if(Page_ClientValidate('send'))
        {            
        
            var _term=document.getElementById("<%# cb_TermNCondition.ClientID %>");
            if(_term)
            {
                if(!_term.checked)
                {
                    alert("<%# hf_condition.Value %>");
                    return false;
                }
            }
            
            
            
            MakeDisabled();
            top.showDiv();
            return true;
                        
        }
        return false;
     }
     function MakeDisabled()
     {          

            document.getElementById("<%# txt_professionalPelephone.ClientID %>").removeAttribute("onkeypress");
    //        document.getElementById("<%# btnSendForPassword.ClientID %>").disabled="disabled";         
       
     }
     function FreeDisabled()
     {
            document.getElementById("<%# txt_professionalPelephone.ClientID %>").setAttribute("onkeypress", "return SendPassword(event);");
 //           document.getElementById("<%# btnSendForPassword.ClientID %>").removeAttribute("disabled");
     }
     function MakeLogin(e)
     {     
        var code;
        if (!e) var e = window.event;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;        
        if(code==13)
        {            
            <%# MakeLogin %>;
            return false;
        }
        return true;
   
     }
    window.onload=function(){top.hideDiv();}
     </script>
</asp:Content>
 
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" >
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>

<div id="primarycontent">
	<div class="form-login">
		<h2><asp:Label ID="lbl_title" runat="server">Advertiser Portal Registration Page</asp:Label></h2>
		<asp:Label runat="server" ID="lblComment"></asp:Label>
		<div class="login">
			<div class="form-field-wrap">
			    <div class="form-field">
				    <label for="form-email" runat="server" id="lbl_email">Your E-mail</label>
                    <asp:TextBox ID="txt_professionalEmail" runat="server" TabIndex="0" CssClass="form-text"></asp:TextBox>
                    <asp:Image ID="img_email" CssClass="icon-q" runat="server" ImageUrl="../images/icon-q.png" AlternateText="Insert your email" />  
                </div>
                <div class="error-msg-wrap">
				    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" CssClass="error-msg" runat="server" ErrorMessage="Missing" Display="Dynamic"
				    ControlToValidate="txt_professionalEmail" ValidationGroup="send"></asp:RequiredFieldValidator>  
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" CssClass="error-msg" runat="server" ErrorMessage="Invalid email" Display="Dynamic"
                    ControlToValidate="txt_professionalEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    ValidationGroup="send"></asp:RegularExpressionValidator>
                </div>
			</div>
			<div class="form-field-wrap">
			    <div class="form-field">
				    <label for="form-cellular" runat="server" id="lbl_cellNum">Your cellphone number</label>
                    <asp:TextBox ID="txt_professionalPelephone" runat="server" TabIndex="0" CssClass="form-text" onkeypress="return SendPassword(event);"></asp:TextBox>
                    <asp:Image ID="img_phone" CssClass="icon-q" runat="server" ImageUrl="../images/icon-q.png" AlternateText="Insert your mobile" /> 
                </div>
                <div class="error-msg-wrap">
				    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPhone" CssClass="error-msg" runat="server" ErrorMessage="Missing" Display="Dynamic" 
				    ControlToValidate="txt_professionalPelephone" ValidationGroup="send" ForeColor="red"></asp:RequiredFieldValidator>  
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" CssClass="error-msg" runat="server" 
                    ErrorMessage="Invalid phone number" ControlToValidate="txt_professionalPelephone" Display="Dynamic" 
                    ValidationExpression="\d+" ValidationGroup="send"></asp:RegularExpressionValidator>
                </div>
                
			</div>
			 <div class="form-checked1" runat="server" id="div_termNconditions">
                    <asp:CheckBox ID="cb_TermNCondition" runat="server" />
                    <a id="a_termNconditions" runat="server" href="javascript:void(0);" target="_blank">
                        <asp:Label ID="lbl_termNconditions" runat="server" Text="I agree to the Terms and Conditions"></asp:Label>
                    </a>
                </div>
                <div class="clear"></div>
                <%/*
                <div class="capcha">
                <telerik:RadCaptcha ID="_RadCaptcha" Runat="server" EnableRefreshImage="true" ValidationGroup="send" ProtectionMode="Captcha"  CaptchaImage-ImageCssClass="captchapict"   CaptchaTextBoxCssClass="form-text" CaptchaTextBoxLabelCssClass="captchalabel" >
               </telerik:RadCaptcha>
                </div>
                */ %>
                <div class="clear"></div>
                <div>
            <asp:Button ID="btnSendForPassword" runat="server" Text="Button" CssClass="loginbtn" validationgroup="send" 
                OnClientClick="return SendPasswordBtn();" OnClick="btnSendForPassword_click"/>
                </div>
			<div class="form-field-wrap">
			    <div class="form-field">
				    <label for="form-password" runat="server" id="lbl_password">Type received password</label>
				    <asp:TextBox ID="txtPassword" TextMode="Password" TabIndex="0" CssClass="form-text" runat="server" onkeypress="return MakeLogin(event);"></asp:TextBox>
				    <asp:Image ID="img_password" CssClass="icon-q" runat="server" ImageUrl="../images/icon-q.png" AlternateText="Insert your password" /> 
				</div>
                <div class="error-msg-wrap">
				    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" CssClass="error-msg" runat="server" ErrorMessage="Missing"
				    ControlToValidate="txtPassword" ValidationGroup="enter"></asp:RequiredFieldValidator>
				</div>
			</div>
			
            <asp:Button ID="Button_Enter" runat="server" Text="Enter" TabIndex="0" CssClass="loginbtn" OnClick="Button1_Click" ValidationGroup="enter" />
            
		</div>
	</div>
	
</div>
<div id="sidebar">
	<div class="add">
		<p><asp:Label ID="lbl_commercial" runat="server" ></asp:Label></p>
	</div>
	
</div>	


<asp:HiddenField ID="_HiddenField_RequestSuccess" runat="server" Value="The information was stored successfully" /> 
<asp:HiddenField ID="_HiddenField_RequestFailed" runat="server" Value="There was an error, please enter info again" />    
<asp:Label ID="lbl_PasswordSent" runat="server" Text="Password was sent to your cell phone!" Visible="false"></asp:Label>
<asp:Label ID="lbl_notFound" runat="server" Text="Wrong password" Visible="false"></asp:Label>
 <asp:Label ID="lbl_WrongEmail" runat="server" Text="User or Password is not correct" Visible="false"></asp:Label>
 
 <asp:HiddenField ID="hf_condition" runat="server" Value="Please confirm the terms and conditions" />
 <asp:Label ID="lbl_emailPhoneExist" runat="server" Text="Please try using different email or phone number" Visible="false"></asp:Label>
 <asp:Label ID="lbl_MissingCaptcha" runat="server" Visible="false" Text="Incorrect access code. Please try again"></asp:Label>	
 <asp:Label ID="lbl_AccountLocked" runat="server" Text="Your account is locked due to many long atte" Visible="false"></asp:Label>
 <asp:Label ID="lbl_AffiliateUser" runat="server" Text="You need to login by Affiliate portal" Visible="false"></asp:Label>

</asp:Content>

 

