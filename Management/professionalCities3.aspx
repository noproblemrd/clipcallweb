<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professionalCities3.aspx.cs" Inherits="Management_professionalCities3" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
    <script type="text/javascript" src="../general.js"></script>
    
    <title>Untitled Page</title>
    <style type="text/css">

        
        .internalNodeClass
        {
            /*color:red;*/             
        }
        
        .TreeNode
        {
          color:green;
        }
        
        
       
    </style>
    <script type="text/javascript">
    var partial='<%#Hidden_Partial.Value%>';
    //alert(partial);
    var strCities="";
    var ifSelectedAll=false;
    
     
    var arrClicked=new Array();
    indexClicked=0;
    
    // get_checked()
    // get_text()
    // get_checkState() 0 no , 1 yes, 2 Indeterminate 
    // get_level() start with 0
    
    function witchAlreadyStep(level)
    {   
        parent.window.witchAlreadyStep(level);        
        parent.window.setLinkStepActive('linkStep3');   
        
    }
    
    function setStep4()
    {  
        alert("bbbbb");
        parent.window.step4();
    }    
    
    function nodePopulating(sender, eventArgs)
    {
       var node = eventArgs.get_node();
       var context = eventArgs.get_context();
       context["CategoryID"] = node.get_value();
    }

    function sendCities(strCities)
    {   
        parent.showDiv();
        	     
        var url; 
          
        url="http://" + location.hostname + ":" + location.port + "<%#ResolveUrl("~")%>WebServiceSupplier.asmx/sendCitiesTree";
        //alert("url:" + url);
        params="strCities=" + escape(strCities);
        //alert("params: " + params);
        var objHttp;
        var ua = navigator.userAgent.toLowerCase(); 

        if(ua.indexOf("msie")!= -1)
        {       
            objHttp =  new ActiveXObject("Msxml2.XMLHTTP");       
        }
        else
        {
            //alert("not msie");
            objHttp=new XMLHttpRequest();
            objHttp.overrideMimeType("application/xml"); 

        }

        if (objHttp == null)
        {
            //alert("Unable to create DOM document!");
            return;
        }      
        
        objHttp.open("POST", url, true); // third parameter false. synchrony 	       

        objHttp.onreadystatechange=function()
        {
                  
            if (objHttp.readyState==4)
            {   
                //alert(objHttp.responseText);     
                           
                var xmlDoc=objHttp.responseXML.documentElement;               		            
               
                var status=xmlDoc.childNodes[0].nodeValue;          
                //alert(status);
                if(document.getElementById("<%#btnSendWebService.ClientID%>").title=="Next")
                {
                        //alert("next");
                        if(status.toLowerCase()=="success")
                        {
                            alert('<%#Hidden_UpdateSuccess.Value%>');                            
                            setStep4();
                        }
                        else
                            alert('<%#Hidden_UpdateFailed.Value%>  ' + status);
                 }
                    
                 else
                 {                        
                        if(status.toLowerCase()=="success")
                        {
                            alert('<%#Hidden_UpdateSuccess.Value%>');
                            location.href='professionalCities3.aspx';                            
                        }
                        else
                            alert('<%#Hidden_UpdateFailed.Value%>  ' + status);
                 }
               
                 parent.hideDiv();
                     
             }
                
             
        }    

        /* use if send is synchrony
        function firefoxOnreadystatechange()
        {
            if (objHttp.readyState==4)
            {   
                //alert(objHttp.responseText);     
                           
                var xmlDoc=objHttp.responseXML.documentElement;               		            
               
                var status=xmlDoc.childNodes[0].nodeValue;          
                //alert(status);
                if(document.getElementById("<%#btnSendWebService.ClientID%>").title=="Next")
                {
                        //alert("next");
                        if(status.toLowerCase()=="success")
                        {
                            alert('<%#Hidden_UpdateSuccess.Value%>');
                            if(mode=="Next")
                                setStep4();
                        }
                        else
                            alert('<%#Hidden_UpdateFailed.Value%>  ' + status);
                    }
                    
                    else
                    {
                        if(status.toLowerCase()=="success")
                        {
                            alert('<%#Hidden_UpdateSuccess.Value%>');                            
                        }
                        else
                            alert('<%#Hidden_UpdateFailed.Value%>  ' + status);
                    }
               
                
                     
                }
                
                parent.hideDiv();
        }
        */
        
        
        objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        objHttp.setRequestHeader("Content-Length", params.length);
        objHttp.setRequestHeader("Connection", "close");
        objHttp.send(params);
        
        /* use if send is synchrony
        var ifFireFox=false;
        if (ua.indexOf("firefox")!= -1)
            ifFireFox=true;
         
        // because of bug in firefox - onreadystatechange not firing when ajax is syncronus   
        if(ifFireFox)
            firefoxOnreadystatechange();
        */    
    
    }
    
    function checkIfThereAreChanges(nodeValue)
    {
         var ifChanged=false;
         
         for(i=0;i<arrClicked.length;i++)
         {
            //alert(arrClicked[i]);
            if(arrClicked[i]==nodeValue)
            {
                ifChanged=true;
                break;
            }
         }
         
         return ifChanged;
        
    }
    
    function getCities()
    {
        parent.showDiv();
        setTimeout("getCities2()",100);
    }
    
    function getCities2()
    {   
        
        
        GetNodes("<%#RadTreeView1.ClientID%>","start");
        //alert(strCities);
        if(strCities.length>0)
        {
            strCities=strCities.substring(0,strCities.length-1);
            //alert(strCities);
            sendCities(strCities);
        }
        
        else
        {
            alert("<%#Hidden_NoChanges.Value%>");
            parent.hideDiv();
        }
            
        strCities="";
        
    }
    
    function GetNodes(node,level)
    {     
          
       var tree;
       var ifBrunchChanged;
       
       if(level=="start")
       {
        tree = $find(node);               
       } 
       else
       {       
        tree=$find("<%#RadTreeView1.ClientID%>");
        tree = tree.findNodeByValue(node.get_value()); 
              
       }

       /*
       for(i=0; i<tree.AllNodes.length; i++)
       {
           alert(tree.AllNodes[i].Text);
       }
       */
       
       for (var i=0; i<tree.get_nodes().get_count();i++)   
       {         
           var node = tree.get_nodes().getNode(i); 
                  
           //alert(node.get_text() + " " + node.get_value());
           
                
               
               if(node.get_level()==0 && (node.get_checkState()==1 || node.get_checkState()==2)) // if first level 0 and also works or partial works
               {
                    ifBrunchChanged=checkIfThereAreChanges(node.get_value()); 
                    //alert("count:" + node.get_nodes().get_count());
                    if(ifSelectedAll) // if clicked button selected all (not checked each checkboxes by hemself)
                    {
                        //alert("click");
                        strCities+="RegionId=" + node.get_value() + ",RegionState=" + node.get_checkState() + 
                        ",IsDirty=True,Level=" + node.get_level() + ",Parents=" + node.get_value() + "&"; 
                        //alert("strCities:" + strCities);
                    }
                    
                    else if (ifBrunchChanged) // if some changes in the brunch
                    { 
                        //alert("change " + node.get_value());
                        //alert("text: " + node.get_text());
                        strCities+="RegionId=" + node.get_value() + ",RegionState=" + node.get_checkState() + 
                        ",IsDirty=" + node.get_attributes().getAttribute("ifExpand") + 
                        ",Level=" + node.get_level() + ",Parents=" + node.get_value() + "&"; 
                        
                        
                        if(node.get_checkState()==2) // if works partial
                        {
                            
                            //alert("have childs" + node.Text);
                            GetNodes(node,"not start");  
                        }
                    }
                    
                    else if (node.get_nodes().get_count()==0) // there are not changes and there is not changes 
                    {
                        //alert("change " + node.get_value());
                        //alert("text: " + node.get_text() + node.get_attributes().getAttribute("ifChecked"));
                        if(node.get_attributes().getAttribute("ifChecked")=="Working")
                            checkState="1";
                        else if(node.get_attributes().getAttribute("ifChecked")=="HasNotWorkingChild")
                            checkState="2";
                        else if(node.get_attributes().getAttribute("ifChecked")=="NotWorking")
                            checkState="0";
                            
                        strCities+="RegionId=" + node.get_value() + ",RegionState=" + checkState + 
                        ",IsDirty=" + node.get_attributes().getAttribute("ifExpand") + 
                        ",Level=" + node.get_level() + ",Parents=" + node.get_value() + "&"; 
                        
                        
                        if(node.get_checkState()==2) // if works partial
                        {
                            
                            //alert("have childs" + node.Text);
                            GetNodes(node,"not start");  
                        }
                    }
                    
                    else // there are not changes and at least expands 2 levels
                    {
                        //alert("change " + node.get_value());
                        //alert("text: " + node.get_text());
                        strCities+="RegionId=" + node.get_value() + ",RegionState=" + node.get_checkState() + 
                        ",IsDirty=" + node.get_attributes().getAttribute("ifExpand") + 
                        ",Level=" + node.get_level() + ",Parents=" + node.get_value() + "&"; 
                        
                        
                        if(node.get_checkState()==2) // if works partial
                        {
                            
                            //alert("have childs" + node.Text);
                            GetNodes(node,"not start");  
                        }
                    }
                    
               }
               
               else if(node.get_level()==0 && node.get_checkState()==0) // first level and not chosen
               {
               
               }
               
               else if(node.get_level()>0 && (node.get_checkState()==0 || node.get_checkState()==2)) // else if level above 0 and he not works with all childs or partial works
               {
                    //alert("text: " + node.get_text() + " " + node.get_parent().get_attributes().getAttribute("ifExpand"));
                    
                    
                    if(node.get_parent().get_attributes().getAttribute("ifExpand")=="True")
                    {
                        strCities+="RegionId=" + node.get_value() + ",RegionState=" + node.get_checkState() + 
                        ",IsDirty=True,Level=" + node.get_level();
                    }
                    
                    else
                    {
                        strCities+="RegionId=" + node.get_value() + ",RegionState=" + node.get_checkState() + 
                        ",IsDirty=False,Level=" + node.get_level();
                    }
                    /*
                    strCities+="RegionId=" + node.get_value() + ",RegionState=" + node.get_checkState() + 
                    ",IsDirty=" + node.get_attributes().getAttribute("ifExpand") + 
                    ",Level=" + node.get_level();
                    */
                    
                    //alert("strCities " + strCities);
                    parentNode="";
                    strParents="";
                    //alert("node.get_level():" + node.get_level());
                    for(indexParent=0;indexParent<=node.get_level();indexParent++)
                    {
                        //alert("indexParent: " + indexParent);
                        if(indexParent==0)
                        {
                            parentNode=node;                        
                        }
                        else
                        {
                            parentNode=parentNode.get_parent();
                        }
                        
                        strParents=parentNode.get_value() + "**" + strParents;
                        //alert("strParents:" + strParents);
                    }
                    
                    //alert("here");
                    indexParent=0;
                    
                    strCities+=",Parents=" + strParents + "&"; 
                    
                    if(node.get_checkState()==2) // if works partial
                    {
                        
                        //alert("have childs" + node.Text);
                        GetNodes(node,"not start");  
                    }
               }
               
               else if(node.get_level()>0 && node.get_checkState()==1) // else if level above 0 and he works with all childs
               {
                    /*
                    if(node.get_parent().get_attributes().getAttribute("ifExpand")=="True")
                    {
                        strCities+="RegionId=" + node.get_value() + ",RegionState=" + node.get_checkState() + 
                        ",IsDirty=True,Level=" + node.get_level();
                    }
                    
                    else
                    {
                        strCities+="RegionId=" + node.get_value() + ",RegionState=" + node.get_checkState() + 
                        ",IsDirty=False,Level=" + node.get_level();
                    }
                    */
               }        
           
           
       }
       
    }
    
    var clickedNode;
    
    
    function onNodeClicking(sender, args) // expand the brunch
	{    
	     //alert("expand"); 
	     clickedNode=args.get_node();  
	     //alert(clickedNode.get_text()); 
	     //alert("OnClientNodeClicking: " + args.get_node().get_text());
	     
	     //args.get_node().set_text("New Node");
	     //alert(args.get_node().get_attributes().getAttribute("ifExpand"));
         //you can also set attributes, like:
         //alert(args.get_node().get_attributes().getAttribute("ifExpand"));
         args.get_node().get_attributes().setAttribute("ifExpand","True");
         //alert(args.get_node().get_attributes().getAttribute("ifExpand"));
         //alert(args.get_node().get_attributes().getAttribute("ifExpand"));
	     
	     //alert("OnClientNodeClicking: " + args.get_node().get_text());
	     
	     //alert("checked:" + args.get_node().get_checked());
	     //alert("get_checkState:" + args.get_node().get_checkState());
	     if(args.get_node().get_checkState()=="0")
	        args.get_node().get_attributes().setAttribute("ifChecked","NotWorking");
	     else if(args.get_node().get_checkState()=="1")
	        args.get_node().get_attributes().setAttribute("ifChecked","Working");
	     else if(args.get_node().get_checkState()=="2")
	        args.get_node().get_attributes().setAttribute("ifChecked","HasNotWorkingChild");
	    //alert("get_checkState:" + args.get_node().get_checkState());
	}

    function setChildsPartialText(node)
    {       
        
        var posPartialSon;
        var currentObj;
        
        for(var i=0;i<node.get_nodes().get_count();i++)
        {            
            currentObj=node.get_nodes().getNode(i);            
            posPartialSon=currentObj.get_text().indexOf(partial);           
            
            /****************  check if partial *********/
               
               
               if (currentObj.get_checkState()==2) // if partial add text partial
               {                   
                    if(posPartialSon==-1)
                    {                        
                        currentObj.set_text(currentObj.get_text() + " (" + partial + ")"); 
                    }   
               }
               
               else 
               {
                    if(posPartialSon!=-1)// if not partial but was before partial remove the text partial
                    {
                        //alert("not partial " + currentObject.get_text() + " " + posPartial);
                        
                        currentObj.set_text(currentObj.get_text().substring(0,posPartialSon-2)); 
                       
                    }   
               }
               
               if(currentObj.get_nodes().get_count()>0)
                    setChildsPartialText(currentObj)
        } 
            
      
    }
    
    function checkIfAllChecked()
    {
         
         var tree = $find("<%#RadTreeView1.ClientID%>");
         
         //alert(tree.get_allNodes().length + " " + tree.get_checkedNodes().length);
         if(tree.get_allNodes().length==tree.get_checkedNodes().length)
         {            
            selectedAll2(true);
         }
         
         else
         {                       
            selectedAll2(false);
         }
    }
    
    function onNodeClicking2(sender, args) // checked the check box
	{     
	    //alert("onNodeClicking2");    
         //alert(arrClicked.length);
                  
         /**************** if checked enter to array the first level value. 
         by doing this prevent sending not changed brunches ********/
         
         var currentObject = args.get_node();
         if(currentObject.get_nodes().get_count()>0)
            setChildsPartialText(currentObject);
          
            checkIfAllChecked();
             
         while (currentObject != null)
         {
             // get_parent() will return null when we reach the treeview
             if (currentObject.get_parent() != null)
             {
               //alert(currentObject.get_value() + " level:" + currentObject.get_level());
               //alert(currentObject.get_text() + " " + currentObject.get_checkState());
               if(currentObject.get_level()=="0") // add to array the first level brunch
               {              
                    
                    
                    arrClicked[indexClicked++]=currentObject.get_value(); 
                                
               }
               
               /****************  check if partial *********/
               var posPartial;
               posPartial=currentObject.get_text().indexOf(partial);
               
               if (currentObject.get_checkState()==2) // if partial add text partial
               {
                    //alert("partail:" + currentObject.get_text());
                    if(posPartial==-1)
                    {
                        ///alert("not yet partial");
                        currentObject.set_text(currentObject.get_text() + " (" + partial + ")"); 
                    }   
               }
               
               else // if not partial but was before partial remove the text partial
               {
                    if(posPartial!=-1)
                    {
                        //alert("not partial " + currentObject.get_text() + " " + posPartial);
                        
                        currentObject.set_text(currentObject.get_text().substring(0,posPartial-2)); 
                       
                    }   
               }
               //alert(currentObject.get_checkState() + " " + currentObject.get_text());
               
             } 
                       
             /****************  check if partial *********/
               
             currentObject = currentObject.get_parent();             
             
         }         
         
        
        
         /**************** end: if checked enter to array the first level value. 
         by doing this prevent sending not changed brunches ********/
	     
	     
	     ///clickedNode=args.get_node();   
	     //alert("OnClientNodeClicking: " + args.get_node().get_text());
	     
	     //args.get_node().set_text("New Node");
	     //alert(args.get_node().get_attributes().getAttribute("ifExpand"));
         //you can also set attributes, like:
         
         //alert(args.get_node().get_attributes().getAttribute("ifExpand"));
         
         args.get_node().get_attributes().setAttribute("ifExpand","True");
         //alert(args.get_node().get_attributes().getAttribute("ifExpand"));
         //alert(args.get_node().getAttribute("ifExpand"));
         //alert(args.get_node().get_attributes().getAttribute("ifExpand"));
	     
	     //alert("OnClientNodeClicking: " + args.get_node().get_text());
	     
	     //alert("checked:" + args.get_node().get_checked());
	     //alert("get_checkState:" + args.get_node().get_checkState());
	     if(args.get_node().get_checkState()=="0")
	     {
	        args.get_node().get_attributes().setAttribute("ifChecked","NotWorking");
	     }
	     
	     else if(args.get_node().get_checkState()=="1")
	        args.get_node().get_attributes().setAttribute("ifChecked","Working");
	     else if(args.get_node().get_checkState()=="2")
	        args.get_node().get_attributes().setAttribute("ifChecked","HasNotWorkingChild");
	    //alert("get_checkState:" + args.get_node().get_checkState());
	}

    function onPopulated()
    {   
        //alert("onPopulated");    
        var ifSonNotWorking=false;
        
        
               
        if(clickedNode)
        {
            //alert(clickedNode.get_text()); 
            for (var i=0; i<clickedNode.get_nodes().get_count();i++)   
            { 
                
                var clickedNodeSon = clickedNode.get_nodes().getNode(i);
                //alert(clickedNodeSon.get_checkState() + " " + clickedNodeSon.get_text());
                if(clickedNodeSon.get_checkState()=="0")
                { 
                    ifSonNotWorking=true;
                    break;
                }
            }
            
           if(ifSonNotWorking)
           {   
                //alert("hhhh");           
               if(clickedNodeSon.get_checked())    
               {     
                   clickedNodeSon.set_checked(false);                                   
               }    
               else    
               {       
                   clickedNodeSon.set_checked(true);               
               } 
               
               if(clickedNodeSon.get_checked())    
               {        
                   clickedNodeSon.set_checked(false);                 
               }    
               else    
               {       
                   clickedNodeSon.set_checked(true);               
               } 
           }            
       }
        
    }
    
    function selectedAll()
    {
        if(!ifSelectedAll)
        {
            document.getElementById("divCheckAll").style.backgroundPosition = "0px 0px";
            ifSelectedAll=true;
            tree=$find("<%#RadTreeView1.ClientID%>");
            
            for (var i=0; i<tree.get_nodes().get_count();i++)   
            { 
               var node = tree.get_nodes().getNode(i);
               node.set_checked(true); 
            } 
        }
        
        else
        {
            document.getElementById("divCheckAll").style.backgroundPosition = "0px -13px";
            ifSelectedAll=false;
            tree=$find("<%#RadTreeView1.ClientID%>");
            
            for (var i=0; i<tree.get_nodes().get_count();i++)   
            { 
               var node = tree.get_nodes().getNode(i);
               node.set_checked(false); 
            } 
        }
            
    }
    
    function selectedAll2(ifSelectedAll)
    {
        
        if(ifSelectedAll) // if checked all the checkboxes manually
        {
             document.getElementById("divCheckAll").style.backgroundPosition = "0px 0px";
             ifSelectedAll=false;
        }
        
        else
        {
            document.getElementById("divCheckAll").style.backgroundPosition = "0px -13px";            
            ifSelectedAll=true;
        }
    }
    
    function searchHeighlight(node,searchValue,level)
    {
        //alert(searchValue + " " + searchValue.length);
        var tree;        
       
        if(level=="start")
        {
         tree = $find(node);               
        } 
        else
        {       
         tree=$find("<%#RadTreeView1.ClientID%>");
         tree = tree.findNodeByValue(node.get_value());               
        }
        
        for (var i=0; i<tree.get_nodes().get_count();i++)   
        { 
           var node = tree.get_nodes().getNode(i);
           if(searchValue.length==0)
            node.unselect();
           else if (node.get_text().toLowerCase().indexOf(searchValue.toLowerCase())!=-1)
            node.select();
           else
            node.unselect();
            
           if(node.get_count>0)
           {
           }
            
           else
            searchHeighlight(node,searchValue,"not start");
        }
    }
    
function btnSerach_onclick() {

}


    </script>
</head>
<body class="iframe-inner step3" style="margin:0;">
<!-- Forest Black Default Hay Office2007 Outlook Simple Sitefinity Sunset Telerik Vista Web20 WebBlue Windows7 -->
<!-- Office2007 Web20-->

    <form id="form2" runat="server">
        <div class="form-item2">
            <label for="txt_search" id="lblMySegments"><asp:Label ID="lbl_MySegments" runat="server" Text="Search for"></asp:Label></label>
            <input type="text" class="form-text" name="txt_search" id="txt_search" onkeyup="searchHeighlight('<%#RadTreeView1.ClientID%>',this.value,'start');"/>
            <!--
            <div class="searchform"> <asp:Button runat="server" ID="btnSearch" Text="GO" class="form-submit" value="Serach" 
                onclick="btnSearch_Click"/></div>
                -->
        </div>
        
        <div class="form-item check-all clearfix" style="margin-bottom: 5px; width: 52px;">
            <div id="divCheckAll" onclick="selectedAll();" ></div>
            <label for="divCheckAll"><asp:Label ID="lbl_CheckAll" runat="server" Text="All" ></asp:Label></label>
        </div>
        
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" ScriptMode="Release" ></telerik:RadScriptManager>        
        <telerik:RadTreeView ID="RadTreeView1" runat="server" MultipleSelect="true" Skin="Office2007" CssClass="RadTreeViewMyClass"       
        CheckBoxes="true" OnClientNodeChecked="onNodeClicking2"  TriStateCheckBoxes="true"  OnClientLoad="checkIfAllChecked"  OnClientNodePopulated="onPopulated"  OnClientNodeExpanded="onNodeClicking"   >
        <WebServiceSettings  Path="../WebServiceGetCityChilds.asmx"  Method="getCityChilds"   />         
        </telerik:RadTreeView>
        
        <input type="button" class="form-submit" runat="server" id="btnSendWebService" onclick="getCities();"  title="" tabindex="8"/>         
        
        <p class="steps" style="margin-top:10px;" runat="server" id="pSteps">
            <asp:Label ID="lbl_Step2" runat="server" Text="STEP 3 OUT"></asp:Label>
            <asp:Label ID="lbl_page" runat="server" ></asp:Label>
		    <img src="<%=ResolveUrl("~")%>Management/images/bullet.gif" alt="bullet" />
		</p>
		 
		<asp:Label ID="lbl_btn_Update" runat="server" Text="UPDATE" Visible="false"></asp:Label>
        <asp:Label ID="lbl_btn_Next" runat="server" Text="NEXT" Visible="false"></asp:Label>
        <asp:HiddenField ID="Hidden_UpdateSuccess" runat="server" Value="The information was stored successfully" />
        <asp:HiddenField ID="Hidden_UpdateFailed" runat="server" Value="Error occurred" />
        <asp:HiddenField ID="Hidden_NoChanges" runat="server" Value="Must mark at least one area" />
        <asp:HiddenField ID="Hidden_Partial" runat="server" Value="partial" />
    </form>

</body>
</html>
