using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;

public partial class Management_professionLoginRename : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.DataBind();
        }
    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string newPassword = txtNewPassword1.Text.Trim();
        if (newPassword.Length < 6)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "6Characters", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_6Character.Text) + "');", true);
            return;
        }
        if (txtNewPassword2.Text != newPassword)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "NotCompare", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NotCompare.Text) + "');", true);
            return;
        }
        Regex _reg = new Regex(RegularExpressionValidator_NewPassword2.ValidationExpression);
        if (!_reg.IsMatch(newPassword))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "NotAllowedCharacter", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AllowedCharacter.Text) + "');", true);
            return;
        }
        if (userManagement.User_Name.Contains(newPassword) || newPassword.Contains(userManagement.User_Name))             
        {
            ClientScript.RegisterStartupScript(this.GetType(), "NotIdential", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_Idential.Text) + "');", true);
            return;
        }
        if (!string.IsNullOrEmpty(userManagement.Email))
        {
            if (userManagement.Email.Contains(newPassword) || newPassword.Contains(userManagement.Email))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "NotIdential", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_Idential.Text) + "');", true);
                return;
            }
        }
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        string result = "";
        try
        {
            result = supplier.ChangePassword(userManagement.GetGuid, txtOldPassword.Text, newPassword);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);
        if(xdd.Element("ChangePassword") == null || xdd.Element("ChangePassword").Element("Error") != null || xdd.Element("ChangePassword").Element("Status").Value != "Success")
            ClientScript.RegisterStartupScript(this.GetType(), "WrongPassword", "alert('" + lbl_incorrectPassword.Text + "');", true);
        else
            mvChangePassword.ActiveViewIndex = 1;        
        
    }
}
