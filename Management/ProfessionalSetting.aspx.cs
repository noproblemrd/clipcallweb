using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Management_ProfessionalSetting : PageSetting
{
    string page_translate;
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
  //      userManagement = UserMangement.GetUserObject(this);
        if (userManagement.IsPublisher())
            this.MasterPageFile = "../Publisher/MasterPageFrame.master";
        else if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
            this.MasterPageFile = ResolveUrl("~") + "Controls/MasterPagePPC.master";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            LoadDateFormat();
            LoadTimeFormat();
            LoadLanguages();
            SetMaxNumberDailyCalls();
            LoadAdvertiserDetail();   
            if(Request.QueryString["update"]=="update")
                ClientScript.RegisterStartupScript(this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        }
        Header.DataBind();
    }

    private void SetMaxNumberDailyCalls()
    {
        tr_MaxNumberDailyCalls.Visible = userManagement.IsPublisher() || !PpcSite.GetCurrent().IsZapSite(siteSetting.GetSiteID);
    }

    private void LoadLanguages()
    {
        string command = "EXEC dbo.GetLanguagesBySiteNameId @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ddl_languages.Items.Add(new ListItem((string)reader["LanguageName"],
                    "" + (int)reader["SiteLangId"]));
            }
            conn.Close();
        }

    }

    private void LoadAdvertiserDetail()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        try
        {
            IAsyncResult ar = _supplier.BeginGetDailyBudget(new Guid(GetGuidSetting()), null, null);

            int TimeId = 0, DateId = 0, SiteLangId = 0;
            string command = "EXEC dbo.GetAdvertiserByUserId @UserId, @SiteNameId";
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@UserId", new Guid(GetGuidSetting()));
                cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
                SqlDataReader reader = cmd.ExecuteReader();
                
                if (reader.Read())
                {
                    TimeId = (int)reader["TimeFormatId"];
                    DateId = (int)reader["DateFormatId"];
                    SiteLangId = (int)reader["SiteLangId"];
                }
                //    else
                //         IsFirstTime = true;
                conn.Close();
            }
            if (TimeId > 0)
            {
                foreach (ListItem li in ddl_TimeFormat.Items)
                {
                    if (li.Value == ("" + TimeId))
                    {
                        li.Selected = true;
                        dic.Add(lbl_TimeFormat.ID, li.Text);
                        break;
                    }
                }
            }
            if (DateId > 0)
            {
                foreach (ListItem li in ddl_dateFormat.Items)
                {
                    if (li.Value == ("" + DateId))
                    {
                        li.Selected = true;
                        dic.Add(lbl_dateFormat.ID, li.Text);
                        break;
                    }
                }
            }
            string _lang = SetDDL_lang("" + SiteLangId);
            if (_lang == string.Empty)
            {
                string _command = "SELECT dbo.GetSiteLangIdDefaultBySiteNameId(@SiteNameId)";
                using (SqlConnection _conn = DBConnection.GetConnString())
                {
                    _conn.Open();
                    SqlCommand _cmd = new SqlCommand(_command, _conn);
                    _cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
                    SiteLangId = (int)_cmd.ExecuteScalar();
                    _conn.Close();
                }
                SetDDL_lang("" + SiteLangId);
            }
            else
                dic.Add(lbl_DefaultLang.ID, _lang);
            WebReferenceSupplier.ResultOfNullableOfInt32 _result = _supplier.EndGetDailyBudget(ar);
            if (_result.Type == WebReferenceSupplier.eResultType.Failure)
            {
                Update_Faild();
                return;
            }
            if (!_result.Value.HasValue)
            {
                cb_DailyBudget.Checked = false;
                div_DailyBudget.Style.Add("visibility", "hidden");
                txt_DailyBudget.Text = string.Empty;
                dic.Add(lbl_DailyBudget.ID, string.Empty);
            }
            else
            {
                cb_DailyBudget.Checked = true;
                div_DailyBudget.Style.Add("visibility", "visible");
                txt_DailyBudget.Text = _result.Value + "";
                dic.Add(lbl_DailyBudget.ID, _result.Value + "");
            }

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        dicDetailsV = dic;
        
    }
    private string SetDDL_lang(string SiteLangId)
    {
        string FindLang = string.Empty;
        foreach (ListItem li in ddl_languages.Items)
        {
            if (li.Value == SiteLangId)
            {
                li.Selected = true;
                FindLang = li.Text;
                break;
            }
        }
        return FindLang;
    }
    private void LoadTimeFormat()
    {
        string command = "EXEC dbo.GetTimeFormat";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ddl_TimeFormat.Items.Add(new ListItem((string)reader["Symbol"], "" + (int)reader["ID"]));
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                Session["exception"] = ex;
                Response.Redirect(ErrorPath);
            }
        }
    }

    private void LoadDateFormat()
    {
        string command = "EXEC dbo.GetDateFormat";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ddl_dateFormat.Items.Add(new ListItem((string)reader["Symbol"], "" + (int)reader["ID"]));
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                Session["exception"] = ex;
                Response.Redirect(ErrorPath);
            }
        }
    }
    protected void imageSend_Click(object sender, EventArgs e)
    {
        bool SaveInCrm = true;
        bool SaveInDB = true;
        int debug = 0;
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        int? _DailyBudget = null;
        if (cb_DailyBudget.Checked)
        {
            int _temp;
            if (!int.TryParse(txt_DailyBudget.Text, out _temp))
            {
                Update_Faild();
                return;
            }
            _DailyBudget = _temp;
        }
        
        IAsyncResult ar = _supplier.BeginUpdateDailyBudget(new Guid(GetGuidSetting()), _DailyBudget, null, null);
        //     _supplier.UpdateDailyBudgetAsync(new Guid(GetGuidSetting()), null)
        int SiteLangId = int.Parse(ddl_languages.SelectedValue);
        string command = "EXEC dbo.SetAdvertiserDetails @UserId, " +
            "@SiteNameId, @TimeFormatId, @DateFormatId, @SiteLangId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@UserId", new Guid(GetGuidSetting()));
                cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
                cmd.Parameters.AddWithValue("@TimeFormatId", int.Parse(ddl_TimeFormat.SelectedValue));
                cmd.Parameters.AddWithValue("@DateFormatId", int.Parse(ddl_dateFormat.SelectedValue));
                cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
                int a = cmd.ExecuteNonQuery();
                conn.Close();
                debug++;



                if (userManagement.IsSupplier())
                {
                    siteSetting.siteLangId = SiteLangId;
                    using (SqlConnection connDate = DBConnection.GetConnString())
                    {
                        command = "EXEC dbo.GetDateTimeFormatByUserId @UserId, @SiteNameId";
                        connDate.Open();
                        SqlCommand _cmd = new SqlCommand(command, connDate);
                        _cmd.Parameters.AddWithValue("@UserId", new Guid(GetGuidSetting()));
                        _cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
                        SqlDataReader _reader = _cmd.ExecuteReader();
                        if (_reader.Read())
                        {
                            siteSetting.DateFormat = (string)_reader["DateFormat"];
                            siteSetting.TimeFormat = (string)_reader["TimeFormat"];
                        }
                        connDate.Close();
                    }
                    Response.Cookies["language"].Value = SiteLangId.ToString();
                    Response.Cookies["language"].Expires = DateTime.Now.AddMonths(3);
                }
            }
            catch (Exception ex)
            {
                dbug_log.ExceptionLog(ex, siteSetting);
                SaveInDB = false;
            }
        }
        try
        {
            WebReferenceSupplier.Result _Result = _supplier.EndUpdateDailyBudget(ar);
            if (_Result.Type == WebReferenceSupplier.eResultType.Failure)
            {
   //             ClientScript.RegisterStartupScript(this.GetType(), "CrmError", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_ServerError.Text) + "');", true);
                SaveInCrm = false;
            }
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            /*
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
             * */
            SaveInCrm = false;            
        }
        if (!SaveInCrm && !SaveInDB)
        {
            Update_Faild();
            return;
        }
        string pagename =
           System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        page_translate = EditTranslateDB.GetPageNameTranslate(pagename, siteSetting.siteLangId);
        UserMangement um = GetUserSetting();
        Dictionary<string, string> dic = dicDetailsV;
        OneCallUtilities.AuditRequest audit_request = new OneCallUtilities.AuditRequest();
        List<OneCallUtilities.AuditEntry> list = new List<OneCallUtilities.AuditEntry>();
        string date_format = ddl_dateFormat.SelectedItem.Text;
        string time_format = ddl_TimeFormat.SelectedItem.Text;
        string _language = ddl_languages.SelectedItem.Text;
        OneCallUtilities.Status _status = OneCallUtilities.Status.Update;
        string Daily_Budget = ((_DailyBudget == null) ? "" : _DailyBudget + "");
        if (SaveInDB)
        {
            if (!dic.ContainsKey(lbl_dateFormat.ID))
            {
                dic.Add(lbl_dateFormat.ID, string.Empty);
                _status = OneCallUtilities.Status.Insert;
            }
            if (dic[lbl_dateFormat.ID] != date_format)
                list.Add(GetBaseDetails(um, _status,
                    pagename, lbl_dateFormat.ID, lbl_dateFormat.Text, date_format, dic[lbl_dateFormat.ID]));

            _status = OneCallUtilities.Status.Update;
            if (!dic.ContainsKey(lbl_TimeFormat.ID))
            {
                dic.Add(lbl_TimeFormat.ID, string.Empty);
                _status = OneCallUtilities.Status.Insert;
            }
            if (dic[lbl_TimeFormat.ID] != time_format)
                list.Add(GetBaseDetails(um, _status,
                    pagename, lbl_TimeFormat.ID, lbl_TimeFormat.Text, time_format, dic[lbl_TimeFormat.ID]));

            _status = OneCallUtilities.Status.Update;
            if (!dic.ContainsKey(lbl_DefaultLang.ID))
            {
                dic.Add(lbl_DefaultLang.ID, string.Empty);
                _status = OneCallUtilities.Status.Insert;
            }
            if (dic[lbl_DefaultLang.ID] != _language)
                list.Add(GetBaseDetails(um, _status,
                    pagename, lbl_DefaultLang.ID, lbl_DefaultLang.Text, _language, dic[lbl_DefaultLang.ID]));
        }
        if (SaveInCrm)
        {
            if (!dic.ContainsKey(lbl_DailyBudget.ID))
            {
                dic.Add(lbl_DailyBudget.ID, string.Empty);
                _status = OneCallUtilities.Status.Insert;
            }
            if (dic[lbl_DailyBudget.ID] != Daily_Budget)
                list.Add(GetBaseDetails(um, _status,
                    pagename, lbl_DailyBudget.ID, lbl_DailyBudget.Text, Daily_Budget, dic[lbl_DailyBudget.ID]));
        }
        OneCallUtilities.OneCallUtilities ocu = WebServiceConfig.GetOneCallUtilitiesReference(this);
        audit_request.AuditOn = true;
        audit_request.AuditEntries = list.ToArray();
        OneCallUtilities.Result _result = new OneCallUtilities.Result();
        try
        {
            _result = ocu.CreateAudit(audit_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
        }
        if (!SaveInCrm)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "CrmError", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_CrmError.Text) + "');", true);
            return;
        }
        if (!SaveInDB)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "CrmError", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_ServerError.Text) + "');", true);
            return;
        }
      
       
        Response.Redirect("ProfessionalSetting.aspx?update=update");
    }
    OneCallUtilities.AuditEntry GetBaseDetails(UserMangement um, OneCallUtilities.Status _status, string pageName,
        string fieldId, string fieldName, string newValue, string oldValue)
    {
        OneCallUtilities.AuditEntry _ae = new OneCallUtilities.AuditEntry();
        _ae.AdvertiseId = new Guid(um.GetGuid);
        _ae.AdvertiserName = um.User_Name;
        _ae.AuditStatus = _status;
        _ae.PageId = pageName;
        _ae.FieldId = fieldId;
        _ae.FieldName = fieldName;
        _ae.NewValue = newValue;
        _ae.OldValue = oldValue;
        _ae.UserId = new Guid(userManagement.GetGuid);
        _ae.UserName = userManagement.User_Name;
        _ae.PageName = page_translate;
        return _ae;
    }
    private void LoadDateTimeFormat()
    {
        string command = "EXEC dbo.GetDateTimeFormatByUserId @UserId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", new Guid(userManagement.GetGuid));
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                if (!reader.IsDBNull(0))
                    siteSetting.DateFormat = (string)reader["DateFormat"];
                if (!reader.IsDBNull(1))
                    siteSetting.TimeFormat = (string)reader["TimeFormat"];
                siteSetting.SetSiteSetting(this);
            }
            conn.Close();
        }
    }
    Dictionary<string, string> dicDetailsV
    {
        get { return (ViewState["dicDetails"] == null ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["dicDetails"]); }
        set { ViewState["dicDetails"] = value; }
    }
    bool IsFirstTime
    {
        get { return (ViewState["FirstTime"] == null) ? false : (bool)ViewState["FirstTime"]; }
        set { ViewState["FirstTime"] = value; }
    }
}
