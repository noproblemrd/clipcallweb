﻿<%@ Page Language="C#" MasterPageFile="~/Management/MasterPageAdvertiser.master" AutoEventWireup="true" CodeFile="MyBalance.aspx.cs" Inherits="Management_MyBalance" Title="Untitled Page" %>



<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="Toolbox" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
  <link href="../Controls/Toolbox/StyleToolboxReport.css" rel="Stylesheet" type="text/css" />

<script  type="text/javascript" src="../Calender/_Calender.js"></script>
<script  type="text/javascript"  >
    window.onload = appl_init;
    /*
    function pageLoad(sender, args)
    {
        hideDiv();
        appl_init();
    }
    */
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function EndHandler()
    {
        hideDiv();
    }
    function BeginHandler()
    {       
        showDiv();
    }
</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">




<div id="center__bg" runat="server" class="page-contentm my-ballance">      
                    
    
    <uc1:Toolbox runat="server" ID="_Toolbox" LoadStyle="false"/>
    <!--<div id="top_hr"></div>
    <div id="dear"><span></span></div>
    <div id="text1"></div>-->
    <div class="contpage">  
   <div class="form-fieldM">                        
    <asp:Label ID="lbl_auction"  CssClass="label" runat="server" Text="Action"></asp:Label>
    
    <asp:DropDownList ID="ddl_auction" runat="server" CssClass="form-select">
    </asp:DropDownList>
     </div>      
    <div class="mybalance"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>
    
  
     <div class="clear"></div>                          
    <div id="list" class="list" style="margin-top:0px;">  
    <asp:UpdatePanel runat="server" ID="_uPanel" UpdateMode="Conditional">
    <ContentTemplate> 
        <div class="data-table" style="margin-top:20px;">
        <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" CssClass="data-table"
         AllowPaging="true" PageSize="20" 
                
                AllowSorting="True" onsorting="_GridView_Sorting" 
                onpageindexchanging="_GridView_PageIndexChanging">
        <RowStyle CssClass="even" />
        <AlternatingRowStyle CssClass="odd" />
        <Columns>
            <asp:TemplateField SortExpression="Date" >
            <HeaderTemplate>
           
                <asp:LinkButton ID="LinkButton1" runat="server" Text="<%# lbl_date.Text %>"  CommandName="Sort" CssClass="titlelink"
                CommandArgument="Date"></asp:LinkButton>
              
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lbl_create" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField SortExpression="Action">
            <HeaderTemplate>
                 <asp:LinkButton ID="LinkButton2" runat="server" Text="<%# lbl_Action.Text %>"  CommandName="Sort"  CssClass="titlelink"
                CommandArgument="Action"></asp:LinkButton>
                
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblAction" runat="server" Text="<%# Bind('Action') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField SortExpression="_Amount" >
            <HeaderTemplate>
                <asp:LinkButton ID="LinkButton3" runat="server" Text="<%# lbl_amount.Text %>"  CommandName="Sort"  CssClass="titlelink"
                CommandArgument="_Amount"></asp:LinkButton>
                
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblAmount" runat="server" Text="<%# Bind('Amount') %>" CssClass="<%# Bind('css_amount') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField SortExpression="_Balance">
            <HeaderTemplate>
                <asp:LinkButton ID="LinkButton4" runat="server" Text="<%# lbl_Balance.Text %>"  CommandName="Sort"  CssClass="titlelink"
                CommandArgument="_Balance"></asp:LinkButton>
                
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblBalance" runat="server" Text="<%# Bind('Balance') %>" ></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
           
        </Columns>
        
        <PagerStyle HorizontalAlign="Center" CssClass="pager" />
        </asp:GridView>
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</div> 
</div> 
<asp:Label ID="lbl_date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Action" runat="server" Text="Action" Visible="false"></asp:Label>
<asp:Label ID="lbl_amount" runat="server" Text="Amount" Visible="false"></asp:Label>
<asp:Label ID="lbl_Balance" runat="server" Text="Balance" Visible="false"></asp:Label>

<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
<asp:Label ID="lbl_NoResult" runat="server" Visible="false"
     Text="There are not results"></asp:Label>

<asp:Label ID="lblTitleMyBallance" runat="server" Text="My Ballance Reports" Visible="false"></asp:Label>
</asp:Content>

