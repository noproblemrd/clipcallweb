﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Management_MyReview : PageSetting
{
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    const string REPORT_NAME = "ReviewsReport";
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        if (userManagement.IsPublisher())
            this.MasterPageFile = "../Publisher/MasterPageFrame.master";
        else if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
            this.MasterPageFile = ResolveUrl("~") + "Controls/MasterPagePPC.master";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        SetToolboxEvents();
        if (!IsPostBack)
        {
            SetToolbox();
        }
    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lblTitleMyReview.Text);
    }
    void SetToolboxEvents()
    {
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
    }

    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordToPrint) + "');", true);
            return;
        }


        Session["data_print"] = dataV;
        Session["grid_print"] = _reapeter;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordExport) + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, REPORT_NAME);
        if (!te.ExecExcel((dataV)))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        DateTime from_date = FromToDate1.GetDateFrom;
        DateTime to_date = FromToDate1.GetDateTo;
        if (from_date == DateTime.MinValue || to_date == DateTime.MinValue)
        {
            to_date = DateTime.Now;
            from_date = to_date.AddMonths(-1);
        }
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.GetReviewsRequest _request = new WebReferenceReports.GetReviewsRequest();
        _request.SupplierId = new Guid(GetGuidSetting());
        _request.FromDate = from_date;
        _request.ToDate = to_date;
        _request.ReviewStatus = WebReferenceReports.ReviewStatus.Approved;

        WebReferenceReports.ResultOfGetReviewsResponse result = null;
        try
        {
            result = _report.GetReviews(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Faild", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Faild", "top.UpdateFailed();", true);
            return;
        }
        Dictionary<Guid, string> dic = new Dictionary<Guid, string>();
        DataTable data = new DataTable();
    //    data.Columns.Add("Number");
        data.Columns.Add("CreatedOn");
        data.Columns.Add("Phone");
        data.Columns.Add("Name");
        data.Columns.Add("Review");
        data.Columns.Add("IsLike");
        data.Columns.Add("ReviewId");
        foreach (WebReferenceReports.ReviewData rd in result.Value.Reviews)
        {
            DataRow row = data.NewRow();
            row["CreatedOn"] = rd.CreatedOn;
            row["Phone"] = rd.Phone;
            row["Name"] = rd.Name;
            string _review = rd.Description;
            string _preview = (_review.Length > 25) ? _review.Substring(0, 22) + "..." : _review;
            row["Review"] = _preview;
            row["IsLike"] = (rd.IsLike) ? @"../images/icon-thumbs-up.png" : @"../images/icon-thumbs-down.png";
            row["ReviewId"] = rd.ReviewId.ToString();
            data.Rows.Add(row);
            dic.Add(rd.ReviewId, _review);
        }
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + lbl_NoResult.Text + "');", true);
        }
        BindData(data);
        dataV = data;
       
    }
    protected void BindData(DataTable data)
    {
        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;
        objPDS.PageSize = ITEM_PAGE;
        objPDS.DataSource = data.DefaultView;

        if (data.Rows.Count > 0)
        {

            objPDS.CurrentPageIndex = CurrentPage;
            lnkPreviousPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkNextPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            LoadPages(data.Rows.Count);
            _reapeter.DataSource = objPDS;
            _reapeter.DataBind();

            div_paging.Visible = true;
            //       PanelResults.Visible = true;
        }
        else
        {
            CleanPager();
        }
        _uPanel.Update();

    }
    void CleanPager()
    {
        //      xmlV = string.Empty;
        PlaceHolderPages.Controls.Clear();
        div_paging.Visible = false;
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        _uPanel.Update();
    }
    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        //if there is only one page
        if (countPages == 1)
        {
            div_paging.Visible = false;
            //       PageListV = null;
            return;
        }

        Dictionary<string, int> dic = new Dictionary<string, int>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add("...", indx);
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add("" + i, i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add("...", indx);
        }
        //       PageListV = dic;
        foreach (KeyValuePair<string, int> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
    }
    HtmlTableCell MakeCell(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();
        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }
    protected void lnkNextPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;
        //     listTR = new List<string>();
        BindData(dataV);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;
        //    listTR = new List<string>();
        BindData(dataV);
    }
    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
        //      listTR = new List<string>();
        BindData(dataV);
    }
    /*
    protected void lb_Create_click(object sender, EventArgs e)
    {
        string ReviewId = ((LinkButton)sender).CommandArgument;
        lbl_CompleteReview.Text = ReviewsV[new Guid(ReviewId)];
        ReviewDescription_popup.Attributes["class"] = "popModal_del";
        _UpdatePanelPopup.Update();
        _mpe.Show();
    }
     * */
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    int CurrentPage
    {
        get
        {
            //Look for current page in ViewState 
            object o = this.ViewState["_CurrentPageReview"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["_CurrentPageReview"] = value;
        }
    }
    /*
    Dictionary<Guid, string> ReviewsV
    {
        get { return (ViewState["Reviews"] == null) ? new Dictionary<Guid, string>() : (Dictionary<Guid, string>)ViewState["Reviews"]; }
        set { ViewState["Reviews"] = value; }
    }
     * */
}
