using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.Web.Services.Protocols;
using System.Text;

public partial class Professional_professionChooseProfessions : PageSetting
{
    public string lang = "eng";
    public List<string> allReadyCertificate = new List<string>();
    public int indexAllReadyCertificate = -1;
    public string arrCertificatesStr="";
    protected string RegistrationStage = "";
    protected string strExpertisesAllAndExpertiseSpecific = "";
    protected string strShowingCertificates = "";
    protected string userName = "";
    protected string userId = "";
    protected string advertiseId = "";
    protected string advertiserName = "";
    protected string isFirstTimeV = "";
    protected string fieldId = "";
    protected string fieldName = "";
    protected string pageId = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {

                string csnameLogin = "setLogin";
                Type cstypeLogin = this.GetType();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager csLogin = this.Page.ClientScript;

                if (!csLogin.IsClientScriptBlockRegistered(cstypeLogin, csnameLogin))
                {
                    string csTextLogin = "top.location='" + ResolveUrl("~") + @"Management/ProfessionLogin.aspx';";
                    csLogin.RegisterClientScriptBlock(cstypeLogin, csnameLogin, csTextLogin, true);
                }
                return;
            }
            if (userManagement.IsSupplier() && siteSetting.GetSiteID == PpcSite.GetCurrent().ZapSiteId)
                btnSendWebService.Visible = false;
            Session["myGuid"] = GetGuidSetting();
            lbl_page.Text = " 6";
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
            RemoveServerCommentPPC();
        }

        ScriptManager1.RegisterAsyncPostBackControl(btnSendWebService);  
  //      lblHideSpan.Attributes.Add("style", "none"
        //Response.Write("Page load");
        //Response.Write("session:" + (int)Session["ProfileId"]);
        ////tableAddProfession.Style.Add("direction", this.GetLocalResourceObject("tableDirection_" + lang + ".Text").ToString());
        ////btnAdd.Style.Add("direction", this.GetLocalResourceObject("addDirection_" + lang + ".Text").ToString());
        ////btnRemove.Style.Add("direction", this.GetLocalResourceObject("removeDirection_" + lang + ".Text").ToString());
        ////tdFindInput.Style.Add("text-align", this.GetLocalResourceObject("tdFindInputAlign_" + lang + ".Text").ToString());

        //if (!IsPostBack)
        //{
        

        /************* parameters foe audit ****************/
        userName = userManagement.User_Name;
        userId = userManagement.GetGuid;
        
        advertiseId = GetGuidSetting();

        UserMangement um = GetUserSetting();
        advertiserName = um.User_Name;

        fieldId = lblYourChoice.ID;
        fieldName = lblYourChoice.InnerText;
        //Response.Write(fieldName);
        pageId = System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        /************* end parameters foe audit ****************/

            initRegistationStep();
     
            Page.Header.DataBind();
    }
    

    private void setCertificates()
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        //Response.Write("GetGuidSetting():" + GetGuidSetting());
        string xmlExpertisesCertificates = string.Empty;
        try
        {
            xmlExpertisesCertificates = supplier.GetSupplierExpertise(siteSetting.GetSiteID, GetGuidSetting());
            Response.Write("xmlExpertisesCertificates:" + xmlExpertisesCertificates);
        }
        catch (SoapException ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }       

        XmlDocument xml = new XmlDocument();
        xml.LoadXml(xmlExpertisesCertificates);

        if (xml["SupplierExpertise"] == null || xml["SupplierExpertise"]["Error"] != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        XmlNode root = xml.DocumentElement;

        TableRow tr;
        TableCell td;

        CheckBox cbx;
        LiteralControl lcl;
        Label lbl;

        string ifCertificate="false";

        for(int i=0;i<root.ChildNodes.Count;i++)
        {
           
            tr = new TableRow();
            tr.ID = "trCertificates_" + root.ChildNodes[i].Attributes["Name"].Value;

            //Response.Write(tr.ProfessionalGuid + "<br>");
            td = new TableCell();

            cbx = new CheckBox();

            cbx.ID = "chkCertificates_" + root.ChildNodes[i].Attributes["Name"].Value.Replace("\"", "&quot;");
            
            arrCertificatesStr += root.ChildNodes[i].Attributes["Name"].Value.Replace("\"", "&quot;") + ",";
            //arrCertificatesStr += root.ChildNodes[i].Attributes["Name"].Value.Replace("\'", "\\'") + ",";

            
            ifCertificate = root.ChildNodes[i].Attributes["Certificate"].Value;
            if (ifCertificate.ToLower()=="true")
                cbx.Checked = true;
            else
                cbx.Checked = false;

            td.Controls.Add(cbx);

            
            //lcl = new LiteralControl("I am a certified " + root.ChildNodes[i].Attributes["Name"].Value);
            lbl = new Label();
            lbl.Text = " " + lblHideSpan.Value + " " + root.ChildNodes[i].Attributes["Name"].Value;
            td.Controls.Add(lbl);

            tr.Cells.Add(td);
            
            tblCertificates.Rows.Add(tr);
            
        }
        //Hide/Show "Your Certification" if not exists segment        
        pan_certification.Attributes["style"] = (tblCertificates.Rows.Count > 1) ? "visibility:visible" : "visibility:hidden";


    }
    

    /*
    private void deleteCertificates()
    {
        //Response.Write("<br>deleteCertificates()<br>");
        ProfileProfessions.deleteProfileCertificate((int)Session["ProfileId"]);
        setCertificates();
    }
    */

    /*
    private void setRegions()
    {


        List<string> listCities = Cities.getRegionsDomestic();
        Table tableRegions = new Table();
        tableRegions.Attributes.Add("align", "center");
        tableRegions.Width = Unit.Percentage(100);
        TableRow rowRegions = null;
        TableCell cellRegions = null;
        CheckBox checkbox;
        int index = -1;
        for (int i = 0; i < listCities.Count; i++)
        {

            if (listCities[i].Length > 0)
            {
                index++;

                if (index % 5 == 0)
                {
                    rowRegions = new TableRow();
                    tableRegions.Rows.Add(rowRegions);
                }

                cellRegions = new TableCell();
                cellRegions.Style.Add("direction", this.GetLocalResourceObject("tableDirection_" + lang + ".Text").ToString());
                cellRegions.Style.Add("text-align", this.GetLocalResourceObject("alignGeneral_" + lang + ".Text").ToString());

                checkbox = new CheckBox();
                checkbox.ID = "chk_" + listCities[i].ToString().Replace("\"", "&quot;");

                checkbox.Text = listCities[i].ToString();

                if (ifRegionAlreadyChoosed(listCities[i].ToString()))
                    checkbox.Checked = true;
                //checkbox.EnableViewState = false;

                //cellRegions.Controls.Add(checkbox);
               // rowRegions.Cells.Add(cellRegions);
            }

        }

        ph_regions.Controls.Add(tableRegions);
    }
    */

   
    protected void btnSend_Click(object sender, EventArgs e)
    {
        /*
        //Response.Write("<br>btnSend_Click<br>");
        //Response.Write("hidden_professions:" + hidden_professions.Value);
        string[] strProfessions;
        strProfessions = hidden_professions.Value.Split('&');
        //Response.Write("<br>strProfessions:" + strProfessions + "<br>");
        ProfileProfessions.deleteProfileProfession((int)Session["ProfileId"]);
        //Response.Write("hidden_professions.Value:" + hidden_professions.Value);
        if (hidden_professions.Value != "")
        {
            for (int i = 0; i < strProfessions.Length; i++)
            {
                ProfileProfessions.insertProfileProfession((int)Session["ProfileId"], strProfessions[i]);
            }
        }
        

        string regionsArray = "";
        ProfileProfessions.deleteProfileRegion((int)Session["ProfileId"]);
        ProfileProfessions.deleteProfileCertificate((int)Session["ProfileId"]);

        foreach (string thing in Request.Form)
        {
            //Response.Write("<br>" + thing + " " + Request.Form[thing]);
            if (thing.IndexOf("chk_") >= 0)
            {                
                regionsArray += thing.Substring(thing.IndexOf("_") + 1) + " ";
               // ProfileProfessions.insertProfileRegion((int)Session["ProfileId"], thing.Substring(thing.IndexOf("_") + 1));
            }

            if (thing.IndexOf("chkCertificates") >= 0)
            {
                //Response.Write(thing + " " + Request.Form[thing] + "<br>");
                ProfileProfessions.insertProfileCertificate((int)Session["ProfileId"], thing.Substring(thing.IndexOf("_") + 1));

            }


        }

       // setRegions();
        setCertificates();
      */

    }

    private void initRegistationStep()
    {
        WebReferenceSite.Site site = WebServiceConfig.GetSiteReference(this);
        string xmlProfessions = string.Empty;
        

        try
        {
            //xmlProfessions = supplier.GetSupplierExpertise(siteSetting.GetSiteID, GetGuidSetting());
            xmlProfessions = site.GetAllExpertises(siteSetting.GetSiteID, GetGuidSetting());
            //Response.Write("xmlProfessions:" + xmlProfessions);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlProfessions);

            if (xml["Expertise"] == null || xml["Expertise"]["Error"] != null)
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }

            StringBuilder sbExpertises = new StringBuilder();
            StringBuilder sbProfessionalExpertises = new StringBuilder();
            StringBuilder sbShowingCertificates = new StringBuilder();
            
            string strExpertises = "";
            string strProfessionalExpertises = "";            

            XmlNode root = xml.DocumentElement; // Expertises
           

            /**************  RegistrationStage Influences ****************/

            RegistrationStage = root.Attributes["RegistrationStage"].InnerText;
            //RegistrationStage = "1";
            string csnameStep = "setStep";
            Type cstypeStep = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager csStep = this.Page.ClientScript;


            if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
            {
                string csTextStep = "witchAlreadyStep(" + RegistrationStage + ");";
                csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
            }

            if (int.Parse(RegistrationStage) >= 2)
            {
                btnSendWebService.Value = lbl_btn_Update.Text;
                btnSendWebService.Attributes.Add("title", "Update");
                pSteps.Visible = false;
                isFirstTimeV = "false";
            }
            else
            {
                btnSendWebService.Value = lbl_btn_Next.Text;
                btnSendWebService.Attributes.Add("title", "Next");
                pSteps.Visible = true;
                isFirstTimeV = "true";
            }

            /**************  End RegistrationStage Influences ****************/

            string ParentName = "";

            TableRow tr;
            TableCell td;

            CheckBox cbx;
            LiteralControl lcl;
            Label lbl;

            string ifCertificate = "false";
            string strCleanExpertiseName = "";
            string strCleanSubExpertiseName = "";

            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++) // element primary
                {
                    if(root.ChildNodes[i].Attributes["ShowCertificate"].InnerText=="True")
                        sbShowingCertificates.Append(root.ChildNodes[i].Attributes["ID"].InnerText + ",");

                    if (root.ChildNodes[i].HasChildNodes)
                    {

                        if (root.ChildNodes[i].Attributes["Selected"].InnerText == "True" )
                        {

                            sbProfessionalExpertises.Append(root.ChildNodes[i].Attributes["Name"].InnerText.Replace("\'", "\\'") + "&");

                            if (root.ChildNodes[i].Attributes["ShowCertificate"].InnerText == "True")
                            {
                                tr = new TableRow();
                                tr.ID = "trCertificates_" + root.ChildNodes[i].Attributes["Name"].Value;

                                //Response.Write(tr.ProfessionalGuid + "<br>");
                                td = new TableCell();

                                cbx = new CheckBox();

                                cbx.ID = "chkCertificates_" + root.ChildNodes[i].Attributes["Name"].Value.Replace("\"", "&quot;");

                                arrCertificatesStr += root.ChildNodes[i].Attributes["Name"].Value.Replace("\"", "&quot;").Replace("\'", "\\'") + ",";


                                ifCertificate = root.ChildNodes[i].Attributes["Certificate"].Value;
                                if (ifCertificate.ToLower() == "true")
                                    cbx.Checked = true;
                                else
                                    cbx.Checked = false;

                                td.Controls.Add(cbx);


                                //lcl = new LiteralControl("I am a certified " + root.ChildNodes[i].Attributes["Name"].Value);
                                lbl = new Label();
                                lbl.Text = " " + lblHideSpan.Value + " " + root.ChildNodes[i].Attributes["Name"].Value;
                                td.Controls.Add(lbl);

                                tr.Cells.Add(td);

                                tblCertificates.Rows.Add(tr);
                            }
                        }

                        for (int y = 0; y < root.ChildNodes[i].ChildNodes.Count; y++)
                        {
                            if (root.ChildNodes[i].Attributes["Selected"].InnerText == "True")
                            {
                                if (root.ChildNodes[i].ChildNodes[y].Attributes["Selected"].InnerText == "True")
                                    sbProfessionalExpertises.Append(root.ChildNodes[i].Attributes["Name"].InnerText.Replace("\'", "\\'") + " >> " + root.ChildNodes[i].ChildNodes[y].InnerText.Replace("\'", "\\'") + "&");




                            }

                        strCleanExpertiseName = root.ChildNodes[i].Attributes["Name"].InnerText.Replace("\'", "\\'");
                        strCleanExpertiseName=strCleanExpertiseName.Replace("=","shaveshave");
                        strCleanSubExpertiseName = root.ChildNodes[i].ChildNodes[y].InnerText.Replace("\'", "\\'");
                        strCleanSubExpertiseName = strCleanSubExpertiseName.Replace("=", "shaveshave");

                        sbExpertises.Append(strCleanExpertiseName + " >> "
                             + strCleanSubExpertiseName  + "=" +
                             root.ChildNodes[i].Attributes["ID"].InnerText + "*" +
                             root.ChildNodes[i].ChildNodes[y].Attributes["ID"].InnerText + "&");
                        }
                    }

                    else
                    {
                        if (root.ChildNodes[i].Attributes["Selected"].InnerText == "True")
                        {
                            sbProfessionalExpertises.Append(root.ChildNodes[i].Attributes["Name"].InnerText.Replace("\'", "\\'") + "&");

                            if (root.ChildNodes[i].Attributes["ShowCertificate"].InnerText == "True")
                            {
                                tr = new TableRow();
                                tr.ID = "trCertificates_" + root.ChildNodes[i].Attributes["Name"].Value;

                                //Response.Write(tr.ProfessionalGuid + "<br>");
                                td = new TableCell();

                                cbx = new CheckBox();

                                cbx.ID = "chkCertificates_" + root.ChildNodes[i].Attributes["Name"].Value.Replace("\"", "&quot;");

                                arrCertificatesStr += root.ChildNodes[i].Attributes["Name"].Value.Replace("\"", "&quot;").Replace("\'", "\\'") + ",";


                                ifCertificate = root.ChildNodes[i].Attributes["Certificate"].Value;
                                if (ifCertificate.ToLower() == "true")
                                    cbx.Checked = true;
                                else
                                    cbx.Checked = false;

                                td.Controls.Add(cbx);


                                //lcl = new LiteralControl("I am a certified " + root.ChildNodes[i].Attributes["Name"].Value);
                                lbl = new Label();
                                lbl.Text = " " + lblHideSpan.Value + " " + root.ChildNodes[i].Attributes["Name"].Value;
                                td.Controls.Add(lbl);

                                tr.Cells.Add(td);

                                tblCertificates.Rows.Add(tr);
                            }
                        }

                        strCleanExpertiseName = root.ChildNodes[i].Attributes["Name"].InnerText.Replace("\'", "\\'");
                        strCleanExpertiseName=strCleanExpertiseName.Replace("=","shaveshave");
                        sbExpertises.Append(strCleanExpertiseName + "=" + 
                            root.ChildNodes[i].Attributes["ID"].InnerText + "&");
                    }


                }

                pan_certification.Attributes["style"] = (tblCertificates.Rows.Count > 1) ? "visibility:visible" : "visibility:hidden";


                strExpertises = sbExpertises.ToString().Substring(0, sbExpertises.ToString().Length - 1);

                if (sbProfessionalExpertises.ToString().Length != 0)
                    strProfessionalExpertises = sbProfessionalExpertises.ToString().Substring(0, sbProfessionalExpertises.ToString().Length - 1);
                else
                    strProfessionalExpertises = "empty";

                strExpertisesAllAndExpertiseSpecific = strExpertises + "*****" + strProfessionalExpertises;

                if (sbShowingCertificates.ToString().Length != 0)
                    strShowingCertificates = sbShowingCertificates.ToString().Substring(0, sbShowingCertificates.ToString().Length - 1);
            }

            else
            {
                strExpertisesAllAndExpertiseSpecific = "empty*****empty";
                strShowingCertificates = "empty";
            }

            //Response.Write(strExpertisesAllAndExpertiseSpecific);
            //Response.Write("strShowingCertificates: " + strShowingCertificates);
            //Response.Write("<br>strRegionsAllAndRegionsSpecific:" + strRegionsAllAndRegionsSpecific);

        }

        catch (SoapException ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
 
    }

    
    
   
}
