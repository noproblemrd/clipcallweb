﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Globalization;
using System.Xml.Linq;
using System.Linq;
using System.Web;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public partial class Management_OverView2 : PageSetting, ICallbackEventHandler
{
    protected const int ITEM_PAGE = 10;
    protected const int PAGE_PAGES = 10;
    private const string RECORD_KEY = "RecordCalls";
    const string REPORT_NAME = "CallsReport";
    protected string PAGE_NAME = PagesNames.MiniSite.ToString();
    string CallBackVal = "";
    protected bool ifRunReport = false;
    protected Dictionary<Guid, string> dicRecords;
    private int _bonusAmount;

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        if (userManagement.IsPublisher())
            this.MasterPageFile = "../Publisher/MasterPageFrame.master";
        else if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
        {
            this.MasterPageFile = ResolveUrl("~") + "Controls/MasterPagePPC2.master";

        }
    }

    /*
    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {
        // http://blogs.msdn.com/b/tom/archive/2008/03/14/validation-of-viewstate-mac-failed-error.aspx
        System.IO.StringWriter stringWriter =
            new System.IO.StringWriter();
        HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);
        base.Render(htmlWriter);
        string html = stringWriter.ToString();
        string[] aspnet_formelems = new string[5];
        aspnet_formelems[0] = "__EVENTTARGET";
        aspnet_formelems[1] = "__EVENTARGUMENT";
        aspnet_formelems[2] = "__VIEWSTATE";
        aspnet_formelems[3] = "__EVENTVALIDATION";
        aspnet_formelems[4] = "__VIEWSTATEENCRYPTED";
        foreach (string elem in aspnet_formelems)
        {
            //Response.Write("input type=""hidden"" name=""" & abc.ToString & """")
            int StartPoint = html.IndexOf("<input type=\"hidden\" name=\"" +
              elem.ToString() + "\"");
            if (StartPoint >= 0)
            {
                //does __VIEWSTATE exist?
                int EndPoint = html.IndexOf("/>", StartPoint) + 2;
                string ViewStateInput = html.Substring(StartPoint,
                  EndPoint - StartPoint);
                html = html.Remove(StartPoint, EndPoint - StartPoint);
                int FormStart = html.IndexOf("<form");
                int EndForm = html.IndexOf(">", FormStart) + 1;
                if (EndForm >= 0)
                    html = html.Insert(EndForm, ViewStateInput);
            }
        }

        writer.Write(html);
    }
    */

    protected void Page_Load(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(inviteBtn);

        SetupClient();
        SetToolboxEvents();
        dicRecords = new Dictionary<Guid, string>();
        // ScriptManager.RegisterStartupScript(this, this.GetType(), "test", "updateSystemMessage('ref effr <a href=\"javascript:void(0);\">link links</a> etgregt rtgreg');", true);
        if (!IsPostBack)
        {

            if (!ifAutorized())
            {
                Response.Redirect(featureListingLink);
            }

            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);

            if (Master is Controls_MasterPagePPC2)
            {
                Master.TurnOnOffServerComment(false);
            }
            _Steps.supplier = supplier;
            _Steps.supplierGuid = GetGuidSetting();
            _Steps.siteId = siteSetting.GetSiteID;

            if (Session["ifEnableRecord"] != null)
                Session.Remove("ifEnableRecord");

            stepListenClientId = _Steps.FindControl("stepListen").ClientID;
            circleStepListenClientId = _Steps.FindControl("circleStepListen").ClientID;

            //setLableTime("Since the beginning of time");
            setLableTime("Last month");
            showLeads(supplier, WebReferenceSupplier.MyLeadsListSortBy.Dates, true);
            //showTestCall(supplier);
            //GetTestCallForReport(Guid supplierId)
            clientIdBalance = this.Master.FindControl("lbl_Balance").ClientID;
            RemoveSiteMap();
            SetToolbox();
            LoadRefundStatus();
            LoadAllowedToRecord();



        }

        Page.Header.DataBind(); // not Page.DataBind() because the controls inside datalist will not fire


        checkSessionSize();

        //Response.Write("totalSessionBytes:" + totalSessionBytes);

    }

    private bool ifAutorized()
    {
        bool ifLeadBooster;
        string plan = LoadPlan(new Guid(GetGuidSetting()));

        if (plan == "FEATURED_LISTING")        
            ifLeadBooster = false;            
        
        else
            ifLeadBooster = true;

        return ifLeadBooster;
    }

    private string LoadPlan(Guid SupplierId)
    {
        string plan = "";

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);        
        WebReferenceSupplier.ResultOfString resultOfString = new WebReferenceSupplier.ResultOfString();

        try
        {
            resultOfString = _supplier.Checkout_GetPlan_Registration2014(SupplierId);
            plan = resultOfString.Value;
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);

        }

        return plan;

    }
    private void RemoveSiteMap()
    {
        Master.SetOffBreadCrumb();
    }

    private void checkSessionSize()
    {
        long totalSessionBytes = 0;
        BinaryFormatter b = new BinaryFormatter();
        System.IO.MemoryStream m;
        foreach (var obj in Session)
        {
            m = new MemoryStream();
            b.Serialize(m, obj);
            totalSessionBytes += m.Length;
        }
    }

    private void SetToolbox()
    {
        _Toolbox.SetTitle(lblTitleAnalytics.Text);
    }
    void SetToolboxEvents()
    {
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
    }

    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordToPrint) + "');", true);
            return;
        }


        Session["data_print"] = dataV;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordExport) + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, REPORT_NAME);
        /* need to fix
        if (!te.ExecExcel(GetExcelTable(dataV)))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
        */
    }
    DataTable GetExcelTable(DataTable data)
    {
        Dictionary<eYesNo, string> dic = new Dictionary<eYesNo, string>();
        foreach (eYesNo s in Enum.GetValues(typeof(eYesNo)))
        {
            string tran = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", s.ToString());
            dic.Add(s, tran);
        }
        DataTable NewData = data.Copy();
        NewData.Columns.Remove("Recording");
        //      NewData.Columns.Remove("HasRecord");
        NewData.Columns.Remove("InRefundStatus");
        //     NewData.Columns.Remove("StatusPic");
        NewData.Columns.Add("Charged");
        NewData.Columns.Remove("CallId");
        NewData.Columns.Remove("RefundStatus");
        foreach (DataRow row in NewData.Rows)
        {
            row["Win"] = (row["Win"].ToString() == ResolveUrl("~") + "Publisher/images/icon-v.png") ? dic[eYesNo.Yes] : dic[eYesNo.No];
            row["Charged"] = (row["StatusPic"].ToString() == ResolveUrl("~") + "Publisher/images/icon-v.png") ? dic[eYesNo.Yes] :
                ((row["StatusPic"].ToString() == ResolveUrl("~") + "Publisher/images/icon-watch.png") ? RefundStatusV[WebReferenceSite.RefundSatus.Pending.ToString()] : dic[eYesNo.No]);
        }
        NewData.Columns.Remove("StatusPic");
        return NewData;

    }
    private void LoadRefundStatus()
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        foreach (string s in Enum.GetNames(typeof(WebReferenceSupplier.RefundSatus)))
        {
            dic.Add(s, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "RefundSatus", s));
        }
        RefundStatusV = dic;
    }

    private void LoadAllowedToRecord()
    {
        /*
        if (userManagement.IsPublisher())
        {
            AllowedToRecord = false;
            return;
        }
         * */
        IAsyncResult arConfigSite;
        string result = string.Empty;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        try
        {
            bool PublishCanHear = true;
            arConfigSite = _site.BeginGetConfigurationSettings(null, null);
            if (userManagement.IsPublisher())
            {
                PublishCanHear = PublisherAllowToRecord();
            }
            result = _site.EndGetConfigurationSettings(arConfigSite);
            if (!PublishCanHear)
            {
                AllowedToRecord = false;
                return;
            }
            XDocument xdd = XDocument.Parse(result);
            if (xdd.Element("Configurations") == null || xdd.Element("Configurations").Element("Error") != null)
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }

            AllowedToRecord = ((from p in xdd.Element("Configurations").Elements()
                                where p.Element("Key").Value == RECORD_KEY
                                select p).FirstOrDefault().Element("Value").Value) == "1";
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }


    }
    bool PublisherAllowToRecord()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfBoolean result = null;
        result = _site.IsCanHearAllRecordings(userManagement.Get_Guid);
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return false;
        }
        return result.Value;
    }

    protected void showLeads(WebReferenceSupplier.Supplier supplier, WebReferenceSupplier.MyLeadsListSortBy sortBy, bool sortDescending)
    {
        /*
        placeHolderCalls.Controls.Clear();
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        */
        /*
        DateTime from_date = FromToDate1.GetDateFrom;
        DateTime to_date = FromToDate1.GetDateTo;
        
        if (to_date == DateTime.MinValue)
        {
            to_date = DateTime.Now;
        }
        if (from_date == DateTime.MinValue)
        {
            from_date = to_date.AddMonths(-1);
        }
        else
        {
            if (from_date > to_date)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + HttpUtility.JavaScriptStringEncode(FromToDate1.GetDateError) + "');", true);

                return;
            }
        }
        */
        WebReferenceSupplier.GetMyLeadsListRequest getMyleadsRequest = new WebReferenceSupplier.GetMyLeadsListRequest();

        switch (timeSelected.InnerText.ToLower())
        {
            case "since the beginning of time":
                getMyleadsRequest.Period = WebReferenceSupplier.MyLeadsListPeriod.All;
                Period = "since the beginning of time";
                break;

            case "last month":
                getMyleadsRequest.Period = WebReferenceSupplier.MyLeadsListPeriod.Month;
                Period = "month";
                break;

            case "last week":
                getMyleadsRequest.Period = WebReferenceSupplier.MyLeadsListPeriod.Week;
                Period = "week";
                break;

            case "last year":
                getMyleadsRequest.Period = WebReferenceSupplier.MyLeadsListPeriod.Year;
                Period = "year";
                break;

            default:
                getMyleadsRequest.Period = WebReferenceSupplier.MyLeadsListPeriod.Month;
                break;
        }

        getMyleadsRequest.SortBy = sortBy;
        getMyleadsRequest.SortDescending = sortDescending;
        getMyleadsRequest.SupplierId = new Guid(GetGuidSetting());

        /*
         [WebMethod]
        public Result<DataModel.AdvertiserDashboards.GetMyLeadsResponse> GetMyLeadsList(DataModel.AdvertiserDashboards.GetMyLeadsListRequest request)


        public class GetMyLeadsResponse
            {
                public bool IsRecordsCalls { get; set; }
                public List<AdvDashboardLeadData> LeadsList { get; set; }
            }

        */

        WebReferenceSupplier.ResultOfGetMyLeadsResponse getmyleadsResponse = new WebReferenceSupplier.ResultOfGetMyLeadsResponse();


        try
        {
            getmyleadsResponse = supplier.GetMyLeadsList(getMyleadsRequest);

            if (getmyleadsResponse.Type == WebReferenceSupplier.eResultType.Success)
            {
                WebReferenceSupplier.AdvDashboardLeadData[] advDashboardLeadData = getmyleadsResponse.Value.LeadsList;
                leadsExist = getmyleadsResponse.Value.LeadsExist;
                ifEnableRecord = getmyleadsResponse.Value.IsRecordsCalls;
                IsLeadBroker = getmyleadsResponse.Value.IsLeadBroker;

                CurrentPage = 0;
                //LoadCallUsing2(advDashboardLeadData, from_date, to_date);                    

                BindData(advDashboardLeadData);
                dataV = advDashboardLeadData;

                //lbl_RecordMached.Text = "(" + advDashboardLeadData.Length + " " + lblRecordMached.Text + ")";
                _uPanel.Update();
            }

            else
            {
                dbug_log.ExceptionLog(new Exception(), "failed display list leads site: " + siteSetting.GetSiteID);
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }



    }

    protected void showTestCall(WebReferenceSupplier.Supplier supplier)
    {

        WebReferenceSupplier.ResultOfAdvDashboardLeadData resultOfAdvDashboardLeadData = new WebReferenceSupplier.ResultOfAdvDashboardLeadData();
        resultOfAdvDashboardLeadData = supplier.GetTestCallForReport(new Guid(GetGuidSetting()));

    }

    protected void BindData(WebReferenceSupplier.AdvDashboardLeadData[] advDashboardLeadData)
    {
        placeHolderCalls.Controls.Clear();
        datalistLeads.Attributes.Add("cellpadding", "0px");
        datalistLeads.DataSource = null;
        datalistLeads.DataBind();
        //datalistLeads.
        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;
        item_page_accumulate = item_page_accumulate + ITEM_PAGE;

        objPDS.PageSize = item_page_accumulate;

        objPDS.DataSource = advDashboardLeadData;

        //bool debug = true;

        if (advDashboardLeadData.Length > 0) // there are leads for this specific period    
        //if (!debug)
        {
            objPDS.CurrentPageIndex = CurrentPage;
            lnkPreviousPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkNextPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            if (lnkNextPage.Enabled)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(false);", true);
            LoadPages(advDashboardLeadData.Length);

            datalistLeads.DataSource = objPDS;
            datalistLeads.DataBind();

            div_paging.Visible = true;

            leadsSeperator.Visible = true;
            noRecords.Visible = false;
            leadsTime.Visible = true;
        }
        else // there is no leads for this specific period
        {
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            WebReferenceSupplier.ResultOfGetStartedTasksStatusResponse resultOfGetStartedTasksStatusResponse = supplier.RetrieveGetStartedTasksStatus(new Guid(GetGuidSetting()));

            WebReferenceSupplier.GetStartedTasksStatusResponse getStartedTasksStatusResponse = resultOfGetStartedTasksStatusResponse.Value;
            WebReferenceSupplier.GetStartedTasksStatus getStartedTasksStatus = getStartedTasksStatusResponse.CallTest;

            //leadsExist = true;

            if (leadsExist) // there are leads before (not check if include test call)
            {
                div_paging.Visible = true;
                noRecords.Visible = true;
                noRecords.InnerHtml = "There are no leads this " + Period;

                leadsSeperator.Visible = false;
                leadsTime.Visible = true;

                if (getStartedTasksStatus.Done == true) // check if at least it has test call before
                {
                    WebReferenceSupplier.ResultOfAdvDashboardLeadData resultOfAdvDashboardLeadData = new WebReferenceSupplier.ResultOfAdvDashboardLeadData();
                    resultOfAdvDashboardLeadData = supplier.GetTestCallForReport(new Guid(GetGuidSetting()));

                    if (resultOfAdvDashboardLeadData.Value != null) // it is null when 2 options: a. no call yet  b. already close call by x
                    {
                        objPDS.CurrentPageIndex = CurrentPage;
                        lnkPreviousPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
                        lnkNextPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
                        lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
                        lnkNextPage.Enabled = !objPDS.IsLastPage;
                        if (lnkNextPage.Enabled)
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(false);", true);
                        LoadPages(advDashboardLeadData.Length);

                        datalistLeads.DataSource = objPDS;
                        datalistLeads.DataBind();

                        leadsSeperator.Visible = true;
                        noRecords.Visible = false;
                    }
                }

            }

            else if (getStartedTasksStatus.Done == true) // there are no leads before but it has tesct call in case the supplier made test call should show the leads table. because the test call is in the header of it
            {
                objPDS.CurrentPageIndex = CurrentPage;
                lnkPreviousPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
                lnkNextPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
                lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
                lnkNextPage.Enabled = !objPDS.IsLastPage;
                if (lnkNextPage.Enabled)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(false);", true);
                LoadPages(advDashboardLeadData.Length);

                datalistLeads.DataSource = objPDS;
                datalistLeads.DataBind();

                div_paging.Visible = true;
                noRecords.Visible = false;

                leadsSeperator.Visible = false;
                leadsTime.Visible = false;
            }

            else
            {
                CleanPager();
                noRecords.Visible = true;
                noRecords.InnerHtml = "Your leads will appear here...";

                leadsSeperator.Visible = false;
                leadsTime.Visible = false;
            }


        }

        _uPanel.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ResetWin", "ResetWin();", true);
    }



    void CleanPager()
    {

        PlaceHolderPages.Controls.Clear();
        div_paging.Visible = false;
        lbl_RecordMached.Text = string.Empty;
        ////_reapeter.DataSource = null;
        ////_reapeter.DataBind();
        _uPanel.Update();
    }


    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        //if there is only one page
        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListV = null;
            return;
        }

        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListV = dic;
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell(kvp.Value, kvp.Key));
    }

    private string GetHtmlObject(string _path)
    {

        StringBuilder sb = new StringBuilder();
        sb.Append(@"<object data=""../play/dewplayer.swf"" width=""200"" height=""20"" name=""dewplayer"" id=""dewplayer"" type=""application/x-shockwave-flash"">");
        sb.Append(@"<param name=""movie"" value=""../play/dewplayer.swf"" />");
        sb.Append(@"<param name=""flashvars"" value=""mp3=" + _path + @"&amp;autostart=1"" />");
        sb.Append(@"<param name=""wmode"" value=""transparent"" />");
        //      sb.Append(@"<embed wmode=""transparent"" src=""flash/home.swf" width="220" height="271" /> 
        sb.Append(@"</object>");
        return sb.ToString();

    }
    /*
    private string setWaiting(string _xml)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append(@"<div id=""chartdiv2"" align=""center""></div>");

        string script = @"LoadChart(""" + _xml + @""",""chartdiv2"", """ +
            ResolveUrl("~") + @"Management/FusionCharts/Charts/Line.swf"");";

        LiteralControl litcon = new LiteralControl(sb.ToString());
        placeHolderCalls.Controls.Add(litcon);
        return script;



    }
     * */
    HtmlTableCell MakeCell(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();
        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }

    int NumOfCallInOneDay(DataTable data, DateTime date)
    {
        int index = 0;
        for (int i = 0; i < data.Rows.Count; i++)
        {
            DateTime dt = ConvertToDateTime.CalanderToDateTime(data.Rows[i]["createdon"].ToString(), siteSetting.DateFormat);
            if (dt.Date.CompareTo(date.Date) == 0)
                index++;
        }
        return index;
    }

    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        return CallBackVal;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        if (eventArgument.StartsWith("[0]"))
            SetListTR(eventArgument);
        else if (eventArgument.StartsWith("[1]"))
            SetChart();

    }
    void SetListTR(string eventArgument)
    {
        eventArgument = eventArgument.Replace("[0]", "");
        List<string> list = listTR;
        string[] args = eventArgument.Split(';');
        if (args[1] == "1")
            list.Add(args[0]);
        else
            list.Remove(args[0]);
        listTR = list;
        CallBackVal = string.Empty;
    }
    void SetChart()
    {

    }

    #endregion
    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }

    protected void lnkNextPage_Click(object sender, EventArgs e)
    {
        /*
        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;
        listTR = new List<string>();
        */
        BindData(dataV);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;
        listTR = new List<string>();
        BindData(dataV);
    }
    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
        listTR = new List<string>();
        BindData(dataV);
    }

    protected void datalistLeads_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            //if (1 == 2)
            //{
            if (dicRecords != null)
                dicRecords.Clear();

            HtmlGenericControl divTime = e.Item.FindControl("divTime") as HtmlGenericControl;
            HtmlGenericControl divStatus = e.Item.FindControl("divStatus") as HtmlGenericControl;
            HtmlGenericControl divDescription = e.Item.FindControl("divDescription") as HtmlGenericControl;

            HtmlAnchor divRecordCalls = e.Item.FindControl("recordCalls") as HtmlAnchor;

            //divRecordCalls.Attributes.Add("style", "color:red");

            HtmlGenericControl backgroundArrowStatus = e.Item.FindControl("backgroundArrowStatus") as HtmlGenericControl;
            HtmlGenericControl backgroundArrowDescription = e.Item.FindControl("backgroundArrowDescription") as HtmlGenericControl;
            HtmlGenericControl backgroundArrowTime = e.Item.FindControl("backgroundArrowTime") as HtmlGenericControl;

            LinkButton lb_status = e.Item.FindControl("lbStatus") as LinkButton;
            LinkButton lb_description = e.Item.FindControl("lbDescription") as LinkButton;
            LinkButton lb_time = e.Item.FindControl("lbTime") as LinkButton;

            if (sortby == WebReferenceSupplier.MyLeadsListSortBy.Status)
            {
                divStatus.Attributes.Add("class", "leadHeader");
                divDescription.Attributes.Add("class", "leadHeaderRegular");
                divTime.Attributes.Add("class", "leadHeaderRegular");
                divRecordCalls.Attributes.Add("class", "linkTitleRecordCalls linkTitleRecordCallsChecked");
                //        divRecordCalls.CssClass = "linkTitleRecordCallsHover";                             

                if (sortDescending)
                {
                    divStatus.Attributes.Add("class", "leadHeader");
                    backgroundArrowStatus.Attributes.Add("class", "spanLeadsTdHeadStatusDesc");

                    lb_description.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowDescription.ClientID + "','spanLeadsTdHeadTitleHoverDown')");
                    lb_description.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowDescription.ClientID + "','')");

                    lb_time.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowTime.ClientID + "','spanLeadsTdHeadTitleHoverDown')");
                    lb_time.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowTime.ClientID + "','')");
                }
                else
                {
                    divStatus.Attributes.Add("class", "leadHeader");
                    backgroundArrowStatus.Attributes.Add("class", "spanLeadsTdHeadStatusAsc");

                    lb_description.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowDescription.ClientID + "','spanLeadsTdHeadTitleHoverUp')");
                    lb_description.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowDescription.ClientID + "','')");

                    lb_time.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowTime.ClientID + "','spanLeadsTdHeadTitleHoverUp')");
                    lb_time.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowTime.ClientID + "','')");

                }



            }


            else if (sortby == WebReferenceSupplier.MyLeadsListSortBy.Heading)
            {

                divDescription.Attributes.Add("class", "leadHeader");
                divStatus.Attributes.Add("class", "leadHeaderRegular");
                divTime.Attributes.Add("class", "leadHeaderRegular");
                divRecordCalls.Attributes.Add("class", "linkTitleRecordCalls");

                if (sortDescending)
                {

                    divDescription.Attributes.Add("class", "leadHeader");
                    backgroundArrowDescription.Attributes.Add("class", "spanLeadsTdHeadDescDesc");

                    lb_status.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowStatus.ClientID + "','spanLeadsTdHeadTitleHoverDown');");
                    lb_status.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowStatus.ClientID + "','');");

                    lb_time.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowTime.ClientID + "','spanLeadsTdHeadTitleHoverDown')");
                    lb_time.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowTime.ClientID + "','')");
                }

                else
                {
                    divDescription.Attributes.Add("class", "leadHeader");
                    backgroundArrowDescription.Attributes.Add("class", "spanLeadsTdHeadDescAsc");

                    lb_status.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowStatus.ClientID + "','spanLeadsTdHeadTitleHoverUp');");
                    lb_status.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowStatus.ClientID + "','');");

                    lb_time.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowTime.ClientID + "','spanLeadsTdHeadTitleHoverUp')");
                    lb_time.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowTime.ClientID + "','')");
                }


            }

            else if (sortby == WebReferenceSupplier.MyLeadsListSortBy.Dates)
            {


                divTime = e.Item.FindControl("divTime") as HtmlGenericControl;
                divStatus.Attributes.Add("class", "leadHeaderRegular");
                divDescription.Attributes.Add("class", "leadHeaderRegular");
                divRecordCalls.Attributes.Add("class", "linkTitleRecordCalls");

                if (sortDescending)
                {

                    backgroundArrowTime.Attributes.Add("class", "spanLeadsTdHeadDetailsDesc");
                    divTime.Attributes.Add("class", "leadHeader");

                    lb_status.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowStatus.ClientID + "','spanLeadsTdHeadTitleHoverDown');");
                    lb_status.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowStatus.ClientID + "','');");

                    lb_description.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowDescription.ClientID + "','spanLeadsTdHeadTitleHoverDown')");
                    lb_description.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowDescription.ClientID + "','')");
                }

                else
                {
                    backgroundArrowTime.Attributes.Add("class", "spanLeadsTdHeadDetailsAsc");
                    divTime.Attributes.Add("class", "leadHeader");

                    lb_status.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowStatus.ClientID + "','spanLeadsTdHeadTitleHoverUp');");
                    lb_status.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowStatus.ClientID + "','');");

                    lb_description.Attributes.Add("onmouseover", "setHeaderLeadTitle('" + backgroundArrowDescription.ClientID + "','spanLeadsTdHeadTitleHoverUp')");
                    lb_description.Attributes.Add("onmouseout", "setHeaderLeadTitle('" + backgroundArrowDescription.ClientID + "','')");
                }



            }

            HtmlAnchor lblRecordCalls = e.Item.FindControl("recordCalls") as HtmlAnchor;

            if (ifEnableRecord == true || (Session["ifEnableRecord"] != null && (bool)Session["ifEnableRecord"] == true))
            {
                lblRecordCalls.Visible = true;
                lblRecordCalls.InnerText = "Stop recording";
                lblRecordCalls.Attributes["onclick"] = "showHideStopRecordCalls(true)";
                //onclick="TurnOnRecordCalls(this);"
            }

            else
            {
                lblRecordCalls.Visible = true;
                lblRecordCalls.InnerText = "Record calls";
                lblRecordCalls.Attributes["onclick"] = "showHideRecordCalls(true)";
            }

            if (2 == 2)
            {
                WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);

                WebReferenceSupplier.ResultOfGetStartedTasksStatusResponse resultOfGetStartedTasksStatusResponse = supplier.RetrieveGetStartedTasksStatus(new Guid(GetGuidSetting()));
                WebReferenceSupplier.GetStartedTasksStatusResponse getStartedTasksStatusResponse = resultOfGetStartedTasksStatusResponse.Value;
                WebReferenceSupplier.GetStartedTasksStatus getStartdTaskStatus = new WebReferenceSupplier.GetStartedTasksStatus();
                getStartdTaskStatus = getStartedTasksStatusResponse.CallTest;
                bool callTestDone = getStartdTaskStatus.Done;

                getStartdTaskStatus = getStartedTasksStatusResponse.ListenRecording;
                bool listenRecordingDone = getStartdTaskStatus.Done;


                if (callTestDone == true)
                {

                    WebReferenceSupplier.ResultOfAdvDashboardLeadData resultOfAdvDashboardLeadData = new WebReferenceSupplier.ResultOfAdvDashboardLeadData();
                    resultOfAdvDashboardLeadData = supplier.GetTestCallForReport(new Guid(GetGuidSetting()));

                    if (resultOfAdvDashboardLeadData.Value != null)
                    {

                        if (resultOfAdvDashboardLeadData.Value.Status == WebReferenceSupplier.AdvDashboardLeadStatus.Won)
                        {
                            /********************** Testcall status *******************/
                            HtmlTableCell tCellTestCall = (HtmlTableCell)e.Item.FindControl("td_statusTestCall");
                            tCellTestCall.Attributes.Add("class", "divLeadsTd");

                            Guid _index;
                            do
                            {
                                _index = Guid.NewGuid();
                                indexJplayerTestCall = _index.ToString();
                            }
                            while (dicRecords.ContainsKey(_index));

                            string _path = resultOfAdvDashboardLeadData.Value.RecordFileLocation;

                            //string refRecording = "javascript:OpenRecord('" + _index + "');";
                            string refRecording = "OpenRecord('" + _index + "');";
                            dicRecords.Add(_index, _path);

                            RecordListV = dicRecords;


                            HtmlGenericControl divJquery_jplayer = new HtmlGenericControl("div");
                            divJquery_jplayer.Attributes.Add("class", "cp-jplayer");
                            divJquery_jplayer.Attributes.Add("ID", "jquery_jplayer_" + _index);

                            HtmlGenericControl dvCp_container = new HtmlGenericControl("div");
                            dvCp_container.Attributes.Add("class", "cp-container");
                            dvCp_container.Attributes.Add("ID", "cp_container_" + _index);
                            dvCp_container.Attributes.Add("onmouseover", "setPlayBackground('" + _index + "','over');");
                            dvCp_container.Attributes.Add("onmouseout", "setPlayBackground('" + _index + "','out');");

                            HtmlGenericControl dvCp_buffer_holder = new HtmlGenericControl("div");
                            dvCp_buffer_holder.Attributes.Add("class", "cp-buffer-holder");
                            dvCp_container.Controls.Add(dvCp_buffer_holder);

                            HtmlGenericControl divCp_buffer_1 = new HtmlGenericControl("div");
                            divCp_buffer_1.Attributes.Add("class", "cp-buffer-1");
                            dvCp_buffer_holder.Controls.Add(divCp_buffer_1);

                            HtmlGenericControl divCp_buffer_2 = new HtmlGenericControl("div");
                            divCp_buffer_2.Attributes.Add("class", "cp-buffer-2");
                            dvCp_buffer_holder.Controls.Add(divCp_buffer_2);

                            HtmlGenericControl divCp_progress_holder = new HtmlGenericControl("div");
                            divCp_progress_holder.Attributes.Add("class", "cp-progress-holder");
                            dvCp_container.Controls.Add(divCp_progress_holder);

                            HtmlGenericControl divCp_progress_1 = new HtmlGenericControl("div");
                            divCp_progress_1.Attributes.Add("class", "cp-progress-1");
                            divCp_progress_holder.Controls.Add(divCp_progress_1);

                            HtmlGenericControl divCp_progress_2 = new HtmlGenericControl("div");
                            divCp_progress_2.Attributes.Add("class", "cp-progress-2");
                            divCp_progress_holder.Controls.Add(divCp_progress_2);

                            HtmlGenericControl divCp_circle_control = new HtmlGenericControl("div");
                            divCp_circle_control.Attributes.Add("class", "cp-circle-control");
                            dvCp_container.Controls.Add(divCp_circle_control);

                            HtmlGenericControl divCp_controls = new HtmlGenericControl("ul");
                            divCp_controls.Attributes.Add("class", "cp-controls");

                            HtmlGenericControl divCp_controlsLi = new HtmlGenericControl("li");

                            //divCp_controlsLi.InnerHtml = "<a id='cpPlay" + _index + "' href=\"#\" class=\"cp-play\" tabindex=\"1\">play</a>";  //change by shai from origin to below
                            divCp_controlsLi.InnerHtml = "<a id='cpPlay" + _index + "' href=\"javascript:void(0)\" class=\"cp-play\" tabindex=\"1\">play</a>"; // change by shai from origin from above

                            divCp_controls.Controls.Add(divCp_controlsLi);

                            HtmlGenericControl divCp_controlsLi2 = new HtmlGenericControl("li");
                            divCp_controlsLi2.InnerHtml = "<a href=\"#\" class=\"cp-pause\" style=\"display:none;\" tabindex=\"1\">pause</a>";
                            divCp_controls.Controls.Add(divCp_controlsLi2);

                            dvCp_container.Controls.Add(divCp_controls);

                            HtmlGenericControl divJp_current_time = new HtmlGenericControl("div");
                            divCp_circle_control.Attributes.Add("class", "jp-current-time");
                            dvCp_container.Controls.Add(divCp_circle_control);

                            HtmlGenericControl divCd_listen = new HtmlGenericControl("div");
                            divCd_listen.Attributes.Add("class", "cd-listen");
                            divCd_listen.InnerText = "LISTEN";
                            dvCp_container.Controls.Add(divCd_listen);
                            divCd_listen.Attributes.Add("title", _index.ToString());

                            HtmlGenericControl divsmall_play = new HtmlGenericControl("div");
                            divsmall_play.Attributes.Add("class", "small-play");

                            dvCp_container.Controls.Add(divsmall_play);

                            string strJPlayerScrip = "";


                            strJPlayerScrip += "$(document).ready(function () {\n";

                            strJPlayerScrip += "indexJplayerTestCall='" + indexJplayerTestCall + "';\n"; // used it method OpenRecord2()

                            if (1 == 2) // instead of initiate all the leads with record i create the player dynamicaly
                            {
                                strJPlayerScrip += "var myCirclePlayer3 = new CirclePlayer(\"#jquery_jplayer_" + _index + "\",\n";
                                strJPlayerScrip += "{\n";
                                strJPlayerScrip += "mp3: \"usa_dev_default.mp3\"\n";
                                strJPlayerScrip += "}, {\n";
                                strJPlayerScrip += "cssSelectorAncestor: \"#cp_container_" + _index + "\",\n";
                                strJPlayerScrip += "swfPath: \"../jPlayer/jQuery.jPlayer.2.1.0.demos/js\",\n";
                                strJPlayerScrip += "supplied: \"mp3\",\n";
                                strJPlayerScrip += " wmode: \"window\"\n";
                                strJPlayerScrip += "});\n";
                            }

                            strJPlayerScrip += "$(\"#cp_container_" + _index + " .cd-listen\").click(function () { // added by shai\n";
                            strJPlayerScrip += "OpenRecord('" + _index + "');\n";
                            strJPlayerScrip += "});\n";


                            strJPlayerScrip += "$(\"#cp_container_" + _index + "\").mouseover(function () { // added by shai\n";
                            strJPlayerScrip += "showHideClockTime('" + _index + "','over');\n";
                            strJPlayerScrip += "});\n";

                            strJPlayerScrip += "$(\"#cp_container_" + _index + "\").click(function () { // added by shai\n";
                            strJPlayerScrip += "OpenRecord('" + _index + "');\n";
                            strJPlayerScrip += "});\n";

                            /*
                            strJPlayerScrip += "$(\"#cp_container_" + _index + " .cp-play\").click(function () { // added by shai\n";
                            strJPlayerScrip += "OpenRecord('" + _index + "');\n";
                            strJPlayerScrip += "});\n";
                            */

                            if (1 == 2) // instead of initiate all the leads with record i create the player dynamicaly
                            {
                                strJPlayerScrip += "$(\"#jquery_jplayer_" + _index + "\").bind($.jPlayer.event.play, function (event) { // Add a listener to report the time play began\n";
                                strJPlayerScrip += "});\n";

                                strJPlayerScrip += "$(\"#jquery_jplayer_" + _index + "\").bind($.jPlayer.event.ended, function (event) { // Add a listener to report the time play began\n";
                                // _step, _supplierGuid, _siteId
                                strJPlayerScrip += "setStepOverView(6,'" + GetGuidSetting() + "','" + siteSetting.GetSiteID + "')\n";
                                strJPlayerScrip += "});\n";

                                strJPlayerScrip += "$(\"#jquery_jplayer_" + _index + "\").bind($.jPlayer.event.error, function (event) { // Add a listener to report the time play began\n";
                                //strJPlayerScrip += "alert(event.jPlayer.error.message);\n";
                                strJPlayerScrip += "});\n";

                                strJPlayerScrip += "$(\"#jquery_jplayer_" + _index + "\").bind($.jPlayer.event.ready, function (event) { // Add a listener to report the time play began\n";
                                //strJPlayerScrip += "alert(\"ready\");\n";
                                strJPlayerScrip += "});\n";

                                strJPlayerScrip += "$(\"#jquery_jplayer_" + _index + "\").bind($.jPlayer.event.warning, function (event) { // Add a listener to report the time play began\n";
                                strJPlayerScrip += "alert(\"warning\");\n";
                                strJPlayerScrip += "});\n";
                            }

                            strJPlayerScrip += "});\n";

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDefaultDates" + _index,
                                     strJPlayerScrip, true);

                            tCellTestCall.Controls.Add(divJquery_jplayer);
                            tCellTestCall.Controls.Add(dvCp_container);

                            /********************** Testcall description/category heading name *******************/

                            HtmlTableCell tCellDescTestCall = (HtmlTableCell)e.Item.FindControl("td_descriptionTestCall");
                            tCellDescTestCall.Attributes.Add("class", "divDescTd");

                            string headingName = resultOfAdvDashboardLeadData.Value.HeadingName;
                            HtmlGenericControl divDesc = new HtmlGenericControl("div");
                            divDesc.Attributes.Add("class", "divDesc");

                            HtmlGenericControl divDescCategory = new HtmlGenericControl("div");
                            divDescCategory.Attributes.Add("class", "divDescCategory");
                            divDescCategory.InnerHtml = "I need " + headingName + "...";

                            divDesc.Controls.Add(divDescCategory);



                            /********************** Testcall description/category content *******************/
                            string descContent = resultOfAdvDashboardLeadData.Value.Description;

                            HtmlGenericControl divDescContent = new HtmlGenericControl("div");
                            divDescContent.Attributes.Add("class", "divDescContent");
                            divDescContent.InnerHtml = descContent;

                            divDesc.Controls.Add(divDescContent);

                            tCellDescTestCall.Controls.Add(divDesc);

                            /************** Testcall Details ************/


                            HtmlGenericControl divDetails = new HtmlGenericControl("div");
                            divDetails.Attributes.Add("class", "divDetails");

                            HtmlTableCell tCellDetailsTestCall = (HtmlTableCell)e.Item.FindControl("td_detailsTestCall");
                            tCellDetailsTestCall.Attributes.Add("class", "divDetailTd");

                            tCellDetailsTestCall.Controls.Add(divDetails);

                            HtmlGenericControl divDetailsX = new HtmlGenericControl("div");
                            divDetailsX.Attributes.Add("class", "divTestCallX");
                            divDetailsX.Attributes.Add("onclick", "closeTestCall('" + GetGuidSetting() + "','" + siteSetting.GetSiteID + "');");


                            if (listenRecordingDone)
                                divDetailsX.Attributes.Add("style", "display:block;");
                            //divDetailsX.ID = "divTestCallX";

                            /************** Details: phone ************/
                            string customerPhone = resultOfAdvDashboardLeadData.Value.CustomerPhone;
                            HtmlGenericControl divDetailsphone = new HtmlGenericControl("div");
                            divDetailsphone.Attributes.Add("class", "divDetailsPhone");
                            customerPhone = customerPhone.Replace("+1", string.Empty);
                            if (customerPhone.Length == 10)
                                customerPhone = "(" + customerPhone.Substring(0, 3) + ") " + customerPhone.Substring(3, 3) + "-" + customerPhone.Substring(6, 4);

                            divDetailsphone.InnerHtml = customerPhone;

                            divDetails.Controls.Add(divDetailsphone);
                            divDetailsphone.Controls.Add(divDetailsX);

                            /************** Details: Region ************/
                            string region = resultOfAdvDashboardLeadData.Value.Region;
                            HtmlGenericControl divDetailsRegion = new HtmlGenericControl("div");
                            divDetailsRegion.Attributes.Add("class", "divDetailsRegion");
                            divDetailsRegion.InnerHtml = region;

                            divDetails.Controls.Add(divDetailsRegion);

                            /************** Details: Time ************/
                            DateTime createOn = resultOfAdvDashboardLeadData.Value.CreatedOn;
                            DateTime CreatedOnSupplierTime = resultOfAdvDashboardLeadData.Value.CreatedOnSupplierTime;

                            HtmlGenericControl divDetailsCreateOn = new HtmlGenericControl("div");
                            divDetailsCreateOn.Attributes.Add("class", "divDetailsCreateOn");

                            TimeSpan diffTime = DateTime.Now - createOn;

                            divDetailsCreateOn.InnerHtml = Utilities.GetTimePassed(createOn, CreatedOnSupplierTime);

                            divDetails.Controls.Add(divDetailsCreateOn);

                            /************** Details: Example lead ************/
                            HtmlGenericControl divDetailsExampleLead = new HtmlGenericControl("div");
                            divDetailsExampleLead.Attributes.Add("class", "divDetailsExampleLead");
                            divDetailsExampleLead.InnerText = "EXAMPLE LEAD";
                            divDetails.Controls.Add(divDetailsExampleLead);

                            tCellDetailsTestCall.Controls.Add(divDetails);

                            HtmlTableCell tdTestCallSeperator = (HtmlTableCell)e.Item.FindControl("testCallSeperator");
                            tdTestCallSeperator.Attributes.Add("class", "td_seperator");

                        }

                    }
                    //}
                }
            }
        }

        if (e.Item.ItemType == ListItemType.Item ||
           e.Item.ItemType == ListItemType.AlternatingItem)
        {

            WebReferenceSupplier.AdvDashboardLeadStatus status = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).Status;


            HtmlGenericControl divStatus = new HtmlGenericControl("div");
            divStatus.Attributes.Add("class", "divStatus");
            divStatus.Attributes.Add("ID", "divStatus" + index);
            index++;

            HtmlGenericControl divStatusText = new HtmlGenericControl("div");
            HtmlGenericControl divStatusTextHover = new HtmlGenericControl("div");

            //if (1 == 2)
            //{

            if (status == WebReferenceSupplier.AdvDashboardLeadStatus.Won)
            {

                divStatusText.Attributes.Add("ID", "divStatusText" + index);
                divStatusText.Attributes.Add("class", "divStatusTextWon");

                Guid _index;
                do
                {
                    _index = Guid.NewGuid();
                }
                while (dicRecords.ContainsKey(_index));

                string _path = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).RecordFileLocation;

                //string refRecording = "javascript:OpenRecord('" + _index + "');";
                string refRecording = "OpenRecord('" + _index + "');";
                dicRecords.Add(_index, _path);

                RecordListV = dicRecords;


                HtmlGenericControl divJquery_jplayer = new HtmlGenericControl("div");
                divJquery_jplayer.Attributes.Add("class", "cp-jplayer");
                divJquery_jplayer.Attributes.Add("ID", "jquery_jplayer_" + _index);

                HtmlGenericControl dvCp_container = new HtmlGenericControl("div");
                dvCp_container.Attributes.Add("class", "cp-container");
                dvCp_container.Attributes.Add("ID", "cp_container_" + _index);
                dvCp_container.Attributes.Add("onmouseover", "setPlayBackground('" + _index + "','over');");
                dvCp_container.Attributes.Add("onmouseout", "setPlayBackground('" + _index + "','out');");

                HtmlGenericControl dvCp_buffer_holder = new HtmlGenericControl("div");
                dvCp_buffer_holder.Attributes.Add("class", "cp-buffer-holder");
                dvCp_container.Controls.Add(dvCp_buffer_holder);

                HtmlGenericControl divCp_buffer_1 = new HtmlGenericControl("div");
                divCp_buffer_1.Attributes.Add("class", "cp-buffer-1");
                dvCp_buffer_holder.Controls.Add(divCp_buffer_1);

                HtmlGenericControl divCp_buffer_2 = new HtmlGenericControl("div");
                divCp_buffer_2.Attributes.Add("class", "cp-buffer-2");
                dvCp_buffer_holder.Controls.Add(divCp_buffer_2);

                HtmlGenericControl divCp_progress_holder = new HtmlGenericControl("div");
                divCp_progress_holder.Attributes.Add("class", "cp-progress-holder");
                dvCp_container.Controls.Add(divCp_progress_holder);

                HtmlGenericControl divCp_progress_1 = new HtmlGenericControl("div");
                divCp_progress_1.Attributes.Add("class", "cp-progress-1");
                divCp_progress_holder.Controls.Add(divCp_progress_1);

                HtmlGenericControl divCp_progress_2 = new HtmlGenericControl("div");
                divCp_progress_2.Attributes.Add("class", "cp-progress-2");
                divCp_progress_holder.Controls.Add(divCp_progress_2);

                HtmlGenericControl divCp_circle_control = new HtmlGenericControl("div");
                divCp_circle_control.Attributes.Add("class", "cp-circle-control");
                dvCp_container.Controls.Add(divCp_circle_control);

                HtmlGenericControl divCp_controls = new HtmlGenericControl("ul");
                divCp_controls.Attributes.Add("class", "cp-controls");

                HtmlGenericControl divCp_controlsLi = new HtmlGenericControl("li");
                //divCp_controlsLi.InnerHtml = "<a id='cpPlay" + _index + "' href=\"#\" class=\"cp-play\" tabindex=\"1\">play</a>";  //change by shai from origin to below
                divCp_controlsLi.InnerHtml = "<a id='cpPlay" + _index + "' href=\"javascript:void(0)\" class=\"cp-play\" tabindex=\"1\">play</a>"; // change by shai from origin from above

                //divCp_controlsLi.InnerHtml = "<a href=\"#\" class=\"cp-play\" tabindex=\"1\">play</a>";
                divCp_controls.Controls.Add(divCp_controlsLi);

                HtmlGenericControl divCp_controlsLi2 = new HtmlGenericControl("li");
                divCp_controlsLi2.InnerHtml = "<a href=\"#\" class=\"cp-pause\" style=\"display:none;\" tabindex=\"1\">pause</a>";
                divCp_controls.Controls.Add(divCp_controlsLi2);

                dvCp_container.Controls.Add(divCp_controls);

                HtmlGenericControl divJp_current_time = new HtmlGenericControl("div");
                divCp_circle_control.Attributes.Add("class", "jp-current-time");
                dvCp_container.Controls.Add(divCp_circle_control);

                HtmlGenericControl divCd_listen = new HtmlGenericControl("div");
                divCd_listen.Attributes.Add("class", "cd-listen");
                divCd_listen.InnerText = "LISTEN";
                divCd_listen.Attributes.Add("title", _index.ToString());
                dvCp_container.Controls.Add(divCd_listen);


                HtmlGenericControl divsmall_play = new HtmlGenericControl("div");
                divsmall_play.Attributes.Add("class", "small-play");

                dvCp_container.Controls.Add(divsmall_play);

                string strJPlayerScrip = "";

                strJPlayerScrip += "$(document).ready(function () {\n";

                if (1 == 2) // instead of initiate all the leads with record i create the player dynamicaly
                {
                    strJPlayerScrip += "var myCirclePlayer3 = new CirclePlayer(\"#jquery_jplayer_" + _index + "\",\n";
                    strJPlayerScrip += "{\n";
                    strJPlayerScrip += "mp3: \"usa_dev_default.mp3\"\n";
                    //strJPlayerScrip += "mp3: \" \"\n";
                    //strJPlayerScrip += "m4a: \"http://www.jplayer.org/audio/m4a/Miaow-07-Bubble.m4a\",\n";
                    //strJPlayerScrip += "oga: \"http://www.jplayer.org/audio/ogg/Miaow-07-Bubble.ogg\"\n";

                    strJPlayerScrip += "}, {\n";
                    strJPlayerScrip += "cssSelectorAncestor: \"#cp_container_" + _index + "\",\n";
                    strJPlayerScrip += "swfPath: \"../jPlayer/jQuery.jPlayer.2.1.0.demos/js\",\n";
                    strJPlayerScrip += "supplied: \"mp3\",\n";
                    strJPlayerScrip += " wmode: \"window\"\n";
                    strJPlayerScrip += "});\n";
                }

                /*
                strJPlayerScrip += "$(\"#cp_container_" + _index + " .cd-listen\").click(function () { // added by shai\n";               
                strJPlayerScrip += "OpenRecord('" + _index + "');\n";
                strJPlayerScrip += "});\n";
                */

                strJPlayerScrip += "$(\"#cp_container_" + _index + "\").mouseover(function () { // added by shai\n";
                strJPlayerScrip += "showHideClockTime('" + _index + "','over');\n";
                strJPlayerScrip += "});\n";

                /*
                strJPlayerScrip += "$(\"#cp_container_" + _index + "\").mouseout(function () { // added by shai\n";
                strJPlayerScrip += "showHideClockTime('" + _index + "','out');\n";
                strJPlayerScrip += "});\n";
                */

                strJPlayerScrip += "$(\"#cp_container_" + _index + "\").click(function () { // added by shai\n";
                strJPlayerScrip += "OpenRecord('" + _index + "');\n";
                strJPlayerScrip += "});\n";

                /* // should not be here the default. instead the container of is triggred to play
                strJPlayerScrip += "$(\"#cp_container_" + _index + " .cp-play\").click(function () { // added by shai\n";
                strJPlayerScrip += "OpenRecord('" + _index + "');\n";
                strJPlayerScrip += "});\n";
                */

                if (1 == 2) // instead of initiate all the leads with record i create the player dynamicaly
                {
                    strJPlayerScrip += "$(\"#jquery_jplayer_" + _index + "\").bind($.jPlayer.event.play, function (event) { // Add a listener to report the time play began\n";
                    strJPlayerScrip += "});\n";

                    strJPlayerScrip += "$(\"#jquery_jplayer_" + _index + "\").bind($.jPlayer.event.ended, function (event) { // Add a listener to report the time play began\n";
                    //strJPlayerScrip += "alert(\"baaa\");\n";
                    strJPlayerScrip += "});\n";

                    strJPlayerScrip += "$(\"#jquery_jplayer_" + _index + "\").bind($.jPlayer.event.error, function (event) { // Add a listener to report the time play began\n";
                    strJPlayerScrip += "alert(event.jPlayer.error.message);\n";
                    strJPlayerScrip += "});\n";
                }

                strJPlayerScrip += "});\n";

                if (!ClientScript.IsStartupScriptRegistered("SetJplayer" + _index))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetJplayer" + _index,
                                        strJPlayerScrip, true);


                divStatus.Controls.Add(divJquery_jplayer);
                divStatus.Controls.Add(dvCp_container);
                divStatus.Attributes.Add("class", ""); // delete the default css


                if (1 == 2)
                {
                    //divStatusText.InnerHtml = "<a href='javascript:void(0);' onclick=" + refRecording + ">LISTEN</a>";
                    divStatusText.InnerHtml = "LISTEN";
                    divStatus.Attributes.Add("style", "background-position:-227px -213px");

                    divStatusTextHover.Attributes.Add("ID", "divStatusTextHover" + index);
                    divStatusTextHover.Attributes.Add("class", "divStatusTextWon");
                    divStatusTextHover.InnerHtml = "";

                    divStatus.Attributes.Add("style", "background-position:-227px -213px");
                    divStatus.Attributes.Add("onmouseover", "changeSpriteBackground(this,'-313px','-213px');showHideTextLead('divStatusTextHover" + index + "','divStatusText" + index + "')");
                    //divStatus.Attributes.Add("onmouseout", "changeSpriteBackground(this,'-227px','-213px');showHideTextLead('divStatusText" + index + "','divStatusTextHover" + index + "')");
                    divStatus.Attributes.Add("onclick", "changeSpriteBackground(this,'-399px','-213px');" + refRecording);
                }


            }

            else if (status == WebReferenceSupplier.AdvDashboardLeadStatus.WonNoAudio)
            {
                divStatusText.Attributes.Add("ID", "divStatusText" + index);
                divStatusText.Attributes.Add("class", "divStatusTextWonNoAudio");
                divStatusText.InnerHtml = "NO AUDIO";

                divStatusTextHover.Attributes.Add("ID", "divStatusTextHover" + index);
                divStatusTextHover.Attributes.Add("class", "divStatusTextNoAudioHover");
                //Session["TurnOnRecordings"]


                divStatusTextHover.InnerHtml = "<div class='recording'>RECORDING</div><div class='wasOff'>WAS OFF</div><div class='RecordCalls'>" +
                    (ifEnableRecord == true || (Session["ifEnableRecord"] != null && (bool)Session["ifEnableRecord"] == true) ? "<a href='javascript:void(0);' class='-TurnOnRecordCalls' onclick='showHideRecordCalls(true);' style='display:none;'>Stop recording</a>" : "<a href='javascript:void(0);' class='-TurnOnRecordCalls' onclick='showHideRecordCalls(true);'>Record calls</a>") +
                    "</div>";
                //       divDetailsLead.InnerHtml = "ID " + leadNumber + " <a id=linkRefund" + leadNumber + " href='javascript:void(0);' onclick='javascript:showHideRefund(\"" + incidentId.ToString() + "\"," + leadNumber + ");'>Report this lead</a>";

                divStatus.Attributes.Add("style", "background-position:-142px -298px");
                divStatus.Attributes.Add("onmouseover", "changeSpriteBackground(this,'-142px','-213px');showHideTextLead('divStatusTextHover" + index + "','divStatusText" + index + "')");
                divStatus.Attributes.Add("onmouseout", "changeSpriteBackground(this,'-142px','-298px');showHideTextLead('divStatusText" + index + "','divStatusTextHover" + index + "')");

            }

            else if (status == WebReferenceSupplier.AdvDashboardLeadStatus.Lost)
            {
                divStatusText.Attributes.Add("ID", "divStatusText" + index);
                divStatusText.Attributes.Add("class", "divStatusTextLost");
                divStatusText.InnerHtml = "LOST BID";

                int bid = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).MyPrice;
                int win = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).WinningPrice;

                divStatusTextHover.Attributes.Add("ID", "divStatusTextHover" + index);
                divStatusTextHover.Attributes.Add("class", "divStatusTextLostHover");
                divStatusTextHover.InnerHtml = "<div class='Bid'>$" + bid.ToString() + "</div><div class='bidTitle'>YOUR BID</div><div class='bidLineDiv'><hr class='bidLine' /></div><div class='won'>$" + win.ToString() + "</div>";

                divStatus.Attributes.Add("style", "background-position:-142px -128px");
                divStatus.Attributes.Add("onmouseover", "changeSpriteBackground(this,'0px','-423px');showHideTextLead('divStatusTextHover" + index + "','divStatusText" + index + "')");
                divStatus.Attributes.Add("onmouseout", "changeSpriteBackground(this,'-142px','-128px');showHideTextLead('divStatusText" + index + "','divStatusTextHover" + index + "')");
            }

            else if (status == WebReferenceSupplier.AdvDashboardLeadStatus.Refunded)
            {
                divStatusText.Attributes.Add("ID", "divStatusText" + index);
                divStatusText.Attributes.Add("class", "divStatusTextRefunded");
                divStatusText.InnerHtml = "REFUNDED";

                DateTime refundenOn = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).RefundedOn;
                string strRefundenOnDayMonth = refundenOn.Day + " " + refundenOn.ToString("MMMM");
                divStatusTextHover.Attributes.Add("ID", "divStatusTextHover" + index);
                divStatusTextHover.Attributes.Add("class", "divStatusTextRefundedHover");
                divStatusTextHover.InnerHtml = "<div class='refundTitle'>REFUNDED</div><div class='refundDayMonth'>" +
                    strRefundenOnDayMonth + "</div><div class='refundYear'>" + refundenOn.Year + "</div>";

                divStatus.Attributes.Add("style", "background-position:-142px -128px");
                divStatus.Attributes.Add("onmouseover", "changeSpriteBackground(this,'0px','-423px');showHideTextLead('divStatusTextHover" + index + "','divStatusText" + index + "')");
                divStatus.Attributes.Add("onmouseout", "changeSpriteBackground(this,'-142px','-128px');showHideTextLead('divStatusText" + index + "','divStatusTextHover" + index + "')");

            }

            else if (status == WebReferenceSupplier.AdvDashboardLeadStatus.Rejected)
            {
                divStatusText.Attributes.Add("class", "divStatusTextRejected");
                divStatusText.InnerHtml = "REJECTED";

                divStatus.Attributes.Add("style", "background-position:-142px -128px");
            }



            HtmlTableCell tCell = (HtmlTableCell)e.Item.FindControl("td_status");

            divStatus.Controls.Add(divStatusText);
            divStatus.Controls.Add(divStatusTextHover);
            tCell.Controls.Add(divStatus);

            //I need an Animal Sitter
            //WebReferenceSupplier.AdvDashboardLeadStatus headingName = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).HeadingName;
            //HtmlGenericControl divDescText = new HtmlGenericControl("div");
            //td_description
            /************** category heading name ************/
            string headingName = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).HeadingName;

            HtmlTableCell tCellDesc = (HtmlTableCell)e.Item.FindControl("td_description");

            HtmlGenericControl divDesc = new HtmlGenericControl("div");
            divDesc.Attributes.Add("class", "divDesc");

            HtmlGenericControl divDescCategory = new HtmlGenericControl("div");
            divDescCategory.Attributes.Add("class", "divDescCategory");
            divDescCategory.InnerHtml = "I need " + headingName + "...";

            divDesc.Controls.Add(divDescCategory);

            /************** category content ************/

            string descContent = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).Description;

            HtmlGenericControl divDescContent = new HtmlGenericControl("div");
            divDescContent.Attributes.Add("class", "divDescContent");
            divDescContent.InnerHtml = descContent;

            divDesc.Controls.Add(divDescContent);

            tCellDesc.Controls.Add(divDesc);

            /************** Details ************/


            HtmlGenericControl divDetails = new HtmlGenericControl("div");
            divDetails.Attributes.Add("class", "divDetails");

            HtmlTableCell tCellDetails = (HtmlTableCell)e.Item.FindControl("td_details");

            tCellDetails.Controls.Add(divDetails);

            /************** Details: phone ************/
            string customerPhone = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).CustomerPhone;
            HtmlGenericControl divDetailsphone = new HtmlGenericControl("div");
            divDetailsphone.Attributes.Add("class", "divDetailsPhone");

            if (status == WebReferenceSupplier.AdvDashboardLeadStatus.Lost || status == WebReferenceSupplier.AdvDashboardLeadStatus.Rejected)
            {
                if (customerPhone.Length == 10)
                    customerPhone = "(" + customerPhone.Substring(0, 3) + ") " + customerPhone.Substring(3, 3) + "-****";
            }
            else
            {
                if (customerPhone.Length == 10)
                    customerPhone = "(" + customerPhone.Substring(0, 3) + ") " + customerPhone.Substring(3, 3) + "-" + customerPhone.Substring(6, 4);
            }

            divDetailsphone.InnerHtml = customerPhone;
            divDetails.Controls.Add(divDetailsphone);

            /************** Details: Region ************/
            string region = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).Region;
            HtmlGenericControl divDetailsRegion = new HtmlGenericControl("div");
            divDetailsRegion.Attributes.Add("class", "divDetailsRegion");
            if (string.IsNullOrEmpty(region))
                divDetailsRegion.InnerHtml = "&nbsp;";
            else
                divDetailsRegion.InnerHtml = region;

            divDetails.Controls.Add(divDetailsRegion);

            /************** Details: Time ************/
            DateTime createOn = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).CreatedOn;
            DateTime CreatedOnSupplierTime = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).CreatedOnSupplierTime;

            HtmlGenericControl divDetailsCreateOn = new HtmlGenericControl("div");
            divDetailsCreateOn.Attributes.Add("class", "divDetailsCreateOn");

            TimeSpan diffTime = DateTime.Now - createOn;

            divDetailsCreateOn.InnerHtml = Utilities.GetTimePassed(createOn, CreatedOnSupplierTime);

            divDetails.Controls.Add(divDetailsCreateOn);

            /************** Details: ID ************/
            string leadNumber = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).LeadNumber;
            Guid incidentId = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).IncidentId;
            bool charge = ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).Charged; // if realy charged before so it is potentional to be refunded
            HtmlGenericControl divDetailsLead = new HtmlGenericControl("div");
            divDetailsLead.Attributes.Add("class", "divDetailsLeadNumber");

            if (status == WebReferenceSupplier.AdvDashboardLeadStatus.Won)
            {
                if (charge)
                    divDetailsLead.InnerHtml = /*"ID " + leadNumber + */" <a id=linkRefund" + leadNumber + " href='javascript:void(0);' onclick='javascript:showHideRefund(\"" + incidentId.ToString() + "\"," + leadNumber + ");'>Report this lead</a>";
                //       else
                //            divDetailsLead.InnerHtml = "ID " + leadNumber;
            }
            else if (status == WebReferenceSupplier.AdvDashboardLeadStatus.WonNoAudio && charge && IsLeadBroker)
            {
                divDetailsLead.InnerHtml = /*"ID " + leadNumber + */" <a id=linkRefund" + leadNumber + " href='javascript:void(0);' onclick='javascript:showHideRefund(\"" + incidentId.ToString() + "\"," + leadNumber + ");'>Report this lead</a>";
            }
            else if (status == WebReferenceSupplier.AdvDashboardLeadStatus.Refunded)
                //    ((WebReferenceSupplier.AdvDashboardLeadData)e.Item.DataItem).
                divDetailsLead.InnerHtml = /*"ID " + leadNumber + */" <span class='refundClaim'>Refund claim approved</span>";
            //   else
            //       divDetailsLead.InnerHtml = "ID " + leadNumber;

            divDetails.Controls.Add(divDetailsLead);

        }

        //}

    }

    protected void inviteBtn_Click(object sender, EventArgs e)
    {
        //Response.Write("dgdfgfdgdf");
        //UpdatePanelLoader.Update();
    }

    protected void timeChanged(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;

        timeSelected.InnerText = lb.Text;
        setLableTime(timeSelected.InnerText);

        item_page_accumulate = 0;
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        showLeads(supplier, sortby, sortDescending);
    }

    protected void setLableTime(string strTime)
    {
        ControlCollection cl = ulTime.Controls;

        foreach (Control control in cl)
        {
            if (control.GetType().Equals(typeof(HtmlGenericControl)))
            {
                HtmlGenericControl li = (HtmlGenericControl)control;
                li.Attributes.Add("class", "");

                ControlCollection clLi = li.Controls;

                foreach (Control controlLi in clLi)
                {
                    if (controlLi.GetType().Equals(typeof(LinkButton)))
                    {
                        LinkButton lb = (LinkButton)controlLi;
                        if (lb.Text.ToLower() == strTime.ToLower())
                            li.Attributes.Add("class", "selectBox-selected");
                    }
                }

            }
        }

        /*
        switch(strTime.ToLower()) 
        {
            case "week":
                liTimeWeek.Attributes.Add("class", "selectBox-selected");
                break;

            case "month":
                liTimeMonth.Attributes.Add("class", "selectBox-selected");
                break;

            case "year":                
                liTimeYear.Attributes.Add("class","selectBox-selected");

                liTimeWeek.Attributes.Add("class", "");
                liTimeMonth.Attributes.Add("class", "");
                liTimeAll.Attributes.Add("class", "");

                break;

            case "all":
                liTimeAll.Attributes.Add("class", "selectBox-selected");
                break;

            default:
                break;
        }
        */

    }

    protected void lbStatus_Command(Object sender, CommandEventArgs e)
    {
        //Label1.Text = "You chose: " + e.CommandName + " Item " + e.CommandArgument;
        //string sortBy = "";


        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.MyLeadsListSortBy lastSortBy;
        lastSortBy = sortby;

        switch ((string)e.CommandArgument)
        {
            case "status":
                sortby = WebReferenceSupplier.MyLeadsListSortBy.Status;
                break;

            case "time":
                sortby = WebReferenceSupplier.MyLeadsListSortBy.Dates;
                break;

            case "heading":
                sortby = WebReferenceSupplier.MyLeadsListSortBy.Heading;
                break;

            default:
                sortby = WebReferenceSupplier.MyLeadsListSortBy.Dates;
                break;
        }

        if (lastSortBy == sortby)
            sortDescending = !sortDescending;

        showLeads(supplier, sortby, sortDescending);


        //ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext8", "setHeaderLeadTitle('" + lb.ClientID + "')", true);
        //lb_status.Attributes.Add("onclick", String.Format("return setHeaderLeadTitle('{0}')", lb_status.ClientID));
        // showLeads(supplier);  
    }


    protected WebReferenceSupplier.AdvDashboardLeadData[] dataV
    {
        //get { return (ViewState["data"] == null) ? null : (WebReferenceSupplier.AdvDashboardLeadData[])ViewState["data"]; }
        get { return (Session["data"] == null) ? null : (WebReferenceSupplier.AdvDashboardLeadData[])Session["data"]; }
        set { Session["data"] = value; }
    }

    List<string> listTR
    {
        get { return (Session["list_TR"] == null) ? new List<string>() : (List<string>)Session["list_TR"]; }
        set { Session["list_TR"] = value; }
    }

    int CurrentPage
    {
        get
        {
            object o = this.ViewState["_CurrentPageAdvertiser"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["_CurrentPageAdvertiser"] = value;
        }
    }
    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }
    bool AllowedToRecord
    {
        get { return (ViewState["AllowedToRecord"] == null) ? false : (bool)ViewState["AllowedToRecord"]; }
        set { ViewState["AllowedToRecord"] = value; }
    }

    Dictionary<Guid, string> RecordListV
    {
        get { return (Session["RecordList"] == null) ? new Dictionary<Guid, string>() : (Dictionary<Guid, string>)Session["RecordList"]; }
        set { Session["RecordList"] = value; }
    }

    protected string GetAudioChrome
    {
        get { return ResolveUrl("~") + "Management/AudioChrome.aspx"; }
    }

    protected Dictionary<string, string> RefundStatusV
    {
        get { return (ViewState["RefundStatus"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["RefundStatus"]; }
        set { ViewState["RefundStatus"] = value; }
    }



    protected int item_page_accumulate
    {
        get { return (ViewState["item_page_accumulate"] == null ? 0 : (int)ViewState["item_page_accumulate"]); }
        set { ViewState["item_page_accumulate"] = value; }
    }

    protected int index
    {
        get { return (ViewState["index"] == null ? 0 : (int)ViewState["index"]); }
        set { ViewState["index"] = value; }
    }

    protected bool ifEnableRecord
    {
        get { return (ViewState["ifEnableRecord"] == null ? false : (bool)ViewState["ifEnableRecord"]); }
        set { ViewState["ifEnableRecord"] = value; }
    }

    protected string clientIdBalance
    {
        get { return (ViewState["clientIdBalance"] == null ? "" : (string)ViewState["clientIdBalance"]); }
        set { ViewState["clientIdBalance"] = value; }
    }


    protected WebReferenceSupplier.MyLeadsListSortBy sortby
    {
        get { return (ViewState["sortby"] == null ? WebReferenceSupplier.MyLeadsListSortBy.Dates : (WebReferenceSupplier.MyLeadsListSortBy)ViewState["sortby"]); }
        set { ViewState["sortby"] = value; }
    }

    protected bool sortDescending
    {
        get { return (ViewState["sortDescending"] == null ? true : (bool)ViewState["sortDescending"]); }
        set { ViewState["sortDescending"] = value; }
    }

    protected string stepListenClientId
    {
        get { return (ViewState["stepListenClientId"] == null ? "" : (string)ViewState["stepListenClientId"]); }
        set { ViewState["stepListenClientId"] = value; }
    }

    protected string circleStepListenClientId
    {
        get { return (ViewState["circleStepListenClientId"] == null ? "" : (string)ViewState["circleStepListenClientId"]); }
        set { ViewState["circleStepListenClientId"] = value; }
    }

    protected string indexJplayerTestCall
    {
        get { return (ViewState["indexJplayerTestCall"] == null ? string.Empty : (string)ViewState["indexJplayerTestCall"]); }
        set { ViewState["indexJplayerTestCall"] = value; }
    }

    public int bonusAmount
    {
        get
        {
            return _bonusAmount;
        }

        set
        {
            _bonusAmount = value;
        }
    }
    protected string TurnOnRecordCallsService
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/TurnOnRecordCalls"); }
    }

    protected string TurnOffRecordCallsService
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/TurnOffRecordCalls"); }
    }

    protected string featureListingLink
    {
        get { return ResolveUrl("~/PPC/frameppc2.aspx"); }
    }

    protected bool leadsExist
    {
        get { return (ViewState["leadsExist"] == null ? false : (bool)ViewState["leadsExist"]); }
        set { ViewState["leadsExist"] = value; }
    }

    protected string Period
    {
        get { return (ViewState["Period"] == null ? string.Empty : (string)ViewState["Period"]); }
        set { ViewState["Period"] = value; }
    }
    protected bool IsLeadBroker
    {
        get { return (ViewState["IsLeadBroker"] == null ? false : (bool)ViewState["IsLeadBroker"]); }
        set { ViewState["IsLeadBroker"] = value; }
    }
}