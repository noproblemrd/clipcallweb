
<%@ Page Language="C#" AutoEventWireup="true"
 MasterPageFile="~/Management/MasterPagePreAdvertiser.master" 
 CodeFile="ProfessionLogin.aspx.cs" Inherits="Management_ProfessionLogin" %>

<%@ MasterType VirtualPath="~/Management/MasterPagePreAdvertiser.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" >

<div id="primarycontent">
	<div class="form-login">
		<h2><asp:Label ID="lbl_title" runat="server" TabIndex="4">Welcome to Advertisers Portal</asp:Label></h2>
		<asp:Label runat="server" ID="lblComment" Font-Size="14pt" ForeColor="Red" Font-Bold="True" TabIndex="4"></asp:Label>
		<div class="login">	 
		    <div class="form-field-wrap">
		        <div class="form-field">
			        <label for="form-email" runat="server" id="lbl_email">Your E-mail</label>
			        <asp:TextBox runat="server"  CssClass="form-text" text="" id="txt_professionalEmail" textmode="SingleLine" TabIndex="0" autocomplete="off"></asp:TextBox>
                    <asp:Image ID="img_email" runat="server" ImageUrl="../images/icon-q.png" AlternateText="Insert your email" />
                </div>
                <br />
                <div class="error-msg-wrap">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" runat="server" CssClass="error-msg" ErrorMessage="Missing email" ControlToValidate="txt_professionalEmail" Display="Dynamic"></asp:RequiredFieldValidator>  
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" CssClass="error-msg" runat="server" ErrorMessage="Invalid email" ControlToValidate="txt_professionalEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
		    </div>
		    <div class="form-field-wrap">
		        <div class="form-field">
			        <label for="form-password" runat="server" id="lbl_password">Your password</label>
			        <asp:TextBox runat="server"  CssClass="form-text"  id="txt_professionalPassword" textmode="Password" TabIndex="0"></asp:TextBox>
                    <asp:Image ID="img_password" runat="server" ImageUrl="../images/icon-q.png" AlternateText="Insert password" />
                </div>
                 <br />
                <div class="error-msg-wrap">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" CssClass="error-msg" ErrorMessage="Missing password" ControlToValidate="txt_professionalPassword" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
		    </div>
            <asp:Button  runat="server" Text="Login" id="btnCheckCode" CssClass="loginbtn" OnClick="btnCheckCode_Click" TabIndex="0" />
		</div>
		
		<p class="service">
		
            <input id="ifRememberMe" class="form-checkbox" type="checkbox" checked="checked" runat="server" tabindex="3" />
            <label for="<%= ifRememberMe.ClientID %>" style="width:auto;"><asp:Label ID="lbl_rememberMe" runat="server" Text="Remember me" TabIndex="5"></asp:Label></label>
            
		    <asp:Image ID="img_rememberMe" runat="server" ImageUrl="../images/icon-q.png" AlternateText="Remember me" />
		    <a href='<%=ResolveUrl("~")%>ResetPassword.aspx?IsPublisher=false' class="help">
                <asp:Label ID="lbl_cantGetInto" runat="server" Text="Can't get into your account?" TabIndex="5"></asp:Label>
	        </a>
	        <span id="span_support" runat="server" visible="false">
		    <a href="javascript:void(0)" target="_blank"><asp:Label ID="lbl_support" runat="server" Text="Support" TabIndex="5"></asp:Label></a>
            <asp:Label ID="lbl_version" runat="server" Text="<%# VERSION %>" CssClass="version_num" ></asp:Label>
            </span>
	    </p>
	</div>
</div>
<div id="sidebar">
	<div class="add">
		<p><asp:Label ID="lbl_commercial" runat="server" ></asp:Label></p>
	</div>
</div>

<asp:HiddenField ID="_HiddenField_RequestFailed" runat="server" Value="There was an error, please repeat the action again" />    
<asp:Label ID="lbl_NotFound" runat="server" Text="User or Password is not correct" Visible="false"></asp:Label>
<asp:Label ID="lbl_PublisherUser" runat="server" Text="You need to login by publisher portal" Visible="false"></asp:Label>
<asp:Label ID="lbl_UserInactive" runat="server" Text="Your account is inactive" Visible="false"></asp:Label>


<asp:Label ID="lbl_AccountLocked" runat="server" Text="Your account is locked due to many login attempts" Visible="false"></asp:Label>
<asp:Label ID="lbl_AffiliateUser" runat="server" Text="You need to login by Affiliate portal" Visible="false"></asp:Label>
</asp:Content>
