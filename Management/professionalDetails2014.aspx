﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professionalDetails2014.aspx.cs" Inherits="Management_professionalDetails2014" ClientIDMode="Static" %>
<%@ Register Src="~/Controls/searchGoogle.ascx" TagName="searchGoogle" TagPrefix="businessDetails" %>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <script src="//code.jquery.com/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../PlaceHolder.js"></script>


    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>

    <script type="text/javascript" src="../ppc/script/JSGeneral.js"></script>
    <script type="text/javascript" src="../general.js"></script>
    <link href="../PPC/samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
    <script type="text/javascript">    try { Typekit.load(); } catch (e) { }</script>
    <link href="../Ladda-master/dist/ladda.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Ladda-master/dist/spin.min.js"></script>
    <script type="text/javascript" src="../Ladda-master/dist/ladda.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>
    <script src="../PPC/ppc.js" type="text/javascript"></script>



                                                                            <style>
     
    #_FileImages {
      /* Note: display:none on the input won't trigger the click event in WebKit.
        Setting visibility: hidden and height/width:0 works great. */
      /*  
      visibility: hidden;
      */
      
      /*
      width: 10px;
      height: 10px;
      */
    }
    
    #fileSelect {
      /* style the button any way you want */
    }
</style>

                                                                                                                                                                                                                                                                                                        <script>




    function UpdateSuccess() {
        SetServerComment2("<%#UpdateSuccess%>");
    }

    function UpdateFailed() {
        SetServerComment2("<%#UpdateFaild%>");

    }

    function showVideoAlready() {
        SetServerComment2("<%#videoAlready%>");
        $("#commentVideo span").text('<%#videoAlready%>');
        $("#commentVideo").show();
    }
    
    function witchAlreadyStep(level, mode) {
        //alert(level + " " + mode); 
        //level="0";   
        parent.window.witchAlreadyStep(level);
        level = level * 1;
        if (mode == "init") {
            if (level != 6 && level != 0)
                parent.window.setInitRegistrationStep(level);
            else
                parent.window.setLinkStepActive('linkStep1');

        }
        else
            parent.window.setLinkStepActive('linkStep1');
    }

    function changeLogo() {
        document.getElementById("hidden_changeLogo").value = "true";
        form1.submit();

    }

    function changePhoto() {
        document.getElementById("hidden_changePhoto").value = "true";
        form1.submit();

    }

    function checkYoutbueUrl() {
        var youTubeurl = $("#txtAddVideo").val();        
        var youTubeTest = /<%#pattern%>/;

        if (youTubeurl.search(youTubeTest) == -1) {            
            $("#commentVideo span").text('<%#videoPath%>');
            $("#commentVideo").show();
            return false;
        }
        else {
            $("#commentVideo").hide();
            return true;
        }

    }


    function init(sender, args) {
        try {

            parent.hideDiv();


            $('.fileLogo').mouseover(function (e) {
                e.stopPropagation();
                if ($('.fileLogo2').css('display') == 'block') {
                    $('.fileLogoDelete').css('display', 'block');
                }

            }
             )


            $('.fileLogo').mouseout(function (e) {
                e.stopPropagation();
                if ($('.fileLogo2').css('display') == 'block') {
                    $('.fileLogoDelete').css('display', 'none');
                }

            }
             )


            $('.fileImages').mouseover(function (e) {
                e.stopPropagation();
                if ($(this).children('.fileImages2').css('display') == 'block') {
                    $(this).children('.fileImagesDelete').css('display', 'block');
                }

            }
             )


            $('.fileImages').mouseout(function (e) {
                e.stopPropagation();
                if ($(this).children('.fileImages2').css('display') == 'block') {
                    $(this).children('.fileImagesDelete').css('display', 'none');
                }

            }
             )

            $('.inputVideoContainer').mouseover(function (e) {
                e.stopPropagation();
                if ($(this).children('.videoDelete').css('display') == 'none') {
                    $(this).children('.videoDelete').css('display', 'block');
                }

            }
             )


            $("#txtAddVideo").focus(function () {
                $("#commentVideo").hide();
            }
            )

            //alert("2");
        } catch (ex) { }
    }


    //init();

    window.onload = init;

   

</script>

 <script type="text/javascript">
     var _gaq = _gaq || [];
     _gaq.push(['_setAccount', 'UA-21441721-1']);
     _gaq.push(['_setDomainName', 'noproblemppc.com']);
     _gaq.push(['_trackPageview']);

     (function () {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();
 </script>

</head>

<body class="bodyDashboard">
    <form id="form1" runat="server">
   <div class="businessMainDetails dashboardIframe businessMainDetailsDashboard"> 
     <uc1:PpcComment runat="server" ID="_comment" />
    <div class="titleFirst">
        <span>Business profile</span>
        
        <div id="containerNextTop" class="containerNext containerNextTop">
             <section class="progress-demo">
                <asp:LinkButton ID="NextBusinessDetailsTop" runat="server" data-style="expand-right" data-color="blue" class="ladda-button" OnClientClick="javascript:return businessDetailsValidationAll('.progress-demo #NextBusinessDetailsTop');" OnClick="NextBusinessDetails_Click" >
                    <span class="ladda-label">Update</span><span class="ladda-spinner">
                    </span><div class="ladda-progress" style="width: 138px;"></div>
                  </asp:LinkButton>                 
                </section>      
        </div>
    </div>   
    
    <div class="businessMainContent">

            <div class="containerName">
                <div>Company name</div>  
                <div class="inputParent">
                    <input id="txt_name" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="name" />
                    <div id="div_NameSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_NameError" class="inputError" style=""> </div>

                     <div id="div_NameComment" style="display:none;" class="inputComment"><%#businessNameError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                </div>
            </div>

            <div class="containerAddress">
                <div>Business address</div>

                <div class="inputParent inputParentAddress">                
                    <businessDetails:searchGoogle runat="server" ID="searchGoogle1"></businessDetails:searchGoogle>

                    <div id="div_AddressSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_AddressError" class="inputError" style=""> </div>             

                    <div id="div_AddressComment" style="display:none;" class="inputComment"><%#businessAdressError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                
                </div>
            </div>

             <div class="containerContactPerson">
                <div>Contact person</div>

                <div class="inputParent inputParentContactPerson">                
                    <input id="txt_ContactPerson" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="mobile number" />

                    <div id="div_ContactPersonSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_ContactPersonError" class="inputError" style=""> </div>             

                    <div id="div_ContactPersonComment" style="display:none;" class="inputComment"><%#businessAdressError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                
                </div>
            </div>

            <div class="containerMobile">
                <div>Mobile number</div>  

                <div class="inputParent inputParentMobile">
                    <input id="txt_mobile" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="mobile number" />
                    <div id="div_MobileSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_MobileError" class="inputError" style=""> </div>             

                    <div id="div_MobileComment" style="display:none;" class="inputComment"><%#mobileError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>

                </div>
            </div>

              <div class="containerMobile">
                <div>Fax number</div>  

                <div class="inputParent inputParentMobile">
                    <input id="txt_fax" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="fax" />
                </div>
            </div>

             <div class="containerEmail">
                <div>Email address</div>  

                <div class="inputParent inputParentEmail">
                    <input id="txt_email" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="address" readonly="readonly"  />
                    <div id="div_EmailSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_EmailError" class="inputError" style=""> </div>             

                    <div id="div_EmailComment" style="display:none;" class="inputComment"><%#emailError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>

                </div>
            </div>

             <div class="containerWebsite">
                <div>Website</div>  

                <div class="inputParent inputParentWebsite">
                    <input id="txt_website" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="website" />
                </div>
            </div>

             <div class="containerAbout">
                <div>About your business</div>  

                <div class="inputParent inputParentAbout">                   
                    <asp:TextBox runat="server" ID="txt_about" class="textActive" HolderClass="place-holder" text-mode="about" TextMode="MultiLine" ></asp:TextBox>
                </div>
            </div>

            <div class="containerNumEmployees">
                <div>Number of employees</div>  

                <div class="inputParent inputParentNumEmployees">
                    <input id="txt_numEmployees" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="numEmployees" />
                </div>
            </div>

            <div class="containerPayments">
                <div>Payment method accepted</div>

                <div class="inputParent">
                    <div class="paymentsControls">
                        <asp:CheckBox  runat="server" ID="chkAmex"  Text="AMEX"/>
                        <asp:CheckBox  runat="server" ID="chkMasterCard"  Text="MASTER CARD"/>                
                        <asp:CheckBox  runat="server" ID="chkVisa"  Text="VISA"/>
                        <asp:CheckBox  runat="server" ID="chkCash"  Text="CASH"/>
                    </div>

                    <div id="commentPayments" class="inputComment" >
                            <span><%#paymentsError%></span>
                            <div id="ArrowBorderVideo" class="chat-bubble-arrow-border"></div>
                            <div id="Div3" class="chat-bubble-arrow"></div>
                    </div>

                </div>               
               
            </div>

            <div class="containerLogo">
                <div>Logo image</div>  

                <div class="inputParent">
                    <div class="fileLogo">

                       <div id="fileLogo1" runat="server" class="fileLogo1"  >
                            <div class="fileLogoIcon">
                                <i class="fa fa-camera icon-camera" id="iconCamera1" ></i>
                            </div>

                            <div class="fileLogoComponent">
                               
                                <input id="_FileLogo"  name="_FileLogo" type="file" class="form-file" onchange="changeLogo();" runat="server"  />
                                <div class="fileLogoAdd">                               
                                 <a href="#" id="lnk_addLogo" class="addLogo">Add a logo</a>
                                </div>      
                            </div>
                            
                                                
                            
                                
                                <!--
                            <asp:Label ID="commentUploadImages" CssClass="error-msg" runat="server" Text="No picture selected" Visible="false"></asp:Label>
                            -->
                        </div>

                        <div id="fileLogo2" runat="server" class="fileLogo2"  >
                            <asp:ImageButton  ID="imgBtnLogo"  class="imgBtnLogo" runat="server"  OnClientClick="return false;"/>
                        </div>
                        
                        <div id="fileLogoDelete" class="fileLogoDelete" runat="server" >                           
                            
                            <div class="fileLogoDeleteIcon">
                                <asp:LinkButton runat="server" ID="lbDeleteFileIcon" onclick="lbDeleteFile_Click"><i class="fa fa-times fa-2"></i></asp:LinkButton>
                            </div>
                            
                            <div class="fileLogoDeleteText">
                                <asp:LinkButton runat="server" ID="lbDeleteFile" onclick="lbDeleteFile_Click">Delete</asp:LinkButton>
                            </div>                            
                            
                        </div>

                       

                    </div>
                </div>
            </div>

            <div>Photo gallery</div>
            <div class="containerImages" runat="server" id="containerImages">
                

                <div id="pnlAddImage" class="inputParent" runat="server">
                <div class="fileImages fileImagesFirst">

                       <div id="fileImages1_1" runat="server" class="fileImages1"  >
                            <div class="fileImagesIcon">
                                <i class="fa fa-camera icon-camera" id="iImages1" ></i>
                            </div>

                            <div class="fileImagesComponent">
                                <input id="FileUploadPhoto" type="file" class="form-file" onchange="changePhoto();" runat="server"    />
                                <div class="fileImagesAdd">
                                    <a href="#" id="lnk_addphoto" class="addLogo">Add a photo</a>                               
                                </div>                                  
                            </div>                           
                            
                        </div>

                        <%--
                        <div id="fileImages2_1" runat="server" class="fileImages2"  >
                            <asp:ImageButton  ID="imgBtnImages1"  class="imgBtnImages" runat="server"  OnClientClick="return false;"/>
                        </div>
                        
                        <div id="fileImagesDelete1" class="fileImagesDelete" runat="server" >                           
                            
                            <div class="fileImagesDeleteIcon">
                                <asp:LinkButton runat="server" ID="lbDeleteImageIcon1" onclick="lbDeleteFile_Click"><i class="fa fa-times fa-2"></i></asp:LinkButton>
                            </div>
                            
                            <div class="fileImagesDeleteText">
                                <asp:LinkButton runat="server" ID="lbDeleteImage1" CommandName="lbDeleteFile_Click"  CommandArgument="1" >Delete</asp:LinkButton>
                            </div>                            
                            
                        </div>
                        --%>
                       

                 </div>                 
                 </div>
                 <div id="pnlImages" class="inputParent" runat="server">

                   
                    

                    <%-- 
                    <div class="fileImages">

                       <div id="Div1" runat="server" class="fileImages1"  >
                            <div class="fileImagesIcon">
                                <i class="fa fa-camera icon-camera" id="i1" style="font-size: 20px; margin-left: 36px; margin-top: 29px;"></i>
                            </div>

                            <div class="fileImagesComponent">
                                <input id="File1" type="file" class="form-file" runat="server"  />
                                <!--                                
                                <button id="fileSelect">Select some files</button>
                                -->
                                <script>
                                    /*
                                    document.querySelector('#fileSelect').addEventListener('click', function (e) {
                                    // Use the native click() of the file input.
                                    document.querySelector('#_FileImages').click();
                                    }, false);
                                    */
                                </script>
                            </div>
                            
                            <div class="fileImagesAdd">
                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Add a photo" 
                                CssClass="addLogo" OnCommand="lb_uploadPhoto_Click" CommandArgument="2"></asp:LinkButton>
                            </div>                        
                            
                            
                        </div>

                        <div id="Div2" runat="server" class="fileImages2"  >
                            <asp:ImageButton  ID="ImageButton1"  class="imgBtnImages" runat="server"  OnClientClick="return false;"/>
                        </div>
                        
                        <div id="Div3" class="fileImagesDelete" runat="server" >                           
                            
                            <div class="fileImagesDeleteIcon">
                                <asp:LinkButton runat="server" ID="LinkButton2" OnCommand="lbDeleteFile_Click" CommandArgument="2"><i class="fa fa-times fa-2"></i></asp:LinkButton>
                            </div>
                            
                            <div class="fileImagesDeleteText">
                                <asp:LinkButton runat="server" ID="LinkButton3" OnCommand="lbDeleteFile_Click" CommandArgument="2">Delete</asp:LinkButton>
                            </div>                            
                            
                        </div>

                       

                    </div>
                   
                 </div>
                 --%>
            </div>

            <div class="clear"></div>
            </div>            

            <div>Video gallery</div>  
            <div class="containerVideo">               

                <asp:Panel ID="pnlAddVideos" CssClass="pnlAddVideos inputParent" runat="server" >
                    <asp:TextBox runat="server" ID="txtAddVideo" class="textActive addVideo"  place_holder="<%#videoHolder%>" holderclass="place-holder"></asp:TextBox>
                                        
                        <div style="" class="inputComment" id="commentVideo">
                            <span>sdfsdfsd</span>
                            <div class="chat-bubble-arrow-border" id="chatBubbleArrowBorderVideo"></div>
                            <div class="chat-bubble-arrow" id="chatBubbleArrowVideo"></div>
                        </div>                   
                   

                    <asp:LinkButton ID="btnAddVideo" runat="server" class="btnAddVideo" OnClientClick="return checkYoutbueUrl()" OnClick="NextBusinessDetails_Click" Text="Add video" ></asp:LinkButton>     

                    <div runat="server" id="ContainerVideos" class="ContainerIframeVideos"></div>

                    <div class="clear"></div>

                </asp:Panel>        

                
            </div>         


            <asp:HiddenField id="hidden_phoneFormat" runat="server"/>
            <asp:HiddenField id="hidden_changeLogo" runat="server" Value="false"/>
            <asp:HiddenField id="hidden_changePhoto" runat="server" Value="false"/>
            
    </div> 
    
   

</div>
    </form>
</body>
</html>
