﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Management_refund : PageSetting
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!userManagement.IsSupplier())
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "redirect", "top.window.location.href = '"+ ResolveUrl("~/Management/LogOut.aspx") + "';", true);
            ApplicationInstance.CompleteRequest();
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadRefundReasons();
            incidentId = Request["incidentId"];
            leadNumber = Request["leadNumber"];
     //       btn_RefundSubmit.Attributes.Add("onmouseover", "sendHover();");
    //        btn_RefundSubmit.Attributes.Add("onmouseout", "sendOut();");
    //        txt_RefundDescription.Attributes.Add("onfocus", "cleanTextBox();");
            
        }
        Page.Header.DataBind();
    }

    private void LoadRefundReasons()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllRefundReasonsResponse result = null;
        try
        {
            result = _site.GetAllRefundReasons();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }


        radionBtnListRefundReasons.Items.Clear();
        
        foreach (WebReferenceSite.RefundReasonData rrd in result.Value.RefundReasons)
        {
            ListItem li = new ListItem(rrd.Name, "" + rrd.Code);
            if(!rrd.Inactive)
                radionBtnListRefundReasons.Items.Add(li);
        }
        radionBtnListRefundReasons.SelectedIndex = 0;
       
    }

     
    protected void btn_RefundSubmit_click(object sender, EventArgs e)
    {    
        
        WebReferenceSite.UpsertRefundRequest urr = new WebReferenceSite.UpsertRefundRequest();
        urr.RefundStatus = WebReferenceSite.RefundSatus.Approved; // automatic approved
        urr.IncidentId = new Guid(incidentId);        
        urr.SupplierId = new Guid(GetGuidSetting());
        urr.RefundReasonCode = int.Parse(radionBtnListRefundReasons.SelectedValue);        
        urr.RefundNoteForAdvertiser = txt_RefundDescription.Text;
        
       
        
        WebReferenceSite.ResultOfUpsertRefundResponse result = ExecRefund(urr);
        if (result == null)
            return;
        
        string balance = string.Format(GetNumberFormat, result.Value.NewBalance);
        userManagement.balance = (double)result.Value.NewBalance;
        //string balance = "11,111";
        Session["balance"] = balance;

        if (result == null )
            return;
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "reloadParent", "closeAndRefresh('" + balance + "','" + leadNumber + "');", true);
        

        //result.Type= WebReferenceSite.eResultType.Success
       
        /*
        switch (result.Value.Status)
        {
            
            case (WebReferenceSite.UpsertRefundStatus.CallWasNotCharged): // from some mistake recieved for example lost lead
            case (WebReferenceSite.UpsertRefundStatus.OK):
                break; 
            
        }
        */
       
    }

    WebReferenceSite.ResultOfUpsertRefundResponse ExecRefund(WebReferenceSite.UpsertRefundRequest _request)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfUpsertRefundResponse result = null;
        //    bool IsUpsert = true;
        try
        {
            result = _site.UpsertRefund(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "reloadParent", "parent.updateSystemMessage('server problem. try later',1);", true);
            return null;
            //          return;
        }

        if (result.Type == WebReferenceSite.eResultType.Failure)
        {

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "reloadParent", "parent.updateSystemMessage('server problem. try later',1);", true);
            return null;
            ///       return;
        }

        else if(result.Value.Status == WebReferenceSite.UpsertRefundStatus.CallWasNotCharged)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "reloadParent", "parent.updateSystemMessage('Problem refund: Call Was Not Charged',1);", true);
            return null;
        }

        else if (result.Value.Status == WebReferenceSite.UpsertRefundStatus.NoRefundAllowed)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "reloadParent", "parent.updateSystemMessage('Problem refund: No Refund Allowed',1);", true);
            return null;
        }

        return result;
    }

    
    protected string incidentId
    {
        get
        {
            return (ViewState["incidentId"] == null ? null : (string)ViewState["incidentId"]);
        }

        set
        {
            ViewState["incidentId"] = value;
        }
    }

    protected string leadNumber
    {
        get
        {
            return (ViewState["leadNumber"] == null ? null : (string)ViewState["leadNumber"]);
        }

        set
        {
            ViewState["leadNumber"] = value;
        }
    }
    protected string HonorCode
    {
        get { return ResolveUrl("~/PPC/HonorCode.aspx"); }
    }
}