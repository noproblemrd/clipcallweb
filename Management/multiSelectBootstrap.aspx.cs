﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Management_multiSelectBootstrap : PageSetting
{
    protected DateTime? vacationFrom{get;set;}
    protected DateTime? vacationTo { get; set; }


    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(lbAddButton);

        if (!IsPostBack)
        {
            /*
            addMultiSelectDay();
            addMultiSelectDay();
            addMultiSelectDay();
            */
            Guid supplierGuid;

            if (Guid.TryParse(GetGuidSetting(), out supplierGuid))
            {
                SupplierId = supplierGuid;
            }

            arrayWhoStay = new ArrayList();
            arrayWhoInInit = new ArrayList();  

            index=0;
            LoadService();
            LoadPanelServices(false);
        }

        else
        {
            index++;
            LoadPanelServices(true);
        }

        Page.Header.DataBind();

    }



    //[WebMethod]
//public Result<GetWorkingHoursResponse> GetWorkingHours_Registration2014(Guid supplierId)
    private bool LoadService()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfGetWorkingHoursResponse resultOfGetWorkingHoursResponse = new WebReferenceSupplier.ResultOfGetWorkingHoursResponse();
        ///Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;

        StringBuilder sb = new StringBuilder();
        sb.Append("containerWorkingHoursObject=");

        try
        {
            //result = _supplier.GetSupplierExpertise(siteSetting.GetSiteID, SupplierId.ToString());
            //resultOfGetWorkingHoursResponse = _supplier.GetWorkingHours_Registration2014(SupplierId);
            resultOfGetWorkingHoursResponse = _supplier.GetWorkingHours_Registration2014(new Guid("8AEA98B3-354A-4ABB-825F-DFD1CD5683D1"));


            var objValue = resultOfGetWorkingHoursResponse.Value;

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            ClientScript.RegisterStartupScript(this.GetType(), "initDefaultDays", "var objUnits='" + jss.Serialize(objValue) + "';", true);

            vacationFrom=resultOfGetWorkingHoursResponse.Value.VacationFrom;
            //vacationFrom = Convert.ToDateTime("12/15/2013");
            vacationTo=resultOfGetWorkingHoursResponse.Value.VacationTo;
            //vacationTo = Convert.ToDateTime("12/17/2013");


            if (!string.IsNullOrEmpty(vacationFrom.ToString()))
            {
                containerVacation.Attributes.Add("style", "display:block;");
                lnk_Vacation.Attributes.Add("style", "display:none;");
            }

            foreach (WebReferenceSupplier.WorkingHoursUnit workingHoursUnit in resultOfGetWorkingHoursResponse.Value.Units)
            {
               
                index++;

                Unit unit = new Unit();
                unit.AllDay = workingHoursUnit.AllDay;
                unit.FromHour = workingHoursUnit.FromHour;
                unit.FromMinute = workingHoursUnit.FromMinute;
                unit.ToHour = workingHoursUnit.ToHour;
                unit.ToMinute = workingHoursUnit.ToMinute;

                arrayWhoInInit.Add(unit);
                
            }

            //var daysOfWeek = new Array();

            /*
             <SupplierExpertise RegistrationStage="2" SubscriptionPlan="LEAD_BOOSTER_AND_FEATURED_LISTING">
              <PrimaryExpertise Name="Devil Worshipper" ID="f6f38910-1058-e311-8fb9-001517d10f6e" Certificate="False" />
             </SupplierExpertise>
            */

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);

            ////ClientScript.RegisterStartupScript(this.GetType(), "StopButtonAnimation", "SendFaild();", true);

            return false;
        }

        
        
        return true;
    }


    

    private void LoadPanelServices(bool isPostback)
    {
        pnlDates.Controls.Clear();

        for (int i = 0; i < index; i++)
        {
            arrayWhoStay.Add(Convert.ToInt32(Utilities.GetMaxValue(arrayWhoStay) + 1));

            HtmlGenericControl inputCategoryContainer = new HtmlGenericControl("div");
            inputCategoryContainer.ID = "inputCategoryContainer" + arrayWhoStay[i];
            inputCategoryContainer.Attributes["class"] = "inputCategoryContainer";

            HtmlGenericControl containerDates = new HtmlGenericControl("div");
            containerDates.ID = "containerDates" + arrayWhoStay[i];
            containerDates.Attributes["class"] = "containerDates";

            DropDownList ddl = new DropDownList();
            ddl.CssClass = "multiselect";
            ddl.Attributes.Add("multiple", "multiple");
            ddl.ID = "selectDate" + arrayWhoStay[i];

            //multiple="multiple"

            ListItem li = new ListItem();

            li.Text = "Sun";
            li.Value = "Sun";

            ddl.Items.Add(li);

            li = new ListItem();
            li.Text = "Mon";
            li.Value = "Mon";

            ddl.Items.Add(li);

            li = new ListItem();
            li.Text = "Tue";
            li.Value = "Tue";

            ddl.Items.Add(li);

            li = new ListItem();
            li.Text = "Wed";
            li.Value = "Wed";

            ddl.Items.Add(li);

            li = new ListItem();
            li.Text = "Thu";
            li.Value = "Thu";

            ddl.Items.Add(li);

            li = new ListItem();
            li.Text = "Fri";
            li.Value = "Fri";

            ddl.Items.Add(li);

            li = new ListItem();
            li.Text = "Sat";
            li.Value = "Sat";

            ddl.Items.Add(li);

            containerDates.Controls.Add(ddl);
            inputCategoryContainer.Controls.Add(containerDates);

            DropDownList ddlFromTime = new DropDownList();
            ddlFromTime.CssClass = "ddlFromTime";
            ddlFromTime.ID = "selectFromTime" + arrayWhoStay[i];
            getItemsFromDate(ddlFromTime);

            string strTime;

            if (!isPostback)
            {
                if (arrayWhoInInit.Count > 0)
                {
                    
                    strTime = ((Unit)arrayWhoInInit[i]).FromHour + ":" + ((Unit)arrayWhoInInit[i]).FromMinute;
                    ddlFromTime.Items.FindByValue(strTime).Selected = true;
                }

            }

            else if(i==index-1)
            {
                ddlFromTime.Items.FindByValue("8:0").Selected = true;
            }


            inputCategoryContainer.Controls.Add(ddlFromTime);


            HtmlGenericControl containerSeperator = new HtmlGenericControl("div");
            containerSeperator.ID = "containerSeperator" + arrayWhoStay[i];
            containerSeperator.Attributes["class"] = "containerSeperator";

            HtmlGenericControl seperator = new HtmlGenericControl("div");
            seperator.ID = "seperator" + arrayWhoStay[i];
            seperator.Attributes["class"] = "seperatorDate";

            


            containerSeperator.Controls.Add(seperator);
            inputCategoryContainer.Controls.Add(containerSeperator);

            DropDownList ddlToTime = new DropDownList();
            ddlToTime.CssClass = "ddlToTime";
            ddlToTime.ID = "selectToTime" + arrayWhoStay[i];
            getItemsFromDate(ddlToTime);
            

            if (!isPostback)
            {
                if (arrayWhoInInit.Count > 0)
                {                   
                    strTime = ((Unit)arrayWhoInInit[i]).ToHour + ":" + ((Unit)arrayWhoInInit[i]).ToMinute;
                    ddlToTime.Items.FindByValue(strTime).Selected = true;
                }

            }

            else if (i == index - 1)
            {
                ddlToTime.Items.FindByValue("19:0").Selected = true;
            }


            inputCategoryContainer.Controls.Add(ddlToTime);

            CheckBox cb = new CheckBox();
            cb.ID = "chk24Hours" + arrayWhoStay[i];
            cb.CssClass = "chk24Hours";
            cb.Attributes.Add("onclick", "setTimeEnabled('" + arrayWhoStay[i] + "');");

            if (!isPostback)
            {
                if (arrayWhoInInit.Count > 0)
                {
                    if (cb.Checked)
                    {
                        ddlFromTime.Enabled = false;
                        ddlToTime.Enabled = false;
                    }
                }
            }

           

            inputCategoryContainer.Controls.Add(cb);


            HtmlGenericControl divTime24 = new HtmlGenericControl("div");
            divTime24.ID = "time24_" + arrayWhoStay[i];
            divTime24.Attributes["class"] = "time24";
            divTime24.InnerText = "24 hrs";

            inputCategoryContainer.Controls.Add(divTime24);


            if (i > 0)
            {
                /*
                 <a href=""><i class="fa fa-times"></i> fa-times</a>
                */

                LinkButton lb = new LinkButton();
                lb.ID = "minus" + arrayWhoStay[i];
                lb.OnClientClick = "javascript:return setSelectBox('selectDate" + arrayWhoStay[i] + "');";             
                lb.Command += new CommandEventHandler(lb_click_minus);
                lb.CommandArgument = arrayWhoStay[i].ToString();
                lb.Text = "<i class='fa fa-times fa-2'></i>";

                inputCategoryContainer.Controls.Add(lb);
                ScriptManager1.RegisterAsyncPostBackControl(lb);
            }


            HtmlGenericControl divClear = new HtmlGenericControl("div");
            divClear.ID = "clear" + arrayWhoStay[i];
            divClear.Attributes["class"] = "clear";

            inputCategoryContainer.Controls.Add(divClear);

            pnlDates.Controls.Add(inputCategoryContainer);
        }
        

    }

    protected void addMultiSelectDay()
    {
        DropDownList ddl = new DropDownList();
        ddl.CssClass = "multiselect";
        ddl.Attributes.Add("multiple", "multiple");
        ddl.ID = "selectDate" + index;

        //multiple="multiple"

        ListItem li = new ListItem();

        li.Text = "Sun";
        li.Value = "Sun";

        ddl.Items.Add(li);

        li = new ListItem();
        li.Text = "Mon";
        li.Value = "Mon";

        ddl.Items.Add(li);

        li = new ListItem();
        li.Text = "Tue";
        li.Value = "Tue";

        ddl.Items.Add(li);

        li = new ListItem();
        li.Text = "Wed";
        li.Value = "Wed";

        ddl.Items.Add(li);

        li = new ListItem();
        li.Text = "Thu";
        li.Value = "Thu";

        ddl.Items.Add(li);

        li = new ListItem();
        li.Text = "Fri";
        li.Value = "Fri";

        ddl.Items.Add(li);

        li = new ListItem();
        li.Text = "Sat";
        li.Value = "Sat";

        ddl.Items.Add(li);

        pnlDates.Controls.Add(ddl);


        CheckBox cb = new CheckBox();
        cb.ID = "chk24Hours";

        pnlDates.Controls.Add(cb);
        

        index++;

    }


    public void setSupplierDays()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfGetWorkingHoursResponse resultOfGetWorkingHoursResponse = new WebReferenceSupplier.ResultOfGetWorkingHoursResponse();
        resultOfGetWorkingHoursResponse=_supplier.GetWorkingHours_Registration2014(new Guid("8AEA98B3-354A-4ABB-825F-DFD1CD5683D1"));
        ////resultOfGetWorkingHoursResponse.Value[0].DaysOfWeek[0] = WebReferenceSupplier.DayOfWeek.Friday;
        ////resultOfGetWorkingHoursResponse.Value[0].DaysOfWeek[1] = WebReferenceSupplier.DayOfWeek.Saturday;
    }   

    protected void lbAddButton_Click(object sender, EventArgs e)
    {
        UpdatePanel1.Update();
    }

    protected void lb_click_minus(object sender, CommandEventArgs e)
    {
        /************  remove the specific control *************/
        Control myControl1 = pnlDates.FindControl("inputCategoryContainer" + e.CommandArgument);
        pnlDates.Controls.Remove(myControl1);

        Control myControlClear1 = pnlDates.FindControl("clear" + e.CommandArgument);
        pnlDates.Controls.Remove(myControlClear1);


        index--;

        arrayWhoStay.Remove(Convert.ToInt32(e.CommandArgument));

        /************  remove the last control added in page load *************/
        pnlDates.Controls.RemoveAt(pnlDates.Controls.Count - 1);
        index--;

        arrayWhoStay.RemoveAt(arrayWhoStay.Count - 1);

        foreach (Control cn in pnlDates.Controls)
        {

        }

        UpdatePanel1.Update();

    }

    protected void getItemsFromDate(DropDownList objSelectbox)
    {

        ListItem liFromTime = new ListItem();
        liFromTime = new ListItem();
        liFromTime.Text = "12 am";
        liFromTime.Value = "24:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "12:30 am";
        liFromTime.Value = "24:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "1 am";
        liFromTime.Value = "1:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "1:30 am";
        liFromTime.Value = "1:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "2 am";
        liFromTime.Value = "2:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "2:30 am";
        liFromTime.Value = "2:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "3 am";
        liFromTime.Value = "3:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "3:30 am";
        liFromTime.Value = "3:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "4 am";
        liFromTime.Value = "4:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "4:30 am";
        liFromTime.Value = "4:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "5 am";
        liFromTime.Value = "5:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "5:30 am";
        liFromTime.Value = "5:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "6 am";
        liFromTime.Value = "6:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "6:30 am";
        liFromTime.Value = "6:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "7 am";
        liFromTime.Value = "7:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "7:30 am";
        liFromTime.Value = "7:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "8 am";
        liFromTime.Value = "8:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "8:30 am";
        liFromTime.Value = "8:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "9 am";
        liFromTime.Value = "9:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "9:30 am";
        liFromTime.Value = "9:30";        
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "10 am";
        liFromTime.Value = "10:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "10:30 am";
        liFromTime.Value = "10:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "11 am";
        liFromTime.Value = "11:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "11:30 am";
        liFromTime.Value = "11:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "12 pm";
        liFromTime.Value = "12:0";
        objSelectbox.Items.Add(liFromTime);


        liFromTime = new ListItem();
        liFromTime.Text = "12:30 pm";
        liFromTime.Value = "12:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "1 pm";
        liFromTime.Value = "13:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "1:30 pm";
        liFromTime.Value = "13:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "2 pm";
        liFromTime.Value = "14:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "2:30 pm";
        liFromTime.Value = "14:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "3 pm";
        liFromTime.Value = "15:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "3:30 pm";
        liFromTime.Value = "15:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "4 pm";
        liFromTime.Value = "16:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "4:30 pm";
        liFromTime.Value = "16:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "5 pm";
        liFromTime.Value = "17:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "5:30 pm";
        liFromTime.Value = "17:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "6 pm";
        liFromTime.Value = "18:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "6:30 pm";
        liFromTime.Value = "18:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "7 pm";
        liFromTime.Value = "19:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "7:30 pm";
        liFromTime.Value = "19:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "8 pm";
        liFromTime.Value = "20:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "8:30 pm";
        liFromTime.Value = "20:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "9 pm";
        liFromTime.Value = "21:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "9:30 pm";
        liFromTime.Value = "21:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "10 pm";
        liFromTime.Value = "22:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "10:30 pm";
        liFromTime.Value = "22:30";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "11 pm";
        liFromTime.Value = "23:0";
        objSelectbox.Items.Add(liFromTime);

        liFromTime = new ListItem();
        liFromTime.Text = "11:30 pm";
        liFromTime.Value = "23:30";
        objSelectbox.Items.Add(liFromTime);          

    }

    protected int index
    {
        get
        {
            return (ViewState["index"] == null ? 0 : (int)ViewState["index"]);
        }
        set
        {

            ViewState["index"] = value;
        }
    }

    protected ArrayList arrayWhoStay
    {
        get
        {
            return (ViewState["arrayWhoStay"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoStay"]);
        }
        set
        {

            ViewState["arrayWhoStay"] = value;
        }
    }

    protected ArrayList arrayWhoInInit
    {
        get
        {
            return (ViewState["arrayWhoInInit"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoInInit"]);
        }
        set
        {

            ViewState["arrayWhoInInit"] = value;
        }
    }

    protected Guid SupplierId
    {
        get { return (ViewState["SupplierId"] == null) ? Guid.Empty : (Guid)ViewState["SupplierId"]; }
        set { ViewState["SupplierId"] = value; }
    }

    [Serializable]
    class Unit
    {
        public bool AllDay {get;set;}
        public int FromHour { get; set; }
        public int FromMinute { get; set; }
        public int ToHour { get; set; }
        public int ToMinute { get; set; }
        public int Order { get; set; }        
    }
}