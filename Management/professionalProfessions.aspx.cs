using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.Text;

public partial class Professional_professionalProfessions : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
            Response.Expires = -1000;
            Response.CacheControl = "no-cache";
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");
            /*
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {
                string csnameLogin = "setLogin";           

                if (!ClientScript.IsClientScriptBlockRegistered(this.GetType(), csnameLogin))
                {
                    string csTextLogin = "alert('t');top.location='" + ResolveUrl("~") + @"Management/ProfessionLogin.aspx';";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), csnameLogin, csTextLogin, true);
                }
                return;
            }
             * */
            //Response.Charset = "utf-8";
            //Response.Write(Server.UrlEncode("�"));
            //Response.Write(Request["startWith"]);

            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            string xmlProfessions = supplier.GetSupplierDetails(siteSetting.GetSiteID, GetGuidSetting());
            //Response.Write(xmlProfessions);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlProfessions);

            StringBuilder sb = new StringBuilder();
            XmlNode root = xml.DocumentElement;
            XmlNode expertise = root.SelectSingleNode("Expertise");

            //string outerExpertise = "";

            //foreach (XmlNode node in root.ChildNodes)
            for (int i = 0; i < expertise.ChildNodes.Count; i++) // element primary
            {
                
                if (expertise.ChildNodes[i].HasChildNodes)
                {
                    for (int y = 0; y < expertise.ChildNodes[i].ChildNodes.Count; y++)
                    {
                        sb.Append(expertise.ChildNodes[i].Attributes["Name"].InnerText + " >> " + expertise.ChildNodes[i].ChildNodes[y].InnerText + "&");
                    }
                }

                else
                    sb.Append(expertise.ChildNodes[i].Attributes["Name"].InnerText + "&");
                
            }

            if (sb.ToString().Length != 0)
                Response.Write(sb.ToString().Substring(0, sb.ToString().Length - 1));
            else
                Response.Write("empty");
            /*
            string professionName = "";
            
            string strReturn = "";

            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {                    
                    professionName = list[i].Profession;

                    if (i < list.Count-1)  
                        strReturn += professionName + "&";                       
                    else
                        strReturn += professionName;           

                }
            }

            else
                strReturn = "empty";

            Response.Write(strReturn);
             */
        }

    }
}
