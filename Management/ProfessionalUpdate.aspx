﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Management/MasterPageAdvertiser.master"
 CodeFile="ProfessionalUpdate.aspx.cs" Inherits="Management_ProfessionalUpdate" validateRequest="false"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asps" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <meta http-equiv="PRAGMA" content="NO-CACHE"/>
    <meta http-equiv="Expires" content="-1"/>

  
    
        
    <script type="text/javascript">
    function OpenWin(url, IsPublisher)
    {
        var oWin = top.window.open(url,"AdvertiserPreview","width=930,height=550,toolbar=no,left=50,scrollbars=yes");
        var ua = navigator.userAgent.toLowerCase();
        if (oWin == null || typeof oWin == "undefined" || !oWin || oWin.closed) {// || oWin.innerHeight==0) {
           
            if(IsPublisher.toLowerCase()=="true" && ua.indexOf("msie")!= -1)                
                alert("<%# GetAllowPopupIE %>");                
            else
                alert("<%# GetAllowPopUp %>");
            return;
        } 
        //Check popup block in chrome
        oWin.onload = function() {
            setTimeout(function() {
                if (oWin.screenX === 0)
                    alert("<%# GetAllowPopUp %>");
            }, 0);
        }
    }
        
    function checkDescLengh(maxLength, control)
    { 
       if(document.getElementById(control).value.length>maxLength)
       {
            //alert(' לא ניתן לכתוב יותר מ ' + maxLength  + 'תווים');
            var sen_a=$get("<%# Hidden_CharacterMax.ClientID %>").value;
            var sen_b=$get("<%# Hidden_Character.ClientID %>").value;
            alert(sen_a+" " + maxLength +" "+sen_b);
            document.getElementById(control).value=document.getElementById(control).value.substring(0,maxLength);
        
       }
    }
    
    
    
    function pageLoad(sender, args)
      {
        
        //remove update progress
        try{
            hideDiv();
        }catch(ex){}
    }
        /*
      function chkConfirmed()
      {
        var is_confirmed = $get("<# chkRegulations.ClientID %>").checked;
        if(!is_confirmed)
            alert("<# GetConfirmed %>");
        else
            showDiv();
           
        return is_confirmed;
      }
      */
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="tbVideo" EnableViewState="true" WatermarkText="Please enter your video's URL from YouTube"></cc1:TextBoxWatermarkExtender>
 
<div class="page-contentm minisite">
    <h2>
        <asp:Label ID="lblWelcometo" runat="server" Text="Welcome to"></asp:Label>
        <asp:Label runat="server" id="lblName"></asp:Label>
        <asp:Label ID="lbl_MiniSite" runat="server" Text="Mini Site"></asp:Label>
    </h2>
	<p>
        <asp:Label ID="lblSubscript" runat="server" >
        This Mini Site will be visible to Web Users. It should be as attractive as possible to make them prefer your business! A complete Mini Site profile is very important for your success.
        </asp:Label>
	</p>
    <div class="clear"></div>
    <div runat="server" visible="false">
	<asps:UpdatePanel runat="server" ID="_updateConfirmed" UpdateMode="Conditional">
		<ContentTemplate>
			<p class="termsminisite" runat="server" id="p_term">
                <asp:CheckBox runat="server"  Checked="true"   ID="chkRegulations"/> 
                <asp:Label ID="lblIAgree" runat="server" Text="By using No Problem I agree to the following"></asp:Label> 
                <a href="javascript:void(0);" target="_blank" id="a_Terms" runat="server">
                    <asp:Label ID="lblTermsNConditions" runat="server" Text="Terms and Conditions"></asp:Label> 
                </a>
            </p>

        </ContentTemplate>
    </asps:UpdatePanel>
        </div>
     <div class="clear"></div>
    <div runat="server" id="div_galleryPics" visible="false">
    <h3><asp:Label ID="lblAddPic" runat="server" Text="Add Pictures to Your Gallery"></asp:Label></h3>
	<p>
		<input id="MyFileImages" type="file" runat="server" class="form-file"/>
		<input id="Submit1" type="submit" value="Upload" runat="server" onserverclick="UploadBtn_Click" class="btn" />
		<asp:Label ID="commentUploadImages" runat="server"></asp:Label>
        <asp:PlaceHolder ID="holderImages" runat="server"></asp:PlaceHolder>
        <label runat="server" id="lblCommentImages"></label>
    </p>
    </div>                     
    <h3><asp:Label ID="lbl_MyLogo" runat="server" Text="My Logo"></asp:Label></h3>
    <p> 
	    <input id="MyFileLogo" type="file" size="20" class="form-file" runat="server" />
    	<input id="Submit2" class="btn" type="submit"    value="Upload" runat="server" onserverclick="UploadBtn2_Click" />
	    <asp:Label ID="commentUploadImages2" runat="server" ></asp:Label>
        <a runat="server" id="a_FilterLink" target="_blank" style="display:block; color: Blue;"></a>
	    <asp:PlaceHolder ID="holderLogo" runat="server"></asp:PlaceHolder>
        <asp:Label ID="lblCommentLogo" runat="server"></asp:Label>
    </p>
    <div runat="server" visible="false">
        <h3><asp:Label ID="lblMyVideo" runat="server" Text="My Video"></asp:Label></h3>
        <asp:TextBox runat="server" ID="tbVideo" CssClass="form-text my-video"></asp:TextBox>
	</div>
    <div runat="server" visible="false">			
        <h3><asp:Label ID="lblShortDescription" runat="server" Text="Short Description of Company "></asp:Label></h3>

        <p class="note">
            <span class="star">*</span>
            <asp:Label ID="lblNo240" runat="server" Text="No more than 240 characters"></asp:Label>
        </p>

        <asp:TextBox MaxLength="1" TextMode="MultiLine" Columns="80" Rows="5" ID="tbShortDescription" CssClass="form-textarea" runat="server"></asp:TextBox>
    </div>

    <h3><asp:Label ID="lblComDescription" runat="server" Text="Company Description"></asp:Label></h3>

    <p class="note">
        <span class="star">*</span>
        <asp:Label ID="lblno5000" runat="server" Text="no more than 500 characters"></asp:Label>
    </p>

    <asp:TextBox MaxLength="1" TextMode="MultiLine" Columns="80" Rows="10" ID="tbDescription" CssClass="form-textarea" runat="server" ></asp:TextBox>

    <label runat="server" id="lblCommentDesc"></label>
    
    <asp:button runat="server" Text="PUBLISH" id="imageSend" CssClass="btn" OnClick="imageSend_Click" />
<%/* 	<asp:button runat="server" Text="PREVIEW" id="imageSendPreview" CssClass="btn" OnClick="imageSendPreview_Click" /> */%>
</div> 

<asp:Label ID="lbl_comment1" runat="server" Text="No picture selected" Visible="false"></asp:Label>
<asp:Label ID="lbl_delete" runat="server" Text="Delete" Visible="false"></asp:Label>

<asp:Label ID="Hidden_AllowPopUp" runat="server" Text="Allow pop-up windows" Visible="false"></asp:Label>
<asp:Label ID="hf_AllowPopupIE" runat="server" Text="Allow pop-up windows. In order for you not to lose data, please click Publish before you confirm the pop-up." Visible="false"></asp:Label>

<asp:Label ID="Hidden_confirmed" runat="server" Text="You can not publish without approval" Visible="false"></asp:Label>

<asp:HiddenField ID="Hidden_CharacterMax" runat="server" Value="You can not write more than"/>
<asp:HiddenField ID="Hidden_Character" runat="server" Value="characters"></asp:HiddenField>
    <asp:Label ID="lbl_ErrorImage" runat="server" Text="Invalid image file" Visible="false"></asp:Label>

</asp:Content>