﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class Management_accountSettings : System.Web.UI.Page
{
    protected string UpdateSuccess;
    protected string UpdateFaild;
    protected string GetServerError;
    protected string PasswordError;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadPageText();

        Page.DataBind();
    }


    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;      

        SetTextFromXml();

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "login"
                          select x).FirstOrDefault();
        
        PasswordError = xelem.Element("password").Element("Validated").Element("Instruction").Element("Error").Value;


    }
    

    void SetTextFromXml()
    {
        XElement xelem = GetSentences();
        UpdateSuccess = xelem.Element("FormMessages").Element("UpdateSuccess").Value;
        UpdateFaild = xelem.Element("FormMessages").Element("UpdateFaild").Value;      

    }

    XElement GetSentences()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "MasterPage"
                          select x).FirstOrDefault();
        return xelem;
    }


    protected void lb_change_email_Click(object sender, EventArgs e)
    {
        
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "UpdateSuccess();", true);

        /*
        if (result.Type == WebReferenceSupplier.eResultType.Success)
        {
            loadDetails();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "UpdateSuccess();", true);

        }

        else
        {
            dbug_log.ExceptionLog(new Exception("failed businessDetails_Registration2014 " + result.Messages[0]));

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
            return;
        }
        */
    }

    protected void lb_change_password_Click(object sender, EventArgs e)
    {

    }
}