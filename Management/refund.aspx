﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="refund.aspx.cs" Inherits="Management_refund" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
	<script type="text/javascript">	    try { Typekit.load(); } catch (e) { }</script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../PlaceHolder.js"></script>
    <link href="../PPC/samplepPpc.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var imgSend = new Image();
        imgSend.src = '../PPC/images/overview/refundClain.png';

        var imgSendHover = new Image();
        imgSendHover.src = '../PPC/images/overview/refundClain-Hover.png';

        function sendHover() {
            //document.getElementById(<%#btn_RefundSubmit.ClientID%>).style.backgroundImage=url(imgSendHover.src);
        }


        function sendOut() {
            //document.getElementById(<%#btn_RefundSubmit.ClientID%>).style.backgroundImage=url(imgSendOut.src);
        }

        function closeAndRefresh(balance, linkRefund) {            
            parent.showHideRefund();
            parent.updateBalance(balance);
            parent.updateSystemMessage("Your refund claim has been approved. Learn more about our <a href='http://www2.noproblemppc.com/LegalRefund.htm'>refund policy</a> and please read our <a href='<%# HonorCode %>'>honor code</a>", 2);
            
            obj = "linkRefund" + linkRefund;

            parent.document.getElementById(obj).style.display = 'none';
            parent.location.hash = "#anchorSystemMessage";
        }
        function _CloseRefund() {
            parent.showHideRefund();

        }
        function _General_Error() {
            _CloseRefund();
            parent._GeneralServerError();
        }
        

    </script>
</head>
<body class="bodyRefund">
    <form id="form1" runat="server">    

    <div class="leftRefund">
        
        <div class="divRefundTitle">Reporting a lead</div>
        
        <div class="divRefundSubTitle">
            This lead is...
        </div>

        <div>
            <asp:RadioButtonList runat="server" ID="radionBtnListRefundReasons" 
                RepeatLayout="Flow" CssClass="ButtonListReason"></asp:RadioButtonList>
        </div>
        
    </div>

    <div class="rightRefund">
        
        <div class="divRefundComent"><span>My comments</span> (optional)</div>
       
        <div class="divRefundDesc">
            <asp:TextBox  ID="txt_RefundDescription"  runat="server" TextMode="MultiLine"  CssClass="refundDescription" place_holder="Tell us was wrong with the lead..." HolderClass="water-mark" ></asp:TextBox>
        </div>

        <div class="divRefundSubmit">
            <asp:Button ID="btn_RefundSubmit" runat="server" OnClick="btn_RefundSubmit_click"  CssClass="refundClain" Text="CLAIM REFUND"  />
        </div>  
           
    </div>

    </form>
</body>
</html>
