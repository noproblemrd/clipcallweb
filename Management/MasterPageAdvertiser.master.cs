using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Management_MasterPageAdvertiser : System.Web.UI.MasterPage
{
    PageSetting ps;
    public Management_MasterPageAdvertiser()
        : base()
    {
        this.ID = PagesNames.Advertiser.ToString();
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //Security- for the browser not save in the cash memory
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");
        ////


        ps = (PageSetting)Page;
        
        if (Session.IsNewSession || ps.siteSetting == null ||
            !ps.userManagement.IsSupplier() ||
            string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
        {
            if (ps.IsCallback())
            {
                Response.End();
                Response.Close();
                return;
            }
            Response.Redirect(ResolveUrl("~") + "Management/LogOut.aspx");
    //        ps.UnrecognizedClient(lbl_logOutMes.Text, ResolveUrl("~/Management/logout.aspx"));
            return;
        }    
            
        MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + ps.siteSetting.GetStyle));
        
        
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        SiteSetting _site = ((PageSetting)Page).siteSetting;
    //    PageSetting ps = (PageSetting)Page;
        if (!IsPostBack)
        {
            if (_site.GetSiteID == PpcSite.GetCurrent().SiteId)
                Response.Redirect("OverView2.aspx");
            string pagename =
                System.IO.Path.GetFileName(ps.Request.ServerVariables["SCRIPT_NAME"]);
       //     setLinksClass(pagename);
      //      SetMenu();

            string logoPath = ConfigurationManager.AppSettings["PublisherLogoWeb"] + @"aspjpeg/" + ps.siteSetting.LogoPath;
            string LogoServerPath = ConfigurationManager.AppSettings["PublisherLogo"] + @"aspjpeg\" + ps.siteSetting.LogoPath;
            if (!string.IsNullOrEmpty(ps.siteSetting.LogoPath) && File.Exists(LogoServerPath))//Server.MapPath(logoPath)))
                ImageLogo.ImageUrl = logoPath;

       //     a_frameAspx.HRef = ResolveUrl("~") + @"Management/frame.aspx";
            lblUserName.Text = " " + ps.userManagement.User_Name + " ";
            lbl_YourBalance.Text = string.Format("{0:#,0.##}", ps.userManagement.balance);
            lbl_Currency.Text = ps.siteSetting.CurrencySymbol;

            lb_increaseBalance.Visible = ps.userManagement.Is_CompleteRegistration;

            if (!string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
                getTranslate(ps.siteSetting.siteLangId);
            
            ps.LoadMasterSiteLinks(this);
           
        }

        if (!string.IsNullOrEmpty(_site.GetGoogleAnalytics))
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GoogleAnalytics", _site.GetGoogleAnalytics, true);
        if (!string.IsNullOrEmpty(_site.GetGoogleCampain))
            Page.Form.Controls.Add(new LiteralControl(_site.GetGoogleCampain));

        Page.Header.DataBind();
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoadDetails", "LoadDetails('" + GetLogOutMessage + "', '" + GetLogOutPath + "');", true);

        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm == null || !sm.IsInAsyncPostBack)
            ps.SetNoIframe();

    }
    /*
    private void SetMenu()
    {
        string pageName =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        
        
        if (pageName == a_frameAspx.HRef)
        {
            a_frameAspx.Attributes.Add("class", "btn-myaccount active");
            btn_myAccount.Attributes.Add("class", "_btn_myaccount active");

            
        }


        else if (pageName == linkProfessionalUpdate.HRef)
        {            
            linkProfessionalUpdate.Attributes.Add("class", "btn-myminisite active");
            btn_myminisite.Attributes.Add("class", "_btn_myminisite active");
        }

        else if (pageName == linkSetting.HRef)
        {
            linkSetting.Attributes.Add("class", "btn-faq active");
            btn_faq.Attributes.Add("class", "_btn_faq active");
        }

        else if (pageName == linkCalls.HRef ||
                pageName == a_balance.HRef ||
                pageName == a_MyReview.HRef)
        {
            linkCalls.Attributes.Add("class", "btn-calls active");
            btn_calls.Attributes.Add("class", "_btn_calls active");

            if (pageName == a_linkCalls.HRef)
                a_linkCalls.Attributes.Add("class", "active");

            else if (pageName == a_balance.HRef)
                a_balance.Attributes.Add("class", "active");

            else if (pageName == a_MyReview.HRef)
                a_MyReview.Attributes.Add("class", "active");
        }
        /*
        else if (pageName == a_balance.HRef)
        {
            /* set parent properties*/
       //     linkCalls.Attributes.Add("class", "btn-calls active");
       //     btn_calls.Attributes.Add("class", "_btn_calls active");

            /* set child properties*/
        //    a_balance.Attributes.Add("class", "active");
      //  }
        
    /*
       
    }
*/
    private bool IfHas4MenueItems()
    {
        return _AdvertiserNavigation.IfHas4MenueItems();
    }
    private void getTranslate(int SiteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "MasterPageAdvertiser.master", SiteLangId);       
    }
    /*
    private void setLinksClass(string pageName)
    {
        
        switch (pageName)
        {
       
            case "frame.aspx":
                a_frameAspx.Attributes.Add("class", "active");                
                break;

            case "ProfessionalUpdate.aspx":                
                linkProfessionalUpdate.Attributes.Add("class", "active");
                break;

            case "MyCalls.aspx":
                linkCalls.Attributes.Add("class", "active");               
                break;

            case "ProfessionalSetting.aspx":                
                linkSetting.Attributes.Add("class", "active");
                break;          

        }

    }
     * */
    protected void lbLogout_Click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveUrl("~") + "Management/LogOut.aspx");
       
    }
    public string GetLogOutMessage
    {
        get
        {
            return lbl_logOutMes.Text;
        }
    }
    public string GetLogOutPath
    {
        get
        {
            return ResolveUrl("~") + "Management/LogOut.aspx";
        }
    }
    protected void lb_increaseBalance_Click(object sender, EventArgs e)
    {
        Response.Redirect("frame.aspx?ToBalance=true");
    }
   
}
