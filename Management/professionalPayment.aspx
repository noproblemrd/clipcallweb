﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professionalPayment.aspx.cs"
    Inherits="Professional_professionalPayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/Management/MasterPageAdvertiser.master" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>

    <script type="text/javascript" src="../general.js"></script>

    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />

    <script type="text/javascript" >
        var IsRechargeChanged = new Boolean();
        var arrPercent;
        var arrValues;
        /*
        function pageLoad(sender, args) {
        IsRechargeChanged = false;
        }
        */
        function setArrs(arrP, arrV) {
            arrPercent = arrP;
            arrValues = arrV;
        }
        function focusTxtBox() {
            document.getElementById("<%# txt_deposite.ClientID %>").focus();
        }
        function setImageCharge() {
            if (document.getElementById("imgCharge").src.indexOf("v.jpg") != -1) {
                document.getElementById("imgCharge").src = '../Management/images/x.png';
            }
            else {
                document.getElementById("imgCharge").src = '../Management/images/v.jpg';
            }

        }

        function witchAlreadyStep(level) {
            parent.window.witchAlreadyStep(level);
            parent.window.setLinkStepActive('linkStep6');

        }
        function CheckMinDeposit(elem) {
            if (!calculateBalance())
                return false;
            var min_deposit = new Number(document.getElementById("<%# hf_MinimumDeposit.ClientID %>").value);
            //       alert("num= "+num+" min= "+min_deposit);
            var num = new Number(elem.value);
            if (num < min_deposit) {

                //      num = min_deposit;
                //       elem.value = num;
                //      calculateBalance();
                document.getElementById("<%# _PanelInvalidDeposit.ClientID %>").style.visibility = "visible";
                return false;
            }
            return true;

        }
        function calculateBalance() {
            document.getElementById("<%# _PanelInvalidDeposit.ClientID %>").style.visibility = "hidden";
            var elm = document.getElementById('<%#txt_deposite.ClientID %>');
            var num = elm.value;

            if (num.length == 0) {

                document.getElementById('<%# txt_AddDeposite.ClientID%>').value = "";
                document.getElementById('<%# txt_NewBalance.ClientID%>').value = "";
                document.getElementById('<%# txt_TotalIncludeVAT.ClientID %>').value = '';
                return false;
            }
            while (num.length > 0) {
                if (isNaN(num) || num.indexOf(' ', 0) > -1) {
                    num = num.substring(0, num.length - 1);
                }
                else
                    break;
            }
            elm.value = Math.round(num);
            if (num.length == 0) {
                document.getElementById('<%# txt_AddDeposite.ClientID%>').value = "";
                document.getElementById('<%# txt_NewBalance.ClientID%>').value = "";
                document.getElementById('<%# txt_TotalIncludeVAT.ClientID %>').value = '';
                return false;
            }


            var parcents = arrPercent.split(';');
            var values = arrValues.split(';');
            var i = -1;
            for (; i < parcents.length - 1; i++) {

                if (num < parseInt(values[i + 1]))
                    break;
            }
            num = parseFloat(num);
            var AmountVAT = Math.round(num * <%# VAT %>);
            document.getElementById('<%# txt_TotalIncludeVAT.ClientID %>').value = AmountVAT;
            var addDeposite;

            if (i < 0) {
                addDeposite = 0;

            }
            else {
                addDeposite = (num * parseInt(parcents[i])) / 100;
            }
            //        document.getElementById('<%# txt_AddDeposite.ClientID%>').value=addDeposite.toFixed(1);
            document.getElementById('<%# txt_AddDeposite.ClientID%>').value = Math.round(addDeposite);
            var currentBalance = convertCleanNum(document.getElementById('<%# lbl_CurrentBalance.ClientID%>').innerHTML);

            var sum = currentBalance + addDeposite + num;
            document.getElementById('<%# txt_NewBalance.ClientID%>').value = Math.round(sum);
            return true;

        }
        function GetPercentPackage(num) {
            var parcents = arrPercent.split(';');
            var values = arrValues.split(';');
            for (var i = 0; i < parcents.length; i++) {

                if (num < parseInt(values[i])) {
                    if (i == 0)
                        return 0;
                    return parcents[i - 1];
                }
            }
            return parcents[parcents.length - 1];
        }
        function convertCleanNum(num) {

            var result = "";
            for (var i = 0; i < num.length; i++) {


                var chr = num.substring(i, i + 1);
                if (chr != ',') {

                    result = result + chr;
                }
            }
            return parseFloat(result);
        }

        function CalculateRecharge(elem) {
            IsRechargeChanged = true;
            Clear_ValidationAutoRecharge();

            var num = elem.value;
            while (!Is_Integer(num) && num.length > 0) {
                num = num.substring(0, num.length - 1);
            }
            elem.value = num;
            if (num.length == 0) {
                document.getElementById("<%# txt_ExtraBonus.ClientID %>").value = "";
                try {
                    document.getElementById("<%# lbl_GetExtraTotalPercent.ClientID %>").innerHTML = "";
                }
                catch (_exc) { }
                RemoveAddReachargedLabels(false);
                //                document.getElementById("<# txt_GetExtraRecharge.ClientID %>").value = "";
                return false;
            }
            RemoveAddReachargedLabels(true);
            var IntNum = parseInt(num);
            var _percent = new Number(document.getElementById("<%# hf_ExtraBonus.ClientID %>").value);
            var _pecebt_package = new Number(GetPercentPackage(IntNum));
            /**** for bonus + package bonus 
            //      _percent += _pecebt_package;
            /**** for bonus + package bonus *****/
            try {
                document.getElementById("<%# lbl_GetExtraTotalPercent.ClientID %>").innerHTML = _percent;
            }
            catch (_exc) { }
            var _value = ((IntNum * _percent) / 100);
            document.getElementById("<%# txt_ExtraBonus.ClientID %>").value = Math.round(_value);
            return true;
        }
        function RemoveAddReachargedLabels(ToShow) {
            try {
                document.getElementById("<%# lbl_ExtraBonus.ClientID %>").style.visibility = (ToShow) ? "visible" : "hidden";
                document.getElementById("<%# lbl_ExtraBonusCurrency.ClientID %>").style.visibility = (ToShow) ? "visible" : "hidden";
                document.getElementById("<%# lbl_GetExtraRecharge.ClientID %>").style.visibility = (ToShow) ? "visible" : "hidden";
                document.getElementById("<%# lblbonuslabel1a.ClientID %>").style.visibility = (ToShow) ? "visible" : "hidden";
            }
            catch (ex) { }

        }
        function GetPaymentMethodSelect() {
            var res = "";
            var RAD = document.getElementById('<%#rbl_PaymentMethod.ClientID %>');
            var _radio = RAD.getElementsByTagName("input");
            for (var i = 0; i < _radio.length; i++) {
                if (_radio[i].checked) {
                    res = _radio[i].value;
                    break;
                }
            }
            return res;
        }
        function PaymentMethod(e) {
            if(!<%# _CanMakeSale %>)
                return;
            var res = GetPaymentMethodSelect();


            var div_cc = document.getElementById('<%# div_CreditCard.ClientID %>');
            var div_payment = document.getElementById('<%# _PanelPaymentTransfer.ClientID %>');
            var IfPaymentCharging = "<%#siteSetting.IfPaymentCharging%>";
            var charchingCompany = "<%#siteSetting.CharchingCompany%>";
            if (res == "<%# ePaymentMethodSIte.CreditCard.ToString() %>") { // CreditCard
                try {
                    div_cc.style.display = 'inline';
                } catch (ex) { }
                try {
                    div_payment.style.display = 'none';
                } catch (ex) { }
                document.getElementById('<%# btn_buyNow.ClientID %>').style.display = 'block';
                document.getElementById('<%# _PanelDeposit.ClientID %>').style.display = 'block';
            }
            else {
                ClearValidationAutoRecharge();
                RemoveAddReachargedLabels(false);
                try {
                    div_payment.style.display = 'block';
                } catch (ex) { }

                if (IfPaymentCharging == "False") {

                    document.getElementById('<%# btn_buyNow.ClientID %>').style.display = 'none';
                    document.getElementById('<%# _PanelDeposit.ClientID %>').style.display = 'none';
                    document.getElementById('<%#lbl_HowTransfer.ClientID%>').style.display = 'block';

                    if (charchingCompany == "platnosci")
                        document.getElementById('<%#platnosciWireTranfer.ClientID%>').style.display = 'none';

                }

                else {
                    if (charchingCompany == "platnosci") {

                        document.getElementById('<%#platnosciWireTranfer.ClientID%>').style.display = 'block';
                    }

                    else {
                        document.getElementById('<%# btn_buyNow.ClientID %>').style.display = 'none';
                        document.getElementById('<%# _PanelDeposit.ClientID %>').style.display = 'none';

                        document.getElementById('<%#lbl_HowTransfer.ClientID%>').style.display = 'block';
                    }
                }

                try {
                    div_cc.style.display = 'none';
                } catch (ex) { }


            }


        }

        function emptyBank() {
            alert("<%#Hidden_WrongBank.Value%>");
            document.getElementById('<%#DropDownListPlatnosciPayTypes.ClientID%>').focus();
        }

        function hideSendButton() {
            document.getElementById("btnSend").style.display = 'none';
            document.getElementById("thanks").style.display = 'block';
        }

        //remove update progress
        function pageLoad(sender, args) {
            try {
                parent.hideDiv();
            } catch (ex) { }
            //IsRechargeChanged = false;
        }
        function SetNewRechargeDetails(IsRecharge) {
            IsRechargeChanged = false;
            document.getElementById("<%# hf_IsRechargeMarked.ClientID %>").value = (IsRecharge) ? "true" : "false";
        }
        function CompleteRegitration(_location) {
            alert(document.getElementById("<%# hf_completeRegistration.ClientID %>").value);
            parent.location = _location;
        }
        function chkTransfer() {
            if (document.getElementById("<%# txt_deposite.ClientID %>").value.length == 0) {
                alert(document.getElementById("<%# hf_MissingDeposit.ClientID %>").value);
                return false;
            }
            if (!CheckMinDeposit(document.getElementById("<%# txt_deposite.ClientID %>")))
                return false;

            var PaymentMethod = GetPaymentMethodSelect();
            if (PaymentMethod == "<%# ePaymentMethodSIte.CreditCard.ToString() %>") {
                var cb_AutoRecharge = document.getElementById("<%# cb_AutoRecharge.ClientID %>");
                if (cb_AutoRecharge && cb_AutoRecharge.checked) {
                    var AutoRecharge = document.getElementById("<%# txt_RechargeAmount.ClientID %>").value;
                    if (!Is_Integer(AutoRecharge)) {
                        document.getElementById("<%# lbl_InvalidRechargeAmount.ClientID %>").style.display = "inline";
                        return false;
                    }
                    var _AutoRecharge = new Number(AutoRecharge);
                    var _min_value = GetMinValueAutoRecharge(); //new Number(document.getElementById("<%# lbl_RechargeMyAccount_value.ClientID %>").innerHTML);

                    if (_min_value > _AutoRecharge) {
                        document.getElementById("<%# lbl_InvalidRechargeAmountMinValue.ClientID %>").style.display = "inline";
                        return false;
                    }
                }
            }

            parent.showDiv()
            return true;
        }
        function GetMinValueAutoRecharge() {
            var _min_deposite = new Number(document.getElementById("<%# hf_MinimumDeposit.ClientID %>").value);
            return _min_deposite;
        }
        function RemoveValidationAutoRecharge(elem) {
            if (!elem.checked) {
                ClearValidationAutoRecharge();
                RemoveAddReachargedLabels(false);
                document.getElementById("<%# txt_RechargeAmount.ClientID %>").setAttribute("readOnly", "readOnly");
            }
            else
                document.getElementById("<%# txt_RechargeAmount.ClientID %>").removeAttribute("readOnly");
        }
        function ClearValidationAutoRecharge() {
            try {
                Clear_ValidationAutoRecharge();
                document.getElementById("<%# txt_ExtraBonus.ClientID %>").value = "";
                document.getElementById("<%# txt_RechargeAmount.ClientID %>").value = "";
                document.getElementById("<%# lbl_GetExtraTotalPercent.ClientID %>").innerHTML = "";

            }
            catch (ex) { }
        }
        function Clear_ValidationAutoRecharge() {
            try {
                document.getElementById("<%# lbl_InvalidRechargeAmount.ClientID %>").style.display = "none";
                document.getElementById("<%# lbl_InvalidRechargeAmountMinValue.ClientID %>").style.display = "none";
            }
            catch (ex) { }

        }
        function chkAutoRecharge() {
            document.getElementById("<%# lbl_InvalidRechargeAmount.ClientID %>").style.display = "none";
            var WasChanged = false
            var IsChecked = document.getElementById("<%# cb_AutoRecharge.ClientID %>").checked;
            var hf_IsRechargeMarked = document.getElementById("<%# hf_IsRechargeMarked.ClientID %>").value;
            var IsWasChecked = (hf_IsRechargeMarked == "true");
            if (hf_IsRechargeMarked.length != 0) {
                if (IsChecked ^ IsWasChecked)
                    WasChanged = true;
                if (IsChecked && IsRechargeChanged)
                    WasChanged = true;
            }
            if (!WasChanged) {
                alert("<%# GetNoRechargeChange %>");
                return false;
            }

            if (IsChecked) {
                var _value = document.getElementById("<%# txt_RechargeAmount.ClientID %>").value;
                if (!Is_Integer(_value)) {
                    document.getElementById("<%# lbl_InvalidRechargeAmount.ClientID %>").style.display = "inline";
                    return false;
                }
                var _val = new Number(_value);
                var _min_value = GetMinValueAutoRecharge(); //new Number(document.getElementById("<%# lbl_RechargeMyAccount_value.ClientID %>").innerHTML);

                if (_min_value > _val) {
                    document.getElementById("<%# lbl_InvalidRechargeAmountMinValue.ClientID %>").style.display = "inline";
                    return false;
                }
            }
            parent.showDiv();
            return true;
        }
        function payByCredit() {
            alert('<%#CreditLinkparams%>');
            window.open('../Payment/credit.aspx?<%#CreditLinkparams%>', 'paymentWindow');
        }
        function SetIncompleteRegistrationPublisher(_location) {
            parent.location = _location;
        }
        function init() {
            parent.goUp();
            RblOptionLoad();
            appl_init();
        }
        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);
        }
        function BeginHandler() {
            parent.showDiv();
        }
        function EndHandler() {
            parent.hideDiv();
        }
        window.onload = init;



        function chk_InvoiceOptions() {
            if (!Page_ClientValidate('MailAddress'))
                return false;
            var _value = GetInvoiceOptionValue();
            if (_value == "Mail")
                return true;
            if (!Page_ClientValidate('MailAddress_email'))
                return false;
            return true;

        }
        function GetInvoiceOptionValue() {
            var rbl_InvoiceOption = document.getElementById("<%# rbl_InvoiceOption.ClientID %>");
            var _radios = rbl_InvoiceOption.getElementsByTagName("input");

            for (var i = 0; i < _radios.length; i++) {
                if (_radios[i].checked)
                    return _radios[i].value;

            }
            return "";
        }
        function chk_EmailOptions(sender, args) {
            var _value = GetInvoiceOptionValue();
            if (_value == "Mail") {
                args.IsValid = true;
                return;
            }
            var _reg = new RegExp("<%# GetEmailRegularExpression %>");
            if (_reg.test(args.Value)) {
                args.IsValid = true;
                return;
            }
            args.IsValid = false;
        }
        function InvoiceOption_click(elem) {
            var hf_InvoiceOptions = document.getElementById("<%# hf_InvoiceOptions.ClientID %>");
            if (elem.checked && elem.value == hf_InvoiceOptions.value)
                return;
            for (var i = 0; i < Page_Validators.length; i++) {
                if (Page_Validators[i].validationGroup == "MailAddress_email" ||
                    Page_Validators[i].validationGroup == "MailAddress")
                    Page_Validators[i].style.display = "none";
            }
            hf_InvoiceOptions.value = elem.value;
            var _details = document.getElementById("<%# hf_InvoiceValue.ClientID %>").value.split(';;;');
            if (_details.length == 2 && _details[0] == elem.value) {
                document.getElementById("<%# txt_MailAddress.ClientID %>").value = _details[1];
                return;
            }
            document.getElementById("<%# txt_MailAddress.ClientID %>").value = "";
        }
        function RblOptionLoad() {
            var rbl_InvoiceOption = document.getElementById("<%# rbl_InvoiceOption.ClientID %>");
            if (!rbl_InvoiceOption)
                return;
            var _radios = rbl_InvoiceOption.getElementsByTagName("input");

            for (var i = 0; i < _radios.length; i++) {
                _radios[i].setAttribute("onclick", "InvoiceOption_click(this);");

            }

        }
        function SetInvoiceOption(_data) {
            document.getElementById("<%# hf_InvoiceValue.ClientID %>").value = _data;
        }
       
    </script>

    <style type="text/css">
        .modalBackground
        {
            background-color: #fff;
            filter: alpha(opacity=70);
            opacity: 0.7px;
        }
    </style>
</head>
<body id="form5" class="iframe-inner step6" style="background-color:transparent;" >
    
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" >
    </cc1:ToolkitScriptManager>
    <h2>
        <asp:Label ID="lbl_wantIncrease" runat="server" Text=" Want to increase your balance?"
            CssClass="_spanShow"></asp:Label>
    </h2>
    <asp:UpdatePanel ID="_UpdatePanelBalance" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Label ID="lbl_YourBalance" runat="server" Text="Your balance:"></asp:Label>
            <asp:Label ID="lbl_CurrentBalance" runat="server"></asp:Label>
            <asp:Label ID="lbl_money" runat="server" Text="<%# Currency_Symbol %>"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
    <h3 class="box-title">
        <asp:Label ID="lbl_SpecialOffers" runat="server" Text="Our special offers" Visible="false">
        </asp:Label>
    </h3>
    <div id="calculetor" class="track_oprions" style="display: block;" runat="server">
    <div id="div_calc" runat="server">
        <div class="Packages_Pricing" id="div_Packages_Pricing" runat="server">
            <asp:Repeater ID="DataListPackege" runat="server">
                <HeaderTemplate>
                    <ul class="Packege_list">
                </HeaderTemplate>
                <ItemTemplate>
                    <li runat="server" id="li_package">
                        <label>
                            <asp:Label ID="lbl_PackegeName" runat="server" Text="<%# Bind('Name') %>"></asp:Label></label>
                        <div class="pack">
                            <p>
                                <asp:Label ID="Label1" runat="server" Text="<%#lbl_PayOver.Text %>"></asp:Label>
                                <asp:Label ID="lbl_PaidAmount" runat="server" Text="<%# Bind('PaidAmount') %>"></asp:Label>
                                <asp:Label ID="Label2" runat="server" Text='<%#siteSetting.CurrencySymbol %>'></asp:Label>
                                <br />
                                <asp:Label ID="Label3" runat="server" Text="<%#lbl_GetExtra.Text %>"></asp:Label>
                                <asp:Label ID="lbl_FreeAmount" runat="server" Text="<%# Bind('FreeAmount') %>"></asp:Label>
                                <asp:Label ID="Label4" runat="server" Text="%"></asp:Label>
                            </p>
                        </div>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div style="text-align:center;" id="div_Payment_Image" runat="server" >
            <asp:Image ID="img_Payment" runat="server" Height="180" Width="440" />
        </div>
        <asp:UpdatePanel ID="_UpdatePanelDeposite" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="_PanelDeposit" runat="server">
                    <table class="TableDeposit">
                        <tr>
                            <td>
                                <asp:Label ID="lbl_deposite" runat="server" Text="I want to deposit"></asp:Label>
                            </td>
                           <td style= "width:80px;">
                                <asp:TextBox ID="txt_deposite" runat="server"  CssClass="form-textauto11" autocomplete="off" onblur="CheckMinDeposit(this);"></asp:TextBox>
                            </td>
                            <td style= "width:50px;">
                                <asp:Label ID="lbl_currency1" runat="server"  CssClass="currency" Text="<%# Currency_Symbol %>"></asp:Label>
                            </td>
                            <td>
                                <div id="_PanelInvalidDeposit" runat="server" class="DivCheckDeposite" style="visibility: hidden;">
                                    <asp:Label ID="lbl_MessMinDeposit" runat="server" CssClass="error-msg" Text="<%# GetMinDepositValidation %>"></asp:Label>
                                    
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="lbl_TotalIncludeVAT" runat="server" Text="Total include VAT"></asp:Label>
                            </td>
                           <td style= "width:80px;">
                                <asp:TextBox ID="txt_TotalIncludeVAT" runat="server"  CssClass="form-textauto11" autocomplete="off" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td style= "width:50px;">
                                <asp:Label ID="Label7" runat="server"  CssClass="currency" Text="<%# Currency_Symbol %>"></asp:Label>
                            </td>
                            <td>
                                
                            </td>
                        </tr>

                        <tr runat="server" id="tr_AddDeposite">
                            <td>
                                <asp:Label ID="lbl_AddDeposite" runat="server" Text="We have added to your deposit"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_AddDeposite" runat="server" CssClass="form-textauto11" ></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lbl_currency2" runat="server"  CssClass="currency" Text="<%# Currency_Symbol %>"></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 45px">
                                <asp:Label ID="lbl_NewBalance" runat="server" Text="Your new balance"></asp:Label>
                            </td>
                            <td style="height: 45px">
                                <asp:TextBox ID="txt_NewBalance" runat="server" CssClass="form-textauto11" ></asp:TextBox>
                            </td>
                            <td style="height: 45px">
                                <asp:Label ID="lbl_currency3" runat="server" CssClass="currency" Text="<%# Currency_Symbol %>"></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    <div class="transfer1">
    <asp:Panel ID="_PanelRadioChoose" runat="server">
        <asp:Label ID="lbl_choose" runat="server" Text="Please choose payment method:" CssClass="payment-method"></asp:Label>
        <asp:RadioButtonList ID="rbl_PaymentMethod" runat="server" RepeatDirection="Horizontal">
        </asp:RadioButtonList>
    </asp:Panel>
    </div>
    <div class="clear"><!-- --></div>
   
    <asp:Panel ID="div_CreditCard" runat="server" CssClass="feilds">
        <div id="div_CreditCardText" runat="server">
            
        </div>
        <div runat="server" id="div_AutoRecharge">
           
            <asp:Label ID="lbl_Title_AutoRecharge" CssClass="bonuslabel2" runat="server"></asp:Label>
         
            <div class="clear"><!-- --></div><br />
            <asp:CheckBox ID="cb_AutoRecharge" runat="server" CssClass="checkbox" onclick="RemoveValidationAutoRecharge(this);" />
            
            <div class="step6autorecharge">
           
                <asp:Label ID="lbl_RechargeMyAccount" runat="server" Text="Recharge my account automatically when my balance falls below"></asp:Label>
                <asp:Label ID="lbl_RechargeMyAccount_value" runat="server" Text="<%# minDepodit %>"></asp:Label>
                <asp:Label ID="Label6" runat="server" Text="<%# _Currency %>"></asp:Label>
                                
               </div>
                <div class="clear"><!-- --></div>
                <div class="bonusgrl">
                 <div class="extrabonus">
                    <asp:Label ID="lbl_RechargeAmount" runat="server" CssClass="bonuslabel" Text="Recharge amount"></asp:Label>
                    <asp:TextBox ID="txt_RechargeAmount" runat="server" CssClass="form-textauto" onkeyup="CalculateRecharge(this);"></asp:TextBox>
                    <asp:Label ID="lbl_RechargeAmountCurrency" runat="server" CssClass="currency2" Text="<%# _Currency %>"></asp:Label>
                    <asp:HiddenField ID="hf_ExtraBonus" runat="server" />
                    <br />
                    <asp:Label ID="lbl_InvalidRechargeAmount" runat="server" Text="Invalid recharge amount" CssClass="error-msg" style="display:none; width:300px;"></asp:Label>
                    <asp:Label ID="lbl_InvalidRechargeAmountMinValue" runat="server" Text="<%# GetMinDepositValidation %>" CssClass="error-msg" style="display:none;"></asp:Label>

                </div>
                
                <div class="extrabonus">
                    <asp:Label ID="lbl_ExtraBonus" runat="server" CssClass="bonuslabel" Text="Your extra bonus"></asp:Label>
                    <asp:TextBox ID="txt_ExtraBonus"  CssClass="form-textauto2"  runat="server"></asp:TextBox>
                    <asp:Label ID="lbl_ExtraBonusCurrency" runat="server" CssClass="currency3" Text="<%# _Currency %>"></asp:Label>
                </div>
                <div class="clear"></div>
                <div class="extrabonus1" id="div_extrabonus1" runat="server" visible="false">
                    <asp:Label ID="lbl_GetExtraRecharge" runat="server" CssClass="bonuslabel1" Text="Get extra"></asp:Label>
                    <div class="extrabonus3">
                    <asp:Label ID="lbl_GetExtraTotalPercent" CssClass="bonuslabel1a" runat="server"></asp:Label>
                    
                    <asp:Label ID="lblbonuslabel1a" runat="server" CssClass="bonuslabel1a" Text="%"></asp:Label>
                    
                    </div>
                    
                </div>
                <div class="clear"><!-- --></div>
                <div class="autorecharge">
                <asp:Button ID="btn_AutoRecharge" CssClass="form-submit" runat="server" Text="Save" OnClick="btn_AutoRecharge_click"
             OnClientClick="return chkAutoRecharge();" />
                
                <asp:HiddenField ID="hf_IsRechargeMarked" runat="server" />
             </div>
                </div>
        </div>
        <div class="creditcard" runat="server" id="divCreditcard">
            <h3>
                <asp:Label ID="lbl_cardDetails" runat="server" Text="Enter credit card details"></asp:Label>
                <img style="cursor: hand;" src="../images/credit-cards.gif" alt="" />
            </h3>
        
        <asp:UpdatePanel ID="_UpdatePanelCreditCard" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="form-fieldSet clearfix">
                    <p class="name">
                        <label for="tb_nameCard" class="must" runat="server" id="lbl_creditCardName">
                            Name of the card holder</label>
                        <asp:TextBox ID="tb_nameCard" runat="server" CssClass="form-text"></asp:TextBox>
                        <asp:Label ID="lbl_InvalidName" runat="server" CssClass="error-msg" Text="Invalid credit card name"
                            Visible="false"></asp:Label>
                    </p>
                    <p class="number">
                        <label for="tb_creditNumber" class="must" runat="server" id="lbl_creditNum">
                            Credit card number</label>
                        <asp:TextBox ID="tb_creditNumber" runat="server" CssClass="form-text"></asp:TextBox>
                        <asp:Label ID="lbl_InvalidCreditCardNum" runat="server" CssClass="error-msg" Text="Invalid credit card number"
                            Visible="false"></asp:Label>
                    </p>
                    <p class="expiry">
                        <label for="ddl_expirymonth" class="must" runat="server" id="lbl_expireDate">
                            Expiry date</label>
                        <asp:DropDownList ID="ddl_expirymonth" runat="server" CssClass="form-select2">
                        </asp:DropDownList>
                        <span class="seperator">/</span>
                        <asp:DropDownList ID="ddl_expiryear" runat="server" CssClass="form-select2">
                        </asp:DropDownList>
                        <asp:Label ID="lbl_InvalidCreditMonth" runat="server" CssClass="error-msg" Text="Invalid credit card month"
                            Visible="false"></asp:Label>
                    </p>
                </div>
                <div class="form-fieldSet clearfix">
                    <p class="code">
                        <label for="tb_cardVerification" class="must" runat="server" id="lbl_vertificationCode">
                            Card verification code</label>
                        <asp:TextBox ID="tb_cardVerification" runat="server" CssClass="form-text"></asp:TextBox>
                        <asp:Label ID="lbl_InvalidCreditCode" runat="server" CssClass="error-msg" Text="Invalid credit card code"
                            Visible="false"></asp:Label>
                    </p>
                    <p class="id">
                        <label for="tb_id" class="must" runat="server" id="lbl_id">
                            Identity card id</label>
                        <asp:TextBox ID="tb_id" runat="server" CssClass="form-text"></asp:TextBox>
                        <asp:Label ID="lbl_InvalidId" runat="server" CssClass="error-msg" Text="Invalid ID"
                            Visible="false"></asp:Label>
                    </p>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
        
    </asp:Panel>
    </div>
    <asp:Panel ID="_PanelPaymentTransfer" runat="server" CssClass="WireTransfer">
        <asp:Label ID="lbl_HowTransfer" runat="server"></asp:Label>
        <asp:Panel runat="server" ID="platnosciWireTranfer" Style="display: none;">
            <asp:Label runat="server" ID="lblBanks">Bank name</asp:Label>
            <asp:DropDownList ID="DropDownListPlatnosciPayTypes" runat="server">
                <asp:ListItem Value="m">mTransfer - mBank</asp:ListItem>
                <asp:ListItem Value="n">MultiTransfer - MultiBank</asp:ListItem>
                <asp:ListItem Value="w">BZWBK - Przelew24</asp:ListItem>
                <asp:ListItem Value="o">Pekeo24Przelew - Bank Pekao</asp:ListItem>
                <asp:ListItem Value="i">Płacę z Inteligo - Inteligo</asp:ListItem>
                <asp:ListItem Value="d">Płać z Nordea</asp:ListItem>
                <asp:ListItem Value="p">Płać z iPKO</asp:ListItem>
                <asp:ListItem Value="h">Płać z BPH</asp:ListItem>
                <asp:ListItem Value="g">Płać z ING</asp:ListItem>
                <asp:ListItem Value="l">LUKAS - e-przelew</asp:ListItem>
                <asp:ListItem Value="u">Eurobank</asp:ListItem>
                <asp:ListItem Value="me">Meritum Bank</asp:ListItem>
                <asp:ListItem Value="wp">Przelew z Polbank</asp:ListItem>
                <asp:ListItem Value="wm">Przelew z Millenium</asp:ListItem>
                <asp:ListItem Value="wk">Przelew z Kredyt Bank</asp:ListItem>
                <asp:ListItem Value="wg">Przelew z BGŻ</asp:ListItem>
                <asp:ListItem Value="wd">Przelew z Deutsche Bank</asp:ListItem>
                <asp:ListItem Value="wr">Przelew z Raiffeisen Bank</asp:ListItem>
                <asp:ListItem Value="wr">Przelew z Raiffeisen Bank</asp:ListItem>
                <asp:ListItem Value="wc">Przelew z Citibank</asp:ListItem>
                <asp:ListItem Value="wn">Przelew z Invest Bank</asp:ListItem>
                <asp:ListItem Value="wi">Przelew z Getin Bank</asp:ListItem>
                <asp:ListItem Value="wy">Przelew z Bankiem Pocztowym</asp:ListItem>
                <asp:ListItem Value="b">bank transfer</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
    </asp:Panel>
    <div class="clear"></div>
    <div id="btnSend" class="divSubmit">
        <asp:Button ID="btn_buyNow" runat="server" Text="BUY NOW!" CssClass="form-submit"
            OnClick="btn_buyNow_Click" OnClientClick="return chkTransfer();" />
        <asp:Label ID="lbl_CanMakeSale" runat="server" Text="Unable to buying credit, you are in a status of 'Legal Handling', you should contact the Finance Department." 
                        Visible="false" CssClass="error-msg" style="font-size: 16px; display: block; text-align:center;"></asp:Label>

    </div>
    <div id="div_InvoiceAddress" runat="server" class="div_InvoiceAddressMain" >
        <asp:Label ID="lbl_InvoiceOption" runat="server" Text="Please choose invoice option" CssClass="bonuslabel2"></asp:Label>
        <asp:RadioButtonList ID="rbl_InvoiceOption" runat="server" RepeatDirection="Horizontal" CssClass="rbl_InvoiceOption">
            <asp:ListItem Value="Email" Text="Email" Selected="True" ></asp:ListItem>
            <asp:ListItem Value="Mail" Text="Mail"></asp:ListItem>
        </asp:RadioButtonList>
        <asp:HiddenField ID="hf_InvoiceOptions" runat="server" Value="Email" />
        <asp:HiddenField ID="hf_InvoiceValue" runat="server" />
        <div class="div_InvoiceAddress">
            <asp:Label ID="lbl_MailAddress" runat="server" Text="Please submit your email / mail address"></asp:Label>
            <asp:TextBox ID="txt_MailAddress" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator_MailAddress" runat="server" 
                ErrorMessage="Missing" ControlToValidate="txt_MailAddress" CssClass="error-msg" 
                Display="Dynamic" ValidationGroup="MailAddress"></asp:RequiredFieldValidator>

            <asp:CustomValidator ID="CustomValidator_MailAddress" runat="server" ErrorMessage="Invalid email"
                ControlToValidate="txt_MailAddress" CssClass="error-msg"  Display="Dynamic"
                ValidationGroup="MailAddress_email" ClientValidationFunction="chk_EmailOptions"></asp:CustomValidator>
        
        </div>
        <asp:Button ID="btn_SetInvoiceOption" runat="server" Text="Save" CssClass="SetInvoiceOption"
         OnClientClick="return chk_InvoiceOptions();" OnClick="btn_SetInvoiceOption_Click" />

    </div>
    <div id="thanks" class="thanks">
        <asp:Label ID="Hidden_Thanks" runat="server" Text="Thanks. your details sent." />
    </div>
    <asp:Panel ID="_PanelStep" runat="server">
        <p class="steps">
            <asp:Label ID="lbl_5out5" runat="server" Text="STEP 6 OUT"></asp:Label>
            <asp:Label ID="lbl_page" runat="server"></asp:Label>
            <img src="../images/bullet.gif" alt="bullet" />
        </p>
    </asp:Panel>
    <asp:Label ID="lbl_mesValidDepositePrice" runat="server" Text="Not illegal deposit price"
        Visible="false"></asp:Label>
    </div>
    <asp:Label ID="lbl_PayOver" runat="server" Text="Pay over " Visible="false"></asp:Label>
    <asp:Label ID="lbl_GetExtra" runat="server" Text="Get extra " Visible="false"></asp:Label>
  <%// <asp:HiddenField ID="_IsSupplier" runat="server" /> %> 
    <asp:HiddenField ID="hf_MissingDeposit" runat="server" Value="Deposit Amount missing"  />
    <asp:HiddenField ID="Hidden_chkItTransfer" runat="server" Value="You should check that the money had been transferred" />
    <asp:Label ID="lbl_WrongAmount" runat="server" Text="Failed. Amount of the deposit is illegal" Visible="false"></asp:Label>
    <asp:HiddenField ID="Hidden_WrongBank" runat="server" Value="Please select bank" />
    <asp:HiddenField ID="Hidden_ChooseBank" runat="server" Value="Choose bank:" />
    <asp:HiddenField ID="Hidden_EmailSupllier" runat="server" Value="We got yours payment details. We will soon take care of your request." />
    <asp:HiddenField ID="Hidden_EmailCompany" runat="server" Value="We got yours payment details." />
    <asp:HiddenField ID="Hidden_PpcPackage" runat="server" Value="PPC Package" />
    <asp:HiddenField ID="hf_completeRegistration" runat="server" Value="Registration completed successfully" />
    <asp:HiddenField ID="hf_MinimumDeposit" runat="server" Value="<%# minDepodit %>" />
    <asp:Label ID="lbl_MinDepositMessage" runat="server" Text="Minimum deposit is" Visible="false"></asp:Label>
    
    
    <asp:HiddenField ID="lbl_WrongVoucher" runat="server" Value="The system didn’t find the voucher that you submitted please try again"></asp:HiddenField>
    <asp:HiddenField ID="lbl_BalanceZero" runat="server" Value="The balance for this voucher is zero. Please try different voucher or contact No Problem for new voucher"></asp:HiddenField>

    <asp:Label ID="lbl_Title_AutoRecharge1" runat="server" Text="Save time in the future with Auto-Recharge and get extra" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Title_AutoRecharge2" runat="server" Text="Bonus" Visible="false"></asp:Label>


    <asp:Label ID="Hidden_WrongDetails" runat="server" Text="Failed. Correct Wrong Details" Visible="false"></asp:Label>

    <asp:Label ID="lbl_PaymentMethodProblems" runat="server" Text="There are problems with the payment method defined" Visible="false"></asp:Label>
   
    <asp:Label ID="lbl_NoRechargeChange" runat="server" Text="There are no new recharge" Visible="false"></asp:Label>
    


    </form>
    
    
    
</body>
</html>
