<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professionalDates.aspx.cs" Inherits="Professional_professionalDates"  Debug="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>No Problem</title>
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
    

    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript" src="../Calender/Calender.js"></script>

    <script type="text/javascript" src="../AutoCompleteCity/AutoCompleteCity.js"></script>
    <script src="//code.jquery.com/jquery-1.7.1.min.js" type="text/javascript"></script>
    <link href="../AutoCompleteCity/StyleAutoComplete.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" >

        function witchAlreadyStep(level) {
            parent.window.witchAlreadyStep(level);
            parent.window.setLinkStepActive('linkStep4');
        }

        function showHideZminut() {
            if (document.getElementById("div_zminut").style.display == 'none')
                document.getElementById("div_zminut").style.display = 'block';
            else
                document.getElementById("div_zminut").style.display = 'none';

        }

        function showZminut() {
            document.getElementById("div_zminut").style.display = 'inline';
        }


        function setDayView(index) {
            //alert("hhh");
            if (document.getElementById("tr" + index).style.display == 'none')
                document.getElementById("tr" + index).style.display = 'inline';
            else
                document.getElementById("tr" + index).style.display = 'none';
        }
        function Header_Update() {
            if (parent.SetHeaderUpdate)
                parent.SetHeaderUpdate(4);
        }
        function status_Update() {
            if (parent.SetAccountStatus)
                parent.SetAccountStatus();
        }
        function setStep5() {

            parent.window.step5();
        }

        function checkTimes(ddlFrom, ddlTo, lbl) {
            var ddl_from = document.getElementById(ddlFrom);
            var ddl_to = document.getElementById(ddlTo);
            if (ddl_from.selectedIndex > ddl_to.selectedIndex) {
                ddl_to.selectedIndex = ddl_from.selectedIndex;

            }
            document.getElementById(lbl).innerHTML = setLabelTimes(ddlFrom, ddlTo);
        }
        function SetEditLink(div_title, div_update) {

            var divTitle = document.getElementById(div_title);
            var divUpdate = document.getElementById(div_update);
            if (divTitle.style.display == 'none') {
                divTitle.style.display = 'inline';
                divUpdate.style.display = 'none';
            }
            else {
                divTitle.style.display = 'none';
                divUpdate.style.display = 'inline';
            }
        }
        function SetTimeDiv(e, div_times, ddl_from, ddl_to, labelStatus) {
            var res;
            if (window.event)
                res = window.event.srcElement.value;
            else
                res = e.target.value;
            var divTimes = document.getElementById(div_times);

            if (res == 'cancel') {
                divTimes.style.visibility = 'hidden';
                var result = document.getElementById('lblSuspend').value;
                document.getElementById(labelStatus).innerHTML = result;
            }
            else {
                divTimes.style.visibility = 'visible';
                document.getElementById(labelStatus).innerHTML = setLabelTimes(ddl_from, ddl_to);

            }
        }
        function setLabelTimes(ddlFrom, ddlTo) {
            var _from = document.getElementById(ddlFrom);
            var _to = document.getElementById(ddlTo);
            return _from.options[_from.selectedIndex].innerHTML + " - " + _to.options[_to.selectedIndex].innerHTML;
        }

        function CalenderShow(sender, args) {
            //   alert('in');
            //      alert(sender._textbox._element.id);
            set_div(sender);
            /*      var txt_box= sender._textbox._element;
            var calender = sender._popupDiv;
            //       alert("top="+findTop(txt_box)+";left="+findLeft(txt_box));
            calender.style.top=findTop(txt_box);
            calender.style.left=findLeft(txt_box);
            calender.style.position = "absolute";*/
        }
        function showCalender(_calender) {
            var calender = $find(_calender);
            var _format = document.getElementById("<%# Hidden_dateFormat.ClientID %>").value;
            var _date = calender.get_element().value;
            var dt = new Date(GetDateFormat(_date, _format));
            //   alert(dt.getMonth()+1);
            //  calender.set_switchMonth=dt.getMonth()+1;
            calender.set_selectedDate(dt);
            calender.show();
        }

        function _CheckDateFrom(sender, args) {
            var _format = document.getElementById("<%# Hidden_dateFormat.ClientID %>").value;
            var start = $get("<%# txtFromDate.ClientID %>").value;
            var end = $get("<%# txtToDate.ClientID %>").value;
            if (!IsValidDate(start, _format)) {
                //       alert('9000');
                $get("<%# txtFromDate.ClientID %>").value = end;
                return false;
            }

            //     CheckDateFrom(start, end);
            var _start = GetDateFormat(start, _format);
            var _end = GetDateFormat(end, _format);
            if (IfLateFromToday(_start)) {
                alert(document.getElementById("<%# hf_PastDate.ClientID %>").value);
                if (document.getElementById("<%# cb_UnlimitedTime.ClientID %>").checked) {
                    var _date = new Date();
                    $get("<%# txtFromDate.ClientID %>").value = GetDateString(new Date(_date), _format);
                    return;
                }
                $get("<%# txtFromDate.ClientID %>").value = end;
                return false;
            }
            if (_start > _end) {
                _end = _start;
                $get("<%# txtToDate.ClientID %>").value = GetDateString(new Date(_start), _format);
            }
            if (!IfMilliTimeOk(_start, _end))
                $get("<%# txtFromDate.ClientID %>").value = end;

        }
        function _CheckDateTo(sender, args) {
            var _format = document.getElementById("<%# Hidden_dateFormat.ClientID %>").value;
            var start = $get("<%# txtFromDate.ClientID %>").value;
            var end = $get("<%# txtToDate.ClientID %>").value;
            if (!IsValidDate(end, _format)) {
                //    alert('9000');
                $get("<%# txtToDate.ClientID %>").value = start;
                return false;
            }
            //     CheckDateFrom(start, end);
            var _start = GetDateFormat(start, _format);
            var _end = GetDateFormat(end, _format);
            if (IfLateFromToday(_end)) {
                alert(document.getElementById("<%# hf_PastDate.ClientID %>").value);
                $get("<%# txtToDate.ClientID %>").value = start;
                return false;
            }
            if (_start > _end) {
                _start = _end;
                $get("<%# txtFromDate.ClientID %>").value = GetDateString(new Date(_end), _format);
            }
            if (!IfMilliTimeOk(_start, _end))
                $get("<%# txtToDate.ClientID %>").value = start;
        }
        function HideCalender(_calender, control) {
            if (mouseover)
                return;

            var calender = $find(_calender);
            calender.hide();
            var date_format = document.getElementById('<%# Hidden_dateFormat.ClientID %>').value;
            if (!validate_Date(control.value, date_format)) {
                alert(document.getElementById('<%# Hidden_dateWrong.ClientID %>').value + " " + date_format);
                control.value = "";

            }

        }

        //////
        ///remove update progress
        function pageLoad(sender, args) {
            /*
            var calender=$find('CalendarPopupFrom');
            for(att in calender)
            {
            if(att.toLowerCase().indexOf("set")==-1)
            continue;
            alert("attribute name= "+att);
            alert("attribute data= "+calender[att]);
            }
            alert( calender.get_element().id);
            */
            //remove update progress
            try {
                parent.hideDiv();
            } catch (ex) { }
        }

        function init() {
            parent.goUp();
            SetMillisexToTimeNow();          

        }

        window.onload = init;


        function IfMilliTimeOk(start, end) {
            var valStr = document.getElementById("<%# hf_MilliTime.ClientID %>").value;
            //  alert(valStr);
            if (valStr.length == 0 || isNaN(valStr)) {
                //      parent.showDiv();
                return true;
            }
            var _value = new Number();
            _value = parseInt(valStr);
            //   alert("range= " +(end-start)+" vakue= "+_value);
            if ((end - start) >= _value) {
                var _days = _value / (24 * 60 * 60 * 1000);
                var _mes = document.getElementById("<%# hf_AvailabilityPeriod1.ClientID %>").value;
                _mes += " " + _days;
                _mes += " " + document.getElementById("<%# hf_AvailabilityPeriod2.ClientID %>").value;
                alert(_mes);
                return false;
            }
            return true;

        }
        function IfLateFromToday(_date) {
            var _today = parseInt(document.getElementById("<%# hf_MilliTimeNow.ClientID %>").value);
            //      alert("today= " + _today + " ||| date= "+_date);
            //   SetMillisexToTimeNow();
            return (_today > _date);
        }
        function SetMillisexToTimeNow() {
            var _now = new Date();
            var _today = new Date(_now.getFullYear(), _now.getMonth(), _now.getDate());
            //      _today.setFullYear(_now.getFullYear() , _now.getMonth() ,_now.getDate(),0,0,0,0);
            //      alert("y= "+_now.getFullYear()+ " m= " +_now.getMonth()+" d= "+_now.getDate());
            //     alert(new Date().getTime());
            document.getElementById("<%# hf_MilliTimeNow.ClientID %>").value = _today.getTime();
        }
        function SetDateToday() {
            var _format = document.getElementById("<%# Hidden_dateFormat.ClientID %>").value;
            var elm_from = document.getElementById("<%# txtFromDate.ClientID %>");
            var elm_to = document.getElementById("<%# txtToDate.ClientID %>");
            var _date = new Date();
            elm_to.value = GetDateString(new Date(_date), _format);
            elm_from.value = GetDateString(new Date(_date), _format);
        }

        function RemoveInactive(sender) {
            for (var i = sender.options.length - 1; i > -1; i--) {
                if (sender.options[i].getAttribute("inactive") != null) {
                    sender.remove(i);
                }
            }
        }
        function SetUnlimitedTime(_elem) {
            /*
            var span_ToDate = document.getElementById("<%# span_ToDate.ClientID %>");
            var span_UnlimitedTime = document.getElementById("<%# span_UnlimitedTime.ClientID %>");
            if (_elem.checked) {
            span_ToDate.style.display = "none";
            span_UnlimitedTime.style.display = "inline";
            }
            else {
            span_ToDate.style.display = "inline";
            span_UnlimitedTime.style.display = "none";
            $find("CalendarPopupTo").set_selectedDate($find("CalendarPopupFrom").get_selectedDate());
            //         var _from = $find("CalendarPopupFrom");
            //         var s = 0;
            }
            */
            var txtToDate = document.getElementById("<%# txtToDate.ClientID %>");
            var span_ToDate = document.getElementById("<%# span_ToDate.ClientID %>");
            var a_toDate = document.getElementById("<%# a_toDate.ClientID %>");
            if (_elem.checked) {
                txtToDate.readOnly = true;
                toggleDisabled(span_ToDate, true);
                a_toDate.style.display = "none";
                txtToDate.value = document.getElementById("<%# lbl_UnlimitedTime.ClientID %>").innerHTML;
            }
            else {
                txtToDate.readOnly = false;
                toggleDisabled(span_ToDate, false);
                a_toDate.style.display = "inline";
                SetDateToday();
            }
        }       


        function updateBalance(balance) {
            $('.lbl_Balance', window.parent.document).html(balance);
        }



        function allTasksDone(bonusAmmount) { 
                          
            var balance=$('.lbl_Balance', window.parent.document).html();            
            balance = balance.replace(',', '');
            balance = parseFloat(balance);
            balance += bonusAmmount * 1;

            updateBalance(addCommas(balance));
           
        }

</script>

</head>
<body id="form3" class="iframe-inner step4" style="background-color:transparent;">
<form id="formdates" action="" method="post" runat="server">
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
 ScriptMode="Release">
     <Services>
        <asp:ServiceReference Path="../WebServiceSite.asmx"></asp:ServiceReference>
     </Services>
</cc1:ToolkitScriptManager> 
<div>
    <center>
        <p>
            <asp:Label ID="lbl_title" runat="server" Text="Please choose the hours during which you would like to receive calls from Web users."></asp:Label>
        </p>
    </center>
</div>         

		
<asp:Repeater runat="server" ID="_reapeter" 
    onitemdatabound="_reapeter_ItemDataBound" >
    <HeaderTemplate>
        <table cellspacing="0" class="main">
    </HeaderTemplate>                     
    <ItemTemplate>       
	    <tr>
			<td class="regular">                
                <asp:Label ID="lbl_day" runat="server" Text="<%# Bind('day') %>" ></asp:Label>
                <asp:Label ID="lbl_OrgDay" runat="server" Text="<%# Bind('OrgDay') %>" Visible="false" ></asp:Label>
			</td>
			<td class="regular">
			    <div id="div_title" runat="server">
			        <asp:Label runat="server" id="lblSundayFrom" EnableViewState="false"></asp:Label> 
			        <asp:Label runat="server" id="lblTo1" EnableViewState="false" Text="<%# Bind('status') %>"></asp:Label>
			    </div>
			    <div id="div_update" runat="server" style="display:none;">				     
				    <p><asp:RadioButtonList runat="server" ID="rb_Mode"   RepeatLayout="Flow"></asp:RadioButtonList></p>
                    <div id="div_times" runat="server">
                        <p>   
                            <asp:DropDownList runat="server" id="ddl_From"   AutoPostBack="false"  EnableViewState="false" CssClass="form-select4">                
                                <asp:ListItem  Value="00:00">00:00</asp:ListItem>
                                <asp:ListItem  Value="01:00">01:00</asp:ListItem>
                                <asp:ListItem  Value="02:00">02:00</asp:ListItem>
                                <asp:ListItem  Value="03:00">03:00</asp:ListItem>
                                <asp:ListItem  Value="04:00">04:00</asp:ListItem>
                                <asp:ListItem  Value="05:00">05:00</asp:ListItem>
                                <asp:ListItem  Value="06:00">06:00</asp:ListItem>
                                <asp:ListItem  Value="07:00">07:00</asp:ListItem>
                                <asp:ListItem  Value="08:00">08:00</asp:ListItem>
                                <asp:ListItem  Value="09:00">09:00</asp:ListItem>
                                <asp:ListItem  Value="10:00">10:00</asp:ListItem>
                                <asp:ListItem  Value="11:00">11:00</asp:ListItem>
                                <asp:ListItem  Value="12:00">12:00</asp:ListItem>
                                <asp:ListItem  Value="13:00">13:00</asp:ListItem>
                                <asp:ListItem  Value="14:00">14:00</asp:ListItem>
                                <asp:ListItem  Value="15:00">15:00</asp:ListItem>
                                <asp:ListItem  Value="16:00">16:00</asp:ListItem>
                                <asp:ListItem  Value="17:00">17:00</asp:ListItem>
                                <asp:ListItem  Value="18:00">18:00</asp:ListItem>
                                <asp:ListItem  Value="19:00">19:00</asp:ListItem>
                                <asp:ListItem  Value="20:00">20:00</asp:ListItem>
                                <asp:ListItem  Value="21:00">21:00</asp:ListItem>
                                <asp:ListItem  Value="22:00">22:00</asp:ListItem>
                                <asp:ListItem  Value="23:00">23:00</asp:ListItem>                            
                            </asp:DropDownList>

                            <asp:Label ID="lbl_To1" runat="server" Text="To" ></asp:Label>                        
                    
                            <asp:DropDownList runat="server" id="ddl_To" CssClass="form-select4" >
                                <asp:ListItem  Value="01:00">01:00</asp:ListItem>
                                <asp:ListItem  Value="02:00">02:00</asp:ListItem>
                                <asp:ListItem  Value="03:00">03:00</asp:ListItem>
                                <asp:ListItem  Value="04:00">04:00</asp:ListItem>
                                <asp:ListItem  Value="05:00">05:00</asp:ListItem>
                                <asp:ListItem  Value="06:00">06:00</asp:ListItem>
                                <asp:ListItem  Value="07:00">07:00</asp:ListItem>
                                <asp:ListItem  Value="08:00">08:00</asp:ListItem>
                                <asp:ListItem  Value="09:00">09:00</asp:ListItem>
                                <asp:ListItem  Value="10:00">10:00</asp:ListItem>
                                <asp:ListItem  Value="11:00">11:00</asp:ListItem>
                                <asp:ListItem  Value="12:00">12:00</asp:ListItem>
                                <asp:ListItem  Value="13:00">13:00</asp:ListItem>
                                <asp:ListItem  Value="14:00">14:00</asp:ListItem>
                                <asp:ListItem  Value="15:00">15:00</asp:ListItem>
                                <asp:ListItem  Value="16:00">16:00</asp:ListItem>
                                <asp:ListItem  Value="17:00">17:00</asp:ListItem>
                                <asp:ListItem  Value="18:00">18:00</asp:ListItem>
                                <asp:ListItem  Value="19:00">19:00</asp:ListItem>
                                <asp:ListItem  Value="20:00">20:00</asp:ListItem>
                                <asp:ListItem  Value="21:00">21:00</asp:ListItem>
                                <asp:ListItem  Value="22:00">22:00</asp:ListItem>
                                <asp:ListItem  Value="23:00">23:00</asp:ListItem>
                                <asp:ListItem  Value="23:59">23:59</asp:ListItem>
                            </asp:DropDownList> 
                        </p> 
                    </div>                     
			    </div>		 
                <asp:Label ID="lbl_fromTime" runat="server" Text="<%# Bind('from') %>" Visible="false"></asp:Label>
                <asp:Label ID="lbl_toTime" runat="server" Text="<%# Bind('until') %>" Visible="false"></asp:Label>
			</td>
			<td class="edit last">
			    <a href="#" runat="server" id="a_Edit">
                    <asp:Label ID="Label1" runat="server" Text="<%# lbl_Edit.Text  %>"></asp:Label>
			    </a>
			</td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</table>			 
	</FooterTemplate>           
</asp:Repeater>
		
<div class="suspend">
    <asp:CheckBox runat="server" id="chk_suspend" />
    <asp:Label ID="lbl_suspend" runat="server" Text="I would like to suspend my account"></asp:Label>
    <asp:Image ID="img_AccountSuspend" runat="server" ImageUrl="../images/icon-q.png" />     
</div>
        
<asp:UpdatePanel ID="_UpdatePanelMessage" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="_PanelDateMessage" runat="server"  Width="200px" CssClass="div_message">
            <asp:Label ID="lbl_InvalidFromDate" runat="server" Visible="false" Text="Invalid From Date"></asp:Label>
            <asp:Label ID="lbl_InvalidToDate" runat="server" Visible="false" Text="Invalid To Date"></asp:Label>
            <asp:Label ID="lbl_ResonsMessage" runat="server" Visible="false" Text="You must choose a reason"></asp:Label>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
        
<div id="div_zminut" style="display: none;"> 
    <asp:UpdatePanel ID="_UpdatePanelToDate" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
            <div class="fromtostep4">    
                <div class="form-field from-date">
                    <span class="from_to_date">
                       <asp:Label ID="lbl_fromDate" runat="server" CssClass="label" Text="From date"></asp:Label><br />
                    </span> 
                       
                    <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-text"
                    onclick="javascript:showCalender('CalendarPopupFrom');" 
                    onblur="javascript:HideCalender('CalendarPopupFrom', this);"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" PopupPosition="BottomLeft" 
                    runat="server"  TargetControlID="txtFromDate" PopupButtonID="a_fromDate"  
                    Format="MM/dd/yyyy" BehaviorID="CalendarPopupFrom" OnClientShown="CalenderShow"
                    OnClientHidden="_CheckDateFrom">
                    </cc1:CalendarExtender>                  
                    <a href="#" runat="server" id="a_fromDate">           
                        <asp:Image  runat="server" ID="imgFromDate" ImageUrl="../images/calendar.png" CssClass="calendar" />                   
                    </a>
                    <cc1:TextBoxWatermarkExtender ID="_WaterMarkFrom" runat="server" TargetControlID="txtFromDate"
                    WatermarkCssClass="WaterMarkText" WatermarkText="MM/dd/yyyy"></cc1:TextBoxWatermarkExtender>
                </div>
                   
                <div class="form-field to-date">
                    <span class="from_to_date">
                        <asp:Label ID="lbl_toDate" runat="server" CssClass="label" Text="To date"></asp:Label><br />
                    </span>    
                    <span runat="server" id="span_ToDate">
                        <asp:TextBox runat="server"  ID="txtToDate" CssClass="form-text"
                        onclick="javascript:showCalender('CalendarPopupTo');" 
                        onblur="javascript:HideCalender('CalendarPopupTo', this);"></asp:TextBox>                  
                        
                        <cc1:CalendarExtender ID="CalendarExtender2" PopupPosition="BottomLeft" runat="server"  
                        TargetControlID="txtToDate" PopupButtonID="a_toDate" Format="MM/dd/yyyy"
                        BehaviorID="CalendarPopupTo" OnClientShown="CalenderShow" OnClientHidden="_CheckDateTo">
                        </cc1:CalendarExtender>                 
                        <a href="#" runat="server" id="a_toDate">            
                            <asp:Image runat="server" ID="imgToDate" ImageUrl="../images/calendar.png" CssClass="calendar" />                  
                        </a>   
                        <cc1:TextBoxWatermarkExtender ID="_WaterMarkTo" runat="server" TargetControlID="txtToDate"
                        WatermarkCssClass="WaterMarkText" WatermarkText="MM/dd/yyyy" BehaviorID="WaterTo"></cc1:TextBoxWatermarkExtender>
                    </span>
                    <span runat="server" id="span_UnlimitedTime">
                        <asp:Label ID="lbl_UnlimitedTime" runat="server" Text="Unlimited time"></asp:Label>
                    </span>
                    
                </div>
            
                
           <div class="form-field">
                <asp:Label ID="lbl_reason" runat="server" CssClass="label4" Text="Reason for unavailability"></asp:Label><br />
                <asp:DropDownList runat="server" ID="ddlReason" CssClass="form-select" onchange="RemoveInactive(this);">
                    
                    <asp:ListItem Text="Vacation" Value="Vacation"></asp:ListItem>
                    <asp:ListItem Text="Load at work" Value="Load at work"></asp:ListItem>
                    <asp:ListItem Text="Sickness" Value="Sickness"></asp:ListItem>
                    <asp:ListItem Text="Difference" Value="Difference"></asp:ListItem>
                </asp:DropDownList>
            </div>
           
            <div class="UnlimitedTime01">  
               <asp:CheckBox ID="cb_UnlimitedTime" runat="server" Text="Unlimited time" onclick="SetUnlimitedTime(this);" />
           </div>
           
            </div>
        </ContentTemplate>
   </asp:UpdatePanel>    
</div>                
<div class="clear"><!-- --></div>
<div class="datessubmit"><asp:Button CssClass="form-submit" runat="server" ID="sndBtn" OnClick="sndBtn_Click" OnClientClick="parent.showDiv();" /></div>

<p class="steps" runat="server" id="pSteps">
    <asp:Label ID="lbl_step" runat="server" Text="STEP 4 OUT"></asp:Label>
    <asp:Label ID="lbl_page" runat="server" ></asp:Label>
    <img src="../images/bullet.gif" alt="bullet" />
</p>
		
<div id="hide_div">
    <asp:Label ID="lbl_SuspendCalls" runat="server" Text="Suspend calls" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ActiveDuring" runat="server" Text="Active during" Visible="false"></asp:Label>
    <asp:HiddenField ID="lblSuspend" runat="server" Value="Suspend" />
</div>

<div>
    <asp:Label ID="lbl_NEXT" runat="server" Text="NEXT" Visible="false"></asp:Label>
    <asp:Label ID="lbl_UPDATE" runat="server" Text="UPDATE" Visible="false"></asp:Label>
    <asp:Button ID="btn_VirtualControl" runat="server" />
</div>
<asp:HiddenField runat="server" id="Hidden_dateWrong" Value="Wrong date format. The right one is"/>
<asp:HiddenField runat="server" id="Hidden_dateFormat" Value="MM/DD/YYYY"/>

<asp:Label ID="lbl_Edit" runat="server" Text="Edit" Visible="false"></asp:Label>
<asp:HiddenField ID="hf_MilliTime" runat="server" />
<asp:HiddenField ID="hf_MilliTimeNow" runat="server" />

<asp:HiddenField ID="hf_PastDate" runat="server" Value="You can not select past date" />

<asp:HiddenField ID="hf_AvailabilityPeriod1" runat="server" Value="Your availability period cannot be longer than" />

<asp:HiddenField ID="hf_AvailabilityPeriod2" runat="server" Value="days" />
<asp:Label ID="lbl_AvailableAtLeastOneDay" runat="server" Text="You must be available at least one day" Visible="false"></asp:Label>

</form>


</body>
</html>