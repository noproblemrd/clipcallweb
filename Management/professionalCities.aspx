﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professionalCities.aspx.cs" Inherits="Management_professionalCities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>No Problem Choose Cities</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
     
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
    <link href="../UpdateProgress/updateProgress.css" />
    
    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=en"></script>

    <script type="text/javascript"> 
    
        var indexArr=-1;     
        
        arrCities=new Array();
        arrGuidCities=new Array();
      
        function addRegion()
        {
            //alert("addRegion()");            
           
            if(document.getElementById("<%#selectNotCities.ClientID%>").selectedIndex>=0)
            {
                
                //alert("addRegion2()");
                var arrDelete=new Array();      
                
                var strRegions;                 
                
                
                for(i=0;i<document.getElementById("<%#selectNotCities.ClientID%>").options.length;i++)
                {
                    if(document.getElementById("<%#selectNotCities.ClientID%>").options[i].selected)
                    {
                        indexArr++;                   
                        
                        arrDelete[indexArr]=i;
                        
                        //alert("splitArrow:" + splitArrow.length);
                        document.getElementById("<%#selectCities.ClientID%>").options[document.getElementById("<%#selectCities.ClientID%>").options.length]=new Option(document.getElementById("<%#selectNotCities.ClientID%>").options[i].text,document.getElementById("<%#selectNotCities.ClientID%>").options[i].value);
                        
                    }    
                             
                }
                
                index=0;
            
                for(i=0;i<arrDelete.length;i++)
                {             
                   //alert(arrDelete[i]);
                    document.getElementById("<%#selectNotCities.ClientID%>").remove(arrDelete[i]-index);
                    index++;      
                       
                } 
                
                indexArr=-1;
                     
                return true;
                
            }
            
            else
            {               
                alert('<%#hidden_placeAtleast.Value%>');              
                return false;
            }
            
            
        }

        
        function removeRegion()
        {
            //alert("addRegion()");            
           
            if(document.getElementById("<%#selectCities.ClientID%>").selectedIndex>=0)
            {
                
                //alert("addRegion2()");
                var arrDelete=new Array();      
                
                var strRegions;                 
                
                
                for(i=0;i<document.getElementById("<%#selectCities.ClientID%>").options.length;i++)
                {
                    if(document.getElementById("<%#selectCities.ClientID%>").options[i].selected)
                    {
                        indexArr++;                   
                        
                        arrDelete[indexArr]=i;
                        
                        //alert("splitArrow:" + splitArrow.length);
                        document.getElementById("<%#selectNotCities.ClientID%>").options[document.getElementById("<%#selectNotCities.ClientID%>").options.length]=new Option(document.getElementById("<%#selectCities.ClientID%>").options[i].text,document.getElementById("<%#selectCities.ClientID%>").options[i].value);
                        
                    }    
                             
                }
                
                index=0;
            
                for(i=0;i<arrDelete.length;i++)
                {             
                   //alert(arrDelete[i]);
                    document.getElementById("<%#selectCities.ClientID%>").remove(arrDelete[i]-index);
                    index++;      
                       
                } 
                
                indexArr=-1;
                     
                return true;
                
            }
            
            else
            {               
                alert('<%#hidden_placeAtleast.Value%>');              
                return false;
            }
            
            
        }        
        
        
        function searchProfessions()
        {
            strFind=document.getElementById("txtFind").value;
            strFindLength=strFind.length;
            //alert("strFind " + strFind);      
            
            document.getElementById("<%#selectCities.ClientID%>").options.length=0;            
            
            for(i=0;i<arrCities.length;i++)
            {
                //alert(arr[i][0] + " " + arr[i][1]);
                if(arrCities[i].toLowerCase().indexOf(strFind.toLowerCase())!=-1)
                {
                    document.getElementById("<%#selectCities.ClientID%>").options[document.getElementById("<%#selectCities.ClientID%>").options.length]= new Option(arrCities[i],arrGuidCities[i]);
                }
            } 		 
    		              
        }
        
        function setStep3()
        {                          
           parent.window.step4();       
         
        } 
        
        function witchAlreadyStep(level)
        {  
            //alert("witchAlreadyStep");   
            parent.window.witchAlreadyStep(level);
            parent.window.setLinkStepActive('linkStep3');            
        }
        
        var ifUpdate=false;
        
        function updateSendButton(title,text)
        {       
            //alert(text);     
            document.getElementById("btnSend").value=text;
            document.getElementById("btnSend").title=title;
            
            if(title=="UPDATE")
            {
                //alert("UPDATE");
                document.getElementById("pSteps").style.display='none';
                ifUpdate=true;
            }
            
        }
        
       function init()
       {
        parent.goUp();
        
         arrCities=new Array();
         arrGuidCities=new Array(); 
         
         for(i=0;i<document.getElementById("<%#selectCities.ClientID%>").options.length;i++)
         {
             //alert(document.getElementById("<%#selectCities.ClientID%>").options[i].text);
             arrCities[i]=document.getElementById("<%#selectCities.ClientID%>").options[i].text;
             arrGuidCities[i]=document.getElementById("<%#selectCities.ClientID%>").options[i].value;
            
         }
       }
    
       window.onload=init;
    </script> 
</head>


<body class="iframe-inner step3" style="background-color:transparent; width:480px;">
<form id="form2" runat="server">
<div id="form-analyticsM">
   <div class="form-field">  
   <asp:Label ID="lblRadius" runat="server" CssClass="label" Text="Vicinity"></asp:Label>

    <select id="radius" class="form-select" onchange="play();getCities();" runat="server"> 
        <option value="5">5</option>  
        <option value="10">10</option>
        <option value="50">50</option>                                                                 
        <option value="100">100</option>
        <option value="250">250</option>
        <option value="500">500</option>  
        <option value="1000">1000</option>                            
    </select>
                                
    <asp:Label ID="lblKM" runat="server" Text="km"></asp:Label>                         
  </div>
  <div class="form-field">
    <asp:Label ID="lblAddress" runat="server" CssClass="label" Text="Address"></asp:Label>:   
    <input type="text" size="32" id="search" class="form-text" readonly="readonly" onfocus="this.select()" title="Type place name or address"/>
    </div>
    </div>
     <div class="clear"></div>
    <div id="map" class="clearfix"></div>
    
    <div class="clear"></div>
        <!--
        <p>
	        <label for="form-sigments" runat="server" id="lblMySegments">Search for</label>
	        <input id="txtFind" class="form-text" onkeyup="searchProfessions();" type="text" />
        </p>
    	-->
    		
	    <div class="lists-selectmap">
            <p class="list-select-all all">
	            <label for="form-all" runat="server" id="lblAllList">Areas excluded from my working area</label>
	            <select id="selectNotCities" name="selectNotCities" multiple="true" class="form-selectmap" runat="server" ondblclick="javascript:addProfession();"></select>			
	            <a href="javascript:void(0);" class="btn" runat="server" id="btnAdd" onclick="javascript:return addRegion();">Add</a>
            </p>

            <p class="list-select-all choice">
	            <label for="form-choice" runat="server" id="lblYourChoice">Your choice</label>
	            <select id="selectCities"  name="selectCities" multiple="true" class="form-selectmap" runat="server" ondblclick="javascript:removeProfession();"></select>
                <input type="hidden" id="hidden_professions" class="form-select" runat="server"  />			
	            <a href="javascript:void(0);" runat="server" id="a_Remove" class="btn" onclick="javascript:removeRegion();">Remove</a>
            </p>
        </div>
    		
        <input type="button"  class="form-submit" id="btnSend" value="NEXT"  onclick="updateCities();"/>
            
	    <p class="steps" id="pSteps">
            <asp:Label ID="lbl_Step3" runat="server" Text="STEP 3 OUT"></asp:Label>
            <asp:Label ID="lbl_page" runat="server" ></asp:Label>
            <img src="<%=ResolveUrl("~")%>Management/images/bullet.gif" alt="bullet" />
        </p>
    </div>
    
<asp:HiddenField ID="Hidden_Update" runat="server" Value="Update" />
<asp:HiddenField ID="Hidden_Next" runat="server" Value="Next" />
<asp:HiddenField ID="Hidden_UpdateSuccess" runat="server" Value="The information was stored successfully" />
<asp:HiddenField ID="Hidden_UpdateFailed" runat="server" Value="Error occurred" />
<asp:Label ID="lbl_allArea" runat="server" Text="All" Visible="false"></asp:Label>
<asp:HiddenField ID="hidden_placeAtleast" runat="server"  Value="Choose at least 1 place"/>

 <script type="text/javascript">

 var arrCities;
 var ifRecievedCities=false;
 var getCitiesByCoordinateXml ="";
 
 function updateCities2()
 { 
   
    if (window.DOMParser)
    {
      parser=new DOMParser();
      xmlDoc=parser.parseFromString(getCitiesByCoordinateXml,"text/xml");
    }
    
    else // Internet Explorer
    {
      xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
      xmlDoc.async="false";
      xmlDoc.loadXML(getCitiesByCoordinateXml); 
    }     
    
    
    /* 
     * <Areas RegistrationStage="3" Longitude="117.48" Latitude="30.54" Radius="100">
       <Area RegionId="00657fd7-5a87-df11-9c13-0003ff727321" Name="宁化县" />
     * <Area RegionId="c75f6d0b-8587-df11-9c13-0003ff727321" Name="余江县" />
     * <Area RegionId="89c69f9f-6a87-df11-9c13-0003ff727321" Name="宜阳县" />
     * <Area RegionId="0369504e-5a87-df11-9c13-0003ff727321" Name="光泽县" />
     * <Area RegionId="f9f25046-5b87-df11-9c13-0003ff727321" Name="武夷山市" />
     * <Area RegionId="9870aab2-8287-df11-9c13-0003ff727321" Name="东乡县" />
     * <Area RegionId="b016e623-8587-df11-9c13-0003ff727321" Name="资溪县" />
     * </Areas>
     * 
    */    
    
    
    xmlRoot=xmlDoc.documentElement;
    
    var Latitude=xmlRoot.attributes.getNamedItem("Latitude").value;
    
    var Longitude=xmlRoot.attributes.getNamedItem("Longitude").value;
    
    var Radius=xmlRoot.attributes.getNamedItem("Radius").value;
    
    var RegistrationStage=xmlRoot.attributes.getNamedItem("RegistrationStage").value;
    
    var AreasChilds=xmlDoc.documentElement.childNodes;
    var strAreasChilds="";
        
    for (i=0;i<AreasChilds.length;i++)
    {         
       //strAreasChilds+=AreasChilds[i].attributes.getNamedItem("RegionId").value + "*" +
       //AreasChilds[i].attributes.getNamedItem("Name").value + ",";
       
       strAreasChilds+=AreasChilds[i].attributes.getNamedItem("RegionId").value + ",";     
    }
    
    strAreasChilds=strAreasChilds.substring(0,strAreasChilds.length-1);
    
    
    
    var url="";
   
    url="setCitiesByCoordinate.aspx?Latitude=" + Latitude + "&Longitude=" + 
        Longitude + "&Radius=" + Radius + "&RegistrationStage=" + RegistrationStage 
        + "&strAreasChilds=" + strAreasChilds;      
    //alert(url);
    
    
    var objHttp;
    var ua = navigator.userAgent.toLowerCase(); 
    
    if(ua.indexOf("msie")!= -1)
    {
      
        objHttp =  new ActiveXObject("Msxml2.XMLHTTP");                
    }
    else
    {
        //alert("not msie");
        objHttp=new XMLHttpRequest();
    }
  
   
	
    if (objHttp == null)
    {
        //alert("Unable to create DOM document!");
        return;
    } 	        
    
    
    objHttp.open("POST", url, true);	       
    
    
    objHttp.onreadystatechange=function()
    {	  
        //alert("objHttp.readyState "+objHttp.readyState);
        //alert(objHttp.responseText);
                   
        if (objHttp.readyState==4)
        {          
            setStep3();    
        }
    }     
    
    
    objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //objHttp.setRequestHeader("Content-Length", params.length);
    //objHttp.setRequestHeader("Connection", "close");
    //objHttp.send(soap);
    objHttp.send(null);	
    
    
    
 }
 
 function setCitiesByCoordinate(Latitude,Longitude,Radius)
 {  
    //alert("setCitiesByCoordinate");
    //document.getElementById("divLoader").style.display='';
    if(document.getElementById("<%=selectCities.ClientID%>").options.length==0)
    {
        alert('<%=hidden_placeAtleast.Value%>');              
        return false;
    }
    
    parent.showDiv();
    
    //document.getElementById("divLoader").style.width=window.screen.availWidth;
    //document.getElementById("divLoader").style.height=window.screen.availHeight;
    
    var url="";
   
    url="setCitiesByCoordinate.aspx";      
    //alert(url);
    
    params="Latitude=" + Latitude + "&Longitude=" + 
        Longitude + "&Radius=" + Radius;
        
        
    var strCitiesSelected="";
    
    if(document.getElementById("<%=selectCities.ClientID%>").options.length>0)
    {
        for(i=0;i<document.getElementById("<%=selectCities.ClientID%>").options.length;i++)
        {
            strCitiesSelected+=document.getElementById("<%=selectCities.ClientID%>").options[i].value + ",";        
        }
        
        strCitiesSelected=strCitiesSelected.substring(0,strCitiesSelected.length-1);       
        
    }
    
    //alert("str:" + strCitiesSelected);
    
    var strCitiesNotSelected="";
    
    if(document.getElementById("<%=selectNotCities.ClientID%>").options.length>0)
    {
        for(i=0;i<document.getElementById("<%=selectNotCities.ClientID%>").options.length;i++)
        {
            strCitiesNotSelected+=document.getElementById("<%=selectNotCities.ClientID%>").options[i].value + ",";        
        }
        
        strCitiesNotSelected=strCitiesNotSelected.substring(0,strCitiesNotSelected.length-1);        
        
    }   
    
    //alert(strCitiesNotSelected);
    
    params+="&strCitiesSelected=" + strCitiesSelected + "&strCitiesNotSelected=" + strCitiesNotSelected;

    //alert("params:" + params);   
    
    var objHttp;
    var ua = navigator.userAgent.toLowerCase(); 
    
    if(ua.indexOf("msie")!= -1)
    {
      
        objHttp =  new ActiveXObject("Msxml2.XMLHTTP");                
    }
    else
    {
        //alert("not msie");
        objHttp=new XMLHttpRequest();
    }
  
   
	
    if (objHttp == null)
    {
        //alert("Unable to create DOM document!");
        return;
    } 	        
    
    if(ifUpdate)
        objHttp.open("POST", url, true);	//true for asynchronous        
    else
        objHttp.open("POST", url, true); //false for synchronous
    
    
    objHttp.onreadystatechange=function()
    {	  
        //alert("objHttp.readyState "+objHttp.readyState);
        if(!ifUpdate)
        {
            if(objHttp.readyState==1)
            {
                //alert("Please Wait...");
            }
        }
         
                 
        if (objHttp.readyState==4)
        {   
            //alert(objHttp.responseText);  
            //document.getElementById("divLoader").style.display='none';
            
            //alert(objHttp.responseText); 
             
            var xmlDoc=objHttp.responseXML.documentElement;
            
            var status=xmlDoc.childNodes[0].childNodes[0].nodeValue;
            //var status=xmlDoc.childNodes[0].text; // work in ie not in firefox         
            
            if(ifUpdate)
            {                
                if(status.toLowerCase()=="success")
                {
                   alert('<%=Hidden_UpdateSuccess.Value%>');
                } 
                
                else
                    alert('<%=Hidden_UpdateFailed.Value%>  ' + status);    
                 
            } 
            
            else
            {
                if(status.toLowerCase()=="success")
                {
                    alert('<%=Hidden_UpdateSuccess.Value%>');
                    setStep3();
                }
                else
                    alert('<%=Hidden_UpdateFailed.Value%>  ' + status);                
                 
            }
            
            parent.hideDiv();
            
        }
        
    }     
    
    
    objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    objHttp.setRequestHeader("Content-Length", params.length);
    objHttp.setRequestHeader("Connection", "close");
    //objHttp.send(soap);
    objHttp.send(params);	
    
    
 }
 
 function updateCities()
 { 
   
    //if(ifRecievedCities)
    //{   
        //alert("here");        
        //updateCities2();
        setCitiesByCoordinate(map.getCenter().lat(),map.getCenter().lng(),document.getElementById("radius")[document.getElementById("radius").selectedIndex].value);

        
    //}
    
    //else
        //setStep3(); 
        
 }
 
 function processXml(strXml)
 {
    
    arrCities=new Array();
    
    if (window.DOMParser)
    {
      parser=new DOMParser();
      xmlDoc=parser.parseFromString(strXml,"text/xml");
    }
    
    else // Internet Explorer
    {
      xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
      xmlDoc.async="false";
      xmlDoc.loadXML(strXml); 
    } 
    
    x=xmlDoc.documentElement.childNodes;
    var country;
    
    for (i=0;i<x.length;i++)
    { 
     if (x[i].nodeType==1)
      {        
        
        for(y=0;y<x[i].childNodes.length;y++)
        {
            if(x[i].childNodes[y].nodeName=="geoplugin_place")
            {                
                arrCities[arrCities.length]=x[i].childNodes[y].text;
            }
            
            if(x[i].childNodes[y].nodeName=="geoplugin_countryCode")
            {                
                country=x[i].childNodes[y].text;
            }
            
        }        
          
      } 
    }
    
    document.getElementById("<%=selectCities.ClientID%>").options.length=0; 
    
    for(i=0;i<arrCities.length;i++)
    {               		              
	    document.getElementById("<%=selectCities.ClientID%>").options[document.getElementById("<%=selectCities.ClientID%>").options.length]= new Option(arrCities[i],i);		                	                    	           
    }
    
    
    //sendToWebServiceNoProblem(map.getCenter().lat(),map.getCenter().lng(),document.getElementById("radius")[document.getElementById("radius").selectedIndex].innerHTML,arrCities,country);

 }

function processXmlCities(strXml)
{
    
    arrCities=new Array();
    arrGuidCities=new Array();
    
    //alert("strXml:" + strXml);
    /* 
    <Areas>
    <Area RegionId="32c70c77-8487-df11-9c13-0003ff727321" Name="峡江县" />
    <Area RegionId="3d3cec6a-8387-df11-9c13-0003ff727321" Name="吉水县" />
    <Area RegionId="747a42e0-8487-df11-9c13-0003ff727321" Name="永丰县" />
    <Area RegionId="775e0e82-8287-df11-9c13-0003ff727321" Name="安福县" />
    <Area RegionId="4e285133-8387-df11-9c13-0003ff727321" Name="吉安市" />
    <Area RegionId="ab638239-8387-df11-9c13-0003ff727321" Name="吉安县" />
    <Area RegionId="83d8b705-5887-df11-9c13-0003ff727321" Name="太和县" />
    </Areas>
    */
    
    if (window.DOMParser)
    {
      parser=new DOMParser();
      xmlDoc=parser.parseFromString(strXml,"text/xml");
    }
    
    else // Internet Explorer
    {
      xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
      xmlDoc.async="false";
      xmlDoc.loadXML(strXml); 
    } 
    
    x=xmlDoc.documentElement.childNodes;
    var country;
    //alert(x.length);
    for (i=0;i<x.length;i++)
    {    
        //alert(x[i].attributes.getNamedItem("Name").value);
            
        arrCities[arrCities.length]=x[i].attributes.getNamedItem("Name").value;
        arrGuidCities[arrGuidCities.length]=x[i].attributes.getNamedItem("RegionId").value; 
    }
    
    document.getElementById("<%=selectCities.ClientID%>").options.length=0; 
    
    for(i=0;i<arrCities.length;i++)
    {               		              
	    document.getElementById("<%=selectCities.ClientID%>").options[document.getElementById("<%=selectCities.ClientID%>").options.length]= new Option(arrCities[i],arrGuidCities[i]);		                	                    	           
    }
    
    document.getElementById("<%=selectNotCities.ClientID%>").options.length=0;
    
    parent.hideDiv();
    //sendToWebServiceNoProblem(map.getCenter().lat(),map.getCenter().lng(),document.getElementById("radius")[document.getElementById("radius").selectedIndex].innerHTML,arrCities,country);

 }



 

 function sendToWebServiceNoProblem(lat,lng,radius,arrCities,country)
 {
    
    strCities="";
    for(i=0;i<arrCities.length;i++)
    {
        if(i==arrCities.length)
            strCities+=arrCities[i];
        else
            strCities+=arrCities[i] + ",";
    }
    
    var url;
    
    switch(country)
    {
        case "CN":
           country="China";
           break;
           
        default:
           break;  
    }
    
    url="setCities.aspx?lat=" + lat + "&lng=" + lng + "&radius=" + radius + "&cities=" + strCities + "&country=" + country;
    //alert("url:" + url);

    
    var objHttp;
    var ua = navigator.userAgent.toLowerCase(); 
    
    if(ua.indexOf("msie")!= -1)
    {
      
        objHttp =  new ActiveXObject("Msxml2.XMLHTTP");                
    }
    else
    {
        //alert("not msie");
        objHttp=new XMLHttpRequest();
    }
  
   
	
    if (objHttp == null)
    {
        //alert("Unable to create DOM document!");
        return;
    } 	        
    
    
    objHttp.open("GET", url, true);	       
    
    objHttp.onreadystatechange=function()
    {	  
        //alert("objHttp.readyState "+objHttp.readyState);          
        if (objHttp.readyState==4)
        {
           //alert(objHttp.responseText);               
      
        }
    }     
    
    
    objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //objHttp.setRequestHeader("Content-Length", params.length);
    //objHttp.setRequestHeader("Connection", "close");
    //objHttp.send(soap);
    objHttp.send(null);	
          
 }
 
 
 
 function getCitiesByCoordinate(lat,lng, radius)
  { 
     //alert("radius:" + radius);
    parent.showDiv();
    var oDate = new Date();
	
     //var radius=radius/1.6; // convert radius km to miles 
     //alert("radius:" + radius);
    var url;
    //url="http://www.geoplugin.net/extras/nearby.gp?lat=" + lat + "&long=" + lng + "&limit=10&format=xml&radius=" + radius;
    //url="http://www.geoplugin.net/extras/nearby.gp?lat=" + lat + "&long=" + lng + "&format=xml&limit=100&&radius=" + radius;
    url="http://" + location.hostname + ":" + location.port + "<%=root%>Management/getCitiesByCoordinate.aspx?lat=" + lat + "&lng=" + lng + "&radius=" + radius + "&randomSeed=" + escape(oDate.toLocaleTimeString());     
    //alert("url:" + url);
    
    var objHttp;
    var ua = navigator.userAgent.toLowerCase(); 
    
    if(ua.indexOf("msie")!= -1)
    {
      
        objHttp =  new ActiveXObject("Msxml2.XMLHTTP");                
    }
    else
    {
        //alert("not msie");
        objHttp=new XMLHttpRequest();
    }
  
   
	
    if (objHttp == null)
    {
        //alert("Unable to create DOM document!");
        return;
    } 	        
    
    //alert("before");
    objHttp.open("GET",url, true);	       
    //alert("end");
    objHttp.onreadystatechange=function()
    {	  
        //alert("objHttp.readyState "+objHttp.readyState);          
        if (objHttp.readyState==4)
        {
           //alert("responseText"+objHttp.responseText);           
           ifRecievedCities=true;
           getCitiesByCoordinateXml=objHttp.responseText;
           //processXml(objHttp.responseText);
           processXmlCities(objHttp.responseText);          
      
        }
    }     
    
    
    objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //objHttp.setRequestHeader("Content-Length", params.length);
    //objHttp.setRequestHeader("Connection", "close");
    //objHttp.send(soap);
    objHttp.send(null);	
        
  }
  
  function getCities()
  {
    
    //if(document.getElementById("radius")[document.getElementById("radius").selectedIndex].innerHTML=="ALL CHINA")
        //getCitiesByCoordinate(map.getCenter().lat(),map.getCenter().lng(),document.getElementById("radius")[document.getElementById("radius").selectedIndex].value);

    //else
        getCitiesByCoordinate(map.getCenter().lat(),map.getCenter().lng(),document.getElementById("radius")[document.getElementById("radius").selectedIndex].value);
    
   
  }
  
 

/**
 * map
 */

var thePoint = new google.maps.LatLng(0, 0);
var mapOpts = {
  zoom: 13,   
  center: thePoint,
  mapTypeId: google.maps.MapTypeId.ROADMAP,
  scaleControl: true,
  scrollwheel: false,
  draggable:false
}
var map = new google.maps.Map(document.getElementById("map"), mapOpts);




/**
 * circle
 */

var circle = new google.maps.Circle({radius: 10, center: thePoint}); 
map.fitBounds(circle.getBounds());

var radius;

// selector function
function play()
{  
  //alert("play"); 
  var selector = document.getElementById("radius");  
  
  radius = selector.options[selector.selectedIndex].value*1;
  
  if(radius<0)
  {    
    radius=1000; 
  } 
  
 
  circle.setRadius(radius * 1000);
  //circle.setRadius(1500 * 1000);
  circle.setCenter(map.getCenter());
  map.fitBounds(circle.getBounds());
  map.circleRadius = radius;
  circle.setMap(map);
  
  
  //alert("before");
  //circle.setMap(map);
  //alert("after");
}

/*
google.maps.event.addListener(map, 'dragend', function()
{
  circle.setCenter(map.getCenter());
  //alert(map.getCenter());
  //play();
  getCities();
});
*/
//play();


/**
* geocoder
*/


var geocoder = new google.maps.Geocoder();
//document.getElementById("search").value = "search";
var lat;
var lot;

function geocode(opts)
{   
  function geocodeResult(response, status) {
  
    if (status == google.maps.GeocoderStatus.OK && response[0]) {
       
      document.getElementById("search").value = response[0].formatted_address;
      //map.fitBounds(response[0].geometry.viewport);
      map.setCenter(response[0].geometry.location); 
      
      lat=response[0].geometry.location.lat();
      lot=response[0].geometry.location.lng();
        
        
        
      //sendToWebService(lat,lot,document.getElementById("radius")[document.getElementById("radius").selectedIndex].innerHTML);
       //alert(lat + ' ' + '<%=lat%>');
       
      play();
      
      <%if(source=="address")%>
      <%{%>
      //alert("1");
        getCities();
      <%}%>       
      
    } else {
         //alert("Problem! " + status);
         
         <%if(Session["City"]!=null)%>         
         <%{%>
            geocode({address:'<%=Session["City"]%>'});
         <%}%>
        
     
    }
  } // trim leading and trailing space with capable browsers
  
  if(opts.address && opts.address.trim)opts.address = opts.address.trim(); 
  if(opts.address || opts.latLng)geocoder.geocode(opts, geocodeResult); // no empty request
  
}


google.maps.event.addListener(map, 'rightclick', function(eve)
{
geocode({latLng: eve.latLng});
});


//geocode({address:'20 XUANWU RD, WEIYANG,CHINA'})
<%if(source=="latLot")%>
<%{ %>
 geocode({latLng:new google.maps.LatLng(<%=lat%>,<%=lot%>)});
 //geocode({latLng:new google.maps.LatLng(35,105)}); 
<%} %>
<%else if(source=="address")%>
<%{ %>
 //geocode({address:'20 XUANWU RD, WEIYANG,CHINA'})
 geocode({address:'<%=address%>'})
 
<%} %>
//geocode({address:'beishi, China'})


//geocode({address:'Antelope Rd 20, North highlands'})

//geocode({address:'800 Bridge Parkway Redwood City, CA 94065'})
//geocode({address:'Tel Aviv,Israel'})

/*
Latitude of China: 35÷00´ North of the Equator

Longitude of China: 105÷00
*/

</script>
    </form>
</body>
</html>
