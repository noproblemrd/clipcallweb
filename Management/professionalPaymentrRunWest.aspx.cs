﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Collections.Specialized;

public partial class Management_professionalPaymentrRunWest : PageSetting
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (string.IsNullOrEmpty(GetGuidSetting()))
                Response.Redirect("LogOut.aspx");
            
            
            
            string csnameStep = "setStep";
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), csnameStep))
            {
                string csTextStep = "witchAlreadyStep(6);";
                ClientScript.RegisterStartupScript(this.GetType(), csnameStep, csTextStep, true);
            }

            LoadDetail();
            LoadIncidentPackages();
            RemoveServerCommentPPC();
            Page.Header.DataBind();
        }


    }

    void LoadDetail()
    {
        try
        {

            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);

            WebReferenceSupplier.GetSupplierChargingDataRequest request = new WebReferenceSupplier.GetSupplierChargingDataRequest();
            request.SupplierId = userManagement.Get_Guid;

            WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result = supplier.GetSupplierChargingData(request);

            if (result.Type == WebReferenceSupplier.eResultType.Success)
            {
                WebReferenceSupplier.GetSupplierChargingDataResponse value = result.Value;
                //inside value is all the data...
                CurrenBalance = value.CurrentBalance;
                //Response.Write(CurrenBalance);
                CompanyName = value.CompanyName;

                FirstName = value.FirstName;
                SecondName = value.LastName;
                CompanyId = value.CompanyId;
                Email = value.Email;
                PhoneNumber = value.PhoneNumber;

                Country = value.AddressCountry;
                State = value.AddressState;
                County = value.AddressCounty;
                City = value.AddressCity;
                Street = value.AddressStreet;
                HouseNumber = value.AddressHouseNumber;
                PostalCode = value.AddressPostalCode;
                Currency = value.Currency;
                //Response.Write("Currency:" + Currency);
                OtherPhone = value.OtherPhone;
                WebSite = value.WebSite;
                Fax = value.Fax;
                CreditCardToken = value.CreditCardToken;
                Last4DigitsCC = value.Last4DigitsCC;
                CardId = value.CreditCardOwnerId;
                AccountNumber = value.AccountNumber;
                ArrNumberPayments = value.PaymentsTable;
                /******* CreditCardToken ***********/

                
                /*
                int extra_bonus = value.RechargeBonusPercent;// string.IsNullOrEmpty(extra_bonus) ? "0" : extra_bonus;
                lbl_Title_AutoRecharge.Text = lbl_Title_AutoRecharge1.Text + " " + extra_bonus + "% " + lbl_Title_AutoRecharge2.Text;
                hf_ExtraBonus.Value = extra_bonus + "";
                */

                int Register_Stage = value.StageInRegistration;
                //Response.Write(Register_Stage);
                if (Register_Stage < 5)
                {
                    string script = "parent.setInitRegistrationStep(" + Register_Stage + ");";
                    ClientScript.RegisterStartupScript(this.GetType(), "GoToStep", script, true);
                    IsFirstTimeV = true;
                    //return;                   
                }
                else if (Register_Stage == 5)
                    IsFirstTimeV = true;
                else
                    IsFirstTimeV = false;

                /*
                bool isRegisterSage = (Register_Stage == 5);

                if (value.RechargeEnabled && !string.IsNullOrEmpty(value.CreditCardToken))
                {
                   
                    div_AutoRecharge.Visible = true;
                    if (value.IsAutoRenew)
                    {
                        int _num = (int)value.RechargeAmount;
                        int _extra_bonus = int.Parse(hf_ExtraBonus.Value);
                        int _bonus = GetBonusPackage(_num);

                        _bonus += _extra_bonus;
                        txt_ExtraBonus.Text = (((_num * _bonus) / 100)) + "";
                        lbl_GetExtraTotalPercent.Text = _bonus + "";
                        //      txt_GetExtraRecharge.Text = (((_num * _bonus) / 100) + _num) + "";
                        cb_AutoRecharge.Checked = true;
                        txt_RechargeAmount.Attributes.Remove("readOnly");
                        txt_RechargeAmount.Text = value.RechargeAmount + "";
                        hf_IsRechargeMarked.Value = "true";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "RemoveAddReachargedLabels", "RemoveAddReachargedLabels(true);", true);
                    }
                    else
                    {
                        txt_RechargeAmount.Attributes.Remove("readOnly");
                        cb_AutoRecharge.Checked = IsFirstTimeV;
                        hf_IsRechargeMarked.Value = (IsFirstTimeV) ? "" : "false";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "RemoveAddReachargedLabels", "RemoveAddReachargedLabels(false);", true);
                    }
                }
                else
                {
                    div_AutoRecharge.Visible = false;
                    btn_AutoRecharge.Visible = false;
                }
                

                _PanelStep.Visible = isRegisterSage;
                if (isRegisterSage)
                    ClientScript.RegisterStartupScript(this.GetType(), "focusTxtBox", "focusTxtBox();", true);
                lbl_CurrentBalance.Text = String.Format("{0:#,0.##}", CurrenBalance);
                */
                
            }

            else
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        

    }

    
    protected void buyNow(Object sender, CommandEventArgs e)
    {
        string auto_id = "";
        if(siteSetting.IfPaymentCharging==true)
        {
            if (siteSetting.CharchingCompany == eCompanyCharging.plimus)
            {
                //string args=(string)e.CommandArgument;
                //string[] arrDepositBonus=args.Split(',');

                int deposit=0;
                int bonus = 0;
                decimal rate = 0;

                switch ((string)e.CommandArgument)
                {
                    case "bronze":
                        deposit = packageBronze.FromAmount;
                        rate = (decimal)packageBronze.BonusPercent;
                        bonus = Convert.ToInt32(rate / 100 * deposit);
                        break;

                    case "silver":
                        deposit = packageSilver.FromAmount;
                        rate = (decimal)packageSilver.BonusPercent;
                        bonus = Convert.ToInt32(rate / 100 * deposit);
                        break;

                    case "gold":
                        deposit = packageGold.FromAmount;
                        rate = (decimal)packageGold.BonusPercent;
                        bonus = Convert.ToInt32(rate / 100 * deposit);
                        break;
                }

                int RechargeAmount = -1;

                string command = "EXEC dbo._SetPayment @site_id ,@company, @advertiser, @details, @RechargeAmount";
                using (SqlConnection conn = DBConnection.GetConnString())
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(command, conn);
                    cmd.Parameters.AddWithValue("@site_id", siteSetting.GetSiteID);
                    cmd.Parameters.AddWithValue("@company", "plimus");
                    cmd.Parameters.AddWithValue("@advertiser", CompanyName);
                    if (RechargeAmount == -1)
                        cmd.Parameters.AddWithValue("@RechargeAmount", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@RechargeAmount", RechargeAmount);
                    string paymentPlimusDetails = "";

                    //      int depositAgorot = int.Parse(deposit) * 100;

                    int _new_balance = Convert.ToInt32(CurrenBalance) + deposit;

                    string bonusType;

                    if (bonus <= 0)
                        bonusType = WebReferenceSupplier.BonusType.None.ToString();
                    else
                        bonusType = WebReferenceSupplier.BonusType.Package.ToString();

                    NameValueCollection nvServerVariables = (NameValueCollection)Request.ServerVariables;
                    string userAgent = nvServerVariables["HTTP_USER_AGENT"];
                    string hostIp = nvServerVariables["REMOTE_HOST"]; //Retrieves the remote host IP Address                
                    string clientIp = Utilities.GetIP(Request);// nvServerVariables["REMOTE_ADDR"]; //Retrieves the user IP Address                
                    string acceptLanguage = nvServerVariables["HTTP_ACCEPT_LANGUAGE"];

                    paymentPlimusDetails = "supplierGuid=" + userManagement.GetGuid +
                        "&supplierId=" + CompanyId +
                        "&advertiserName=" + Server.UrlEncode(CompanyName) +
                        "&balanceOld=" + CurrenBalance +
                        "&userId=" + userManagement.GetGuid +
                        "&userName=" + Server.UrlEncode(userManagement.User_Name) +
                        "&siteId=" + siteSetting.GetSiteID +
                        "&siteLangId=" + siteSetting.siteLangId +
                        "&fieldId=" + lbl_YourBalance.ID +
                        "&fieldName=" + Server.UrlEncode(lbl_YourBalance.Text) +
                        "&isFirstTimeV=" + IsFirstTimeV.ToString() +
                        "&bonusAmount=" + bonus +
                        "&balanceNew=" + _new_balance +
                        "&email=" + Server.UrlEncode(Email) +
                        "&phoneNumber=" + PhoneNumber +
                        "&otherPhone=" + OtherPhone +
                        "&country=" + Country +
                        "&city=" + City +
                        "&streetNumber=" + Server.UrlEncode(StreetNumber) +
                        "&postalCode=" + PostalCode +
                        "&webSite=" + Server.UrlEncode(WebSite) +
                        "&vat=" + siteSetting.Vat +
                        "&ifAccountingCharging=" + Convert.ToString(siteSetting.IfAccountingCharging) +
                        "&ppcPackage=" + Server.UrlEncode(Hidden_PpcPackage.Value) +
                        "&currency=" + Currency +
                        "&accountNumber=" + AccountNumber +
                        "&fax=" + Fax + "&amount=" + deposit + "&bonusType=" + bonusType +
                        "&userAgent=" + Server.UrlEncode(userAgent) + "&hostIp=" + hostIp + "&clientIp=" +
                        clientIp + "&acceptLanguage=" + Server.UrlEncode(acceptLanguage);


                    cmd.Parameters.AddWithValue("@details", paymentPlimusDetails);

                    auto_id = Convert.ToString(cmd.ExecuteScalar());
                    conn.Close();
                }
               
                

                string __form = string.Empty;

                __form = (CompanyCharging.GetPlimusForm(this, deposit, auto_id, CompanyName, FirstName, SecondName, Email, StreetNumber, City, State, PostalCode, Country, PhoneNumber, Fax));

                if (__form.Length != 0)
                {
                    Response.Write(__form);
                    //Response.Write(@"<script type=""text/javascript"">window.location='professionalDetails.aspx';</script>");
                    Response.Flush();
                    Response.End();
                }
            

            }
       
        }
    }


    private void LoadIncidentPackages()
    {
        /*
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("PaidAmount");
        data.Columns.Add("FreeAmount");
        data.Columns.Add("image");
        */
        packageBronze = new Package();
        packageSilver = new Package();
        packageGold = new Package();
        //SortedDictionary<int, int> pricePercentList = new SortedDictionary<int, int>();


        try
        {
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            WebReferenceSupplier.GetSupplierChargingDataRequest request = new WebReferenceSupplier.GetSupplierChargingDataRequest();
            request.SupplierId = userManagement.Get_Guid;

            WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result = supplier.GetSupplierChargingData(request);
                        
            if (result.Type == WebReferenceSupplier.eResultType.Success)
            {
                WebReferenceSupplier.PricingPackageData[] packages = result.Value.Packages;
                decimal rate=0;
                int bonus = 0;
                foreach (WebReferenceSupplier.PricingPackageData package in packages)
                {                   
                    WebReferenceSupplier.PricingPackageSupplierType pricingType=package.SupplierType;
                    string PackageName = package.Name.ToLower();
                    if (pricingType== WebReferenceSupplier.PricingPackageSupplierType.All)
                    {
                        switch(PackageName)
                        {
                            case "bronze":
                                
                                packageBronze.Name=PackageName;
                                packageBronze.FromAmount=package.FromAmount;
                                packageBronze.BonusPercent=package.BonusPercent;
                                packageBronze.SupplerType="All";
                                rate = (decimal)packageBronze.BonusPercent;
                                bonus = Convert.ToInt32(rate / 100 * packageBronze.FromAmount);
                                packageBronze.Total = bonus + packageBronze.FromAmount;
                                totalBronze.InnerText = "$" + packageBronze.Total.ToString();
                                if (packageBronze.BonusPercent > 0)
                                {
                                    PayOnlyBronze.InnerText = "Pay only $" + packageBronze.FromAmount;
                                    freeBronze.InnerText = "$" + bonus + " FREE";
                                    crossBronze.Visible = true;
                                }
                                break;

                            case "silver":
                                
                                packageSilver.Name=PackageName;
                                packageSilver.FromAmount = package.FromAmount;
                                packageSilver.BonusPercent = package.BonusPercent;
                                packageSilver.SupplerType="All";
                                rate = (decimal)packageSilver.BonusPercent;
                                bonus = Convert.ToInt32(rate / 100 * packageSilver.FromAmount);
                                packageSilver.Total = bonus + packageSilver.FromAmount;
                                totalSilver.InnerText = "$" + packageSilver.Total.ToString();
                                if (packageSilver.BonusPercent > 0)
                                {
                                    PayOnlySilver.InnerText = "Pay only $" + packageSilver.FromAmount;
                                    freeSilver.InnerText = "$" + bonus + " FREE";
                                    crossSilver.Visible = true;
                                }
                                break;

                            case "gold":                          
                                
                                packageGold.Name=PackageName;
                                packageGold.FromAmount = package.FromAmount;
                                packageGold.BonusPercent = package.BonusPercent;
                                packageGold.SupplerType="All";
                                rate = (decimal)packageGold.BonusPercent;
                                bonus = Convert.ToInt32(rate / 100 * packageGold.FromAmount);
                                packageGold.Total = bonus + packageGold.FromAmount;
                                totalGold.InnerText = "$" + packageGold.Total.ToString();
                                if (packageGold.BonusPercent > 0)
                                {
                                    PayOnlyGold.InnerText = "Pay only $" + packageGold.FromAmount;
                                    freeGold.InnerText = "$" + bonus + " FREE";
                                    crossGold.Visible = true;
                                }
                                break;                                 
    
                        }
                    }

                }
                /*
                int indx = 0;


                if (list.Count == 1 && list[0] == ePaymentMethodSIte.WireTransfer.ToString() && packages.Length == 0)
                {
                    div_calc.Visible = false;
                    btn_buyNow.Visible = false;
                    //       return;
                }
                else
                {


                    foreach (WebReferenceSupplier.PricingPackageData package in packages)
                    {
                        int PaidAmount = package.FromAmount;
                        int FreeAmount = package.BonusPercent;
                        DataRow row = data.NewRow();
                        row["Name"] = package.Name;
                        row["PaidAmount"] = PaidAmount;
                        row["FreeAmount"] = FreeAmount;
                        switch (indx % 3)
                        {
                            case (0): row["image"] = "images/package-p.png";
                                break;
                            case (1): row["image"] = "images/package-g.png";
                                break;
                            case (2): row["image"] = "images/package-s.png";
                                break;
                        }
                        indx++;
                        data.Rows.Add(row);
                        if (!pricePercentList.ContainsKey(PaidAmount))
                            pricePercentList.Add(PaidAmount, FreeAmount);
                    }
                    div_Packages_Pricing.Visible = true;
                    div_Payment_Image.Visible = false;
                    if (data.Rows.Count > 0)
                        lbl_SpecialOffers.Visible = true;
                    else
                    {
                        string payment_image = DBConnection.GetPaymentImage(siteSetting.GetSiteID);
                        if (!string.IsNullOrEmpty(payment_image))
                        {
                            div_Packages_Pricing.Visible = false;
                            div_Payment_Image.Visible = true;
                            img_Payment.ImageUrl = payment_image;
                        }
                        tr_AddDeposite.Style.Add(HtmlTextWriterStyle.Display, "none");
                    }


                    DataListPackege.DataSource = data;
                    DataListPackege.DataBind();

                    string PercentToJavaScript;

                    pricesV = PriceToJavaScript(pricePercentList, out PercentToJavaScript);
                    percentsV = PercentToJavaScript;
                }
                //Load Invoice Options
                div_InvoiceAddress.Visible = result.Value.InvoiceSendOptionsEnabled;
                if (result.Value.InvoiceSendOptionsEnabled)
                {
                    hf_InvoiceOptions.Value = result.Value.InvoiceSendOption.ToString();
                    txt_MailAddress.Text = string.IsNullOrEmpty(result.Value.InvoiceSendAddress) ?
                        result.Value.Email : result.Value.InvoiceSendAddress;
                    hf_InvoiceValue.Value = result.Value.InvoiceSendOption.ToString() + ";;;" + txt_MailAddress.Text;
                    if (result.Value.InvoiceSendOption == WebReferenceSupplier.InvoiceSendOption.Email)
                    {
                        rbl_InvoiceOption.SelectedIndex = rbl_InvoiceOption.Items.IndexOf(rbl_InvoiceOption.Items.FindByValue(WebReferenceSupplier.InvoiceSendOption.Email.ToString()));
                    }
                    else
                    {
                        rbl_InvoiceOption.SelectedIndex = rbl_InvoiceOption.Items.IndexOf(rbl_InvoiceOption.Items.FindByValue(WebReferenceSupplier.InvoiceSendOption.Mail.ToString()));
                    }
                }
                */ 
                
            }
            
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

    }

    protected string CompanyName
    {
        get { return (ViewState["CompanyName"] == null) ? "" : (string)ViewState["CompanyName"]; }
        set { ViewState["CompanyName"] = value; }
    }

    protected string FirstName
    {
        get { return (ViewState["FirstName"] == null) ? "" : (string)ViewState["FirstName"]; }
        set { ViewState["FirstName"] = value; }
    }

    protected string SecondName
    {
        get { return (ViewState["SecondName"] == null) ? "" : (string)ViewState["SecondName"]; }
        set { ViewState["SecondName"] = value; }
    }

    protected string CompanyId
    {
        get { return (ViewState["CompanyId"] == null) ? "" : (string)ViewState["CompanyId"]; }
        set { ViewState["CompanyId"] = value; }
    }

    protected string Email
    {
        get { return (ViewState["Email"] == null) ? "" : (string)ViewState["Email"]; }
        set { ViewState["Email"] = value; }
    }

    protected string PhoneNumber
    {
        get { return (ViewState["PhoneNumber"] == null) ? "" : (string)ViewState["PhoneNumber"]; }
        set { ViewState["PhoneNumber"] = value; }
    }

    protected string Country
    {
        get { return (ViewState["Country"] == null) ? "" : (string)ViewState["Country"]; }
        set { ViewState["Country"] = value; }
    }

    protected string State
    {
        get { return (ViewState["State"] == null) ? "" : (string)ViewState["State"]; }
        set { ViewState["State"] = value; }
    }

    protected string County
    {
        get { return (ViewState["County"] == null) ? "" : (string)ViewState["County"]; }
        set { ViewState["County"] = value; }
    }

    protected string City
    {
        get { return (ViewState["City"] == null) ? "" : (string)ViewState["City"]; }
        set { ViewState["City"] = value; }
    }

    protected string Street
    {
        get { return (ViewState["Street"] == null) ? "" : (string)ViewState["Street"]; }
        set { ViewState["Street"] = value; }
    }

    protected string HouseNumber
    {
        get { return (ViewState["HouseNumber"] == null) ? "" : (string)ViewState["HouseNumber"]; }
        set { ViewState["HouseNumber"] = value; }
    }

    protected string StreetNumber
    {
        get { return (ViewState["HouseNumber"] == null) ? "" : Street + " " + HouseNumber; }

    }

    protected string PostalCode
    {
        get { return (ViewState["PostalCode"] == null) ? "" : (string)ViewState["PostalCode"]; }
        set { ViewState["PostalCode"] = value; }
    }

    protected decimal CurrenBalance
    {
        get { return (ViewState["CurrenBalance"] == null) ? 0 : (decimal)ViewState["CurrenBalance"]; }
        set { ViewState["CurrenBalance"] = value; }

    }

    protected string Currency
    {
        get { return (ViewState["Currency"] == null) ? "" : (string)ViewState["Currency"]; }
        set { ViewState["Currency"] = value; }
    }

    protected string CompanyFullAddress
    {
        get
        {
            return Street + " " + HouseNumber + (County == null ? " " : " " + County) + (State == null ? " " : " " + State) + (Country == null ? " " : " " + Country) + (PostalCode == null ? " " : " " + PostalCode);
        }

    }

    protected string OtherPhone
    {
        get { return (ViewState["OtherPhone"] == null) ? "" : (string)ViewState["OtherPhone"]; }
        set { ViewState["OtherPhone"] = value; }
    }

    protected string WebSite
    {
        get { return (ViewState["WebSite"] == null) ? "" : (string)ViewState["WebSite"]; }
        set { ViewState["WebSite"] = value; }
    }

    protected string Fax
    {
        get { return (ViewState["Fax"] == null) ? "" : (string)ViewState["Fax"]; }
        set { ViewState["Fax"] = value; }
    }

    protected string CreditCardToken
    {
        get { return (ViewState["CreditCardToken"] == null) ? "" : (string)ViewState["CreditCardToken"]; }
        set { ViewState["CreditCardToken"] = value; }
    }

    protected string Last4DigitsCC
    {
        get { return (ViewState["Last4DigitsCC"] == null) ? "" : (string)ViewState["Last4DigitsCC"]; }
        set { ViewState["Last4DigitsCC"] = value; }
    }

   
    protected bool IsFirstTimeV
    {
        get { return (ViewState["IsFirstTime"] == null) ? false : (bool)ViewState["IsFirstTime"]; }
        set { ViewState["IsFirstTime"] = value; }
    }

    protected string CardId
    {
        get { return (ViewState["CardId"] == null) ? "" : (string)ViewState["CardId"]; }
        set { ViewState["CardId"] = value; }
    }
   

    protected string AccountNumber
    {
        get { return (ViewState["AccountNumber"] == null) ? "" : (string)ViewState["AccountNumber"]; }
        set { ViewState["AccountNumber"] = value; }

    }

    protected WebReferenceSupplier.PaymentsKeyValuePair[] ArrNumberPayments
    {
        get { return (WebReferenceSupplier.PaymentsKeyValuePair[])ViewState["ArrNumberPayments"]; }
        set { ViewState["ArrNumberPayments"] = value; }
    }


    protected Package packageBronze
    {
        get { return (ViewState["packageBronze"] == null) ? null : (Package)ViewState["packageBronze"]; }
        set { ViewState["packageBronze"] = value; }
    }

    protected Package packageSilver
    {
        get { return (ViewState["packageSilver"] == null) ? null : (Package)ViewState["packageSilver"]; }
        set { ViewState["packageSilver"] = value; }
    }

    protected Package packageGold
    {
        get { return (ViewState["packageGold"] == null) ? null : (Package)ViewState["packageGold"]; }
        set { ViewState["packageGold"] = value; }
    }

    protected decimal Total
    {
        get { return (ViewState["Total"] == null) ? 0 : (decimal)ViewState["Total"]; }
        set { ViewState["Total"] = value; }
    }


    protected string ClientIp
    {
        get { return (ViewState["ClientIp"] == null) ? "" : (string)ViewState["ClientIp"]; }
        set { ViewState["ClientIp"] = value; }

    }

    protected string ClientBrowser
    {
        get { return (ViewState["ClientBrowser"] == null) ? "" : (string)ViewState["ClientBrowser"]; }
        set { ViewState["ClientBrowser"] = value; }

    }

    protected string AcceptLanguage
    {
        get { return (ViewState["AcceptLanguage"] == null) ? "" : (string)ViewState["AcceptLanguage"]; }
        set { ViewState["AcceptLanguage"] = value; }

    }

    protected string ServerIp
    {
        get { return (ViewState["ServerIp"] == null) ? "" : (string)ViewState["ServerIp"]; }
        set { ViewState["ServerIp"] = value; }

    }

    
}