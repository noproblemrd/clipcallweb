using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.Data.SqlClient;
using System.Web.Services.Protocols;
using System.Text;

public partial class Professional_professionalDetails : PageSetting
{
    const string MY_CLICK = "MyClick";
    const string TIME_ZONE_DEFAULT = "PublisherTimeZone";
    const string DatePickerFormat = @"{0:MM/dd/yyyy}";
    public string PostBackStr;
    string page_translate;
    Dictionary<WebReferenceSupplier.DapazStatus, string> ListDapazStatus;
    protected void Page_Load(object sender, EventArgs e)
    {        
        btnSend.Attributes.Add("onmousedown", "javascript:TurnOffErrorCity();");
        PostBackStr = Page.ClientScript.GetPostBackEventReference(btnSend, MY_CLICK);
        LoadDapazStatusSentences();
        if (!IsPostBack)
        {
            
            lbl_page.Text = " 6";
            if (!(userManagement.IsPublisher() || userManagement.IsSupplier()))
            {

                Response.Redirect(ResolveUrl("~") + @"Management/LogOut.aspx", false);               
                return;
            }
            
            if (!string.IsNullOrEmpty(siteSetting.GetSiteID))
            {                
                GetRegularExprenssions();               
                SetRecordPannel();
            }
            Panel_Ranking.Visible = userManagement.IsPublisher();
            LoadTimeZone();
            LoadFundsStatuses();
            LoadUsers();
            LoadBusinessTypes();
            if (!string.IsNullOrEmpty(GetGuidSetting()))
                getDetails(GetGuidSetting());
            else
            {
                _PanelMoveToPPC.Visible = false;
                LoadFirstTimeDetails();                
            }
            SetPanels();
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
            RemoveServerCommentPPC();
    
        }
        else
        {
            string event_argument = Request["__EVENTARGUMENT"];
            if (event_argument == MY_CLICK)
                btnSend_MyClick();
        }
        Page.Header.DataBind();

    }

    private void LoadBusinessTypes()
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfArrayOfStringIntPair result = null;
        try
        {
            result = supplier.GetBusinessTypes();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if(result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_BusinessType.Items.Clear();
        ddl_BusinessType.Items.Add(new ListItem("", "-1"));
        foreach(WebReferenceSupplier.StringIntPair sip in result.Value)
        {
            ListItem li = new ListItem(sip.Key, sip.Value.ToString());
            ddl_BusinessType.Items.Add(li);
        }
    }
    private void LoadFundsStatuses()
    {
        ddl_FundsStatus.Items.Clear();
        foreach (WebReferenceSite.FundsStatus fs in Enum.GetValues(typeof(WebReferenceSite.FundsStatus)))
        {
            ddl_FundsStatus.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "FundsStatus", fs.ToString()), fs.ToString()));
        }
    }
    private void LoadDapazStatusSentences()
    {
        ListDapazStatus = new Dictionary<WebReferenceSupplier.DapazStatus, string>();
        foreach (WebReferenceSupplier.DapazStatus ds in Enum.GetValues(typeof(WebReferenceSupplier.DapazStatus)))
            ListDapazStatus.Add(ds, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "DapazStatus", ds.ToString()));
    }
    private void LoadUsers()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllPublishersResponse result = new WebReferenceSite.ResultOfGetAllPublishersResponse();
        try
        {
            result = _site.GetAllPublishers();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        
        ddl_AccountManager.Items.Clear();
        ddl_AccountManager.Items.Add(new ListItem("",
            (Guid.Empty.ToString())));
        foreach (WebReferenceSite.PublisherData pd in result.Value.Publishers)
        {
            ddl_AccountManager.Items.Add(new ListItem(pd.Name, pd.AccountId.ToString()));
        }
        ddl_AccountManager.SelectedIndex = 0;
    }
    private void SetRecordPannel()
    {
        _PanelRecord.Visible = DBConnection.IfSiteRecord(siteSetting.GetUrlWebReference);
    }

    private void LoadFirstTimeDetails()
    {
        string csnameStep = "setStep";
        Type cstypeStep = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager csStep = this.Page.ClientScript;
        string csTextStep = "";

        if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
        {
            csTextStep = "witchAlreadyStep(0,'click');";
            csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
        }
        btnSend.Text = lbl_NEXT.Text;
        pSteps.Visible = true;
        
 //       _PanelAccountStatus.Visible = false;
        setDefaultCountry();
        setDefaultTimeZone();
        osekId.Focus();
    }

    private void SetPanels()
    {
        PanelDisplay pd = new PanelDisplay(this);
        List<string> list = pd.StartTo();
        string ToLevel = MakeStringLevel(pd.GetList);
        dicV = pd.GetList;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetLevels", "Set_level_txtId('" + ToLevel + "');", true);
        int i = 0;
        foreach (string s in list)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDisableTxtbox" + i++, "SetDisableTxtbox('" + s + "');", true);            
        }
        if (!userManagement.IsPublisher())
        {
            _PanelAccountManager.Visible = false;
            _PanelFundsStatus.Visible = false;
            _PanelStatus.Visible = false;
            _PanelBizId.Visible = false;
        }
        _PanelPassword.Visible = userManagement.IsPublisher();
    }
    private string MakeStringLevel(Dictionary<string, PanelValidate> dic)
    {
        StringBuilder sb = new StringBuilder();
        foreach (KeyValuePair<string, PanelValidate> kvp in dic)
        {
            if (kvp.Value.AutoCompleteLevel > 0)
            {
                sb.Append(kvp.Value.txtBoxId + ";" + kvp.Value.AutoCompleteLevel + ";" + kvp.Value.txtBoxParentId + 
                    ";" + kvp.Value.RequiredFieldValidatorId + ",");
            }
        }
        if (sb.Length == 0)
            return string.Empty;
        return sb.ToString().Substring(0, sb.Length - 1);
    }
    private void setDefaultTimeZone()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = _site.GetConfigurationSettings();
        }
        catch (Exception ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["Configurations"] == null || xdd["Configurations"]["Error"] != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_TimeZone.SelectedIndex = -1;
        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == TIME_ZONE_DEFAULT)
            {
                string _value = node["Value"].InnerText;
                (ddl_TimeZone.Items.FindByValue(_value)).Selected = true;
                break;
            }
        }
    }

    private void GetRegularExprenssions()
    {       
        CustomValidatorPhone.ValidationExpression = siteSetting.GetPhoneRegularExpression;
        RegularExpressionValidatorPhone.ValidationExpression = siteSetting.GetPhoneRegularExpression;
        RegularExpressionValidatorFax.ValidationExpression = siteSetting.GetPhoneRegularExpression;
        RegularExpressionValidatorZipCode.ValidationExpression = siteSetting.GetZipCodeRegularExpression;
    }
   
    private void LoadTimeZone()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = _site.GetAllTimeZones();
        }
        catch (Exception ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["TimeZones"] == null || xdd["TimeZones"]["Error"] != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (XmlNode node in xdd["TimeZones"].ChildNodes)
        {
            ddl_TimeZone.Items.Add(new ListItem(node.Attributes["TimeZoneName"].InnerText,
                node.Attributes["TimeZoneCode"].InnerText));
        }
    }
    

    protected void getDetails(string ProfessionalGuid)
    {        
        
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfAdvertiserGeneralInfo result = null;
        try
        {
            result = supplier.GetSupplierGeneralInfo(new Guid(ProfessionalGuid));            
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {            
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Dictionary<string, string> dic = new Dictionary<string, string>();
        
        //Session["streetChange"] = "false";
        /*
        XmlNode node = doc.DocumentElement["Details"];
        if (node == null)
        {
   //         dbug_log.ExceptionLog(new Exception("step3"), result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Faild_logout", "top.UpdateFailed();window.location='LogOut.aspx';", true);
            return;
        }
         * */
        WebReferenceSupplier.AdvertiserGeneralInfo advertiser = result.Value;
        _PanelMoveToPPC.Visible = false; //userManagement.IsPublisher();
        txt_Status.Text = ListDapazStatus[advertiser.DapazStatus];

        dic.Add(txt_Status.ID, ListDapazStatus[advertiser.DapazStatus]);
        ddl_MoveTo.Items.Clear();
        ddl_MoveTo.Items.Add(new ListItem("", ""));
        foreach (string str in DpzUtilities.GetStatusOptionsChange(advertiser.DapazStatus))
        {
            WebReferenceSupplier.DapazStatus _status;
            if(!Enum.TryParse(str, out _status))
                continue;
            ddl_MoveTo.Items.Add(new ListItem(ListDapazStatus[_status], str));
        }
        ddl_MoveTo.SelectedIndex = 0;

        //string FundsStatus = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "FundsStatus", advertiser.FundsStatus.ToString());
        foreach (ListItem li in ddl_FundsStatus.Items)
        {
            li.Selected = li.Value == advertiser.FundsStatus.ToString();
        }
        dic.Add(ddl_FundsStatus.ID, ddl_FundsStatus.SelectedItem.Text);


        string _first_Name = advertiser.FirstName;
        firstName.Text = _first_Name;
        dic.Add(firstName.ID, _first_Name);
        
        string _osekId = advertiser.CompanyId;
        osekId.Text = _osekId;
        dic.Add(osekId.ID, _osekId);

        string _second_name = advertiser.LastName;
        secondName.Text = _second_name;
        dic.Add(secondName.ID, _second_name);


        string _company_name = advertiser.Name;
        companyName.Text = _company_name;
        dic.Add(companyName.ID, _company_name);

        string _phone = advertiser.CompanyPhone;
        phone.Text = _phone;
        dic.Add(phone.ID, _phone);

        string _fax = advertiser.Fax;
        fax.Text = _fax;
        dic.Add(fax.ID, _fax);

        string _SmsNumber = advertiser.SmsNumber;
        txt_SmsNumber.Text = _SmsNumber;
        dic.Add(txt_SmsNumber.ID, _SmsNumber);

        string _email = advertiser.Email;
        email.Text = _email;
        dic.Add(email.ID, _email);

        string UserManager = advertiser.ManagerId.ToString();
        dic.Add(lbl_AccountManager.ID, UserManager);

        string referralCode = advertiser.ReferralCode;
        txt_ReferralCode.Text = referralCode;
        dic.Add(txt_ReferralCode.ID, referralCode);

        string licenseNumber = advertiser.LicenseId;
        txt_licenseNumber.Text = licenseNumber;
        dic.Add(txt_licenseNumber.ID, licenseNumber);

        string custom1 = advertiser.Custom1;
        dic.Add(txt_Custom1.ID, custom1);
        string custom2 = advertiser.Custom2;
        dic.Add(txt_Custom2.ID, custom2);
        string custom3 = advertiser.Custom3;
        dic.Add(txt_Custom3.ID, custom3);
        string custom4 = advertiser.Custom4;
        dic.Add(txt_Custom4.ID, custom4);
        string custom5 = advertiser.Custom5;
        dic.Add(txt_Custom5.ID, custom5);
        txt_Custom1.Text = custom1;
        txt_Custom2.Text = custom2;
        txt_Custom3.Text = custom3;
        txt_Custom4.Text = custom4;
        txt_Custom5.Text = custom5;

        string _biz_id = advertiser.BizId;
        dic.Add(txt_BizId.ID, _biz_id);
        txt_BizId.Text = _biz_id;

        string _dateOfBirth = (advertiser.DateOfBirth == DateTime.MinValue) ? "" : String.Format(DatePickerFormat, advertiser.DateOfBirth);
        dic.Add(txt_DateOfBirth.ID, _dateOfBirth);
        txt_DateOfBirth.Text = _dateOfBirth;
        hf_DateOfBirth.Value = _dateOfBirth;

        dic.Add(txt_BankAccountNumber.ID, advertiser.BankAccountNumber);
        txt_BankAccountNumber.Text = advertiser.BankAccountNumber;

        dic.Add(txt_BankRoutingNumbe.ID, advertiser.BankRoutingNumber);
        txt_BankRoutingNumbe.Text = advertiser.BankRoutingNumber;

        dic.Add(txt_ein.ID, advertiser.EIN);
        txt_ein.Text = advertiser.EIN;

        dic.Add(txt_SocialSecurityNumber.ID, advertiser.SocialSecurityNumber);
        txt_SocialSecurityNumber.Text = advertiser.SocialSecurityNumber;

        ListItem _businessType = ddl_BusinessType.Items.FindByValue(advertiser.BusinessType + "");
        dic.Add(ddl_BusinessType.ID, _businessType.Text);
        ddl_BusinessType.SelectedIndex = ddl_BusinessType.Items.IndexOf(_businessType);

   //     dic.Add(txt_FundsStatus.ID, advertiser

        bool _AllowRecordings = advertiser.AllowRecordings;// (!(node.Attributes["AllowRecordings"] == null || node.Attributes["AllowRecordings"].Value == "False"));
        _CheckBoxRecord.Checked = _AllowRecordings;
        dic.Add(_CheckBoxRecord.ID, _AllowRecordings.ToString());

        cb_SendSms.Checked = !advertiser.DoNotSendSMS;
        dic.Add(cb_SendSms.ID, advertiser.DoNotSendSMS.ToString());

        string _timeZone = advertiser.TimeZone + "";
        
        ddl_TimeZone.SelectedIndex = -1;
        if (!string.IsNullOrEmpty(_timeZone))
        {
            foreach (ListItem li in ddl_TimeZone.Items)
            {
                if (li.Value == _timeZone)
                {
                    li.Selected = true;
                    _timeZone = li.Text;
                    break;
                }
            }
        }
        else
            setDefaultTimeZone();

        dic.Add(ddl_TimeZone.ID, _timeZone);

        foreach (ListItem li in ddl_AccountManager.Items)
        {
            li.Selected = (li.Value == UserManager);
        }
       
        string mode="";
        if(Request["mode"]!=null)
            mode=Request["mode"].ToString();
        else
            mode="click";
        

        
        
        /************ set country ****************/
        string countryNameDefault = advertiser.CountryName;

        if (string.IsNullOrEmpty(countryNameDefault))
        {                

            string command = "EXEC dbo.GetSiteDetailBySiteNameId @SiteId";
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteId", siteSetting.GetSiteID);
                SqlDataReader reader = cmd.ExecuteReader();
                string countryNameDefaultDB = "";
                if (reader.Read())
                {
                    countryNameDefaultDB = (string)reader["CountryName"];
                    countryNameDefault = countryNameDefaultDB;
                }

                else
                    countryNameDefault = "";

                conn.Close();
            }
        }

        dic.Add(countryName.ID, countryNameDefault);

        countryName.SelectedIndex = -1;
        for (int i = 0; i < countryName.Items.Count; i++)
        {
            if (countryName.Items[i].Text == countryNameDefault)
            {

                countryName.Items[i].Selected = true;
                break;
            }
        }

        /************ end set country ****************/
        //get guid regions
        CityNameNode = advertiser.CityName;
        dic.Add(cityName.ID, CityNameNode);

        StreetNameNode = advertiser.StreetName;
        dic.Add(streetName.ID, StreetNameNode);

        string _District = advertiser.DistrictName;
        dic.Add(txt_District.ID, _District);

        string _state = advertiser.StateName;
        dic.Add(stateName.ID, _state);

        hf_stateGuid.Value = (advertiser.StateId == Guid.Empty) ? string.Empty : advertiser.StateId.ToString();
        hf_cityGuid.Value = (advertiser.CityId == Guid.Empty) ? string.Empty : advertiser.CityId.ToString();
        hf_districtGuid.Value = (advertiser.DistrictId == Guid.Empty) ? string.Empty : advertiser.DistrictId.ToString();
        hf_StreetGuid.Value = (advertiser.StreetId == Guid.Empty) ? string.Empty : advertiser.StreetId.ToString();

        stateName.Text = _state;
        streetName.Text = StreetNameNode;
        cityName.Text = CityNameNode;
        txt_District.Text = _District;

        string _house_num = advertiser.HouseNum;
        houseNum.Text = _house_num;
        dic.Add(houseNum.ID, _house_num);

        string _zip = advertiser.Zipcode;
        zip.Text = _zip;
        dic.Add(zip.ID, _zip);

        int PublisherRanking = advertiser.PublisherRanking;
        
        txt_Ranking.Text = "" + PublisherRanking;
        dic.Add(txt_Ranking.ID, PublisherRanking.ToString());
        string HouseNum = "";
        string StreetName = "";
        
        if (countryName.Items[countryName.SelectedIndex].Text == "China")
        {
            HouseNum = houseNum.Text;
            if (HouseNum.IndexOf("号") == -1)
                HouseNum = HouseNum + "号";

            StreetName = streetName.Text;

            if (StreetName.IndexOf("路") == -1)
                StreetName = StreetName + "路";
        }

        else
        {
            StreetName = streetName.Text;
            HouseNum = houseNum.Text;
        }

        Session["address"] = StreetName + " " + HouseNum + "," + cityName.Text + "," +
           (stateName.Text == "" ? "" : stateName.Text + ",") + countryName.Items[countryName.SelectedIndex].Value;

        string _site = advertiser.CompanySite;
        site.Text = _site;
        dic.Add(site.ID, _site);

        string _contact_phone = advertiser.ContactPhone;
        contactPhone.Text = _contact_phone;
        dic.Add(contactPhone.ID, _contact_phone);

        string _employee = advertiser.NumberOfEmployees + "";
        txt_employee.Text = _employee;
        dic.Add(txt_employee.ID, _employee);

        

        /************ password ****************/
        txt_password.Text = advertiser.Password;

        /*********** Supplier Status **********/
        /*
        if (userManagement.IsPublisher())
        {
            LoadInactivityReasons();
            _PanelAccountStatus.Visible = true;
            Image_status.ImageUrl = Utilities.GetRamzorImage(advertiser.SupplierStatus);
            if (advertiser.SupplierStatus == WebReferenceSupplier.SupplierStatus.Inactive)
                Image_status.OnClientClick = "return ConfirmActiveAccount();";
            else 
                Image_status.OnClientClick = "InactiveAccount('" + GetGuidSetting() + "'); return false;";
        }
        else
            _PanelAccountStatus.Visible = false;
        */
        /**************  RegistrationStage Influences ****************/

        int _RegistrationStage = advertiser.RegistrationStage;// node.Attributes["RegistrationStage"].InnerText;
     //   _RegistrationStage = 9;
        string csnameStep = "setStep";
        Type cstypeStep = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager csStep = this.Page.ClientScript;
        string csTextStep = "";

        if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
        {
            csTextStep = "witchAlreadyStep(" + _RegistrationStage + ",\"" + mode + "\");";
            csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
        }
     //   int _RegistrationStage = Convert.ToInt32(RegistrationStage);
        if (_RegistrationStage > 0)
        {
            btnSend.Text = lbl_UPDATE.Text;
            pSteps.Visible = false;
        }
        else
        {
            LoadFirstTimeDetails();
            pSteps.Visible = true;
        }
        RegistrationStageV = _RegistrationStage;
        dicDetailsV = dic;
        /**************  End RegistrationStage Influences ****************/

    }
    
    WebReferenceSupplier.AuditEntry SetAuditEntry(string FieldId, WebReferenceSupplier.Status _status,
            string fieldName, string _newValue, string _oldValue, string pageName)
    {
        
        UserMangement um = GetUserSetting();
        if (um == null)
            um = new UserMangement(Guid.Empty.ToString(), companyName.Text);
        WebReferenceSupplier.AuditEntry ae = new WebReferenceSupplier.AuditEntry();
        ae.AdvertiseId = new Guid(um.GetGuid);
        ae.AdvertiserName = um.User_Name;
        ae.AuditStatus = _status;
        ae.FieldId = FieldId;
        ae.FieldName = fieldName;
        ae.NewValue = _newValue;
        ae.OldValue = _oldValue;
        ae.PageId = pageName;
        ae.UserId = new Guid(userManagement.GetGuid);
        ae.UserName = userManagement.User_Name;
        ae.PageName = page_translate;
        return ae;
    }
    protected void btnSend_MyClick()
    {
        
        List<WebReferenceSupplier.AuditEntry> list = new List<WebReferenceSupplier.AuditEntry>();
        Dictionary<string, string> dic = dicDetailsV;
        StringBuilder xml = new StringBuilder();
        WebReferenceSupplier.Status _statusUpdate = (string.IsNullOrEmpty(GetGuidSetting()) ||
            (userManagement.IsSupplier() && RegistrationStageV < 2))?
            WebReferenceSupplier.Status.Insert : WebReferenceSupplier.Status.Update;
        WebReferenceSupplier.Status _statusDelete = WebReferenceSupplier.Status.Delete;
        string pagename =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        //       lbl_errorCity.Visible = false;
        page_translate = EditTranslateDB.GetPageNameTranslate(pagename, siteSetting.siteLangId);
        lbl_errorCity.Attributes["style"] = "display:none;";
        _UpdatePanelCity.Update();
        string FirstName = firstName.Text;
        string OsekId = osekId.Text;
        string SecondName = secondName.Text;
        string CompanyName = companyName.Text;
        string Phone = phone.Text;
        string Fax = fax.Text;
        string Email = email.Text;
        //string CountryName = countryName.Text;
        string CountryName = countryName.Items[countryName.SelectedIndex].Text;
        string StateName = stateName.Text;
        string CityName = cityName.Text;

        string StreetName = streetName.Text;
        string district = txt_District.Text;
        string PublisherRanking = txt_Ranking.Text;
        string custom1 = txt_Custom1.Text;
        string custom2 = txt_Custom2.Text;
        string custom3 = txt_Custom3.Text;
        string custom4 = txt_Custom4.Text;
        string custom5 = txt_Custom5.Text;
        string SmsNumber = txt_SmsNumber.Text;
        Guid AccountManager = new Guid(ddl_AccountManager.SelectedValue);
        if (_PanelAccountManager.Visible && PpcSite.GetCurrent().IsZapSite(siteSetting.GetSiteID))
        {
            if (AccountManager == Guid.Empty)
            {
                _PanelAccountManager.Attributes.Add("class", "must");
                lbl_MustSelectAccountManager.Style[HtmlTextWriterStyle.Display] = "inline";
                return;
            }
        }
        /*
        if (AutoCompleteExtenderCity.Enabled)
        {
            if (!(!RequiredFieldValidatorAddress.Visible && string.IsNullOrEmpty(CityName)))
            {
                if (_UpdatePanelCity.Visible && !new AutoComplete().IsExists(CityName))
                {
                    //         lbl_errorCity.Visible = true;
                    lbl_errorCity.Attributes["style"] = "display:inline;";
                    _UpdatePanelCity.Update();
                    return;
                }
            }
        }
        */

        Session["City"] = CityName;
        Session["streetOrCityChange"] = "false";
        bool ifChange = false;

        if (StreetNameNode != "" && StreetNameNode != null)
        {
            if (StreetName != StreetNameNode)
            {
                //Session["streetChange"] = "true"; 
                ifChange = true;
            }

        }

        if (ifChange == false)
        {
            if (CityNameNode != "" && CityNameNode != null)
            {
                if (CityName != CityNameNode)
                {
                    ifChange = true;
                }
            }
        }

        if (ifChange == true)
            Session["streetOrCityChange"] = "true";

        string HouseNum =  houseNum.Text;
 //       string HouseNum = houseNum.Text;
        string Zip = zip.Text;

        string StreetNameConvert = "";
        string HouseNumConvert = "";

        if (countryName.Items[countryName.SelectedIndex].Text == "China")
        {
            HouseNumConvert = houseNum.Text;
            if (HouseNumConvert.IndexOf("号") == -1)
                HouseNumConvert = HouseNumConvert + "号";

            StreetNameConvert = streetName.Text;

            if (StreetNameConvert.IndexOf("路") == -1)
                StreetNameConvert = StreetNameConvert + "路";
        }

        else
        {
            HouseNumConvert = houseNum.Text;
            StreetNameConvert = streetName.Text;
        }

        Session["address"] = StreetNameConvert + " " + HouseNumConvert + "," +
            CityName + "," + (StateName == "" ? "" : StateName + ",") +
              countryName.Items[countryName.SelectedIndex].Value;

        string Site = site.Text;
        string ContactPhone = contactPhone.Text;
        string Employees = txt_employee.Text;
        int _employees;
        if (!int.TryParse(Employees, out _employees))
        {
            Employees = string.Empty;
            _employees = -1;
        }
        string _timeZone = ddl_TimeZone.SelectedValue;
        string _timeZoneVal = ddl_TimeZone.SelectedItem.Text;
        string _phone = phone.Text.Trim();
        bool _AllowRecordings = _CheckBoxRecord.Checked;
        //    string CrmGuid = "82a2492b-fe43-dc11-876b-0013724c9692";


        //    Response.Write("<br>CrmGuid: " + CrmGuid);
        //     xml = @"<InternetSupplier SupplierId=""" + CrmGuid + @""">" +
        Dictionary<string, PanelValidate> _dic = dicV;
        string stateId = string.IsNullOrEmpty(StateName) ? string.Empty :
            new RegionService().GetGuid(StateName, GetPanelDisplayByTxtboxId(stateName.ClientID, _dic).AutoCompleteLevel.ToString());
        string cityId = string.IsNullOrEmpty(CityName) ? string.Empty :
            new RegionService().GetGuid(CityName, GetPanelDisplayByTxtboxId(cityName.ClientID, _dic).AutoCompleteLevel.ToString());
        string districtId = string.IsNullOrEmpty(district) ? string.Empty :
            new RegionService().GetGuid(district, GetPanelDisplayByTxtboxId(txt_District.ClientID, _dic).AutoCompleteLevel.ToString());
        string StreetId = string.IsNullOrEmpty(StreetName) ? string.Empty :
            new RegionService().GetGuid(StreetName, GetPanelDisplayByTxtboxId(streetName.ClientID, _dic).AutoCompleteLevel.ToString());
        hf_cityGuid.Value = cityId;
        hf_districtGuid.Value = districtId;
        hf_stateGuid.Value = stateId;
        hf_StreetGuid.Value = StreetId;


        WebReferenceSupplier.UpsertAdvertiserRequest _request = new WebReferenceSupplier.UpsertAdvertiserRequest();
        _request.IsFromAdvertiserPortal = userManagement.IsSupplier();
        _request.SupplierId = (string.IsNullOrEmpty(GetGuidSetting())) ? Guid.Empty : new Guid(GetGuidSetting());

        
        if (!dic.ContainsKey(companyName.ID))
            dic.Add(companyName.ID, string.Empty);
        if (dic[companyName.ID] != CompanyName)
        {   
            _request.Company = CompanyName;
            list.Add(SetAuditEntry(lbl_company.ID, ((CompanyName.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_company.InnerHtml, CompanyName, dic[companyName.ID], pagename));
        }
        if (!dic.ContainsKey(email.ID))
            dic.Add(email.ID, string.Empty);
        if (dic[email.ID] != Email)
        {
            _request.Email = Email;
            list.Add(SetAuditEntry(lbl_email.ID, ((Email.Length == 0)?_statusDelete:_statusUpdate),
                lbl_email.InnerHtml, Email, dic[email.ID], pagename));
            if (userManagement.IsSupplier())
                userManagement.Email = Email;
        }
        if (!dic.ContainsKey(txt_SmsNumber.ID))
            dic.Add(txt_SmsNumber.ID, string.Empty);
        if (dic[txt_SmsNumber.ID] != SmsNumber)
        {
            _request.SmsNumber = SmsNumber;
            list.Add(SetAuditEntry(lbl_SmsNumber.ID, ((SmsNumber.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_SmsNumber.InnerHtml, SmsNumber, dic[txt_SmsNumber.ID], pagename));
        }
        if (!dic.ContainsKey(firstName.ID))
            dic.Add(firstName.ID, string.Empty);
        if (dic[firstName.ID] != FirstName)
        {
            _request.FirstName = FirstName;
            list.Add(SetAuditEntry(lbl_FName.ID, ((FirstName.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_FName.InnerHtml, FirstName, dic[firstName.ID], pagename));
        }
        if (!dic.ContainsKey(secondName.ID))
            dic.Add(secondName.ID, string.Empty);
        if (dic[secondName.ID] != SecondName)
        {
            _request.LastName = SecondName;
            list.Add(SetAuditEntry(lbl_LName.ID, ((SecondName.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_LName.InnerHtml, SecondName, dic[secondName.ID], pagename));
        }
        if (!dic.ContainsKey(contactPhone.ID))
            dic.Add(contactPhone.ID, string.Empty);
        if (dic[contactPhone.ID] != ContactPhone)
        {
            _request.ContactPhone = ContactPhone;
            list.Add(SetAuditEntry(lbl_contactPerson.ID, ((ContactPhone.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_contactPerson.InnerHtml, ContactPhone, dic[contactPhone.ID], pagename));
        }
        
        if (!dic.ContainsKey(phone.ID))
            dic.Add(phone.ID, string.Empty);
        if (dic[phone.ID] != _phone)
        {
            _request.CompanyPhone = _phone;
            list.Add(SetAuditEntry(lbl_phone.ID, ((_phone.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_phone.InnerHtml, _phone, dic[phone.ID], pagename));
        }
        if (!dic.ContainsKey(fax.ID))
            dic.Add(fax.ID, string.Empty);
        if (dic[fax.ID] != Fax)
        {
            _request.Fax = Fax;
            list.Add(SetAuditEntry(lbl_fax.ID, ((Fax.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_fax.InnerHtml, Fax, dic[fax.ID], pagename));
        }
        if (!dic.ContainsKey(countryName.ID))
            dic.Add(countryName.ID, string.Empty);
        if (dic[countryName.ID] != CountryName)
        {
            _request.CountryName = CountryName;
            list.Add(SetAuditEntry(lblCountry.ID, ((CountryName.Length == 0) ? _statusDelete : _statusUpdate),
                lblCountry.InnerHtml, CountryName, dic[countryName.ID], pagename));
        }
        if (!dic.ContainsKey(stateName.ID))
            dic.Add(stateName.ID, string.Empty);
        if (dic[stateName.ID] != StateName)
        {
            _request.StateName = StateName;
            list.Add(SetAuditEntry(lblState.ID, ((StateName.Length == 0) ? _statusDelete : _statusUpdate),
                lblState.InnerHtml, StateName, dic[stateName.ID], pagename));
        }
        if (!string.IsNullOrEmpty(stateId))
            _request.StateId = new Guid(stateId);
        if (!dic.ContainsKey(cityName.ID))
            dic.Add(cityName.ID, string.Empty);
        if (dic[cityName.ID] != CityName)
        {
            _request.CityName = CityName;
            list.Add(SetAuditEntry(lbl_city.ID, ((CityName.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_city.InnerHtml, CityName, dic[cityName.ID], pagename));
        }
        if (!string.IsNullOrEmpty(cityId))
            _request.CityId = new Guid(cityId);
        if (!dic.ContainsKey(txt_District.ID))
            dic.Add(txt_District.ID, string.Empty);
        if (dic[txt_District.ID] != district)
        {
            _request.DistrictName = district;
            list.Add(SetAuditEntry(lbl_district.ID, ((district.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_district.InnerHtml, district, dic[txt_District.ID], pagename));
        }
        if (!string.IsNullOrEmpty(districtId))
            _request.DistrictId = new Guid(districtId);
        if (!dic.ContainsKey(streetName.ID))
            dic.Add(streetName.ID, string.Empty);
        if (dic[streetName.ID] != StreetName)
        {
            _request.StreetName = StreetName;
            list.Add(SetAuditEntry(lbl_street.ID, ((StreetName.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_street.InnerHtml, StreetName, dic[streetName.ID], pagename));
        }
        if (!string.IsNullOrEmpty(StreetId))
            _request.StreetId = new Guid(StreetId);
        if (!dic.ContainsKey(houseNum.ID))
            dic.Add(houseNum.ID, string.Empty);
        if (dic[houseNum.ID] != HouseNum)
        {
            _request.HouseNum = HouseNum;
            list.Add(SetAuditEntry(lbl_house.ID, ((HouseNum.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_house.InnerHtml, HouseNum, dic[houseNum.ID], pagename));
        }
        if (!dic.ContainsKey(zip.ID))
            dic.Add(zip.ID, string.Empty);
        if (dic[zip.ID] != Zip)
        {
            _request.Zipcode = Zip;
            list.Add(SetAuditEntry(lbl_zip.ID, ((Zip.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_zip.InnerHtml, Zip, dic[zip.ID], pagename));
        }
        if (!dic.ContainsKey(osekId.ID))
            dic.Add(osekId.ID, string.Empty);
        if (dic[osekId.ID] != OsekId)
        {
            _request.CompanyId = OsekId;
            list.Add(SetAuditEntry(lbl_companyNum.ID, ((OsekId.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_companyNum.InnerHtml, OsekId, dic[osekId.ID], pagename));
        }
        if (!dic.ContainsKey(site.ID))
            dic.Add(site.ID, string.Empty);
        if (dic[site.ID] != Site)
        {
            _request.CompanySite = Site;
            list.Add(SetAuditEntry(lbl_website.ID, ((Site.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_website.InnerHtml, Site, dic[site.ID], pagename));
        }
        if (!dic.ContainsKey(txt_employee.ID))
            dic.Add(txt_employee.ID, string.Empty);
        if (dic[txt_employee.ID] != Employees)
        {
            _request.NumberOfEmployees = _employees;// int.Parse(Employees);
            list.Add(SetAuditEntry(lbl_Employee.ID, ((Employees.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_Employee.InnerHtml, Employees, dic[txt_employee.ID], pagename));
        }
        if (!dic.ContainsKey(ddl_TimeZone.ID))
            dic.Add(ddl_TimeZone.ID, string.Empty);
        if (dic[ddl_TimeZone.ID] != _timeZoneVal)
        {
            _request.TimeZone = int.Parse(_timeZone);
            list.Add(SetAuditEntry(lbl_TimeZone.ID, ((_timeZoneVal.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_TimeZone.InnerHtml, _timeZoneVal, dic[ddl_TimeZone.ID], pagename));
        }
        if (!dic.ContainsKey(_CheckBoxRecord.ID))
            dic.Add(_CheckBoxRecord.ID, string.Empty);
        if(dic[_CheckBoxRecord.ID]!=_AllowRecordings.ToString())
        {
            _request.AllowRecordings = _AllowRecordings;
            list.Add(SetAuditEntry(lbl_Record.ID, _statusUpdate,
                lbl_Record.InnerHtml, _AllowRecordings.ToString(), dic[_CheckBoxRecord.ID], pagename));
        }
        if (!dic.ContainsKey(cb_SendSms.ID))
            dic.Add(cb_SendSms.ID, string.Empty);
        if (dic[cb_SendSms.ID] != (!cb_SendSms.Checked).ToString())
        {
            _request.DoNotSendSms = !cb_SendSms.Checked;
            list.Add(SetAuditEntry(lbl_SendSms.ID, _statusUpdate,
                lbl_SendSms.InnerHtml, (!cb_SendSms.Checked).ToString(), dic[cb_SendSms.ID], pagename));
        }
        if (!dic.ContainsKey(txt_Ranking.ID))
            dic.Add(txt_Ranking.ID, string.Empty);
        if (dic[txt_Ranking.ID] != PublisherRanking)
        {
            _request.PublisherRanking = int.Parse(PublisherRanking);
            list.Add(SetAuditEntry(lbl_Ranking.ID, ((PublisherRanking.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_Ranking.InnerHtml, PublisherRanking, dic[txt_Ranking.ID], pagename));
        }
        string licenseNumber = txt_licenseNumber.Text;
        if (!dic.ContainsKey(txt_licenseNumber.ID))
            dic.Add(txt_licenseNumber.ID, string.Empty);
        if (dic[txt_licenseNumber.ID] != licenseNumber)
        {
            _request.LicenseId = licenseNumber;
            list.Add(SetAuditEntry(lbl_licenseNumber.ID, ((licenseNumber.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_licenseNumber.InnerHtml, licenseNumber, dic[txt_licenseNumber.ID], pagename));
        }
        string referralCode = txt_ReferralCode.Text;
        if (!dic.ContainsKey(txt_ReferralCode.ID))
            dic.Add(txt_ReferralCode.ID, string.Empty);
        if (dic[txt_ReferralCode.ID] != referralCode)
        {
            _request.ReferraleCode = referralCode;
            list.Add(SetAuditEntry(lbl_ReferralCode.ID, ((referralCode.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_ReferralCode.InnerHtml, referralCode, dic[txt_ReferralCode.ID], pagename));
        }
        if (!dic.ContainsKey(txt_Custom1.ID))
            dic.Add(txt_Custom1.ID, string.Empty);
        if (dic[txt_Custom1.ID] != custom1)
        {
            _request.Custom1 = custom1;
            list.Add(SetAuditEntry(lbl_Custom1.ID, ((custom1.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_Custom1.InnerHtml, custom1, dic[txt_Custom1.ID], pagename));
        }
        if (!dic.ContainsKey(txt_Custom2.ID))
            dic.Add(txt_Custom2.ID, string.Empty);
        if (dic[txt_Custom2.ID] != custom2)
        {
            _request.Custom2 = custom2;
            list.Add(SetAuditEntry(lbl_Custom2.ID, ((custom2.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_Custom2.InnerHtml, custom2, dic[txt_Custom2.ID], pagename));
        }
        if (!dic.ContainsKey(txt_Custom3.ID))
            dic.Add(txt_Custom3.ID, string.Empty);
        if (dic[txt_Custom3.ID] != custom3)
        {
            _request.Custom3 = custom3;
            list.Add(SetAuditEntry(lbl_Custom3.ID, ((custom3.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_Custom3.InnerHtml, custom3, dic[txt_Custom3.ID], pagename));
        }
        if (!dic.ContainsKey(txt_Custom4.ID))
            dic.Add(txt_Custom4.ID, string.Empty);
        if (dic[txt_Custom4.ID] != custom4)
        {
            _request.Custom4 = custom4;
            list.Add(SetAuditEntry(lbl_Custom4.ID, ((custom4.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_Custom4.InnerHtml, custom4, dic[txt_Custom4.ID], pagename));
        }
        if (!dic.ContainsKey(txt_Custom5.ID))
            dic.Add(txt_Custom5.ID, string.Empty);
        if (dic[txt_Custom5.ID] != custom5)
        {
            _request.Custom5 = custom5;
            list.Add(SetAuditEntry(lbl_Custom5.ID, ((custom5.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_Custom5.InnerHtml, custom5, dic[txt_Custom5.ID], pagename));
        }
        
        if (!dic.ContainsKey(ddl_FundsStatus.ID))
            dic.Add(ddl_FundsStatus.ID, string.Empty);
        if (dic[ddl_FundsStatus.ID] != ddl_FundsStatus.SelectedItem.Text)
        {
            WebReferenceSupplier.FundsStatus fs;
            if (!Enum.TryParse(ddl_FundsStatus.SelectedValue, out fs))
                fs = WebReferenceSupplier.FundsStatus.Normal;
            _request.FundsStatus = fs;
            list.Add(SetAuditEntry(lbl_FundsStatus.ID, _statusUpdate,
                lbl_FundsStatus.InnerHtml, ddl_FundsStatus.SelectedItem.Text, dic[ddl_FundsStatus.ID], pagename));
        }

        if (dic[ddl_BusinessType.ID] != ddl_BusinessType.SelectedItem.Text)
        {
            
            list.Add(SetAuditEntry(lbl_BusinessType.ID, _statusUpdate,
                lbl_BusinessType.InnerHtml, ddl_BusinessType.SelectedItem.Text, dic[ddl_BusinessType.ID], pagename));
            if(ddl_BusinessType.SelectedItem.Value == "-1")
                _request.BusinessType =  null;
            else
                _request.BusinessType = int.Parse(ddl_BusinessType.SelectedItem.Value);
        }
        string _dateOfBirth = hf_DateOfBirth.Value;//txt_DateOfBirth.Text;
        if (!dic.ContainsKey(txt_DateOfBirth.ID))
            dic.Add(txt_DateOfBirth.ID, string.Empty);
        if (dic[txt_DateOfBirth.ID] != _dateOfBirth)
        {
            if (string.IsNullOrEmpty(_dateOfBirth))
                _request.DateOfBirth = null;
            else
                _request.DateOfBirth=DateTime.ParseExact(_dateOfBirth, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            list.Add(SetAuditEntry(lbl_DateOfBirth.ID, ((_dateOfBirth.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_DateOfBirth.InnerHtml, _dateOfBirth, dic[txt_DateOfBirth.ID], pagename));
        }

        if (!dic.ContainsKey(txt_BankAccountNumber.ID))
            dic.Add(txt_BankAccountNumber.ID, string.Empty);
        if (dic[txt_BankAccountNumber.ID] != txt_BankAccountNumber.Text)
        {
            _request.BankAccountNumber = txt_BankAccountNumber.Text;
            list.Add(SetAuditEntry(lbl_BankAccountNumber.ID, ((txt_BankAccountNumber.Text.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_BankAccountNumber.InnerHtml, txt_BankAccountNumber.Text, dic[txt_BankAccountNumber.ID], pagename));
        }
        if (!dic.ContainsKey(txt_BankRoutingNumbe.ID))
            dic.Add(txt_BankRoutingNumbe.ID, string.Empty);
        if (dic[txt_BankRoutingNumbe.ID] != txt_BankRoutingNumbe.Text)
        {
            _request.BankRoutingNumber = txt_BankRoutingNumbe.Text;
            list.Add(SetAuditEntry(lbl_BankRoutingNumbe.ID, ((txt_BankRoutingNumbe.Text.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_BankRoutingNumbe.InnerHtml, txt_BankRoutingNumbe.Text, dic[txt_BankRoutingNumbe.ID], pagename));
        }
        if (!dic.ContainsKey(txt_ein.ID))
            dic.Add(txt_ein.ID, string.Empty);
        if (dic[txt_ein.ID] != txt_ein.Text)
        {
            _request.EIN = txt_ein.Text;
            list.Add(SetAuditEntry(lbl_ein.ID, ((txt_ein.Text.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_ein.InnerHtml, txt_ein.Text, dic[txt_ein.ID], pagename));
        }
        if (!dic.ContainsKey(txt_SocialSecurityNumber.ID))
            dic.Add(txt_SocialSecurityNumber.ID, string.Empty);
        if (dic[txt_SocialSecurityNumber.ID] != txt_SocialSecurityNumber.Text)
        {
            _request.SocialSecurityNumber = txt_SocialSecurityNumber.Text;
            list.Add(SetAuditEntry(lbl_SocialSecurityNumber.ID, ((txt_SocialSecurityNumber.Text.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_SocialSecurityNumber.InnerHtml, txt_SocialSecurityNumber.Text, dic[txt_SocialSecurityNumber.ID], pagename));
        }


         

        string NewStatus = ddl_MoveTo.SelectedValue;
        WebReferenceSupplier.DapazStatusChangeOptions ds;
        
        if (Enum.TryParse(NewStatus, out ds))
        {
            if (!dic.ContainsKey(txt_Status.ID))
                dic.Add(txt_Status.ID, string.Empty);
            if (dic[txt_Status.ID] != ddl_MoveTo.SelectedItem.Text)
            {
                _request.NewDapazStatus = ds;
                list.Add(SetAuditEntry(lbl_Status.ID,  _statusUpdate,
                    lbl_Status.InnerHtml, ddl_MoveTo.SelectedItem.Text, dic[txt_Status.ID], pagename));
            }
        }
        
        if (!dic.ContainsKey(txt_BizId.ID))
            dic.Add(txt_BizId.ID, string.Empty);
        string _BizId = txt_BizId.Text;
        if (dic[txt_BizId.ID] != _BizId)
        {
            _request.BizId = _BizId;
            list.Add(SetAuditEntry(lbl_BizId.ID, ((_BizId.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_BizId.InnerHtml, _BizId, dic[txt_BizId.ID], pagename));
        }
        if (!dic.ContainsKey(lbl_AccountManager.ID))
            dic.Add(lbl_AccountManager.ID, string.Empty);
        if (dic[lbl_AccountManager.ID] != AccountManager.ToString())
        {
            _request.ManagerId = AccountManager;
            string UserAccountManager = (_request.ManagerId == Guid.Empty) ? string.Empty :
                ddl_AccountManager.SelectedItem.Text;
            string old_value = string.Empty;
            foreach(ListItem _li in ddl_AccountManager.Items)
            {
                if(_li.Value==dic[lbl_AccountManager.ID])
                {
                    old_value=_li.Text;
                    break;
                }
            }
            list.Add(SetAuditEntry(lbl_AccountManager.ID, ((UserAccountManager.Length == 0) ? _statusDelete : _statusUpdate),
                lbl_AccountManager.Text, UserAccountManager, 
                old_value, pagename));
        }
       
        if (list.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noNewData", "top.NoNewData();", true);
            return;
        }
      //  _request.
        _request.AuditOn = (RegistrationStageV < 2 && userManagement.IsPublisher()) ? false : true;
        _request.AuditEntries = list.ToArray();

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfUpsertAdvertiserResponse result = null;
        try
        {
            result = supplier.UpsertAdvertiser(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }        
        
        string txtButton = btnSend.Text;

        if (result.Value.Status == WebReferenceSupplier.UpsertAdvertiserStatus.EmailDuplicate)
        {           
           
            ScriptManager.RegisterStartupScript(this, this.GetType(), "errorMessage", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_emailExist.Text) + "');", true);
            email.CssClass = "txt_focus";
            email.Focus();
            return;
        }
        else if (result.Value.Status == WebReferenceSupplier.UpsertAdvertiserStatus.PhoneDuplicate)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "errorMessage", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_phoneExists.Text) + "');", true);
            contactPhone.CssClass = "txt_focus";
            contactPhone.Focus();
            return;
        }
        else if (result.Value.Status == WebReferenceSupplier.UpsertAdvertiserStatus.BizIdDuplicate)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "errorMessage", "alert('" + HttpUtility.JavaScriptStringEncode(WebReferenceSupplier.UpsertAdvertiserStatus.BizIdDuplicate.ToString()) + "');", true);
            txt_BizId.CssClass = "txt_focus";
            txt_BizId.Focus();
            return;
        }

        if (string.IsNullOrEmpty(GetGuidSetting()))
        {
            string guID = result.Value.SupplierId.ToString();
            this.SetGuidSetting(new UserMangement(guID, CompanyName, 0, false));
            OneCallUtilities.AuditRequest audit_request = new OneCallUtilities.AuditRequest();
            List<OneCallUtilities.AuditEntry> _list = new List<OneCallUtilities.AuditEntry>();
            foreach (WebReferenceSupplier.AuditEntry _ae in _request.AuditEntries)
            {
                OneCallUtilities.AuditEntry ocu_ae = new OneCallUtilities.AuditEntry();
                ocu_ae.AdvertiseId = new Guid(guID);
                ocu_ae.AdvertiserName = CompanyName;
                ocu_ae.AuditStatus = OneCallUtilities.Status.Insert;
                ocu_ae.FieldId = _ae.FieldId;
                ocu_ae.FieldName = _ae.FieldName;
                ocu_ae.NewValue = _ae.NewValue;
                ocu_ae.OldValue = _ae.OldValue;
                ocu_ae.PageId = _ae.PageId;
                ocu_ae.UserId = _ae.UserId;
                ocu_ae.UserName = CompanyName;
                ocu_ae.PageName = page_translate;
                _list.Add(ocu_ae);
            }
            OneCallUtilities.OneCallUtilities ocu = WebServiceConfig.GetOneCallUtilitiesReference(this);
            
            audit_request.AuditOn = true;

            audit_request.AuditEntries = _list.ToArray();
            OneCallUtilities.Result _result = new OneCallUtilities.Result();
            try
            {
                _result = ocu.CreateAudit(audit_request);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            }            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetUserNameSetting", "SetUserNameSetting('" + CompanyName + "');", true);
        }
        if (userManagement.IsSupplier())
            userManagement.User_Name = CompanyName;
        else
        {
            SetUserNameSetting(CompanyName);
            SetSettingTab();
        }
        string _script = "try{parent.SetUserName('" + HttpUtility.HtmlEncode(CompanyName) + "');}catch(ex){}";
        //      string _script = "parent.SetUserName('" + CompanyName + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "setUserName", _script, true);

        if (txtButton == lbl_NEXT.Text)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setStep2", "setStep2();", true);
        else
        {
            getDetails(GetGuidSetting());
            SetPanels();
        }
        //Refresh the search advertiser parent page
        if (userManagement.IsPublisher())
            ScriptManager.RegisterStartupScript(this, this.GetType(), "run_report", "top.run_report();", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        
    }

    private void SetSettingTab()
    {
        string _script = "parent.SetSettingTab('" + ResolveUrl("~") + "Management/ProfessionalSetting.aspx" + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetSettingTab", _script, true);
    }
    private PanelValidate GetPanelDisplayByTxtboxId(string TxtboxId, Dictionary<string, PanelValidate> dic)
    {
        foreach (KeyValuePair<string, PanelValidate> kvp in dic)
        {
            if (kvp.Value.txtBoxId == TxtboxId)
                return kvp.Value;
        }
        return null;
    }

    

    public string StreetNameNode
    {
        get
        {
            return (string)ViewState["StreetNameNode"];
        }

        set
        {
            ViewState["StreetNameNode"]=value;
        }

    }

    public string CityNameNode
    {
        get
        {
            return (string)ViewState["CityNameNode"];
        }

        set
        {
            ViewState["CityNameNode"] = value;
        }

    }

    private void setDefaultCountry()
    {
        /************ set country ****************/
        string countryNameDefault = "";

        

        string command = "EXEC dbo.GetSiteDetailBySiteNameId @SiteId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            string countryNameDefaultDB = "";
            if (reader.Read())
            {
                countryNameDefaultDB = (string)reader["CountryName"];
                countryNameDefault = countryNameDefaultDB;
            }

            else
                countryNameDefault = "";

            conn.Close();
        }
        countryName.SelectedIndex = -1;
        for (int i = 0; i < countryName.Items.Count; i++)
        {
            if (countryName.Items[i].Text == countryNameDefault)
            {

                countryName.Items[i].Selected = true;
                break;
            }
        }

        /************ end set country ****************/
    }
    
    Dictionary<string, PanelValidate> dicV
    {
        get { return (ViewState["PanelValidate"] == null ? new Dictionary<string, PanelValidate>() : (Dictionary<string, PanelValidate>)ViewState["PanelValidate"]); }
        set { ViewState["PanelValidate"] = value; }
    }
    Dictionary<string, string> dicDetailsV
    {
        get { return (ViewState["dicDetails"] == null ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["dicDetails"]); }
        set { ViewState["dicDetails"] = value; }
    }
    int RegistrationStageV
    {
        get { return (ViewState["RegistrationStage"] == null) ? 1 : (int)ViewState["RegistrationStage"]; }
        set { ViewState["RegistrationStage"] = value; }
    }

    
}
