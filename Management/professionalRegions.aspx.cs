using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;

public partial class Management_professionalRegions : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {
                string csnameLogin = "setLogin";
                Type cstypeLogin = this.GetType();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager csLogin = this.Page.ClientScript;

                if (!csLogin.IsStartupScriptRegistered(cstypeLogin, csnameLogin))
                {
                    string csTextLogin = "redirect();";
                    csLogin.RegisterStartupScript(cstypeLogin, csnameLogin, csTextLogin, true);
                }
            }

            int level = Convert.ToInt16(Request["level"]);
           
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            string xmlProfessions = supplier.HandleAdvertiserRegions(siteSetting.GetSiteID, level, GetGuidSetting(), "", "");
            //Response.Write(xmlProfessions);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlProfessions);

            StringBuilder sb = new StringBuilder();
            XmlNode root = xml.DocumentElement; // Regions

            string ParentName = "";

            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++) // element primary
                {
                    ParentName = root.ChildNodes[i].Attributes["ParentName"].InnerText;

                    if (root.ChildNodes[i].Attributes["ConnectedToAdvertiser"].InnerText == "True")
                    {
                        if (ParentName!="")
                            sb.Append(ParentName +  " >> " + root.ChildNodes[i].Attributes["Name"].InnerText + "&");
                        else
                            sb.Append(root.ChildNodes[i].Attributes["Name"].InnerText + "&");
                    }
                }

                if (sb.ToString().Length>0)
                    Response.Write(sb.ToString().Substring(0, sb.ToString().Length - 1));
                else
                    Response.Write("empty");
            }

            else
                Response.Write("empty");
            

            /*
            if (level == "1")
                level = "TelAviv1";
            else if (level == "2")
                level = "petach tikva";
            else if (level == "3")
                level = "beit shemesh&lud";
            Response.Write(level);
            */
        
        }
    }
}
