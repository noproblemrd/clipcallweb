﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professionalPaymentrRunWest.aspx.cs" Inherits="Management_professionalPaymentrRunWest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        window.onload = function () {
            parent.hideDiv();
        }
        function witchAlreadyStep(level) {            
            
            parent.window.witchAlreadyStep(level);
            parent.window.setLinkStepActive('linkStep6');

        }

        var imgBronze = new Image();
        imgBronze.src = '<%#ResolveUrl("~")%>PPC/images/bronze.png';
        
        var imgBronzeHover = new Image();
        imgBronzeHover.src = '<%#ResolveUrl("~")%>PPC/images/bronze-hover.png';

        var imgSilver = new Image();
        imgSilver.src = '<%#ResolveUrl("~")%>PPC/images/silver.png';

        var imgSilverHover = new Image();
        imgSilverHover.src = '<%#ResolveUrl("~")%>PPC/images/silver-hover.png';

        var imgGold = new Image();
        imgGold.src = '<%#ResolveUrl("~")%>PPC/images/gold.png';

        var imgGoldHover = new Image();
        imgGoldHover.src = '<%#ResolveUrl("~")%>PPC/images/gold-hover.png';

       
    </script>

    <link href="../PPC/samplepPpc.css" rel="stylesheet" type="text/css" />    
    
	<script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
	<script type="text/javascript">	    try { Typekit.load(); } catch (e) { }</script>
   
</head>
<body class="step6RunWest" style="background-color:transparent;">
    <form id="form1" runat="server" >
    
    <div class="iframe-inner" >
        
        <div class="title">
            <asp:Label ID="lbl_YourBalance" runat="server" Text="Adding Credit" CssClass="titleFirst"></asp:Label>
        </div>

        
        <div id="parentPackages" class="parentPackages">
            
            <div id="bronze" class="bronze"><img id="objBronze" src="../PPC/images/bronze.png" onmouseover="this.src=imgBronzeHover.src" onmouseout="this.src=imgBronze.src"/></div>
            <div id="silver" class="silver"><img id="objSilver" src="../PPC/images/silver.png" onmouseover="this.src=imgSilverHover.src" onmouseout="this.src=imgSilver.src"/></div>
            <div id="gold" class="gold"><img id="objGold" src="../PPC/images/gold.png" onmouseover="this.src=imgGoldHover.src" onmouseout="this.src=imgGold.src"/></div>
            
            <div ID="totalBronze" runat="server" class="totalBronze total" onmouseover="document.images['objBronze'].src=imgBronzeHover.src"></div>
            <div ID="totalSilver" runat="server" class="totalSilver total" onmouseover="document.images['objSilver'].src=imgSilverHover.src"></div>
            <div ID="totalGold" runat="server" class="totalGold total"  onmouseover="document.images['objGold'].src=imgGoldHover.src"></div>
            
            <div id="PayOnlyBronze" runat="server" class="PayOnlyBronze payOnly" onmouseover="document.images['objBronze'].src=imgBronzeHover.src"></div>
            <div id="PayOnlySilver" runat="server" class="PayOnlySilver payOnly" onmouseover="document.images['objSilver'].src=imgSilverHover.src"></div>
            <div id="PayOnlyGold" runat="server" class="PayOnlyGold payOnly"  onmouseover="document.images['objGold'].src=imgGoldHover.src"></div>
            
            <div id="freeBronze" runat="server" class="freeBronze free" onmouseover="document.images['objBronze'].src=imgBronzeHover.src"></div>
            <div id="freeSilver" runat="server" class="freeSilver free" onmouseover="document.images['objSilver'].src=imgSilverHover.src"></div>            
            <div id="freeGold" runat="server" class="freeGold free"  onmouseover="document.images['objGold'].src=imgGoldHover.src"></div>  
                      
            <div id="crossBronze" class="crossBronze cross" runat="server" visible="false" onmouseover="document.images['objBronze'].src=imgBronzeHover.src"></div>
            <div id="crossSilver" class="crossSilver cross" runat="server" visible="false" onmouseover="document.images['objSilver'].src=imgSilverHover.src"></div>
            <div id="crossGold" class="crossGold cross" runat="server" visible="false"  onmouseover="document.images['objGold'].src=imgGoldHover.src"></div>
            
            <div class="btnBronze" onmouseover="document.images['objBronze'].src=imgBronzeHover.src"><asp:Button ID="btn_bronze" runat="server" Text="Buy" OnCommand="buyNow" CommandName="buyBronze" CommandArgument="bronze" class="btn_next" /></div>
            <div class="btnSilver" onmouseover="document.images['objSilver'].src=imgSilverHover.src"><asp:Button ID="btn_silver"  runat="server" Text="Buy" OnCommand="buyNow" CommandName="buySilver" CommandArgument="silver" class="btn_next btn_next_silver"/></div>
            <div class="btnGold" onmouseover="document.images['objGold'].src=imgGoldHover.src"><asp:Button ID="btn_gold"  runat="server" Text="Buy" OnCommand="buyNow" CommandName="buyGold" CommandArgument="gold" class="btn_next"/></div>
            

        </div>
       
       


        <asp:HiddenField ID="Hidden_PpcPackage" runat="server" Value="PPC Package" />
    </div>
    
    </form>
</body>
</html>
