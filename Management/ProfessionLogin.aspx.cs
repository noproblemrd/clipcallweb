﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
//using Mantis.OneCall.Components;
//using Mantis.OneCall.Business;
//using Mantis.Data;
//using Mantis.Utilities;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Web.Services.Protocols;

public partial class Management_ProfessionLogin : PageSetting
{

    const string SCRAMBEL = "*5106sp";
    protected void Page_Load(object sender, EventArgs e)
    {
        
        
        if (!IsPostBack)
        {

           // if (Session["UserGuid"] != null)
       //     Session["UserGuid"] = null;

           if (!string.IsNullOrEmpty(siteSetting.GetSiteID))
           {              
              
               LoadTextLogin(siteSetting.GetSiteID);
               setTextLogin();              
           }
         
           if (Request.Cookies["professionalLogin"] != null)
            {
          //      txt_professionalEmail.Text = Request.Cookies["professionalLogin"].Values["professionalEmail"];
                string _password = Request.Cookies["professionalLogin"].Values["professionalPassword"];
                if (!string.IsNullOrEmpty(_password))
                {
                    try
                    {
                        _password = EncryptString.Decrypt_String(_password, SCRAMBEL);
                    }
                    catch (Exception exc) { }
                    string[] newPassword = _password.Split(';');
                    if (newPassword.Length == 2)
                    {
                        txt_professionalPassword.Attributes.Add("value", newPassword[0]);
                        txt_professionalEmail.Text = newPassword[1];
                    }
                }
            }
            if (Request.Cookies["rememberme"] != null && Request.Cookies["rememberme"].Value == "false")
                ifRememberMe.Checked = false;
        }
        lbl_version.DataBind();

    }
   
    private void setTextLogin()
    {
        string command = "SELECT dbo.GetTextLoginBySiteNameId(@siteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@siteNameId", siteSetting.GetSiteID);
            lbl_commercial.Text = (string)cmd.ExecuteScalar();
            conn.Close();
        }
    }
    
    private void LoadDateTimeFormat()
    {
        string command = "EXEC dbo.GetDateTimeFormatByUserId @UserId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@UserId", new Guid(userManagement.GetGuid));
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                if (!reader.IsDBNull(0))
                    siteSetting.DateFormat = (string)reader["DateFormat"];

                if (!reader.IsDBNull(1))
                    siteSetting.TimeFormat = (string)reader["TimeFormat"];
            }
            conn.Close();
        }
    }
    private void LoadTextLogin(string siteId)
    {
        lbl_commercial.Text = DBConnection.GetTextLogin(siteId);
    }
       
    protected void btnCheckCode_Click(object sender, EventArgs e)
    {
        if (siteSetting == null || string.IsNullOrEmpty(siteSetting.GetSiteID))
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            if (!DBConnection.LoadSiteId(host, this))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
        }
        string email = txt_professionalEmail.Text.Trim();
        string passWord = txt_professionalPassword.Text;
        
        //Check if try to access multi times

        bool IsApprove = DBConnection.IsLoginApproved(email, siteSetting.GetSiteID);
        
        if (!IsApprove)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "AccountLocked", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AccountLocked.Text) + "');", true);
            return;
        }
        /**********/


        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;        
        
        
        try
        {
            _response = supplier.UserLogin(email, passWord);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            lblComment.Text = "Failed";
            return;
        }
        if (_response.Type != WebReferenceSupplier.eResultType.Success)
        {
            lblComment.Text = "Failed";
            return;
        }
        switch (_response.Value.UserLoginStatus)
        {
            case(WebReferenceSupplier.eUserLoginStatus.NotFound):
                DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Supplier);
                ClientScript.RegisterStartupScript(this.GetType(), "UserLoginStatus", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NotFound.Text) + "');", true);
                return;
            case(WebReferenceSupplier.eUserLoginStatus.UserInactive):
                DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Supplier);
                ClientScript.RegisterStartupScript(this.GetType(), "UserLoginStatus", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_UserInactive.Text) + "');", true);
                return;
        }
        if (_response.Value.AffiliateOrigin != Guid.Empty)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Supplier);
            ClientScript.RegisterStartupScript(this.GetType(), "AffiliateUser", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AffiliateUser.Text) + "'); window.location='" + ResolveUrl("~") + "Affiliate/AffiliateLogin.aspx';", true);
            return;
        }
        
        if (_response.Value.SecurityLevel > 0)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Supplier);
            ClientScript.RegisterStartupScript(this.GetType(), "PublisherUser", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_PublisherUser.Text) + "'); window.location='" + ResolveUrl("~") + "Publisher/PublisherLogin.aspx';", true);
           return;
        }
        DBConnection.InsertLogin(email, _response.Value.UserId, siteSetting.GetSiteID, eUserType.Supplier);
        bool RegistrationStage = _response.Value.StageInRegistration > 5;
        userManagement = new UserMangement(_response.Value.UserId.ToString(), _response.Value.Name, (double)_response.Value.Balance,
            RegistrationStage, email);
        userManagement.SetUserObject(this);
        int sitelangid = DBConnection.GetSiteLangIdByUserId(new Guid(userManagement.GetGuid), siteSetting.GetSiteID);
        if (sitelangid > 0)
        {
            siteSetting.siteLangId = sitelangid;
            Response.Cookies["language"].Value = sitelangid.ToString();
            Response.Cookies["language"].Expires = DateTime.Now.AddMonths(3);
        }
        
        LoadDateTimeFormat();
        if (ifRememberMe.Checked)
        {
            string newPassword = passWord + ";" + email;
            newPassword = EncryptString.Encrypt_String(newPassword, SCRAMBEL);
     //       Response.Cookies["professionalLogin"]["professionalEmail"] = email;
            Response.Cookies["professionalLogin"]["professionalPassword"] = newPassword;
            Response.Cookies["professionalLogin"].Expires = DateTime.Now.AddMonths(3);
            Response.Cookies["professionalLogin"].HttpOnly = true;
            Response.Cookies["rememberme"].Values.Clear();
        }
        else
        {
            Response.Cookies["professionalLogin"].Values.Clear();
            Response.Cookies["rememberme"].Value = "false";
            Response.Cookies["rememberme"].Expires = DateTime.Now.AddMonths(3);
        }
        Response.Cookies["language"].Value = "" + 0;
        Response.Cookies["language"].Expires = DateTime.Now.AddMonths(3);
        /*
        if (Heading.GetCurrent()!= null && Heading.GetCurrent().SiteId == siteSetting.GetSiteID)
        {
            Response.Redirect(ResolveUrl("~") + "PPC/FramePpc.aspx");
        }
        else
         * */

        string UrlReferrer = (Request.Cookies["referrer"] == null) ? string.Empty : Request.Cookies["referrer"].Value;
        HttpCookie _cookie = new HttpCookie("referrer");
        _cookie.Value = "";
        _cookie.Expires = DateTime.Now.AddDays(-2);
        Response.Cookies.Add(_cookie);
        if (!string.IsNullOrEmpty(UrlReferrer))
            Response.Redirect(UrlReferrer);
        else
            Response.Redirect("frame.aspx");
    }
    
    
}
