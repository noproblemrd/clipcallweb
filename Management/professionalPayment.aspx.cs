using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web.Services.Protocols;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Linq;

public partial class Professional_professionalPayment : PageSetting
{
   protected List<string> list;
   protected string CreditLinkparams = "";
   protected int minDepodit
   {
       get { return (ViewState["minDepodit"] == null) ? 0 : (int)ViewState["minDepodit"]; }
       set { ViewState["minDepodit"] = value; }
   }
   protected string _Currency
   {
       get { return siteSetting.CurrencySymbol; }
   }
   protected string GetMinDepositValidation
   {
       get { return lbl_MinDepositMessage.Text + " " + minDepodit; }
   }
   protected void Page_Load(object sender, EventArgs e)
    {
       if(!userManagement.IsSupplier())
           Response.Redirect("LogOut.aspx");
 //       ToolkitScriptManager1.RegisterAsyncPostBackControl(btn_buyNow);
        ToolkitScriptManager1.RegisterAsyncPostBackControl(btn_AutoRecharge);
        ToolkitScriptManager1.RegisterAsyncPostBackControl(btn_SetInvoiceOption);
        txt_deposite.Attributes.Add("onkeyup", "calculateBalance();");
        txt_AddDeposite.Attributes.Add("readonly", "readonly");
        txt_NewBalance.Attributes.Add("readonly", "readonly");
        rbl_PaymentMethod.Attributes.Add("onclick", "javascript:PaymentMethod(event);");
        CreditLinkparams = "suppllierId=" + userManagement.GetGuid + "&SiteId=" + siteSetting.GetSiteID;
        if (!IsPostBack)
        {
            /*
            if (siteSetting.IfPaymentCharging == true)
            {
                
                returnPayPal = "http://" + Request.Url.Host + ":"
                    + Request.Url.Port + ResolveUrl("~") +
                    "Management/professionLogin.aspx";                
            }
            PageUrl = "http://" + Request.Url.Host + ":" + Request.Url.Port + ResolveUrl("~");
            */
            //Set step register
            string csnameStep = "setStep";
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), csnameStep))
            {               
                string csTextStep = "witchAlreadyStep(6);";
                ClientScript.RegisterStartupScript(this.GetType(), csnameStep, csTextStep, true);
            }
            //set load site format
           
            LoadPaymentMethods();                
      //      LoadSymbolMoney();         
            
            LoadCreditCardYears();
            LoadCreditCardMonths();

            LoadIncidentPackages();
            LoadDetail();
         //   LoadChargingCompanyDetails();
            BindTextToPage();
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
        }
        string script = "setArrs('" + percentsV + "','" + pricesV + "');";
        ClientScript.RegisterStartupScript(this.GetType(), "loadval", script, true);
    //    SetRblInvoiceOption();
        form1.DataBind();
       
        
    }

  
  

   private void BindTextToPage()
   {
       this.DataBind();
       lbl_page.Text = " 6";
       lbl_HowTransfer.Text = DBConnection.GetTextPaymentTransfer(siteSetting.GetSiteID);
       string CreditCardText = DBConnection.GetTextCreditCard(siteSetting.GetSiteID);
       if (string.IsNullOrEmpty(CreditCardText))
           div_CreditCardText.Visible = false;
       else
       {
           div_CreditCardText.Visible = true;
           div_CreditCardText.InnerHtml = CreditCardText;
       }
       
   }
 
    private void LoadCreditCardYears()
    {
        int year = DateTime.Now.Year;
        for (int i = 0; i < 10; i++)
        {
            int NewYear = year + i;
            ddl_expiryear.Items.Add(new ListItem(NewYear.ToString(), NewYear.ToString().Substring(2, 2)));
        }
    }

   
    private void LoadCreditCardMonths()
    {
        int month = DateTime.Now.Month;

        ListItem li;
        string strMonth = "";

        for (int i = 1; i <= 12; i++)
        {
            if (i <= 9)
                strMonth = "0" + i.ToString();
            else
                strMonth = i.ToString();

            li = new ListItem(strMonth);
            if (month == i)
                li.Selected = true;
            ddl_expirymonth.Items.Add(li);
        }

    }

    private void PaymentMethodProblems()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "NoPaymentMethod", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_PaymentMethodProblems.Text) + "');" +
                "window.location='professionalCallPurchase.aspx';", true);
    }
    private void LoadCreditCardMethod()
    {
        if (!siteSetting.IfPaymentCharging && !siteSetting.IfEmailCharging)
        {
            PaymentMethodProblems();
            return;
        }
        lbl_HowTransfer.Attributes["style"] = "display:none;";
        if (siteSetting.IfPaymentCharging == true)
        {
            // inside div_CreditCard
            divCreditcard.Visible = false; // credit card images


        }
        else                 
            divCreditcard.Visible = true;
        

        div_CreditCard.Attributes["style"] = "display:inline";
    }
    private void LoadWireTransferMethod()
    {
        if (siteSetting.IfPaymentCharging && siteSetting.CharchingCompany == eCompanyCharging.platnosci)
        {
            ListItem liChooseBank = new ListItem();
            liChooseBank.Text = Hidden_ChooseBank.Value;
            liChooseBank.Value = "";
            DropDownListPlatnosciPayTypes.Items.Insert(0, liChooseBank);
            platnosciWireTranfer.Visible = true;
            lbl_HowTransfer.Visible = false;
        }
        else
        {
            lbl_HowTransfer.Visible = true;
            platnosciWireTranfer.Visible = false;
        }
        
        
    }
    private void LoadPaymentMethods()
    {
        list = new List<string>();
        string command = "EXEC dbo.GetPaymentMethodsBySiteNameId @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string _value = (string)reader["PaymentName"];
                rbl_PaymentMethod.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "ePaymentMethodSIte", _value), _value));

                list.Add(_value);
            }
            conn.Close();
        }
        if (list.Count == 0)
        {
            PaymentMethodProblems();
            return;
        }
        if (list.Count > 1)
        {
            
            foreach (ListItem li in rbl_PaymentMethod.Items)
            {
                if (li.Value == ePaymentMethodSIte.CreditCard.ToString())
                {
                    li.Selected = true;
                    break;
                }
            }
            LoadCreditCardMethod();
            LoadWireTransferMethod();
 
        }
        else // only one payment method
        {
            _PanelRadioChoose.Visible = false;
            if (list[0] == ePaymentMethodSIte.CreditCard.ToString()) // credir car payment method
            {
                _PanelPaymentTransfer.Visible = false; // the description of payment transfer

                LoadCreditCardMethod();
            }
            else // wire tranfer payment method
            {
                div_CreditCard.Visible = false;
                _PanelPaymentTransfer.Visible = true;

                if (siteSetting.IfPaymentCharging == true) // supplier who has charging company
                {
                    _PanelDeposit.Visible = true;
                    btn_buyNow.Visible = true;
                }

                else // supplier who does not have charging company
                {
                    _PanelDeposit.Visible = false;
                    btn_buyNow.Visible = false;
                }
                LoadWireTransferMethod();
               
            }
        }
        if (rbl_PaymentMethod.SelectedIndex == -1)
            rbl_PaymentMethod.SelectedIndex = 0;
        
    }
    private void LoadIncidentPackages()
    {
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("PaidAmount");
        data.Columns.Add("FreeAmount");
        data.Columns.Add("image");
   
        SortedDictionary<int, int> pricePercentList = new SortedDictionary<int, int>();


        try
        {
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            WebReferenceSupplier.GetSupplierChargingDataRequest request = new WebReferenceSupplier.GetSupplierChargingDataRequest();
            request.SupplierId = userManagement.Get_Guid;

            WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result = supplier.GetSupplierChargingData(request);
            
            if (result.Type == WebReferenceSupplier.eResultType.Success)
            {
                WebReferenceSupplier.PricingPackageData[] packages = result.Value.Packages;
               
                 

                int indx = 0;


                if (list.Count == 1 && list[0] == ePaymentMethodSIte.WireTransfer.ToString() && packages.Length == 0)
                {
                    div_calc.Visible = false;
                    btn_buyNow.Visible = false;
                    //       return;
                }
                else
                {


                    foreach (WebReferenceSupplier.PricingPackageData package in packages)
                    {
                        int PaidAmount = package.FromAmount;
                        int FreeAmount = package.BonusPercent;
                        DataRow row = data.NewRow();
                        row["Name"] = package.Name;
                        row["PaidAmount"] = PaidAmount;
                        row["FreeAmount"] = FreeAmount;
                        switch (indx % 3)
                        {
                            case (0): row["image"] = "images/package-p.png";
                                break;
                            case (1): row["image"] = "images/package-g.png";
                                break;
                            case (2): row["image"] = "images/package-s.png";
                                break;
                        }
                        indx++;
                        data.Rows.Add(row);
                        if (!pricePercentList.ContainsKey(PaidAmount))
                            pricePercentList.Add(PaidAmount, FreeAmount);
                    }
                    div_Packages_Pricing.Visible = true;
                    div_Payment_Image.Visible = false;
                    if (data.Rows.Count > 0)
                        lbl_SpecialOffers.Visible = true;
                    else
                    {
                        string payment_image = DBConnection.GetPaymentImage(siteSetting.GetSiteID);
                        if (!string.IsNullOrEmpty(payment_image))
                        {
                            div_Packages_Pricing.Visible = false;
                            div_Payment_Image.Visible = true;
                            img_Payment.ImageUrl = payment_image;
                        }
                        tr_AddDeposite.Style.Add(HtmlTextWriterStyle.Display, "none");
                    }


                    DataListPackege.DataSource = data;
                    DataListPackege.DataBind();

                    string PercentToJavaScript;

                    pricesV = PriceToJavaScript(pricePercentList, out PercentToJavaScript);
                    percentsV = PercentToJavaScript;
                }
                //Load Invoice Options
                div_InvoiceAddress.Visible = result.Value.InvoiceSendOptionsEnabled;
                if (result.Value.InvoiceSendOptionsEnabled)
                {
                    hf_InvoiceOptions.Value = result.Value.InvoiceSendOption.ToString();
                    txt_MailAddress.Text = string.IsNullOrEmpty(result.Value.InvoiceSendAddress) ?
                        result.Value.Email : result.Value.InvoiceSendAddress;
                    hf_InvoiceValue.Value = result.Value.InvoiceSendOption.ToString() + ";;;" + txt_MailAddress.Text;
                    if (result.Value.InvoiceSendOption == WebReferenceSupplier.InvoiceSendOption.Email)
                    {
                        rbl_InvoiceOption.SelectedIndex = rbl_InvoiceOption.Items.IndexOf(rbl_InvoiceOption.Items.FindByValue(WebReferenceSupplier.InvoiceSendOption.Email.ToString()));
                    }
                    else
                    {
                        rbl_InvoiceOption.SelectedIndex = rbl_InvoiceOption.Items.IndexOf(rbl_InvoiceOption.Items.FindByValue(WebReferenceSupplier.InvoiceSendOption.Mail.ToString()));
                    }
                }
            }
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
       
    }
    int SavePercent(int amount, int free)
    {
        double temp = (100.0 * (double)free) / (double)amount;
        return (int)Math.Round(temp);
    }
    string PriceToJavaScript(SortedDictionary<int, int> list, out string PercentToJavaScript)
    {
        string resultPrice = "";
        PercentToJavaScript = "";
        foreach (KeyValuePair<int, int> kvp in list)
        {
            resultPrice += kvp.Key.ToString() + ";";
            PercentToJavaScript += kvp.Value.ToString() + ";";
        }

        if (string.IsNullOrEmpty(resultPrice))
        {
            PercentToJavaScript = "0";
            return "0";
        }
        PercentToJavaScript = PercentToJavaScript.Substring(0, PercentToJavaScript.Length - 1);
        return resultPrice.Substring(0, resultPrice.Length - 1);
    }
   
    void LoadDetail()
    {
        try
        {
            
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            
            WebReferenceSupplier.GetSupplierChargingDataRequest request=new WebReferenceSupplier.GetSupplierChargingDataRequest();
            request.SupplierId= userManagement.Get_Guid;
           
            WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result = supplier.GetSupplierChargingData(request);

            if (result.Type == WebReferenceSupplier.eResultType.Success)
            {
                WebReferenceSupplier.GetSupplierChargingDataResponse value = result.Value;
                if (!value.CanChargeByFundsStatus)
                {
                    btn_AutoRecharge.Visible = false;
                    btn_buyNow.Visible = false;
                    lbl_CanMakeSale.Visible = true;
                    CanMakeSale = false;
                }
                else
                    CanMakeSale = true;
                //inside value is all the data...
                CurrenBalance = value.CurrentBalance;
                SupplierBizId = value.SupplierBizId;
                 /*
                CompanyName = value.CompanyName;
                
                FirstName = value.FirstName;
                SecondName = value.LastName;
                CompanyId = value.CompanyId;
                Email = value.Email;
                PhoneNumber = value.PhoneNumber;
                
                Country = value.AddressCountry;
                State = value.AddressState;
                County = value.AddressCounty;
                City=value.AddressCity;
                Street = value.AddressStreet;
                HouseNumber = value.AddressHouseNumber;
                PostalCode = value.AddressPostalCode;
                Currency = value.Currency;
                //Response.Write("Currency:" + Currency);
                OtherPhone = value.OtherPhone;
                WebSite = value.WebSite;
                Fax = value.Fax;
                CreditCardToken = value.CreditCardToken;
                Last4DigitsCC = value.Last4DigitsCC;
                CardId = value.CreditCardOwnerId;
                AccountNumber = value.AccountNumber;
                ArrNumberPayments = value.PaymentsTable;
                  * */
                vatV = value.Vat;
                /******* CreditCardToken ***********/
                
                minDepodit = (value.MinDepositPrice > value.MaxIncidentPrice) ? (int)value.MinDepositPrice : (int)value.MaxIncidentPrice;

                int extra_bonus = value.RechargeBonusPercent;// string.IsNullOrEmpty(extra_bonus) ? "0" : extra_bonus;
                lbl_Title_AutoRecharge.Text = lbl_Title_AutoRecharge1.Text + " " + extra_bonus + "% " + lbl_Title_AutoRecharge2.Text;
                hf_ExtraBonus.Value = extra_bonus + "";

                int Register_Stage = value.StageInRegistration;
                //Response.Write(Register_Stage);
                if (Register_Stage < 5)
                {
                    string script = "parent.setInitRegistrationStep(" + Register_Stage + ");";
                    ClientScript.RegisterStartupScript(this.GetType(), "GoToStep", script, true);
                    IsFirstTimeV = true;
                    //return;                   
                }
                else if (Register_Stage == 5)
                    IsFirstTimeV = true;
                else
                    IsFirstTimeV = false;


                bool isRegisterSage = (Register_Stage == 5);

                if (siteSetting.GetSiteID != PpcSite.GetCurrent().ZapSiteId && value.RechargeEnabled && !string.IsNullOrEmpty(value.CreditCardToken))
                {
                    /*
                    if (string.IsNullOrEmpty(value.CreditCardToken))                    
                        btn_AutoRecharge.Visible = false;                    
                    else
                        btn_AutoRecharge.Visible = true;
                    */
                    div_AutoRecharge.Visible = true;
                    if (value.IsAutoRenew)
                    {
                        int _num = (int)value.RechargeAmount;
                        int _extra_bonus = int.Parse(hf_ExtraBonus.Value);
                        int _bonus = GetBonusPackage(_num);

                        _bonus += _extra_bonus;
                        txt_ExtraBonus.Text = (((_num * _bonus) / 100)) + "";
                        lbl_GetExtraTotalPercent.Text = _bonus + "";
                        //      txt_GetExtraRecharge.Text = (((_num * _bonus) / 100) + _num) + "";
                        cb_AutoRecharge.Checked = true;
                        txt_RechargeAmount.Attributes.Remove("readOnly");
                        txt_RechargeAmount.Text = value.RechargeAmount + "";
                        hf_IsRechargeMarked.Value = "true";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "RemoveAddReachargedLabels", "RemoveAddReachargedLabels(true);", true);
                    }
                    else
                    {
                        txt_RechargeAmount.Attributes.Remove("readOnly");
                        cb_AutoRecharge.Checked = IsFirstTimeV;
                        hf_IsRechargeMarked.Value = (IsFirstTimeV) ? "" : "false";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "RemoveAddReachargedLabels", "RemoveAddReachargedLabels(false);", true);
                    }
                }
                else
                {
                    div_AutoRecharge.Visible = false;
                    btn_AutoRecharge.Visible = false;
                }
                
                _PanelStep.Visible = isRegisterSage;
                if (isRegisterSage)
                    ClientScript.RegisterStartupScript(this.GetType(), "focusTxtBox", "focusTxtBox();", true);
                lbl_CurrentBalance.Text = String.Format("{0:#,0.##}", value.CurrentBalance);

                string _script = "parent.SetBalance('" + String.Format("{0:#,0.##}", value.CurrentBalance) + "');";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetBalance", _script, true);

                _UpdatePanelBalance.Update();
                _UpdatePanelDeposite.Update();
            }

            else
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add(lbl_YourBalance.ID, String.Format("{0:#,0.##}", CurrenBalance));
        dicValueV = dic;
       
    }
    private int GetBonusPackage(int num)
    {
        string[] percents = percentsV.Split(';');
        string[] values = pricesV.Split(';');
        for (int i = 0; i < percents.Length; i++)
        {
            int _value = int.Parse(values[i]);
            if (num < _value)
            {
                if (i == 0)
                    return 0;
                return int.Parse(percents[i - 1]);
            }
        }
        return int.Parse(percents[percents.Length - 1]);
    }
    /*
    private void LoadChargingCompanyDetails()
    {


        if (siteSetting.CharchingCompany != eCompanyCharging.none)
        {
            switch (siteSetting.CharchingCompany)
            {              
                case (eCompanyCharging.pelecard):
                {                    
                    string command = "EXEC dbo.GetChargingCompanyDetails @company";
                    SqlConnection conn = DBConnection.GetConnString();
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(command, conn);
                    cmd.Parameters.AddWithValue("@company", siteSetting.CharchingCompany);
                    SqlDataReader reader = cmd.ExecuteReader();
                  
                    if (reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            switch ((string)reader["item"])
                            {
                                case "userName" :
                                    PeleCardUserName=(string)reader["value"];
                                    break;

                                case "password" :
                                    PeleCardPassword=(string)reader["value"];
                                    break;

                                case "termNo":
                                    PeleCardTermNo=(string)reader["value"];
                                    break;

                                case "shopNo":
                                    PelecardShopNo=(string)reader["value"];
                                    break;

                                default:
                                    break;
                            }
                            
                        }

                    }

                    reader.Close();
                    conn.Close();
                       
                    break;

                }

                default:
                    break;

            }
        }

    }
    */
    void ClearNLoad()
    {
        txt_deposite.Text = "";
        txt_AddDeposite.Text = "";
        txt_NewBalance.Text = "";
        txt_TotalIncludeVAT.Text = "";
        LoadDetail();
    }
    private void buyNow_CreditCard(decimal _deposit)
    {
   //     bool is_Valid = true;
        List<Supplier_Pricing_Component> list_spc = new List<Supplier_Pricing_Component>();
        if (_deposit < minDepodit)
        {
   //         is_Valid = false;
            _PanelInvalidDeposit.Attributes["style"] = "visibility:visible";
            return;
        }
        else
            _PanelInvalidDeposit.Attributes["style"] = "visibility:hidden";
        string bonusType;
        decimal _BonusDeposite;
        if (!decimal.TryParse(txt_AddDeposite.Text, out _BonusDeposite))
            _BonusDeposite = 0;
        if (_BonusDeposite <= 0)
        {
            bonusType = WebReferenceSupplier.BonusType.None.ToString();
            _BonusDeposite = 0;
        }
        else
        {
            bonusType = WebReferenceSupplier.BonusType.Package.ToString();
            Supplier_Pricing_Component _spc = new Supplier_Pricing_Component();
            _spc.Amount = _BonusDeposite;
            _spc.BaseAmount = 0;
            _spc.PaymentType = ePaymentType.Bonus;
            _spc.comment = bonusType;
            list_spc.Add(_spc);
        }

     //   decimal _new_balance = _deposit + _BonusDeposite;
        
        Supplier_Pricing_Component spc = new Supplier_Pricing_Component();
        spc.Amount = _deposit;
        spc.BaseAmount = 0;
        spc.PaymentType = ePaymentType.Manual;
        list_spc.Add(spc);

        decimal NewBalance = CurrenBalance + _deposit + _BonusDeposite;
        int PaymentId = DpzUtilities.SetPayment(list_spc, siteSetting.GetSiteID, _deposit, ePaymentMethod.CreditCard,
            new Guid(GetGuidSetting()), SupplierBizId, false, userManagement.Email, (int)NewBalance, (int)CurrenBalance, false);
        if (PaymentId <= 0)
        {
            Update_Faild();
            return;
        }
        //    ClientScript.RegisterClientScriptBlock(this.GetType(), "redirect", "window.location='" + ResolveUrl("~/Publisher/ZapCreditCard.aspx?PaymentId=" + PaymentId.ToString()") + "'", true);
        Response.Redirect(ResolveUrl("~/Publisher/ZapCreditCard.aspx?PaymentId=" + PaymentId.ToString()));

        /*
        if (!siteSetting.IfPaymentCharging)
        {
            int resultCardVerification;
    //        long resultCardNumber;

            bool is_Valid = true;
            lbl_InvalidName.Visible = false;
            lbl_InvalidCreditCardNum.Visible = false;
            lbl_InvalidCreditCode.Visible = false;
            lbl_InvalidId.Visible = false;
            lbl_InvalidCreditMonth.Visible = false;
            
            if (_deposit < minDepodit)
            {
                is_Valid = false;
                _PanelInvalidDeposit.Attributes["style"] = "visibility:visible";
            }
            else
                _PanelInvalidDeposit.Attributes["style"] = "visibility:hidden";

            if (string.IsNullOrEmpty(tb_nameCard.Text))
            {
                is_Valid = false;
                lbl_InvalidName.Visible = true;
            }

            if (string.IsNullOrEmpty(tb_creditNumber.Text))
            {
                is_Valid = false;
                lbl_InvalidCreditCardNum.Visible = true;
            }
            else// if (!long.TryParse(tb_creditNumber.Text, out resultCardNumber))
            {
                string _creditCardNumber = tb_creditNumber.Text;
                Regex objNotPositivePattern = new Regex("^\\.{3,3}[0-9]{4,4}$"); // example ...5677

                if (!objNotPositivePattern.IsMatch(_creditCardNumber))
                {
                    is_Valid = false;
                    lbl_InvalidCreditCardNum.Visible = true;
                }
            }

            if (!int.TryParse(tb_cardVerification.Text, out resultCardVerification) || tb_cardVerification.Text.Trim().Length > 4)
            {
                is_Valid = false;
                lbl_InvalidCreditCode.Visible = true;
            }



            if (tb_id.Text.Trim().Length == 0 || tb_id.Text.Trim().Length > 10)
            {
                is_Valid = false;
                lbl_InvalidId.Visible = true;
            }

            int monthNow = DateTime.Now.Month;
            int monthChosen = Convert.ToInt32(ddl_expirymonth.SelectedItem.Text);

            int yearNow = DateTime.Now.Year;
            int yearChosen = Convert.ToInt32(ddl_expiryear.SelectedItem.Text);

            if (monthChosen < monthNow && yearChosen <= yearNow)
            {
                is_Valid = false;
                lbl_InvalidCreditMonth.Visible = true;
            }

            _UpdatePanelCreditCard.Update();

            if (!is_Valid)
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "wrongDetails"))
                {
                    string _script = "alert('" + HttpUtility.JavaScriptStringEncode(Hidden_WrongDetails.Text) + "');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "wrongDetails", _script, true);
                }
                return false;
            }



        }
        LogEdit.SaveLog("Accounting_" + siteSetting.siteLangId, "\r\n$$$$$$$$$$ start " + CompanyName + " $$$$$$$$$$");

        if (siteSetting.IfEmailCharging == true && siteSetting.IfPaymentCharging==false)
        {
            try
            {
                MailSender mailSender = new MailSender();

                /************ first email send to company ************ /
                mailSender.From = "Billing@noproblem.co.il";
                //mailSender.From = "flairman@walla.co.il";
                mailSender.To = "Billing@noproblem.co.il";
                //mailSender.To = "Billing@ppcnp.com";

                //mailSender.To = "shaimark@yahoo.com"; 
                //mailSender.Bcc = "shaimark@yahoo.com";

                mailSender.FromName = "NO PROBLEM billing";
                mailSender.IsText = true;
                mailSender.Subject = "Date:" + DateTime.Now.ToString() +
                    " Site URL:" + Request.Url.Host;
                string body = Hidden_EmailCompany.Value + "\n" +
                    "Date: " + DateTime.Now.ToString() + "\n" +
                    "Site: " + Request.Url.Host + "\n" +
                    "SiteId: " + siteSetting.GetSiteID + "\n" +
                    "Company name: " + HttpContext.Current.Server.HtmlDecode(CompanyName) + "\n" +
                    "Company Number: " + CompanyId + "\n" +
                    "Phone: " + PhoneNumber + "\n" +
                    "Country: " + Country + "\n" +
                    "State: " + State + "\n" +
                    "County: " + County + "\n" +
                    "Street: " + Street + "\n" +
                    "House Number: " + HouseNumber + "\n" +
                    "Postal Code: " + PostalCode + "\n" +
                    "Current Balance: " + CurrenBalance + " " + Currency + "\n" +
                    "I Want To Deposit: " + _deposit + " " + Currency + "\n" +
                    "Bonus: " + txt_AddDeposite.Text + " " + Currency + "\n" +
                    "New Balance: " + txt_NewBalance.Text + " " + Currency + "\n" +
                    "Identity Card Number: " + tb_id.Text + "\n" +
                    "Name On Credit Card: " + tb_nameCard.Text + "\n" +                            
                    "Credit Card Number: " + tb_creditNumber.Text + "\n" +
                    "Expiration Date: " + ddl_expirymonth.SelectedValue + " " + ddl_expiryear.SelectedValue + "\n" +
                    "CVV2: " + tb_cardVerification.Text + "\n";



                mailSender.Body = body;
                mailSender.RemoteHost = "127.0.0.1";
                bool ifOkSentNoProblemBilling=mailSender.Send();

                if (ifOkSentNoProblemBilling)
                {
                    LogEdit.SaveLog("Accounting_" + siteSetting.GetSiteID, "Email: Sent to  noproblem\r\nsuccess");

                }

                else
                {
                    LogEdit.SaveLog("Accounting+" + siteSetting.GetSiteID, "Email: Sent to  noproblem\r\nfailed");

                }

                /*************** second email send to supplier ********* /
                mailSender.From = "Billing@noproblem.co.il";
                //mailSender.From = "flairman@walla.co.il";
                mailSender.To = Email;
                //mailSender.To = "shaimark@yahoo.com"; 


                mailSender.FromName = "NO PROBLEM billing";
                mailSender.IsText = true;
                mailSender.Subject = "Date:" + DateTime.Now.ToString() +
                    " Site URL:" + Request.Url.Host;
                body = Hidden_EmailSupllier.Value + "\n" +
                    "Date: " + DateTime.Now.ToString() + "\n" +
                    "Site: " + Request.Url.Host + "\n" +
                    "SiteId: " + siteSetting.GetSiteID + "\n" +
                    "Company name: " + HttpContext.Current.Server.HtmlDecode(CompanyName)+ "\n" +
                    "Company Number: " + CompanyId + "\n" +
                    "Phone: " + PhoneNumber + "\n" +
                    "Country: " + Country + "\n" +
                    "State: " + State + "\n" +
                    "County: " + County + "\n" +
                    "Street: " + Street + "\n" +
                    "House Number: " + HouseNumber + "\n" +
                    "Postal Code: " + PostalCode + "\n" +
                    "Current Balance: " + CurrenBalance + " " + Currency + "\n" +
                    "I Want To Deposit: " + _deposit + " " + Currency + "\n" +
                    "Bonus: " + txt_AddDeposite.Text + " " + Currency + "\n" +
                    "New Balance: " + txt_NewBalance.Text + " " + Currency + "\n" +
                    "Name On Credit Card: " + tb_nameCard.Text + "\n" +
                    "Credit Card Number: xxxxxxxxxxxx" + tb_creditNumber.Text.Substring(tb_creditNumber.Text.Length - 4, 4) + "\n" +
                    "Expiration Date: " + ddl_expirymonth.SelectedValue + " " + ddl_expiryear.SelectedValue + "\n";




                mailSender.Body = body;
                mailSender.RemoteHost = "127.0.0.1";
                bool ifOkSentMailSupllier = mailSender.Send();

                if (ifOkSentMailSupllier)
                {
                    //if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "hideSendButton"))
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "hideSendButton", "hideSendButton();", true); // in the function also show positive sent message
                    LogEdit.SaveLog("Accounting_" + siteSetting.GetSiteID, "Email: Sent to  " + CompanyName + "\r\nsuccess");

                }

                else
                {
                    LogEdit.SaveLog("Accounting_" + siteSetting.GetSiteID, "Email: Sent to  " + CompanyName + "\r\nfailed");

                }

                if (siteSetting.IfPaymentCharging == false)
                {
                    if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "UpdateSuccess"))
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
                }

                return true;
            }

            catch(Exception ex)
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                LogEdit.SaveLog("Accounting_" + siteSetting.GetSiteID, "Email: Sent to  " + CompanyName + "\r\nfailed " + ex.Message);

                return false;
            }
        }
        string bonusType;
        int _BonusDeposite;
        if (!int.TryParse(txt_AddDeposite.Text, out _BonusDeposite))
            _BonusDeposite = 0;
        if (_BonusDeposite <= 0)
        {
            bonusType = WebReferenceSupplier.BonusType.None.ToString();
            _BonusDeposite = 0;
        }
        else
            bonusType = WebReferenceSupplier.BonusType.Package.ToString();

        int _new_balance = _deposit + _BonusDeposite;

        string __form = string.Empty;

        if (siteSetting.CharchingCompany == eCompanyCharging.pelecard)
        {
            Session["returnToSite"] = "http://" + Request.Url.Host + ":"
                + Request.Url.Port + ResolveUrl("~") +
                "Management/frame.aspx";

            int RechargeAmount = -1;
            if (cb_AutoRecharge.Visible && cb_AutoRecharge.Checked)
                int.TryParse(txt_RechargeAmount.Text, out RechargeAmount);
            bool ToRecharge = RechargeAmount > 0;
             /*       
            string command = "EXEC dbo._SetPayment @site_id ,@company, @advertiser, @details, @RechargeAmount";
            SqlConnection conn = DBConnection.GetConnString();
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@site_id", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@company", siteSetting.CharchingCompany.ToString());
            cmd.Parameters.AddWithValue("@advertiser", CompanyName);
            if (RechargeAmount == -1)
                cmd.Parameters.AddWithValue("@RechargeAmount", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@RechargeAmount", RechargeAmount);
            string paymentPelecardDetails = "";

      //      int depositAgorot = int.Parse(deposit) * 100;
            

            
                                     

      

            cmd.Parameters.AddWithValue("@details", paymentPelecardDetails);

            string auto_id = Convert.ToString(cmd.ExecuteScalar());
            * /
            string paymentPelecardDetails = null;
            decimal PelecardAmount;// = _deposit * 100 * ((vatV + 100m) / 100m);
            if (PpcSite.GetCurrent().UserZap == "NoProblemInvoicesProd")
            {
                PelecardAmount = _deposit * 100;
                paymentPelecardDetails = "supplierGuid=" + userManagement.GetGuid +
                    "&supplierId=" + CompanyId +
                    "&advertiserName=" + Server.UrlEncode(CompanyName) +
                    "&balanceOld=" + dicValueV[lbl_YourBalance.ID] +
                    "&userId=" + userManagement.GetGuid +
                    "&userName=" + Server.UrlEncode(userManagement.User_Name) +
                    "&siteId=" + siteSetting.GetSiteID +
                    "&siteLangId=" + siteSetting.siteLangId +
                    "&fieldId=" + lbl_YourBalance.ID +
                    "&fieldName=" + Server.UrlEncode(lbl_YourBalance.Text) +
                    "&isFirstTimeV=" + IsFirstTimeV.ToString() +
                    "&bonusAmount=" + Int32.Parse(txt_AddDeposite.Text) +
                    "&balanceNew=" + _new_balance +
                    "&email=" + Server.UrlEncode(Email) +
                    "&phoneNumber=" + PhoneNumber +
                    "&otherPhone=" + OtherPhone +
                    "&country=" + Country +
                    "&city=" + City +
                    "&streetNumber=" + Server.UrlEncode(StreetNumber) +
                    "&postalCode=" + PostalCode +
                    "&webSite=" + Server.UrlEncode(WebSite) +
                    "&vat=" + siteSetting.Vat +
                    "&ifAccountingCharging=" + Convert.ToString(siteSetting.IfAccountingCharging) +
                    "&ppcPackage=" + Server.UrlEncode(Hidden_PpcPackage.Value) +
                    "&currency=" + Currency +
                    "&accountNumber=" + AccountNumber +
                    "&fax=" + Fax + "&amount=" + _deposit + "&bonusType=" + bonusType;
            }
            else
                PelecardAmount = _deposit * 100;// _deposit * 100 * ((vatV + 100m) / 100m);
            int auto_id = DpzUtilities.SetPayment(null, siteSetting.GetSiteID, _deposit, WebReferenceSupplier.eSupplierPricingComponentType.Manual, userManagement.Get_Guid, SupplierBizId, _BonusDeposite, RechargeAmount, ToRecharge, paymentPelecardDetails);

            int tempNumberOfPayments = 3;

            for (int i = 0; i < ArrNumberPayments.Length; i++)
            {
                WebReferenceSupplier.PaymentsKeyValuePair paymentKeyValuePair = ArrNumberPayments[i];
                if (_deposit >= paymentKeyValuePair.FromPrice)
                {
                    tempNumberOfPayments = paymentKeyValuePair.NumberOfPayments;
                }
            }
            
            Session["pelecardDepositAgorot"] = _deposit * 100;
            Session["pelecardAuto_id"] = auto_id;
            Session["peleCardUserName"] = PeleCardUserName;
            Session["peleCardPassword"] = PeleCardPassword;
            Session["peleCardTermNo"] = PeleCardTermNo;
            Session["pelecardShopNo"] = PelecardShopNo;
            Session["pelecardPageUrl"] = PageUrl;

            if (PpcSite.GetCurrent().UserZap == "NoProblemInvoicesProd")
                __form = CompanyCharging.GetPelecardForm(this, (int)PelecardAmount, auto_id.ToString(), tempNumberOfPayments);
            else
                __form = CompanyCharging.GetPelecardFormQA(this, (int)PelecardAmount, auto_id.ToString(), tempNumberOfPayments);
           
            
        }

        else if (siteSetting.CharchingCompany == eCompanyCharging.plimus)
        {
            int RechargeAmount = -1;
            if (cb_AutoRecharge.Visible && cb_AutoRecharge.Checked)
                int.TryParse(txt_RechargeAmount.Text, out RechargeAmount);

            string command = "EXEC dbo._SetPayment @site_id ,@company, @advertiser, @details, @RechargeAmount";
            SqlConnection conn = DBConnection.GetConnString();
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@site_id", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@company", "plimus");
            cmd.Parameters.AddWithValue("@advertiser", CompanyName);
            if (RechargeAmount == -1)
                cmd.Parameters.AddWithValue("@RechargeAmount", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@RechargeAmount", RechargeAmount);
            string paymentPelecardDetails = "";

            //      int depositAgorot = int.Parse(deposit) * 100;


            paymentPelecardDetails = "supplierGuid=" + userManagement.GetGuid +
                "&supplierId=" + CompanyId +
                "&advertiserName=" + Server.UrlEncode(CompanyName) +
                "&balanceOld=" + dicValueV[lbl_YourBalance.ID] +
                "&userId=" + userManagement.GetGuid +
                "&userName=" + Server.UrlEncode(userManagement.User_Name) +
                "&siteId=" + siteSetting.GetSiteID +
                "&siteLangId=" + siteSetting.siteLangId +
                "&fieldId=" + lbl_YourBalance.ID +
                "&fieldName=" + Server.UrlEncode(lbl_YourBalance.Text) +
                "&isFirstTimeV=" + IsFirstTimeV.ToString() +
                "&bonusAmount=" + Int32.Parse(txt_AddDeposite.Text) +
                "&balanceNew=" + _new_balance +
                "&email=" + Server.UrlEncode(Email) +
                "&phoneNumber=" + PhoneNumber +
                "&otherPhone=" + OtherPhone +
                "&country=" + Country +
                "&city=" + City +
                "&streetNumber=" + Server.UrlEncode(StreetNumber) +
                "&postalCode=" + PostalCode +
                "&webSite=" + Server.UrlEncode(WebSite) +
                "&vat=" + vatV +
                "&ifAccountingCharging=" + Convert.ToString(siteSetting.IfAccountingCharging) +
                "&ppcPackage=" + Server.UrlEncode(Hidden_PpcPackage.Value) +
                "&currency=" + Currency +
                "&accountNumber=" + AccountNumber +
                "&fax=" + Fax + "&amount=" + _deposit + "&bonusType=" + bonusType;




            cmd.Parameters.AddWithValue("@details", paymentPelecardDetails);

            string auto_id = Convert.ToString(cmd.ExecuteScalar());
            /*
            title string Title   Mr 
            companyName string Company Name   Best Comp 
            firstName string First Name   John 
            lastName string Last Name   Doe 
            email string Email Address The customer's email, must be a valid email address john@doe.com 
            validateEmail string Verify Email Address Must be exactly the same as the email field john@doe.com 
            address1 string Address Line 1   1234 St. James Dr. 
            address2 string Address Line 2   Suite 123 
            city string City   San Diego 
            state string State   CA 
            zipCode string Zip Code/Postal Code   78467 
            country two letter code (link reference for full country list) Country http://www.plimus.com/jsp/buynow_countries.jsp us 
            workPhone string Phone   34567896 
            workExtension string Ext   567 
            faxNumber string Fax   987654333 
            */
            /*
            int tempNumberOfPayments = 3;

            for (int i = 0; i < ArrNumberPayments.Length; i++)
            {
                WebReferenceSupplier.PaymentsKeyValuePair paymentKeyValuePair = ArrNumberPayments[i];
                if (_deposit >= paymentKeyValuePair.FromPrice)
                {
                    tempNumberOfPayments = paymentKeyValuePair.NumberOfPayments;
                }
            }          
            * /

            __form = (CompanyCharging.GetPlimusForm(this, _deposit, auto_id, CompanyName, FirstName, SecondName, Email, StreetNumber,City,State,PostalCode,Country,PhoneNumber,Fax));
           
        }

        else if (siteSetting.CharchingCompany ==  eCompanyCharging.paypal)
        {
            /***************** for pay pal *************** /
            decimal payBeforeVat = 0;
            string deposit = _deposit + "";
            if (siteSetting.ifVatIncluded == true)
            {
                decimal vat = (vatV + 100m) / 100m;

                payBeforeVat = (decimal)_deposit / vat;
                payBeforeVat = Math.Round(payBeforeVat, 2, MidpointRounding.AwayFromZero); //AwayFromZero. The one's digit is always rounded up to the next digit. This is the most commonly known rounding method. It is known as symmetric arithmetic rounding
                deposit = payBeforeVat.ToString();
            }

            __form = (CompanyCharging.GetPaypalForm(this, _deposit, FirstName, SecondName, Email,
                Currency, lbl_CurrentBalance.ID, lbl_CurrentBalance.Text, CurrenBalance.ToString(), IsFirstTimeV));

        }

        else if (siteSetting.CharchingCompany == eCompanyCharging.platnosci)
        {
            /* amount in grosz Grosz, a coin used in Poland as a hundredth part of 1 złoty. Złoty is Polish currency * /
            int grosz = _deposit * 100;
            Session["returnPlatnosci"] = "http://" + Request.Url.Host + ":"
                + Request.Url.Port + ResolveUrl("~") +
                "Management/professionLogin.aspx";

            __form = CompanyCharging.GetPlatnosciForm(this, _deposit, FirstName, SecondName,
                Email, PhoneNumber, lbl_CurrentBalance.ID, lbl_CurrentBalance.Text, _new_balance.ToString(),
                IsFirstTimeV, Street, HouseNumber, City, PostalCode,
                CompanyName, "", Hidden_PpcPackage.Value,
                Int32.Parse(txt_AddDeposite.Text).ToString(), lbl_YourBalance.Text.Replace("\"", "&quot;"), bonusType);
        }

        else if (siteSetting.CharchingCompany == eCompanyCharging.interinfo)
        {
            int RechargeAmount = -1;
            if (cb_AutoRecharge.Visible && cb_AutoRecharge.Checked)
                int.TryParse(txt_RechargeAmount.Text, out RechargeAmount);

            string command = "EXEC dbo._SetPayment @site_id ,@company, @advertiser, @details, @RechargeAmount";
            SqlConnection conn = DBConnection.GetConnString();
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@site_id", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@company", "interinfo");
            cmd.Parameters.AddWithValue("@advertiser", CompanyName);
            if (RechargeAmount == -1)
                cmd.Parameters.AddWithValue("@RechargeAmount", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@RechargeAmount", RechargeAmount);
            string paymentInterInfoDetails = "";

            //      int depositAgorot = int.Parse(deposit) * 100;

            paymentInterInfoDetails = "supplierGuid=" + userManagement.GetGuid +
                "&supplierId=" + CompanyId +
                "&advertiserName=" + Server.UrlEncode(CompanyName) +
                "&balanceOld=" + dicValueV[lbl_YourBalance.ID] +
                "&userId=" + userManagement.GetGuid +
                "&userName=" + Server.UrlEncode(userManagement.User_Name) +
                "&siteId=" + siteSetting.GetSiteID +
                "&siteLangId=" + siteSetting.siteLangId +
                "&fieldId=" + lbl_YourBalance.ID +
                "&fieldName=" + Server.UrlEncode(lbl_YourBalance.Text) +
                "&isFirstTimeV=" + IsFirstTimeV.ToString() +
                "&bonusAmount=" + Int32.Parse(txt_AddDeposite.Text) +
                "&balanceNew=" + _new_balance +
                "&email=" + Server.UrlEncode(Email) +
                "&phoneNumber=" + PhoneNumber +
                "&otherPhone=" + OtherPhone +
                "&country=" + Country +
                "&city=" + City +
                "&streetNumber=" + Server.UrlEncode(StreetNumber) +
                "&postalCode=" + PostalCode +
                "&webSite=" + Server.UrlEncode(WebSite) +
                "&vat=" + vatV +
                "&ifAccountingCharging=" + Convert.ToString(siteSetting.IfAccountingCharging) +
                "&ppcPackage=" + Server.UrlEncode(Hidden_PpcPackage.Value) +
                "&currency=" + Currency +
                "&accountNumber=" + AccountNumber +
                "&fax=" + Fax + "&amount=" + _deposit + "&bonusType=" + bonusType; ;


            cmd.Parameters.AddWithValue("@details", paymentInterInfoDetails);

            string auto_id = Convert.ToString(cmd.ExecuteScalar());

            string commandGuid = "EXEC dbo._GetPaymentGuid @auto_id";
            SqlCommand cmdGuid = new SqlCommand(commandGuid, conn);
            cmdGuid.Parameters.AddWithValue("@auto_id", auto_id);
            string guid = Convert.ToString(cmdGuid.ExecuteScalar());

            __form = CompanyCharging.GetInterInfoForm(this, _deposit, auto_id, guid);
        }
        /*
            Into this script you need to pass the 3 variables:
            Amount – payment amount
            Order nr – the order number on your side. Based on what we can send back the information about the approved payment
            Description – description of the payment, ie: No Problem Service. (All these 3 variables are mandatory). 

            <form method="post" action="http://1188.dms.ee/noproblem_payment.php">
            <input type="hidden" name="amount" value="50,5" />
            <input type="hidden" name="order_nr" value="123123-123" />
            <input type="hidden" name="description" value="Payment for something" />
            <input type="submit" name="" value="Maksa" />

        * /

        if (__form.Length != 0)
        {
            Response.Write(__form);
            Response.Write(@"<script type=""text/javascript"">window.location='professionalDetails.aspx';</script>");
            Response.Flush();
            Response.End();
        }
            
        
        return true;
             * */
    }
    protected void btn_buyNow_Click(object sender, EventArgs e)
    {
        if (!CanMakeSale)
        {
            btn_AutoRecharge.Visible = false;
            btn_buyNow.Visible = false;
            lbl_CanMakeSale.Visible = true;
            return;
        }
        int _deposit;
         _PanelInvalidDeposit.Attributes.Add("style", "visibility:hidden");
        if (!int.TryParse(txt_deposite.Text, out _deposit) || _deposit < minDepodit)
        {

            _PanelInvalidDeposit.Attributes.Add("style", "visibility:visible");
            _UpdatePanelDeposite.Update();

            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "wrongDetails3"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "wrongDetails3", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_WrongAmount.Text) + "');", true);
            return;
        }
        buyNow_CreditCard((decimal)_deposit);
        /*
        bool IsSuccess = false;
        if (rbl_PaymentMethod.SelectedValue == ePaymentMethodSIte.CreditCard.ToString())
        {
            IsSuccess = buyNow_CreditCard((decimal)_deposit);
        }
        else
        {
            
            if (siteSetting.IfPaymentCharging && siteSetting.CharchingCompany == eCompanyCharging.platnosci)
            {
                /* amount in grosz Grosz, a coin used in Poland as a hundredth part of 1 złoty. Złoty is Polish currency * /
                int grosz = _deposit * 100;
                Session["returnPlatnosci"] = "http://" + Request.Url.Host + ":"
                    + Request.Url.Port + ResolveUrl("~") +
                    "Management/professionLogin.aspx";

                string bonusType;

                if (Int32.Parse(txt_AddDeposite.Text) <= 0)
                    bonusType = WebReferenceSupplier.BonusType.None.ToString();
                else
                    bonusType = WebReferenceSupplier.BonusType.Package.ToString();

                string __form = CompanyCharging.GetPlatnosciForm(this, _deposit, FirstName, SecondName,
                    Email, PhoneNumber, lbl_CurrentBalance.ID, lbl_CurrentBalance.Text, txt_NewBalance.Text,
                    IsFirstTimeV, Street, HouseNumber, City, PostalCode,
                    CompanyName, DropDownListPlatnosciPayTypes.SelectedValue,
                    Hidden_PpcPackage.Value, txt_AddDeposite.Text, lbl_YourBalance.Text.Replace("\"", "&quot;"), bonusType);


                txt_NewBalance.Text = "";
                tb_nameCard.Text = "";
                tb_creditNumber.Text = "";
                tb_cardVerification.Text = "";
                tb_id.Text = "";
                ddl_expirymonth.SelectedIndex = 0;
                ddl_expiryear.SelectedIndex = 0;
                Response.Write(__form);

                Response.Flush();
                Response.End();
            }
        }
        if (!userManagement.Is_CompleteRegistration && IsSuccess)
        {
            string _location = ResolveUrl("~") + "Management/ProfessionalUpdate.aspx";
            userManagement.Is_CompleteRegistration = true;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "completeRegistration", "CompleteRegitration('" + _location + "');", true);
        }
       
        ClearNLoad();
        LoadDetail();
        
        _UpdatePanelBalance.Update();
        _UpdatePanelDeposite.Update();

        */
    }
    protected void btn_AutoRecharge_click(object sender, EventArgs e)
    {
        if (!CanMakeSale)
        {
            btn_AutoRecharge.Visible = false;
            btn_buyNow.Visible = false;
            lbl_CanMakeSale.Visible = true;
        }
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.UpdateAutoRechargeOptionsRequest _request = new WebReferenceSupplier.UpdateAutoRechargeOptionsRequest();
        _request.SupplierId = userManagement.Get_Guid;// new Guid(GetGuidSetting());
        _request.CreatedByUserId = userManagement.Get_Guid;
        if (cb_AutoRecharge.Checked)
        {
            int RechargeAmount;
            if (!int.TryParse(txt_RechargeAmount.Text, out RechargeAmount))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
            _request.IsAutoRecharge = true;
            _request.RechargeAmount = RechargeAmount;          
        }
        else
            _request.IsAutoRecharge = false;

        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.UpdateAutoRechargeOptions(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetNewRechargeDetails", 
            "SetNewRechargeDetails(" + cb_AutoRecharge.Checked.ToString().ToLower() + ");", true);
        
    }
    void notValidPriceCall(string message)
    {
        string script = "alert('" + message + "');";
        ClientScript.RegisterStartupScript(this.GetType(), "notValidCall", script, true);
    }
    /*
    private void LoadSymbolMoney()
    {
        
        lbl_money.Text = siteSetting.CurrencySymbol;
        lbl_currency1.Text = siteSetting.CurrencySymbol;
        lbl_currency2.Text = siteSetting.CurrencySymbol;
        lbl_currency3.Text = siteSetting.CurrencySymbol;
    }
    */
    protected void btn_SetInvoiceOption_Click(object sender, EventArgs e)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.UpdateInvoiceSendOptionsRequest _request = new WebReferenceSupplier.UpdateInvoiceSendOptionsRequest();
        _request.SupplierId = userManagement.Get_Guid;
        WebReferenceSupplier.InvoiceSendOption iso;
        if (!Enum.TryParse(rbl_InvoiceOption.SelectedValue, out iso))
            iso = WebReferenceSupplier.InvoiceSendOption.Email;
        _request.InvoiceSendOption = iso;
        _request.InvoiceSendAddress = txt_MailAddress.Text;
        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.UpdateInvoiceSendOptions(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
    //    hf_InvoiceValue.Value = rbl_InvoiceOption.SelectedValue + ";;;" + _request.InvoiceSendAddress;
        string _value = rbl_InvoiceOption.SelectedValue + ";;;" + _request.InvoiceSendAddress;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetInvoiceOption", "SetInvoiceOption('" + _value + "');", true);
        Update_Success();
    }
    decimal vatV
    {
        get { return (ViewState["vatV"] == null) ? 0m : (decimal)ViewState["vatV"]; }
        set { ViewState["vatV"] = value; }
    }

    protected decimal VAT
    {
        get { return (vatV / 100m) + 1m; }
    }
    protected string _CanMakeSale
    {
        get { return CanMakeSale.ToString().ToLower(); }
    }
    bool CanMakeSale
    {
        get { return (ViewState["CanMakeSale"] == null) ? false : (bool)ViewState["CanMakeSale"]; }
        set { ViewState["CanMakeSale"] = value; }
    }
    protected decimal CurrenBalance
    {
        get { return (ViewState["CurrenBalance"] == null) ? 0 : (decimal)ViewState["CurrenBalance"]; }
        set { ViewState["CurrenBalance"] = value; }

    }
    protected Dictionary<string, string> dicValueV
    {
        get { return (ViewState["dicValue"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["dicValue"]; }
        set { ViewState["dicValue"] = value; }
    }
    protected bool IsFirstTimeV
    {
        get { return (ViewState["IsFirstTime"] == null) ? false : (bool)ViewState["IsFirstTime"]; }
        set { ViewState["IsFirstTime"] = value; }
    }
    protected string percentsV
    {
        get { return (ViewState["percents"] == null) ? "0" : (string)ViewState["percents"]; }
        set { ViewState["percents"] = value; }
    }
    protected string pricesV
    {
        get { return (ViewState["prices"] == null) ? "0" : (string)ViewState["prices"]; }
        set { ViewState["prices"] = value; }
    }
    protected string SupplierBizId
    {
        get { return (ViewState["SupplierBizId"] == null) ? "" : (string)ViewState["SupplierBizId"]; }
        set { ViewState["SupplierBizId"] = value; }
    }
    protected string GetEmailRegularExpression
    {
        get { return Utilities.RegularExpressionForJavascript(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"); }
    }
    protected string GetNoRechargeChange
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_NoRechargeChange.Text); }
    }
    protected string Currency_Symbol
    {
        get { return siteSetting.CurrencySymbol; }
        
    }
    /*
     * 
    
   


    protected string priceMethodV
    {
        get { return (ViewState["priceMethod"] == null) ? "" : (string)ViewState["priceMethod"]; }
        set { ViewState["priceMethod"] = value; }
    }

    protected bool IsCreditCardMethodV
    {
        get { return (ViewState["IsCreditCardMethod"] == null) ? false :
            (bool)ViewState["IsCreditCardMethod"];
            }

        set { ViewState["IsCreditCardMethod"] = value; }
    }

    protected bool IsWireTransferMethodV
    {
        get
        {
            return (ViewState["IsWireTransferMethodV"] == null) ? false :
                (bool)ViewState["IsWireTransferMethodV"];
        }

        set { ViewState["IsWireTransferMethodV"] = value; }
    }
    protected string CompanyName
    {
        get { return (ViewState["CompanyName"] == null) ? "" : (string)ViewState["CompanyName"]; }
        set { ViewState["CompanyName"] = value; }
    }

    protected string FirstName
    {
        get { return (ViewState["FirstName"] == null) ? "" : (string)ViewState["FirstName"]; }
        set { ViewState["FirstName"] = value; }
    }

    protected string SecondName
    {
        get { return (ViewState["SecondName"] == null) ? "" : (string)ViewState["SecondName"]; }
        set { ViewState["SecondName"] = value; }
    }

    protected string CompanyId
    {
        get { return (ViewState["CompanyId"] == null) ? "" : (string)ViewState["CompanyId"]; }
        set { ViewState["CompanyId"] = value; }
    }

    protected string Email
    {
        get { return (ViewState["Email"] == null) ? "" : (string)ViewState["Email"]; }
        set { ViewState["Email"] = value; }
    }

    protected string PhoneNumber
    {
        get { return (ViewState["PhoneNumber"] == null) ? "" : (string)ViewState["PhoneNumber"]; }
        set { ViewState["PhoneNumber"] = value; }
    }

    protected string Country
    {
        get { return (ViewState["Country"] == null) ? "" : (string)ViewState["Country"]; }
        set { ViewState["Country"] = value; }
    }

    protected string State
    {
        get { return (ViewState["State"] == null) ? "" : (string)ViewState["State"]; }
        set { ViewState["State"] = value; }
    }

    protected string County
    {
        get { return (ViewState["County"] == null) ? "" : (string)ViewState["County"]; }
        set { ViewState["County"] = value; }
    }

    protected string City
    {
        get { return (ViewState["City"] == null) ? "" : (string)ViewState["City"]; }
        set { ViewState["City"] = value; }
    }

    protected string Street
    {
        get { return (ViewState["Street"] == null) ? "" : (string)ViewState["Street"]; }
        set { ViewState["Street"] = value; }
    }

    protected string HouseNumber
    {
        get { return (ViewState["HouseNumber"] == null) ? "" : (string)ViewState["HouseNumber"]; }
        set { ViewState["HouseNumber"] = value; }
    }

    protected string StreetNumber 
    {
        get { return (ViewState["HouseNumber"] == null) ? "" : Street + " " + HouseNumber; }
        
    }

    protected string PostalCode
    {
        get { return (ViewState["PostalCode"] == null) ? "" : (string)ViewState["PostalCode"]; }
        set { ViewState["PostalCode"] = value; }
    }

    

    protected string Currency
    {
        get { return (ViewState["Currency"] == null) ? "" : (string)ViewState["Currency"]; }
        set { ViewState["Currency"] = value; }
    }

    protected string CompanyFullAddress
    {
        get
        {
            return Street + " " + HouseNumber + (County == null ? " " : " " + County) + (State == null ? " " : " " + State) + (Country == null ? " " : " " + Country) + (PostalCode == null ? " " : " " + PostalCode);
        }

    }

    protected string OtherPhone
    {
        get { return (ViewState["OtherPhone"] == null) ? "" : (string)ViewState["OtherPhone"]; }
        set { ViewState["OtherPhone"] = value; }
    }

    protected string WebSite
    {
        get { return (ViewState["WebSite"] == null) ? "" : (string)ViewState["WebSite"]; }
        set { ViewState["WebSite"] = value; }
    }

    protected string Fax
    {
        get { return (ViewState["Fax"] == null) ? "" : (string)ViewState["Fax"]; }
        set { ViewState["Fax"] = value; }
    }

    protected string CreditCardToken
    {
        get { return (ViewState["CreditCardToken"] == null) ? "" : (string)ViewState["CreditCardToken"]; }
        set { ViewState["CreditCardToken"] = value; }
    }

    protected string Last4DigitsCC
    {
        get { return (ViewState["Last4DigitsCC"] == null) ? "" : (string)ViewState["Last4DigitsCC"]; }
        set { ViewState["Last4DigitsCC"] = value; }
    }


    

    protected string CardId
    {
        get { return (ViewState["CardId"] == null) ? "" : (string)ViewState["CardId"]; }
        set { ViewState["CardId"] = value; }
    }


    protected string returnPayPal
    {
        get { return (ViewState["returnPayPal"] == null) ? "" : (string)ViewState["returnPayPal"]; }
        set { ViewState["returnPayPal"] = value; }
 
    }

    protected string ChargingAccount
    {
        get { return (ViewState["ChargingAccount"] == null) ? "" : (string)ViewState["ChargingAccount"]; }
        set { ViewState["ChargingAccount"] = value; }

    }

    protected string ChargingUsername
    {
        get { return (ViewState["ChargingUsername"] == null) ? "" : (string)ViewState["ChargingUsername"]; }
        set { ViewState["ChargingUsername"] = value; }

    }

    protected string ChargingPassword
    {
        get { return (ViewState["ChargingPassword"] == null) ? "" : (string)ViewState["ChargingPassword"]; }
        set { ViewState["ChargingPassword"] = value; }

    }

    protected string ChargingShop
    {
        get { return (ViewState["ChargingShop"] == null) ? "" : (string)ViewState["ChargingShop"]; }
        set { ViewState["ChargingShop"] = value; }

    }

    protected string PeleCardUserName
    {
        get { return (ViewState["peleCardUserName"] == null) ? "" : (string)ViewState["peleCardUserName"]; }
        set { ViewState["peleCardUserName"] = value; }

    }

    protected string PeleCardPassword
    {
        get { return (ViewState["peleCardPassword"] == null) ? "" : (string)ViewState["peleCardPassword"]; }
        set { ViewState["peleCardPassword"] = value; }

    }

    protected string PeleCardTermNo
    {
        get { return (ViewState["peleCardTermNo"] == null) ? "" : (string)ViewState["peleCardTermNo"]; }
        set { ViewState["peleCardTermNo"] = value; }

    }

    protected string PelecardShopNo
    {
        get { return (ViewState["pelecardShopNo"] == null) ? "" : (string)ViewState["pelecardShopNo"]; }
        set { ViewState["pelecardShopNo"] = value; }

    }

    protected string PageUrl
    {
        get { return (ViewState["pageUrl"] == null) ? "" : (string)ViewState["pageUrl"]; }
        set { ViewState["pageUrl"] = value; }

    }

    protected string AccountNumber
    {
        get { return (ViewState["AccountNumber"] == null) ? "" : (string)ViewState["AccountNumber"]; }
        set { ViewState["AccountNumber"] = value; }

    }

    protected WebReferenceSupplier.PaymentsKeyValuePair[] ArrNumberPayments
    {
        get { return (WebReferenceSupplier.PaymentsKeyValuePair[])ViewState["ArrNumberPayments"]; }
        set { ViewState["ArrNumberPayments"] = value; }
    }
   
    */
}
