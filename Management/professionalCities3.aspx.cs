using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class Management_professionalCities3 : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            //RadTreeView1.Attributes["dir"] = "rtl";
            //Session["Site"] = new SiteSetting("1");
            //RadTreeView1.LoadContentFile("tree.xml");
            /*
             StringBuilder sbXml = new StringBuilder();
             sbXml.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
             sbXml.Append("<Tree>"); 
             sbXml.Append("<Node Text=\"cangzhou\" Value=\"089C9AFE-E89E-DF11-9C13-0003FF727321\" ifExpand=\"False\" ExpandMode=\"WebService\" >");
             //sbXml.Append("<Node Text=\"botou\" Value=\"5A02771F-E99E-DF11-9C13-0003FF727321\" ifExpand=\"False\" ExpandMode=\"WebService\" />");
             //sbXml.Append("<Node Text=\"nanpi\" Value=\"5402771F-E99E-DF11-9C13-0003FF727321\" ifExpand=\"False\" ExpandMode=\"WebService\" />");     
             sbXml.Append("</Node>");
             sbXml.Append("<Node Text=\"cangzhou\" Value=\"089C9AFE-E89E-DF11-9C13-0003FF727321\"   >");
             //sbXml.Append("<Node Text=\"botou\" Value=\"5A02771F-E99E-DF11-9C13-0003FF727321\" ifExpand=\"False\" ExpandMode=\"WebService\" />");
             //sbXml.Append("<Node Text=\"nanpi\" Value=\"5402771F-E99E-DF11-9C13-0003FF727321\" ifExpand=\"False\" ExpandMode=\"WebService\" />");     
             sbXml.Append("</Node>");
             sbXml.Append("</Tree>"); 
             //Response.Write(sbXml.ToString());
       
             RadTreeView1.LoadXmlString(sbXml.ToString());
            */


            if (string.IsNullOrEmpty(GetGuidSetting()))
            {
                string csnameLogin = "setLogin";
                Type cstypeLogin = this.GetType();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager csLogin = this.Page.ClientScript;

                if (!csLogin.IsClientScriptBlockRegistered(cstypeLogin, csnameLogin))
                {
                    string csTextLogin = "top.location='" + ResolveUrl("~") + @"Management/ProfessionLogin.aspx';";
                    csLogin.RegisterClientScriptBlock(cstypeLogin, csnameLogin, csTextLogin, true);
                }
                return;
            }
            if (userManagement.IsSupplier() && siteSetting.GetSiteID == PpcSite.GetCurrent().ZapSiteId)
                btnSendWebService.Visible = false;
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
            lbl_page.Text = " 6";

            Session["supllierId"] = GetGuidSetting();

            WebServiceGetCityChilds cityChilds = new WebServiceGetCityChilds(Hidden_Partial.Value);
            Dictionary<string, string> dic;
            dic = cityChilds.getCityChildsLevelOne(RadTreeView1);

            faildInitTree(dic);
            initRegistationStep(dic);


            //WebReferenceSite.Site s=WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
            //siteSetting.GetUrlWebReference
            /*
            RadTreeView1.DataSource = cityChilds.getCityChilds();
            RadTreeView1.DataTextField = "Text";
            RadTreeView1.DataValueField = "Value";
           
            RadTreeView1.DataBind();
            */
            RemoveServerCommentPPC();



        }

        Page.DataBind();
    }

    private void faildInitTree(Dictionary<string, string> dic)
    {
        string resultType = dic["ResultType"];
        
        if (resultType == "failure")
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);

        }
    }

    


    private void initRegistationStep(Dictionary<string, string> dic)
    {

        string RegistrationStage = dic["RegistrationStep"];

        string csnameStep = "setStep";
        Type cstypeStep = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager csStep = this.Page.ClientScript;


        if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
        {
            string csTextStep = "witchAlreadyStep(" + RegistrationStage + ");";
            csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
        }


        if (int.Parse(RegistrationStage) >= 3)
        {
            btnSendWebService.Value = lbl_btn_Update.Text;
            btnSendWebService.Attributes.Add("title", "Update");
            pSteps.Visible = false;
        }
        else
        {
            btnSendWebService.Value = lbl_btn_Next.Text;
            btnSendWebService.Attributes.Add("title", "Next");
            pSteps.Visible = true;
        }


    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
}       
      
    

   

