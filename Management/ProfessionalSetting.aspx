<%@ Page Language="C#" MasterPageFile="~/Management/MasterPageAdvertiser.master" 
AutoEventWireup="true" CodeFile="ProfessionalSetting.aspx.cs" 
Inherits="Management_ProfessionalSetting"  %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script type="text/javascript" language="javascript" >
    function CheckValidation() {
        if (!document.getElementById("<%# cb_DailyBudget.ClientID %>").checked)
            return true;
        if (!Page_ClientValidate('DailyBudget'))
            return false;
        return true;
    }
    function DailyBudgetClick(_Checked) {
        document.getElementById("<%# div_DailyBudget.ClientID %>").style.visibility = (_Checked) ? "visible" : "hidden";
        if (!_Checked) {
            document.getElementById("<%# txt_DailyBudget.ClientID %>").value = "";
            CleanDailyBudgetValidation();
        }
    }
    function CleanDailyBudgetValidation() {
        for (i = 0; i < Page_Validators.length; i++) {
            if (Page_Validators[i].validationGroup == "DailyBudget") {
                Page_Validators[i].style.display = "none";
            }
        }
    }

</script>

   
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<div class="page-contentm page-settings">
    <h2><asp:Label ID="lblTitleAnalytics" runat="server" Text="Welcome to your Settings"></asp:Label></h2>
    <table>
    <tr>
    <td></td>
    <td>
        <asp:Label ID="lbl_dateFormat" runat="server" Text="Choose date format" CssClass="label"></asp:Label>
    </td>
    <td>
        <asp:DropDownList ID="ddl_dateFormat" runat="server" CssClass="form-select"></asp:DropDownList>
    </td>
    </tr>

    <tr>
    <td></td>
    <td>
        <asp:Label ID="lbl_TimeFormat" runat="server" Text="Choose time format" CssClass="label"></asp:Label>
    </td>
    <td>
         <asp:DropDownList ID="ddl_TimeFormat" runat="server" CssClass="form-select"></asp:DropDownList>
    </td>
    </tr>

    <tr>
    <td></td>
    <td>
        <asp:Label ID="lbl_DefaultLang" runat="server" Text="Choose default language" CssClass="label"></asp:Label>
    </td>
    <td>
        <asp:DropDownList ID="ddl_languages" runat="server" CssClass="form-select"></asp:DropDownList>
    </td>
    </tr>
        
    <tr runat="server" id="tr_MaxNumberDailyCalls">
    <td >
        
    </td>
    <td>
        <asp:CheckBox ID="cb_DailyBudget" runat="server" onclick="DailyBudgetClick(this.checked)" />
        <asp:Label ID="lbl_DailyBudget" runat="server" Text="Max number of daily calls" CssClass="label"></asp:Label>
    </td>
    <td>        
        <div id="div_DailyBudget" runat="server">
            <asp:TextBox CssClass="max_numbers" ID="txt_DailyBudget" runat="server"></asp:TextBox>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator_DailyBudget" runat="server" ErrorMessage="Missing"
             Display="Dynamic" CssClass="error-msg" ValidationGroup="DailyBudget" ControlToValidate="txt_DailyBudget"></asp:RequiredFieldValidator>
            
            <asp:RangeValidator ID="RangeValidator_DailyBudget" runat="server" ErrorMessage="The value in this field is between 2 and 10,000"
             Display="Dynamic" CssClass="error-msg" ValidationGroup="DailyBudget" ControlToValidate="txt_DailyBudget"
             Type="Integer" MaximumValue="10000" MinimumValue="2"></asp:RangeValidator>
        </div>
    </td>
    </tr>
    </table>


    <asp:button runat="server" text="Save" id="imageSend" CssClass="form-submit" OnClick="imageSend_Click" OnClientClick="return CheckValidation();"/>
</div>
    
    
    <asp:Label ID="lbl_CrmError" runat="server" Text="Server problem! max number of daily calls did not saved" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ServerError" runat="server" Text="Server problem! Update faild in date and time format and default language" Visible="false"></asp:Label>
</asp:Content>

