﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Diagnostics;
//using System.Reflection;
//using Mantis.OneCall.Business;
//using Mantis.OneCall.Components;
using System.Collections.Generic;
//ASPJPEGLibusing Mantis.Data;
using System.Xml;
using System.Data.SqlClient;
using System.Web.Services.Protocols;
using System.Threading;
using System.Text;

public partial class Management_ProfessionalUpdate : PageSetting
{
    string _logo;
    string _description;

    string TempWebImage = System.Configuration.ConfigurationManager.AppSettings["professionalImagesTempWeb"];
    string TempImage = System.Configuration.ConfigurationManager.AppSettings["professionalImagesTemp"];
    string CurrentImage = System.Configuration.ConfigurationManager.AppSettings["professionalImagesPrefix"];

    string page_translate;
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        if (userManagement.IsPublisher())
            this.MasterPageFile = "../Publisher/MasterPageFrame.master";
        else if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
            this.MasterPageFile = ResolveUrl("~") + "Controls/MasterPagePPC.master";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        tbDescription.Attributes.Add("onkeypress", "checkDescLengh(500,'" + tbDescription.ClientID + "')");
        tbShortDescription.Attributes.Add("onkeypress", "checkDescLengh(240,'" + tbShortDescription.ClientID + "')");
        ScriptManager ScriptManager1 = ScriptManager.GetCurrent(this);
      //  ScriptManager1.RegisterAsyncPostBackControl(imageSendPreview);
        ScriptManager1.RegisterAsyncPostBackControl(imageSend);        

        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {
                string csnameLogin = "setLogin";
                Type cstypeLogin = this.GetType();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager csLogin = this.Page.ClientScript;

                if (!csLogin.IsClientScriptBlockRegistered(cstypeLogin, csnameLogin))
                {
                    string csTextLogin = "top.location='ProfessionLogin.aspx';";
                    csLogin.RegisterClientScriptBlock(cstypeLogin, csnameLogin, csTextLogin, true);
                }
                return;
            }

                     
            cleanSession();
            LoadDetail();
       //     SetDirectorySetting();
            
       //     SetAdvertiseDB();
            LoadPageLinks();
            BindDataTemp();
            //saveXml();
            
        }
        showLogo();
   //     showLinkButton();
        Header.DataBind();
        
    }

    private void LoadDetail()
    {

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfLogoDescription result = null;
        try
        {
            result = supplier.GetSupplierLogoDescription(new Guid(GetGuidSetting()));
            //Response.Write(result);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        /*
        if(userManagement.IsPublisher())
        {
            a_FilterLink.Visible = true;
            a_FilterLink.HRef = "http://gpnt083/DTags/Business/Index/" + result.Value.BizId;
            a_FilterLink.InnerText = "http://gpnt083/DTags/Business/Index/" + result.Value.BizId;
        }
        else
            a_FilterLink.Visible = false;
         * */
        logoV = result.Value.Logo;
        DescriptionV = result.Value.Description;
      
        Dictionary<string, string> dic = new Dictionary<string, string>();
  //      dic.Add(lblMyVideo.ID, CleanEnters(_video_url));
  //      dic.Add(lblShortDescription.ID, CleanEnters(_short_desc));
  //      dic.Add(lblComDescription.ID, CleanEnters(DescriptionV));
        dic.Add(lblComDescription.ID, DescriptionV);
        dic.Add(lbl_MyLogo.ID, logoV);

        dicValueV = dic;
       
    }
    private void BindDataTemp()
    {
        tbDescription.Text = DescriptionV;
     //   tbShortDescription.Text = ShortDescriptionV;
     //   tbVideo.Text = VideoV;       
    }
   

    private void SetDirectorySetting()
    {
        string tempDirectory = System.Configuration.ConfigurationManager.AppSettings["professionalImagesTemp"];
        Random rnd = new Random();
        int tempD = rnd.Next(1000, 99999);
        string temp_Directory = tempDirectory + tempD.ToString();
        while (Directory.Exists(temp_Directory))
        {
            DirectoryInfo di = new DirectoryInfo(temp_Directory);

            if (DateTime.Now.AddHours(-2) > di.LastAccessTime)
            {
                try
                {
                    di.Delete(true);
                }
                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
                }
            }
            else
            {
                tempD = rnd.Next(1000, 99999);
                temp_Directory = tempDirectory + tempD.ToString();
            }
        }
        TempDirectoryV = tempD.ToString();
        Directory.CreateDirectory(TempDirectoryV);


        /* Gallery directory
        string professionalImagesroot = System.Configuration.ConfigurationManager.AppSettings["professionalImagesPrefix"];

        string ProfessionalGuid = Utilities.CleanStringGuid(GetGuidSetting());
        string str_path = professionalImagesroot + ProfessionalGuid;
        CurrentDirectoryV = str_path;
        if (!Directory.Exists(str_path))
        {
            SetBadDirectory(str_path, professionalImagesroot);
        }
        else
        {
            if (!File.Exists(str_path + @"\AC_RunActiveContent.js") || !File.Exists(str_path + @"\Shai_Gallery.html") || !File.Exists(str_path + @"\Shai_Gallery.swf"))
                SetBadDirectory(str_path, professionalImagesroot);
            else if(!Directory.Exists(str_path + @"\images") || !Directory.Exists(str_path + @"\images\standard"))
                SetBadDirectory(str_path, professionalImagesroot);
        }
        CheckImagesStandard(str_path + @"\images");
        DirectoryCopy.CopyAll(new DirectoryInfo(str_path), new DirectoryInfo(TempDirectoryV));
         * */
    }

    private void CheckImagesStandard(string str_path)
    {
        List<string> files = new List<string>();
        files.AddRange(Directory.GetFiles(str_path));
      //  string[] files = Directory.GetFiles(str_path);
         List<string> StanFiles = new List<string>();
         string path_standard = str_path + @"\standard";
        StanFiles.AddRange(Directory.GetFiles(path_standard));
        RemoveDBFile(files);
        RemoveDBFile(StanFiles);
    //    string[] StanFiles = Directory.GetFiles(path_standard);
        for(int i=files.Count-1;i>-1;i--)
        {

            bool IsFind = false;
            for(int j=StanFiles.Count-1;j>-1;j--)
            {

                if (Path.GetFileName(files[i]) == Path.GetFileName(StanFiles[j]))
                {
                    files.RemoveAt(i);
                    StanFiles.RemoveAt(j);
                    IsFind = true;
                    break;
                }
            }
            if (!IsFind)
            {
                string file_name = Path.GetFileName(files[i]);
                File.Copy(files[i], path_standard + @"\" + file_name);
                files.RemoveAt(i);
            }
        }
        foreach (string str in StanFiles)
        {
            File.Delete(str);
        }

    }
    void RemoveDBFile(List<string> list)
    {

        for (int i = list.Count - 1; i > -1; i--)
        {
            string[] filesplit = list[i].Split('.');
            if (filesplit.Length < 2)
                list.RemoveAt(i);
            if (filesplit[filesplit.Length - 1].ToLower() == "db")
                list.RemoveAt(i);
        }
    }
    void SetBadDirectory(string _path, string professionalImagesroot)
    {
        if (Directory.Exists(_path))
            Directory.Delete(_path, true);
        Directory.CreateDirectory(_path);
        Directory.CreateDirectory(_path + @"\images");
        Directory.CreateDirectory(_path + @"\images\standard");

        File.Copy(professionalImagesroot + @"AC_RunActiveContent.js",
            _path + @"\AC_RunActiveContent.js");
        File.Copy(professionalImagesroot + @"Shai_Gallery.html",
          _path + @"\Shai_Gallery.html");
        File.Copy(professionalImagesroot + @"Shai_Gallery.swf",
           _path + @"\Shai_Gallery.swf");

    }
    /*
    private void chkConfirmed(bool isConfirmed)
    {
        IsFirstTimeV = !isConfirmed;
        chkRegulations.Checked = isConfirmed;
        chkRegulations.Enabled = !isConfirmed;
    }
     * */
    /*
    private void SetAdvertiseDB()
    {
        string logo = "";
        string command = "EXEC dbo.GetAdvertiserDetails @UserId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", new Guid(GetGuidSetting()));
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                chkConfirmed((bool)reader["IsConfirmed"]);
                if (!reader.IsDBNull(1))
                    logo = reader.GetString(1);
            }
            else
                chkConfirmed(false);
            conn.Close();
        }
        string professionalLogos = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
        string str_path = professionalLogos + logo;
        if (File.Exists(str_path))
        {
            string str_temp_path = TempDirectoryV + @"\" + logo;
    //        string str_temp_http_path = TempDirectoryWebV + @"/" + logo;
            
            
            if (File.Exists(str_temp_path))
                File.Delete(str_temp_path);
            File.Copy(str_path, str_temp_path);
            logoV = logo;
            
        }
    }
    */
    /*
    protected void LinkButton_Command(Object sender, CommandEventArgs e)
    {

        string str_path = (string)(e.CommandArgument);
        if (File.Exists(str_path))
        {
            File.Delete(str_path);
            saveXml();
            
        }
 
    }
     * */

    protected void LinkButtonLogo_Command(Object sender, CommandEventArgs e)
    {
        //Response.Write("You chose: " + e.CommandName + " Item " + e.CommandArgument);
        string str_path = (string)e.CommandArgument;

        if (File.Exists(str_path))
        {
            File.Delete(str_path);
            logoV = "";
            showLogo();
        }
    }
   /*
    protected void showLinkButton()
    {
       
        string professionalImagesThumbnailSuffixWeb = System.Configuration.ConfigurationManager.AppSettings["professionalImagesThumbnailSuffixWeb"];
        string professionalImagesThumbnailSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesThumbnailSuffix"];

        DirectoryInfo _di = new DirectoryInfo(TempDirectoryV + professionalImagesThumbnailSuffix);

        int index = 0;

        Label label; ;
        LinkButton lb;
        Image img;

        Table table = new Table();

        TableRow tableRow = new TableRow();

        table.Rows.Add(tableRow);

      
        table.BorderWidth = 1;
        holderImages.Controls.Clear();
        foreach (FileInfo path in _di.GetFiles())
        {

           
            if (path.Extension != ".db")
            {
                string webImage = TempDirectoryWebV + professionalImagesThumbnailSuffixWeb + path.Name;
   
                index++;

              
                TableCell tableCellImg = new TableCell();
                tableCellImg.Attributes.Add("style", "text-align:center");
                tableRow.Cells.Add(tableCellImg);
                img = new Image();
                img.ImageUrl = webImage;
                img.Width = 80;
                img.Height = 60;
                tableCellImg.Controls.Add(img);

                lb = new LinkButton();
                lb.Text = lbl_delete.Text;
                lb.ID = "lb" + index;
                lb.Command += new CommandEventHandler(LinkButton_Command);
                lb.CommandName = "Delete";
                lb.CommandArgument = path.FullName;

                tableCellImg.Controls.Add(new LiteralControl("<br>"));
                tableCellImg.Controls.Add(lb);

                if (index == 8)
                {
                    MyFileImages.Visible = false;
                    Submit1.Visible = false;
                    lblAddPic.Visible = false;
                }
                
            }
            holderImages.Controls.Add(table);

        }


    }

    */

    protected void showLogo()
    {
        holderLogo.Controls.Clear();
        if (string.IsNullOrEmpty(logoV))           
            return; 
        
        
        string logoPath = logoV;
      
    //    string str_temp_path = TempDirectoryV + @"\" + logoName;
   //     string str_temp_http_path = TempDirectoryWebV + @"/" + logoName;
 //       if (File.Exists(str_temp_path))
 //       {
            //label.Text += "<a href='javascript:void(0);' onclick=\"OpenOutside2('" + str_path_http + "');\">Logo</a>";
           
            LinkButton lb;
            Image img;
            Table tableLogo = new Table();
            TableRow trLogo = new TableRow();
            TableCell tableCellImg = new TableCell();

            tableLogo.BorderWidth = 1;

            
           

            tableCellImg.Attributes.Add("style", "text-align:center");
            trLogo.Cells.Add(tableCellImg);
            img = new Image();
            img.ID = "img_logo";
            img.ImageUrl = logoPath + "?" + DateTime.Now.Millisecond;
            img.Width = 82;
            img.Height = 85;
            tableCellImg.Controls.Add(img);

            tableCellImg.Controls.Add(new LiteralControl("<br>"));

            lb = new LinkButton();
            lb.Text = lbl_delete.Text;
            lb.ID = "lbLogo";
            lb.Command += new CommandEventHandler(LinkButtonLogo_Command);
            lb.CommandName = "Delete";
            lb.CommandArgument = logoPath;
            

            tableCellImg.Controls.Add(lb);
            trLogo.Controls.Add(tableCellImg);
            tableLogo.Controls.Add(trLogo);            
            holderLogo.Controls.Add(tableLogo);
   //     }
    }
    /*
    protected void saveXml()//(string professionId)
    {
        try
        {
            XmlDocument xmlDocument = new XmlDocument();

            XmlDeclaration xmlDeclaration;
            xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "utf-8", "");
            xmlDocument.AppendChild(xmlDeclaration);

            string professionalImagesThumbnailSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesThumbnailSuffix"];

            DirectoryInfo df = new DirectoryInfo(TempDirectoryV + professionalImagesThumbnailSuffix);

            XmlElement xmlRoot = xmlDocument.CreateElement("photos");
            xmlRoot.SetAttribute("path", "images/");
            xmlDocument.AppendChild(xmlRoot);

            FileInfo[] fileInfoArray = df.GetFiles();

            XmlElement xmlItem;

            int i = 0;

            foreach (FileInfo file in fileInfoArray)
            {
                i++;

                if (file.Extension != ".db")
                {
                    xmlItem = xmlDocument.CreateElement("photo");
                    xmlItem.SetAttribute("name", "");
                    xmlItem.SetAttribute("url", file.Name);
                    xmlItem.SetAttribute("fid", i.ToString());
                    xmlItem.InnerText = "";
                    xmlRoot.AppendChild(xmlItem);
                }

            }

            xmlDocument.Save(TempDirectoryV + @"\data.xml");

        }

        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
    }
    */
    protected void UploadBtn_Click(Object sender, EventArgs e)
    {
        
        commentUploadImages.Text = "";
        lblCommentImages.InnerHtml = "";
        String UploadedFile = (MyFileImages.PostedFile == null) ? string.Empty : MyFileImages.PostedFile.FileName;
        if (string.IsNullOrEmpty(UploadedFile))
        {
            commentUploadImages.Text = lbl_comment1.Text;
        }
        else
        {
            
            int ExtractPos = UploadedFile.LastIndexOf("\\") + 1;

            //to retrieve only Filename from the complete path
            String UploadedFileName = UploadedFile.Substring(ExtractPos, UploadedFile.Length - ExtractPos);
            string fileExt = System.IO.Path.GetExtension(UploadedFileName).ToUpper();
            
            string professionalImagesStandardSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesStandardSuffix"];
            string str_path_standard = TempDirectoryV + professionalImagesStandardSuffix + UploadedFileName;

            MyFileImages.PostedFile.SaveAs(str_path_standard);


            ASPJPEGLib.IASPJpeg objJpeg;

            objJpeg = new ASPJPEGLib.ASPJpeg();
            try
            {
                objJpeg.Open(str_path_standard);
            }
            catch (Exception ex)
            {
                if(File.Exists(str_path_standard))
                    File.Delete(str_path_standard);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "InvalidImage", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_ErrorImage.Text) + "');", true);
                return;
            }

            // Decrease image size by 50%
            objJpeg.Width = 300;
            objJpeg.Height = 225;

            // Create thumbnail and save it to disk

            // Compute path to source image
            string professionalImagesThumbnailSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesThumbnailSuffix"];

            string str_path_thumbNail = TempDirectoryV + professionalImagesThumbnailSuffix + UploadedFileName;

            objJpeg.Save(str_path_thumbNail);

     //       saveXml();
   //         showLinkButton();

        }
        

    }


    //Upload logo
    protected void UploadBtn2_Click(Object sender, EventArgs e)
    {
       
        
            lblCommentLogo.Text = "";
            commentUploadImages2.Text = "";

            string UploadedFile = (MyFileLogo.PostedFile == null) ? "" : MyFileLogo.PostedFile.FileName;
            
            if (UploadedFile == "")
            {
                commentUploadImages2.Text = lbl_comment1.Text;
                return;
            }

                    //to retrieve only Filename from the complete path
                FileInfo file_info = new FileInfo(UploadedFile);
                string UploadedFileName = file_info.Name;//UploadedFile.Substring(ExtractPos, UploadedFile.Length - ExtractPos);

                AmazonS3Connector amazonConnector = new AmazonS3Connector();
                string NewName = amazonConnector.UploadFile(MyFileLogo.PostedFile.InputStream, MyFileLogo.PostedFile.FileName, new Guid(GetGuidSetting()));
        /*
                string NewName = Utilities.CleanStringGuid(GetGuidSetting()) + ".jpg";
                   
                  
                string str_path_standard = TempDirectoryV + @"\" + NewName;//professionalLogos + GetGuidSetting() + ".jpg";

                        MyFileLogo.PostedFile.SaveAs(str_path_standard);
        
                        ASPJPEGLib.IASPJpeg objJpeg;
                        objJpeg = new ASPJPEGLib.ASPJpeg();
                        try
                        {
                            objJpeg.Open(str_path_standard);
                        }
                        catch (Exception ex)
                        {
                            dbug_log.ExceptionLog(ex, siteSetting);
                            if (File.Exists(str_path_standard))
                                File.Delete(str_path_standard);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "InvalidImage", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_ErrorImage.Text) + "');", true);
                            return;
                        }
                        if (!string.IsNullOrEmpty(logoV) && File.Exists(TempDirectoryV + @"\" + logoV))
                            File.Delete(TempDirectoryV + @"\" + logoV);
                        // Decrease image size by 50%
                        objJpeg.Width = 82;
                        objJpeg.Height = 85;

                        // Create thumbnail and save it to disk

                        // Compute path to source image
                //        string str_path_thumbNail = professionalLogos + GetGuidSetting() + ".jpg";

        
                        objJpeg.Save(str_path_standard);// (str_path_thumbNail);

             //           Random rnd = new Random();
        */
                        logoV = NewName;
                        showLogo();
                       
                    
                
            
       
    }
    /*
    protected void imageSendPreview_Click(object sender, EventArgs e)
    {
        IfPreviewV = true;
        DescriptionV = HttpUtility.HtmlEncode(tbDescription.Text);
        ShortDescriptionV = HttpUtility.HtmlEncode(tbShortDescription.Text);
        VideoV = GetCheckVideoUrl(tbVideo.Text);
        string csnameStep = "OpenPreview";
       
        string path = ResolveUrl("~")+"professional.aspx";
        string _script = "OpenWin('" + path +"','"+userManagement.IsPublisher().ToString()+"');";
        //update for script
        _updateConfirmed.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), csnameStep, _script, true);
             
           
    }
     * */
    
    private string GetCheckVideoUrl(string _VideoUrl)
    {
        string VideoUrl = string.Empty;
        if (_VideoUrl.Contains("<") || _VideoUrl.Contains(">"))
        {
            int _start = _VideoUrl.IndexOf(@"src=""");
            if (_start == -1)
                return string.Empty;
            _start += 5;
            int _end = _VideoUrl.IndexOf('"', _start);
            if (_end == -1)
                return string.Empty;
            _VideoUrl = _VideoUrl.Substring(_start, _end - _start);
        }
        if (!_VideoUrl.Contains("\""))
            VideoUrl = _VideoUrl;
        
    
        if(!VideoUrl.ToLower().Contains("youtube"))
            return string.Empty;
    //to check if the YouTube address is valid
    //        string s1;
        using (System.Net.WebClient client = new System.Net.WebClient())
        {
            try
            {
                Stream stream = client.OpenRead(VideoUrl);
                //            s1 = client.DownloadString(VideoUrl);
            }
            catch (Exception ex)
            {
                VideoUrl = string.Empty;
            }
        }
         
        return VideoUrl;
    }
    WebReferenceSupplier.AuditEntry SetAuditEntry(string FieldId, WebReferenceSupplier.Status _status,
            string fieldName, string _newValue, string _oldValue, string pageName)
    {

        UserMangement um = GetUserSetting();
        WebReferenceSupplier.AuditEntry ae = new WebReferenceSupplier.AuditEntry();
        ae.AdvertiseId = new Guid(um.GetGuid);
        ae.AdvertiserName = um.User_Name;
        ae.AuditStatus = _status;
        ae.FieldId = FieldId;
        ae.FieldName = fieldName;
        ae.NewValue = _newValue;
        ae.OldValue = _oldValue;
        ae.PageId = pageName;
        ae.UserId = new Guid(userManagement.GetGuid);
        ae.UserName = userManagement.User_Name;
        ae.PageName = page_translate;
        return ae;
    }
    private string CleanEnters(string str)
    {
        if (string.IsNullOrEmpty(str))
            return null;
        string result = str.Replace("\n\r", "");
        result = result.Replace("\n", "");
        return result;
    }
    private bool SaveData()
    {
   
        string _description = tbDescription.Text;
        string _logo = logoV;
   
        Dictionary<string, string> dic = dicValueV;

        if (_description == dic[lblComDescription.ID] && _logo == dic[lbl_MyLogo.ID])
        {
            return true;
        }

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.UpdateLogoDescriptionRequest _request = new WebReferenceSupplier.UpdateLogoDescriptionRequest();
        _request.SupplierId = new Guid(GetGuidSetting());
        _request.Logo = _logo;
        _request.Description = _description;
        WebReferenceSupplier.ResultOfBoolean result = null;
        try
        {
            result = supplier.UpdateSupplierLogoDescription(_request);
            //Response.Write(result);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return false;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return false;
        }
        return result.Value;
        /*
        List<WebReferenceSupplier.AuditEntry> list = new List<WebReferenceSupplier.AuditEntry>();
        WebReferenceSupplier.Status _statusInsert = (IsFirstTimeV)? WebReferenceSupplier.Status.Insert:
            WebReferenceSupplier.Status.Update;
        WebReferenceSupplier.Status _statusDelete = WebReferenceSupplier.Status.Delete;
        string pagename =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        page_translate = EditTranslateDB.GetPageNameTranslate(pagename, siteSetting.siteLangId);

        WebReferenceSupplier.UpsertAdvertiserRequest _request = new WebReferenceSupplier.UpsertAdvertiserRequest();
    //    StringBuilder xml = new StringBuilder();
        _request.IsFromAdvertiserPortal = userManagement.IsSupplier();
        _request.SupplierId = new Guid(GetGuidSetting());
       
        if (LongDescription != dic[lblComDescription.ID])
        {
            _request.LongDescription = LongDescription;
            list.Add(SetAuditEntry(lblComDescription.ID, (string.IsNullOrEmpty(LongDescription) ? _statusDelete : _statusInsert),
                lblComDescription.Text, LongDescription, dic[lblComDescription.ID], pagename));
        }


        
        if (list.Count != 0)
        {
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
            _request.AuditOn = true;
            _request.AuditEntries = list.ToArray();
            WebReferenceSupplier.ResultOfUpsertAdvertiserResponse result = null;
            try
            {
                result = supplier.UpsertAdvertiser(_request);
            }
            catch (Exception ex)
            {
                dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return false;
            }

            if (result.Type == WebReferenceSupplier.eResultType.Failure)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return false;
            }
        }
        
        string FixedLogoDirectory = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
        string _logo = logoV;

        if (!string.IsNullOrEmpty(logoV))
        {
            //     Random _rnd = new Random();
            while (File.Exists(FixedLogoDirectory + _logo))
                File.Delete(FixedLogoDirectory + _logo);
            //           _logo = (_rnd.Next(1000)).ToString() + logoV;
            if (File.Exists(TempDirectoryV + @"\" + _logo))
            {
                File.Copy(TempDirectoryV + @"\" + _logo, FixedLogoDirectory + _logo);
                File.Delete(TempDirectoryV + @"\" + _logo);
            }
        }
        else
        {
            _logo = Utilities.CleanStringGuid(GetGuidSetting()) +@".jpg";
            while (File.Exists(FixedLogoDirectory + _logo))
                File.Delete(FixedLogoDirectory + _logo);
        }
        
        string logoOld = "";
        string command = "EXEC dbo.SetAdvertiser @UserId, @SiteId, @LogoFile";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@UserId", new Guid(GetGuidSetting()));
                cmd.Parameters.AddWithValue("@SiteId", siteSetting.GetSiteID);
                if (string.IsNullOrEmpty(logoV))
                    cmd.Parameters.AddWithValue("@LogoFile", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LogoFile", _logo);
                logoOld = (string)cmd.ExecuteScalar();
                conn.Close();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return false;
            }
        }

        
        string FixedDirectory = CurrentImage + Utilities.CleanStringGuid(GetGuidSetting());
        string suffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesThumbnailSuffix"];

        if (Directory.Exists(FixedDirectory))
           DirectoryCopy._deleteAllFiles(new DirectoryInfo(FixedDirectory));
       else
           Directory.CreateDirectory(FixedDirectory);


       DirectoryCopy.CopyAll(new DirectoryInfo(TempDirectoryV), new DirectoryInfo(FixedDirectory));
        
        
  */
    }
    string ClearVideo(XmlDataDocument xdd)
    {
        foreach (XmlNode node in xdd["object"].ChildNodes)
        {
            if (node.Name == "embed")
            {
                foreach (XmlAttribute att in node.Attributes)
                {
                    if (att.Name == "src")
                        return att.Value;
                }
            }
        }
        return "";
    }
    protected void imageSend_Click(object sender, EventArgs e)
    {
        
        
        if (SaveData())
        {
            cleanSession();

        //    SetDirectorySetting();
            LoadDetail();
 //           SetAdvertiseDB();

            BindDataTemp();
       //     saveXml();
       //     chkRegulations.Enabled = false;
    //        _updateConfirmed.Update();
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Success", "top.UpdateSuccess();", true);
  //          Response.Redirect("ProfessionalUpdate.aspx");
        }
        
    }


    string TempDirectoryV
    {
        get
        {
            return (Session["TempDirectory"] == null) ? string.Empty :
            TempImage + (string)Session["TempDirectory"];
        }
        set { Session["TempDirectory"] = value; }
    }
    string TempDirectoryWebV
    {
        get  
        {
            return (Session["TempDirectory"] == null) ? string.Empty :
            TempWebImage + (string)Session["TempDirectory"];
        }
       
    }
    string CurrentDirectoryV
    {
        get
        {
            return (Session["CurrentDirectory"] == null) ? string.Empty :
            CurrentImage + (string)Session["CurrentDirectory"];
        }
        set { Session["CurrentDirectory"] = value; }
    }
    
    string DescriptionV
    {
        get
        {
            return (ViewState["Description"] == null) ? string.Empty :
                (string)ViewState["Description"];
        }
        set { ViewState["Description"] = value; }
    }
    /*
   string ShortDescriptionV
   {
       get
       {
           return (Session["ShortDescription"] == null) ? string.Empty :
               (string)Session["ShortDescription"];
       }
       set { Session["ShortDescription"] = value; }
   }
      
   string VideoV
   {
       get
       {
           return (Session["tbVideo"] == null) ? string.Empty :
               (string)Session["tbVideo"];
       }
       set { Session["tbVideo"] = value; }
   }
     * * */
    string logoV
    {
        get
        {
            return (ViewState["logoSetting"] == null) ? string.Empty :
                (string)ViewState["logoSetting"];
        }
        set { ViewState["logoSetting"] = value; }
    }
    bool IfPreviewV
    {
        get { return (Session["IfPreview"] == null) ? false : (bool)Session["IfPreview"]; }
        set { Session["IfPreview"] = value; }
    }
    void cleanSession()
    {
        TempDirectoryV = "";       
        CurrentDirectoryV = "";
     //   VideoV = "";
    //    ShortDescriptionV = "";
        DescriptionV = "";
        IfPreviewV = false;
    }
    Dictionary<string, string> dicValueV
    {
        get { return (ViewState["dicValue"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["dicValue"]; }
        set { ViewState["dicValue"] = value; }
    }
    bool IsFirstTimeV
    {
        get { return (ViewState["IsFirstTime"] == null) ? false : (bool)ViewState["IsFirstTime"]; }
        set { ViewState["IsFirstTime"] = value; }
    }
    protected string GetAllowPopUp
    {
        get { return HttpUtility.JavaScriptStringEncode(Hidden_AllowPopUp.Text); }
    }
    protected string GetAllowPopupIE
    {
        get { return HttpUtility.JavaScriptStringEncode(hf_AllowPopupIE.Text); }
    }
    protected string GetConfirmed
    {
        get { return HttpUtility.JavaScriptStringEncode(Hidden_confirmed.Text); }
    }
}
