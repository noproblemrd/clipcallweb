﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyCallDetails.aspx.cs" Inherits="Management_MyCallDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/Toolbox/ToolboxWizardReport.ascx" tagname="Toolbox" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />

    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {       
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }
    function RefundThisCall(elem)
    {
        CleanPageValidators();    
        document.getElementById("<%# div_RefundThis.ClientID %>").style.visibility =
            (elem.checked) ? "visible" : "hidden";
    }
    function RefundTab(elem)
    {
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        try
        {            
            document.getElementById("<%# div_Details.ClientID %>").style.display = "none";            
            if(is_explorer)
                document.getElementById("<%# a_details.ClientID %>").removeAttribute("className");
            else
                document.getElementById("<%# a_details.ClientID %>").removeAttribute("class");
    //        alert(document.getElementById("<%# a_details.ClientID %>").className);
        }
        catch(ex){} 
        try
        { 
            document.getElementById("<%# div_complaints.ClientID %>").style.display = "none";
            if(is_explorer)            
                document.getElementById("<%# a_Complaints.ClientID %>").removeAttribute("className");
            else
                document.getElementById("<%# a_Complaints.ClientID %>").removeAttribute("class");
        }
        catch(ex){} 
        document.getElementById("<%# div_Refund.ClientID %>").style.display = "block";
        elem.className = "selected";
        
    }
    function DetailsTab(elem)
    {
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        try
        {                    
            document.getElementById("<%# div_Refund.ClientID %>").style.display = "none";
            if(is_explorer) 
                document.getElementById("<%# a_Refund.ClientID %>").removeAttribute("className");  
            else
                document.getElementById("<%# a_Refund.ClientID %>").removeAttribute("class");
        }
        catch(ex){}
        try
        {    
            document.getElementById("<%# div_complaints.ClientID %>").style.display = "none"; 
            if(is_explorer)
                document.getElementById("<%# a_Complaints.ClientID %>").removeAttribute("className");
            else 
                document.getElementById("<%# a_Complaints.ClientID %>").removeAttribute("class"); 
        }
        catch(ex){}
        document.getElementById("<%# div_Details.ClientID %>").style.display = "block";
        elem.className = "selected";
        
    }
    function ComplaintTab(elem)
    {
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        try
        {
            document.getElementById("<%# div_Details.ClientID %>").style.display = "none";
            if(is_explorer)
                document.getElementById("<%# a_details.ClientID %>").removeAttribute("className"); 
            else
                document.getElementById("<%# a_details.ClientID %>").removeAttribute("class");            
        }
        catch(ex){}
        try
        {
            document.getElementById("<%# div_Refund.ClientID %>").style.display = "none";
            if(is_explorer)
                document.getElementById("<%# a_Refund.ClientID %>").removeAttribute("className");
            else
                document.getElementById("<%# a_Refund.ClientID %>").removeAttribute("class");            
        }
        catch(ex){}
        document.getElementById("<%# div_complaints.ClientID %>").style.display = "block";
        elem.className = "selected";
        
    }
    function CheckRefund()
    {
        var IsValid = true;
        var RefundReasons = document.getElementById("<%# lbl_ValidateRefundReasons.ClientID %>");
        RefundReasons.style.display = "none";
        if (!Page_ClientValidate('Refund'))
            IsValid = false;
        var ddl_RefundReasons = document.getElementById("<%# ddl_RefundReasons.ClientID %>");
        if(ddl_RefundReasons.selectedIndex == 0)
        {
            IsValid = false;
            RefundReasons.style.display = "inline";
        }           
        if(document.getElementById("<%# hf_UserType.ClientID %>").value == "supplier")
        {
            var RefundCondition = document.getElementById("<%# cb_RefundCondition.ClientID %>");
            if(RefundCondition && !RefundCondition.checked)
            {
                alert("<%# lbl_refundCheckValid.Text %>");
                IsValid = false;
            }
            if(document.getElementById("<%# txt_RefundDescription.ClientID %>").value.length == 0)
                alert("<%# lbl_PleaseAddComment.Text %>");
        }
        return IsValid;
    }
    function CleanPageValidators()
    {
       
        for (var i = 0;i < Page_Validators.length;  i++)
        {      
		    Page_Validators[i].style.visibility = "hidden";// = "none";	
        } 
    }
    //not in used
    function IframeClosed()
    {
    alert('tryh');
        parent.CloseIframe();
        <%# Get_run_report() %>;
    }
    function CloseIframe_after()
    {
        <%# Get_run_report() %>;
    }
    ///////

    // confirm refund
    function ConfirmedRefund()
    {
        var _result = confirm("<%# GetConfirmRefundText %>");
        if(_result){
            showDiv();
            <%# confirm_refund() %>;
        }
    }

    function CheckAllowToRecord()
    {
        if (!document.getElementById("<%# cb_AllowToRecord.ClientID %>").checked)
        {
            alert("<%# GetAllowToRecord %>");
            return false;
        }
        return true;
    }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
        </cc1:ToolkitScriptManager>
     
        <div class="tabsM">
            <ul>
                <li>
                    <a runat="server" id="a_details" href="javascript:void(0);" onclick="DetailsTab(this);" class="selected">Details</a>
                </li>
                <li runat="server" id="li_Refund">
                    <a runat="server" id="a_Refund" href="javascript:void(0);" onclick="RefundTab(this);">Refund</a>
                </li>
                <li runat="server" id="li_Complaints">
                    <a id="a_Complaints" href="javascript:void(0);" runat="server" onclick="ComplaintTab(this);" >Complaints</a>
                </li>
            </ul>
        </div>
      
        
        
        
        <%// DETAILS %>
        <div id="div_Details" class="containertabM" runat="server">
        
            <div class="complaint_rowM"> 
            
                <div class="form-fieldtabM" runat="server" id="div_WinningPrice"> 
                    <asp:Label ID="lbl_WinningPrice" runat="server" CssClass="label" Text="Winning Price"></asp:Label>:
                    <asp:TextBox ID="txt_WinningPrice" runat="server" CssClass="form-textcompM Label_ro" ReadOnly="true"></asp:TextBox>
                </div>
           
                 <div class="form-fieldtabM"> 
                    <asp:Label ID="lbl_Price" runat="server" CssClass="label" Text="Price"></asp:Label>:
                    <asp:TextBox ID="txt_Price" runat="server" CssClass="form-textcompM Label_ro" ReadOnly="true"></asp:TextBox>
                </div>
             
                 <div class="form-fieldtabM">
                    <asp:Label ID="lbl_CustomerPhone" runat="server" CssClass="label" Text="Customer Phone"></asp:Label>:
                    <asp:TextBox ID="txt_CustomerPhone" runat="server" CssClass="form-textcompM Label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                  
                
                <div class="form-fieldtabM">
                <div runat="server" id="div_WorkDone">
                    <asp:Label ID="lbl_WorkDone" runat="server" CssClass="label"  Text="Work Done"></asp:Label>:
                    <asp:DropDownList ID="ddl_WorkDone" CssClass="form-selectcompM" runat="server">
                    </asp:DropDownList>
                 </div>   
              </div>
             
            </div>
         
           
          
            <div class="clear"></div>
            
              
            <div class="complaint_row2M"> 
             
                <div class="form-fieldlarge2M"> 
                 
                    <asp:Label ID="lbl_DescriptionCall" runat="server" CssClass="label" Text="<%# lblDescription.Text %>"></asp:Label>:<br />
                    <asp:TextBox ID="txt_DescriptionCall" runat="server" CssClass="form-textarealarge2M Label_ro" ReadOnly="true" Rows="4"
                     TextMode="MultiLine"></asp:TextBox>
                   
                </div>
              
                <div class="clear"></div>
                <div class="save2M" runat="server" id="div_submitWorDone">
                    <asp:Button ID="btn_Submit" runat="server" Text="Submit" CssClass="form-submit" OnClick="btn_WorkWasDone_click" />
                </div>
                 
               
            </div>
           
            
        </div>
        
        <%// REFUND %>
        <div id="div_Refund" style="display:none;" class="containertabM" runat="server">
        <asp:UpdatePanel ID="_UpdatePanel_refund" runat="server">
        <ContentTemplate>
        
            <asp:HiddenField ID="hf_UserType" runat="server" />
            <div class="complaint_row4M">
             <div class="form-fieldtab2M">
                <div id="div_RefundThisCall" runat="server" visible="false">
                    <asp:CheckBox ID="cb_RefundThisCall" runat="server" onclick="javascript:RefundThisCall(this);" />
                    <label for="<%# cb_RefundThisCall.ClientID %>">
                        <asp:Label ID="lbl_RefundThisCall" runat="server" Text="I would like to ask for refund for this call"></asp:Label>
                    </label>
                    
                </div>
                <div id="div_RefundExceeded" runat="server" visible="false">

                    <asp:Label ID="lbl_RefundExceeded" runat="server" Text="You have exceeded the refund request presentage"></asp:Label>
                </div>
                <div id="div_RefundOver" runat="server" visible="false">

                    <asp:Label ID="lblRefundOver" runat="server" Text="<%# GetRefundOver() %>"></asp:Label>
                </div>
                
                <div id="div_NoPhoneConnection" runat="server" visible="false">

                    <asp:Label ID="lbl_NoPhoneConnection" runat="server" Text="You can't ask for refund for calls that weren't been answered"></asp:Label>
                </div>

                <div id="div_AccountShouldBeAvailable" runat="server" visible="false">

                    <asp:Label ID="lbl_AccountShouldBeAvailable" runat="server" Text="In order to ask for refund your account should be available."></asp:Label>
                </div>

                <div id="div_NoRecord" runat="server" visible="false">

                    <asp:Label ID="lbl_NoRecord" runat="server" Text="You can't ask for refund for calls that weren’t been recorded"></asp:Label>
                    <div runat="server" id="div_AllowToRecord" visible="false" class="div_AllowToRecord">
                        <asp:CheckBox ID="cb_AllowToRecord" runat="server" Text="I would like to start record my calls so I'll be able to ask for refunds in the future" />
                        <asp:Button ID="btn_AllowToRecord" runat="server" Text="Save" OnClientClick="return CheckAllowToRecord();"
                         OnClick="btn_AllowToRecord_Click" CssClass="form-submit" style="display:inline-block;" />
                    </div>
                </div>
               </div> 
              </div> 
    <div id="div_RefundThis" style="visibility:hidden;" runat="server">
              <div class="complaint_rowM">
                 
                    <div id="div_BlackList" runat="server" class="form-fieldtabxM">
                        <asp:CheckBox ID="cb_BlackList" runat="server" />
                        <label for="<%# cb_BlackList.ClientID %>">
                            <asp:Label ID="lbl_BlackList" runat="server" Text="Send to black list"></asp:Label>
                        </label> 
                    </div>
               
                
              <div class="form-fieldtabM">
                    <asp:Label ID="lbl_RefundReasons" runat="server" CssClass="label"  Text="Refund reasons"></asp:Label>
                    <asp:TextBox ID="txt_RefundReasons" runat="server" CssClass="form-textcompM Label_ro" ReadOnly="true"></asp:TextBox>
                    <asp:DropDownList ID="ddl_RefundReasons" CssClass="form-selectcompM" runat="server">
                    </asp:DropDownList>
                    <asp:Label ID="lbl_ValidateRefundReasons" runat="server"  Text="Missing" CssClass="error-msg"
                     style="display:none;"></asp:Label>
              </div>
               
                
                <div runat="server" id="div_RefundStatus" class="form-fieldtabM">
                    <asp:Label ID="lbl_RefundStatus" runat="server" CssClass="label"  Text="Refund status"></asp:Label>
                    <asp:TextBox ID="txt_RefundStatus" runat="server" CssClass="form-textcompM Label_ro"  ReadOnly="true"></asp:TextBox>
                    <asp:DropDownList ID="ddl_RefundStatus" CssClass="form-selectcompM" runat="server">
                    </asp:DropDownList>
                </div>
                
                
               </div> 
             
                 
                <div class="clear"></div>
               <div class="complaint_rowM">
               
                    <div runat="server" id="div_RefundDescription" class="form-fieldtxtarea2M">
                        <asp:Label ID="lbl_RefundDescription" runat="server"  CssClass="label" Text="Description"></asp:Label>
                        <asp:TextBox ID="txt_RefundDescription" runat="server" CssClass="form-textarea_comp1M" TextMode="MultiLine" Rows="5"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator_RefundDescription" 
                        runat="server" ErrorMessage="Missing" ValidationGroup="Refund" CssClass="error-msg"
                        Display="Static" ControlToValidate="txt_RefundDescription"></asp:RequiredFieldValidator>
                    </div>    
                   
                   
                   
                   
                    <div runat="server" id="div_RefundReplay" class="form-fieldtxtarea2M">
                        <asp:Label ID="lbl_RefundReplay" runat="server" CssClass="label" Text="Publisher comment"></asp:Label>
                        <asp:TextBox ID="txt_RefundReplay" runat="server" CssClass="form-textarea_comp1M" TextMode="MultiLine" Rows="5" 
                        ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator_RefundReplay" 
                        runat="server" ErrorMessage="Missing" ValidationGroup="Refund" CssClass="error-msg"
                        Display="Static" ControlToValidate="txt_RefundReplay"></asp:RequiredFieldValidator>
                    </div>
                   
                 </div> 
                 <div class="clear"><!-- --></div>
                   <div class="complaint_rowM2"> 
                        <div runat="server" id="div_RefundCondition">
                            <span class="terms" runat="server" id="span_term" >
                                <asp:CheckBox ID="cb_RefundCondition" runat="server"/>                            
                                <label  id="label_refund">
                                    <asp:Label ID="lbl_AgreeRequest" runat="server" Text="By sending the refund request I agree to the"></asp:Label>
                                </label>
                                <a href="javascript:void(0);" id="a_termNcondition" runat="server" target="_blank">
                                    <asp:Label ID="lbl_TermsConditions" runat="server" Text="I accept terms and conditions"></asp:Label>
                                </a>                    
                            </span>
                        </div>
                     </div>
                      <div class="form-fieldtabMcheck">
                       
                            <div runat="server" id="div_RefundSubmit">
                                <asp:Button ID="btn_RefundSubmit" runat="server" Text="Submit"
                                 OnClick="btn_RefundSubmit_click" ValidationGroup="Refund"  CssClass="saveM22" OnClientClick="return CheckRefund();" />
                            </div>
                      
                       </div>
            </div> 
            </div>
        </ContentTemplate>
        </asp:UpdatePanel>
        </div>
        <%// COMPLAINTS %>
        <div runat="server" id="div_complaints" style="display:none;">
        <div style="margin-top:30px;">
            <uc1:Toolbox ID="_Toolbox" runat="server" />
        </div>
        <div class="tablecomp">
        <asp:UpdatePanel ID="_UpdatePanel_Complaints" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false"
                CssClass="data-table" AllowPaging="True"  Width="630px"
                onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />        
            <FooterStyle CssClass="footer"  />
            <PagerStyle CssClass="pager" />
            <Columns>                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label01" runat="server" Text="<%# lbl_All.Text %>"></asp:Label>
                    <br />
                    <asp:CheckBox ID="cb_all" runat="server" >
                    </asp:CheckBox>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cb_choose" runat="server" />                    
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label03" runat="server" Text="<%# lbl_ID.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    
                   <asp:LinkButton ID="lb_ID" runat="server" Text="<%# Bind('ID') %>" OnClientClick="<%# Bind('ScriptComplaint') %>"></asp:LinkButton>        
                    <asp:Label ID="lbl_guid" runat="server" Text="<%# Bind('guid') %>" Visible="false"></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label04" runat="server" Text="<%# lbl_CreatedOn.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>            
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label05" runat="server" Text="<%# lbl_Title.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text="<%# Bind('Title') %>"></asp:Label>            
                </ItemTemplate>
                </asp:TemplateField>
                                
                
                <asp:TemplateField>              
                <HeaderTemplate>
                    <asp:Label ID="Label07" runat="server" Text="<%# lbl_Status.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text="<%# Bind('Status') %>"></asp:Label>            
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>              
                <HeaderTemplate>
                    <asp:Label ID="Label08" runat="server" Text="<%# lbl_FollowUp.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text="<%# Bind('FollowUp') %>"></asp:Label>            
                </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
            </asp:GridView>
        </ContentTemplate>
        </asp:UpdatePanel>
        </div>
        </div>
        
    
    <div id="divLoader" class="divLoader">   
    <table>
        <tr>
            <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
        </tr>
    </table>
    </div>
    <asp:Label ID="lblDescription" runat="server" Text="Description" Visible="false"></asp:Label> 
    <div style="display:none;">
        <asp:Button ID="btn_complaints_virtual" runat="server" Text="Button" style="display:none;" />
    </div>

    <%// REFUND SENTENCES %>
    <asp:Label ID="lbl_ChooseItem" runat="server" Visible="false" Text="Choose"></asp:Label>
    <asp:Label ID="lbl_refundCheckValid" runat="server" Text="You need to accept the terms" style="display: none;" CssClass="error-msg"></asp:Label>
    
    <%// COMPAINTS SENTENCES %>
    <asp:Label ID="lbl_All" runat="server" Text="All" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ID" runat="server" Text="ID" Visible="false"></asp:Label>
    <asp:Label ID="lbl_CreatedOn" runat="server" Text="Created on" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Title" runat="server" Text="Title" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Status" runat="server" Text="Status" Visible="false"></asp:Label>    
    <asp:Label ID="lbl_FollowUp" runat="server" Text="Follow up" Visible="false"></asp:Label>
    
    <asp:Label ID="lbl_TicketNumber" runat="server" Text="Ticket number" Visible="false"></asp:Label>
    <asp:Label ID="lblRefundThisCall" runat="server" Text="Refund this call" Visible="false"></asp:Label>
    <asp:Label ID="lbl_PleaseAddComment" runat="server" Text="Please add comment" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ErrorTicket" runat="server" Text="There are errors with this call, please try again" Visible="false"></asp:Label>

    <asp:Label ID="lbl_ConfirmRefund" runat="server" Text="You have exceeded the refund requests percentage. After sending this request we will hold your campaign for 30 days in order to investigate those requests and make sure you'll get quality leads in the future. Do you wish to send this refund request and freeze you campaign?" Visible="false"></asp:Label>
    <asp:Label ID="lbl_CheckAllowToRecprd" runat="server" Text="You have to mark the check box" Visible="false"></asp:Label>
    <asp:Label ID="lbl_RefundOver" runat="server" Text="You can't ask for refund for calls that has been created more than {0} days ago" Visible="false"></asp:Label>
    <asp:Label ID="lbl_CampaignOnHold" runat="server" Text="Thank you. Your request has been sent to our customer service and you campaign is now on hold" Visible="false"></asp:Label>
    <asp:Label ID="lbl_RecoededFromNow" runat="server" Text="Thank you. Your calls will be recorded from this moment on" Visible="false"></asp:Label>

    </form>
</body>
</html>
