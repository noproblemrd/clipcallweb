﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Drawing;
using System.Text.RegularExpressions;

public partial class Management_professionalDetails2014 : PageSetting
{
    protected string nameHolder;
    protected string companyAddressHolder;
    protected string ContactPersonHolder;
    protected string mobileNumberHolder;
    protected string emailHolder;
    protected string faxHolder;
    protected string websiteHolder;
    protected string aboutHolder;
    protected string numEmployeesHolder;
    protected string videoHolder;
    

    protected string regFormatPhone;
    protected string mobileError;
    protected string businessAdressError;
    protected string businessNameError;
    protected string emailError;
    protected string photoMissing;
    protected string photoFormat;
    protected string photoSave;
    protected string photoLookfor;
    protected string photoDelete;
    protected string videoAlready;
    protected string videoPath;
    protected string paymentsError;


    protected string mobile;
    protected string companyName;
    protected string businessAddress;

    protected string UpdateSuccess;
    protected string UpdateFaild;
    protected string NoNewData;
   
 
    protected const string pattern = @"(?:https?:\/\/)?(?:www\.)?(?:(?:(?:youtube.com\/watch\?[^?]*v=|youtu.be\/)([\w\-]+))(?:[^\s?]+)?)";
    const string replacement = "http://www.youtube.com/embed/$1";


    protected void Page_Load(object sender, EventArgs e)
    {
        
        LoadPageText();

        SetTextFromXml();

        string whichControlMakePostback = Request.Form["__EVENTTARGET"];

        if (!IsPostBack)
        {
            arrayWhoStay = new ArrayList();            
            arrayWhoInInit = new ArrayList();
            dicVideoStay=new Dictionary<int,string>();
            arrayImagesWhoStay = new ArrayList();

            loadDetails();
            LoadPanelServices(false);
            LoadLogo();
            
            
        }

        else
        {
            if (whichControlMakePostback == "btnAddVideo" && !string.IsNullOrEmpty(txtAddVideo.Text))
            {
                index++;                
            }


            LoadPanelServices(true);

            indexImages++;

            

            if (searchGoogle1.region.Length > 0)
            {
                string _script = "setChooseFromList(true);"; // in post back make sure when the field id fulfilled make flad that is clciked from list
                Page.ClientScript.RegisterStartupScript(this.GetType(), "setChooseFromList", _script, true);
            }


        }


        string ifLogoChanged = Request.Form["hidden_changeLogo"];
        string ifPhotoChanged = Request.Form["hidden_changePhoto"];
        
        

        if (ifLogoChanged == "true")
        {
            uploadLogo();
            hidden_changeLogo.Value = "false";
        }

        if (ifPhotoChanged == "true")
        {
            uploadPhoto();
            hidden_changePhoto.Value = "false";
        }

        /*
        if (whichControlMakePostback == "lb_uploadImages1")
        {
            uploadPhoto();
        }
        */


        LoadImagesAndBuild();

        txt_name.Attributes["place_holder"] = nameHolder;
        txt_mobile.Attributes["place_holder"] = mobileNumberHolder;
        txt_ContactPerson.Attributes["place_holder"] = ContactPersonHolder;
        txt_fax.Attributes["place_holder"] = faxHolder;
        txt_email.Attributes["place_holder"] = emailHolder;
        txt_website.Attributes["place_holder"] = websiteHolder;
        txt_about.Attributes["place_holder"] = aboutHolder;
        txt_numEmployees.Attributes["place_holder"] = numEmployeesHolder;

        regFormatPhone = PpcSite.GetCurrent().PhoneReg;// DBConnection.GetPhoneRegularExpression(SiteId); // for usa ^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$ 
        //regFormatPhone = regFormatPhone.Replace("\\", "\\\\");
        hidden_phoneFormat.Value = regFormatPhone;              

        Page.DataBind();

       
    }


    private void loadDetails()
    {
        index = 0;
        arrayWhoInInit.Clear();

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfGetBusinessProfileResponse resultOfGetBusinessProfileResponse = new WebReferenceSupplier.ResultOfGetBusinessProfileResponse();

        try
        {
            
            resultOfGetBusinessProfileResponse = _supplier.GetBusinessProfile(new Guid(GetGuidSetting()));

            string csnameStep = "setStep";
            Type cstypeStep = this.GetType();

            ClientScriptManager csStep = this.Page.ClientScript;
            string csTextStep = "";

            if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
            {
                csTextStep = "witchAlreadyStep(0,'click');";
                csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
            }

            if (resultOfGetBusinessProfileResponse.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed GetBusinessDetails_Registration2014 " + resultOfGetBusinessProfileResponse.Messages[0]));

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
            }

            else
            {               
                
                txt_name.Value = resultOfGetBusinessProfileResponse.Value.Name;
                txt_ContactPerson.Value = resultOfGetBusinessProfileResponse.Value.ContactPerson;

                if (!String.IsNullOrEmpty(resultOfGetBusinessProfileResponse.Value.FullAddress))
                    searchGoogle1.region = resultOfGetBusinessProfileResponse.Value.FullAddress;

                decimal lat = resultOfGetBusinessProfileResponse.Value.Longitude;
                searchGoogle1.lat = lat;

                decimal lng = resultOfGetBusinessProfileResponse.Value.Longitude;
                searchGoogle1.lng = lng;

                txt_mobile.Value=resultOfGetBusinessProfileResponse.Value.Phone;
                txt_fax.Value = resultOfGetBusinessProfileResponse.Value.Fax;
                txt_email.Value=resultOfGetBusinessProfileResponse.Value.Email;
                txt_website.Value=resultOfGetBusinessProfileResponse.Value.Website;
                txt_about.Text = resultOfGetBusinessProfileResponse.Value.About;
                txt_numEmployees.Value = Convert.ToString(resultOfGetBusinessProfileResponse.Value.NumberOfEmployees);

                string[] videos = resultOfGetBusinessProfileResponse.Value.Videos;

                if (videos.Length == 0)
                {
                    //index++;
                }

                else
                {
                    for (int i = 0; i < videos.Length; i++)
                    {
                        arrayWhoInInit.Add(videos[i]);
                        index++;
                    }
                }


                /*
                        public bool PayWithAMEX { get; set; }
                        public bool PayWithMASTERCARD { get; set; }
                        public bool PayWithVISA { get; set; }
                        public bool PayWithCASH { get; set; }

                */

                  chkAmex.Checked=resultOfGetBusinessProfileResponse.Value.PayWithAMEX;
                  chkMasterCard.Checked=resultOfGetBusinessProfileResponse.Value.PayWithMASTERCARD; 
                  chkVisa.Checked=resultOfGetBusinessProfileResponse.Value.PayWithVISA;
                  chkCash.Checked = resultOfGetBusinessProfileResponse.Value.PayWithCASH;                       
                       

            }

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
        }


    }


   

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;


        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "BusinessDetails"
                          select x).FirstOrDefault();

        nameHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("CompanyName").Value;
        companyAddressHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("CompanyAddress").Value;
        mobileNumberHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("MobileNumber").Value;
        ContactPersonHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("ContactPersonName").Value;
        faxHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("fax").Value;
        websiteHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("website").Value;
        aboutHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("about").Value;
        numEmployeesHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("numEmployees").Value;
        videoHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("video").Value;
       

        businessNameError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("BusinessName").Value;
        mobileError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("MobileNumber").Value;
        businessAdressError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("BusinessAdress").Value;
        photoMissing = xelem.Element("Validated").Element("Instruction").Element("Error").Element("photoMissing").Value;
        photoFormat = xelem.Element("Validated").Element("Instruction").Element("Error").Element("photoFormat").Value;
        photoSave = xelem.Element("Validated").Element("Instruction").Element("Error").Element("photoSave").Value;
        photoLookfor = xelem.Element("Validated").Element("Instruction").Element("Error").Element("photoSave").Value;
        photoDelete = xelem.Element("Validated").Element("Instruction").Element("Error").Element("photoDelete").Value;
        videoAlready = xelem.Element("Validated").Element("Instruction").Element("Error").Element("videoAlready").Value;
        videoPath = xelem.Element("Validated").Element("Instruction").Element("Error").Element("videoPath").Value;
        paymentsError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("paymentsError").Value;

        xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "login"
                          select x).FirstOrDefault();

        emailHolder = xelem.Element("Email").Element("Validated").Element("Instruction").Element("General").Value;       
        emailError = xelem.Element("Email").Element("Validated").Element("Error").Value;
        
    }


    void SetTextFromXml()
    {
        XElement xelem = GetSentences();
        UpdateSuccess = xelem.Element("FormMessages").Element("UpdateSuccess").Value;
        UpdateFaild = xelem.Element("FormMessages").Element("UpdateFaild").Value;
        NoNewData = xelem.Element("FormMessages").Element("NoNewData").Value;

    }

    XElement GetSentences()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "MasterPage"
                          select x).FirstOrDefault();
        return xelem;
    }


   
    private void LoadPanelServices(bool isPostback)
    {           

        blnVideoPath = true;
        ContainerVideos.Controls.Clear();        

        for (int i = 0; i < index; i++)
        {

            /* example how it looks in html
              <div class="inputCategoryContainer" id="inputCategoryContainer1">
                  <input type="text" id="pt0" style="display: inline;" class="_Heading textActive place-holder ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
                  <input type="text" holderclass="place-holder" place_holder="e.g., plumber, computer technician, etc." class="_Heading textActive ui-autocomplete-input" id="txt_Category1" name="ctl00$ContentPlaceHolder1$txt_Category1" style="display: none;" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
                  <div style="display:none;" class="inputValid" id="div_ServiceSmile1"></div>
                  <div style="display:none;" class="inputError" id="div_ServiceError1"></div>
                  <div style="display:none;" class="inputComment" id="div_ServiceComment1">e.g., plumber, computer technician, etc.
                      <div class="chat-bubble-arrow-border" id="chatBubbleArrowBorder1"></div>
                      <div class="chat-bubble-arrow" id="chatBubbleArrow1"></div>
                 </div>
                 <div class="clear" id="clear1"></div>
             </div>
            */

            /* must set here in page load and not in click events to make view state for the controls !!!*/

            //if (!arrayWhoStay.Contains(i)) // because ther isn't value zero all time in the beginnig of loop add max number to arrayWhoStay
            arrayWhoStay.Add(Convert.ToInt32(GetMaxValue(arrayWhoStay) + 1));          
            
                HtmlGenericControl inputCategoryContainerVideo = new HtmlGenericControl("div");
                inputCategoryContainerVideo.ID = "inputVideoContainer" + arrayWhoStay[i];
                inputCategoryContainerVideo.Attributes["class"] = "inputVideoContainer";

                
                HtmlGenericControl youtubeIframe = new HtmlGenericControl("iframe");

                //<iframe width="560" height="315" src="//www.youtube.com/embed/jo0-E64MKNM" frameborder="0" allowfullscreen></iframe>
                youtubeIframe.ID = "iframeYoutube" + arrayWhoStay[i];
                youtubeIframe.Attributes["id"] = "iframeYoutube" + arrayWhoStay[i];
                youtubeIframe.Attributes["width"] = "122px";
                youtubeIframe.Attributes["height"] = "82px";

                // need to convert from regular link off the all page http://www.youtube.com/watch?v=RanylSGvzlw to http://youtu.be/RanylSGvzlw
                // or need to convert from share to embed like http://youtu.be/jo0-E64MKNM to http://www.youtube.com/embed/jo0-E64MKNM                

                if (!isPostback)
                {
                    if (arrayWhoInInit.Count > 0)
                    {
                        if (arrayWhoInInit[i].ToString().LastIndexOf('=') != -1) // link of page
                            youtubeIframe.Attributes["src"] = arrayWhoInInit[i].ToString() + "?controls=0";
                        else // link of share
                            youtubeIframe.Attributes["src"] = arrayWhoInInit[i].ToString() + "?controls=0";

                        dicVideoStay.Add(Convert.ToInt32(arrayWhoStay[i]), youtubeIframe.Attributes["src"]);                     

                    }
                }

                else
                {                    
                   
                    if (i == (index - 1)) // last one is the new one from text box
                    {
                        

                        if (!string.IsNullOrEmpty(txtAddVideo.Text))
                        {                        
                            
                           

                            var rgx = new Regex(pattern);
                            var result = rgx.Replace(txtAddVideo.Text, replacement);

                            try
                            {
                                if (txtAddVideo.Text.LastIndexOf('=') != -1) // link of page
                                    youtubeIframe.Attributes["src"] = result + "?controls=0";
                                else
                                    youtubeIframe.Attributes["src"] = "http://www.youtube.com/embed/" + txtAddVideo.Text.Substring(txtAddVideo.Text.IndexOf('/')) + "?controls=0";

                                if (dicVideoStay.ContainsValue(youtubeIframe.Attributes["src"]))
                                {
                                    index--;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "showVideoAlready", "showVideoAlready();", true);
                                    txtAddVideo.Text = "";
                                    blnVideoPath = false;
                                }

                                else
                                    dicVideoStay.Add(Convert.ToInt32(arrayWhoStay[i]), youtubeIframe.Attributes["src"]);
                            }

                            catch(Exception ex)
                            {
                                index--;
                                dbug_log.ExceptionLog(ex);
                                txtAddVideo.Text = "";
                                blnVideoPath = false;   
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
                            }

                           
                        }

                        else // if its is empty use the current one
                        {
                            youtubeIframe.Attributes["src"] = dicVideoStay[Convert.ToInt32(arrayWhoStay[i])];
                        }
                    }

                    else
                    {
                        youtubeIframe.Attributes["src"] = dicVideoStay[Convert.ToInt32(arrayWhoStay[i])];
                    }

                }

                youtubeIframe.Attributes["frameborder"] = "0";
                youtubeIframe.Attributes["allowfullscreen"] = "true";      

               
               

                if (blnVideoPath)
                {
                    inputCategoryContainerVideo.Controls.Add(youtubeIframe);

                    HtmlGenericControl videoDelete = new HtmlGenericControl("div");
                    videoDelete.ID = "videoDelete" + arrayWhoStay[i];
                    videoDelete.Attributes["class"] = "videoDelete";
                    inputCategoryContainerVideo.Controls.Add(videoDelete);

                    HtmlGenericControl videoDeleteText = new HtmlGenericControl("div");
                    videoDeleteText.ID = "videoDeleteText" + arrayWhoStay[i];
                    videoDeleteText.Attributes["class"] = "videoDeleteText";
                    videoDelete.Controls.Add(videoDeleteText);


                    LinkButton lbDeleteVideo = new LinkButton();
                    lbDeleteVideo.ID = "lbDeleteVideo" + arrayWhoStay[i];
                    lbDeleteVideo.Attributes["ID"] = "lbDeleteVideo" + arrayWhoStay[i];
                    lbDeleteVideo.Command += new CommandEventHandler(lb_click_minus);
                    lbDeleteVideo.CommandName = "DeleteVideo";
                    lbDeleteVideo.CommandArgument = arrayWhoStay[i].ToString();
                    lbDeleteVideo.Text = "Delete";

                    videoDeleteText.Controls.Add(lbDeleteVideo);


                    inputCategoryContainerVideo.Controls.Add(videoDelete);


                    ContainerVideos.Controls.Add(inputCategoryContainerVideo);
                }
            
          
        }
       
    }

    protected void lb_click_minus(object sender, CommandEventArgs e)
    {
        /************  remove the specific control *************/
        Control myControl1 = pnlAddVideos.FindControl("inputVideoContainer" + e.CommandArgument);
        ContainerVideos.Controls.Remove(myControl1);

        index--;

        arrayWhoStay.Remove(Convert.ToInt32(e.CommandArgument));

        dicVideoStay.Remove(Convert.ToInt32(e.CommandArgument));

        NextBusinessDetails_Click(sender, e);

        foreach (Control cn in pnlAddVideos.Controls)
        {

        }       

    }

    protected void NextBusinessDetails_Click(object sender, EventArgs e)
    {
        /************  remove the last control added in page load *************/        
        
        if (blnVideoPath)
        {
            WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
            WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
            WebReferenceSupplier.SetBusinessProfileDataRequest businessProfileDataRequest = new WebReferenceSupplier.SetBusinessProfileDataRequest();

            try
            {
                businessProfileDataRequest.SupplierId = new Guid(GetGuidSetting());
                businessProfileDataRequest.Name = txt_name.Value;
                businessProfileDataRequest.ContactPerson = txt_ContactPerson.Value;
                businessProfileDataRequest.FullAddress = searchGoogle1.region;
                businessProfileDataRequest.Phone = Utilities.GetCleanPhone(txt_mobile.Value);
                businessProfileDataRequest.Fax = txt_fax.Value;
                //businessProfileDataRequest.Email = txt_email.Value; read only
                businessProfileDataRequest.Website = txt_website.Value;
                businessProfileDataRequest.About = txt_about.Text;

                if (!String.IsNullOrEmpty(txt_numEmployees.Value))
                {
                    int numEmployees;
                    if (Int32.TryParse(txt_numEmployees.Value, out numEmployees))
                    {
                        businessProfileDataRequest.NumberOfEmployees = numEmployees;
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "SetServerComment('Number of employees must be a number');", true);
                }



                businessProfileDataRequest.Latitude = searchGoogle1.lat;
                businessProfileDataRequest.Longitude = searchGoogle1.lng;

                List<string> listVideo = new List<string>();

                foreach (Control cnOuter in ContainerVideos.Controls)
                {

                    foreach (Control cnInner in cnOuter.Controls)
                    {

                        if (cnInner.ID.IndexOf("iframeYoutube") != -1)
                        {
                            HtmlGenericControl youtubeIframe = new HtmlGenericControl("iframe");
                            youtubeIframe = (HtmlGenericControl)cnInner;
                            if (!String.IsNullOrEmpty(youtubeIframe.Attributes["src"]))
                                listVideo.Add(youtubeIframe.Attributes["src"].Replace("?controls=0", ""));

                        }
                    }

                }


                businessProfileDataRequest.Videos = listVideo.ToArray();

                businessProfileDataRequest.PayWithAMEX = chkAmex.Checked;
                businessProfileDataRequest.PayWithMASTERCARD = chkMasterCard.Checked;
                businessProfileDataRequest.PayWithVISA = chkVisa.Checked;
                businessProfileDataRequest.PayWithCASH = chkCash.Checked;

                result = _supplier.SetBusinessProfileData(businessProfileDataRequest);


                if (result.Type == WebReferenceSupplier.eResultType.Success)
                {
                    loadDetails();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "UpdateSuccess();", true);

                }

                else
                {
                    dbug_log.ExceptionLog(new Exception("failed businessDetails_Registration2014 " + result.Messages[0]));

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
                    return;
                }

                txtAddVideo.Text = "";

            }

            catch (Exception ex)
            {
                dbug_log.ExceptionLog(ex);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
                return;
            }
        }
    }

    protected void lbAddButton_Click(object sender, EventArgs e)
    {
        /************  remove the last control added in page load for video*************/

        //Response.Write(pnlImages.Controls.Count);

        /*
        if (pnlImages.Controls.Count > 0)
        {
            pnlImages.Controls.RemoveAt(pnlImages.Controls.Count - 1);
            indexImages--;
        }
       */


        ////for future but for pnlImages arrayWhoStay.RemoveAt(arrayWhoStay.Count - 1);
        ////updatePanel1.Update();
    }

    public int GetMaxValue(ArrayList arrList)
    {
        ArrayList copyList = new ArrayList(arrList);

        if (copyList == null || copyList.Count == 0)
        {
            return 0;
        }

        else
        {
            copyList.Sort();

            copyList.Reverse();

            return Convert.ToInt32(copyList[0]);
        }

    }

    void LoadLogo()
    {        
        string command = "EXEC dbo.GetAdvertiserDetails @UserId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", new Guid(GetGuidSetting()));
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {               
                if (!reader.IsDBNull(1))
                    logo = reader.GetString(1);
            }
            
            conn.Close();
        }

        //Response.Write("logo: " + ConfigurationManager.AppSettings["professionalLogosWeb"] + logo);
        if (!string.IsNullOrEmpty(logo))
        {
            imgBtnLogo.ImageUrl = ConfigurationManager.AppSettings["professionalLogosWeb"] + new Guid(GetGuidSetting()) + @"\"  + logo + "?rnd=" + DateTime.Now.Millisecond;
            fileLogo1.Attributes.Add("style", "display:none");
            fileLogo2.Attributes.Add("style", "display:block");
        }

        else
        {
            
            //imgBtnLogo.Attributes.Add("style","display:none;");
            fileLogo1.Attributes.Add("style", "display:block");
            fileLogo2.Attributes.Add("style", "display:none");
        }

       
    }


    

    private void LoadImagesAndBuild()
    {
        indexImages = 1;
        //indexImages = 7;        
        pnlImages.Controls.Clear();

        string professionalImagesroot = System.Configuration.ConfigurationManager.AppSettings["professionalImagesPrefix"];
        string professionalImagesStandardSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesStandardSuffix"];
        string professionalImagesThumbnailSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesThumbnailSuffix"];

        string ProfessionalGuid = Utilities.CleanStringGuid(GetGuidSetting());
        string str_path = professionalImagesroot + ProfessionalGuid;

        //           DirectoryInfo df = new DirectoryInfo( professionalImagesPrefix + professionId + professionalImagesThumbnailSuffix);
        DirectoryInfo df = new DirectoryInfo(str_path + professionalImagesThumbnailSuffix);
        if (!df.Exists)
            return;

        FileInfo[] fileInfoArray = df.GetFiles();       

        string professionalImagesrootWeb = System.Configuration.ConfigurationManager.AppSettings["professionalImagesPrefixWeb"];
        string professionalImagesThumbnailSuffixWeb = System.Configuration.ConfigurationManager.AppSettings["professionalImagesThumbnailSuffixWeb"];
        string str_path_web = professionalImagesrootWeb + ProfessionalGuid;

        int i = 0;

        foreach (FileInfo file in fileInfoArray)
        {
            if (file.Extension != ".db")
            {
                
                   arrayImagesWhoStay.Add(Convert.ToInt32(GetMaxValue(arrayImagesWhoStay) + 1));


                    HtmlGenericControl divFileImages = new HtmlGenericControl("div");
                    divFileImages.Attributes["class"] = "fileImages";
                    pnlImages.Controls.Add(divFileImages);

                    /*
                    HtmlGenericControl fileImages1 = new HtmlGenericControl("div");
                    fileImages1.Attributes["class"] = "fileImages1";
                    fileImages1.Attributes["id"] = "fileImages1_" + arrayImagesWhoStay[i];
                    divFileImages.Controls.Add(fileImages1);

                    HtmlGenericControl fileImagesIcon = new HtmlGenericControl("div");
                    fileImagesIcon.Attributes["class"] = "fileImagesIcon";
                    fileImages1.Controls.Add(fileImagesIcon);

                    HtmlGenericControl iImages = new HtmlGenericControl("i");
                    iImages.Attributes["class"] = "fa fa-camera icon-camera";
                    iImages.Attributes["id"] = "iImages" + arrayImagesWhoStay[i];
                    fileImagesIcon.Controls.Add(iImages);

                    HtmlGenericControl fileImagesComponent = new HtmlGenericControl("div");
                    fileImagesComponent.Attributes["class"] = "fileImagesComponent";
                    fileImages1.Controls.Add(fileImagesComponent);

                    HtmlInputFile FileImages = new HtmlInputFile();
                    FileImages.Attributes["id"] = "FileImages" + arrayImagesWhoStay[i];
                    FileImages.Attributes["class"] = "form-file";
                    fileImagesComponent.Controls.Add(FileImages);

                    HtmlGenericControl fileImagesAdd = new HtmlGenericControl("div");
                    fileImagesAdd.Attributes["class"] = "fileImagesAdd";
                    fileImages1.Controls.Add(fileImagesAdd);


                    LinkButton lbAddImage = new LinkButton();
                    lbAddImage.Attributes["ID"] = "lb_uploadImages" + arrayImagesWhoStay[i];
                    lbAddImage.Command += new CommandEventHandler(lb_uploadPhoto_Click);
                    lbAddImage.CommandName = "AddPhoto";
                    lbAddImage.CommandArgument = arrayImagesWhoStay[i].ToString();
                    lbAddImage.Text = "Add a photo";

                    fileImagesAdd.Controls.Add(lbAddImage);
                    */


                    HtmlGenericControl fileImages2 = new HtmlGenericControl("div");
                    fileImages2.Attributes["class"] = "fileImages2";
                    fileImages2.Attributes["id"] = "fileImages2_" + arrayImagesWhoStay[i];
                    divFileImages.Controls.Add(fileImages2);

                    ImageButton imgBtnImages = new ImageButton();
                    imgBtnImages.Attributes["class"] = "imgBtnImages";
                    imgBtnImages.Attributes["id"] = "imgBtnImages" + arrayImagesWhoStay[i];
                    imgBtnImages.Attributes["OnClientClick"] = "return false;";
                    imgBtnImages.ImageUrl = str_path_web + professionalImagesThumbnailSuffixWeb + file.Name;

                    fileImages2.Controls.Add(imgBtnImages);

                    HtmlGenericControl fileImagesDelete = new HtmlGenericControl("div");
                    fileImagesDelete.Attributes["class"] = "fileImagesDelete";
                    divFileImages.Controls.Add(fileImagesDelete);

                    HtmlGenericControl fileImagesDeleteIcon = new HtmlGenericControl("div");
                    fileImagesDeleteIcon.Attributes["class"] = "fileImagesDeleteIcon";
                    fileImagesDelete.Controls.Add(fileImagesDeleteIcon);

                    LinkButton lbDeleteImageIcon = new LinkButton();
                    lbDeleteImageIcon.Attributes["ID"] = "lbDeleteImageIcon" + arrayImagesWhoStay[i];
                    lbDeleteImageIcon.Command += new CommandEventHandler(lb_deletePhoto_Click);
                    lbDeleteImageIcon.CommandName = "DeletePhoto";
                    //lbDeleteImageIcon.CommandArgument = arrayImagesWhoStay[i].ToString();
                    lbDeleteImageIcon.CommandArgument = file.Name;

                    fileImagesDeleteIcon.Controls.Add(lbDeleteImageIcon);

                    HtmlGenericControl iDeleteText = new HtmlGenericControl("i");
                    iDeleteText.Attributes["class"] = "fa fa-times fa-2";
                    lbDeleteImageIcon.Controls.Add(iDeleteText);


                    HtmlGenericControl fileImagesDeleteText = new HtmlGenericControl("div");
                    fileImagesDeleteText.Attributes["class"] = "fileImagesDeleteText";
                    fileImagesDelete.Controls.Add(fileImagesDeleteText);


                    LinkButton lbDeleteImage = new LinkButton();
                    lbDeleteImage.Attributes["ID"] = "lbDeleteImage" + arrayImagesWhoStay[i];
                    lbDeleteImage.Command += new CommandEventHandler(lb_deletePhoto_Click);
                    lbDeleteImage.CommandName = "DeletePhoto";
                    lbDeleteImage.CommandArgument = file.Name;
                    lbDeleteImage.Text = "Delete";

                    fileImagesDeleteText.Controls.Add(lbDeleteImage);

                    i++;
                    //lbDeleteImageIcon.Attributes["ID"] = "lbDeleteImageIcon" + indexImages;
                    ////indexImages
                    /*
                    <div class="fileImages">

                                    <div id="fileImages1_1" runat="server" class="fileImages1"  >
                                         <div class="fileImagesIcon">
                                             <i class="fa fa-camera icon-camera" id="iImages1" style="font-size: 20px; margin-left: 36px; margin-top: 29px;"></i>
                                         </div>

                                         <div class="fileImagesComponent">
                                             <input id="FileImages1" type="file" class="form-file" runat="server"  />                   
                                
                                         </div>
                            
                                         <div class="fileImagesAdd">
                                             <asp:LinkButton ID="lb_uploadImages1" runat="server" Text="Add a photo" 
                                             CssClass="addLogo" onclick="lb_uploadLogo_Click"></asp:LinkButton>
                                         </div>                        
                            
                            
                                     </div>

                                     <div id="fileImages2_1" runat="server" class="fileImages2"  >
                                         <asp:ImageButton  ID="imgBtnImages1"  class="imgBtnImages" runat="server"  OnClientClick="return false;"/>
                                     </div>
                        
                                     <div id="fileImagesDelete1" class="fileImagesDelete" runat="server" >                           
                            
                                         <div class="fileImagesDeleteIcon">
                                             <asp:LinkButton runat="server" ID="lbDeleteImageIcon1" OnCommand="lb_uploadPhoto_Click" CommandArgument="1"><i class="fa fa-times fa-2"></i></asp:LinkButton>
                                         </div>
                            
                                         <div class="fileImagesDeleteText">
                                             <asp:LinkButton runat="server" ID="lbDeleteImage1" onclick="lbDeleteFile_Click">Delete</asp:LinkButton>
                                         </div>                            
                            
                                     </div>

                       

                                 </div>
                    */
               
            }           

        }

        /*
        for (int i = 0; i < indexImages; i++)
        {
            
        }
        */
        
        //containerImages.Controls.Add(pnlImages);
    }

    void InvalidImage()
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "InvalidImage", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_ErrorImage.Text) + "');", true);
    }

    protected void uploadLogo()
    {
        string _path = ConfigurationManager.AppSettings["professionalLogos"] + new Guid(GetGuidSetting()) + "/";

        string UploadedFile = _FileLogo.PostedFile.FileName;
        
        if (string.IsNullOrEmpty(UploadedFile))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "logoMissing", "SetServerComment2('" + photoMissing + "');", true);
            return;
        }

        string FullName = System.IO.Path.GetFileName(UploadedFile);
        string _name = System.IO.Path.GetFileNameWithoutExtension(UploadedFile);

        string _extension = System.IO.Path.GetExtension(UploadedFile);
        


        switch (_extension.ToLower())
        {
            case ".jpg":
                break;
            case ".png":
                break;
            case ".gif":
                break;
            case ".jpeg":
                break;
            default:
                dbug_log.ExceptionLog(new Exception(GetGuidSetting() + " try to load forbidden format logo " + _extension));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "photoFormat", "SetServerComment2('" + photoFormat + "');", true);
                return;
        }


        if (string.IsNullOrEmpty(_extension))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "logoMissing", "SetServerComment2('" + photoMissing + "');", true);
            return;
        }


        string str_path_standard = _path + _name + "_logo" + _extension;

        if (!Directory.Exists(_path))
            Directory.CreateDirectory(_path);

        try
        {
            _FileLogo.PostedFile.SaveAs(str_path_standard);
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "logoSave", "SetServerComment2('" + HttpUtility.JavaScriptStringEncode(photoSave) + "');", true);
            return;
        }

        ASPJPEGLib.IASPJpeg objJpeg;
        objJpeg = new ASPJPEGLib.ASPJpeg();
        try
        {
            objJpeg.Open(str_path_standard);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "logoLookfor", "SetServerComment2('" + HttpUtility.JavaScriptStringEncode(photoLookfor) + "');", true);
            return;
        }       
        
        string new_name = Utilities.cleanSpecialCharsUrlFreindly3(txt_name.Value) + "_logo" + _extension;
        string str_path_thumbNail = _path + new_name;

        decimal ratio = (decimal)objJpeg.Width / (decimal)objJpeg.Height;

        int widthThumb;
        widthThumb = (int)Math.Round(80 * ratio);

        objJpeg.Height = 83;
        objJpeg.Width = widthThumb;   


        try
        {
            try
            {
                objJpeg.Save(str_path_thumbNail);
            }

            catch (Exception ex)
            {
                dbug_log.ExceptionLog(ex, siteSetting);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "logoSave", "SetServerComment2('" + HttpUtility.JavaScriptStringEncode(photoSave) + "');", true);
                return;
            }


            if (File.Exists(str_path_thumbNail))
            {
                fileLogo1.Attributes.Add("style", "display:none");
                fileLogo2.Attributes.Add("style", "display:block");

                imgBtnLogo.ImageUrl = ConfigurationManager.AppSettings["professionalLogosWeb"] + new_name;
                logo = new_name;
                string logoOld = "";
                string command = "EXEC dbo.SetAdvertiser @UserId, @SiteId, @LogoFile";
                using (SqlConnection conn = DBConnection.GetConnString())
                {
                    try
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(command, conn);
                        cmd.Parameters.AddWithValue("@UserId", new Guid(GetGuidSetting()));
                        cmd.Parameters.AddWithValue("@SiteId", siteSetting.GetSiteID);
                        cmd.Parameters.AddWithValue("@LogoFile", new_name);
                        logoOld = (string)cmd.ExecuteScalar();
                        conn.Close();

                        if (File.Exists(str_path_standard))
                        {
                            File.Delete(str_path_standard);
                        }

                        LoadLogo();
                    }
                    catch (Exception ex)
                    {
                        dbug_log.ExceptionLog(ex);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
                    }
                }
            }
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
        }
        
    }


    protected void lbDeleteFile_Click(object sender, EventArgs e)
    {
        string _path = ConfigurationManager.AppSettings["professionalLogos"];
        string strLogoPath = _path + new Guid(GetGuidSetting()) + "/" + logo;
        //Response.Write("strLogoPath:" + strLogoPath);
        try
        {
            if (File.Exists(strLogoPath))
            {
                File.Delete(strLogoPath);

                logo = "";

                /*
                imgBtnLogo.ImageUrl = ConfigurationManager.AppSettings["professionalLogosWeb"] + logo;
                fileLogo1.Attributes.Add("style", "display:block");
                fileLogo2.Attributes.Add("style", "display:none");
                */

                string command = "EXEC dbo.DeleteLogo @UserId, @SiteNameId";
                using (SqlConnection conn = DBConnection.GetConnString())
                {
                    try
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(command, conn);
                        cmd.Parameters.AddWithValue("@UserId", new Guid(GetGuidSetting()));
                        cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
                        cmd.ExecuteScalar();
                        conn.Close();

                        LoadLogo();
                        
                    }
                    catch (Exception ex)
                    {
                        dbug_log.ExceptionLog(ex);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
                    }
                }
                
            }
        }

        catch(Exception ex)
        {
            
        }        
       
    }


    private void uploadPhoto()
    {
        string professionalImagesroot = System.Configuration.ConfigurationManager.AppSettings["professionalImagesPrefix"];
        string professionalImagesStandardSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesStandardSuffix"];
        string professionalImagesThumbnailSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesThumbnailSuffix"];

        string ProfessionalGuid = Utilities.CleanStringGuid(GetGuidSetting());
        string str_path = professionalImagesroot + ProfessionalGuid;       

        string UploadedFile = FileUploadPhoto.PostedFile.FileName;
       
        if (string.IsNullOrEmpty(UploadedFile))
        {
            //commentUploadImages.Visible = true;            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "photoMissing", "SetServerComment2('" + photoMissing + "');", true);
            return;
        }
       
        string FullName = System.IO.Path.GetFileName(UploadedFile);
        string _name = System.IO.Path.GetFileNameWithoutExtension(UploadedFile);
        
        string _extension = System.IO.Path.GetExtension(UploadedFile);

        switch (_extension.ToLower())
        {
            case ".jpg":
                break;
            case ".png":
                break;
            case ".gif":
                break;
            case ".jpeg":
                break;
            default:
                dbug_log.ExceptionLog(new Exception(GetGuidSetting()  + " try to load forbidden format photo " + _extension));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "photoFormat", "SetServerComment2('" + photoFormat + "');", true);
                return;
        }


        if (string.IsNullOrEmpty(_extension))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "photoMissing", "SetServerComment2('" + photoMissing + "');", true);            
            return;
        }
       

        /************  standard photo *****************/

        string str_path_standard = str_path + professionalImagesStandardSuffix + Utilities.cleanSpecialCharsUrlFreindly3(txt_name.Value) + "_" + _name + _extension;

        if (!Directory.Exists(str_path))
            Directory.CreateDirectory(str_path);

        if (!Directory.Exists(str_path + professionalImagesStandardSuffix))
            Directory.CreateDirectory(str_path + professionalImagesStandardSuffix);

        try
        {
            FileUploadPhoto.PostedFile.SaveAs(str_path_standard);           
        }

        catch(Exception ex)
        {
            dbug_log.ExceptionLog(ex,siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "photoSave", "SetServerComment2('" + HttpUtility.JavaScriptStringEncode(photoSave) + "');", true);
            return;
        }


        /************  thumbnails photos *****************/
        string str_path_thumbNail = str_path + professionalImagesThumbnailSuffix + Utilities.cleanSpecialCharsUrlFreindly3(txt_name.Value) + "_" + _name + _extension;


        ASPJPEGLib.IASPJpeg objJpeg;
        objJpeg = new ASPJPEGLib.ASPJpeg();
        try
        {
            objJpeg.Open(str_path_standard);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "photoLookfor", "SetServerComment2('" + HttpUtility.JavaScriptStringEncode(photoLookfor) + "');", true);
            return;
        }
        
        decimal ratio = (decimal)objJpeg.Width / (decimal)objJpeg.Height;

        int widthThumb;
        widthThumb = (int)Math.Round(80 * ratio);        

        objJpeg.Height = 80;
        objJpeg.Width = widthThumb;        

        try
        {
            objJpeg.Save(str_path_thumbNail);
           
            if (File.Exists(str_path_thumbNail))
            {

            }          

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "photoSave", "SetServerComment2('" + HttpUtility.JavaScriptStringEncode(photoSave) + "');", true);
            return;
        }
        
    }


    protected void minusVideo()
    {
        /************  remove the last control added in page load for video*************/
        pnlAddVideos.Controls.RemoveAt(pnlAddVideos.Controls.Count - 1);
        index--;

        arrayWhoStay.RemoveAt(arrayWhoStay.Count - 1);        
    }    

    protected void lb_deletePhoto_Click(object sender, CommandEventArgs e)
    {
        LinkButton lb = (LinkButton)sender;        

        string professionalImagesroot = System.Configuration.ConfigurationManager.AppSettings["professionalImagesPrefix"];
        string professionalImagesStandardSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesStandardSuffix"];
        string professionalImagesThumbnailSuffix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesThumbnailSuffix"];

        string ProfessionalGuid = Utilities.CleanStringGuid(GetGuidSetting());
        string str_path = professionalImagesroot + ProfessionalGuid;

        string str_path_standard = str_path + professionalImagesStandardSuffix + e.CommandArgument;
        string str_path_thumbNail = str_path + professionalImagesThumbnailSuffix + e.CommandArgument;

       
        try
        {
            if (File.Exists(str_path_standard))
            {
                File.Delete(str_path_standard);
            }

            if (File.Exists(str_path_thumbNail))
            {
                File.Delete(str_path_thumbNail);
            }


            HtmlGenericControl grandfather = (HtmlGenericControl)lb.Parent.Parent.Parent;
            pnlImages.Controls.Remove(grandfather);
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "photoDelete", "SetServerComment2('" + HttpUtility.JavaScriptStringEncode(photoDelete) + "');", true);
            return;
        }

    }

    string FileNameV
    {
        get { return (ViewState["pic"] == null) ? string.Empty : (string)ViewState["pic"]; }
        set { ViewState["pic"] = value; }
    }

    protected string GetServerError
    {
        get { return (ViewState["GetServerError"] == null) ? " " : ViewState["GetServerError"].ToString(); }
        set { ViewState["GetServerError"] = value; }
    }

    protected int index
    {
        get
        {
            return (ViewState["index"] == null ? 0 : (int)ViewState["index"]);
        }
        set
        {

            ViewState["index"] = value;
        }
    }

    protected int indexImages
    {
        get
        {
            return (ViewState["indexImages"] == null ? 0 : (int)ViewState["indexImages"]);
        }
        set
        {

            ViewState["indexImages"] = value;
        }
    }

    

    protected Dictionary<int, string> dicVideoStay
    {
        get { return (ViewState["dicVideoStay"] == null) ? new Dictionary<int, string>() : (Dictionary<int, string>)ViewState["dicVideoStay"]; }
        set { ViewState["dicVideoStay"] = value; }
    }

    protected ArrayList arrayWhoStay
    {
        get
        {
            return (ViewState["arrayWhoStay"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoStay"]);
        }
        set
        {

            ViewState["arrayWhoStay"] = value;
        }
    }

    protected ArrayList arrayImagesWhoStay
    {
        get
        {
            return (ViewState["arrayImagesWhoStay"] == null ? new ArrayList() : (ArrayList)ViewState["arrayImagesWhoStay"]);
        }
        set
        {

            ViewState["arrayImagesWhoStay"] = value;
        }
    }



    protected ArrayList arrayWhoInInit
    {
        get
        {
            return (ViewState["arrayWhoInInit"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoInInit"]);
        }
        set
        {

            ViewState["arrayWhoInInit"] = value;
        }
    }


    protected string logo
    {
        get
        {
            return (ViewState["logo"] == null ? "" : (string)ViewState["logo"]);
        }

        set
        {
            ViewState["logo"] = value;
        }
    }

    protected bool blnVideoPath
    {
        get
        {
            return (ViewState["blnVideoPath"] == null ? true : (bool)ViewState["blnVideoPath"]);
        }

        set
        {
            ViewState["blnVideoPath"] = value;
        }
    }
    
}