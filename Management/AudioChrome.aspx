﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AudioChrome.aspx.cs" Inherits="Management_AudioChrome" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
    VIDEO{
        margin: auto;
        position: absolute;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
    }
    </style>
  <!--  <script type="text/javascript" src="AudioJquery/jwplayer.js"></script>
    
    <script type="text/javascript">
    
    window.onload = function()
    {
        jwplayer().play();
    }
    
    
    </script>
    -->
</head>
<body>
    <form id="form1" runat="server">
    
         <video controls="controls" autoplay="autoplay">
            <source src="<%# GetPath %>" type="audio/mpeg">
        </video>
    </form>
</body>
</html>
