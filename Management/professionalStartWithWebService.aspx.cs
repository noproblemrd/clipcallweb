using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Collections.Generic;
using System.Text;

public partial class Professional_professionalStartWithWebService : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        WebReferenceSite.Site site = WebServiceConfig.GetSiteReference(this);
        string strProfession=Request["startWith"];
        string xmlProfessions = site.GetExpertise(siteSetting.GetSiteID, strProfession);
        
        
        XmlDocument xml = new XmlDocument();
        xml.LoadXml(xmlProfessions);

        StringBuilder sb = new StringBuilder();
        XmlNode root = xml.DocumentElement; // Expertise        

        //string outerExpertise = "";

        //foreach (XmlNode node in root.ChildNodes)
        if (root.HasChildNodes)
        {
            for (int i = 0; i < root.ChildNodes.Count; i++) // element primary
            {

                if (root.ChildNodes[i].HasChildNodes)
                {
                    for (int y = 0; y < root.ChildNodes[i].ChildNodes.Count; y++)
                    {
                        sb.Append(root.ChildNodes[i].Attributes["Name"].InnerText + " >> "
                            + root.ChildNodes[i].ChildNodes[y].InnerText + "=" +
                            root.ChildNodes[i].Attributes["ID"].InnerText + "*" +
                            root.ChildNodes[i].ChildNodes[y].Attributes["ID"].InnerText + "&");
                    }
                }

                else
                    sb.Append(root.ChildNodes[i].Attributes["Name"].InnerText + "=" +
                        root.ChildNodes[i].Attributes["ID"].InnerText + "&");

            }

            Response.Write(sb.ToString().Substring(0, sb.ToString().Length - 1));
        }

        else
            Response.Write("empty");
        /*
        WebReferenceSupplier.Supplier supplier=WebServiceConfig.GetSupplierReference(this); 
        string xmlProfessions=supplier.("0","870C4060-455C-DF11-80CD-0003FF727321");
        */
        //Response.Write(xmlProfessions);
            
            /*
            XmlNode root = xml.DocumentElement;
            string outerExpertise = "";

            //foreach (XmlNode node in root.ChildNodes)
            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                //Response.Write(root.ChildNodes[i].Attributes["Id"].InnerText + " " + root.ChildNodes[i].Attributes["Name"].InnerText + "<br>");
                if (root.ChildNodes[i].HasChildNodes)
                {
                   
                    for (int y = 0; y < root.ChildNodes[i].ChildNodes.Count; y++)
                    {
                        if (i == root.ChildNodes.Count - 1 && y == root.ChildNodes[i].ChildNodes.Count-1)
                            outerExpertise += root.ChildNodes[i].Attributes["Name"].InnerText + "=" + root.ChildNodes[i].Attributes["Id"].InnerText + " >> " + root.ChildNodes[i].ChildNodes[y].Attributes["Name"].InnerText + "=" + root.ChildNodes[i].Attributes["Id"].InnerText;
                        else
                            outerExpertise += root.ChildNodes[i].Attributes["Name"].InnerText + "=" + root.ChildNodes[i].Attributes["Id"].InnerText + " >> " + root.ChildNodes[i].ChildNodes[y].Attributes["Name"].InnerText + "=" + root.ChildNodes[i].Attributes["Id"].InnerText + "&";
                    }
                   
                }

                else
                {
                   
                    if(i==root.ChildNodes.Count-1)
                        outerExpertise += root.ChildNodes[i].Attributes["Name"].InnerText + "=" + root.ChildNodes[i].Attributes["Id"].InnerText;
                    else
                        outerExpertise += root.ChildNodes[i].Attributes["Name"].InnerText + "=" + root.ChildNodes[i].Attributes["Id"].InnerText + "&";
                     
                }

            }
            //Response.Write("expertise:" + expertise);
            //outerExpertise = "cur";
            Response.Write(outerExpertise);
             */
     }       

  
}
