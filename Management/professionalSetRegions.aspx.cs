using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Management_professionalSetRegions : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.ContentType = "text/xml";

        int level = Convert.ToInt16(Request["level"]);
        //int level = 1;

        string regions = Request["regions"].ToString();
        //string regions="*";
        //string regions = "f9f459cd-669e-df11-9c13-0003ff727321*";
        //string regions = "*";
        //string regions = "f9f459cd-669e-df11-9c13-0003ff727321,f9f459cd-669e-df11-9c13-0003ff727322*f9f459cd-669e-df11-9c13-0003ff727321,f9f459cd-669e-df11-9c13-0003ff727325";
        int posAsterisk=regions.IndexOf("*");
        //Response.Write("posAsterisk:" + posAsterisk);        

        StringBuilder sb = new StringBuilder();
        sb.Append("<Regions Level=\"" + level  + "\">");

        string[] splitRegions=regions.Split('*');

        string chosenRegions=splitRegions[0];
        string chosenNotRegions=splitRegions[1];


        if (chosenRegions != "")
            {
                string[] splitChosenRegions = chosenRegions.Split(',');

                for (int i = 0; i < splitChosenRegions.Length; i++)
                {
                    sb.Append("<Region ID=\"" + splitChosenRegions[i] + "\" Selected=\"True\"/>");
                }

            }

            if (chosenNotRegions != "")
            {
                string[] splitChosenNotRegions = chosenNotRegions.Split(',');

                for (int i = 0; i < splitChosenNotRegions.Length; i++)
                {
                    sb.Append("<Region ID=\"" + splitChosenNotRegions[i] + "\" Selected=\"False\"/>");
                }

            }
        

        sb.Append("</Regions>");

        //Response.Write(sb.ToString());
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        string result = "";

        try
        {
            result = supplier.HandleAdvertiserRegions(siteSetting.GetSiteID, level, GetGuidSetting(), "", sb.ToString());

        }

        catch(Exception err)
        {
            result = err.Message;
        }

        //Response.Write("professionsetRegions result: " + sb.ToString());
        Response.Write(result);
    }
}
