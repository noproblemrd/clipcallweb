﻿<%@ WebHandler Language="C#" Class="closeTestCall" %>

using System;
using System.Web;

public class closeTestCall : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        
        string supplierGuid = context.Request["supplierGuid"];
        string siteId = context.Request["siteId"];
        
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, siteId);
        WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
        
        try
        {
            result=supplier.TestCallClosedInReport(new Guid(supplierGuid));

            if (result.Type == WebReferenceSupplier.eResultType.Success)
            {
                context.Response.Write("success");
            }

            else
            {
                context.Response.Write("unsuccess");
                //dbug_log.ExceptionLog("failure close test call" + supplierGuid + " " + siteId , siteId);                
            }
        }
            
        catch(Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteId);
        }
        
            
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}