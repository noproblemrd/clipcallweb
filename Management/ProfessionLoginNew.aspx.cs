﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Data.SqlClient;


public partial class Management_ProfessionLoginNew : PageSetting
{

    const string MY_CLICK = "GetPass";
    const string MY_login = "login";
    const string MY_SendPassword = "SendPassword";

    protected string MakeLogin;
    protected string professionalUserId;
    protected string professionalEmail;
    protected string professionalPelephone;
    protected string pass;
    protected string GetPassword;


    string page_name;
    string page_translate;
    public string Get_Password
    {
        get { return GetPassword; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
    /*    btnCheckCode.ImageUrl = ResolveUrl("~") + "Management/imagesLogin/btn-bg.png";
        btnCheckCode.Height = 0;
        btnCheckCode.AlternateText = "Login";   */


        GetPassword = ClientScript.GetPostBackEventReference(this, MY_CLICK);
        MakeLogin = ClientScript.GetPostBackEventReference(this, MY_login);
        
        if (!IsPostBack)
        {


            if (!string.IsNullOrEmpty(siteSetting.GetSiteID))
            {

                LoadTextLogin(siteSetting.GetSiteID);
                RegularExpressionValidatorPhone.ValidationExpression = siteSetting.GetMobileRegularExpression;// DBConnection.GetMobileRegularExpression(siteSetting.GetSiteID);//siteSetting.GetPhoneRegularExpression;
                setTextLogin();
            }
            LoadPageLinks();
   
               
        }
        else
        {
            string event_argument = Request["__EVENTARGUMENT"];
            if (event_argument == MY_CLICK)
                _SendPassword();
            else if (event_argument == MY_login)
                _LogIn();
        }
        Page.Header.DataBind();
    }

    
    private void setTextLogin()
    {
        string command = "SELECT dbo.GetTextLoginBySiteNameId(@siteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@siteNameId", siteSetting.GetSiteID);
            lbl_commercial.Text = (string)cmd.ExecuteScalar();
            conn.Close();
        }
    }
   
    private void LoadTextLogin(string siteId)
    {
        lbl_commercial.Text = DBConnection.GetTextLogin(siteId);
    }

    protected void btnSendForPassword_click(object sender, EventArgs e)
    {
        _SendPassword();
    }
    private void _SendPassword()
    {
        if (siteSetting == null || string.IsNullOrEmpty(siteSetting.GetSiteID))
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            if (!DBConnection.LoadSiteId(host, this))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
        }
        /*
        if(!_RadCaptcha.IsValid)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MissingCaptcha", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_MissingCaptcha.Text) + "');", true);
            return;
        }
         */
        string email = txt_professionalEmail.Text.Trim();
        string cellular = txt_professionalPelephone.Text.Trim();
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        string result = string.Empty;
        try
        {
            result = supplier.SupplierSignUp(email, cellular, string.Empty);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            lblComment.Text = _HiddenField_RequestFailed.Value;
            ClientScript.RegisterStartupScript(this.GetType(), "FreeDisabled", "FreeDisabled()", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["SupplierSignUp"] == null || xdd["SupplierSignUp"]["Error"] != null)
        {
            if (xdd["SupplierSignUp"] != null)
            {
                string _err = xdd["SupplierSignUp"]["Error"].InnerText.ToLower();
                lblComment.Text = lbl_emailPhoneExist.Text;
                /*
                if (_err == "email allready exists")
                    lblComment.Text = lbl_emailExist.Text;
                else if (_err == "phone allready exists")
                    lblComment.Text = lbl_phoneExists.Text;
                 * */
            }
            else
                lblComment.Text = _HiddenField_RequestFailed.Value;
        }
        else
        {
            page_name = System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
            page_translate = EditTranslateDB.GetPageNameTranslate(page_name, siteSetting.siteLangId);
            Guid _id = new Guid(xdd["SupplierSignUp"]["Status"].InnerText);
            cb_TermNCondition.Enabled = false;
            SetAudit(email, cellular, _id);
            lblComment.Text = lbl_PasswordSent.Text;
            txtPassword.Focus();
        }
        emailV = email;
        ClientScript.RegisterStartupScript(this.GetType(), "FreeDisabled", "FreeDisabled();", true);
    }
    void SetAudit(string email, string cellular, Guid _id)
    {
        OneCallUtilities.AuditRequest audit_request = new OneCallUtilities.AuditRequest();
        List<OneCallUtilities.AuditEntry> list = new List<OneCallUtilities.AuditEntry>();
        OneCallUtilities.AuditEntry ocu_ae = GetBaseDetails(_id);
        ocu_ae.FieldId = "lbl_contactPerson";
        ocu_ae.FieldName = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "professionalDetails.aspx", "lbl_contactPerson");
        ocu_ae.NewValue = cellular;
        list.Add(ocu_ae);
        ocu_ae = GetBaseDetails(_id);
        ocu_ae.FieldId = "lbl_email";
        ocu_ae.FieldName = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "professionalDetails.aspx", "lbl_email");
        ocu_ae.NewValue = email;
        list.Add(ocu_ae);

        OneCallUtilities.OneCallUtilities ocu = WebServiceConfig.GetOneCallUtilitiesReference(this);
        audit_request.AuditOn = true;
        audit_request.AuditEntries = list.ToArray();
        OneCallUtilities.Result _result = new OneCallUtilities.Result();
        try
        {
            _result = ocu.CreateAudit(audit_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
        }           
     
    }
    OneCallUtilities.AuditEntry GetBaseDetails(Guid _id)
    {
        OneCallUtilities.AuditEntry _ae = new OneCallUtilities.AuditEntry();
        _ae.AdvertiseId = _id;
        _ae.AuditStatus = OneCallUtilities.Status.Insert;
        _ae.PageId = page_name;
        _ae.PageName = page_translate;
        _ae.UserId = _id;
        return _ae;
    }
    protected void btnSendForPassword_Click(object sender, EventArgs e)
    {
        _SendPassword();
    }
    private void _LogIn()
    {
        if (siteSetting == null || string.IsNullOrEmpty(siteSetting.GetSiteID))
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            if (!DBConnection.LoadSiteId(host, this))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
        }
        string email = txt_professionalEmail.Text.Trim();
        string passWord = txtPassword.Text;

        //Check if try to access multi times
        /* not nescery captcha
        string command = "SELECT dbo.GetLogInApproved(@UserName, @SiteNameId, @IP)";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@UserName", email);
        cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
        cmd.Parameters.AddWithValue("@IP", Utilities.GetIP(Request));
        bool IsApprove = (bool)cmd.ExecuteScalar();
        conn.Close();
        if (!IsApprove)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "AccountLocked", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AccountLocked.Text) + "');", true);
            return;
        }
         * */
        /***********/
        bool IsApprove = DBConnection.IsLoginApproved(email, siteSetting.GetSiteID);
      
        if (!IsApprove)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "AccountLocked", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AccountLocked.Text) + "');", true);
            return;
        }
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;


        try
        {
            _response = supplier.UserLogin(email, passWord);
        }
        catch (Exception ex)
        {
            Session["exception"] = ex;
            Response.Redirect(ErrorPath);
        }
        if (_response.Type != WebReferenceSupplier.eResultType.Success)
        {
            lblComment.Text = "Failed";
            return;
        }
        if (_response.Value.UserLoginStatus == WebReferenceSupplier.eUserLoginStatus.NotFound || _response.Value.UserId==Guid.Empty)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Supplier);
            lblComment.Text = (email == emailV) ? lbl_notFound.Text : lbl_WrongEmail.Text;
            return;
        }
        if (_response.Value.AffiliateOrigin != Guid.Empty)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Supplier);
            ClientScript.RegisterStartupScript(this.GetType(), "AffiliateUser", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AffiliateUser.Text) + "'); window.location='" + ResolveUrl("~") + "Affiliate/AffiliateLogin.aspx';", true);
            return;
        }
        if (_response.Value.SecurityLevel > 0)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Supplier);
            Response.Redirect(ResolveUrl("~") + "Publisher/PublisherLogin.aspx");
            return;
        }
        DBConnection.InsertLogin(email, _response.Value.UserId, siteSetting.GetSiteID, eUserType.Supplier);
       // InsertLogin(email, Guid.Empty);
        bool RegistrationStage = _response.Value.StageInRegistration > 5;
        userManagement = new UserMangement(_response.Value.UserId.ToString(), _response.Value.Name, (double)_response.Value.Balance, RegistrationStage);
        
        int sitelangid = DBConnection.GetSiteLangIdByUserId(new Guid(userManagement.GetGuid), siteSetting.GetSiteID);
        if (sitelangid > 0)
        {
            siteSetting.siteLangId = sitelangid;
            Response.Cookies["language"].Value = sitelangid.ToString();
            Response.Cookies["language"].Expires = DateTime.Now.AddMonths(3);
        }
        userManagement.SetUserObject(this);
   //     siteSetting.DateFormat = DATE_FORMAT;
  //      siteSetting.TimeFormat = TIME_FORMAT;
       
        //   siteSetting.SetDicLinks(false, this);
        Response.Redirect("frame.aspx");   
    }
    /*
    void InsertLogin(string email, Guid _id)
    {
        string command = "EXEC dbo.InsertLogin @UserName, @SiteNameId, @UserId, @IP, @Success";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@UserName", email);
        cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
        cmd.Parameters.AddWithValue("@IP", Utilities.GetIP(Request));
        if (_id == Guid.Empty)
        {
            cmd.Parameters.AddWithValue("@UserId", DBNull.Value);
            cmd.Parameters.AddWithValue("@Success", false);
        }
        else
        {
            cmd.Parameters.AddWithValue("@UserId", _id);
            cmd.Parameters.AddWithValue("@Success", true);
        }
        int a = cmd.ExecuteNonQuery();
        conn.Close();
    }
     * */
    protected void Button1_Click(object sender, EventArgs e)
    {
        _LogIn();
    }
    string emailV
    {
        get { return (ViewState["email"] == null) ? string.Empty : (string)ViewState["email"]; }
        set { ViewState["email"] = value; }
    }
}
