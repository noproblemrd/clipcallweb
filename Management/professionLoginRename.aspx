<%@ Page Language="C#" MasterPageFile="~/Management/MasterPageAdvertiser.master" AutoEventWireup="true" CodeFile="professionLoginRename.aspx.cs" Inherits="Management_professionLoginRename" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<script type="text/javascript" src="../general.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<div class="page-contentm my-calls">
	<h2><asp:Label ID="lblSubTitle" runat="server">Change the password</asp:Label></h2>
	<div class="form-login">
	<asp:MultiView ID="mvChangePassword" runat="server" ActiveViewIndex="0">
        <asp:View runat="server" ID="mv_step1" >
           <div class="form-field">
                <asp:Label ID="lblOldPassword" CssClass="label" runat="server" Text="Old password:"></asp:Label>
                <asp:TextBox ID="txtOldPassword" CssClass="new_pass" runat="server" TextMode="Password"></asp:TextBox>
                
                <div class="invalid"><asp:RequiredFieldValidator ID="RequiredFieldValidator_OldPassword" CssClass="error-msg" runat="server"
                ErrorMessage="Missing" Display="Static" ControlToValidate="txtOldPassword"
                ValidationGroup="RenewPassword"></asp:RequiredFieldValidator></div>
            </div>
            <div class="form-field">
                <asp:Label ID="lblNewPassword1" CssClass="label" runat="server" Text="New password:"></asp:Label>
                <asp:TextBox ID="txtNewPassword1" CssClass="new_pass" runat="server" TextMode="Password"></asp:TextBox>
               
                <div class="invalid"><asp:RequiredFieldValidator ID="RequiredFieldValidator_NewPassword1" CssClass="error-msg" runat="server"
                ErrorMessage="Missing" Display="Dynamic" ControlToValidate="txtNewPassword1"
                ValidationGroup="RenewPassword"></asp:RequiredFieldValidator></div>
                
                
                
            </div>
            <div class="form-field">
                <asp:Label ID="lblNewPassword2" CssClass="label" runat="server" Text="New password:"></asp:Label>
                <asp:TextBox ID="txtNewPassword2" CssClass="new_pass" runat="server" TextMode="Password"></asp:TextBox>
                 
               <div class="invalid"> <asp:RequiredFieldValidator ID="RequiredFieldValidator_NewPassword2" CssClass="error-msg" runat="server"
                ErrorMessage="Missing" Display="Dynamic" ControlToValidate="txtNewPassword2"
                ValidationGroup="RenewPassword"></asp:RequiredFieldValidator></div>
                
                <div class="invalid"><asp:RegularExpressionValidator ID="RegularExpressionValidator_NewPassword2" 
                runat="server" ErrorMessage="<%# lbl_AllowedCharacter.Text %>"
                ControlToValidate="txtNewPassword2" Display="Dynamic" ValidationGroup="RenewPassword"
                CssClass="error-msg" ValidationExpression="^[0-9a-zA-Z]+$"></asp:RegularExpressionValidator>
           
	        <asp:CompareValidator ID="CompareValidator1" runat="server" CssClass="error-msg"
	        ErrorMessage="<%# lbl_NotCompare.Text %>" ControlToCompare="txtNewPassword1" 
	        ControlToValidate="txtNewPassword2" ValidationGroup="RenewPassword" Display="Static"></asp:CompareValidator>
                </div>
        </div>                     
		    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="form-submitnew" OnClick="btnSubmit_Click" ValidationGroup="RenewPassword" />
		</asp:View>
        
        <asp:View runat="server" ID="mv_step2">
            <asp:Label ID="lbl_SuccessCahnge" runat="server" Text="Password changed successfully"></asp:Label>
        </asp:View>
        
        <asp:View runat="server" ID="mv_step3">       
            <asp:Label ID="lbl_NotSuccessChange" runat="server" Text="The password has not been changed!!!"></asp:Label>
            <asp:Label ID="lbl_NotSuccessChange2" runat="server" Text="Please try again."></asp:Label>
        </asp:View>
    </asp:MultiView>
    </div>			
    <asp:Label ID="lbl_incorrectPassword" runat="server" Text="Current password is wrong!" Visible="false"></asp:Label>
     <asp:Label ID="lbl_NotCompare" runat="server" Text="The new password does not match" Visible="false"></asp:Label>
     <asp:Label ID="lbl_6Character" runat="server" Text="The password must to be at least 6 characters" Visible="false"></asp:Label>
     <asp:Label ID="lbl_AllowedCharacter" runat="server" Text="The password should contain only digits and characters" Visible="false"></asp:Label>
     <asp:Label ID="lbl_Idential" runat="server" Text="The password must not be identical to the name or email" Visible="false"></asp:Label>
    
</div>
</asp:Content>

