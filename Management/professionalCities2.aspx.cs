using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.Web.Services.Protocols;
using System.Text;

public partial class Management_professionalCities2 : PageSetting
{
    public string lang = "eng";
    public List<string> allReadyCertificate = new List<string>();
    public int indexAllReadyCertificate = -1;
    public string arrCertificatesStr = "";
    protected string RegistrationStage = "";
    protected string strRegionsAllAndRegionsSpecific="";

    protected void Page_Load(object sender, EventArgs e)
    {
        //ScriptManager1.RegisterAsyncPostBackControl(btnSendWebService);      
      
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {
                string csnameLogin = "setLogin";
                Type cstypeLogin = this.GetType();

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager csLogin = this.Page.ClientScript;

                if (!csLogin.IsClientScriptBlockRegistered(cstypeLogin, csnameLogin))
                {
                    string csTextLogin = "top.location='" + ResolveUrl("~") + @"Management/ProfessionLogin.aspx';";
                    csLogin.RegisterClientScriptBlock(cstypeLogin, csnameLogin, csTextLogin, true);
                }
                return;
            }

            if (!string.IsNullOrEmpty(siteSetting.GetSiteID))
            {
               
                getExplanation(siteSetting.GetSiteID);
            }

            lbl_page.Text = " 6";

            initRegistationStep();
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));

        }

        else
        {


            foreach (string thing in Request.QueryString)
            {
                //Response.Write("<br>" + thing + " " + Request.QueryString[thing].ToString());
            }

            foreach (string thing in Request.Form)
            {
                //Response.Write("<br>" + thing + " " + Request.Form[thing].ToString());
            }

        }
        Page.Header.DataBind();


        //Response.Write(this.GetLocalResourceObject("lblDescResource1.Text").ToString());
        //lblDesc.Text = this.GetLocalResourceObject("lblDescResource1.Text").ToString();
    }

    private void getExplanation(string siteId)
    {
        string pagename =
           System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        
        
        Dictionary<string, bool> dicControls = DBConnection.GetLiteralsControls(pagename, LiteralType.Explanation);
        ExplanationControls ex = new ExplanationControls(this, dic, dicControls);
        ex.StartInsertExplanation();
    }

    


    private void initRegistationStep()
    {        
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        string xmlRegions = string.Empty;
        try
        {
            //xmlRegions = supplier.HandleAdvertiserRegions(siteSetting.GetSiteID, 1, GetGuidSetting(), "", "");
            xmlRegions = supplier.HandleAdvertiserRegions(siteSetting.GetSiteID, 1, GetGuidSetting(), "", "");
            //Response.Write(xmlRegions);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlRegions);

            StringBuilder sbRegions = new StringBuilder();
            StringBuilder sbProfessionalRegions = new StringBuilder();

            XmlNode root = xml.DocumentElement; // Regions

            string ParentName = "";

            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++) // element primary
                {
                    ParentName = root.ChildNodes[i].Attributes["ParentName"].InnerText;

                    if (ParentName != "")
                    {
                        sbRegions.Append(ParentName + " >> " + root.ChildNodes[i].Attributes["Name"].InnerText +
                             "=" + root.ChildNodes[i].Attributes["ID"].InnerText + "*" +
                             root.ChildNodes[i].Attributes["HasChildRegions"].InnerText
                            + "&");
                    }
                    else
                    {
                        sbRegions.Append(root.ChildNodes[i].Attributes["Name"].InnerText + "=" +
                            root.ChildNodes[i].Attributes["ID"].InnerText + "*" + root.ChildNodes[i].Attributes["HasChildRegions"].InnerText
                            + "&");
                    }

                    //Response.Write(sbRegions.ToString().Substring(0, sbRegions.ToString().Length - 1));

                    if (root.ChildNodes[i].Attributes["ConnectedToAdvertiser"].InnerText == "True")
                    {
                        if (ParentName != "")
                            sbProfessionalRegions.Append(ParentName + " >> " + root.ChildNodes[i].Attributes["Name"].InnerText + "&");
                        else
                            sbProfessionalRegions.Append(root.ChildNodes[i].Attributes["Name"].InnerText + "&");
                    }
                }

                string strRegions = "";
                strRegions = sbRegions.ToString().Substring(0, sbRegions.ToString().Length - 1);

                string strProfessionalRegions = "";

                if (sbProfessionalRegions.ToString().Length > 0)
                {
                    strProfessionalRegions = sbProfessionalRegions.ToString().Substring(0, sbProfessionalRegions.ToString().Length - 1);
                    //Response.Write(sbProfessionalRegions.ToString().Substring(0, sbProfessionalRegions.ToString().Length - 1));
                }
                else
                {
                    //sbProfessionalRegions.Append("empty");
                    strProfessionalRegions = "empty";
                    //Response.Write(sbProfessionalRegions.ToString());
                }

                //Response.Write(strRegions + "*****" + strProfessionalRegions);
                strRegionsAllAndRegionsSpecific = strRegions + "*****" + strProfessionalRegions;
            }

            else
                strRegionsAllAndRegionsSpecific = "empty*****empty";


            if (xml["Regions"] == null || xml["Regions"]["Error"] != null)
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
            //XmlNode root = xml.DocumentElement;

            /**************  RegistrationStage Influences ****************/

            RegistrationStage = root.Attributes["RegistrationStage"].InnerText;
            //RegistrationStage = "2";
            string csnameStep = "setStep";
            Type cstypeStep = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager csStep = this.Page.ClientScript;


            if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
            {
                string csTextStep = "witchAlreadyStep(" + RegistrationStage + ");";
                csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
            }

            if (int.Parse(RegistrationStage) >= 3)
            {
                btnSendWebService.Value = lbl_btn_Update.Text;
                btnSendWebService.Attributes.Add("title", "Update");
                pSteps.Visible = false;
            }
            else
            {
                btnSendWebService.Value = lbl_btn_Next.Text;
                btnSendWebService.Attributes.Add("title", "Next");
                pSteps.Visible = true;
            }

        }
        catch (SoapException ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        //Response.Write("xmlRegions:" + xmlRegions);
        //XmlDocument xml = new XmlDocument();
        //xml.LoadXml(xmlRegions);

       
        /**************  End RegistrationStage Influences ****************/
    }
   

   


  
    
}
