﻿<%@ Page Language="C#" MasterPageFile="~/Management/MasterPageAdvertiser.master" AutoEventWireup="true" 
CodeFile="MyReview.aspx.cs" Inherits="Management_MyReview" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="Toolbox" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
  <link href="../Controls/Toolbox/StyleToolboxReport.css" rel="Stylesheet" type="text/css" />

<script  type="text/javascript" src="../Calender/_Calender.js"></script>
<script  type="text/javascript"  > 
    function pageLoad(sender, args) 
    {         
        appl_init();
    }
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();  
    } 
    

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">


<div id="center__bg" runat="server" class="page-contentm my-calls">    
    <uc1:Toolbox runat="server" ID="_Toolbox" LoadStyle="false"/>  
                    
    
    
<div class="contpage">  
    
     
    <div class="myreviews2"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>
    
  
     <div class="clear"></div>                          
    <div id="list" class="list" style="margin-top:0px;">  
    <asp:UpdatePanel runat="server" ID="_uPanel" UpdateMode="Conditional">
    <ContentTemplate> 
        <div class="data-table" style="margin-top:20px;">
        
        
        <asp:Repeater runat="server" ID="_reapeter"   >
            <HeaderTemplate>
                <table class="data-table">
                    <thead>
                        <tr>
                            <th><asp:Label ID="Label50" runat="server" Text="<%#lbl_date.Text %>"></asp:Label></th>                                
                            	        
					        <th><asp:Label ID="Label1" runat="server" Text="<%#lbl_Name.Text %>"></asp:Label></th>
					        <th><asp:Label ID="Label51" runat="server" Text="<%#lbl_Rating.Text %>"></asp:Label></th>
					        
					    </tr>
                    </thead>
                <tbody>
            </HeaderTemplate>
                        
            <ItemTemplate>
                <tr runat="server" id="tr_show" class="odd">
                    
                    <td><asp:Label ID="Label200" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label></td>
				    
				    <td><asp:Label ID="Label3" runat="server" Text="<%# Bind('Name') %>"></asp:Label></td>
				    <td>
                        <asp:Image ID="img_like" runat="server" ImageUrl="<%# Bind('IsLike') %>" />
				    </td>
				    					    
				</tr>
				<tr>
				     <td colspan="5" class="even">
				        <div style="text-align:center;">
                            <asp:Label ID="lbl_review" runat="server" Text="<%# Bind('Review') %>"></asp:Label>
				        </div>
				        
                    </td>
				</tr>
			</ItemTemplate>
            <FooterTemplate>
                </tbody>
            </table>
            </FooterTemplate>
        </asp:Repeater>          
            
             <asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">     
                <ul>
                    <li class="first"><asp:LinkButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click">Previous page</asp:LinkButton></li>
                    <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
                    <li class="last"><asp:LinkButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click">Next page</asp:LinkButton></li>
                </ul>
            </asp:Panel>  
         </div>           
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<div id="clear"></div>
</div>
<asp:Label ID="lbl_date" runat="server" Text="Created on" Visible="false"></asp:Label>
<asp:Label ID="lbl_Name" runat="server" Text="Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_Review" runat="server" Text="Review" Visible="false"></asp:Label>
<asp:Label ID="lbl_Rating" runat="server" Text="Rating" Visible="false"></asp:Label>

<asp:Label ID="lblTitleMyReview" runat="server" Text="My Review" Visible="false"></asp:Label>
<asp:Label ID="lbl_NoResult" runat="server" Visible="false"
     Text="There are no results. Please change your report criterion and try again."></asp:Label>
     
</asp:Content>

