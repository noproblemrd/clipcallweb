﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Controls/MasterPagePPC.master"
 CodeFile="OverView.aspx.cs" Inherits="Management_OverView" %>
 
 <%@ MasterType VirtualPath="~/Controls/MasterPagePPC.master" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="Toolbox" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/steps.ascx" TagName="Steps" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/invite.ascx" TagName="Invite" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AnsweringRate.ascx" TagName="AnsweringRate" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AccountCompleteness.ascx" TagName="AccountCompleteness" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/PhoneAvailability.ascx" TagName="PhoneAvailability" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
    
<!--[if lte IE 8]>
	<link href="../ppc/samplepPpcIe.css" rel="stylesheet" type="text/css" />
<![endif]-->

<link href="../Controls/Toolbox/StyleToolboxReport.css" rel="Stylesheet" type="text/css" />
<!--
<link rel="stylesheet" href="FusionCharts/Contents/Style.css" type="text/css" />
-->
<!--
<script  type="text/javascript" src="FusionCharts/JSClass/FusionCharts.js"></script>
-->
<!--<script type="text/javascript" src="../general.js" ></script>-->
<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<!--<script src="webcreate-infinite-ajax-scroll-1ff389a/jquery.ias.js" type="text/javascript"></script>-->

<script  type="text/javascript" src="../Calender/_Calender.js"></script>
<script type="text/javascript"  src="../CallService.js"></script>
<script type="text/javascript" src="../scripts/EmailValidation.js"></script>

<script type="text/javascript" src="../jplayer/jQuery.jPlayer.2.1.0.demos/js/jquery.jplayer.js"></script>
<script type="text/javascript" src="../jplayer/jQuery.jPlayer.2.1.0.demos/js/jquery.transform.js"></script>
<script type="text/javascript" src="../jplayer/jQuery.jPlayer.2.1.0.demos/js/jquery.grab.js"></script>
<script type="text/javascript" src="../jplayer/jQuery.jPlayer.2.1.0.demos/js/mod.csstransforms.min.js"></script>
<script type="text/javascript" src="../jplayer/jQuery.jPlayer.2.1.0.demos/js/circle.player.js"></script>
 
<script  type="text/javascript"  >


    var ifRunReport = false;
    var ifEnableScrollInfinite = true;

    function changeSpriteBackground(obj, left, top) {
        document.getElementById(obj.id).style.backgroundPosition = left + " " + top;
    }

    function showHideTextLead(objShowId, objHideId) {
        //alert(objShowId);
        document.getElementById(objShowId).style.display = 'block';
        document.getElementById(objHideId).style.display = 'none';
    }
    // Status - lost - hover
    function appl_init() {
        
        //alert(indexJplayerTestCall);
        document.getElementById("tr_loader").style.display = 'none';
        //hideDiv();
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);

        //getViewStateLength();

    }

    function getViewStateLength() {
        // Determine view state size
        var viewStateField = $("input[id=__VIEWSTATE][type=hidden]");

        if (viewStateField != null) {
            var viewStateLength = viewStateField.val().length;

            var ViewStateThreshold = 10000;

            var hostName = $(location).attr('host');

            //if (viewStateLength > ViewStateThreshold )
            alert('The view state size for this page, ' + viewStateLength + ' bytes, exceeds the threshold (' + ViewStateThreshold + ' bytes).');
        }
    }

    function BeginHandler() {
        //getViewStateLength();
        //alert("BeginHandler");
        //alert("cvbcvbv" + document.getElementById("<%#datalistLeads.ClientID%>"));
        document.getElementById("tr_loader").style.display = 'block';
        //alert("BeginHandler2");      
        showDiv();
    }

    function OnBeginRequest(sender, args) {
        // to destroy this function in UpdateProgress\UpdateProgress.js
        //document.getElementById('divLoader').style.display = 'block';
    }

    function endRequest(sender, args) {
        // to destroy this function in UpdateProgress\UpdateProgress.js
        //document.getElementById('divLoader').style.display = 'none';
    }

    function ShowDivPPC() {
        // to destroy this function in Conytrols/MasterPagePpc
    }

    function HideDivPPC() {
        // to destroy this function in Conytrols/MasterPagePpc
    }

    function EndHandler() {
        //alert("EndHandler");
       
        document.getElementById("tr_loader").style.display = 'none';
        hideDiv();
    }

    function showHideDivOverView() {

        if (document.getElementById("divLoaderOverView").style.display == 'none') {
            alert("sfdfdf");
            $('#divLoaderOverViewScreen').show();
            document.getElementById("divLoaderOverView").style.display = 'block';
        }

        else {
            document.getElementById("divLoaderOverView").style.display = 'none';
            $('#divLoaderOverViewScreen').hide();
        }
    }

    function showHideRefund(incidentId, leadNumber) {

        if (typeof (incidentId) == 'undefined') { // for example when go back from reund declaration page 
            $('#divLoaderOverViewScreen').hide();
            document.getElementById("divRefund").style.display = 'none';
        }
        else {
            if (document.getElementById("divRefund").style.display == 'none') {
                $('#divLoaderOverViewScreen').show();
                document.getElementById("divRefund").style.display = 'block';
                document.getElementById("iframeRefund").src = 'refund.aspx?incidentId=' + incidentId + "&leadNumber=" + leadNumber;
            }

            else {
                $('#divLoaderOverViewScreen').hide();
                document.getElementById("divRefund").style.display = 'none';
            }
        }
    }

    function showHideTestCall(callerId, supplierPhone, supplierId) {

        if (document.getElementById("divTestCall").style.display == 'none') {
            $('#divLoaderOverViewScreen').show();
            document.getElementById("divTestCall").style.display = 'block';
            document.getElementById("iframeTestCall").src = '../ppc/testCall.aspx?callerId=' + encodeURIComponent(callerId) + '&supplierPhone=' + encodeURIComponent(supplierPhone) + '&supplierId=' + encodeURIComponent(supplierId);
        }

        else {

            $('#divLoaderOverViewScreen').hide();
            location.reload(true);
            document.getElementById("divTestCall").style.display = 'none';
        }
    }

    function ChangeCalendarView(sender, args) {
        sender._switchMode("", true);
    }
    /*
    function LoadChart(fileXML, divId, chartPath) {
    alert("chart");
    var chart = new FusionCharts(chartPath, 'ChartId2', '650', '400', '0', '0');
    chart.addParam("WMode", "Transparent");
    chart.setDataXML(fileXML);
    chart.render(divId);

    }

    */

    function OpenCloseDetail(contr, td_open, _iframe, _path) {
        alert("OpenCloseDetail");
        var contrl = document.getElementById(contr);
        var _div = document.getElementById("<%# center__bg.ClientID %>");
        var _heigth = _div.scrollHeight;
        contr = "[0]" + contr;
        if (contrl.style.display == 'none') {
            CheckFor(contr + ";1");
            contrl.style.display = '';
            document.getElementById(td_open).className = "open first";
            _div.style.height = (_heigth + 400) + 'px';
            if (document.getElementById(_iframe).src == "javascript:void(0)") {
                document.getElementById(_iframe).src = _path;
                showDiv();
            }
        }
        else {
            CheckFor(contr + ";-1");
            contrl.style.display = 'none';
            document.getElementById(td_open).className = "close first";
            _div.style.height = (_heigth - 400) + 'px';
        }

    }
    function ResetWin() {
        document.getElementById("<%# center__bg.ClientID %>").style.height = 'auto';
    }
    function CheckFor(contr) {
        CallServer(contr, "");
    }
    function ReciveServerData(retValue) {


    }

    function listen(_id) {
        $("#jquery_jplayer_" + _id).jPlayer("play");
    }

    function OpenRecord(_id) {

        //alert($(".cp-pause").is(":visible"));
        if ($(".cp-pause").is(":visible") == false) {
            //alert("OpenRecord:" + _id);
            $("#cp_container_" + _id + " .cp-play").css('background-position', '-28px -514px');
            var srcAudio = $("#jquery_jplayer_" + _id).jPlayer("getSrc");

            if (typeof srcAudio == "object") { // before init player it is object           

                var myCirclePlayer = new CirclePlayer("#jquery_jplayer_" + _id,
	        {
	            mp3: "usa_dev_default.mp3"
	        }, {
	            cssSelectorAncestor: "#cp_container_" + _id,
	            swfPath: "../jPlayer/jQuery.jPlayer.2.1.0.demos/js",
	            supplied: "mp3",
	            wmode: "window"

	        });


                $("#jquery_jplayer_" + _id).bind($.jPlayer.event.ready, function (event) { // Add a listener to report the time play began

                    var url = "../RecordService.asmx/GetPath2";
                    var params = "_id=" + _id;
                    CallWebService(params, url, OnCompleteRecord, On_Error);

                });

                $("#jquery_jplayer_" + _id).bind($.jPlayer.event.error, function (event) { // Add a listener to report the time play began
                    //alert(event.jPlayer.error.message);
                });
            }

            else { // when pause 
                $("#jquery_jplayer_" + _id).jPlayer("play");
                document.getElementById("cpPlay" + _id).style.backgroundPosition = "0px -514px"; // show the play situation. it is more way than the built in method to make play when click just inside in the cpPlay link. it is for clicking in the container of it the cp_container
               
            }
        }

        else {
            $("#jquery_jplayer_" + _id).jPlayer("pause");
        }

        /*************** before *******************/
        /*    
        $("#cp_container_" + _id + " .cp-play").css('background-position', '-28px -514px');
	        
        var srcAudio = $("#jquery_jplayer_" + _id).jPlayer("getSrc"); // getSrc added by shai m to api of jplayer in jquery.jplayer.js
	        
        if (srcAudio == "usa_dev_default.mp3") { // first time. if there is the default src we don't wan't to here it
        $("#jquery_jplayer_" + _id).jPlayer("clearMedia");

        var url = "../RecordService.asmx/GetPath2";
        var params = "_id=" + _id;
        CallWebService(params, url, OnCompleteRecord, On_Error);
        }

        else { // second time and forever
        $("#jquery_jplayer_" + _id).jPlayer("play");
        }
        */
        /*************** end before *******************/

    }

    function OpenRecord2() {
        // indexJplayerTestCall comes from cs page
        var srcAudio = $("#jquery_jplayer_" + indexJplayerTestCall).jPlayer("getSrc");

        if (typeof srcAudio == "object") { // before init player it is object
           
            var myCirclePlayer = new CirclePlayer("#jquery_jplayer_" + indexJplayerTestCall,
	        {
	            mp3: "usa_dev_default.mp3"
	        }, {
	            cssSelectorAncestor: "#cp_container_" + indexJplayerTestCall,
	            swfPath: "../jPlayer/jQuery.jPlayer.2.1.0.demos/js",
	            supplied: "mp3",
	            wmode: "window"

	        });


	        $("#jquery_jplayer_" + indexJplayerTestCall).bind($.jPlayer.event.ready, function (event) { // Add a listener to report the time play began
	            var url = "../RecordService.asmx/GetPath2";
	            var params = "_id=" + indexJplayerTestCall;
	            CallWebService(params, url, OnCompleteRecord, On_Error);

	        });

	        $("#jquery_jplayer_" + indexJplayerTestCall).bind($.jPlayer.event.error, function (event) { // Add a listener to report the time play began
                //alert(event.jPlayer.error.message);
               
            });

            //alert('<%#GetGuidSetting()%> <%#siteSetting.GetSiteID%>');
	        $("#jquery_jplayer_" + indexJplayerTestCall).bind($.jPlayer.event.ended, function (event) { // Add a listener to report the time play began

                setStepOverView(6,'<%#GetGuidSetting()%>','<%#siteSetting.GetSiteID%>');

            });
            
        }

        else { // it is srtring
            $("#jquery_jplayer_<%=indexJplayerTestCall%>").jPlayer("play");
        }

        /*************** before *************/
        /*
        $("#jquery_jplayer_<%=indexJplayerTestCall%>").jPlayer("clearMedia");
        //$("#jquery_jplayer_<%=indexJplayerTestCall%>").jPlayer("pause");
        
        CallWebService(params, url, OnCompleteRecord, On_Error);
        */
        /*************** end before **************/
    }

    function OnCompleteRecord(arg) {
        //alert("OnCompleteRecord:" + arg);
        //$("#jquery_jplayer_" + _id).jPlayer("destroy");


        params = arg.split('&');

        var pathAudio = params[0];
        //alert("pathAudio: " + pathAudio);
        var _id = params[1];
        //alert($("#jquery_jplayer_" + _id).jPlayer.media.src);

        //alert("_id: " + _id);
        if (arg.length == 0) {
            alert("<%# lbl_RecordError.Text %>");
            return;
        }

        //$("#jquery_jplayer_" + _id).jPlayer.mp3);

        if (pathAudio == "exist") {
            alert("here");
            //$("#jquery_jplayer_" + _id).jPlayer("play");

            $("#jquery_jplayer_" + _id).jPlayer("setMedia", {
                mp3: pathAudio
                //mp3: "usa_dev_default.mp3"                    
            }).jPlayer("play");

        }
        else {

            $("#jquery_jplayer_" + _id).jPlayer("setMedia", {
                mp3: pathAudio
                //mp3: "usa_dev_default.mp3"                    
            }).jPlayer("play");
        }
    }    

    function showHideClockTime(_id, mode) {
        //alert("mode");
        if (mode == "over") {
            $("#cp_container_" + _id + " .cd-listen").hide();
            $("#cp_container_" + _id + " .small-play").hide();

            if ($("#cp_container_" + _id + " .jp-current-time").html() == "")
                $("#cp_container_" + _id + " .jp-current-time").html("00:00");

            $("#cp_container_" + _id + " .jp-current-time").show();
        }

        else if (mode == "out") {
            //alert("out");

            //-54px -514px

            //if ($("#cp_container_" + _id + " .cp-play").css('backgroundPosition') == "-54px -514px") {
            if ($("#cp_container_" + _id + " .cp-play").css('display') == "block") {
                //alert($("#cp_container_" + _id + " .cp-play").css('backgroundPosition'));
                $("#cp_container_" + _id + " .cd-listen").show();
                $("#cp_container_" + _id + " .small-play").show();
                $("#cp_container_" + _id + " .jp-current-time").hide();
            }
        }

    }

    function CheckPopupBlocker() {
        //        if (_hasPopupBlocker(oWin))
        //            alert(document.getElementById('<#Hidden_AllowPopUp.ClientID  %>').value);
    }
    function On_Error() { }

    function setHeaderLeadTitle(obj, className) {
        //alert(obj + " " + className);
        document.getElementById(obj).className = className;
    }

    // Open popup complaints
    function CloseIframe() {
        alert("CloseIframe");
        $find('_modal').hide();
        document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie popModal_del2_hide";
        document.getElementById("<%# _iframe.ClientID %>").src = "javascript:void(0);";
    }
    function OpenIframe(_path) {
        alert(_path);
        document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie";
        document.getElementById("<%# _iframe.ClientID %>").src = _path;
        HideIframeDiv();
        $find('_modal').show();
    }
    function HideIframeDiv() {
        alert("HideIframeDiv");
        document.getElementById("divIframeLoader").style.display = "block";
        document.getElementById("div_iframe_main").style.display = "none";
    }
    function ShowIframeDiv() {
        alert("ShowIframeDiv");
        document.getElementById("divIframeLoader").style.display = "none";
        document.getElementById("div_iframe_main").style.display = "block";
    }
    function CloseIframe_after() {

        //       window.frames[0].IframeClosed();
        CloseIframe();
    }

    var tasksLeftGeneral;

    function closeSteps(bonusAmount) {
        closeVideo();
        document.getElementById("steps").style.display = 'none';
        document.getElementById("stepsOpen").style.display = 'block';

        if (tasksLeftGeneral > 0) {
            var strStepsOpen = tasksLeftGeneral + " steps away from $" + bonusAmount + " prize";
            document.getElementById("stepsOpenText").innerHTML = strStepsOpen;
        }

        else {
            document.getElementById("stepsOpenText").style.display = 'none';
        }

    }


    function updateBalance(balance) {
        document.getElementById("<%#clientIdBalance%>").innerHTML = balance;




    }

    function openSteps() {
        document.getElementById("steps").style.display = 'block';
        document.getElementById("stepsOpen").style.display = 'none';
    }

    function updateSystemMessage(str, level) {
        SetServerComment(str);
    /*
        document.getElementById("<#systemMessage.ClientID%>").style.display = 'block';
        document.getElementById("<#systemMessageText.ClientID%>").innerHTML = str;
        //if (str.length > 118) {

        if (level == 2) {
            document.getElementById("<#systemMessage.ClientID%>").style.height = '47px';
            document.getElementById("<#systemMessageText.ClientID%>").style.margin = '-17px auto auto'; // 2 lines

        }
        else {
            document.getElementById("<#systemMessage.ClientID%>").style.height = '32px';
            document.getElementById("<#systemMessageText.ClientID%>").style.margin = '-7px auto auto';  // 1 line
        }
      */
    }

    var intervalScroll;
    var enableNextList = false;

    function resetScroll() {
        //alert("resetScroll");
        window.onscroll = scroll;
        clearInterval(intervalScroll);
    }

    function getPaging() {
        //alert("getPaging");
        __doPostBack('ctl00$ContentPlaceHolderBody$lnkNextPage', '');
        window.onscroll = null;
        intervalScroll = setTimeout('resetScroll()', 250);
    }

    function enableNext(mode) {
        //alert("mode:" + mode);          
        enableNextList = mode;
    }

    function scroll() {
        //alert("cvbcvbv" + document.getElementById("<%#datalistLeads.ClientID%>"));
        //if (ifRunReport == true && enableNextList==true) {
        if (document.getElementById("<%#datalistLeads.ClientID%>") != null) { // use the scroll just when there are leads
            if (enableNextList == true) {
                if (navigator.appName == "Microsoft Internet Explorer")
                    scrollPosition = document.documentElement.scrollTop;
                else
                    scrollPosition = window.pageYOffset;

                var listHeight = document.getElementById("list").offsetHeight;
                //document.getElementById("myText").innerHTML = "ifNextEnabled:" + enableNextList + " scrollPosition:" + scrollPosition + " contentHeight:" + document.getElementById("list").offsetHeight;

                if (scrollPosition > 0.9 * listHeight)
                    getPaging();

            }
        }

    }

    window.onscroll = scroll;

    function pageLoad(sender, args) // must use it and not $(document).ready(function () because the partial post back destroy the function after post back
    {

        $(".selectBox-dropdown").click(function () {
            $(".selectBox-dropdown-menu").toggle();
            $(".selectBox-dropdown").toggleClass('selectBox-dropdown-clicked');
        }
        );

        $(".selectBox-dropdown").mouseover(function () {
            //if ($(".selectBox-dropdown-menu").is(":hidden"))
            //.selectBox-menuShowing
            $(".selectBox-dropdown").addClass('selectBox-menuShowing');
            if ($(".selectBox-dropdown-menu").is(":visible"))
                $(".selectBox-dropdown").addClass('selectBox-dropdown-clicked');
        });

        $(".selectBox-dropdown").mouseout(function () {
            if ($(".selectBox-dropdown-menu").is(":hidden"))
                $(".selectBox-dropdown").removeClass('selectBox-menuShowing');
            
            //$(".selectBox-dropdown").removeClass('selectBox-dropdown-clicked');


        }
        );

        $(".selectBox-dropdown").blur(function () {
            if ($(".selectBox-dropdown-menu").is(":visible")) {

                //$(".selectBox-dropdown-menu").hide();
                //$(".selectBox-dropdown").removeClass('selectBox-dropdown-clicked');
            }
            //$(".selectBox-dropdown").toggleClass('selectBox-dropdown-clicked'); 
        }
        );

        $(".selectBox-dropdown-menu").click(function () {
            $(".selectBox-dropdown-menu").hide();
            //$(".selectBox-dropdown").addClass('selectBox-menuShowing');

        }
        );

        $(".selectBox-dropdown-menu").mouseout(function () {
            //$(".selectBox-dropdown-menu").hide();
           

        }
        );


        $(".selectBox-options").mouseleave(function (evt) {
            $(".selectBox-dropdown-menu").hide();
            $(".selectBox-dropdown").removeClass('selectBox-menuShowing');
            $(".selectBox-dropdown").removeClass('selectBox-dropdown-clicked');           
            //mouseout fires when the pointer moves out of the child element as well, while mouseleave fires only when the pointer moves out of the bound element.
        }
        );

        /*
        $(".selectBox-dropdown").mouseleave(function (evt) {
            $(".selectBox-dropdown-menu").hide();
            $(".selectBox-dropdown").removeClass('selectBox-menuShowing');
            //mouseout fires when the pointer moves out of the child element as well, while mouseleave fires only when the pointer moves out of the bound element.
        }
        );
        */

        $("body").bind('click', function (ev) {
            var myID = ev.target.id;
            if (myID.indexOf('timeSelected') == -1) {
                $(".selectBox-dropdown-menu").hide();
                $(".selectBox-dropdown").removeClass('selectBox-menuShowing');
                $(".selectBox-dropdown").removeClass('selectBox-dropdown-clicked'); 
            }
            ///alert(myID);
            // timeSelected

        });

    }

    function updateTaskLeft(tasksLeft) {
        document.getElementById("spanTaskLeft").innerHTML = tasksLeft;
    }

    function allTasksDone() {

        var balance = document.getElementById("<%#clientIdBalance%>").innerHTML;
        balance = balance.replace(',', '');
        balance = parseFloat(balance);
        balance += bonusAmmount * 1;

        updateBalance(addCommas(balance));

        document.getElementById("descriptionStep").style.display = 'none';

        updateSystemMessage("Hero You righteously earned $" + bonusAmmount + " to spend getting phone leads!", 1); // this parameter bonusAmmount declared in the ascx steps.ascx
    }

    function setStepOverView(_step, _supplierGuid, _siteId) {
        //alert("setStepOverView");
        var params;
        params = "step=" + _step + "&supplierGuid=" + _supplierGuid + "&siteId=" + _siteId;

        //alert(params);
        var url;

        url = "setStep.ashx";

        var objHttp;
        var ua = navigator.userAgent.toLowerCase();

        if (ua.indexOf("msie") != -1) {
            objHttp = new ActiveXObject("Msxml2.XMLHTTP");

        }
        else {
            objHttp = new XMLHttpRequest();
        }



        if (objHttp == null) {
            //alert("Unable to create DOM document!");
            return;
        }


        objHttp.open("POST", url, true); // true if you want the request to be asynchronous and false if it should be a synchronous request

        objHttp.onreadystatechange = function () {

            if (objHttp.readyState == 4) {
                //alert("responseText " + objHttp.responseText);
                resultSplit = objHttp.responseText.split(',');
                var result = resultSplit[0];
                var ifAllTasksDone = resultSplit[1].toLowerCase();
                var tasksLeft = resultSplit[2].toLowerCase();

                if (result == "unsuccess") {


                }

                else {
                    //alert("success");                   

                    document.getElementById("<%=circleStepListenClientId%>").className = 'stepDoneCircle';
                    document.getElementById("<%=stepListenClientId%>").className = 'stepDone';
                    $(".divTestCallX").show();
                    updateTaskLeft(tasksLeft);
                    tasksLeftGeneral = tasksLeft;

                    if (ifAllTasksDone == "true")
                        allTasksDone();
                }
            }
        }


        objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        objHttp.setRequestHeader("Content-Length", params.length);
        objHttp.setRequestHeader("Connection", "close");

        objHttp.send(params);


        return true;

    }


    function closeTestCall(_supplierGuid, _siteId) {

        var params;
        params = "supplierGuid=" + _supplierGuid + "&siteId=" + _siteId;

        //alert(params);
        var url;

        url = "closeTestCall.ashx";

        var objHttp;
        var ua = navigator.userAgent.toLowerCase();

        if (ua.indexOf("msie") != -1) {
            objHttp = new ActiveXObject("Msxml2.XMLHTTP");

        }
        else {
            objHttp = new XMLHttpRequest();
        }



        if (objHttp == null) {
            //alert("Unable to create DOM document!");
            return;
        }


        objHttp.open("POST", url, true); // true if you want the request to be asynchronous and false if it should be a synchronous request

        objHttp.onreadystatechange = function () {
            //alert("responseText " + objHttp.responseText);
            if (objHttp.readyState == 4) {
                //alert("responseText " + objHttp.responseText);

                if (objHttp.responseText == "unsuccess") {

                    alert("unsuccess");
                }

                else {
                    //alert("success");
                    $(".divLeadsTrTestCall").hide();
                }
            }
        }


        objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        objHttp.setRequestHeader("Content-Length", params.length);
        objHttp.setRequestHeader("Connection", "close");

        objHttp.send(params);


        return true;

    }

    function setPlayBackground(index, mode) {

        if (mode == "over") {
            document.getElementById("cpPlay" + index).style.backgroundImage = "url('../ppc/images/overview/sprite.png')";
            document.getElementById("cpPlay" + index).style.backgroundPosition = "0px -514px";
        }

        else if (mode == "out") {

            document.getElementById("cpPlay" + index).style.backgroundImage = "url('../ppc/images/overview/sprite.png')";
            document.getElementById("cpPlay" + index).style.backgroundPosition = "-54px -514px";

        }


    }

    function init() {
        //if (history.length > 0)
        //history.go(+1);
        //showHideTestCall();
        //alert("1 " + bonusAmmount);
        //updateSystemMessage("df sdf sdf sdf sdf sdf sdf sdf sdf sfsd fsdfsdf dsd  sdfsd sd fd sdf sd sdf sdfsd sf sdf ssd fsdsd ds dsf ds sdf sdf fs df fsd fsd fds zxc xc zxc zx czxc zxc zxc x czxc zxc zx zxczx zx x cx xv xc vxcv xcv xcv v vxc xcv xcv xcv cv xcv xcv cv xcv xcv xcv cv cvc vxc vxcv xcv xcv cvx cvc vxc vxcv xcv xcv xcv xcv xcv cv cvv ",2);
        //updateSystemMessage("df sdf sdf sdf sdf sdf sdf sdf sdf sfsd fsdfsdf dsd  sdfsd sd fd sdf sd sdf sdfsd sf ", 1);
    }

    function TurnOnRecordCalls(elem) {
        $.ajax({
            url: "<%# TurnOnRecordCallsService %>",
            data: {},
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {

                if (data.d == true) {
                    updateSystemMessage("Great decision! From now on, all your customer calls will be recorded.");

                    $('a.-TurnOnRecordCalls').each(function () {
                        $(this).hide();
                    });
                    //GoToCommentPlace();
                    location.hash = "#anchorSystemMessage";
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //                alert('faild');
            }
        });
    }


   

    /*
    $(document).ready(function () {
    alert("reasdy");
    $("#jquery_jplayer_1").jPlayer({
    ready: function (event) {
    alert("ghjghgj");
    $(this).jPlayer("setMedia", {
    m4a: "http://www.jplayer.org/audio/m4a/TSP-01-Cro_magnon_man.m4a",
    oga: "http://www.jplayer.org/audio/ogg/TSP-01-Cro_magnon_man.ogg"
    });
    },
    swfPath: "../jPlayer/jQuery.jPlayer.2.1.0.demos/js",
    supplied: "m4a, oga",
    wmode: "window"
    });
    });
    */

    /*
    if (window.attachEvent) {
    window.attachEvent('onclick', function () { alert("baaa"); });
    alert("cookie");
    }
    else if (window.addEventListener) {
    window.addEventListener('click', function () {
    alert("baaa");
    //alert(document.getElementById("<%#ulTime.ClientID%>").style.display);
    if ($(".selectBox-dropdown-menu").is(":visible")) {
    alert("visible");
    }

    }, false);
       
    }
    */
    window.onload = appl_init;
    
    
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">
       
 
            
<div id="center__bg" runat="server" class="page-contentm my-calls" style="margin-top:-50px;">  <!--class="page-contentm my-calls"-->
     <!--<uc1:Toolbox runat="server" ID="_Toolbox" LoadStyle="false"/>-->

<div id="divLoaderOverViewScreen" class="divLoaderOverViewScreen" style="display:none;">
    
</div>

<div id="divLoaderOverView" class="divLoaderOverView" style="display:none;">        
    <a href="javascript:void(0);" onclick="javascript:showHideDivOverView()">invite</a>
        <asp:Button ID="inviteBtn" runat="server" onclick="inviteBtn_Click"/>
    <div style="margin:0 auto;width:300px;height:300px;background-color:white;">
        this is center
        <span>Hold on... it’s comin’!</span>
        <br />
        <br />
        <img alt="Loading..." src="../PPC/images/ajax-loader.gif"/>
    </div>
</div> 

<div id="divRefund" class="divOverViewRefund" style="display:none;">
    <div class="divRefundClose" onclick="javascript:showHideRefund();">
        <img src="../PPC/images/overview/popup-X.png" alt="X" />
    </div>    
    <div class="divIframeRefund">
        <iframe id="iframeRefund" class="iframeRefund" frameborder="0" scrolling="no"></iframe>
    </div>    
</div>

<div id="divTestCall" class="divOverViewTestCall" style="display:none;">
    <div class="divTestCallClose" onclick="javascript:showHideTestCall();">
        <img src="../PPC/images/overview/popup-X.png" alt="X" />
    </div>    
    <div class="divIframeTestCall">
        <iframe id="iframeTestCall" class="iframeTestCall" frameborder="0" scrolling="no"></iframe>
    </div>    
</div> 

<div class="overviewpage">   

    <div class="left" >
    <a name="anchorSystemMessage"></a>
        <uc1:PpcComment runat="server" ID="_comment" />
        
        <div id="stepsOpen" class="stepsOpen" >
            <div class="stepsOpenInner" onclick="javascript:openSteps();">
                <div class="getStarted">Get Started</div>
                <div id="stepsOpenText" class="stepsOpenText"></div>
            </div>           
        </div>

        

        <div id="steps" class="steps">            
            <uc1:Steps runat="server" ID="_Steps"/>           
        </div>     

        <div id="list" class="leads" >                                  
                                             
                                             
        <asp:UpdatePanel runat="server" ID="_uPanel" UpdateMode="Conditional" >
            <ContentTemplate> 
               
                <asp:PlaceHolder runat="server" ID="placeHolderCalls"></asp:PlaceHolder>
                <div class="clear"></div>
                <div class="title" style="float:left;">My Leads</div>
                
                <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"  CssClass="leadsCount"></asp:Label></div>

                <div id="leadsTime" class="leadsTime" runat="server">
                    
                        <a class="selectBox selectBox-dropdown" style="display: inline-block; -moz-user-select: none;" title="" tabindex="0">
                        <span  id="timeSelected" class="selectBox-label" runat="server">Last month</span>
                        <span class="selectBox-arrow" ></span>
                        </a>

                        <ul runat="server" id="ulTime" class="selectBox-dropdown-menu selectBox-options" style="-moz-user-select: none; width: 197px;display:none;">
                            <li  ID="liTimeAll" runat="server"  class="selectBox-selected">                       
                             <asp:LinkButton ID="lbTimeAll" runat="server" onclick="timeChanged" Text="Since the beginning of time">Since the beginning of time</asp:LinkButton>
                            </li>   

                            <li  ID="liTimeYear" runat="server">                        
                             <asp:LinkButton ID="lbTimeYear" runat="server" onclick="timeChanged" Text="Last year">Last year</asp:LinkButton>
                            </li>

                            <li  ID="liTimeMonth" runat="server">                        
                             <asp:LinkButton ID="lbTimeMonth" runat="server" onclick="timeChanged" Text="Last month">Last month</asp:LinkButton>
                            </li>


                            <li  ID="liTimeWeek" runat="server">                        
                            <asp:LinkButton ID="lbTimeWeek" runat="server" onclick="timeChanged" Text="Last week">Last week</asp:LinkButton>
                            </li>                                    

                        </ul>                    
                               
                </div> 
                    
                <div class="clear"></div>

                <div runat="server" class="leadsSeperator TopSeperator" id="leadsSeperator" ></div>

                <div class="divLeadsRecords">                   
                   <asp:DataList runat="server" ID="datalistLeads"  
                        OnItemDataBound="datalistLeads_ItemDataBound"   Width="623px" 
                       >
                        <HeaderTemplate>
                               
                            <table class="divLeadsTable" cellspacing="0" cellpadding="0" border="0" >
                                <tr class="divLeadsTrHead">                                    
                                    <td class="divLeadsTdHead divLeadsTdHeadStatus">
                                    <div runat="server" id="divStatus" >
                                        <asp:LinkButton ID="lbStatus" runat="server" CommandArgument="status" CommandName="sort" 
                                            OnCommand="lbStatus_Command" >Status</asp:LinkButton><div id="backgroundArrowStatus" runat="server"></div>
                                           <a runat="server" id="recordCalls" class="linkTitleRecordCalls -TurnOnRecordCalls" visible="false" onclick="TurnOnRecordCalls(this);" href="javascript:void(0);">Record Calls</a>
                                       <!--     <asp:HyperLink ID="_recordCalls" runat="server" NavigateUrl="../PPC/FramePpc.aspx?step=4" Text="Record Calls" Visible="false" CssClass="linkTitleRecordCalls"></asp:HyperLink>                              -->
                                           
                                    </div>                                        
                                    </td>
                                    <td class="divLeadsTdHead divLeadsTdHeadDesc">
                                        <div runat="server" id="divDescription" >
                                             <asp:LinkButton ID="lbDescription" runat="server" CommandArgument="heading" CommandName="sort" 
                                                    OnCommand="lbStatus_Command" >Description</asp:LinkButton><div id="backgroundArrowDescription" runat="server"></div>
                                             
                                             
                                        </div>                          
                                    </td>
                                    <td class="divLeadsTdHead divLeadsTdHeadDetails">
                                        <div runat="server" id="divTime" >                                            
                                            <asp:LinkButton ID="lbTime" runat="server" CommandArgument="time" CommandName="sort" 
                                                    OnCommand="lbStatus_Command" >Delivered</asp:LinkButton>                                                                                
                                            <div id="backgroundArrowTime" runat="server"></div>
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" ><div class="leadsSeperator"></div></td>
                                </tr>

                                <tr class="divLeadsTrTestCall">
                                    <td id="td_statusTestCall" runat="server" ></td> <!-- the class divLeadsTd added dynanically when need to show-->
                                    <td id="td_descriptionTestCall" runat="server" ></td>
                                    <td id="td_detailsTestCall" runat="server"></td> <!-- the class divDetailTd added dynanically when need to show-->
                                </tr>

                               <tr>
                                    <td runat="server" id="testCallSeperator" colspan="3" ></td><!-- the class td_seperator added dynanically when need to show-->
                               </tr> 
                               
                               
                        </HeaderTemplate>

                        <ItemTemplate>                        

                            <tr class="divLeadsTr">
                                <td id="td_status" runat="server" class="divLeadsTd"></td>
                                <td id="td_description" runat="server" class="divDescTd"></td>
                                <td id="td_details" runat="server" class="divDetailTd"></td>                                
                            </tr>
                            
                            <tr>
                                <td id="td_seperator" colspan="3" class="td_seperator"></td>
                            </tr>                           
                        </ItemTemplate>

                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:DataList>                    

                    <div id="noRecords" runat="server"  class="noRecords" visible="false">
                        
                    </div>

                    <div id="tr_loader" class="loaderInfinite" >                       
                            <img src="../ppc/images/overview/loader-scroll.gif" alt="Loading..." />                                             
                    </div>

                    <div class="leadsBottom" ></div>

                    </div>

                <asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">
                    <div runat="server" id="div_paging_hover" style="height:50px;">
                    
                    </div>     
                    <ul>
                        <li class="first"><asp:LinkButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click">Previous page</asp:LinkButton></li>
                        <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
                        <li class="last"><asp:LinkButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click">Next page</asp:LinkButton></li>
                        
                    </ul>
                </asp:Panel>
                
            </ContentTemplate>
        </asp:UpdatePanel>       
    </div>

    </div>
    
    <div class="right" >
        
        <div class="right-general invite">
            <uc1:Invite runat="server" ID="Invite1"/> 
            <div class="shadow200"></div>          
        </div>
       
        <div class="right-general">
            <uc1:AccountCompleteness runat="server" ID="_AccountCompleteness" />
            <div class="shadow200"></div>
        </div>
        <div class="right-general">
            <uc1:AnsweringRate runat="server" ID="_answeringRate" />
            <div class="shadow200"></div>
        </div>
        <div class="right-general div-PhoneAvailability">
            <uc1:PhoneAvailability runat="server" ID="_PhoneAvailability" />
            <div class="shadow200"></div>
        </div>
       <div class="right-general WhatsNext" runat="server" id="div_what_next" visible="false">
           <asp:Label ID="lbl_WhatsNext" runat="server" Text="What's next?"></asp:Label>
           <div class="shadow200"></div>
       </div>
       <div class="right-footer">
            <span>Onecall © 2013</span>
            <ul>
                <li><a href="http://www2.noproblemppc.com/">Home</a></li>
                <li><a href="http://www2.noproblemppc.com/adv.html">Advertisers</a></li>
                <li><a href="http://www2.noproblemppc.com/publisher.html">Affiliates</a></li>
                <li class="FoterMenuLast"><a href="http://www2.noproblemppc.com/partners.html">Partners</a></li>
                <li><a href="http://www2.noproblemppc.com/about.html">About</a></li>
                <li><a href="http://www2.noproblemppc.com/GeneralTermsService.htm">Legal</a></li>
                <li class="FoterMenuLast"><a href="http://www2.noproblemppc.com/contact.html">Contact Us</a></li>
            </ul>
       </div>
    </div>
    
</div>   
     
<div id="clear" class="clear"></div>
 
</div>           
  

<asp:Panel ID="panel_iframe" runat="server" CssClass="popModal_del2_ie popModal_del2_hide" style="display:none;"
 Width="750" Height="440">
 <div id="divIframeLoader" class="divIframeLoader">   
    <table>
        <tr>
            <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
        </tr>
    </table>
</div>
<div id="div_iframe_main">
    <div class="top"></div>
    <a id="a1" runat="server" class="span_A" href="javascript:void(0);" onclick="javascript:CloseIframe();"></a>
    <iframe runat="server" id="_iframe" name="_iframe" width="650px" height="560px" frameborder="0" class="iframe3" ALLOWTRANSPARENCY="true"></iframe>
    <div class="bottom2"></div>
</div>
</asp:Panel>


<cc1:ModalPopupExtender ID="_mpe" runat="server"
            TargetControlID="btn_virtual"
            PopupControlID="panel_iframe"
             BackgroundCssClass="modalBackground" 
             
              BehaviorID="_modal"               
              DropShadow="false">
</cc1:ModalPopupExtender>   
 
    <div style="display:none;">

    <asp:Button ID="btn_virtual" runat="server"  style="display:none;" CausesValidation="false"/>
</div>
        <asp:UpdatePanel ID="UpdatePanelVirtual" runat="server">
        </asp:UpdatePanel>
    
        <asp:Label ID="lblDate" runat="server" Text="Date" Visible="false"></asp:Label>       
        <asp:Label ID="lblPrimaryExpertise" runat="server" Text="Heading" Visible="false"></asp:Label>        
       <asp:Label ID="lblRequestSurce" runat="server" Text="Request source" Visible="false"></asp:Label>       
        <asp:Label ID="lblRecordings" runat="server" Text="Recording" Visible="false"></asp:Label>        
        <asp:Label ID="lblRequestNumber" runat="server" Text="Request number" Visible="false"></asp:Label>        
        <asp:Label ID="lblCharge" runat="server" Text="Win" Visible="false"></asp:Label>        
        <asp:Label ID="lblStatusPic" runat="server" Text="Charged" Visible="false"></asp:Label> 
        
        
     
     <asp:Label ID="lbl_NoResult" runat="server" Visible="false"
     Text="There are not results"></asp:Label>
     
      <asp:Label ID="lbl_ChooseItem" runat="server" Visible="false"
     Text="Choose"></asp:Label>
     
        <asp:Label ID="lbl_dateFromBeforTo" runat="server" Text="From Date must be before To Date!"
         Visible="false"></asp:Label>
         
        <asp:Label ID="lbl_RecordError" runat="server" Text="Server problem. Can not hear the recording"
         Visible="false"></asp:Label>
      
      <asp:Label ID="lbl_UnitCalls" runat="server" Text="Unit Calls" Visible="false"></asp:Label> 
      <asp:Label ID="lbl_Days" runat="server" Text="Days" Visible="false"></asp:Label> 
      <asp:Label ID="lbl_Calls" runat="server" Text="Calls" Visible="false"></asp:Label> 
     
     <asp:Label ID="lblRecordMached" runat="server" Text="records" Visible="false"></asp:Label>
   <asp:Label ID="lblTitleAnalytics" runat="server" Text="My Calls Reports" Visible="false"></asp:Label>
    </asp:Content>