﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LocationMap.aspx.cs" Inherits="Management_LocationMap" %>

<%@ Register src="../Controls/MapControl.ascx" tagname="MapControl" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&amp;sensor=false&language=en"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyByICNsdF00OXlUGTnzGsIfNmk0Mo7xqcw&sensor=false&libraries=places&language=en"></script>
  <!--  <script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places" type="text/javascript"></script>-->

    <script type="text/javascript" src="<%# ResolveUrl("~") %>PPC/script/JSGeneral.js"></script>
    <script type="text/javascript" src="../CallService.js"></script>
    <script type="text/javascript" src="../general.js"></script>

     <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="../PPC/script/jquery-ui-1.8.16.custom.effect.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../PlaceHolder.js"></script>

    <link href="<%# ResolveUrl("~") %>PPC/samplepPpc.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        window.onload = function () {
            parent.window.setLinkStepActive("linkStep3");
            parent.hideDiv();
        }
        function CheckLocationDetails() {
           
            /*
            var LPlace = GetSelectLargeAreaId();
            var FullAddress = GetFullAddress();
            if (FullAddress.length == 0 || LPlace == "false") {
                var _comment = (!LPlace) ? "<# _map.ChooseLargerArea %>" : "<# _map.SelectFromList %>";
                SetServerComment(_comment);
                SetInvalidLocation();
                return false;
            }
            */
            var _details = GetAllDetails();
            if (_details.length == 0) {
                SetServerComment("<%# _map.SelectFromList %>");
                SetInvalidLocation();
                return false;
            }
           
            document.getElementById("<%# hf_lat.ClientID %>").value = _details.lat_lng.lat();
            document.getElementById("<%# hf_lng.ClientID %>").value = _details.lat_lng.lng();
            document.getElementById("<%# hf_radius.ClientID %>").value = (_details.radius * 1.6);
            document.getElementById("<%# hf_LargePlace.ClientID %>").value = _details.AreaId;
            document.getElementById("<%# hf_FullAddress.ClientID %>").value = _details.FullAddress;
            parent.showDiv();
            return true;
        }
        function UpdateSuccessStep4() {
            top.SetServerComment('Update success');
            parent.step4();
        }
    
    </script>
</head>
<body class="location_map">
    <form id="form1" runat="server" >
    <div >
        <asp:HiddenField ID="hf_lat" runat="server" />
        <asp:HiddenField ID="hf_lng" runat="server" />
        <asp:HiddenField ID="hf_radius" runat="server" />
        <asp:HiddenField ID="hf_LargePlace" runat="server" />
        <asp:HiddenField ID="hf_FullAddress" runat="server" />
        <uc1:MapControl ID="_map" runat="server" />



        <asp:LinkButton ID="btn_SetLocation" runat="server" Text="Update" OnClientClick="return CheckLocationDetails();" OnClick="btn_SetLocation_Click" CssClass="btn_next"></asp:LinkButton>
        
    </div>
    </form>
</body>
</html>
