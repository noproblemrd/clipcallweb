<%@ Page Language="C#" AutoEventWireup="true" 
MasterPageFile="~/Management/MasterPageAdvertiser.master" 
CodeFile="frame.aspx.cs" Inherits="Management_frame"%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    
    

<script type="text/javascript">    
    
    var arrStepsNames=new Array();
    
    
    
    function SentenceInit()
    {
        arrStepsNames[0]="";     
        arrStepsNames[1]= "<%#Hidden_GeneralInfo.Value%>";
        arrStepsNames[2]= "<%#GetSegmentsArea%>";
        arrStepsNames[3]= "<%#GetCities%>";
        arrStepsNames[4]= "<%#Get_Time%>"; 
        arrStepsNames[5]= "<%#Get_MyCredits%>";
        arrStepsNames[6]= "<%#GetPaymentInfo%>"; 
    }
    
    function witchAlreadyStep(level)
    {      
        //alert("level:" + level);
        if(arrStepsNames.length==0)
             SentenceInit();
        var steps=document.getElementById("ulSteps");
        var alreadyStep=level*1+1;
        
        for(i=0;i<steps.childNodes.length;i++)
        {
            if(steps.childNodes[i].id!=undefined)
            {
                liNumber=steps.childNodes[i].id.replace('liStep','');
                //alert(steps.childNodes[i].id + ' ' + liNumber);
                listStepObj=document.getElementById('liStep' + liNumber);
                if(liNumber<=alreadyStep)
                {      
                               
                   listStepObj.innerHTML="<a id='linkStep" +
                    liNumber + "' href='javascript:void(0);' style='cursor:hand;' class='mouseover' onclick='javascript:step" +
                     liNumber + "();'><span class='mouseover' >" + liNumber + 
                     "</span><span class='titleNavigation'>" + arrStepsNames[liNumber]+ "</span></a>";
                }    
                else
                {
                   listStepObj.innerHTML="<a id='linkStep" +
                    liNumber + "' href='javascript:void(0);' style='cursor:text;'  onclick='javascript:return false;'><span class='mouseover'>" + liNumber + 
                     "</span><span class='titleNavigation'>" + arrStepsNames[liNumber]+ "</span></a>";
                }                
            }            
        }


     //   SetIframeHeight();
        
        //step5();         
      
    }
    
    function setInitRegistrationStep(level)
    {        
        //setLinkStepActive('linkStep' + (level*1+1));
        //alert("level " + level);
        switch(level)
        {
           case 0:
             step1();
             break;
             
           case 1:
             step2();
             break;
             
           case 2:
             step3();
             break;
             
           case 3:
             step4();
             break;
             
           case 4:
             step5();
             break;
             
           case 5:
             step1();
             break;
             
           case 6:
             step1();
             break;    
           
           default:
            break;  
        }
    }
    
    
    function init()
    {       
        var ifFinished=false;
        if(ifFinished)
            witchAlreadyStep(1);        
        //setLinkStepActive('linkStep1');
   //     document.getElementById("<%# iframe.ClientID %>").document.onload = OnIframeLoad;
    }
    /*
    function OnIframeLoad() {
    
        var _domain = "";
        try {
            _domain = window.frames["iframe"].document.domain;
        }
        catch (ex) {
            document.getElementById("<%# iframe.ClientID %>").src = "professionalDetails.aspx";
            return;
        }
        if(_domain != top.document.domain)
            window.frames["iframe"].history.go(-1);
            
    }
    */
    function setLinkStepActive(linkId)
    {       
      document.getElementById(linkId).className="active mouseover";      
    }    
   
    function goUp()
    {
       window.scrollTo(0,0); 
    }
    
    function step1()
    {     
        //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/professionalDetails.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '950px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no");     
    }    
   
    
    function step2()
    { 
       
       //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/professionchooseprofessions.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '670px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no");  
    }
    
    function step3()
    {  
        //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/<%#cityPage%>';
        document.getElementById("<%# iframe.ClientID %>").height = '900px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow-x:hidden; overflow-y:auto;");
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "auto");  
    }
    
    function step4()
    {  
        //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/professionalDates.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '850px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no");    
    }
    
    function step5()
    {        
        //window.scrollTo(0,0);      
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/professionalCallPurchase.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '670px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow-x:hidden; overflow-y:auto;");
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no");    
         
    }
    
    function step6()
    {
          
         document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/professionalPayment.aspx';
         document.getElementById("<%# iframe.ClientID %>").height = '860px';
         document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow-x:hidden; overflow-y:auto;");
         document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no");    

          
    }




    window.onload = init;
    
    
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server"> 
  
<!--------------------------------- Here starts the relevant section -------------------------------->								
				
<div class="iframe-content">
    <iframe id="iframe" class="iframe"  frameborder="0" scrolling="no" runat="server" ></iframe>
</div>
					
<div class="tab-navigation" id="tab_navigation">
	<ul id="ulSteps">
		<li id="liStep1" class="first"></li>
		<li id="liStep2"></li>
        <li id="liStep3"></li>
		<li id="liStep4"></li>
		<li id="liStep5"></li>
		<li id="liStep6" class="last"></li>
	</ul>
</div>


					
<asp:HiddenField ID="Hidden_GeneralInfo" runat="server" Value="General Info" />
<asp:HiddenField ID="Hidden_SegmentsArea" runat="server" Value="Heading" />
<asp:HiddenField ID="Hidden_Cities" runat="server" Value="Working Area" />
<asp:HiddenField ID="Hidden_Time" runat="server"  Value="Availability Schedule"/>
<asp:HiddenField ID="Hidden_MyCredits" runat="server" Value="My credits" />
<asp:HiddenField ID="Hidden_PaymentInfo" runat="server" Value="Payment Info" />
<div id="div_height" runat="server"></div>

</asp:Content>
