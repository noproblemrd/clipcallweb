<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Management/MasterPageAdvertiser.master"
 CodeFile="Mycalls.aspx.cs" Inherits="Management_Mycalls2"%>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="Toolbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
  <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />

  <link href="../Controls/Toolbox/StyleToolboxReport.css" rel="Stylesheet" type="text/css" />

<link rel="stylesheet" href="FusionCharts/Contents/Style.css" type="text/css" />
<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>

<script  type="text/javascript" src="FusionCharts/JSClass/FusionCharts.js"></script>

<!--<script type="text/javascript" src="../general.js" ></script>-->
<!--<script type="text/javascript" src="../jQuery/jquery.min.js" ></script>-->

<script  type="text/javascript" src="../Calender/_Calender.js"></script>
<script type="text/javascript"  src="../CallService.js"></script>


 
<script  type="text/javascript"  >  
    window.onload = appl_init;
    function appl_init() {
        hideDiv();       
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {       
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }
    
    function ChangeCalendarView(sender,args)
    {
        sender._switchMode("", true);           
    }
    
    function LoadChart(fileXML, divId, chartPath)
    {
        var chart = new FusionCharts(chartPath, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataXML(fileXML);
        chart.render(divId);

    }
    
   
    function OpenCloseDetail(contr, td_open, _iframe, _path)
    {
        var contrl=document.getElementById(contr);
        var _div = document.getElementById("<%# center__bg.ClientID %>");
        var _heigth = _div.scrollHeight;
        contr = "[0]" + contr;
        if(contrl.style.display=='none')  
        { 
            CheckFor(contr+";1");            
            contrl.style.display='';
            document.getElementById(td_open).className="open first";  
            _div.style.height=(_heigth+400)+'px';
            if(document.getElementById(_iframe).src == "javascript:void(0)")
            {
                document.getElementById(_iframe).src = _path;
                showDiv();
            }
        }
        else
        {
            CheckFor(contr+";-1");
            contrl.style.display='none';
            document.getElementById(td_open).className="close first";
            _div.style.height=(_heigth-400)+'px';
        }
        
    }
    function ResetWin()
    {
        document.getElementById("<%# center__bg.ClientID %>").style.height='auto';
    }
    function CheckFor(contr) {
        CallServer(contr, "");
        }
    function ReciveServerData(retValue) {
       
        
    }


    //record
    function OpenRecord(record_path) {
        var _url = '<%# record_path %>';
        _url = _url + "?record=" + encodeURIComponent(record_path);
        window.open(_url, 'record', 'height=300,width=300');
    }
    /*
    function OpenRecord(_id)
   {
        var url = "../RecordService.asmx/GetPath";
        var params = "_id="+_id;
       CallWebService(params, url, OnCompleteRecord, On_Error);
   }
   function OnCompleteRecord(arg)
   {
        if(arg.length==0)
        {
            alert("<%# lbl_RecordError.Text %>");
            return;
        }
 
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        if(is_chrome)
        {
            oWin = window.open('<%# GetAudioChrome %>?audio='+ arg, "recordWin", "resizable=0,width=500,height=330");     
            setTimeout("CheckPopupBlocker()", 2000);
            
        }
        else
            window.location.href=arg;

    }
    */
    function CheckPopupBlocker() {
//        if (_hasPopupBlocker(oWin))
//            alert(document.getElementById('<#Hidden_AllowPopUp.ClientID  %>').value);
    }
   function On_Error(){}
  
   // Open popup complaints
   function CloseIframe()
    {
       $find('_modal').hide();
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie popModal_del2_hide";
       document.getElementById("<%# _iframe.ClientID %>").src = "javascript:void(0);";
    }
    function OpenIframe(_path)
    {   
        
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie";
       document.getElementById("<%# _iframe.ClientID %>").src = _path;
       HideIframeDiv();  
       $find('_modal').show();
    }
    function HideIframeDiv()
    {
       document.getElementById("divIframeLoader").style.display = "block";
       document.getElementById("div_iframe_main").style.display = "none";
    }
    function ShowIframeDiv()
    {
        document.getElementById("divIframeLoader").style.display = "none";
       document.getElementById("div_iframe_main").style.display = "block";
    }
    function CloseIframe_after()
    {
 //       window.frames[0].IframeClosed();
        CloseIframe();
    }
</script>

</asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">
        
       
 
            
<div id="center__bg" runat="server" class="page-contentm my-calls">  <!--class="page-contentm my-calls"-->
     <uc1:Toolbox runat="server" ID="_Toolbox" LoadStyle="false"/>
  
   
<div class="contpage">       
    <div class="form-fieldM">          
        
         	<div><asp:Label ID="lbl_Status" CssClass="label" runat="server" Text="Status"></asp:Label></div>
                <asp:DropDownList ID="lb_status" CssClass="form-select" runat="server"  Width="155px">
                <asp:ListItem Text="Win" Value="Win"></asp:ListItem>
                 <asp:ListItem Text="Lost" Value="Lose"></asp:ListItem>
                 <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
            </asp:DropDownList>
      
     </div> 
  
       <div class="mycalls"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>  
        <div class="clear"></div>                            
    <div id="list" class="list">          
        <asp:UpdatePanel runat="server" ID="_uPanel" UpdateMode="Conditional">
            <ContentTemplate>                    
                <asp:PlaceHolder runat="server" ID="placeHolderCalls"></asp:PlaceHolder>
                <div class="clear"></div>
                <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                <div class="div_gridMyCall">
                    <asp:Repeater runat="server" ID="_reapeter">
                    <HeaderTemplate>
                    <table class="data-table">
                        <thead>
                            <tr>
                                <th style="width: 30px;">&nbsp;</th>
                                <th style="width: 40px;"><asp:Label ID="Label31" runat="server" Text="<%#lblRequestNumber.Text %>"></asp:Label></th>
			                    <th style="width: 120px;"><asp:Label ID="Label50" runat="server" Text="<%#lblDate.Text %>"></asp:Label></th>
			                
			                    <th style="width: 140px;"><asp:Label ID="Label1" runat="server" Text="<%#lblPrimaryExpertise.Text %>"></asp:Label></th>
			                    <th style="width: 180px;"><asp:Label ID="Label51" runat="server" Text="<%#lblRequestSurce.Text %>"></asp:Label></th>
			                    <th style="width: 55px;"><asp:Label ID="Label32" runat="server" Text="<%#lblCharge.Text %>"></asp:Label></th>
			                    <th style="width: 55px;"><asp:Label ID="Label2" runat="server" Text="<%#lblStatusPic.Text %>"></asp:Label></th>
			                    <th style="width: 40px;" runat="server" id="th_records"><asp:Label ID="Label53" runat="server" Text="<%#lblRecordings.Text %>"></asp:Label></th>
                            </tr>
                        </thead>
                        <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr runat="server" id="tr_show">
                                <td class="close first" id="td_openClose" runat="server"><a class="arrow" runat="server" id="a_open"></a></td>
                                <td><asp:Label ID="lbl_TicketNumber" runat="server" Text="<%# Bind('TicketNumber') %>"></asp:Label></td>
						        <td><asp:Label ID="Label30" runat="server" Text="<%# Bind('Created_On') %>"></asp:Label></td>
						       
						        <td><asp:Label ID="Label3" runat="server" Text="<%# Bind('PrimaryExpertise') %>"></asp:Label></td>
						        <td><asp:Label ID="Label55" runat="server" Text="<%# Bind('Title') %>"></asp:Label></td>
						        <td><asp:Image ID="Image_win" runat="server" ImageUrl="<%# Bind('win') %>" /></td>
						        <td>
						            <span id="span_StatusTitle" runat="server" visible="<%# Bind('InRefundStatus') %>" class="StatusTitle">
                                        <asp:Image ID="img_status" runat="server" ImageUrl="<%# Bind('StatusPic') %>" AlternateText="<%# Bind('RefundStatus') %>" />
                                        <asp:Label ID="lbl_Refund_status_tooltip" runat="server" Text="<%# Bind('RefundStatus') %>" CssClass="tooltip"></asp:Label>
                                    </span>
						        </td>
						        <td runat="server" id="td_records">
						            <a id="a_recording" runat="server" href="<%# Bind('Recording') %>" visible="<%# Bind('HasRecord') %>">
                                        <asp:Image ID="img_record" runat="server" ImageUrl="~/Management/calls_images/icon_speaker.png" />
                                    </a>
                                    <asp:Label ID="lbl_CallId" runat="server" Text="<%# Bind('CallId') %>" Visible="false"></asp:Label> 
						        </td>
						    </tr> 
						    <tr id="tr_openClose" runat="server" style="display: none;">
						       <td colspan="8" runat="server" id="td_details">
						            <iframe runat="server" id="iframe_details" marginheight="0" src="javascript:void(0)" 
	                                marginwidth="0" frameborder="0" class="iframeM" width="620px"  height="350px" scrolling="no"
	                                ></iframe>
						       </td>        
						    </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                    </table>
                    </FooterTemplate>
                    </asp:Repeater>
                   
                </div>
                <asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">     
                    <ul>
                        <li class="first"><asp:LinkButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click">Previous page</asp:LinkButton></li>
                        <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
                        <li class="last"><asp:LinkButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click">Next page</asp:LinkButton></li>
                    </ul>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>       
    </div>
 </div>        
<div id="clear"></div>
  
 
</div> 
           
           


<asp:Panel ID="panel_iframe" runat="server" CssClass="popModal_del2_ie popModal_del2_hide" style="display:none;"
 Width="750" Height="440">
 <div id="divIframeLoader" class="divIframeLoader">   
    <table>
        <tr>
            <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
        </tr>
    </table>
</div>
<div id="div_iframe_main">
    <div class="top"></div>
    <a id="a1" runat="server" class="span_A" href="javascript:void(0);" onclick="javascript:CloseIframe();"></a>
    <iframe runat="server" id="_iframe" name="_iframe" width="650px" height="560px" frameborder="0" class="iframe3" ALLOWTRANSPARENCY="true"></iframe>
    <div class="bottom2"></div>
</div>
</asp:Panel>
<cc1:ModalPopupExtender ID="_mpe" runat="server"
            TargetControlID="btn_virtual"
            PopupControlID="panel_iframe"
             BackgroundCssClass="modalBackground" 
             
              BehaviorID="_modal"               
              DropShadow="false">
</cc1:ModalPopupExtender>    
    <div style="display:none;">

    <asp:Button ID="btn_virtual" runat="server"  style="display:none;" CausesValidation="false"/>
</div>
        <asp:UpdatePanel ID="UpdatePanelVirtual" runat="server">
        </asp:UpdatePanel>
    
        <asp:Label ID="lblDate" runat="server" Text="Date" Visible="false"></asp:Label>       
        <asp:Label ID="lblPrimaryExpertise" runat="server" Text="Heading" Visible="false"></asp:Label>        
       <asp:Label ID="lblRequestSurce" runat="server" Text="Request source" Visible="false"></asp:Label>       
        <asp:Label ID="lblRecordings" runat="server" Text="Recording" Visible="false"></asp:Label>        
        <asp:Label ID="lblRequestNumber" runat="server" Text="Request number" Visible="false"></asp:Label>        
        <asp:Label ID="lblCharge" runat="server" Text="Win" Visible="false"></asp:Label>        
        <asp:Label ID="lblStatusPic" runat="server" Text="Charged" Visible="false"></asp:Label> 
        
        
     
     <asp:Label ID="lbl_NoResult" runat="server" Visible="false"
     Text="There are not results"></asp:Label>
     
      <asp:Label ID="lbl_ChooseItem" runat="server" Visible="false"
     Text="Choose"></asp:Label>
     
        <asp:Label ID="lbl_dateFromBeforTo" runat="server" Text="From Date must be before To Date!"
         Visible="false"></asp:Label>
         
        <asp:Label ID="lbl_RecordError" runat="server" Text="Server problem. Can not hear the recording"
         Visible="false"></asp:Label>
      
      <asp:Label ID="lbl_UnitCalls" runat="server" Text="Unit Calls" Visible="false"></asp:Label> 
      <asp:Label ID="lbl_Days" runat="server" Text="Days" Visible="false"></asp:Label> 
      <asp:Label ID="lbl_Calls" runat="server" Text="Calls" Visible="false"></asp:Label> 
     
     <asp:Label ID="lblRecordMached" runat="server" Text="records matched your query" Visible="false"></asp:Label>
   <asp:Label ID="lblTitleAnalytics" runat="server" Text="My Calls Reports" Visible="false"></asp:Label>
    </asp:Content>
