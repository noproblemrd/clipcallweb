﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="categories2014.aspx.cs" Inherits="Management_categories2014" ClientIDMode="Static" %>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="//code.jquery.com/jquery-1.7.1.min.js" type="text/javascript"></script>

     <script type="text/javascript" src="../PlaceHolder.js"></script>
    <script type="text/javascript" src="../ppc/script/jquery-ui-1.8.16.custom.autocomplete.min.js"></script>

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
    <link href="../Formal/jquery-ui-1.8.16.custom.css" type="text/css" rel="Stylesheet" />

    <script type="text/javascript" src="../ppc/script/JSGeneral.js"></script>
    <script type="text/javascript" src="../ppc/ppc.js"></script>
    <script type="text/javascript" src="../general.js"></script>


    <link href="../PPC/samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>
    <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
    <script type="text/javascript">        try { Typekit.load(); } catch (e) { }</script>
    <link href="../Ladda-master/dist/ladda.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Ladda-master/dist/spin.min.js"></script>
    <script type="text/javascript" src="../Ladda-master/dist/ladda.min.js"></script>
 <script>
   
     function setPlaceHolder()
     {
        $("input[type=text],input[type=password], textarea").filter(function () {
            return $(this).attr("place_holder") !== undefined;
            }).each(function (index) {
            var _place_holde = $(this).attr('place_holder');
            var _empty_class = $(this).attr('EmptyClass');
            var _elem;
            if ($(this).is("textarea")) {
                _elem = ($("<textarea>")//.offset({ top: this.offsetTop, left: this.offsetLeft })
                //         .height(this.offsetHeight)
                //         .width(this.offsetWidth)
                    .attr('id', "pt" + index)
                    .css({ 'display': 'inline' }).get(0));

                if ($(this).attr('cols') != undefined)
                    $(_elem).attr('cols', $(this).attr('cols'));
                if ($(this).attr('rows') != undefined)
                    $(_elem).attr('rows', $(this).attr('rows'));
            }
            else {
                _elem = ($("<input>").attr("type", "text")//.offset({ top: this.offsetTop, left: this.offsetLeft })
                //             .height(this.offsetHeight)
                //             .width(this.offsetWidth)

                    .attr('id', "pt" + index)
                    .css({ 'display': 'inline' }).get(0));
            }
            if ($(this).attr('type') == 'password' && $(this).attr('autocomplete') != 'off')
                $(this).attr('autocomplete', 'off');
            //     if ($(this).attr('holder-position') == "none")
            //         $(_elem).css('position', '');
            if ($(this).attr('class')) {
                //   $(_elem).attr('class', $(this).attr('class'));
                var _classes = $(this).attr('class').split(' ');
                for (var i = 0; i < _classes.length; i++) {
                    var str = $.trim(_classes[i]);
                    if (str.length > 0 && str.charAt(0) != '-')
                        $(_elem).addClass(str);
                }
            }
            $(_elem).val(_place_holde);
            //    $(_elem).appendTo($(this).parent());
            $(this).parent().get(0).insertBefore(_elem, this)
            if ($(this).val().length > 0)
                $(_elem).hide();
            else
                $(this).hide();
            if ($(this).attr('HolderClass') != undefined)
                $(_elem).addClass($(this).attr('HolderClass'));
            else
                $(_elem).css({ 'color': '#CCCCCC', 'fontStyle': 'italic', 'fontWeight': 'lighter' });
            var _inputId = this;
            $(_elem).focus(function () {
                $(this).hide();
                $(_inputId).show();
                $(_inputId).focus();
                if (_empty_class && _empty_class.length > 0)
                    $(_elem).removeClass(_empty_class);
            });
            $(this).blur(function () {
                if ($(this).val().length > 0)
                    return;
                $(this).hide();
                if (_empty_class && _empty_class.length > 0)
                    $(_elem).addClass(_empty_class);
                $(_elem).show();
                $(_elem).blur();

            });

        });
     }

     /********** Service  ****************/
     var service_place_holder;
     var IsSelected = false;

     function setAutoComplete()
     {
         $("._Heading").autocomplete({
                
             source: function (request, response) {
                <%--
                 var _txt = document.getElementById("<%# txt_Category.ClientID  %>");
                 --%>
                                 
                 SourceText = request.term;
                
                 $.ajax({
                     url: "<%# GetHeadingServiceList %>",
                     data: "{ 'TagPrefix': '" + request.term + "'}",
                     dataType: "json",
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     dataFilter: function (data) { return data; },
                     success: function (data) {

                         response($.map(data.d, function (item) {
                             return {
                                 value: item
                             }
                         }))
                     },
                     error: function (XMLHttpRequest, textStatus, errorThrown) {
                         //              alert(textStatus);
                     },
                     complete: function (jqXHR, textStatus) {
                         //  HasInHeadingRequest = false;
                     }

                 });
             },
             minLength: 1,
             close: function (event, ui) {              
               
                 //alert("sfsd");
                 if (!IsSelected && event.target.value.length != 0) {
                     SlideElement($(this).siblings('.inputComment').attr('id'), true); 
                     $(this).siblings('.inputError').show();
                 }
                 IsSelected = false;
                 
             },
             open: function (event, ui) {
                
                 SlideElement($(this).siblings('.inputComment').attr('id'), false); 
                 $(this).siblings('.inputError').hide();
                
             },
             select: function (event, ui) {                
              
                 SlideElement($(this).siblings('.inputComment').attr('id'), false); 
                
                 $(this).siblings('.inputValid').show();
                 $(this).siblings('.inputError').hide();
                 IsSelected = true;
             }
         });
     }

     $(function () {
        setAutoComplete();
     });

     function Set_Service() {   
         
         /************ check at least the first category that is valid *************/

         /*
         var txt_Category = document.getElementById("txt_Category1");
         var _service = txt_Category.value;        
         
         
         var div_ServiceError = document.getElementById("div_ServiceError1");
         RemoveCssClass(txt_Category, "textError");
         RemoveCssClass(txt_Category, "textValid");
         if (_service.length == 0) {
             _SetServerComment("<%# FieldMissing %>");
             div_ServiceError.style.display = "block";
             //AddCssClass(txt_Category, "textError");
             <%--
             document.getElementById("<%# a_categories.ClientID %>").style.display = "block";
             --%>
             return false;
         }

         div_ServiceError.style.display = "none";
         */

         var ifValid=true;

         //document.getElementById("div_ServerComment").style.display="none";

         var _service_accumulation="";
         $('._Heading[place_holder]').each(function()
            {
                //alert($(this).val());
                 if ($(this).val().length== 0) {                    
                    $(this).siblings('.inputError').show();
                    $(this).siblings('.inputValid').hide();
                    ifValid=false;
                 }

                 else
                 {                 
                    $(this).siblings('.inputError').hide();
                    $(this).siblings('.inputValid').show();
                 }

                _service_accumulation+=$(this).val()+ ",";
            }
         )
         

         if(!ifValid)
         {
            _SetServerComment("<%# FieldMissing %>");                                   
            return false;
         }      
         
         if (!ifExplorer8AndLess())
         {            
            activeButtonAnim('.progress-demo #btn_SendService',200);
         }
         
         //div_ServiceError.style.display = "none";
         document.getElementById("<%# hf_Heading.ClientID %>").value = _service_accumulation;
         //alert(document.getElementById("<%# hf_Heading.ClientID %>").value);
         
         return true;
     }

     function CheckHeading(_elem) {

         //  Text_Blur(_elem, service_place_holder);
         setTimeout(function () {
             // SlideElement("div_ServiceComment", false);// document.getElementById("div_ServiceComment").style.display = "none";
             var div_ServiceSmile = document.getElementById("div_ServiceSmile");
             var div_ServiceError = document.getElementById("div_ServiceError");
             //   var div_ServiceComment = document.getElementById("div_ServiceComment");

             var name = _elem.value; //GetText_PlaceHolder(_elem, service_place_holder);
             if (name.length == 0) {
                 div_ServiceSmile.style.display = "none";
                 div_ServiceError.style.display = "none";
                 RemoveCssClass(_elem, "textError");
                 return;
             }
             var prms = "name=" + name;
             var url = "<%# GetIsHeadingService %>";

             CallWebService(prms, url,
                function (arg) {

                    if (arg.toLowerCase() == "true") {
                        div_ServiceSmile.style.display = "block";

                        div_ServiceError.style.display = "none";
                        RemoveCssClass(_elem, "textError");
                        AddCssClass(_elem, "textValid");
                        IsSelected = true;
                        SlideElement("div_ServiceComment", false);
                    }
                    else {
                        div_ServiceSmile.style.display = "none";

                        div_ServiceError.style.display = "block";
                        AddCssClass(_elem, "textError");
                        SlideElement("div_ServiceComment", true);
                    }
                },
                function () {
                    SlideElement("div_ServiceComment", false);
                    div_ServiceSmile.style.display = "none";

                    div_ServiceError.style.display = "block";
                    AddCssClass(_elem, "textError");
                    SlideElement("div_ServiceComment", true);
                });

         }, 250);
     } 
     
     
    function setValidIcons()
    {
        $('._Heading[place_holder]').each(function()
            {
                //alert($(this).val());
                if($(this).val()!="")
                {
                    $(this).siblings('.inputError').hide();
                    $(this).siblings('.inputValid').show();
                }

                else
                {
                    $(this).siblings('.inputError').hide();
                    $(this).siblings('.inputValid').hide();
                }
            }
        )
    }

    function UpdateSuccess() {                
            SetServerComment2("<%#UpdateSuccess%>");
    }

    function UpdateFailed() {
        SetServerComment2("<%#UpdateFaild%>");

    }

    function invalidData() {       
        SetServerComment2("<%#invalidData%>");

    }

    function SendSucceed() {            
            //stopActiveButtonAnim();
            UpdateSuccess();
    }

    function SendFaild() {
        UpdateFailed();
        //stopActiveButtonAnim();
    }


    function init() {

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndHandler);

        setValidIcons();

         parent.window.setLinkStepActive("linkStep2");
         parent.hideDiv();
    }

    function startRequest(sender, e) {
        //alert("baaa start");
    }
    function EndHandler(sender, e){
      //alert("baaa");
      setAutoComplete();
      setPlaceHolder();
      setValidIcons();
    }   

    window.onload = init;
    
 </script>


    <script>
        /*
        function init(sender, args) {
            try {
                //alert("init");
                parent.window.setLinkStepActive("linkStep2");
                parent.hideDiv();
            } catch (ex) { }
        }

        window.onload = init;
        */
    </script>

     <script type="text/javascript">
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-21441721-1']);
         _gaq.push(['_setDomainName', 'noproblemppc.com']);
         _gaq.push(['_trackPageview']);

         (function () {
             var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
             ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
             var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
    </script>


</head>
<body class="bodyDashboard">
    <form id="form1" runat="server">
    <div>
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
   

    <div class="businessCategories dashboardIframe"> 
      
        <uc1:PpcComment runat="server" ID="PpcComment1" />

        <div class="titleFirst">
            <span>Categories</span>
        </div>   
   

        <div class="inputParent">  
        
                <%--       
                <div class="inputCategoryContainer">
                    <asp:TextBox ID="txt_Category" runat="server" CssClass="_Heading textActive"  HolderClass="place-holder"></asp:TextBox>           
               
                    <div class="inputValid" style="display:none;"  id="div_ServiceSmile"></div>
                    <div class="inputError" style="display:none;"  id="div_ServiceError"></div>
                    <div class="inputComment" style="display:none;" id="div_ServiceComment">                    
                            <%= ServicePlaceHolder%>
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                 </div>
                --%>               
                 
                <asp:UpdatePanel runat="server" ID="updatePanel1" UpdateMode="Conditional">
                     <ContentTemplate>
                           <asp:Panel ID="pnlAddServices" CssClass="pnlAddServices" runat="server">
                           </asp:Panel>                
                      </ContentTemplate>
                </asp:UpdatePanel>

                <div class="newCategory">
                    <asp:LinkButton ID="lbAddButton" runat="server" onclick="lbAddButton_Click" >+ New category</asp:LinkButton>
                </div>  
               
        </div>        
        
        <div id="containerNext" class="containerNext">
                <section class="progress-demo">
                <asp:LinkButton ID="btn_SendService" runat="server" data-style="expand-right" data-color="blue" class="ladda-button" OnClientClick="javascript:return Set_Service();" OnClick="btn_SendService_click">
                    <span class="ladda-label">Update</span><span class="ladda-spinner">
                    </span><div class="ladda-progress" style="width: 138px;"></div>
                  </asp:LinkButton>                 
                </section>      
         </div>  


           

        <asp:HiddenField ID="hf_Heading" runat="server" />
    </div>
    
     
    </div>
    </form>
</body>
</html>
