﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Management_AudioChrome : System.Web.UI.Page
{
    string _audio;
    protected void Page_Load(object sender, EventArgs e)
    {
        _audio = Request.QueryString["audio"];
        form1.DataBind();

    }
    protected string GetPath
    {
        get { return _audio; }
    }
}
