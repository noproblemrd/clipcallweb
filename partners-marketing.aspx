﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPPartner.master" AutoEventWireup="true" CodeFile="partners-marketing.aspx.cs" Inherits="partners_marketing" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">


			
			
	 		<div class="subtext_partners">
    		<p>Pay-per-call campaigns are a great addition to your client's media mix&#151;they convert 10X the rate of search campaigns! No Problem drives ready-to-spend prospects to your client's call center or local office and your client pays only for calls received. That's true value rather than empty clicks.</p>
			</div>
		
			<div class="factspartners">
				<h3 class="subclass">Some facts on No Problem's pay-per-call platform:</h3>

        		<div class="marletingfactsblock01"><p><span class="sloganpartnersleft">An average CPM of</span></p><h4><span class="smalltextpartnersright">$3000 to $5000 USD</span></h4></div>

       			<div class="marletingfactsblock02"><h4><span class="sloganpartners">Exposure to tens of thousands</span></h4><p><span class="slogansmallpartners">of customer requests per day</span></p></div>

        		<div class="marletingfactsblock03"><h4><span class="sloganpartners">Full 30% to 40%</span></h4><p><span class="slogansmallpartners">conversion rate from calls to real jobs</span></p></div>

        		<div class="marletingfactsblock04"><h4><span class="sloganpartners">Over 95% of our customers</span></h4><p><span class="slogansmallpartners">are loyal and increasing their exposure</span></p></div>

      </div>
	
			<div class="clear"></div> 

		
		<div class="boxleft"> 

		    	<h3 class="subclass">How pay-per-call campaigns <br />can benefit your clients:</h3>

		        <div class="toggle-content">

				
					
				<h5>Connect by phone immediately to motivated customers</h5>
				Consumers who reach out by phone are typically more ready to spend. Our pay-per-call system brings them right to your client.
				
				<h5>Pay only for actual calls </h5>
				<p>If your client doesn't receive calls, she doesn't pay a penny. </p>
				
				<h5>Decide the price only after evaluating the lead</h5>
				<p>Your client can listen to the customer request and decide whether to accept the call and how much he is willing to pay.</p>
				
				<h5>Track calls, manage budget, calculate ROI and more</h5>
				<p>Our Advertiser Dashboard lets your client completely control her total budget, decide where to do business and when, and track the amount of calls she receives, the details of each call and how many calls she's lost.</p>
				
				<h5>No set up fees, contracts or monthly fees</h5>
				<p>We believe your client should pay only for results. No calls? 
				He doesn't pay.</p>
				 
				<p>Contact us to hear more about how we can bring value to your clients.</p>			

				
				<a class="contact_us" href="contact.aspx"></a>

				</div>
			</div>	
				<div class="boxright"> 
	            <h3 class="subclass">Here's how it works:</h3>
     
				
				<div class="adv01pink">We find ready-to-spend customers through our online affiliates and partner network.</div>
	           	<div class="adv02pink">Your client decides how much the call is worth to him after hearing the request.</div>
				<div class="adv03pink">We put your client through to the customer. Your client pays only for this call.</div>
			</div>
				
</asp:Content>

