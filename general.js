﻿// JScript File
String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
String.prototype.RemoveSpace = function() { return this.replace(/\s/g, ''); };

Date.prototype.addHours = function (n) {this.setHours (this.getHours () + n)}
Date.prototype.addDays = function (n) {this.setDate (this.getDate () + n)}
Date.prototype.addMonth = function (n) {this.setMonth (this.getMonth () + n)}
Date.prototype.addFullYear = function (n) {this.setFullYear (this.getFullYear () + n)}




function redirect(src)
{   
    //alert(window.parent.location.pathname);
    //alert(window.parent.location.pathname);
    
    if(window.parent.location.pathname==window.location.pathname)
    {
        alert("this is top");
        location.href=src;
    }
    else
    {
        //alert("this is not top");        
        top.location=src;            
    }
}

function openWin(src)
{    
    
    window.open(src,'win');
}



////////////////
///session end
    var _interval;
    var mesLogOut;
    var LogOutPath;
    function LoadLogOutPath(_logout) {
        LogOutPath = _logout;
    }
    function LoadDetails(_mesLogOut, _LogOutPath)
    {
        mesLogOut=_mesLogOut;
        LogOutPath=_LogOutPath;
    }
    function sessionEnd(milSeconds)
   {
        clearInterval(_interval);
        _interval = window.setInterval("MakeLogOut();", milSeconds);
   }
   function MakeLogOut()
   {
       clearInterval(_interval);
        if(mesLogOut)
            alert(mesLogOut);
        window.location=LogOutPath;
   }
   function AddEndSession(milSeconds)
  {
    try{
        top.sessionEnd(milSeconds);
        }
        catch(ex){}
        
  }
  ///session end
  
  
  function validateNumber(number)
  {
    if(number=="" || isNaN(number))
    {
        //alert("Wrong date format2. The right one is MM/DD/YYYY");
        return false;        
    }
    
    return true;
    
  }  
  
  function maxNumber(number,maxNumber)
  {
    if(number*1>maxNumber*1)
    {
        //alert("Wrong date format4. The right one is MM/DD/YYYY");
        return false;
    }
    return true;
  }
  function ValidateDateNum(num, express)
  {
        while(num.length > 0 && num.substring(0,1)== "0")
            num=num.substring(1,num.length)
       
        if(num=="" || isNaN(num))
            return false;
         
        var number=parseInt(num);
       
        if(express.toLowerCase()=="MM".toLowerCase())
        {
            if(number<13 && number>0)
                return true;
            else
                return false;
        }
        if(express.toLowerCase()=="DD".toLowerCase())
        {
            if(number<32 && number>0)
                return true;
            else
                return false;
        }
        if(express.toLowerCase()=="YYYY".toLowerCase())
        {
            if(number<2020 && number>2001)
                return true;            
        }
        return false;
            
  }
  function GetDateString(_date, format)
  {    
    var formats = format.split('/');
    var result = "";
    var temp = "";
    for(var i=0; i<formats.length; i++)
    {
        if(formats[i].toLowerCase() == "mm")
        {
            temp = (_date.getMonth()+1)+"";
            if(temp.length==1)
                temp="0"+temp;
            result += temp + '/';                      
        }
        else if (formats[i].toLowerCase() == "dd")
        {
            temp = _date.getDate()+"";
            if(temp.length==1)
                temp="0"+temp;
            result += temp + '/';
            if(result.length==2)
                result="0"+result;
           
        }
        else if(formats[i].toLowerCase() == "yyyy")
            result += _date.getFullYear();
    }
    return result;
  }
  function GetDateFormat(str, format)
  {
//    if(format.substring(0,2).toLowerCase()=="mm")
 //       return Date.parse(str);
 //   alert(str);
    var formats = format.split('/');
    var strs = str.split('/');
    var newStr = "";
    
    newStr += strs[GetDateHelp("mm", formats)] + '/';
    newStr += strs[GetDateHelp("dd", formats)] + '/';
    newStr += strs[GetDateHelp("yyyy", formats)];
    return Date.parse(newStr);
    
  }
  function GetDateHelp(format, formats)
  {
    for(var i=0; i<formats.length; i++)
    {
        if(formats[i].toLowerCase()==format)
            return i;
    }
    return -1;
  }
  function IsInteger(strInt)
  {
        for(var i= 0; i< strInt.length; i++)
        {
            var c = strInt.charAt(i);
            if(!(c >= 0 && c <= 9))
                return false;
        }
        return true;
    }
    function IsValidDateByFormat(strDate, _format) {
        var strs = strDate.split('/');
        if (strs.length != 3)
            return false;
        var formats = _format.split('/');
        if (formats.length != 3)
            return false;
        var days = strs[GetDateHelp("dd", formats)];
        if (days.length != 2 || !IsInteger(days))
            return false;
        days = new Number(days);//  parseInt(days);
        if (days < 1 || days > 31)
            return false;
        var month = strs[GetDateHelp("mm", formats)];
        if (month.length != 2 || !IsInteger(month))
            return false;
        //       month = parseInt(month);
        month = new Number(month);
        if (month < 1 || month > 12)
            return false;
        var years = strs[GetDateHelp("yyyy", formats)];
        if (years.length != 4 || !IsInteger(years))
            return false;
        years = new Number(years); //parseInt(years);
        if (years < 2010 || years > 2020)
            return false;
        return true;
    }
    
  function IsValidDate(strDate, _format)
  {
        
        var strs = strDate.split('/');
        if(strs.length != 3)
            return false;
        var formats = _format.split('/');
        var days = strs[GetDateHelp("dd", formats)];
        if(days.length != 2 || !IsInteger(days))
            return false;
        var month = strs[GetDateHelp("mm", formats)];
        if(month.length != 2 || !IsInteger(month))
            return false;
        var years = strs[GetDateHelp("yyyy", formats)];
        if(years.length != 4 || !IsInteger(years))
            return false;
        return true;
        
  }
  function validate_Date(strDate, RegularExpression)
  {
      
        if(strDate.length==0 || strDate.toLowerCase() == RegularExpression.toLowerCase())
            return true;
           
        var splitNumber=strDate.split("/");
        var splitExpression=RegularExpression.split("/");
        for (var i=0;i<splitNumber.length;i++)
        {
            if(!ValidateDateNum(splitNumber[i], splitExpression[i]))            
                return false;            
        }
        return true;
  }
  function validaeDate(strDate,msg)
  {   
    //alert(msg);
    if(strDate.length==0)
        return true;
    splitSlash=strDate.split("/");
    
    if(splitSlash.length!=3)
    {
        alert(msg);
        return false;
    }
    
    
    
    for(i=0;i<splitSlash.length;i++)
    {
        
        if(!validateNumber(splitSlash[i]))
        {
            
            alert(msg);
            return false;
            break;
            
        }
            
        
    }
    
    if(splitSlash[0].length>2 || splitSlash[1].length>2 || splitSlash[2].length!=4)
    {
        alert(msg);  
        return false;
      
    }
    
    if(!maxNumber(splitSlash[0],12) || !maxNumber(splitSlash[1],31) ) 
    {
        alert(msg);
        return false;
    }
           
    return true;
  }
  
  //find absulte point of element
  function findTop(sent)
{
 

    var iReturnValue = 0;
    while( sent != null ) {
    iReturnValue += sent.offsetTop;
    sent = sent.offsetParent;
    }
    return iReturnValue+20;
}
function findLeft(sent)
{    
    var iReturnValue = 0;
    while( sent != null ) {
    iReturnValue += sent.offsetLeft;
    sent = sent.offsetParent;
    }
   
    return iReturnValue;
}

function showDivPleaseWait()
{    
    try
    {
        document.getElementById("div_PleaseWait").style.display='block';    
    }
    catch(ex){}
}

function hideDivPleaseWait()
{    
    try
    {
        document.getElementById("div_PleaseWait").style.display='none';   
    }
    catch(ex){}
}
function showDiv() {
    //alert("showDiv4");
    if (self.ShowDivPPC)
        ShowDivPPC();
    else {
        document.getElementById("divLoader").style.display = 'block';
    }  
}

function hideDiv() {
    //alert("hideDiv");
    if (window.localTime)
        window.localTime.start();
    if (self.HideDivPPC)
        HideDivPPC();
    else
        document.getElementById("divLoader").style.display='none';   
}

function showDiv2()
{    
    document.getElementById("divLoader2").style.display='block';    
}

function hideDiv2()
{     
    document.getElementById("divLoader2").style.display='none';   
}

function showDivCredit(source)
{   
    top.document.getElementById("divCredit").style.display='block';    
    
    //parent.document.getElementById("divCredit").style.display='block';    
    top.document.getElementById("iframeCredit").src=source;    
}

function hideDivCredit()
{
    top.document.getElementById("divCredit").style.display='none';
}

function trim(stringToTrim)
{
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
/*
function ifEnterClick() // has problems if i call it from firefox so iused it inline html code
{   

  if ((event.which && event.which == 13) ||
    (event.keyCode && event.keyCode == 13))
    {
        alert("22222");
        return false;
    }
}
*/

function getElementsByClass( searchClass, domNode, tagName)
{ 
    if (domNode == null) domNode = document;
    if (tagName == null) tagName = '*';
    var el = new Array();
    var tags = domNode.getElementsByTagName(tagName);
    var tcl = " "+searchClass+" ";
    var j=0;
    for(var i=0; i<tags.length; i++) {        
	    var test = " " + tags[i].className + " ";
	    if (test.indexOf(tcl) != -1) 
		    el[j++] = tags[i];
    } 
    return el;
} 
function getElementByIdByElement(_id, obj)
{
//childNodes
    if(!obj.hasChildNodes())
        return null;
    var children = obj.childNodes;
    for(var i=0; i<children.length; i++)
    {
        if(children[i].id == _id)
            return children[i];
        var result = getElementByIdByElement(_id, children[i]);
        if(result != null) return result;
    }
    return null;
}

function getElementsWithMultiClassesByClass( searchClass, domNode, tagName)
{ 
    if (domNode == null) domNode = document;
    if (tagName == null) tagName = '*';
    var el = new Array();
    var tags = domNode.getElementsByTagName(tagName);
    
    var j=0;
    for(var i=0; i<tags.length; i++)
    {
   //     alert(tags[i].tagName);
        var classes = tags[i].className.split(' ');
        for(var i2 = 0; i2< classes.length; i2++)
        {
            
            if(classes[i2] == searchClass)
            {
                el[j++] = tags[i];                
                break;
            }
        }
    }
    return el;
    /*
    var tcl = " "+searchClass+" ";
    var j=0;
    for(var i=0; i<tags.length; i++) { 
	    var test = " " + tags[i].className + " ";
	    if (test.indexOf(tcl) != -1) 
		    el[j++] = tags[i];
    } 
    return el;
    */
} 
function getParentByTagName(obj, tag)
{
	var obj_parent = obj.parentNode;
	if (!obj_parent) return null;
	if (obj_parent.tagName.toLowerCase() == tag) return obj_parent;
	else return getParentByTagName(obj_parent, tag);
}
function getParentByClassName(obj, class_name)
{    
	var obj_parent = obj.parentNode;
	if (!obj_parent) return null;
	    
    if(obj_parent.className == null)
        return getParentByClassName(obj_parent, class_name);
        
    var classes = obj_parent.className.split(' ');
    for(var i=0; i< classes.length; i++)
    {
        if(classes[i] == class_name)
            return obj_parent;
    }
	return getParentByClassName(obj_parent, class_name);
}
function getElementsByName( searchName, domNode, tagName)
{ 
    if (domNode == null) domNode = document;
    if (tagName == null) tagName = '*';
    var el = new Array();
    var tags = domNode.getElementsByTagName(tagName);
    var tcl = " "+searchName+" ";
    var j=0;
    for(var i=0; i<tags.length; i++) { 
	    var test = " " + tags[i].name + " ";
	    if (test.indexOf(tcl) != -1) 
		    el[j++] = tags[i];
    } 
    
    return el;
} 
function RemoveAllChildNodes(obj)
{
    while(obj.hasChildNodes()){
	        obj.removeChild(obj.lastChild);
        }
}

function GetValueFromStringValues(val, str)
{
    var indx = str.indexOf(val);
    if(indx == -1)
        return "";
    var nextIndex = str.indexOf("=", indx);
    if(nextIndex == -1)
        return "";
    indx = str.indexOf("&", nextIndex);
    if(indx == -1)
        indx = str.length;
    return str.substring(nextIndex+1, indx);
}

//disable div
function toggleDisabled(el, ToDisable) {
    try {
//        el.disabled = (ToDisable);

        if (ToDisable)
            el.disabled = 'disabled';
        else
            el.disabled = '';
           
    }
    catch(E){}
    
    if (el.childNodes && el.childNodes.length > 0) {
        for (var x = 0; x < el.childNodes.length; x++) {
            toggleDisabled(el.childNodes[x], ToDisable);
        }
    }
}
//Calculating the Number of Days Between Any Two Dates
function days_between(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24;
 
    // Convert both dates to milliseconds
  //  var date1_ms = date1.getTime()
 //   var date2_ms = date2.getTime()

    // Calculate the difference in milliseconds
//    var difference_ms = Math.abs(date1_ms - date2_ms)
    var difference_ms = Math.abs(date1 - date2);
    
    // Convert back to days and return
    return Math.round(difference_ms/ONE_DAY);

}

function CheckMaxLengthTextbox(elem, _max, message, elemCountID)
{
    var elemCount = document.getElementById(elemCountID);
    
   if(elem.value.length > _max) 
   {
        alert(message);
//        while(elem.value.length > _max)
            elem.value = elem.value.substring(0, elem.value.length - (elem.value.length - _max));    
   }
   elemCount.innerHTML = elem.value.length;
}
function Is_Integer(num)
{
    if(num.length == 0)
        return false;
    for (var i = 0; i < num.length; i++) {
        var c = num.charAt(i);
        if (i == 0) {
            if (c == '-')
                continue;
        }        
        if(isNaN(c))
            return false;        
    }
    return true;
}
function RemoveCssClass(_element, class_name) {
   // if(_element.className)
    var _classes = _element.className.split(' ');
    var NewClass = new String();
    for (var i = 0; i < _classes.length; i++) {
        if (_classes[i].length == 0 || _classes[i] == class_name)
            continue;
        NewClass += _classes[i] + " ";
    }
    if (NewClass.length == 0)
        _element.className = "";
    _element.className = NewClass.substring(0, NewClass.length - 1);
}
function AddCssClass(_element, class_name) {
    if (_element.className.indexOf(class_name) > -1)
        return;
    _element.className += " " + class_name;
}



function closeDiv(objId) {
    document.getElementById(objId).style.display = 'none';
}

function toggle(objId) {
    if (document.getElementById(objId).style.display == 'none')
        document.getElementById(objId).style.display = 'block';
    else
        document.getElementById(objId).style.display = 'none';
}

function changeBackground(objId, css) {
    document.getElementById(objId).className = css;
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function goBack() {
    window.history.back()
}

function goToUrl(url) {
    location.href = url;
}
function ExcellExecuting() {
    var btn = document.querySelector('.excel_btn');
    var span_excel = document.querySelector('.span_excel');
    if (btn && span_excel) {
        btn.style.display = "none";
        span_excel.style.display = "inline-block";
    }
}
function ExcellExecuted() {
    var btn = document.querySelector('.excel_btn');
    var span_excel = document.querySelector('.span_excel');
    if (btn && span_excel) {
        btn.style.display = "inline-block";
        span_excel.style.display = "none";
    }
}
function create_excel(pathCreateExcel, sessionTableName, reportName, downloadExcel) {
    ExcellExecuting();
    var path = pathCreateExcel;
    var _data = '{"SessionDatatableName":"' + sessionTableName + '","ReportName":"' + reportName + '"}';
    $.ajax({
        url: path,
        data: _data,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            if (data.d.length == 0) {
                return;
            }
            var _iframe = document.createElement('iframe');
            _iframe.style.display = 'none';
            _iframe.src = downloadExcel + '?fn=' + data.d;
            document.getElementsByTagName('body')[0].appendChild(_iframe);

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        },
        complete: function (jqXHR, textStatus) {
            //  HasInHeadingRequest = false;
            ExcellExecuted();
        }

    });
}