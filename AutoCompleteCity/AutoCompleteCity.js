﻿// JScript File
var sentTxtBox;
var lblMessage;
var _flag;
//var hidden_id;

//complete word for user
function SendRequest(txt_box){ 
 //   return;    
    sentTxtBox = txt_box;
    
 //    var _val=hidden_id;
    
     var sent = txt_box.value;
//    alert(sent);
     AutoComplete.GetOneWord(sent, OnComplete, OnError, OnTimeOut);
}
function OnComplete(arg)
{       
    sentTxtBox.value=arg;
}

function SendRequestMessage(txt_box, _lbl)
{    
    lblMessage = _lbl;
    sentTxtBox = txt_box;
   
 //    var _val=hidden_id;
    
     var sent = txt_box.value;
//    alert(sent);
     AutoComplete.GetOneWord(sent, OnCompleteMes, OnError, OnTimeOut);
}
function OnCompleteMes(arg)
{

    var city=sentTxtBox.value.toLowerCase();
 //   alert(city+"==="+arg.substring(0, city.length).toLowerCase());
    if(city.length>0 && arg.length==0)
    {
        lblMessage.style.display="inline";
        sentTxtBox.value=arg;
        return;
    }
    if(city.substring(0, arg.length) != arg.toLowerCase())
        lblMessage.style.display="inline";
    else
        lblMessage.style.display="none";
    sentTxtBox.value=arg;
}
//complete word for user

function CheckIfmatch(txt_box, _lbl, RequiredFieldValidator)
{
    
    lblMessage = _lbl;
    sentTxtBox = txt_box;
   
    var sent = txt_box.value;
    if(sent.length==0)
    {
        if(RequiredFieldValidator)
            sentTxtBox.className="txt_focus";
        return;
    }
    _flag=true;
     AutoComplete.chkMatcReturnCorrect(sent, OnComleteIfMatch, OnError, OnTimeOut);
}
function OnComleteIfMatch(arg)
{
    var city=sentTxtBox.value.toLowerCase();
    if(arg.length==0)
    {
        lblMessage.style.display="inline";
        sentTxtBox.className="txt_focus";
    }
    else
    {
        lblMessage.style.display="none";
        sentTxtBox.className="form-text";
    }
    sentTxtBox.value=arg;
    _flag=false;
}



function OnTimeOut(arg)
{
     _flag=false;
//	alert("timeOut has occured");
}

function OnError(arg)
{
     _flag=false;
	alert("error has occured: " + arg._message);
}
function GetFlag()
{
    return _flag;
}
function SetFlag(flg)
{
    _flag=flg;
}

function CheckIfmatchNoneCss(txt_box, _lbl, RequiredFieldValidator, region_level)
{
    
    lblMessage = _lbl;
    sentTxtBox = txt_box;
   
    var sent = txt_box.value;
    if(sent.length==0)
    {
 //       if(RequiredFieldValidator)
  //          sentTxtBox.className="txt_focus";
        return;
    }
    _flag=true;
     AutoComplete.chkMatcReturnCorrect(sent, region_level, OnComleteIfMatchNoneCss, OnError, OnTimeOut);
}
function OnComleteIfMatchNoneCss(arg)
{
    var city=sentTxtBox.value.toLowerCase();
    if(arg.length==0)
    {
        lblMessage.style.display="inline";
//        sentTxtBox.className="txt_focus";
    }
    else
    {
        lblMessage.style.display="none";
 //       sentTxtBox.className="form-text";
    }
    sentTxtBox.value=arg;
    _flag=false;
}