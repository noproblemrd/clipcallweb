﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="noPpc.aspx.cs" Inherits="general_noPpc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title runat="server" id="myTitle">&nbsp;</title>
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" media="screen" runat="server" />

    <script type="text/javascript">
    var counter_cookies=0;
    
    function checkEnter(e)
    {       
        // prevent to click enter in the phone input 
        
        
         
        var code;
        if (!e) var e = window.event;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
    	/*
        var targ="";
        if (e.target) targ = e.target;
        else if (e.srcElement) targ = e.srcElement;
        */
        //alert(code);
        if(code*1==13)
        {   
            ifOk=callNonPpcService();  
                 
            if(ifOk)
            {                           
                return true;                
            }
            else
                return false;
             
        }     
        
               	
     
    } 
    
    function get_cookie(cookie_name)
    {
      var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

      if (results)
        return ( unescape ( results[2] ) );
      else
        return null;
    }

    function set_cookie(name,value,exp_y,exp_m,exp_d,exp_hh,exp_mm,exp_ss,path,domain,secure)
    {           
      //alert("name:" + name + " value:" + value + " exp_y:" + exp_y );
      var cookie_string = name + "=" + escape(value);

      if (exp_y)
      {
        var expires = new Date (exp_y,exp_m,exp_d,exp_hh,exp_mm,exp_ss);
        //var expires = new Date (exp_y,exp_m,exp_d);
        /*
        alert(expires.getFullYear());
        alert(expires.getMonth());
        alert(expires.getDate());
        alert(expires.getHours());
        alert(expires.getMinutes());
        alert(expires.getSeconds());
        alert(expires.toGMTString());
        alert(expires.toTimeString());
        alert("toString:" + expires.toString());
           
        alert(expires.toTimeString());
        */
        cookie_string += ";expires=" + expires.toGMTString();
        //cookie_string += "; expires=" + expires.toTimeString();
        //alert(cookie_string);
      }

      if (path)
            cookie_string += "; path=" + escape (path);

      if (domain)
            cookie_string += "; domain=" + escape (domain);
      
      if (secure)
            cookie_string += "; secure";
      
      //alert("cookie_string: "+cookie_string);
      document.cookie = cookie_string;
      //document.cookie = 'mycookie=hello;expires=Fri, 17 Dec 2010 10:00:00 GMT';
    }
    

    
    function callNonPpcService()
    {              
        /***************  validation phone nimber *******************/
        var phoneNumber=document.getElementById("<%#phone.ClientID%>").value.replace(/\s/g,""); // remove spaces

        var ifValidPhone=true;         
        
        //alert("phoneNumber" + phoneNumber);

        var regFormatPhone='<%#regFormatPhone%>';            
        //alert(regFormatPhone);            
        if (phoneNumber.search(regFormatPhone)==-1) //if match failed
        {                    
           ifValidPhone=false;
        }	        
       
        if (!ifValidPhone)
        {
            alert(document.getElementById("<%#HiddenNotCorrectPhoneNum.ClientID%>").value);           
            return false;
        }
	    /***************  end validation phone nimber *******************/ 
	      
        /*************** cookies *****************/
        var current_date = new Date();
        var cookie_year = current_date.getFullYear();
        //alert("cookie_year:"+cookie_year);
        var cookie_month = current_date.getMonth();
        //cookie_month=cookie_month*1+1;
        //alert("cookie_month:" + cookie_month);
        var cookie_day = current_date.getDate();
        //cookie_day=cookie_day*1+1;
        //alert("cookie_day:"+cookie_day);
        var cookie_hh = current_date.getHours();
        //alert("cookie_hh:"+cookie_hh);           
        var cookie_mm = current_date.getMinutes();
        cookie_mm=cookie_mm+5;
        var cookie_ss = current_date.getSeconds();
        //var PhoneNumber=$("PhoneNumber").value;
        
        
        if(!get_cookie('<%#AdvertiserId%>' + phoneNumber))
        {
            counter_cookies++;
            //alert("empty " + counter_cookies);
            
            set_cookie('<%#AdvertiserId%>' + phoneNumber,counter_cookies,cookie_year,cookie_month,cookie_day,cookie_hh,cookie_mm,cookie_ss);
            return true;
        }
        
        else
        {            
            alert(document.getElementById("<%#HiddenCookie.ClientID%>").value);
            return false;  
        } 
        
        /*************** end cookies *****************/     
	               
        
    }
   
    
    </script>
</head>
<body><div  class="click2call" align="center">
    <form id="formPopUp" runat="server">
        <div runat="server" id="divNonPpcExplanation" class="call">
            <p>
                <asp:Label runat="server" ID="lblPleaseInsert">Please insert your phone number and the advertiser will call you</asp:Label>
                <strong><asp:Label ID="lblFree" runat="server">FREE!</asp:Label></strong>
            </p>
            <div class="form-field">
                <input runat="server" id="phone" type="text"  class="form-text"  onkeypress="return checkEnter();"  />
                <asp:Button runat="server" ID="btnCall" CssClass="form-submit" Text="Call" OnClientClick="return callNonPpcService();" />
            </div>
        </div>
        <div runat="server" id="divNonPpcThanks" class="response" visible="false">
            <p><strong><asp:Label ID="lblThank" runat="server">Thank you,</asp:Label></strong><br /><asp:Label runat="server" ID="lblCallSoon">The advertiser will call you soon...</asp:Label></p>
        </div>
        
        
        <asp:HiddenField ID="HiddenNotCorrectPhoneNum" runat="server" Value="Not Correct Format Phone Number" />
        <asp:HiddenField ID="HiddenCookie" runat="server" Value="We have already recieved your request" />
    </form></div>
</body>
</html>
