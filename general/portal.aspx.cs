﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class general_portal : System.Web.UI.Page
{
    protected string AdvertiserPhoneNumber1="";
    protected string AdvertiserPhoneNumber2="";
    protected string SiteId = "";
    protected string SiteLangId = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AdvertiserPhoneNumber1 = ConfigurationManager.AppSettings["NonPpcExampleAdvertiser1Phone"].ToString();
            AdvertiserPhoneNumber2 = ConfigurationManager.AppSettings["NonPpcExampleAdvertiser2Phone"].ToString();
            SiteLangId = ConfigurationManager.AppSettings["NonPpcExampleSiteLangId"].ToString();
            string _host = Request.ServerVariables["SERVER_NAME"];
            
            SiteId=DBConnection.GetSiteNameIdByHost(_host);            
            
            Page.DataBind();

            
        }
    }
}
