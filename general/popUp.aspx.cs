﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class general_popUp : PageSetting
{
   
    protected string regFormatPhone = "";

    protected string root = "";
    protected string ContactPhoneNumber = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            /*
            root = ResolveUrl("~");
            SiteId = Request["SiteId"];
            SiteLangId = Convert.ToInt32(Request["SiteLangId"]);
            //Response.Write("SiteLangId:" + SiteLangId);

            SiteSetting ss = new SiteSetting(SiteId);
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + ss.GetStyle));


            AdvertiserId = Request["AdvertiserId"];
            AdvertiserName = Request["AdvertiserName"];
            AdvertiserPhoneNumber1 = Request["AdvertiserPhoneNumber1"];
            AdvertiserPhoneNumber2 = Request["AdvertiserPhoneNumber2"];

            regFormatPhone = DBConnection.GetPhoneRegularExpression(SiteId);
            regFormatPhone = regFormatPhone.Replace("\\", "\\\\");


            string pagename = System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
            DBConnection.LoadTranslateToControl(this, pagename, SiteLangId);

            Page.Header.DataBind();
            */

        }

        else
        {
            /*
            divNonPpcExplanation.Visible=false;
            divNonPpcThanks.Visible = true;
            
            ContactPhoneNumber = Request["phone"].Trim();
            
            WebServiceCall wsCall = new WebServiceCall();
            */
            //string result=wsCall.callNonPpc(SiteId,AdvertiserId,AdvertiserName,AdvertiserPhoneNumber1,AdvertiserPhoneNumber2,ContactPhoneNumber);
            
        }
        
    }
   

    public string SiteId
    {
        get { return ViewState["siteId"]==null ? "" : ViewState["siteId"].ToString(); }

        set { ViewState["siteId"]=value; }       
    }

    public int SiteLangId
    {
        get { return Convert.ToInt32(ViewState["SiteLangId"]); }

        set { ViewState["SiteLangId"] = value; }
    }

    public string AdvertiserId
    {
        get { return ViewState["AdvertiserId"]==null ? "" : ViewState["AdvertiserId"].ToString(); }

        set { ViewState["AdvertiserId"] = value; }
    }

    public string AdvertiserName
    {
        get { return ViewState["AdvertiserName"]==null? "" : ViewState["AdvertiserName"].ToString(); }

        set { ViewState["AdvertiserName"] = value; }
    }

    public string AdvertiserPhoneNumber1
    {
        get { return ViewState["AdvertiserPhoneNumber1"]==null ? "" : ViewState["AdvertiserPhoneNumber1"].ToString(); }

        set { ViewState["AdvertiserPhoneNumber1"] = value; }
    }

    public string AdvertiserPhoneNumber2
    {
        get { return ViewState["AdvertiserPhoneNumber2"]==null ? "" : ViewState["AdvertiserPhoneNumber2"].ToString(); }

        set { ViewState["AdvertiserPhoneNumber2"] = value; }
    }

}
