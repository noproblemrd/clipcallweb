﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPAbout.master" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="about" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNPAbout.master" %>


<%-- Add content controls here --%>
<asp:Content ID="_Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">


<div id="wrapadv">

	 	<div class="subtext_about"><p>No Problem's advanced pay-per-call technology offers advertisers </p><p>and publishers more.</p></div>

		

		

			<div class="aboustext">

      <p>Our goal is to essentially help advertisers and publishers alike grow businesses and make more money.</p>&nbsp;<p></p>



      <p>No Problem's pay-per-call platform helps advertisers reach ready-to-spend customers directly by phone and provides them with a cost-efficient pricing system--they pay only for actual calls and decide how much a call is worth when they hear the customer request. We also give them new tools for managing their business more efficiently. Our Advertiser Dashboard lets them completely control their total budgets, decide where they do business and when, and track the amount of calls they receive, the details of each call and how many calls they've lost.</p>&nbsp;<p></p> 



      <p>No Problem helps publishers increase their site revenue and traffic simply by installing our widget. There are virtually no IT costs or set-up requirements, and publishers receive a higher CPM than any other affiliate program.</p>

			</div>

	

 			<div class="facts">

 			<h3 class="subclass">We think the numbers speak for themselves!</h3>  <br />

				<h4><span class="sloganabout">Full 30% to 40%</span></h4>

				<p><span class="slogansmallabout">conversion rate from calls to real jobs</span></p>

			<br />

      <p><span class="sloganaboutleft">An average CPM of</span></p>

			<h4><span class="smalltextaboutright">$3000 to $5000 USD</span></h4>

				

			</div>



      <h3 class="subclass">How do we know what you need? We started out as a directory publisher.</h3><br />



      <p>Before we began helping online directory publishers across the globe increase their profitability with our pay-per-call platform, we used it ourselves for 5 years on our own online directory. It worked so well that our directory was purchased by one of the world's most forward-thinking and profitable directory publishers in 2010!</p>&nbsp;<p></p> 



      <p>No Problem was founded in 2006 in Ra'anana, Israel and currently has operations in China, Finland, Poland, Germany, Hungary, the Baltic States, South America, Israel, and several other markets. We are funded by Jerusalem Capital and a group of super angels well-steeped in the service directory business.</p>&nbsp;<p></p>



	</div> <!-- End wrapadv -->

</asp:Content>
