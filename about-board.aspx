﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPAbout.master" AutoEventWireup="true" CodeFile="about-board.aspx.cs" Inherits="about_board" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<div id="wrapadv">

  <div class="aboutmanagmentleft" style="margin-bottom:100px;margin-left: 0px;">
<h5>Jacob Ner-David - Chairman</h5>
<p>Jacob has been a successful entrepreneur for more than a decade. He has built and managed four companies to profitable exits, raising over $150 million from leading venture capital funds, institutional investors, and strategic industry players. He has created over a billion dollars of shareholder value for his investors. Two of these companies are considered market leaders in the TES sector.</p>
<p>In 1996, Jacob co-founded and served as Chairman & CEO of Delta Three (NASDAQ: DDDC), leading the company to the market capitalization of over $1.8 billion. In 1999, Jacob founded and served as Chairman & CEO of NomadIQ, until its acquisition by Omnisky (NASDAQ: OMNY).</p>
<p>In 2004 Jacob co-founded Double Fusion, today a dominant provider of in-game advertising services, backed by Accel Partners, JVP, and Jerusalem Capital.</p>
<p>&nbsp;</p>
<h5>Kobi Livne - Director</h5>

<p>Mr. Livne held several senior positions at Amdocs. In his last position he served as Group President and Member of the Amdocs Management. In addition he was the CEO of Amdocs Israel. </p>
<p>Currently Mr. Livne is a director of several companies. Mr. Livne holds a BA in Mathematics and Computer Science and an MBA in Business management/MIS - Tel Aviv University. Israel.</p>
 
 <p>&nbsp;</p>

 <h5>Maya Segal - Director</h5>
<p>Bsc In Mathematics & Computer Science at Ben Gurion University, Israel. Maya has worked with Amdocs (NYSE:DOX), and for the past several years has worked in asset management. In addition, Maya is a Indie film producer.</p>



  </div>
  
  <div class="aboutmanagmentright" style="margin-right: 150px;">
  



<h5>Shmuel Cabilly - Director</h5>
<p>
Dr. Shmuel Cabilly holds a PhD in Immunology from the Hebrew university (1980). He spent four years in the US as a post-doctoral fellow heading a joint project of the City of Hope Research Institute and Genentech where he developed a new technology for recombinant antibody production. Today there are already 20 drugs in the market based on this technology among which are Herceptin, Rituxan, Traceba, Raptiva, Xolair and more. Dr Cabilly was also a co-founder and a chief scientist of Ethrog Biotechnology where he invented core technologies. The company is based in Israel, it generate a profit and in 2001 It was sold to Invitrogen which distributes its products throughout the world. Dr. Cabilly is now an investor and a board member of several companies. 
</p>

<!--
<p>Mr. Livne held several senior positions at Amdocs. </p>
<p>In his last position he served as Group President and Member of the Amdocs Management. In addition he was the CEO of Amdocs Israel. </p>
<p>Currently Mr. Livne is a director of several companies. Mr. Livne holds a BA in Mathematics and Computer Science and an MBA in Business management/MIS – Tel Aviv University. Israel. </p>
-->
<p>&nbsp;</p>


<h5>Daniel Shaked - CEO</h5>
<p>Daniel is an experienced high-tech executive who prior to founding No Problem, worked as a VP Operations at Dassault Systems Israel (Dasty, Nasdaq).  Successfully led the R&D processes, Global Customer Service, QA, IT, Professional Services and Planning and Control departments.</p>
<p>Holds a bachelor degree in Mechanical Engineering and MBA. </p>
<p>Prior that served for years as a commanding Logistics officer  in IDF.</p>
  </div>

	</div> <!-- End wrapadv -->

</div>
</asp:Content>

