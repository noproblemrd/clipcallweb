﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TexBoxtError.aspx.cs" Inherits="TexBoxtError" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
    function ErrorText(){
        alert("<%# GetError %>");
        history.go(-1);
    }
    function RedirectBack() {
        history.go(-1);
   //     window.location.href = '<# Redirect %>';
    }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <asp:Label ID="lbl_Error" runat="server" Text="You try to insert illegal characters" Visible="false"></asp:Label>
    </form>
</body>
</html>
