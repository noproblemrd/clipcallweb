﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPPre.master" AutoEventWireup="true" CodeFile="adv-freetrial.aspx.cs" Inherits="adv_freetrial" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNPPre.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="CallService.js"></script>
<script type="text/javascript" src="general.js"></script>
<script type="text/javascript" >
    var place_holder_mail = "e.g. me@awesomesite.com";
    var place_holder_phone = "e.g. 1.212.1234567";
    var place_holder_code = "";
    window.onload = function () {
        document.getElementById("txt_email").value = place_holder_mail;
        document.getElementById("txt_phone").value = place_holder_phone;

    }
    function GetEmailText() {
        var txt_email = document.getElementById("txt_email");
        if (txt_email.value == place_holder_mail && txt_email.className == "place-holder")
            return "";
        return txt_email.value;
    }
    function GetPhoneText() {
        var txt_phone = document.getElementById("txt_phone");
        if (txt_phone.value == place_holder_phone && txt_phone.className == "place-holder")
            return "";
        return txt_phone.value;
    }
    function NewLogIn() {
        var span_phone_error = document.getElementById("span_phone_error");
        var span_phone_valid = document.getElementById("span_phone_valid");

        var span_email_error = document.getElementById("span_email_error");
        var span_email_valid = document.getElementById("span_email_valid");
        RemoveErrorServer();
        var email = GetEmailText();
        var phone = GetPhoneText();
        var Is_Ok = true;
        if (!CheckEmail(email)) {
            span_email_error.style.display = "inline";
            span_phone_valid.style.display = "none";
            Is_Ok = false;
        }
        else {
            span_email_error.style.display = "none";
            span_phone_valid.style.display = "inline";
        }

        if (!CheckPhone(phone)) {
            span_phone_error.style.display = "inline";
            span_phone_valid.style.display = "none";
            Is_Ok = false;
        }
        else {
            span_phone_error.style.display = "none";
            span_phone_valid.style.display = "inline";
        }

        if (!Is_Ok)
            return;
        toggleDisabledDiv1(true);
        var _params = "SiteNameId=" + "<%# Site_Id %>" + "&email=" + email + "&phone=" + phone;
        var _url = "<%# GetPath %>";
        CallWebService(_params, _url,
                function (arg) {
                    if (arg == "0") {
                        document.getElementById("div_1").style.display = "none";
                        document.getElementById("div_2").style.display = "block";
                    }
                    else if (arg == "-1") {
                        var _mes = "<%# GetEmailExist %>";
                        SetErrorServer(_mes);
                        toggleDisabledDiv1(false);
                    }
                    else {
                        var __mes = "<%# GetServerError %>";
                        SetErrorServer(__mes);
                        toggleDisabledDiv1(false);
                    }
                },
                function () {
                    var __mes = "<%# GetServerError %>";
                    SetErrorServer(__mes);
                    toggleDisabledDiv1(false);
                });


    }
    function toggleDisabledDiv1(ToDisable) {
        var div_1 = document.getElementById("div_1");
        var span_sending = document.getElementById("span_sending");
        var btn_send = document.getElementById("btn_send");
        toggleDisabled(div_1, ToDisable);
        if (ToDisable) {
            span_sending.style.display = "inline";
            btn_send.className = "get-sms-onsend";
        }
        else {
            span_sending.style.display = "none";
            btn_send.className = "get-sms";
        }
    }
    function toggleDisabledDiv2(ToDisable) {
        var div_2 = document.getElementById("div_2");
        var span_Login = document.getElementById("span_Login");
        var btn_code = document.getElementById("btn_code");
        toggleDisabled(div_2, ToDisable);
        if (ToDisable) {
            span_Login.style.display = "inline";
            btn_code.className = "smscode-onsend";
        }
        else {
            span_Login.style.display = "none";
            btn_code.className = "smscode";
        }
    }
    function SetErrorServer(_mess) {
        document.getElementById("span_ServerError").innerHTML = _mess;
        document.getElementById("div_ServerError").style.display = "block";
    }
    function RemoveErrorServer() {
        document.getElementById("span_ServerError").innerHTML = "";
        document.getElementById("div_ServerError").style.display = "none";
    }
    function showDiv() {
        document.getElementById("divLoader").style.display = 'block';
    }

    function hideDiv() {
        document.getElementById("divLoader").style.display = 'none';
    }
    function Log_in() {
        RemoveErrorServer();
        var span_code_error = document.getElementById("span_code_error");

        var email = document.getElementById("txt_email").value;
        var password = document.getElementById("txt_pass").value;
        if (password.length == 0) {
            span_code_error.style.display = "inline";
            return;
        }
        else
            span_code_error.style.display = "none";

        var _params = "SiteNameId=" + "<%# Site_Id %>" + "&email=" + email + "&password=" + password;
        var _url = "<%# GetPathLogIn %>";
        toggleDisabledDiv2(true);
        CallWebService(_params, _url,
                function (arg) {
                    if (arg.indexOf(".aspx") == -1) {
                        if (arg == "NotFound")
                            SetErrorServer("<%# PasswordNotFound %>");
                        else
                            SetErrorServer("<%# GetServerError %>");
                        toggleDisabledDiv2(false);
                    }
                    else {
                        document.getElementById("div_2").style.display = "none";
                        document.getElementById("div_3").style.display = "block";
                        //       setTimeout("window.open('" + arg + "', '_blank' );", 2000);
                        setTimeout("window.location='" + arg + "';", 2000);
                    }
                },
                function () {
                    SetErrorServer("<%# GetServerError %>");
                    toggleDisabledDiv2(false);
                });

    }
    function CheckEmail(_email) {
        if (_email.length == 0)
            return false;
        var _reg = new RegExp("<%# GetEmailExpresion %>");
        return _reg.test(_email);
    }
    function CheckPhone(_phone) {
        if (_phone.length == 0)
            return false;
        var _reg = new RegExp("<%# GetPhoneExpresion %>");
        return _reg.test(_phone);
    }
    function EmailFocus(_value) {
        var txt_email = document.getElementById("txt_email");
        if (txt_email.className == "place-holder" && txt_email.value == place_holder_mail) {
            txt_email.className = "";
            txt_email.value = "";
        }
    }
    function PhoneFocus(_value) {
        var txt_phone = document.getElementById("txt_phone");
        if (txt_phone.className == "place-holder" && txt_phone.value == place_holder_phone) {
            txt_phone.className = "";
            txt_phone.value = "";
        }
    }
    function ChekValidEmail(_value) {
        var span_email_error = document.getElementById("span_email_error");
        var span_email_valid = document.getElementById("span_email_valid");
        var span_email_process = document.getElementById("span_email_process");
        var txt_email = document.getElementById("txt_email");
        if (_value.length == 0) {
            span_email_error.style.display = "none";
            span_email_valid.style.display = "none";
            span_email_process.style.display = "none";
            txt_email.value = place_holder_mail;
            txt_email.className = "place-holder";
        }
        else if (!CheckEmail(_value)) {
            span_email_error.style.display = "inline";
            span_email_valid.style.display = "none";
            span_email_process.style.display = "none";
        }
        else {
            span_email_error.style.display = "none";
            span_email_valid.style.display = "inline";
            span_email_process.style.display = "none";
        }
    }
    function ChekValidPhone(_value) {
        var span_phone_error = document.getElementById("span_phone_error");
        var span_phone_valid = document.getElementById("span_phone_valid");
        var span_phone_process = document.getElementById("span_phone_process");
        var txt_phone = document.getElementById("txt_phone");
        if (_value.length == 0) {
            span_phone_error.style.display = "none";
            span_phone_valid.style.display = "none";
            span_phone_process.style.display = "none";
            txt_phone.value = place_holder_phone;
            txt_phone.className = "place-holder";
        }
        else if (!CheckPhone(_value)) {
            span_phone_error.style.display = "inline";
            span_phone_valid.style.display = "none";
            span_phone_process.style.display = "none";
        }
        else {
            span_phone_error.style.display = "none";
            span_phone_valid.style.display = "inline";
            span_phone_process.style.display = "none";
        }
    }
    function ChekValidEmail_KeyPress(_value) {
        var span_email_error = document.getElementById("span_email_error");
        var span_email_valid = document.getElementById("span_email_valid");
        var span_email_process = document.getElementById("span_email_process");
        if (_value.length == 0) {
            span_email_error.style.display = "none";
            span_email_valid.style.display = "none";
            span_email_process.style.display = "none";
        }
        else if (!CheckEmail(_value)) {
            span_email_error.style.display = "none";
            span_email_valid.style.display = "none";
            span_email_process.style.display = "inline";
        }
        else {
            span_email_error.style.display = "none";
            span_email_valid.style.display = "inline";
            span_email_process.style.display = "none";
        }
    }
    function ChekValidPhone_KeyPress(_value) {
        var span_phone_error = document.getElementById("span_phone_error");
        var span_phone_valid = document.getElementById("span_phone_valid");
        var span_phone_process = document.getElementById("span_phone_process");
        if (_value.length == 0) {
            span_phone_error.style.display = "none";
            span_phone_valid.style.display = "none";
            span_phone_process.style.display = "none";
        }
        else if (!CheckPhone(_value)) {
            span_phone_error.style.display = "none";
            span_phone_valid.style.display = "none";
            span_phone_process.style.display = "inline";
        }
        else {
            span_phone_error.style.display = "none";
            span_phone_valid.style.display = "inline";
            span_phone_process.style.display = "none";
        }
    }
    function CheckPassword(_value) {
        var span_code_error = document.getElementById("span_code_error");
        if (_value.length > 0)
            span_code_error.style.display = "none";
    }
        
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="wrapadv">
	     <div class="regnav"><a href="adv.aspx">Advertisers</a>&nbsp; > &nbsp;Registration</div>
	 	<div class="reg_subtext"><p>Register now to start receiving quality Internet leads to your phone. </p><p>&nbsp;</p><p>Don't forget&#151;the first $30 worth of calls is on us!</p><p>&nbsp;</p><p class="subclass">Type in your info below to get registered.</p><p class="subclass">It's quick and easy.</p></div>
		
		
			<div style="margin-left: 6px;">
			<ul id="div_ServerError" class="NoneFieldError" style="display:none;">
			    <li>
			        <span id="span_ServerError"></span>
			        <span id="Li1">!</span>
			    </li>
			</ul>
            
		
			<div class="regoform" id="div_1" >
			<label>Email</label>
 			<div class="inputform">
 			<table id="table_email"  class="register-table">
            <tr>
            <td>
                <input type="text" id="txt_email"  
                    onkeyup="ChekValidEmail_KeyPress(this.value);" class="place-holder"
                         onblur="ChekValidEmail(this.value);" onfocus="EmailFocus(this.value);"/>
            </td>
            <td class="MessageValid">
                <div id="span_email_error" class="span_email_error" style="display:none;">
                <div class="validationIcon">
                    <img alt="Smiley Error" src="Formal/images/redSmileyError.png" />
                </div>
                
                <!--
                <span style="padding-bottom:20px;">
                    We need you to enter valid address
                </span>
                -->
                 <div class="validationTextErrorSubmit">
                   We need you to enter valid address...
                </div>
                </div>
                <span  class="error-msg" id="span_email_valid" style="display:none;">
                    <img alt="Smiley Valid" src="Formal/images/greenSmileyError.png" />                        
                </span>
                <div id="span_email_process" class="span_email_process" style="display:none;">
                    <div class="validationIcon">
                        <img alt="Smiley Valid" src="Formal/images/OrangeSmileyInstructor.png" />                        
                    </div>
                    <div  class="validationTextErrorKeyUp" >
                        e.g. me@awesomesite.com
                    </div>
                </div>
                
            </td>
            </tr>
            </table>          
           
            </div>
			<br />
			<label>Mobile Phone Number</label>
 			<div class="inputform">
 			<table id="table_phone" class="register-table">
            <tr>
            <td>
                <input type="text" id="txt_phone"  class="place-holder"
                    onkeyup="ChekValidPhone_KeyPress(this.value);" 
                             onblur="ChekValidPhone(this.value);" onfocus="PhoneFocus(this.value);"/>
            </td>
            <td class="MessageValid">

                <div id="span_phone_error" class="span_email_error" style="display:none;">
                    <div class="validationIcon">
                        <img alt="Smiley Error" src="Formal/images/redSmileyError.png" />
                    </div>

                    <div class="validationTextErrorSubmit">
                       We didn't get your phone number. Please note that we currenlty accept advertisers with a US mobile phone only.
                    </div>
                </div>

                <div id="span_phone_process" class="span_email_process" style="display:none;">
                    <div class="validationIcon">
                        <img alt="Smiley Valid" src="Formal/images/OrangeSmileyInstructor.png" />                        
                    </div>
                    <div class="validationTextErrorKeyUp" >
                        e.g. 1(646)1234567 
                    </div>
                </div>
                <span id="span_phone_valid" style="display:none;" class="error-msg" >
                    <img alt="Smiley Valid" src="Formal/images/greenSmileyError.png" />
                    
                </span>
                
            </td>
            </tr>
            </table>
            
            </div>
           <div style="margin-left: 294px;">
                <input type="button" id="btn_send" class="get-sms" onclick="NewLogIn();" />
                <br/>
                <span id="span_sending" style="display:none;">Sending sms...</span>

                
  		    </div>
			</div>
	


		
	<div class="regoform" id="div_2"  style="display:none;">
			
		<label>Enter SMS code here</label>
 		<div class="inputform">
 		<table  class="register-table">
            <tr>
            <td>
            <input type="password" id="txt_pass" onkeyup="CheckPassword(this.value);" 
                    onblur="CheckPassword(this.value);" />
            
            </td>
            <td>
                <div id="span_code_error" class="span_email_error" style="display:none;">
                    <img alt="Smiley Error" src="Formal/images/redSmileyError.png" />                
                    Please enter the code
                </div>
            </td>
            </tr>
            </table>
            
	    </div>
	    <div class="div_smscode">
                <input type="button" id="btn_code" class="smscode" onclick="Log_in();" />
                <br />
                <span id="span_Login" style="display:none;">Login...</span>
		    </div>
    </div> 
    <div class="regtext" id="div_3" style="display:none;" runat="server">
				Get ready&#151;you're about to start getting voice leads from real customers! <br />
                Hold on while we direct you to our advertising portal...
    </div>
    </div>
</div>
</asp:Content>

