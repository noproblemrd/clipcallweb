﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPPre.master" AutoEventWireup="true" CodeFile="adv.aspx.cs" Inherits="adv" %>


<%@ MasterType VirtualPath="~/Controls/MasterPageNPPre.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="advmain">	
	<div id="wrapadv">
	 	<div class="subtext">
		    <p>Have you ever wondered how much</p>
            <p>value you're getting for the amount</p>
            <p>you spend on online advertising?</p>

            <p>&nbsp;</p>

            <p class="subtextDark">Are you paying for clicks without</p>
            <p class="subtextDark">knowing how many actual calls</p>
            <p class="subtextDark">they are generating for you?</p>
        </div>
		
		
			<div class="singup">
			<p><a href="adv-freetrial.aspx">Sign up</a> for pay-per-call and start getting quality Internet leads directly to your business phone. Pay-per-call campaigns convert 10X the rate of search campaigns. With No Problem's pay-per-call platform, you pay only for actual calls that connect you with motivated customers AND you decide the price per call before you accept it.</p>
			</div>
			<div class="advfree"><h5><a href="adv-freetrial.aspx"></a></h5></div>
			<div class="clear"></div> 
			
			<div class="facts">
			<h3 class="subclass">Some facts on No Problem'' pay-per-call platform:</h3>
			
			<h4><span class="sloganadv">Full 30% to 40%</span></h4>
				<p><span class="sloganadvsmall">conversion rate from calls to real jobs</span><br /><br /></p>
			
			<h4><span class="sloganadv">Exposure to tens of thousands</span></h4>
				<p><span class="sloganadvsmall">customer requests per day</span><br /><br /></p>
			
			<h4><span class="sloganadv">OVER 95%</span></h4>
				<p><span class="sloganadvsmall">of our customers are loyal and increasing their exposure</span><br /><br /></p>
			</div>
			
			
			<div class="boxadv"> 
               <h3>Local Service Providers</h3>
            	<div class="boxcontent">We bring you new customers so you can concentrate on your work. Pay only for real leads.</div>
				<div class="boxfreetrial"><a href="adv-freetrial.aspx"></a></div>
			</div>
			
			<div class="boxadv"> 
               <h3>Local Businesses</h3>
            	<div class="boxcontent">Gain more value for your marketing dollars; pay only for real time phone leads from customers that need your service.</div>
				<div class="boxfreetrial"><a href="adv-freetrial.aspx"></a></div>
			</div>
			<div class="boxadv"> 
               <h3>Nationwide Advertisers</h3>
            	<div class="boxcontent">Improve lead quality and draw impressive conversion rates by adding pay-per-call into your media mix.</div>
				<div class="boxfreetrial"><a href="adv-freetrial.aspx"></a></div>
			</div>
	
			<div class="clear"></div> 
			<a name="anchorVideo"></a>
			  <div id="video" style="display:none;">
			    <img src="images/xclose.png" width="31" height="31" class="right" onclick="javascript:showHide();" style="padding-top:24px;padding-right:10px;padding-bottom:5px" alt="close"/>
                <iframe id="iframeVideo" width="950" height="531" src="" frameborder="0" allowfullscreen></iframe>
              </div>
			
			<div class="boxleft"> 
		    	<h3 class="subclass">Why you should be using pay-per-call:</h3>
		        <div class="toggle-content">
								<h5>Connect by phone immediately to motivated customers</h5>
				Consumers who reach out by phone are typically more ready to spend. Our pay-per-call system brings them right to you.
				
				<h5>Pay only for actual calls</h5>
				<p>If you don't receive calls, you don't pay a penny.</p>
				
				<h5>Decide the price you'll pay only after evaluating the lead</h5>
				<p>Only you know how much a lead is worth to you. Listen to the customer request and decide whether you accept the call and how much you are willing to pay.</p>
				
				<h5>Track calls, manage your budget, calculate ROI and more</h5>
				<p>Our Advertiser Dashboard lets you completely control your total budget, decide where you do business and when, and track the amount of calls you receive, the details of each call and how many calls you've lost.</p>
				
				<h5>No set up fees, contracts, or monthly fees</h5>
				<p>We believe you should pay only for results. No calls? You don't pay.</p>
				</div>
			</div>
				
			<div class="boxright"> 
	            <h3 class="subclass">Here's how it works:</h3>
				
            <div id="phone" style="display:block;">      
                <img src="Formal/images/video.png" width="279" height="158"  onclick="javascript:showHide();" alt="video" />   
            </div>
     
				
				<div class="adv01">We find ready-to-spend customers through our online affiliates and partner network.</div>
	           	<div class="adv02">You decide how much the call is worth to you after hearing the request.</div>
				<div class="adv03">We put you through to the customer. You pay only for this call.</div>
			</div>
			
			<div class="clear"></div>
			<div class="qa">
			<h3 class="subclass">Here are our answers to your questions:</h3>
			
			<div class="aq"><span class="number">1.</span> <span class="question">How much will it cost me? </span><br />
			<p class="answer">You decide. Go to No Problem's advertiser portal to set your price per call. Then start getting phone calls!</p></div>

			<div class="aq"><span class="number">2.</span> <span class="question">How can I keep track of my leads?</span><br />
			<p class="answer">We maintain a list of every call you receive on our advertiser portal. Just log in and take a look. <br /> Our powerful reports allow you 
    to track how many calls you've received, the details of each call and how <br />many calls you've lost. You can even listen back to 
    calls and retrieve important information you may have<br /> missed, like the customer's address or the amount of your quote.  </p></div>
			
			<div class="aq"><span class="number">3.</span> <span class="question">I'm going to be out on jobs all day. Do I need to be in front of a computer to respond to leads?</span><br />
			<p class="answer">Certainly not. You receive a phone call from our automated service wherever you are and we connect<br /> you through to a    
    motivated customer immediately. It's that easy.</p></div>

			
			</div>

	</div> <!-- End wrapadv -->
</div>


</asp:Content>

