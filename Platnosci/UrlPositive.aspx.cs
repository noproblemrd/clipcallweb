﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Platnosci_UrlPositive : System.Web.UI.Page
{
    protected string returnPage = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["returnPlatnosci"] != null)
        {
            returnPage = Session["returnPlatnosci"].ToString();            
        }

        else
        {
            returnPage = "http://" + Request.Url.Host + ":"
                            + Request.Url.Port + ResolveUrl("~") +
                            "Management/professionLogin.aspx";           
        }

        //LogEdit.SaveLog("Accounting_", "Credit Platnosci: Charging ");

    }
}
