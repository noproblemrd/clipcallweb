﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UrlNegative.aspx.cs" Inherits="Platnosci_UrlNegative" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style-platnosci.css" rel="stylesheet" type="text/css" />

    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="wrap">
        <div class="negativebig">
            Płatność nie została zrealizowana.
            <br />
            
        </div>
        
          <div class="text">
          Prosimy skontaktować się z przedstawicielem swojego banku
          <br />
             w celu weryfikacji błędu.
          <br />
          error number <%=Request["error"]%>
          </div>
       
        <div class="buton"><a href="<%=returnPage%>">Wróć do serwisu</a></div> 
    </div>
    
    <!--
        At the top: Płatność nie została zrealizowana.
        Below: Prosimy skontaktować się z przedstawicielem swojego banku w celu weryfikacji błędu.
        Button: Wróć do serwisu

        Payment was failed.
        Please contact to your bank to verify the error.
        Back to the service
    -->
    </form>
</body>
</html>
