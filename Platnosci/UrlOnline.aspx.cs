﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using System.IO;
using System.Xml;

public partial class Platnosci_UrlOnline : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Write("OK");
        // Request.Form recieve session_id=12345656&ts=1302525687105&pos_id=85290&sig=b1379636edfb9072121474f0e03bc198

        string session_id="";
        session_id = Request["session_id"];

        string ts = "";
        ts = Request["ts"];

        string pos_id = "";
        pos_id = Request["pos_id"];

        // k1 372c55888eb3944dd19c3703569a8bd4 
        // k2 877b8e40ef99fdf9bf6354e05f11fa18
        //sig = md5(pos_id + session_id + ts + key1)
        string Myts=DateTime.Now.ToString();

        string Mysig = "";

        if(pos_id=="86966") // test
            Mysig = GetMD5Hash(pos_id + session_id + Myts + "c7601c33d4b88916858107a8bb5046ee");
        else if(pos_id=="85290") // production
            Mysig = GetMD5Hash(pos_id + session_id + Myts + "372c55888eb3944dd19c3703569a8bd4");
       

        SupllierPayment sp = new SupllierPayment();
        sp.paymentMethod = WebReferenceSupplier.ePaymentMethod.CreditCard;//"CreditCard";

        string strWebRquest = "https://www.platnosci.pl/paygw/UTF/Payment/get?pos_id=" + pos_id 
            + "&session_id=" + session_id +  "&ts=" + Myts + "&sig=" + Mysig;

        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(strWebRquest);

        webRequest.Method = "POST";

        Stream webStream = webRequest.GetRequestStream();
        webStream.Close();
        WebResponse webResponse = webRequest.GetResponse();
        webStream = webResponse.GetResponseStream();


        TextReader reader = new StreamReader(webStream);
        string strResponse = reader.ReadToEnd();

        /*
         <response>
	        <status>OK</status>
	        <trans>
		        <id>130139324</id>
		        <pos_id>85290</pos_id>
		        <session_id>12345662</session_id>
		        <order_id></order_id>
		        <amount>1000</amount>
		        <status>99</status>
		        <pay_type>t</pay_type>
		        <pay_gw_name>pt</pay_gw_name>
		        <desc>Payment description</desc>
		        <desc2></desc2>
		        <create>2011-04-11 16:08:54</create>
		        <init>2011-04-11 16:08:58</init>
		        <sent>2011-04-11 16:08:59</sent>
		        <recv>2011-04-11 16:08:59</recv>
		        <cancel></cancel>
		        <auth_fraud>1</auth_fraud>
		        <ts>1302530949567</ts>
		        <sig>0be83c19cbd8291deff62b4e3cb55fbd</sig>	</trans>
        </response>
        */

       

        if (strResponse != "")
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(strResponse);
            string status = xd["response"]["status"].InnerText;

            if (status == "OK")
            {
                string id = "";
                string trans_status = "";
                string amount = "";
                string pay_type = "";
                string desc2 = "";
                string supplierId="";
                string advertiserName="";
                string balanceOld="";
                string userId="";
                string userName="";
                string siteId="";
                string siteLangId="";
                string filedId="";
                string fieldName="";
                string isFirstTimeV="";
                string bonusAmount="";
                string balanceNew = "";
                //Response.Write(xd["response"]["status"].InnerText);
                if (xd["response"]["trans"] != null)
                {
                    sp.chargingCompany = "platnosci";

                    XmlNode xnTrans = xd["response"]["trans"];
                    foreach (XmlNode xn in xnTrans)
                    {
                        switch (xn.Name)
                        {
                            case "id":
                                id = xn.InnerText;
                                sp.transactionId = xn.InnerText;
                                break;

                            case "status":
                                trans_status = xn.InnerText;
                                break;

                            case "amount":
                                double dbl;
                                dbl=double.Parse(xn.InnerText)/100;                                 
                                amount = dbl.ToString();                                
                                sp.deposit = amount;
                                break;

                            case "pay_type":
                                pay_type = xn.InnerText;
                                break;

                            case "desc2":
                                /*
                                 supplierId={4E4B277A-D2F0-DF11-8A5C-0003FF727321}
                                 &advertiserName=test--1
                                 &balanceOld=141364.0000
                                 &userId=b3d9d61c-873e-e011-a69e-0003ff727321
                                 &userName=User1
                                 &siteId=1
                                 &siteLangId=23
                                 &filedId=lbl_YourBalance
                                 &fieldName=יתרה
                                 &isFirstTimeV=False
                                 &bonusAmount=0
                                 &balanceNew=141365 
                                */

                                desc2 = xn.InnerText;
                                break;

                            default:
                                break;

                        } // close switch                                              

                    } // close foreach

                   

                    if (desc2 != "")
                    {
                        string[] arrDesc2 = { };

                        arrDesc2 = desc2.Split('&');

                        string[] nameValueCustom;
                        for (int i = 0; i < arrDesc2.Length; i++)
                        {
                            /*
                            SupplierId={ECAD4D81-5FD4-DF11-81D3-001517D1792A}&
                                AdvertiserName=Company Test&
                                    isFirstTimeV=False&OldValue=5,334&
                                        UserId=a6a59cdf-ba9b-df11-92c8-a4badb37a26f&
                                            UserName=user1&siteLangId=1&
                                            siteId=1015&BonusAmount=72&
                                            NewValue=6306
                            */

                            nameValueCustom = arrDesc2[i].Split('=');
                            if (nameValueCustom[0] == "bonusAmount")
                                sp.discount = nameValueCustom[1];                           
                   
                            if (nameValueCustom[0] == "bonusType")
                                sp.discountType = nameValueCustom[1];

                            if (nameValueCustom[0] == "bonusExtraAmount")
                                sp.extraBonus = nameValueCustom[1];

                            if (nameValueCustom[0] == "extraReasonBonusId")
                                sp.extraReasonBonusId = nameValueCustom[1]; 

                            if (nameValueCustom[0] == "supplierId")
                                sp.guid = nameValueCustom[1];

                            if (nameValueCustom[0] == "advertiserName")
                                sp.name = nameValueCustom[1];

                            if (nameValueCustom[0] == "fieldId")
                                sp.fieldId = nameValueCustom[1];

                            if (nameValueCustom[0] == "fieldName")
                                sp.fieldName = nameValueCustom[1];

                            if (nameValueCustom[0] == "balanceNew")
                                sp.balanceNew = nameValueCustom[1];


                            if (nameValueCustom[0] == "isFirstTimeV")
                                sp.isFirstTimeV = Convert.ToBoolean(nameValueCustom[1]);

                            if (nameValueCustom[0] == "balanceOld")
                                sp.balanceOld = nameValueCustom[1];

                            if (nameValueCustom[0] == "userId")
                                sp.userId = nameValueCustom[1];

                            if (nameValueCustom[0] == "userName")
                                sp.userName = nameValueCustom[1];


                            if (nameValueCustom[0] == "siteLangId")
                                sp.siteLangId = Convert.ToInt32(nameValueCustom[1]);

                            if (nameValueCustom[0] == "siteId")
                                sp.siteId = nameValueCustom[1];


                            if (nameValueCustom[0] == "isAutoRenew")
                                sp.isAutoRenew = Convert.ToBoolean(nameValueCustom[1]);

                            if (nameValueCustom[0] == "isRecharge")
                            {
                                if (nameValueCustom[1] == "false")
                                    sp.isRecharge = false;
                            }

                        } // end for

                         LogEdit.SaveLog("Accounting_" + sp.siteLangId.ToString() , "Credit Platnosci: Charging " +
                                  strResponse + " " + sp.chargingCompany);

                         if (trans_status == "99")
                         {
                           //  WebServiceCharging wsCharging = new WebServiceCharging();
                           //  wsCharging.ChargingGeneral(sp);
                             DpzUtilities.ChargingSupplier(sp, null, Session);
                         }
                    } // end desc2
                }
            }
        }
       
    }


    public string GetMD5Hash(string input)
    {
        System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
        bs = x.ComputeHash(bs);
        System.Text.StringBuilder s = new System.Text.StringBuilder();
        foreach (byte b in bs)
        {
            s.Append(b.ToString("x2").ToLower());
        }
        string password = s.ToString();
        return password;
    }
}
