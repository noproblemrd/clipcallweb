﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UrlPositive.aspx.cs" Inherits="Platnosci_UrlPositive" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style-platnosci.css" rel="stylesheet" type="text/css" />

    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="wrap">
        <div class="positivebig">Dziękujemy za dokonanie wpłaty.</div>
        
          <div class="text">          
            Oczekujemy na potwierdzenie płatności.          
          </div>
       
        <div class="buton" ><a  href="<%=returnPage%>">Wróć do serwisu</a></div> 
    </div>
    
    <!--
        At the top: Dziękujemy za dokonanie wpłaty.
        Below: Oczekujemy na potwierdzenie płatności.
        Button: Wróć do serwisu

        Thanks for the payment.
        We are waiting for payment confirmation.
        Back to the service.    
    -->
    
    </form>
</body>
</html>
