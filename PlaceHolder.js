﻿/*
attributes:
place_holder="text in place holder"
HolderClass="css class of place holder style"
EmptyClass=
*/

$(function () {
    $("input[type=text],input[type=password], textarea").filter(function () {
        return $(this).attr("place_holder") !== undefined;
    }).each(function (index) {
        var _place_holde = $(this).attr('place_holder');
        var _empty_class = $(this).attr('EmptyClass');
        var _elem;
        if ($(this).is("textarea")) {
            _elem = ($("<textarea>")//.offset({ top: this.offsetTop, left: this.offsetLeft })
            //         .height(this.offsetHeight)
            //         .width(this.offsetWidth)
                .attr('id', "pt" + index)
                .css({ 'display': 'inline' }).get(0));

            if ($(this).attr('cols') != undefined)
                $(_elem).attr('cols', $(this).attr('cols'));
            if ($(this).attr('rows') != undefined)
                $(_elem).attr('rows', $(this).attr('rows'));
        }
        else {
            _elem = ($("<input>").attr("type", "text")//.offset({ top: this.offsetTop, left: this.offsetLeft })
            //             .height(this.offsetHeight)
            //             .width(this.offsetWidth)

                .attr('id', "pt" + index)
                .css({ 'display': 'inline' }).get(0));
        }
        if ($(this).attr('type') == 'password' && $(this).attr('autocomplete') != 'off')
            $(this).attr('autocomplete', 'off');
        //     if ($(this).attr('holder-position') == "none")
        //         $(_elem).css('position', '');
        if ($(this).attr('class')) {
            //   $(_elem).attr('class', $(this).attr('class'));
            var _classes = $(this).attr('class').split(' ');
            for (var i = 0; i < _classes.length; i++) {
                var str = $.trim(_classes[i]);
                if (str.length > 0 && str.charAt(0) != '-')
                    $(_elem).addClass(str);
            }
        }
        $(_elem).val(_place_holde);
        //    $(_elem).appendTo($(this).parent());
        $(this).parent().get(0).insertBefore(_elem, this)
        if ($(this).val().length > 0)
            $(_elem).hide();
        else
            $(this).hide();
        if ($(this).attr('HolderClass') != undefined)
            $(_elem).addClass($(this).attr('HolderClass'));
        else
            $(_elem).css({ 'color': '#CCCCCC', 'fontStyle': 'italic', 'fontWeight': 'lighter' });
        var _inputId = this;

        
        $(_elem).focus(function () {        
        $(this).hide();
        $(_inputId).show();
        $(_inputId).focus();
        if (_empty_class && _empty_class.length > 0)
        $(_elem).removeClass(_empty_class);
        });


        $(this).blur(function () {
            if ($(this).val().length > 0)
                return;
            $(this).hide();
            if (_empty_class && _empty_class.length > 0)
                $(_elem).addClass(_empty_class);
            $(_elem).show();
            $(_elem).blur();

        });
/*
        $(this).change(function () {
            if (!$(this).is(':visible')) {
                if ($(this).val().length > 0) {
                    $(_elem).hide();
                    $(this).show();
                    $(this).focus();
                    if (_empty_class && _empty_class.length > 0)
                        $(_elem).removeClass(_empty_class);
                }                
            }
        });
*/
    });

});