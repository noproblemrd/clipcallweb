﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class sendEmail : System.Web.UI.Page
{
    protected string from = "shaim@onecall.co.il";
    protected string category = "";
    protected string subject = "";
    protected string message = "";
    protected string mode = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        /*************** send to company ****************/
        if(Request["email"]!=null)
            from=Request["email"];
        MailSender mailSender = new MailSender();
        mailSender.From = from;
        mailSender.To = "shaim@noproblem.co.il;shaip@noproblem.co.il;idod@noproblem.co.il;daniels@noproblem.co.il";
        mailSender.IsText = false;

        if (Request["mode"] != null)
        {
            mode = Request["mode"];

            if (mode == "wizard")
                mailSender.Subject = "New wizard registration";
            else if(mode == "contactUs")
                mailSender.Subject = "Contact us request";
        }

        StringBuilder sb = new StringBuilder();
        sb.Append("email: " + from);

        if (Request["category"] != null)
        {
            category = Request["category"];
            if (!string.IsNullOrEmpty(category))
                sb.Append("<br>category: " + category);
        }

        if (Request["subject"] != null)
        {
            subject = Request["subject"];
            if (!string.IsNullOrEmpty(subject))
                sb.Append("<br>subject: " + subject);
        }

        if (Request["message"] != null)
        {
            message = Request["message"];
            if (!string.IsNullOrEmpty(message))
                sb.Append("<br>message: " + message);
        }

        string body = sb.ToString();

        mailSender.Body = body;
        bool ifSend=mailSender.Send();


        if (ifSend==true)
            Response.Write("success");
        else // false
            Response.Write("unsuccess");

        /*************** send to client ****************/
        mailSender.From = "shaim@onecall.co.il";
        mailSender.To = from;
        mailSender.IsText = false;

        if (Request["mode"] != null)
        {
            mode = Request["mode"];

            if (mode == "wizard")
                mailSender.Subject = "New wizard registration";
            else if (mode == "contactUs")
                mailSender.Subject = "Contact us request";
        }

        sb = new StringBuilder();
        sb.Append("email: " + from);

        if (Request["category"] != null)
        {
            category = Request["category"];
            if (!string.IsNullOrEmpty(category))
                sb.Append("<br>category: " + category);
        }

        if (Request["subject"] != null)
        {
            subject = Request["subject"];
            if (!string.IsNullOrEmpty(subject))
                sb.Append("<br>subject: " + subject);
        }

        if (Request["message"] != null)
        {
            message = Request["message"];
            if (!string.IsNullOrEmpty(message))
                sb.Append("<br>message: " + message);
        }

        body = sb.ToString();

        ifSend = mailSender.Send();




    }
}