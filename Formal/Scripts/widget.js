﻿var place_holder_mail = "e.g. me@awesomesite.com";
var place_holder_message = "Tell us how can we work together.";
/*
var imgSend = new Image();
imgSend.src = 'formal/images/contactUs/send.png';

var imgHover = new Image();
imgHover.src = 'formal/images/contactUs/send-hover.png';

var imgClick = new Image();
imgClick.src = 'formal/images/contactUs/clicked.png';
*/
function showHideForm() {
    if (document.getElementById("formContact").style.display == "none") {
        document.getElementById("formContact").style.display = 'block';

        document.getElementById("formContactError").style.display = 'none';
        document.getElementById("formContactCategories").style.display = 'block';
        document.getElementById("formContactEmail").style.display = 'block';
        document.getElementById("formContactSubject").style.display = 'block';
        document.getElementById("formContactMessage").style.display = 'block';
        document.getElementById("formContactSend").style.display = 'block';
    }
    else
        document.getElementById("formContact").style.display = 'none';
}


function EmailFocus(_value) {
    //alert(_value);
    var txt_email = document.getElementById("emailContactUs");
    if (txt_email.className == "place-holder" && txt_email.value == place_holder_mail) {
        txt_email.className = "";
        txt_email.value = "";
    }
}

function MessageFocus(_value) {
    //alert(_value);
    var txt_message = document.getElementById("messageContactUs");

    if (txt_message.className == "textareaContactUs" && txt_message.value == place_holder_message) {
        txt_message.className = "";
        txt_message.value = "";
    }
}


imgSubmitOver = new Image();
imgSubmitOver.src = "../formal/images/wizard/submit-hover.png";

imgSubmitOut = new Image();
imgSubmitOut.src = "../formal/images/wizard/submit.png";

imgRedError = new Image();
imgRedError.src = "../formal/images/wizard/redSmileyError.png";

imgOrangeError = new Image();
imgOrangeError.src = "../formal/images/wizard/OrangeSmileyInstructor.png";

imgGreenError = new Image();
imgGreenError.src = "../formal/images/wizard/greenSmileyError.png";



function showHide() {
    if (document.getElementById("video").style.display == 'block') {
        document.getElementById("video").style.display = 'none';
        document.getElementById("phone").style.display = 'block';
        document.getElementById("iframeVideo").src = "";
        document.getElementById("div_boxhomeadvertiser").style.marginTop = '100px';
        document.getElementById("div_boxhomeadpublisher").style.marginTop = '100px';

    }
    else {
        //alert(document.getElementById("iframeVideo").style.left);


        document.getElementById("iframeVideo").src = 'http://www.youtube.com/embed/2lnZoIF2sVE?rel=0&amp;hd=1&autoplay=1&autohide=1';
        document.getElementById("video").style.display = 'block';
        document.getElementById("phone").style.display = 'none';
        window.location.hash = "anchorVideo";
        document.getElementById("div_boxhomeadvertiser").style.marginTop = '30px';
        document.getElementById("div_boxhomeadpublisher").style.marginTop = '30px';

    }
}


function showHideWizard() {

 //   if (document.getElementById("wrapperWizard").style.display == 'none') {
        //      document.getElementById("wrapperWizard").style.display = "block";        
        //      $('#wrapperWizard').show('drop', { direction: "up" }, 500);
        $('#wrapperWizard').slideToggle("slow");
  //  }
//    else
    //      document.getElementById("wrapperWizard").style.display = "none";
  //      $('#wrapperWizard').hide('drop', { direction: "up" }, 500);

    //window.location.hash = "";
    window.location.hash = "wizardTop";

}

function showWizard() {    
    document.getElementById("wrapperWizard").style.display = "block";
    window.location.hash = "";
    window.location.hash = "wizardTop";

}

function validation(mode) {
    
    
    var email
    email = document.getElementById("email").value;
    
    var ifValidEmail = true;
    //var regFormatEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    var regFormatEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;   

    if (email.search(regFormatEmail) == -1) //if match failed
    {

        ifValidEmail = false;

        document.getElementById("validationText").className = '';

        if (mode == 'blur') {
            if (email == "") {
                document.getElementById("validation").style.display = 'none';
                document.getElementById("validationText").className = '';
            }
            else {
                document.getElementById("validation").style.display = 'block';
                document.getElementById("validationIcon").innerHTML = "<img src='" + imgRedError.src + "'>";
                document.getElementById("validationText").innerHTML = 'Enter valid email address';
                //document.getElementById("validationText").style.color = 'red';
                document.getElementById("validationText").className = 'validationTextErrorSubmit';
            }
        }

        else if (mode == 'keyup') {
            document.getElementById("validation").style.display = 'block';
            document.getElementById("validationIcon").innerHTML = "<img src='" + imgOrangeError.src + "'>";
            document.getElementById("validationText").innerHTML = 'Enter valid email address';
            document.getElementById("validationText").className = 'validationTextErrorKeyUp'; 
            //document.getElementById("validationText").style.color = 'orange';
        }

        if (mode == 'submit') {
            document.getElementById("validation").style.display = 'block';
            document.getElementById("validationIcon").innerHTML = "<img src='" + imgRedError.src + "'>";
            document.getElementById("validationText").innerHTML = 'Enter valid email address';
            //document.getElementById("validationText").style.color = 'red';
            document.getElementById("validationText").className = 'validationTextErrorSubmit';
        }
    }

    else {
        document.getElementById("validation").style.display = 'block';
        document.getElementById("validationIcon").innerHTML = "<img src='" + imgGreenError.src + "'>";
        document.getElementById("validationText").innerHTML = '';
    }


    return ifValidEmail;
    //alert(ifValidEmail);
 

}


function sendWizard() {

    if (!validation('submit'))
        return false;
    
    email = document.getElementById("email").value;

    if (email == "") {
        alert("Must fill email");
        document.getElementById("email").select();
        return;
    }

    var params;
    params = "mode=wizard&email=" + email;
   
    var url;
   
    var posLastSlash;
    posLastSlash = location.href.lastIndexOf('/');
    //alert();

    url = location.href.substring(0, posLastSlash + 1) + "formal/sendEmailFormal.aspx";
    
    var objHttp;
    var ua = navigator.userAgent.toLowerCase();

    if (ua.indexOf("msie") != -1) {
        // alert("msie");
        objHttp = new ActiveXObject("Msxml2.XMLHTTP");

    }
    else {
        //alert("not msie");
        objHttp = new XMLHttpRequest();
    }



    if (objHttp == null) {
        //alert("Unable to create DOM document!");
        return;
    }


    objHttp.open("POST", url, true);

    objHttp.onreadystatechange = function () {

        if (objHttp.readyState == 4) {
        //    alert(objHttp.responseText);

            if (objHttp.responseText == "unsuccess") {

                //document.getElementById("wizardText").className = 'NoneFieldError';
                strError = '<div id="validationIcon" class="validationIcon"><img src="formal/images/error-message.png" /></div><div class="validationTextErrorServer">Problem in sending.</div>';
                document.getElementById("wizardText").innerHTML = strError;

                //document.getElementById("wizardText").innerHTML = "We will send you the link with instructions for installing the widget on your site when we launch it. If you need any help or have any questions, you can check our FAQs or contact us by email.";
                document.getElementById("wizardTitle1").innerHTML = "Try again";
                document.getElementById("wizardTitle2").innerHTML = "";
            }

            else {

                document.getElementById("wizardText").innerHTML = "We received your email address. Check out our <a href='http://www2.noproblemppc.com/faq.htm' style='color:#8EBB03;text-decoration:none;' onmouseover='this.style.color=\"white\";' onmouseout='this.style.color=\"#8EBB03\";'>FAQs</a>";

                document.getElementById("wizardTitle1").innerHTML = "Thank you!";
                document.getElementById("wizardTitle2").innerHTML = "";

            }

            document.getElementById("wizardTitle").style.top = '90px';
            document.getElementById("wizardText").style.width = '500px';
            document.getElementById("wizardInput").style.display = 'none';
            document.getElementById("wizardEmailText").style.display = 'none';
            document.getElementById("wizardSubmit").style.display = 'none';
            document.getElementById("validation").style.display = 'none';




        }

    }
    objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    objHttp.setRequestHeader("Content-Length", params.length);
    objHttp.setRequestHeader("Connection", "close");

    objHttp.send(params);


    return true;

}


function validationContactUs(mode,category) {

    if (category == "email" || category == "all") {
       
        var email
        email = document.getElementById("emailContactUs").value;

        var ifValid = true;
        //var regFormatEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        var regFormatEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (email == "" || email == place_holder_mail) {
            ifValid = false;

            if (mode == 'blur') {
                document.getElementById("validationContactUs").style.display = 'none';
                document.getElementById("emailContactUs").value = place_holder_mail;
                document.getElementById("emailContactUs").className = 'place-holder';
            }

            else if (mode == 'submit') {
                document.getElementById("validationContactUs").style.display = 'inline-block';
                document.getElementById("validationIconContactUs").innerHTML = "<img src='" + imgRedError.src + "'>";
                document.getElementById("validationTextContactUs").innerHTML = 'Enter valid email address';
                //document.getElementById("validationText").style.color = 'red';
                document.getElementById("validationTextContactUs").className = 'validationTextErrorSubmit';
            }
        }

        else if (email.search(regFormatEmail) == -1) //if match failed
        {
            document.getElementById("validationContactUs").style.display = 'inline-block';

            ifValid = false;

            document.getElementById("validationTextContactUs").className = '';

            if (mode == 'blur') {
                document.getElementById("validationIconContactUs").innerHTML = "<img src='" + imgRedError.src + "'>";
                document.getElementById("validationTextContactUs").innerHTML = 'Enter valid email address';
                //document.getElementById("validationText").style.color = 'red';
                document.getElementById("validationTextContactUs").className = 'validationTextErrorSubmit';
            }

            else if (mode == 'keyup') {
                document.getElementById("validationIconContactUs").innerHTML = "<img src='" + imgOrangeError.src + "'>";
                document.getElementById("validationTextContactUs").innerHTML = 'Enter valid email address';
                document.getElementById("validationTextContactUs").className = 'validationTextErrorKeyUp';
                //document.getElementById("validationText").style.color = 'orange';
            }

            else if (mode == 'submit') {
                document.getElementById("validationIconContactUs").innerHTML = "<img src='" + imgRedError.src + "'>";
                document.getElementById("validationTextContactUs").innerHTML = 'Enter valid email address';
                //document.getElementById("validationText").style.color = 'red';
                document.getElementById("validationTextContactUs").className = 'validationTextErrorSubmit';
            }
        }

        else {
            document.getElementById("validationContactUs").style.display = 'inline-block';
            document.getElementById("validationIconContactUs").innerHTML = "<img src='" + imgGreenError.src + "'>";
            document.getElementById("validationTextContactUs").innerHTML = '';
        }
    }

    
    if (category == "subject" || category == "all") {

        

        var subject
        subject = document.getElementById("subjectContactUs").value;        


        if (subject == "") //if match failed
        {

            ifValid = false;

            document.getElementById("validationTextContactUsSubject").className = '';

            if (mode == 'blur') {
                document.getElementById("validationContactUsSubject").style.display = 'none';                           
                document.getElementById("subjectContactUs").className = 'place-holder';
            }

            else if (mode == 'keyup') {
                document.getElementById("subjectContactUs").className = '';
                document.getElementById("validationContactUsSubject").style.display = 'inline-block';
                document.getElementById("validationIconContactUsSubject").innerHTML = "<img src='" + imgOrangeError.src + "'>";
                document.getElementById("validationTextContactUsSubject").innerHTML = 'Enter subject';
                document.getElementById("validationTextContactUsSubject").className = 'validationTextErrorKeyUp';
               
            }

            else if (mode == 'submit') {
                document.getElementById("subjectContactUs").className = '';
                document.getElementById("validationContactUsSubject").style.display = 'inline-block';
                document.getElementById("validationIconContactUsSubject").innerHTML = "<img src='" + imgRedError.src + "'>";
                document.getElementById("validationTextContactUsSubject").innerHTML = 'Enter subject';
                document.getElementById("validationTextContactUsSubject").className = 'validationTextErrorSubmit';
            }
        }

        else {
            document.getElementById("subjectContactUs").className = '';
            document.getElementById("validationContactUsSubject").style.display = 'inline-block';
            document.getElementById("validationIconContactUsSubject").innerHTML = "<img src='" + imgGreenError.src + "'>";
            document.getElementById("validationTextContactUsSubject").innerHTML = '';
        }
    }
   
    
    if (category == "message" || category == "all") {

        document.getElementById("validationContactUsMessage").style.display = 'inline-block';

        var message
        message = document.getElementById("messageContactUs").value;
               
        if (message == "" || message == place_holder_message) //if match failed
        {
            
            ifValid = false;

            document.getElementById("validationTextContactUsMessage").className = '';

            if (mode == 'blur') {
                document.getElementById("validationContactUsMessage").style.display = 'none';
                document.getElementById("messageContactUs").value = place_holder_message;
                document.getElementById("messageContactUs").className = 'textareaContactUs';
                /*
                document.getElementById("validationIconContactUs").innerHTML = "<img src='" + imgRedError.src + "'>";
                document.getElementById("validationTextContactUs").innerHTML = 'Enter valid email address';
               
                document.getElementById("validationTextContactUs").className = 'validationTextErrorSubmit';
                */
            }

            else if (mode == 'keyup') {
                document.getElementById("validationIconContactUsMessage").innerHTML = "<img src='" + imgOrangeError.src + "'>";
                document.getElementById("validationTextContactUsMessage").innerHTML = 'Enter message';
                document.getElementById("validationTextContactUsMessage").className = 'validationTextErrorKeyUp';

            }

            if (mode == 'submit') {

                document.getElementById("validationIconContactUsMessage").innerHTML = "<img src='" + imgRedError.src + "'>";
                document.getElementById("validationTextContactUsMessage").innerHTML = 'Enter message';                
                document.getElementById("validationTextContactUsMessage").className = 'validationTextErrorSubmit';
            }
        }

        else {
            document.getElementById("validationIconContactUsMessage").innerHTML = "<img src='" + imgGreenError.src + "'>";
            document.getElementById("validationTextContactUsMessage").innerHTML = '';
        }
       

    }

    if (category == "category" || category == "all") {

        document.getElementById("validationContactUsCategory").style.display = 'inline-block';

        len = document.forms["form1"].categoryContactUs.length;
        var ifChooseRadioCategory = false;

        for (i = 0; i < len; i++) {
            if (document.forms["form1"].categoryContactUs[i].checked) {
                ifChooseRadioCategory = true;
            }
        }

        if (!ifChooseRadioCategory) {

            ifValid = false;

            if (mode == 'click') {

            }
            if (mode == 'submit') {

                document.getElementById("validationIconContactUsCategory").innerHTML = "<img src='" + imgRedError.src + "'>";
                document.getElementById("validationTextContactUsCategory").innerHTML = 'Enter category';
                document.getElementById("validationTextContactUsCategory").className = 'validationTextErrorSubmit';
            }
        }

        else {
            document.getElementById("validationIconContactUsCategory").innerHTML = "<img src='" + imgGreenError.src + "'>";
            document.getElementById("validationTextContactUsCategory").innerHTML = '';
        }
    }

    return ifValid;
    


}

function sendContactUs() {

    
    if (!validationContactUs('submit','all'))
        return false;
      

    len = document.forms["form1"].categoryContactUs.length;
    category = "";
    for (i = 0; i <len; i++)
    {
        if (document.forms["form1"].categoryContactUs[i].checked)
        {
            category = document.forms["form1"].categoryContactUs[i].value
        }
    }   
    
    email = document.getElementById("emailContactUs").value;    
    subject = document.getElementById("subjectContactUs").value;
    message = document.getElementById("messageContactUs").value;

    if (email == "") {
        alert("Must fill email");
        document.getElementById("email").select();
        return;
    }

    var params;
    params = "mode=contactUs&category=" + category + 
        "&email=" + email + "&subject=" + subject + "&message=" + message;
    //alert(params);
    var url;

    var posLastSlash;
    posLastSlash = location.href.lastIndexOf('/');
    //alert();

    url = location.href.substring(0, posLastSlash + 1) + "formal/sendEmailFormal.aspx";

    var objHttp;
    var ua = navigator.userAgent.toLowerCase();

    if (ua.indexOf("msie") != -1) {
        // alert("msie");
        objHttp = new ActiveXObject("Msxml2.XMLHTTP");

    }
    else {
        //alert("not msie");
        objHttp = new XMLHttpRequest();
    }



    if (objHttp == null) {
        //alert("Unable to create DOM document!");
        return;
    }


    objHttp.open("POST", url, true);

    objHttp.onreadystatechange = function () {

        if (objHttp.readyState == 4) {
            //alert(objHttp.responseText);

            if (objHttp.responseText == "unsuccess") {

                strError = '<div id="validationIcon" class="validationIcon"><img src="formal/images/error-message.png" /></div><div class="validationTextErrorServer">Problem in sending.</div>';
                document.getElementById("formContactErrorText").innerHTML = strError;

                document.getElementById("formContactErrorTitle").innerHTML = "Try again";                
                
            }

            else {

            // take the default text
              
            }

            document.getElementById("formContactError").style.display = 'block';

            document.getElementById("formContactCategories").style.display = 'none';
            document.getElementById("formContactEmail").style.display = 'none';
            document.getElementById("formContactSubject").style.display = 'none';
            document.getElementById("formContactMessage").style.display = 'none';
            document.getElementById("formContactSend").style.display = 'none';



        }

    }
    objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    objHttp.setRequestHeader("Content-Length", params.length);
    objHttp.setRequestHeader("Connection", "close");

    objHttp.send(params);


    return true;

}