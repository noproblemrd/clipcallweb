﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class audio_MediaPlayer : System.Web.UI.Page
{
    string _audio;
    protected void Page_Load(object sender, EventArgs e)
    {
        _audio = Request.QueryString["audio"];
        if (string.IsNullOrEmpty(_audio))
            return;
        try
        {
            _audio = EncryptString.DecodeRecordPath(_audio);
        }
        catch (Exception exc)
        {
            return;
        }
        form1.DataBind();

    }
    protected string GetPath
    {
        get { return _audio; }
    }
}