﻿<%@ WebHandler Language="C#" Class="DownloadVideo" %>

using System;
using System.Web;
using System.Net;
using System.IO;

public class DownloadVideo : IHttpHandler {
    const string VIDEO_NAME = "video.mp4";
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();

        string str = context.Request.QueryString["str"];
        if (string.IsNullOrEmpty(str))
        {
            VideoProblem(context);
            return;
        }
        try
        {
            str = EncryptString.DecodeRecordPath(str);
        }
        catch (Exception exc)
        {
            VideoProblem(context);
            return;
        }
        
        Uri videoUri = new Uri(str);
        string ext = System.IO.Path.GetExtension(videoUri.AbsoluteUri);
        if (ext.Equals(".m3u8"))
        {
            VideoConvertorManager vcm = new VideoConvertorManager(videoUri.AbsoluteUri);
            
            byte[] videoBytes = vcm.GetVideoBytes();
            if(videoBytes == null)
            {
                VideoProblem(context);
                return;
            }
            context.Response.ContentType = "video/mp4";
            context.Response.AppendHeader("Content-disposition", "attachment; filename=" + VIDEO_NAME);
            context.Response.BinaryWrite(videoBytes);
            return;
        }
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(videoUri.AbsoluteUri);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        Stream file = response.GetResponseStream();
        context.Response.ContentType = "video/mp4";
        context.Response.AppendHeader("Content-disposition", "attachment; filename=" + VIDEO_NAME);
        context.Response.BinaryWrite(ReadFully(file));  
        
    }
    void VideoProblem(HttpContext context)
    {
        context.Response.ContentType = "text/html";
        context.Response.Write("<html><head><script type=\"text/javascript\">");
        context.Response.Write("alert('Something went wrong with this video!'); window.history.back();");
        context.Response.Write("</script></head><body></body></html>");
    }
    public byte[] ReadFully(Stream input)
    {
        byte[] buffer = new byte[16 * 1024];
        using (MemoryStream ms = new MemoryStream())
        {
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            return ms.ToArray();
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}