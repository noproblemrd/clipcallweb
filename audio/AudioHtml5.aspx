﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AudioHtml5.aspx.cs" Inherits="audio_AudioHtml5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <video controls="controls" autoplay="autoplay">
            <source src="<%# GetPath %>" type="audio/mpeg">
        </video>
    
    </div>
    </form>
</body>
</html>
