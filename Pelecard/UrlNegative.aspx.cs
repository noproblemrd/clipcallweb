﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Pelecard_UrlNegative : PageSetting
{
    protected string returnPage = "";
    protected string resultTransuctionStatus = "";
    

    protected string pelecardUserName = "";                      
    protected string pelecardPassword = "";                    
    protected string pelecardTermNo = "";
    protected string pelecardShopNo = "";

    protected string resultTransuctionSum = "";
    protected string resultTransuctionMyParam = "";
    
    
    protected void Page_Load(object sender, EventArgs e)
    
    {
        //Response.Write(Request.UrlReferrer.AbsoluteUri);

        /*
        string resultTransuctionNumConfirm = "";

        if (Request.Form["result"] != null)
        {
            result = Request.Form["result"];

            if (result.Length >= 3) // pos 0 till pos 2 length 2
                result = result.Substring(0, 3);
        }

        string strGeneral = "";

        foreach (string str in Request.Form)
            strGeneral += str + " " + Request.Form[str];

        LogEdit.SaveLog("Accounting_", "Pelecard negative qa:" + Request.QueryString.ToString() +
            result + "\r\nconfirm: " + resultTransuctionNumConfirm + " strGeneral:" + strGeneral);



        if (Session["returnToSite"] != null)
            returnPage = Session["returnToSite"].ToString();
        else
            returnPage = "http://" + Request.Url.Host + ":"
                            + Request.Url.Port + ResolveUrl("~") +
                            "Management/professionLogin.aspx";
        */

        string result = "";       
        string resultTransuctionNumConfirm = "";
        

        string siteLangId = "";
        string siteAdvertiserName = "";
        string siteAdvertiserGuid = "";
        string accountNumber = "";

        /******* GET COMPANY DETAIS *******/
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            string commandPelecardCompany = "EXEC dbo.GetChargingCompanyDetails @company";
            //SqlConnection connAccounting = DBConnection.GetConnString();
            //conn.Open();
            SqlCommand cmdPelecardCompany = new SqlCommand(commandPelecardCompany, conn);
            cmdPelecardCompany.Parameters.AddWithValue("@company", siteSetting.CharchingCompany.ToString());
            SqlDataReader readerPelecardCompany = cmdPelecardCompany.ExecuteReader();


            if (readerPelecardCompany.HasRows)
            {


                while (readerPelecardCompany.Read())
                {
                    switch ((string)readerPelecardCompany["item"])
                    {
                        case "userName":
                            pelecardUserName = (string)readerPelecardCompany["value"];
                            break;

                        case "password":
                            pelecardPassword = (string)readerPelecardCompany["value"];
                            break;

                        case "termNo":
                            pelecardTermNo = (string)readerPelecardCompany["value"];
                            break;

                        case "shopNo":
                            pelecardShopNo = (string)readerPelecardCompany["value"];
                            break;

                        default:
                            break;
                    }

                }

            }

            readerPelecardCompany.Close();

            /******* END GET COMPANY DETAIS *******/

            if (Session["returnToSite"] != null)
                returnPage = Session["returnToSite"].ToString();
            else
                returnPage = "http://" + Request.Url.Host + ":"
                                + Request.Url.Port + ResolveUrl("~") +
                                "Management/professionLogin.aspx";

            if (Request.Form["result"] != null)
            {
                result = Request.Form["result"];
                // the length of the string is 140 characters
                // the 2 characters in the end are CL\LF

                if (result.Length >= 3) // pos 0 till pos 2 length 2
                    resultTransuctionStatus = result.Substring(0, 3);


                if (result.Length >= 44) // pos 36 till pos 43 length 8
                {
                    int amount = Int32.Parse(result.Substring(35, 8)) / 100; //סכום באגורות i put 100 return 00000100
                    resultTransuctionSum = amount.ToString();
                }

                if (result.Length >= 78) // pos 71 till pos 77 length 7
                    resultTransuctionNumConfirm = result.Substring(70, 7);


                if (result.Length >= 138) // pos 120 till pos 138 length 19
                    resultTransuctionMyParam = result.Substring(119, 19).Trim(); // פרטים ששלחתי בטקסט חופשי       


                /*
                string strGeneral = "";

            
                foreach (string str in Request.Form)
                    strGeneral+=str+ " " + Request.Form[str];
                */

                if (!String.IsNullOrEmpty(resultTransuctionMyParam))
                {
                    SupllierPayment sp = new SupllierPayment();

                    sp.deposit = resultTransuctionSum;
                    sp.chargingCompany = siteSetting.CharchingCompany.ToString();
                    sp.transactionId = resultTransuctionNumConfirm;

                    string command = "EXEC dbo._SetConfirmGetPayment @auto_id";
                    //SqlConnection conn = DBConnection.GetConnString();
                    //conn.Open();
                    SqlCommand cmd = new SqlCommand(command, conn);
                    cmd.Parameters.AddWithValue("@auto_id", resultTransuctionMyParam);
                    string paymentDetails = Convert.ToString(cmd.ExecuteScalar());


                    string[] arrPaymentDetails = { };

                    arrPaymentDetails = paymentDetails.Split('&');

                    string[] nameValueCustom;
                    for (int i = 0; i < arrPaymentDetails.Length; i++)
                    {
                        /*
                        SupplierId={ECAD4D81-5FD4-DF11-81D3-001517D1792A}&
                            AdvertiserName=Company Test&
                                isFirstTimeV=False&OldValue=5,334&
                                    UserId=a6a59cdf-ba9b-df11-92c8-a4badb37a26f&
                                        UserName=user1&siteLangId=1&
                                        siteId=1015&BonusAmount=72&
                                        NewValue=6306
                        */

                        nameValueCustom = arrPaymentDetails[i].Split('=');
                        if (nameValueCustom[0] == "bonusAmount")
                            sp.discount = nameValueCustom[1];


                        if (nameValueCustom[0] == "supplierGuid")
                        {
                            sp.guid = nameValueCustom[1];
                            siteAdvertiserGuid = nameValueCustom[1];
                        }

                        if (nameValueCustom[0] == "advertiserName")
                        {
                            sp.name = nameValueCustom[1];
                            siteAdvertiserName = nameValueCustom[1];
                        }

                        if (nameValueCustom[0] == "fieldId")
                            sp.fieldId = nameValueCustom[1];

                        if (nameValueCustom[0] == "fieldName")
                            sp.fieldName = nameValueCustom[1];

                        if (nameValueCustom[0] == "balanceNew")
                            sp.balanceNew = nameValueCustom[1];


                        if (nameValueCustom[0] == "isFirstTimeV")
                            sp.isFirstTimeV = Convert.ToBoolean(nameValueCustom[1]);

                        if (nameValueCustom[0] == "balanceOld")
                            sp.balanceOld = nameValueCustom[1];

                        if (nameValueCustom[0] == "userId")
                            sp.userId = nameValueCustom[1];

                        if (nameValueCustom[0] == "userName")
                            sp.userName = nameValueCustom[1];


                        if (nameValueCustom[0] == "siteLangId")
                        {
                            sp.siteLangId = Convert.ToInt32(nameValueCustom[1]);
                            siteLangId = Convert.ToString(sp.siteLangId);
                        }

                        if (nameValueCustom[0] == "siteId")
                            sp.siteId = nameValueCustom[1];


                        if (nameValueCustom[0] == "isAutoRenew")
                            sp.isAutoRenew = Convert.ToBoolean(nameValueCustom[1]);

                        if (nameValueCustom[0] == "accountNumber")
                        {
                            sp.accountNumber = nameValueCustom[1];
                            accountNumber = nameValueCustom[1];
                        }

                    }

                    // end for
                }


                LogEdit.SaveLog("Accounting_" + siteLangId, "Pelecard negative :" +
                "\r\naccountNumber: " + accountNumber +
                "\r\nadvertiserName: " + Server.UrlDecode(siteAdvertiserName) +
                "\r\nresult: " + result +
                "\r\nconfirm: " + resultTransuctionNumConfirm +

                "\r\nresultTransuctionSum: " + resultTransuctionSum +
                "\r\nresultTransuctionStatus: " + resultTransuctionStatus +
                "\r\nresultTransuctionMyParam: " + resultTransuctionMyParam);

                MailSender mail = new MailSender();
                mail.To = "invoices@noproblem.co.il;saarm@noproblem.co.il";
                mail.Bcc = "shaim@onecall.co.il";
                mail.IsText = false;
                string body = "";
                body = "Pelecard negative :" +
                "<br>Account number: " + accountNumber +
                "<br>Advertiser Vat Name : " + Server.UrlDecode(siteAdvertiserName) +
                "<br>result: " + result +
                "<br>confirm: " + resultTransuctionNumConfirm +
                "<br>avertiser name: " + siteAdvertiserName +
                "<br>resultTransuctionSum: " + resultTransuctionSum +
                "<br>resultTransuctionStatus: " + resultTransuctionStatus +
                "<br>resultTransuctionMyParam: " + resultTransuctionMyParam;
                mail.Body = body;
                mail.From = "invoices@noproblem.co.il";
                mail.Subject = "Negative transaction from pelecard";
                mail.Send();


                if (!String.IsNullOrEmpty(resultTransuctionSum))
                    resultTransuctionSum = resultTransuctionSum + "00";

                Page.DataBind();
                conn.Close();
            }
        }

    }
}
