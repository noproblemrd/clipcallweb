﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UrlNegative.aspx.cs" Inherits="Pelecard_UrlNegative" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>בעיית חיוב</title>
    <link href="style-pelecard.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
       //alert(document.referrer);
       
       function sendFormPeleCard()
       {         
          MyformPelecrd=document.forms["formPelecard"];
          MyformPelecrd.total.value = '<%#resultTransuctionSum%>';
          MyformPelecrd.Parmx.value = '<%#resultTransuctionMyParam%>';
          //alert(MyformPelecrd.total.value);
          MyformPelecrd.submit();           
       }
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
     <div class="wrap">
        <div class="negativebig">
            העיסקה לא בוצעה      
        </div>
        <div class="xline"></div>
        
          <div class="text">
           טעות מספר <%#resultTransuctionStatus%>          
          </div>
       <div class="submit">
            <div class="buton"><a href="<%=returnPage%>">חזור לאתר</a></div>            
            <div class="buton"><a href="javascript:void(0);" onclick="javascript:sendFormPeleCard();">נסה שנית</a></div> 
       </div>
    </div>    
  
    
    </form>
    
    
     <form style="display:none;" action="https://gateway.pelecard.biz/" method="post" name="formPelecard" id="formPelecard" target="_top">
        <input name="Logo" type="text" value="http://qa.ppcnp.com/images/logos/logoNoProblem.jpg" size="20"  /> - לוגו <br />
        <input name="userName" type="text" value="<%#pelecardUserName%>" size="20" /> - שם משתמש  <br />
        <input name="password" type="text" value="<%#pelecardPassword%>" size="20" /> - סיסמא  <br />
        <input name="termNo" type="text" value="<%#pelecardTermNo%>" size="20" /> -  מסוף <br />
        <input name="shopNo" type="text" value="<%#pelecardShopNo%>" size="20" /> - חנות  <br />       
        <input name="token" type="text" value="" size="20" /> - טוקן  <br />
        <input name="total" type="text" value="" size="20" /> - סכום  <br />
        <input name="currency" type="text" value="1" size="20" /> - מטבע  <br />
        <input name="creditType" type="text" value="DebitRegularType" size="20" /> - סוג אשראי  <br />
        <input name="paymentsNo" type="text" value="" size="20" /> - תשלומים  <br />
        <input name="firstPymnt" type="text" value="" size="20" /> - תשלום ראשון  <br />
        <input name="id" type="text" value="" size="20" /> -  ת.ז <br />
        <!--<input name="cvv2" type="text" value="Must" size="20" /> -  cvv2 <br />-->
        <input name="authNum" type="text" value="" size="20" /> -  אישור <br />
        <input name="Parmx" type="text" value="Test parmx" size="20" /> - פרטים נוספים  <br />
        <input name="hideParmx" type="text" value="True" size="20" />True - הסתר פרטים נוספים <br />
        <input name="goodUrl" type="text" value="<%#Session["pelecardPageUrl"]%>Pelecard/UrlPositive.aspx" size="20" /> - החזרת תשובה תקינה ל:  <br />
        <input name="errorUrl" type="text" value="<%#Session["pelecardPageUrl"]%>Pelecard/UrlNegative.aspx" size="20" /> - החזרת תשובת שגיאה ל:  <br />
       

        <input name="frmAction" type="text" value="CreateToken" size="20" />CreateToken - יצירת טוקן  <br />
        <input name="J5" type="text" value ="" size="20" /> J5  <br />
    </form>
</body>
</html>
