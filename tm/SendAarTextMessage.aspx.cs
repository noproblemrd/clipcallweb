﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class tm_SendAarTextMessage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Header.DataBind();
    }
    protected void btn_send_Click(object sender, EventArgs e)
    {
        Guid IncidentId;
        if (!Guid.TryParse(txt_IncidentId.Text, out IncidentId))
            return;
        int count;
        if (!int.TryParse(txt_SupplierCount.Text, out count))
            return;
        string PromptName = txt_PromptName.Text;
        AarService.AarService _aar = WebServiceConfig.GetAarService();
        _aar.Timeout = 600000;
        string _response;
        try
        {
            _response = _aar.ExecuteAarTextMessage(count, IncidentId, PromptName);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            _response = exc.Message;
        }
        lbl_Response.Text = _response;

    }
}