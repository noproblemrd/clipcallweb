﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendAarTextMessage.aspx.cs" Inherits="tm_SendAarTextMessage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">       
        function _Click() {
            document.querySelector('#<%# btn_send.ClientID %>').style.display = "none";
            document.querySelector('#lbl_wait').style.display = "inline";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <table>
            <tr>
                <td>
                    Incident ID:
                </td>
                <td>
                    <asp:TextBox ID="txt_IncidentId" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Supplier count:
                </td>
                <td>
                    <asp:TextBox ID="txt_SupplierCount" runat="server"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>
                    Prompt name:
                </td>
                <td>
                    <asp:TextBox ID="txt_PromptName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btn_send" runat="server" Text="SEND" OnClick="btn_send_Click" OnClientClick="_Click();" />
                    <span style="display:none;" id="lbl_wait">Please wait...</span>
                </td>
            </tr>
             <tr>
                <td colspan="2">
                    <asp:Label ID="lbl_Response" runat="server" ></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
