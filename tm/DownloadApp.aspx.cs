﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class tm_DownloadApp : System.Web.UI.Page
{
    Guid AarCallId;
    const string APP_STORE = "https://itunes.apple.com/us/app/clipcall-hire-local-professionals/id1014454741";
    protected void Page_Load(object sender, EventArgs e)
    {
        LandingOnGetAppLink();
        RedirectTo();
    }
    private void RedirectTo()
    {
        if (Request.UserAgent != null)
        {
            string userAgent = Request.UserAgent.ToLower();
            if (userAgent.Contains("android"))
            {
   //             Response.Redirect("http://lets.clipcall.it/132100c7");
                Response.Redirect("https://play.google.com/store/apps/details?id=it.clipcall");
            }
            else
            {
                Response.Redirect("http://lets.clipcall.it/651ff14a");
            }
        }
        else
        {
            Response.Redirect("http://lets.clipcall.it/651ff14a");
        }
    }
    private void LandingOnGetAppLink()
    {
        bool isRobot = false;
        System.Collections.Specialized.NameValueCollection headers = Request.Headers;
        for (int i = 0; i < headers.Count; i++)
        {
            string key = headers.GetKey(i);


            string value = headers.Get(i);
            if (key.ToLower() == "from")
                if (value.ToLower().Contains("googlebot"))
                    isRobot = true;
            if (key.ToLower() == "referer")
                if (value.ToLower().Contains("www.google."))
                    isRobot = true;
        }
        if (Request.UserAgent.ToLower().Contains("googlebot"))
            isRobot = true;
        if (isRobot)
            return;
        if (!Guid.TryParse(Request.QueryString["callid"], out AarCallId))
            return;
        AarService.AarService aarService = WebServiceConfig.GetAarService();
        try
        {
            aarService.LandingOnGetAppLink(AarCallId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
    }
}