﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="callback.aspx.cs" Inherits="tm_callback" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <meta name="robots" content="nofollow"><meta name="googlebot" content="noindex">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
        <script type="text/javascript">
            
            $(function () {
                var _url = '<%# GetCbUrl %>';
                if (_url.length == 0)
                    return;
                $.ajax({
                    url: _url,
                    data: "{}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {

                        
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //              alert(textStatus);
                    },
                    complete: function (jqXHR, textStatus) {
                        //  HasInHeadingRequest = false;
                    }

                });
            })
            
        </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="hf_callid" runat="server" />
		<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <div runat="server" id="_div"></div>
    </div>
    </form>
</body>
</html>
