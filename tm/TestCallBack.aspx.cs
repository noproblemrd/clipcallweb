﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class tm_TestCallBack : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Label1.Text = Request.UserAgent;
        NameValueCollection headers = base.Request.Headers;
        for (int i = 0; i < headers.Count; i++)
        {
            string key = headers.GetKey(i);
            string value = headers.Get(i);
            _div.InnerHtml = key + " = " + value + "<br/>";
           // base.Response.Write(key + " = " + value + "<br/>");
        }
    }
}