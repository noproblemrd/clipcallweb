﻿<%@ WebHandler Language="C#" Class="cb" %>

using System;
using System.Web;

public class cb : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();

        context.Response.ContentType = "application/json";
        Guid AarCallId;
        if (!Guid.TryParse(context.Request.QueryString["callid"], out AarCallId))
            return;
        System.Collections.Specialized.NameValueCollection headers = context.Request.Headers;
        /*
        for (int i = 0; i < headers.Count; i++)
        {
            string key = headers.GetKey(i);
            string value = headers.Get(i);
            _div.InnerHtml = key + " = " + value + "<br/>";
            // base.Response.Write(key + " = " + value + "<br/>");
        }
        */
        string str = context.Request.UserAgent;
        bool isRobot = false;
        //   if (Request.UserAgent.Contains(")
        //    NameValueCollection headers = base.Request.Headers;
        for (int i = 0; i < headers.Count; i++)
        {
            string key = headers.GetKey(i);


            string value = headers.Get(i);
            if (key.ToLower() == "from")
                if (value.ToLower().Contains("googlebot"))
                    isRobot = true;
            if (key.ToLower() == "referer")
                if (value.ToLower().Contains("www.google."))
                    isRobot = true;
            str += key + " = " + value + "; ";
            // base.Response.Write(key + " = " + value + "<br/>");
        }
        /*
        string str2 = string.Empty;
        foreach (string s in context.Request.ServerVariables)
            str2 += s + "=" + context.Request.ServerVariables[2] + "; ";
         * */
        dbug_log.MessageLog("---" + isRobot.ToString().ToUpper() + "---; ", str);
        /*
        AarService.AarLandingPageRequestData _request = new AarService.AarLandingPageRequestData();
        _request.AarCallId = AarCallId;
        _request.Browser = context.Request.Browser.Browser;
        _request.Platform = context.Request.Browser.Platform;
        _request.UserAgent = context.Request.UserAgent;
        _request.IP = Utilities.GetIP(context.Request);
        AarService.AarService _aar = WebServiceConfig.GetAarService();
        try
        {
            _aar.UpdateCameToLandingPage(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }      
       */
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}