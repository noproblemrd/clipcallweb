﻿using System;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using AarService;

public partial class tm_callback1 : PageClipCallLandingPage
{

    
    protected void Page_Load(object sender, EventArgs e)
    {
        Guid _id;
        if (!Guid.TryParse(Request.QueryString["callid"], out _id))
        {
            Response.Redirect(GENERAL_REDIRECT);
            return;
        }
        AarCallId = _id;

      
    }
    protected Guid AarCallId { get; set; }
   
    
    
    protected string GetCbUrl
    {
        get
        {
            return ResolveUrl("~/tm/cb.ashx?callid=" + AarCallId);
        }
    }


    [WebMethod(MessageName = "GetIncidentDetails")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static AarCallIncidentData GetIncidentDetails(Guid callId)
    {
        
        AarService.AarService aarService = WebServiceConfig.GetAarService();
        AarCallIncidentData data = aarService.GetAarCallIncidentData(callId);
        return data;
    }

    [WebMethod(MessageName = "ConnectToCustomer")]
    public static ConnectSupplierToCustomerResponse ConnectToCustomer(Guid callId, Guid incidentId)
    {        
        AarService.AarService aarService = WebServiceConfig.GetAarService();
        ConnectSupplierToCustomerResponse response = aarService.ConnectSupplierToCustomer(callId, incidentId);
        return response;
    }

    [WebMethod(MessageName = "FireEvent")]
    public static void FireEvent(string eventName, Guid callId, Guid incidentId)
    {
        AarService.AarService aarService = WebServiceConfig.GetAarService();
        aarService.FireEvent(eventName,callId, incidentId);
    }


    [WebMethod(MessageName = "ReportLead")]
    public static void ReportLead(Guid callId, Guid incidentId, int reportType, string commentText)
    {
        AarService.AarService aarService = WebServiceConfig.GetAarService();
        aarService.ReportLead(callId, incidentId, reportType, commentText);
    }


    [WebMethod(MessageName = "UpdateRequestMetadata")]
    public static void UpdateRequestMetadata(Guid callId, bool isRobot)
    {
        var context = HttpContext.Current;
        
        AarService.AarLandingPageRequestData request = new AarLandingPageRequestData
        {
            AarCallId = callId,
            Browser = context.Request.Browser.Browser,
            Platform = context.Request.Browser.Platform,
            UserAgent = context.Request.UserAgent,
            IP = Utilities.GetIP(context.Request),
            isRobot = isRobot
        };
        AarService.AarService aarService = WebServiceConfig.GetAarService();
        try
        {
            aarService.UpdateCameToLandingPage(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }      
    }

}