﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="callback1.aspx.cs" Inherits="tm_callback1" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta name="robots" content="nofollow"><meta name="googlebot" content="noindex">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title></title>

    <link href="www/lib/ionic/css/ionic.css" rel="stylesheet">
    <link href="www/css/style.css" rel="stylesheet">
    <link href="www/css/font-awesome.css" rel="stylesheet" />

   <script src="//use.typekit.net/lbv4vuj.js"></script>
   <script>try{Typekit.load({ async: true });}catch(e){}</script>
      <script src="../jwplayer8/jwplayer.js" type="text/javascript"></script>
     <script type="text/javascript">jwplayer.key = "y0rIShXb57vnSkTfeDGVEdJRxWxvMmdar4NZxr6O60U=";</script>
<!--    <script type="text/javascript">jwplayer.key = "YiTTzdj7vuYrnKLzmlEHBtKK4FOLklGGehNIf6rsjWg=";</script>-->
    <!-- IF using Sass (run gulp sass first), then uncomment below and remove the CSS includes above
    <link href="css/ionic.app.css" rel="stylesheet">
    -->
       <script type="text/javascript">
           window.app = {};
           window.app.callId = <%=Guid.Empty == this.AarCallId ? "''" : "'"+this.AarCallId.ToString()+"'" %>;
           window.app.isRobot = <%=IsRobot()%>;
           window.app.useJwPlayer = <%= useJwPlayer()%>;
           window.app.GeneralRedirect = '<%= GENERAL_REDIRECT%>';
    </script>
    <!-- ionic/angularjs js -->
      

    <script src="www/lib/ionic/js/ionic.bundle.min.js"></script>

    <!-- cordova script (this will be a 404 during development) 
    <script src="www/cordova.js"></script>-->

    <!-- your app's js -->
    <script src="www/js/app.js"></script>
    <script src="www/js/services.js"></script>
    <script src="www/js/controllers.js"></script>
  </head>
  <body ng-app="aarApp">     
    <ion-nav-view></ion-nav-view>
       <script type="text/javascript">
           (function (d, s, i, r) {
               if (d.getElementById(i)) { return; }
               var n = d.createElement(s), e = d.getElementsByTagName(s)[0];
               n.id = i; n.src = '//js.hs-analytics.net/analytics/' + (Math.ceil(new Date() / r) * r) + '/2156669.js';
               e.parentNode.insertBefore(n, e);
           })(document, "script", "hs-analytics", 300000);
    </script>
  </body>
</html>






<%--<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link href="aar.css" rel="stylesheet" />
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-route.js"></script>
    <script type="text/javascript">
        window.app = {};
        window.app.callId = <%=Guid.Empty == this.AarCallId ? "''" : "'"+this.AarCallId.ToString()+"'" %>;
    </script>
    <title></title>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
        <script type="text/javascript">
            $(function () {
                var _url = '<%# GetCbUrl %>';
                if (_url.length == 0)
                    return;
                $.ajax({
                    url: _url,
                    data: "{}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {

                        
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //              alert(textStatus);
                    },
                    complete: function (jqXHR, textStatus) {
                        //  HasInHeadingRequest = false;
                    }

                });
            })
        </script>
</head>
<body ng-app="aarApp">    
    <div class="container-fluid">
        <img src="logo.png" class="img-responsive center-block" />
        <div ng-view></div>

    </div>
    <script src="app.js"></script>
</body>
</html>

--%>
