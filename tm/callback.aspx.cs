﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

public partial class tm_callback : System.Web.UI.Page
{
    Guid AarCallId;
    protected void Page_Load(object sender, EventArgs e)
    {
     //   Label1.Text = Request.UserAgent;
       // Response.Redirect("http://10.0.0.107:8088/tm/TestCallBack.aspx" + Request.Url.Query);
        
	   Label1.Text = Request.UserAgent;
       

        if (!Guid.TryParse(Request.QueryString["callid"], out AarCallId))
            return;
        if (Request.UserAgent == "bitlybot")
            return;
        /*
        AarService.AarLandingPageRequestData _request = new AarService.AarLandingPageRequestData();
        _request.AarCallId = AarCallId;
        _request.Browser = Request.Browser.Browser;
        _request.Platform = Request.Browser.Platform;
        _request.UserAgent = Request.UserAgent;
        AarService.AarService _aar = WebServiceConfig.GetAarService();
        try
        {
            _aar.UpdateCameToLandingPage(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
         * */
        Header.DataBind();
       

    }
    protected string GetCbUrl
    {
        get 
        {
            return ResolveUrl("~/tm/cb.ashx?callid=" + AarCallId);
        }
    }
}