<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Management/MasterPagePreAdvertiser.master" CodeFile="ResetPassword.aspx.cs" Inherits="Management_ResetPassword" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
 <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" >
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>

<div id="primarycontent">	
	<div class="form-login">
		<h2><asp:Label ID="lbl_title" runat="server" TabIndex="4">Forgot password / Reset Password</asp:Label></h2>	
		<div class="login">
        <div class="div_ResetPasswordComment">
		<asp:Label runat="server" ID="lblComment" Font-Size="14pt" ForeColor="Red" Font-Bold="True" TabIndex="4" CssClass="remember-comment"></asp:Label>
		    <div class="form-field-wrap">
		        <div class="form-field">
		            <label for="<%= txt_professionalPhone.ClientID %>" runat="server" id="lbl_phone">Phone number</label>
		            <asp:TextBox runat="server" CssClass="form-text" id="txt_professionalPhone" textmode="SingleLine" autocomplete="off"></asp:TextBox>
		        </div>
		        <div class="error-msg-wrap">
		            <a href="" class="q" title="What is this?" tabindex="3"></a>
					<asp:RequiredFieldValidator ID="RequiredFieldValidatorPhone" CssClass="error-msg" Display="Dynamic" runat="server" ErrorMessage="Missing" ControlToValidate="txt_professionalPhone"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" CssClass="error-msg" runat="server" Display="Dynamic" ErrorMessage="Invalid phone number" ControlToValidate="txt_professionalPhone" ></asp:RegularExpressionValidator>
		        </div>
            </div>
            <div class="form-field-wrap">
		        <div class="form-field">
		            <label for="<%= txt_email.ClientID %>" runat="server" id="lbl_email">Email</label>
		            <asp:TextBox runat="server" CssClass="form-text" id="txt_email" textmode="SingleLine" autocomplete="off"></asp:TextBox>
		        </div>
		        <div class="error-msg-wrap">
		            <a href="" class="q" title="What is this?" tabindex="3"></a>
					<asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" CssClass="error-msg" Display="Dynamic" runat="server" ErrorMessage="Missing" ControlToValidate="txt_email"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" 
                        CssClass="error-msg" runat="server" Display="Dynamic" 
                        ErrorMessage="Invalid Email" ControlToValidate="txt_email" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ></asp:RegularExpressionValidator>
		            
		        </div>
		        </div>
                <div class="capcha">
                    <telerik:RadCaptcha ID="_RadCaptcha" Runat="server" EnableRefreshImage="true" CaptchaImage-ImageCssClass="captchapict"  CaptchaTextBoxCssClass="form-textcaptcha" CaptchaTextBoxLabelCssClass="captchalabel">
                    </telerik:RadCaptcha>
                    </div>
		    
			<asp:Button  runat="server" Text="Send password" id="btnSendPassword" CssClass="form-submit" OnClick="btnCheckCode_Click" TabIndex="2"  />
            </div>
		</div>	
	</div>
</div>
	        
<asp:Label ID="lbl_PasswordSentOk" runat="server" Visible="false" Text="The new password was sent to your mobile"></asp:Label>
<asp:Label ID="lbl_Failed" runat="server" Visible="false" Text="The process failed, try again"></asp:Label>	
<asp:Label ID="lbl_MissingCaptcha" runat="server" Visible="false" Text="Incorrect access code. Please try again"></asp:Label>	
<asp:Label ID="lbl_RefreshImage" runat="server" Visible="false" Text="Refresh image"></asp:Label>
<asp:Label ID="lbl_AccessCode" runat="server" Visible="false" Text="Enter access code"></asp:Label>
</asp:Content>
