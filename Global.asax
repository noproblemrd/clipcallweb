﻿<%@ Application Language="C#" %>
<script runat="server">
    const string SCRAMBEL = "*5106sp";
    private int online = 30000;
    static PhoneDidJob phoneDidJob;
    static PhoneDidJob supplierAvailble;
    void Application_Start(object sender, EventArgs e) 
    {
                
        // Code that runs on application startup
        log4net.Config.XmlConfigurator.Configure();
        dbug_log.MessageLog("Application Start: " + DateTime.Now.ToString());
        PpcSite.SetCachOnAppStart();
        Application["online"] = online;
        Application["TimerRunning"] = false;
        /*
        phoneDidJob = new PhoneDidJob();
        phoneDidJob.StartPhoneDidJobTimer();
        supplierAvailble = new PhoneDidJob();
        supplierAvailble.StartSupplierAvailbleTimer();
        GC.Collect();
         * */
        /* Remove unnecessary (NoProblem) jobs 16-05-2017
        PhoneDidScheduleJob phoneDidJob = new PhoneDidScheduleJob();
        phoneDidJob.Start();
        SupplierAvailableScheduleJob supplierScheduleJob = new SupplierAvailableScheduleJob();
        supplierScheduleJob.Start();
         */
    //    DataExposure.SetExposureCach("Exposure", null, CacheItemRemovedReason.DependencyChanged);
    }            
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

 //       DataExposure.FlushTable();
        dbug_log.MessageLog("Application End: " + DateTime.Now.ToString());        
    }
       
    void Application_Error(object sender, EventArgs e) 
    {         
        Exception ex = Server.GetLastError();       
        dbug_log.Global_Exception(ex, Server, Context );             
    }
    
     
    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        Application.Lock();
        Application["online"] = Convert.ToInt32(Application["online"]) + 1;
        Application.UnLock();
    }
    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        
        Application.Lock();
        Application["online"] = Convert.ToInt32(Application["online"]) - 1;
        Application.UnLock();
    }
    
    void Application_AcquireRequestState(object sender, EventArgs e) 
    {
        
    }     
    
      
    
    
    void Application_BeginRequest(object sender, EventArgs e)
    {
        /*
        var host = Request.Url.Host.ToString().ToLower();

        
        if (host == "www.noproblemppc.com" || host == "noproblemppc.com")
        //if (host=="localhost")
        {
            Response.Clear();
            Response.Status = "301 Moved Permanently";
            string newHost;
            newHost = "www.noproblem.me";
            
            string newLocation;
            newLocation = Request.Url.ToString().ToLower().Replace(Request.Url.Host.ToString(), newHost);

            Response.AddHeader("Location", newLocation);
        }
         * */

    } 
      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
</script>
