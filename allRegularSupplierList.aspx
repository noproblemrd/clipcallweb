<%@ Page Language="C#" AutoEventWireup="true" CodeFile="allRegularSupplierList.aspx.cs" Inherits="allRegularSupplierList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="siteStyle.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript">
     function setIframeSize()
    {    
           
        var newheight;
        var newwidth;
        var obj;
        
        
        obj=parent.document.getElementById("iframeNoProblem");
        
        
        try
        {
            if(document.getElementById)
            {

                if(obj.contentDocument)
                {                 
                    // Firefox, Opera
                    newheight=obj.contentDocument.body.scrollHeight; 
                    
                    //newwidth=obj.contentDocument.body.scrollWidth; doesn't work why?                    
                    newwidth=obj.contentWindow.document.documentElement.scrollWidth;
                    //alert("newwidth:" + newwidth);
                    
                }
                
                else if(obj.contentWindow) 
                {
                    // Internet Explorer 
                    newheight=obj.contentWindow.document.body.scrollHeight; 
                    //alert(newheight);
                    newwidth=obj.contentWindow.document.body.scrollWidth;
                    
                    //alert(newwidth);
                }
                
                else if(obj.document) 
                {
                    // Others? 
                    newheight=obj.document.body.scrollHeight; 
                    newwidth=obj.document.body.scrollWidth;
                }
            }            
             
        }   
    
        catch(err)
        {

            alert(err.message);

        }       
                         
          
        obj.style.height=newheight + 'px';        
        obj.style.width=newwidth + 'px'; 
        
        //alert(obj.style.width + " " + obj.style.height );  
       
    }   
    
    function showRegulrSuppliersList()
    {           
        var url; 
          
        url="http://" + location.hostname + ":" + location.port + "<%=root%>suppliersIframe.aspx?SiteId=<%=SiteId%>&ExpertiseCode=<%=ExpertiseCode%>&ExpertiseLevel=<%=ExpertiseLevel%>&RegionCode=<%=RegionCode%>&RegionLevel=<%=RegionLevel%>";

       
        //alert(url);
        var objHttp;
        var ua = navigator.userAgent.toLowerCase(); 
        
        if(ua.indexOf("msie")!= -1)
        {
           // alert("msie");
            objHttp =  new ActiveXObject("Msxml2.XMLHTTP");
            //objHttp = new ActiveXObject("Microsoft.XMLHTTP");
            //objHttp=new XMLHttpRequest();
            //objXmlDoc = new ActiveXObject("Msxml2.DOMDocument");
        }
        else
        {
            //alert("not msie");
            objHttp=new XMLHttpRequest();
        }
        
        if (objHttp == null)
        {
            //alert("Unable to create DOM document!");
            return;
        } 	        
        
        
        objHttp.open("GET", url, true);	       
        
        objHttp.onreadystatechange=function()
        {	  
            //alert("objHttp.readyState "+objHttp.readyState);          
            if (objHttp.readyState==4)
            {   
                //alert(objHttp.responseText);       
                document.getElementById("divIframe").innerHTML=objHttp.responseText;        
                setIframeSize();      
            }
         }
         
        objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        //objHttp.setRequestHeader("Content-Length", params.length);
        //objHttp.setRequestHeader("Connection", "close");
        //objHttp.send(soap);
        objHttp.send(null);
    }
</script>

</head>
<body onload="showRegulrSuppliersList();">
    <form id="form1" runat="server">
    <div id="divIframe"></div>
    
    </form>
</body>
</html>
