﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPAbout.master" AutoEventWireup="true" CodeFile="about-managment.aspx.cs" Inherits="about_managment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<div id="wrapadv">

  <div class="aboutmanagmentleft">
  <h5>Daniel Shaked - CEO & Founder</h5>
<p>Daniel founded No Problem in 2006 with a vision of it becoming a global company providing a Web traffic monetization Software platform for directory publishers. Under his leadership No Problem ISRAEL grew from concept to become a successful Pay per Call local publisher utilizing the No Problem unique technology platform.  Prior to founding No Problem, Daniel served as VP Operations at SmarTeam LTd, acquired by Dassault Systems (NASDAQ: DASTY).</p>
<p>Daniel served for 12 years as an officer in the Israel Defense Forces and currently holds the rank of LTC in the IDF Active Reserves.  Daniel holds a bachelor degree in Mechanical Engineering and MBA.</p>
  <p><p>&nbsp; </p> 

  <h5>Amichai Zuntz - EVP Business Development</h5>
<p>Amichai brings over 10 years of experience in sales and business management to No Problem. Prior to joining No Problem, Amichai served as VP Sales at WebsPlanet, The leading provider of websites solutions where he was in charge of the worldwide sales of the company, Amichai also served as VP Sales at PLYmedia, a company that specializes in online video enrichment and video affiliation network.</p>
<p>Prior to that, Amichai served as Director of Sales and Services business at Celtro, a cellular optimization company, responsible for the promotion of Celtro’s activities in Africa. Amichai also served as manager of the Services Business unit at Verint (NASDAQ: VRNT) where he marketed and negotiated terms and conditions of support programs to worldwide customers.</p>
<p>Amichai holds a B.A. from Bar-Ilan University, Israel in Economics and Industrial Engineering.</p>
<p>&nbsp; </p> 

<h5>Shai Peretz - VP R&D</h5> 
<p>
Shai Perez brings more than 14 years of results-oriented visionary with unique background in software engineering, strong strategic and long range planning abilities
expertise in Internet technologies, ecommerce, billing, enterprise software, system integration and IT
infrastructure

Prior to joining NO PROBLEM, Shai owned software and professional services company combining
core values of innovative and standards-based technology.
Pioneer in bi-directional media streaming for large scale over the web

Prior to that ,Shai worked for several years as an Information System Manager at Dassualt Systems and at Emblaze Systems, responsible for all company's Information Technology

Shai holds BSC in Software Engineering with honors from Tel Aviv University

</p>


  </div>
  
  <div class="aboutmanagmentright">
  

<h5>Ido Dan - Chief Experience Officer</h5>
<p>Ido is a seasoned User Experience executive and entrepreneur with over 16 years of experience. Prior to jointing No Problem he co-founded two startups, and held key roles with consumer companies, including eBay, inc. (NASDAQ: EBAY) where he was a senior User Interface designer, and worked with multidisciplinary teams on online projects and design strategies. Prior to joining eBay, Ido co-founded Contact Networks, Inc. and served as the director of the UI group of the automatically-updating platform company. He served as an officer in IDF, and retired as a Captain. Ido also worked for the McCann-Erickson advertising agency, where he led the early multimedia department. He has multiple patents pending for his desktop UI solutions.</p>
<p>&nbsp; </p>

<h5>Einat Domb-Har – CFO</h5>
<p>Einat has 17 years of private funding, IPO, M&A and financial management experience.</p>
<p>Prior to joining No Problem, Einat was the CFO of Olista Corporation, a telecom start-up that was acquired by Connectiva Systems. Einat was also the CFO of the venture capital fund, StageOne, and of Lab-One Innovations. In addition, Einat was Controller at Pitango, Israel largest venture capital fund, and was a Senior Manager at E&Y Israel.  Presently, Einat is a Director of the public company, Silicom.</p>
<p>Einat is also a professional coach and provides business and financial consultation to entrepreneurs and small business.</p>
<p>Einat is a CPA and mediator and graduated with a B.A in Economics and Accounting from Tel-Aviv University and an M.B.A, specializing in Finance, from Bradford University. </p>
<p>&nbsp; </p>

<h5>Ron Aharoni – VP Product and Delivery</h5>
<p>Ron has joined No Problem after many years as a senior project Executive both in Matrix, a leading Software solution provider & in ITNavigator.</p>
<p>As a senior executive Ron has lead, monitored & controlled dozens of complex and high scale successful implementations to the satisfaction of his enterprise customers. </p>
<p>Ron holds a Bsc. in Management & Industrial engineering with honors from the Open University.</p>

<p>&nbsp; </p>

<h5>Yosh Sagie - SVP Global Sales</h5>
<p>Yosh has over 35 years of engineering and marketing experience with various hi-tech companies and industries, Eleven of them in the Yellow Pages industry.</p>
<p>For 5 years Yosh successfully led  Amdocs’ vision, strategy and World Wide Marketing departments.</p>
<p>Prior joining No Problem Yosh consulted a number of leading Yellow Pages solution providers helping them to establish a strong hold in the Yellow Pages Directory Industry.</p>
<p>Yosh holds a Bsc. and Msc. with honor in System and Industrial Engineering from University of Arizona.</p>

  </div>

</div> <!-- End wrapadv -->

</asp:Content>

