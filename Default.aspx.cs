using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Url.Host == "www.noproblemppc.com" || Request.Url.Host == "noproblemppc.com" || Request.Url.Host == "noproblem.me" || Request.Url.Host == "www.noproblem.me")
            Response.Redirect("main.aspx", false);
        else
            Response.Redirect("publisher/publisherlogin.aspx", false);
    }
}
