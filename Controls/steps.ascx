﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="steps.ascx.cs" Inherits="Controls_steps" %>

<script type="text/javascript">
    var tasksLeftGeneral = "<%=countNotDone%>";
</script>

<div id="closeStep" class="closeStep" onclick="closeSteps('<%=bonusAmount%>')"></div>

<div class="title titleGetStarted" onclick="closeSteps('<%=bonusAmount%>')">
    Get Started
</div>

<div id="descriptionStep" class="descriptionStep">
    <%if (countNotDone > 0) %>
    <%{ %>
        You are only <span id="spanTaskLeft"><%=countNotDoneStep%></span> away from getting $<%=bonusAmount%> worth of NoProblem credit
    <%} %>
</div>

<div>
     <div id="circleStepRegister"  runat="server">
         <div id="circleStepNumberRegister" class="circleStepNumber" runat="server">1</div>
     </div>
     <div id="stepRegister" runat="server"></div>
</div>

<div class="clear"></div>



<div class="stepVideo" >
    <div id="circleStepVideo"  runat="server">
         <div id="circleStepNumberVideo" class="circleStepNumber" runat="server">2</div>
     </div>

     <div id="stepVideo" runat="server" ></div>
   
     
     <div id="video_container" style="height:auto"></div>
     
     <script type="text/javascript" src="../management/swfobject.js"></script>
     
     <script type="text/javascript">

         // https://developers.google.com/youtube/js_api_reference

         var bonusAmmount = "<%=bonusAmount%>";

         var params = { allowScriptAccess: "always" };
         var atts = { id: "myytplayer" };

         function showVideo() {           
            
                 if (document.getElementById("stepVideoClose").style.display != 'block') {
                     document.getElementById("stepVideoClose").style.display = 'block';

                     
                         var c = document.getElementById("ytapiplayer");

                         if (!c) {
                             var d = document.createElement("div");
                             d.setAttribute("id", "ytapiplayer");
                             document.getElementById("video_container").appendChild(d);
                         }

                         /*
                         swfobject.embedSWF("http://www.youtube.com/v/2lnZoIF2sVE?enablejsapi=1&playerapiid=ytplayer&version=3&autoplay=1&autohide=1&controls=1;",
                           "ytapiplayer", "637", "359", "8", null, null, params, atts);
                         */

                         swfobject.embedSWF("http://www.youtube.com/v/97q0JRDvuik?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1;",
                           "ytapiplayer", "637", "358", "8", null, null, params, atts);

                         //alert(p);
                         //http://www.youtube.com/embed/2lnZoIF2sVE?enablejsapi=1&playerapiid=ytplayer&version=3&autoplay=1 // see it's defferent from the formal whrer the link is
                        
                     }                 
             
         }

         function onYouTubePlayerReady(playerId) {
             //alert("onit");
             ytplayer = document.getElementById("myytplayer");
             ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
         }

         function onytplayerStateChange(newState) {

             //Possible values are unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5)
             if (newState == 0) {
                 //alert("end <%=supplierGuid%> <%=siteId%>");
                 setStep(2, '<%=supplierGuid%>', '<%=siteId%>');
             }
         }
         
</script>

</div>



<div class="clear"></div>

<div>
     <div id="circleStepWorkHours"  runat="server">
         <div id="circleStepNumberWorkHours" class="circleStepNumber" runat="server">3</div>
     </div>
     <div id="stepWorkHours" runat="server"></div>      
</div>

<div class="clear"></div>


<div class="stepHonor">
     <div id="circleStepHonor"  runat="server">
         <div id="circleStepNumberHonor" class="circleStepNumber" runat="server">4</div>
     </div>
     <div id="stepHonor" runat="server"></div>     
</div>

<div class="clear"></div>


<div>
     <div id="circleStepTestCall"  runat="server">
         <div id="circleStepNumberTestCall" class="circleStepNumber" runat="server">5</div>
     </div>
     <div id="stepTestCall" runat="server"></div>   
</div>

<div class="clear"></div>


<div>
     <div id="circleStepListen"  runat="server">
         <div id="circleStepNumberListen" class="circleStepNumber" runat="server">6</div>
     </div>
     <div id="stepListen" runat="server"></div>   
</div>

<div class="clear"></div>


 <!--
<asp:DataList runat="server" ID="dataListTaskStatus" 
    onitemdatabound="dataListTaskStatus_ItemDataBound">
    <ItemTemplate>
        <div id="circleStep" runat="server">
            <div id="circleStepNumber" class="circleStepNumber" runat="server"></div>
        </div>
        <div id="step" runat="server"><%# DataBinder.Eval(Container.DataItem, "Description")%></div>
            
    </ItemTemplate>                
</asp:DataList>
-->

    <script type="text/javascript">
        var ifShowToEndVideo = false;

        function closeVideo() {        
            
            swfobject.removeSWF('myytplayer');
            document.getElementById('stepVideoClose').style.display = 'none';            
            
        }

        function setStep(_step, _supplierGuid, _siteId) {
            //alert("setStep");
            var params;
            params = "step=" + _step + "&supplierGuid=" + _supplierGuid + "&siteId=" + _siteId;

            //alert(params);
            var url;

            url = "setStep.ashx";

            var objHttp;
            var ua = navigator.userAgent.toLowerCase();

            if (ua.indexOf("msie") != -1) {
                objHttp = new ActiveXObject("Msxml2.XMLHTTP");

            }
            else {
                objHttp = new XMLHttpRequest();
            }



            if (objHttp == null) {
                //alert("Unable to create DOM document!");
                return;
            }


            objHttp.open("POST", url, true); // true if you want the request to be asynchronous and false if it should be a synchronous request

            objHttp.onreadystatechange = function () {

                if (objHttp.readyState == 4) {
                    //alert("responseText " + objHttp.responseText);

                    resultSplit = objHttp.responseText.split(',');
                    var result = resultSplit[0];
                    var ifAllTasksDone = resultSplit[1].toLowerCase();
                    var tasksLeft = resultSplit[2].toLowerCase();

                    if (result == "unsuccess") {
                        // ALSO GOES HERE IF TRY THE SAME STEP THAT WAS DONE BEFORE

                    }

                    else {
                        //alert("success");
                        ifShowToEndVideo = true;
                        document.getElementById("<%=circleStepVideo.ClientID%>").className = 'stepDoneCircle';
                        document.getElementById("<%=stepVideo.ClientID%>").className = 'stepDone';
                        closeVideo();
                        updateTaskLeft(tasksLeft);
                        tasksLeftGeneral = tasksLeft;                       
                        if (ifAllTasksDone == "true")
                            allTasksDone();
                    }
                }

            }


            objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            objHttp.setRequestHeader("Content-Length", params.length);
            objHttp.setRequestHeader("Connection", "close");

            objHttp.send(params);


            return true;

        }

        //setStep();
    </script>


