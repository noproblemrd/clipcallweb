﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MapControl.ascx.cs" Inherits="Controls_MapControl" %>
<script type="text/javascript">
    var _navigator = navigator.appVersion.toLowerCase();

    if (document.addEventListener) {
        document.addEventListener("DOMContentLoaded", SetDetailsLocationOnload, false); 
    }
    else if(window.attachEvent) {
        window.attachEvent('load', SetDetailsLocationOnload);           
    }
    function SetDetailsLocationOnload() {
        location_place_holder = "<%# LocationPlaceHplder %>";
        var searchTextField = document.getElementById("searchTextField");
   //     $(searchTextField).attr("place_holder", location_place_holder);
        if (searchTextField.addEventListener) {
            searchTextField.addEventListener("blur", ChkLocationOnBlur, false);
            searchTextField.addEventListener("focus", ChkLocationOnFocus, false);
        }
        else if (searchTextField.attachEvent) {
            searchTextField.attachEvent("onblur", ChkLocationOnBlur);
            searchTextField.attachEvent("onfocus", ChkLocationOnFocus);
        }
    }
    function _SetServerComment(_comment) {

        if (top.SetServerCommentMaster)
            top.SetServerCommentMaster(_comment);
        else {
            SetServerComment(_comment);
            window.scrollTo(0, 0);
        }
    }

    function SetFullAddress(_address) {
        var searchTextField = document.getElementById('searchTextField');
        searchTextField.value = _address;
        RemoveCssClass(searchTextField, "place-holder");
        AddCssClass(searchTextField, "textValid");
        full_name = _address;
        document.getElementById("<%#  hf_FullNameAddress.ClientID %>").value = _address + ";;;" + _address;
    }
    function SetMapOnLoad(radius, _lat, _lng) {
        lat_lng = new google.maps.LatLng(_lat, _lng);
        _radius = parseFloat(radius);
        _radius = radius;
        SetAnchorStyle();
        SetMap();
    }
    var _zoom; // = 13;
    var _radius = 30;
    var lat_lng;
    var full_name;
    function initialize() {
        var input = document.getElementById('searchTextField');
       
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            //          var test = this;
            var place = autocomplete.getPlace();
            //    var latLng = new google.maps.LatLng(-87.7506440644, 41.8834526307);
            full_name = place.formatted_address;
            if (!place.geometry)
                return;
            lat_lng = place.geometry.location;
            document.getElementById("<%#  hf_FullNameAddress.ClientID %>").value = place.formatted_address + ";;;" + input.value;
            document.getElementById("div_LocationError").style.display = "none";
            document.getElementById("div_LocationSmile").style.display = "block";

            SlideElement("div_LocationComment", false); // document.getElementById("div_LocationComment").style.display = "none";
            RemoveCssClass(input, "textError");
            AddCssClass(input, "textValid");
            RemoveServerComment();

            //Set default values of map
            _radius = 30;
            document.getElementById("<%# div_places.ClientID %>").style.display = "none";
            RemoveSelectLargeAreas();
            SetAnchorStyle();

            SetMap();

        });
        $('#menu_radius').find('a').each(function () {
            $(this).click(function () {
                
                var rds = this.getAttribute("val");
                if (isNaN(rds))
                    SetAnchorAllClick(this);
                else {
                    $('#<%# div_places.ClientID %>').hide();
                    _radius = parseFloat(rds);
                    RemoveSelectLargeAreas();
                    SetAnchorStyle();
                }

                SetMapZoom();
            });
        });
        /*
        var _ul = document.getElementById("menu_radius");
        var a_arr = _ul.getElementsByTagName("a");
        for (var i = 0; i < a_arr.length; i++) {
            google.maps.event.addDomListener(a_arr[i], 'click', function () {
                //          alert(this.innerHTML)
                document.getElementById("<# div_places.ClientID %>").style.display = "none";
                var rds = this.getAttribute("val");
                if (isNaN(rds)) {
                    if (!SetAreas(this))
                        return;
                }
                else {
                    _radius = parseFloat(rds);
                    RemoveSelectLargeAreas();
                    SetAnchorStyle();
                }
                
                SetMapZoom();

                
                //          SetMap();
            });
        }
        */
        //  lat_lng = new google.maps.LatLng(40.8142509259259, -73.9374254171401);
        //   SetMap();
    }
    function SetAnchorAllClick(elem) {
        SetAreas(elem);
        $(elem).unbind('click');
    }
    function SetAnchorStyle() {
        var _ul = document.getElementById("menu_radius");
        var a_arr = _ul.getElementsByTagName("a");
        for (var j = 0; j < a_arr.length; j++) {
            var ValText = a_arr[j].getAttribute("val");
            if (isNaN(ValText)) {
          //      var FirstClass = (_radius == 35) ? "btnMilesSelected" : "btnMiles";
                //      a_arr[j].parentNode.className = FirstClass + " btnMilesLast";
                if (_radius > 30)
                    AddCssClass(a_arr[j], "Selected");
                else
                    RemoveCssClass(a_arr[j], "Selected");
                continue;
            }
            var _val = parseFloat(ValText);
            if (_val <= _radius && _radius < 35)
            //AddCssClass(a_arr[j].parentNode, "btnMilesBackgroundSelected");
                a_arr[j].className = "Selected";
            else
            //             RemoveCssClass(a_arr[j].parentNode, "btnMilesBackgroundSelected");
              RemoveCssClass(a_arr[j], "Selected");
        }
    }

    // Register an event listener to fire when the page finishes loading.
    google.maps.event.addDomListener(window, 'load', initialize);

    /*
    function SetNeighborhood(lat, lng) {
    var params = "lat=" + lat + "&lng=" + lng + "&radius=" + _radius;
    var url = "<# GetNeighborhood %>";
    CallWebService(params, url, function (arg) {
    document.getElementById("lbl_result").innerHTML = arg;
    },
    function () { alert('error'); });
    }
    */
    var map;
    var marker;
    var circle;
    var geocoder = new google.maps.Geocoder();
    function SetMap() {
        SetZoom();
        document.getElementById("div_map").style.display = "block";
        var options = {
            'zoom': _zoom,
            'scrollwheel': false,
            'center': lat_lng,
            'draggable': false,
            //    'maxZoom': _zoom,
            //    'minZoom': _zoom,
            'mapTypeId': google.maps.MapTypeId.ROADMAP
        };
        //       SetNeighborhood(lat_lng.lat(), lat_lng.lng());
        map = new google.maps.Map(document.getElementById('map_canvas'),
            options);

        // Create a draggable marker which will later on be binded to a
        // Circle overlay.
        //   map.addControl(new google.maps.LocalSearch());

        marker = new google.maps.Marker({
            map: map,
            position: lat_lng, // place.geometry.location, //new google.maps.LatLng(55, 0),
            draggable: false,
            title: 'My business location'
        });

        // Add a Circle overlay to the map.
        if (_radius <= 30) {
            circle = new google.maps.Circle({
                map: map,
                radius: _radius * 1000 * 1.6 // 1.6km = 1mile 
            });
        }
        else {
            circle = new google.maps.Circle();
            circle.setMap(null);
        }

        // Since Circle and Marker both extend MVCObject, you can bind them
        // together using MVCObject's bindTo() method.  Here, we're binding
        // the Circle's center to the Marker's position.
        // http://code.google.com/apis/maps/documentation/v3/reference.html#MVCObject
        circle.bindTo('center', marker, 'position');
        /**********  Bound zip code area  *************/
        /*
        var encodedPoints = "e|uxFdmdbM_XcGScBjMgm@hGo[tE_WrDgOz@kHz@_D~Hw`@fJgc@vLon@RwBvBwLrDoPjHo_@jCkMvBkMf@oAz@gEz@kHiIqJuWiIz@wBgTsNkCcBRsISwBzOrNz@wBnAcGf@oAwBcBz@{JvBsIbBkHz@cBjCSvBsSbGwBjM_DrNwB~McBvGwBz@{EzJ_g@zEoU~C_Sf@ScGsDrDgTnFoZz@sI~Hod@vBcQbBgJfO{|@pMoy@vNzF`z@jCdaAnSdZoCrMuSvZcObPObM^rTqE`GpG`D`GqFpHeXtPg[rPe]qHwZbPn@pHdTtT`YhMgH`m@hPxc@pgAhPhXiAtb@wSri@am@lQ{k@h`CybBiAi`@oa@io@OKxsBgrBz@cBzJoPnTu`@fFeL~Uac@jRc^zBoD`GwL|Oi\\~@}@nPiZfGeLpDwGfRw^dKuOjHgOlAmB~@mBpb@oDhJQfJa@dLy@dJ?j@\\xHjDrD`DvBdApAr@~ClCvLvG`Bx@jMbIrEfCvDzBlChQI|E`AdMpHli@H~@x@~Ch@jCPlCh@tCR~BlAnHPv@h@fBd@Ph@pDlFlMjHgCbGeB`NiCld@kI`DyAnA?bUX~NaAbB?fGQvEi@nTjBnB`@zc@Mh\\c@tIwCxE`B|GHjAChOLxE~@jR|Gd@?zO|@|Fb@fLn@`D?`Db@xBRvJmDbQBjNgGd^eEjImAdFvSl@tKfAnCpAdSvBhGdK|_@zEg@bOjCfCNpb@uDjIcAt@lMoLgGyBjD~AzLhTfOhn@h{BzQ~@xQ}@v`@_DzAyKdTeXu@uNxIwM_DeG}HyYIsIr@cBtAyG`@e@hEkC`LgGxBXpKcL`@{EfJ{KfJ{AvUjDxC}@zG`A|IcI~C{@Idg@MtFvGnSbU|[`IdKiYcEe@zeCje@lfEhm@hkD~dBbuGdaBpzIe~@Ic^{Ia\\nQwVlv@a\\ja@xQ`aGu@|aA{Lpj@oIvQwU`Eaa@wb@ek@ob@u^pIm^vn@wU`hCet@bi@miArPmkAoXwy@ef@kmAeuAmzBtLoU~T_\\fDsg@kReKeSsf@qIm_@~OiNj^koBmTmuDyl@wj@w[{cJ{_Gwe@s]{bCox@}_Asb@{dBw~@cm@g\\{m@oZktCcy@";
        var  _points = [];
        var decodedPoints = google.maps.geometry.encoding.decodePath(encodedPoints);
        for (var i = 0; i < decodedPoints.length; i++) {
        _points.push(new google.maps.LatLng(decodedPoints[i].lat(), decodedPoints[i].lng()));
        }
            
        var polyYea = new google.maps.Polygon({
        paths: _points,
        fillColor: '#444444',
        fillOpacity: 0.35
        });
        polyYea.setMap(map);
        
        var encodedPoints2 = "_gxwFlsacMoAgEsIcVvG_N_DgJ{O{@kRoPrv@ce@~HsNnFoP~C_NrIoAbQcLz@oAvBsIz@oF~HrD~MnFzEzEjCnAbBnAnK~CzESzEnFjCbBfEg@vBz@~H{Ef@g@R{@oAoKSgJzEgTd^c|AbQpCjoBlTeGlvAj|A`nAreAzsBl`AbmAuh@f_BwG~CkCf@{OzEgOfEg@RsNvBwLf@{J~Cg@?aFhLkA~De@TuYxu@uKaJol@tY_s@}qAgm@k\\{EcBsl@wVcBoAgT{^sIce@cBwGsNk\\{Y{J_SzTcGoFcBsIkMrDgTsI{Tgc@";
        var _points2 = [];
        var decodedPoints2 = google.maps.geometry.encoding.decodePath(encodedPoints2);
        for (var i = 0; i < decodedPoints2.length; i++) {
        _points2.push(new google.maps.LatLng(decodedPoints2[i].lat(), decodedPoints2[i].lng()));
        }
             
        var polyYea2 = new google.maps.Polygon({
        paths: _points2,
        fillColor: '#444444',
        fillOpacity: 0.35
        });
        polyYea2.setMap(map);
        */
    }
    function SetMapZoom() {
        SetZoom();
        map.setZoom(_zoom);


        // Add a Circle overlay to the map.
        if (_radius <= 30) {
            circle.setRadius(_radius * 1000 * 1.6);
            circle.setMap(map);
        }
        else             
            circle.setMap(null);
        
        
    }
    function SetZoom() {
        if (_radius < 3)
            _zoom = 13;
        else if (_radius < 5)
            _zoom = 12;
        else if (_radius < 10)
            _zoom = 11;
        else if (_radius < 20)
            _zoom = 10;
        else if (_radius <= 30)
            _zoom = 9;
        else if (_radius > 30)
            _zoom = 8;

    }

    function SetPlaces(_id, IsChoosen) {
        var hid_places = document.getElementById('hid_places');
        if (IsChoosen)
            hid_places.value += _id + ',';
        else
            hid_places.value = hid_places.value.replace(_id + ',', '');
    }
    
    /*location*/
    var location_place_holder;
    function ChkLocationOnFocus() {
        var callerElement;
        if (this == window)
            callerElement = window.event.srcElement;
        else
            callerElement = this;// window.event.target || window.event.srcElement;
        Text_Focus(callerElement, location_place_holder);
        RemoveCssClass(callerElement, "textValid");
    }
    function ChkLocationOnBlur() {
        var callerElement;
        if (this == window) {
            callerElement = window.event.srcElement;
            if (callerElement.value == "Enter a location")
                callerElement.value = "";
        }
        else
            callerElement = this;
        RemoveCssClass(callerElement, "textError");
        var SourceVal = document.getElementById("<%# hf_FullNameAddress.ClientID %>").value.split(';;;');
        var _location = callerElement.value;// GetText_PlaceHolder(callerElement, location_place_holder);
        if (_location.length == 0) {
            RemoveCssClass(callerElement, "textValid");
            document.getElementById("div_LocationSmile").style.display = "none";
            document.getElementById("div_LocationError").style.display = "none";
            SlideElement("div_LocationComment", false);
            full_name = "";
            document.getElementById("<%# hf_FullNameAddress.ClientID %>").value == "";
            $('#map_canvas').empty();
            $('#div_map').hide();           
            map = new google.maps.Map(document.getElementById('map_canvas'), null);
        }
        else if (SourceVal.length == 2 && (_location == SourceVal[0] || _location == SourceVal[1])) {
            
            document.getElementById("div_LocationSmile").style.display = "block";
            document.getElementById("div_LocationError").style.display = "none";
            SlideElement("div_LocationComment", false);
            AddCssClass(callerElement, "textValid");
        }
        else {
      //      _SetServerComment("<%# SelectFromList %>");
            document.getElementById("div_LocationError").style.display = "block";
            document.getElementById("div_LocationSmile").style.display = "none";
            SlideElement("div_LocationComment", true);
            AddCssClass(callerElement, "textError");
        }
        // document.getElementById("div_LocationComment").style.display = "none";
    }
    function ChkLocationOnKeyPress(elem, e) {
        document.getElementById("div_LocationError").style.display = "none";
        document.getElementById("div_LocationSmile").style.display = "none";
        if (elem.value.length == 0)
            SlideElement("div_LocationComment", false);
        else
            SlideElement("div_LocationComment", true);
   //     document.getElementById("div_LocationComment").style.display = (elem.value.length == 0) ? "none" : "block";
        RemoveCssClass(elem, "textError");

        if (elem.value.length == 0)
            return;
        var keynum;
        if (window.event) // IE            
            keynum = e.keyCode
        else if (e.which) // Netscape/Firefox/Opera            
            keynum = e.which
        if (keynum == 13) {


            /*
            https://developers.google.com/maps/documentation/javascript/geocoding
            The status code may return one of the following values:
            google.maps.GeocoderStatus.OK indicates that the geocode was successful.
            google.maps.GeocoderStatus.ZERO_RESULTS indicates that the geocode was successful but returned no results. This may occur if the geocode was passed a non-existent address or a latng in a remote location.
            google.maps.GeocoderStatus.OVER_QUERY_LIMIT indicates that you are over your quota. Refer to the Usage limits exceeded article in the Maps for Business documentation, which also applies to non-Business users, for more information.
            google.maps.GeocoderStatus.REQUEST_DENIED indicates that your request was denied for some reason.
            google.maps.GeocoderStatus.INVALID_REQUEST generally indicates that the query (address or latLng) is missing.
            */


            geocoder.geocode({ address: elem.value }, function (response, status) {
                if (status == google.maps.GeocoderStatus.OK && response[0]) {
                    lat_lng = response[0].geometry.location;
                    _radius = 30;
                    document.getElementById("<%#  hf_FullNameAddress.ClientID %>").value = response[0].formatted_address + ";;;" + elem.value;
                    $('#div_LocationError').hide();
                    $('#div_LocationSmile').show();
                    SlideElement("div_LocationComment", false);
                    full_name = response[0].formatted_address;
                    RemoveSelectLargeAreas();
                    SetAnchorStyle();
                    SetMap();
                }
                else {
                    $('#map_canvas').empty();
                    $('#div_map').hide();
                    $('#<%#  hf_FullNameAddress.ClientID %>').val('');
                    full_name = '';
                    map = new google.maps.Map(document.getElementById('map_canvas'), null);
                    document.getElementById("div_LocationError").style.display = "block";
                    SlideElement("div_LocationComment", true);
                }
            });
            
        }


    }
    
    function SetInvalidLocation() {
        document.getElementById("div_LocationError").style.display = "block";
        document.getElementById("div_LocationSmile").style.display = "none";
        SlideElement("div_LocationComment", true);// document.getElementById("div_LocationComment").style.display = "none";
        var searchTextField = document.getElementById("searchTextField");
        RemoveCssClass(searchTextField, "textValid");
        AddCssClass(searchTextField, "textError");
    }
    function SetAreas(elem) {
        var div_places = document.getElementById("<%# div_places.ClientID %>");
        if (div_places.style.display == "block")
            return false;
        _radius = 35;

        var _ul = document.getElementById("menu_radius");
        var a_arr = _ul.getElementsByTagName("a");
        for (var j = 0; j < a_arr.length; j++) {
            RemoveCssClass(a_arr[j], "Selected");
        }
        elem.className = "Selected";
        var url = "<%# GetLargeAreas %>";
        var params = "lat=" + lat_lng.lat() + "&lng=" + lat_lng.lng();

        CallWebService(params, url,
                    function (arg) {
                        if (arg == "faild") {
                            _SetServerComment("Server faild");
                            return;
                        }
                        var args = eval('(' + arg + ')');
                        var IsMutch = false;
                        var SavedRegionId = $('#<%# hf_SavedRegionId.ClientID %>').val();
                        if (SavedRegionId == "ALL") {
                            _SetLargeArea(document.getElementById('<%# a_Region0.ClientID %>'));
                            IsMutch = true;
                        }
                        for (var i = 0; i < args.length; i++) {
                            var _is_select = false;
                            if (!IsMutch && args[i]._id == SavedRegionId) {
                                _is_select = true;
                                IsMutch = true;
                            }
                            if (!IsMutch && args[i].level == "3")
                                _is_select = true;
                            SetAreaDetails(args[i]._id, args[i].level, args[i].name, _is_select);

                        }
                        div_places.style.display = "block";
                    },
                    function () { alert('error'); }
                );
        return true;
    }
    function SetAreaDetails(_id, level, name, IsSelect) {
        var _a;
        if (level == "1") {
            _a = document.getElementById("<%# a_Region1.ClientID %>");
            _a.setAttribute("_id", _id);
            
        }
        else if (level == "2") {
            _a = document.getElementById("<%# a_Region2.ClientID %>");
            _a.setAttribute("_id", _id);
        }
        else if (level == "3") {
            _a = document.getElementById("<%# a_Region3.ClientID %>");
            _a.setAttribute("_id", _id);
            
        }
        if (IsSelect)
            _SetLargeArea(_a);
        //    _a.addEventListener('click', SetLargeArea, false)

        _a.innerHTML = name;
        $(_a).attr('title', name);
    }
    /*
    function SetLargeArea() {
    _SetLargeArea(this);
    }
    */
    function _SetLargeArea(elem) {
        RemoveSelectLargeAreas();
        AddCssClass(elem, "selected");
        var _id = elem.getAttribute("_id");
        document.getElementById("<%# hf_LargeArea.ClientID %>").value = _id;

    }
    function RemoveSelectLargeAreas() {
        var table_large_area = document.getElementById("table_large_area");
        var anchors = table_large_area.getElementsByTagName("a");
        for (var i = 0; i < anchors.length; i++) {
            RemoveCssClass(anchors[i], "selected");            
        }
        document.getElementById("<%# hf_LargeArea.ClientID %>").value = "";
        var $a_more = $('a[val="More"]');
        $a_more.click(function () { SetAnchorAllClick($a_more.get(0)); });
    }
    function GetFullAddress() {
        if (full_name == null || full_name.length == 0)
            return "";
        return full_name;
    }
    function GetSelectLargeAreaId() {
        var _id = document.getElementById("<%# hf_LargeArea.ClientID %>").value;
        if (document.getElementById("<%# div_places.ClientID %>").style.display == "none")
            return "";
        if (_id.length == 0)
            return "false";
        return _id;
    }
    function GetAllDetails() {
        var __FullName = GetFullAddress();
        if (__FullName.length == 0)
            return "";
        var TempName = document.getElementById("<%#  hf_FullNameAddress.ClientID %>").value.split(';;;');
        var search = GetText_PlaceHolder(document.getElementById('searchTextField'), location_place_holder);
        var IsOk = false;
        for (var i = 0; i < TempName.length; i++) {
            if (search == TempName[i])
                IsOk = true;
        }
        if (!IsOk)
            return "";
        var _details = new Object();
        _details.lat_lng = lat_lng;
        _details.radius = _radius;
        _details.FullAddress = __FullName;
        _details.AreaId = GetSelectLargeAreaId();
        return _details;
    }
    

</script>
<asp:HiddenField ID="hf_FullNameAddress" runat="server" />
<div class="titleSecond"><asp:Label ID="Label1" runat="server" Text="My business is located at"></asp:Label></div>
<div class="clear"></div>
<div class="inputParent">           
    <div class="inputSun">
         <input id="searchTextField" type="text" size="50"   class="textActive" place_holder="<%# LocationPlaceHplder %>"
            onkeyup="ChkLocationOnKeyPress(this, event);" onkeydown="return (event.keyCode!=13);" HolderClass="place-holder"  />
    </div>

    <div class="inputValid" style="display:none;"  id="div_LocationSmile">
                
    </div>
    <div class="inputError" style="display:none;"  id="div_LocationError">
               
    </div>
    <div class="inputComment" style="display:none;" id="div_LocationComment">
            <%= LocationPlaceHplder %>
        <div class="chat-bubble-arrow-border"></div>
        <div class="chat-bubble-arrow"></div>
    </div>

    
    
    
</div>



    
<div id="div_map" style="display:none;" class="div_map">
    <div class="titleSecond"><asp:Label ID="Label2" runat="server" Text="The area my service covers"></asp:Label></div>
    <div class="clear"></div>
    <div id="map_canvas" style="width:550px; height:400px;"></div>
    <div class="localization" id="menu_radius">
                
               
       <table class="TableMiles">
            <tr>
            <td>
                <a href="javascript:void(0);" val="0.5" class="Selected">0.5mi</a>
            </td>
            <td>
                <a href="javascript:void(0);" val="1" class="Selected">1mi</a>
            </td>
            <td>
                <a href="javascript:void(0);" val="2" class="Selected">2mi</a>
            </td>
            <td>
                <a href="javascript:void(0);" val="3" class="Selected">3mi</a>
            </td>
            <td>
                <a href="javascript:void(0);" val="5">5mi</a>
            </td>
            <td>
                <a href="javascript:void(0);" val="10">10mi</a>
            </td>
            <td>
                <a href="javascript:void(0);" val="20">20mi</a>
            </td>
            <td>
                <a href="javascript:void(0);" val="30">30mi</a>
            </td>
            <td>
                <a href="javascript:void(0);" val="More">More</a>
            </td>
            </tr>
        </table>
    </div>
    <div class="clear"></div>
     <div id="lbl_result" ></div>
     <div id="div_places" style="display:none;" class="localization" runat="server">
        <asp:HiddenField ID="hf_LargeArea" runat="server" />
     
        <div class="topGreenBox">
            <div class="handleGreen"></div>
        </div>
        <div class="greenBox">  
                                     
            <table class="TableLargeArea" id="table_large_area">
            <thead>
            <tr>
            <th colspan="4">
                    <span>
                        Extended area I cover
                </span>
            </th>
            </tr>
            </thead>
            <tr>
            <td>
                <a href="javascript:void(0);" id="a_Region3" runat="server" onclick="_SetLargeArea(this);"></a>
            </td>
            <td>
            
                <a href="javascript:void(0);" id="a_Region2" runat="server" onclick="_SetLargeArea(this);"></a>
                
            </td>
            <td>
                <a href="javascript:void(0);" id="a_Region1" runat="server" onclick="_SetLargeArea(this);"></a>
            </td>
            <td>
                <a href="javascript:void(0);" id="a_Region0" onclick="_SetLargeArea(this);" _id="all" runat="server" title="USA">USA</a>
            </td>
                
            </tr>
            </table>

        </div>
    </div>
    <asp:HiddenField ID="hf_SavedRegionId" runat="server" />
</div>