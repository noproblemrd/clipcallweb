﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Controls_seo_popularHeadings : System.Web.UI.UserControl
{
    protected PpcSite _cache;
    public string state;
    public string stateAbbreviation;    
    public string city;
    protected string region;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(city))
            region = city + ", " + stateAbbreviation;
        else
            region = stateAbbreviation;

        _cache = PpcSite.GetCurrent();
        showHeadings();
        Page.DataBind();
        
    }

    public string setTitle(string myRegion)
    {
        if(String.IsNullOrEmpty(myRegion))
            return "<span class='wrapperInnerRightTitleTop'>Popular categories</span>";
        else
            return "<span class='wrapperInnerRightTitleTop'>Popular categories</span><br><span class='wrapperInnerRightTitleBottom'>in " + myRegion + "</span>";
    }

    private void showHeadings()
    {
        
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(null, _cache.SiteId);
        WebReferenceSupplier.ResultOfListOfString resultHeadings = null;       

        if (!String.IsNullOrEmpty(city) && !String.IsNullOrEmpty(state))
        {
            resultHeadings = _supplier.SEOGetPopularHeadingsInRegion(state,city);

            DataTable table = new DataTable();
            table.Columns.Add("heading");
            table.Columns.Add("url");
           
            foreach (string strHeading in resultHeadings.Value)
            {
                DataRow dr = table.NewRow();
                dr["heading"] = strHeading;
                dr["url"] = "~/profession/" + Utilities.cleanSpecialCharsUrlFreindly2(strHeading) + "/" + state + "/" + city;
                table.Rows.Add(dr);
            }

            DataListPopularHeadings.DataSource = table;
            DataListPopularHeadings.DataBind();
        }


        else if (!String.IsNullOrEmpty(state))
        {
            resultHeadings = _supplier.SEOGetPopularHeadingsInRegion(state);

            DataTable table = new DataTable();
            table.Columns.Add("heading");
            table.Columns.Add("url");
            DataRow dr;
            foreach (string strHeading in resultHeadings.Value)
            {
                dr = table.NewRow();
                dr["heading"] = strHeading;
                dr["url"] = "~/profession/" + Utilities.cleanSpecialCharsUrlFreindly2(strHeading) + "/" + state;
                table.Rows.Add(dr);
            }

            DataListPopularHeadings.DataSource = table;
            DataListPopularHeadings.DataBind();
        }

        /*
        else
        {
            resultHeadings = _supplier.SEOGetPopularHeadingsInRegion(state);

            DataTable table = new DataTable();
            table.Columns.Add("heading");
            table.Columns.Add("url");
            DataRow dr;
            foreach (string strHeading in resultHeadings.Value)
            {
                dr = table.NewRow();
                dr["heading"] = strHeading;
                dr["url"] = "~/profession/" + strHeading;
                table.Rows.Add(dr);
            }

            DataListPopularHeadings.DataSource = table;
            DataListPopularHeadings.DataBind();
        }
        */


    }
}