﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="relatedHeadings.ascx.cs" Inherits="Controls_seo_relatedHeadings" %>
<div class="wrapperInnerRightTitle" id="wrapperInnerRightTitle" runat="server" visible="false"><span class="wrapperInnerRightTitleTop">Related categories</span><br /><span class="wrapperInnerRightTitleBottom">in <%#region%></span></div>

<div id="wrapperInnerRightRelatedHeadingsContent" class="wrapperInnerRightPopularCitiesFew">
    <asp:DataList ID="DataListRelatedHeadings" runat="server" RepeatColumns="1" RepeatDirection="Vertical" CssClass="tblCities">
    <ItemTemplate>               
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("url")%>'><%#Eval("heading")%></asp:HyperLink>     
    </ItemTemplate>
</asp:DataList>
           
</div>