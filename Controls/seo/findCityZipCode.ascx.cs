﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_seo_findCityZipCode : System.Web.UI.UserControl
{
    protected string Profession;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void setProfession(string profession)
    {
        Profession = profession;
    }

    public string getRoot
    {
        get
        {
            return ResolveUrl("~");
        }
    }

    public string getProfession
    {
        get
        {
            return Profession;
        }
    }
}