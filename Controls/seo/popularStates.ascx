﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="popularStates.ascx.cs" Inherits="Controls_seo_popularStates" %>

<div class="wrapperInnerRightTitle">Popular states</div>
<div id="wrapperInnerRightPopularStatesFew" class="wrapperInnerRightPopularStatesFew">
    <asp:DataList ID="DataListPopularStatesRightFew" runat="server" RepeatColumns="1" RepeatDirection="Vertical" CssClass="tblCities">
    <ItemTemplate>  
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%#"~/profession/" + Utilities.cleanSpecialCharsUrlFreindly2(categoryAddress) + "/" + Eval("fullName") %>' ><%#Eval("fullName")%></asp:HyperLink>

            </ItemTemplate>
</asp:DataList>
<a href="javascript:void(0);" onclick="showAllStates(true);">All...</a>
               
</div>
        
<div id="wrapperInnerRightPopularStatesAll" class="wrapperInnerRightPopularStatesAll">
    <asp:DataList ID="DataListPopularStatesRightAll" runat="server" RepeatColumns="1" RepeatDirection="Vertical" CssClass="tblCities">
    <ItemTemplate>  
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%#"~/profession/" + Utilities.cleanSpecialCharsUrlFreindly2(categoryAddress) + "/" + Eval("fullName") %>' ><%#Eval("fullName")%></asp:HyperLink>

            </ItemTemplate>
</asp:DataList>
</div>


 