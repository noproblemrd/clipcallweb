﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="suppliers.ascx.cs" Inherits="Controls_seo_suppliers" %>
<div class="titleSuppliers">
    <h3 id="titleSuppliers"  runat="server"  ></h3>
    </div>  

<script>
    function encodeTwitterOrGooglePlus(url) {
        //alert(url + "\n" + url.replace(/%20/g, "%2520"));
        return url.replace(/%20/g, "%2520");
    }

    function hoverSupplier(recordId) {
        alert(recordId);
    }

   
    /* this my function eliminate the default function
         prevent show the unnecessary paramters in the address web page
         when the rewrite add when paging */
    function __doPostBack(eventTarget, eventArgument) {            
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                //alert(theForm.action);
                theForm.action = theForm.action.substring(0,theForm.action.indexOf('?'));
                theForm.submit();
            }
    }      

    $(document).ready(function () {
        /*
        $('.suppliersDetails').mouseenter(function (evt) {



            $(this).closest('.suppliersDetails').animate({
                backgroundColor: '#6e9518',
                WebkitTransition: 'all 0.1s ease 0s',
                MozTransition: 'all 0.1s ease 0s',
                MsTransition: 'all 0.1s ease 0s',
                OTransition: 'all 0.1s ease 0s',
                transition: 'all 0.1s ease 0s'
            }
            )

            //$(this).closest('.suppliersDetails').css('background', '#6e9518');


            $(this).closest('.suppliersDetails').find('.details1').css('color', 'white');
            $(this).closest('.suppliersDetails').find('.details1 .heading A').css('color', 'white');
            $(this).closest('.suppliersDetails').find('.details1 .services A').css('color', 'white');
            $(this).closest('.suppliersDetails').find('.details2').css('color', 'white');
            $(this).closest('.suppliersDetails').find('.details2 .duty').css('color', 'white');


        }
        )

        $('.suppliersDetails').mouseleave(function (evt) {

            $(this).closest('.suppliersDetails').animate({
                backgroundColor: 'white',
                WebkitTransition: 'all 0.1s ease 0s',
                MozTransition: 'all 0.1s ease 0s',
                MsTransition: 'all 0.1s ease 0s',
                OTransition: 'all 0.1s ease 0s',
                transition: 'all 0.1s ease 0s'
            }
            )

            //$(this).closest('.suppliersDetails').fadeOut(1);
            //$(this).closest('.suppliersDetails').css('background', 'white');
            $(this).closest('.suppliersDetails').find('.details1').css('color', '#333333');
            $(this).closest('.suppliersDetails').find('.details1 .heading A').css('color', '#21A9E5');
            $(this).closest('.suppliersDetails').find('.details1 .services A').css('color', '#21A9E5');
            $(this).closest('.suppliersDetails').find('.details2').css('color', '#333333');
            $(this).closest('.suppliersDetails').find('.details2 .duty').css('color', 'green');
        }
        )
        */
    }
    );
   
</script>

<asp:DataList ID="DataListSuppliers" runat="server" RepeatDirection="Vertical" CssClass="tblSuppliers">
        <ItemTemplate>   
        <%--
            <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#Eval("Name") %>' NavigateUrl='<%#"~/professoin/" + Eval("Name") + "/" + Eval("Id") %>' ></asp:HyperLink>
            --%>
            <div class="suppliersDetails" id="suppliersDetails<%#Eval("Id").ToString()%>"  >

                               
                
                   <div class="supplierImage">
                    <%#LoadLogo(Eval("Id").ToString())%>
                   </div>

                   <div class="details1">                

                   
                    <div class="heading"><a href="<%#root%>business/<%#Utilities.cleanSpecialCharsUrlFreindly3(Eval("Name").ToString().Trim())%>/<%#Eval("Id")%>"><%#Utilities.setfirstCapital(Eval("Name").ToString().Trim())%></a></div>
                    
              
                    <div class="region">
                    <%#Eval("Region")%>            
                    </div>               

                      <div class="services">
                 <%#arrayToString((string[])Eval("Services"))%>
                 </div> 
                                
                   </div>
                 
                   <div class="details2">   
                      <div class="phone">
                        <%#Utilities.americanFormatPhone(Eval("Phone").ToString())%>
                      </div> 
                      
                      <div class="duty" runat="server" id="duty">
                        <%#onDuty(Eval("OnDuty").ToString())%>
                      </div> 
                                               
                   </div>

                 
            </div>
             <!--
             <div class="pas"></div>
             -->

             <div class="pas"><hr class="seperator" size="1" /></div>
             

             

        </ItemTemplate>

       
    </asp:DataList>
    

<asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">        
    <ul>
        <li class="first"><asp:ImageButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click"  ImageUrl="~/Consumer/images/previous.png"/></li>
            
        <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
        <li class="last"><asp:ImageButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click"  ImageUrl="~/Consumer/images/next.png"/></li>
                        
    </ul>
</asp:Panel>