﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="searchProfessionRegion.ascx.cs" Inherits="Controls_seo_searchProfessionRegion"  ClientIDMode="Static"%>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>


<div class="request">
    <div class="searchLeft"></div>
    <div class="searchRight"></div>

  <div class="searchMiddle">
    <div class="div_txtSearch">
        <asp:TextBox ID="txt_Professional" runat="server" CssClass="textActive -Heading" place_holder="What type of pro do you need?" holderclass="place-holder"  onkeyup="return OpenDetails(event);"></asp:TextBox>
        
    </div>
                
    <div class="searchContent">Covering</div>    
            
    <div class="div_Region">
    <% /*  
        <asp:DropDownList ID="ddl_AdvNum" runat="server">
        </asp:DropDownList>
        */ %>
        <div id="location" class="div_TitleSpan location">
            <span>Your location is</span>
        </div>

        <asp:TextBox ID="txt_Region" runat="server" CssClass="txt-region" 
                 placeholder="what region?" onclick="this.value=''"></asp:TextBox>

        <%-- 
         <asp:TextBox ID="txt_Region" runat="server" CssClass="textActive txt-zipcode" 
                 placeholder="ggggggggggggg" onclick="this.value=''"></asp:TextBox>
        --%>

        <%--
        <asp:TextBox ID="txt_ZipCode" runat="server" CssClass="textActive txt-zipcode -item-zipcode" 
                place_holder="Enter zip code" holder-position="none" HolderClass="place-holder"></asp:TextBox>
        --%>

        <!--
        <input type="text" id="address" style="width: 500px;" onclick="this.value=''" placeholder="ggggggggggggg"  />      
        -->

        <div class="inputComment ZipCodeComment" style="display:none;" id="ZipCodeComment">
            <asp:Label ID="lbl_ZipCodeComment" runat="server" ></asp:Label>
            <div class="chat-bubble-arrow"></div>
        </div>
    </div>

    <script>

        var place;

        function setAdrres() {

            if (!place.geometry) {
                // Inform the user that a place was not found and return.
                alert("not found");
                return;
            }

            //alert(place.formatted_address);
            var str_address;
            var city;

            //alert("city " + city);
            var state;
            var zipCode;

            for (var i = 0; i < place.address_components.length; i++) {
                //alert(place.address_components[i].types[0] + " " + place.address_components[i].long_name);

                if (place.address_components[i].types[0] == "postal_code")
                    zipCode = place.address_components[i].long_name;

                if (place.address_components[i].types[0] == "locality")
                    city = place.address_components[i].long_name;

                if (place.address_components[i].types[0] == "administrative_area_level_1")
                    state = place.address_components[i].long_name;


            }

            if (state)
                str_address = state;

            if (city)
                str_address += " " + city;

            if (zipCode)
                str_address += " " + zipCode;

            alert("str_address: " + str_address);

            //alert("found");
        }

        var options = {
            //types: ['(cities)'],
            componentRestrictions: { country: 'Usa' }
        };


        var autocomplete = new google.maps.places.Autocomplete($("#txt_Region")[0], options);

    
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
        place = autocomplete.getPlace();
        setAdrres();
   




        //console.log(place.address_components);
        });
        



 </script>

    <div class="div_btnSearch" id="div_btn_SpeakToButton">
        <a href="javascript:GetWaterMark();" class="searchBtn"></a>
    </div>

  </div>
   

</div>
