﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Controls_seo_Quoates2 : System.Web.UI.UserControl
{
    protected const int ITEM_PAGE = 6;
    protected const int PAGE_PAGES = 10;
    protected string category;
    protected string restoreUrl;

    public delegate void RibbondEventHandler(object sender, EventArgsRibbon args);
    public event RibbondEventHandler eventRibbon;

    public bool showHeadingTitle = false;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void OnEventRibbon(EventArgsRibbon e)
    {
        if (eventRibbon != null)
            eventRibbon(this, e);
    }


    public void setRibbon(int cnt, string category, string title, string state, string city, string zipCode, string restoreUrl)
    {
        if (cnt > 0)
            titleleads.InnerHtml = title;
        else
            titleleads.Visible = false;

        lnkNextPage.PostBackUrl = restoreUrl;
        lnkPreviousPage.PostBackUrl = restoreUrl;

        EventArgsRibbon eventArgsRibbon = new EventArgsRibbon();
        eventArgsRibbon.Category = category;
        eventArgsRibbon.Count = cnt;
        eventArgsRibbon.State = state;
        eventArgsRibbon.City = city;
        eventArgsRibbon.ZipCode = zipCode;
        OnEventRibbon(eventArgsRibbon);
    }

    public void showLeads(WebReferenceSupplier.SEOLeadData[] leads)
    {
        BindData(leads, category);
        dataV = leads;
    }



    protected void BindData(WebReferenceSupplier.SEOLeadData[] LeadData, string category)
    {
        DataListLeads.DataSource = null;
        DataListLeads.DataBind();

        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;

        objPDS.PageSize = ITEM_PAGE;

        foreach (WebReferenceSupplier.SEOLeadData seoLeadData in LeadData)
        {
            //seoLeadData.
        }

        objPDS.DataSource = LeadData;

        if (LeadData.Length > 0) // there are leads for this specific period    
        //if (!debug)
        {
            objPDS.CurrentPageIndex = CurrentPage;


            if (objPDS.IsFirstPage)
                lnkPreviousPage.Visible = false;
            else
                lnkPreviousPage.Visible = true;


            if (objPDS.IsLastPage)
                lnkNextPage.Visible = false;
            else
                lnkNextPage.Visible = true;

            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            /*
            if (lnkNextPage.Enabled)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(false);", true);
            */

            LoadPages(LeadData.Length);

            DataListLeads.DataSource = objPDS;
            DataListLeads.DataBind();

            div_paging.Visible = true;

            /*
            leadsSeperator.Visible = true;
            noRecords.Visible = false;
            leadsTime.Visible = true;
            */
        }

        else
        {
            div_paging.Visible = false;
        }

    }

    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        //if there is only one page
        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListV = null;
            return;
        }

        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListV = dic;
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell2(kvp.Value, kvp.Key));
    }

    HtmlTableCell MakeCell(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();

        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }

    HtmlGenericControl MakeCell2(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }

        //HtmlTableCell htc = new HtmlTableCell();
        HtmlGenericControl li = new HtmlGenericControl("li");
        li.Controls.Add(lb);
        li.Attributes.Add("class", "td_num_pages");

        return li;
    }


    /*
  DataPager pager = (DataPager)Page.FindControl("VideosDataPager");
 pager.Controls.Clear();

 int count = pager.TotalRowCount;
 int pageSize = pager.PageSize;
 int pagesCount = count / pageSize + (count % pageSize == 0 ? 0 : 1);
 int pageSelected = pager.StartRowIndex / pageSize + 1;

 if (pageSelected>1)
 {
 // first page
 HyperLink img = new HyperLink();
 img.ImageUrl = "/images/first.png";
 img.Text = "First page";
 img.NavigateUrl = "/videos--cat--" + catnameforPaging + "--1.aspx?s="+ NvUtils.GetCurrentSessionKey();
 VideosList.Controls.Add(img);
 // gap
 Literal space = new Literal();
 space.Text = " ";
 VideosList.Controls.Add(space);
 }


 // paging

 for (int i = 1; i <= pagesCount; ++i)
 {
     if (pageSelected != i)
     {
         HyperLink link = new HyperLink();
         link.NavigateUrl = "/videos--cat--" + catnameforPaging + "--1.aspx?s="+ NvUtils.GetCurrentSessionKey()+"&p=" + i.ToString();
         link.Text = i.ToString();
         VideosList.Controls.Add(link);


     }
     else
     {
         Literal lit = new Literal();
         lit.Text = i.ToString();
         VideosList.Controls.Add(lit);
     }

     Literal spaceb = new Literal();
     spaceb.Text = " ";
     VideosList.Controls.Add(spaceb);

}
 if (pageSelected < pagesCount)
 {

     // last page
     HyperLink imgb = new HyperLink();
     imgb.ImageUrl = "/images/last.png";
     imgb.Text = "Last page";
     imgb.NavigateUrl = "/videos--cat--" + catnameforPaging + "--1.aspx?s=" + NvUtils.GetCurrentSessionKey() + "&p=" + pagesCount;
     VideosList.Controls.Add(imgb);
 }
  */




    protected void lnkNextPage_Click(object sender, EventArgs e)
    {

        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;
        listTR = new List<string>();



        BindData(dataV, category);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;
        listTR = new List<string>();
        BindData(dataV, category);
    }
    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
        listTR = new List<string>();
        BindData(dataV, category);
    }

    public string formatHeading(string heading)
    {
        string tempHeading = "";
        if (!string.IsNullOrEmpty(heading))
            tempHeading = heading + " needed";
        return tempHeading;
    }

    protected string IsNew(string ifNew)
    {
        if (ifNew.ToLower() == "true")
            return "New!";
        else
            return "";
    }

    protected string IsNew2(string ifNew)
    {
        if (ifNew.ToLower() == "true")
            return "";

        ////return "<span class=\"delivered take\" onclick='openClaim(\"TakeThisJob\",\"2_0_1\")'>Take this job!</span>";

        else
            return "<span class=\"delivered handled\">Request handled</span>";
    }


    List<string> listTR
    {
        get { return (Session["list_TR"] == null) ? new List<string>() : (List<string>)Session["list_TR"]; }
        set { Session["list_TR"] = value; }
    }

    protected WebReferenceSupplier.SEOLeadData[] dataV
    {
        //get { return (ViewState["data"] == null) ? null : (WebReferenceSupplier.AdvDashboardLeadData[])ViewState["data"]; }
        get { return (Session["data"] == null) ? null : (WebReferenceSupplier.SEOLeadData[])Session["data"]; }
        set { Session["data"] = value; }
    }


    int CurrentPage
    {
        get
        {
            object o = this.ViewState["_CurrentPageAdvertiser"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["_CurrentPageAdvertiser"] = value;
        }
    }


    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }
}