﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class Controls_seo_suppliers : System.Web.UI.UserControl
{
    protected const int ITEM_PAGE = 15;
    protected const int PAGE_PAGES = 10;
    protected string category;
    protected string root;
    protected string url;
    protected string regionOfLeads="NY";
    protected PpcSite _cache2;
    protected string logoForFacebook;
    public int indexList {get; set;}
    protected string restoreUrl;


    protected void Page_Load(object sender, EventArgs e)
    {
        root = ResolveUrl("~");

        if (Request.Url.Host == "localhost")
        {
            url = Request.Url.Host + ":" + Request.Url.Port + root;
        }

        else
        {
            url = "http://" + Request.Url.Host + root;
            
        }

        logoForFacebook = url + "images/logos/logoForFacebook.png";
        

        //_cache2 = PpcSite.GetCurrent();
        Page.DataBind();
    }

    public void showSuppliers(WebReferenceSupplier.SEOSupplierData[] suppliers, PpcSite _cache, string localRestoreUrl)
    {
        _cache2 = _cache;
        BindData(suppliers, category);
        dataV = suppliers;
        restoreUrl = localRestoreUrl;
        lnkPreviousPage.PostBackUrl = restoreUrl;
        lnkNextPage.PostBackUrl = restoreUrl;
    }


    protected string LoadLogo(string id)
    {
        string logo = "";
        string command = "EXEC dbo.GetAdvertiserDetails @UserId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", new Guid(id));
            cmd.Parameters.AddWithValue("@SiteNameId", PpcSite.GetCurrent().SiteId);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                if (!reader.IsDBNull(1))
                    logo = reader.GetString(1);
            }

            conn.Close();
        }

        string imgSource="";
        //Response.Write("logo: " + ConfigurationManager.AppSettings["professionalLogosWeb"] + logo);
        if (!string.IsNullOrEmpty(logo))
        {           
            imgSource = "<img src='" + ConfigurationManager.AppSettings["professionalLogosWeb"] + new Guid(id) + @"\" + logo + "' />";
        }

        else
        {
            //imgSource = "<img src='" + ConfigurationManager.AppSettings["professionalLogosWeb"] + @"9968AEC1-35FD-43E9-A325-530C3001E03F\" + logo + "' />";
            imgSource = "<div class='frame'></div>";
        }

        return imgSource;
    }

    public void setTitle(int cnt)
    {
        if (cnt==1)
            titleSuppliers.InnerHtml = string.Format("{0:n0}", cnt) + " local business found";   
        else
            titleSuppliers.InnerHtml = string.Format("{0:n0}", cnt) + " local businesses found";      
    }

    
    protected string arrayToString(string[] str)
    {
        string temp="";

        for (int i = 0; i < str.Length; i++)
        {
            if (i == str.Length - 1)
            {
                // temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>";
                temp += "<a href='" + root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(str[i]) + "'>" + str[i] + "</a>";
            }
            else
            {
                //temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>" + ", ";
                temp += "<a href='" + root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(str[i]) + "'>" + str[i] + "</a>" + ", ";
            }
        }

        //temp = "<b>Services:</b> " + temp;

        return temp;
    }

    
    protected string arrayToString2(string[] str)
    {
        string temp = "";
        string code;
        HeadingCode hd;


        for (int i = 0; i < str.Length; i++)
        {

            code = _cache2.GetHeadingCodeByKeywordHeadingName(str[i]);
            hd = _cache2.GetHeadingDetails(code);

            if (i == str.Length - 1)
            {
                // temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>.";

                temp += hd.Title.ToLower();

            }
            else if (i == str.Length - 2)
            {
                // temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>.";

                temp += hd.Title.ToLower() + " and ";

            }
            else
            {
                //temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>" + ", ";               

                temp += hd.Title.ToLower() + ", ";
            }
        }

        return temp;
    }

   

    protected void BindData(WebReferenceSupplier.SEOSupplierData[] LeadData, string category)
    {
        DataListSuppliers.DataSource = null;
        DataListSuppliers.DataBind();
        //LeadData[0].Name;
        //LeadData[0].Phone;
        //LeadData[0].Region;        
        //LeadData[0].Services;
        //LeadData[0].OnDuty

        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;

        objPDS.PageSize = ITEM_PAGE;

        objPDS.DataSource = LeadData;
        
        if (LeadData.Length > 0) // there are leads for this specific period    
        //if (!debug)
        {
            objPDS.CurrentPageIndex = CurrentPage;
            if (objPDS.IsFirstPage)
                lnkPreviousPage.Visible=false;
            else
                lnkPreviousPage.Visible = true;
           

            if (objPDS.IsLastPage)
                lnkNextPage.Visible = false;
            else
                lnkNextPage.Visible = true;
            
            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            /*
            if (lnkNextPage.Enabled)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(false);", true);
            */

            indexList++;

            LoadPages(LeadData.Length);

            DataListSuppliers.DataSource = objPDS;
            DataListSuppliers.DataBind();

            div_paging.Visible = true;

            /*
            leadsSeperator.Visible = true;
            noRecords.Visible = false;
            leadsTime.Visible = true;
            */
        }

        else
        {
            titleSuppliers.Visible = false;
            div_paging.Visible = false;
        }

    }

    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        //if there is only one page
        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListV = null;
            return;
        }

        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListV = dic;
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell2(kvp.Value, kvp.Key));
    }



  





    protected string getRoot3
    {
        get
        {
            if (Request.Url.Host == "localhost")
                return Request.Url.Host + ":" + Request.Url.Port + ResolveUrl("~");
            else
                return "http://" + Request.Url.Host;
        }

    }

    HtmlTableCell MakeCell(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();

        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }

    HtmlGenericControl MakeCell2(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }

        //HtmlTableCell htc = new HtmlTableCell();
        HtmlGenericControl li = new HtmlGenericControl("li");
        li.Controls.Add(lb);
        li.Attributes.Add("class", "td_num_pages");

        return li;
    }

    protected string onDuty(string duty)
    {
        if (duty.ToLower() == "true")
            return "<span style='color:#2EA700'>ON DUTY<span>";
        else
            return "<span style='color:#666'>OFF DUTY<span>";
    }

    protected void lnkNextPage_Click(object sender, EventArgs e)
    {

        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;
        listTR = new List<string>();

        BindData(dataV, category);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;
        listTR = new List<string>();
        BindData(dataV, category);
    }
    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
        listTR = new List<string>();
        BindData(dataV, category);
    }

    List<string> listTR
    {
        get { return (Session["list_TR"] == null) ? new List<string>() : (List<string>)Session["list_TR"]; }
        set { Session["list_TR"] = value; }
    }

    protected WebReferenceSupplier.SEOSupplierData[] dataV
    {
        //get { return (ViewState["data"] == null) ? null : (WebReferenceSupplier.AdvDashboardLeadData[])ViewState["data"]; }
        get { return (Session["dataSuppliers"] == null) ? null : (WebReferenceSupplier.SEOSupplierData[])Session["dataSuppliers"]; }
        set { Session["dataSuppliers"] = value; }
    }


    int CurrentPage
    {
        get
        {
            object o = this.ViewState["_CurrentPageAdvertiser"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["_CurrentPageAdvertiser"] = value;
        }
    }


    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }

    
}