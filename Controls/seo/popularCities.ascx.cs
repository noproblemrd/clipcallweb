﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Controls_seo_popularCities : System.Web.UI.UserControl
{
    protected PpcSite _cache;
    public string categoryAddress;

    protected void Page_Load(object sender, EventArgs e)
    {
        _cache = PpcSite.GetCurrent();
        showRegions();
       
    }

    
    protected void showRegions()
    {

        try
        {           

            if (_cache.cities.Count > 0)
            {                
                List<City> rndListCities = Utilities.getRandomList(_cache.cities, 5);
                DataTable dtFewCities = new DataTable();
                dtFewCities.Columns.Add("CityName");
                dtFewCities.Columns.Add("StateFullName");
                dtFewCities.Columns.Add("StateAbbreviation");

                for (int i = 0; i < 5; i++)
                {
                    DataRow dr = dtFewCities.NewRow();
                    dr["CityName"] = rndListCities[i].CityName;
                    dr["StateFullName"] = rndListCities[i].StateFullName;
                    dr["StateAbbreviation"] = rndListCities[i].StateAbbreviation;
                    dtFewCities.Rows.Add(dr);
                }


                DataListPopularCitiesRightFew.DataSource = dtFewCities;
                DataListPopularCitiesRightFew.DataBind();
            }

            DataListPopularCitiesRightAll.DataSource = _cache.cities;
            DataListPopularCitiesRightAll.DataBind();
        }

        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, _cache.SiteId);
            return;
        }       


    }
}