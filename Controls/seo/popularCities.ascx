﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="popularCities.ascx.cs" Inherits="Controls_seo_popularCities" %>
<div class="wrapperInnerRightTitle">Popular cities</div>
<div id="wrapperInnerRightPopularCitiesFew" class="wrapperInnerRightPopularCitiesFew">
    <asp:DataList ID="DataListPopularCitiesRightFew" runat="server" RepeatColumns="1" RepeatDirection="Vertical" CssClass="tblCities">
    <ItemTemplate>               
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#"~/profession/" + Utilities.cleanSpecialCharsUrlFreindly2(categoryAddress) + "/" + Eval("StateFullName") + "/" + Eval("CityName") %>' ><%#Eval("CityName")%>, <%#Eval("StateAbbreviation")%></asp:HyperLink>     
    </ItemTemplate>
</asp:DataList>
<a href="javascript:void(0);" onclick="showAllCities(true);">All...</a>
               
</div>
        
<div id="wrapperInnerRightPopularCitiesAll" class="wrapperInnerRightPopularCitiesAll">
    <asp:DataList ID="DataListPopularCitiesRightAll" runat="server" RepeatColumns="1" RepeatDirection="Vertical" CssClass="tblCities">
    <ItemTemplate>               
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#"~/profession/" + Utilities.cleanSpecialCharsUrlFreindly2(categoryAddress) + "/" + Eval("StateFullName") + "/" + Eval("CityName") %>' ><%#Eval("CityName")%>, <%#Eval("StateAbbreviation")%></asp:HyperLink>     
    </ItemTemplate>
</asp:DataList>
</div>