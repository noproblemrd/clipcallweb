﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;
public partial class Controls_seo_suppliers2 : System.Web.UI.UserControl
{
    protected const int ITEM_PAGE = 10; // 15
    protected const int PAGE_PAGES = 10;
    protected string category;
    protected string root;
    protected string url;
    protected string regionOfLeads = "NY";
    protected PpcSite _cache2;
    protected string logoForFacebook;
    public int indexList { get; set; }
    protected string restoreUrl;
    protected List<supplierItem> listSupplieritem=new List<supplierItem>();
    

    protected void Page_Load(object sender, EventArgs e)
    {
        root = ResolveUrl("~");
       
        if (Request.Url.Host == "localhost")
        {
            url = "http://" + Request.Url.Host + ":" + Request.Url.Port + root;            
        }

        else
        {
            url = "http://" + Request.Url.Host + root;            
        }        
        
        logoForFacebook = url + "images/logos/logoForFacebook.png";

         

        //_cache2 = PpcSite.GetCurrent();
        Page.DataBind();
    }

    public void showSuppliers(WebReferenceSupplier.SEOSupplierData[] suppliers, PpcSite _cache, string localRestoreUrl)
    {
        _cache2 = _cache;
        BindData(suppliers, category);
        dataV = suppliers;
        restoreUrl = localRestoreUrl;
        lnkPreviousPage.PostBackUrl = restoreUrl;
        lnkNextPage.PostBackUrl = restoreUrl;
        
    }


    protected void Item_Created(Object sender, DataListItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
        {            
            HtmlGenericControl divHeadingIndex = (HtmlGenericControl)e.Item.FindControl("headingIndex"); // div 

            divHeadingIndex.InnerText = (e.Item.ItemIndex + 1).ToString();

            index++;

        }

    }

    protected void Item_Bound(Object sender, DataListItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
        {            

            HtmlGenericControl divRegion = (HtmlGenericControl)e.Item.FindControl("region"); // div 
           

            
            string supplierId = DataBinder.Eval(e.Item.DataItem, "Id").ToString();
            string supplierName = DataBinder.Eval(e.Item.DataItem, "Name").ToString();
            bool IsPaying = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "IsPaying"));
            //HtmlGenericControl supplierImage = (HtmlGenericControl)e.Item.FindControl("supplierImage" + supplierId); // div             

            HtmlInputHidden hdn_supplierId = (HtmlInputHidden)e.Item.FindControl("hdn_supplierId");
            hdn_supplierId.Value = supplierId;

            string supplierUrl="";
            if (Request.Url.Host == "localhost")
            {
                supplierUrl = "http://" + Request.Url.Host + ":" + Request.Url.Port + root + "business/" + supplierName + "/" + supplierId;
            }

            else
            {
                supplierUrl = "http://" + Request.Url.Host + root + "business/" + supplierName + "/" + supplierId;
            }

            HtmlInputHidden hdn_supplierUrl = (HtmlInputHidden)e.Item.FindControl("hdn_supplierUrl");
            hdn_supplierUrl.Value = supplierUrl;

            HtmlAnchor linkPohne = (HtmlAnchor)e.Item.FindControl("linkPhone");
            linkPohne.Attributes.Add("onclick", "javascript:openDetails('" + supplierId + "','" + linkPohne.InnerText + "','" + supplierName + "','" + supplierUrl + "',0);");
            linkPohne.Attributes.Add("href", "javascript:void(0);");

            HtmlAnchor linkPhoneIcon = (HtmlAnchor)e.Item.FindControl("linkPhoneIcon");
            linkPhoneIcon.Attributes.Add("onclick", "javascript:openDetails('" + supplierId + "','" + linkPohne.InnerText + "','" + supplierName + "','" + supplierUrl + "',0);");
            linkPhoneIcon.Attributes.Add("href", "javascript:void(0);");

            /*
            supplierItem supplierItem = new supplierItem();
            if (listSupplieritem.Count < ITEM_PAGE)
            {
                supplierItem.address = divRegion.InnerText;
                supplierItem.phone = linkPohne.InnerText;
                supplierItem.img = LoadLogo(supplierId, divRegion.InnerText);
                listSupplieritem.Add(supplierItem);
            }  
            */

            HtmlGenericControl  iconFrameEdit = (HtmlGenericControl)e.Item.FindControl("iconFrameEdit"); // div;
            if(IsPaying)
                iconFrameEdit.Attributes.Add("onclick", "goToLogin();");
            else
                iconFrameEdit.Attributes.Add("onclick", "openClaim('editNumberCategory','4_4_0','" + supplierName + "','" + supplierId + "');");
            
        }

    }
    


    protected string LoadLogo(string id, string address)
    {
        string logo = "";
        string command = "EXEC dbo.GetAdvertiserDetails @UserId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", new Guid(id));
            cmd.Parameters.AddWithValue("@SiteNameId", PpcSite.GetCurrent().SiteId);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                if (!reader.IsDBNull(1))
                    logo = reader.GetString(1);
            }

            conn.Close();
        }

        string imgSource = "";
        //Response.Write("logo: " + ConfigurationManager.AppSettings["professionalLogosWeb"] + logo);
        if (!string.IsNullOrEmpty(logo))
        {
            imgSource = "<img src='" + ConfigurationManager.AppSettings["professionalLogosWeb"] + new Guid(id) + @"\" + logo + "' height='80px'/>";
        }

        else
        {

            imgSource = "<script>setStreetView('supplierImage" + id + "','" + address + "');</script>";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "setStreetView", script, true);

            
            //imgSource = "<img src='" + ConfigurationManager.AppSettings["professionalLogosWeb"] + @"9968AEC1-35FD-43E9-A325-530C3001E03F\" + logo + "' />";
            //script = "<div class='frame'></div>";
        }

        return imgSource;
    }

    public void setTitle(int cnt)
    {
        if (cnt == 1)
            titleSuppliers.InnerHtml = string.Format("{0:n0}", cnt) + " local business found";
        else
            titleSuppliers.InnerHtml = string.Format("{0:n0}", cnt) + " local businesses found";
    }


    protected string getYelpDatails(string phone, string supplierName)
    {

        /*
          documentation http://www.yelp.com/developers/documentation/v1/phone_api#sampleResponse
        */

        /* important comments
        1. i use this service wich is by v1. the new one is v2.
        2. When will access to the API v1.0 be turned off?

            The Yelp v1.0 API is deprecated and it is strongly recommended that developers 
             not use it. There are no current plans to turn it off but using it is at your own 
        risk.

        3. What's the best way to match a specific business?

            Our phone number API matches a business's phone number to the data we have in our 
            database. This is the best way to find data on a specific business.


        4. At this juncture, we only offer the phone match API via the deprecated V1 
           service.
           best
           Yelp API Team
        */

        StringBuilder sbYelp = new StringBuilder();
        /* Remove on 18-10-2015
        try
        {
            var request = WebRequest.Create("http://api.yelp.com/phone_search?phone=" + phone  + "&ywsid=J4sSbZsr63xaLAYJPRqQ5Q");  // example with 1 business returned 

            //var request = WebRequest.Create("http://api.yelp.com/phone_search?phone=2122444011&ywsid=J4sSbZsr63xaLAYJPRqQ5Q"); // example with 2 business returned
            //var request = WebRequest.Create("http://api.yelp.com/phone_search?phone=2122931393&ywsid=J4sSbZsr63xaLAYJPRqQ5Q");  // example with 1 business returned          
            //var request = WebRequest.Create("http://api.yelp.com/phone_search?phone=0543388912&ywsid=J4sSbZsr63xaLAYJPRqQ5Q"); // example with irrelevant number                    

            string text;
            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
            }

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            dynamic item = jss.Deserialize<object>(text);

            string status = item["message"]["text"];

            //Response.Write("<br>" + item["businesses"].Length);

            if (status == "OK" && item["businesses"]!=null && item["businesses"].Length>0)
            {

                string rating_img_url = item["businesses"][0]["rating_img_url"];                
                string business_url = item["businesses"][0]["url"];
                int review_count = item["businesses"][0]["review_count"];
                string rating_img_url2 = item["businesses"][0]["reviews"][0]["rating_img_url"];
                string user_photo_url_small = item["businesses"][0]["reviews"][0]["user_photo_url_small"];
                string user_photo_url = item["businesses"][0]["reviews"][0]["user_photo_url"];
                string user_url = item["businesses"][0]["reviews"][0]["user_url"];
                string review_url = item["businesses"][0]["reviews"][0]["url"];
                string text_excerpt = item["businesses"][0]["reviews"][0]["text_excerpt"];
                string review_date = item["businesses"][0]["reviews"][0]["date"];
                string review_user_name = item["businesses"][0]["reviews"][0]["user_name"];                    

                sbYelp.Append("<div class='yelpContent0'>");
                sbYelp.Append("<div class='yelpPreview'>");

                sbYelp.Append("<div class='yelpOpener'>");
                sbYelp.Append("<div class='yelpOpenerObj' ></div><div class='yelpOpenerText'>Recent review for " + supplierName + "</div>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewCounts'>");

                if (review_count == 1)
                    sbYelp.Append("Based on <a href='" + business_url + "' target='_blank' >1 review</a>");
                else
                    sbYelp.Append("Based on <a href='" + business_url + "' target='_blank' >" + review_count + " reviews</a>");

                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewAvg'>");
                sbYelp.Append("<img src='" + rating_img_url + "'>");
                sbYelp.Append("</div>");

                sbYelp.Append("</div>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpContent'>");

                sbYelp.Append("<div class='yelpContent1'>");
                sbYelp.Append("<div class='yelpUserImage'>");
                sbYelp.Append("<img src='" + user_photo_url_small + "' width='40px' height='40px'>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewDetails'>");
                sbYelp.Append("<div class='yelpReviewStar'>");
                sbYelp.Append("<img src='" + rating_img_url2 + "'>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewDate'>");
                sbYelp.Append(review_date);
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewUser'>");
                sbYelp.Append(review_user_name);
                sbYelp.Append("</div>");

                sbYelp.Append("</div>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpContent2'>");
                sbYelp.Append(text_excerpt + " <a href='" + review_url + "' target='_blank'>read more</a>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpContent3'>");
                sbYelp.Append("<img src='" + url + "consumer/images/reviewsFromYelpWHT.gif'>");
                sbYelp.Append("</div>");

                sbYelp.Append("</div>");
            }

            else
            {
                sbYelp.Append("&nbsp;");
            }

        }

        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, _cache2.SiteId);
            sbYelp.Append("&nbsp;");
        }

        */
        return sbYelp.ToString();


        /*
        
         * example of stracture
        phone:2122444011 mover in new york "Divine Moving"        
        
        {"message": {"text": "OK", "code": 0, "version": "1.1.1"},                 "businesses": 
        [{"rating_img_url": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/f1def11e4e79/ico/stars/v1/stars_5.png", 
        "country_code": "US", "id": "7U0CPySKcB9JNUqcsOWZuQ", "is_closed": false, "city": 
        "New York", "mobile_url": "http://m.yelp.com/biz/divine-moving-and-storage-ltd-
        new-york-3", "review_count": 1, "zip": "10022", "state": "NY", 
        "rating_img_url_small": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/c7623205d5cd/ico/stars/v1/stars_small_5.pn
        g", "address1": "845 3rd Ave", "address2": "6th Fl", "address3": "", "phone": 
        "2122444011", "state_code": "NY", "categories": [{"category_filter": "movers", 
        "search_url": "http://www.yelp.com/search?cflt=movers\u0026find_desc=
        \u0026find_loc=845+3rd+Ave%2C+New+York+10022", "name": "Movers"}], "photo_url": 
        "http://media4.fl.yelpcdn.com/bpthumb/PVcN75tT13Ic1LXZrWG1Bw/ms", "distance": 
        0.0, "name": "Divine Moving \u0026 Storage Ltd", "neighborhoods": [{"url": 
        "http://www.yelp.com/search?exclude_start=True\u0026find_desc=
        \u0026find_loc=Midtown+East%2C+Manhattan%2C+NY", "name": "Midtown East"}], "url": 
        "http://www.yelp.com/biz/divine-moving-and-storage-ltd-new-york-3", "country": 
        "USA", "avg_rating": 5.0, "nearby_url": "http://www.yelp.com/search?find_desc=
        \u0026find_loc=845+3rd+Ave%2C+New+York+10022", "reviews": 
        [{"rating_img_url_small": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/c7623205d5cd/ico/stars/v1/stars_small_5.pn
        g", "user_photo_url_small": 
        "http://media1.fl.yelpcdn.com/upthumb/ZwtU0FeNTeV2OEePBg3Nqg/ss", 
        "rating_img_url": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/f1def11e4e79/ico/stars/v1/stars_5.png", 
        "rating": 5, "user_url": "http://www.yelp.com/user_details?
        userid=ens7LiIE4Cs4RjW5Mz65Mw", "url": "http://www.yelp.com/biz/divine-moving-
        and-storage-ltd-new-york-3?hrid=gTg13yHduBos9yOrybuuuA", "mobile_uri": 
        "/biz/divine-moving-and-storage-ltd-new-york-3?full=True
        \u0026hrid=gTg13yHduBos9yOrybuuuA", "text_excerpt": "After three misfires with 
        three very shady moving companies, one of the passerby neighbors overhearing my 
        conversation with Jakop the doorman, chimed-in and...", "user_photo_url": 
        "http://media1.fl.yelpcdn.com/upthumb/ZwtU0FeNTeV2OEePBg3Nqg/ms", "date": "2014-
        03-09", "user_name": "George M.", "id": "gTg13yHduBos9yOrybuuuA"}], 
        "photo_url_small": 
        "http://media4.fl.yelpcdn.com/bpthumb/PVcN75tT13Ic1LXZrWG1Bw/ss"}, 
        {"rating_img_url": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/5ef3eb3cb162/ico/stars/v1/stars_3_half.png
        ", "country_code": "US", "id": "SwA8bsQ4WxOXi1z84QJhgQ", "is_closed": false, 
        "city": "New York", "mobile_url": "http://m.yelp.com/biz/divine-moving-and-
        storage-new-york-2", "review_count": 28, "zip": "10022", "state": "NY", 
        "rating_img_url_small": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/2e909d5d3536/ico/stars/v1/stars_small_3_ha
        lf.png", "address1": "845 3rd Ave", "address2": "6Fl", "address3": "", "phone": 
        "2122444011", "state_code": "NY", "categories": [{"category_filter": "movers", 
        "search_url": "http://www.yelp.com/search?cflt=movers\u0026find_desc=
        \u0026find_loc=845+3rd+Ave%2C+New+York+10022", "name": "Movers"}, 
        {"category_filter": "selfstorage", "search_url": "http://www.yelp.com/search?
        cflt=selfstorage\u0026find_desc=\u0026find_loc=845+3rd+Ave%2C+New+York+10022", 
        "name": "Self Storage"}], "photo_url": 
        "http://media4.fl.yelpcdn.com/bpthumb/C4x0I5v-WPzhSWUzcfY6EQ/ms", "distance": 
        0.0, "name": "Divine Moving \u0026 Storage", "neighborhoods": [{"url": 
        "http://www.yelp.com/search?exclude_start=True\u0026find_desc=
        \u0026find_loc=Midtown+East%2C+Manhattan%2C+NY", "name": "Midtown East"}], "url": 
        "http://www.yelp.com/biz/divine-moving-and-storage-new-york-2", "country": "USA", 
        "avg_rating": 3.5, "nearby_url": "http://www.yelp.com/search?find_desc=
        \u0026find_loc=845+3rd+Ave%2C+New+York+10022", "reviews": 
        [{"rating_img_url_small": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/c7623205d5cd/ico/stars/v1/stars_small_5.pn
        g", "user_photo_url_small": 
        "http://media1.fl.yelpcdn.com/upthumb/8HUCxwxBV3UCmNik_SQk-w/ss", 
        "rating_img_url": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/f1def11e4e79/ico/stars/v1/stars_5.png", 
        "rating": 5, "user_url": "http://www.yelp.com/user_details?
        userid=QIxQ4IvK8DsvmCsABMAzFA", "url": "http://www.yelp.com/biz/divine-moving-
        and-storage-new-york-2?hrid=CiI0yfcU3AEvAMd7cV3veg", "mobile_uri": "/biz/divine-
        moving-and-storage-new-york-2?full=True\u0026hrid=CiI0yfcU3AEvAMd7cV3veg", 
        "text_excerpt": "I have been with Divine Moving and Storage for 5 months now and 
        it was a great experience working with them. I moved to a small apartment last 
        February and...", "user_photo_url": 
        "http://media1.fl.yelpcdn.com/upthumb/8HUCxwxBV3UCmNik_SQk-w/ms", "date": "2014-
        08-13", "user_name": "Diane B.", "id": "CiI0yfcU3AEvAMd7cV3veg"}], 
        "photo_url_small": "http://media4.fl.yelpcdn.com/bpthumb/C4x0I5v-
        WPzhSWUzcfY6EQ/ss"}]}
        
        */


        /*
         Response Codes:

        This section outlines the response codes that can be expected from a "BusinessReviewSearch" operation.
        Response Code 	Response Message 	Description
        0 	OK 	Indicates the request completed without error.
        1 	Server error 	Indicates that a system error occurred and that the request was unable to be processed.
        2 	Invalid YWSID 	Returned when an invalid YWSID is supplied with the API request.
        3 	Missing YWSID 	Indicates that  the required YWSID parameter was not supplied with the request.
        4 	Exceed daily API request limit 	Returned when the number of requests executed for a particular YWSID on a particular day exceeds the specified limit.
        5 	API not available 	Indicates that the Yelp API is currently not available
        6 	Did not understand query 	Indicates an invalid API request was sent
        300 	Invalid phone number 	Indicates that the value of the "phone" parameter was not a valid 10-digit telephone number 
         
        */

    }


    protected string arrayToString(string[] str)
    {
        string temp = "";

        for (int i = 0; i < str.Length; i++)
        {
            if (i == str.Length - 1)
            {
                // temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>";
                temp += "<div class='serviceSpecific'><a href='" + root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(str[i]) + "'>" + str[i] + "</a></div>";
            }
            else
            {
                //temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>" + ", ";
                temp += "<div class='serviceSpecific'><a href='" + root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(str[i]) + "'>" + str[i] + "</a>" + "  </div>";
            }
        }

        //temp = "<b>Services:</b> " + temp;

        return temp;
    }


    protected string arrayToString2(string[] str)
    {
        string temp = "";
        string code;
        HeadingCode hd;


        for (int i = 0; i < str.Length; i++)
        {

            code = _cache2.GetHeadingCodeByKeywordHeadingName(str[i]);
            hd = _cache2.GetHeadingDetails(code);

            if (i == str.Length - 1)
            {
                // temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>.";

                temp += hd.Title.ToLower();

            }
            else if (i == str.Length - 2)
            {
                // temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>.";

                temp += hd.Title.ToLower() + " and ";

            }
            else
            {
                //temp += "<a href='" + root + "consumer/profession.aspx?profession=" + str[i] + "'>" + str[i] + "</a>" + ", ";               

                temp += hd.Title.ToLower() + ", ";
            }
        }

        return temp;
    }



    protected void BindData(WebReferenceSupplier.SEOSupplierData[] LeadData, string category)
    {
        DataListSuppliers.DataSource = null;
        DataListSuppliers.DataBind();
        //LeadData[0].Name;
        //LeadData[0].Phone;
        //LeadData[0].Region;        
        //LeadData[0].Services;
        //LeadData[0].OnDuty
        
        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;

        objPDS.PageSize = ITEM_PAGE;

        objPDS.DataSource = LeadData;

        if (LeadData.Length > 0) // there are leads for this specific period    
        //if (!debug)
        {
            objPDS.CurrentPageIndex = CurrentPage;
            if (objPDS.IsFirstPage)
                lnkPreviousPage.Visible = false;
            else
                lnkPreviousPage.Visible = true;


            if (objPDS.IsLastPage)
                lnkNextPage.Visible = false;
            else
                lnkNextPage.Visible = true;

            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            /*
            if (lnkNextPage.Enabled)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(false);", true);
            */

            indexList++;

            LoadPages(LeadData.Length);

            
            DataListSuppliers.DataSource = objPDS;
            DataListSuppliers.DataBind();

            div_paging.Visible = true;

            /*
            leadsSeperator.Visible = true;
            noRecords.Visible = false;
            leadsTime.Visible = true;
            */
        }

        else
        {
            titleSuppliers.Visible = false;
            div_paging.Visible = false;
        }

    }

    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        //if there is only one page
        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListV = null;
            return;
        }

        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListV = dic;
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell2(kvp.Value, kvp.Key));
    }









    protected string getRoot3
    {
        get
        {
            if (Request.Url.Host == "localhost")
                return Request.Url.Host + ":" + Request.Url.Port + ResolveUrl("~");
            else
                return "http://" + Request.Url.Host;
        }

    }

    HtmlTableCell MakeCell(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();

        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }

    HtmlGenericControl MakeCell2(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }

        //HtmlTableCell htc = new HtmlTableCell();
        HtmlGenericControl li = new HtmlGenericControl("li");
        li.Controls.Add(lb);
        li.Attributes.Add("class", "td_num_pages");

        return li;
    }

    protected string onDuty(string duty)
    {
        if (duty.ToLower() == "true")
            return "<span style='color:#2EA700'>ON DUTY<span>";
        else
            return "<span style='color:#666'>OFF DUTY<span>";
    }

    protected void lnkNextPage_Click(object sender, EventArgs e)
    {

        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;
        listTR = new List<string>();

        BindData(dataV, category);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;
        listTR = new List<string>();
        BindData(dataV, category);
    }
    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
        listTR = new List<string>();
        BindData(dataV, category);
    }

    List<string> listTR
    {
        get { return (Session["list_TR"] == null) ? new List<string>() : (List<string>)Session["list_TR"]; }
        set { Session["list_TR"] = value; }
    }

    protected WebReferenceSupplier.SEOSupplierData[] dataV
    {
        //get { return (ViewState["data"] == null) ? null : (WebReferenceSupplier.AdvDashboardLeadData[])ViewState["data"]; }
        get { return (Session["dataSuppliers"] == null) ? null : (WebReferenceSupplier.SEOSupplierData[])Session["dataSuppliers"]; }
        set { Session["dataSuppliers"] = value; }
    }


    int CurrentPage
    {
        get
        {
            object o = this.ViewState["_CurrentPageAdvertiser"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["_CurrentPageAdvertiser"] = value;
        }
    }


    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }


    public int index
    {
        get
        {
            return (ViewState["index"]==null)? 0 : (int)ViewState["index"];
        }

        set
        {
            ViewState["index"] = value;
        }

    }

    public class supplierItem
    {
        public string phone {get;set; }
        public string address { get; set; }
        public string img { get; set; }
    }

}