﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Quoates2.ascx.cs" Inherits="Controls_seo_Quoates2" %>      

      <div class="titleleads">
        <h3 id="titleleads"  runat="server" class="secondTitle" ></h3>
      </div>       

      <script>
          /* this my function eliminate the default function
          prevent show the unnecessary paramters in the address web page
          when the rewrite add when paging */

          function __doPostBack(eventTarget, eventArgument) {
              if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                  theForm.__EVENTTARGET.value = eventTarget;
                  theForm.__EVENTARGUMENT.value = eventArgument;
                  //alert(theForm.action);
                  theForm.action = theForm.action.substring(0, theForm.action.indexOf('?'));
                  theForm.submit();
              }
          } 
      </script>

    <asp:DataList ID="DataListLeads" runat="server" RepeatDirection="Vertical" CssClass="tblProfessions">
            <ItemTemplate>   
            <%--
              <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#Eval("Name") %>' NavigateUrl='<%#"~/professoin/" + Eval("Name") + "/" + Eval("Id") %>' ></asp:HyperLink>
              --%>              
              <%if (showHeadingTitle) %>
              <%{ %>
              <div class="heading">
              
                <%#Eval("HeadingName") == null ? "" : formatHeading(Eval("HeadingName").ToString())%> 
              </div>
              <%} %>

             
              <div class="description">
                <span class="descriptionNew"><%#IsNew(Eval("IsNew").ToString())%></span> "<%#Utilities.setfirstCapital(Eval("Description").ToString())%>"
              </div>

              <div class="region">
                <span>In <%#Eval("Region")%></span>
                <%#IsNew2(Eval("IsNew").ToString())%>
              </div>              
                    
              <hr  class="seperator" size="1"/>            
             
              <div class="leadSeperator"></div>
             

            </ItemTemplate>
       </asp:DataList>
    

    <asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">        
        <ul>
            <li class="first"><asp:ImageButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click"  ImageUrl="~/Consumer/images/previous.png"/></li>
            
            <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
            <li class="last"><asp:ImageButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click"  ImageUrl="~/Consumer/images/next.png" /></li>
                        
        </ul>
    </asp:Panel>