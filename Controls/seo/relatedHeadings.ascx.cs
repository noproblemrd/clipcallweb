﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Controls_seo_relatedHeadings : System.Web.UI.UserControl
{
    protected PpcSite _cache;
    public string state;
    public string stateAbbreviation;
    public string city;
    public string category;
    protected string region;    
    public eHeadingGroup eheadingGroup;
    public string code;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(city))
            region = city + ", " + stateAbbreviation;
        else
            region = stateAbbreviation;

        _cache = PpcSite.GetCurrent(); 

        
        showHeadings(code);
        Page.DataBind();
    }


    private void showHeadings(string code)
    {
        string[] categoryDisplay = _cache.GetCategoryDisplay(code);

        if (categoryDisplay != null)
        {
            List<string> listHeadingCodes = new List<string>();

            for (int iDisplay = 0; iDisplay < categoryDisplay.Length; iDisplay++)
            {
                listHeadingCodes.AddRange(_cache.GetHeadingCodeByCategoryNameDisplay(categoryDisplay[iDisplay]));
            }

            List<string> listHeadingNames = _cache.GetHeadingNameByCategoryNameDisplay(listHeadingCodes.ToArray());
            List<string> listExcludedHeading = Utilities.getExcludedList(listHeadingNames, category);

            if (listExcludedHeading.Count > 0)
                wrapperInnerRightTitle.Visible = true;

            if (listExcludedHeading.Count > 5)
            {
                listExcludedHeading = Utilities.getRandomList(listExcludedHeading, 5);
            }

            DataTable table = new DataTable();
            table.Columns.Add("heading");
            table.Columns.Add("url");

            for (int i = 0; i < listExcludedHeading.Count; i++)
            {
                DataRow dr = table.NewRow();
                dr["heading"] = listExcludedHeading[i];
                if (!String.IsNullOrEmpty(city))
                    dr["url"] = "~/profession/" + Utilities.cleanSpecialCharsUrlFreindly2(listExcludedHeading[i]) + "/" + state + "/" + city;
                else if (!String.IsNullOrEmpty(state))
                    dr["url"] = "~/profession/" + Utilities.cleanSpecialCharsUrlFreindly2(listExcludedHeading[i]) + "/" + state;
                else
                    dr["url"] = "~/profession/" + Utilities.cleanSpecialCharsUrlFreindly2(listExcludedHeading[i]);

                table.Rows.Add(dr);
            }

            DataListRelatedHeadings.DataSource = table;
            DataListRelatedHeadings.DataBind();
        }
    }

   
}