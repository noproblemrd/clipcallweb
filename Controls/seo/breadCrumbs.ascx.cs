﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Controls_seo_breadCrumbs : System.Web.UI.UserControl
{
    protected string root;
    protected void Page_Load(object sender, EventArgs e)
    {
        root = Parent.ResolveUrl("~");
        Page.DataBind();
    }

    public void setBreadCrumb(string profession)
    {
        root = Parent.ResolveUrl("~");
        breadcrumb2.InnerHtml = "<strong>" + profession + "</strong>";
        breadcrumb2.Visible = true;    

    }

    public void setBreadCrumbBusiness(string business)
    {
        root = Parent.ResolveUrl("~");
        breadcrumb2.InnerHtml = "<strong>" + business + "</strong>";
        breadcrumb2.Visible = true;

    }

    public void setBreadCrumb(string profession, string professionPlural, string state)
    {
        root = Parent.ResolveUrl("~");
        HtmlAnchor linkProfession = new HtmlAnchor();
        //linkProfession.HRef = root + "consumer/profession.aspx?profession=" + profession;
        linkProfession.HRef = root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(profession);
        linkProfession.InnerText = professionPlural;
        breadcrumb2.Controls.Add(linkProfession);
        breadcrumb2.Visible = true;

        arrow2to3.Visible = true;

        breadcrumb3.InnerHtml = "<strong>" + state + "</strong>";
        breadcrumb3.Visible = true;

    }

    public void setBreadCrumb(string profession, string professionPlural , string state, string city)
    {
        root = Parent.ResolveUrl("~");
        HtmlAnchor linkProfession = new HtmlAnchor();
        //linkProfession.HRef = root + "consumer/profession.aspx?profession=" + profession;
        linkProfession.HRef = root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(profession);
        linkProfession.InnerText = professionPlural;
        breadcrumb2.Controls.Add(linkProfession);
        breadcrumb2.Visible = true;

        arrow2to3.Visible = true;

        HtmlAnchor linkSatate = new HtmlAnchor();
        //linkSatate.HRef = root + "consumer/profession.aspx?profession=" + profession + "&state=" + state;
        linkSatate.HRef = root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(profession) + "/" + state;
        linkSatate.InnerText = state;
        breadcrumb3.Controls.Add(linkSatate);
        breadcrumb3.Visible = true;

        arrow3to4.Visible = true;

        breadcrumb4.InnerHtml = "<strong>" + city + "</strong>";
        breadcrumb4.Visible = true;
    }

    public void setBreadCrumb(string profession, string professionPlural , string state, string city, string zipCode)
    {
        root = Parent.ResolveUrl("~");
        HtmlAnchor linkProfession = new HtmlAnchor();
        //linkProfession.HRef = root + "consumer/profession.aspx?profession=" + profession;
        linkProfession.HRef = root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(profession);
        linkProfession.InnerText = professionPlural;
        breadcrumb2.Controls.Add(linkProfession);
        breadcrumb2.Visible = true;

        arrow2to3.Visible = true;

        HtmlAnchor linkSatate = new HtmlAnchor();
        //linkSatate.HRef = root + "consumer/profession.aspx?profession=" + profession + "&state=" + state;
        linkSatate.HRef = root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(profession) + "/" + state;
        linkSatate.InnerText = state;
        breadcrumb3.Controls.Add(linkSatate);
        breadcrumb3.Visible = true;

        arrow3to4.Visible = true;

        HtmlAnchor linkCity = new HtmlAnchor();
        //linkCity.HRef = root + "consumer/profession.aspx?profession=" + profession + "&state=" + state + "&city=" + city;
        linkCity.HRef = root + "profession/" + Utilities.cleanSpecialCharsUrlFreindly2(profession) + "/" + state + "/" + city;
        linkCity.InnerText = city;
        breadcrumb4.Controls.Add(linkCity);
        breadcrumb4.Visible = true;

        arrow4to5.Visible = true;

        breadcrumb5.InnerHtml = "<strong>" + zipCode + "</strong>";
        breadcrumb5.Visible = true;
    }

    public void setBreadCrumbCategoryGroup(string category)
    {
        root = Parent.ResolveUrl("~");
        breadcrumb2.InnerHtml = "<strong>" + category + "</strong>";
        breadcrumb2.Visible = true;     

    }

    protected string getRoot
    {
        get
        {

            string path=ResolveUrl("~");

            string serverName = Request.Url.Host;

            if (serverName == "localhost")
                path = path + "main.aspx";

            return path;

        }

    }


}