﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_seo_dynamicTitle : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void setTitleProfession(string profession)
    {
        professionTitle.InnerText = Utilities.setfirstCapital(profession) + " covering...";
    }

    public void setTitleState(string profession, string abvState, string state)
    {
        professionTitle.InnerText = Utilities.setfirstCapital(profession) + " covering " + abvState + " (" + state + ")";
    }

    public void setTitleCity(string profession, string state, string city)
    {
        professionTitle.InnerText = Utilities.setfirstCapital(profession) + " covering " + city + ", " + state;
    }

    public void setTitleZipCode(string profession, string stateAbbreviation, string city, string zipCode)
    {
        professionTitle.InnerText = Utilities.setfirstCapital(profession) + " covering " + zipCode + " (" + city + ",  " + stateAbbreviation + ")";
    }

    public void setTitleCategoryGroup(string profession)
    {
        professionTitle.InnerText = Utilities.setfirstCapital(profession);
    }

    public void setTitleBusiness(string business)
    {
        professionTitle.InnerText = business;
    }

    public void setTitleError(string err, string profession)
    {
        professionError.Visible = true;
        professionError.InnerText = err + ": " + profession;
    }

    public void setTitleError2(string err)
    {
        professionError.Visible = true;
        professionError.InnerText = err;
    }
}