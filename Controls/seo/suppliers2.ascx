﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="suppliers2.ascx.cs" Inherits="Controls_seo_suppliers2" %>
<div class="titleSuppliers">
    <h3 id="titleSuppliers"  runat="server" class="secondTitle" ></h3>
 </div>  

 <div style="height:2px;background:#7D7D7D"></div>
 

<script>
    function encodeTwitterOrGooglePlus(url) {
        //alert(url + "\n" + url.replace(/%20/g, "%2520"));
        return url.replace(/%20/g, "%2520");
    }

    function hoverSupplier(recordId) {
        alert(recordId);
    }


    /* this my function eliminate the default function
    prevent show the unnecessary paramters in the address web page
    when the rewrite add when paging */
    function __doPostBack(eventTarget, eventArgument) {
        if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
            theForm.__EVENTTARGET.value = eventTarget;
            theForm.__EVENTARGUMENT.value = eventArgument;
            //alert(theForm.action);
            theForm.action = theForm.action.substring(0, theForm.action.indexOf('?'));
            theForm.submit();
        }
    }

    $(document).ready(function () {
        /*
        $('.suppliersDetails').mouseenter(function (evt) {



        $(this).closest('.suppliersDetails').animate({
        backgroundColor: '#6e9518',
        WebkitTransition: 'all 0.1s ease 0s',
        MozTransition: 'all 0.1s ease 0s',
        MsTransition: 'all 0.1s ease 0s',
        OTransition: 'all 0.1s ease 0s',
        transition: 'all 0.1s ease 0s'
        }
        )

        //$(this).closest('.suppliersDetails').css('background', '#6e9518');


        $(this).closest('.suppliersDetails').find('.details1').css('color', 'white');
        $(this).closest('.suppliersDetails').find('.details1 .heading A').css('color', 'white');
        $(this).closest('.suppliersDetails').find('.details1 .services A').css('color', 'white');
        $(this).closest('.suppliersDetails').find('.details2').css('color', 'white');
        $(this).closest('.suppliersDetails').find('.details2 .duty').css('color', 'white');


        }
        )

        $('.suppliersDetails').mouseleave(function (evt) {

        $(this).closest('.suppliersDetails').animate({
        backgroundColor: 'white',
        WebkitTransition: 'all 0.1s ease 0s',
        MozTransition: 'all 0.1s ease 0s',
        MsTransition: 'all 0.1s ease 0s',
        OTransition: 'all 0.1s ease 0s',
        transition: 'all 0.1s ease 0s'
        }
        )

        //$(this).closest('.suppliersDetails').fadeOut(1);
        //$(this).closest('.suppliersDetails').css('background', 'white');
        $(this).closest('.suppliersDetails').find('.details1').css('color', '#333333');
        $(this).closest('.suppliersDetails').find('.details1 .heading A').css('color', '#21A9E5');
        $(this).closest('.suppliersDetails').find('.details1 .services A').css('color', '#21A9E5');
        $(this).closest('.suppliersDetails').find('.details2').css('color', '#333333');
        $(this).closest('.suppliersDetails').find('.details2 .duty').css('color', 'green');
        }
        )
        */
    }
    );
   
</script>


<script type="text/javascript">

    function setStreetView(divStreetView,location) {
        //alert(location + ' ' + divStreetView );
        //var userLocation = 'Ocean Street, Swampscott, MA 01907';
        //var userLocation = '88888';
        //var userLocation = 'MA';
        //var userLocation = 'Albany 12201';
        //var userLocation = 'Department of State 2050 Bamako Place Washington, DC 20521-2050';

        //var userLocation = 'State University of New York Library 1400 Washington Ave Albany NY 12222';

        //var userLocation = '236 8th Ave, New York, NY 10011, USA';
        //var userLocation = '7 Corson Ave, Staten Island, NY 10301, USA';

        //var userLocation = '1381 Utica Ave, Brooklyn, NY 11203, USA';
        var userLocation = location;
        //var userLocation = "MA"; 
        var userPOV = { heading: 120, pitch: 0, zoom: 1 };

        var geocoder = new google.maps.Geocoder();


        /*
        location_type stores additional data about the specified location. The following values are currently supported:

        "ROOFTOP" indicates that the returned result is a precise geocode for which we have location information accurate down to street address precision.
        "RANGE_INTERPOLATED" indicates that the returned result reflects an approximation (usually on a road) interpolated between two precise points (such as intersections). Interpolated results are generally returned when rooftop geocodes are unavailable for a street address.
        "GEOMETRIC_CENTER" indicates that the returned result is the geometric center of a result such as a polyline (for example, a street) or polygon (region).
        "APPROXIMATE" indicates that the returned result is approximate.
        */

        /*

        The following types are supported and returned by the geocoder in both the address type and address component type arrays:

        street_address indicates a precise street address.
        route indicates a named route (such as "US 101").
        intersection indicates a major intersection, usually of two major roads.
        political indicates a political entity. Usually, this type indicates a polygon of some civil administration.
        country indicates the national political entity, and is typically the highest order type returned by the Geocoder.
        administrative_area_level_1 indicates a first-order civil entity below the country level. Within the United States, these administrative levels are states. Not all nations exhibit these administrative levels.
        administrative_area_level_2 indicates a second-order civil entity below the country level. Within the United States, these administrative levels are counties. Not all nations exhibit these administrative levels.
        administrative_area_level_3 indicates a third-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
        administrative_area_level_4 indicates a fourth-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
        administrative_area_level_5 indicates a fifth-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
        colloquial_area indicates a commonly-used alternative name for the entity.
        locality indicates an incorporated city or town political entity.
        sublocality indicates a first-order civil entity below a locality. For some locations may receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality level is a civil entity. Larger numbers indicate a smaller geographic area.
        neighborhood indicates a named neighborhood
        premise indicates a named location, usually a building or collection of buildings with a common name
        subpremise indicates a first-order entity below a named location, usually a singular building within a collection of buildings with a common name
        postal_code indicates a postal code as used to address postal mail within the country.
        natural_feature indicates a prominent natural feature.
        airport indicates an airport.
        park indicates a named park.
        point_of_interest indicates a named point of interest. Typically, these "POI"s are prominent local entities that don't easily fit in another category, such as "Empire State Building" or "Statue of Liberty."




        */


        geocoder.geocode({ 'address': userLocation }, function (results, status) {
            if (window.console)
                console.log("street view: " + userLocation + " " + status);
            if (status == google.maps.GeocoderStatus.OK) {
                // var latLng = results[0].geometry.location;

                var types = results[0].types;
                var blnStreetAccurate = false;
                for (var i = 0; i < types.length; i++) {


                    if (types[i] == "street_address")
                        blnStreetAccurate = true;
                }


                //alert(blnStreetAccurate);

                if (blnStreetAccurate) {
                    //alert("in:" + divStreetView);
                    new google.maps.StreetViewPanorama(document.getElementById(divStreetView),
                                       { position: results[0].geometry.location, pov: userPOV, visible: true, addressControl: false, panControl: false, disableDefaultUI: true });
                    document.getElementById(divStreetView).style.width = '188px';
                    document.getElementById(divStreetView).style.height = '82px';

                }

                else
                    document.getElementById(divStreetView).style.display = 'none';
            } else {
                //alert("Geocode failed. Reason: " + status);
                document.getElementById(divStreetView).style.display = 'none';
            }
        });
    }

    //google.maps.event.addDomListener(window, 'load', init);
 
       

    </script>


<asp:DataList ID="DataListSuppliers" runat="server" RepeatDirection="Vertical" CssClass="tblSuppliers"  OnItemCreated="Item_Created" OnItemDataBound="Item_Bound">
        <ItemTemplate>   
        <%--
            <asp:HyperLink ID="HyperLink1" runat="server" Text='<%#Eval("Name") %>' NavigateUrl='<%#"~/professoin/" + Eval("Name") + "/" + Eval("Id") %>' ></asp:HyperLink>
            --%>
            <div class="suppliersDetails" id="suppliersDetails<%#Eval("Id").ToString()%>"  >              
                   

                   <div class="details1">                

                    
                    <div class="heading">
                        <div id="headingIndex" class="headingIndex" runat="server">1</div>
                        <a href="<%#root%>business/<%#Utilities.cleanSpecialCharsUrlFreindly3(Eval("Name").ToString().Trim())%>/<%#Eval("Id")%>"><%#Utilities.setfirstCapital(Eval("Name").ToString().Trim())%></a>
                    </div>
                    
                    <div class="clear"></div>

                    <div id="region" class="region" runat="server"><%#Eval("Region")%></div>               

                     <div class="duty" runat="server" id="duty">
                        <%#onDuty(Eval("OnDuty").ToString())%>
                      </div> 
                     <div class="services">
                        <%#arrayToString((string[])Eval("Services"))%>

                        <div class="clear"></div> 
                     </div> 
                     
                               
                   </div>
                 
                   <div class="details2"> 

                      <div class="phoneIcon">
                          <a id="linkPhoneIcon" runat="server" href="#">
                              <span class="icon-stack">    
                                    <i class="icon-circle icon-stack-base"></i>
                                    <i class="icon-phone"></i>    
                              </span> 
                          </a>
                      </div>

                      <div class="phone">                    
                         
                        <a  id="linkPhone" runat="server"  href="#" ><%#Utilities.americanFormatPhone(Eval("Phone").ToString())%></a>
                       
                      </div> 
                      
                      <div class="clear"></div>

                      <div  id="supplierImage<%#Eval("Id").ToString()%>" class="supplierImage"><%#LoadLogo(Eval("Id").ToString(),Eval("Region").ToString())%></div>                     
                                               
                   </div>

                   <div class="clear"></div>

                   <!--
                   <div class="reviews" >Recent reviews for Security Public Storage</div>
                   -->

                   <div class="reviews" ><%#getYelpDatails(Eval("Phone").ToString(), Eval("Name").ToString().Trim())%></div>

                   <div class="boxDynamic">
                        <div class="boxDynamic1">
                            <div class="boxDynamicCell close">
                            <span class="icon-stack" onclick="closeDetails(this);">
                                <i class="icon-circle icon-stack-base"></i>
                                <i class="icon-remove"></i>
                            </span>                        
                            </div>

                            <div class="boxDynamicCell details">
                                <div class="boxDynamicName"><a href="" id="boxDynamicNameLink"></a></div>

                                <div class="boxDynamicPhone" id="boxDynamicPhone"></div>
                            </div>
                        </div>                       

                        <div class="boxDynamic2">
                            <div class="boxDynamicCell icons">
                                <div class="iconsRow">
                                    <div class="iconFrame" id="iconFrameSms" onclick="openMobile(this);">
                                        <div class="iconFrameText" style="display: none;">Send to my phone</div>
                                        <div class="divIconPhoneImage"><img src="<%#url%>Consumer2/images/sms.png"></div>   
                                        <input id="hdn_supplierId" type="hidden" runat="server" />
                                        <input id="hdn_supplierUrl" type="hidden" runat="server" />
                                    </div>
                                </div>
                            
                                <!--
                                <div class="iconsRow">
                                    <div class="iconFrame">
                                        <div class="iconFrameText" id="iconFramePhone" style="display: none;">Arrange a call back</div>
                                        <i class="icon-phone"></i>
                                    </div>
                                </div>
                                -->
                                <%--
                                <a onClick="RefreshParent('<%# DataBinder.Eval(Container.DataItem, "shop")%>')">
                                <%# DataBinder.Eval(Container.DataItem, 'shopname')%>
                                </a>
                                --%>

                                <div class="iconsRow">
                                    <div class="iconFrame" id="iconFrameEdit" runat="server" >
                                        <div class="iconFrameText" style="display: none;">Edit this number</div>
                                        <i class="icon-pencil"></i>
                                    </div>
                                </div>


                            </div>
                        </div>
                       
                    </div>

            </div>
             <!--
             <div class="pas"></div>
             -->

             <!--
             <div class="pas"><hr class="seperator" size="1" /></div>
             -->

             

        </ItemTemplate>

       
    </asp:DataList>
    

<asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">        
    <ul>
        <li class="first"><asp:ImageButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click"  ImageUrl="~/Consumer/images/previous.png"/></li>
            
        <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
        <li class="last"><asp:ImageButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click"  ImageUrl="~/Consumer/images/next.png"/></li>
                        
    </ul>
</asp:Panel>