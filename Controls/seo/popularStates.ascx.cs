﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Controls_seo_popularStates : System.Web.UI.UserControl
{
    protected PpcSite _cache;
    public string categoryAddress;

    protected void Page_Load(object sender, EventArgs e)
    {
        _cache = PpcSite.GetCurrent();
        showRegions();
    }


    protected void showRegions()
    {

        try
        {

            if (_cache.states.Count > 0)
            {
                
                DataTable dtFewCities = new DataTable();
                dtFewCities.Columns.Add("fullName");               

                for (int i = 0; i < _cache.states.Count; i++)
                {
                    if(_cache.states[i].isPopular)
                    {
                        DataRow dr = dtFewCities.NewRow();
                        dr["fullName"] = _cache.states[i].fullName;                  
                        dtFewCities.Rows.Add(dr);
                    }
                }



                DataListPopularStatesRightFew.DataSource = dtFewCities;
                DataListPopularStatesRightFew.DataBind();
                
            }

            DataListPopularStatesRightAll.DataSource = _cache.states;
            DataListPopularStatesRightAll.DataBind();
        }

        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, _cache.SiteId);
            return;
        }


    }
}