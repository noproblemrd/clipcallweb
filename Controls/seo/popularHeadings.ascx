﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="popularHeadings.ascx.cs" Inherits="Controls_seo_popularHeadings" %>
<div class="wrapperInnerRightTitle" runat="server" id="wrapperInnerRightTitle"><%#setTitle(region)%></div>

<div id="wrapperInnerRightPopularHeadingsContent" class="wrapperInnerRightPopularCitiesFew">
    <asp:DataList ID="DataListPopularHeadings" runat="server" RepeatColumns="1" RepeatDirection="Vertical" CssClass="tblCities">
    <ItemTemplate>               
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("url")%>'><%#Eval("heading")%></asp:HyperLink>     
    </ItemTemplate>
</asp:DataList>
               
</div>
   
