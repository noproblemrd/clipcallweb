<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FromToDate.ascx.cs" Inherits="FromToDate" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit"
 TagPrefix="cc1" %>
<script type="text/javascript">
    Date.prototype.addMonth = function (n) {this.setMonth (this.getMonth () + n)}
    Date.prototype.addDays = function (n) {this.setDate (this.getDate () + n)}
    function validateDates()
    {   
        if(!validate_Date(document.getElementById("<%#form_from.ClientID%>").value, 
                document.getElementById('<%# Hidden_DteFormat.ClientID%>').value))
        {
      //      alert('1');
            document.getElementById("<%#form_from.ClientID%>").focus();
            document.getElementById("<%#form_from.ClientID%>").select();
            return false;
        }
        
        
        if(!validate_Date(document.getElementById("<%#form_to.ClientID%>").value,
                document.getElementById('<%# Hidden_DteFormat.ClientID%>').value))
        {
    //        alert('2');
            document.getElementById("<%#form_to.ClientID%>").focus();
            document.getElementById("<%#form_to.ClientID%>").select();
            return false;
        }
        try {MakeBeforeAction();}
        catch(ex){}
        return true;
    }
     function showTableBeforeFlash(sender,args)
    {
 //       alert(sender);
         set_div(sender);
  //      document.getElementById("tblBeforeFlash").style.display='inline'; 
    }
    function hideTableBeforeFlash()
    {
 //       document.getElementById("tblBeforeFlash").style.display='none';
        _CheckDateFrom();
    }    
    
    function showCalender(_calender)
    {
        var calender=$find(_calender);
        calender.show();
    }
    function HideCalender(_calender, control)
    {
        
        var dateMessage = document.getElementById('<%#lbl_wrongDate.ClientID %>').value;
        var calender=$find(_calender);  
        var dateFormat = document.getElementById('<%#Hidden_DteFormat.ClientID  %>').value;        
        Hide_Calender(dateMessage, calender, dateFormat, control);
 //      _CheckDateFrom();
        var _format = document.getElementById('<%# Hidden_DteFormat.ClientID%>').value;
        var start=$get("<%# form_from.ClientID %>");
        var end=$get("<%# form_to.ClientID %>");      
        var _start = GetDateFormat(start.value, _format);       
        var _end = GetDateFormat(end.value, _format);
      
        if(_start>_end)
        {
    //        alert(typeof  _start);
            //alert("endId= "+end.id+" startId = "+start.id+" control = "+control.id);
            if(start.id == control.id)
            {
      //      alert(GetDateString(new Date(_start), dateFormat));
                end.value = GetDateString(new Date(_start), dateFormat);
      //          alert("sof= "+end.value);
                
            }
            else
            {
                start.value = GetDateString(new Date(_end), dateFormat);
      //          alert('6');
            }
        }
    }
    function ClearDates()
    {
   
        document.getElementById('<%# form_from.ClientID %>').value="";
        document.getElementById('<%# form_to.ClientID %>').value="";  
        var _panel=document.getElementById('<%# _dateError.ClientID %>');
        try{
            _panel.style.display="none";
        }catch(ex){}
        try{
        $find("WaterTo").set_Text("");
        $find("WaterFrom").set_Text("");
        }
        catch(ex){}
    
    }
    function _CheckDateFrom()
    {       
//        alert("check");
        var _format = document.getElementById('<%# Hidden_DteFormat.ClientID%>').value;
        var start=$get("<%# form_from.ClientID %>").value;
        var end=$get("<%# form_to.ClientID %>").value;      
        var _start = GetDateFormat(start, _format);       
        var _end = GetDateFormat(end, _format);          
        if(_start>_end)
            alert('<%#lbl_dateError.Text%>');
        
        WebServiceSite.SetDate(start, end, 
            function (arg)
            {
                var start2=$get("<%# form_from.ClientID %>").value;
                var end2=$get("<%# form_to.ClientID %>").value;
                var _start2 = GetDateFormat(start2, _format);       
                var _end2 = GetDateFormat(end2, _format);
                if(_start2 == _start && _end2 == _end)
                    OnCompleteChk(arg);
            }
            , OnError, OnTimeOut);
    }
    function OnCompleteChk(arg)
    {     
       
         var waterTo=$find("WaterTo");          
         var waterFrom=$find("WaterFrom");      
         var dates=arg.split(";");
         waterTo.set_Text(dates[0]);
         if(dates.length>1)                     
         {
            waterFrom.set_Text(dates[1]);
         }       
           
    }
    function OnTimeOut(arg)
    {
//	    alert("timeOut has occured");
    }

    function OnError(arg)
    {
//	    alert("error has occured: " + arg._message);
    }
    function ChkValidation(exp, txt)
    {
        
        var _reg=new RegExp(exp);
        var _txt = document.getElementById(txt).value;
         _txt=_txt.trim();
         if(_txt.length==0)
            return true;
        if(_txt.search(_reg)==-1)
            return false;
        return true;
    }
 //   window.onload=DefaultTime;
    function DefaultTime()
    {
       
        var form_from = document.getElementById("<%# form_from.ClientID %>");
        var form_to = document.getElementById("<%# form_to.ClientID %>");
        var _format = document.getElementById('<%# Hidden_DteFormat.ClientID%>').value;
    //    alert("DefaultTime" + form_from.value);

        if(form_from.value == _format || form_from.value.length == 0) 
        {
            var from_date = new Date();
            from_date.addMonth(-1);
            form_from.value = GetDateString(from_date, _format);
        }
        if (form_to.value == _format || form_to.value.length == 0) {
            var to_date = new Date();
            form_to.value = GetDateString(to_date, _format);
        }
    }
    function SetDateDayDifference(days) {
        var form_from = document.getElementById("<%# form_from.ClientID %>");
        var form_to = document.getElementById("<%# form_to.ClientID %>");
        var _format = document.getElementById('<%# Hidden_DteFormat.ClientID%>').value;
        var from_date = new Date();
        from_date.addDays(-1*days);
        form_from.value = GetDateString(from_date, _format);
        var to_date = new Date();
        form_to.value = GetDateString(to_date, _format);

    }
    function GetDateNow()
    {
        return document.getElementById("<%# hf_DateNow.ClientID %>").value;
    }
   

</script>

<div class="dates2">
<div class="label">
    <asp:Label ID="lbl_fromFate" runat="server"  Text="Creation Date From"></asp:Label>
</div>
    <div class="fromdate">
        
        
	    
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="form_from" EnableViewState="true" WatermarkText="mm/dd/yyyy" BehaviorID="WaterFrom"></cc1:TextBoxWatermarkExtender>
        <asp:TextBox ID="form_from" runat="server" CssClass="form-textcal" autocomplete="off" onfocus="javascript:showCalender('CalendarPopupFrom');" onblur="javascript:HideCalender('CalendarPopupFrom', this);"></asp:TextBox>
        <cc1:CalendarExtender ID="CalendarExtender1" PopupPosition="BottomRight" runat="server" TargetControlID="form_from" PopupButtonID="a_fromDate" Format="MM/dd/yyyy" OnClientShown="showTableBeforeFlash"  OnClientHidden="hideTableBeforeFlash" BehaviorID="CalendarPopupFrom"></cc1:CalendarExtender>
    </div>
    </div>
    <div class="dates2">
  <div class="label">
    <asp:Label ID="lbl_toDate" runat="server" Text="To"></asp:Label>
  </div>
    <div class="fromdate">
        
	    
	    <cc1:TextBoxWatermarkExtender  ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="form_to" EnableViewState="true" WatermarkText="mm/dd/yyyy" BehaviorID="WaterTo"></cc1:TextBoxWatermarkExtender>
        <asp:TextBox ID="form_to" runat="server" CssClass="form-textcal" autocomplete="off" onfocus="javascript:showCalender('CalendarPopupTo');" onblur="javascript:HideCalender('CalendarPopupTo', this);"></asp:TextBox>
       
	    <cc1:CalendarExtender ID="CalendarExtender2" PopupPosition="BottomRight" runat="server"  TargetControlID="form_to" PopupButtonID="a_toDate" Format="MM/dd/yyyy" OnClientShown="showTableBeforeFlash" OnClientHidden="hideTableBeforeFlash" BehaviorID="CalendarPopupTo"></cc1:CalendarExtender>
        
        <asp:HiddenField runat="server" id="lbl_wrongDate" Value="Wrong date format. The right one is"/>
    </div>
</div>
<div class="clear"></div>
<asp:Button ID="btnSubmit" CssClass="CreateReportSubmit2" runat="server" Text="Run Report" OnClick="btnSubmit_Click"/>

<div class="error_msg">
    <asp:Panel ID="_dateError" runat="server" CssClass="error-msg" Visible="false"><br />
        <asp:Label ID="lbl_dateError" runat="server" Text="From Date should be sooner or same as &#34;to date&#34;"></asp:Label>
    </asp:Panel>
    <asp:HiddenField ID="Hidden_DteFormat" runat="server" />
    <asp:HiddenField ID="hf_DateNow" runat="server" />
</div>


