﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_MasterPageConsumer : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.DataBind();
    }
    protected string GetStyle
    {
        get { return ResolveUrl("~") + "PPC/samplepPpc.css"; }
    }
    protected string GetHeaderStyle
    {
        get { return ResolveUrl("~") + "PPC/HeaderStyle.css"; }
    }
}
