﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_PpcComment2 : System.Web.UI.UserControl
{
    public string myStyle;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page is RegisterPage)
        {
            RegisterPage rp = (RegisterPage)Page;
            MultipleComment = HttpUtility.JavaScriptStringEncode(rp.MissingInfo);
            GeneralError = HttpUtility.JavaScriptStringEncode(rp.ServerError);
        }
        this.DataBind();
    }
    public void SetServerComment(string comment)
    {
        lt_ServerComment.Text = comment;
        ShowComment();
    }
    public void SetServerComment(string comment, List<KeyValuePair<string, string>> list)
    {
        string sen = comment;
        for (int i = 0; i < list.Count; i++)
        {
            string anchor = "<a href='" + list[i].Value + "'>" + list[i].Key + "</a>";
            sen = sen.Replace("{" + i + "}", anchor);
        }
        lt_ServerComment.Text = sen;
        ShowComment();
    }
    void ShowComment()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowComment", "ShowComment();", true);
    }
    public string MultipleComment { get; set; }
    public string GeneralError { get; set; }
}