﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_plan : System.Web.UI.UserControl
{
    PageSetting ps;
    protected string supplierId {get;set; }
    protected void Page_Load(object sender, EventArgs e)
    {       

        ps = (PageSetting)Page;
        if (!IsPostBack)
        {
            
            supplierId = ps.GetGuidSetting();
            loadPlan();
        }
    }

    protected void loadPlan()
    {

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfString resultOfString = new WebReferenceSupplier.ResultOfString();

        try
        {
            resultOfString = _supplier.Checkout_GetPlan_Registration2014(new Guid(supplierId));
            string plan = resultOfString.Value;

            
            if (plan == "LEAD_BOOSTER_AND_FEATURED_LISTING")
            {
                plan = "Lead Booster";
            }
            


            else if (plan == "FEATURED_LISTING")
            {
                plan = "Featured Listing";
            }

            lblPlan.InnerText = plan;
        }

        catch
        {
        }

        
    }


   
}