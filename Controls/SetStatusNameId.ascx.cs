﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml.Linq;

public partial class Publisher_SetStatusNameId : System.Web.UI.UserControl, IPostBackEventHandler
{
    private const string SET_STATUS = "SET_STATUS";
    string ExcelName;
    WebReferenceSite.eTableType _eTableType;
    PageSetting ps;
    public void _OnLoadComplete()
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
  //      base.OnLoadComplete(e);
    }
    public void SetStatusType(string _ExcelName, WebReferenceSite.eTableType ___eTableType, string _PageName)
    {
        _eTableType = ___eTableType;
        ExcelName = _ExcelName;
        lblSubTitle.Text = _PageName;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ps = (PageSetting)Page;
        (ScriptManager.GetCurrent(Page)).RegisterAsyncPostBackControl(btn_Set);
        if (!IsPostBack)
        {
            DBConnection.LoadTranslateToControl(this, "SetStatusNameId.ascx", ps.siteSetting.siteLangId);

            SetToolBox();
            popUpaddUser.DataBind();
            LoadReasons();
            this.DataBind();
        }
        SetToolBoxEvents();
        
    }

    private void LoadReasons()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(ps);
        WebReferenceSite.ResultOfListOfTableRowData result = null;
        try
        {
            result = _site.GetTableData(_eTableType);

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, ps.siteSetting.GetSiteID);            
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }


        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            if (!Page.ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        DataTable data = new DataTable();
        data.Columns.Add("ID", typeof(string));
        data.Columns.Add("GuidId", typeof(string));
        data.Columns.Add("Name", typeof(string));

        foreach (WebReferenceSite.TableRowData trd in result.Value)
        {

            if (trd.Inactive)
                continue;
            DataRow row = data.NewRow();
            row["GuidId"] = trd.Guid.ToString();
            row["ID"] = trd.Id;
            row["Name"] = trd.Name;
            data.Rows.Add(row);
        }
        dataV = data;
    }
    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
        Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
    }

    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        bool IsSelect = false;
        int indx = -1;
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
            {
                if (IsSelect)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ChooseOne", "alert('" + Toolbox1.ChooseOne + "');", true);
                    return;
                }
                IsSelect = true;
                indx = row.RowIndex;
            }
        }
        if (indx > -1)
        {
            GridViewRow row = _GridView.Rows[indx];
            hf_guid.Value = ((Label)row.FindControl("lbl_GuidId")).Text;
            txt_ID.Text = ((Label)row.FindControl("lbl_CodeId")).Text;
            txt_Name.Text = ((Label)row.FindControl("lbl_ReasonName")).Text;
        }
        popUpaddUser.Attributes["class"] = "popModal_del";
        _mpe.Show();
    }

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        List<Guid> list = new List<Guid>();
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            if (cb.Checked)
            {
                Guid _id = new Guid(((Label)row.FindControl("lbl_GuidId")).Text);
                list.Add(_id);
            }
        }
        if (list.Count == 0)
            return;
        if (list.Count == dataV.Rows.Count)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CantDelete", @"alert(""" + lbl_CantDelete.Text + @""");", true);
            return;
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(ps);
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.DeleteTablesData(_eTableType, list.ToArray());
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        LoadReasons();
        _btn_Search();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataViewV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            return;
        }
        Session["data_print"] = dataViewV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        if (dataViewV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordExport + "');", true);
            return;
        }
        ToExcel to_excel = new ToExcel(ps, ExcelName);
        if (!to_excel.ExecExcel(dataViewV, _GridView))
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void Toolbox1_AddExec(object sender, EventArgs e)
    {

    }

    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        _btn_Search();
    }
    private void _btn_Search()
    {
        string name = Toolbox1.GetTxt;
        HtmlInputText _tb = Toolbox1.GetTextBoxSearch;
        
        if (string.IsNullOrEmpty(name))
            dataViewV = dataV;
        else
        {
            name = name.ToLower();
            DataTable data = dataV;
            data.TableName = "Search";

            EnumerableRowCollection<DataRow> query = from Search in data.AsEnumerable()
                                                     where ((Search.Field<string>("Name").ToLower().Contains(name))
                                                        || (Search.Field<string>("ID").ToLower().Contains(name)))
                                                     orderby Search.Field<string>("Name")
                                                     select Search;


            dataViewV = (query.Count() == 0) ? null : query.CopyToDataTable();
        }
        _GridView.PageIndex = 0;
        SetGridView();
    }
    void SetGridView()
    {
        StringBuilder sb = new StringBuilder();
        DataView data = dataViewV.AsDataView();
        //    _GridView.PageIndex = 0;
        _GridView.DataSource = data;
        _GridView.DataBind();

        if (data.Count == 0)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "noreasultSearch", "alert('" + lbl_noreasultSearch.Text + "');", true);
            CBsV = string.Empty;
            _updatePanelTable.Update();
            return;
        }
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }

        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        _updatePanelTable.Update();
    }
    protected void btn_Set_Click(object sender, EventArgs e)
    {

        _Set_Status();
    }
    void _Set_Status()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(ps);
        WebReferenceSite.UpsertTableDataRequest _request = new WebReferenceSite.UpsertTableDataRequest();
        Guid _guid = (string.IsNullOrEmpty(hf_guid.Value)) ? Guid.Empty : new Guid(hf_guid.Value);
        _request.Guid = _guid;
        _request.Type = _eTableType;
        _request.Name = txt_Name.Text.Trim();
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.UpsertTableData(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ClearPopUp", "ClearPopUp();", true);
        _mpe.Hide();
        LoadReasons();
        _btn_Search();
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView();
    }
    private void SetToolBox()
    {
        Toolbox1.RemoveAttach();
        string script = "openPopup(); return false;";
        Toolbox1.SetClientScriptToAdd(script);
        Toolbox1.SetConfirmDelete = lbl_Delete.Text;
    }


    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    private DataTable dataViewV
    {
        get { return (Session["dataView"] == null) ? new DataTable() : (DataTable)Session["dataView"]; }
        set { Session["dataView"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }


    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        if (eventArgument == SET_STATUS)
            _Set_Status();
    }
    public string Get_tab_click()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(this, SET_STATUS);
        myPostBackOptions.PerformValidation = false;
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);

    }
    
    #endregion
}