﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="statusBar.ascx.cs" Inherits="Controls_statusBar" %>
<div class="statusBar" id="Status_Bar" runat="server" visible="false">
    <div id="statusBarChoosePlan" class="statusBarChoosePlanOn" runat="server">
        <span id="statusBarChoosePlanNumber" class="statusBarNumber" runat="server">1</span>
        <span id="statusBarChoosePlanNumberPast" class="statusBarNumberPast" runat="server"></span>                        
        <span id="statusBarChoosePlanDesc" class="statusBarDesc statusBarDescPast" runat="server">Choose plan</span>
    </div>

    <div id="statusBarBusinessDetails" class="statusBarBusinessDetailsOn" runat="server">
        <span id="statusBarBusinessDetailsNumber" class="statusBarNumber" runat="server">2</span>
        <span id="statusBarBusinessDetailsNumberPast" class="statusBarNumberPast" runat="server"></span>
        <span id="statusBarBusinessDetailsDesc" class="statusBarDesc" runat="server">Enter business details</span>
    </div>

    <div id="statusBarCheckout" class="statusBarCheckoutOn" runat="server">
        <span id="statusBarCheckoutNumber" class="statusBarNumber" runat="server">3</span>
        <span id="statusBarCheckoutNumberPast" class="statusBarNumberPast" runat="server"></span>
        <span id="statusBarCheckoutDesc" class="statusBarDesc" runat="server">Checkout</span>
    </div>
</div>