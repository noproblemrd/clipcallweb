﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegisterB.ascx.cs" Inherits="Controls_Registrtation_RegisterB" %>


<div class="flavorB">
<div class="registration joinus ">

    

    <div class="registrationTitle">
        <div id="registrationContainerVideoIframe" class="registrationContainerVideoIframe">       
            <div style="" class="registrationVideoIframe" id="registrationVideoIframe">
	            <div>
                    <a href="javascript:showHideVideo2();" class="x-close"></a> 
                </div>
            </div>
        </div>

        <div class="clear"></div>

         <div class="registrationTitle1">GROW YOUR BUSINESS!</div>
         <div class="registrationTitleGeneral registrationTitle1 registrationTitle1_2">...WITH REAL-TIME PHONE LEADS</div>       
                       
    </div>

    <div class="clear"></div>  

    <div class="registrationLeft">
     
        <div class="registrationLeftContainer">
        <div class="registrationLeftContainerTitle">
            BEGIN YOUR FREE
            PAY-PER-CALL TRIAL
        </div>
        
    
       <div class="socialNetworks">

        <span class="icon-stack icon-stackFacebook">
            <a onclick="faceBookLogIn2();" href="javascript:;">
                Sign up with
                <i class="icon-facebook"></i>
            </a>
        </span>      

        <span id="googleCustomSignIn" class="icon-stack icon-stackGooglePlus">            
            <a href="javascript:void(0);"> 
                <span class="googlePlusText">Sign up with</span>               
                <i class="icon-google-plus"></i>
            </a>
        </span>

       

    </div>

       <div class="clear"></div>       

       <div class="or">Or</div>

       <div class="registrationInputs" >    
        
            

            <div class="emailContainer">            
            <div class="inputParent">
                <input id="txt_email" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="email" />
                <div id="div_EmailSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_EmailError" class="inputError" style=""> </div>

                

                <div class="inputValid" style="display:none;"  id="div1"></div>

                <div id="div_EmailComment" style="display:none;" class="inputComment"><%#EmailError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

                <div id="div_EmailComment2" style="display:none;" class="inputComment inputComment2"><%#EmailError2%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        <div class="passwordContainer">            
            <div class="inputParent">
                <input id="txt_password" type="password" class="textActive" runat="server" HolderClass="place-holder" text-mode="pelephone" ifCheckPassword="yes"/>

                <div id="div_PasswordSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_PasswordError" class="inputError" style=""> </div>

                <div class="inputValid" style="display:none;"  id="div2"></div>

                <div id="div_PasswordComment" style="display:none;" class="inputComment"><%#PasswordError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        </div>
   
       
       

        <div> 
  </div>
   
       <div class="clear"></div>

       <div class="containerLogin">

            <div class="loginCell registerLogin">
                 <section class="progress-demo">
                    <a id="linkRegistration" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" button-mode="phone" onclick="javascript:preCheckRegistrationStep('regularRegister');">
                        <span class="ladda-label">Sign up</span><span class="ladda-spinner">
                        </span><div class="ladda-progress" style="width: 138px;"></div>
                    </a>
                 </section>               
            </div>          

            <div class="loginCell alreadyRegistered">
                Already registered? <span class="spanLinkLogin"><a href="<%#ResolveUrl("~")%>ppc/ppcLogin.aspx">Login</a></span>
            </div>
        
             

        </div>
            <div class="registrationLeftContainerBottom"></div>

        </div>
    </div>
   
    <div class="registrationRight"> 
    
        <div class="registrationRightContent1">
                Sign up now and receive
                <br />
                $30 worth of prepaid
                <br />
                phone leads!
        </div>
       
        <div class="registrationRightContent2">
            All we need are a few details and your credit card
            <br />
            info (don't worry, your card won’t be charged unless you
            <br />
            choose to continue advertising with us past the trial).
            <br />
            Let’s begin your trial! <span class="setBold">Sign up now!</span>
        </div>

        <div class="registrationContainerVideo">
            <div class="arrow01"></div>
            <div class="titleThird registrationContainerVideoTitle">
                Here’s how it works
            </div>            
            <div onclick="javascript:showHideVideo2('http://www.youtube.com/v/97q0JRDvuik?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1');" class="registrationVideo"></div>              
        </div>

    </div>
    
    
</div>

<div class="seperateHorizontal" ></div>

<div class="registrationMiddle">
    <div class="registrationMiddleTitle">
        <div class="registrationMiddleTitle1">Growing Your Business</div>
        
        <div class="registrationMiddleTitle2">With PAY-PER-CALL Is As Easy As 1-2-3:</div>
        
    </div>

    <div class="registrationMiddleContents">
        <div class="registrationMiddleContent">
            <div class="registrationMiddleImg registrationMiddleImg1">
                <div class="white-circle"><div class="white-circle-number">1</div></div>

            </div>

            <div class="registrationMiddleText">
                <span>Browser form displays in local
                <br />
                web search-</span> Consumers 
                <br />
                complete the form and ask to
                <br />
                speak to available businesses.              
            </div>

        </div>

        <div class="registrationMiddleContent">
            <div class="registrationMiddleImg registrationMiddleImg2">
                <div class="white-circle"><div class="white-circle-number2">2</div></div>
            </div>

             <div class="registrationMiddleText">
                <span>You decide what to pay-</span> Assess 
                <br />
                the job, decide what the lead is
                <br />
                worth, and place your bid - all via
                <br />
                our mobile phone app.               
            </div>           

        </div>

        <div class="registrationMiddleContent registrationMiddleContentLast">
            <div class="registrationMiddleImg registrationMiddleImg3">
                <div class="white-circle"><div class="white-circle-number2">3</div></div>
            </div>

            <div class="registrationMiddleText">
                <span>Speak to customers ready to
                <br />
                spend-</span> Bid winner speaks to 
                <br />
                consumer within <u>seconds</u> of 
                <br />
                their web search. Voice
                <br />
                connection = high conversion.
              
            </div>

        </div>

    </div>

    <div class="seperateHorizontal" ></div>

    <div class="improvement">
    <div class="registrationMiddleTitle">
        <div class="registrationMiddleTitle1">            
            Clicks don’t increase profits (except Google’s...)      
        </div>
        
        <div class="registrationMiddleTitle1 registrationMiddleTitle1_2">       
            and Yellow Pages are dead 
        </div>

        <div class="registrationMiddleTitle2">With pay-per-call you’ve got nothing to lose and lots of customers to gain.</div>
        
        <div class="registrationMiddleTitle3">Here’s why...</div>
    </div>    
    <div class="points">

    <div class="pointsLeft">

        <div class="point">

            <div class="pointIcon pointIcon1">
            
            </div>

            <div class="pointContent pointContent2line">
                <div class="pointContentTitle">Phone leads are most likely to result in a booked job.</div>

                <div class="pointContentText">
                    <div>Phone calls convert in a way that clicks only dream of - a full 35% of your calls should result in</div>
                    <div>paid work!</div>
                </div>
            </div>
       
        </div>

        <div class="clear"></div>

        <div class="point">
            <div class="pointIcon pointIcon2">
            
            </div>

            <div class="pointContent pointContent2line">
                <div class="pointContentTitle">You only pay for the phone leads you accept.</div>
                <div class="pointContentText">
                    <div>If no one calls, or you don’t accept the calls you get, you don’t pay a cent. No set-ups or hidden</div>
                    <div>fees. All you need is a credit card to get started.</div>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="point">
            <div class="pointIcon pointIcon3">
            </div>

            <div class="pointContent pointContent1line">
                <div class="pointContentTitle">Complete control. Powerful reports.</div>

                <div class="pointContentText">
                    <div>You decide the budget, where you do business and when. Track your number of calls, the</div>  
                    <div>details of each and your profits.</div>          
                </div>
            </div>
        </div>

    </div>



     </div>
    </div>

    <div class="clear"></div>
    


 </div>


 <input type="hidden" id="hdn_flavor" value="flavor B"/>
 </div>