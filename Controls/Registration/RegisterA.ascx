﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegisterA.ascx.cs" Inherits="Controls_Registrtation_RegisterA" %>

<div class="registration joinus">

    <div id="registrationContainerVideoIframe" class="registrationContainerVideoIframe">       
        <div style="" class="registrationVideoIframe" id="registrationVideoIframe">
	        <div>
                <a href="javascript:showHideVideo2();" class="x-close"></a> 
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="registrationLeft">

        <div class="titleFirst">
        <span>Join NoProblem</span>
    </div>

        <div class="connectWith">
            <span>Connect with</span>
        </div>
        
    
       <div class="socialNetworks">

        <span id="googleCustomSignIn" class="icon-stack icon-stackGooglePlus">
            
            <a href="javascript:void(0);">
                <i class="icon-circle icon-stack-base" style="color: rgb(210, 71, 55);"></i>
                <i class="icon-google-plus"></i>
            </a>
        </span>

        <span class="icon-stack icon-stackFacebook">
            <a onclick="faceBookLogIn2();" href="javascript:;">
                <i class="icon-circle icon-stack-base" style="color: rgb(59, 89, 153);"></i>
                <i class="icon-facebook"></i>
            </a>
        </span>      

    </div>

       <div class="clear"></div>       

       <div class="registrationInputs" >    
        
            

             <div class="emailContainer">
            <div>Email</div>  
            <div class="inputParent">
                <input id="txt_email" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="email" />
                <div id="div_EmailSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_EmailError" class="inputError" style=""> </div>

                

                <div class="inputValid" style="display:none;"  id="div1"></div>

                <div id="div_EmailComment" style="display:none;" class="inputComment"><%#EmailError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

                <div id="div_EmailComment2" style="display:none;" class="inputComment inputComment2"><%#EmailError2%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        <div class="passwordContainer">
            <div>Password</div>
            <div class="inputParent">
                <input id="txt_password" type="password" class="textActive" runat="server" HolderClass="place-holder" text-mode="pelephone" ifCheckPassword="yes"/>

                <div id="div_PasswordSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_PasswordError" class="inputError" style=""> </div>

                <div class="inputValid" style="display:none;"  id="div2"></div>

                <div id="div_PasswordComment" style="display:none;" class="inputComment"><%#PasswordError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        </div>
   
       
       

        <div> 
  </div>
   
       <div class="clear"></div>

       <div class="containerLogin">
            <div class="loginCell alreadyRegistered">
                Already registered? <span class="spanLinkLogin"><a href="ppcLogin.aspx">Login</a></span>
            </div>
        
            <div class="loginCell registerLogin">
                 <section class="progress-demo">
                    <a id="linkRegistration" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" button-mode="phone" onclick="javascript:preCheckRegistrationStep('regularRegister');">
                        <span class="ladda-label">Next</span><span class="ladda-spinner">
                        </span><div class="ladda-progress" style="width: 138px;"></div>
                    </a>
                 </section>               
            </div>            

        </div>
    
    </div>
   
    <div class="registrationRight">        
        <div class="registrationContainerVideo">
            <div class="titleThird registrationContainerVideoTitle">
                Watch our video
            </div>            
            <div onclick="javascript:showHideVideo2('http://www.youtube.com/v/97q0JRDvuik?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1');" class="registrationVideo"></div>              
        </div>

    </div>
    
    
</div>

<div class="seperateHorizontal" ></div>

<div class="registrationMiddle ">
    <div class="titleThird registrationMiddleTitle ">How NoProblem works?</div>

    <div class="registrationMiddleContents">
        <div class="registrationMiddleContent">
            <div class="registrationMiddleImg">
                <img src="images/howWorks1.png" />
            </div>

            <div class="registrationMiddleText">
                We find ready-to-spend customers
                <br />
                through our online affiliates and 
                <br />
                partner network.
            </div>

        </div>

        <div class="registrationMiddleContent">
            <div class="registrationMiddleImg">
                <img src="images/howWorks2.png" />
            </div>

             <div class="registrationMiddleText">
                You decide how much the call is
                <br />
                worth to you after hearing the 
                <br />
                request.
            </div>           

        </div>

        <div class="registrationMiddleContent registrationMiddleContentLast">
            <div class="registrationMiddleImg">
                <img src="images/howWorks3.png" />
            </div>

            <div class="registrationMiddleText">
                We put you through to the
                <br />
                customer. You pay only for this
                <br />
                call.
            </div>

        </div>

    </div>

    <div class="seperateHorizontal" ></div>

    <div class="improvement">
    <div class="whatsNew titleThird">
    What’s new?
    </div>    
    <div class="points">

    <div class="pointsLeft">

        <div class="point">

            <div class="pointIcon"><img src="images/pay.jpg"></div>

            <div class="pointContent pointContent2line">
                <div class="pointContentTitle">Pay only for leads you accept</div>

                <div class="pointContentText">
                    <div>We wont charge you when you are not</div>
                    <div>available to accept the lead.</div>
                </div>
            </div>
       
        </div>

        <div class="clear"></div>

        <div class="point">
            <div class="pointIcon"><img src="images/50.jpg"></div>

            <div class="pointContent pointContent2line">
                <div class="pointContentTitle">No need to purchase credit anymore</div>
                <div class="pointContentText">
                    <div>You pay per each lead you accept, your</div>
                    <div>auto-recharge has been canceled.</div>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="point">
            <div class="pointIcon"><img src="images/cover-area.jpg"></div>

            <div class="pointContent pointContent1line">
                <div class="pointContentTitle">Multiple cover area</div>

                <div class="pointContentText">
                    <div>Be available on more than one location.</div>            
                </div>
            </div>
        </div>

    </div>

     <div class="pointsRight">

        <div class="point">
            <div class="pointIcon"><img src="images/hours.jpg"></div>

            <div class="pointContent pointContent1line">
                <div class="pointContentTitle">Working hours enhancements</div>

                <div class="pointContentText">
                    <div>You can now split your day into shifts.</div>            
                </div>
            </div>

        </div>

        <div class="clear"></div>

        <div class="point point4line">
            <div class="pointIcon"><img src="images/business-page.jpg"></div>

            <div class="pointContent pointContent4line">
                <div class="pointContentTitle">Premium business page</div>

                <div class="pointContentText">
                    <div>Appear in NoProblem revamped directory</div> 
                    <div>for free: featured exposure, no competitor</div> 
                    <div>ads, link to your website, photo and video</div>
                    <div>gallery and more.</div>              
                </div>
            </div>

        </div>

        <div class="clear"></div>

     </div>


     </div>
    </div>

    <div class="clear"></div>
    <div class="seperateHorizontal" ></div>

    <div class="paypercall">
        <div class="titleThird registrationMiddleTitle ">Why you should be using pay-per-call:</div>
        
        <div class="paypercallContents">
            <div class="paypercallContent">
                <div class="paypercallContentTitle">
                    Connect by phone immediately to motivated customers
                </div>

                <div class="paypercallContentText">
                    Consumers who reach out by phone are typically more ready to spend. Our pay-per-call system brings them right to you.
                </div>
            </div>

            <div class="paypercallContent">
                <div class="paypercallContentTitle">
                    Pay only for actual calls
                </div>

                <div class="paypercallContentText">
                    If you don't receive calls, you don't pay a penny. 
                </div>
            </div>

            <div class="paypercallContent">
                <div class="paypercallContentTitle">
                   Decide the price you'll pay only after evaluating the lead 
                </div>

                <div class="paypercallContentText">
                    Only you know how much a lead is worth to you. Listen to the customer request and decide whether you accept the call and
                    <br />
                    how much you are willing to pay. 
                </div>
            </div>

           <div class="paypercallContent">
                <div class="paypercallContentTitle">
                    Track calls, manage your budget, calculate ROI and more
                </div>

                <div class="paypercallContentText">
                    Our Advertiser Dashboard lets you completely control your total budget, decide where you do business and when, and track 
                    <br />
                    the amount of calls you receive, the details of each call and how many calls you've lost.
                </div>
            </div>

            <div class="paypercallContent">
                <div class="paypercallContentTitle">
                    No set up fees
                </div>

                <div class="paypercallContentText">                    
                    We believe you should pay only for results. No calls? You don't pay.
                </div>
            </div>



        </div>

    </div>

 </div>

  <input type="hidden" id="hdn_flavor" value="flavor A"/>
