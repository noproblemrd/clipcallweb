﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegisterD.ascx.cs" Inherits="Controls_Registration_RegisterD" %>
<div class="flavorB flavorC flavorD">
<div class="registration joinus">

  

    <div class="clear"></div>
    
    <div class="registrationTitle">
            
           <div id="registrationContainerVideoIframe" class="registrationContainerVideoIframe">       
                <div style="" class="registrationVideoIframe" id="registrationVideoIframe">
	                <div>
                        <a id="videoClose" name="videoClose" href="javascript:showHideVideo2();" class="x-close"></a> 
                    </div>
                </div>
            </div>

            
            <div class="registrationTitle1">Thousands of locals are searching online for your services.</div>
            <div class="registrationTitle2">Speak to them before your competition does!</div> 
            
                 
    </div>
    <div class="clear"></div>
    <div class="registrationRight"> 
        <div class="registrationRightBg"></div>

    </div>

    <div class="registrationLeft">
        <div class="ribbon"></div>
        <div class="registrationLeftContainer">
        <div class="registrationLeftContainerTitle">
            Start My FREE Lead
            Booster Trial Now!
        </div>
        
    
       

       <div class="registrationInputs" >    
        
            

            <div class="emailContainer">            
            <div class="inputParent">
                <input id="txt_email" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="email" />
                <div id="div_EmailSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_EmailError" class="inputError" style=""> </div>

                

                <div class="inputValid" style="display:none;"  id="div1"></div>

                <div id="div_EmailComment" style="display:none;" class="inputComment"><%#EmailError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

                <div id="div_EmailComment2" style="display:none;" class="inputComment inputComment2"><%#EmailError2%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        <div class="passwordContainer">            
            <div class="inputParent">
                <input id="txt_password" type="password" class="textActive" runat="server" HolderClass="place-holder" text-mode="pelephone" ifCheckPassword="yes"/>

                <div id="div_PasswordSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_PasswordError" class="inputError" style=""> </div>

                <div class="inputValid" style="display:none;"  id="div2"></div>

                <div id="div_PasswordComment" style="display:none;" class="inputComment"><%#PasswordError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        </div>
   
       
       

        <div> 
  </div>
   
       <div class="clear"></div>

       <div class="containerLogin">

            <div class="loginCell registerLogin">
                 <section class="progress-demo">
                    <a id="linkRegistration" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" button-mode="phone" onclick="javascript:preCheckRegistrationStep('regularRegister');">
                        <span class="ladda-label">Be the first to speak to customers</span><span class="ladda-spinner">
                        </span><div class="ladda-progress" style="width: 138px;"></div>
                    </a>
                 </section>               
            </div>          

            <div class="loginCell alreadyRegistered">
                Already registered? <span class="spanLinkLogin"><a href="<%#ResolveUrl("~")%>ppc/ppcLogin.aspx">Login</a></span>
            </div>
        
             

        </div>

        <div class="clear"></div>       

       <div class="or">Or</div>

       <div class="socialNetworks">

        <span class="icon-stack icon-stackFacebook">
            <a onclick="faceBookLogIn2();" href="javascript:;">                
                <i class="icon-facebook"></i>
            </a>
        </span>      

        <span id="googleCustomSignIn" class="icon-stack icon-stackGooglePlus">            
            <a href="javascript:void(0);"> 
                <span class="googlePlusText"></span>               
                <i class="icon-google-plus"></i>
            </a>
        </span>

       

    </div>

      
             
       <div class="registrationLeftContainerBottom"></div>

        </div>
    </div>
   
   <div class="clear"></div>
    
    
    
</div>

    <div class="learnMore">Learn more</div>

<div class="seperateHorizontal"></div>

<div class="registrationPreMiddle">
    <div class="registrationPreMiddleContent">
        <div class="registrationPreMiddleContentRight">
            
            <div class="titleThird registrationContainerVideoTitle">
                    Here’s how it works
            </div>

            <div class="registrationContainerVideo">              
                  
                <div onclick="javascript:showHideVideo2('http://www.youtube.com/v/qG_PVFk2NmQ?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1');" class="registrationVideo"></div>              
                       
            </div>      

        </div>

        <div class="registrationPreMiddleContentLeft">
             <div class="registrationPreMiddleContentLeftContent">
                   <div class="videoMainTitle">Receive $30 in FREE</div>
                   
                   <div class="videoMainTitle">phone leads to speak to </div>
                   
                   <div class="videoTitle">customers before your</div>
                   
                   <div class="videoTitle">competition.</div>
                   
                   <div class="videoMainTitle videoMainSignUp">Sign up now!</div>
                   
            </div>       
        </div>

        <div class="clear"></div>

        <div class="registrationPreMiddleContentBottom">
            We believe in NoProblem’s pay-per-call phone lead system.
           
            That’s why we’re giving you $30 worth of prepaid phone leads to
            
            try it out! Don’t want to continue after it’s spent? NoProblem...
            
            But we think <span>you’re going to love what you hear!</span>
        </div>
              
    </div>   

</div>

<div class="seperateHorizontal" ></div>

<div class="registrationMiddle ">
    <div class="registrationMiddleTitle ">
        <div class="registrationMiddleTitle1">Your new customers are waiting on the line</div>       
        <div class="registrationMiddleTitle2">Here’s how we get you to it:</div>
    </div>

    <div class="registrationMiddleContents">
        <div class="registrationMiddleContent">
            <div class="registrationMiddleContentLeft">
                <div class="registrationMiddleImg registrationMiddleImg1">                

                </div>
            </div>
            
            <div class="registrationMiddleContentRight">
                <div class="registrationMiddleText">
                   <span>Browser form displays in local
                    <br />
                    web search-</span> Consumers 
                    <br />
                    complete the form and ask to
                    <br />
                    speak to available businesses.   
                </div>
            </div>           

        </div>

        <div class="clear"></div>

        <div class="registrationMiddleContent">

            <div class="registrationMiddleContentLeft">
                <div class="registrationMiddleText">
                    <span>You decide what to pay-</span> Assess 
                    <br />
                    the job, decide what the lead is
                    <br />
                    worth, and place your bid - all via
                    <br />
                    our mobile phone app.   
                </div>  
            </div>     

            <div class="registrationMiddleContentRight">
                <div class="registrationMiddleImg registrationMiddleImg2">
                
                </div>
            </div>                

        </div>

        <div class="clear"></div>

        <div class="registrationMiddleContent registrationMiddleContentLast">
            <div class="registrationMiddleContentLeft">
                 <div class="registrationMiddleImg registrationMiddleImg3">
                
                </div>
            </div>
           
            <div class="registrationMiddleContentRight">
                <div class="registrationMiddleText">
                    <span>Speak to customers ready to
                    <br />
                    spend-</span> Bid winner speaks to 
                    <br />
                    consumer within <u>seconds</u> of 
                    <br />
                    their web search. Voice
                    <br />
                    connection = high conversion.
                </div>
            </div>
           

        </div>

        <div class="clear"></div>

    </div>

    <div class="seperateHorizontal" ></div>

    <div class="improvement">
    <div class="registrationMiddleTitle">
        <div class="registrationMiddleTitle1">Pay-per-call will boost your revenue</div>   
        <div class="registrationMiddleTitle3">Don't believe us? Listen to what advertisers have to say...</div>
    </div>    
    <div class="points">

    <div class="pointsLeft">

        <div class="point">           

            <div class="pointContent">               

                <div class="pointContentText">
                    <div class="cite"></div>
                    <div >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thanks to NoProblem, I am almost fully
                    booked with great jobs that are high paying and
                    pretty interesting... I can finally afford to be picky
                    about the work I take on (no more nickel and diming
                    customers grinding me on price). I am starting to see
                    a real difference in my bottom line.</div> 
                   
                   <div class="author">Julian M. - Locksmith in New York</div>
                </div>
            </div>
       
        </div>        

    </div>

    <div class="pointsRight">

        <div class="point">           

            <div class="pointContent">               

                <div class="pointContentText">
                    <div class="cite"></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I wasn’t sure whether NoProblem would be a
                    good fit for my moving business. I wasn’t thrilled with
                    pay per click ads and phone book listings.
                    NoProblem has been different. As soon as I signed
                    up, they began sending me great calls from real
                    customers and I can easily track how much extra
                    business it is generating. I can tell you one thing - it’s
                    working way better than other advertising I’ve done. 
                    
                    <div class="author">Richard G. – Mover in LA</div>
                </div>
            </div>
       
        </div>

        <div class="clear"></div>

    </div>



     </div>
    </div>

    <div class="clear"></div>
    
    <div class="anotherRegistration">
        <div class="anotherRegistrationTop">
            <div class="anotherRegistrationTitle">Start My FREE Lead Booster Trial Now!</div>
             <div class="loginCell alreadyRegistered">
                Already registered? <span class="spanLinkLogin"><a href="<%#ResolveUrl("~")%>ppc/ppcLogin.aspx">Login</a></span>
            </div>
        </div>

        <div class="clear"></div>

        <div class="anotherRegistrationBottom">
             <div class="registrationInputs" >    
        
            

            <div class="emailContainer">            
            <div class="inputParent">
                <input id="txt_email_another" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="email" />
                <div id="div_EmailSmile_another" class="inputValid" style="display:none;"> </div>
                <div id="div_EmailError_another" class="inputError" style=""> </div>

                

                <div class="inputValid" style="display:none;"  id="div3"></div>

                <div id="div_EmailComment_another" style="display:none;" class="inputComment"><%#EmailError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

                <div id="div_EmailComment2_another" style="display:none;" class="inputComment inputComment2"><%#EmailError2%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

            <div class="passwordContainer">            
                <div class="inputParent">
                    <input id="txt_password_another" type="password" class="textActive" runat="server" HolderClass="place-holder" text-mode="pelephone" ifCheckPassword="yes"/>

                    <div id="div_PasswordSmile_another" class="inputValid" style="display:none;"> </div>
                    <div id="div_PasswordError_another" class="inputError" style=""> </div>

                    <div class="inputValid" style="display:none;"  id="div4"></div>

                    <div id="div_PasswordComment_another" style="display:none;" class="inputComment"><%#PasswordError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>

                </div>
            </div>

            </div>

         <div class="containerLogin">

            <div class="loginCell registerLogin">
                 <section class="progress-demo">
                    <a id="linkRegistration_another" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" button-mode="phone" onclick="javascript:preCheckRegistrationStep_another('regularRegister');">
                        <span class="ladda-label">Be the first to speak to customers</span><span class="ladda-spinner">
                        </span><div class="ladda-progress" style="width: 138px;"></div>
                    </a>
                 </section>               
            </div>          

           
        
             

        </div>

         <div class="clear"></div>

        </div>
       
    </div>

 </div>


 <input type="hidden" id="hdn_flavor" value="flavor D"/>
 </div>