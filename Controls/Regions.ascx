﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Regions.ascx.cs" Inherits="Controls_Regions" %>



<script>
    function changeRegion() {
        if (document.getElementById("titleRegion2Link").innerHTML == "Browse states...") {
            document.getElementById("titleRegion2Link").innerHTML = "Browse popular cities...";
            document.getElementById("titleRegion1").innerHTML = "All states";
            
            document.getElementById("popularCitiesDetails").style.display = 'none';
            document.getElementById("allStates").style.display = 'block';


        }
        else {
            document.getElementById("titleRegion2Link").innerHTML = "Browse states...";
            document.getElementById("titleRegion1").innerHTML = "Popular cities";
            document.getElementById("popularCitiesDetails").style.display = 'block';
            document.getElementById("allStates").style.display = 'none';
        }
    }

</script>
<div class="regions">
        <span id="titleRegion1" class="titleRegion1">Popular cities</span>  <span id="titleRegion2" class="titleRegion2"><a id="titleRegion2Link" href="javascript:void(0);" onclick="javascript:changeRegion();">Browse states...</a></span>


        <div id="popularCitiesDetails" class="popularCitiesDetails">
        <div style="float:left">
            Atlanta, GA
            <br />
            Austin, TX
            <br />
            Baltimore, MD
            <br />
            Boston, MA
            <br />
            Charlotte, NC
            <br />
            Chicago, IL
            <br />
            Columbus, OH
            <br />
            Dallas, TX
        </div>

        <div style="float:left">
            Denver, CO
            <br />
            Fort Worth, TX
            <br />
            Houston, TX
            <br />
            Indianapolis, IN
            <br />
            Jacksonville, FL
            <br />
            Kansas City, MO
            <br />
            Las Vegas, NV
            <br />
            Los Angeles, CA
        </div>

        <div style="float:left">
            Louisville, KY
            <br />
            Miami, FL
            <br />
            Minneapolis, MN
            <br />
            New York, NY
            <br />
            Oklahoma City, OK
            <br />
            Philadelphia, PA
            <br />
            Phoenix, AZ
            <br />
            Pittsburgh, PA
        </div>

        <div style="float:left">
            Portland, OR
            <br />
            San Antonio, TX
            <br />
            San Diego, CA
            <br />
            San Francisco, CA
            <br />
            Seattle, WA
            <br />
            Washington, DC
            <br />
            Search by states...
        </div>
        </div>

        <div id="allStates" class="allStates">
        <div style="float:left">
            Atlanta2, GA
            <br />
            Austin2, TX
            <br />
            Baltimore2, MD
            <br />
            Boston2, MA
            <br />
            Charlotte2, NC
            <br />
            Chicago, IL
            <br />
            Columbus, OH
            <br />
            Dallas, TX
        </div>

        <div style="float:left">
            Denver, CO
            <br />
            Fort Worth, TX
            <br />
            Houston, TX
            <br />
            Indianapolis, IN
            <br />
            Jacksonville, FL
            <br />
            Kansas City, MO
            <br />
            Las Vegas, NV
            <br />
            Los Angeles, CA
        </div>

        <div style="float:left">
            Louisville, KY
            <br />
            Miami, FL
            <br />
            Minneapolis, MN
            <br />
            New York, NY
            <br />
            Oklahoma City, OK
            <br />
            Philadelphia, PA
            <br />
            Phoenix, AZ
            <br />
            Pittsburgh, PA
        </div>

        <div style="float:left">
            Portland, OR
            <br />
            San Antonio, TX
            <br />
            San Diego, CA
            <br />
            San Francisco, CA
            <br />
            Seattle, WA
            <br />
            Washington, DC
            <br />
            Search by states...
        </div>
        </div>
</div>