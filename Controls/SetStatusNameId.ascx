﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SetStatusNameId.ascx.cs" Inherits="Publisher_SetStatusNameId" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>

<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }
    function closePopup() {
        document.getElementById("<%# popUpaddUser.ClientID %>").className = "popModal_del popModal_del_hide";
        ClearPopUp();
        $find('_modal').hide();
    }
    function ClearPopUp() {        

        document.getElementById("<%# txt_ID.ClientID %>").value = "";
        document.getElementById("<%# txt_Name.ClientID %>").value = "";
        document.getElementById("<%# hf_guid.ClientID %>").value = "";

        var validators = Page_Validators;
        for (var i = validators.length - 1; i > -1; i--) {
            validators[i].style.display = "none";
        }

    }
    function openPopup() {
        document.getElementById("<%# popUpaddUser.ClientID %>").className = "popModal_del";
        ClearPopUp();
        $find('_modal').show();
    }
    function Set_Status(e)
     {     
        var code;
        if (!e) var e = window.event;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;        
        if(code==13)
        {            
            <%# Get_tab_click() %>;
            return false;
        }
        return true;
   
     }
 </script>
 <uc1:Toolbox ID="Toolbox1" runat="server" />

<h5>
    <asp:Label ID="lblSubTitle" runat="server"></asp:Label>
</h5>
<div class="page-content minisite-content5">			
    <div id="form-analyticsseg">
	   
	    <asp:UpdatePanel ID="_updatePanelTable" runat="server" UpdateMode="Conditional">
	    <ContentTemplate>
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" ShowFooter="true" 
                CssClass="data-table"
                AllowPaging="True" 
                    onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                 <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                <FooterStyle CssClass="footer"  />
                
                <Columns>                     
                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                        <br />
                        <asp:CheckBox ID="cb_all" runat="server" >
                        </asp:CheckBox>                    
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="cb_choose" runat="server" />                    
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# lbl_ID.Text %>"></asp:Label>                     
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl_CodeId" runat="server" Text="<%# Bind('ID') %>"></asp:Label>
                        <asp:Label ID="lbl_GuidId" runat="server" Text="<%# Bind('GuidId') %>" Visible="false"></asp:Label> 
                    </ItemTemplate>                
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label4" runat="server" Text="<%# lbl_Name.Text %>"></asp:Label>                     
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl_ReasonName" runat="server" Text="<%# Bind('Name') %>"></asp:Label>                        
                    </ItemTemplate>                
                    </asp:TemplateField>

                    
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<div id="popUpaddUser" runat="server" sclass="content" style="max-height:450px;display:none;" class="popModal_del popModal_del_hide"  >
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
        <div class="content" >
            <div class="modaltit">
                <h2><asp:Label ID="lbl_PopupTitle" runat="server" Text="<%# lblSubTitle.Text %>"></asp:Label></h2>
            </div>
            <div>
    	    <center>    	    
                <table>
    	            <tr>
    	            <td>
                        <asp:Label ID="lblid" runat="server" Text="<%# lbl_ID.Text %>"></asp:Label>   	        
    	            </td>
    	            <td>
    	                <asp:TextBox ID="txt_ID" CssClass="form-text" runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:HiddenField ID="hf_guid" runat="server" />
    	            </td>                
    	            </tr> 

                    <tr>
    	            <td>
                        <asp:Label ID="lblName" runat="server" Text="<%# lbl_Name.Text %>"></asp:Label>   	        
    	            </td>
    	            <td>
    	                <asp:TextBox ID="txt_Name" CssClass="form-text" runat="server" onkeypress="return Set_Status(event);"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Missing"
                         Display="Dynamic" ValidationGroup="EditUnavailability" ControlToValidate="txt_Name" 
                         CssClass="error-msg"></asp:RequiredFieldValidator>
    	            </td>                
    	            </tr>                

                    
                </table>
            </center>
            </div>  	    
            <div class="content2">
	            <table class="Table_Button">
                <tr>
                <td>
                    <div class="unavailabilty2">
                    <asp:Button ID="btn_Set" runat="server" Text="Set" ValidationGroup="EditUnavailability"
                     OnClick="btn_Set_Click" CssClass="btn2" />
        
              
                    <input id="btn_cancel" type="button" class="btn" value="Cancel" runat="server" onclick="javascript:closePopup();"/>
                    </div>
                </td>
                </tr>
                </table>
	        </div>
	    </div>
	<div class="bottom2"></div>        	
</div>

<cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btn_virtual"
    PopupControlID="popUpaddUser"
    BackgroundCssClass="modalBackground" 
              
    BehaviorID="_modal"               
    DropShadow="false">
</cc1:ModalPopupExtender>

<div style="display:none;">
    <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />                     
</div>

<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
<asp:Label ID="lbl_ID" runat="server" Text="ID" Visible="false"></asp:Label>
<asp:Label ID="lbl_Name" runat="server" Text="Name" Visible="false"></asp:Label>


<asp:Label ID="lbl_noreasultSearch" runat="server" Text="There are no results for this search" Visible="false"></asp:Label>
<asp:Label ID="lbl_CantDelete" runat="server" Text="You can not delete all the statuses" Visible="false"></asp:Label>

<asp:Label ID="lbl_Delete" runat="server" Text="This values won't be available for new updates. In all the records that used this values it will still" Visible="false"></asp:Label>

