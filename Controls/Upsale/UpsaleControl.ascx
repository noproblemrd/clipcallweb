﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpsaleControl.ascx.cs" Inherits="Controls_Upsale_UpsaleControl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script type="text/javascript">

    function ChangeRegionLevel(elem) {
        showDiv();
        var _level = elem.options[elem.selectedIndex].value;
        $find("AutoRegion")._contextKey = _level;

        document.getElementById("<%# txt_City.ClientID %>").value = "";
        AutoComplete.SetCitiesItemsUpsale(_level, OnCompleteUpsaleCity, OnErrorEx, OnTimeOutEx);
    }

    function upsale(_phone, UpsaleId, ExpertiseCode, _description)//, _region, regionLevel)
    {
        showDiv();
        document.getElementById("<%# popUp_V1.ClientID %>").className = "popModal_del3";
        $get("<%# txt_MyPhone.ClientID %>").value = _phone;
        document.getElementById("<%# txt_RequestDesc.ClientID %>").value = _description;
        document.getElementById("<%# hf_UpsaleId.ClientID %>").value = UpsaleId;
        //   document.getElementById("<%# txt_City.ClientID %>").value = _region;

        var ddl_Expertiser = document.getElementById("<%# ddl_Expertiser.ClientID %>");
        for (var i = 0; i < ddl_Expertiser.options.length; i++) {
            if (ddl_Expertiser.options[i].value == ExpertiseCode) {
                ddl_Expertiser.selectedIndex = i;
                break;
            }
        }

        regionLevel = 1;

        document.getElementById("<%# ddl_regionLevel.ClientID %>").selectedIndex = regionLevel - 1;

        $find("AutoRegion").set_contextKey(regionLevel);
        $find('_modalV1').show();

        AutoComplete.SetCitiesItemsUpsale(regionLevel, OnCompleteUpsaleCity, OnErrorEx, OnTimeOutEx);
    }
    function upsaleWithRegion(_phone, UpsaleId, ExpertiseCode, _region, regionLevel, _description) {
        document.getElementById("<%# popUp_V1.ClientID %>").className = "popModal_del";
        $get("<%# txt_MyPhone.ClientID %>").value = _phone;
        document.getElementById("<%# txt_RequestDesc.ClientID %>").value = _description;
        document.getElementById("<%# hf_UpsaleId.ClientID %>").value = UpsaleId;

        var ddl_Expertiser = document.getElementById("<%# ddl_Expertiser.ClientID %>");
        for (var i = 0; i < ddl_Expertiser.options.length; i++) {
            if (ddl_Expertiser.options[i].value == ExpertiseCode) {
                ddl_Expertiser.selectedIndex = i;
                break;
            }
        }
        var ddl_regionLevel = document.getElementById("<%# ddl_regionLevel.ClientID %>");

        document.getElementById("<%# ddl_regionLevel.ClientID %>").selectedIndex = regionLevel - 1;
        document.getElementById("<%# txt_City.ClientID %>").value = _region;
        $find("AutoRegion").set_contextKey(regionLevel);
        $find('_modalV1').show();

        var url = "<%# GetPath_SetCitiesItemsUpsale %>";
        var params = "_level=" + regionLevel;
        //      AutoComplete.SetCitiesItemsUpsale(regionLevel, OnCompleteUpsaleCity, OnErrorEx, OnTimeOutEx);
        CallWebService(params, url, OnCompleteUpsaleCity,
            function () {
                alert('error');
            });
    }
    function OnCompleteUpsaleCity(arg) {
        if (arg == "false") {
            top.UpdateFailed();
        }

        hideDiv();
    }
    function ClearValidators() {
        var validators = Page_Validators;

        for (var i = 0; i < Page_Validators.length; i++) {
            Page_Validators[i].style.display = "none";
        }
        document.getElementById("<%# lbl_DescLength.ClientID %>").style.display = "none";
    }
    function ClosePopUpV() {
        var div_V1 = document.getElementById("<%# popUp_V1.ClientID %>");
        var div_V2 = document.getElementById("<%# popUp_V2.ClientID %>");
        div_V1.className = "popModal_del3 popModal_del3_hide";
        div_V2.className = "popModal_del3 popModal_del3_hide";

        $find('_modalV1').hide();
        $find('_modalV2').hide();


        ClearValidators();
        document.getElementById('<%# _div_auction.ClientID %>').innerHTML = "";
        document.getElementById("<%# txt_RequestDesc.ClientID %>").value = "";
        document.getElementById("<%# lbl_count.ClientID %>").innerHTML = "0";
        document.getElementById("<%# txt_City.ClientID %>").value = "";
        document.getElementById("<%# lbl_MissingExpertise.ClientID %>").style.display = "none";
        document.getElementById("<%# lbl_ErrorCity.ClientID %>").style.display = "none";
        document.getElementById("form-suppliers").selectedIndex = 1;
        document.getElementById("<%# ddl_Expertiser.ClientID %>").selectedIndex = 0;
        document.getElementById("<%# hf_UpsaleId.ClientID %>").value = "";

    }

    function OnTimeOutEx(arg) {
        alert("timeOut has occured");
        hideDiv();
    }
    function OnErrorEx(arg) {
        alert("Error has occured");
        hideDiv();
    }
    function GetLblCityError() {
        return $get('<%# lbl_ErrorCity.ClientID %>');
    }
    function GetFieldValidatorCity() {
        return $get('<%# RequiredFieldValidatorCity.ClientID %>');
    }
    var ElementTextCity;
    function GoFind(e, elm) {
        if (e.keyCode == 13) {

            CheckIfmatch(elm, GetLblCityError(), GetFieldValidatorCity());
            ElementTextCity = elm;
            WaitToFind();

            return false;
        }

    }
    function IgnorEnter(e) {
        if (e.keyCode == 13)
            return false;
        return true;
    }
    function BackStep() {
        document.getElementById("<%# lbl_DescLength.ClientID %>").style.display = "none";
        document.getElementById("<%# txt_RequestDesc.ClientID %>").value = "";
        document.getElementById("<%# _div_auction.ClientID %>").innerHTML = "";
        document.getElementById("form-suppliers").selectedIndex = 1;
        document.getElementById("<%# lbl_count.ClientID %>").innerHTML = "0";
        $('#<%# txt_Name.ClientID %>').val('');
        $find('_modalV2').hide();
        $find('_modalV1').show();
    }

    function GetList() {
        var if_ok = true;
        if (!Page_ClientValidate('SearchSupplier'))
            if_ok = false;
        var lbl_MissingExpertise = document.getElementById("<%# lbl_MissingExpertise.ClientID %>");
        lbl_MissingExpertise.style.display = "none";
        var PrExpertise = document.getElementById("<%# ddl_Expertiser.ClientID %>");
        var ExpertiseCode = PrExpertise.options[PrExpertise.selectedIndex].value;
        if (ExpertiseCode == "-1") {
            lbl_MissingExpertise.style.display = "inline";
            if_ok = false;
        }
        if (!if_ok)
            return false;
        document.getElementById('<%# _div_auction.ClientID %>').innerHTML = "";
        var ddl_regionLevel = document.getElementById("<%# ddl_regionLevel.ClientID %>");
        var RegionLevel = ddl_regionLevel.options[ddl_regionLevel.selectedIndex].value;

        var ExpertiseLevel = 1;
        var city = document.getElementById("<%# txt_City.ClientID %>").value;
        var UpsaleId = document.getElementById("<%# hf_UpsaleId.ClientID %>").value;

        Auction.GetUpsaleList(ExpertiseCode, ExpertiseLevel, city, UpsaleId, RegionLevel, OnCompleteEAuction, OnErrorEx, OnTimeOutEx);
        showDiv();
    }
    function OnCompleteEAuction(arg) {
        hideDiv();

        if (arg == "empty") {
            if (!confirm("<%# NoResult %>"))
               // UpsaleAAR();
                return;
        }
        else if (arg == "none") {
            return;
        }
        else
            document.getElementById('<%# _div_auction.ClientID %>').innerHTML = arg;
        $find('_modalV1').hide();
        $find('_modalV2').show();
    }
    
    function InitiateCalls() {
        if (!Page_ClientValidate('auction'))
            return false;
        var _phone = $get("<%# txt_MyPhone.ClientID %>").value;
        var _desc = $get("<%# txt_RequestDesc.ClientID %>").value;
        _desc = GetStringForJson(_desc);
        var ddl_expertise = $get("<%# ddl_Expertiser.ClientID %>");
        var _expertise = ddl_expertise.options[ddl_expertise.selectedIndex].text;


        var UpsaleId = document.getElementById("<%# hf_UpsaleId.ClientID %>").value;
        var name = $('#<%# txt_Name.ClientID %>').val();
        name = GetStringForJson(name);
        var _email = $('#<%# txt_email.ClientID %>').val();
        var num_supplier = document.getElementById("form-suppliers");
        var _num = parseInt(num_supplier.options[num_supplier.selectedIndex].value);
        var RequestId = '<%# RequestIdV %>';
        
        if (document.getElementById("<%# rb_SelectTime.ClientID %>").checked) {
            var current_date = new Date();
            var request_date = new Date();
            var ddl_AMPM = document.getElementById("<%# ddl_AMPM.ClientID %>");
            var ddl_hours = document.getElementById("<%# ddl_hours.ClientID %>");
            var ddl_minutes = document.getElementById("<%# ddl_minutes.ClientID %>");

            var request_hour = parseInt(ddl_hours.options[ddl_hours.selectedIndex].value);
            if (request_hour == 12)
                request_hour = 0;
            var request_minutes = parseInt(ddl_minutes.options[ddl_minutes.selectedIndex].value);
            if (ddl_AMPM.options[ddl_AMPM.selectedIndex].value == "PM")
                request_hour = request_hour + 12;

            request_date.setHours(request_hour);
            request_date.setMinutes(request_minutes);
            if (current_date > request_date)
                request_date.addDays(1);

            var s_date = request_date.getUTCFullYear() + ";" + (request_date.getUTCMonth() + 1) + ";" +
                                request_date.getUTCDate() + ";" + request_date.getUTCHours() + ";" +
                                request_date.getUTCMinutes();

            var _data = '{"_phone": "' + _phone + '", "_desc": "' + _desc + '", "UpsaleId": "' + UpsaleId + '", "NumExper": ' +
                            _num + ', "_date": "' + s_date + '","_RequestId": "' + RequestId + '", "name": "' + name + '", "email": "' + _email + '"}';
           
            /*
            Auction.createServiceRequestSchedule(_phone, _desc, UpsaleId, _num, s_date, RequestId,
            OnCompleteRequest, OnErrorEx, OnTimeOutEx);
            */

            $.ajax({
                url: '<%# createServiceRequestSchedule %>',
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    OnCompleteRequest(data.d);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    OnErrorEx(XMLHttpRequest);
                }
            });

        }
        else {
            //Auction.createServiceRequest(_phone, _desc, UpsaleId, _num, RequestId, OnCompleteRequest, OnErrorEx, OnTimeOutEx);
            var _data = '{"_phone": "' + _phone + '", "_desc": "' + _desc + '", "UpsaleId": "' + UpsaleId + '", "NumExper": ' +
                            _num + ', "_RequestId": "' + RequestId + '", "name": "' + name + '", "email": "' + _email + '"}';
            $.ajax({
                url: '<%# createServiceRequest %>',
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    OnCompleteRequest(data.d);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    OnErrorEx(XMLHttpRequest);
                }
            });
        }
        showDiv();

    }
    function GetStringForJson(str) {
        var new_str = '';
        for (var i = 0; i < str.length; i++) {
            if (str.charAt(i) == "'")
                new_str += "\'";
            else if (str.charAt(i) == '"')
                new_str += '\\"';
            else
                new_str += str.charAt(i);
        }
        return new_str;
    }
    function OnCompleteRequest(arg) {

        hideDiv();
        var args = arg.split(";");
        switch (args[0]) {

            case ("Success"):
                ShowMessageIncident(args[1], true);
                break;
            default: ShowMessageIncident(args[1], false);
                break;
        }

    }
    function ShowMessageIncident(arg, IsSuccess) {

        document.getElementById('<%# Div_StatusMessage.ClientID %>').className = "popModal_del3";
        document.getElementById("<%# lbl_StatusMessage.ClientID %>").innerHTML = arg;
        $find('_modalMessage').show();
        var t = setTimeout("HideMessageIncident(" + IsSuccess + ");", 2000);
    }
    function HideMessageIncident(IsSuccess) {
        $find('_modalMessage').hide();
        document.getElementById('<%# Div_StatusMessage.ClientID %>').className = "popModal_del3 popModal_del3_hide";
        if (IsSuccess) {
            ClosePopUpV();
            try {
                Complete_request();
            }
            catch (ex) { }
        }
    }
    /*
    function Complete_request() {
        showDiv();
        //   < GetPostBackEventAfterAuction %>; 
    }
    */

    function setPosition() {
        var ua = navigator.userAgent.toLowerCase();

        if (ua.indexOf("msie") == -1) {
            var panel = document.getElementById("<%#Panel_showCity.ClientID %>");
            var elm = document.getElementById('<%#txt_City.ClientID %>');
            panel.style.top = findTop(elm);
            panel.style.left = findLeft(elm);
            panel.style.position = "absolute";
        }
    }

    function checkLength(elm, e) {
        var code = e.keyCode ? e.keyCode : e.which;

        if (code != 13 && elm.value.length > 140) {
            elm.value = elm.value.substring(0, elm.value.length - 2);
            document.getElementById("<%# lbl_DescLength.ClientID %>").style.display = "block";
        }
        else
            document.getElementById("<%# lbl_DescLength.ClientID %>").style.display = "none";
    }
    function CountLetter(elm) {
        document.getElementById("<%# lbl_count.ClientID %>").innerHTML = elm.value.length;
    }
    function MakeBeforeAction() {
        showDiv();
    }
    function GetRegionLevel() {
        var ddl_regionLevel = document.getElementById("<%# ddl_regionLevel.ClientID %>");
        var RegionLevel = ddl_regionLevel.options[ddl_regionLevel.selectedIndex].value;
        return RegionLevel;
    }

    /******************/
    function SetScheduleDate() {
        var _date = new Date();
        var minute = _date.getMinutes();
        var hour = _date.getHours();
        var ddl_AMPM = document.getElementById("<%# ddl_AMPM.ClientID %>");
        var ddl_hours = document.getElementById("<%# ddl_hours.ClientID %>");
        var ddl_minutes = document.getElementById("<%# ddl_minutes.ClientID %>");
        minute = minute + 2;
        var multi = parseInt(minute / 15) + 1;
        minute = multi * 15;
        if (minute > 45) {
            minute = 0;
            hour++;
        }
        if (hour > 23) {
            SetDDLString(ddl_AMPM, "AM");
            hour = hour - 24;
        }
        else if (hour > 11) {
            SetDDLString(ddl_AMPM, "PM");
            hour = hour - 12;
        }
        else
            SetDDLString(ddl_AMPM, "AM");
        SetDDLInt(ddl_hours, hour);
        SetDDLInt(ddl_minutes, minute);


    }
    function SetDDLString(ddl, _value) {
        for (var i = 0; i < ddl.options.length; i++) {
            if (ddl.options[i].value == _value) {
                ddl.selectedIndex = i;
                break;
            }
        }
    }
    function SetDDLInt(ddl, _value) {
        for (var i = 0; i < ddl.options.length; i++) {
            if (parseInt(ddl.options[i].value) == _value) {
                ddl.selectedIndex = i;
                break;
            }
        }
    }
    function RB_Scheduled() {

        var _span = document.getElementById("sp_Scheduled");
        var _spanTime = document.getElementById("span_time");
        var _spanNow = document.getElementById("span_now");
        if (document.getElementById("<%= rb_now.ClientID  %>").checked) {
            _span.style.display = "none";
            _spanTime.className = "radio-wrap select-time disabled";
            _spanNow.className = "radio-wrap now";
        }
        else {
            _span.style.display = "";
            _spanTime.className = "radio-wrap select-time open";
            _spanNow.className = "radio-wrap now disabled";
        }
    }

</script>

<div id="popUp_V1" runat="server" class="popModal_del3 popModal_del3_hide" style="display:none;"> 
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A"></a>
    <div class="content">
        <asp:UpdatePanel ID="_UpdatePanelPopModal" runat="server" UpdateMode="Conditional">
            <ContentTemplate>  
                <div class="f1">
                    <h2><asp:Label ID="Label110" runat="server" Text="<%# lbl_TitleForm.Text %>"></asp:Label></h2>
                    
                                <div class="field">
                                    <asp:Label ID="lbl_RegionLevel" CssClass="label" runat="server" Text="Region level"></asp:Label>
                                    <asp:DropDownList ID="ddl_regionLevel" runat="server"  CssClass="form-selectup" onchange="javascript:ChangeRegionLevel(this);">
                                    </asp:DropDownList>
                               </div>
                            
                                <div class="field">
                                    <asp:Label ID="lbl_City" CssClass="label" runat="server" Text="City"></asp:Label>
                                    <asp:TextBox ID="txt_City" runat="server" 
                                    CssClass="form-text" onblur="javascript:CheckIfmatchNoneCss(this, GetLblCityError(), GetFieldValidatorCity(), GetRegionLevel());" onkeypress="return GoFind(event, this);" autocomplete="off"></asp:TextBox>
                                    <br /><asp:RequiredFieldValidator runat="server" ControlToValidate="txt_City" Display="Dynamic" CssClass="error-msg" ID="RequiredFieldValidatorCity" ValidationGroup="SearchSupplier" >Missing</asp:RequiredFieldValidator>
                                    <asp:Label ID="lbl_ErrorCity" runat="server" style="display:none;" Text="Choose city from the list" CssClass="error-msg"></asp:Label>
                               </div>
                            
                                <div class="field">
                                    <asp:Label ID="lbl_ExpertiseChoose" runat="server" Text="Service" CssClass="label" ></asp:Label>
                                    <asp:DropDownList ID="ddl_Expertiser" runat="server" CssClass="form-selectup"></asp:DropDownList>
                                    <asp:Label ID="lbl_MissingExpertise" runat="server" style="display:none;" Text="Missing" CssClass="error-msg"></asp:Label><br />
                                </div>
                           
                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" 
                    UseContextKey="true"
                    TargetControlID="txt_City"
                    ServicePath="~/Publisher/AutoComplete.asmx"
                    ServiceMethod="GetSuggestionsUpsale" 
                    MinimumPrefixLength="1"
                    CompletionSetCount="4"
                    EnableCaching="false"
                    CompletionInterval="10"
                    CompletionListCssClass="completionList"
                    CompletionListHighlightedItemCssClass="itemHighlighted"
                    CompletionListItemCssClass="_listItem"   
                    OnClientShown="setPosition"                        
                    CompletionListElementID="Panel_showCity" 
                    ContextKey="1"
                    BehaviorID="AutoRegion"
                    runat="server">
                    </cc1:AutoCompleteExtender>       
                  <asp:Panel ID="Panel_showCity" runat="server"></asp:Panel>
                  <input id="btn_Search" type="button" class="btn3" value="Search" runat="server" onclick="javascript:GetList();" validationgroup="SearchSupplier" causesvalidation="true"/>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
   
</div>
 <div class="bottom"></div>
<cc1:ModalPopupExtender ID="_mpeV" runat="server"
TargetControlID="btn_virtualV"
PopupControlID="popUp_V1"
BackgroundCssClass="modalBackground" 
BehaviorID="_modalV1"
DropShadow="false"
></cc1:ModalPopupExtender> 

<div id="popUp_V2" runat="server" class="popModal_del3 popModal_del3_hide" style="display:none;"> 
    <div class="top"></div>
    <a id="a_closeV2" runat="server" class="span_A"></a>
    <div class="content">
    <asp:UpdatePanel ID="_UpdatePanelPopModalV2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>  
            <div class="f1">
                <h2><asp:Label ID="Label114" runat="server" Text="<%# lbl_TitleForm.Text %>"></asp:Label></h2>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_MyPhone" CssClass="label" runat="server" Text="Phone Number"></asp:Label>
                            <asp:TextBox ID="txt_MyPhone" CssClass="form-text" runat="server" onkeypress="return IgnorEnter(event);" autocomplete="off"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPhone" runat="server" ControlToValidate="txt_MyPhone" ErrorMessage="Missing" Display="Dynamic" ValidationGroup="auction" CssClass="error-msg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" runat="server"  ValidationGroup="auction" ErrorMessage="Not valid" Display="Dynamic" ValidationExpression="\d+" ControlToValidate="txt_MyPhone" CssClass="error-msg"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            <asp:Label ID="lbl_name" runat="server" Text="Name" CssClass="label"></asp:Label>
                            <asp:TextBox ID="txt_Name" runat="server" CssClass="form-text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_NumExpertises" runat="server" Text="Number of services" CssClass="label"></asp:Label>
                            <select id="form-suppliers" class="form-select">
	                            <option value="1">1</option>
	                            <option value="2" selected="selected">2</option>
	                            <option value="3">3</option>
	                            <option value="4">4</option>
	                            <option value="5">5</option>
	                            <option value="6">6</option>
	                        </select>
                        </td>
                        <td>
                            <asp:Label ID="lbl_email" runat="server" Text="email" CssClass="label"></asp:Label>
                            <asp:TextBox ID="txt_email" runat="server" CssClass="form-text"></asp:TextBox>
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator_email" runat="server"  ValidationGroup="auction" ErrorMessage="Not valid" Display="Dynamic" ValidationExpression="\d+" ControlToValidate="txt_email" CssClass="error-msg"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lbl_Request" CssClass="label" runat="server" Text="Request Description"></asp:Label>
                           
                            <asp:TextBox ID="txt_RequestDesc" runat="server" Rows="4" TextMode="MultiLine" CssClass="text-area" onkeydown="checkLength(this, event);"
                             onkeyup="javascript:CountLetter(this);"></asp:TextBox> 
                            
                           
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDesc" runat="server" ControlToValidate="txt_RequestDesc" ErrorMessage="Missing" Display="Dynamic" ValidationGroup="auction" ></asp:RequiredFieldValidator>
                            <asp:Label ID="lbl_DescLength" runat="server" Text="You can not fit more than 140 letters" CssClass="error-msg" style="display:none;"></asp:Label>
                            
                            <span class="count">140/<asp:Label ID="lbl_count" runat="server" Text="0" ></asp:Label></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span id="span_now" class="radio-wrap now">
                                <asp:RadioButton ID="rb_now" runat="server" GroupName="Scheduled" onclick="javascript:RB_Scheduled();" Checked="true" Text="NOW"  />
                            </span>
                    
                            <span id="span_time" class="radio-wrap select-time disabled">
                                <asp:RadioButton ID="rb_SelectTime" runat="server" GroupName="Scheduled" onclick="javascript:RB_Scheduled();" Text="Select time" />
                                <span id="sp_Scheduled" class="time" style="display: none;" >
                                    <asp:DropDownList ID="ddl_hours" runat="server">
                                        <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                        <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                        <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                        <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                        <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddl_minutes" runat="server">
                                        <asp:ListItem Value="00" Text="00"></asp:ListItem>
                                        <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                        <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                        <asp:ListItem Value="45" Text="45"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddl_AMPM" runat="server">
                                        <asp:ListItem Value="AM" Text="AM"></asp:ListItem>
                                        <asp:ListItem Value="PM" Text="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                </span>
                        
                                <span class="corner-right"></span>
                            </span>
                        </td>
                    </tr>
                </table>
                 <div class="buttons_div">
	            <input id="btn_NewSearch" type="button" class="btn" value="New search" runat="server" onclick="javascript:BackStep();" /> 
	            <span id="span_initiateCalls" >                     
                    <input id="btn_initiateCalls" type="button" class="btn" value="Initiate Calls" runat="server" onclick="javascript:InitiateCalls();" validationgroup="auction" causesvalidation="true"/>
                </span>
                </div>
            </div>
	        <div id="_div_auction" runat="server" style="border: 0 auto;"></div>
	    </ContentTemplate>
	</asp:UpdatePanel>
	</div>
    <div class="bottom"></div>
</div>
	                
<cc1:ModalPopupExtender ID="_mpeV2" runat="server"
TargetControlID="btn_virtualV"
PopupControlID="popUp_V2"
BackgroundCssClass="modalBackground" 
BehaviorID="_modalV2"
Y="5"   
DropShadow="false"
></cc1:ModalPopupExtender>      
                    
    
<div id="Div_StatusMessage" runat="server" class="popModal_del3 popModal_del3_hide" style="z-index:99999;display:none;"> 
    <div class="top"></div> 
    <div class="content">    
        <div class="f1">       
            <h2><asp:Label ID="lbl_StatusMessage" runat="server" ForeColor="Red" ></asp:Label></h2>
        </div>
           
    </div>
       
</div>

                  
    <cc1:ModalPopupExtender ID="_mpeMessage" runat="server"
    TargetControlID="btn_virtualMessage"
    PopupControlID="Div_StatusMessage"
    BackgroundCssClass="modalBackground" 
    BehaviorID="_modalMessage"                  
    DropShadow="false"
    ></cc1:ModalPopupExtender>

<asp:Label ID="lbl_TitleForm" runat="server" Text="Upsale form" Visible="false"></asp:Label>
<asp:Label ID="lbl_NoResult" runat="server" Text="There are no result! Send request anyway? (Trial advertisers are not returned in the search)" Visible="false"></asp:Label>

<asp:HiddenField ID="hf_UpsaleId" runat="server" />
 <div style="display:none;">
        <asp:Button ID="btn_virtualV" runat="server" style="display:none;"  />           
        <asp:Button ID="btn_virtualMessage" runat="server" style="display:none;"  /> 
        <asp:Button ID="btn_virtualUpsaleDetails" runat="server" style="display:none;"  /> 
    </div>