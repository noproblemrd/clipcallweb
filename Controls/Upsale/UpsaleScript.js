﻿    function ChangeRegionLevel(elem) {
        showDiv();
        var _level = elem.options[elem.selectedIndex].value;
        $find("AutoRegion")._contextKey = _level;

        document.getElementById("<%# txt_City.ClientID %>").value = "";
        AutoComplete.SetCitiesItemsUpsale(_level, OnCompleteUpsaleCity, OnErrorEx, OnTimeOutEx);
    }

    function upsale(_phone, UpsaleId, ExpertiseCode)//, _region, regionLevel)
    {
        showDiv();
        document.getElementById("<%# popUp_V1.ClientID %>").className = "popModal_del3";
        $get("<%# txt_MyPhone.ClientID %>").value = _phone;
        document.getElementById("<%# hf_UpsaleId.ClientID %>").value = UpsaleId;
        //   document.getElementById("<%# txt_City.ClientID %>").value = _region;

        var ddl_Expertiser = document.getElementById("<%# ddl_Expertiser.ClientID %>");
        for (var i = 0; i < ddl_Expertiser.options.length; i++) {
            if (ddl_Expertiser.options[i].value == ExpertiseCode) {
                ddl_Expertiser.selectedIndex = i;
                break;
            }
        }

        regionLevel = 1;

        document.getElementById("<%# ddl_regionLevel.ClientID %>").selectedIndex = regionLevel - 1;

        $find("AutoRegion").set_contextKey(regionLevel);
        $find('_modalV1').show();

        AutoComplete.SetCitiesItemsUpsale(regionLevel, OnCompleteUpsaleCity, OnErrorEx, OnTimeOutEx);
    }
    function upsaleWithRegion(_phone, UpsaleId, ExpertiseCode, _region, regionLevel)
    {
        document.getElementById("<%# popUp_V1.ClientID %>").className = "popModal_del";
        $get("<%# txt_MyPhone.ClientID %>").value = _phone;
        

        var ddl_Expertiser = document.getElementById("<%# ddl_Expertiser.ClientID %>");
        for (var i = 0; i < ddl_Expertiser.options.length; i++) {
            if (ddl_Expertiser.options[i].value == ExpertiseCode) {
                ddl_Expertiser.selectedIndex = i;
                break;
            }
        }
        var ddl_regionLevel = document.getElementById("<%# ddl_regionLevel.ClientID %>");
       
        document.getElementById("<%# ddl_regionLevel.ClientID %>").selectedIndex = regionLevel - 1;
        document.getElementById("<%# txt_City.ClientID %>").value = _region;
        $find("AutoRegion").set_contextKey(regionLevel);
        $find('_modalV1').show();

        var url = "<%# GetPath_SetCitiesItemsUpsale %>";
        var params = "_level=" + regionLevel;
        //      AutoComplete.SetCitiesItemsUpsale(regionLevel, OnCompleteUpsaleCity, OnErrorEx, OnTimeOutEx);
        CallWebService(params, url, OnCompleteUpsaleCity,
            function () {
                alert('error');
            });
    }
    function OnCompleteUpsaleCity(arg) {
        if (arg == "false") {
            top.UpdateFailed();
        }

        hideDiv();
    }
 function ClearValidators()
    {
        var validators = Page_Validators;
        
        for (var i = 0;i < Page_Validators.length;  i++)
        {      
			Page_Validators[i].style.display = "none";	
	    } 
	    document.getElementById("<%# lbl_DescLength.ClientID %>").style.display="none";      
    }
    function ClosePopUpV()
    {
        var div_V1 = document.getElementById("<%# popUp_V1.ClientID %>");
        var div_V2 = document.getElementById("<%# popUp_V2.ClientID %>");
        div_V1.className="popModal_del3 popModal_del3_hide"; 
        div_V2.className="popModal_del3 popModal_del3_hide"; 
        
        $find('_modalV1').hide();
        $find('_modalV2').hide();
   
        
        ClearValidators();
        document.getElementById('<%# _div_auction.ClientID %>').innerHTML="";
        document.getElementById("<%# txt_RequestDesc.ClientID %>").value="";
        document.getElementById("<%# lbl_count.ClientID %>").innerHTML = "0";
        document.getElementById("<%# txt_City.ClientID %>").value="";               
        document.getElementById("<%# lbl_MissingExpertise.ClientID %>").style.display="none"; 
        document.getElementById("<%# lbl_ErrorCity.ClientID %>").style.display="none";     
        document.getElementById("form-suppliers").selectedIndex=1;       
        document.getElementById("<%# ddl_Expertiser.ClientID %>").selectedIndex=0;
        document.getElementById("<%# hf_UpsaleId.ClientID %>").value="";
       
    }
   
    function OnTimeOutEx(arg)
    {  
	    alert("timeOut has occured");
	    hideDiv();
    }
    function OnErrorEx(arg)
    {  
	    alert("Error has occured");
	    hideDiv();
    }
    function GetLblCityError()
    {
        return $get('<%# lbl_ErrorCity.ClientID %>');
    }
    function GetFieldValidatorCity()
    {
        return $get('<%# RequiredFieldValidatorCity.ClientID %>');
    }
    var ElementTextCity;
    function GoFind(e, elm)
    {
        if(e.keyCode==13)
        {
            
            CheckIfmatch(elm, GetLblCityError(), GetFieldValidatorCity());
            ElementTextCity=elm;
             WaitToFind();
            
            return false;
        }
        
    }
    function IgnorEnter(e)
    {
         if(e.keyCode==13)
            return false;
         return true;
    }
    function BackStep()
    {
        document.getElementById("<%# lbl_DescLength.ClientID %>").style.display="none";
        document.getElementById("<%# txt_RequestDesc.ClientID %>").value="";
        document.getElementById("<%# _div_auction.ClientID %>").innerHTML="";
        document.getElementById("form-suppliers").selectedIndex=1; 
        document.getElementById("<%# lbl_count.ClientID %>").innerHTML = "0";
        $find('_modalV2').hide();
        $find('_modalV1').show();        
    }
   
    function GetList()
    {
        var if_ok=true;
        if(!Page_ClientValidate('SearchSupplier'))
            if_ok= false;
        var lbl_MissingExpertise=document.getElementById("<%# lbl_MissingExpertise.ClientID %>");
        lbl_MissingExpertise.style.display="none";
        var PrExpertise=document.getElementById("<%# ddl_Expertiser.ClientID %>");
        var ExpertiseCode=PrExpertise.options[PrExpertise.selectedIndex].value;
        if(ExpertiseCode=="-1")
        {
            lbl_MissingExpertise.style.display="inline";
            if_ok= false;
        }
        if(!if_ok)
            return false;
        document.getElementById('<%# _div_auction.ClientID %>').innerHTML="";
        var ddl_regionLevel = document.getElementById("<%# ddl_regionLevel.ClientID %>");
        var RegionLevel = ddl_regionLevel.options[ddl_regionLevel.selectedIndex].value;
  
        var ExpertiseLevel = 1;
        var city=document.getElementById("<%# txt_City.ClientID %>").value;
        var UpsaleId = document.getElementById("<%# hf_UpsaleId.ClientID %>").value;
       
        Auction.GetUpsaleList(ExpertiseCode, ExpertiseLevel, city, UpsaleId, RegionLevel, OnCompleteEAuction, OnErrorEx, OnTimeOutEx);
        showDiv();
    }
    function OnCompleteEAuction(arg)
    {
        hideDiv();
        
        if(arg=="empty")
        {
            alert(document.getElementById("<%# lbl_NoResult.ClientID %>").value);
            return;
        }
        else if(arg=="none")
        {
            return;
        }
        document.getElementById('<%# _div_auction.ClientID %>').innerHTML=arg;
        $find('_modalV1').hide();
        $find('_modalV2').show();
    }
    function InitiateCalls()
    {
         if(!Page_ClientValidate('auction'))
            return false;
         var _phone=$get("<%# txt_MyPhone.ClientID %>").value;
         var _desc=$get("<%# txt_RequestDesc.ClientID %>").value; 
         var ddl_expertise = $get("<%# ddl_Expertiser.ClientID %>");
         var _expertise = ddl_expertise.options[ddl_expertise.selectedIndex].text;
     
         
        var UpsaleId=document.getElementById("<%# hf_UpsaleId.ClientID %>").value;
    
         var num_supplier = document.getElementById("form-suppliers");
         var _num = parseInt(num_supplier.options[num_supplier.selectedIndex].value);
   
         Auction.createServiceRequest(_phone, _desc, UpsaleId, _num, OnCompleteRequest, OnErrorEx, OnTimeOutEx);
        showDiv();
         
    }
    
    function OnCompleteRequest(arg)
    {
    
        hideDiv();
        var args=arg.split(";");
        switch(args[0])
        {
       
            case("Success"):
                ShowMessageIncident(args[1], true);
                break;
            default: ShowMessageIncident(args[1], false);
                break;
        }        
        
    }
    function ShowMessageIncident(arg, IsSuccess)
    {
        
        document.getElementById('<%# Div_StatusMessage.ClientID %>').className="popModal_del3"; 
        document.getElementById("<%# lbl_StatusMessage.ClientID %>").innerHTML=arg;
        $find('_modalMessage').show();
        var t=setTimeout("HideMessageIncident("+IsSuccess+");",2000);
    }
    function HideMessageIncident(IsSuccess)
    {
        $find('_modalMessage').hide();
        document.getElementById('<%# Div_StatusMessage.ClientID %>').className="popModal_del3 popModal_del3_hide"; 
        if(IsSuccess)
        {
            ClosePopUpV();
            Complete_request();
        }
    }
    function Complete_request()
    {        
        showDiv();
     //   < GetPostBackEventAfterAuction %>; 
    }
   
    function setPosition()
    {     
        var ua = navigator.userAgent.toLowerCase(); 
       
        if(ua.indexOf("msie")== -1)
        {
            var panel = document.getElementById("<%#Panel_showCity.ClientID %>");
            var elm=document.getElementById('<%#txt_City.ClientID %>');
            panel.style.top=findTop(elm);
            panel.style.left=findLeft(elm);        
            panel.style.position="absolute";
        }
    }
   
    function checkLength(elm, e)
    {
         var code = e.keyCode ? e.keyCode : e.which;
        
        if(code != 13 && elm.value.length > 140)
        {
            elm.value=elm.value.substring(0, elm.value.length-2);
            document.getElementById("<%# lbl_DescLength.ClientID %>").style.display="block";
        }
        else
            document.getElementById("<%# lbl_DescLength.ClientID %>").style.display="none";
    }
    function CountLetter(elm)
    {
        document.getElementById("<%# lbl_count.ClientID %>").innerHTML=elm.value.length;
    }
    function MakeBeforeAction()
    {
        showDiv();
    }
    function CloseUpsaleDetails()
    {
         $find('_modalUpsaleDetails').hide();
         document.getElementById("<%# _iframe.ClientID %>").src = "";
    }