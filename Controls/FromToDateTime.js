﻿/*

*/
var _UrlPathWebServiceSite;
function SetUrlPathWebServiceSite(_path) {
    _UrlPathWebServiceSite = _path;
}
function showCalender(_calender) {
    var calender = $find(_calender);
    calender.show();
}
function HideCalender(_calender, control) {
    var _parent = getParentByClassName(control, "div_from_to_parent");

    var dateMessage = getElementsWithMultiClassesByClass("lbl_wrongDate", _parent, "input")[0].value;//    document.getElementById('<%#lbl_wrongDate.ClientID %>').value;
    var calender = $find(_calender);
    var _format = GetDateFormatFromControl(_parent);
    Hide_Calender(dateMessage, calender, _format, control);

    var start = GetFromElement(_parent);
    var end = GetToElement(_parent);
    var _start = GetDateFormat(start.value, _format);
    var _end = GetDateFormat(end.value, _format);

    if (_start > _end) {
        if (start.id == control.id) {
            end.value = GetDateString(new Date(_start), _format);

        }
        else {
            start.value = GetDateString(new Date(_end), _format);
        }
    }
}
function ShowDivCalender(sender, args) {
    set_div(sender);
 //   sender._popupDiv.style.zIndex = "100000";
}
function HideDivCalender(sender,args) {
    _CheckDateFrom(sender._id);
}
function _CheckDateFrom(_id) {
    var _element = document.getElementById(_id);
    var _parent = getParentByClassName(_element, "div_from_to_parent");
    var _format = GetDateFormatFromControl(_parent);
    var start = GetFromElement(_parent).value;
    var end = GetToElement(_parent).value;
    var _start = GetDateFormat(start, _format);
    var _end = GetDateFormat(end, _format);
    if (_start > _end)
        alert(GetDateError(_parent));

    var params = "_start=" + start + "&_end=" + end;
    
    CallWebService(params, _UrlPathWebServiceSite, 
            function (arg) {
                var start2 = GetFromElement(_parent).value;
                var end2 = GetToElement(_parent).value;
                var _start2 = GetDateFormat(start2, _format);
                var _end2 = GetDateFormat(end2, _format);
                if (_start2 == _start && _end2 == _end)
                    OnCompleteChk(arg, _parent);
            }
            , function () { alert('88'); });
}
function OnCompleteChk(arg, _parent) {

    var txtTo = GetToElement(_parent);
    var txtFrom = GetFromElement(_parent);
    var dates = arg.split(";");
    txtTo.value = dates[0]
    if (dates.length > 1) {
        txtFrom.value = dates[1];
    }

}
function SetDate_Today(elemId) {
    var _elem = document.getElementById(elemId);
    var _parent = getParentByClassName(_elem, "div_from_to_parent");
    var _format = GetDateFormatFromControl(_parent);
    var to_date = new Date();
    var from_date = new Date();
    to_date.addDays(1);
    var txt_from = GetFromElement(_parent)
    var txt_to = GetToElement(_parent)
    txt_from.value = GetDateString(from_date, _format);
    txt_to.value = GetDateString(to_date, _format);
}
function IfSoonerFromToday(elemId) {
    var _elem = document.getElementById(elemId);
    var _parent = getParentByClassName(_elem, "div_from_to_parent");
    var _format = GetDateFormatFromControl(_parent);
    var _today = new Date();
    var _today_only = new Date(_today.getFullYear(), _today.getMonth(), _today.getDate(), 0, 0, 0, 0);
    var date_from = GetFromElement(_parent).value;
    var _from = GetDateFormat(date_from, _format);
    if (_from < _today_only.getTime()) {
//        var dateMessage = getElementsWithMultiClassesByClass("hf_dateError", _parent, "input")[0].value;
//        alert(dateMessage);
        return false;
    }
    return true;
}
function GetDateFormatFromControl(ParentElem) {
    var _elem = getElementsWithMultiClassesByClass("date_format", ParentElem, "input")[0];
    return _elem.value;
}
function GetFromElement(ParentElem) {
    return getElementsWithMultiClassesByClass("From_Date", ParentElem, "input")[0];
}
function GetToElement(ParentElem) {
    return getElementsWithMultiClassesByClass("To_Date", ParentElem, "input")[0];
}
function GetDateError(ParentElem) {
    var _err = getElementsWithMultiClassesByClass("hf_dateError", ParentElem, "input")[0];
    return _err.value;
}
function GetCalenderFrom(ParentElem) {

    var _calender = getElementsByClass("from_id", ParentElem, "input")[0];
    return $find(_calender.id);
}
function GetCalenderTo(ParentElem) {

    var _calender = getElementsByClass("to_id", ParentElem, "input")[0];
    return $find(_calender.id);
}
function Reset_Values(elemId) {
    SetDate_Today(elemId);
    var _elem = document.getElementById(elemId);
    var _parent = getParentByClassName(_elem, "div_from_to_parent");
    var _ddls = _parent.getElementsByTagName("select");
    for (var i = 0; i < _ddls.length; i++) {
        _ddls[i].selectedIndex = 0;
    }
}