﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_invite : System.Web.UI.UserControl
{
    PageSetting ps;
    private int InvitationsLeft;
    protected int InvitationSuccessBonus;
    protected bool RequestedMoreInvites;
    protected string siteId;
    private string _supplierId;
    protected bool JustGotMoreInvites;

    protected void Page_Load(object sender, EventArgs e)
    {
        ps = (PageSetting)Page;
        if (!IsPostBack)
        {            
            siteId = ps.siteSetting.GetSiteID;
            supplierId = ps.GetGuidSetting();
            LoadFriendsData();
        }
    }

    private void LoadFriendsData()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfInvitationData result = null;
        try
        {
            result = _supplier.GetInvitationsData(new Guid(supplierId)); 
            InvitationsLeft=result.Value.InvitationsLeft;
            InvitationSuccessBonus = result.Value.InvitationSuccessBonus;
            //InvitationNowBonus = result.Value.InvitationRegisteredBonus;
            RequestedMoreInvites=result.Value.RequestedMoreInvites;
            JustGotMoreInvites = result.Value.JustGotMoreInvites;
            span_FriendsNum.InnerText = InvitationsLeft.ToString();

       

            // = 0;

            //Response.Write("invitation left: " + InvitationsLeft);

            if (InvitationsLeft > 0)
            {
                inviteButton.Attributes.Add("onclick", "gotoInvitePage();");
               
                int rest = InvitationsLeft % 3;
                //Response.Write("rest:" + rest);
                switch (rest)
                {
                    case 0:
                        friend1.Attributes.Add("class", "invitePro01");
                        friend2.Attributes.Add("class", "invitePro02");
                        friend3.Attributes.Add("class", "invitePro03");
                       
                        break;

                    case 1:
                        friend1.Attributes.Add("class", "invitePro01 invitePro01Registered");
                        friend2.Attributes.Add("class", "invitePro02 invitePro02Registered");
                        break;

                    case 2:                       
                        friend1.Attributes.Add("class", "invitePro01 invitePro01Registered");
                        
                        break;

                    default:
                        friend1.Attributes.Add("class", "invitePro01");
                        friend2.Attributes.Add("class", "invitePro02");
                        friend3.Attributes.Add("class", "invitePro03");
                        break;
                }
            }

            else // 0 invites
            {
                friend1.Attributes.Add("class", "invitePro01 invitePro01Registered");
                friend2.Attributes.Add("class", "invitePro02 invitePro02Registered");
                friend3.Attributes.Add("class", "invitePro03 invitePro03Registered");

                inviteButton.InnerText = "REQUEST MORE INVITES";
                inviteButton.Attributes.Add("onclick", "askMoreInvites();");

                if (RequestedMoreInvites)
                {
                    inviteWait.Attributes.Add("style", "display:block");
                    inviteButton.Attributes.Add("style", "display:none");
                    
                    
                    
                }

                else
                {
                    inviteWait.Attributes.Add("style","display:none");
                    inviteButton.Attributes.Add("style", "display:block");                   
                }
            }

            if (JustGotMoreInvites)
            {
                if (!this.Page.ClientScript.IsStartupScriptRegistered(this.GetType(), "SetServerComment2"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetServerComment2", "SetServerComment('You have been given 3 more NoProblem invitations. Invite your friends and earn $" + InvitationSuccessBonus + " and more in credit!');", true);
                }
            }

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return;

        /*
        var ans = _supplier.GetInvitationsData(Guid.Empty);

        var asn2 = _supplier.InviteYourFriend(
            new WebReferenceSupplier.CreateInvitationRequest()
            {
                
            });
        asn2.Value.Status == WebReferenceSupplier.CreateInvitationStatus.
        */

        
    }

    protected string supplierId
    {
        get
        {
            return _supplierId;
        }

        set
        {
            _supplierId = value;
        }


    }

    
}