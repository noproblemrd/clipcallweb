﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Controls_DropDown_DropDown : System.Web.UI.UserControl
{
    int DefaultWidth = 100;
   
    const int DiffWidth = 36;
    int ul_width = 0;
    int ul_maxHeight = 250;
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadScripts();
        LoadSizes();
  //      SetWidth();
    }

    private void LoadSizes()
    {
        ul_select.Style.Add("max-height", ul_maxHeight.ToString() + "px");
        a_main.Style[HtmlTextWriterStyle.Width] = (DefaultWidth) + "px";
        if (ul_width == 0)
            ul_width = DefaultWidth + 44;
        else
            ul_width += 44;
        ul_select.Style[HtmlTextWriterStyle.Width] = (ul_width) + "px";
    }
   
    void LoadScripts()
    {
        HtmlGenericControl script = new HtmlGenericControl("script");
        script.Attributes.Add("language", "javascript");
        script.Attributes.Add("type", "text/javascript");
        script.Attributes.Add("src", ResolveUrl("~") + @"Controls/DropDown/DropDownJquery.js");
        Page.Header.Controls.Add(script);

        HtmlGenericControl link = new HtmlGenericControl("link");
        link.Attributes.Add("rel", "stylesheet");
        link.Attributes.Add("type", "text/css");
        link.Attributes.Add("href", ResolveUrl("~") + @"Controls/DropDown/DropDownJquery.css");
        Page.Header.Controls.Add(link);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadDropDown", "var drop_down = new DropDown('" + SelectContent.ClientID + "', '" + div_details.ClientID + "'); drop_down.Init();", true);
    }
    public void LoadDetails(ListItem[] items)
    {
        foreach (ListItem li in items)
        {
            HiddenField hf = new HiddenField();
            hf.Value = li.Value + ";;;" + li.Text + ";;;" + li.Selected.ToString().ToLower();
            div_details.Controls.Add(hf);
        }
    }
    public string Title
    {
        get { return SelectTitle.Text; }
        set { SelectTitle.Text = value; }
    }
    /*
    public void SetWidth()
    {
        a_main.Style[HtmlTextWriterStyle.Width] = DefaultWidth + "px";         
    }
    */
    public int DivMaxHeight
    {
        set { ul_maxHeight = value; }
    }
    public int Width
    {
        set
        {
            /*
            int _width = value - DiffWidth;
            a_main.Style[HtmlTextWriterStyle.Width] = _width + "px";
            ul_select.Style[HtmlTextWriterStyle.Width] = (_width + 44) + "px";
             * */
            DefaultWidth = value - DiffWidth;
        }
        
    }
    public int UlWidth
    {
        set
        {
            ul_width = value;
        }
    }
}