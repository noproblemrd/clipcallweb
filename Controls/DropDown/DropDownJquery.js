﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />

function DropDown(_idSelect, _idDetails) {
    this.values = [];    
    this.$container = null;
    this.Init = function () {
        var _this = this;
        $(function () {
            _this.$container = $('#'+_idSelect);
            $('#' + _idDetails).find('input[type="hidden"]').each(function () {
                var _details = $(this).val().split(';;;');
                if (_details.length == 0)
                    return;
                var _val = _details[0];
                var _text = (_details.length < 2) ? _val : _details[1];
                var _select = (_details.length < 3) ? false : (_details[2] == "true");
                _this.values.push(new ListItem(_val, _text, _select));
            });
            var ul = _this.$container.find('ul');
            for(var i = 0; i < _this.values.length; i++){
                var _a = $(document.createElement('a')).attr('href', 'javascript:void(0);').html(_this.values[i].text);
                var _hf = $(document.createElement('input')).attr('type', 'hidden').val(_this.values[i].value);
                var _li = $(document.createElement('li')).append(_a).append(_hf);
                if(_this.values[i].select || i == 0){
                    _this.$container.find('.-SelectText').html(_this.values[i].text);
                    _this.$container.find('.-SelectValue').html(_this.values[i].value);
                }
                ul.append(_li);
            }
            _this.$container.find('.-selectBox-dropdown-menu').click(function(){
                 $(this).toggle();
                _this.$container.find(".-a_selectBox").toggleClass('selectBox-dropdown-clicked');
            });
            _this.$container.find(".-a_selectBox").click(function (evt) {
                _this.$container.find(".-selectBox-dropdown-menu").toggle();
                $(this).toggleClass('selectBox-dropdown-clicked');
                evt.stopPropagation();
            });
            _this.$container.find(".-a_selectBox").mouseover(function () {
                $(this).addClass('selectBox-menuShowing');
                if (_this.$container.find(".-selectBox-dropdown-menu").is(":visible"))
                    $(this).addClass('selectBox-dropdown-clicked');
            });
            _this.$container.find(".-a_selectBox").mouseout(function () {
                if (_this.$container.find(".-selectBox-dropdown-menu").is(":hidden"))
                    $(this).removeClass('selectBox-menuShowing');
            });
            _this.$container.find(".-a_selectBox").blur(function () {
                if (_this.$container.find(".-selectBox-dropdown-menu").is(":visible")) {

                }
            });
            _this.$container.find(".-selectBox-dropdown-menu").click(function (evt) {
                $(this).hide();
            });

            _this.$container.find(".-selectBox-dropdown-menu li").click(function (evt) {
                _this.$container.find('.-SelectText').html($(this).find('a').html());
                _this.$container.find('.-SelectValue').html($(this).find('input[type="hidden"]').val());
         //       $("#subTypeExpertiseSelected").text($(this).text()).css('color', 'black');
         //       $("#linkSubTypeExpertiseSelected").css('borderColor', '#CCCCCC');
         //       cleanErrors();


            });
            /*
            $(".expertiseType .selectBox-dropdown-menu").mouseout(function () {
                //$(".selectBox-dropdown-menu").hide();


            });
            */
            $(document).click(function (event) {
                if (!$(event.target).closest('.-selectBox-dropdown-menu').length) {
                    $('.-selectBox-dropdown-menu').hide();
                };
            });
            $(window).blur(function () {                
               $('.-selectBox-dropdown-menu').hide();                
            });
            _this.$container.find(".-selectBox-dropdown-menu").mouseleave(function (evt) {
          //      $(this).hide();
          //      _this.$container.find(".-a_selectBox").removeClass('selectBox-menuShowing');
          //      _this.$container.find(".-a_selectBox").removeClass('selectBox-dropdown-clicked');
                //mouseout fires when the pointer moves out of the child element as well, while mouseleave fires only when the pointer moves out of the bound element.
            });


            /*
            $(".selectBox-dropdown").mouseleave(function (evt) {
            $(".selectBox-dropdown-menu").hide();
            $(".selectBox-dropdown").removeClass('selectBox-menuShowing');
            //mouseout fires when the pointer moves out of the child element as well, while mouseleave fires only when the pointer moves out of the bound element.
            }
            );
            */
            /*
            $("body").click(function (ev) {

                var myID = ev.target.id;
                //alert(myID);
                if (myID.indexOf('subTypeExpertiseSelected') == -1) {
                    $(".expertiseType .selectBox-dropdown-menu").hide();
                    $(".expertiseType .selectBox-dropdown").removeClass('selectBox-menuShowing');
                    $(".expertiseType .selectBox-dropdown").removeClass('selectBox-dropdown-clicked');
                }
                ///alert(myID);
                // timeSelected

            });
            */


        });

    };
}
function ListItem(value, text, select) {
    this.value = value;
    this.text = text;
    this.select = select;
}
