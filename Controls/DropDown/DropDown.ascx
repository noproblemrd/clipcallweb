﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DropDown.ascx.cs" Inherits="Controls_DropDown_DropDown" %>

<div class="-SelectContent" id="SelectContent" runat="server">                        
    <asp:Label ID="SelectTitle" runat="server" ></asp:Label>
    <div>
        <a tabindex="0" title="" style="display: inline-block; -moz-user-select: none;" class="-a_selectBox" runat="server" id="a_main">
            <asp:Label ID="lbl_text" runat="server" CssClass="-SelectText selectBox-label"></asp:Label>
            <asp:Label ID="lbl_value" runat="server" style="display:none;"  CssClass="-SelectValue"></asp:Label>                 
                <span class="selectBox-arrow"></span>
        </a>         	
	 

        <ul style="-moz-user-select: none; width: 152px;display:none;" class="-selectBox-dropdown-menu" id="ul_select" runat="server">
                    
        </ul>                  
    </div>                      
</div>
<div runat="server" id="div_details" style="display:none;"></div>
               
            
            
