﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Conversion.ascx.cs" Inherits="Controls_Conversion" %>
<asp:Repeater ID="_Repeater" runat="server">
<HeaderTemplate>
    <table class="data-table2">
    <tr>
        <th>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Exposers.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label5" runat="server" Text="<%# lbl_Requests.Text %>"></asp:Label>
        </th>
        <th>
           <asp:Label ID="Label7" runat="server" Text="<%# lbl_RequestsPercent.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label9" runat="server" Text="<%# lbl_Calls.Text %>"></asp:Label>
        </th>
        <th>
           <asp:Label ID="Label11" runat="server" Text="<%# lbl_CallsPercent.Text %>"></asp:Label>
        </th>
    </tr>
</HeaderTemplate>
<ItemTemplate>
    <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="<%# Bind('Date') %>" CssClass="-date"></asp:Label> 
        </td>
        <td>
           <asp:Label ID="Label4" runat="server" Text="<%# Bind('ExposureCount') %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label6" runat="server" Text="<%# Bind('RequestCount') %>"></asp:Label>
        </td>
        <td>
           <asp:Label ID="Label8" runat="server" Text="<%# Bind('RequestPercent') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label10" runat="server" Text="<%# Bind('CallCount') %>"></asp:Label>      
        </td>
        <td>
            <asp:Label ID="Label12" runat="server" Text="<%# Bind('CallsPercent') %>"></asp:Label> 
        </td>
    </tr>
      
</ItemTemplate>
<FooterTemplate>
    </table>
</FooterTemplate>
</asp:Repeater>

 <asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Exposers" runat="server" Text="Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_Requests" runat="server" Text="Requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_RequestsPercent" runat="server" Text="Requests percent" Visible="false"></asp:Label>
<asp:Label ID="lbl_Calls" runat="server" Text="Calls" Visible="false"></asp:Label>
<asp:Label ID="lbl_CallsPercent" runat="server" Text="Calls percent" Visible="false"></asp:Label>
                         