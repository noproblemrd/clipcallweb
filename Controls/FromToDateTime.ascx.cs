﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_FromToDateTime : System.Web.UI.UserControl
{
    private const string CSS_FROM_TEXTBOX = "From_Date";
    private const string CSS_TO_TEXTBOX = "To_Date";
    PageSetting _ps;
    protected void Page_Load(object sender, EventArgs e)
    {
        _ps = (PageSetting)Page;
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        ScriptReference srC = new ScriptReference(ResolveUrl("~") + "Calender/_Calender.js");
        if (!sm.Scripts.Contains(srC))
            sm.Scripts.Add(srC);
        ScriptReference srF = new ScriptReference(ResolveUrl("~") + "Controls/FromToDateTime.js");
        if (!sm.Scripts.Contains(srF))
            sm.Scripts.Add(srF);
        if (!IsPostBack)
        {
            LoadTranslate();
            this.DataBind();
            LoadTimes();
    //        LoadDateFormat();
        }
        if (!sm.IsInAsyncPostBack)
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SetUrlPathWebServiceSite", "SetUrlPathWebServiceSite('" + GetUrlPathWebServiceSite + "');", true);
        SetTextAttributes();
       

    }

    private void LoadTranslate()
    {
        string pagename = "FromToDateTime.ascx";
        DBConnection.LoadTranslateToControl(this, pagename, _ps.siteSetting.siteLangId);
    }

    
    private void LoadTimes()
    {
        for (int i = 0; i < 24; i++)
        {
            string time_hour = ((i < 10) ? "0" + i : i.ToString());
            string time1 = time_hour + ":00";
            LoadTimeToDDLs(time1);            
            time1 = time_hour + ":30";
            LoadTimeToDDLs(time1);
        }
        
    }
    void LoadTimeToDDLs(string _str)
    {
        ListItem li1 = new ListItem(_str, _str);
        ListItem li2 = new ListItem(_str, _str);
        ddl_from.Items.Add(li1);
        ddl_to.Items.Add(li2);
    }
    public void _ErrorDate()
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "GetWrongDate", "alert('" + GetWrongDate + "');", true);
    }
    public void _ErrorDateSooner()
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "GetWrongDate", "alert('" + GetDateError + "');", true);
    }
    private void SetTextAttributes()
    {
        txt_From.Attributes.Add("onfocus", "javascript:showCalender('" + hf_From.ClientID + "');");
        txt_From.Attributes.Add("onblur", "javascript:HideCalender('" + hf_From.ClientID + "', this);");
        txt_To.Attributes.Add("onfocus", "javascript:showCalender('" + hf_To.ClientID + "');");
        txt_To.Attributes.Add("onblur", "javascript:HideCalender('" + hf_To.ClientID + "', this);");
    }
    public string CssFromTextbox
    {
        set { txt_From.CssClass = CSS_FROM_TEXTBOX + " " + value; }
    }
    public string CssToTextbox
    {
        set { txt_To.CssClass = CSS_TO_TEXTBOX + " " + value; }
    }
    public string CssFromSelect
    {
        set { ddl_from.CssClass += " " + value; }
    }
    public string CssToSelect
    {
        set { ddl_to.CssClass += " " + value; }
    }
    public string DateFormat
    {
        set
        {
            TextBoxWatermarkExtender_From.WatermarkText = value.ToLower();
            TextBoxWatermarkExtender_To.WatermarkText = value.ToLower();
            CalendarExtender_From.Format = value;
            CalendarExtender_To.Format = value;
            hf_DateFormat.Value = value;
        }
    }
    protected string GetDateError
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_dateError.Text); }
    }
    protected string GetWrongDate
    {
        get { return HttpUtility.JavaScriptStringEncode(lblWrongDate.Text + " " + hf_DateFormat.Value); }
    }
    private string GetUrlPathWebServiceSite
    {
        get { return ResolveUrl("~") + "WebServiceSite.asmx/SetDate"; }
    }
    public DateTime GetDateFrom
    {
        get
        {

            if (string.IsNullOrEmpty(txt_From.Text))
                return DateTime.MinValue;            
            DateTime date = ConvertToDateTime.CalanderToDateTime(txt_From.Text, _ps.siteSetting.DateFormat);
            string[] times = ddl_from.SelectedValue.Split(':');
            date = date.AddHours((double)int.Parse(times[0]));
            date = date.AddMinutes((double)int.Parse(times[1]));
            return date;
        }
    }
    public DateTime GetDateTo
    {
        get
        {
            if (string.IsNullOrEmpty(txt_To.Text))
                return DateTime.MinValue;
            DateTime date = ConvertToDateTime.CalanderToDateTime(txt_To.Text, _ps.siteSetting.DateFormat);
            string[] times = ddl_to.SelectedValue.Split(':');
            date = date.AddHours((double)int.Parse(times[0]));
            date = date.AddMinutes((double)int.Parse(times[1]));
            return date;
        }
    }
    public void SetDate_To_Today(bool _Today)
    {

        if (!_Today)
            return;
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SetDate_Today", "SetDate_Today('" + hf_From.ClientID + "');", true);
        
    }
    public string GetScriptIfDateNotSooner()
    {
        return "IfSoonerFromToday('" + hf_From.ClientID + "')";
    }
    public string GetClientScriptResetValues()
    {
        return "Reset_Values('" + hf_From.ClientID + "')";
    }
    public void SetDateFrom(DateTime date_from)
    {
        txt_From.Text = string.Format(_ps.siteSetting.DateFormat, date_from);
        SetTimeInDDL(date_from, ddl_from);
    }
    public void SetDateTo(DateTime date_to)
    {
        txt_To.Text = string.Format(_ps.siteSetting.DateFormat, date_to);
        SetTimeInDDL(date_to, ddl_to);
    }
    private void SetTimeInDDL(DateTime date, DropDownList _ddl)
    {
        string _time = string.Format(_ps.siteSetting.GetDefaultTimeFormat, date);
        for (int i = 0; i < _ddl.Items.Count; i++)
        {
            if (_time == _ddl.Items[i].Value)
            {
                _ddl.SelectedIndex = i;
                return;
            }
        }
        string[] hour_min = _time.Split(':');
        string NewTime = "";
        int _min = int.Parse(hour_min[1]);
        int _promote = 0;
        if (_min > 30)
        {
            NewTime = "00";
            _promote = 1;
        }
        else
        {
            if (_min > 0)
                NewTime = "30";
            else
                NewTime = "00";
        }
        int _hour = int.Parse(hour_min[0]) + _promote;
        if (_hour > 20)
            NewTime = "20:30";
        else
            NewTime = ((_hour > 9) ? "" : "0") + _hour + ":" + NewTime;

        for (int i = 0; i < _ddl.Items.Count; i++)
        {
            if (NewTime == _ddl.Items[i].Value)
            {
                _ddl.SelectedIndex = i;
                return;
            }
        }
    }
}