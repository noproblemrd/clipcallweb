﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_searchGoogle : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write("lat:" + lat + " lng:" + lng);
    }

    public string region
    {
        get { return txt_Region.Text; }
        set
        {
            txt_Region.Text = value;
            setChooseFromList();


        }
    }


    public void setChooseFromList()
    {
        string _script = "setChooseFromList(true);";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "setChooseFromList", _script, true); 
    }

    public decimal lat
    {
        get { return decimal.Parse(hiddem_lat.Value); }
        set { hiddem_lat.Value = value.ToString(); }
    }

    public decimal lng
    {
        get { return decimal.Parse(hiddem_lng.Value); }
        set { hiddem_lng.Value = value.ToString(); }
    }

    public string state
    {
        get { return hidden_state.Value; }
        set { hidden_state.Value = value; }
    }
}