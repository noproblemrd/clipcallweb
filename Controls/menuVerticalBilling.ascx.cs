﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_menuVerticalBilling : System.Web.UI.UserControl
{
    public string whoActive;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (whoActive == "billingOverview")
        {            
            linkStep1.Attributes.Add("class", "active mouseover");
        }


        else if (whoActive == "defaultPriceSettings")
        {            
            linkStep2.Attributes.Add("class", "active mouseover");
        }

        else if (whoActive == "paymentMethod")
        {            
            linkStep3.Attributes.Add("class", "active mouseover");
        }
            
        else if (whoActive == "transactions")
        {            
            linkStep4.Attributes.Add("class", "active mouseover");
        }

        if (Session["plan"].ToString() == "FEATURED_LISTING")
            linkStep2.Visible = false;

        string host;
        host = Request.Url.Host;

        string urlPayment;
        if (host == "localhost")
            urlPayment = "http://" + host + ":" + Request.Url.Port + ResolveUrl("~") + "ppc/" + "PaymentMethod.aspx";
        else
            urlPayment = "http://" + host + "/ppc/" + "PaymentMethod.aspx";

        linkStep3.HRef = urlPayment;

        

    }
}