﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_menuVerticalFaq : System.Web.UI.UserControl
{
    public string whoActive;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (whoActive == "faq")
        {
            linkStep1.Attributes.Add("class", "active mouseover");
        }


        else if (whoActive == "tutorials")
        {
            linkStep2.Attributes.Add("class", "active mouseover");
        }
    }
}