﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class Controls_MasterPagePPC : System.Web.UI.MasterPage
{
    PageSetting ps;
    string pageName;
    protected const string ATTRIBUTE = "isauthentic";
    
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);       
         ps = (PageSetting)Page;
         //Security- for the browser not save in the cash memory
         Response.Cache.SetCacheability(HttpCacheability.NoCache);
         Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
         Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
         Response.Expires = 0;
         Response.CacheControl = "no-cache";
         Response.Cache.SetNoStore();
         Response.AppendHeader("Pragma", "no-cache");
        ////
         if (!ps.userManagement.IsSupplier() || ps.siteSetting.GetSiteID != PpcSite.GetCurrent().SiteId)
             Response.Redirect(ResolveUrl("~") + "Management/logout.aspx?referrer=" + HttpUtility.UrlDecode(Request.Url.PathAndQuery));
         if (!IsPostBack)
         {
             LoadSupplierStatus();
         }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
        
        if (!IsPostBack)
        {
           
            lbl_name.Text = ps.userManagement.User_Name;
            lbl_currency.Text = _Currency;
            /*
            if (Session["balance"] != null)
                lbl_Balance.Text = (string)Session["balance"];
            else
                lbl_Balance.Text = _Balance;
             * */
            if(string.IsNullOrEmpty(UpdateSuccess))
                SetTextFromXml();
            SetMenu();
            
       //     LoadLinks();
        }
   //     Page.ClientScript.RegisterStartupScript(this.GetType(), "LogOutOnSessionEnd", "LoadLogOutPath('" + ResolveUrl("~/Management/LogOut.aspx") + "?reason=" + eReasonLogout.SessionEnd.ToString() + "');", true);
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoadDetails", "LoadDetails('" + GetLogOutMessage + "', '" + GetLogOutPath + "');", true);

        Page.Header.DataBind();
        
    }
    /*
    private void LoadLinks()
    {
        string name = string.IsNullOrEmpty(((PageSetting)Page).userManagement.User_Name) ? "NULL" : ((PageSetting)Page).userManagement.User_Name;
        ParamToAnchor pta = new ParamToAnchor(this, ATTRIBUTE, name);
        pta.StartTo();
    }
     * */
    /*
    public void SetBreadCrumb(List<KeyValuePair<string, string>> crumbs)
    {
        _Breadcrumb.LoadBreadCrumb(crumbs);
    }
     * */
    private void LoadSupplierStatus()
    {
        AccountStatus _status = new AccountStatus();
        WebReferenceSupplier.SupplierStatusContainer result = _status._GetAccountStatus(ps.userManagement);
        
        a_OffDuty.HRef = GetOnDutyLink;
        if (result == null)
        {
            div_OffDuty.Visible = false;
            div_MainMessage.Visible = false;
            ps.Update_Faild();
            return;
        }
        ps.userManagement.balance = (double)result.Balance;
        lbl_Balance.Text = string.Format(ps.GetNumberFormat, result.Balance);
        if (result.FistPaymentNeeded)
        {
            div_MainMessage.Visible = true;
            ServerMessage sm = new ServerMessage(GetSentences().Element("UnavailableMessage").Element("NewUserAddCredit"));
            lt_MainMessage.Text = sm.GetHtmlMessage(ResolveUrl("~/PPC/AddCredit.aspx"));
            a_OffDuty.Attributes.Add("class", "_suspended");
            a_OffDuty.InnerHtml = "OFF DUTY";
            a_OffDuty.HRef = "";

        }
        else if (result.UnavailabilityReasonId != Guid.Empty)
        {
            


            div_MainMessage.Visible = true;
            ServerMessage sm;
            XElement xelem = GetSentences().Element("UnavailableMessage");
            
            switch (result.UnavailavilityType)
            {
                case (WebReferenceSupplier.UnavailavilityTypes.UserCreated):
                    sm = new ServerMessage(xelem.Element("UserCreated"));
                    lt_MainMessage.Text = sm.GetHtmlMessage("javascript:SetEndSuspended();", ResolveUrl("~/PPC/FramePpc.aspx?step=4"));
                    break;
                case (WebReferenceSupplier.UnavailavilityTypes.OutOfMoney):
                    sm = new ServerMessage(xelem.Element("OutOfMoney"));
                    lt_MainMessage.Text = sm.GetHtmlMessage(ResolveUrl("~/PPC/AddCredit.aspx"));
                    break;
                case (WebReferenceSupplier.UnavailavilityTypes.OverRefundPercent):
                    sm = new ServerMessage(xelem.Element("OverRefundPercent"));
                    lt_MainMessage.Text =  string.Format(sm.Message, result.DaysToEndOfOverRefundPercent.ToString());
                    break;
                case (WebReferenceSupplier.UnavailavilityTypes.DailyBudget):
                    sm = new ServerMessage(xelem.Element("DailyBudget"));
                    lt_MainMessage.Text = sm.GetHtmlMessage(ResolveUrl("~/Management/ProfessionalSetting.aspx"));
                    break;
            }
            a_OffDuty.Attributes.Add("class", "suspended");
            a_OffDuty.InnerHtml = (result.IsAvailableHours) ? "ON DUTY" : "OFF DUTY";
        }
        else
        {
            div_MainMessage.Visible = false;
            if (result.IsAvailableHours)
            {
                a_OffDuty.Attributes.Add("class", "OnDuty");
                a_OffDuty.InnerHtml = "ON DUTY";
            }
            else
            {
                a_OffDuty.Attributes.Add("class", "OffDuty");
                a_OffDuty.InnerHtml = "OFF DUTY";
            }
        }
        /*
        WebReferenceSupplier.UnavailavilityTypes.DailyBudget;
        WebReferenceSupplier.UnavailavilityTypes.OutOfMoney;
        WebReferenceSupplier.UnavailavilityTypes.OverRefundPercent;
        WebReferenceSupplier.UnavailavilityTypes.UserCreated;
         * */
    }
    public void SetOffBreadCrumb()
    {
        _SiteMapPath.Visible = false;
    }
    void SetTextFromXml()
    {
        XElement xelem = GetSentences();
        UpdateSuccess = xelem.Element("FormMessages").Element("UpdateSuccess").Value;
        UpdateFaild = xelem.Element("FormMessages").Element("UpdateFaild").Value;
        NoNewData = xelem.Element("FormMessages").Element("NoNewData").Value;
        
    }
    XElement GetSentences()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "MasterPage"
                          select x).FirstOrDefault();
        return xelem;
    }
    
    protected string WidgetScript
    {
        get
        {
            return ResolveUrl("~") + "Formal/Scripts/widget.js";
        }
    }
    protected string EndSuspended
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/ToAvailability"); }
    }
    protected string GetOnDutyLink
    {
        get { return ResolveUrl("~/PPC/FramePpc.aspx") + "?step=4"; }
    }
    protected string GetStyle
    {
        get { return ResolveUrl("~") + "PPC/samplepPpc.css"; }
    }
    protected string GetHeaderStyle
    {
        get { return ResolveUrl("~") + "PPC/HeaderStyle.css"; }
    }
    protected string GetSGeneraltyle
    {
        get { return ResolveUrl("~") + "Management/style.css"; }
    }
    protected string GetAddCreditPath()
    {
        return ResolveUrl("~") + "PPC/AddCredit.aspx";
    }
    protected string _Currency
    {
        get { return ps.siteSetting.CurrencySymbol; }
    }
    protected string _Balance
    {
        get { return string.Format(ps.GetNumberFormat, ps.userManagement.balance); }
    }
    /*navigate*/
    private void SetMenu()
    {
        pageName =
            (System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"])).ToLower();

        if (IsPageMatch(a_overView))
        {
            a_overView.Attributes.Add("class", "btn-myoverview active");
            btn_myoverview.Attributes.Add("class", "_btn_myoverview active");
   //         navigationArrow.Attributes.Add("style", "left:58px");
        }

        else if (IsPageMatch(a_frameAspx))
        {
            a_frameAspx.Attributes.Add("class", "btn-myaccount active");
            btn_myAccount.Attributes.Add("class", "_btn_myaccount active");
  //          navigationArrow.Attributes.Add("style", "left:128px");
        }


        else if (IsPageMatch(linkProfessionalUpdate))
        {
            linkProfessionalUpdate.Attributes.Add("class", "btn-myminisite active");
            btn_myminisite.Attributes.Add("class", "_btn_myminisite active");
        }

        else if (IsPageMatch(linkSetting))
        {
            linkSetting.Attributes.Add("class", "btn-faq active");
            btn_faq.Attributes.Add("class", "_btn_setting active");
  //          navigationArrow.Attributes.Add("style", "left:297px");
        }
        else if (IsPageMatch(a_Billing) || IsPageMatch(a_AddCredit) || IsPageMatch(a_transactions))
        {
            a_Billing.Attributes.Add("class", "active");
            Li_Billing.Attributes.Add("class", "active");            
            if (IsPageMatch(a_AddCredit))
                a_AddCredit.Attributes.Add("class", "active");
            else if (IsPageMatch(a_transactions))
                a_transactions.Attributes.Add("class", "active");
            
        }

        else if (IsPageMatch(a_earnCredit) || IsPageMatch(a_inviteAFriend))
        {
            a_earnCredit.Attributes.Add("class", "active");
            btn_earnCredit.Attributes.Add("class", "active");
            if (IsPageMatch(a_inviteAFriend))
                a_inviteAFriend.Attributes.Add("class", "active");
           
           
        }

        else if (IsPageMatch(linkHelp))
        {
            linkHelp.Attributes.Add("class", "btn-help active");
            btn_help.Attributes.Add("class", "_btn_help active");
            //         navigationArrow.Attributes.Add("style", "left:363px");
        }
/*
        else if (IsPageMatch(linkCalls) ||
                IsPageMatch(a_balance) ||
                IsPageMatch(a_MyReview))
        {
            linkCalls.Attributes.Add("class", "btn-calls active");
            btn_calls.Attributes.Add("class", "_btn_calls active");

            if (IsPageMatch(a_linkCalls))
            {
                a_linkCalls.Attributes.Add("class", "active");
                //           navigationArrow.Attributes.Add("style", "left:209px");
            }

            else if (IsPageMatch(a_balance))
                a_balance.Attributes.Add("class", "active");

            else if (IsPageMatch(a_MyReview))
                a_MyReview.Attributes.Add("class", "active");
        }
 * */
    }
    bool IsPageMatch(HtmlAnchor _a)
    {
        string[] paths = _a.HRef.Split('/');
        string _path = paths[paths.Length - 1];
        if (_path.Contains('?'))
            _path = _path.Split('?')[0];
        return (_path.ToLower() == pageName);
    }
    public bool IfHas4MenueItems()
    {
        return !span_forlink.Visible;
    }
    public void TurnOnOffServerComment(bool TurnOn)
    {
        _comment.Visible = TurnOn;
    }
    protected string GetLogOut
    {
        get { return ResolveUrl("~") + "Management/logout.aspx"; }
    }
    protected string EndSessionMessage
    {
        get { return HttpUtility.JavaScriptStringEncode("Your session has expired. Please login again."); }
    }
    protected string JSGeneral
    {
        get { return ResolveUrl("~") + "PPC/script/JSGeneral.js"; }
    }
    protected string UpdateSuccess
    {
        get { return (ViewState["UpdateSuccess"] == null)?string.Empty: (string)ViewState["UpdateSuccess"]; }
        set { ViewState["UpdateSuccess"] = value; }
    }
    protected string UpdateFaild
    {
        get { return (ViewState["UpdateFaild"] == null) ? string.Empty : (string)ViewState["UpdateFaild"]; }
        set { ViewState["UpdateFaild"] = value; }
    }

    protected string NoNewData
    {
        get { return (ViewState["NoNewData"] == null) ? string.Empty : (string)ViewState["NoNewData"]; }
        set { ViewState["NoNewData"] = value; }
    }
    public string GetLogOutMessage
    {
        get
        {
            return HttpUtility.HtmlEncode("Your session has expired. Please login again.");
        }
    }
    public string GetLogOutPath
    {
        get
        {
            return ResolveUrl("~/Management/LogOut.aspx");
        }
    }
    protected string SetAccountStatus
    {
        get { return ResolveUrl("~/Publisher/AccountStatus.asmx/GetAccountStatusSupplier"); }
    }
    protected string ParamsToCorporateLink
    {
        get { return string.IsNullOrEmpty(((PageSetting)Page).userManagement.User_Name) ? "NULL" : ((PageSetting)Page).userManagement.User_Name; }
    }
}
