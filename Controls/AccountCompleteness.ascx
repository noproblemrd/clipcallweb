﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountCompleteness.ascx.cs" Inherits="Controls_AccountCompleteness" %>
<div class="AccountCompleteness">
    <h4>Account Completeness</h4>
    <div class="div_AccountCompletenessBorder">
        <div class="div_AccountCompletenessScal" runat="server" id="div_AccountCompletenessScal">
            <asp:Label ID="lbl_AccountCompleteness" runat="server" Text=""></asp:Label>
        </div>
    
    </div>
    <div class="div_AccountCompletenessLink" runat="server" id="div_AccountCompletenessLink">
        <a runat="server" id="a_AddPaymentMethod" href="javascript:void(0);">Add credit</a>
        <asp:Label ID="lbl_PercentageInLink" runat="server" ></asp:Label>
    </div>
</div>