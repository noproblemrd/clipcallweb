﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhoneAvailability.ascx.cs" Inherits="Controls_PhoneAvailability" %>
<!--<script type="text/javascript" src="../scripts/FusionCharts/FusionCharts.js"></script>-->
<script type="text/javascript">
/*
    var dataString = "<graph  caption='Marketing Expenses'  shownames='1' showPercentageinLabel='1' showPercentageValues='1' numberPrefix='$' decimalPrecision='2'    bgColor='f1f1f1'>" +
            "<set value='212000' name='Banners' color='99CC00' alpha='60'/>" +
        "<set value='96800' name='Print Ads' color='7E7E7E'/>" +
        "<set value='26400' name='Service' color='C4D0A2'/>" +
        "<set value='29300' name='Others' color='ACACAC'/>" +
"</graph>";
*/
/*
    var dataString = '<chart caption="Marketing Expenses" bgColor="FFFFFF,CCCCCC" showPercentageValues="1" plotBorderColor="FFFFFF" numberPrefix="$" isSmartLineSlanted="0" showValues="0" showLabels="0" showLegend="1">' +
	'<set value="35" label="ON DUTY" color="99CC00" alpha="60"/>' +
	'<set value="65" label="OFF DUTY" color="333333" alpha="60"/>' +
'</chart>';
*/
/*
    $(function () {
        FusionCharts.setCurrentRenderer('javascript');
        var chart = new FusionCharts("<# GetChartSwf %>", "__ChartId", "200", "200", "0", "0");
        chart.setXMLData(dataString);
        chart.render("__chartdiv");
    });
    */
    /*
    google.load("visualization", "1", { packages: ["corechart"] }); //corechart
    google.setOnLoadCallback(drawChart);
   */
    google.load('visualization', '1', { packages: ['imagepiechart'] });
    google.setOnLoadCallback(drawChart);

    function drawChart(){
        //  var __data = <%# _GetChartData %>
        var __data = new google.visualization.DataTable();
        __data.addColumn('string', 'name'); // Implicit domain label col.
        __data.addColumn('number', 'percent'); // Implicit series 1 data col.
       
        __data.addRows([
  
  ['off duty', <%# OFF_DUTY %>],
  ['on duty', <%# ON_DUTY %>]

]);
/*
        var __options = {
            width: 200, height: 200,
            colors: ['#ECD078', '#D95B43'],
            legend: { position: 'none' },
            pieSliceText: 'none'
           
            
        };
       */
        var __options = {
            legend: 'none',
            height: 150,
            width: 150,
            colors: ['#ff763c', '#bbd40f']
        };
        // Create and draw the visualization.
        //    var _chart =   new google.visualization.PieChart(document.getElementById('__chartdiv'));
      var _chart =  new google.visualization.ImagePieChart(document.getElementById('__chartdiv'));
      _chart.draw(__data, __options);
     
    }
</script>
<div class="PhoneAvailability">
    <h4>Phone Availability Hours</h4>
    <div runat="server" id="ChartDivParent">
        <div id="__chartdiv"></div>
    </div>
    <div class="div-grey-balloon div_OnDuty" runat="server" id="div_OnDuty">
        <div class="div_info">
            <asp:Label ID="span_OnDuty" runat="server"></asp:Label>
            <span>on duty</span>
        </div>
    </div>
    <div class="div-grey-balloon div_OffDuty" runat="server" id="div_OffDuty">
        <div class="div_info">
            <asp:Label ID="span_OffDuty" runat="server"></asp:Label>
            <span>off duty</span>
        </div>
    </div>
    <div class="white-circle" runat="server" id="WhiteCircle">
        <asp:Label ID="span_percent" runat="server" CssClass="span-percent"></asp:Label>
        <span>ON DUTY</span>
    </div>
    <div class="div_ExtendHours">
        <a href="javascript:void(0);" id="a_ExtendHours" runat="server">Extend hours</a>
    </div>
</div>