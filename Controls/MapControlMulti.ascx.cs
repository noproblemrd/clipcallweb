﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Collections;


public partial class Controls_MapControlMulti : System.Web.UI.UserControl
{
    Guid _RegionId;
    bool _selectAll = false;
    public string widthMap="869px";
    public string heightMap="226px";
    
    public delegate void SendEventHandler(object sender, SendEventArgs e);
    public event SendEventHandler SendStr;

    protected void Page_Load(object sender, EventArgs e)
    {   
            ScriptManager1.RegisterAsyncPostBackControl(lbAddButton);   
        

            if (!IsPostBack)
            {
                div_map.Attributes.Add("style", "display:none;width:" + widthMap + "; height:" + heightMap + ";");
                arrayWhoStay = new ArrayList();
                listLocatinData = new List<LocationData>();
                LoadLocation();
                LoadPanelLocation(false);
                initializeMultiMap();
                
                

            }

            else
            {
                index++;
                //Response.Write("index before: " + index);
                
                LoadPanelLocation(true);

            }


            //Response.Write("index: " + index);

            this.Page.Header.DataBind();

            this.DataBind();
        
    }

    public void SetFullAddress(string _address, int index)
    {
        string _script = "SetFullAddress('" + _address + "'," + index + ");";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "SetFullAddress" + index, _script, true);       
    }

    public void SetMap(string radius, string lat, string lng, int index)
    {
        if (index == 1)
        {
            string _script = "SetMapOnLoad('" + radius + "', '" + lat + "', '" + lng + "'," + index + ");";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SetMapOnLoad" + index, _script, true);
        }

        /*
        if (IsMoreSelected)
        {
            XElement xarea = _areas.Element("SelectedAreas");
            div_places.Style[HtmlTextWriterStyle.Display] = "block";
            foreach (XElement _xel in xarea.Elements("Area"))
            {
                string _level = _xel.Attribute("Level").Value;
                string RegionId = _xel.Attribute("RegionId").Value;
                HtmlAnchor _ha = new HtmlAnchor();
                bool __selectedall = false;
                switch (_level)
                {

                    case ("ALL"): _ha = a_Region0;
                        __selectedall = true;
                        break;
                    case ("1"): _ha = a_Region1;
                        goto default;
                    case ("2"): _ha = a_Region2;
                        goto default;
                    case ("3"): _ha = a_Region3;
                        goto default;
                    default:
                        _ha.InnerHtml = _xel.Attribute("Name").Value;
                        _ha.Attributes.Add("_id", RegionId);
                        break;
                }

                if (_xel.Attribute("Selected").Value == "true")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SetLargeArea", "_SetLargeArea(document.getElementById('" + _ha.ClientID + "'));", true);
                    Guid.TryParse(RegionId, out _RegionId);
                    _selectAll = __selectedall;
                    hf_SavedRegionId.Value = (__selectedall) ? "ALL" : _RegionId.ToString();
                }
            }
        }
        else
            div_places.Style[HtmlTextWriterStyle.Display] = "none";
        */
    }


    private void LoadLocation()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);        
        string strWorkAreas;

        /*
        index++;
        return;
        */

        try
        {
            
            WebReferenceSupplier.GetCoverAreaRequest getCoverAreaRequest=new WebReferenceSupplier.GetCoverAreaRequest();
            getCoverAreaRequest.SupplierId = SupplierId;
            
            WebReferenceSupplier.ResultOfString resultOfString=new WebReferenceSupplier.ResultOfString();
            resultOfString = _supplier.GetCoverAreas_Registration2014(getCoverAreaRequest);

            if (resultOfString.Type == WebReferenceSupplier.eResultType.Failure)
            {

                string _script = @"_SetServerComment(""" + HttpUtility.HtmlEncode(ServerError) + @""");";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerProblem", _script, true);

                dbug_log.ExceptionLog(new Exception("failed retrieve GetCoverAreas_Registration2014 for " + getCoverAreaRequest.SupplierId.ToString() + " message: " + resultOfString.Messages[0]));
                return;
            }

            else
            {
                strWorkAreas = resultOfString.Value;
            }

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            return;
        }
              

        /*
        strWorkAreas = @"<?xml version=""1.0"" encoding=""utf-16""?>
        <Areas xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
          <Area Longitude=""-112.07403729999999"" Latitude=""33.4483771"" Radius=""6dffe2a3-3a2a-e111-bd7e-001517d10f6e"" FullAddress=""Phoenix, AZ, United States"">
            <SelectedAreas>
              <LargeArea RegionId=""6dffe2a3-3a2a-e111-bd7e-001517d10f6e"" Name=""Phoenix"" Level=""3"" />
              <LargeArea RegionId=""f23a637c-392a-e111-bd7e-001517d10f6e"" Name=""Maricopa"" Level=""2"" />
              <LargeArea RegionId=""cf31ce5b-392a-e111-bd7e-001517d10f6e"" Name=""AZ"" Level=""1"" />
            </SelectedAreas>
          </Area>
        </Areas>";
        */


        
        XDocument doc = XDocument.Parse(strWorkAreas);
        

        if (doc.Element("WorkArea") != null)
            return;
        if (!doc.Element("Areas").HasElements)
        {
            // if there isn't childs it is the beginning
            index++;
            return;
        }

        /*
        index++;
        return;
        */

        SendEventArgs send = new SendEventArgs(doc.Element("Areas").Attribute("SubscriptionPlan").Value);

        if (SendStr != null)
            SendStr(this, send);
        /*
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.SetPlanType(elem.Attribute("SubscriptionPlan").Value; 
        */


        // must be at least one area from fulfill business detials in step 3
        foreach (XElement elem in doc.Element("Areas").Elements("Area"))
        {
            index++;

            string radius = elem.Attribute("Radius").Value;//.Replace(',', '.');
            if (radius == "all")
                radius = "ALL";

            //  double _radius = Math.Round(double.Parse(radius), 2);
            string selectedArea;
            selectedArea = radius;            


            decimal _radiusM = (Utilities.IsGUID(radius) || radius == "ALL") ? 35m : decimal.Parse(radius.Replace(',', '.'));
            double _radius = (Utilities.IsGUID(radius) || radius == "ALL") ? 35 : double.Parse(radius.Replace(',', '.')) / 1.6; // in the xml , crm the radius is real the radius and not miles
            //  double _radius = (double)_radiusM;
            _radius = Math.Round(_radius, 2);
            radius = _radius.ToString();

            string lat = elem.Attribute("Latitude").Value.Replace(',', '.');
            string lng = elem.Attribute("Longitude").Value.Replace(',', '.');           
           
            string FullAddress = elem.Attribute("FullAddress").Value;            

            XElement namedAreas = elem.Element("SelectedAreas");
            
            List<LocationDataNamedRegions> listNamedRegions = new List<LocationDataNamedRegions>();
            if (namedAreas.HasElements)
            {
                int indexNamedRegions=0;
                foreach (XElement _xel in namedAreas.Elements("LargeArea"))
                {                

                    string Level = _xel.Attribute("Level").Value;
                    string Name = _xel.Attribute("Name").Value;   
                    string RegionId = _xel.Attribute("RegionId").Value;

                    LocationDataNamedRegions namedRegion = new LocationDataNamedRegions();
                    namedRegion.RegionId = RegionId;
                    namedRegion.Name = Name;
                    namedRegion.Level = Level;

                    listNamedRegions.Add(namedRegion);

                    indexNamedRegions++;

                }
            }

            LocationData ld = new LocationData();

            ld.Radius = _radiusM;
            ld.Lat = decimal.Parse(lat);
            ld.Lng = decimal.Parse(lng);
            ld.RegionId = Region_ID;            
            ld.SelectAll = Selected_All;
            ld.FullAddress = FullAddress;
            ld.namedRegions = listNamedRegions.ToArray();          
            
            ld.SelectedArea = selectedArea;
            bool selectedAreaIsArea = false;

            for (int iRegions = 0; iRegions < ld.namedRegions.Length; iRegions++)
            {
                
                if (ld.namedRegions[iRegions].RegionId == ld.SelectedArea)
                {
                    if (Utilities.IsGUID(ld.SelectedArea))
                        ld.SelectedAreaValue = ld.SelectedArea + "=" + ld.namedRegions[iRegions].Level;
                    else if (ld.SelectedArea == "ALL")
                    {
                        ld.SelectedAreaValue = "ALL=ALL";
                    }

                    selectedAreaIsArea = true;                   
                }

               
            }

            if (!selectedAreaIsArea)
                ld.SelectedAreaValue = ld.SelectedArea;

            listLocatinData.Add(ld);

            LocationDataV = ld;
        }


       
       


    }

    private void LoadPanelLocation(bool isPostback)
    {
        //index++;        

        pnlAddLocation.Controls.Clear();

        HtmlGenericControl pnlAddServicesTitle = new HtmlGenericControl("div");
        pnlAddServicesTitle.Attributes["class"] = "pnlAddServicesTitle";        

        pnlAddLocation.Controls.Add(pnlAddServicesTitle);


        for (int i = 0; i < index; i++)
        {

            /* example how it looks in html
             <div class="inputContainer">         
    
                <input id="searchTextField1" type="text" size="50"   class="textActive" place_holder="<%# LocationPlaceHplder %>"
                onkeyup="ChkLocationOnKeyPress(this, event);" onkeydown="return (event.keyCode!=13);" HolderClass="place-holder"  />
   

                <div class="inputValid" style="display:none;"  id="div_LocationSmile1">
                
                </div>
                <div class="inputError" style="display:none;"  id="div_LocationError1">
               
                </div>
                <div class="inputComment" style="display:none;" id="div_LocationComment1">
                        <%= LocationPlaceHplder %>
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

                <asp:DropDownList runat="server" ID="ddlCoverArea1" class="coverArea">
                    <asp:ListItem>0.5mi</asp:ListItem>
                    <asp:ListItem>1mi</asp:ListItem>
                    <asp:ListItem>2mi</asp:ListItem>
                    <asp:ListItem>3mi</asp:ListItem>
                    <asp:ListItem>10mi</asp:ListItem>
                    <asp:ListItem>20mi</asp:ListItem>
                    <asp:ListItem>30mi</asp:ListItem>

                </asp:DropDownList>
    
                <asp:Label runat="server" ID="lblZipCode" class="numberZipCodes">100</asp:Label>
               <div class="clear"></div>
    
            </div>
            */

            /* must set here in page load and not in click events to make view state for the controls !!!*/

            //if (!arrayWhoStay.Contains(i)) // because ther isn't value zero all time in the beginnig of loop add max number to arrayWhoStay            
            arrayWhoStay.Add(Convert.ToInt32(GetMaxValue(arrayWhoStay) + 1));

            HtmlGenericControl inputCategoryContainer = new HtmlGenericControl("div");
            inputCategoryContainer.ID = "inputCategoryContainer" + arrayWhoStay[i];
            inputCategoryContainer.Attributes["class"] = "inputContainer";


            TextBox tb = new TextBox();
            tb.ID = "searchTextField" + arrayWhoStay[i];            
            tb.CssClass = "textActive";
            tb.Attributes.Add("place_holder", LocationPlaceHplder);
            tb.Attributes.Add("holderClass", "place-holder");
            tb.Attributes.Add("type","text");
            //tb.Attributes.Add("onkeyup","ChkLocationOnKeyPress(this, event);");
            tb.Attributes.Add("onkeydown","return (event.keyCode!=13);");

      
            if (!isPostback)
            {
                if (listLocatinData.Count()> 0)
                {
                    tb.Text = listLocatinData[i].FullAddress;
                }
                    
            }           

            HtmlGenericControl serviceSmile = new HtmlGenericControl("div");
            serviceSmile.ID = "div_LocationSmile" + arrayWhoStay[i];
            serviceSmile.Attributes["class"] = "inputValid";


            if (!isPostback)
            {
                if (listLocatinData.Count() > 0)
                {
                    serviceSmile.Attributes.Add("style", "display:block;width:1px;height:1px;");
                    /*
                    serviceSmile.Attributes.Add("style", "width:29px;")
    left: 630px;
    position: absolute;
    top: 8px;
    width: 29px;
                     * */
                }

                else
                {
                    serviceSmile.Attributes.Add("style", "display:none;");
                }

            } 
   
            else
                serviceSmile.Attributes.Add("style", "display:none;");

            HtmlGenericControl serviceError = new HtmlGenericControl("div");
            serviceError.ID = "div_LocationError" + arrayWhoStay[i];
            serviceError.Attributes["class"] = "inputError";
            serviceError.Attributes.Add("style", "display:none;");

            HtmlGenericControl serviceComment = new HtmlGenericControl("div");
            serviceComment.ID = "div_LocationComment" + arrayWhoStay[i];
            serviceComment.Attributes["class"] = "inputComment";
            serviceComment.Attributes.Add("style", "display:none;");
            serviceComment.InnerText = LocationPlaceHplder;


            HtmlGenericControl chatBubbleArrowBorder = new HtmlGenericControl("div");
            chatBubbleArrowBorder.ID = "chatBubbleArrowBorder" + arrayWhoStay[i];
            chatBubbleArrowBorder.Attributes["class"] = "chat-bubble-arrow-border";

            serviceComment.Controls.Add(chatBubbleArrowBorder);

            HtmlGenericControl chatBubbleArrow = new HtmlGenericControl("div");
            chatBubbleArrow.ID = "chatBubbleArrow" + arrayWhoStay[i];
            chatBubbleArrow.Attributes["class"] = "chat-bubble-arrow";

            serviceComment.Controls.Add(chatBubbleArrow);

            inputCategoryContainer.Controls.Add(tb);            
            inputCategoryContainer.Controls.Add(serviceSmile);
            inputCategoryContainer.Controls.Add(serviceError);
            inputCategoryContainer.Controls.Add(serviceComment);

            DropDownList ddlCoverArea=new DropDownList();


            ddlCoverArea.ID= "ddlCoverArea" + arrayWhoStay[i];
            
            ddlCoverArea.Attributes.Add("class", "coverArea");

            ListItem listItemCoverArea = new ListItem();
            string hiddenCoverArea="";           

            


            try
            {

                
                if (listLocatinData[i].namedRegions != null)
                {
                    for (int indexNamedRegions = 0; indexNamedRegions < listLocatinData[i].namedRegions.Length; indexNamedRegions++)
                    {
                        listItemCoverArea = new ListItem();
                        listItemCoverArea.Value = listLocatinData[i].namedRegions[indexNamedRegions].RegionId + "=" + listLocatinData[i].namedRegions[indexNamedRegions].Level;
                        listItemCoverArea.Text = listLocatinData[i].namedRegions[indexNamedRegions].Name;
                        ddlCoverArea.Items.Add(listItemCoverArea);
                        hiddenCoverArea += listItemCoverArea.Value + "$";
                    }

                }
            }

            catch(Exception Ex)
            {
                // after post back there isn't alreay details
                // there isn't 
                ddlCoverArea.Attributes.Add("disabled", "disabled");
            }


            

            listItemCoverArea = new ListItem();

            listItemCoverArea.Value = "0.5";
            listItemCoverArea.Text = "0.5mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "1";
            listItemCoverArea.Text = "1mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "2";
            listItemCoverArea.Text = "2mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "3";
            listItemCoverArea.Text = "3mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "5";
            listItemCoverArea.Text = "5mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "10";
            listItemCoverArea.Text = "10mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "12.5";
            listItemCoverArea.Text = "12.5mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "20";
            listItemCoverArea.Text = "20mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "30";
            listItemCoverArea.Text = "30mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";
            
            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "50";
            listItemCoverArea.Text = "50mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "75";
            listItemCoverArea.Text = "75mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "100";
            listItemCoverArea.Text = "100mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "125";
            listItemCoverArea.Text = "125mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "150";
            listItemCoverArea.Text = "150mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "175";
            listItemCoverArea.Text = "175mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            try
            {
                ListItem li = ddlCoverArea.Items.FindByValue(listLocatinData[i].SelectedAreaValue);
                if (li != null)
                    li.Selected = true;
            }
            catch (Exception Ex)
            {
                // after post back there isn't alreay details
                // there isn't 
            }

            inputCategoryContainer.Controls.Add(ddlCoverArea);
           

            Label lblZipCode=new Label();
            lblZipCode.ID="lblZipCode" + arrayWhoStay[i];
            lblZipCode.CssClass="numberZipCodes";

            MapService mapService = new MapService();
            try
            {
                string numberOfZipCodes = mapService.getNumberOfZipCode(listLocatinData[i].SelectedArea, listLocatinData[i].Lat, listLocatinData[i].Lng, listLocatinData[i].Radius);
                
                    if(numberOfZipCodes!="failed")
                        lblZipCode.Text=numberOfZipCodes;
            }

            catch(Exception ex)
            {

            }
            



            /*
             [WebMethod]
                public Result<int> GetNumberOfZipcodes(Guid regionId, decimal latitude, decimal longitude, decimal radius)

                if regionId != Guid.Empty 
                then 
                I use regionId 
                else 
                I use lat/long/radius.
                When the area is “nationwide” put “41,751” in zipcodes.
            */



            inputCategoryContainer.Controls.Add(lblZipCode);
                

            if (i > 0)
            {
                /*
                    <a href=""><i class="fa fa-times"></i> fa-times</a>
                */

                LinkButton lb = new LinkButton();
                lb.ID = "minus" + arrayWhoStay[i];
                lb.Command += new CommandEventHandler(lb_click_minus);
                lb.CommandArgument = arrayWhoStay[i].ToString();
                lb.Text = "<i class='fa fa-times fa-2'></i>";                
                lb.OnClientClick = "CloneFromDdlAreas();";
                inputCategoryContainer.Controls.Add(lb);
                ScriptManager1.RegisterAsyncPostBackControl(lb);
            }           

            HtmlGenericControl divClear = new HtmlGenericControl("div");
            divClear.ID = "clear" + arrayWhoStay[i];
            divClear.Attributes["class"] = "clear";          


            inputCategoryContainer.Controls.Add(divClear);


            HiddenField hiddenField = new HiddenField();
            hiddenField.ID = "hf_areas" + arrayWhoStay[i];
           
        
            inputCategoryContainer.Controls.Add(hiddenField);

            HiddenField hiddenFieldLat = new HiddenField();
            hiddenFieldLat.ID = "hf_lat" + arrayWhoStay[i];

            if (!isPostback)
            {
                if (listLocatinData.Count() > 0)
                {
                    hiddenFieldLat.Value = listLocatinData[i].Lat.ToString();
                }

            }    
            inputCategoryContainer.Controls.Add(hiddenFieldLat);

            HiddenField hiddenFieldLng = new HiddenField();
            hiddenFieldLng.ID = "hf_lng" + arrayWhoStay[i];

            if (!isPostback)
            {
                if (listLocatinData.Count() > 0)
                {
                    hiddenFieldLng.Value = listLocatinData[i].Lng.ToString();
                }

            }

            inputCategoryContainer.Controls.Add(hiddenFieldLng);

            pnlAddLocation.Controls.Add(inputCategoryContainer);

            try
            {
                SetFullAddress(listLocatinData[i].FullAddress, (i + 1));
            }

            catch
            {
                // add new will fell here cause there isn't class locationData for it
            }


            if (listLocatinData.Count!=0 &&  i == 0)
                SetMap(listLocatinData[i].Radius.ToString(), listLocatinData[i].Lat.ToString(), listLocatinData[i].Lng.ToString(), 1);

        }

        


    }

    

    protected void lbAddButton_Click(object sender, EventArgs e)
    {
        

        initializeMultiMap();
        
        updatePanel1.Update();      
      
    }

    protected void lb_click_minus(object sender, CommandEventArgs e)
    {
        /************  remove the specific control *************/
        Control myControl1 = pnlAddLocation.FindControl("inputCategoryContainer" + e.CommandArgument);
        pnlAddLocation.Controls.Remove(myControl1);

        Control myControlClear1 = pnlAddLocation.FindControl("clear" + e.CommandArgument);
        pnlAddLocation.Controls.Remove(myControlClear1);


        index--;

        arrayWhoStay.Remove(Convert.ToInt32(e.CommandArgument));

        /************  remove the last control added in page load *************/
        pnlAddLocation.Controls.RemoveAt(pnlAddLocation.Controls.Count - 1);
        index--;

        arrayWhoStay.RemoveAt(arrayWhoStay.Count - 1);

        foreach (Control cn in pnlAddLocation.Controls)
        {

        }


        initializeMultiMap();

        this.DataBind();
        updatePanel1.Update();

    }

    private void initializeMultiMap()
    {
        string strArrayWhoSta = "";
        for (int i = 0; i < index; i++)
        {
            if (i != index - 1)
                strArrayWhoSta += arrayWhoStay[i] + ",";
            else
                strArrayWhoSta += arrayWhoStay[i].ToString();
        }

        string _script = "initializeMulti(" + strArrayWhoSta + ");";
        ScriptManager.RegisterStartupScript(Page, this.GetType(), "initializeMulti", _script, true);
    }    


    private void bindMapToTextBox()
    {
        for (int i = 0; i < index; i++)
        {
            string _script = "initializeMap(" + arrayWhoStay[i] + ");";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "initializeMap" + arrayWhoStay[i], _script, true);
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "bindMapToTextBox" + arrayWhoStay[i], _script, true);
        }
    }


    public int GetMaxValue(ArrayList arrList)
    {
        ArrayList copyList = new ArrayList(arrList);

        if (copyList == null || copyList.Count == 0)
        {
            return 0;
        }

        else
        {
            copyList.Sort();

            copyList.Reverse();

            return Convert.ToInt32(copyList[0]);
        }

    }

    public Guid Region_ID
    {
        get { return _RegionId; }
    }
    public bool Selected_All
    {
        get { return _selectAll; }
    }

    protected string GetLargeAreas
    {
        get { return ResolveUrl("~") + "PPC/MapService.asmx/GetLargeAreas2"; }
    }

    protected string GetNumberZipCodes
    {
        get { return ResolveUrl("~") + "PPC/MapService.asmx/getNumberOfZipCode"; }
    }

    protected string SetDetails
    {
        get { return ResolveUrl("~") + "PPC/MapService.asmx/SetDetails2"; }
    }

    

    public string LocationPlaceHplder
    {
        //       get { return "Please enter a location:"; }
        get { return (string)ViewState["LocationPlaceHplder"]; }
        set { ViewState["LocationPlaceHplder"] = value; }
    }
    public string SelectFromList
    {
        //       get { return "Select location from the list"; }
        get { return (string)ViewState["SelectFromList"]; }
        set { ViewState["SelectFromList"] = value; }
    }

    public string FieldMissing
    {
        get { return (string)ViewState["FieldMissing"]; }
        set { ViewState["FieldMissing"] = value; }
    }
    /*
    public string ChooseLargerArea
    {
        get { return (string)ViewState["ChooseLargerArea"]; }
        set { ViewState["ChooseLargerArea"] = value; }
    }
     * */
    public string ServerError
    {
        get { return (string)ViewState["ServerError"]; }
        set { ViewState["ServerError"] = value; }
    }


    protected int index
    {
        get
        {
            return (ViewState["index"] == null ? 0 : (int)ViewState["index"]);
        }
        set
        {

            ViewState["index"] = value;
        }
    }


    protected ArrayList arrayWhoStay
    {
        get
        {
            return (ViewState["arrayWhoStay"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoStay"]);
        }
        set
        {

            ViewState["arrayWhoStay"] = value;
        }
    }

    protected ArrayList arrayWhoInInit
    {
        get
        {
            return (ViewState["arrayWhoInInit"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoInInit"]);
        }
        set
        {

            ViewState["arrayWhoInInit"] = value;
        }
    }


    bool IsFirstTime
    {
        get { return (ViewState["IsFirstTime"] == null) ? false : (bool)ViewState["IsFirstTime"]; }
        set { ViewState["IsFirstTime"] = value; }
    }
    LocationData LocationDataV
    {
        get { return (ViewState["LocationData"] == null) ? null : (LocationData)ViewState["LocationData"]; }
        set { ViewState["LocationData"] = value; }
    }

    public string getArrayWhoStay(int i)
    {
        return "werwer";
    }


    List<LocationData> listLocatinData
    {
        get { return (ViewState["listLocatinData"] == null) ? null : (List<LocationData>)ViewState["listLocatinData"]; }
        set { ViewState["listLocatinData"] = value; }
    }

    public Guid SupplierId
    {

        get { return (ViewState["SupplierId"]== null) ? Guid.Empty : (Guid)ViewState["SupplierId"]; }
        set { ViewState["SupplierId"] = value; }
    }

    public class SendEventArgs : EventArgs
    {
        public string StrSend { get; set; }

        public SendEventArgs(string StrSend)
        {
            this.StrSend = StrSend;
        }

    }
}