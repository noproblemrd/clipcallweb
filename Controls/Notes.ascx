﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Notes.ascx.cs" Inherits="Publisher_Notes" %>
<script type="text/javascript" >
    var HasUpload = false;
    
    function ShowInsert()
    {
        var elm = document.getElementById('<%# div_show.ClientID %>');
        if(elm.style.display=='none')
        {
            elm.style.display='inline';
            var elmlink=document.getElementById('<%# lblShowInsert.ClientID %>');
            
            elmlink.innerHTML=document.getElementById('<%# Hidden_lblHide.ClientID %>').value;
            document.getElementById("<%# img_Insert.ClientID %>").src = "images/icon-remove.png";            
        }
        else
        {
            HideInsert();
        }
    }
    function HideInsert()
    {
        var elm = document.getElementById('<%# div_show.ClientID %>');
        elm.style.display='none';
        var elmlink=document.getElementById('<%# lblShowInsert.ClientID %>');
        elmlink.innerHTML=document.getElementById('<%# Hidden_lblShow.ClientID %>').value;
        
        document.getElementById("<%# txtInsertComment.ClientID %>").value = "";
        document.getElementById("<%# lbl_file_name.ClientID %>").innerHTML = "";
        document.getElementById("<%# span_upload.ClientID %>").style.display = "inline";
        document.getElementById("<%# f_name.ClientID %>").value = "";
        document.getElementById("<%# img_Insert.ClientID %>").src = "<%# GetIconAdd %>";
        ClearValidations();
        if(HasUpload)
            RemoveUploadFile();
    }

    function PassFile()
    {
        ClearValidations();
        
        var _file = document.getElementById("<%# f_name.ClientID %>");
        if(_file.value.length==0)
        {
            alert(document.getElementById("<%# lbl_NoFile.ClientID %>").value);
            HasUpload = false;
            return false;
        }
        
        HasUpload = true;       
        top.showDiv();
        
        return true;      
    }

    
    function RemoveUploadFile()
    {
        var _file = document.getElementById("<%# f_name.ClientID %>");
        
        document.getElementById("<%# span_upload.ClientID %>").style.display = "inline";
        document.getElementById("<%# btn_RemoveFile.ClientID %>").style.display = "none";        
        document.getElementById("<%# lbl_file_name.ClientID %>").innerHTML = "";
        RemoveFileUpload();
        return (_file.value.length != 0);
    }
    function ChkFile()
    {
        ClearValidations();
        var _comment = document.getElementById("<%# txtInsertComment.ClientID %>").value;
        if(_comment.length == 0)
        {
            document.getElementById("<%# lbl_MissingComment.ClientID %>").style.display = "inline";
            return false;
        }
        var _file = document.getElementById("<%# f_name.ClientID %>");
        if(!HasUpload && _file.value.length > 0)
        {
            alert(document.getElementById("<%# hf_UploadFile.ClientID %>").value);
            return false;
        }        
        return true;
    }
    
    function OpenFile(_location)
    {
        document.getElementById("<%# hf_fileName.ClientID %>").value = _location;
        <%# Get_tab_click() %>;
    }
    function ClearValidations()
    {
        document.getElementById("<%# lbl_MissingComment.ClientID %>").style.display = "none";
    }
    function UploadedFile(){
        HasUpload = true;
    }

    /*****  ICallbackEventHandler  ****/
    function RemoveFileUpload() {
        HasUpload = false;
        CallServer("", "ee");
    }
    function ReciveServerData(retValue) {
    }
     /*****  ICallbackEventHandler  ****/
    
    </script>
<div id="main2" style="overflow:hidden;" >
    
     
    <div id="formnotes" style="overflow-y:auto; max-height:450px;">
    <table  class="main" >
    <tr>
    
    <td>
    <H3>
        <a href="javascript:void(0);" onclick="ShowInsert();" >
            <asp:Image ID="img_Insert" runat="server" ImageUrl="~/images/icon-add.png" />
            <asp:Label ID="lblShowInsert" runat="server" Text='<%# Hidden_lblShow.Value %>' Font-Bold="True" ></asp:Label>
        </a>
    </H3>
    </td>
    </tr>
    
    <tr id="InsertComment" >
        <td class="regular2">

            <div id="div_show" style="display:none;" runat="server">

                <asp:Label ID="lblInsertComment" runat="server" Text="Insert Comment"></asp:Label>:
                <br />

                <asp:TextBox ID="txtInsertComment" runat="server"  TextMode="MultiLine" 
                    Rows="3" Width="90%" CssClass="notesmargin"></asp:TextBox>
                <br />

                <div class="clear"><!-- --></div>

                <div class="errornotes">  
                        <asp:Label ID="lbl_MissingComment" runat="server" Text="Missing" CssClass="error-msg"
                         style="display:none;"></asp:Label>
                </div>  
           
                <div class="clear"><!-- --></div>

                <div class="noteswrap">
                    
                    <div class="notesupload">            
                        <asp:Label ID="lbl_FileUpload" runat="server" Text="Upload file"></asp:Label>

                        <br />
                        <div runat="server"  id="span_upload" class="notes_controls" >
                                
                                <asp:UpdatePanel ID="_UpdatePanel_fileUpload" runat="server" UpdateMode="Conditional" style="float:left;">
                                    <ContentTemplate>            
                                        <input type="file" class="notebtn" id="f_name" runat="server" />        
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                
                                
                                <asp:Button ID="btn_upload" CssClass="browse_comp" style="clear:none;" runat="server" Text="Upload" 
                                        OnClientClick="return PassFile();" onclick="btn_upload_Click"  />
                         </div>

                         <asp:Label ID="lbl_file_name" runat="server" ForeColor="Red"></asp:Label>
        
          
                         <asp:Button ID="btn_RemoveFile" runat="server" Text="Remove" OnClick="Reset_click" OnClientClick="return RemoveUploadFile();"  CssClass="form-submit" style="display:none;"/>

                         <asp:Button ID="btnInsert" runat="server" CssClass="form-submit" OnClientClick="return ChkFile();"
                            Text="Insert" OnClick="btnInsert_Click" ValidationGroup="note" />
                    </div>      
                           
                </div>   
               

                <div class="clear"><!-- --></div>

            </div>

        </td>        
    </tr>
        
    <tr>
        
    <td class="regular2">
    
    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    
        <asp:DataList ID="dlComments" runat="server" Width="100%">
         <ItemStyle CssClass="regular" />
        <ItemTemplate>
           <div class="notetit"> 
             <asp:Label ID="lblDate" runat="server" Text='<%# Bind("CreatedOn") %>'></asp:Label>
                <asp:Label ID="lblTime" runat="server" Text='<%# Bind("CreatedOnTime") %>'></asp:Label>
                <asp:Label ID="lblCreateBy" runat="server" Text="<%# lbl_by.Text %>"></asp:Label>
                <asp:Label ID="lblBy" runat="server" Text='<%# Bind("CreatedBy") %>'></asp:Label>
                <div class="attachment"><a id="a_attach" runat="server" href='<%# Bind("File") %>' visible='<%# Bind("HasFile") %>'>
                    <asp:Image ID="img_attach" runat="server" ImageUrl="~/images/attachment1.png" /></a></div>
                </div>
            <br />
            
            <asp:Label ID="lblComment" runat="server" Text='<%# Bind("Text") %>'></asp:Label>           
            
            
        </ItemTemplate>
        </asp:DataList>
            <asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">     
                <ul>
                    <li class="first"><asp:LinkButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click">Previous page</asp:LinkButton></li>
                    <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
                    <li class="last"><asp:LinkButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click">Next page</asp:LinkButton></li>
                </ul>
            </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>
    </td>
    </tr>
    </table>
   
   
    </div>
   
		
        <asp:HiddenField ID="Hidden_lblTitleComment" runat="server" Value="The comment was created in - " />
        <asp:HiddenField ID="Hidden_lblCreateBy" runat="server" Value="By: " />
        <asp:HiddenField ID="Hidden_lblShow" runat="server" Value="Open comment" />
        <asp:HiddenField ID="Hidden_lblHide" runat="server" Value="Close comment" />
        
        <asp:HiddenField ID="lbl_NoFile" runat="server" Value="No file was selected" />
        <asp:HiddenField ID="hf_UploadFile" runat="server" Value="upload the file or remove it" />
        
        <asp:HiddenField ID="hf_fileName" runat="server" />
   <asp:Label ID="lbl_by" runat="server" Text="by" Visible="false"></asp:Label>
    <asp:Label ID="lbl_InvalidFile" runat="server" Text="You cannot upload exe file" Visible="false"></asp:Label>
</div>

