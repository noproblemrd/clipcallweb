﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class Publisher_Notes : System.Web.UI.UserControl, IPostBackEventHandler, ICallbackEventHandler
{
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    private const string OPEN_FILE_CLICK = "OpenFile";
    private const string FILE_NAME = "Request";

    public event System.EventHandler SetNoteTab;
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        sm.RegisterAsyncPostBackControl(btn_RemoveFile);
        if (!IsPostBack)
        {
            LoadTranslate();
            this.DataBind();
        }
        else
        {
            Dictionary<int, string> dic = PageListV;
            foreach (KeyValuePair<int, string> kvp in dic)
                PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
        }
        SetCallBackEvent();
        
    }

    private void LoadTranslate()
    {
        DBConnection.LoadTranslateToControl(this, "Notes.ascx", ((PageSetting)Page).siteSetting.siteLangId);
    }
    public void LoadData(Guid TicketId, WebReferenceSite.NoteType note_type)
    {            
     
        TicketIdV = TicketId;
        NoteTypeV = note_type;
        LoadData();
    }
    private void LoadData()
    {
        PageSetting _page = (PageSetting)Page;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_page);

        WebReferenceSite.ResultOfListOfNoteData result = null;
        try
        {
            result = _site.GetNotes(TicketIdV, NoteTypeV);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, _page.siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        DataTable data = new DataTable();
        data.Columns.Add("CreatedOn");
        data.Columns.Add("CreatedOnTime");
        data.Columns.Add("CreatedBy");
        data.Columns.Add("File");
        data.Columns.Add("HasFile", typeof(bool));
        data.Columns.Add("Text");

        foreach (WebReferenceSite.NoteData hnd in result.Value)
        {

            string CreatedBy = hnd.CreatedByName;//.CreatedById.ToString();
            string Text = hnd.Description;
            string CreatedOn = string.Format(_page.siteSetting.DateFormat, hnd.CreatedOn);
            string FilePath = hnd.File;
            DataRow row = data.NewRow();

            row["CreatedBy"] = CreatedBy;
            row["Text"] = Text;
            row["CreatedOn"] = CreatedOn;
            row["CreatedOnTime"] = string.Format(_page.siteSetting.TimeFormat, hnd.CreatedOn);

            if (!string.IsNullOrEmpty(FilePath))
            {
                row["HasFile"] = true;
                row["File"] = @"javascript:OpenFile('" + FilePath + "')";
            }
            else
                row["HasFile"] = false;
            data.Rows.Add(row);
        }
        BindData(data);
        dataV = data;
    }
    protected void OpenFile()
    {
        string file_name = hf_fileName.Value;
        string[] _names = file_name.Split('.');
        string extention = string.Empty;
        if (_names.Length > 1)
            extention = "." + _names[1];
        string path = System.Configuration.ConfigurationManager.AppSettings["NotesFiles"] +
            file_name;
        FileInfo fi = new FileInfo(path);
        string _ContentType = Utilities.GetMimeType(fi);
        Page.Response.ContentType = _ContentType;
        Page.Response.AppendHeader("Content-disposition", "attachment; filename=notes" + extention);

        Page.Response.BinaryWrite(File.ReadAllBytes(path));
        Page.Response.End();
    }
    protected void btn_upload_Click(object sender, EventArgs e)
    {
        if (this.SetNoteTab != null)
            this.SetNoteTab(this, EventArgs.Empty);
        div_show.Style[HtmlTextWriterStyle.Display] = "block";
        img_Insert.ImageUrl = ResolveUrl("~") + "images/icon-remove.png";
        string name = f_name.PostedFile.FileName;
        if (string.IsNullOrEmpty(name))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "noFile", "alert('" + lbl_NoFile.Value + "');", true);
            return;
        }
        string extension = System.IO.Path.GetExtension(name);
        if (extension == ".exe" || extension == ".com" || extension == ".bat")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "InvalidFile", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_InvalidFile.Text) + "');", true);
            return;
        }
     //   string localPath = System.Configuration.ConfigurationManager.AppSettings["NotesFiles"];
        string localPath = System.Configuration.ConfigurationManager.AppSettings["NotesFiles"];
        string FileName = string.Empty;
        do
        {
            FileName = Utilities.RandomString(10);
        } while (File.Exists(localPath + FileName + extension));
        string _path = localPath + FileName + extension;

        f_name.PostedFile.SaveAs(_path);
        FileNameV = FileName + extension;
        lbl_file_name.Text = FileName + extension;
        span_upload.Attributes.Add("style", "display:none");
        btn_RemoveFile.Attributes.Add("style", "display:inline");
        lblShowInsert.Text = Hidden_lblHide.Value;
  //      ScriptManager.RegisterStartupScript(this, this.GetType(), "UploadedFile", "UploadedFile();", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDiv", "UploadedFile(); top.hideDiv();", true);
    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {        
        if (this.SetNoteTab != null)
            this.SetNoteTab(this, EventArgs.Empty);
        PageSetting _page = (PageSetting)Page;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_page);
        WebReferenceSite.NoteData hnd = new WebReferenceSite.NoteData();
        if (!string.IsNullOrEmpty(FileNameV))
            hnd.File = FileNameV;
        hnd.CreatedById = new Guid(_page.userManagement.GetGuid);
        hnd.Description = txtInsertComment.Text;
        hnd.RegargingObjectId = TicketIdV;
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.CreateNote(hnd, NoteTypeV);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, _page.siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        _page.ClientScript.RegisterStartupScript(this.GetType(), "HideInsert", "HideInsert();", true);
        txtInsertComment.Text = string.Empty;
        FileNameV = null;
        lbl_file_name.Text = string.Empty;
        span_upload.Attributes.Add("style", "display:inline");
        div_show.Style[HtmlTextWriterStyle.Display] = "none";
        btn_RemoveFile.Style.Add(HtmlTextWriterStyle.Display, "none");
        img_Insert.ImageUrl = GetIconAdd;
        LoadData();

    }
    protected string GetIconAdd
    {
        get { return ResolveUrl("~") + "images/icon-add.png"; }
    }
    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        if (eventArgument == OPEN_FILE_CLICK)
            OpenFile();
    }
    public string Get_tab_click()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(this, OPEN_FILE_CLICK);
        myPostBackOptions.PerformValidation = false;
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);

    }

    #endregion
    #region table
    protected void BindData(DataTable data)
    {
        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;
        objPDS.PageSize = ITEM_PAGE;
        objPDS.DataSource = data.DefaultView;

        if (data.Rows.Count > 0)
        {

            objPDS.CurrentPageIndex = CurrentPage;
            lnkPreviousPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkNextPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            LoadPages(data.Rows.Count);
            dlComments.DataSource = objPDS;
            dlComments.DataBind();

            div_paging.Visible = true;
        }
        else
        {
            div_paging.Visible = false;
            dlComments.DataSource = null;
            dlComments.DataBind();
        }
        _UpdatePanel.Update();
    }
    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        //if there is only one page
        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListV = null;
            return;
        }
        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListV = dic;
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
    }
    LinkButton MakeCell(int indx, string _item)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }

        return lb;
    }

    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
        BindData(dataV);
    }
    protected void lnkNextPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;

        BindData(dataV);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;

        BindData(dataV);
    }
    int CurrentPage
    {
        get
        {
            //Look for current page in ViewState 
            object o = this.ViewState["CurrentPage"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["CurrentPage"] = value;
        }
    }
    DataTable dataV
    {
        get
        {
            return (ViewState["data"] == null) ? new DataTable() :
              (DataTable)ViewState["data"];
        }
        set { ViewState["data"] = value; }
    }
    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }
    string FileNameV
    {
        get { return (Session["FileName"] == null) ? string.Empty : (string)Session["FileName"]; }
        set { Session["FileName"] = value; }
    }
    #endregion
    Guid TicketIdV
    {
        get { return (ViewState["TicketId"] == null) ? Guid.Empty : (Guid)ViewState["TicketId"]; }
        set { ViewState["TicketId"] = value; }
    }
    WebReferenceSite.NoteType NoteTypeV
    {
        get { return (ViewState["NoteType"] == null) ? WebReferenceSite.NoteType.HelpDesk : (WebReferenceSite.NoteType)ViewState["NoteType"]; }
        set { ViewState["NoteType"] = value; }
    }
    protected void Reset_click(object sender, EventArgs e)
    {
        _UpdatePanel_fileUpload.Update();
    }
    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        return string.Empty;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        string file_name = FileNameV;
        string localPath = System.Configuration.ConfigurationManager.AppSettings["NotesFiles"];

        string _path = localPath + file_name;
        if (File.Exists(_path))
        {
            try
            {
                File.Delete(_path);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, ((PageSetting)Page).siteSetting);
            }
        }
        FileNameV = string.Empty;
    }
    protected void SetCallBackEvent()
    {
        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);
    }

    #endregion
}
