﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class Controls_MasterPageNP2 : System.Web.UI.MasterPage
{
    //  const string SITE_ID = "Sales";
    //  PageSetting ps;
    /*
    protected override void OnInit(EventArgs e)
    {
        if (Page.GetType() != typeof(PageSetting))
            Response.Redirect(ResolveUrl("~") + "index.aspx");
        ps = (PageSetting)Page;
        if (!ps.userManagement.IsSupplier() || ps.siteSetting.GetSiteID==SITE_ID)            
            Response.Redirect(ResolveUrl("~") + "Management/LogOut.aspx");
        
        base.OnInit(e);
    }
     * */
   

    protected override void OnInit(EventArgs e)
    {
        if (Page is PageSetting)
        {
            PageSetting ps = (PageSetting)Page;
            ps.ClearExceptSiteSetting();
            if (string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, ps))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");

                if (Request.Cookies["language"] != null)
                {
                    int SiteLangId = int.Parse(Request.Cookies["language"].Value);
                    if (DBConnection.IfSiteLangIdExists(SiteLangId, ps.siteSetting.GetSiteID))
                        ps.siteSetting.siteLangId = SiteLangId;
                }
            }
            if (ps.siteSetting.GetSiteID != PpcSite.GetCurrent().SiteId)
                Response.Redirect(ResolveUrl("~") + "Management/ProfessionLogin.aspx");
        }
        /*
        base.OnInit(e);
        ps = (PageSetting)Page;
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, ps))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            }
            if (ps.siteSetting.GetSiteID != PpcSite.GetCurrent().SiteId)
                Response.Redirect(ResolveUrl("~") + "Management/ProfessionLogin.aspx");
        }
        
        */
    }

    public void hideNameAndLogout()
    {
        
        lbl_name.Visible = false;
        linkLogout.Visible = false;
       
    }

   
    private string getFacebookAppId()
    {
        string appId = "";
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        appId = xdoc.Element("Sites").Element("SitePPC").Element("FacebookAppId").Value;

        return appId;
    }

    public void SetStatusBar(string choosePlan, string businessDetails, string checkout)
    {
        StatusBar1.setStatuses(choosePlan, businessDetails, checkout);
        ////Status_Bar.Visible = true;
        ////Status_Bar.Attributes["class"] = ClassName;
    }

    public void SetContent(string paddingTop)
    {
        content.Attributes.Add("style", "padding-top:37px;");
    }

    public void SetContentPaddingBottom(string padding)
    {
        content.Attributes.Add("style", "padding-bottom:" + padding + ";");
    }

    public void SetContentMinHeight(string minHeight)
    {
        content.Attributes.Add("style", "min-height:" + minHeight + ";");
    }
   

    public void SetBlackMenu()
    {       
        blackMenu.Attributes.Add("style", "display:block;");
    }

    public void SetPlanType(string strPlanType)
    {
        planType.Attributes.Add("style", "display:block");

        if(strPlanType=="LEAD_BOOSTER_AND_FEATURED_LISTING")
            planType.InnerText = "Lead Booster plan";
        else
            planType.InnerText = "Featured Listing plan";
    }

    public void RemoveSiteMap()
    {
        _SiteMapPath.Visible = false;
    }

    public void SetServerComment(string comment)
    {
        //ClientScript.RegisterStartupScript(this.GetType(), "SetComment", @"SetServerComment(""" + _comment + @""");", true);
        _comment.SetServerComment(comment);
    }

    protected void formatLendingPage()
    {
        _body.Attributes["class"] = "landingPage";
        homeheader.Visible = false;        
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write("notInLoginAndRegister:" + notInLoginAndRegister);
        //linkLogout.HRef = ResolveUrl("~/PPC/login.aspx");

        string registerFormat;
        registerFormat = (Request["format"]==null) ? "" : Request["format"].ToString();

        bool landingPage;

        if (registerFormat.ToLower()=="landingpage")
            landingPage = true;
        else
             landingPage = false;

        if (landingPage)
        {
            formatLendingPage();
        }

        else
        {
            UserControl ctlFormalTopHeader = (UserControl)Page.LoadControl("~/Controls/FormalTopHeader.ascx");
            placeHolderFormalTopHeader.Controls.Add(ctlFormalTopHeader);

            UserControl ctlFormalFooter = (UserControl)Page.LoadControl("~/Controls/FormalFooter.ascx");
            placeHolderFormalFooter.Controls.Add(ctlFormalFooter); 
           
            
        }
        
        //<uc1:FormalTopHeader runat="server" ID="_FormalTopHeader" />
        facebookAppId = getFacebookAppId();
        _comment.myStyle = "display:none;";

        string urlPage = this.Request.Url.AbsolutePath;
        FileInfo fileInfo = new FileInfo(urlPage);
        //Response.Write(fileInfo.Name);

        if (fileInfo.Name.ToLower() == "register.aspx" || fileInfo.Name.ToLower() == "registera.aspx"  || fileInfo.Name.ToLower() == "registerb.aspx"
           || fileInfo.Name.ToLower() == "registerc.aspx" || fileInfo.Name.ToLower() == "registerd.aspx" || fileInfo.Name.ToLower() == "ppclogin.aspx")
        {  


        }
        else
        {
            /*
            var js = new HtmlGenericControl("script");
            js.Attributes["type"] = "text/javascript";
            js.Attributes["src"] = "../ppc/socialNetworks.js";
            Page.Header.Controls.Add(js);
            */
            
            Controls_socialNetworks2 socialNetworks = (Controls_socialNetworks2)Page.LoadControl("~/Controls/socialNetworks2.ascx");
            socialNetworks.facebookAppId = facebookAppId;
            placeHolderSocial.Controls.Add(socialNetworks);

           
        }


        if (this.Request.Url.ToString().ToLower().IndexOf("login") != -1 || (fileInfo.Name.ToLower().IndexOf("register")!=-1) ||
            fileInfo.Name.ToLower() == "forgotpassword.aspx" || fileInfo.Name.ToLower() == "changepassword.aspx")
        {            
            linkLogout.HRef = ResolveUrl("~/ppc/ppcLogin.aspx");
            linkLogout.InnerText = "Login";
            
        }

        else
        {
            if (fileInfo.Name.ToLower() == "paymentlegacy.aspx" || fileInfo.Name.ToLower() == "resendyouremailmobile.aspx" ||
                fileInfo.Name.ToLower() == "verifyyouremailmanually.aspx" || fileInfo.Name.ToLower() == "verifyyouremailmanually.aspx" ||
                    fileInfo.Name.ToLower() == "verifyyouremailmanuallycallback.aspx" )
            {
                linkLogout.HRef = ResolveUrl("~/ppc/logoutMobile.aspx");
            }            

            else
            {
                if (Session["Source2014"] != null)
                {

                    if (Session["Source2014"].ToString() == "facebook")
                    {
                        linkLogout.Attributes.Add("onclick", "return faceBookLogOut();");
                        linkLogout.HRef = "javascript:void(0);";
                    }

                    else if (Session["Source2014"].ToString() == "googlePlus")
                    {
                        linkLogout.Attributes.Add("onclick", "return signOutGooglePlus();");
                        linkLogout.HRef = "javascript:void(0);";
                    }

                    else if (Session["Source2014"].ToString() == "website")
                    {
                        linkLogout.HRef = ResolveUrl("~/ppc/logout.aspx");
                    }

                }
            }
        }

        if (fileInfo.Name.ToLower() == "verifyyouremailmanually.aspx")
        {
            subContent2.Attributes.Add("style", "width:890px;");
            _comment.myStyle = "margin-top:20px;display:none;";
        }

        if (fileInfo.Name.ToLower() == "ppcloginmobile.aspx")
        {
            _comment.myStyle = "margin-top:-20px;display:none;";            
        }

        if (fileInfo.Name.ToLower() == "resendyouremail.aspx")
        {
            //subContent2.Attributes.Add("style", "width:752px;"); 
            _comment.myStyle = "width:700px;display:none;";
        }

        if (fileInfo.Name.ToLower() == "ppclogin.aspx" || fileInfo.Name.ToLower() == "ppcloginmobile.aspx" ||
             fileInfo.Name.ToLower() == "register.aspx" || fileInfo.Name.ToLower() == "forgotpassword.aspx" || fileInfo.Name.ToLower() == "changepassword.aspx")
        {            
            lbl_name.Text = "<a href='register.aspx'>Join NoProblem</a>";
            _comment.myStyle = "width:96%;display:none;";
        }

        else
            lbl_name.Text=(Session["Username2014"]==null)? "" : Session["Username2014"].ToString();
        
        Page.Header.DataBind();
        //       Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadDetails", "LoadDetails('" + GetLogOutMessage + "', '" + GetLogOutPath + "');", true);

    }

    public void RemoveRibbon()
    {
        div_seret.Visible = false;
        //div_benefits.Style[HtmlTextWriterStyle.MarginTop] = "28px";
        //      FormalFooter1.Visible = false;
        //       div_bottom.Visible = true;
    }


    public void setRibbon()
    {
        
        if (Session["flavorRegistration"] != null)
        {
            WebReferenceSupplier.eAdvertiserRegistrationLandingPageType eLandingType = (WebReferenceSupplier.eAdvertiserRegistrationLandingPageType)Session["flavorRegistration"];

            if (eLandingType != WebReferenceSupplier.eAdvertiserRegistrationLandingPageType.A)
            {
                div_seret.Visible = true;
                div_seret.Attributes.Add("style", "background-image:url('images/red-ribbon_bonus.png');right:-24px;top:-8px;");
            }
            else
            {
                div_seret.Visible = true;
                div_seret.Attributes.Add("style", "background-image:url('images/redStrip.png');right:-5px;top:-6px;");
            }
        }
        
    }

    public void setRibbon2(WebReferenceSupplier.eAdvertiserRegistrationLandingPageType eLandingType)
    {
        if (eLandingType != WebReferenceSupplier.eAdvertiserRegistrationLandingPageType.A)
        {
            div_seret.Visible = false;
            div_seret.Attributes.Add("style", "background-image:url('images/red-ribbon_bonus.png');right:-24px;top:-8px;");
        }
        else
        {
            div_seret.Visible = true;
            div_seret.Attributes.Add("style", "background-image:url('images/redStrip.png');right:-5px;top:-6px;");
        }
    }



    public void stepRedirect(int step)
    {
        /*
        1 = VerifyYourEmail
        2 = ChoosePlan
        3 = BusinessDetails_General
        4 = BusinessDetails_Category
        5 = BusinessDetails_Area
        6 = Checkout
        7 = Dashboard
        */
        ////if (Session["authorize"] == "yes")
        if (1==1)
        {
            switch (step)
            {
                case 1:
                    //Response.Redirect('ResendYourEmail.aspx?id=' + data.supplierId + "&email=" + data.email);
                    break;

                case 2:
                    Response.Redirect("choosePlan.aspx",true);
                    Response.End();
                    break;

                case 3:
                    Response.Redirect("businessMainDetails.aspx",true); 
                    Response.End();
                    break;

                case 4:
                    Response.Redirect("categories.aspx",true); 
                    Response.End();
                    break;

                case 5:
                    Response.Redirect("location2.aspx",true); 
                    Response.End();
                    break;

                case 6:
                    Response.Redirect("checkout.aspx",true); 
                    Response.End();
                    break;
                
                case 7:
                    Response.Redirect("ppcLogin.aspx", true); 
                    Response.End();
                    break;
                
            }
        }

        else
        {
            Response.Redirect("ppcLogin.aspx",true);
            Response.End();
        }

    }

    public void ServerProblem(string _mess)
    {
        string _script = @"_SetServerComment(""" + HttpUtility.HtmlEncode(_mess) + @""");";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerProblem", _script, true);
    }


    protected string WidgetScript
    {
        get
        {
            return ResolveUrl("~") + "Formal/Scripts/widget.js";
        }
    }
    protected string GetStyle
    {
        get { return ResolveUrl("~") + "PPC/samplepPpc2.css?version=4"; }
    }
    protected string GetHeaderStyle
    {
        get { return ResolveUrl("~") + "PPC/HeaderStyle.css"; }
    }

    public string GetLogOutMessage
    {
        get
        {
            return HttpUtility.HtmlEncode("Your session has expired. Please login again.");
        }
    }
    public string GetLogOutPath
    {
        get
        {
            return ResolveUrl("~") + "PPC/RequestInvitation.aspx";
        }
    }

    public string facebookAppId
    {
        get
        {
            return (ViewState["facebookAppId"]==null)?"":ViewState["facebookAppId"].ToString(); 
        }

        set
        {
            ViewState["facebookAppId"] = value;
        }
    }


    public bool notInLoginAndRegister
    {
        get;
        set;
    }

}
