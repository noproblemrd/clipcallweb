﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PpcComment2.ascx.cs" Inherits="Controls_PpcComment2" %>
<script type="text/javascript">
    var _serverComment = new Server_Comment('<%# MultipleComment %>', '<%# GeneralError %>');
    function Server_Comment(MultipleComment, GeneralError) {
        this.MultipleComment = MultipleComment;
        this.GeneralError = GeneralError;
    }
    function RemoveServerComment() {
        
        $('#div_ServerComment').removeClass('comment-show');
        $('#div_ServerComment').removeClass('comment-show-2line');
        
        $(".container_comment").hide(500);
        
        //$(".container_comment").css('display', 'none');

        
        setTimeout(function () {
            $("#lbl_ServerComment").html('');
            $('a.a_systemMessageClose').hide();
            
        }, 200);

    }

    function RemoveServerComment2() {

        $('#div_ServerComment').removeClass('comment-show');
        $('#div_ServerComment').removeClass('comment-show-2line');

        $(".container_comment").attr('style', '');

        $(".container_comment").hide(500);

        //$(".container_comment").css('display', 'none');


        setTimeout(function () {
            $("#lbl_ServerComment").html('');
            $('a.a_systemMessageClose').hide();
            //$(".container_comment").css('display', 'none');
        }, 200);

    }


    function SetServerComment(_comment) {        
        $("#lbl_ServerComment").html(_comment);
        ShowComment();
    }

    function SetServerComment2(_comment) {
        $("#lbl_ServerComment").html(_comment);
        ShowComment2();
    }

    function _SetServerComment(_comment) {
        SetServerComment(_comment);
        window.scrollTo(0, 0);
    }

    function GeneralServerError() {
        $("#lbl_ServerComment").html(_serverComment.GeneralError);
        ShowComment();
    }
    function _GeneralServerError() {
        GeneralServerError();
        window.scrollTo(0, 0);
    }
    function GeneralMultipleComment() {
        $("#lbl_ServerComment").html(_serverComment.MultipleComment);
        ShowComment();
    }
    function _GeneralMultipleComment() {
        GeneralMultipleComment();
        window.scrollTo(0, 0);
    }   

    function ShowComment() {
        $(".container_comment").css('display', 'block');
        $("#lbl_ServerComment").show(500);
        //$("#lbl_ServerComment").animate({ "height": "20px" }, 1000);
        $('#div_ServerComment').removeClass('comment-show-2line');
        $('#div_ServerComment').addClass('comment-show');
        $('a.a_systemMessageClose').show();

        
        setTimeout(function () {
            if ($("#lbl_ServerComment").get(0).offsetHeight > 20) {
                $('#div_ServerComment').removeClass('comment-show');
                $('#div_ServerComment').addClass('comment-show-2line');
            }
        }, 300);
        
    }
    

</script>
<div class="container_comment" style="<%#myStyle%>">
    <div class="comment" id="div_ServerComment" >
        
        <span id="lbl_ServerComment">
            <asp:Literal ID="lt_ServerComment" runat="server"></asp:Literal>
        </span>
        
        <a class="a_systemMessageClose" href="javascript:RemoveServerComment2();" style="display:none;"></a>
       
    </div>    
</div>
