﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Web.UI.HtmlControls;

public partial class Controls_MapControl : System.Web.UI.UserControl
{
    Guid _RegionId;
    bool _selectAll = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DataBind();

    }
    public void SetFullAddress(string _address)
    {

        string _script = "SetFullAddress('" + _address + "');";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "SetFullAddress", _script, true);
        
    }
    public void SetMap(string radius, string lat, string lng, bool IsMoreSelected, XElement _areas)
    {
        string _script = "SetMapOnLoad('" + radius + "', '" + lat + "', '" + lng + "');";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "SetMapOnLoad", _script, true);
        if (IsMoreSelected)
        {
            XElement xarea = _areas.Element("SelectedAreas");
            div_places.Style[HtmlTextWriterStyle.Display] = "block";
            foreach (XElement _xel in xarea.Elements("Area"))
            {
                string _level = _xel.Attribute("Level").Value;
                string RegionId = _xel.Attribute("RegionId").Value;
                HtmlAnchor _ha = new HtmlAnchor();
                bool __selectedall = false;
                switch (_level)
                {

                    case ("ALL"): _ha = a_Region0;
                        __selectedall = true;
                        break;
                    case ("1"): _ha = a_Region1;
                        goto default;
                    case ("2"): _ha = a_Region2;
                        goto default;
                    case ("3"): _ha = a_Region3;
                        goto default;
                    default:
                        _ha.InnerHtml = _xel.Attribute("Name").Value;
                        _ha.Attributes.Add("_id", RegionId);                        
                        break;
                }
                
                if (_xel.Attribute("Selected").Value == "true")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SetLargeArea", "_SetLargeArea(document.getElementById('" + _ha.ClientID + "'));", true);
                    Guid.TryParse(RegionId, out _RegionId);
                    _selectAll = __selectedall;
                    hf_SavedRegionId.Value = (__selectedall) ? "ALL" : _RegionId.ToString();
                }
            }
        }
        else
            div_places.Style[HtmlTextWriterStyle.Display] = "none";
    }
    public Guid Region_ID
    {
        get { return _RegionId; }
    }
    public bool Selected_All
    {
        get { return _selectAll; }
    }
    protected string GetLargeAreas
    {
        get { return ResolveUrl("~") + "PPC/MapService.asmx/GetLargeAreas"; }
    }
    public string LocationPlaceHplder
    {
 //       get { return "Please enter a location:"; }
        get { return (string)ViewState["LocationPlaceHplder"]; }
        set { ViewState["LocationPlaceHplder"] = value; }
    }
    public string SelectFromList
    {
 //       get { return "Select location from the list"; }
        get { return (string)ViewState["SelectFromList"]; }
        set { ViewState["SelectFromList"] = value; }
    }
   
    public string FieldMissing
    {
        get { return (string)ViewState["FieldMissing"]; }
        set { ViewState["FieldMissing"] = value; }
    }
    /*
    public string ChooseLargerArea
    {
        get { return (string)ViewState["ChooseLargerArea"]; }
        set { ViewState["ChooseLargerArea"] = value; }
    }
     * */
    public string ServerError
    {
        get { return (string)ViewState["ServerError"]; }
        set { ViewState["ServerError"] = value; }
    }

}