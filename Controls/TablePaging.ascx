﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TablePaging.ascx.cs" Inherits="TablePaging" %>

<asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">     
    <ul>
        <li class="first"><asp:LinkButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click">Previous page</asp:LinkButton></li>
        <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
        <li class="last"><asp:LinkButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click">Next page</asp:LinkButton></li>
    </ul>
</asp:Panel>
