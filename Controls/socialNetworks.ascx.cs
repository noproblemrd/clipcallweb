﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.IO;

public partial class Controls_socialNetworks : System.Web.UI.UserControl
{
    public string facebookAppId;
    protected string siteId;    
    protected PpcSite _cache;
    protected string registerReferrer="";
    protected string EmailDuplicate="";
    protected string WrongCombination = "";
    protected string comeFrom = "";
    protected string flavor = "";
    protected Guid hitId=Guid.Empty;
    public WebReferenceSupplier.eAdvertiserRegistrationLandingPageType LandingPage { get; set; }
    protected string registerError;
    protected bool flagDisableSocialCheck=false;


    protected void Page_Load(object sender, EventArgs e)
    {
        // faceBookAppId: 392131104255963 dev
        // faceBookAppId: 486879858091228 qa
        //_cache = PpcSite.GetCurrent();
        
        if (!IsPostBack)
        {
            //siteId = _cache.SiteId;
            registerReferrer = Request["registerReferrer"];
            comeFrom = Request["comeFrom"];
            flavor = Request["flavor"];
            registerError = Request["error"];

            

            SetHit();
            disableSocialCheck();
            LoadPageText();
        }
        

        facebookAppId = getFacebookAppId();        

        Page.DataBind();
    }

    private void disableSocialCheck()
    {
        FileInfo fileinfo = new FileInfo(Request.Url.AbsolutePath);   
        if (fileinfo.Name.ToLower()=="ppclogin.aspx")
        {
            if (registerError != null && registerError.Length > 0)
                flagDisableSocialCheck = true;
        }
        
    }

    private void SetHit()
    {

        FileInfo fileinfo = new FileInfo(Request.Url.AbsolutePath);        

        if (fileinfo.Name.ToLower().IndexOf("register")!=-1)
        {            
            if (Request.Cookies["npRegisterHit24"] == null && Request.Cookies["npRegisterAllredyProcess"] == null) // first time in page
            {
                try
                {
                    LandedOnJoinPage lojp = new LandedOnJoinPage(Utilities.GetIP(Request), comeFrom, Request.Browser.Browser, Request.Browser.Version, flavor, LandingPage);
                    hitId = lojp.InsertHit();

                    HttpCookie cookieHit24 = new HttpCookie("npRegisterHit24");
                    cookieHit24.Value = hitId.ToString();
                    cookieHit24.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookieHit24);
                }

                catch (Exception ex)
                {
                    dbug_log.ExceptionLog(ex);
                }
            }

            else if (Request.Cookies["npRegisterHit24"] == null && Request.Cookies["npRegisterAllredyProcess"] != null) // if he already succees in process registration enable next time new hit id 
            {
                try
                {
                    LandedOnJoinPage lojp = new LandedOnJoinPage(Utilities.GetIP(Request), comeFrom, Request.Browser.Browser, Request.Browser.Version, flavor, LandingPage);
                    hitId = lojp.InsertHit();

                    HttpCookie cookieHit24 = new HttpCookie("npRegisterHit24");
                    cookieHit24.Value = hitId.ToString();
                    cookieHit24.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookieHit24);
                }

                catch (Exception ex)
                {
                    dbug_log.ExceptionLog(ex);
                }
            }

            else if (Request.Cookies["npRegisterHit24"] != null && Request.Cookies["npRegisterAllredyProcess"] != null) // if he already succees in process registration
            {
                try
                {
                    Guid ConvertHitGuyid;
                    if (Guid.TryParse(Request.Cookies["npRegisterHit24"].Value, out ConvertHitGuyid))
                        hitId = ConvertHitGuyid;
                }

                catch (Exception ex)
                {
                    dbug_log.ExceptionLog(ex);
                }
            }

            else if (Request.Cookies["npRegisterHit24"] != null && Request.Cookies["npRegisterAllredyProcess"] == null)
            {
                // this scenario when surfer makes refresh in the register page or come back while the npRegisterHit24 exists
                // we use its saved hit in the cookie
                // if he new one the crm will insert its remembered hit
                // if he old one the crm sees it us update action and will not insert as hit
                Guid ConvertHitGuyid;
                if (Guid.TryParse(Request.Cookies["npRegisterHit24"].Value, out ConvertHitGuyid))
                    hitId = ConvertHitGuyid;

            }
        }
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();      

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "login"
                          select x).FirstOrDefault();

        EmailDuplicate = xelem.Element("Email").Element("Validated").Element("Instruction").Element("EmailDuplicate").Value;
        WrongCombination = xelem.Element("Email").Element("Validated").Element("Instruction").Element("WrongCombination").Value;
    }   


    private string getFacebookAppId()
    {
        string appId="";
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        appId = xdoc.Element("Sites").Element("SitePPC").Element("FacebookAppId").Value;

        return appId;
    }

    //EmailDuplicate


    protected string checkRegistrationStatusSocialNetworks
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/RegisterSupplierSocialNetworks"); }
    }

    protected string checkRegistrationStatusRegular
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/RegisterSupplierRegular"); }
    }
    
    protected string checkLoginStatusRegular
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/LoginSupplierRegular"); }
    }
    
}