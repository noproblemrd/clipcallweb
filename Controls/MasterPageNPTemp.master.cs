﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_MasterPageNPTemp : System.Web.UI.MasterPage
{
    //  const string SITE_ID = "Sales";
    //  PageSetting ps;
    /*
    protected override void OnInit(EventArgs e)
    {
        if (Page.GetType() != typeof(PageSetting))
            Response.Redirect(ResolveUrl("~") + "index.aspx");
        ps = (PageSetting)Page;
        if (!ps.userManagement.IsSupplier() || ps.siteSetting.GetSiteID==SITE_ID)            
            Response.Redirect(ResolveUrl("~") + "Management/LogOut.aspx");
        
        base.OnInit(e);
    }
     * */
    protected override void OnInit(EventArgs e)
    {
        if (Page is PageSetting)
        {
            PageSetting ps = (PageSetting)Page;
            ps.ClearExceptSiteSetting();
            if (string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, ps))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");

                if (Request.Cookies["language"] != null)
                {
                    int SiteLangId = int.Parse(Request.Cookies["language"].Value);
                    if (DBConnection.IfSiteLangIdExists(SiteLangId, ps.siteSetting.GetSiteID))
                        ps.siteSetting.siteLangId = SiteLangId;
                }
            }
            if (ps.siteSetting.GetSiteID != PpcSite.GetCurrent().SiteId)
                Response.Redirect(ResolveUrl("~") + "Management/ProfessionLogin.aspx");
        }
        /*
        base.OnInit(e);
        ps = (PageSetting)Page;
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, ps))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            }
            if (ps.siteSetting.GetSiteID != PpcSite.GetCurrent().SiteId)
                Response.Redirect(ResolveUrl("~") + "Management/ProfessionLogin.aspx");
        }
        
        */
    }
    public void SetStatusBar(string ClassName)
    {
        Status_Bar.Visible = true;
        Status_Bar.Attributes["class"] = ClassName;
    }


    public void removeSideBar()
    {
        sideBar.Visible = false;
        sideBarBottom.Visible = false;
    }

    public void RemoveSiteMap()
    {
        _SiteMapPath.Visible = false;
    }
    public void SetServerComment(string comment)
    {
        //ClientScript.RegisterStartupScript(this.GetType(), "SetComment", @"SetServerComment(""" + _comment + @""");", true);
        _comment.SetServerComment(comment);
    }
    public void SetBenefitFreeText(string txt)
    {
        lbl_benefits.Visible = false;
        lit_benefits.Visible = true;
        lit_benefits.Text = txt;
    }
    public void SetBenefits(string _benefit)
    {
        lbl_benefits.Text = _benefit;
    }
    public void BenefitsLinks(bool ToShow)
    {
        benefit_link.Visible = ToShow;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.DataBind();
        //       Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadDetails", "LoadDetails('" + GetLogOutMessage + "', '" + GetLogOutPath + "');", true);

    }
    public void RemoveRibbon()
    {
        div_seret.Visible = false;
        div_benefits.Style[HtmlTextWriterStyle.MarginTop] = "28px";
        //      FormalFooter1.Visible = false;
        //       div_bottom.Visible = true;
    }
    protected string WidgetScript
    {
        get
        {
            return ResolveUrl("~") + "Formal/Scripts/widget.js";
        }
    }
    protected string GetStyle
    {
        get { return ResolveUrl("~") + "PPC/samplepPpc.css"; }
    }
    protected string GetHeaderStyle
    {
        get { return ResolveUrl("~") + "PPC/HeaderStyle.css"; }
    }

    public string GetLogOutMessage
    {
        get
        {
            return HttpUtility.HtmlEncode("Your session has expired. Please login again.");
        }
    }
    public string GetLogOutPath
    {
        get
        {
            return ResolveUrl("~") + "PPC/RequestInvitation.aspx";
        }
    }

}
