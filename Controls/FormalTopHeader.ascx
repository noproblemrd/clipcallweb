﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormalTopHeader.ascx.cs" Inherits="Controls_FormalTopHeader" %>
<script type="text/javascript">
    $(function () {
        $('#BaseTopmenulinks').css('marginLeft', GetRigth() + 'px');
        $(window).resize(function () {
            $('#BaseTopmenulinks').css('marginLeft', GetRigth() + 'px');
        });
    });
    function GetRigth() {
        var _marginLeft = $('#topmenu').get(0).offsetWidth;
        _marginLeft = _marginLeft - 950;
        _marginLeft = ((_marginLeft < 0) ? _marginLeft : _marginLeft / 2) + 650;
        if (_marginLeft < 0)
            return 0;
        return _marginLeft;
    }
</script>
<div id="wrapperWizard" style="display:none;">
    <div id="wizard" class="wizard" > 
              
        <div class="title" id="wizardTitle" >
        
            <h1 id="wizardTitle1">Help your visitors.</h1>    
            <h1 id="wizardTitle2">Monetize your traffic.</h1>
            <!--
            <h1>Stay tuned!</h1>    
            <h1>Widget launches November 1st</h1>
            -->
        </div> 
        
        <div id="wizardText" class="text">
        
            Leave your email to be notified when the widget launches.
        
        </div>  
        
       
        <div id="wizardInput" class="wizardInput" >
        
            <div id="formWizard" >
                <input id="email" name="email" type="text" class="textActive inputWizard" onkeyup="validation('keyup');" onblur="validation('blur');" />            
            </div>
        
        </div> 

        <div id="wizardEmailText" class="sendTitle">
        
            Email address
        
        </div>
       
        <div id="wizardSubmit" class="send">
            <a href="javascript:void(0);" onclick="javascript:sendWizard();" class="submit"></a>
      <%--       <img src="<%= ResolveUrl("~") %>Formal/images/wizard/submit.png" onclick="javascript:sendWizard();" onmouseover="this.src=imgSubmitOver.src;" onmouseout="this.src=imgSubmitOut.src;" alt="Wizard"/>--%>
          
        </div>

        <div class="validation" id="validation" style="display:none;">
            <div id="validationIcon" class="validationIcon"><img src="<%= ResolveUrl("~") %>Formal/images/wizard/OrangeSmileyInstructor.png" alt="Smile" /></div>
            <div id="validationText" >Enter valid email address</div>
       </div>

    </div>
</div>

<div id="topmenu">  
    <div class="BaseTopmenulinks" id="BaseTopmenulinks">
    
        <div id="topmenulinks">
        <div id="toplinks">
        <asp:HyperLink ID="a_adv" runat="server" CssClass="toplink"  Text="Advertisers" NavigateUrl="http://www2.noproblemppc.com/adv.html"></asp:HyperLink>
        <asp:HyperLink ID="a_publisher" runat="server" CssClass="toplink"  Text="Affiliates" NavigateUrl="http://www2.noproblemppc.com/publisher.html"></asp:HyperLink>
        
        <asp:HyperLink ID="a_partners" runat="server" CssClass="toplink"  Text="Partners" NavigateUrl="http://www2.noproblemppc.com/partners.html"></asp:HyperLink>
        </div>
  
            <a runat="server" id="linkTopWidget" class="topwidget" visible="false"></a>
        </div>
    </div>
</div>
