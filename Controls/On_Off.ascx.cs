﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Publisher_On_Off : System.Web.UI.UserControl
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetTranslate(((PageSetting)Page).siteSetting.siteLangId);
        }
        HtmlGenericControl script = new HtmlGenericControl("script");
        script.Attributes.Add("language", "javascript");
        script.Attributes.Add("type", "text/javascript");
        script.ID = "script_OnOff";
        script.Attributes.Add("src", ResolveUrl("~") + @"Controls/ON_OFF.js");

        foreach (Control c in Page.Header.Controls)
        {
            if (c.ID == script.ID)
                return;
        }
        
        Page.Header.Controls.Add(script);
        
    }
   
    protected void Page_Load(object sender, EventArgs e)
    {
        
        

    }
    
    /*
    public void SetElements(int i)
    {
        string script = "SetElements('" + a_lbl_onOff.ClientID + "', '" + a_onOff.ClientID + "', '" +
            Hidden_OnOffValue.ClientID + "', '" + Hidden_Off.ClientID + "', '" + Hidden_ON.ClientID + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetElements" + i, script, true);
        a_onOff.Attributes.Add("href", "javascript:BtnOnOff(" + i + ");");
    }
     * */
    void GetTranslate(int siteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "On_Off.ascx", siteLangId);
    }
    
    public bool _ON
    {
        set
        {
            a_onOff.Attributes["class"] = "btn-on-off " + ((value) ? "on" : "off");
            hf_OnOffValue.Value = ((value) ? Hidden_ON.Value : Hidden_Off.Value);
            a_lbl_onOff.Text = hf_OnOffValue.Value;
        }
        get
        {
            return (hf_OnOffValue.Value == Hidden_ON.Value);
        }
    }
    public bool _InAuction
    {
        get 
        {
            return (a_onOff.HRef.StartsWith("javascript:void(0);"));
        }
        set 
        {
            a_onOff.HRef = (value) ? "javascript:void(0);" : "";
            div_on_off.Attributes["class"] = (value) ? "details" : "off_disable";
        }

    }
}
