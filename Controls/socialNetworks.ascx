﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="socialNetworks.ascx.cs" Inherits="Controls_socialNetworks" %>

<div id="fb-root"></div>

<script>


    function checkRegistrationStep(registerEmail, registerId, registerName, registerPassword, source) {
        //alert(source);
        //alert(registerEmail);
        var _source;
        if (source == "regularLogin")
            _source = "<%#checkLoginStatusRegular%>";
        else if (source == "regularRegister")
            _source = "<%#checkRegistrationStatusRegular%>";
        else
            _source = "<%#checkRegistrationStatusSocialNetworks%>";

        var _data;
        if (source == "regularLogin")
            _data = "{ email: '" + registerEmail + "', password:'" + registerPassword + "'}";
        else if (source == "regularRegister")
            _data = "{ email: '" + registerEmail + "', password:'" + registerPassword + "', referrer: '<%#registerReferrer%>' , comeFrom: '<%#comeFrom%>', registrationHitId: '<%#hitId%>' }";
        else
            _data = "{ email: '" + registerEmail + "', id:'" + registerId + "', name:'" + registerName + "',source:'" + source + "', referrer: '<%#registerReferrer%>', comeFrom: '<%#comeFrom%>', registrationHitId: '<%#hitId%>' }";

       
        $.ajax({
            url: _source,
            data: _data,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { // This boils the response string down 
                //  into a proper JavaScript Object().
                var msg = eval('(' + data + ')');

                /*
                If you aren’t familiar with the “.d” I’m referring to, it is simply a security feature that Microsoft added in ASP.NET 3.5’s version
                of ASP.NET AJAX. By encapsulating the JSON response within a parent object,
                the framework helps protect against a particularly nasty XSS vulnerability.

                */

                // If the response has a ".d" top-level property,
                //  return what's below that instead.
                if (msg.hasOwnProperty('d'))
                    return msg.d;
                else
                    return msg;
            },
            success: function (data) {
                //alert(data.result + ' ' + data.step);

                if (window.console)
                    console.log(data.result + ' ' + data.step);



                switch (data.result.toLowerCase()) {
                    case "ok":

                        /*
                        1 = VerifyYourEmail
                        2 = ChoosePlan
                        3 = BusinessDetails_General
                        4 = BusinessDetails_Category
                        5 = BusinessDetails_Area
                        6 = Checkout
                        7 = Dashboard
                        */

                        switch (data.step) {
                            case 1:
                                location.href = 'ResendYourEmail.aspx?id=' + data.supplierId + "&email=" + data.email;
                                break;

                            case 2:
                                location.href = 'choosePlan.aspx';
                                break;

                            case 3:
                                location.href = 'businessMainDetails.aspx';
                                break;

                            case 4:
                                location.href = 'categories.aspx';
                                break;

                            case 5:

                                location.href = 'location2.aspx';
                                break;

                            case 6:
                                location.href = 'checkout.aspx';
                                //alert('<%#ResolveUrl("~")%>ppc/frameppc2.aspx');
                                //location.href = '<%#ResolveUrl("~")%>ppc/frameppc2.aspx';
                                break;

                            case 7:
                                location.href = 'beforeEnterance.aspx';
                                break;

                            default:
                                break;
                        }

                        break;

                    case "failed":
                        //alert("failed: " + data.statusCode);
                        if (!ifExplorer())
                            stopActiveButtonAnim();

                        switch (data.statusCode) {

                            case "ANOTHER_ACCOUNT_ALREADY_HAS_THIS_EMAIL":
                                //SetServerComment('<%#EmailDuplicate%>');
                                location.href = 'ppcLogin.aspx?error=emailDuplicate&prePopulateEmail=' + data.email;
                                break;

                            case "WRONG_EMAIL_PASSWORD_COMBINATION":
                                SetServerComment('<%#WrongCombination%>');

                                break;

                            case "SEND_TO_LEGACY_PROCESS":
                                location.href = 'improvement.aspx?redirect=verifyYourEmailManually';
                                break;
                            //case "MORE_THAN_ONE_ACCOUNT_FOUND_SEND_TO_EMAIL_LOGIN":   

                            case "INACTIVE_ACCOUNT":                                                          
                                location.href = 'ppcLogin.aspx?error=inactiveAccount&prePopulateEmail=' + data.email;
                                break;

                            case "server page or server problem":
                                SetServerComment(_serverComment.GeneralError);
                                break;

                            default:
                                SetServerComment(_serverComment.GeneralError);
                                break;
                        }


                        break;

                    default:
                        break;
                }



            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //alert('faild ' + errorThrown);
                SetServerComment(_serverComment.GeneralError + ' ' + errorThrown);
                if (window.console)
                    console.log("failed " + textStatus + " " + errorThrown);
            }
        });
    }

    /*********************** facebook **************************/
    var flagFromRegiser = false; // true from login
    var flagDisableSocialCheck;
    flagDisableSocialCheck = "<%#flagDisableSocialCheck%>"; //  true: prevent recrusive calls

    window.fbAsyncInit = function () {        
        // appId: 392131104255963 dev
        // appId: 486879858091228 qa
        // appId: 699958330043400 production
        FB.init({
            appId: '<%#facebookAppId%>',
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true,  // parse XFBML
            version: 'v2.0'
        });

        // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
        // for any authentication related change, such as login, logout or session refresh. This means that
        // whenever someone who was previously logged out tries to log in again, the correct case below 
        // will be handled. 
        FB.Event.subscribe('auth.authResponseChange', function (response) {
            //alert(response.status);
            // Here we specify what we do with the response anytime this event occurs. 
            // see ex[lanation https://developers.facebook.com/docs/facebook-login/login-flow-for-web/
            /* response include

            {
            status: 'connected',
            authResponse:
            {
            accessToken: '...',
            expiresIn:'...',
            signedRequest:'...',
            userID:'...'
            }
            }

            */

            if (response.status === 'connected') {
                // here he also entered after  he cliced on "ok" buttom in facebook process when he aske him for approve
                // The response object is returned with a status field that lets the app know the current
                // login status of the person. In this case, we're handling the situation where they 
                // have logged in to the app.
                
                if (!flagFromRegiser && flagDisableSocialCheck.toLowerCase()=="false") {
                    getFaceBookProfile();
                }
                //faceBookLogIn2();
            } else if (response.status === 'not_authorized') {
                // In this case, the person is logged into Facebook, but not into the app, so we call
                // FB.login() to prompt them to do so. 
                // In real-life usage, you wouldn't want to immediately prompt someone to login 
                // like this, for two reasons:
                // (1) JavaScript created popup windows are blocked by most browsers unless they 
                // result from direct interaction from people using the app (such as a mouse click)
                // (2) it is a bad experience to be continually prompted to login upon page load.
                ////FB.login();

            } else {
                // In this case, the person is not logged into Facebook, so we call the login() 
                // function to prompt them to do so. Note that at this stage there is no indication
                // of whether they are logged into the app. If they aren't then they'll see the Login
                // dialog right after they log in to Facebook. 
                // The same caveats as above apply to the FB.login() call here.
                ////FB.login();
            }

            if (window.console)
                console.log("status:" + response.status);
        });
    };

    // Load the SDK asynchronously
    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement('script'); js.id = id; js.async = true;        
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        ref.parentNode.insertBefore(js, ref);
    } (document));

    // Here we run a very simple test of the Graph API after login is successful. 
    // This testAPI() function is only called in those cases.



    function getFaceBookProfile() {        
        if (window.console)
            console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function (response) {
            if (window.console)
                console.log('Good to see you, ' + ' ' + response.id + ' ' + response.name + ' ' + response.email + '.');

            if (response.email)
            {
                checkRegistrationStep(response.email, response.id, response.name,'', 'facebook');
            }
            else
            {
                faceBookLogIn2();
            }
        });
    }

    function faceBookLogOut() {
        FB.logout(function (response) {
            
            // Person is now logged out
        });

       
    }

    function faceBookLogIn() {
        FB.login(function (response) {
            if (response.authResponse) {
                if (window.console)
                    console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', function (response) {
                    if (window.console)
                        console.log('Good to see you, ' + response.name + '.');
                });

                FB.api('/me/feed', 'post', { message: 'Hello, world!' });

            } else {
                if (window.console)
                    console.log('User cancelled login or did not fully authorize.');
            }
        }, { scope: 'email' });
    }

    function faceBookUiFeed(email, facebookId, facebookName, flavor)
    {
        FB.ui({ method: 'feed', link: 'www.noproblem.me', description: 'NoProblem gets you quotes from the best professionals in your area. It simply works!' },
                       function (response) {
                           if (!response || response.error) {
                               if (window.console)
                                   console.log('Error occured or canceled wall.');
                               _gaq.push(['_trackEvent', 'Join us: facebook', 'sign up - error occured or disagree share post', flavor]);
                               //alert("error occured or disagree share post");
                           }
                           else {
                               if (window.console)
                                   console.log('Share by the surfer Post ID: ' + response.post_id);
                               _gaq.push(['_trackEvent', 'Join us: facebook', 'sign up - agree share post', flavor]);
                           }
                           //alert("ddgdf");
                           _gaq.push(['_trackEvent', 'Join us: facebook', 'sign up - call crm', flavor]);
                           checkRegistrationStep(email, facebookId, facebookName, '', 'facebook');
                           if (window.console)
                               console.log('Send to crm: email:' + email + ' id:' + facebookId + ' name:' + facebookName);

                       });
    }

    var counterMissingEmail=0;

    function faceBookLogIn2(auth_type_value) {
        //alert("inside" + auth_type_value);
        // the auth_type_value whould be undefined when we call this function without sending any real value
        // the auth_type_value can be 'rerequest' when we actually send this value
        flagFromRegiser = true;

        var flavor;
        flavor = document.getElementById("hdn_flavor");
        if (flavor)
            flavor = document.getElementById("hdn_flavor").value;
        else
            flavor = "";

        FB.login(function (response) {
            //alert('open dialog facebook login');
            
            _gaq.push(['_trackEvent', 'Join us: facebook', 'sign up - open dialog facebook login', flavor]);
            if (response.authResponse) {
                if (window.console)
                    console.log('before post.');
                //alert("authorized login");
                _gaq.push(['_trackEvent', 'Join us: facebook', 'sign up - authorized login', flavor]);
                var email = "";
                var facebookId = "";
                var facebookName = "";

                FB.api('/me', function (response) {
                    if (window.console)
                        console.log('before feed ' + response.email + " " + response.id + " " + response.name);
                    
                    email = response.email;
                    //alert('email:' + email + ' response.email:' + response.email);
                    facebookId = response.id;
                    facebookName = response.name;

                    if (!email) {
                        counterMissingEmail++;

                       

                        if (counterMissingEmail == 1)
                        {
                            var msgMissingEmail;
                            msgMissingEmail = 'You must provide your Facebook email address to register or login with Facebook; otherwise, use a different registration or login option.\nClick OK to continue.';
                            alert(msgMissingEmail);

                            faceBookLogIn2('rerequest');
                            return;
                        }

                        else // counterMissingEmail=2
                        {
                            counterMissingEmail = 0;
                            return;
                        }
                    }

                    else
                    {                  
                        faceBookUiFeed(email, facebookId, facebookName, flavor);                        

                    }
                        

                    
                });                
                   
                
            }
            else {
                // comes here when:
                // 1. leaves the dialog logon
                // 2. leaves next step the "authorize email and friends"
                if (window.console)
                    console.log('register scope email: User cancelled login or did not fully authorize.');
                _gaq.push(['_trackEvent', 'Join us: facebook', 'sign up - user cancelled login or did not fully authorize', flavor]);
                //alert("User cancelled login or did not fully authorize");

            }
        }, { scope: 'email', auth_type: auth_type_value });
    }



</script>


<!--*********************** google sign in **************************/-->
<!-- Place this asynchronous JavaScript just before your </body> tag -->
  
  <script type="text/javascript">
      
          (function () {
              var po = document.createElement('script');
              po.type = 'text/javascript'; po.async = true;
              po.src = 'https://apis.google.com/js/client:plusone.js?onload=render';
              var s = document.getElementsByTagName('script')[0];
              s.parentNode.insertBefore(po, s);
          })();

          function render() {
              gapi.signin.render('googleCustomSignIn', {
                  'callback': 'signinCallback',
                  'clientid': '868196622582-vl6pg174p48t8chura0dsoa0r84opvgt.apps.googleusercontent.com',
                  'cookiepolicy': 'single_host_origin',
                  'requestvisibleactions': 'http://schemas.google.com/AddActivity',
                  'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
              });
          }

          $(document).ready(function () {
              $('#googleCustomSignIn').click(function () {
                  flagDisableSocialCheck = "false";
              }
              )
          }
          )
          /**
          * Uses the JavaScript API to request the user's profile, which includes
          * their basic information. When the plus.profile.emails.read scope is
          * requested, the response will also include the user's primary email address
          * and any other email addresses that the user made public.
          */
          function loadProfile() {
              var request = gapi.client.plus.people.get({ 'userId': 'me' });
              request.execute(loadProfileCallback);
          }

          /**
          * Callback for the asynchronous request to the people.get method. The profile
          * and email are set to global variables. Triggers the user's basic profile
          * to display when called.
          */
          function loadProfileCallback(obj) {             
              profile = obj;

              // Filter the emails object to find the user's primary account, which might
              // not always be the first in the array. The filter() method supports IE9+.
              email = obj['emails'].filter(function (v) {
                  return v.type === 'account'; // Filter out the primary email
              })[0].value; // get the email from the filtered results, should always be defined.

              var id = obj.id
              var name = obj.name.givenName + ' ' + obj.name.familyName;

              var flavor;
              flavor = document.getElementById("hdn_flavor");
              if (flavor)
                  flavor = document.getElementById("hdn_flavor").value;
              else
                  flavor = "";

              _gaq.push(['_trackEvent', 'Join us: google', 'sign up - call crm', flavor]);
              checkRegistrationStep(email, id, name, '', 'googlePlus');
              
              if (window.console)
                console.log('email:' + email + ' id:' + id + ' name:' + name);
          }

          
          function signinCallback(authResult) {
              //alert("baaaaa 1" + authResult['status']['signed_in']);

              var flavor;
              flavor = document.getElementById("hdn_flavor");
              if (flavor)
                  flavor = document.getElementById("hdn_flavor").value;
              else
                  flavor = "";

              if (authResult['status']['signed_in']) {
                  // Update the app to reflect a signed in user
                  // Hide the sign-in button now that the user is authorized, for example:
                  ////document.getElementById('customBtn').setAttribute('style', 'display: none');

                  //alert(location.href);
                  if(flagDisableSocialCheck.toLowerCase() == "false")
                    gapi.client.load('plus', 'v1', loadProfile);  // Trigger request to get the email address.
              } else {
                  //alert("1: " + authResult['error']);
                  // Update the app to reflect a signed out user
                  // Possible error values:
                  //   "user_signed_out" - User is signed-out
                  //   "access_denied" - User denied access to your app
                  //   "immediate_failed" - Could not automatically log in the user
                  if (window.console)
                      console.log('Sign-in state: ' + authResult['error']);

                  if (authResult['error'] != "immediate_failed")
                    _gaq.push(['_trackEvent', 'Join us: google', 'sign up - user is signed-out ' + authResult['error'], flavor]); 
              }
          }
      
  </script>