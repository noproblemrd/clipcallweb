﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class Publisher_Toolbox_ToolboxWizardReport : System.Web.UI.UserControl
{
    public event System.EventHandler AddExec;
    public event System.EventHandler ExcelExec;
    public event System.EventHandler DeleteExec;
    public event System.EventHandler PrintExec;
    public event System.EventHandler EditExec;
    public event System.EventHandler AttachExec;
    public event System.EventHandler SaveExec;

    StringBuilder sb;

    public string GetUploadFileLocation
    {
        get { return ResolveUrl("~") + "Controls/Toolbox/UploadFile.aspx"; }
    }
    public string GetUploadFileLocationEvent
    {
        get
        {
            PostBackOptions myPostBackOptions = new PostBackOptions(this, "upload");
            myPostBackOptions.PerformValidation = false;

            return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {


        HtmlLink link = new HtmlLink();
        link.Href = ResolveUrl("~") + "Publisher/style.css";
        link.Attributes.Add("rel", "stylesheet");
        link.Attributes.Add("type", "text/css");
        Page.Header.Controls.Add(link);

        //TODO if it ok without this js

        /*
        HtmlGenericControl _script = new HtmlGenericControl("script");
        //type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.5.min.js"
        _script.Attributes.Add("language", "javascript");
        _script.Attributes.Add("type", "text/javascript");
        _script.Attributes.Add("src", @"http://code.jquery.com/jquery-1.5.min.js");
        Page.Header.Controls.Add(_script);
        */
        HtmlGenericControl script = new HtmlGenericControl("script");
        //type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.5.min.js"
        script.Attributes.Add("language", "javascript");
        script.Attributes.Add("type", "text/javascript");
        script.Attributes.Add("src", ResolveUrl("~") + @"Controls/Toolbox/Toolbox.js");

        if (!IsPostBack)
        {
            GetTranslate(((PageSetting)Page).siteSetting.siteLangId);
            SetClientScriptToDeletePre();
        }
        Page.Header.Controls.Add(script);


    }

    private void SetClientScriptToDeletePre()
    {
        sb = new StringBuilder();
        sb.Append("if (!IfMarked()) {alert('" + _lbl_chooseOnlyOneForDelete.Text + "'); return false;}");
        sb.Append("var result=confirm('" + GetConfirmDelete + "');");
        sb.Append("if(!result) return false;");
        _lb_delete.OnClientClick = sb.ToString();
    }
    public LinkButton GetDeleteControl()
    {
        return _lb_delete;
    }
    /*
    protected override void Render(HtmlTextWriter writer)
    {
        
        Page.ClientScript.RegisterForEventValidation(this.UniqueID, "search");
       


        base.Render(writer);

    }
     * */

    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        if (!IsPostBack)
        {            
            GetTranslate(((PageSetting)Page).siteSetting.siteLangId);
        }
         * */
        IToolBoxControl itbc = null;
        if (Page is IToolBoxControl)
            itbc = (IToolBoxControl)Page;
        if (itbc != null)
            itbc.SetToolBoxEvents(this);
        if (IsPostBack)
        {
            string _arg = Request["__EVENTARGUMENT"];
            if (_arg == "upload")
            {
                if (AttachExec != null)
                    AttachExec(null, EventArgs.Empty);
            }
        }
        else
        {
            if (itbc != null)
                itbc.SetToolBox(this);
        }

        this.DataBind();
    }
    public string GetUrlFile()
    {
        return hf_file_location.Value;
    }
    void GetTranslate(int siteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "ToolboxWizardReport.ascx", siteLangId);
    }
    
    public void RemoveAttach()
    {
        li_attach.Visible = false;
    }
    public void RemoveAdd()
    {
        li_add.Visible = false;
    }
    public void RemoveExcel()
    {
        li_excel.Visible = false;
    }
    public void RemovePrint()
    {
        li_print.Visible = false;
    }
    public void RemoveEdit()
    {
        li_Edit.Visible = false;
    }
    public void RemoveDelete()
    {
        li_delete.Visible = false;
    }

    public void RemoveSave()
    {
        li_save.Visible = false;
    }
    public void RemoveEditConditions()
    {
        _lb_Edit.OnClientClick = string.Empty;
    }
    protected void lb_save_Click(object sender, EventArgs e)
    {
        if (this.SaveExec != null)
            this.SaveExec(this, EventArgs.Empty);
    }
    protected void lb_add_Click(object sender, EventArgs e)
    {
        if (this.AddExec != null)
            this.AddExec(this, EventArgs.Empty);
    }

    protected void lb_delete_Click(object sender, EventArgs e)
    {
        if (this.DeleteExec != null)
            this.DeleteExec(this, EventArgs.Empty);
    }
    protected void lb_excel_Click(object sender, EventArgs e)
    {
        if (this.ExcelExec != null)
            this.ExcelExec(this, EventArgs.Empty);
    }
    protected void lb_print_Click(object sender, EventArgs e)
    {
        if (this.PrintExec != null)
            this.PrintExec(this, EventArgs.Empty);
    }
    protected void lb_edit_Click(object sender, EventArgs e)
    {
        if (this.EditExec != null)
            this.EditExec(this, EventArgs.Empty);
    }
    public string ChooseOnlyOne
    {
        get { return _lbl_chooseOnlyOneForDelete.Text; }
    }
    public string NoRecordDisplay
    {
        get { return _lbl_noRecords.Text; }
    }
    public string DeleteError
    {
        get { return _lbl_DelteError.Text; }
    }
    public string ChooseOne
    {
        get { return _lbl_ChooseOne.Text; }
    }

    public void SetClientScriptToSave(string script)
    {
        _lb_save.OnClientClick = script;
    }

    public void SetClientScriptToAdd(string script)
    {
        _lb_add.OnClientClick = script;
    }
    public string NoRecordExport
    {
        get { return _lbl_noExport.Text; }
    }
    public void SetClientScriptToDelete(string script)
    {
        sb.Append(script);
        _lb_delete.OnClientClick = sb.ToString();
    }
    public string GetConfirmDelete
    {
        get { return _lbl_confirmDelete.Text; }
    }
    public string ReportTitle
    {
        get { return _lbl_title.Text; }
        set { _lbl_title.Text = value; }
    }
    public string GetRecordMaches(int _count)
    {
        return _count + " " + lbl_RecordMached.Text;
    }
}

