﻿    var cb_elem = new Array();
    function SetCBs(elements)
    {   
        cb_elem = [];     
        var elems = elements.split(";");
        for(var i=0;i<elems.length;i++)
        {
            cb_elem.push(elems[i]);            
        }
    }
    function CheckSetAllCBs(CBAll)
    {
        var CB_all = document.getElementById(CBAll);
        for(var i=0;i<cb_elem.length;i++)
        {            
            var _cb = document.getElementById(cb_elem[i]);
            if(!_cb.checked)
            {                
                CB_all.checked = false;   
                return;
            }
        }
        CB_all.checked = true;
    }
    function CheckSetOneCBs()
    {
        var IfMark = false;
        for(var i=0;i<cb_elem.length;i++)
        {            
            var _cb = document.getElementById(cb_elem[i]);
            if(_cb.checked)
            {                
                if(IfMark)
                    return false;
                IfMark = true;
            }
        }
        return IfMark;
    }
    function SetAllCBs(_elm)
    { 
        
        var elm = document.getElementById(_elm);
        for(var i=0;i<cb_elem.length;i++)
        {          
            var _cb = document.getElementById(cb_elem[i]); 
            _cb.checked = elm.checked;
        }
    }
    function IfMarked()
    {
//        alert(cb_elem.length);
        for(var i=0;i<cb_elem.length;i++)
        {          
            var _cb = document.getElementById(cb_elem[i]); 
//            alert(_cb.checked);
            if(_cb.checked)
                return true;           
        }
        return false;
    }