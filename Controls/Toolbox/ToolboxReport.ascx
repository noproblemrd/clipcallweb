﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ToolboxReport.ascx.cs" Inherits="Publisher_Toolbox_ToolboxReport" %>


<div class="toolbox">
    
    <ul class="tools">
        
        <li id="li_excel" runat="server">         
            
            <asp:LinkButton ID="__lb_excel" runat="server" CssClass="excel excel_btn" 
                title="Export to Excel" onclick="lb_excel_Click"></asp:LinkButton>  
            <span class="span_excel" id ="span_excel" style="display:none;"></span>
            
        </li>
        <li id="li_print" runat="server">
            <asp:LinkButton ID="_lb_print" runat="server" CssClass="print" title="Print" 
                onclick="lb_print_Click"></asp:LinkButton>            
        </li>
        
        
        
    </ul>
    <div class="div_mid">
    
    <asp:Label ID="_lbl_title" runat="server" CssClass="span_mid"></asp:Label>
    </div >
</div>
<asp:Label ID="_lbl_noExport" runat="server" Text="There are no records to export" Visible="false"></asp:Label>
<asp:Label ID="_lbl_noRecords" runat="server" Text="There are no records to display" Visible="false"></asp:Label>
<asp:Label ID="lbl_RecordMached" runat="server" Text="records matched your query" Visible="false"></asp:Label>


