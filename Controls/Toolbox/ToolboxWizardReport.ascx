﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ToolboxWizardReport.ascx.cs" Inherits="Publisher_Toolbox_ToolboxWizardReport" %>


<script type="text/javascript" language="javascript">

    
    function openUploadFile()
    {
        window.open('<%# GetUploadFileLocation %>', 'my_uploadFile', 'fullscreen=no,width=400,height=100,resizable=no');
    }
    function ReciveUploadFile(_url)
    {
        document.getElementById("<%# hf_file_location.ClientID %>").value=_url;
        <%# GetUploadFileLocationEvent %>;
    }
    function IfCheckedOne()
    {
        if(!CheckSetOneCBs())
        {
            alert("<%# _lbl_chooseOnlyOneForEdit.Text %>");
            return false;
        }
        return true;
    }
    
</script>

<div class="toolbox">
    
    <ul class="tools">
        <li id="li_add" runat="server">
            <asp:LinkButton ID="_lb_add" runat="server" CssClass="add" title="Add Item" 
                onclick="lb_add_Click"></asp:LinkButton>
      
        </li>
        <li id="li_delete" runat="server">
            <asp:LinkButton ID="_lb_delete" runat="server" CssClass="delete" 
                title="Delete Item" onclick="lb_delete_Click"></asp:LinkButton>            
        </li>
        <li id="li_excel" runat="server">
            <asp:LinkButton ID="_lb_excel" runat="server" CssClass="excel" 
                title="Export to Excel" onclick="lb_excel_Click"></asp:LinkButton>  
            
        </li>
        <li id="li_print" runat="server">
            <asp:LinkButton ID="_lb_print" runat="server" CssClass="print" title="Print" 
                onclick="lb_print_Click"></asp:LinkButton>            
        </li>
        <li id="li_Edit" runat="server">
            <asp:LinkButton ID="_lb_Edit" runat="server" CssClass="edit" title="Edit" 
                onclick="lb_edit_Click" OnClientClick="return IfCheckedOne();"></asp:LinkButton>            
        </li>
        <li id="li_attach" runat="server">
            <a id="_lb_attach" runat="server" class="attach" title="Attach" onclick="javascript:openUploadFile();"></a>
             
        </li>
        
        <li id="li_save" runat="server">
            <asp:LinkButton ID="_lb_save" runat="server" CssClass="save" title="save" 
                onclick="lb_save_Click"></asp:LinkButton>  
             
        </li>
    </ul>
    <div class="div_mid">
    
    <asp:Label ID="_lbl_title" runat="server" CssClass="span_mid"></asp:Label>
    </div >
</div>



<asp:Label ID="_lbl_noRecords" runat="server" Text="There are no records to display" Visible="false"></asp:Label>
<asp:Label ID="_lbl_DelteError" runat="server" Text="There are errors in this auction" Visible="false"></asp:Label>

<asp:Label ID="_lbl_ChooseOne" runat="server" Text="You can choose only one" Visible="false"></asp:Label>
<asp:Label ID="_lbl_noExport" runat="server" Text="There are no records to export" Visible="false"></asp:Label>

<asp:Label ID="_lbl_confirmDelete" runat="server" Text="Are you sure you want to delete" Visible="false"></asp:Label>
<asp:HiddenField ID="hf_ChooseAtleast" runat="server" Value="You must choose at least one record" />

<asp:Label ID="_lbl_chooseOnlyOneForDelete" runat="server" Text="Please choose report for deletion" Visible="false"></asp:Label>


<asp:Label ID="_lbl_chooseOnlyOneForEdit" runat="server" Text="Please choose one report for edit" Visible="false"></asp:Label>

<asp:HiddenField ID="hf_file_location" runat="server" />

<asp:Label ID="lbl_RecordMached" runat="server" Text="records matched your query" Visible="false"></asp:Label>

