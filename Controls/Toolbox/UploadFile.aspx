﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadFile.aspx.cs" Inherits="Publisher_Toolbox_UploadFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">
    function PassFile()
    {
        var _file = document.getElementById("f_name");
        if(_file.value.length==0)
        {
            alert(document.getElementById("<%# lbl_NoFile.ClientID %>").value);
            return false;
        }
        return true;
      //  alert(_file.value);
    //    window.opener.ReciveUploadFile(_file.value);
    //    window.close();
    }
    function CloseWin(name)
    {
        window.opener.ReciveUploadFile(name);
        window.close();
    }
    function CloseWin_Error() {
        
        window.close();
    }
    
    
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <input type="file" id="f_name" runat="server" />
        <asp:Button ID="btn_upload" runat="server" Text="Upload" 
            OnClientClick="return PassFile();" onclick="btn_upload_Click"  />
        
      <!--  <input id="_btn_upload" type="button" value="Upload" onclick="javascript:PassFile();" />-->
      <asp:HiddenField ID="lbl_NoFile" runat="server" Value="No file was selected" />
        <asp:Label ID="lbl_FileTypeError" runat="server" Text="Wrong file or file type incorrect" Visible="false"></asp:Label>
    </div>
    </form>
</body>
</html>
