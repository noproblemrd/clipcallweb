﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Publisher_Toolbox_ToolboxReport : System.Web.UI.UserControl
{
    
    public event System.EventHandler ExcelExec;
   
    public event System.EventHandler PrintExec;
    bool _LoadStyle = true;
    public bool LoadStyle
    {
        get { return _LoadStyle; }
        set { _LoadStyle = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        if (!_LoadStyle)
            return;
        HtmlLink link = new HtmlLink();
        link.Href = ResolveUrl("~") + "Publisher/style.css";
        link.Attributes.Add("rel", "stylesheet");
        link.Attributes.Add("type", "text/css");
        bool ThereIs = false;
        foreach (Control _control in Page.Header.Controls)
        {
            if (_control.GetType() != typeof(HtmlLink))
                continue;
            if (((HtmlLink)_control).Href.Contains("style.css") && ((HtmlLink)_control).AppRelativeTemplateSourceDirectory.Contains("Publisher"))
                ThereIs = true;
        }
        if (!ThereIs)
            Page.Header.Controls.Add(link);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetTranslate(((PageSetting)Page).siteSetting.siteLangId);
        }
    }
    void GetTranslate(int siteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "ToolboxReport.ascx", siteLangId);
    }
    protected void lb_print_Click(object sender, EventArgs e)
    {
        if (this.PrintExec != null)
            this.PrintExec(null, EventArgs.Empty);
    }
    protected void lb_excel_Click(object sender, EventArgs e)
    {
        if (this.ExcelExec != null)
            this.ExcelExec(null, EventArgs.Empty);
    }
    public void SetTitle(string _title)
    {
        _lbl_title.Text = _title;
    }
    public string NoRecordExport
    {
        get { return _lbl_noExport.Text; }
    }
    public string NoRecordToPrint
    {
        get { return _lbl_noRecords.Text; }
    }
    public string RecordMached
    {
        get { return lbl_RecordMached.Text; }
    }
    public string GetRecordMaches(int _count)
    {
        return _count + " " + lbl_RecordMached.Text;
    }
    public string GetRecordMaches()
    {
        return "Thousands of " + lbl_RecordMached.Text;
    }
    public LinkButton GetExcelButton
    {
        get { return __lb_excel; }
    }
    public LinkButton GetPrintButton
    {
        get { return _lb_print; }
    }
    public void SetExcelJavascript(string _script)
    {
        __lb_excel.OnClientClick = _script + "return false;";
    }
    public void SetPrintJavascript(string _script)
    {
        _lb_print.OnClientClick = _script + "return false;";
    }
    public void RemovePrintButton()
    {
        li_print.Visible = false;
    }
    public void RemoveExcelButton()
    {
        li_excel.Visible = false;
    }
    public void SetButtonPrintAsAsync()
    {
      //  ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(__lb_excel);
        ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(_lb_print);
    }
}
