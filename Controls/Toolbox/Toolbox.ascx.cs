﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class Publisher_Toolbox_Toolbox : System.Web.UI.UserControl, IPostBackEventHandler
{
    protected void Page_Init(object sender, EventArgs e)
    {
        HtmlGenericControl script = new HtmlGenericControl("script");
        script.Attributes.Add("language", "javascript");
        script.Attributes.Add("type", "text/javascript");
        script.Attributes.Add("src", ResolveUrl("~") + @"Controls/Toolbox/Toolbox.js");
        Page.Header.Controls.Add(script);
        if(!IsPostBack)
            GetTranslate(((PageSetting)Page).siteSetting.siteLangId);

    }
    public event System.EventHandler SearchExec;
    public event System.EventHandler AddExec;
    public event System.EventHandler ExcelExec;
    public event System.EventHandler DeleteExec;
    public event System.EventHandler PrintExec;
    public event System.EventHandler EditExec;
    public event System.EventHandler AttachExec;
      

    string Searc_onkeypress;
    StringBuilder sb;
    string _Search_onEnterPressValid;
    public string Search_onEnterPressValid
    {
        get
        {
            if(string.IsNullOrEmpty(_Search_onEnterPressValid))
                return "true";
            else
                return _Search_onEnterPressValid;
        }
    }
    public string Searc_OnKeyPress
    {
        get 
        {
            if (string.IsNullOrEmpty(Searc_onkeypress))
                return string.Empty;
            else
                return Searc_onkeypress;
        }
 //       set { Searc_onkeypress = value; }
    }
    public string GetUploadFileLocation
    {
        get { return ResolveUrl("~") + "Controls/Toolbox/UploadFile.aspx?UploadType=" + FileUploadTypeV; }
    }
    public string GetUploadFileLocationEvent
    {
        get
        {
            PostBackOptions myPostBackOptions = new PostBackOptions(this, "upload");
            myPostBackOptions.PerformValidation = false;
            return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);//.GetPos
        }
    }
    

    private void SetClientScriptToDeletePre()
    {
        sb = new StringBuilder();
        sb.Append("if (!IfMarked()) {alert('" + hf_ChooseAtleast.Value + "'); return false;}");
        sb.Append("var result=confirm('" + Utilities.EncodeJsString(GetConfirmDelete) + "');");
        sb.Append("if(!result) return false;");
        _lb_delete.OnClientClick = sb.ToString();
    }
    /*
    protected override void Render(HtmlTextWriter writer)
    {
        
        Page.ClientScript.RegisterForEventValidation(this.UniqueID, "search");
       


        base.Render(writer);

    }
     * */
   
    protected void Page_Load(object sender, EventArgs e)
    {
        IToolBoxControl itbc = null;
        if (Page is IToolBoxControl)
            itbc = (IToolBoxControl)Page;
        if (itbc != null)
            itbc.SetToolBoxEvents(this);
        if (!IsPostBack)
        {
            _txt_search.Value = hf_searchWate.Value;
            
            SetClientScriptToDeletePre();
            if (itbc != null)
                itbc.SetToolBox(this);
        }
        else
        {
            string _arg = Request["__EVENTARGUMENT"];
            if (_arg == "search")
                Search_click();
            else if (_arg == "upload")
            {
                if (AttachExec != null)
                    AttachExec(null, EventArgs.Empty);
            }
        }
    //    string stam = Request.Form["__EVENTARGUMENT"];
        PostBackOptions myPostBackOptions = new PostBackOptions(this, "search");
        myPostBackOptions.PerformValidation = false;
        
        Searc_onkeypress = Page.ClientScript.GetPostBackEventReference(myPostBackOptions);//.GetPostBackEventReference(this, "search");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WaterSearch", "try{OnToolboxOnload();}catch(ex){}", true);
        this.DataBind();
    }
    public string GetUrlFile()
    {
        return hf_file_location.Value;
    }
    void GetTranslate(int siteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "Toolbox.ascx", siteLangId);
    }
    public void RemoveSearch()
    {
        div_search.Visible = false;
    }
    public void RemoveAttach()
    {
        li_attach.Visible = false;
    }
    public void RemoveAdd()
    {
        li_add.Visible = false;
    }
    public void RemoveExcel()
    {
        li_excel.Visible = false;
    }
    public void RemovePrint()
    {
        li_print.Visible = false;
    }
    public void RemoveEdit()
    {
        li_Edit.Visible = false;
    }
    public void RemoveDelete()
    {
        li_delete.Visible = false;
    }

    public void SearchValidation(string _reg, string validMessage)
    {                
        _btn_submit.OnClientClick = GetStringValidation(_reg, validMessage);
    }
    public void AddValidation(string _reg, string validMessage)
    {
        _lb_add.OnClientClick = GetStringValidation(_reg, validMessage);
    }
    string GetStringValidation(string arg, string _mess)
    {
        _Search_onEnterPressValid = "SearchTxtValidation('" + arg + "', '" + _mess + "')";
        return "return SearchTxtValidation('" + arg + "', '"+_mess+"');";
    }

    public void SearchValidationExists(string _reg, string validMessage, string validExists)
    {
        if (string.IsNullOrEmpty(_reg))
            _reg = "^.+$";
        _btn_submit.OnClientClick = GetStringValidationExists(_reg, validMessage, validExists);
    }
    public void AddValidationExists(string _reg, string validMessage, string validExists)
    {
        if (string.IsNullOrEmpty(_reg))
            _reg = "^.+$";
        _lb_add.OnClientClick = GetStringValidationExists(_reg, validMessage, validExists);
    }
    string GetStringValidationExists(string arg, string _mess, string validExists)
    {
        return "return checkAll('" + arg + "', '" + _mess + "', '"+validExists+"');";
    }

    public string GetTxt
    {
        get 
        {
            if (_txt_search.Value == hf_searchWate.Value &&
               hf_IsWatermark.Value == "true")
                return string.Empty;
            
            return _txt_search.Value;
        }
    }

    protected void btn_submit_Click(object sender, EventArgs e)
    {
        Search_click();
    }
    void Search_click()
    {
        if (_txt_search.Value == hf_searchWate.Value && hf_IsWatermark.Value == "true")
  //             _txt_search.Attributes["class"].Contains("watermark"))
            _txt_search.Value = string.Empty;
        if (this.SearchExec != null)
            this.SearchExec(this, EventArgs.Empty);
    }
    protected void lb_add_Click(object sender, EventArgs e)
    {
        if (this.AddExec != null)
            this.AddExec(this, EventArgs.Empty);
    }
    
    protected void lb_delete_Click(object sender, EventArgs e)
    {
        if (this.DeleteExec != null)
            this.DeleteExec(this, EventArgs.Empty);
    }
    protected void lb_excel_Click(object sender, EventArgs e)
    {
        if (this.ExcelExec != null)
            this.ExcelExec(this, EventArgs.Empty);
    }
    protected void lb_print_Click(object sender, EventArgs e)
    {
        if (this.PrintExec != null)
            this.PrintExec(this, EventArgs.Empty);
    }
    protected void lb_edit_Click(object sender, EventArgs e)
    {
        if (this.EditExec != null)
            this.EditExec(this, EventArgs.Empty);
    }

    

    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        Search_click();
    }
   
    #endregion
    public string NoRecordDisplay
    {
        get { return _lbl_noRecords.Text; }
    }
    public string DeleteError
    {
        get { return _lbl_DelteError.Text; }
    }
    public string ChooseOne
    {
        get { return _lbl_ChooseOne.Text; }
    }
    public HtmlInputText GetTextBoxSearch
    {
        get { return _txt_search; }
    }
    public string GetWaterTextSearch
    {
        get { return hf_searchWate.Value; }
    }
    public void SetClientScriptToAdd(string script)
    {
        _lb_add.OnClientClick = script;
    }
    public void SetClientScriptToEdit(string script)
    {
        _lb_Edit.OnClientClick = script;
    }
    public string NoRecordExport
    {
        get { return _lbl_noExport.Text; }
    }
    public void SetClientScriptToDelete(string script)
    {       
        sb.Append(script);
        _lb_delete.OnClientClick = sb.ToString();
    }
    public void SetExcelJavascript(string _script)
    {
        _lb_excel.OnClientClick = _script + "return false;";
    }
    public string GetConfirmDelete
    {
        get { return _lbl_confirmDelete.Text; }
    }
    public string SetConfirmDelete
    {
        set { _lbl_confirmDelete.Text = value; }
    }
    public LinkButton GetEditButton
    {
        get { return _lb_Edit; }
    }
    public Button GetSearchButton
    {
        get { return _btn_submit; }
    }
    public void SetFileUploadType(eFileType _FileUploadType)
    {
        FileUploadTypeV = _FileUploadType;
    }
    public void VisibleDollarIcon(string _OnClickScript)
    {
        li_Dollar.Visible = true;
        _OnClickScript = string.IsNullOrEmpty(_OnClickScript) ? "void(0);" : _OnClickScript;
        a_Dollar.Attributes.Add("onclick", _OnClickScript);
    }
    eFileType FileUploadTypeV
    {
        get { return ViewState["FileUploadType"] == null ? eFileType.NONE : (eFileType)ViewState["FileUploadType"]; }
        set { ViewState["FileUploadType"] = value; }
    }
    
}
