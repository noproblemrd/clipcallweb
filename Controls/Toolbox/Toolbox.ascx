﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Toolbox.ascx.cs" Inherits="Publisher_Toolbox_Toolbox" %>

<script type="text/javascript">

 //   OnToolboxOnload();
    function OnToolboxOnload()
    {
        var IsWatermark = document.getElementById("<%# hf_IsWatermark.ClientID %>");
        var inputSearchDefault = document.getElementById("<%# hf_searchWate.ClientID %>").value;
        var txt_search = document.getElementById("<%# _txt_search.ClientID %>");
        if((txt_search.value == inputSearchDefault &&  IsWatermark.value == "true") || txt_search.value == "")
        {
            txt_search.value = inputSearchDefault;
            txt_search.className = "form-text watermark";
            IsWatermark.value = "true";
        }
        else
        {
            txt_search.className = "form-text";
            IsWatermark.value = "false";
        }
        
        txt_search.onfocus = function()
                        {                            
                            if(txt_search.value == inputSearchDefault &&  IsWatermark.value == "true")
                                txt_search.value = "";
                            txt_search.className = "form-text";                            
                        };
        txt_search.onblur = function()
                        {
                              if(txt_search.value == ""){
                                    txt_search.value = inputSearchDefault;
                                    txt_search.className = "form-text watermark";
                                    IsWatermark.value = "true";
                                }
                                else
                                {
                         //           alert('po');
                                    txt_search.className = "form-text";
                                    IsWatermark.value = "false";
                                }
                        };
        
    }
    

    function SearchTxtValidation(_reg, _mess)
    {
        var _water = document.getElementById("<%# hf_searchWate.ClientID %>").value;
        var _validat = document.getElementById("<%# _lbl_invalid.ClientID %>");
        _validat.style.visibility = "hidden";
        var _txt=document.getElementById("<%# _txt_search.ClientID %>").value;
        
        if(_txt==_water && 
                document.getElementById("<%# _txt_search.ClientID %>").className.indexOf("watermark") != -1)
        {         
           document.getElementById("<%# _txt_search.ClientID %>").value="";
           _txt="";
        }
        if(_txt.length==0)
            return true;
        var _reg_exp=new RegExp(_reg);
        
        if(_txt.search(_reg_exp)==-1)
        {
            _validat.innerHTML=_mess;
            _validat.style.visibility = "visible";
            return false;            
        }
        else
            return true;
       
    }
    function TxtExists(_mess)
    {
        var _validat = document.getElementById("<%# _lbl_invalid.ClientID %>");
        _validat.style.visibility = "hidden";
        var _val = new String();
        _val = document.getElementById("<%# _txt_search.ClientID %>").value;
        _val = _val.RemoveSpace();
        if(_val.length==0 || (_val==document.getElementById("<%# hf_searchWate.ClientID %>").value &&
                    document.getElementById("<%# _txt_search.ClientID %>").className.indexOf("watermark") != -1))
        {
            document.getElementById("<%# _txt_search.ClientID %>").value="";            
            _validat.innerHTML=_mess;
            _validat.style.visibility = "visible";
            return false;
        }
        return true;
    }
    function checkAll(_reg, _notExists, _notValid)
    {
        if(!TxtExists(_notExists))
            return false;
        else
        {
            var reges = _reg.split('|||');
            if(reges.length == 1)
                return SearchTxtValidation(_reg, _notValid);
            var validations = _notValid.split('|||');
            for(var i=0; i<reges.length; i++)
            {
                //alert(reges[i]);
                if(!SearchTxtValidation(reges[i], validations[i]))
                {
                    alert(validations[i]);
                    return false;
                }
            }
        }
        return true;
    }
    function SearchPress(e)
    {
        
        var code;
        if (!e) var e = window.event;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        
      //  alert(code);
        if(code == 13)
        {
            if(!(<%# Search_onEnterPressValid %>))
                return false;
            <%# Searc_OnKeyPress %>;
            return false; 
        }
        return true;
    }
    function openUploadFile()
    {
        window.open('<%# GetUploadFileLocation %>', 'my_uploadFile', 'fullscreen=no,width=400,height=100,resizable=no');
    }
    function ReciveUploadFile(_url)
    {
        document.getElementById("<%# hf_file_location.ClientID %>").value=_url;
        <%# GetUploadFileLocationEvent %>;
    }
    function IfCheckedOne()
    {
        if(!CheckSetOneCBs())
        {
            alert("<%# _lbl_chooseOnlyOneForEdit.Text %>");
            return false;
        }
        return true;
    }
</script>

<div class="toolbox">
    <div class="form-toolbox-search" id="div_search" runat="server">
     
        <input type="text" class="form-text watermark" value="Search" id="_txt_search" runat="server"
            onkeypress="return SearchPress(event);" />
    
        <asp:Button ID="_btn_submit" runat="server" Text="" CssClass="form-submit-search" 
            onclick="btn_submit_Click"   />
        <asp:HiddenField ID="hf_IsWatermark" runat="server" Value="true" />
       
    </div>
    
    <ul class="tools">
        <li id="li_add" runat="server">
            <asp:LinkButton ID="_lb_add" runat="server" CssClass="add" title="Add Item" 
                onclick="lb_add_Click"></asp:LinkButton>
      
        </li>
        <li id="li_delete" runat="server">
            <asp:LinkButton ID="_lb_delete" runat="server" CssClass="delete" 
                title="Delete Item" onclick="lb_delete_Click"></asp:LinkButton>            
        </li>
        <li id="li_excel" runat="server">
            <asp:LinkButton ID="_lb_excel" runat="server" CssClass="excel" 
                title="Export to Excel" onclick="lb_excel_Click"></asp:LinkButton>  
            
        </li>
        <li id="li_print" runat="server">
            <asp:LinkButton ID="_lb_print" runat="server" CssClass="print" title="Print" 
                onclick="lb_print_Click"></asp:LinkButton>            
        </li>
        <li id="li_Edit" runat="server">
            <asp:LinkButton ID="_lb_Edit" runat="server" CssClass="edit" title="Edit" 
                onclick="lb_edit_Click" OnClientClick="return IfCheckedOne();"></asp:LinkButton>            
        </li>
        <li id="li_attach" runat="server">
            <a id="_lb_attach" runat="server" class="attach" title="Attach" onclick="javascript:openUploadFile();"></a>
             
        </li>
        <li id="li_Dollar" runat="server" visible="false">
            <a id="a_Dollar" runat="server" class="dollar" title="Payment" href="javascript:void(0);"></a>
                  
        </li>
    </ul>
</div>
<div class="div_validation">
    <asp:Label ID="_lbl_invalid" runat="server" Text="Invalid" CssClass="error-msg"
     style="visibility:hidden;"></asp:Label>     
</div>
<asp:HiddenField ID="hf_searchWate" runat="server" Value="Search" />

<asp:Label ID="_lbl_noRecords" runat="server" Text="There are no records to display" Visible="false"></asp:Label>
<asp:Label ID="_lbl_DelteError" runat="server" Text="There are errors in this auction" Visible="false"></asp:Label>

<asp:Label ID="_lbl_ChooseOne" runat="server" Text="You can choose only one" Visible="false"></asp:Label>
<asp:Label ID="_lbl_noExport" runat="server" Text="There are no records to export" Visible="false"></asp:Label>

<asp:Label ID="_lbl_chooseOnlyOneForEdit" runat="server" Text="Please choose one record for edit" Visible="false"></asp:Label>

<asp:Label ID="_lbl_confirmDelete" runat="server" Text="Are you sure you want to delete" Visible="false"></asp:Label>
<asp:HiddenField ID="hf_ChooseAtleast" runat="server" Value="You must choose at least one record" />
<asp:HiddenField ID="hf_file_location" runat="server" />
