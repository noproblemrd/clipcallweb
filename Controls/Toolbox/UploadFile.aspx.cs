﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Publisher_Toolbox_UploadFile : PageSetting
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!userManagement.IsPublisher())
                Response.Redirect(ResolveUrl("~") + @"Publisher/PublisherLogin.aspx");
            string UploadType = Request.QueryString["UploadType"];
            eFileType eft;
            if (!Enum.TryParse(UploadType, out eft))
                eft = eFileType.NONE;
            ExtensionFilesV = eft;
        }
        Header.DataBind();
    }

    private bool IsFileType(string localPath)
    {
        eFileType eft = ExtensionFilesV;
        string extension = System.IO.Path.GetExtension(localPath);
        switch(eft)
        {
            case(eFileType.NONE):
                return true;
            case(eFileType.EXCEL):
                return (extension == ".xls" || extension == ".xlsx");
            case(eFileType.IMAGE):
                return IsImage(localPath);

        }
        return false;
       
    }
    bool IsImage(string localPath)
    {
        ASPJPEGLib.IASPJpeg objJpeg;
        objJpeg = new ASPJPEGLib.ASPJpeg();
        try
        {

            objJpeg.Open(localPath);
        }
        catch (Exception exc)
        {
            return false;
        }
        return true;
    }
    protected void btn_upload_Click(object sender, EventArgs e)
    {

        string name = f_name.PostedFile.FileName;
        if (string.IsNullOrEmpty(name))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "noFile", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoFile.Value) + "');", true);
            return;
        }
        string extension = System.IO.Path.GetExtension(name);
  //      if (extension == ".xls" || extension == ".xlsx")
 //       {
            string localPath = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
            int rnd = new Random().Next();
            localPath += rnd + "upload" + extension;
            //      string _file = Server.HtmlEncode(localPath);
            string _file = localPath.Replace(@"\", @"\\");
            f_name.PostedFile.SaveAs(localPath);
        if(IsFileType(localPath))
            ClientScript.RegisterStartupScript(this.GetType(), "CloseWin", "CloseWin('" + _file + "');", true);
  //      }
        else
//        {
            ClientScript.RegisterStartupScript(this.GetType(), "ErrorFile", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_FileTypeError.Text) + "');", true);
 //       }

    }
    eFileType ExtensionFilesV
    {
        get { return ViewState["ExtensionFiles"] == null ? eFileType.NONE : (eFileType)ViewState["ExtensionFiles"]; }
        set { ViewState["ExtensionFiles"] = value; }
    }
}
