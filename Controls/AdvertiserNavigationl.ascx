﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvertiserNavigationl.ascx.cs" Inherits="Controls_AdvertiserNavigationl" %>
<script type="text/javascript">
    function SetDisableClass() {
        document.getElementById("<%# btn_myminisite.ClientID %>").setAttribute("class", "mini-disabled");
        // document.getElementById("btn-myminisite").setAttribute("class", "mini-disabled");
    }
    function SetDisableClassMyCall() {
        document.getElementById("<%# btn_calls.ClientID %>").setAttribute("class", "calls-disabled");

        //   document.getElementById("btn-calls").setAttribute("class", "calls-disabled");
    }
    function SetSettingAtLast() {
        document.getElementById("<%#btn_faq.ClientID%>").setAttribute("class", "last");

    }

</script>
<div class="navigation" runat="server" id="div_navigation">
		<ul id="ul_navigation" runat="server">
		    
		    <li id="btn_myAccount" class="_btn_myaccount first" runat="server">
	            <a id="a_frameAspx" runat="server" class="btn-myaccount" href="../Management/frame.aspx">	            
                    <asp:Label ID="lbl_myAccount" runat="server" Text="My Account" CssClass="span_navigation"></asp:Label>
                </a>	                       
			</li>		   
			
			<li id="btn_myminisite" class="_btn_myminisite" runat="server">
	            <a id="linkProfessionalUpdate" runat="server" class="btn-myminisite" href="../Management/ProfessionalUpdate.aspx">
	                <asp:Label ID="lbl_myminisite" runat="server" Text="My Mini Site" CssClass="span_navigation"></asp:Label>
	            </a>
	           
			</li>
			
			 <li id="btn_calls" class="_btn_calls" runat="server">
	            <a id="linkCalls" runat="server" class="btn-calls" href="../Management/MyCalls.aspx">
	                <asp:Label ID="lbl_MyReports" runat="server" Text="My Reports" CssClass="span_navigation"></asp:Label>
	            </a>
	            
	            <ul>
	                <li id="linkCalls2" runat="server" ><a href="../Management/MyCalls.aspx" id="a_linkCalls" runat="server">My Calls</a></li>
	                <li id="li_balance" runat="server" ><a href="../Management/MyBalance.aspx" id="a_balance" runat="server">My balance</a></li>
	                <li id="li_MyReview" runat="server" class="last"><a href="../Management/MyReview.aspx" id="a_MyReview" runat="server">My review</a></li>
			    </ul>
			    
			</li>			
            
            <li id="btn_faq" class="_btn_setting" runat="server">
	            <a id="linkSetting" runat="server" class="btn-faq" href="../Management/ProfessionalSetting.aspx">
	                <asp:Label ID="lbl_Setting" runat="server" Text="Settings" CssClass="span_navigation"></asp:Label>
	           </a>
	           
			</li>         
            
            
			<li class="last" id="btn_support" > <span runat="server" id="span_forlink"  ><a href="javascript:void(0)" runat="server" id="a_support" target="_blank" class="btn-support">
               
                <asp:Label ID="lbl_support" runat="server" Text="Support"  CssClass="span_navigation"></asp:Label></a>
                </span>
                
            </li> 
            
			
		</ul>
	</div>
