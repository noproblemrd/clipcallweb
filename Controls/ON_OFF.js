﻿/// <reference path="http://code.jquery.com/jquery-latest.js" />
/*
var elements = new Array();
function OnOff(a_lbl_onOff, a_onOff, Hidden_OnOffValue, Hidden_Off, Hidden_ON)
{
    this.label = a_lbl_onOff;
    this.a = a_onOff;
    this.hf = Hidden_OnOffValue;
    this.hf_off = Hidden_Off;
    this.hf_ON = Hidden_ON;
}

function SetElements(a_lbl_onOff, a_onOff, Hidden_OnOffValue, Hidden_Off, Hidden_ON)
{
    var elm=new OnOff(a_lbl_onOff, a_onOff, Hidden_OnOffValue, Hidden_Off, Hidden_ON);
    elements.push(elm);
  //  alert(elements.length);
}
function BtnOnOff(i)
{
    //var t=new OnOff();
    t=elements[i];
    
    
    var el=document.getElementById(t.label);
    var el_a=document.getElementById(t.a);
    var _hidden=document.getElementById(t.hf);
    if (el_a.className == 'btn-on-off on')
    {
        el_a.className = 'btn-on-off off';
        _hidden.value = document.getElementById(t.hf_off).value;
        el.innerHTML = _hidden.value;
    } 
    else 
    {
        el_a.className = 'btn-on-off on';
        _hidden.value = document.getElementById(t.hf_ON).value;
        el.innerHTML = _hidden.value;
    }
}
function ClearArrayElements() {
    elements.length = 0;
}
/*************/

$(function () {
    _SetOnOff();
});
function _SetOnOff() {
    $('.btn-on-off').each(function () {
        if (typeof $(this).attr('href') == 'undefined' || $(this).attr('href').length == 0) {
            $(this).removeAttr('href');
            return;
        }
        $(this).click(function () {
            var _on = $(this).closest('.details').find('input[id*="Hidden_ON"]').val();
            var _off = $(this).closest('.details').find('input[id*="Hidden_Off"]').val();
            var _value = "";
            if ($(this).find('span').html() == _on) {
                $(this).removeClass('on');
                $(this).addClass('off');
                _value = _off;
            }
            else {
                $(this).removeClass('off');
                $(this).addClass('on');
                _value = _on;
            }

            $(this).find('span').html(_value);
            $(this).closest('.details').find('input[id*="hf_OnOffValue"]').val(_value);
        });
    });
}