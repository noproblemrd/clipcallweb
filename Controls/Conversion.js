﻿$(function () {
    $('.data-table2').find('tr:visible').each(function () {
        $(this).click(function () {
            var _tr = $(this).next('tr');

        });
    });
});
function GetTRDetails(_tr) {
    var _date = $(_tr).find('.-date').html();
    $.ajax({
        url: "<%# GetDpzSupplierDetails %>",
        data: "{'date':'" + _date + "'}",
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            if (data.d.length == 0) {
                return;
            }
            var _data = eval('(' + data.d + ')');

            if (_data.ResponseStatus != 'OK') {
                alert(_data.ResponseStatus);
            }
            else {
                ClearAccountDialogValidation();
                $('#hf_PageIndex').val('');
                $('#<%# txt_Name.ClientID %>').val(_data.Name);
                $('#<%# txt_BizId.ClientID %>').val(_data.BizId);
                $('#<%# txt_VirtualNumber.ClientID %>').val(_data.VirtualNumber);
                $('#<%# cb_IsActive.ClientID %>').prop('checked', _data.IsActive);
                $('#<%# cb_SendSms.ClientID %>').prop('checked', _data.SendSMS);
                $('#<%# hf_SupplierId.ClientID %>').val(_data.SupplierId);
                $('#<%# txt_PhoneNumber.ClientID %>').val(_data.PhoneNumber);
                $('#<%# txt_SmsNumber.ClientID %>').val(_data.SmsNumber);
                $(".AccountDialog").dialog('open');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //              alert(textStatus);
            hideDiv();
        },
        complete: function (jqXHR, textStatus) {
            //  HasInHeadingRequest = false;
            hideDiv();
        }
    });
};