﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="invite.ascx.cs" Inherits="Controls_invite" %>
<script>
    function gotoInvitePage() {
        location.href = "../ppc/invitePro.aspx";
    }

    function askMoreInvites() {
        
        $.ajax({
            url: "../ppc/inviteMorePro.ashx",
            data: { siteId: "<%=siteId%>", supplierId: "<%=supplierId%>" },
            dataType: "json",
            type: "POST",
            //dataFilter: function (data) { return data; },
            success: function (data) {
                //alert(data);

                //alert(data.result);

                if (data.result == "success") {
                    $("#<%=inviteWait.ClientID%>").show();
                    $("#<%=inviteButton.ClientID%>").hide();
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //                alert('faild');
            }
        });
    }
</script>

<div class="inviteContent">   
    <h4 >Invite a Pro</h4>
    <span class="friendsLeft"><span runat="server" id="span_FriendsNum"></span> <span>left</span></span>
    <div class="clear"></div>
    <div class="inviteDesc">
        Make some dough. Earn $<%=InvitationSuccessBonus%> + more!
    </div>

</div>

<div class="div_FriendsPic">
    <div id="friend1" class="invitePro01" runat="server">
    </div>
    
    <div id="friend2" class="invitePro02" runat="server">
    </div>

    <div id="friend3" class="invitePro03" runat="server">
    </div>
</div>

<div class="clear"></div>

<div id="inviteButton" class="inviteButton" runat="server"  >
   INVITE NOW!
</div>


<div id="inviteWait" class="inviteWait" runat="server">
    Request for more invites sent.
</div>

