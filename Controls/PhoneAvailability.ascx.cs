﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;

public partial class Controls_PhoneAvailability : System.Web.UI.UserControl
{
    //<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    PageSetting ps;
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        HtmlGenericControl hl = new HtmlGenericControl("script");
        hl.Attributes["src"] = "http://www.google.com/jsapi";
        hl.Attributes["type"] = "text/javascript";
        Page.Header.Controls.Add(hl);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ps = (PageSetting)Page;
        if (!IsPostBack)
        {
            LoadPhoneAvailability();
            a_ExtendHours.HRef = GetExtendHours;
        }
        this.DataBind();
    }

    private void LoadPhoneAvailability()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfPhoneAvailabilityData result;
        try
        {
            result = _supplier.GetPhoneAvailabilityData(new Guid(ps.GetGuidSetting()));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return;
        span_OnDuty.Text = result.Value.LeadsOn + " leads";
        ON_DUTY = result.Value.PercentageOfWeeklyHours + "";
        OFF_DUTY = (100 - result.Value.PercentageOfWeeklyHours) + "";
        span_OffDuty.Text = result.Value.LeadOff + " leads";
        span_percent.Text = result.Value.PercentageOfWeeklyHours + "%";
        if(result.Value.LeadOff == 0)
            div_OffDuty.Visible = false;
        if(result.Value.LeadsOn == 0)
            div_OnDuty.Visible = false;
        if (result.Value.LeadOff == 0 && result.Value.LeadsOn == 0)
        {
            ChartDivParent.Style[HtmlTextWriterStyle.MarginLeft] = "30px";
            WhiteCircle.Style[HtmlTextWriterStyle.Left] = "55px";
        }
    }
    /*
    protected string GetChartSwf
    {
        get
        {
            return ResolveUrl("~/scripts/FusionCharts/Doughnut2D.swf");
        }
    }
    string GetChartData(int OnDuty)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(@"var dataString = '<chart  bgColor=""FFFFFF"" showBorder=""0"" showPercentageValues=""1"" plotBorderColor=""FFFFFF"" numberPrefix=""$"" isSmartLineSlanted=""1""");
        sb.Append(@" showValues=""0"" showLabels=""0"" showLegend=""0"">");
        sb.Append(@"<set value="""+OnDuty+@""" label=""ON DUTY"" color=""99CC00""/>");
        sb.Append(@"<set value=""" + (100-OnDuty)+@""" label=""OFF DUTY"" color=""333333""/>");
        sb.Append(@"</chart>';");
        return sb.ToString();
    }
     * */
    /*
    void GetChartData(int OnDuty)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("google.visualization.arrayToDataTable([");
        sb.Append("['Task', 'Hours per Day'],");
        sb.Append("['ON DUTY', " + OnDuty + "],");
        sb.Append("['OFF DUTY', " + (100 - OnDuty) + "]");
        sb.Append("]);");
        _GetChartData =  sb.ToString();
    
    }
     * */
    protected string _GetChartData { get; set; }
    protected string GetExtendHours
    {
        get { return ResolveUrl("~/PPC/FramePpc2.aspx?step=4"); }
    }
    protected string ON_DUTY
    {
        get;
        set;
    }
    protected string OFF_DUTY
    {
        get;
        set;
    }
}