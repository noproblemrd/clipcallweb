﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FromToDateTime.ascx.cs" 
Inherits="Controls_FromToDateTime" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit"
 TagPrefix="cc1" %>

<div class="div_from_to_parent">
<div class="dates2">
<div class="label">
    <asp:Label ID="lbl_fromFate" runat="server"  Text="Date From"></asp:Label>
</div>
    <div class="fromdate div_from">
        
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender_From" runat="server" TargetControlID="txt_From"
        EnableViewState="true" WatermarkText="mm/dd/yyyy" BehaviorID="WaterFrom"></cc1:TextBoxWatermarkExtender>

        <asp:TextBox ID="txt_From" runat="server" CssClass="From_Date" autocomplete="off"></asp:TextBox>

        <cc1:CalendarExtender ID="CalendarExtender_From" PopupPosition="BottomRight" runat="server" 
        TargetControlID="txt_From" PopupButtonID="a_fromDate" Format="MM/dd/yyyy" 
        OnClientShown="ShowDivCalender"  OnClientHidden="HideDivCalender" 
        BehaviorID="<%# hf_From.ClientID %>"></cc1:CalendarExtender>
        
        <input id="hf_From" type="hidden" class="from_id" runat="server" />
        <asp:DropDownList ID="ddl_from" runat="server">
        </asp:DropDownList>


    </div>
    </div>
    <div class="dates2">
  <div class="label">
    <asp:Label ID="lbl_toDate" runat="server" Text="To"></asp:Label>
  </div>
    <div class="fromdate div_to">
        
	    
	    <cc1:TextBoxWatermarkExtender  ID="TextBoxWatermarkExtender_To" runat="server" TargetControlID="txt_To" 
        EnableViewState="true" WatermarkText="mm/dd/yyyy" BehaviorID="WaterTo"></cc1:TextBoxWatermarkExtender>

        <asp:TextBox ID="txt_To" runat="server" CssClass="To_Date" autocomplete="off"></asp:TextBox>
       
	    <cc1:CalendarExtender ID="CalendarExtender_To" PopupPosition="BottomRight" runat="server"  
        TargetControlID="txt_To" PopupButtonID="a_toDate" Format="MM/dd/yyyy" 
        OnClientShown="ShowDivCalender" OnClientHidden="HideDivCalender" 
        BehaviorID="<%# hf_To.ClientID %>"></cc1:CalendarExtender>
        
         <input id="hf_To" type="hidden" class="to_id" runat="server" />
         <input id="lbl_wrongDate" type="hidden" class="lbl_wrongDate" runat="server" value="<%# GetWrongDate %>" />

        <asp:DropDownList ID="ddl_to" runat="server">
        </asp:DropDownList>
        
    </div>
</div>
<div class="clear"></div>

<div class="error_msg">
    
    <asp:Label ID="lbl_dateError" runat="server" Text="From Date should be sooner or same as to date" Visible="false"></asp:Label>
    <asp:Label ID="lblWrongDate" runat="server" Text="Wrong date format. The right one is" Visible="false"></asp:Label>
    

    <input id="hf_dateError" type="hidden" class="hf_dateError" runat="server"  Value="<%# GetDateError %>"/>
    <input id="hf_DateFormat" type="hidden" class="date_format" runat="server"  Value="MM/dd/yyyy"/>

    
</div>
</div>