﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="searchGoogle.ascx.cs" Inherits="Controls_searchGoogle" %>
     
     <!-- onblur="checkGeocode(this, event);" --> 
   <asp:TextBox ID="txt_Region" runat="server" CssClass="textActive" place_holder="Enter your location" HolderClass="place-holder" text-mode="name" ></asp:TextBox>
        

   <script>

        var place;
        var clickedFromList=false;

        function setAdrres() {

            if (!place.geometry) {
                // Inform the user that a place was not found and return.
                alert("not found");
                return;
            }

            //alert(place.formatted_address);
            var str_address;
            var city;

            //alert("city " + city);
            var state;
            var stateShort;
            var zipCode;

            for (var i = 0; i < place.address_components.length; i++) {
                //alert(place.address_components[i].types[0] + " " + place.address_components[i].long_name);

                if (place.address_components[i].types[0] == "postal_code")
                    zipCode = place.address_components[i].long_name;

                if (place.address_components[i].types[0] == "locality")
                    city = place.address_components[i].long_name;

                if (place.address_components[i].types[0] == "administrative_area_level_1") {
                    state = place.address_components[i].long_name;
                    stateShort = place.address_components[i].short_name;
                }

                
            }

            

            if (state) {
                str_address = state;
                document.getElementById("hidden_state").value = stateShort;
                //alert(document.getElementById("hidden_state").value);
            }

            if (city)
                str_address += " " + city;

            if (zipCode)
                str_address += " " + zipCode;

            //alert("str_address: " + str_address);

            //alert("found");
        }

        var options = {
            //types: ['(cities)'],
            //componentRestrictions: { country: 'Usa' }
        };


        var autocomplete = new google.maps.places.Autocomplete($("#txt_Region")[0], options);


        google.maps.event.addListener(autocomplete, 'place_changed', function () {
           
            place = autocomplete.getPlace();
            setAdrres();
            clickedFromList = true;
            //alert(clickedFromList);
            regionValidation();

            lat_lng = place.geometry.location;
            lat = lat_lng.lat();
            lng = lat_lng.lng();

            document.getElementById("hiddem_lat").value = lat;
            document.getElementById("hiddem_lng").value = lng;

            //console.log(place.address_components);
        });



        function setChooseFromList(blnChoosed) {
            clickedFromList = blnChoosed;
        }

        function checkGeocode(elem, e) {
            alert("here");

            /*
            https://developers.google.com/maps/documentation/javascript/geocoding
            The status code may return one of the following values:
            google.maps.GeocoderStatus.OK indicates that the geocode was successful.
            google.maps.GeocoderStatus.ZERO_RESULTS indicates that the geocode was successful but returned no results. This may occur if the geocode was passed a non-existent address or a latng in a remote location.
            google.maps.GeocoderStatus.OVER_QUERY_LIMIT indicates that you are over your quota. Refer to the Usage limits exceeded article in the Maps for Business documentation, which also applies to non-Business users, for more information.
            google.maps.GeocoderStatus.REQUEST_DENIED indicates that your request was denied for some reason.
            google.maps.GeocoderStatus.INVALID_REQUEST generally indicates that the query (address or latLng) is missing.
            */

            var geocoder = new google.maps.Geocoder();
            //"componentRestrictions":{"country":"DE"}
            geocoder.geocode({ 'address': elem.value, 'componentRestrictions': { 'country': 'DE'} }, function (response, status) {
                alert(google.maps.GeocoderStatus);
                if (status == google.maps.GeocoderStatus.OK && response[0]) {
                    alert("inside");
                    /*
                    lat_lng = response[0].geometry.location;
                    _radius = 30;
                    document.getElementById("hf_FullNameAddress").value = response[0].formatted_address + ";;;" + elem.value;
                    $('#div_LocationError' + index).hide();
                    $('#div_LocationSmile' + index).show();
                    SlideElement("div_LocationComment" + index, false);
                    full_name = response[0].formatted_address;

                    SetMap();
                    */
                }
                else {
                    /*
                    $('#map_canvas').empty();
                    $('#div_map').hide();
                    $('#hf_FullNameAddress').val('');
                    full_name = '';
                    map = new google.maps.Map(document.getElementById('map_canvas'), null);
                    document.getElementById("div_LocationError").style.display = "block";
                    SlideElement("div_LocationComment", true);
                    */
                }
            });
        }
 </script>

 <asp:HiddenField runat="server" ID="hiddem_lat" />
 <asp:HiddenField runat="server" ID="hiddem_lng" />
 <asp:HiddenField runat="server" ID="hidden_state" />
 <asp:HiddenField runat="server" ID="hidden_city" />
 