﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Controls_steps : System.Web.UI.UserControl
{
    protected WebReferenceSupplier.Supplier _supplier;
    protected string _supplierGuid;
    protected string _supplierPhone;
    protected bool _ifTestRecord = false;
    protected bool _ifTestRecord2 = false;
    protected int _countNotDone=0;
    protected int _bonusAmount;
    protected string _siteId;    
    protected string _callerId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           // _supplier = WebServiceConfig.GetSupplierReference((PageSetting)Page);

            tasksStatus(_supplier);          
           
        }
    }


 
    protected void tasksStatus(WebReferenceSupplier.Supplier supplier)
    {

        WebReferenceSupplier.ResultOfGetStartedTasksStatusResponse resultOfGetStartedTasksStatusResponse = supplier.RetrieveGetStartedTasksStatus(new Guid(supplierGuid));
        
        WebReferenceSupplier.GetStartedTasksStatusResponse getStartedTasksStatusResponse = resultOfGetStartedTasksStatusResponse.Value;
        bonusAmount = getStartedTasksStatusResponse.BonusAmount;
        
        countNotDone = getStartedTasksStatusResponse.TasksLeft;
        supplierPhone = getStartedTasksStatusResponse.SupplierPhone;
        callerId = getStartedTasksStatusResponse.CallerId;

        /*************************** register **********************/
        WebReferenceSupplier.GetStartedTasksStatus register = getStartedTasksStatusResponse.Register;

        registerDescription = register.Description;
        stepRegister.InnerHtml=registerDescription;

        registerDone = register.Done;

        if (registerDone == true)
        {
            stepRegister.Attributes.Add("class", "stepDone");
            circleStepRegister.Attributes.Add("class", "stepDoneCircle");
        }

        else
        {
            stepRegister.Attributes.Add("class", "stepNotDone");
            circleStepRegister.Attributes.Add("class", "stepNotDoneCircle");            
        }

        //WebReferenceSupplier.GetStartedTaskType registerTaskType = register.TaskType;
        registerRank = register.Rank;

        /*************************** update calls **********************/
        WebReferenceSupplier.GetStartedTasksStatus workHours = getStartedTasksStatusResponse.WorkHours;

        workHoursDescription = workHours.Description;
        stepWorkHours.InnerHtml = "<a href='../PPC/FramePpc.aspx?step=4'>" + workHoursDescription + "</a>";

        workHoursDone = workHours.Done;

        if (workHoursDone == true)
        {
            stepWorkHours.Attributes.Add("class", "stepDone");
            circleStepWorkHours.Attributes.Add("class", "stepDoneCircle");
        }

        else
        {
            stepWorkHours.Attributes.Add("class", "stepNotDone");
            circleStepWorkHours.Attributes.Add("class", "stepNotDoneCircle");
        }

        int workHoursRank = workHours.Rank;

        /*************************** video **********************/

        /*
        WebReferenceSupplier.GetStartedTasksStatus video = getStartedTasksStatusResponse.WatchVideo;

        videoDescription = video.Description;
        stepVideo.InnerHtml = "<a href='javascript:void(0);' onclick='javascript:showVideo();'>"
            + videoDescription + "</a><div id='stepVideoClose' onclick='closeVideo();' class='stepVideoClose'></div>";

        videoDone = video.Done;

        if (videoDone == true)
        {
            stepVideo.Attributes.Add("class", "stepDone");
            circleStepVideo.Attributes.Add("class", "stepDoneCircle");
        }

        else
        {
            stepVideo.Attributes.Add("class", "stepNotDone");
            circleStepVideo.Attributes.Add("class", "stepNotDoneCircle");
        }
        
        videoRank = register.Rank;
        */

       
        /*************************** honor **********************/
        WebReferenceSupplier.GetStartedTasksStatus honor = getStartedTasksStatusResponse.HonorCode;

        honorDescription = honor.Description;
        //stepHonor.InnerHtml = honorDescription;
        stepHonor.InnerHtml = "<a href='../PPC/honorCode.aspx'>"
           + honorDescription + "</a>";       

        honorDone = honor.Done;

        if (honorDone == true)
        {
            stepHonor.Attributes.Add("class", "stepDone");
            circleStepHonor.Attributes.Add("class", "stepDoneCircle");
        }

        else
        {
            stepHonor.Attributes.Add("class", "stepNotDone");
            circleStepHonor.Attributes.Add("class", "stepNotDoneCircle");
        }

        honorRank = honor.Rank;
            
        /*************************** test call **********************/
        WebReferenceSupplier.GetStartedTasksStatus testCall = getStartedTasksStatusResponse.CallTest;
        
        testCallDescription = testCall.Description;
        
        
        testCallDone = testCall.Done;

        if (testCallDone == true)
        {
            stepTestCall.Attributes.Add("class", "stepDone");
            circleStepTestCall.Attributes.Add("class", "stepDoneCircle");
            stepTestCall.InnerHtml = testCallDescription; 
        }

        else
        {
            stepTestCall.Attributes.Add("class", "stepNotDone");
            circleStepTestCall.Attributes.Add("class", "stepNotDoneCircle");
            stepTestCall.InnerHtml = "<a href='javascript:void(0);' onclick='showHideTestCall(\"" + callerId + "\",\"" + supplierPhone + "\",\"" + supplierGuid + "\");'>" + testCallDescription + "</a>"; 
        }

        testCallRank = testCall.Rank;       

        /*************************** listen **********************/
        WebReferenceSupplier.GetStartedTasksStatus listen = getStartedTasksStatusResponse.ListenRecording;

        listenDescription = listen.Description;
        //stepListen.InnerHtml = listenDescription;

        listenDone = listen.Done;

        if (testCallDone == true)
        {
            if (listenDone == true)
            {
                stepListen.Attributes.Add("class", "stepDone");
                circleStepListen.Attributes.Add("class", "stepDoneCircle");
                stepListen.InnerHtml = listenDescription;
                
            }

            else
            {
                stepListen.Attributes.Add("class", "stepNotDone");
                circleStepListen.Attributes.Add("class", "stepNotDoneCircle");
                stepListen.InnerHtml = "<a href='javascript:void(0);' onclick='javascript:OpenRecord2()' >" + listenDescription + "</a>";
            }
        }

        else // if there isn't test call yet make listen disabled
        {
            stepListen.Attributes.Add("class", "stepDepended");
            circleStepListen.Attributes.Add("class", "stepDependedCircle");
            stepListen.InnerHtml = listenDescription;
            
        }

        listenRank = listen.Rank;

        /*
        if (registerDone == true && workHoursDone == true && videoDone == true && honorDone == true && testCallDone == true && listenDone==true)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "systemMessage", "updateSystemMessage('Hero You righteously earned $" + bonusAmount + " to spend getting phone leads!');", true);
            //return;
        }
        */

        /*
        <div id="circleStepVideo"  runat="server">
         <div id="circleStepNumberVideo" class="circleStepNumber" runat="server">2</div>
     </div>
     <div id="stepVideo" runat="server"></div
        */


        //register.TaskType= WebReferenceSupplier.GetStartedTaskType.
        /*
        WebReferenceSupplier.GetStartedTasksStatus[] getStartedTasksStatus = getStartedTasksStatusResponse.TasksList;
        bonusAmount=getStartedTasksStatusResponse.BonusAmount;
        dataListTaskStatus.DataSource = getStartedTasksStatus;
        dataListTaskStatus.DataBind();
        */
      
    }

    protected void dataListTaskStatus_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item ||
           e.Item.ItemType == ListItemType.AlternatingItem)
        {
            bool status = Convert.ToBoolean(
                ((WebReferenceSupplier.GetStartedTasksStatus)e.Item.DataItem).Done);
           
            WebReferenceSupplier.GetStartedTaskType taskType= (WebReferenceSupplier.GetStartedTaskType)((WebReferenceSupplier.GetStartedTasksStatus)e.Item.DataItem).TaskType;
            
            HtmlGenericControl divStep = (HtmlGenericControl)e.Item.FindControl("step");
            HtmlGenericControl circleStep = (HtmlGenericControl)e.Item.FindControl("circleStep");
            HtmlGenericControl circleStepNumber = (HtmlGenericControl)e.Item.FindControl("circleStepNumber");

            string rank = Convert.ToString(
                ((WebReferenceSupplier.GetStartedTasksStatus)e.Item.DataItem).Rank);
            circleStepNumber.InnerHtml = rank;
            

            if (status) // done
            {
                divStep.Attributes.Add("class", "stepDone");
                circleStep.Attributes.Add("class", "stepDoneCircle");
                
                if (taskType == WebReferenceSupplier.GetStartedTaskType.call_test )
                {
                    ifTestRecord = true;
                }

                /*
                else if (taskType == WebReferenceSupplier.GetStartedTaskType.watch_video)
                {    
                    
                    //HtmlGenericControl divVideo = new HtmlGenericControl("<br>");
                    //this.dataListTaskStatus.Controls.Add(divVideo); 
                   
                    //Literal lc = new Literal();
                    //this.dataListTaskStatus.Controls.Add(lc);
                    
                }
                */
                
            }
            else // not done
            {
                countNotDone++;

                if (taskType == WebReferenceSupplier.GetStartedTaskType.listen_recording && !ifTestRecord)
                {
                    divStep.Attributes.Add("class", "stepDepended");
                    circleStep.Attributes.Add("class", "stepDependedCircle");
                }
               
                else
                {
                        
                    divStep.Attributes.Add("class", "stepNotDone");
                    circleStep.Attributes.Add("class", "stepNotDoneCircle");

                    /*
                    if (taskType == WebReferenceSupplier.GetStartedTaskType.watch_video)
                    {
                        
                        //HtmlGenericControl divVideo = new HtmlGenericControl("<div id='video'>ertrtrt</div>");
                        //this.dataListTaskStatus.Controls.Add(divVideo);
                        
                    }
                    */
                }
            }

                       

        }

    }

    public int getBonus()
    {
        return bonusAmount;
    }
    
    public WebReferenceSupplier.Supplier supplier
    {
        get
        {
            return _supplier;
        }

        set
        {
            _supplier = value;
        }
    }
    

    public string supplierGuid
    {
        get
        {
            return _supplierGuid;
        }

        set
        {
            _supplierGuid = value;
        }
    }

    public string siteId
    {
        get
        {
            return _siteId;
        }

        set
        {
            _siteId = value;
        }
    }


    

    public bool ifTestRecord
    {
        get
        {
            return _ifTestRecord;
        }

        set
        {
            _ifTestRecord = value;
        }
    }

    public bool ifTestRecord2
    {
        get
        {
            return _ifTestRecord;
        }

        set
        {
            _ifTestRecord = value;
        }
    }

    protected int countNotDone
    {
        get
        {
            return _countNotDone;
        }

        set
        {
            _countNotDone = value;
        }
    }
    protected string countNotDoneStep
    {
        get { return (_countNotDone == 1) ? _countNotDone + " step" : _countNotDone + " steps"; }
    }
    protected int bonusAmount
    {
        get
        {
            return _bonusAmount;
        }

        set
        {
            _bonusAmount = value;
        }
    }


    protected string supplierPhone
    {
        get
        {
            return _supplierPhone;
        }

        set
        {
            _supplierPhone = value;
        }
    }

    protected string callerId
    {
        get
        {
            return _callerId;
        }

        set
        {
            _callerId = value;
        }
    }

    #region register
    protected string registerDescription
    {
        get
        {
            return (ViewState["registerDescription"] == null ? "" : (string)ViewState["registerDescription"]);
        }

        set
        {
            ViewState["registerDescription"] = value;
        }
    }

    protected bool registerDone
    {
        get
        {
            return (ViewState["registerDone"] == null ? false : (bool)ViewState["registerDone"]);
        }

        set
        {
            ViewState["registerDone"] = value;
        }
    }

    /*
    protected WebReferenceSupplier.GetStartedTaskType registerTaskType
    {
        get
        {
            return (ViewState["registerTaskType"] == null ? null : (WebReferenceSupplier.GetStartedTaskType)ViewState["registerTaskType"]);
        }

        set
        {
            ViewState["registerTaskType"] = value;
        }
    }
    */



    protected int registerRank
    {
        get
        {
            return (ViewState["registerRank"] == null ? 0 : (int)ViewState["registerRank"]);
        }

        set
        {
            ViewState["registerRank"] = value;
        }
    } 
    #endregion


    #region video
    protected string videoDescription
    {
        get
        {
            return (ViewState["videoDescription"] == null ? "" : (string)ViewState["videoDescription"]);
        }

        set
        {
            ViewState["videoDescription"] = value;
        }
    }

    protected bool videoDone
    {
        get
        {
            return (ViewState["videoDone"] == null ? false : (bool)ViewState["videoDone"]);
        }

        set
        {
            ViewState["videoDone"] = value;
        }
    }

    /*
    protected WebReferenceSupplier.GetStartedTaskType registerTaskType
    {
        get
        {
            return (ViewState["registerTaskType"] == null ? null : (WebReferenceSupplier.GetStartedTaskType)ViewState["registerTaskType"]);
        }

        set
        {
            ViewState["registerTaskType"] = value;
        }
    }
    */



    protected int videoRank
    {
        get
        {
            return (ViewState["videoRank"] == null ? 0 : (int)ViewState["videoRank"]);
        }

        set
        {
            ViewState["videoRank"] = value;
        }
    }

    #endregion


    #region WorkHors
    protected string workHoursDescription
    {
        get
        {
            return (ViewState["workHoursDescription"] == null ? "" : (string)ViewState["workHoursDescription"]);
        }

        set
        {
            ViewState["workHoursDescription"] = value;
        }
    }

    protected bool workHoursDone
    {
        get
        {
            return (ViewState["workHoursDone"] == null ? false : (bool)ViewState["workHoursDone"]);
        }

        set
        {
            ViewState["workHoursDone"] = value;
        }
    }

    /*
    protected WebReferenceSupplier.GetStartedTaskType registerTaskType
    {
        get
        {
            return (ViewState["registerTaskType"] == null ? null : (WebReferenceSupplier.GetStartedTaskType)ViewState["registerTaskType"]);
        }

        set
        {
            ViewState["registerTaskType"] = value;
        }
    }
    */



    protected int workHoursRank
    {
        get
        {
            return (ViewState["workHoursRank"] == null ? 0 : (int)ViewState["workHoursRank"]);
        }

        set
        {
            ViewState["workHoursRank"] = value;
        }
    } 
    #endregion


    #region honor
    protected string honorDescription
    {
        get
        {
            return (ViewState["honorDescription"] == null ? "" : (string)ViewState["honorDescription"]);
        }

        set
        {
            ViewState["honorDescription"] = value;
        }
    }

    protected bool honorDone
    {
        get
        {
            return (ViewState["honorDone"] == null ? false : (bool)ViewState["honorDone"]);
        }

        set
        {
            ViewState["honorDone"] = value;
        }
    }

    /*
    protected WebReferenceSupplier.GetStartedTaskType registerTaskType
    {
        get
        {
            return (ViewState["registerTaskType"] == null ? null : (WebReferenceSupplier.GetStartedTaskType)ViewState["registerTaskType"]);
        }

        set
        {
            ViewState["registerTaskType"] = value;
        }
    }
    */



    protected int honorRank
    {
        get
        {
            return (ViewState["honorRank"] == null ? 0 : (int)ViewState["honorRank"]);
        }

        set
        {
            ViewState["honorRank"] = value;
        }
    }
    #endregion


    #region test call
    protected string testCallDescription
    {
        get
        {
            return (ViewState["testCallDescription"] == null ? "" : (string)ViewState["testCallDescription"]);
        }

        set
        {
            ViewState["testCallDescription"] = value;
        }
    }

    public bool testCallDone
    {
        get
        {
            return (ViewState["testCallDone"] == null ? false : (bool)ViewState["testCallDone"]);
        }

        set
        {
            ViewState["testCallDone"] = value;
        }
    }

    public string getTestCallDone()
    {
        return "ddfgd";
    }
    /*
    protected WebReferenceSupplier.GetStartedTaskType registerTaskType
    {
        get
        {
            return (ViewState["registerTaskType"] == null ? null : (WebReferenceSupplier.GetStartedTaskType)ViewState["registerTaskType"]);
        }

        set
        {
            ViewState["registerTaskType"] = value;
        }
    }
    */



    protected int testCallRank
    {
        get
        {
            return (ViewState["testCallRank"] == null ? 0 : (int)ViewState["testCallRank"]);
        }

        set
        {
            ViewState["testCallRank"] = value;
        }
    }
    #endregion


    #region listen
    protected string listenDescription
    {
        get
        {
            return (ViewState["listenDescription"] == null ? "" : (string)ViewState["listenDescription"]);
        }

        set
        {
            ViewState["listenDescription"] = value;
        }
    }

    protected bool listenDone
    {
        get
        {
            return (ViewState["listenDone"] == null ? false : (bool)ViewState["listenDone"]);
        }

        set
        {
            ViewState["listenDone"] = value;
        }
    }

    /*
    protected WebReferenceSupplier.GetStartedTaskType registerTaskType
    {
        get
        {
            return (ViewState["registerTaskType"] == null ? null : (WebReferenceSupplier.GetStartedTaskType)ViewState["registerTaskType"]);
        }

        set
        {
            ViewState["registerTaskType"] = value;
        }
    }
    */



    protected int listenRank
    {
        get
        {
            return (ViewState["listenRank"] == null ? 0 : (int)ViewState["listenRank"]);
        }

        set
        {
            ViewState["listenRank"] = value;
        }
    }
    #endregion


    
}