﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="menuVerticalBilling.ascx.cs" Inherits="Controls_menuVerticalBilling" %>

<div class="navigationContainer">
<div id="tab_navigation" class="tab-navigation">
	<ul id="ulSteps">
		<li id="liStep1"><a runat="server" class="mouseover" style="cursor:hand;" href="../ppc/billingoverview2.aspx" id="linkStep1"><span class="titleNavigation titleNavigationFirst">Billing overview</span></a></li>
		<li id="liStep2"><a runat="server" class="mouseover" style="cursor:hand;" href="../ppc/defaultPriceSettings.aspx" id="linkStep2"><span class="titleNavigation">Default price settings</span></a></li>
        <li id="liStep3"><a runat="server" class="mouseover" style="cursor:hand;" href="../ppc/PaymentMethod.aspx" id="linkStep3"><span class="titleNavigation">Payment method</span></a></li>
		<li id="liStep4"><a runat="server" class="mouseover" style="cursor:hand;" href="../ppc/transactions2014.aspx" id="linkStep4"><span class="titleNavigation titleNavigationLast">Transactions</span></a></li>		
	</ul>
</div>
</div>