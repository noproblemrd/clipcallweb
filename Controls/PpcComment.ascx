﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PpcComment.ascx.cs" Inherits="Controls_PpcComment" %>
<script type="text/javascript">
    var _serverComment = new Server_Comment('<%# MultipleComment %>', '<%# GeneralError %>');
    function Server_Comment(MultipleComment, GeneralError) {
        this.MultipleComment = MultipleComment;
        this.GeneralError = GeneralError;
    }
    function RemoveServerComment() {        
        $('#div_ServerComment').removeClass('comment-show');
        $('#div_ServerComment').removeClass('comment-show-2line');
        setTimeout(function () {
            $("#lbl_ServerComment").html('');
            $('a.a_systemMessageClose').hide();
        }, 200);
    }
    function SetServerComment(_comment) {
        $("#lbl_ServerComment").html(_comment);
        ShowComment();
    }

    function SetServerComment2(_comment) {        
        $("#lbl_ServerComment").html(_comment);
        ShowComment2();
    }

    function _SetServerComment(_comment) {
        SetServerComment(_comment);
        window.scrollTo(0, 0);
    }

    function SetServerComment3(_comment) {
        $("#lbl_ServerComment").html(_comment);
        ShowComment3();
    }

    function GeneralServerError() {
        $("#lbl_ServerComment").html(_serverComment.GeneralError);
        ShowComment();
    }
    function _GeneralServerError() {
        GeneralServerError();
        window.scrollTo(0, 0);
    }
    function GeneralMultipleComment() {
        $("#lbl_ServerComment").html(_serverComment.MultipleComment);
        ShowComment();
    }
    function _GeneralMultipleComment() {
        GeneralMultipleComment();
        window.scrollTo(0, 0);
    }
    function ShowComment() {
        //alert("gghjghj");
        
        $('#div_ServerComment').removeClass('comment-show-2line');
        $('#div_ServerComment').addClass('comment-show');
        
        $('a.a_systemMessageClose').show();
        setTimeout(function () {
        if ($("#lbl_ServerComment").get(0).offsetHeight > 20) {
        $('#div_ServerComment').removeClass('comment-show');
        $('#div_ServerComment').addClass('comment-show-2line');
        }
        }, 300);
       
    }

    function ShowComment2() {        
        $(".container_comment").css('display','block');
        $("#lbl_ServerComment").show(500);
        $('#div_ServerComment').removeClass('comment-show-2line');
        $('#div_ServerComment').addClass('comment-show');
        $('a.a_systemMessageClose').show();
        setTimeout(function () {
            if ($("#lbl_ServerComment").get(0).offsetHeight > 20) {
                //alert($("#lbl_ServerComment").get(0).offsetHeight);
                $('#div_ServerComment').removeClass('comment-show');
                $('#div_ServerComment').addClass('comment-show-2line');
            }
        }, 300);
    }

    function ShowComment3() {
        $(".container_comment").css('display', 'block');
        $("#lbl_ServerComment").show(500);
        $('#div_ServerComment').removeClass('comment-show-2line');
        $('#div_ServerComment').addClass('comment-show');
        $('a.a_systemMessageClose').show();       
    }

</script>
<div class="container_comment">
    <div class="comment" id="div_ServerComment" >
        
        <span id="lbl_ServerComment">
            <asp:Literal ID="lt_ServerComment" runat="server"></asp:Literal>
        </span>
        
        <a class="a_systemMessageClose" href="javascript:RemoveServerComment();" style="display:none;"></a>
       
    </div>    
</div>
