﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Controls_SubNavigator : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetLinks();
        }
    }

    private void SetLinks()
    {
        string pagename =
         System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        
        ScannerTypeControl<HtmlAnchor> a_scanner = new ScannerTypeControl<HtmlAnchor>(this);
        a_scanner.StartTo();
        List<HtmlAnchor> list = a_scanner.GetControls();
        foreach (HtmlAnchor ctr in list)
        {
            if (GetPageName(ctr.HRef).StartsWith(pagename))
                ctr.Attributes.Add("class", "active");
        }
        ScannerTypeControl<HyperLink> hl_scanner = new ScannerTypeControl<HyperLink>(this);
        hl_scanner.StartTo();
        List<HyperLink> list2 = hl_scanner.GetControls();
        foreach (HyperLink ctr in list2)
        {
            if (GetPageName(ctr.NavigateUrl).StartsWith(pagename))
                ctr.CssClass = "active";
        }

    }
   // private void SetLinks(List<Control> list
    string GetPageName(string _url)
    {
        string[] strs = _url.Split('/');
        return strs[strs.Length - 1];
    }

}