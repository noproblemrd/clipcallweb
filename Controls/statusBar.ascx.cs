﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_statusBar : System.Web.UI.UserControl
{
    public string choosePlanStatus="pre";
    public string businessDetailsStatus = "pre";
    public string checkoutStatus = "pre";
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public void setStatuses(string choosePlan, string businessDetails, string checkout)
    {
        Status_Bar.Visible = true;

        if (choosePlan == "pre")
        {
            statusBarChoosePlan.Attributes["class"] = "statusBarChoosePlanOff";
            statusBarChoosePlanDesc.Attributes["class"] = "statusBarDesc statusBarDescPre";

            statusBarChoosePlanNumber.Visible = true;
            statusBarChoosePlanNumberPast.Visible = false;
        }
        else if (choosePlan == "current")
        {
            statusBarChoosePlan.Attributes["class"] = "statusBarChoosePlanOn";
            statusBarChoosePlanDesc.Attributes["class"] = "statusBarDesc statusBarDescPre";

            statusBarChoosePlanNumber.Visible = true;
            statusBarChoosePlanNumberPast.Visible = false;
        }
        else if (choosePlan == "past")
        {
            statusBarChoosePlan.Attributes["class"] = "statusBarChoosePlanOn";
            statusBarChoosePlanDesc.Attributes["class"] = "statusBarDesc statusBarDescPast";

            statusBarChoosePlanNumber.Visible = false;
            statusBarChoosePlanNumberPast.Visible = true;
        }



        if (businessDetails == "pre")
        {
            statusBarBusinessDetails.Attributes["class"] = "statusBarBusinessDetailsOff";
            statusBarBusinessDetailsDesc.Attributes["class"] = "statusBarDesc statusBarDescPre";

            statusBarBusinessDetailsNumber.Visible = true;
            statusBarBusinessDetailsNumberPast.Visible = false;
        }

        else if (businessDetails == "current")
        {
            statusBarBusinessDetails.Attributes["class"] = "statusBarBusinessDetailsOn";
            statusBarBusinessDetailsDesc.Attributes["class"] = "statusBarDesc statusBarDescPre";

            statusBarBusinessDetailsNumber.Visible = true;
            statusBarBusinessDetailsNumberPast.Visible = false;
        }

        else if (businessDetails == "past")
        {
            statusBarBusinessDetails.Attributes["class"] = "statusBarBusinessDetailsOn";
            statusBarBusinessDetailsDesc.Attributes["class"] = "statusBarDesc statusBarDescPast";

            statusBarBusinessDetailsNumber.Visible = false;
            statusBarBusinessDetailsNumberPast.Visible = true;
        }

        if (checkout == "pre")
        {
            statusBarCheckout.Attributes["class"] = "statusBarCheckoutOff";
            statusBarCheckoutDesc.Attributes["class"] = "statusBarDesc statusBarDescPre";

            statusBarCheckoutNumber.Visible = true;
            statusBarCheckoutNumberPast.Visible = false;
        }

        else if (checkout == "current")
        {
            statusBarCheckout.Attributes["class"] = "statusBarCheckoutOn";
            statusBarCheckoutDesc.Attributes["class"] = "statusBarDesc statusBarDescPre";

            statusBarCheckoutNumber.Visible = true;
            statusBarCheckoutNumberPast.Visible = false;
        }

        else if (checkout == "past")
        {
            statusBarCheckout.Attributes["class"] = "statusBarCheckoutOn";
            statusBarCheckoutDesc.Attributes["class"] = "statusBarDesc statusBarDescPast";

            statusBarCheckoutNumber.Visible = false;
            statusBarCheckoutNumberPast.Visible = true;
        }
        
    }
}