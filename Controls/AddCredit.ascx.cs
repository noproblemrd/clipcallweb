﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_AddCredit : System.Web.UI.UserControl
{
    string UrlWebReference;
    Guid SupplierId;
    string NumberFormat;
    public event EventHandler BuyCredit;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadPackages();
        }
    }
    public void SetCreditDetails(string UrlWebReference, Guid supplier_id, string NumberFormat)
    {
        this.UrlWebReference = UrlWebReference;
        this.SupplierId = supplier_id;
        this.NumberFormat = NumberFormat;
    }
    private void LoadPackages()
    {
        List<Package> list = (PackageV == null) ?
            Package.GetPackages(UrlWebReference, SupplierId) :
            PackageV;
        foreach (Package p in list)
        {
            switch (p.Name)
            {
                case ("bronze"): lbl_AmountBronze.Text = string.Format(NumberFormat, p.FromAmount);
                    lbl_BonusBronze.Text = string.Format(NumberFormat, p.BonusAmount);
                    break;
                case ("silver"): lbl_AmountSilver.Text = string.Format(NumberFormat, p.FromAmount);
                    lbl_BonusSilver.Text = string.Format(NumberFormat, p.BonusAmount);
                    break;
                case ("gold"): lbl_AmountGold.Text = string.Format(NumberFormat, p.FromAmount);
                    lbl_BonusGold.Text = string.Format(NumberFormat, p.BonusAmount);
                    break;
            }
        }
        PackageV = list;
    
    }
    protected void lb_BuyBronze_Click(object sender, EventArgs e)
    {
        EventArgsBuyCredit eabc = GetPackageByType("bronze");
        if (BuyCredit != null)
            BuyCredit(this, eabc);
    }
    protected void lb_BuySilver_Click(object sender, EventArgs e)
    {
        EventArgsBuyCredit eabc = GetPackageByType("silver");
        if (BuyCredit != null)
            BuyCredit(this, eabc);
    }
    protected void lb_BuyGold_Click(object sender, EventArgs e)
    {
        EventArgsBuyCredit eabc = GetPackageByType("gold");
        if (BuyCredit != null)
            BuyCredit(this, eabc);
    }
    
    List<Package> PackageV
    {
        get { return (Session["Package"] == null) ? null : (List<Package>)Session["Package"]; }
        set { Session["Package"] = value; }
    }
    EventArgsBuyCredit GetPackageByType(string _type)
    {
        
        foreach (Package p in PackageV)
        {
            if (p.Name == _type)
            {
                EventArgsBuyCredit ev = new EventArgsBuyCredit();
                ev.Amount = p.FromAmount;
                ev.BonusPercent = p.BonusPercent;
                ev.BonusAmount = p.BonusAmount;
                return ev;
            }
        }
        return null;
    }
}

