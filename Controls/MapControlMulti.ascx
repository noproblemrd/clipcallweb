﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MapControlMulti.ascx.cs" Inherits="Controls_MapControlMulti"   ClientIDMode="Static" %>

<asp:HiddenField ID="hf_FullNameAddress" runat="server" />

<div class="inputParent">

<div id="div_map" runat="server" style="display:none;width:869px; height:226px;" class="div_map" >
    
    <div id="map_canvas" style="width:100%; height:100%;"></div>    
    
    
     <div id="lbl_result" ></div>
     <div id="div_places" style="display:none;" class="localization" runat="server">
        <asp:HiddenField ID="hf_LargeArea" runat="server" />
     
        <div class="topGreenBox">
            <div class="handleGreen"></div>
        </div>
        <div class="greenBox">  
                                     
            <table class="TableLargeArea" id="table_large_area">
            <thead>
            <tr>
            <th colspan="4">
                    <span>
                        Extended area I cover
                </span>
            </th>
            </tr>
            </thead>
            <tr>
            <td>
                <a href="javascript:void(0);" id="a_Region3" runat="server" onclick="_SetLargeArea(this);"></a>
            </td>
            <td>
            
                <a href="javascript:void(0);" id="a_Region2" runat="server" onclick="_SetLargeArea(this);"></a>
                
            </td>
            <td>
                <a href="javascript:void(0);" id="a_Region1" runat="server" onclick="_SetLargeArea(this);"></a>
            </td>
            <td>
                <a href="javascript:void(0);" id="a_Region0" onclick="_SetLargeArea(this);" _id="all" runat="server" title="USA">USA</a>
            </td>
                
            </tr>
            </table>

        </div>
    </div>

     


    <asp:HiddenField ID="hf_SavedRegionId" runat="server" />
</div>

<div class="mapTitle">
    <span class="titleAddress">Address</span>
    <span class="titleCoverArea">Cover area</span>
    <span class="titleCoverZipCode">Zip codes</span>
</div>
    <asp:ScriptManager ID="ScriptManager1" runat="server" >
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatePanel1" runat="server"  UpdateMode="Conditional">
        <ContentTemplate>
        <script type="text/javascript">
            var arrPlaces = new Array();

            var _navigator = navigator.appVersion.toLowerCase();

            
            if (document.addEventListener) {                
                document.addEventListener("DOMContentLoaded", SetDetailsLocationOnload, false);
            }
            else if (window.attachEvent) {                
                window.attachEvent('onload', SetDetailsLocationOnload);
            }           


            /*
            function SetDetailsLocationOnload() {
            location_place_holder = "<%# LocationPlaceHplder %>";
            var searchTextField = document.getElementById("searchTextField" + index);
            //     $(searchTextField).attr("place_holder", location_place_holder);
            if (searchTextField.addEventListener) {
            searchTextField.addEventListener("blur", ChkLocationOnBlur, false);
            searchTextField.addEventListener("focus", ChkLocationOnFocus, false);
            }
            else if (searchTextField.attachEvent) {
            searchTextField.attachEvent("onblur", ChkLocationOnBlur);
            searchTextField.attachEvent("onfocus", ChkLocationOnFocus);
            }
            }
            */


            function SetDetailsLocationOnload() {
                
                location_place_holder = "<%# LocationPlaceHplder %>";

                $('input[id^="searchTextField"]').each(function () { // all searchTextField's
                    
                    if (this.addEventListener) {
                        this.addEventListener("blur", ChkLocationOnBlur, false);
                        this.addEventListener("focus", ChkLocationOnFocus, false);
                    }
                    else if (this.attachEvent) {
                        this.attachEvent("onblur", ChkLocationOnBlur);
                        this.attachEvent("onfocus", ChkLocationOnFocus);
                    }
                }
        )


            }



            function _SetServerComment(_comment) {

                if (top.SetServerCommentMaster)
                    top.SetServerCommentMaster(_comment);
                else {
                    SetServerComment(_comment);
                    window.scrollTo(0, 0);
                }
            }

            /*
            function SetFullAddress(_address) {
            var searchTextField = document.getElementById('searchTextField1');
            searchTextField.value = _address;
            RemoveCssClass(searchTextField, "place-holder");
            AddCssClass(searchTextField, "textValid");
            full_name = _address;
            document.getElementById("<%#  hf_FullNameAddress.ClientID %>").value = _address + ";;;" + _address;
            }
            */


            function SetFullAddress(_address, index) {

                var searchTextField = document.getElementById('searchTextField' + index);
                searchTextField.value = _address;
                RemoveCssClass(searchTextField, "place-holder");
                AddCssClass(searchTextField, "textValid");

                var objPlace = new Object();
                full_name = _address;
                objPlace.full_name = full_name;
                arrPlaces[index - 1] = objPlace;

                document.getElementById("<%#  hf_FullNameAddress.ClientID %>").value = _address + ";;;" + _address;
            }


            function SetMapOnLoad(radius, _lat, _lng, index) {
                
                lat_lng = new google.maps.LatLng(_lat, _lng);

                _radius = parseFloat(radius);
                _radius = radius;

                arrPlaces[index - 1].lat_lng = lat_lng;
                arrPlaces[index - 1].radius = _radius;

                if (index == 1)
                    SetMap();
            }

            var _zoom; // = 13;
            var _radius = 30;
            var lat_lng;
            var full_name;

            

            function initializeMap(index) {
               
                var options = {
                    //types: ['(cities)'],
                    //componentRestrictions: { country: 'Usa' }
                };

                var input = document.getElementById('searchTextField' + index);
                var autocomplete;
                autocomplete = new google.maps.places.Autocomplete(input, options);

                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    
                    //          var test = this;
                    var place = autocomplete.getPlace();
                    //    var latLng = new google.maps.LatLng(-87.7506440644, 41.8834526307);
                    full_name = place.formatted_address;

                    var objPlace = new Object();
                    objPlace.full_name = full_name;

                    if (!place.geometry)
                        return;
                    lat_lng = place.geometry.location;
                    objPlace.lat_lng = lat_lng;

                    document.getElementById("<%#  hf_FullNameAddress.ClientID %>").value = place.formatted_address + ";;;" + input.value;
                    document.getElementById("div_LocationError" + index).style.display = "none";
                    document.getElementById("div_LocationSmile" + index).style.display = "block";

                    SlideElement("div_LocationComment" + index, false); // document.getElementById("div_LocationComment").style.display = "none";
                    RemoveCssClass(input, "textError");
                    AddCssClass(input, "textValid");
                    RemoveServerComment();

                    //Set default values of map
                    _radius = 30;
                    objPlace.radius = _radius;

                    arrPlaces[index - 1] = objPlace;


                    document.getElementById("<%# div_places.ClientID %>").style.display = "none";

                    SetAreas(index);

                    SetMap();

                });

            }

            function initializeMulti() {
                //alert("initializeMulti now ");               
                for (i = 0; i < arguments.length; i++) {
                    initializeMap(arguments[i]);
                }
            }

            // Register an event listener to fire when the page finishes loading.
            //google.maps.event.addDomListener(window, 'load', initializeMulti);

            /*
            function SetNeighborhood(lat, lng) {
            var params = "lat=" + lat + "&lng=" + lng + "&radius=" + _radius;
            var url = "<# GetNeighborhood %>";
            CallWebService(params, url, function (arg) {
            document.getElementById("lbl_result").innerHTML = arg;
            },
            function () { alert('error'); });
            }
            */
            var map;
            var marker;
            var circle;
            var geocoder = new google.maps.Geocoder();
            function SetMap() {

                SetZoom();                
                document.getElementById("div_map").style.display = "block";
                var options = {
                    'zoom': _zoom,
                    'scrollwheel': false,
                    'center': lat_lng,
                    'draggable': false,

                    //    'maxZoom': _zoom,
                    //    'minZoom': _zoom,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP
                   
                };
                //       SetNeighborhood(lat_lng.lat(), lat_lng.lng());
                map = new google.maps.Map(document.getElementById('map_canvas'),
            options);

                // Create a draggable marker which will later on be binded to a
                // Circle overlay.
                //   map.addControl(new google.maps.LocalSearch());

                marker = new google.maps.Marker({
                    map: map,
                    position: lat_lng, // place.geometry.location, //new google.maps.LatLng(55, 0),
                    draggable: false,
                    title: 'My business location'
                });

                // Add a Circle overlay to the map.
                
                if (_radius <= 175) {
                    circle = new google.maps.Circle({
                        map: map,
                        radius: _radius * 1000 * 1.6, // 1.6km = 1mile,
                        fillColor: "green",
                        strokeOpacity: 0
                        
                    });
                }

                else if (_radius=="ALL=ALL") {
                    circle = new google.maps.Circle({
                        map: map,
                        radius: 175 * 1000 * 1.6, // 1.6km = 1mile,
                        fillColor: "green",
                        strokeOpacity: 0

                    });
                }

                else {
                    circle = new google.maps.Circle();
                    circle.setMap(null);
                }

                // Since Circle and Marker both extend MVCObject, you can bind them
                // together using MVCObject's bindTo() method.  Here, we're binding
                // the Circle's center to the Marker's position.
                // http://code.google.com/apis/maps/documentation/v3/reference.html#MVCObject
                circle.bindTo('center', marker, 'position');
                /**********  Bound zip code area  *************/
                /*
                var encodedPoints = "e|uxFdmdbM_XcGScBjMgm@hGo[tE_WrDgOz@kHz@_D~Hw`@fJgc@vLon@RwBvBwLrDoPjHo_@jCkMvBkMf@oAz@gEz@kHiIqJuWiIz@wBgTsNkCcBRsISwBzOrNz@wBnAcGf@oAwBcBz@{JvBsIbBkHz@cBjCSvBsSbGwBjM_DrNwB~McBvGwBz@{EzJ_g@zEoU~C_Sf@ScGsDrDgTnFoZz@sI~Hod@vBcQbBgJfO{|@pMoy@vNzF`z@jCdaAnSdZoCrMuSvZcObPObM^rTqE`GpG`D`GqFpHeXtPg[rPe]qHwZbPn@pHdTtT`YhMgH`m@hPxc@pgAhPhXiAtb@wSri@am@lQ{k@h`CybBiAi`@oa@io@OKxsBgrBz@cBzJoPnTu`@fFeL~Uac@jRc^zBoD`GwL|Oi\\~@}@nPiZfGeLpDwGfRw^dKuOjHgOlAmB~@mBpb@oDhJQfJa@dLy@dJ?j@\\xHjDrD`DvBdApAr@~ClCvLvG`Bx@jMbIrEfCvDzBlChQI|E`AdMpHli@H~@x@~Ch@jCPlCh@tCR~BlAnHPv@h@fBd@Ph@pDlFlMjHgCbGeB`NiCld@kI`DyAnA?bUX~NaAbB?fGQvEi@nTjBnB`@zc@Mh\\c@tIwCxE`B|GHjAChOLxE~@jR|Gd@?zO|@|Fb@fLn@`D?`Db@xBRvJmDbQBjNgGd^eEjImAdFvSl@tKfAnCpAdSvBhGdK|_@zEg@bOjCfCNpb@uDjIcAt@lMoLgGyBjD~AzLhTfOhn@h{BzQ~@xQ}@v`@_DzAyKdTeXu@uNxIwM_DeG}HyYIsIr@cBtAyG`@e@hEkC`LgGxBXpKcL`@{EfJ{KfJ{AvUjDxC}@zG`A|IcI~C{@Idg@MtFvGnSbU|[`IdKiYcEe@zeCje@lfEhm@hkD~dBbuGdaBpzIe~@Ic^{Ia\\nQwVlv@a\\ja@xQ`aGu@|aA{Lpj@oIvQwU`Eaa@wb@ek@ob@u^pIm^vn@wU`hCet@bi@miArPmkAoXwy@ef@kmAeuAmzBtLoU~T_\\fDsg@kReKeSsf@qIm_@~OiNj^koBmTmuDyl@wj@w[{cJ{_Gwe@s]{bCox@}_Asb@{dBw~@cm@g\\{m@oZktCcy@";
                var  _points = [];
                var decodedPoints = google.maps.geometry.encoding.decodePath(encodedPoints);
                for (var i = 0; i < decodedPoints.length; i++) {
                _points.push(new google.maps.LatLng(decodedPoints[i].lat(), decodedPoints[i].lng()));
                }
            
                var polyYea = new google.maps.Polygon({
                paths: _points,
                fillColor: '#444444',
                fillOpacity: 0.35
                });
                polyYea.setMap(map);
        
                var encodedPoints2 = "_gxwFlsacMoAgEsIcVvG_N_DgJ{O{@kRoPrv@ce@~HsNnFoP~C_NrIoAbQcLz@oAvBsIz@oF~HrD~MnFzEzEjCnAbBnAnK~CzESzEnFjCbBfEg@vBz@~H{Ef@g@R{@oAoKSgJzEgTd^c|AbQpCjoBlTeGlvAj|A`nAreAzsBl`AbmAuh@f_BwG~CkCf@{OzEgOfEg@RsNvBwLf@{J~Cg@?aFhLkA~De@TuYxu@uKaJol@tY_s@}qAgm@k\\{EcBsl@wVcBoAgT{^sIce@cBwGsNk\\{Y{J_SzTcGoFcBsIkMrDgTsI{Tgc@";
                var _points2 = [];
                var decodedPoints2 = google.maps.geometry.encoding.decodePath(encodedPoints2);
                for (var i = 0; i < decodedPoints2.length; i++) {
                _points2.push(new google.maps.LatLng(decodedPoints2[i].lat(), decodedPoints2[i].lng()));
                }
             
                var polyYea2 = new google.maps.Polygon({
                paths: _points2,
                fillColor: '#444444',
                fillOpacity: 0.35
                });
                polyYea2.setMap(map);
                */
            }
            function SetMapZoom() {
                SetZoom();
                map.setZoom(_zoom);


                // Add a Circle overlay to the map.
                if (_radius <= 175) {
                    circle.setRadius(_radius * 1000 * 1.6);
                    circle.setMap(map);
                }
                else
                    circle.setMap(null);


            }
            function SetZoom() {
               
                if (_radius < 2)
                    _zoom = 13;
                else if (_radius < 5)
                    _zoom = 12;
                else if (_radius < 10)
                    _zoom = 11;
                else if (_radius < 20)
                    _zoom = 10;                
                else if (_radius <= 50)
                    _zoom = 8;
                else if (_radius <= 125)
                    _zoom = 6;                
                else if (_radius <= 175)
                    _zoom = 5;
                else if (_radius=="ALL=ALL") // NATIONWIDE
                    _zoom =3;
                else // GENERAL AREAS CITY STATE
                    _zoom = 10;
                
                
                //alert(_zoom);
            }

            function SetPlaces(_id, IsChoosen) {
                var hid_places = document.getElementById('hid_places');
                if (IsChoosen)
                    hid_places.value += _id + ',';
                else
                    hid_places.value = hid_places.value.replace(_id + ',', '');
            }

            /*location*/
            var location_place_holder;
            function ChkLocationOnFocus() {
                var callerElement;
                if (this == window)
                    callerElement = window.event.srcElement;
                else
                    callerElement = this; // window.event.target || window.event.srcElement;
                Text_Focus(callerElement, location_place_holder);
                RemoveCssClass(callerElement, "textValid");
            }

            function ChkLocationOnBlur() {
                
                var callerElement;
                if (this == window) {
                    callerElement = window.event.srcElement;
                    if (callerElement.value == "Enter a location")
                        callerElement.value = "";
                }
                else
                    callerElement = this;

                var index = callerElement.id.charAt(callerElement.id.length - 1);

                document.getElementById("div_LocationSmile" + index).style.width = '29px';
                document.getElementById("div_LocationSmile" + index).style.height = '29px';

                RemoveCssClass(callerElement, "textError");
                var SourceVal = document.getElementById("<%# hf_FullNameAddress.ClientID %>").value.split(';;;');
                var _location = callerElement.value; // GetText_PlaceHolder(callerElement, location_place_holder);
                if (_location.length == 0) {                    
                    RemoveCssClass(callerElement, "textValid");
                    document.getElementById("div_LocationSmile" + index).style.display = "none";
                    document.getElementById("div_LocationError" + index).style.display = "none";
                    SlideElement("div_LocationComment" + index, false);
                    full_name = "";
                    //arrPlaces[index].objPlace.full_name = full_name;
                    document.getElementById("<%# hf_FullNameAddress.ClientID %>").value == "";
                    /*
                    $('#map_canvas').empty();
                    $('#div_map').hide();
                    map = new google.maps.Map(document.getElementById('map_canvas'), null);
                    */
                }
                else if (SourceVal.length == 2 && (_location == SourceVal[0] || _location == SourceVal[1])) {                    
                    document.getElementById("div_LocationSmile" + index).style.display = "block";
                    document.getElementById("div_LocationError" + index).style.display = "none";
                    SlideElement("div_LocationComment" + index, false);
                    AddCssClass(callerElement, "textValid");
                }
                else {                    
                    //      _SetServerComment("<%# SelectFromList %>");
                    document.getElementById("div_LocationError" + index).style.display = "block";
                    document.getElementById("div_LocationSmile" + index).style.display = "none";
                    SlideElement("div_LocationComment" + index, true);
                    AddCssClass(callerElement, "textError");
                }
                // document.getElementById("div_LocationComment").style.display = "none";
            }
            function ChkLocationOnKeyPress(elem, e) {
                var index = elem.id.charAt(elem.id.length - 1);

                document.getElementById("div_LocationError" + index).style.display = "none";
                document.getElementById("div_LocationSmile" + index).style.display = "none";
                if (elem.value.length == 0)
                    SlideElement("div_LocationComment" + index, false);
                else
                    SlideElement("div_LocationComment" + index, true);
                //     document.getElementById("div_LocationComment").style.display = (elem.value.length == 0) ? "none" : "block";
                RemoveCssClass(elem, "textError");

                if (elem.value.length == 0)
                    return;
                var keynum;
                if (window.event) // IE            
                    keynum = e.keyCode
                else if (e.which) // Netscape/Firefox/Opera            
                    keynum = e.which
                if (keynum == 13) {

                    geocoder.geocode({ address: elem.value }, function (response, status) {
                        if (status == google.maps.GeocoderStatus.OK && response[0]) {
                            lat_lng = response[0].geometry.location;
                            _radius = 30;
                            document.getElementById("<%#  hf_FullNameAddress.ClientID %>").value = response[0].formatted_address + ";;;" + elem.value;
                            $('#div_LocationError' + index).hide();
                            $('#div_LocationSmile' + index).show();
                            SlideElement("div_LocationComment" + index, false);
                            full_name = response[0].formatted_address;

                            SetMap();
                        }
                        else {
                            $('#map_canvas').empty();
                            $('#div_map').hide();
                            $('#<%#  hf_FullNameAddress.ClientID %>').val('');
                            full_name = '';
                            map = new google.maps.Map(document.getElementById('map_canvas'), null);
                            document.getElementById("div_LocationError").style.display = "block";
                            SlideElement("div_LocationComment", true);
                        }
                    });

                }


            }

            function SetInvalidLocation(index) {                
                document.getElementById("div_LocationError" + index).style.display = "block";
                document.getElementById("div_LocationSmile" + index).style.display = "none";
                SlideElement("div_LocationComment" + index, true); // document.getElementById("div_LocationComment").style.display = "none";
                var searchTextField = document.getElementById("searchTextField" + index);
                RemoveCssClass(searchTextField, "textValid");
                AddCssClass(searchTextField, "textError");
            }


            function SetAreas(index) {

                
                _radius = 35;

                var url = "<%# GetLargeAreas %>";
                var params = "lat=" + lat_lng.lat() + "&lng=" + lat_lng.lng();
                $('#hf_lat' + index).val(lat_lng.lat());
                $('#hf_lng' + index).val(lat_lng.lng());

                //var chosenCoverArea = $('#ddlCoverArea' + index).val();
                var chosenCoverArea = $('#ddlCoverArea' + index).val();

                CallWebService(params, url,
                    function (arg) {

                        if (arg == "faild") {
                            _SetServerComment("Server faild");
                            return;
                        }
                        var args = eval('(' + arg + ')');



                        $('#ddlCoverArea' + index).empty();

                        $('#ddlCoverArea' + index).prop('disabled', false);


                        var strAreas = "";

                        for (var i = 0; i < args.length; i++) {
                            //SetAreaDetails(args[i]._id, args[i].level, args[i].name, _is_select);                           

                            $('#ddlCoverArea' + index).append($('<option>', {
                                value: args[i]._id + "=" + args[i].level,
                                text: args[i].name
                            }));

                            strAreas += args[i]._id + "=" + args[i].name + "&";

                            if (args[i].level == 3) {
                                $('#lblZipCode' + index).text(args[i].numZipcodes);
                                chosenCoverArea = args[i]._id + "=" + args[i].level;
                            }
                        }


                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "ALL",
                            text: "Worldwide"
                        }));
                        strAreas += "ALL=Worldwide&";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "0.5",
                            text: "0.5mi"
                        }));
                        strAreas += "0.5=0.5mi&";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "1",
                            text: "1mi"
                        }));
                        strAreas += "1=1mi&";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "2",
                            text: "2mi"
                        }));
                        strAreas += "2=2mi&";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "3",
                            text: "3mi"
                        }));
                        strAreas += "3=3mi&";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "5",
                            text: "5mi"
                        }));
                        strAreas += "5=5mi&";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "10",
                            text: "10mi"
                        }));
                        strAreas += "10=10mi&";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "12.5",
                            text: "12.5mi"
                        }));
                        strAreas += "12.5=12.5mi&";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "20",
                            text: "20mi"
                        }));
                        strAreas += "20=20mi&";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "30",
                            text: "30mi"
                        }));
                        strAreas += "30=30mi";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "50",
                            text: "50mi"
                        }));
                        strAreas += "50=50mi";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "75",
                            text: "75mi"
                        }));
                        strAreas += "75=75mi";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "100",
                            text: "100mi"
                        }));
                        strAreas += "100=100mi";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "125",
                            text: "125mi"
                        }));
                        strAreas += "125=125mi";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "150",
                            text: "150mi"
                        }));
                        strAreas += "150=150mi";

                        $('#ddlCoverArea' + index).append($('<option>', {
                            value: "175",
                            text: "175mi"
                        }));
                        strAreas += "175=175mi";                       


                        $('#ddlCoverArea' + index + ' option[value="' + chosenCoverArea + '"]').attr('selected', 'selected');

                        //$('#hf_areas' + index).val(strAreas);
                        $('#hiddenCoverArea' + index).val(strAreas);



                        //alert($('#hf_areas' + index).val());
                    },
                    function () { alert('error'); }
                );
                return true;
            }


            function SetZipCodes(index,regionid,lat,lng,radius) {               

                var url = "<%# GetNumberZipCodes %>";
                //public string getNumberOfZipCode(string selectedArea, decimal lat, decimal lng, decimal radius)
                var params = "selectedArea=" + regionid + "&lat=" + lat + "&lng=" + lng + "&radius=" + radius;

                CallWebService(params, url,
                    function (arg) {                       
                        if (arg == "faild") {
                            _SetServerComment("Server faild");
                            return;
                        }

                        //var args = eval('(' + arg + ')');
                        $('#lblZipCode' + index).text(arg);

                    },
                    function () { alert('error'); }
                );
                return true;
            }

            function SetAreaDetails(_id, level, name, IsSelect) {
                var _a;
                if (level == "1") {
                    _a = document.getElementById("<%# a_Region1.ClientID %>");
                    _a.setAttribute("_id", _id);

                }
                else if (level == "2") {
                    _a = document.getElementById("<%# a_Region2.ClientID %>");
                    _a.setAttribute("_id", _id);
                }
                else if (level == "3") {
                    _a = document.getElementById("<%# a_Region3.ClientID %>");
                    _a.setAttribute("_id", _id);

                }
                if (IsSelect)
                    _SetLargeArea(_a);
                //    _a.addEventListener('click', SetLargeArea, false)

                _a.innerHTML = name;
                $(_a).attr('title', name);
            }
            /*
            function SetLargeArea() {
            _SetLargeArea(this);
            }
            */
            function _SetLargeArea(elem) {

                AddCssClass(elem, "selected");
                var _id = elem.getAttribute("_id");
                document.getElementById("<%# hf_LargeArea.ClientID %>").value = _id;

            }

            function GetFullAddress() {
                if (full_name == null || full_name.length == 0)
                    return "";
                return full_name;
            }
            function GetSelectLargeAreaId() {
                var _id = document.getElementById("<%# hf_LargeArea.ClientID %>").value;
                if (document.getElementById("<%# div_places.ClientID %>").style.display == "none")
                    return "";
                if (_id.length == 0)
                    return "false";
                return _id;
            }

            function GetAllDetails() {
                return "";
                var __FullName = GetFullAddress();
                if (__FullName.length == 0)
                    return "";
                var TempName = document.getElementById("<%#  hf_FullNameAddress.ClientID %>").value.split(';;;');
                var search = GetText_PlaceHolder(document.getElementById('searchTextField'), location_place_holder);
                var IsOk = false;
                for (var i = 0; i < TempName.length; i++) {
                    if (search == TempName[i])
                        IsOk = true;
                }
                if (!IsOk)
                    return "";
                var _details = new Object();
                _details.lat_lng = lat_lng;
                _details.radius = _radius;
                _details.FullAddress = __FullName;
                _details.AreaId = GetSelectLargeAreaId();
                return _details;
            }

            function setMultiDetails(urlSucces, comment, listObj, ifRedirect) {


                $.ajax({
                    type: 'POST',
                    url: '<%#SetDetails%>',
                    data: "{areasCover:" + JSON.stringify(listObj) + "}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    dataFilter: function (data) { // This boils the response string down 
                        //  into a proper JavaScript Object().
                        var msg = eval('(' + data + ')');

                        /*
                        If you aren’t familiar with the “.d” I’m referring to, it is simply a security feature that Microsoft added in ASP.NET 3.5’s version
                        of ASP.NET AJAX. By encapsulating the JSON response within a parent object,
                        the framework helps protect against a particularly nasty XSS vulnerability.

                        */

                        // If the response has a ".d" top-level property,
                        //  return what's below that instead.
                        if (msg.hasOwnProperty('d'))
                            return msg.d;
                        else
                            return msg;
                    },
                    success: function (data) {                     


                        if (data.result == "failed") {
                            SendFaild();
                            SetServerComment(comment);
                        }
                        else {
                            if (ifRedirect)
                                location.href = urlSucces;
                            else
                                SendSucceed();
                        }

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //alert('faild ' + errorThrown);
                        SetServerComment(comment);

                        if(window.console)
                            console.log("failed " + textStatus + " " + errorThrown);
                    }
                });
               
            }




            function GetAllDetailsMulti(urlSuccess,comment,ifRedirect) {               
                var indexArray=0;
                var index;

                var arrAreas = new Array();
                //alert(3);
                var containerObject=new Object();
                containerObject.supplierId = "<%#SupplierId.ToString()%>";
                //alert(4);
                $('input[id^="searchTextField"]').each(function () {

                    var obj = new Object();

                    index = $(this).attr('id').charAt($(this).attr('id').length - 1);

                    obj.order = index;
                    obj.address = $(this).val();

                    obj.latitude = $('#hf_lat' + index).val();
                    obj.longitude = $('#hf_lng' + index).val();

                    obj.radius = $('#ddlCoverArea' + index).val().split('=')[0];

                    obj.numberOfZipcodes = $('#lblZipCode' + index).text();

                    var level;

                    $('select[id^="ddlCoverArea' + index + '"]  option').each(function () {


                        if ($(this).val().indexOf('=') != -1) {
                            levelArray = $(this).val().split('=');
                            level = levelArray[1];
                            if (level == 1) {
                                obj.selectedArea1Name = $(this).text();
                                //alert(obj.selectedArea1Name);
                                obj.SelectedArea1Id = levelArray[0];
                            }

                            else if (level == 2) {
                                obj.selectedArea2Name = $(this).text();
                                obj.SelectedArea2Id = levelArray[0];
                            }

                            else if (level == 3) {
                                obj.selectedArea3Name = $(this).text();
                                obj.SelectedArea3Id = levelArray[0];
                            }
                        }


                    }

                    )


                    arrAreas[indexArray] = obj;
                    indexArray++;
                }

                )

                containerObject.areas = arrAreas;

                setMultiDetails(urlSuccess, comment, containerObject, ifRedirect);

                return "";
            }

           var  arrAreas=new Array();
           

           function CloneToDdlAreas() {
               //alert("CloneToDdlAreas");
               $('select[id^="ddlCoverArea"]').each(function () {

                   for (var i = 0; i < arrAreas.length; i++) {

                       if (this.id == arrAreas[i].id) {
                           $(this).html(arrAreas[i].html);
                           $(this).val(arrAreas[i].selected);
                           $(this).prop('disabled', arrAreas[i].disabled);
                           var index = this.id.charAt(this.id.length - 1);
                           $("#lblZipCode" + index).text(arrAreas[i].zipcode);
                           $('#div_LocationSmile' + index).css({'display': arrAreas[i].smileDispaly,'width':'1px','height':'1px'});
                           $('#div_LocationError' + index).css('display', arrAreas[i].errorDispaly);
                           $('#hf_lat' + index).val(arrAreas[i].lat);                           
                           $('#hf_lng' + index).val(arrAreas[i].lng);
                           
                       }
                   }

               }
               )  
                     
           }

           function CloneFromDdlAreas() {
               //alert("CloneFromDdlAreas");
               var i = 0;
               arrAreas.length = 0;

               $('select[id^="ddlCoverArea"]').each(function () {
                   var obj = new Object();
                   obj.id = this.id;
                   obj.html = $(this).html();
                   obj.selected = $(this).val();
                   obj.disabled = $(this).prop('disabled');


                   var index = this.id.charAt(this.id.length - 1);
                   obj.zipcode = $("#lblZipCode" + index).text();
                   obj.smileDispaly = $('#div_LocationSmile' + index).css('display');
                   obj.errorDispaly = $('#div_LocationError' + index).css('display');
                   obj.lat = $('#hf_lat' + index).val();                   
                   obj.lng = $('#hf_lng' + index).val();
                  

                   arrAreas[i] = obj;
                   i++;
               }
                )
           }


           function emptyDdlAreas() {
                $('#ddlCoverArea1').empty();
           }

           function init() {

                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndHandler);

                //setValidIcons();
            }
            function startRequest(sender, e) {
                
            }
            function EndHandler(sender, e) {
                setDdlCoverAreaEvent();
                CloneToDdlAreas();
                SetDetailsLocationOnload();
            }
          

            function setDdlCoverAreaEvent() {

                $('select[id^="ddlCoverArea"]').change(function () {
                    /* before
                    _radius = $(this).val();
                    SetMapZoom();
                    var index = this.id.charAt(this.id.length - 1);
                    var lat = lat_lng.lat();
                    var lng = lat_lng.lng();

                    if (isNaN(this.value)) {
                    if (this.value.indexOf("=") == -1) // nationwide value:ALL
                    {
                    SetZipCodes(index, this.value, lat, lng, 0);
                    }
                    else {                           
                    SetZipCodes(index, this.value.split('=')[0], lat, lng, 0);
                    }
                    }
                    else
                    SetZipCodes(index, this.value, lat, lng, _radius);

                    }
                    )
                    */

                    //alert(this.value);
                    _radius = $(this).val();
                    var index = this.id.charAt(this.id.length - 1);
                    var regionid = this.value;


                    geocoder.geocode({ address: $("#searchTextField" + index).val() }, function (response, status) {



                        if (status == google.maps.GeocoderStatus.OK && response[0]) {
                            lat_lng = response[0].geometry.location;


                            /*
                            document.getElementById("<#  hf_FullNameAddress.ClientID %>").value = response[0].formatted_address + ";;;" + elem.value;
                            $('#div_LocationError' + index).hide();
                            $('#div_LocationSmile' + index).show();
                            SlideElement("div_LocationComment" + index, false);
                            full_name = response[0].formatted_address;
                            */

                            SetMap();

                            var lat = lat_lng.lat();
                            var lng = lat_lng.lng();

                            if (isNaN(regionid)) {
                                if (regionid.indexOf("=") == -1) // nationwide value:ALL
                                {
                                    SetZipCodes(index, regionid, lat, lng, 0);
                                }
                                else {
                                    SetZipCodes(index, regionid.split('=')[0], lat, lng, 0);
                                }
                            }
                            else
                                SetZipCodes(index, regionid, lat, lng, _radius);

                        }
                        else {

                        }
                    });
                }
                )
            }

            $(document).ready(function () {
                init();
                setDdlCoverAreaEvent();



            }
    )

            function pageLoad() {
               
            }
</script>
            <asp:Panel ID="pnlAddLocation" CssClass="pnlAddLocation" runat="server">

 <%--                        
<div class="inputContainer">           
    
    <input id="searchTextField1" type="text" size="50"   class="textActive" place_holder="<%# LocationPlaceHplder %>"
    onkeyup="ChkLocationOnKeyPress(this, event);" onkeydown="return (event.keyCode!=13);" HolderClass="place-holder"  />
   

    <div class="inputValid" style="display:none;"  id="div_LocationSmile1">
                
    </div>
    <div class="inputError" style="display:none;"  id="div_LocationError1">
               
    </div>
    <div class="inputComment" style="display:none;" id="div_LocationComment1">
            <%= LocationPlaceHplder %>
        <div class="chat-bubble-arrow-border"></div>
        <div class="chat-bubble-arrow"></div>
    </div>

    <asp:DropDownList runat="server" ID="ddlCoverArea1" class="coverArea">
        <asp:ListItem>0.5mi</asp:ListItem>
        <asp:ListItem>1mi</asp:ListItem>
        <asp:ListItem>2mi</asp:ListItem>
        <asp:ListItem>3mi</asp:ListItem>
        <asp:ListItem>10mi</asp:ListItem>
        <asp:ListItem>20mi</asp:ListItem>
        <asp:ListItem>30mi</asp:ListItem>

    </asp:DropDownList>
    
    <asp:Label runat="server" ID="lblZipCode" class="numberZipCodes">100</asp:Label>
   <div class="clear"></div>
    
</div>


<div class="inputContainer">        
    
    <input id="searchTextField2" type="text" size="50"   class="textActive" place_holder="<%# LocationPlaceHplder %>"
    onkeyup="ChkLocationOnKeyPress(this, event);" onkeydown="return (event.keyCode!=13);" HolderClass="place-holder"  />
   

    <div class="inputValid" style="display:none;"  id="div_LocationSmile2">
                
    </div>
    <div class="inputError" style="display:none;"  id="div_LocationError2">
               
    </div>
    <div class="inputComment" style="display:none;" id="div_LocationComment2">
            <%= LocationPlaceHplder %>
        <div class="chat-bubble-arrow-border"></div>
        <div class="chat-bubble-arrow"></div>
    </div>

    <asp:DropDownList runat="server" ID="ddlCoverArea2" class="coverArea">
        <asp:ListItem>0.5mi</asp:ListItem>
        <asp:ListItem>1mi</asp:ListItem>
        <asp:ListItem>2mi</asp:ListItem>
        <asp:ListItem>3mi</asp:ListItem>
        <asp:ListItem>10mi</asp:ListItem>
        <asp:ListItem>20mi</asp:ListItem>
        <asp:ListItem>30mi</asp:ListItem>

    </asp:DropDownList>
    
    <asp:Label runat="server" ID="Label3" class="numberZipCodes">100</asp:Label>

   <a href="" id="minus4">
    <i class="fa fa-times fa-2"></i>
   </a>
    
   <div class="clear"></div>
    
    
</div>


<div class="inputContainer">        
    
    <input id="Text1" type="text" size="50"   class="textActive" place_holder="<%# LocationPlaceHplder %>"
    onkeyup="ChkLocationOnKeyPress(this, event);" onkeydown="return (event.keyCode!=13);" HolderClass="place-holder"  />
   

    <div class="inputValid" style="display:none;"  id="div1">
                
    </div>
    <div class="inputError" style="display:none;"  id="div2">
               
    </div>
    <div class="inputComment" style="display:none;" id="div3">
            <%= LocationPlaceHplder %>
        <div class="chat-bubble-arrow-border"></div>
        <div class="chat-bubble-arrow"></div>
    </div>

    <asp:DropDownList runat="server" ID="DropDownList1" class="coverArea">
        <asp:ListItem>0.5mi</asp:ListItem>
        <asp:ListItem>1mi</asp:ListItem>
        <asp:ListItem>2mi</asp:ListItem>
        <asp:ListItem>3mi</asp:ListItem>
        <asp:ListItem>10mi</asp:ListItem>
        <asp:ListItem>20mi</asp:ListItem>
        <asp:ListItem>30mi</asp:ListItem>

    </asp:DropDownList>
   
   <asp:Label runat="server" ID="Label4" class="numberZipCodes">100</asp:Label>

   <a href="" id="A1">
    <i class="fa fa-times fa-2"></i>
   </a>
    
   <div class="clear"></div>
    
    
</div>
--%>
</asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
 

</div>


    
    
   

<div class="newCategory">
      <asp:LinkButton ID="lbAddButton" runat="server" onclick="lbAddButton_Click"  OnClientClick="CloneFromDdlAreas();emptyDdlAreas();">+ New location</asp:LinkButton>
</div>  