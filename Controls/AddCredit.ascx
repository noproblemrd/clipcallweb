﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddCredit.ascx.cs" Inherits="Controls_AddCredit" %>

<div id="parentPackages" class="parentPackages">
            
    <div class="div-packages">
        <div class="div_plain">
            
                <div class="plan-money">
                    <span class="plan-currency">$</span>
                    <asp:Label ID="lbl_AmountBronze" runat="server" CssClass="Amount">50</asp:Label>
                </div>
                <div class="clear"></div>
                <div class="bonus-container">
                    <span class="currency">$</span>
                    <asp:Label ID="lbl_BonusBronze" runat="server" CssClass="bonus">4</asp:Label>
                    <span class="free-bonus">FREE BONUS</span>
                </div>
                <div class="clear"></div>
            <asp:LinkButton ID="lb_BuyBronze" runat="server" CssClass="BuyNowSmall" 
                    onclick="lb_BuyBronze_Click">BUY NOW</asp:LinkButton>
            
        </div>
        <div class="div_plain SilverMiddel">
             <div class="plan-money">
                <span class="plan-currency">$</span>
                <asp:Label ID="lbl_AmountSilver" runat="server" CssClass="Amount">50</asp:Label>
            </div>
            <div class="clear"></div>
            <div class="bonus-container">
                <span class="currency">$</span>
                <asp:Label ID="lbl_BonusSilver" runat="server" CssClass="bonus">4</asp:Label>
                <span class="free-bonus">FREE BONUS</span>
            </div>
            <div class="clear"></div>
            <asp:LinkButton ID="lb_BuySilver" runat="server" CssClass="BuyNowLarge" 
                 onclick="lb_BuySilver_Click">BUY NOW</asp:LinkButton>
        </div>
        <div class="div_plain">
             <div class="plan-money">
                <span class="plan-currency">$</span>
                <asp:Label ID="lbl_AmountGold" runat="server" CssClass="Amount">50</asp:Label>
            </div>
            <div class="clear"></div>
            <div class="bonus-container bonus-container-gold">
                <span class="currency">$</span>
                <asp:Label ID="lbl_BonusGold" runat="server" CssClass="bonus">4</asp:Label>
                <span class="free-bonus">FREE BONUS</span>
            </div>
            <div class="clear"></div>
            <asp:LinkButton ID="lb_BuyGold" runat="server" CssClass="BuyNowSmall" 
                 onclick="lb_BuyGold_Click">BUY NOW</asp:LinkButton>
        </div>
    </div>
            

</div>
