using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;

public partial class FromToDate : System.Web.UI.UserControl
{
    public event System.EventHandler ReportExec;
    const string DATE_FORMAT = "{0:MM/dd/yyyy}";
    bool AllredySetScript = false;
    PageSetting _ps;
    protected void Page_Load(object sender, EventArgs e)
    {      
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        ServiceReference sr = new ServiceReference(ResolveUrl("~") + "WebServiceSite.asmx");
        if (!sm.Services.Contains(sr))
            sm.Services.Add(sr);
        ScriptReference s_r = new ScriptReference(ResolveUrl("~") + "Calender/_Calender.js");
        if (!sm.Scripts.Contains(s_r))
            sm.Scripts.Add(s_r);

        if (!Page.GetType().IsSubclassOf(typeof(PageSetting)))
        {
            Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            return;
        }
        _ps = (PageSetting)Page;
        List<string> cssClasses = new List<string>();
        if (_ps.userManagement.IsPublisher())
        {
     //       cssClasses.Add(@"../FromToDate.css");
            cssClasses.Add(ResolveUrl("~")+@"Calender/CalenderDiv.css");
        }
        foreach (string cssClass in cssClasses)
        {
            if (Utilities.HasStyleSheetLoaded(Page, cssClass) == false)
            {
                System.Diagnostics.Debug.WriteLine("Adding style");
                // Add the style sheet
                Literal css = new Literal();
                css.Text = "<link href=\"" + cssClass + "\" rel=\"Stylesheet\" type=\"text/css\" />\n";
                Page.Header.Controls.Add(css);
            }
        }
        
  //      ScriptManager sm = (ScriptManager)Page.Page.Master.FindControl("ScriptManager1");
        if (!IsPostBack)
        {               
            if (_ps.siteSetting != null && !string.IsNullOrEmpty(_ps.siteSetting.GetSiteID))
                getTranslate(_ps.siteSetting.siteLangId);
             
            setDateFormat(_ps.siteSetting.DateFormat);
            if (!AllredySetScript)            
                SetClientScript();
            /*
            PageSetting ps = (PageSetting)Page;
            DateTime date = DateTime.Now;
            form_to.Text = string.Format(ps.siteSetting.DateFormat, date);
            hf_DateNow.Value = string.Format(ps.siteSetting.DateFormat, date);
            date = date.AddMonths(-1);
            form_from.Text = string.Format(ps.siteSetting.DateFormat, date);
             * */
        }
        this.DataBind();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "DefaultTime", "DefaultTime();", true);
        
        //dbug
        /*
        string Lang = "he-IL";//set your culture here
        System.Threading.Thread.CurrentThread.CurrentCulture =
            new System.Globalization.CultureInfo(Lang);
         * */
    }
    public void SetDateInterval(int days)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "SetDateDayDifference", "SetDateDayDifference(" + days + ");", true);
    }
    public void SetDateIntervalInServer(int days)
    {
        PageSetting _page = (PageSetting)Page;
        DateTime _date = DateTime.Now;
        form_to.Text = string.Format(_page.siteSetting.DateFormat, _date);
        _date = _date.AddDays(-1 * days);
        form_from.Text = string.Format(_page.siteSetting.DateFormat, _date);
    }
    public void SetDateIntervalInServer(DateTime from, DateTime to = default(DateTime))
    {
        PageSetting _page = (PageSetting)Page;
        if (to == default(DateTime))
            to = DateTime.Now;
        form_to.Text = string.Format(_page.siteSetting.DateFormat, to);
        form_from.Text = string.Format(_page.siteSetting.DateFormat, from);
    }
    private void SetClientScript()
    {
        btnSubmit.OnClientClick = GetDateTimeValidation();
        AllredySetScript = true;
    }
    public void SetClientScript(string _reg, string txtId)
    {
    //    string _script = "return ChkValidation('" + _reg + "', '" + txtId + "');";
        string _script = "if (!ChkValidation('" + _reg + "', '" + txtId + "')) return false;";
        btnSubmit.OnClientClick = GetDateTimeValidation() + _script;
        AllredySetScript = true;
    }
    string GetDateTimeValidation()
    {
        return "if (!validateDates()) return false;";
    }
    private void setDateFormat(string DateFormat)
    {
        if (string.IsNullOrEmpty(DateFormat))
            DateFormat = "{0:MM/dd/yyyy}";
        string FormatDisplay = ConvertToDateTime.GetDateFormatDisplay(DateFormat);
        CalendarExtender1.Format = FormatDisplay;
        CalendarExtender2.Format = FormatDisplay;
        
        TextBoxWatermarkExtender1.WatermarkText = FormatDisplay;
        TextBoxWatermarkExtender2.WatermarkText = FormatDisplay;
        Hidden_DteFormat.Value = FormatDisplay;
    }
    private void getTranslate(int siteLangId)
    {
        string pagename = "FromToDate.ascx";
        DBConnection.LoadTranslateToControl(this, pagename, siteLangId);
    }
    public DateTime GetDateFrom
    {
        get
        {

            if (string.IsNullOrEmpty(form_from.Text))
                return DateTime.MinValue;
            return ConvertToDateTime.CalanderToDateTime(form_from.Text, ((PageSetting)Page).siteSetting.DateFormat);
        }
        set
        {
            form_from.Text = string.Format(((PageSetting)Page).siteSetting.DateFormat, value);
        }
    }
    public DateTime GetDateTo
    {
        get
        {
            if (string.IsNullOrEmpty(form_to.Text))
                return DateTime.MinValue;
            return ConvertToDateTime.CalanderToDateTime(form_to.Text, ((PageSetting)Page).siteSetting.DateFormat);
        }
        
    }
    public string GetDateError
    {
        get
        {
            return lbl_dateError.Text;
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (this.ReportExec != null)
            this.ReportExec(this, EventArgs.Empty);
    }
    public Button GetBtnSubmit()
    {
        return btnSubmit;
    }
    public void SetProgress()
    {
        string _script = @"$('.CreateReportSubmit2').click(function() {
              showDiv();
            });";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "show_Div", _script, true); 
    }
    public void RemoveButton()
    {
        btnSubmit.Visible = false;
    }
    public string GetWrongDateFormat()
    {
        return lbl_wrongDate.Value + " " + ((PageSetting)Page).siteSetting.DateFormatClean;
    }
    public string SetButtonText
    {
        set { btnSubmit.Text = value; }
    }
    
}
