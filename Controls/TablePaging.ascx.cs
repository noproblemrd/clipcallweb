﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class TablePaging : System.Web.UI.UserControl
{
    int ITEM_PAGE = 20;
    int PAGE_PAGES = 10;
    
    int Count_Pages
    {
        get { return (ViewState["Count_Pages"] == null) ? 0 : (int)ViewState["Count_Pages"]; }
        set { ViewState["Count_Pages"] = value; }
    }
   
    public event System.EventHandler _lnkPage_Click;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            
            LoadPagesList();
        }
        else
        {
            LoadTranslate();
            if (Page is IOnLoadControl)
                ((IOnLoadControl)Page).__OnLoad(this);
        }
            }
    
    
    protected void LoadTranslate()
    {
        DBConnection.LoadTranslateToControl(this, "TablePaging.ascx", ((PageSetting)Page).siteSetting.siteLangId);
    }
    void LoadPagesList()
    {
        Dictionary<int, string> dic = PageListV;
        PlaceHolderPages.Controls.Clear();
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell(kvp.Value, kvp.Key));
    }
    
    public void LoadPages()
    {
        if (Count_Pages < 2)
        {
            div_paging.Visible = false;
            PageListV = null;
            return;
        }
        div_paging.Visible = true;
        lnkPreviousPage.Enabled = (CurrentPage != 1);
        lnkNextPage.Enabled = (CurrentPage != Count_Pages);
        
        Dictionary<int, string> dic = new Dictionary<int, string>();
    //    List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= Count_Pages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= Count_Pages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListV = dic;
        LoadPagesList();

    }
    HtmlGenericControl MakeCell(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
     //   HtmlTableCell htc = new HtmlTableCell();
        HtmlGenericControl htc = new HtmlGenericControl("li");
        htc.ID = "li_" + indx;
        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }

    int CurrentPage
    {
        get
        {
            return (ViewState["pageIndex"] == null) ? 0 : (int)ViewState["pageIndex"];            
        }
        set
        {
            ViewState["pageIndex"] = value;
        }
    }

    protected void lnkNextPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;
        EventArgsPageIndex _event = new EventArgsPageIndex(CurrentPage, ITEM_PAGE);
 //       LoadPages(Count_Pages);
        if (this._lnkPage_Click != null)
            this._lnkPage_Click(this, _event);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;
        EventArgsPageIndex _event = new EventArgsPageIndex(CurrentPage, ITEM_PAGE);
  //      LoadPages(Count_Pages);
        if (this._lnkPage_Click != null)
            this._lnkPage_Click(this, _event);
    }

    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp;
        EventArgsPageIndex _event = new EventArgsPageIndex(CurrentPage, ITEM_PAGE);
        if (this._lnkPage_Click != null)
            this._lnkPage_Click(this, _event);
    }

    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }
    public int Current_Page
    {
        get { return CurrentPage; }
        set { CurrentPage = value; }
    }
    public int Pages_Count
    {
        set { Count_Pages = value; }
    }
    public int ItemInPage
    {
        set { ITEM_PAGE = value; }
        get { return ITEM_PAGE; }
    }
    public int PagesInPaging
    {
        set { PAGE_PAGES = value; }
    }
    public void InitialPages(int rows)
    {
        Count_Pages = (rows / ITEM_PAGE) + 1;
        CurrentPage = 1;
        LoadPages();
    }
    //dbug
    public PlaceHolder phV
    {
        get { return PlaceHolderPages; }
    }

}