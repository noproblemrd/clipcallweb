﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


public partial class Controls_MasterPageNPPartner : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetLinks();
            Master.SetTitle1 = "We're happy";
            Master.SetTitle2 = "to share our success.";
        }
    }
    private void SetLinks()
    {
        string pagename =
         System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);

        ScannerTypeControl<HtmlAnchor> a_scanner = new ScannerTypeControl<HtmlAnchor>(this);
        a_scanner.StartTo();
        List<HtmlAnchor> list = a_scanner.GetControls();
        foreach (HtmlAnchor ctr in list)
        {
            if (GetPageName(ctr.HRef).StartsWith(pagename))
                ctr.Attributes.Add("class", "active");
        }
        

    }
    // private void SetLinks(List<Control> list
    string GetPageName(string _url)
    {
        string[] strs = _url.Split('/');
        return strs[strs.Length - 1];
    }
}
