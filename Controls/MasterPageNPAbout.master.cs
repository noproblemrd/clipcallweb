﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_MasterPageNPAbout : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Master.SetTitle1 = "We're more than just another";
            Master.SetTitle2 = "pay-per-call platform";
        }
    }
    public string SetTitle1
    {
        set { Master.SetTitle1 = value; }
    }
    public string SetTitle2
    {
        set { Master.SetTitle2 = value; }
    }
}
