﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Controls_AdvertiserNavigationl : System.Web.UI.UserControl
{
    string pageName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            /*
            PageSetting ps = (PageSetting)Page;
            if (!ps.userManagement.Is_CompleteRegistration)
            {
                linkProfessionalUpdate.HRef = "javascript:void(0)";
                ps.ClientScript.RegisterStartupScript(this.GetType(), "setDisableClass", "SetDisableClass();", true);
                linkCalls.HRef = "javascript:void(0)";
                ps.ClientScript.RegisterStartupScript(this.GetType(), "SetDisableClassMyCall", "SetDisableClassMyCall();", true);
            }
            else if (!ps.siteSetting.HasMiniSite)
            {
                linkProfessionalUpdate.HRef = "";
                ps.ClientScript.RegisterStartupScript(this.GetType(), "setDisableClass", "SetDisableClass();", true);
            }
             * */
            SetMenu();
        }
        this.DataBind();
    }
    private void SetMenu()
    {
        pageName =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);


        if (IsPageMatch(a_frameAspx))
        {
            a_frameAspx.Attributes.Add("class", "btn-myaccount active");
            btn_myAccount.Attributes.Add("class", "_btn_myaccount active");


        }


        else if (IsPageMatch(linkProfessionalUpdate))
        {
            linkProfessionalUpdate.Attributes.Add("class", "btn-myminisite active");
            btn_myminisite.Attributes.Add("class", "_btn_myminisite active");
        }

        else if (IsPageMatch(linkSetting))
        {
            linkSetting.Attributes.Add("class", "btn-faq active");
            btn_faq.Attributes.Add("class", "_btn_faq active");
        }

        else if (IsPageMatch(linkCalls) ||
                IsPageMatch(a_balance) ||
                IsPageMatch(a_MyReview))
        {
            linkCalls.Attributes.Add("class", "btn-calls active");
            btn_calls.Attributes.Add("class", "_btn_calls active");

            if (IsPageMatch(a_linkCalls))
                a_linkCalls.Attributes.Add("class", "active");

            else if (IsPageMatch(a_balance))
                a_balance.Attributes.Add("class", "active");

            else if (IsPageMatch(a_MyReview))
                a_MyReview.Attributes.Add("class", "active");
        }
    }
    bool IsPageMatch(HtmlAnchor _a)
    {
        string[] paths = _a.HRef.Split('/');
        string _path = paths[paths.Length - 1];
        if (_path.Contains('?'))        
            _path = _path.Split('?')[0];
        return (_path == pageName);
    }
    public bool IfHas4MenueItems()
    {
        return !span_forlink.Visible;
    }
}