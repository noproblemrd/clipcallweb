﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_AnsweringRate : System.Web.UI.UserControl
{
    PageSetting ps;
    protected void Page_Load(object sender, EventArgs e)
    {
        ps = (PageSetting)Page;
        if (!IsPostBack)
        {
            LoadAnsweringRate();
        }
    }

    private void LoadAnsweringRate()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfInt32 result;
        try
        {
            result = _supplier.GetAnsweringRate(new Guid(ps.GetGuidSetting()));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return;
      //  result.Value = 50;
        lbl_Percent.Text = result.Value.ToString() + "%";
        if (result.Value < 36)
            div_ballon.Attributes["class"] += " RedBBL";
        else if (result.Value < 75)
            div_ballon.Attributes["class"] += " yellowBBL";
        else
            div_ballon.Attributes["class"] += " greenBBL";
        int left = (((193 * result.Value) / 100) - 20);
        div_ballon.Style.Add(HtmlTextWriterStyle.Left, left + "px");
    }
}