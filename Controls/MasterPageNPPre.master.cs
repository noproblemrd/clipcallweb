﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_MasterPageNPPre : System.Web.UI.MasterPage
{
    PageSetting ps;
    string _CssClass;
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ps = (PageSetting)Page;
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, ps))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
                /*
                string home_page = DBConnection.GetHomePage(ps.siteSetting.GetSiteID);
                if (!string.IsNullOrEmpty(home_page))
                    Response.Redirect(ResolveUrl("~") + home_page);
                 * */
            }
            if (ps.siteSetting.GetSiteID != PpcSite.GetCurrent().SiteId)
                Response.Redirect(ResolveUrl("~") + "Management/ProfessionLogin.aspx");
        }
        else
        {
            if (Session.IsNewSession)
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, ps))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DataBind();
    }
    protected string GetCssClass
    {
        get
        {
            if(string.IsNullOrEmpty(_CssClass))
            {
            string pagename =
         System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
            if (pagename.StartsWith("publisher.aspx"))
                return "pubheader";
            else if (pagename.StartsWith("adv.aspx"))
                return "advheader";
            else if (pagename.StartsWith("partners.aspx") ||
                pagename.StartsWith("partners-marketing.aspx") || pagename.StartsWith("partners-franchisees.aspx"))
                return "partnerheader";
            else if (pagename.StartsWith("about.aspx") || pagename.StartsWith("about-managment.aspx") ||
                pagename.StartsWith("about-board.aspx") || pagename.StartsWith("contact.aspx"))
                return "headerau";
            else if (pagename.StartsWith("adv-freetrial.aspx") || pagename.StartsWith("PpcLogin.aspx"))
                return "";
            else
                return "pre";
            }
            else
                return _CssClass;
        }
    }
    
    protected string WidgetScript
    {
        get
        {
            return ResolveUrl("~") + "Formal/Scripts/widget.js";
        }
    }
    protected string bxSliderScript
    {
        get
        {
            return ResolveUrl("~") + "Formal/Scripts/jquery.bxSlider.min.js";
        }
    }
    public string SetTitle1
    {
        set { lbl_Title1.Text = value; }
    }
    public string SetTitle2
    {
        set { lbl_Title2.Text = value; }
    }
    public bool TopMenu
    {
        get { return _FormalTopHeader.Visible; }
        set { _FormalTopHeader.Visible = value; }
    }
    public string SetCssClass
    {
        set { _CssClass = value; }
    }
}
