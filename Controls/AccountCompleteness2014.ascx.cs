﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_AccountCompleteness2014 : System.Web.UI.UserControl
{
    PageSetting ps;
    protected void Page_Load(object sender, EventArgs e)
    {
        ps = (PageSetting)Page;
        if (!IsPostBack)
        {
            LoadaccountCompletness();
        }
    }

    private void LoadaccountCompletness()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfCompletenessData result;
        try
        {
            result = _supplier.GetCompletenessData(new Guid(ps.GetGuidSetting()));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return;
        //  result.Value.PercentageDone = 50;
        lbl_AccountCompleteness.Text = result.Value.PercentageDone + "%";
        int width = ((188 * result.Value.PercentageDone) / 100);
        div_AccountCompletenessScal.Style.Add(HtmlTextWriterStyle.Width, width + "px");
        if (result.Value.PercentageDone < 98)
            div_AccountCompletenessScal.Attributes["class"] += " NotComplete";
        lbl_PercentageInLink.Text = result.Value.PercentageInLink + "%";
        string _href = ResolveUrl("~/PPC/FramePpc2.aspx");

        switch (result.Value.LinkTo)
        {

            case (WebReferenceSupplier.CompletenessDataLinkOptions.WorkHours):
                a_AddPaymentMethod.InnerText = "Add when to receive calls";
                _href += "?step=4";
                break;
            case (WebReferenceSupplier.CompletenessDataLinkOptions.Details):
                a_AddPaymentMethod.InnerText = "Complete my profile";
                break;
            case (WebReferenceSupplier.CompletenessDataLinkOptions.Payment):
                a_AddPaymentMethod.InnerText = "Add credit";
                _href = ResolveUrl("~/PPC/AddCredit.aspx");
                break;
            default: div_AccountCompletenessLink.Visible = false;
                break;
        }
        a_AddPaymentMethod.HRef = _href;
    }
}