﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adv : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Master.SetTitle1 = "Real calls, real customers.";
            Master.SetTitle2 = "Not just clicks.";
        }
    }
}