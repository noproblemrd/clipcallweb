﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Net;

public partial class writeToFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string payment_type=Request["payment_type"];// could be instant or eCheck 
        string payment_status=Request["payment_status"];
        /*
        The status of the payment:
        Canceled_Reversal: A reversal has been canceled. For example, you
        won a dispute with the customer, and the funds for the transaction that was
        reversed have been returned to you.
        Completed: The payment has been completed, and the funds have been
        added successfully to your account balance.
        Created: A German ELV payment is made using Express Checkout.
        Denied: You denied the payment. This happens only if the payment was
        previously pending because of possible reasons described for the
        pending_reason variable or the Fraud_Management_Filters_x
        variable.
        Expired: This authorization has expired and cannot be captured.
        Failed: The payment has failed. This happens only if the payment was
        made from your customer’s bank account.
        Pending: The payment is pending. See pending_reason for more
        information.
        Refunded: You refunded the payment.
        Reversed: A payment was reversed due to a chargeback or other type of
        reversal. The funds have been removed from your account balance and
        returned to the buyer. The reason for the reversal is specified in the
        ReasonCode element.
        Processed: A payment has been accepted.
        Voided: This authorization has been voided.
        */
        string txn_type = Request["txn_type"]; // Payment received; source is a Buy Now, Donation, or Auction Smart Logos button
        
        string file_path = Server.MapPath("~") + @"\paypal.log";

        SupllierPayment sp = new SupllierPayment();
        sp.deposit = Request["mc_gross"];
        
        //sp.deposit = "200";

        sp.transactionId = Request["txn_id"];
        sp.fieldId = Request["option_selection1"];
        sp.fieldName = Request["option_selection2"];
        sp.balanceOld = Request["option_selection3"];
        sp.userId = Request["option_selection4"];
        sp.userName = Request["option_selection5"];
        sp.isFirstTimeV = Convert.ToBoolean(Request["option_selection6"]);
        sp.siteId = Request["option_selection7"];
        sp.siteLangId = Convert.ToInt32(Request["option_selection8"]);
        sp.chargingCompany = "paypal";

        string item_name = Request["item_name"];
        LogEdit.SaveLog("Accounting_" + Request["option_selection8"], "item_name: " + item_name + " " + Server.HtmlDecode(item_name) + " " + Server.UrlEncode(item_name));

        if (payment_type != "instant" || payment_status != "Completed" || txn_type != "web_accept")
        {
            LogEdit.SaveLog("Accounting_" + Request["option_selection7"], "not valid transaction for us:\r\npayment_type: " + payment_type + " payment_status: " + payment_status + " txn_type: " + txn_type);
            return;

        }

        string custom = Request["custom"] == null ? "" : Request["custom"];

        //LogEdit.SaveLog("Accounting_" + sp.siteId, "custom " + custom);

        string[] arrCustom = {};
        if(custom!="")
            arrCustom = custom.Split('&');
        
        string[] nameValueCustom;
        for (int i = 0; i < arrCustom.Length; i++)
        {
            /*
            SupplierId={ECAD4D81-5FD4-DF11-81D3-001517D1792A}&
                AdvertiserName=Company Test&
                    isFirstTimeV=False&OldValue=5,334&
                        UserId=a6a59cdf-ba9b-df11-92c8-a4badb37a26f&
                            UserName=user1&siteLangId=1&
                            siteId=1015&BonusAmount=72&
                            NewValue=6306
            */

            nameValueCustom = arrCustom[i].Split('=');
            if (nameValueCustom[0] == "bonusAmount")
                sp.discount = nameValueCustom[1];

            if (nameValueCustom[0] == "supplierId")
                sp.guid = nameValueCustom[1];

            if (nameValueCustom[0] == "advertiserName")
                sp.name = nameValueCustom[1];
            
            if (nameValueCustom[0] == "fieldId")
                sp.fieldId = nameValueCustom[1];

            if (nameValueCustom[0] == "fieldName")
                sp.fieldName = nameValueCustom[1];

            if (nameValueCustom[0] == "balanceNew")
                sp.balanceNew = nameValueCustom[1];

            /*
            if (nameValueCustom[0] == "isFirstTimeV")
                sp.isFirstTimeV = Convert.ToBoolean(nameValueCustom[1]);

            if (nameValueCustom[0] == "balanceOld")
                sp.balanceOld = nameValueCustom[1];           
            
            if (nameValueCustom[0] == "userId")
                sp.userId = nameValueCustom[1];

            if (nameValueCustom[0] == "userName")
                sp.userName = nameValueCustom[1];            

            
            if (nameValueCustom[0] == "siteLangId")
                sp.siteLangId = Convert.ToInt32(nameValueCustom[1]);
            
            if (nameValueCustom[0] == "siteId")
                sp.siteId = nameValueCustom[1];
            */

            if (nameValueCustom[0] == "isAutoRenew")
                sp.isAutoRenew = Convert.ToBoolean(nameValueCustom[1]);

           
      }

        LogEdit.SaveLog("Accounting_" + sp.siteLangId, "Credit PayPal: Charging Received request " 
            + sp.name + "\r\ncustom " + Request.Form);

        try
        {
            string strRequestForm = "";
            StringBuilder sb = new StringBuilder();
            // call back to paypal and send the  same request that came and add cmd=_notify-validate
            foreach (string strForm in Request.Form)
            {
                //strRequestForm += "&" + strForm + "=" + Server.UrlEncode(Request.Form[strForm]);
                sb.Append("&" + strForm + "=" + Server.UrlEncode(Request.Form[strForm]));
            }

            string strWebRquest = Request["option_selection9"] + "?cmd=_notify-validate" + sb.ToString();
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(strWebRquest);

            webRequest.Method = "POST";
            
            Stream webStream = webRequest.GetRequestStream();
            webStream.Close();
            WebResponse webResponse = webRequest.GetResponse();
            webStream = webResponse.GetResponseStream();


            TextReader reader = new StreamReader(webStream);
            string strResponse = reader.ReadToEnd();

            if (strResponse == "VERIFIED") // verified if its is 
            {

                try
                {
                    /*
                    using (FileStream fs = new FileStream(file_path, FileMode.Append))
                    {
                        using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                        {

                            tw.WriteLine("baaa2 " + DateTime.Now.ToString() + " " + Request.QueryString);
                            tw.WriteLine("baaa2" + DateTime.Now.ToString() + " " + Request.Form);
                        }
                    }
                    */

                    LogEdit.SaveLog("Accounting_" + sp.siteLangId, "Credit PayPal: Charging Authenicted request " + sp.name);


                  //  WebServiceCharging wsCharging = new WebServiceCharging();
                    //wsCharging.ChargingGeneral(sp);
                    DpzUtilities.ChargingSupplier(sp, null, Session);

                }

                catch (Exception ex)
                {
                    LogEdit.SaveLog("Accounting_" + sp.siteLangId, "Credit PayPal: probelm with crm "
                        + sp.name + "\r\n" + ex.Message + "\r\n" + ex.StackTrace);

                }


            }

            else
            {
                LogEdit.SaveLog("Accounting_" + sp.siteLangId, "Credit PayPal invalid " + strResponse);
 
            }
       
        
        }

        catch (Exception ex)
        {
            LogEdit.SaveLog("Accounting_" +
                sp.siteLangId, "Credit PayPal: probelm to send auth to paypal "
                + sp.name + "\r\n" + ex.Message + "\r\n" + ex.StackTrace);

        }
    }
       
}
