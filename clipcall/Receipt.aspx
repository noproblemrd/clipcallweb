﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Receipt.aspx.cs" Inherits="clipcall_Receipt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta name="robots" content="nofollow"><meta name="googlebot" content="noindex"/>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
    <link type="text/css" rel="stylesheet" href="Style.css" />
    <title></title>
    <script type="text/javascript">
   //     var click_event = /iPad|iPhone|iPod/.test(navigator.userAgent)/* && !window.MSStream*/ ? "touchstar" : "click";
       var click_event = "click";
        document.addEventListener("DOMContentLoaded", function (event) {
            var a_questionMark = document.querySelector('#a_questionMark');
            if (a_questionMark) {
                a_questionMark.addEventListener(click_event, function (e) {
                    document.querySelector("#div_quote").style.display = "block";
                    document.body.addEventListener(click_event, RemoveDiv);
                    e.stopImmediatePropagation();
                    e.preventDefault();
                }, false);
                document.querySelector("#div_quote").addEventListener(click_event, function (e) {
                    e.stopImmediatePropagation();
                    e.preventDefault();
                }, false);
            }
           
        }, false);
        
        function RemoveDiv() {
            document.querySelector("#div_quote").style.display = "none";
            document.body.removeEventListener(click_event, RemoveDiv);
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="tmain">
                <tr>
                    <td  class="td_left">
                        <asp:Image ID="Image1" runat="server" ImageUrl="<%# logo %>" AlternateText="ClipCall" Width="166" Height="23" /> <% /* Width="201" Height="<%# LogoHeight >"  */%>
                    </td> 
                    <td  class="td_right">
                        <asp:Label ID="Label12" runat="server" Text="<%# date %>"></asp:Label>
                    </td>  
                </tr>
                <tr class="trbackground">                    
                    <td colspan="2" class="td_center amount">
                        <asp:Label ID="lbl_amount" runat="server" Text="<%# amount %>"></asp:Label>
                    </td>                   
                </tr>
                <tr class="trbackground">
                   <td colspan="2" class="td_center brand">
                        <asp:Label ID="lbl_creditCard" runat="server" Text="<%# creditCard %>"></asp:Label>
                    </td>  
                </tr>
                
                <tr class="trtable bld border-bottom">
                    <td class="td_left">
                        <asp:Label ID="Label1" runat="server" Text="Description"></asp:Label>
                    </td>
                    <td class="td_right">
                         <asp:Label ID="Label2" runat="server" Text="Amount"></asp:Label>
                    </td>
                    
                </tr>
                 <tr class="trtable">
                    <td class="td_left">
                        <asp:Label ID="Label5" runat="server" Text="<%# PrimeOrQuote %>"></asp:Label>
                        <div runat="server" visible="<%# !IsPrimeCharge %>" style="position:relative;" class="div_quoteDetails">
                            <asp:Label ID="Label19" runat="server" Text="*Includes Service fee"></asp:Label>
                            <a href="javascript:void(0);" id="a_questionMark" class="question-mark">?</a>
                            <div style="display:none; position:absolute; " id="div_quote">
                                <asp:Label ID="Label20" runat="server" Text="This service fee helps us run our platform and may vary between 3%-10% based on job size and complexity."></asp:Label>
                            </div>
                        </div>
                    </td>
                    <td class="td_right">
                         <asp:Label ID="Label6" runat="server" Text="<%# QuoteAmount %>"></asp:Label>
                        
                        
                        
                    </td>                    
                </tr>

                 <tr class="trtable" runat="server" visible="<%# HasPrimeDiscount %>">
                    <td class="td_left">
                        <asp:Label ID="Label13" runat="server" Text="VIP discount"></asp:Label>
                    </td>
                    <td class="td_right">
                         <asp:Label ID="Label14" runat="server" Text="<%# PrimeDiscount %>"></asp:Label>
                    </td>
                    
                </tr>

                <tr class="trtable" runat="server" visible="<%# HasCredit %>">
                    <td class="td_left">
                        <asp:Label ID="Label15" runat="server" Text="Credit"></asp:Label>
                    </td>
                    <td class="td_right">
                         <asp:Label ID="Label16" runat="server" Text="<%# Credit %>"></asp:Label>
                    </td>
                    
                </tr>

                  <tr class="trtable" runat="server" visible="<%# IsPrimeInstallments %>">
                    <td class="td_left">
                        <asp:Label ID="Label17" runat="server" Text="VIP Free Financing"></asp:Label>
                    </td>
                    <td class="td_right">
                         <asp:Label ID="Label18" runat="server" Text="<%# PrimeInstallmentsValue %>"></asp:Label>
                    </td>
                    
                </tr>

                <tr class="trtable bld border-top">
                    <td class="td_left">
                        <asp:Label ID="Label3" runat="server" Text="Total Charged"></asp:Label>
                    </td>
                    <td class="td_right">
                         <asp:Label ID="Label4" runat="server" Text="<%# amountBottom %>"></asp:Label>
                    </td>
                    
                </tr>
                <tr class="delimiter">
                    <td></td>
                    <td></td>
                </tr>
                <tr class="trbackground empty-delimiter" runat="server" visible="<%# _isPrime %>">
                    <td></td>
                    <td></td>
                </tr>
                <tr class="trtable trbackground border-bottom" runat="server" visible="<%# !_isPrime %>">
                    <td class="td_left">
                        <asp:Label ID="Label7" runat="server" Text="Scope of work"></asp:Label>
                    </td>
                    <td class="td_right">
                         <asp:Label ID="Label8" runat="server" Text="<%# ProjectNumber %>"></asp:Label>
                    </td>
                    
                </tr>
               
                <tr class="trtable trbackground" runat="server" visible="<%# !_isPrime %>">
                   <td colspan="2" style="padding-bottom: 23px;" >
                         <asp:Label ID="Label9" runat="server" Text="<%# ScopeOfWork %>"></asp:Label>
                    </td> 
                     
                </tr>
                <tr class="trtable-min trbackground" runat="server" visible="<%# !_isPrime %>">
                   <td colspan="2" >
                         <asp:Label ID="Label10" runat="server" Text="<%# IncludeMaterials %>" Visible="<%# HasValueIncludeMaterials %>" CssClass="<%# IncludeMaterialsClass %>"></asp:Label>
                    </td> 
                     
                </tr>
                <tr class="trtable-min trbackground" runat="server" visible="<%# !_isPrime %>">
                   <td colspan="2" >
                         <asp:Label ID="Label11" runat="server" Text="<%# Guarantee %>" Visible="<%# HasValueGuarantee %>" CssClass="<%# GuaranteeClass %>"></asp:Label>
                    </td> 
                     
                </tr>
                <tr>
                    <td colspan="2" class="td_center needhelp">
                        <span>
                            <span style="margin-right: 10px;">Need help?</span>
                            <span runat="server" visible="<%# IsClipCallOrigin %>">
                                <a href="mailto:support@clipcall.it">support@clipcall.it</a>
                            </span>
                             <span runat="server" visible="<%# !IsClipCallOrigin %>">
                                 Contact us through the Live Support tab in the app.
                            </span>
                        </span>
                    </td> 
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
