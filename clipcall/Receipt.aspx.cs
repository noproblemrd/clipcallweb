﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class clipcall_Receipt : System.Web.UI.Page
{
    const string DATE_FORMAT = "{0:MMM dd, yyyy}";
    const string CURRENCY_FORMAT = "${0:#,0.##}";
    const string CREDITCARD_FORMAT = "{0} ****{1}";
    const string SUBSCRIPTIN_MEMBER = "{0} member";
    const string INCLUDE_MATERIALS = "The price {0}include materials";
    const string GUARANTEE_WORK = "I guarantee my work for {0} months";
    const string WITHOUT_GUARANTEE = "Work guarantee not applicable";
    const string PROS_QUOTE = "Total Quote";
    const string PRIME_MEMBER = "Subscription to VIP";

    const string V_ICON = "span-includes V-icon";
    const string X_ICON = "span-includes X-icon";

    const int CLIPCALL_LOGO_HEIGHT = 28;
    const int VERIZON_LOGO_HEIGHT = 50;

    const string SUBSCRIPTION_PLAN_INSTALLMENTS = "PrimeInstallments";

    const string CLIPCALL_LOGO = @"~/ccimages/ClipCall-logo.png";
    const string VERIZON_LOGO = @"~/ccimages/vz-powered-by-cc.png";

    decimal _amount, _quoteAmount, _primeDiscount, _credit;
    string _creditCardName;
    string _creditCardNumber;
    string _projectId, _description, _subscriptionPlan;
    DateTime _date;
    int? _guaranteeDuration;
    bool? _includeMaterials;
    protected bool _isPrime, _isClipCallOrigin;

 //   bool _hasPrimeDiscount, _hasCredit;

    protected void Page_Load(object sender, EventArgs e)
    {
        Guid paymentResponseId;
        if(!Guid.TryParse(Request.QueryString["prid"], out paymentResponseId))
        {
            Response.Redirect("http://www.clipcall.it/main/");
            return;
        }
        LoadData(paymentResponseId);
        this.form1.DataBind();
        Header.DataBind();
    }

    private void LoadData(Guid paymentResponseId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);

        ClipCallReport.ResultOfReceiptDataResponse response = null;
        try
        {
            response = report.GetReceipt(paymentResponseId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (response.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        _amount = response.Value.Amount;
        _creditCardName = response.Value.CreditCardType;
        _creditCardNumber = response.Value.CreditCardNumber;
        _projectId = response.Value.ProjectId;
        _isPrime = string.IsNullOrEmpty(response.Value.ProjectId);
        _date = response.Value.CreatedOn;
        _quoteAmount = response.Value.QuoteAmount;
        _description = response.Value.QuoteDescription;
        _guaranteeDuration = response.Value.GuaranteeDuration;
        _includeMaterials = response.Value.IncludeMaterials;
        _subscriptionPlan = response.Value.SubscriptionPlan;
        _primeDiscount = response.Value.PlanDiscount;
        _credit = response.Value.CustomerCredit;
        _isClipCallOrigin = response.Value.Origin == ClipCallReport.eProjectOrigin.ClipCall;
        //      _isClipCallOrigin = true;
 //       Image1.Height
    }
    private void Update_Faild()
    {
        Response.Redirect("http://www.clipcall.it/main/");
    }
    protected string PrimeDiscount
    {
        get { return string.Format(CURRENCY_FORMAT, (_primeDiscount*-1)); }
    }
    protected string Credit
    {
        get { return string.Format(CURRENCY_FORMAT, (_credit*-1)); }
    }
    protected bool HasPrimeDiscount
    {
        get { return _primeDiscount > 0; }
    }
    protected bool HasCredit
    {
        get { return _credit > 0; }
    }
    protected bool IsPrimeInstallments
    {
        get { return _subscriptionPlan == SUBSCRIPTION_PLAN_INSTALLMENTS; }
    }
    // Not in used
    protected Unit LogoHeight
    {
        get
        {
            return _isClipCallOrigin ? new Unit(CLIPCALL_LOGO_HEIGHT) : new Unit(VERIZON_LOGO_HEIGHT);
        }
    }
    protected string PrimeInstallmentsValue
    {
        get
        {
            if (!IsPrimeInstallments)
                return null;
            return string.Format(CURRENCY_FORMAT, (_amount * 12)) + " (" + string.Format(CURRENCY_FORMAT, _amount) + @"/moX12)";
        }
    }
    protected string PrimeOrQuote
    {
        get
        {
            return _isPrime ? PRIME_MEMBER : PROS_QUOTE;
        }
    }
    
    protected bool IsPrimeCharge
    {
        get
        {
            return _isPrime;
        }
    }
    protected bool IsClipCallOrigin
    {
        get {
                return _isClipCallOrigin;
            //return true;
        }
    }
    protected string logo
    {
        get
        {
            return _isClipCallOrigin ? CLIPCALL_LOGO : VERIZON_LOGO;
        }
    }
    protected string amountBottom
    {
        get
        {
            return (!IsPrimeInstallments) ? string.Format(CURRENCY_FORMAT, _amount) : string.Format(CURRENCY_FORMAT, _amount) + @"/mo";
        }
    }
    protected string amount
    {
        get
        {
            return string.Format(CURRENCY_FORMAT, _amount);
        }
    }
    protected string creditCard
    {
        get
        {
            return string.Format(CREDITCARD_FORMAT, _creditCardName, _creditCardNumber);
        }
    }
    protected string ScopeOfWork
    {
        get
        {
            if (!string.IsNullOrEmpty(_description))
                return _description;
            return SubscriptionPlan;
        }
    }
    protected string date
    {
        get
        {
            return string.Format(DATE_FORMAT, _date);
        }
    }
    protected string QuoteAmount
    {
        get
        {
            if(_quoteAmount > 0)
                return string.Format(CURRENCY_FORMAT, _quoteAmount);
            return amount + @"/month"; ;
        }
    }
    protected string ProjectNumber
    {
        get
        {
            if(!string.IsNullOrEmpty(_projectId))
                return string.Format("Project ID: {0}", _projectId);
            return SubscriptionPlan;
        }
    }
    protected string SubscriptionPlan
    {
        get { return string.Format(SUBSCRIPTIN_MEMBER, _subscriptionPlan); }
    }
    protected string Guarantee
    {
        get
        {
            if (_guaranteeDuration.HasValue && _guaranteeDuration.Value > 0)
                return string.Format(GUARANTEE_WORK, _guaranteeDuration.Value);
            return WITHOUT_GUARANTEE;
        }
    }
    protected bool HasValueGuarantee
    {
        get { return _guaranteeDuration.HasValue; }
    }
    protected string IncludeMaterials
    {
        get
        {
            if (_includeMaterials.HasValue)
                return _includeMaterials.Value ? string.Format(INCLUDE_MATERIALS, string.Empty) : string.Format(INCLUDE_MATERIALS, "does not ");
            return null;
        }
    }
    protected bool HasValueIncludeMaterials
    {
        get { return _includeMaterials.HasValue; }
    }
    protected string IncludeMaterialsClass
    {
        get { return (_includeMaterials.HasValue && _includeMaterials.Value) ? V_ICON : X_ICON; }
    }
    protected string GuaranteeClass
    {
        get { return (_guaranteeDuration.HasValue && _guaranteeDuration.Value > 0) ? V_ICON : X_ICON; }
    }
}