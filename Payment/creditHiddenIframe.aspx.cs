﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Payment_hiddeniframe : System.Web.UI.Page
{
    protected string action;
    protected string SiteLangId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        action=Request["action"];
        SiteLangId = Request["SiteLangId"];

        if (action == "wrongDetails")
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "wrongDetails"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "wrongDetails", "wrongDetails();", true);
        }

        else if (action == "success")
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "updateSuccess"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "updateSuccess", "updateSuccess();", true);

        }

        else if (action == "unsuccess")
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "updateUnSuccess"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "updateUnSuccess", "updateUnSuccess();", true);
        }

        else if (action == "decline")
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "decline"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "decline", "decline();", true);
        }

        string pagename = System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        DBConnection.LoadTranslateToControl(this, pagename, Convert.ToInt32(SiteLangId));
 
        Page.Header.DataBind();
    }
}
