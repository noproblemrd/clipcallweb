﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="creditHiddenIframe.aspx.cs" Inherits="Payment_hiddeniframe" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">
    function closGrandFatherPopUpModal()
    {             
        top.hideDivCredit();              
    }
    
    function hideGrandFatherLoader() 
    {  
          
        top.hideDiv2();
    }
             
    function showGrandFatherLoader()
    {        
        top.showDiv2();
    }
    
    function wrongDetails()
    {
        alert('<%#Hidden_WrongDetails.Value%>')
        hideGrandFatherLoader(); 
    }
    
    function updateSuccess()
    {
        alert('<%#Hidden_Success.Value%>');
        hideGrandFatherLoader();
        //alert(location.href);
        //alert(parent.location.href);
        //alert(parent.parent.location.href);
        //alert(top.window.location.href);
        //top.window.location.href='framePublisher.aspx';
        //top.window.baa();
        
        
        closGrandFatherPopUpModal();
    }
    
    function updateUnSuccess()
    {
        alert('<%#Hidden_UnSuccess.Value%>');        
        hideGrandFatherLoader();
    }
    
    function decline()
    {
        alert('<%#Hidden_CreditCardDeclined.Value%>');
        hideGrandFatherLoader();        
    }
    
   
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="Hidden_WrongDetails" runat="server" Value="Wrong Details" />
        <asp:HiddenField ID="Hidden_Success" runat="server" Value="Payment was executed successfully. Please refresh the page to reflect the changes in the account" />
        <asp:HiddenField ID="Hidden_UnSuccess" runat="server" Value="There was an error, please enter info again!!!" />  
        <asp:HiddenField ID="Hidden_CreditCardDeclined" runat="server" Value="Declined. Please check card details" />
    </div>
    </form>
</body>
</html>
