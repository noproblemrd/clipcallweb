﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

public partial class jsnop : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["callback"]))
            this.JsonPCallback();
    }

    public void JsonPCallback()
    {
        string Callback = Request.QueryString["callback"];
        
  //      Dictionary<string, string> dic = new Dictionary<string, string>();
        List<TheTest> list = new List<TheTest>();
        /*
        while (Request.Params[p + i] != null)
        {
            string newPhone = "1-800-" + Request.Params[p + i];
            dic.Add(Request.Params[p + i], newPhone);
        }
         * */
        string phones = Request.Params["phones"];
        if (!string.IsNullOrEmpty(phones))
        {
            string[] _phones = phones.Split(',');
            foreach (string str in _phones)
            {
                string newPhone = "1-800-" + str;
         //       dic.Add(str, newPhone);
                TheTest _test = new TheTest() { PhonId=str, VirtualPhone=newPhone };
                list.Add(_test);
            }
            
        }
        if (!string.IsNullOrEmpty(Callback))
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
       //     string result = jss.Serialize(dic);
            // *** Do whatever you need
   //         Response.Write(Callback + "(["+result+"]);");
 //           Response.Write(Callback + "( {\"x\":10 , \"y\":100} );");
             
        //    JavaScriptSerializer jss = new JavaScriptSerializer();
            string result = jss.Serialize(list);
            Response.Write(Callback + "( " + result + " );"); ;
        }

        Response.End();
    }
}
public class TheTest
{
    public string PhonId;
    public string VirtualPhone;
    
}