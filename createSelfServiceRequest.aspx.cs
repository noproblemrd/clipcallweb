﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class createSelfServiceRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string ExpertiseCode = Request["ExpertiseCode"];
        string ExpertiseType = Request["ExpertiseType"];
        string RegionCode = Request["RegionCode"];
        string RegionLevel = Request["RegionLevel"];
        string SiteId = Request["SiteId"];
        string SupplierId = Request["SupplierId"];
        string OriginId = Request["OriginId"];
        string ContactPhoneNumber = Request["ContactPhoneNumber"];


        LogEdit.SaveLog(SiteId, "Start:" + "\r\nsurfer phone: " +
        ContactPhoneNumber + 
        "\r\nSession: " + Session.SessionID +
        "\r\nip: " + Utilities.GetIP(Request));


        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(this, SiteId);
        WebReferenceCustomer.SupplierServiceRequest request=new WebReferenceCustomer.SupplierServiceRequest();
        request.ExpertiseCode = ExpertiseCode;
        request.ExpertiseType = Convert.ToInt32(ExpertiseType);
        request.RegionCode = RegionCode;
        request.RegionLevel = Convert.ToInt32(RegionLevel);
        request.RequestDescription = "";
        request.SiteId = SiteId;
        request.SupplierId = new Guid(SupplierId);
        request.OriginId = new Guid(OriginId);
        request.ContactPhoneNumber = ContactPhoneNumber;
        request.SessionId = Session.SessionID;

        WebReferenceCustomer.ResultOfSupplierServiceResponse response = new WebReferenceCustomer.ResultOfSupplierServiceResponse();

        response=customer.CreateSupplierSpecificService(request);

        if (response.Type == WebReferenceCustomer.eResultType.Success)
        {
            //Response.Write(serviceResponse.ServiceRequestId + " status:" + serviceResponse.Status.ToString().ToLower());
            Response.Write(response.Value.ServiceRequestId);


            LogEdit.SaveLog(SiteId, "CreateSelfServiceRequest status:" + response.Value.Status.ToString() +
                "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " +
                ContactPhoneNumber +
                "\r\nSession: " + Session.SessionID + "\r\nip: " + Utilities.GetIP(Request));

        }

        else
        {
            Response.Write("unsuccess");
            LogEdit.SaveLog(SiteId, response.Value.Status.ToString() +
                 "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " + ContactPhoneNumber +                
                "\r\nSession: " + Session.SessionID +
                "\r\nip: " + Utilities.GetIP(Request) + "\r\nmessage: " + response.Messages.ToArray().ToString());

        }

    }


}
