﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Unsubscribe : System.Web.UI.Page
{
    protected string Email { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        string idStr = Request.QueryString["id"];
        Guid id;
        if (!Guid.TryParse(idStr, out id))
        {
            SetSuccessOrFailure(false);
            return;
        }
        bool ok = CallUnsubscribe(id);
        SetSuccessOrFailure(ok);
    }

    private bool CallUnsubscribe(Guid id)
    {
        bool retVal = false;
        var crmClient = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        var crmResult = crmClient.UnsubscribeFromBulkEmails(
            new WebReferenceSupplier.UnsubscribeFromBulkEmailsRequest()
            {
                SupplierId = id
            });
        if (crmResult.Type == WebReferenceSupplier.eResultType.Success
            && crmResult.Value.Success)
        {
            Email = crmResult.Value.Email;
            retVal = true;
        }
        return retVal;
    }

    private void SetSuccessOrFailure(bool isSuccess)
    {
        fail.Visible = !isSuccess;
        success.Visible = isSuccess;
    }
}