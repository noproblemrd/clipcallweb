using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class Management_ResetPassword : PageSetting
{
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        string IsPublisher = Request.QueryString["IsPublisher"];
        //      userManagement = UserMangement.GetUserObject(this);
        if (!string.IsNullOrEmpty(IsPublisher) && IsPublisher == "true")
            this.MasterPageFile = "~/Publisher/MasterPagePrePublisher.master";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, this))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            }

            SetTextToPage();
            
        }
    }
    void SetTextToPage()
    {
        string _expression = DBConnection.GetMobileRegularExpression(siteSetting.GetSiteID);
        RegularExpressionValidatorPhone.ValidationExpression = (string.IsNullOrEmpty(_expression)) ? @"^\d+" : _expression;
        _RadCaptcha.CaptchaLinkButtonText = lbl_RefreshImage.Text;
        _RadCaptcha.CaptchaTextBoxLabel = lbl_AccessCode.Text;
    }
    protected void btnCheckCode_Click(object sender, EventArgs e)
    {
        
        if (siteSetting == null || string.IsNullOrEmpty(siteSetting.GetSiteID))
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            if (!DBConnection.LoadSiteId(host, this))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
        }
        if (!_RadCaptcha.IsValid)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MissingCaptcha", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_MissingCaptcha.Text) + "');", true);
            return;
        }
        string phone = txt_professionalPhone.Text;
        string email = txt_email.Text;
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResetPasswordRequest _request = new WebReferenceSupplier.ResetPasswordRequest();
        _request.PhoneNumber = phone;
        _request.Email = email;
        WebReferenceSupplier.ResultOfResetPasswordResponse result = null;
        try
        {
            result = supplier.ResetPassword(_request);
        }
        catch (Exception ex)
        {
            lblComment.Text = lbl_Failed.Text;
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            lblComment.Text = lbl_Failed.Text;
        else
        {
            if (result.Value.ResetPasswordStatus != WebReferenceSupplier.ResetPasswordResponseStatus.OK)
                lblComment.Text = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "ResetPasswordResponseStatus", result.Value.ResetPasswordStatus.ToString());
            else
                lblComment.Text = lbl_PasswordSentOk.Text;
        }
       
    }
   
}
