﻿function CallWebService(params, url, func, func_error)
{
    var objHttp;
    var ua = navigator.userAgent.toLowerCase(); 
 
    if(ua.indexOf("msie")!= -1)
    {      
        objHttp =  new ActiveXObject("Msxml2.XMLHTTP");        
    }
    else
    {       
        objHttp=new XMLHttpRequest();
        objHttp.overrideMimeType("application/xml"); 
    }
    if (objHttp == null)
    {    
        func_error();   
        return;
    } 	        

    var cleanMsg="";
    
    objHttp.open("POST", url, true);

    objHttp.onreadystatechange = function () {

        if (objHttp.readyState == 4) {
            //alert(objHttp.responseXML.documentElement.childNodes[0].nodeValue);
            if (objHttp.status == 200) {
                if (objHttp.responseXML.childNodes.length == 0)
                    func(objHttp.responseText);
                else if (objHttp.responseXML.documentElement.childNodes.length == 0)
                    func("");
                else
                    func(objHttp.responseXML.documentElement.childNodes[0].nodeValue);
            }
            else {
                       //alert(objHttp.status);
                func_error();
            }
        }
    }
    //objHttp.setRequestHeader('SOAPAction','http://tempuri.org/GetBadWords');
 //    var _length = (typeof (params) == 'undefined' || params == null) ? 0 : params.length;
    objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  //  objHttp.setRequestHeader("Content-Length", _length);
 //   objHttp.setRequestHeader("Connection", "close");
    objHttp.send(params);
}
