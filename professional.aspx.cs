using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.Services.Protocols;
using System.Data.SqlClient;

public partial class professional :PageSetting
{

    protected string supplierGUID; 
    protected bool ifFlash = false; 
    protected string logosrc = ""; 

    protected string description = ""; 
    protected string VideoUrl = "";  
    protected string SiteNameId = ""; 
    protected string professionalImagesPrefixWeb = ""; 
    string professionalIconsWeb = System.Configuration.ConfigurationManager.AppSettings["professionalIconsWeb"];
    string TempWebImage = System.Configuration.ConfigurationManager.AppSettings["professionalImagesTempWeb"];
    string TempImage = System.Configuration.ConfigurationManager.AppSettings["professionalImagesTemp"];

    protected string _pathReview = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {   
            
            if(siteSetting==null || string.IsNullOrEmpty(siteSetting.GetSiteID))
                if(!SiteSetting.LoadSiteId(this, Request.ServerVariables["SERVER_NAME"]))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            if(!siteSetting.HasMiniSite)
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");

            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
            //translate in the functions
            if (IfPreviewV)
                setPreviewDetails();
            else
                SetDefaultData();

            divVideo.Visible = (VideoUrl != "0" && VideoUrl != "");
            _flash.Visible = ifFlash;
                        
        }
        
        Page.Header.DataBind();
    }
    private void SetDefaultData()
    {
        supplierGUID = Request["supplierGUID"];
        SiteNameId = Request.QueryString["SiteId"];
        _pathReview = ResolveUrl("~") + @"Reviews.aspx";
        _pathReview += "?SiteId=" + SiteNameId + "&Id=" + supplierGUID;
        
        if (string.IsNullOrEmpty(supplierGUID) || string.IsNullOrEmpty(SiteNameId))
            Response.Redirect("Management/LogOut.aspx");

        //load translation for consumers
        siteSetting = new SiteSetting(SiteNameId);
        LoadTranslate(siteSetting.siteLangId);


        supplierGUID = Utilities.CleanStringGuid(supplierGUID);
    //    GetTranslate(SiteNameId);
        SetData();
      
        professionalImagesPrefixWeb = System.Configuration.ConfigurationManager.AppSettings["professionalImagesPrefixWeb"];
        professionalImagesPrefixWeb += supplierGUID;
        
        string professionalImagesPrefix = System.Configuration.ConfigurationManager.AppSettings["professionalImagesPrefix"];
        professionalImagesPrefix += supplierGUID;

        string professionalLogos = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
        string professionalLogosWeb = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];

        
        ifFlash = File.Exists(professionalImagesPrefix + @"\shai_Gallery.html");

        

        /************** check if file exists ****************/
        logosrc = professionalLogos + supplierGUID + ".jpg";

        if (File.Exists(logosrc))
        {
            logosrc = professionalLogosWeb + supplierGUID + ".jpg";
        }
        else
            logosrc = professionalIconsWeb + "user-icon.png";

        ds_logo.Src = logosrc;

        /************ another opportunity from db ****************
        //string logoName = GetLogoFileName(supplierGUID);

        //logosrc = professionalLogos + logoName;
        /************ end another opportunity from db ****************/
        
    }
    private void SetData()
    {
        string company = "";  
        string numberofemployees = "";   
        string surveygradecounter = "";
        string sumassistancerequests = "";
        string counties = "";
        string expertises = "";    
        string CountryName = "";
        string StateName = "";
        string CityName = "";
        string StreetName = "";
        string HouseNum = "";
        string Zipcode = "";
        string directPhone = "";
        string strCities = "";
        string ExtensionNumber = "";

        WebReferenceSupplier.Supplier wsSupplier = WebServiceConfig.GetSupplierReference(this, SiteNameId);
        WebReferenceSupplier.ResultOfMiniSiteData result = null;
        try
        {
            result = wsSupplier.GetMiniSiteData(new Guid(supplierGUID));
            //Response.Write(xmlSupplier);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ClientScript.RegisterStartupScript(this.GetType(), "error", "alert('" + _HiddenField_LoadFailed.Value + "');", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "error", "alert('" + _HiddenField_LoadFailed.Value + "');", true);
            return;
        }


        company = result.Value.Name;// details.Attributes["Company"].InnerText;

        //accountnumber = details.Attributes["AccountNumber"].InnerText;
        //Response.Write("accountnumber:" + accountnumber);
        numberofemployees = result.Value.NumberOfEmployees + "";
        description = result.Value.LongDescription;
   //     surveygradecounter = result.Value.SurveyGradeCounter + "";
        sumassistancerequests = result.Value.SumAssistanceRequests + "";
        VideoUrl = Server.UrlDecode(result.Value.VideoUrl);
        StreetName = result.Value.StreetName;
        HouseNum = result.Value.HouseNum;
        CityName = result.Value.CityName;
        StateName = result.Value.StateName;
        int _up = result.Value.Likes;
        int _down = result.Value.Dislikes;
 //       CountryName = result.Value.CountryName;
        Zipcode = result.Value.Zipcode;
        directPhone = Request["DirectNumber"];
        ExtensionNumber = Request["ExtensionNumber"];
        //CountryName="China" StateName="" CityName="dl" DistrictName="District1" StreetName="zarhin" HouseNum="10" Zipcode="1234"
        lblCompany.Text = company;
        lblDesc.Text = Server.UrlDecode(description);        
        lblEmployees.Text = numberofemployees;
        lblRequests.Text = sumassistancerequests;
        lbl_handup.Text = _up + "";
        lbl_handown.Text = _down + "";
        //lblSurveys.Text = surveygradecounter;
        if (string.IsNullOrEmpty(directPhone) && (userManagement.IsSupplier() || userManagement.IsPublisher()))        
            lblDirectPhone.Text = GetDirectPhone;        
        else if (string.IsNullOrEmpty(directPhone))
            lblDirectPhone.Text = string.Empty;
        else
        {            
            lblDirectPhone.Text = (string.IsNullOrEmpty(ExtensionNumber) ? directPhone : ExtensionNumber + "-" + directPhone);
        }

        lblAddress.Text = StreetName + " " + HouseNum 
            + (string.IsNullOrEmpty(CityName) ? "" : @"," + CityName) + (string.IsNullOrEmpty(StateName) ? "" : @"," + StateName) + " " + Zipcode;

        string xmlResult;
        try
        {
            xmlResult = wsSupplier.GetSupplierExpertise(siteSetting.GetSiteID, supplierGUID);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ClientScript.RegisterStartupScript(this.GetType(), "error", "alert('" + _HiddenField_LoadFailed.Value + "');", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(xmlResult);
        if (xdd["SupplierExpertise"] == null || xdd["SupplierExpertise"]["Error"] != null)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "error", "alert('" + _HiddenField_LoadFailed.Value + "');", true);
            return;
        }

     //   string certificate = "";
        foreach (XmlNode node in xdd["SupplierExpertise"].ChildNodes)
        {
            expertises += node.Attributes["Name"].Value;
            string certificate = node.Attributes["Certificate"].Value;
            if (certificate == "True")
                certificate = "<img src='images/icons/icon-certified.gif'> Certified!";
            else
                certificate = "";
            if (node.HasChildNodes)
            {
                expertises += "(";
                foreach (XmlNode child_node in node.ChildNodes)
                {
                    expertises += child_node.InnerText + ",";
                }
                expertises = expertises.Substring(0, expertises.Length - 1);
                expertises += ") " + certificate;
            }
            expertises += "&nbsp;&nbsp;";
        }
        /*
        XmlNode expertise = root.SelectSingleNode("Expertise");
        if (expertise.HasChildNodes)
        {
            for (int i = 0; i < expertise.ChildNodes.Count; i++)
            {
                certificate = "";
                XmlNode primary = expertise.ChildNodes[i];
                expertises += primary.Attributes["Name"].InnerText;
                certificate = primary.Attributes["Certificate"].InnerText;
                if (certificate == "True")
                    certificate = "<img src='images/icons/icon-certified.gif'> Certified!";
                else
                    certificate = "";
                if (primary.HasChildNodes)
                {
                    expertises += "(";
                    for (int y = 0; y < primary.ChildNodes.Count; y++)
                    {
                        if (y == primary.ChildNodes.Count - 1)
                            expertises += primary.ChildNodes[y].InnerText;
                        else
                            expertises += primary.ChildNodes[y].InnerText + ",";
                    }
                    expertises += ") " + certificate;
                }

                expertises += "&nbsp;&nbsp;";
            }
        }
        */
        lblExpertise.Text = expertises;

        //Response.Write("expertises:" + expertises);

        /* ************** Waiting for design ********************/
        /*
        string xmlCities = string.Empty;
        string mode = "";

        
        try
        {
            string command = "EXEC dbo.GetSiteDetailBySiteNameId @SiteId";
            SqlConnection conn = DBConnection.GetConnString();
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteId", SiteNameId);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {               
                if ((bool)reader["MapArea"]) // map
                {                    
                    xmlCities = wsSupplier.GetWorkArea(SiteNameId, supplierGUID);
                    mode = "Map";
                }
                else // table
                {                    
                    xmlCities = wsSupplier.HandleAdvertiserRegions(SiteNameId, 1, supplierGUID, "", "");
                    mode = "Table";
                }
            }

        }

        catch (SoapException ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "error", "alert('" + _HiddenField_LoadFailed.Value + "');", true);
            return;
        }

        //Response.Write("xmlCities:" + xmlCities);
        

       
        
        XmlDataDocument xmlCitiesDataDocument = new XmlDataDocument();

        if (mode == "Map")
        {
            xmlCitiesDataDocument.LoadXml(xmlCities);


            XmlNode xmlNodeSelectedAreas = xmlCitiesDataDocument["Areas"].SelectSingleNode("SelectedAreas");

            for (int i = 0; i < xmlNodeSelectedAreas.ChildNodes.Count; i++)
            {
                string _city = xmlNodeSelectedAreas.ChildNodes[i].Attributes["Name"].InnerText;
                if (string.IsNullOrEmpty(_city))
                    continue;
                strCities += _city + ",";
            }

        }

        
        if (strCities.Length > 0)
        {
            lblCities.Text = strCities.Substring(0, strCities.Length - 1);
        }
        */

        /* ************** Waiting for design ********************/

    }

    private void setPreviewDetails()
    {
        supplierGUID = GetGuidSetting();
        SiteNameId = siteSetting.GetSiteID;
  //      GetTranslate(SiteNameId);
        SetData();  
        
        ifFlash = File.Exists(TempDirectoryV + @"\shai_Gallery.html");
  
        if (File.Exists(TempDirectoryV + @"\" + logoV))
            logosrc = TempDirectoryWebV + @"/" + logoV;
        else
            logosrc = professionalIconsWeb + "user-icon.png";

        ds_logo.Src = logosrc;
        VideoUrl = VideoV;
        lblDesc.Text = DescriptionV;
        professionalImagesPrefixWeb = TempDirectoryWebV;
        
    }

    private string GetLogoFileName(string supplierGUID)
    {
        string logo = string.Empty;
        string command = "EXEC dbo.GetAdvertiserDetails @UserId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", new Guid(supplierGUID));
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                logo = (reader.IsDBNull(1)) ? "" : reader.GetString(1);
            }
            conn.Close();
        }
        return logo;
    }
   
    bool IfPreviewV
    {
        get { return (Session["IfPreview"] == null) ? false : (bool)Session["IfPreview"]; }
    }
    string TempDirectoryV
    {
        get
        {
            return (Session["TempDirectory"] == null) ? string.Empty :
            TempImage + (string)Session["TempDirectory"];
        }        
    }
    string TempDirectoryWebV
    {
        get
        {
            return (Session["TempDirectory"] == null) ? string.Empty :
            TempWebImage + (string)Session["TempDirectory"];
        }

    }
  
    string DescriptionV
    {
        get
        {
            return (Session["Description"] == null) ? string.Empty :
                (string)Session["Description"];
        }        
    }
    string ShortDescriptionV
    {
        get
        {
            return (Session["ShortDescription"] == null) ? string.Empty :
                (string)Session["ShortDescription"];
        }       
    }
    string VideoV
    {
        get
        {
            return (Session["tbVideo"] == null) ? string.Empty :
                (string)Session["tbVideo"];
        }        
    }
    string logoV
    {
        get
        {
            return (Session["logoSetting"] == null) ? string.Empty :
                (string)Session["logoSetting"];
        }       
    }
    string GetDirectPhone
    {
        get { return "[" + lbl_DirectPhone.Text + "]"; }
    }
}
