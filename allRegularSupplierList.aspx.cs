using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class allRegularSupplierList : System.Web.UI.Page
{
    protected string SiteId = "";
    protected string ExpertiseCode = "";
    protected string ExpertiseLevel = "";
    protected string RegionCode = "";
    protected string RegionLevel = "";
    protected string MaxResults = "";
    protected string root = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        root = ResolveUrl("~");
        SiteId = Request["SiteId"];
        ExpertiseCode = Request["ExpertiseCode"];
        ExpertiseLevel = Request["ExpertiseLevel"];
        RegionCode = Request["RegionCode"];
        RegionLevel = Request["RegionLevel"];
        MaxResults = "-1";

    }
}
