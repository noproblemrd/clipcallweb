using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;


/// <summary>
/// Summary description for WebServiceSite
/// </summary>
[System.Web.Script.Services.ScriptService()]
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WebServiceSite : System.Web.Services.WebService {

    public WebServiceSite () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

   /*
    [WebMethod]
    public string GetBadWords(string SiteId)
    {
        string command = "EXEC dbo.GetBadWordsBySiteNameId @SiteNameId";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@SiteNameId", SiteId);
        SqlDataReader reader = cmd.ExecuteReader();
        StringBuilder _sb = new StringBuilder();
        while (reader.Read())
        {
            _sb.Append((string)reader["Word"] + ",");
        }
        conn.Close();
        if (_sb.Length < 1)
            return "Empty";
        return _sb.ToString().Substring(0, _sb.Length - 1);        
    }
    */
    [WebMethod(EnableSession = true)]
    public string SetDate(string _start, string _end)
    {
        string includStart = "";   
        if (Session == null || Session["Site"] == null)
            return string.Empty;
        CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");
        SiteSetting ss = (SiteSetting)Session["Site"];
        DateTime start = ConvertToDateTime.CalanderToDateTime(_start, ss.DateFormat);
        DateTime end = ConvertToDateTime.CalanderToDateTime(_end, ss.DateFormat);
        
        if (start == DateTime.MinValue)
        {
            if (end == DateTime.MinValue)
                return string.Empty;
            else
  //              return string.Format(ss.DateFormat, end);
                return end.ToString(ss.DateFormatClean, ci);
        }
        /*
        if ((start - DateTime.UtcNow.Date).Days > 0)
        {
            start = DateTime.Today;
  //          includStart = ";" + string.Format(ss.DateFormat, DateTime.Now);
            includStart = ";" + DateTime.Now.ToString(ss.DateFormatClean, ci);
        }
         * */
        if (end == DateTime.MinValue)
            return start.ToString(ss.DateFormatClean, ci) + includStart;
  //          return string.Format(ss.DateFormat, start) + includStart;
        if (end < start)
            return start.ToString(ss.DateFormatClean, ci) + includStart;
 //           return string.Format(ss.DateFormat, start) + includStart;
//        return string.Format(ss.DateFormat, end) + includStart;
        return end.ToString(ss.DateFormatClean, ci) + includStart;
    }
    [WebMethod(EnableSession = true)]
    public string GetExpertiseRanking(string expertiseId, string price, string UserId)
    {
        if (Session == null || Session["Site"] == null || Session["UserGuid"] == null)
            return string.Empty;
        SiteSetting ss = (SiteSetting)Session["Site"];
        UserMangement um = (UserMangement)Session["UserGuid"];
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(ss.GetUrlWebReference);
        WebReferenceSupplier.GetRankingInExpertiseRequest  _request = new WebReferenceSupplier.GetRankingInExpertiseRequest ();
        _request.ExpertiseId = new Guid(expertiseId);
        decimal _price;
        if (!decimal.TryParse(price, out _price))
            return string.Empty;
        _request.Price = _price;
        _request.SupplierId = new Guid(UserId);
        WebReferenceSupplier.ResultOfInt32 result = null;
        try
        {
            result = supplier.GetRankingInExpertise(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return string.Empty;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return string.Empty;
        return result.Value.ToString();

    }


    [WebMethod]
    public string[] GetHeadingByTagPerfixStartWithAny2(string TagPrefix)
    {
        PpcSite _heading = PpcSite.GetCurrent();

        string[] attornies = new string[]{
            "Personla Injury attorney",
            "DUI attorney",
            "Divorce attorney",
            "Bankruptcy attorney"
        };

        string[] byHeading = _heading.GetHeadingByTagPerfixStartWithAny(TagPrefix, 4);

        if (byHeading.Length == 0) // can't find in headings look in keywords
        {
            string[] headingkewords = _heading.GetHeadingNamesByKeywordStartWith(TagPrefix, 4);
            if (headingkewords.Contains("undefined category"))
            {
                return headingkewords.Where(searchText => searchText != "undefined category").Concat(attornies).ToArray();
            }

            else
            {
                return headingkewords;
            }
        }
        else
            return byHeading.Where(searchText => searchText != "undefined category").ToArray();



    }


    [WebMethod]
    public ArrayList GetHeadingsByGroup(string Group, string TagPrefix, string SiteId, int CountToDisplay)
    {
        ArrayList listExpertises = new ArrayList();
        ArrayList listExpertisesChosen = new ArrayList();
        string tagPrefix = TagPrefix.ToLower();
        DateTime dateTimeExpire = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 08, 0, 0);
        dateTimeExpire = dateTimeExpire.AddDays(1);

        if (HttpRuntime.Cache["expertiseArray"] == null)
        {
            WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises = customer.GetSpecialCategoryGroup(eHeadingGroup.attorney.ToString());
            List<WebReferenceCustomer.ExpertiseInSpecialCategoryContainer> objCategory = new List<WebReferenceCustomer.ExpertiseInSpecialCategoryContainer>(rsListExprtises.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises2 = customer.GetSpecialCategoryGroup(eHeadingGroup.personalInjuryAttorney.ToString());
            objCategory.AddRange(rsListExprtises2.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises3 = customer.GetSpecialCategoryGroup(eHeadingGroup.criminalAttorney.ToString());
            objCategory.AddRange(rsListExprtises3.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises4 = customer.GetSpecialCategoryGroup(eHeadingGroup.duiAttorney.ToString());
            objCategory.AddRange(rsListExprtises4.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises5 = customer.GetSpecialCategoryGroup(eHeadingGroup.divorceAttorney.ToString());
            objCategory.AddRange(rsListExprtises5.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises6 = customer.GetSpecialCategoryGroup(eHeadingGroup.bankruptcyAttorney.ToString());
            objCategory.AddRange(rsListExprtises6.Value);

            for (int i = 0; i < objCategory.Count; i++)
            {
                listExpertises.Add(objCategory[i].Name.ToLower());
            }

            HttpRuntime.Cache.Insert("expertiseArray", listExpertises, null, dateTimeExpire, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
        }

        else
        {
            listExpertises = (ArrayList)HttpRuntime.Cache["expertiseArray"];
        }

        //int countToDisplay = listExpertises.Count > CountToDisplay ? CountToDisplay : listExpertises.Count;
        int indexDisplay = 0;

        for (int i = 0; i < listExpertises.Count; i++)
        {
            string str = (string)listExpertises[i];
            if (str.ToLower().Contains(tagPrefix) && indexDisplay < CountToDisplay)
            {

                listExpertisesChosen.Add(str.Replace("attorney", "").Trim());
                indexDisplay++;
            }

        }

        //listExpertises.Capacity = 6;

        return listExpertisesChosen;



    }
   



    [WebMethod]
    public string[] GetHeadingsByGroup2(string Group, string TagPrefix, string SiteId, int CountToDisplay)
    {
        ArrayList listExpertises = new ArrayList();
        ArrayList listExpertisesChosen = new ArrayList();
        string tagPrefix = TagPrefix.ToLower();
        DateTime dateTimeExpire = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 08, 0, 0);
        dateTimeExpire = dateTimeExpire.AddDays(1);

        if (HttpRuntime.Cache["expertiseArray"] == null)
        {
            WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises = customer.GetSpecialCategoryGroup(eHeadingGroup.attorney.ToString());
            List<WebReferenceCustomer.ExpertiseInSpecialCategoryContainer> objCategory = new List<WebReferenceCustomer.ExpertiseInSpecialCategoryContainer>(rsListExprtises.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises2 = customer.GetSpecialCategoryGroup(eHeadingGroup.personalInjuryAttorney.ToString());
            objCategory.AddRange(rsListExprtises2.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises3 = customer.GetSpecialCategoryGroup(eHeadingGroup.criminalAttorney.ToString());
            objCategory.AddRange(rsListExprtises3.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises4 = customer.GetSpecialCategoryGroup(eHeadingGroup.duiAttorney.ToString());
            objCategory.AddRange(rsListExprtises4.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises5 = customer.GetSpecialCategoryGroup(eHeadingGroup.divorceAttorney.ToString());
            objCategory.AddRange(rsListExprtises5.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises6 = customer.GetSpecialCategoryGroup(eHeadingGroup.bankruptcyAttorney.ToString());
            objCategory.AddRange(rsListExprtises6.Value);
            WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises7 = customer.GetSpecialCategoryGroup(eHeadingGroup.SsdAttorney.ToString());
            objCategory.AddRange(rsListExprtises7.Value);

            for (int i = 0; i < objCategory.Count; i++)
            {
                listExpertises.Add(objCategory[i].Name.ToLower());
            }

            HttpRuntime.Cache.Insert("expertiseArray", listExpertises, null, dateTimeExpire, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
        }

        else
        {
            listExpertises = (ArrayList)HttpRuntime.Cache["expertiseArray"];
        }


        IEnumerable<string> query;

        if (tagPrefix.Split(' ').Length == 1)
        {

            query = (from x in listExpertises.OfType<string>()
                     where x.ToLower().Split(' ').Any(searchTerm => searchTerm.StartsWith(TagPrefix.ToLower()))
                     select x.Replace("attorney", "").Trim()).Take(CountToDisplay);
        }

        else
        {
            query = (from x in listExpertises.OfType<string>()
                     where x.ToLower().StartsWith(TagPrefix.ToLower())
                     select x.Replace("attorney", "").Trim()).Take(CountToDisplay);
        }


        PpcSite _heading = PpcSite.GetCurrent();
        ArrayList StringArraytoArrayList = new ArrayList();
        StringArraytoArrayList.AddRange(query.ToArray());        

        if (StringArraytoArrayList.Count == 0) // can't find in headings look in keywords
        {
            string[] headingkewords = _heading.GetHeadingNamesByKeywordStartWith(TagPrefix, CountToDisplay,"Attorney");           

            return headingkewords;
           
        }
        else
        {
            return query.ToArray();
        }


    }
}

