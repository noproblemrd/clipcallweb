using System;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using AjaxControlToolkit;



/// <summary>
/// Summary description for WebServiceAotoComplete
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class WebServiceAotoComplete : System.Web.Services.WebService {

    public Dictionary<string, string> list; 
    public WebServiceAotoComplete () {

        this.list = new Dictionary<string, string>();
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string[] GetSuggestions(string prefixText, int count, string contextKey)
    {
        contextKey = contextKey.Substring(0, contextKey.Length - 1);

        string[] items = contextKey.Split(';');
        foreach (string str in items)
        {
            string[] itemssub = str.Split('&');
            this.list.Add(itemssub[0], itemssub[1]);
        }
        List<string> responses = new List<string>();

        foreach (string str in list.Values)
        {
            if (str.ToLower().StartsWith(prefixText.ToLower()))
            {
                responses.Add(str);
                if (responses.Count >= count)
                    break;
            }
        }
        return responses.ToArray();
    }
    
}

