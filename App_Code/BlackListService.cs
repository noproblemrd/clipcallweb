﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;

/// <summary>
/// Summary description for BlackListService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class BlackListService : System.Web.Services.WebService {
    const int MAX_SUGGESTS = 10;
    public BlackListService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public List<string> SuggestList(string str)
    {
        List<string> list = BlackListV;
        IEnumerable<string> query = (from x in list
                                     where x.ToLower().StartsWith(str.ToLower())
                                     select x).Take(MAX_SUGGESTS);
        return query.ToList();
    }
    [WebMethod(true)]
    public string CheckOne(string str)
    {
        List<string> list = BlackListV;
        string query = (from x in list
                                     where x.ToLower().StartsWith(str.ToLower())
                                     select x).FirstOrDefault();
        return query;
    }
    public void SetBlackList(string _WebReference)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_WebReference);
        WebReferenceSite.SearchBlackListRequest _search = new WebReferenceSite.SearchBlackListRequest();
       
       
        WebReferenceSite.ResultOfSearchBlackListResponse _response = null;
        try
        {
            _response = _site.SearchBlackList(_search);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, _WebReference);
            return;
        }
        if (_response.Type == WebReferenceSite.eResultType.Failure)
        {            
            return;
        }
        List<string> list = new List<string>();
        foreach (string str in _response.Value.PhoneNumbers)
        {
            if (!list.Contains(str))
                list.Add(str);
        }
        BlackListV = list;
    }
    public void SetBlackList()
    {
        if (Session["Site"] == null)
            BlackListV = new List<string>();
        string _WebReference = ((SiteSetting)Session["Site"]).GetUrlWebReference;
        SetBlackList(_WebReference);
        if (Session["_BlackList"] == null)
            BlackListV = new List<string>();
    }
    List<string> BlackListV
    {
        get
        {
            if (Session["_BlackList"] == null)
                SetBlackList();
            return (List<string>)Session["_BlackList"];
        }
        set
        {
            Session["_BlackList"] = value;
        }
    }
    
}

