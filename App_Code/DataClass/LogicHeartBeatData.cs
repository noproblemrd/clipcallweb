﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LogicHeartBeatData
/// </summary>
public class LogicHeartBeatData
{
    public Guid OriginId { get; set; }
    public DateTime Date { get; set; }
    public int Hits { get; set; }
    public DateTime CreatedOn { get; set; }
	public LogicHeartBeatData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}