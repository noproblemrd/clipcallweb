﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for DataResultRequest
/// </summary>
public class DataResultRequest : DataResult
{
	public DataResultRequest():base()
	{
		//
		
		//
	}
    public int RecordToPay { get; set; }
    public int RecordNotToPay { get; set; }
}