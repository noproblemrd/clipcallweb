﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AdEngineCampaignData
/// </summary>
[Serializable()]
public class AdEngineCampaignData
{
    public Guid ID { get; set; }
    public string URL { get; set; }
    public string Name { get; set; }
    public int width { get; set; }
    public int height { get; set; }
	public AdEngineCampaignData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public AdEngineCampaignData(Guid id, string url, string name, int width, int height)
    {
        this.ID = id;
        this.URL = url;
        this.Name = name;
        this.width = width;
        this.height = height;
    }
}