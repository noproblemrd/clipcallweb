﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpecialInjectionData
/// </summary>
public class SpecialInjectionData
{
    public Guid originId { get; set; }
    public Guid injectionId { get; set; }
    
	public SpecialInjectionData()
	{
		

	}
    public SpecialInjectionData(Guid originId, Guid injectionId)
    {
        this.originId = originId;
        this.injectionId = injectionId;
    }
}