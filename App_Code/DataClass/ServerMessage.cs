﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ServerMessage
/// </summary>
public class ServerMessage
{
     public string Message {  get;  private set; }
     List<string> Links {  get;  set; }
    public ServerMessage(XElement xelem)
	{

        this.Message = xelem.Element("Message").Value;
        IEnumerable<string> result = from x in xelem.Elements("Links")
                                     select x.Value;
        Links = result.ToList();
	}
    public string GetHtmlMessage(params string[] scripts)
    {
        string mainmessage = Message;
  //      List<string> list;
        int i = 0;
        for (; i < Links.Count && i < scripts.Length; i++)
        {
            
      //      list = new List<string>(Message.Split(new string[] { "{" + i + "}" }, StringSplitOptions.None));
      //      mainmessage += list[0];

            string anchor = "<a href='" + scripts[i] + "'>" + Links[i] + "</a>";
            mainmessage = mainmessage.Replace("{" + i + "}", anchor);
    //        if (list.Count > 1)
    //            Message = list[1];
        }
        i--;
        if (i == -1)
        {
            mainmessage = Message;
            for (i = 0; i < scripts.Length; i++)            
                mainmessage = mainmessage.Replace("{" + i + "}", scripts[i]);
           return mainmessage;
        }
  //      list = new List<string>(Message.Split(new string[] { "{" + i + "}" }, StringSplitOptions.None));
 //       if (list.Count > 1)
 //           mainmessage += list[1];
        return mainmessage;
    }
    public string GetHtmlMessageJavascriptEncode(params string[] scripts)
    {
        string mainmessage = HttpUtility.JavaScriptStringEncode(Message);
        int i = 0;
        for (; i < Links.Count && i < scripts.Length; i++)
        {

            string anchor = "<a href='" + scripts[i] + "'>" + Links[i] + "</a>";
            mainmessage = mainmessage.Replace("{" + i + "}", anchor);
        }
        i--;
        if (i == -1)
        {
            mainmessage = Message;
            for (i = 0; i < scripts.Length; i++)
                mainmessage = mainmessage.Replace("{" + i + "}", scripts[i]);
            return mainmessage;
        }
        return mainmessage;
    }

}