﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for SoldCallReportData
/// </summary>
public delegate DataTable func_groupBy(WebReferenceReports.Reports _report, WebReferenceReports.CallsReportRequest _request,
    out PagingData _pagingData);
[Serializable]
public class SoldCallReportData
{
    public eGroupBySoldCallReport GroupBy { get; set; }
    public WebReferenceReports.CallsReportRequest _Request { get; set; }
    public func_groupBy _func { get; set; }

	public SoldCallReportData()
	{
		//
		
		//
	}
}