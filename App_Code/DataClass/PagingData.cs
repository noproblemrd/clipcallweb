﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PagingData
/// </summary>
public class PagingData
{
    public decimal Sum { get; set; }
    public int TotalPages { get; set; }
    public int TotalRows { get; set; }
    public int CurrentPage { get; set; }
	public PagingData()
	{
		//
		
		//
	}
}