﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LocationData
/// </summary>
[Serializable()]
public class LocationData
{
    public decimal Lat { get; set; }
    public decimal Lng { get; set; }
    public decimal Radius { get; set; }
    public Guid RegionId { get; set; }
    public bool SelectAll { get; set; }
    public bool IsMoreSelected { get; set; }
    public string FullAddress { get; set; }
    public LocationDataNamedRegions[] namedRegions { get; set; }

    public string SelectedArea { get; set; }
    public string SelectedAreaValue { get; set; }

	public LocationData()
	{
		//
		
		//
	}
}


/*
<Area RegionId=""ALL"" Name=""ALL"" Level=""ALL"" Selected=""true"" />
<Area RegionId=""02b5cbe3-3a2a-e111-bd7e-001517d10f6e"" Name=""New York"" Level=""3"" Selected=""false"" />
<Area RegionId=""cf41637c-392a-e111-bd7e-001517d10f6e"" Name=""Manhattan"" Level=""2"" Selected=""false"" />
<Area RegionId=""f031ce5b-392a-e111-bd7e-001517d10f6e"" Name=""NY"" Level=""1"" Selected=""false"" />
*/

[Serializable()]
public class LocationDataNamedRegions
{
    public string RegionId;
    public string Name;
    public string Level;
}