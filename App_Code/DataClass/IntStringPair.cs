﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IntStringPair
/// </summary>
[Serializable()]
public class IntStringPair
{
    public int num { get; set; }
    public string str { get; set; }
	public IntStringPair()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}