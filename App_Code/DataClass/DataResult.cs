﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for DataResult
/// </summary>
public class DataResult
{
    public DataTable data { get; set; }
    public decimal Sum { get; set; }
    public int TotalPages { get; set; }
    public int TotalRows { get; set; }
    public int CurrentPage { get; set; }
	public DataResult()
	{
		//
		
		//
	}
}