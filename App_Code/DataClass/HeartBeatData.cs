﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HeartBeatData
/// </summary>
public class HeartBeatData
{
    public Guid InjectionId { get; set; }
    public Guid OriginId { get; set; }
    public DateTime Date { get; set; }
    public int Hits { get; set; }
    public DateTime CreatedOn { get; set; }
	public HeartBeatData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}