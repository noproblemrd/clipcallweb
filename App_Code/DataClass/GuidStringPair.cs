﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GuidStringPair
/// </summary>
public class GuidStringPair
{
    public Guid Id { get; set; }
    public string Name { get; set; }
	public GuidStringPair()
	{
		

	}
    public GuidStringPair(Guid Id, string Name)
    {
        this.Id = Id;
        this.Name = Name;
    }
}