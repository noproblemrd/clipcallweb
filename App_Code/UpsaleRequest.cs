﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UpsaleRequest
/// </summary>
[Serializable()]
public class UpsaleRequest
{
    public Guid ExpertiserId { get; set; }
    public DateTime from_date { get; set; }
    public DateTime to_date { get; set; }
	public UpsaleRequest(Guid _expertiserId, DateTime _from, DateTime _to)
	{
        ExpertiserId = _expertiserId;
        from_date = _from;
        to_date = _to;
	}
}