﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for MapService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class MapService : System.Web.Services.WebService {

    public MapService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public string GetNeighborhood(double lat, double lng, double radius)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        
        string result = string.Empty;
        try
        {
            result = _supplier.GetCitiesByCoordinate(_user.Get_Guid, new decimal(radius), new decimal(lng), new decimal(lat));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return "faild";
        }
         XDocument xdoc = XDocument.Parse(result);
        if(xdoc.Element("GetCitiesByCoordinate") != null)
            return "faild";

        Dictionary<Guid, string> dic = new Dictionary<Guid, string>();
        DicPlaces = dic;
        
        foreach (XElement _elem in xdoc.Element("Areas").Elements())        
            dic.Add(new Guid(_elem.Attribute("RegionId").Value), _elem.Attribute("Name").Value);

        NeighborhoodControl control = new NeighborhoodControl(dic);
        return control.GetControl().ToString();            
    }
    [WebMethod(true)]
    public string SetDetails(string lat, string lng, string radius, string address, string name, string email, string LargePlace)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        string result = "";
        decimal _lat, _lng, _radius;
        if (!decimal.TryParse(lat, out _lat) || !decimal.TryParse(lng, out _lng) || !decimal.TryParse(radius, out _radius))
            return new JavaScriptSerializer().Serialize(new { status = "error" });
        Guid RegionId = Guid.Empty;
        bool SelectAll = false;
        if (Utilities.IsGUID(LargePlace))
            RegionId = new Guid(LargePlace);
        else if (LargePlace == "all")
            SelectAll = true;
        Guid SupplierId = RegisterPage.GetSupplierId(HttpContext.Current);
        try
        {
            result = _supplier.SetCitiesByCoordinate(SupplierId, _radius, _lng, _lat, address, name, email, RegionId, SelectAll, true, "", true);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return new JavaScriptSerializer().Serialize(new { status = "error" });
        }
        XDocument doc = XDocument.Parse(result);
        if (doc.Element("SetCitiesByCoordinate").Element("Error") != null)
            return new JavaScriptSerializer().Serialize(new { status = "error" });
        string _status = doc.Element("SetCitiesByCoordinate").Element("Status").Value.ToLower();
        if (_status == "success")
        {
            bool IsApproved = (doc.Element("SetCitiesByCoordinate").Element("invitationApproved").Value.ToLower() == true.ToString().ToLower());
      //      double _balance = Convert.ToDouble(Utilities.GetBalance(SupplierId, SiteSettingV.GetUrlWebReference));
         //  UserMangement um = new UserMangement(sup//  _user.UpgradePreSupplier(name, email, _balance);
            return new JavaScriptSerializer().Serialize(new { status = "success", IsApproved = IsApproved });
        }
        return new JavaScriptSerializer().Serialize(new { status = "EmailDuplicate" });
    }


    [WebMethod(true)]
    public string SetDetails2(AreaCovers areasCover)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        WebReferenceSupplier.SetCoverAreasRequest SetCoverAreasRequest=new WebReferenceSupplier.SetCoverAreasRequest();
        WebReferenceSupplier.ResultOfSetCoverAreasResponse resultOfSetCoverAreasResponse = new WebReferenceSupplier.ResultOfSetCoverAreasResponse();

        SetCoverAreasRequest.SupplierId = areasCover.supplierId;

        List<WebReferenceSupplier.AreaRequest> listArea = new List<WebReferenceSupplier.AreaRequest>();
        WebReferenceSupplier.AreaRequest area; 
        
        for (int i = 0; i < areasCover.areas.Count(); i++)
        {
            area=new WebReferenceSupplier.AreaRequest();
            area.Order=areasCover.areas[i].order;
            area.Address = areasCover.areas[i].address;
            area.Latitude = areasCover.areas[i].latitude;
            area.Longitude = areasCover.areas[i].longitude;
            area.NumberOfZipcodes = areasCover.areas[i].numberOfZipcodes;
            area.Radius = areasCover.areas[i].radius;
            area.SelectedArea1Id = areasCover.areas[i].selectedArea1Id;
            area.SelectedArea1Name = areasCover.areas[i].selectedArea1Name;
            area.SelectedArea2Id = areasCover.areas[i].selectedArea2Id;
            area.SelectedArea2Name = areasCover.areas[i].selectedArea2Name;
            area.SelectedArea3Id = areasCover.areas[i].selectedArea3Id;
            area.SelectedArea3Name = areasCover.areas[i].selectedArea3Name;
            listArea.Add(area);
        }


        SetCoverAreasRequest.Areas = listArea.ToArray();
        

        try
        {
            resultOfSetCoverAreasResponse=_supplier.SetCoverAreas__Registration2014(SetCoverAreasRequest);            

            if (resultOfSetCoverAreasResponse.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed in method SetCoverAreas__Registration2014 with supplierId " + areasCover.supplierId + " message: " + resultOfSetCoverAreasResponse.Messages[0]), SiteSettingV);


                var resultParams = new { result = "failed" };

                System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

                return (jss.Serialize(resultParams));
            }

            else
            {
                var resultParams = new { result = "success" };

                System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

                return (jss.Serialize(resultParams));
                
            }


        }

        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);

            var resultParams = new { result = "failed" };

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            return (jss.Serialize(resultParams));
        }

        
    }
      



    [WebMethod(true)]
    public string GetLargeAreas(string lat, string lng)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        WebReferenceSupplier.ResultOfListOfRegionMinData result = null;
        decimal _lat, _lng;
        List<LargeAreasDetail> list = new List<LargeAreasDetail>();
        if (!decimal.TryParse(lat, out _lat) || !decimal.TryParse(lng, out _lng))
            return "faild";
        try
        {
            result = _supplier.GetMoreMapData(_lng, _lat);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return "faild";
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return "faild";
        string strResult = string.Empty;

        foreach (WebReferenceSupplier.RegionMinData rmd in result.Value)
        {
     //       strResult += rmd.Id.ToString() + ";;;" + rmd.Level + ";;;" + rmd.Name + "$$$";
            LargeAreasDetail lad = new LargeAreasDetail() { _id = rmd.Id.ToString(), level = rmd.Level, name = rmd.Name };
            list.Add(lad);
            
        }
        if (strResult.Length > 1)
            strResult = strResult.Substring(0, strResult.Length - 3);
        list.Sort(delegate(LargeAreasDetail p1, LargeAreasDetail p2) { return p1.level.CompareTo(p2.level); });
     //   return strResult;
        JavaScriptSerializer jss = new JavaScriptSerializer();
        return jss.Serialize(list);
    }

    [WebMethod(true)]
    public string GetLargeAreas2(string lat, string lng)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        WebReferenceSupplier.ResultOfListOfRegionMinData result = null;
        decimal _lat, _lng;
        List<LargeAreasDetail> list = new List<LargeAreasDetail>();
        if (!decimal.TryParse(lat, out _lat) || !decimal.TryParse(lng, out _lng))
            return "faild";
        try
        {
            result = _supplier.GetMoreMapData(_lng, _lat);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return "faild";
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return "faild";
       

        int numOfZipCodes;

        foreach (WebReferenceSupplier.RegionMinData rmd in result.Value)
        {
            //       strResult += rmd.Id.ToString() + ";;;" + rmd.Level + ";;;" + rmd.Name + "$$$";
            if(rmd.Level==3)
                numOfZipCodes=rmd.NumberOfZipcodes;
            else
                numOfZipCodes=0;

            LargeAreasDetail lad = new LargeAreasDetail() { _id = rmd.Id.ToString(), level = rmd.Level, name = rmd.Name, numZipcodes = numOfZipCodes };

            
            list.Add(lad);

        }
      
        //list.Sort(delegate(LargeAreasDetail p1, LargeAreasDetail p2) { return p1.level.CompareTo(p2.level); });
        
        JavaScriptSerializer jss = new JavaScriptSerializer();
        return jss.Serialize(list);
    }

    //, numZipcodes = rmd.NumberOfZipcodes

    [WebMethod(true)]
    public string getNumberOfZipCode(string selectedArea, decimal lat, decimal lng, decimal radius)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfInt32 resultOfInt32 = new WebReferenceSupplier.ResultOfInt32();

        string returnNumZipCodes;       

        if (selectedArea == "ALL")
        {
            returnNumZipCodes = "41751";
        }
        else
        {
            try
            {
                Guid outReginoId;
                if (Guid.TryParse(selectedArea, out outReginoId))
                    resultOfInt32 = _supplier.GetNumberOfZipcodes(new Guid(selectedArea), 0, 0, radius);
                else
                    resultOfInt32 = _supplier.GetNumberOfZipcodes(Guid.Empty, lat, lng, radius);

                if (resultOfInt32.Type == WebReferenceSupplier.eResultType.Failure)
                {
                    returnNumZipCodes = "failed";
                }

                else
                {
                    returnNumZipCodes = resultOfInt32.Value.ToString();
                }
            }

            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, SiteSettingV);
                returnNumZipCodes="faild";
            }
        }

        return returnNumZipCodes;
      
    }


    UserMangement _user
    {
        get { return (Session["UserGuid"] == null) ? new UserMangement() : (UserMangement)Session["UserGuid"]; }
        set { Session["UserGuid"] = value; }
    }
    SiteSetting SiteSettingV
    {
        get { return (Session["Site"] == null) ? null : (SiteSetting)Session["Site"]; }
        set { Session["Site"] = value; }
    }
    Dictionary<Guid, string> DicPlaces
    {
        get { return (Session["DicPlaces"] == null) ? new Dictionary<Guid, string>() : (Dictionary<Guid, string>)Session["DicPlaces"]; }
        set { Session["DicPlaces"] = value; }
    }

}
public class LargeAreasDetail
{
    public string _id { get; set; }
    public int level { get; set; }
    public string name { get; set; }
    public int numZipcodes { get; set; }

    public LargeAreasDetail()
    {
    }
}

public class AreaCovers
{
    public Guid supplierId { get; set; }
    public AreaCover[] areas { get; set; }
}

public class AreaCover
{
    public int order { get; set; }
    public string address { get; set; }
    public decimal latitude { get; set; }
    public decimal longitude { get; set; }
    public string radius { get; set; }
    public int numberOfZipcodes { get; set; }
    public string selectedArea1Name { get; set; }
    public string selectedArea1Id { get; set; }
    public string selectedArea2Name { get; set; }
    public string selectedArea2Id { get; set; }
    public string selectedArea3Name { get; set; }
    public string selectedArea3Id { get; set; }
}



