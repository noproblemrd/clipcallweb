﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for PageClipCallLandingPage
/// </summary>
public class PageClipCallLandingPage : Page
{
    protected const string GENERAL_REDIRECT = "http://www.clipcall.it/";
	public PageClipCallLandingPage()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    protected string IsRobot()
    {
        return GetIsRobot().ToString().ToLower();
    }
    protected string useJwPlayer()
    {
        return UseJwPlayer().ToString().ToLower();
    }
    private bool GetIsRobot()
    {
        System.Collections.Specialized.NameValueCollection headers = Request.Headers;
        for (int i = 0; i < headers.Count; i++)
        {
            string key = headers.GetKey(i);


            string value = headers.Get(i);
            if (key.ToLower() == "from")
                if (value.ToLower().Contains("googlebot"))
                    return true;
            if (key.ToLower() == "referer")
                if (value.ToLower().Contains("www.google."))
                    return true;
        }
        string userAgent = Request.UserAgent.ToLower();
        if (userAgent.Contains("googlebot"))
            return true;
        if (userAgent == "bitlybot")
            return true;
        return false;
    }
    private bool UseJwPlayer()
    {
        // return false;
        string userAgent = Request.UserAgent.ToLower();
        if (userAgent.Contains("android"))
        {
            if (userAgent.Contains("samsungbrowser"))
                return true;
            if (userAgent.Contains("opr/"))
                return true;
            return false;
        }
        else if (userAgent.Contains("iphone") || userAgent.Contains("ipdo"))
            return false;
        return true;
    }
}