﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using System.Web.UI;
using System.Reflection;
using System.Text;
using System.IO;

/// <summary>
/// Summary description for DapazService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class DapazService : System.Web.Services.WebService {
    private const string BASE_PATH = "~/Publisher/DpzControls/";
    public DapazService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public string GetVirtualNumber(string PageIndex)
    {
        JavaScriptSerializer jss = new JavaScriptSerializer();
        int _PageIndex;
        if (!int.TryParse(PageIndex, out _PageIndex))
            _PageIndex = 1;
        else if (_PageIndex < 1)
            _PageIndex = 0;
        if (_PageIndex == 0 || GetSessionVirtualNumbers.Length == 0)
        {
            _PageIndex = 1;
            WebReferenceSite.GetAvailableVirtualNumbersResponse data = DpzUtilities.GetAvailableVirtualNumbers((SiteSetting)Session["Site"]);
            GetSessionVirtualNumbers = data.NumbersList;
            DpzVirtualNumbers dvn = new DpzVirtualNumbers(data.NumbersList, _PageIndex);
            return jss.Serialize(dvn);
        }
        DpzVirtualNumbers dvn2 = new DpzVirtualNumbers(GetSessionVirtualNumbers, _PageIndex);
        return jss.Serialize(dvn2);
    }
    [WebMethod(true)]
    public string GetSupplirDetails(string supplierid)
    {
        Session["dpz_audit"] = null;
        GetSessionVirtualNumbers = null;
        Guid SupplierId;
        if (!Guid.TryParse(supplierid, out SupplierId))
            return string.Empty;
  //      SupplierAudit audit = new SupplierAudit(SupplierId);
        SiteSetting ss = (SiteSetting)Session["Site"];
        WebReferenceSite.RetrieveGoldenNumberAdvertiserResponse result = DpzUtilities.GetSupplierDetails(ss, SupplierId);
        DpzSupplier _supplier = new DpzSupplier();
        _supplier.ResponseStatus = result.ResponseStatus.ToString();
        if (result.ResponseStatus != WebReferenceSite.eRetrieveGoldenNumberAdvertiserStatus.OK)
            return new JavaScriptSerializer().Serialize(_supplier);
        /*
        WebReferenceSite.eRetrieveGoldenNumberAdvertiserStatus.OK;
        WebReferenceSite.eRetrieveGoldenNumberAdvertiserStatus.SupplierDoesNotExist;
        WebReferenceSite.eRetrieveGoldenNumberAdvertiserStatus.SupplierIsNotDapazGoldenNumberAdvertiser;
         * */
        UserMangement um = (UserMangement)Session["UserGuid"];
        SupplierAudit audit = new SupplierAudit(SupplierId, um.Get_Guid, result.Name, "SearchSuppliers.aspx", um.User_Name, ss.siteLangId, "GoldenNumberAdvertiser");
        _supplier.HeadingName = result.ExpertiseName;
        audit.Add("txt_Heading", (result.ExpertiseName == null) ? string.Empty : result.ExpertiseName);
        _supplier.Region = result.Region;       
        _supplier.IsActive = result.IsActive;
        audit.Add("cb_IsActive", result.IsActive.ToString());
        _supplier.BizId = result.BizId;
        audit.Add("txt_BizId", (result.BizId == null) ? string.Empty : result.BizId);
        _supplier.Name = result.Name;
        audit.Add("txt_Name", (result.Name == null) ? string.Empty : result.Name);
        _supplier.PhoneNumber = result.PhoneNumber;
        audit.Add("txt_PhoneNumber", (result.PhoneNumber == null) ? string.Empty : result.PhoneNumber);
        _supplier.SmsNumber = result.SmsNumber;
        audit.Add("txt_SmsNumber", (result.SmsNumber == null) ? string.Empty : result.SmsNumber);
        _supplier.SupplierId = result.SupplierId;        
        _supplier.VirtualNumber = result.VirtualNumber;
        audit.Add("txt_VirtualNumber", (result.VirtualNumber == null) ? string.Empty : result.VirtualNumber);
        _supplier.SendSMS = !result.DoNotSendSMS;
        audit.Add("cb_SendSms", (!result.DoNotSendSMS).ToString());
        _supplier.Status = result.DapazStatus.ToString();
        audit.Add("txt_GoldenStatus", EditTranslateDB.GetControlTranslate(ss.siteLangId, "DapazStatus", result.DapazStatus.ToString()));
        _supplier.FundsStatus = result.FundsStatus.ToString();
        audit.Add("ddl_FundsStatus", EditTranslateDB.GetControlTranslate(ss.siteLangId, "FundsStatus", result.FundsStatus.ToString()));
        _supplier.MoveToStatuses = DpzUtilities.GetStatusOptionsChange(result.DapazStatus);
        Session["dpz_audit"] = audit;
        JavaScriptSerializer jss = new JavaScriptSerializer();
        return jss.Serialize(_supplier);
    //    if(result.ResponseStatus== WebReferenceSite.eRetrieveGoldenNumberAdvertiserStatus.
        
    }
    [WebMethod(true)]
    public string SetSupplirDetails(string supplierid, string BizId, string IsActive, string name,
                        string PhoneNumber, string SmsNumber, string VirtualNumber, string SendSMS, 
                        string MoveTo, string FundsStatus)
    {
        bool _IsActive = (IsActive.ToLower() == "true");
        bool _SendSMS = (SendSMS.ToLower() == "true");
        JavaScriptSerializer jss = new JavaScriptSerializer();
        if (string.IsNullOrEmpty(supplierid))
        {
            WebReferenceSite.CreateGoldenNumberAdvertiserResponse data = DpzUtilities.CreateSupplier((SiteSetting)Session["Site"], BizId, _IsActive, name, PhoneNumber, SmsNumber, VirtualNumber, _SendSMS, MoveTo, FundsStatus);
            /*
            WebReferenceSite.eCreateGoldenNumberAdvertiserStatus.OK;
            WebReferenceSite.eCreateGoldenNumberAdvertiserStatus.VirtualNumberNotAvailable;
             * data.ResponseStatus;
            data.SupplierId
             * */

            if (data == null)
                return string.Empty;
            var result = new { ResponseStatus = data.ResponseStatus.ToString(), SupplierId = data.SupplierId.ToString() };
            
            return jss.Serialize(result);
        }
        else
        {
            Guid SupplierId;
            if (!Guid.TryParse(supplierid, out SupplierId))
                return string.Empty;

            WebReferenceSite.UpdateGoldenNumberAdvertiserResponse data = DpzUtilities.SetSupplierDetails(Session, SupplierId, BizId, _IsActive, name, PhoneNumber, SmsNumber, VirtualNumber, _SendSMS, MoveTo, FundsStatus);
            if (data == null)
                return string.Empty;
            /*
            WebReferenceSite.eUpdateGoldenNumberAdvertiserStatus.SupplierDoesNotExist;
            WebReferenceSite.eUpdateGoldenNumberAdvertiserStatus.OK;
            WebReferenceSite.eUpdateGoldenNumberAdvertiserStatus.SupplierIsNotDapazGoldenNumberAdvertiser;
            WebReferenceSite.eUpdateGoldenNumberAdvertiserStatus.VirtualNumberNotAvailable;
             * */
            var result = new { ResponseStatus = data.ResponseStatus.ToString(), SupplierId = supplierid };
            return jss.Serialize(result);
        }
    }
    [WebMethod(true)]
    public string GetSupplierList(string str)
    {
        WebReferenceReports.DapazReportsSupplierContainer[] result = DpzUtilities.GetSupplierByStr((SiteSetting)Session["Site"], str);
        List<DpzSupplier> list = new List<DpzSupplier>();
        foreach (WebReferenceReports.DapazReportsSupplierContainer data in result)
        {
            DpzSupplier ds = new DpzSupplier();
            ds.BizId = data.BizId;
            ds.Name = data.Name;
            ds.SupplierId = data.Id;
            list.Add(ds);
        }
        ContainerPages<DpzSupplier> cp = new ContainerPages<DpzSupplier>(list.ToArray());
        ContainerPageSupplierList = cp;
        ContainerPageResult<DpzSupplier> cpr = cp.GetPage(1);
        return new JavaScriptSerializer().Serialize(cpr);
    }
    [WebMethod(true)]
    public string GetSupplierListPage(int IndexPage)
    {
        if (IndexPage < 1)
            IndexPage = 1;
        ContainerPages<DpzSupplier> cp = ContainerPageSupplierList;
        if (cp == null)
            return string.Empty;
        ContainerPageResult<DpzSupplier> cpr = cp.GetPage(IndexPage);
        return new JavaScriptSerializer().Serialize(cpr);
    }
    [WebMethod(true)]
    public string GetDistributionReport()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl =
             (UserControl)page.LoadControl(BASE_PATH + "CallDistributionReportControl.ascx");

        MethodInfo _translate = ctl.GetType().GetMethod("SetDataByPage");
        bool IsBindData = (bool)_translate.Invoke(ctl, null);
        if (!IsBindData)
            return "done";
        ctl.EnableViewState = false;

        //Render control to string
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }
    [WebMethod(true)]
    public string GetCallReport()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl =
             (UserControl)page.LoadControl(BASE_PATH + "CallReportControl.ascx");
        
        MethodInfo _translate = ctl.GetType().GetMethod("SetDataByPage");
        bool IsBindData = (bool)_translate.Invoke(ctl, null);
        if (!IsBindData)
            return "done";
        ctl.EnableViewState = false;

        //Render control to string
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }
    [WebMethod(true)]
    public string GetSourceCallReport()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl =
             (UserControl)page.LoadControl(BASE_PATH + "SourceCallReport.ascx");

        MethodInfo _translate = ctl.GetType().GetMethod("SetDataByPage");
        bool IsBindData = (bool)_translate.Invoke(ctl, null);
        if (!IsBindData)
            return "done";
        ctl.EnableViewState = false;

        //Render control to string
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }
    [WebMethod(true)]
    public string GetPPCControlReport()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl =
             (UserControl)page.LoadControl(BASE_PATH + "PPAControlReportControl.ascx");

        MethodInfo _translate = ctl.GetType().GetMethod("SetDataByPage");
        bool IsBindData = (bool)_translate.Invoke(ctl, null);
        if (!IsBindData)
            return "done";
        ctl.EnableViewState = false;

        //Render control to string
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }
    [WebMethod(true)]
    public string CreateExcel()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl;
        MethodInfo GetCompleteTable;
        switch (ReportExecV)
        {
            case (eDpzReportType.NULL):
                return string.Empty;
            case (eDpzReportType.CallDistributionReportByDay):
            case (eDpzReportType.CallDistributionReportByHour):
            case (eDpzReportType.CallDistributionReportByMonth):               
                ctl =
                     (UserControl)page.LoadControl(BASE_PATH + "CallDistributionReportControl.ascx");
                GetCompleteTable = ctl.GetType().GetMethod("CreateExcel");
                return (string)GetCompleteTable.Invoke(ctl, null);                
            case (eDpzReportType.CallReport):
                ctl =
                     (UserControl)page.LoadControl(BASE_PATH + "CallReportControl.ascx");
                GetCompleteTable = ctl.GetType().GetMethod("CreateExcel");
                return (string)GetCompleteTable.Invoke(ctl, null);  
            case (eDpzReportType.ConversationSourceReport):
               ctl =
                     (UserControl)page.LoadControl(BASE_PATH + "SourceCallReport.ascx");
                GetCompleteTable = ctl.GetType().GetMethod("CreateExcel");
                return (string)GetCompleteTable.Invoke(ctl, null);  
        }
        return string.Empty;
    }
    [WebMethod(true)]
    public string CreateExcelPPAControlReport()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "PPAControlReportControl.ascx");
        MethodInfo GetCompleteTable = ctl.GetType().GetMethod("CreateExcel");
        return (string)GetCompleteTable.Invoke(ctl, null);
       
        return string.Empty;
    }
    [WebMethod]
    public string GetCreditGuardUrl(int PaymentId, string cc_name, string cc_expier, string cc_id)
    {
        CreditGuardUrlResponse _response = null;

    //    _response = DpzUtilities.GetCreditGuardUrl(PaymentId, cc_name, cc_expier, cc_id);
        return new JavaScriptSerializer().Serialize(_response);
        //new JavaScriptSerializer().Serialize(rrc)
    }
    [WebMethod(true)]
    public string GetCreditGuardToken(int PaymentId)
    {
        SiteSetting ss = (Session["Site"] == null) ? new SiteSetting() : (SiteSetting)Session["Site"];
        TokenAmount ta = null;// DpzUtilities.GetCreditGuardToken(PaymentId, ss.siteLangId);
        return new JavaScriptSerializer().Serialize(ta);
    }
    [WebMethod(true)]
    public string ExecZapPayment(int PaymentId)
    {
        UserMangement um = (Session["UserGuid"] == null) ? new UserMangement() : (UserMangement)Session["UserGuid"];
        SiteSetting ss = (Session["Site"] == null) ? new SiteSetting() : (SiteSetting)Session["Site"];
        return new JavaScriptSerializer().Serialize(DpzUtilities.ExecZapPayment(PaymentId, um, ss));// ? "success" : "faild";
    }
    
    
    string[] GetSessionVirtualNumbers
    {
        get { return (Session["VirtualNumbers"] == null) ? new string[] { } : (string[])Session["VirtualNumbers"]; }
        set { Session["VirtualNumbers"] = value; }
    }
    ContainerPages<DpzSupplier> ContainerPageSupplierList
    {
        get { return (Session["ContainerPageSupplier"] == null) ? null : (ContainerPages<DpzSupplier>)Session["ContainerPageSupplier"]; }
        set { Session["ContainerPageSupplier"] = value; }
    }
    eDpzReportType ReportExecV
    {
        get { return (Session["ReportExecV"] == null) ? eDpzReportType.NULL : (eDpzReportType)Session["ReportExecV"]; }
        set { Session["ReportExecV"] = value; }
    }
    /*
    DataTable DistributionReportV
    {
        get { return (Session["DistributionReport"] == null) ? null : (DataTable)Session["DistributionReport"]; }        
    }
   */
    
}
