using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Net;

/// <summary>
/// Summary description for WebServiceConfig
/// </summary>
public class WebServiceConfig
{
	public WebServiceConfig()
	{
		//
		
		//
	}
    public static WebReferenceSite.Site GetSiteReference(PageSetting _page)
    {
        
        WebReferenceSite.Site _site = new WebReferenceSite.Site();
        if (_page.siteSetting != null && !string.IsNullOrEmpty(_page.siteSetting.GetUrlWebReference))
            _site.Url = _page.siteSetting.GetUrlWebReference + "Site.asmx";
       
        return _site;
    }
    static string GetWebReferenceURLBySiteId(string SiteId)
    {
        if (string.IsNullOrEmpty(SiteId))
            return string.Empty;
        string _url = string.Empty;
        string command = "EXEC dbo.GetSiteDetailBySiteNameId @SiteId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                cmd.Parameters.AddWithValue("@SiteId", SiteId);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                    _url = (reader.IsDBNull(7)) ? "" : reader.GetString(7);

            }
            conn.Close();
        }
        return _url;
    }
    public static WebReferenceSite.Site GetSiteReference(Page _page, string SiteId)
    {
        WebReferenceSite.Site _site = new WebReferenceSite.Site();
        if (_page == null)
        {
   //         dbug_log.ExceptionLog(new Exception(), GetURL(SiteId) + "Site.asmx");
            _site.Url = GetURL(SiteId) + "Site.asmx";
            return _site;
        }
        string url = (_page.Session["WebReferenceURL"] == null) ? string.Empty : (string)_page.Session["WebReferenceURL"];
        if (string.IsNullOrEmpty(url))
        {
            /*
            string command = "EXEC dbo.GetSiteDetailBySiteNameId @SiteId";
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteId", SiteId);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                    url = (reader.IsDBNull(7)) ? "" : reader.GetString(7);
                else
                {
                    _page.Session["WebReferenceURL"] = string.Empty;
                    return _site;
                }
                conn.Close();
            }
            _page.Session["WebReferenceURL"] = url;
             * */
            url = GetWebReferenceURLBySiteId(SiteId);
            _page.Session["WebReferenceURL"] = url;
            if (string.IsNullOrEmpty(url))
                return _site;
            
        }

       
        _site.Url = url + "Site.asmx";
        return _site;
    }
    
    public static WebReferenceSite.Site GetSiteReference(string WebReferenceURL)
    {
        WebReferenceSite.Site _site = new WebReferenceSite.Site();

        _site.Url = WebReferenceURL + "Site.asmx";

        return _site;
    }
    public static WebReferenceSupplier.Supplier GetSupplierReference(string WebReferenceURL)
    {
        WebReferenceSupplier.Supplier _supplier = new WebReferenceSupplier.Supplier();

        _supplier.Url = WebReferenceURL + "Supplier.asmx";

        return _supplier;
    }
    public static WebReferenceSupplier.Supplier GetSupplierReference(PageSetting _page)
    {
        WebReferenceSupplier.Supplier _supplier = new WebReferenceSupplier.Supplier();
        if (_page.siteSetting != null && !string.IsNullOrEmpty(_page.siteSetting.GetUrlWebReference))
            _supplier.Url = _page.siteSetting.GetUrlWebReference + "Supplier.asmx";
       
        return _supplier;
    }
    public static WebReferenceSupplier.Supplier GetSupplierReference(Page _page, string SiteId)
    {
        WebReferenceSupplier.Supplier _supplier = new WebReferenceSupplier.Supplier();
        if (_page == null)
        {
            _supplier.Url = GetURL(SiteId) + "Supplier.asmx";
            return _supplier;
        }
        string url = (_page.Session["WebReferenceURL"] == null) ? string.Empty : (string)_page.Session["WebReferenceURL"];
        if (string.IsNullOrEmpty(url))
        {
            url = GetWebReferenceURLBySiteId(SiteId);
            _page.Session["WebReferenceURL"] = url;
            if (string.IsNullOrEmpty(url))
                return _supplier;
        }
        _supplier.Url = url + "Supplier.asmx";
        return _supplier;
    }
    public static WebReferenceCustomer.CustomersService GetCustomerReference(PageSetting _page)
    {
        WebReferenceCustomer.CustomersService _customer = new WebReferenceCustomer.CustomersService();
        if (_page.siteSetting != null && !string.IsNullOrEmpty(_page.siteSetting.GetUrlWebReference))
            _customer.Url = _page.siteSetting.GetUrlWebReference + "CustomersService.asmx";
        return _customer;
    }
    public static WebReferenceCustomer.CustomersService GetCustomerReference(Page _page, string SiteId)
    {
        
        WebReferenceCustomer.CustomersService _customer = new WebReferenceCustomer.CustomersService();
        if (_page == null)
        {
 //           dbug_log.ExceptionLog(new Exception(), "2= " + SiteId);
            _customer.Url = GetURL(SiteId) + "CustomersService.asmx";
            return _customer;
        }
        if (_page.Session["WebReferenceURL"] == null)
        {
            _page.Session["WebReferenceURL"] = GetURL(SiteId);
        }
        if (!string.IsNullOrEmpty((string)_page.Session["WebReferenceURL"]))
            _customer.Url = (string)_page.Session["WebReferenceURL"] + "CustomersService.asmx";
        return _customer;
    }

    public static WebReferenceTwilio.TwilioEntryPoint GetTwilioReference(PageSetting _page)
    {
        WebReferenceTwilio.TwilioEntryPoint _client = new WebReferenceTwilio.TwilioEntryPoint();
        if (_page.siteSetting != null && !string.IsNullOrEmpty(_page.siteSetting.GetUrlWebReference))
            _client.Url = _page.siteSetting.GetUrlWebReference + "TwilioEntryPoint.asmx";
        return _client;
    }
    public static WebReferenceTwilio.TwilioEntryPoint GetTwilioReference(string WebReferenceURL)
    {
        WebReferenceTwilio.TwilioEntryPoint _twillio = new WebReferenceTwilio.TwilioEntryPoint();

        _twillio.Url = WebReferenceURL + "TwilioEntryPoint.asmx";

        return _twillio;
    }
    public static WebReferenceTwilio.TwilioEntryPoint GetTwilioReference(Page _page, string SiteId)
    {

        WebReferenceTwilio.TwilioEntryPoint _client = new WebReferenceTwilio.TwilioEntryPoint();
        if (_page == null)
        {
            //           dbug_log.ExceptionLog(new Exception(), "2= " + SiteId);
            _client.Url = GetURL(SiteId) + "TwilioEntryPoint.asmx";
            return _client;
        }
        if (_page.Session["WebReferenceURL"] == null)
        {
            _page.Session["WebReferenceURL"] = GetURL(SiteId);
        }
        if (!string.IsNullOrEmpty((string)_page.Session["WebReferenceURL"]))
            _client.Url = (string)_page.Session["WebReferenceURL"] + "TwilioEntryPoint.asmx";
        return _client;
    }

    public static string GetURL(string SiteId)
    {
        if (string.IsNullOrEmpty(SiteId))
            return string.Empty;
        string url = string.Empty;
        string command = "EXEC dbo.GetSiteDetailBySiteNameId @SiteId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteId", SiteId);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                    url = (reader.IsDBNull(7)) ? "" : reader.GetString(7);
                conn.Close();
            }
            catch (Exception ex)
            {
                SiteSetting ss = new SiteSetting(SiteId);
                dbug_log.ExceptionLog(ex, ss);
                return "";
            }
        }
        if (string.IsNullOrEmpty(url))
            dbug_log.ExceptionLog(new Exception(), "site= " + SiteId + @" //No siteId \r\nConnectionString= " +
                DBConnection.GetConnString().ConnectionString);
        return url;
    }
    public static WebReferenceCustomer.CustomersService GetCustomerReference(string WebReferenceURL)
    {
        WebReferenceCustomer.CustomersService _customer = new WebReferenceCustomer.CustomersService();

        _customer.Url = WebReferenceURL + "CustomersService.asmx";

        return _customer;
    }
    public static WebReferenceReports.Reports GetReportsReference(PageSetting _page)
    {
        WebReferenceReports.Reports _report = new WebReferenceReports.Reports();
        if (_page.siteSetting != null && !string.IsNullOrEmpty(_page.siteSetting.GetUrlWebReference))
            _report.Url = _page.siteSetting.GetUrlWebReference + "Reports.asmx";
        return _report;
    }
    public static WebReferenceReports.Reports GetReportsReference(Page _page, string SiteId)
    {
        WebReferenceReports.Reports _report = new WebReferenceReports.Reports();
        if (_page == null)
        {
            _report.Url = GetURL(SiteId) + "Reports.asmx";
            return _report;
        }
        if (_page.Session["WebReferenceURL"] == null)
        {
            _page.Session["WebReferenceURL"] = GetURL(SiteId);
        }
        if (!string.IsNullOrEmpty((string)_page.Session["WebReferenceURL"]))
            _report.Url = (string)_page.Session["WebReferenceURL"] + "Reports.asmx";
        return _report;
    }
    public static ClipCallReport.ClipCallReport GetClipCallReportReference(PageSetting _page)
    {

        ClipCallReport.ClipCallReport _report = new ClipCallReport.ClipCallReport();
        if (_page.siteSetting != null && !string.IsNullOrEmpty(_page.siteSetting.GetUrlWebReference))
            _report.Url = _page.siteSetting.GetUrlWebReference + "ClipCallReport.asmx";

        return _report;
    }
    public static ClipCallReport.ClipCallReport GetClipCallReportReference(string WebReferenceURL)
    {
        ClipCallReport.ClipCallReport _report = new ClipCallReport.ClipCallReport();

        _report.Url = WebReferenceURL + "ClipCallReport.asmx";

        return _report;
    }
    public static WebReferenceReports.Reports GetReportsReference(string WebReferenceURL)
    {
        WebReferenceReports.Reports _report = new WebReferenceReports.Reports();

        _report.Url = WebReferenceURL + "Reports.asmx";

        return _report;
    }
    public static OneCallUtilities.OneCallUtilities GetOneCallUtilitiesReference(PageSetting _page)
    {

        OneCallUtilities.OneCallUtilities ocu = new OneCallUtilities.OneCallUtilities();
        if (_page.siteSetting != null && !string.IsNullOrEmpty(_page.siteSetting.GetUrlWebReference))
            ocu.Url = _page.siteSetting.GetUrlWebReference + "OneCallUtilities.asmx";

        return ocu;
    }
    public static OneCallUtilities.OneCallUtilities GetOneCallUtilitiesReference(Page _page, string SiteId)
    {
        OneCallUtilities.OneCallUtilities ocu = new OneCallUtilities.OneCallUtilities();
        if (_page == null)
        {
            ocu.Url = GetURL(SiteId) + "OneCallUtilities.asmx";
            return ocu;
        }
        string url = (_page.Session["WebReferenceURL"] == null) ? string.Empty : (string)_page.Session["WebReferenceURL"];
        if (string.IsNullOrEmpty(url))
        {          
            url = GetWebReferenceURLBySiteId(SiteId);
            _page.Session["WebReferenceURL"] = url;
            if (string.IsNullOrEmpty(url))
                return ocu;
        }
        ocu.Url = url + "OneCallUtilities.asmx";
        return ocu;
    }
    public static OneCallUtilities.OneCallUtilities GetOneCallUtilitiesReference(string WebReferenceURL)
    {
        OneCallUtilities.OneCallUtilities ocu = new OneCallUtilities.OneCallUtilities();

        ocu.Url = WebReferenceURL + "OneCallUtilities.asmx";

        return ocu;
    }
    /*
    public static WebReferenceZapServices.ZapServices GetZapServicesReference(PageSetting _page)
    {

        WebReferenceZapServices.ZapServices _site = new WebReferenceZapServices.ZapServices();
        if (_page.siteSetting != null && !string.IsNullOrEmpty(_page.siteSetting.GetUrlWebReference))
            _site.Url = _page.siteSetting.GetUrlWebReference + "ZapServices.asmx";

        return _site;
    }
    public static WebReferenceZapServices.ZapServices GetZapServicesReference(Page _page, string SiteId)
    {
        WebReferenceZapServices.ZapServices _site = new WebReferenceZapServices.ZapServices();
        if (_page == null)
        {
            //         dbug_log.ExceptionLog(new Exception(), GetURL(SiteId) + "Site.asmx");
            _site.Url = GetURL(SiteId) + "ZapServices.asmx";
            return _site;
        }
        string url = (_page.Session["WebReferenceURL"] == null) ? string.Empty : (string)_page.Session["WebReferenceURL"];
        if (string.IsNullOrEmpty(url))
        {
            url = GetWebReferenceURLBySiteId(SiteId);
            _page.Session["WebReferenceURL"] = url;
            if (string.IsNullOrEmpty(url))
                return _site;
        }
        _site.Url = url + "ZapServices.asmx";
        return _site;
    }
    public static WebReferenceZapServices.ZapServices GetZapServicesReference(string WebReferenceURL)
    {
        WebReferenceZapServices.ZapServices _site = new WebReferenceZapServices.ZapServices();

        _site.Url = WebReferenceURL + "ZapServices.asmx";

        return _site;
    }
    */
    public static WebReferenceDeskapp.Deskapp GetDeskappServicesReference()
    {
        return GetDeskappServicesReference(PpcSite.GetCurrent().UrlWebReference);
    }
    public static WebReferenceDeskapp.Deskapp GetDeskappServicesReference(string WebReferenceURL)
    {
        WebReferenceDeskapp.Deskapp _Deskapp = new WebReferenceDeskapp.Deskapp();

        _Deskapp.Url = WebReferenceURL + "Deskapp.asmx";

        return _Deskapp;
    }
    /*
    public static ZapPayment.NPCreateInvoiceERP GetZapInvoiceService()
    {
        ServicePointManager.DefaultConnectionLimit = 50;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
        ServicePointManager.MaxServicePointIdleTime = 1000 * 5 * 1;
        ServicePointManager.UseNagleAlgorithm = true;
        ServicePointManager.Expect100Continue = false;
        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
        ZapPayment.NPCreateInvoiceERP client = new ZapPayment.NPCreateInvoiceERP();
        client.Url = PpcSite.GetCurrent().ZapService;
        return client;
    }
     * */
     /*
    public static WebReferenceZapUrlCreditGuard.PMTGetPayPageURLCREDITGUARD GetUrlCreditGuard()
    {
 //       if (PpcSite.GetCurrent().UserZap == "NoProblemInvoicesDev")
//            return new WebReferenceZapUrlCreditGuard.PMTGetPayPageURLCREDITGUARD();
        ServicePointManager.DefaultConnectionLimit = 50;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
        ServicePointManager.MaxServicePointIdleTime = 1000 * 5 * 1;
        ServicePointManager.UseNagleAlgorithm = true;
        ServicePointManager.Expect100Continue = false;
        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
        WebReferenceZapUrlCreditGuard.PMTGetPayPageURLCREDITGUARD guard = new WebReferenceZapUrlCreditGuard.PMTGetPayPageURLCREDITGUARD();
        //guard.Url = PpcSite.GetCurrent().ZapService + "PMTGetPayPageURLCREDITGUARD";
        guard.Url = PpcSite.GetCurrent().ZapService + "PMTGetPayPageURLCREDITGUARD";
        return guard;
    }
    public static WebReferenceZapQueryCreditGuard.PMTTrxQueryCREDITGUARD GetQueryCreditGuard()
    {
//        if (PpcSite.GetCurrent().UserZap == "NoProblemInvoicesDev")
//            return new WebReferenceZapQueryCreditGuard.PMTTrxQueryCREDITGUARD();
        ServicePointManager.DefaultConnectionLimit = 50;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
        ServicePointManager.MaxServicePointIdleTime = 1000 * 5 * 1;
        ServicePointManager.UseNagleAlgorithm = true;
        ServicePointManager.Expect100Continue = false;
        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
        WebReferenceZapQueryCreditGuard.PMTTrxQueryCREDITGUARD guard = new WebReferenceZapQueryCreditGuard.PMTTrxQueryCREDITGUARD();
        //guard.Url = PpcSite.GetCurrent().ZapService + "PMTTrxQueryCREDITGUARD";
        guard.Url = PpcSite.GetCurrent().ZapService + "PMTTrxQueryCREDITGUARD";
        return guard;
    }
    */
    private static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
        return true;
    }

    public static AarService.AarService GetAarService()
    {
        AarService.AarService _AarService = new AarService.AarService();

        _AarService.Url = PpcSite.GetCurrent().UrlWebReference + "AarService.asmx";

        return _AarService;
    }
}
