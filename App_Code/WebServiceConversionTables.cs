﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for WebServiceConversionTables
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceConversionTables : System.Web.Services.WebService {

    public WebServiceConversionTables () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string convertHeiseCitiesCode(string hieseCityCode, int subSiteId)
    {
        string noproblemCityCode = "";
        int noproblemCityLevel = 0;
        string command = "EXEC dbo.GetHeiseRegionCodeLevelCode @hieseCityCode,@subSiteId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@hieseCityCode", hieseCityCode);
            cmd.Parameters.AddWithValue("@subSiteId", subSiteId);
            SqlDataReader reader = cmd.ExecuteReader();

            

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    noproblemCityCode = (string)reader["noproblemCityCode"];
                    noproblemCityLevel = (int)reader["noproblemCityLevel"];
                }
            }

            reader.Close();
            conn.Close();
        }
        return noproblemCityCode + "&" + noproblemCityLevel.ToString();
    }
    
}

