﻿using System.IO;
using System.Net;
using System.Web;
using System.Xml;
using System;
using Microsoft.Ajax.Utilities;
using System.Collections.Generic;
using System.Web.Script.Serialization;

/// <summary>
/// A C# wrapper around the Google Closure Compiler web service.
/// </summary>
public class GoogleClosure
{
    /*
	private const string PostData = "js_code={0}&output_format=xml&output_info=compiled_code&compilation_level=ADVANCED_OPTIMIZATIONS";
    private const string PostDataSimple = "js_code={0}&output_format=xml&output_info=compiled_code&compilation_level=SIMPLE_OPTIMIZATIONS";
	private const string ApiEndpoint = "http://closure-compiler.appspot.com/compile";
    */
    private const string ApiEndpoint = "http://10.154.28.138/JSCompilerServices/api/compile";
	/// <summary>
	/// Compresses the specified file using Google's Closure Compiler algorithm.
	/// <remarks>
	/// The file to compress must be smaller than 200 kilobytes.
	/// </remarks>
	/// </summary>
	/// <param name="file">The absolute file path to the javascript file to compress.</param>
	/// <returns>A compressed version of the specified JavaScript file.</returns>
    /*
    public string Compress(string _source, bool IsFile, PpcSite _cache)
	{
        XmlDocument xml = null;
        try
        {
            string source = (IsFile) ? File.ReadAllText(_source) : _source;
            xml = CallApi(source);
        
            return xml.SelectSingleNode("//compiledCode").InnerText;
        }
        catch (Exception exc)
        {
            string mess = "xml= " + ((xml == null) ? "null" : xml.OuterXml);
            _cache.GetCacheLog.WriteCacheLog(exc, new string[] { mess }, false);
            return MiniScript(_source, _cache);
        }
       
	}
     */
    public string Compress(string _source, bool IsFile, PpcSite _cache)
    {
       
        string source = (IsFile) ? File.ReadAllText(_source) : _source;
        string _script = CallApi(source);          
        if(string.IsNullOrEmpty(_script))
            return MiniScript(_source, _cache);
        return _script;
        

    }
    string MiniScript(string OriginalScript, PpcSite _cache)
    {
        Minifier mf = new Minifier();

        string _script = mf.MinifyJavaScript(OriginalScript);
        if (mf.ErrorList.Count > 0)
        {
            List<string> list_error = new List<string>();
            
            foreach (ContextError ce in mf.ErrorList)
                list_error.Add(ce.Message);
            _cache.GetCacheLog.WriteCacheLog(new Exception("Minifier"), list_error.ToArray(), false);
        }
        return _script;
    }
   

	/// <summary>
	/// Calls the API with the source file as post data.
	/// </summary>
	/// <param name="source">The content of the source file.</param>
	/// <returns>The Xml response from the Google API.</returns>
     /*
    private static string CallApi(string source)
    {
        string compiled = string.Empty;
        try
        {
            RestSharp.RestClient client = new RestSharp.RestClient(ApiEndpoint);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.AddJsonBody(source);
            var response = client.Execute(request);
            var content = response.Content;
            if (response.StatusCode != HttpStatusCode.OK)
                return string.Empty;
            compiled = (string)(new JavaScriptSerializer().Deserialize(content, typeof(string)));
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        return compiled;
    }
    */
    private static string CallApi(string source)
    {
        ////////
        return string.Empty;
        ////////
        string compiled = string.Empty;
        try
        {
            string json_source = new JavaScriptSerializer().Serialize(source);

            byte[] data = System.Text.Encoding.UTF8.GetBytes(json_source);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ApiEndpoint);
            request.ContentLength = data.Length;
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Timeout = 600000;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();
    //        request.GetRequestStream().Write(data, 0, data.Length);
            
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode != HttpStatusCode.OK)
                return string.Empty;
            string content = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
            compiled = (string)(new JavaScriptSerializer().Deserialize(content, typeof(string)));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return string.Empty;
        }
        return compiled;
        //     string compiled = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(content);
    }
    /*
	private static XmlDocument CallApi(string source)
	{
		using (WebClient client = new WebClient())
		{
            
                client.Headers.Add("content-type", "application/x-www-form-urlencoded");
                string data = string.Format(PostData, HttpUtility.UrlEncode(source));
                string result = client.UploadString(ApiEndpoint, data);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result);
                return doc;
           
		}
	}
    */
    /*
    public string CompressSimple(string _source, bool IsFile)
    {
        XmlDocument xml = null;
        try
        {
            string source = (IsFile) ? File.ReadAllText(_source) : _source;
            xml = CallApiSimple(source);        
            return xml.SelectSingleNode("//compiledCode").InnerText;        
        }
        catch (Exception exc)
        {
            string mess = "xml= " + ((xml == null) ? "null" : xml.OuterXml);
            dbug_log.ExceptionLog(exc, mess);
            return _source;
        }
        
        
    }
    */

    /// <summary>
    /// Calls the API with the source file as post data.
    /// </summary>
    /// <param name="source">The content of the source file.</param>
    /// <returns>The Xml response from the Google API.</returns>
    /*
    private static XmlDocument CallApiSimple(string source)
    {
        using (WebClient client = new WebClient())
        {
            client.Headers.Add("content-type", "application/x-www-form-urlencoded");
            string data = string.Format(PostDataSimple, HttpUtility.UrlEncode(source));
            string result = client.UploadString(ApiEndpoint, data);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            return doc;
        }
    }
     * */
}