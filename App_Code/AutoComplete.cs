using System;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using AjaxControlToolkit;
using System.Text;
using System.Xml;
using System.Linq;


/// <summary>
/// Summary description for AutoComplete
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AutoComplete : System.Web.Services.WebService {
  //  public static Dictionary<string, Auto_Complete> AutoDic = new Dictionary<string, Auto_Complete>();
 //   const int COUNT = 12;
    SortedDictionary<string, string> list;
    
    
    public AutoComplete () {

        
    //    ((SortedDictionary<string, string>)Session["AutoCompleteList"]).Count
    }
    [WebMethod]
    public SortedDictionary<string, string> SetCitiesItems(PageSetting _page, out Dictionary<string, string> dic)
    {
        dic = new Dictionary<string, string>();
        SortedDictionary<string, string> list = new SortedDictionary<string, string>();
        //    List<string> list = new List<string>();

        //dbug polin
        //   WebReferenceSuplierPlanetPolin.Supplier _sp = new WebReferenceSuplierPlanetPolin.Supplier();
        //    WebReferenceSupplier.Supplier _sp = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.Supplier _sp = WebServiceConfig.GetSupplierReference(_page);

        string result = string.Empty;
        try
        {
            result = _sp.GetRegions(1);
        }
        catch (SoapException ex)
        {
            
            return new SortedDictionary<string, string>();
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);

        if (xdd["Areas"] == null || xdd["Areas"]["Error"] != null)
        {            
            return new SortedDictionary<string, string>();
        }

        foreach (XmlNode node in xdd["Areas"].ChildNodes)
        {
            string name = node.Attributes["Name"].InnerText;
            string engName = node.Attributes["EnglishName"].InnerText;
            string RegionId = node.Attributes["RegionId"].InnerText;
            if (string.IsNullOrEmpty(name))
                continue;
            list.Add(name, engName);
            dic.Add(name, RegionId);

        }

        return list;
    }
    [WebMethod]
    public SortedDictionary<string, string> SetCitiesItems(PageSetting _page, int _level)
    {
       
  //      string _id = ss.GetSiteID +"," + regNum;
  //      if(AutoDic.ContainsKey(_id))
  //          return _id;
        SortedDictionary<string, string> list = new SortedDictionary<string, string>();
    //    List<string> list = new List<string>();

        //dbug polin
     //   WebReferenceSuplierPlanetPolin.Supplier _sp = new WebReferenceSuplierPlanetPolin.Supplier();
    //    WebReferenceSupplier.Supplier _sp = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.Supplier _sp = WebServiceConfig.GetSupplierReference(_page);

        string result = string.Empty;
        try
        {
            result = _sp.GetRegions(_level);
        }
        catch (SoapException ex)
        {
            return new SortedDictionary<string, string>();
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);

        if (xdd["Areas"] == null || xdd["Areas"]["Error"] != null)
            return new SortedDictionary<string, string>();

        foreach (XmlNode node in xdd["Areas"].ChildNodes)
        {
            string name = node.Attributes["Name"].InnerText;
            string engName = node.Attributes["EnglishName"].InnerText;
            string RegionId = node.Attributes["RegionId"].InnerText;
            if (string.IsNullOrEmpty(name))
                continue;
            list.Add(name, engName);
            

        }      
  
        return list;
    }
    [WebMethod(true)]
    public string SetCitiesItemsUpsale(int _level)
    {
        if (listUpsales(_level).Count > 0)
            return "OK";
        SiteSetting ss = (SiteSetting)Session["Site"];

        //      string _id = ss.GetSiteID +"," + regNum;
        //      if(AutoDic.ContainsKey(_id))
        //          return _id;
        Dictionary<string, string> list = new Dictionary<string, string>();
        //    List<string> list = new List<string>();

        //dbug polin
        //   WebReferenceSuplierPlanetPolin.Supplier _sp = new WebReferenceSuplierPlanetPolin.Supplier();
        //    WebReferenceSupplier.Supplier _sp = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.Supplier _sp = WebServiceConfig.GetSupplierReference(ss.GetUrlWebReference);

        string result = string.Empty;
        try
        {
            result = _sp.GetRegions(_level);
        }
        catch (SoapException ex)
        {
            return "false";
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);

        if (xdd["Areas"] == null || xdd["Areas"]["Error"] != null)
            return "false";
        
            foreach (XmlNode node in xdd["Areas"].ChildNodes)
            {
                string name = node.Attributes["Name"].InnerText;
                string engName = node.Attributes["EnglishName"].InnerText;
                string RegionCode = node.Attributes["RegionCode"].InnerText;
                if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(RegionCode))
                    continue;
                int i = 1;
                string add_to_string = string.Empty;
                while (list.ContainsKey(name + add_to_string))
                {
                    add_to_string = " (" + (i++) + ")";
                }
                list.Add(name + add_to_string, RegionCode);
            }
        
        SetListUpsales(_level, list);
        return "true";
    }
    [WebMethod(EnableSession = true)]
    public string GetOneWord(string prefixText)
    {
        if (prefixText.Trim() == string.Empty)
            return "";
        if (Session == null || Session["AutoCompleteList"] == null)
            return "";
        else
            this.list = (SortedDictionary<string, string>)Session["AutoCompleteList"];
        if (list.Count == 0)
            return "";
        
        string result;
//        SortedDictionary<string, string> sd = AutoDic[_guid].list;
        prefixText = prefixText.ToLower();
        if (IsEnglish(prefixText))
        {
  
            if (list.ContainsValue(prefixText))
                return prefixText;
            SortedDictionary<string, string>.ValueCollection svc = list.Values;
 
            while (prefixText.Length > 0)
            {
                foreach (string str in svc)                
                    if (str.ToLower().StartsWith(prefixText))
                        return str;
                prefixText = prefixText.Substring(0, prefixText.Length - 1);
            }

        }
        else
        {

            if (list.ContainsKey(prefixText))
                return prefixText;
            SortedDictionary<string, string>.KeyCollection kvc = list.Keys;
            while (prefixText.Length > 0)
            {
                foreach (string str in kvc)
                    if (str.ToLower().StartsWith(prefixText))
                        return str;
                prefixText = prefixText.Substring(0, prefixText.Length - 1);
            }
        }
        return "";
        /*
        do
        {
            result = list.Find(
                delegate(string st)
                {
                    return st.StartsWith(prefixText);
                }
                );
            if(prefixText.Length > 0)
                prefixText = prefixText.Substring(0, prefixText.Length - 1);
        } while (string.IsNullOrEmpty(result));
        return result;
         * */
        
       
    }
    private bool IsEnglish(string name)
    {

        if (!((name[0] > 64 && name[0] < 91) || (name[0] > 96 && name[0] < 123)))
            return false;
            
        
        return true;
    }
    //not in used
    /*
    [WebMethod]
    public string _GetSuggestions(string _guid, string prefixText)
    {
        string[] str = GetSuggestions(prefixText, 10);
        string result = "";
        foreach (string st in str)
            result += st + ";";
        if(result.Length > 0)
            result = result.Substring(0, result.Length - 1);
        return result;
    }
     * */
    [WebMethod(EnableSession = true)]
  
    public string[] GetSuggestions(string prefixText, int count)
    {
        List<string> responses = new List<string>();
        if (Session == null || Session["AutoCompleteList"] == null)
            return responses.ToArray();
        else
            this.list = (SortedDictionary<string, string>)Session["AutoCompleteList"];
/*        contextKey = contextKey.Substring(0, contextKey.Length - 1);

        string[] items = contextKey.Split(';');
        foreach (string str in items)
        {
            string[] itemssub = str.Split('&');
            this.list.Add(itemssub[0], itemssub[1]);
        }
 * */
        
       
  //      SortedDictionary<string, string> list = AutoDic[contextKey].list;
        if (IsEnglish(prefixText))
        {
            SortedDictionary<string, string>.ValueCollection sdv = list.Values;
            foreach (string str in sdv)
            {
                
                if (str.ToLower().StartsWith(prefixText.ToLower()))
                {
                    string _key = "";
                    foreach (KeyValuePair<string,string> kvp in list)
                    {
                        if (kvp.Value == str)
                        {
                            _key = kvp.Key;
                            break;
                        }
                    }
                    responses.Add(str + " - " + _key);
                    if (responses.Count >= count)
                        break;
                }
            }
        }
        else
        {
            SortedDictionary<string, string>.KeyCollection sdk = list.Keys;
            foreach (string str in sdk)
            {

                if (str.ToLower().StartsWith(prefixText.ToLower()))
                {

                    responses.Add(str + " - " + list[str]);
                    if (responses.Count >= count)
                        break;
                }
            }
        }
        return responses.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public string[] GetSuggestionsUpsale(string prefixText, int count, string contextKey)
    {
     
        Dictionary<string, string> dic = listUpsales(int.Parse(contextKey));
       
        List<string> result  = (from x in dic
                                            where x.Key.ToLower().StartsWith(prefixText.ToLower())
                                            select x.Key).Take(count).ToList();//kv => kv.Key, kv => kv.Value);
          
        return result.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public bool IsExists(string str)
    {
        if(str.Contains(" - "))
            str = str.Split('-')[0].Trim();
        if (Session == null || Session["AutoCompleteList"] == null)
            return false;
        else
            this.list = (SortedDictionary<string, string>)Session["AutoCompleteList"];
        if (list.ContainsKey(str) || list.ContainsValue(str))
            return true;
        return false;
    }
    [WebMethod(EnableSession = true)]
    public string chkMatcReturnCorrect(string prefixText, int regionLevel)
    {
        Dictionary<string, string> dic = listUpsales(regionLevel);
        List<string> list = (from x in dic
                where x.Key.ToLower() == prefixText.ToLower()
                select x.Key).ToList();

        if(list.Count==0)
            return "";
        return list[0];
    }
    Dictionary<string, string> listUpsales(int i)
    {
        return (Session["listUpsale" + i] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)Session["listUpsale" + i]; 
 //       set { Session["listUpsale"] = value; }
    }
    void SetListUpsales(int i, Dictionary<string, string> dic)
    {
        Session["listUpsale"+i] = dic;
    }
   
}

