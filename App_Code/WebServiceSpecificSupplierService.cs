﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;

/// <summary>
/// Summary description for WebServiceSpecificSupplierService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceSpecificSupplierService : System.Web.Services.WebService {

    public WebServiceSpecificSupplierService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string call(string ExpertiseCode, string ExpertiseType, string RegionCode, string RegionLevel, string SiteId, string SupplierId, string OriginId, string ContactPhoneNumber)
    {
        string replayStr = "";

        LogEdit.SaveLog(SiteId, "Start:" + "\r\nsurfer phone: " +
        ContactPhoneNumber + 
        "\r\nSession: " + Session.SessionID +
        "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));


        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.SupplierServiceRequest request=new WebReferenceCustomer.SupplierServiceRequest();
        request.ExpertiseCode = ExpertiseCode;
        request.ExpertiseType = Convert.ToInt32(ExpertiseType);
        request.RegionCode = RegionCode;
        request.RegionLevel = Convert.ToInt32(RegionLevel);
        request.RequestDescription = "";
        request.SiteId = SiteId;
        request.SupplierId = new Guid(SupplierId);
        request.OriginId = new Guid(OriginId);
        request.ContactPhoneNumber = ContactPhoneNumber;

        WebReferenceCustomer.ResultOfSupplierServiceResponse response = new WebReferenceCustomer.ResultOfSupplierServiceResponse();

        response=customer.CreateSupplierSpecificService(request);

        if (response.Type == WebReferenceCustomer.eResultType.Success)
        {
            //Response.Write(serviceResponse.ServiceRequestId + " status:" + serviceResponse.Status.ToString().ToLower());
            //Response.Write(response.Value.ServiceRequestId);
            replayStr = response.Value.ServiceRequestId;

            LogEdit.SaveLog(SiteId, "CreateSelfServiceRequest status:" + response.Value.Status.ToString() +
                "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " +
                ContactPhoneNumber +
                "\r\nSession: " + Session.SessionID + "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

        }

        else
        {
            //Response.Write("unsuccess");
            LogEdit.SaveLog(SiteId, response.Value.Status.ToString() +
                 "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " + ContactPhoneNumber +                
                "\r\nSession: " + Session.SessionID +
                "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request) + "\r\nmessage: " + response.Messages.ToArray().ToString());

            replayStr = "unsuccess";
        }

        return replayStr;
    }
    
}

