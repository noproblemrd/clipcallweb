﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;

/// <summary>
/// Summary description for SalesConsumer
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class SalesConsumer : System.Web.Services.WebService {

    public SalesConsumer () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetIfKeywordExistReturnWaterMark(string keyword) {
        PpcSite _site = PpcSite.GetCurrent();
        string _code = _site.GetHeadingCodeByKeywordHeadingName(keyword);
        if (string.IsNullOrEmpty(_code))
            return string.Empty;
        var result = new { code = _code, WaterMark = _site.GetHeadingDetails(_code).WaterMark, title = _site.GetHeadingDetails(_code).GetDescription() };
        JavaScriptSerializer jss = new JavaScriptSerializer();
        return jss.Serialize(result);
    }
    [WebMethod]
    public string GetLastThreeCases(string code, string CasesIds)
    {
        string[] _ids = CasesIds.Split(new char[]{';'}, StringSplitOptions.RemoveEmptyEntries );
        List<Guid> _cases_ids = new List<Guid>();
        foreach (string str in _ids)
        {
            Guid newid;
            if (!Guid.TryParse(str, out newid))
                newid = Guid.Empty;
            _cases_ids.Add(newid);
        }
        
        PpcSite ps = PpcSite.GetCurrent();
        Guid _id = Guid.Empty;
        if (!string.IsNullOrEmpty(code))
            _id = ps.GetHeadinGuidByCode(code);
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, ps.SiteId);
        WebReferenceCustomer.ResultOfListOfCustomerPageCaseData result = null;
        try
        {
            result = _customer.GetLastThreeCases(_id);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, "GetBidCount");
            return "faild";
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
            return "faild";

        List<RequestCase> list = new List<RequestCase>();
        foreach (WebReferenceCustomer.CustomerPageCaseData cpcd in result.Value)
        {
            string Heading_code = ps.GetHeadingCodeByGuid(cpcd.HeadingId);
            RequestCase rc = new RequestCase();
            rc.description = cpcd.Description;
            rc.HeadingName = ps.headings[Heading_code].SingularName;
            rc.city = cpcd.RegionName;
            rc.ID = cpcd.CaseId;
            rc.SetDate(cpcd.CreatedOn);
            rc.IsPrivacy = ps.HeadingOfGroup(Heading_code, eHeadingGroup.attorney);
            rc.ago = Utilities.GetDateBefore(cpcd.CreatedOn);
            list.Add(rc); 
            
        }
        list.Sort(delegate(RequestCase rc1, RequestCase rc2) { return rc2.GetDate().CompareTo(rc1.GetDate()); });
        int indexDiffertent = 0;
        if (list.Count == 3 && _cases_ids.Count == 3)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].ID != _cases_ids[i])
                    indexDiffertent++;
            }
        }
        if (indexDiffertent > 0)
            SalesUtility.AddToCounter(indexDiffertent);

        return new JavaScriptSerializer().Serialize(list);
    }
    [WebMethod]
    public string GetTotalCaseCount()
    {
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, PpcSite.GetCurrent().SiteId);
        WebReferenceCustomer.ResultOfInt32 result = null;
        try
        {
            result = _customer.GetTotalCaseCount();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, "GetTotalCaseCount");
            return "faild";
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
            return "faild";
        return result.Value.ToString();
    }
    [WebMethod]
    public string ContactUs(string email, string subject, string message)
    {
        string NoProblemEmail = "hi@noproblemppc.com";
        StringBuilder sb = new StringBuilder();
        sb.Append("email: " + email);
        sb.Append("<br/>subject: " + subject);
        sb.Append("<br/>message: " + message);
        MailSender MailContactUs = new MailSender();
        MailContactUs.IsText = false;
        MailContactUs.Subject = subject;
        MailContactUs.To = NoProblemEmail;
        MailContactUs.Body = sb.ToString();
        bool IsSent = MailContactUs.Send();
        if (!IsSent)
            dbug_log.ExceptionLog(new Exception("Email send faild"), "Faild send contact us");
        return "";
    }
    [WebMethod]
    public string GetLastCases(int CasesNum)
    {
        PpcSite ps = PpcSite.GetCurrent();
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps.UrlWebReference);
        WebReferenceSupplier.ResultOfListOfCustomerPageCaseData result = null;
        try
        {
            result = _supplier.GetLastCases(CasesNum);
        }
        catch (Exception xec)
        {
            dbug_log.ExceptionLog(xec);
            return string.Empty;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return string.Empty;
        List<RequestCase> list = new List<RequestCase>();

        
        foreach (WebReferenceSupplier.CustomerPageCaseData cpcd in result.Value)
        {
            string Heading_code = ps.GetHeadingCodeByGuid(cpcd.HeadingId);
            RequestCase rc = new RequestCase();
            rc.description = cpcd.Description;
            rc.HeadingName = ps.headings[Heading_code].SingularName;
            rc.city = cpcd.RegionName;
            rc.ID = cpcd.CaseId;
            rc.SetDate(cpcd.CreatedOn);
            rc.IsPrivacy = ps.IsPrivacyHeading(Heading_code);
            rc.ago = Utilities.GetDateBefore(cpcd.CreatedOn);
            rc.lat = cpcd.Latitude;
            rc.lng = cpcd.Longitude;            
            list.Add(rc);
        }
         
        return new JavaScriptSerializer().Serialize(list);
    }
    [WebMethod]
    public string GetCasesTest(int CasesNum)
    {
        PpcSite ps = PpcSite.GetCurrent();
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps.UrlWebReference);
        WebReferenceSupplier.ResultOfListOfCustomerPageCaseData result = null;
        try
        {
            result = _supplier.GetLastCases(CasesNum);
        }
        catch (Exception xec)
        {
            dbug_log.ExceptionLog(xec);
            return string.Empty;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return string.Empty;
        List<RequestCase> list = new List<RequestCase>();
        foreach (WebReferenceSupplier.CustomerPageCaseData cpcd in result.Value)
        {
            string Heading_code = ps.GetHeadingCodeByGuid(cpcd.HeadingId);
            RequestCase rc = new RequestCase();
            rc.description = cpcd.Description;
            rc.HeadingName = ps.headings[Heading_code].SingularName;
            rc.city = cpcd.RegionName;
            rc.ID = cpcd.CaseId;
            rc.SetDate(cpcd.CreatedOn);
            rc.IsPrivacy = ps.IsPrivacyHeading(Heading_code);
            rc.ago = Utilities.GetDateBefore(cpcd.CreatedOn);
            rc.lat = cpcd.Latitude;
            rc.lng = cpcd.Longitude;
            list.Add(rc);
        }
        List<RequestCase> new_list = new List<RequestCase>();
        for (int i = list.Count - 1; i > list.Count - 4; i--)
        {
            new_list.Add(list[i]);
        }
        for (int i = 0; i < list.Count - 4; i++)
            new_list.Add(list[i]);

        return new JavaScriptSerializer().Serialize(new_list);
    }
}
