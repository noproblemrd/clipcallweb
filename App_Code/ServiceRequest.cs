﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ServiceRequest
/// </summary>
public class ServiceRequest
{
    public MongoDB.Bson.ObjectId exposureId { get; set; }
    public string originId { get; set; }
    public string siteId { get; set; }
    public Guid advertiserId { get; set; }
    
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string phone { get; set; }
    public string email { get; set; }
    public string zipCode { get; set; }
    public string Country { get; set; }
    public string preSoldMessage { get; set; }
    public int numOfSuppliers { get; set; }
    public string requestDescription { get; set; }
    public int step { get; set; }
    public decimal? preSoldPrice { get; set; }
  
	public ServiceRequest()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}