﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

/// <summary>
/// Summary description for WebServiceLogIn
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceLogIn : System.Web.Services.WebService {

    public WebServiceLogIn () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string LogIn(string SiteNameId, string email, string password)
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, SiteNameId);
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;
        try
        {
            _response = supplier.UserLogin(email, password);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            return "Faild";
        }
        if (_response.Type != WebReferenceSupplier.eResultType.Success)
        {
            return "Faild";
        }
        switch (_response.Value.UserLoginStatus)
        {
            case (WebReferenceSupplier.eUserLoginStatus.NotFound):
                return "NotFound";
            case (WebReferenceSupplier.eUserLoginStatus.UserInactive):
                return "UserInactive";
        }
        // check if the user is affiliate
        if (_response.Value.AffiliateOrigin != Guid.Empty)
        {
            return "AffiliateUser";
        }
        // check if the user is supplier
        if (_response.Value.SecurityLevel > 0)
        {           
            return "PublisherUser";
        }
        Guid _id = new Guid();
        string URL = "";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            string command = "EXEC dbo.SetApiUserLogIn @SiteNameId, @UserId, @StageInRegistration, @SecurityLevel, " +
                                "@Balance, @UserName, @Email";

            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            cmd.Parameters.AddWithValue("@UserId", _response.Value.UserId);
            cmd.Parameters.AddWithValue("@StageInRegistration", _response.Value.StageInRegistration);
            cmd.Parameters.AddWithValue("@SecurityLevel", _response.Value.SecurityLevel);
            cmd.Parameters.AddWithValue("@Balance", _response.Value.Balance);
            cmd.Parameters.AddWithValue("@UserName", _response.Value.Name);
            cmd.Parameters.AddWithValue("@Email", email);
            SqlDataReader reader = cmd.ExecuteReader();
            
            if (reader.Read())
            {
                _id = (Guid)reader["Id"];
                URL = (string)reader["URL"];
            }
           

            conn.Close();
        }
        string _path = string.Format("{0}://{1}{2}",
                   HttpContext.Current.Request.Url.Scheme,
                   URL + ((HttpContext.Current.Request["SERVER_PORT"] != "" && URL == "localhost") ? (":" + HttpContext.Current.Request["SERVER_PORT"]) : string.Empty),
                   (HttpContext.Current.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Current.Request.ApplicationPath
                   );
        return _path + "/ApiLogIn.aspx?login=" + _id.ToString();
    }
    [WebMethod]
    public bool NewUser(string SiteNameId, string email, string phone)
    {
        return NewUserWithOrigin(SiteNameId, email, phone, string.Empty);
    }

    [WebMethod]
    public bool NewUserWithOrigin(string siteNameId, string email, string phone, string originCode)
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, siteNameId);
        string result = string.Empty;
        try
        {
            result = supplier.SupplierSignUp(email, phone, originCode);
        }
        catch (Exception ex)
        {
            SiteSetting ss = new SiteSetting(siteNameId);
            dbug_log.ExceptionLog(ex, ss);
            return false;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["SupplierSignUp"] == null || xdd["SupplierSignUp"]["Error"] != null)
        {
            if (xdd["SupplierSignUp"] != null && xdd["SupplierSignUp"]["Error"].InnerText.ToLower() == "email allready exists")
                return false;
            else
                return false;
        }
        return true;
    }
    [WebMethod]
    public int NewUserRegistration(string SiteNameId, string email, string phone)
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, SiteNameId);
        string result = string.Empty;
        try
        {
            result = supplier.SupplierSignUp(email, phone, string.Empty);
        }
        catch (Exception ex)
        {
            SiteSetting ss = new SiteSetting(SiteNameId);
            dbug_log.ExceptionLog(ex, ss);
            return -10;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["SupplierSignUp"] == null || xdd["SupplierSignUp"]["Error"] != null)
        {
            if (xdd["SupplierSignUp"] != null && xdd["SupplierSignUp"]["Error"].InnerText.ToLower().Contains("email"))
                return -1;
            else
                return -1;
        }
        return 0;
    }
    
}

