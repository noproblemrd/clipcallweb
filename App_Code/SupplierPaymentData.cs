﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SupplierPaymentData
/// </summary>
public class SupplierPaymentData
{
    public List<Supplier_Pricing_Component> list_component { get; set; }
    public Decimal TotalAmount { get; set; }
    public string Name { get; set; }
    public int PaymentId { get; set; }
    public Guid SupplierId { get; set; }
    public string BizId { get; set; }
    public ePaymentMethod PaymentMethod { get; set; }
    public bool IsFirstTime { get; set; }
    public int BalanceOld { get; set; }
    public int BalanceNew { get; set; }
    public string email { get; set; }
    private bool _IsFromDollar = false;
    public bool IsFromDollar
    {
        get { return _IsFromDollar; }
        set { _IsFromDollar = value; }
    }
    /***** CREDIT CARD   *****/
    public string CreditCardCompany { get; set; }
    public string CreditCarddHolderName { get; set; }
    public string CreditCardPersonIdNumber { get; set; }
    //public string CreditCard_Id { get; set; }
    public string CreditCardToken { get; set; }
    public string CreditCard_Expier { get; set; }
    public string CreditCardTxId { get; set; }
   
    /***** REFUND *****/
    public string InvoiceNumber { get; set; }

    /***** BANK DETAILS  *****/
    public string Bank {get;set;}
    public string BankBranch {get;set;}
    public string BankAccount { get; set; }
    public DateTime WireTransfeCheckDate { get; set; }

    /****** CHECK  ******/
    public string CheckNumber { get; set; }
    
    /****** VOUCHER  *****/
    public string VoucherNumber { get; set; }
    public Guid VoucherReasonId { get; set; }
	public SupplierPaymentData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public WebReferenceSupplier.ePaymentMethod GetCrmPaymentMethod()
    {
        WebReferenceSupplier.ePaymentMethod epm;
        if (!Enum.TryParse(PaymentMethod.ToString(), out epm))
            epm = WebReferenceSupplier.ePaymentMethod.Cash;
        return epm;
    }
    public bool HasRecharge()
    {
        IEnumerable<Supplier_Pricing_Component> query = from x in list_component
                                                        where x.PaymentType == ePaymentType.Recharge//where x.PaymentType == ePaymentType.AutoLow || x.PaymentType == ePaymentType.AutoMaxMonthly
                                                        select x;
        return query.Count() > 0;
    }
    public decimal GetRecharge()
    {
        IEnumerable<Supplier_Pricing_Component> query = from x in list_component
                                                        where x.PaymentType == ePaymentType.Recharge//where x.PaymentType == ePaymentType.AutoLow || x.PaymentType == ePaymentType.AutoMaxMonthly
                                                        select x;
        if (query.Count() == 0)
            return 0m;
        decimal result = (from x in list_component
                          where x.PaymentType == ePaymentType.Recharge//where x.PaymentType == ePaymentType.AutoLow || x.PaymentType == ePaymentType.AutoMaxMonthly
                          select x.Amount).Sum();
        return result;
    }
    public ePaymentType GetRechargeTypeCode()
    {
        IEnumerable<Supplier_Pricing_Component> query = from x in list_component
                                                        where x.PaymentType == ePaymentType.AutoLow || x.PaymentType == ePaymentType.AutoMaxMonthly
                                                        select x;
        if (query.Count() == 0)
            return ePaymentType.AutoMaxMonthly;
        return query.FirstOrDefault().PaymentType;
        /*
        string result = (from x in list_component
                          where x.PaymentType == ePaymentType.Recharge
                          select x.comment).FirstOrDefault();
        WebReferenceSupplier.eSupplierPricingComponentType type;
        if (!Enum.TryParse(result, out type))
            type = WebReferenceSupplier.eSupplierPricingComponentType.AutoMaxMonthly;
        return type;
         * */
    }
    public decimal GetValueByType(ePaymentType ept)
    {
        IEnumerable<Supplier_Pricing_Component> query = from x in list_component
                                                        where x.PaymentType == ept
                                                        select x;
        if (query.Count() == 0)
            return 0m;
        decimal result = (from x in list_component
                          where x.PaymentType == ept
                          select x.Amount).Sum();
        return result;
    }
    public List<WebReferenceSupplier.SupplierPricingComponent> GetListCrmType()
    {
        List<WebReferenceSupplier.SupplierPricingComponent> list = new List<WebReferenceSupplier.SupplierPricingComponent>();
        if (list_component != null)
        {

            foreach (Supplier_Pricing_Component spc in list_component)
            {
                switch(spc.PaymentType)
                {
                    case(ePaymentType.Bonus):
                    case(ePaymentType.Recharge):
                    case(ePaymentType.BonusExtra):
                        continue;
                }
                list.Add(spc.GetCrmType());
            }
        }

        return list;
    }
    public string Last4digitOfToken
    {
        get 
        {
            if (CreditCardToken != null && CreditCardToken.Length > 4)
                return this.CreditCardToken.Substring(CreditCardToken.Length - 4);
            return string.Empty;
        }
    }
}