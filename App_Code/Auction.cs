using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.UI;
using System.Xml;
using System.Text;
using System.Reflection;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


/// <summary>
/// Summary description for Auction
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class Auction : System.Web.Services.WebService {
    readonly string professionalLogos = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
    readonly string professionalLogosWeb = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];
    public Auction () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    /*
    [WebMethod(true)]
    public string GetList(string ExpertiseCode, int ExpertiseLevel, string city, string IncidentId)
    {
        SiteSetting ss = (SiteSetting)Session["Site"];

        string RegionCode;
        SortedDictionary<string, string> dic = (SortedDictionary<string, string>)Session["AutoCompleteList"];
        RegionCode = dic[city];
        int RegionLevel;
        RegionLevel = 1;

        int MaxResults = -1;        

        string origionId = ss.GetOrigionId.ToString();
        
        WebReferenceCustomer.SearchRequest searchRequest = new WebReferenceCustomer.SearchRequest();
        searchRequest.ExpertiseCode = ExpertiseCode;
        searchRequest.ExpertiseLevel = ExpertiseLevel;
        searchRequest.MaxResults = MaxResults;
        searchRequest.RegionCode = RegionCode;
        searchRequest.RegionLevel = RegionLevel;
        searchRequest.SiteId = ss.GetSiteID;
        searchRequest.OriginId = origionId;
   //     searchRequest.IncidentId = IncidentId;
        /****************************
        searchRequest.UpsaleId = new Guid(IncidentId);
        /****************************

        return GetList(searchRequest);
    }
*/
    
    public string GetList(WebReferenceCustomer.SearchRequest searchRequest) {
        Session["searchRequest"] = searchRequest;
        SiteSetting ss = (SiteSetting)Session["Site"];
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(ss.GetUrlWebReference);
        string xmlSuppliers = customer.SearchSiteSuppliers(searchRequest);

        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(xmlSuppliers);
        if (xdd["Suppliers"] == null || xdd["Suppliers"]["Error"] != null)
            return "empty";
        
        DataTable data = new DataTable();
        data.Columns.Add("SupplierName");
        data.Columns.Add("ShortDescription");
        data.Columns.Add("NumberOfEmployees");
        data.Columns.Add("SupplierUrl");
        data.Columns.Add("Logo");
        data.Columns.Add("SumAssistanceRequests");
        data.Columns.Add("ExtensionNumber");
        data.Columns.Add("DirectNumber");
        foreach(XmlNode node in xdd["Suppliers"].ChildNodes)
        {
            string SupplierId = node.Attributes["SupplierId"].Value;
            string SupplierNumber = node.Attributes["SupplierNumber"].Value;
            string SupplierName = node.Attributes["SupplierName"].Value;
            string DirectNumber = node.Attributes["DirectNumber"].Value;
            string ShortDescription = node.Attributes["ShortDescription"].Value;
                        //SumSurvey = xmlTextReader.GetAttribute("SumSurvey");
            string SumAssistanceRequests = node.Attributes["SumAssistanceRequests"].Value;
            string NumberOfEmployees = node.Attributes["NumberOfEmployees"].Value;
            string Certificate = node.Attributes["Certificate"].Value;
      //      string isAvailable = node.Attributes["isAvailable"].Value;
            string ExtensionNumber = "";// node.Attributes["ExtensionNumber"].Value;

            string directNumberSograim = "";
            string directNumberRegular = "";
            if (DirectNumber.Length > 0)
            {
                DirectNumber = DirectNumber.Substring(1, DirectNumber.Length - 1);
                if (DirectNumber.Length > 3)
                {
                    directNumberSograim = "(" + DirectNumber.Substring(0, 3) + ")";
                    directNumberRegular = DirectNumber.Substring(3, DirectNumber.Length - 3);
                }
            }
            string _path = HttpContext.Current.Request.ApplicationPath;
            if (_path != @"/")
                _path = _path + @"/";
      
            string SupplierUrl = _path+"professional.aspx?supplierGUID={" + SupplierId + "}&SiteId=" + ss.GetSiteID
                            + "&DirectNumber=" + DirectNumber + "&ExtensionNumber=" + ExtensionNumber;
            string Logo = (File.Exists(professionalLogos + SupplierId + ".jpg")) ?
                professionalLogosWeb + SupplierId + @".jpg" : 
                _path + "images/icons/user-icon.png";

            DataRow row = data.NewRow();
            row["SupplierName"] = SupplierName;
            row["ShortDescription"] = ShortDescription;
            row["NumberOfEmployees"] = NumberOfEmployees;
            row["SupplierUrl"] = SupplierUrl;
            row["Logo"] = Logo;
            row["SumAssistanceRequests"] = SumAssistanceRequests;
            row["ExtensionNumber"] = ExtensionNumber;
            row["DirectNumber"] = DirectNumber;
            data.Rows.Add(row);
        }
        string _resolveURL = string.Format("{0}://{1}{2}",
                HttpContext.Current.Request.Url.Scheme,
                HttpContext.Current.Request.ServerVariables["HTTP_HOST"],
                (HttpContext.Current.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Current.Request.ApplicationPath
                );  
       Page page = new Page();
       UserControl ctl = 
            (UserControl)page.LoadControl(@"~/Publisher/Auction/SearchResult.ascx");
       
       MethodInfo _translate = ctl.GetType().GetMethod("SetTranslate");
       _translate.Invoke(ctl, new object[] { ss.siteLangId });
       MethodInfo m_load = ctl.GetType().GetMethod("LoadData");
       m_load.Invoke(ctl, new object[] { data });
       ctl.EnableViewState = false;
       
        //Render control to string
       StringBuilder sb = new StringBuilder();  
       using (StringWriter sw = new StringWriter(sb))
       {
           using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
           {
               ctl.RenderControl(textWriter);
           }
       }        
       return sb.ToString();
      
    }
    [WebMethod(true)]
    public string GetServiceRequestPanel(string _expertise)
    {
        SiteSetting ss = (SiteSetting)Session["Site"];
        Page page = new Page();
        UserControl ctl =
            (UserControl)page.LoadControl("~/Publisher/Auction/MatchedContractors.ascx");
        MethodInfo _translate = ctl.GetType().GetMethod("SetTranslate");
        _translate.Invoke(ctl, new object[] { ss.siteLangId });
        MethodInfo m_load = ctl.GetType().GetMethod("LoadData");
        m_load.Invoke(ctl, new object[] { _expertise });
        ctl.EnableViewState = false;

        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }
    [WebMethod(true)]
    public string createServiceRequest(string _phone, string _desc, string UpsaleId, int NumExper, string _RequestId, string name, string email)
    {
        Guid RequestId;
        if (!Guid.TryParse(_RequestId, out RequestId))
            RequestId = Guid.Empty;
        string _response;
        SiteSetting ss = (SiteSetting)Session["Site"];
        try
        {
            
            UserMangement _user = (UserMangement)Session["UserGuid"];
            WebReferenceCustomer.SearchRequest searchRequest = (WebReferenceCustomer.SearchRequest)Session["searchRequest"];
            WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(ss.GetUrlWebReference);
            WebReferenceCustomer.ServiceRequest serviceRequest = new WebReferenceCustomer.ServiceRequest();
            serviceRequest.RegionCode = searchRequest.RegionCode;
            serviceRequest.RegionLevel = searchRequest.RegionLevel;
            serviceRequest.ExpertiseCode = searchRequest.ExpertiseCode;
            serviceRequest.ExpertiseType = searchRequest.ExpertiseLevel;
            serviceRequest.ContactFullName = name;
            serviceRequest.ContactEmail = email;
            serviceRequest.ContactPhoneNumber = _phone;
            serviceRequest.RequestDescription = _desc;
            serviceRequest.PrefferedCallTime = DateTime.Now;
            serviceRequest.UserId = _user.Get_Guid;
            serviceRequest.UpsaleRequestId = RequestId;
            
            serviceRequest.UpsaleId = new Guid(UpsaleId);
            
            serviceRequest.SiteId = ss.GetSiteID;
            serviceRequest.OriginId = ss.GetOrigionId;//new Guid("{CF879946-455C-DF11-80CD-0003FF727321}");
            serviceRequest.NumOfSuppliers = NumExper;

            WebReferenceCustomer.ServiceResponse serviceResponse = new WebReferenceCustomer.ServiceResponse();
            IAsyncResult ar = customer.BeginCreateServiceRequest(serviceRequest, null, null);

            serviceResponse = customer.EndCreateServiceRequest(ar);

            Session["IncidentId"] = serviceResponse.ServiceRequestId;
            _response = serviceResponse.Status.ToString();
            _response += ";" + EditTranslateDB.GetControlTranslate(ss.siteLangId, "StatusCode", _response);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            throw exc;
        }
        return _response;
       // WebReferenceCustomer.StatusCode.
        /*
        WebReferenceCustomer.StatusCode.BlackList;
        WebReferenceCustomer.StatusCode.CreateIncidentAccountFailed;
        WebReferenceCustomer.StatusCode.CreateIncidentFailed;
        WebReferenceCustomer.StatusCode.NoDefaultUser;
        WebReferenceCustomer.StatusCode.NoSuppliers;
        WebReferenceCustomer.StatusCode.Success;
         * 
         * */
        
    }
    [WebMethod(true)]
    public string createServiceRequestSchedule(string _phone, string _desc, string UpsaleId, int NumExper, string _date, string _RequestId, string name, string email)
    {
        Guid RequestId;
        if (!Guid.TryParse(_RequestId, out RequestId))
            RequestId = Guid.Empty;
        string _response;
        SiteSetting ss = (SiteSetting)Session["Site"];
        try
        {

            UserMangement _user = (UserMangement)Session["UserGuid"];
            WebReferenceCustomer.SearchRequest searchRequest = (WebReferenceCustomer.SearchRequest)Session["searchRequest"];
            WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(ss.GetUrlWebReference);
            WebReferenceCustomer.ScheduledServiceRequest serviceRequest = new WebReferenceCustomer.ScheduledServiceRequest();
            serviceRequest.RegionCode = searchRequest.RegionCode;
            serviceRequest.RegionLevel = searchRequest.RegionLevel;
            serviceRequest.ExpertiseCode = searchRequest.ExpertiseCode;
            serviceRequest.ExpertiseType = searchRequest.ExpertiseLevel;
            serviceRequest.ContactFullName = name;
            serviceRequest.ContactPhoneNumber = _phone;
            serviceRequest.ContactEmail = email;
            serviceRequest.RequestDescription = _desc;
            serviceRequest.PrefferedCallTime = DateTime.Now;
            serviceRequest.UserId = _user.Get_Guid;
            serviceRequest.UpsaleRequestId = RequestId;

            serviceRequest.UpsaleId = new Guid(UpsaleId);

            serviceRequest.SiteId = ss.GetSiteID;
            serviceRequest.OriginId = ss.GetOrigionId;//new Guid("{CF879946-455C-DF11-80CD-0003FF727321}");
            serviceRequest.NumOfSuppliers = NumExper;
            string[] str = _date.Split(';');
            DateTime dt = new DateTime(int.Parse(str[0]), int.Parse(str[1]), int.Parse(str[2]), int.Parse(str[3]), int.Parse(str[4]), 0, DateTimeKind.Utc);
            serviceRequest.PrefferedCallTime = dt;

            WebReferenceCustomer.ResultOfScheduledServiceResponse serviceResponse = new WebReferenceCustomer.ResultOfScheduledServiceResponse();
      //      IAsyncResult ar = customer.BeginCreateServiceRequest(serviceRequest, null, null);

            serviceResponse = customer.CreateScheduledService(serviceRequest);
            if(serviceResponse.Type==WebReferenceCustomer.eResultType.Failure)
                return WebReferenceCustomer.StatusCode.CreateIncidentFailed.ToString();

            Session["IncidentId"] = serviceResponse.Value.ServiceRequestId;
            _response = serviceResponse.Value.Status.ToString();
            _response += ";" + EditTranslateDB.GetControlTranslate(ss.siteLangId, "StatusCode", _response);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            throw exc;
        }
        return _response;
        
    }
    [WebMethod(true)]
    public string CheckAuction2()
    {
 //       return "";
        string _path = HttpContext.Current.Request.ApplicationPath;
        SiteSetting ss = (SiteSetting)Session["Site"];
  //      Guid _guid = (Guid)Session["IncidentId"];
        string incidentId = (string)Session["IncidentId"];//_guid.ToString();
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(ss.GetUrlWebReference);
        string xmlSuppliers = customer.GetSuppliersStatus(incidentId);
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(xmlSuppliers);

        if (xdd["Suppliers"] == null || !xdd["Suppliers"].HasChildNodes 
            || xdd["Suppliers"]["Error"] != null)
            return "";
      //  dbug_log.SaveAuctionUpsale(xmlSuppliers, incidentId);
       
        string _IncidentStatus = xdd["Suppliers"].Attributes["IncidentStatus"].Value;
        string _mode = xdd["Suppliers"].Attributes["Mode"].Value;
        if (_IncidentStatus == "Completed")
        {
            WebReferenceCustomer.SearchRequest searchRequest = (WebReferenceCustomer.SearchRequest)Session["searchRequest"];
            string message = @"<span style=""display:none;"">TurnOff</span>";
            return message + GetList(searchRequest);
        }

        DataTable data = new DataTable();
        data.Columns.Add("SupplierId");
        data.Columns.Add("SupplierNumber");
        data.Columns.Add("SupplierName");
        data.Columns.Add("DirectNumber");
        data.Columns.Add("ShortDescription");
        data.Columns.Add("SumAssistanceRequests");
        data.Columns.Add("NumberOfEmployees");
        data.Columns.Add("Certificate");
        data.Columns.Add("isAvailable");
        data.Columns.Add("Logo");
        data.Columns.Add("CallIcon");
        data.Columns.Add("CallIconClass");
        foreach (XmlNode node in xdd["Suppliers"].ChildNodes)
        {

            string SupplierId = node.Attributes["SupplierId"].Value;
            string SupplierNumber = node.Attributes["SupplierNumber"].Value;
            string SupplierName = node.Attributes["SupplierName"].Value;
        //    string DirectNumber = node.Attributes["DirectNumber"].Value;
            string ShortDescription = node.Attributes["ShortDescription"].Value;
            
            string SumAssistanceRequests = node.Attributes["SumAssistanceRequests"].Value;
            string NumberOfEmployees = node.Attributes["NumberOfEmployees"].Value;
            string Certificate = node.Attributes["Certificate"].Value;
        //    string isAvailable = node.Attributes["isAvailable"].Value;
            string Status = node.Attributes["Status"].Value;

            DataRow row = data.NewRow();
            row["SupplierId"] = SupplierId;
            row["SupplierName"] = SupplierName;
            row["SupplierNumber"] = SupplierNumber;
        //    row["DirectNumber"] = DirectNumber;
            row["ShortDescription"] = ShortDescription;
            row["SumAssistanceRequests"] = SumAssistanceRequests;
            row["NumberOfEmployees"] = NumberOfEmployees;
            row["Certificate"] = Certificate;
            /*
             * string SupplierUrl = _path+"/professional.aspx?supplierGUID={" + SupplierId + "}&SiteId=" + ss.GetSiteID
                            + "&DirectNumber=" + DirectNumber + "&ExtensionNumber=" + ExtensionNumber;*/
            string Logo = (File.Exists(professionalLogos + SupplierId + ".jpg")) ?
                professionalLogosWeb + SupplierId + @".jpg" : 
                _path + "/images/icons/user-icon.png";
            row["Logo"] = Logo;
            if (Status == "Initiated")
            {
                row["CallIcon"] = _path + "/images/icon-calling.gif";
                row["CallIconClass"] = "label2";
            }
            else
            {
                row["CallIcon"] = _path + "/images/icons/icon-call-now_clear.gif";
                row["CallIconClass"] = "label";
            }
            data.Rows.Add(row);
        }
        Page page = new Page();
        UserControl ctl = (UserControl)page.LoadControl("~/Publisher/Auction/AuctionLive.ascx");
        MethodInfo _translate = ctl.GetType().GetMethod("SetTranslate");
        _translate.Invoke(ctl, new object[] { ss.siteLangId });
        MethodInfo m_load = ctl.GetType().GetMethod("LoadData");
        m_load.Invoke(ctl, new object[] { data, _mode });
        ctl.EnableViewState = false;

        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }

        return sb.ToString();        
    }
    [WebMethod(true)]
    public string GetUpsaleList(string ExpertiseCode, int ExpertiseLevel, string city, string UpsaleId, int RegionLevel)
    {
        SiteSetting ss = (SiteSetting)Session["Site"];

        string RegionCode;
        Dictionary<string, string> dic = listUpsales(RegionLevel);
        if (!dic.ContainsKey(city))
            return "none";
        RegionCode = dic[city];

        int MaxResults = -1;

        string origionId = ss.GetOrigionId.ToString();

        WebReferenceCustomer.SearchRequest searchRequest = new WebReferenceCustomer.SearchRequest();
        searchRequest.ExpertiseCode = ExpertiseCode;
        searchRequest.ExpertiseLevel = ExpertiseLevel;
        searchRequest.MaxResults = MaxResults;
        searchRequest.RegionCode = RegionCode;
        searchRequest.RegionLevel = RegionLevel;
        searchRequest.SiteId = ss.GetSiteID;
        searchRequest.OriginId = origionId;
  
        searchRequest.UpsaleId = new Guid(UpsaleId);
       

        return GetUpsaleList(searchRequest);
    }

    public string GetUpsaleList(WebReferenceCustomer.SearchRequest searchRequest)
    {
        Session["searchRequest"] = searchRequest;
        SiteSetting ss = (SiteSetting)Session["Site"];
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(ss.GetUrlWebReference);
        string xmlSuppliers = customer.SearchSiteSuppliers(searchRequest);

        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(xmlSuppliers);
        if (xdd["Suppliers"] == null || xdd["Suppliers"]["Error"] != null || !xdd["Suppliers"].HasChildNodes)
            return "empty";
        
        DataTable data = new DataTable();
        data.Columns.Add("SupplierName");
        data.Columns.Add("SupplierId");
        data.Columns.Add("SupplierUrl");
        data.Columns.Add("ExtensionNumber");
        data.Columns.Add("DirectNumber");
        data.Columns.Add("HasExtension", typeof(bool));
        foreach (XmlNode node in xdd["Suppliers"].ChildNodes)
        {
            string SupplierId = node.Attributes["SupplierId"].Value;
            string SupplierName = node.Attributes["SupplierName"].Value;
            string DirectNumber = node.Attributes["DirectNumber"].Value;
            string ExtensionNumber = (node.Attributes["ExtensionNumber"] == null) ? string.Empty : node.Attributes["ExtensionNumber"].Value;
            DataRow row = data.NewRow();
            if (SupplierName.Length > 50)
                SupplierName = SupplierName.Substring(0, 46) + "...";
            row["SupplierName"] = SupplierName;
            row["ExtensionNumber"] = ExtensionNumber;
            row["DirectNumber"] = DirectNumber;
            row["HasExtension"] = (!string.IsNullOrEmpty(ExtensionNumber));
            data.Rows.Add(row);
        }
        Page page = new Page();
        
        UserControl ctl =
             (UserControl)page.LoadControl(@"~/Publisher/Auction/UpsaleSearch.ascx");

        MethodInfo m_load = ctl.GetType().GetMethod("LoadData");
        m_load.Invoke(ctl, new object[] { data, ss.siteLangId });
        ctl.EnableViewState = false;

        StringBuilder sb = new StringBuilder();
        
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        
        return sb.ToString();

       
    }
    Dictionary<string, string> listUpsales(int i)
    {
        return (Session["listUpsale" + i] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)Session["listUpsale" + i];
        //       set { Session["listUpsale"] = value; }
    }
    void SetListUpsales(int i, Dictionary<string, string> dic)
    {
        Session["listUpsale" + i] = dic;
    }
}
