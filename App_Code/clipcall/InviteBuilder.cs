﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for InviteBuilder
/// </summary>
public class InviteBuilder
{
	public InviteBuilder()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string inviteJS { get; private set; }
    public string invite2JS { get; private set; }
    public Guid SetInviteJs(PpcSite _cache)
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        string domain = xdoc.Element("Sites").Element("clipcall").Element("WebInvite").Element("domain").Value;
        string aid = xdoc.Element("Sites").Element("clipcall").Element("WebInvite").Element("default_account").Value;
        Guid defaultAccountId;
        if (!Guid.TryParse(aid, out defaultAccountId))
            defaultAccountId = Guid.Empty;

        string line;
        StringBuilder InviteSB = new StringBuilder();
        string InvitePath = System.AppDomain.CurrentDomain.BaseDirectory + @"invite\invite1.0.js";

        if (File.Exists(InvitePath))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(InvitePath);
                while ((line = file.ReadLine()) != null)
                {
                    InviteSB.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        InviteSB.Replace("{domain}", domain);
        GoogleClosure gc = new GoogleClosure();
        inviteJS = gc.Compress(InviteSB.ToString(), false, _cache);


        StringBuilder Invite2SB = new StringBuilder();
        string InvitePath2 = System.AppDomain.CurrentDomain.BaseDirectory + @"invite\v2\invite2.0.js";

        if (File.Exists(InvitePath2))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(InvitePath2);
                while ((line = file.ReadLine()) != null)
                {
                    Invite2SB.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        Invite2SB.Replace("{domain}", domain);
 //       invite2JS = gc.Compress(Invite2SB.ToString(), false, _cache);
        invite2JS = Invite2SB.ToString();

        return defaultAccountId; 
    }
}