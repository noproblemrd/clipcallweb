﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InactiveSupplierManager
/// </summary>
public class InactiveSupplierManager
{
	public InactiveSupplierManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static ClipCallReport.GeneralResponseWR InactiveSupplierReason(Guid supplierId, ClipCallReport.eInactiveReason inactiveReason)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfGeneralResponseWR result = null;
        try
        {
            result = ccreport.InactiveSupplier(supplierId, inactiveReason);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ClipCallReport.GeneralResponseWR _response = new ClipCallReport.GeneralResponseWR()
            {
                FailedReason = exc.Message,
                IsSucceeded = false
            };
            return _response;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            ClipCallReport.GeneralResponseWR _response = new ClipCallReport.GeneralResponseWR()
            {
                FailedReason = "Server Failure",
                IsSucceeded = false
            };
            return _response;
        }
        return result.Value;
    }
    public static ClipCallReport.GeneralResponseWR ActiveSupplier(Guid supplierId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfGeneralResponseWR result = null;
        try
        {
            result = ccreport.ActiveSupplier(supplierId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ClipCallReport.GeneralResponseWR _response = new ClipCallReport.GeneralResponseWR()
            {
                 FailedReason = exc.Message,
                IsSucceeded = false
            };
            return _response;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            ClipCallReport.GeneralResponseWR _response = new ClipCallReport.GeneralResponseWR()
            {
                FailedReason = "Server Failure",
                IsSucceeded = false
            };
            return _response;
        }
        return result.Value;
    }
}