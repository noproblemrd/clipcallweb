﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;

/// <summary>
/// Summary description for GeneralService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class GeneralService : System.Web.Services.WebService {

    public GeneralService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public List<OriginRevenueData> GetOriginRevenue(DateTime date) {
        /*
        string command = @"
                    select new_originid, new_originidName, New_NetActualRevenue
                    from Sales_MSCRM.dbo.New_financialdashboarddailydata
                    where new_originid is not null
	                    and New_Date = @date
                    ";
         * */
        string command = @"
                        SELECT dbo.GetOriginNameByOriginId(new_originid) OriginName
	                        ,new_originid ,sum([revenue]) Revenue     
                      FROM [Sales_MSCRM].[dbo].[tbl_profit_aggregation]
                      where   [date] = @date
                      group by [new_originid]";
        List<OriginRevenueData> list = new List<OriginRevenueData>();
        try
        {
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@date", date);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        OriginRevenueData gdp = new OriginRevenueData();
                        gdp.ID = (Guid)reader["new_originid"];
                        gdp.num = (reader["Revenue"] == DBNull.Value) ? 0m : (decimal)reader["Revenue"];
                        gdp.OriginName = (reader["OriginName"] == DBNull.Value) ? string.Empty : (string)reader["OriginName"];
                        list.Add(gdp);
                    }
                }
                conn.Close();
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        return list;
    }
    [WebMethod]
    public List<OriginRevenueData> GetOriginRevenueByDateCountry(DateTime date, string country)
    {
        string command = @"
                        SELECT dbo.GetOriginNameByOriginId(new_originid) OriginName
	                        ,new_originid 
                          ,sum([revenue]) Revenue      
                       FROM [Sales_MSCRM].[dbo].[tbl_profit_aggregation]
                      where   [date] = @date and Country = @country
                      group by [new_originid]";
        List<OriginRevenueData> list = new List<OriginRevenueData>();
        try
        {
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@date", date);
                    cmd.Parameters.AddWithValue("@country", country);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        OriginRevenueData gdp = new OriginRevenueData();
                        gdp.ID = (Guid)reader["new_originid"];
                        gdp.num = (reader["Revenue"] == DBNull.Value) ? 0m : (decimal)reader["Revenue"];
                        gdp.OriginName = (reader["OriginName"] == DBNull.Value) ? string.Empty : (string)reader["OriginName"];
                        list.Add(gdp);
                    }
                }
                conn.Close();
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        return list;
    }
    [WebMethod]
    public List<GuidStringPair> GetAllOrigins()
    {
        List<GuidStringPair> list = new List<GuidStringPair>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetAllOrigins(true);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return null;        
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return null;
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            list.Add(new GuidStringPair(gsp.Id, gsp.Name));
        }
        return list;
    }
    
}
