﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SetWorkingHoursRequest
/// </summary>
public class SetWorkingHoursRequest
{
    public List<WorkingHoursUnit> Units { get; set; }
    public DateTime? VacationFrom { get; set; }
    public DateTime? VacationTo { get; set; }
    public Guid SupplierId { get; set; }

    public SetWorkingHoursRequest()
    {
        Units = new List<WorkingHoursUnit>();
    }

    internal WebReferenceSupplier.WorkingHoursUnit[] ToWebServiceUnitsArray()
    {
        List<WebReferenceSupplier.WorkingHoursUnit> retVal = new List<WebReferenceSupplier.WorkingHoursUnit>();

        foreach (var item in Units)
        {
            WebReferenceSupplier.WorkingHoursUnit data = new WebReferenceSupplier.WorkingHoursUnit();
            data.AllDay = item.AllDay;
            data.DaysOfWeek = TurnToWeberviceDaysOfWeekArray(item.DaysOfWeek);
            data.FromHour = item.FromHour;
            data.FromMinute = item.FromMinute;
            data.Order = item.Order;
            data.ToHour = item.ToHour;
            data.ToMinute = item.ToMinute;
            retVal.Add(data);
        }

        return retVal.ToArray();
    }

    private WebReferenceSupplier.DayOfWeek[] TurnToWeberviceDaysOfWeekArray(List<DayOfWeek> list)
    {
        List<WebReferenceSupplier.DayOfWeek> retVal = new List<WebReferenceSupplier.DayOfWeek>();

        foreach (var item in list)
        {
            switch (item)
            {
                case DayOfWeek.Sunday:
                    retVal.Add(WebReferenceSupplier.DayOfWeek.Sunday);
                    break;
                case DayOfWeek.Monday:
                    retVal.Add(WebReferenceSupplier.DayOfWeek.Monday);
                    break;
                case DayOfWeek.Tuesday:
                    retVal.Add(WebReferenceSupplier.DayOfWeek.Tuesday);
                    break;
                case DayOfWeek.Wednesday:
                    retVal.Add(WebReferenceSupplier.DayOfWeek.Wednesday);
                    break;
                case DayOfWeek.Thursday:
                    retVal.Add(WebReferenceSupplier.DayOfWeek.Thursday);
                    break;
                case DayOfWeek.Friday:
                    retVal.Add(WebReferenceSupplier.DayOfWeek.Friday);
                    break;
                case DayOfWeek.Saturday:
                    retVal.Add(WebReferenceSupplier.DayOfWeek.Saturday);
                    break;
            }
        }

        return retVal.ToArray();
    }
}