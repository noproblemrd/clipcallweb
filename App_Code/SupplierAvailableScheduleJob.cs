﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SupplierAvailableScheduleJob
/// </summary>
public class SupplierAvailableScheduleJob : ScheduleJob
{
    static object didLock = new object();
    const int MINUTES_TO_WAIT = 10;
	public SupplierAvailableScheduleJob()
        : base(MINUTES_TO_WAIT * 60, "SupplierAvailableScheduleJob")
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public override void DoTask()
    {
        try
        {
            lock (didLock)
            {
                PpcSite.GetCurrent().UpdateSpecialSupplierAvailable();
            }
        }
        catch (Exception exc)
        {
            //   _this.WriteLog(exc.Message);
            dbug_log.ExceptionLog(exc);
        }
    }
}