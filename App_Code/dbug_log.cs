using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.SessionState;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using log4net;


/// <summary>
/// Summary description for dbug_log
/// </summary>
public class dbug_log
{
//    private static readonly ILog LOG = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType, );

	public dbug_log()
	{
		//
		
		//
	}
    public static void Global_Exception(Exception ex, HttpServerUtility server, HttpContext context)
    {

        
        if (ex is HttpRequestValidationException)
        {
            HttpRequestValidationException hrve = ((HttpRequestValidationException)ex);

            //Ilegal character
            HttpResponse Response = HttpContext.Current.Response;
            dbug_log.ExceptionGlobalLog(hrve);
            string URL = HttpContext.Current.Request.Url.Host;
            string _path = string.Format("{0}://{1}{2}",
                   HttpContext.Current.Request.Url.Scheme,
                   URL + ((HttpContext.Current.Request["SERVER_PORT"] != "" && URL == "localhost") ? (":" + HttpContext.Current.Request["SERVER_PORT"]) : string.Empty),
                   (HttpContext.Current.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Current.Request.ApplicationPath
                   );
           
            string TexBoxtError = _path + "/TexBoxtError.aspx";


        //    string _redirect = "redirect=" + HttpContext.Current.Request.Url.AbsoluteUri;
            Response.Clear();
            //Ilegal Cookie
            string reason = "reason=";
            if (hrve.Message.Contains("Cookies"))
                reason += "cookie";
       //         Response.Cookies.Clear();
            Response.StatusCode = 200;

            Response.Redirect(TexBoxtError + "?" + reason);
        //    Response.End();
        }
        else if (ex is HttpUnhandledException)
        {
            HttpUnhandledException _hue = (HttpUnhandledException)ex;
            dbug_log.ExceptionGlobalLog(_hue);
      //      if(_hue.ErrorCode == 500
        }
        else if (ex is InvalidOperationException)
        {
            UnknownPath(HttpContext.Current.Response, ex);
            
        }
        else if (ex is HttpException)
        {
            HttpResponse Response = HttpContext.Current.Response;
            HttpException _he = (HttpException)ex;


            if (_he.Message == "A potentially dangerous Request.Path value was detected from the client (?).")
            {
                Uri _uri;
                if (Uri.TryCreate(HttpContext.Current.Request.Url.PathAndQuery, UriKind.RelativeOrAbsolute, out _uri))
                {
                    //  dbug_log.ExceptionLog(new Exception(_uri.ToString()));
                    //       context.Response.Clear();
                    server.ClearError();

                    //      context.RewritePath(_uri.ToString(), false);

                    server.TransferRequest(_uri.ToString());

                    //  IHttpHandler httpHandler = new DefaultHttpHandler();
                    //    httpHandler.ProcessRequest(context);
                    //     context.RewritePath(_uri.ToString(), false);

                    return;
                }

            }
            dbug_log.ExceptionGlobalLog(_he);            
            //    Page not exist    //
            if (_he.GetHttpCode() == 404 || _he.Message.Contains("A potentially dangerous Request.Path"))
            {
                UnknownPath(Response, _he);
                return;
            }

            else if (ex.InnerException is ViewStateException)
            {
                dbug_log.ExceptionGlobalLog(_he);
                ErrorManagment.CatchViewStateException((ViewStateException)ex.InnerException);
            }

        }
        else if (ex is ViewStateException)
        {
            ViewStateException vse = (ViewStateException)ex;
            dbug_log.ExceptionGlobalLog(vse);
            if (vse.IsConnected)
                ErrorManagment.CatchViewStateException(vse);
        }
        else
            dbug_log.ExceptionGlobalLog(ex);
        if (context.Request.PhysicalPath.ToLower().Contains((context.Request.PhysicalApplicationPath + "npsb").ToLower()))
            context.Response.Cookies.Add(new HttpCookie("npsb_error", "true"));
    }
    private static void UnknownPath(HttpResponse Response, Exception ex)
    {
        string ExtensionFile = HttpContext.Current.Request.CurrentExecutionFilePathExtension;
        string _path = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
        string _AcceptType = (HttpContext.Current.Request.AcceptTypes == null || HttpContext.Current.Request.AcceptTypes.Length == 0) ? string.Empty : HttpContext.Current.Request.AcceptTypes[0];
        bool ifLocalHost = false;

        if (HttpContext.Current.Request.Url.Host.ToLower() == "localhost")
            ifLocalHost = true;
        else
            ifLocalHost = false;        

        if (_AcceptType == "text/html")
        {

            if (_path.Contains(@"/publisher/"))
            {
                if (ifLocalHost)
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/Publisher/SearchSupplierApp.aspx");
                else
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "Publisher/SearchSupplierApp.aspx");
            }
            else if (_path.Contains(@"/management/"))
            {
                if (ifLocalHost)
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/Management/frame.aspx");
                else
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "Management/frame.aspx");
            }
            else if (_path.Contains(@"/affiliate/"))
            {
                if (ifLocalHost)
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/Affiliate/AffiliateRequestReport.aspx");
                else
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "Affiliate/AffiliateRequestReport.aspx");

            }

            else if (_path.Contains(@"/ppc/"))
            {
                if (ifLocalHost)
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/customErrors/404.htm");
                else
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "customErrors/404.htm");                
            }

            else
            {
                if (ifLocalHost)
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/main.aspx");
                else
                    Response.Redirect(HttpContext.Current.Request.ApplicationPath + "main.aspx");

            }
        }

        else if (ExtensionFile.ToLower() == ".asmx")
            Response.Write(ex.Message);
    }
    public static void ExceptionLog(Exception exc, string message, params object[] _params)
    {
        string _message = string.Format(message, _params);
        ExceptionLog(exc, _message);
    }
    public static void ExceptionLog(Exception exc, string message)
    {
        ErrorManagment em = new ErrorManagment();
        HttpRequest request = null;
        bool HasRequest = GetHasRequest(out request);
        //    System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"]);

        HttpSessionState _sess = (!HasRequest) ? null : HttpContext.Current.Session;
        SiteSetting ss = (_sess == null || _sess["Site"] == null) ? new SiteSetting() : (SiteSetting)_sess["Site"];
        em.SiteId = string.IsNullOrEmpty(ss.GetSiteID) ? "NULL" : ss.GetSiteID;
        UserMangement _user = (_sess == null || _sess["UserGuid"] == null) ? null : (UserMangement)_sess["UserGuid"];
        em.UserId = (_user == null) ? Guid.Empty : _user.Get_Guid;
        em.UserName = (_user == null) ? string.Empty : _user.User_Name;
        em.Info = message;
        if (HasRequest)
        {
            em.ClientIP = Utilities.GetIP(request);
            em.Browser_Version = request.Browser.Browser + ", " + request.Browser.MajorVersion;
        }
        ILog LOG = LogManager.GetLogger("RollingFileAppenderError");
       
        LOG.Error(em, exc);
    }
    public static void ExceptionLog(Exception exc, object obj)
    {
        ILog LOG = LogManager.GetLogger("RollingFileAppenderError");

        LOG.Error(obj, exc);
    }
    public static void MessageLog(params string[] messages)
    {
        ErrorManagment em = new ErrorManagment();
        HttpRequest request = null;
        bool HasRequest = GetHasRequest(out request);
        //    System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"]);

        HttpSessionState _sess = (!HasRequest) ? null : HttpContext.Current.Session;
        SiteSetting ss = (_sess == null || _sess["Site"] == null) ? null : (SiteSetting)_sess["Site"];
        em.SiteId = (ss == null || string.IsNullOrEmpty(ss.GetSiteID)) ? "NULL" : ss.GetSiteID;
        UserMangement _user = (_sess == null || _sess["UserGuid"] == null) ? null : (UserMangement)_sess["UserGuid"];
        em.UserId = (_user == null) ? Guid.Empty : _user.Get_Guid;
        em.UserName = (_user == null) ? string.Empty : _user.User_Name;
        StringBuilder message = new StringBuilder();
        foreach (string str in messages)
        {
            message.AppendLine(str);
        }
        em.Info = message.ToString();
        if (HasRequest)
        {
            em.ClientIP = Utilities.GetIP(request);
            em.Browser_Version = request.Browser.Browser + ", " + request.Browser.MajorVersion;
        }
        ILog LOG = LogManager.GetLogger("RollingFileAppenderError");
        
        LOG.Error(em);
    }
    /*
    public static void MessageLog(string[] messages)
    {
        ErrorManagment em = new ErrorManagment();
        HttpRequest request = null;
        bool HasRequest = GetHasRequest(out request);
        //    System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"]);

        HttpSessionState _sess = (!HasRequest) ? null : HttpContext.Current.Session;
        SiteSetting ss = (_sess == null || _sess["Site"] == null) ? null : (SiteSetting)_sess["Site"];
        em.SiteId = (ss == null || string.IsNullOrEmpty(ss.GetSiteID)) ? "NULL" : ss.GetSiteID;
        UserMangement _user = (_sess == null || _sess["UserGuid"] == null) ? null : (UserMangement)_sess["UserGuid"];
        em.UserId = (_user == null) ? Guid.Empty : _user.Get_Guid;
        em.UserName = (_user == null) ? string.Empty : _user.User_Name;
        em.Info = message;
        if (HasRequest)
        {
            em.ClientIP = Utilities.GetIP(request);
            em.Browser_Version = request.Browser.Browser + ", " + request.Browser.MajorVersion;
        }
        ILog LOG = LogManager.GetLogger("RollingFileAppenderError");

        LOG.Error(em);
    }
     */
    public static void ExceptionLog(Exception exc, SiteSetting _site)
    {
        ErrorManagment em = new ErrorManagment();
        HttpRequest request = null;
        bool HasRequest = GetHasRequest(out request);
        //    System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"]);

        HttpSessionState _sess = (!HasRequest) ? null : HttpContext.Current.Session;
        em.SiteId = string.IsNullOrEmpty(_site.GetSiteID) ? "NULL" : _site.GetSiteID;
        UserMangement _user = (_sess == null || _sess["UserGuid"] == null) ? null : (UserMangement)_sess["UserGuid"];
        em.UserId = (_user == null) ? Guid.Empty : _user.Get_Guid;
        em.UserName = (_user == null) ? string.Empty : _user.User_Name;
        if (HasRequest)
        {
            em.ClientIP = Utilities.GetIP(request);
            em.Browser_Version = request.Browser.Browser + ", " + request.Browser.MajorVersion;
        }
        ILog LOG = LogManager.GetLogger("RollingFileAppenderError");
        LOG.Error(em, exc);

    }
    public static void ExceptionLog(Exception exc)
    {
        ErrorManagment em = new ErrorManagment();
        HttpRequest request = null;
        bool HasRequest = GetHasRequest(out request);
     //    System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"]);

        HttpSessionState _sess = (!HasRequest) ? null : HttpContext.Current.Session;
        SiteSetting ss = (_sess == null || _sess["Site"] == null) ? new SiteSetting() : (SiteSetting)_sess["Site"];
        em.SiteId = string.IsNullOrEmpty(ss.GetSiteID) ? "NULL" : ss.GetSiteID;
        UserMangement _user = (_sess == null || _sess["UserGuid"] == null) ? null : (UserMangement)_sess["UserGuid"];
        em.UserId = (_user == null) ? Guid.Empty : _user.Get_Guid;
        em.UserName = (_user == null) ? string.Empty : _user.User_Name;
        if (HasRequest)
        {
            em.ClientIP = Utilities.GetIP(request);
            em.Browser_Version = request.Browser.Browser + ", " + request.Browser.MajorVersion;
        }
        ILog LOG = LogManager.GetLogger("RollingFileAppenderError");
        LOG.Error(em, exc);
    }
    private static bool GetHasRequest(out HttpRequest hr)
    {
        hr = null;
        bool HasRequest = true;
        try
        {
            hr = HttpContext.Current.Request;
        }
        catch (Exception _exc)
        {
            HasRequest = false;
        }
        return HasRequest;
    }
    /*
    public static void AppStartLog(string message)
    {
        string path = ConfigurationManager.AppSettings["Logs"];
        path += "App_Start" + GetDateFileFormat(DateTime.Now) + ".log";
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Append))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(message);
                }
            }
        }
        finally { }
    }
     * */
    /*
    private static StringBuilder _ExceptionGlobalLog(Exception exc)
    {
        HttpRequest request = null;
        bool HasRequest = GetHasRequest(out request);
        string pagename = (!HasRequest) ? "NO REQUEST" : request.Url.AbsoluteUri;
       //  System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"]);

        HttpSessionState _sess = (!HasRequest) ? null : HttpContext.Current.Session;
        SiteSetting ss = (_sess == null || _sess["Site"] == null) ? new SiteSetting() : (SiteSetting)_sess["Site"];
        string siteId = string.IsNullOrEmpty(ss.GetSiteID) ? "NULL" : ss.GetSiteID;
        UserMangement _user = (_sess == null || _sess["UserGuid"] == null) ? new UserMangement() : (UserMangement)_sess["UserGuid"];
        string _guid = (string.IsNullOrEmpty(_user.GetGuid)) ? "NULL" : _user.GetGuid;
        string _name = (string.IsNullOrEmpty(_user.User_Name)) ? "NULL" : _user.User_Name;
        string InnerExc = ((exc.InnerException == null || string.IsNullOrEmpty(exc.InnerException.StackTrace)) ?
                        "NULL" : exc.InnerException.StackTrace);
        Exception exInner = exc.GetBaseException();
        string InnerException = (exInner == null) ? "NULL" : exInner.Message;

        StackTrace st = new StackTrace(exc, true);
        StackFrame[] sf = st.GetFrames();

        StackTrace stInner = null;
        StackFrame[] sfInner = null;

        if (exInner != null)
        {
            stInner = new StackTrace(exInner, true);
            sfInner = stInner.GetFrames();
        }             
  
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("date: " + DateTime.Now.ToString());

        sb.AppendLine("message: " + exc.Message);
        sb.AppendLine("HelpLink: " + exc.HelpLink);
        sb.AppendLine("Source: " + exc.Source);
        sb.AppendLine("StackTrace: " + exc.StackTrace);
  //      for(int i=0;i<sf.Length;i++){
        if (sf != null && sf.Length > 0)
            sb.AppendLine("Line Number: " + sf[0].GetFileLineNumber());
  //      }
        sb.AppendLine("TargetSite: " + exc.TargetSite);
        sb.AppendLine("Inner message: " + InnerException);
        sb.AppendLine("Inner StackTrace: " + InnerExc);
        if (sfInner != null && sfInner.Length > 0)
            sb.AppendLine("Inner Line Number: " + sfInner[0].GetFileLineNumber());
        sb.AppendLine("Page Name: " + pagename);
        sb.AppendLine("Site: " + siteId);
        sb.AppendLine("UserId: " + _guid);
        sb.AppendLine("User Name: " + _name);
        if (HasRequest)
        {
            sb.AppendLine("Server: " + request.ServerVariables["LOCAL_ADDR"]);
            sb.AppendLine("Server Name: " + request.ServerVariables["SERVER_NAME"]);
            sb.AppendLine("Client IP: " + Utilities.GetIP(request));
            sb.AppendLine("Browser: " + request.Browser.Browser + " Version: " + request.Browser.MajorVersion);
        }
        
        return sb;
    }

    */
    public static void ExceptionGlobalLog(Exception exc, object obj_message)
    {
        ILog LOG = LogManager.GetLogger("RollingFileAppenderError");
        LOG.Error(obj_message, exc);
    }
    public static void ExceptionGlobalLog(Exception exc)
    {
        if (HttpContext.Current == null || HttpContext.Current.Request == null)
        {
            ExceptionGlobalLog(exc, new { Global = "Global" });
            return;
        }
        var _details = new { Global = "Global", URL = HttpContext.Current.Request.Url.AbsoluteUri };
        ExceptionGlobalLog(exc, _details);
    }
    public static void ExceptionGlobalLog(HttpUnhandledException exc)
    {
        if (HttpContext.Current == null || HttpContext.Current.Request == null)
        {
            ExceptionGlobalLog(exc, new { general = "Global", info = "HttpCode: " + exc.GetHttpCode() });
            return;
        }
        ExceptionGlobalLog(exc, new { general = "Global", info = "HttpCode: " + exc.GetHttpCode(), URL = HttpContext.Current.Request.Url.AbsoluteUri });
    }
    public static void TranslationException(Exception exc, string PageName, string controlID, int SiteLangId)
    {
        string pagename = HttpContext.Current.Request.Url.AbsoluteUri;
 //        System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"]);
        string path = ConfigurationManager.AppSettings["Logs"];
        path += "Translate" + GetDateFileFormat(DateTime.Now) + ".log";
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Append))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine("date: " + DateTime.Now.ToString());
                    tw.WriteLine("PageName: " + PageName);
                    tw.WriteLine("controlID: " + controlID);
                    tw.WriteLine("SiteLangId: " + SiteLangId);
                    tw.WriteLine("message: " + exc.Message);
                    tw.WriteLine("HelpLink: " + exc.HelpLink);
                    tw.WriteLine("Source: " + exc.Source);
                    tw.WriteLine("StackTrace: " + exc.StackTrace);
                    tw.WriteLine("TargetSite: " + exc.TargetSite);
                    tw.WriteLine("Page Name: " + pagename);
                    tw.WriteLine();
                }
            }
        }
        catch (Exception ex) { }
    }
    /*
    public static void UrlEmpty(string domain, Guid OriginId, string SiteId)
    {
      //  string pagename = HttpContext.Current.Request.Url.AbsoluteUri;
        //        System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"]);
        string path = ConfigurationManager.AppSettings["Logs"];
        path += "UrlEmpty" + GetDateFileFormat(DateTime.Now) + ".log";
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Append))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine("date: " + DateTime.Now.ToString());

                    tw.WriteLine("Domain: " + domain);
                    tw.WriteLine("Origin: " + OriginId.ToString());
                    tw.WriteLine("SiteId: " + SiteId);
                    tw.WriteLine();
                }
            }
        }
        catch (Exception ex) { }
    }
    */
    private static string GetDateFileFormat(DateTime dt)
    {
        return dt.Day + "-" + dt.Month + "-" + dt.Year;
    }
    /*
    private static string GetPathGlobal()
    {
        string path = ConfigurationManager.AppSettings["Logs"];
        path += "Global" + GetDateFileFormat(DateTime.Now) + ".log";
        return path;
    }
    private static string GetPathSliderLog()
    {
        string path = ConfigurationManager.AppSettings["Logs"];
        path += "Slider_" + GetDateFileFormat(DateTime.Now) + ".log";
        return path;
    }
     * */
    static List<object> GetSliderStringFormat(SliderData sd, HttpContext context, eSliderLog _sliderlog,  out string _format)
    {
        int i = 0;
        List<object>  list = new List<object>();
        _format = "message: {" + (i++) + "}\r\n";
        list.Add(_sliderlog.ToString());
        if (sd != null)
        {
            PropertyInfo[] _pis = sd.GetType().GetProperties();
            foreach (PropertyInfo pi in _pis)
            {
                Object o = null;

                if (pi.PropertyType == typeof(string) || pi.PropertyType == typeof(Guid) || pi.PropertyType == typeof(bool))
                    o = pi.GetValue(sd, null);
                if (o != null)
                {
                    _format += pi.Name + ": {" + (i++) + "}\r\n";
                    list.Add(o);
                }
            }
        }
        if (context != null)
        {
            _format += "NoProblem URL: {" + (i++) + "}\r\n";
            list.Add(context.Request.Url.AbsoluteUri);
            _format += "Server:  {" + (i++) + "}\r\n";
            list.Add(context.Request.ServerVariables["LOCAL_ADDR"]);
            _format += "Server Name:  {" + (i++) + "}\r\n";
            list.Add(context.Request.ServerVariables["SERVER_NAME"]);
            _format += "Client IP:  {" + (i++) + "}\r\n";
            list.Add(Utilities.GetIP(context.Request));
            _format += "SessionId: {" + (i++) + "}\r\n";
            list.Add((context.Session == null) ? string.Empty : context.Session.SessionID);
            _format += "Browser:  {" + (i++) + "}\r\n";
            list.Add(context.Request.Browser.Browser);
            _format += "Version: {" + (i++) + "}";
            list.Add(context.Request.Browser.MajorVersion);
        }
        return list;
    }
    static string GetSliderStringFormatUpdate(Object obj)
    {
        int i = 0;
        
        string _format = string.Empty;// = "message: {" + (i++) + "}\r\n";

        foreach (PropertyInfo pi in obj.GetType().GetProperties())
        {
            _format += pi.Name + ": {" + (i++) + "}";//"message: {" + (i++) + "}\r\n";
        }
        return _format;
    }
    public static void SliderLog(WibiyaUser wu, HttpContext context, eSliderLog _sliderlog)
    {
        SliderData sd = new SliderData();
        sd.ControlName = eFlavour.Flavor_General_Search;
        if (wu != null)
        {
            sd.ClientDomain = wu.ClientDomain;
            sd.MaxResults = -1;
            sd.OriginId = wu.OriginId;
            sd.SiteId = wu.SiteId;
            sd.Url = wu.UrlReferrer;
        }
        string _format;
        ILog LOG = LogManager.GetLogger("SliderLogger");
        List<object> list = GetSliderStringFormat(sd, context, _sliderlog, out _format);
        LOG.InfoFormat(_format, list.ToArray());

    }
    public static void SliderLog(SliderData sd, HttpContext context, eSliderLog _sliderlog)
    {
        string format;
        List<object> list = GetSliderStringFormat(sd, context, _sliderlog, out format);
        ILog LOG = LogManager.GetLogger("SliderLogger");
        LOG.InfoFormat(format, list.ToArray());
        
    }
    public static void SliderLogUpdate(Object Messages)
    {        
        ILog LOG = LogManager.GetLogger("SliderLogger");
        string _format = GetSliderStringFormatUpdate(Messages);
        LOG.InfoFormat(_format, Messages);

    }
    public static void SliderLog(SliderData sd, HttpContext context, Exception exc)
    {
       // StringBuilder sb = _ExceptionGlobalLog(exc);
        string format;
        List<object> list = GetSliderStringFormat(sd, context, eSliderLog.Slider_Exception, out format);
        ILog LOG = LogManager.GetLogger("SliderErrorLogger");
        LOG.Error(string.Format(format, list.ToArray()), exc);
    }
    public static void SliderExceptionLog(HttpContext context, string message)
    {
        // StringBuilder sb = _ExceptionGlobalLog(exc);
        string format;
        List<object> list = new List<object>();// GetSliderStringFormat(sd, context, eSliderLog.Slider_Exception, out format);
        format = "message: {" + (0) + "}\r\n";
        list.Add(message);

        ILog LOG = LogManager.GetLogger("SliderErrorLogger");
        LOG.Error(string.Format(format, list.ToArray()), new Exception(message));
    }
    public static void SliderLog(WebReferenceCustomer.ServiceRequest _request, HttpContext context, Exception exc)
    {
      //  StringBuilder sb = _ExceptionGlobalLog(exc);
        SliderData sd = new SliderData();
        sd.ClientDomain = _request.Domain;
        eFlavour ef;
        if (Enum.TryParse(_request.ControlName, out ef))
            sd.ControlName = ef;
        sd.ExpertiseCode = _request.ExpertiseCode;
        sd.Keyword = _request.Keyword;
        sd.MaxResults = -1;
        sd.OriginId = _request.OriginId;
        sd.PageName = _request.PageName;
        sd.PlaceInWebSite = _request.PlaceInWebSite;
        sd.RegionCode = _request.RegionCode;
        sd.RegionLevel = _request.RegionLevel.ToString();
        sd.SiteId = _request.SiteId;
        sd.Url = _request.Url;
        SliderLog(sd, context, exc);
    }
    public static void SliderLog(SliderData sd, HttpContext context, eSliderLog _sliderlog, string Message)
    {
        string format;
        List<object> list = GetSliderStringFormat(sd, context, eSliderLog.Slider_Exception, out format);
        list.Add(Message);
        format += "Message:  {" + (list.Count - 1) + "}\r\n";
        ILog LOG = LogManager.GetLogger("SliderLogger");
        LOG.InfoFormat(format, list.ToArray());
    }
    /*
    public static void WriteLog(string mess, string RequestId)
    {
        string path = ConfigurationManager.AppSettings["Logs"];
        path += "RequestYoav" + GetDateFileFormat(DateTime.Now) + ".log";
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Append))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine("RequestId: " + RequestId);
                    tw.WriteLine("date: " + DateTime.Now.ToString());
                    tw.WriteLine("Details: " + mess);
                    tw.WriteLine();
                }
            }
        }
        catch (Exception ex) { }
    }
     * */
    public static void PelecardPayment(Exception exc, SupllierPayment sp)
    {
        PelecardPayment(exc, sp, null);
    }
    public static void PelecardPayment(Exception exc, SupllierPayment sp, string message)
    {
        string path = ConfigurationManager.AppSettings["Logs"];
        path += "PelecardPayment" + GetDateFileFormat(DateTime.Now) + ".log";
        Type type = typeof(SupllierPayment);
        PropertyInfo[] piArry = type.GetProperties();
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Append))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    if(!string.IsNullOrEmpty(message))
                        tw.WriteLine("Message: " + message);
                    tw.WriteLine("Date: " + DateTime.Now.ToString());
                    if(exc != null)
                        tw.WriteLine(ExceptionToLog(exc));
                    if (sp != null)
                    {
                        foreach (PropertyInfo pi in piArry)
                        {
                            object obj = (pi.GetValue(sp, null));
                            if(obj != null)
                                tw.WriteLine(pi.Name + ": " + (pi.GetValue(sp, null)).ToString());
                        }
                    }
                    tw.WriteLine("");
                }
            }
        }
        catch (Exception _exc)
        {
        }
    }
    private static string ExceptionToLog(Exception exc)
    {
        if (exc == null)
            return string.Empty;
        string InnerExc = ((exc.InnerException == null || string.IsNullOrEmpty(exc.InnerException.StackTrace)) ?
                        "NULL" : exc.InnerException.StackTrace);
        Exception exInner = exc.GetBaseException();
        string InnerException = (exInner == null) ? "NULL" : exInner.Message;

        StackTrace st = new StackTrace(exc, true);
        StackFrame[] sf = st.GetFrames();

        StackTrace stInner = null;
        StackFrame[] sfInner = null;

        if (exInner != null)
        {
            stInner = new StackTrace(exInner, true);
            sfInner = stInner.GetFrames();
        }

        StringBuilder sb = new StringBuilder();
        sb.AppendLine("date: " + DateTime.Now.ToString());

        sb.AppendLine("message: " + exc.Message);
        sb.AppendLine("HelpLink: " + exc.HelpLink);
        sb.AppendLine("Source: " + exc.Source);
        sb.AppendLine("StackTrace: " + exc.StackTrace);
        //      for(int i=0;i<sf.Length;i++){
        if (sf != null && sf.Length > 0)
            sb.AppendLine("Line Number: " + sf[0].GetFileLineNumber());
        //      }
        sb.AppendLine("TargetSite: " + exc.TargetSite);
        sb.AppendLine("Inner message: " + InnerException);
        sb.AppendLine("Inner StackTrace: " + InnerExc);
        if (sfInner != null && sfInner.Length > 0)
            sb.AppendLine("Inner Line Number: " + sfInner[0].GetFileLineNumber());
        return sb.ToString();
    }
    
    
}
