﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UpdateCacheResponse
/// </summary>
public enum eServerUpdate
{
    Success,
    PartialSuccess,
    DataFaildProblem,
    Faild,
    UserNotFound,
    UpdateInProcessing
}
public enum eIntextXmlUpdate
{
    Success,    
    Faild,
    UserNotFound
}
[Serializable()]
public class UpdateCacheResponse
{
    private eServerUpdate _status;
    public int CrmFailds { get; set; }
    public int WebServerFaild { get; set; }
    public string status
    {
        get { return _status.ToString(); }
    }
    public void SetStatus(eServerUpdate _status)
    {
        this._status = _status;
    }
	public UpdateCacheResponse()
	{
        CrmFailds = 0;
        WebServerFaild = 0;
	}
}
[Serializable()]
public class UpdateIntextXmlResponse
{
    private eIntextXmlUpdate _status;
    public List<string> OriginFailds { get; set; }
    public string status
    {
        get { return _status.ToString(); }
    }
    public void SetStatus(eIntextXmlUpdate _status)
    {
        this._status = _status;
    }
    public UpdateIntextXmlResponse()
    {
        _status = eIntextXmlUpdate.Success;
        OriginFailds = new List<string>();
    }
}