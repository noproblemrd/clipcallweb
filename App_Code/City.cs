﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for City
/// </summary>
[Serializable()]
public class City
{
    public string CityName { get; set; }
    public string StateAbbreviation { get; set; }
    public string StateFullName { get; set; }

	public City()
	{
		//
		
		//
	}
}