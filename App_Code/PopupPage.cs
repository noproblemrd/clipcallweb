﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for PopupPage
/// </summary>
public class PopupPage : PopupPageBase
{
    
    protected SliderViewData _SliderViewData;
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        
        _SliderViewData = SalesUtility.GetSliderViewData(sd);
        if (_SliderViewData == null)
        {
            Response.Flush();
            Response.End();
            return;
        }
        sd.ControlName = _SliderViewData.FlavorName;
        sd.InsertExposure();
    } 
    
}