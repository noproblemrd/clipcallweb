using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using AjaxControlToolkit;

/// <summary>
/// Summary description for PanelDisplay
/// </summary>
public class PanelDisplay : ScannerControls
{
    protected Dictionary<string, PanelValidate> _list;
    public PanelDisplay(PageSetting page):
        base(page)
	{
        string pageName=System.IO.Path.GetFileName(page.Request.ServerVariables["SCRIPT_NAME"]);
        this._list = DBConnection.GetPanelListDisplay(page.siteSetting.GetSiteID, page.siteSetting.siteLangId, pageName);
	}
    public Dictionary<string, PanelValidate> GetList
    {
        get
        {
            return _list;
        }
    }
    protected void SetPanelDisplay(Control control)
    {
        if (!string.IsNullOrEmpty(control.ID) && _list.ContainsKey(control.ID)
            && control.GetType() == (typeof(Panel)))
        {
            Panel _panel = ((Panel)control);
            _panel.Visible = _list[control.ID].Show;
            FindControl fn = new FindControl(control, typeof(RequiredFieldValidator));
            Control cnt = fn.StartTo();
            FindControl fn_txtBox = new FindControl(control, typeof(TextBox));
            TextBox _tb = (TextBox)fn_txtBox.StartTo();
            if (_tb != null)
            {
                //           _tb = (TextBox)(fn_txtBox.StartTo());
                _list[control.ID].txtBoxId = _tb.ClientID;
            }
            else
            {
                FindControl fn_ddl = new FindControl(control, typeof(DropDownList));
                DropDownList _ddl = (DropDownList)fn_ddl.StartTo();
                if(_ddl != null)
                    _list[control.ID].txtBoxId = _ddl.ClientID;
            }
            if (cnt != null)
            {
                ((RequiredFieldValidator)cnt).Visible = _list[control.ID].Valid;
                if (_list[control.ID].Valid)
                    _panel.CssClass = "must";
            }
            if (_list[control.ID].HasAuto)
            {
                fn = new FindControl(control, typeof(HiddenField));
                HiddenField _hf = (HiddenField)fn.StartTo();
                _list[control.ID].RegionGuid = _hf.Value;
                fn = new FindControl(control, typeof(AutoCompleteExtender));
                Control _autocom = fn.StartTo();
                fn = new FindControl(control, typeof(RequiredFieldValidator));
                string RequiredFieldValidatorId = ((RequiredFieldValidator)fn.StartTo()).ClientID;
                _list[control.ID].RequiredFieldValidatorId = RequiredFieldValidatorId;
                if (_autocom != null)
                {
                    bool _IsAutocom = _list[control.ID].AutoCom;
                    AutoCompleteExtender acex = ((AutoCompleteExtender)_autocom);
                    acex.Enabled = _IsAutocom;
               //     _tb.Attributes.Remove("onblur");
             //       _tb.Attributes.Add("onblur", _list[control.ID].onblur);
             
                    if (!_IsAutocom && !string.IsNullOrEmpty(_list[control.ID].onblur))
                    {                        
                        _tb.Attributes.Remove("onblur");
                        _tb.Attributes.Add("onblur", _list[control.ID].onblur);                        
                    }
 
                    if (_IsAutocom)
                    {
                        acex.ContextKey = _list[control.ID].AutoCompleteLevel.ToString();
                        
                        if (_list[control.ID].ParentPanelId == string.Empty)
                            new RegionService().SetList(_list[control.ID].AutoCompleteLevel, string.Empty);
                        if (string.IsNullOrEmpty(_hf.Value))                            
                            ////////////////////////////////////////////////////////////
                            _tb.Text = string.Empty;
                        _list[control.ID].txtValue = _tb.Text;
                    }
                }
            }            
        }
    }
    public List<string> StartTo()
    {
        List<string> list = new List<string>();
        if (_list.Count == 0)
            return list;
        myFunc func = new myFunc(SetPanelDisplay);
        ApplyToControls(_control, func);
        foreach (KeyValuePair<string, PanelValidate> kvp in _list)
        {
            if (!string.IsNullOrEmpty(kvp.Value.ParentPanelId))
            {
                kvp.Value.txtBoxParentId = _list[kvp.Value.ParentPanelId].txtBoxId;
                string region = _list[kvp.Value.ParentPanelId].RegionGuid;
                if (string.IsNullOrEmpty(region))
                    list.Add(kvp.Value.txtBoxId);
                else
                {
                    string result = new RegionService().SetList(kvp.Value.AutoCompleteLevel, region);
                    if (result == "false")
                        list.Add(kvp.Value.txtBoxId);
                }

            }
        }
        return list;
    }
}
