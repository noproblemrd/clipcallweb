﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

/// <summary>
/// Summary description for WebServiceNPUtilities
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceNPUtilities : System.Web.Services.WebService
{

    public WebServiceNPUtilities()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string UpsertAdvertiser(string siteId, string advertiserXml, string callBackUrl, string customParam)
    {
        WebReferenceSupplier.ResultOfGuid result = null;
        try
        {
            var client = WebServiceConfig.GetSupplierReference(null, siteId);
            result = client.UpsertAdvertiserApi(advertiserXml, callBackUrl, customParam);
        }
        catch (Exception exc)
        {
            LogEdit.SaveLog(siteId, string.Format("Exception in InsertAdvertiser. Exception: {0}", exc.Message));
            result = new WebReferenceSupplier.ResultOfGuid();
            result.Type = WebReferenceSupplier.eResultType.Failure;
            result.Messages = new string[1];
            result.Messages[0] = "Unexpected error ocurred";
        }
        XElement root = new XElement("NPResponse");
        XElement status = new XElement("Status", result.Type.ToString());
        root.Add(status);
        XElement messages = new XElement("Messages");
        foreach (var msg in result.Messages)
        {
            XElement msgElmt = new XElement("Message", msg);
            messages.Add(msgElmt);
        }
        root.Add(messages);
        XElement valueElmt = new XElement("Value");
        root.Add(valueElmt);
        valueElmt.Add(new XElement("requestId", result.Type == WebReferenceSupplier.eResultType.Success ? result.Value.ToString() : ""));
        return root.ToString();
    }

    [WebMethod]
    public string DepositToAdvertiser(string siteId, string advertiserId, int amount, Guid UserId)
    {
        WebReferenceSupplier.ResultOfCreateSupplierPricingResponse result = null;
        if (Utilities.IsGUID(advertiserId))
        {
            WebReferenceSupplier.CreateSupplierPricingRequest request = new WebReferenceSupplier.CreateSupplierPricingRequest();
            request.CreatedByUserId = UserId;
            request.BonusAmount = 0;
            request.BonusType = WebReferenceSupplier.BonusType.None;
            request.ExtraBonus = 0;
            request.PaymentAmount = amount;
            request.SupplierId = new Guid(advertiserId);
            request.UpdateRechargeFields = false;
            request.PaymentMethod = WebReferenceSupplier.ePaymentMethod.Cash;
            try
            {
                var client = WebServiceConfig.GetSupplierReference(null, siteId);
                result = client.CreateSupplierPricing(request);
            }
            catch (Exception exc)
            {
                LogEdit.SaveLog(siteId, string.Format("Exception in InsertAdvertiser. Exception: {0}", exc.Message));
                result = new WebReferenceSupplier.ResultOfCreateSupplierPricingResponse();
                result.Type = WebReferenceSupplier.eResultType.Failure;
                result.Messages = new string[1];
                result.Messages[0] = "Unexpected error ocurred";
            }
        }
        else
        {
            result = new WebReferenceSupplier.ResultOfCreateSupplierPricingResponse();
            result.Type = WebReferenceSupplier.eResultType.Failure;
            result.Messages = new string[1];
            result.Messages[0] = "advertiserId must be a valid Guid";
        }
        XElement root = new XElement("NPResponse");
        XElement status = new XElement("Status", result.Type.ToString());
        root.Add(status);
        XElement messages = new XElement("Messages");
        foreach (var msg in result.Messages)
        {
            XElement msgElmt = new XElement("Message", msg);
            messages.Add(msgElmt);
        }
        root.Add(messages);
        XElement valueElmt = new XElement("Value");
        root.Add(valueElmt);
        valueElmt.Add(new XElement("DepositId", result.Type == WebReferenceSupplier.eResultType.Success ? result.Value.PricingId.ToString() : ""));
        return root.ToString();
    }

    [WebMethod]
    public string ActivateAdvertiser(string siteId, string advertiserId)
    {
        WebReferenceSupplier.ResultOfSupplierStatus result = null;
        if (Utilities.IsGUID(advertiserId))
        {
            Guid guid = new Guid(advertiserId);
            try
            {
                var client = WebServiceConfig.GetSupplierReference(null, siteId);
                result = client.ActivateSupplier(guid, guid);
            }
            catch (Exception exc)
            {
                LogEdit.SaveLog(siteId, string.Format("Exception in ActivateAdvertiser. Exception: {0}", exc.Message));
                result = new WebReferenceSupplier.ResultOfSupplierStatus();
                result.Type = WebReferenceSupplier.eResultType.Failure;
                result.Messages = new string[1];
                result.Messages[0] = "Unexpected error ocurred";
            }
        }
        else
        {
            result = new WebReferenceSupplier.ResultOfSupplierStatus();
            result.Type = WebReferenceSupplier.eResultType.Failure;
            result.Messages = new string[1];
            result.Messages[0] = "advertiserId must be a valid Guid";
        }
        XElement root = new XElement("NPResponse");
        XElement status = new XElement("Status", result.Type.ToString());
        root.Add(status);
        XElement messages = new XElement("Messages");
        foreach (var msg in result.Messages)
        {
            XElement msgElmt = new XElement("Message", msg);
            messages.Add(msgElmt);
        }
        root.Add(messages);
        XElement valueElmt = new XElement("Value");
        root.Add(valueElmt);
        return root.ToString();
    }
    /*
    [WebMethod]
    public string DeactivateAdvertiser(string siteId, string advertiserId)
    {
        WebReferenceSupplier.Result result = null;
        if (Utilities.IsGUID(advertiserId))
        {
            Guid guid = new Guid(advertiserId);
            try
            {
                var client = WebServiceConfig.GetSupplierReference(null, siteId);
                result = client.DeactivateSupplier(guid, Guid.Empty);
            }
            catch (Exception exc)
            {
                LogEdit.SaveLog(siteId, string.Format("Exception in DeactivateAdvertiser. Exception: {0}", exc.Message));
                result = new WebReferenceSupplier.ResultOfSupplierStatus();
                result.Type = WebReferenceSupplier.eResultType.Failure;
                result.Messages = new string[1];
                result.Messages[0] = "Unexpected error ocurred";
            }
        }
        else
        {
            result = new WebReferenceSupplier.ResultOfSupplierStatus();
            result.Type = WebReferenceSupplier.eResultType.Failure;
            result.Messages = new string[1];
            result.Messages[0] = "advertiserId must be a valid Guid";
        }
        XElement root = new XElement("NPResponse");
        XElement status = new XElement("Status", result.Type.ToString());
        root.Add(status);
        XElement messages = new XElement("Messages");
        foreach (var msg in result.Messages)
        {
            XElement msgElmt = new XElement("Message", msg);
            messages.Add(msgElmt);
        }
        root.Add(messages);
        XElement valueElmt = new XElement("Value");
        root.Add(valueElmt);
        return root.ToString();
    }
     * */
    /*
    [WebMethod(true)]
    public string CompletePaymentOperation(string siteId, string paymentId, string guid, string transactionId)
    {
        string retVal;
        try
        {
            PaymentsManager manager = new PaymentsManager();
            retVal = manager.CompletePaymentOperation(siteId, paymentId, guid, transactionId);
        }
        catch (Exception exc)
        {
            LogEdit.SaveLog("Exception in method CompletePaymentOperation.",
                string.Format("siteId: {0}, paymentId: {1}.\r\nException message: {2},\r\nStack: {3}", siteId, paymentId, exc.Message, exc.StackTrace));
            retVal = "1, unexpected exception ocurred, please contact \"No Problem\"";
        }
        return retVal;
    }
    */
    [WebMethod]
    public string UpdateSupplierBizId(string biz_Id, string nP_Id, string siteId)
    {
        OneCallUtilities.UpdateSupplierBizIdRequest request = new OneCallUtilities.UpdateSupplierBizIdRequest();
        request.Biz_Id = biz_Id;
        bool isBadGuid = false;
        try
        {
            request.NP_Id = new Guid(nP_Id);
        }
        catch (Exception exc)
        {
            isBadGuid = true;
        }
        OneCallUtilities.Result result = null;
        if (isBadGuid == false)
        {
            request.SiteId = siteId;
            OneCallUtilities.OneCallUtilities client = WebServiceConfig.GetOneCallUtilitiesReference(null, siteId);
            result = client.UpdateSupplierBizId(request);
        }
        else
        {
            result = new OneCallUtilities.Result();
            result.Type = OneCallUtilities.eResultType.Failure;
        }
        return result.Type.ToString();
    }

    [WebMethod]
    public string ResetPassword(string phoneNumber, string siteId, string email)
    {
        WebReferenceSupplier.Supplier client = WebServiceConfig.GetSupplierReference(null, siteId);
        WebReferenceSupplier.ResetPasswordRequest request = new WebReferenceSupplier.ResetPasswordRequest();
        request.PhoneNumber = phoneNumber;
        request.Email = email;
        var response = client.ResetPassword(request);
        if (response.Type == WebReferenceSupplier.eResultType.Failure)
        {
            return "Error";
        }
        return response.Value.ResetPasswordStatus.ToString();
    }

    [WebMethod]
    public string ChangePassword(string email, string oldPassword, string newPassword, string siteId)
    {
        WebReferenceSupplier.Supplier client = WebServiceConfig.GetSupplierReference(null, siteId);
        string result = client.ChangePasswordByEmail(email, oldPassword, newPassword);
        return result;
    }

    [WebMethod]
    public string GetGeneralSupplierData(string supplierId, string originCode, string siteId)
    {
        if (Utilities.IsGUID(supplierId) == false)
        {
            return "supplierId must be a Guid - Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx)";
        }
        OneCallUtilities.OneCallUtilities client = WebServiceConfig.GetOneCallUtilitiesReference(null, siteId);
        var data = client.GetGeneralSupplierData(new Guid(supplierId), originCode);
        if (data.AccountId == Guid.Empty)
        {
            return "supplier not found";
        }
        XElement root = new XElement("GeneralSupplierData");
        XElement headingsElement = new XElement("Headings");
        foreach (var head in data.Expertises)
        {
            headingsElement.Add(new XElement("Heading", head));
        }
        root.Add(headingsElement);
        XElement regionsElement = new XElement("Regions");
        foreach (var reg in data.Regions)
        {
            regionsElement.Add(new XElement("Region", reg));
        }
        root.Add(regionsElement);
        root.Add(new XElement("Name", data.Name));
        root.Add(new XElement("NumberOfRequests", data.CallsNumber));
        root.Add(new XElement("DirectNumber", data.DirectNumber));
        root.Add(new XElement("NumberOfEmployees", data.EmployeeNumber));
        root.Add(new XElement("LongDescription", data.LongDescription));
        root.Add(new XElement("ShortDescription", data.ShortDescription));
        root.Add(new XElement("VideoUrl", data.Video));
        root.Add(new XElement("NumberOfSurveys", data.SurveyNumber));
        root.Add(new XElement("Likes", data.Likes));
        root.Add(new XElement("Dislikes", data.Dislikes));
        root.Add(new XElement("LogoUrl", GetLogo(data.AccountId)));
  //      XElement _xelement = GetImages(data.AccountId);
  //      if(_xelement != null)
  //          root.Add(_xelement);
        root.Add(GetImages(data.AccountId));
        return root.ToString();
    }

    private XElement GetImages(Guid supplierId)
    {
        XElement imagesElement = new XElement("Images");
        string imagesUrl = System.Configuration.ConfigurationManager.AppSettings["professionalImagesWebStandard"];
        string imagesFilePath = System.Configuration.ConfigurationManager.AppSettings["professionalImagesStandard"];
        string imagesUrlReady = string.Format(imagesUrl, supplierId);
        string imagesPathReady = string.Format(imagesFilePath, supplierId);
        if (!Directory.Exists(imagesPathReady))
            return imagesElement;
        string[] files = Directory.GetFiles(imagesPathReady);
        foreach (var file in files)
        {
            string extension = Path.GetExtension(file);
            if (IsImageExtension(extension))
            {
                string url = imagesUrlReady + Path.GetFileName(file);
                XElement imageElement = new XElement("ImageUrl", url);
                imagesElement.Add(imageElement);
            }
        }
        return imagesElement;
    }

    private bool IsImageExtension(string extension)
    {
        extension = extension.ToLower();
        bool retVal =
            extension == ".jpg"
            || extension == ".jpeg"
            || extension == ".png"
            || extension == ".pbm"
            || extension == ".gif";
        return retVal;
    }

    private string GetLogo(Guid supplierId)
    {
        string logosUrl = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];
        string logosFile = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
        string supplierIdStr = supplierId.ToString();
        string logoUrlReady = logosUrl + supplierId + ".jpg";
        string logoFile = logosFile + supplierId + ".jpg";
        if (File.Exists(logoFile))
            return logoUrlReady;
        else
            return string.Empty;
    }
}

