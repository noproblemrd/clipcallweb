﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RegionDirectory
/// </summary>
public class RegionDirectory
{
    public enum eStatusDirectory
    {
        zipCode,
        city,
        state
    }

    public string CityDirectory { get; set; }
    public string ZipCodeDirectory { get; set; }
    public string StateDirectory { get; set; }
    public eStatusDirectory StatusDirectory { get; set; }

	public RegionDirectory()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}