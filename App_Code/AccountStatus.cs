﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for AccountStatus
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class AccountStatus : System.Web.Services.WebService {

    public AccountStatus () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]    
    public string GetAccountStatus() {
        SiteSetting _site = (SiteSetting)Session["Site"];
        UserMangement _user = (UserMangement)Session["GuidSetting"];
        
        WebReferenceSupplier.SupplierStatusContainer result = _GetAccountStatus(_user);
        if (result == null)
            return string.Empty;
        
        string img_url = Utilities.GetRamzorImage(result.SupplierStatus, result.IsInactiveWithMoney);
        string _status = EditTranslateDB.GetControlTranslate(_site.siteLangId, "Supplier_Status", result.SupplierStatus.ToString());
        
        var _val = new { img_url = img_url, status = _status, CanGoOnDuty = result.CanGoOnDuty, UnavailabilityReasonName = result.UnavailabilityReasonName };

        JavaScriptSerializer jss = new JavaScriptSerializer();
        return jss.Serialize(_val);
 
    //    return _status + ";;;" + img_url;
    }
    public WebReferenceSupplier.SupplierStatusContainer _GetAccountStatus(UserMangement _user)
    {
        
        if (_user == null)
            return null;
        SiteSetting _site = (SiteSetting)Session["Site"];
        //   PageSetting ps = (PageSetting)Page;
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(_site.GetUrlWebReference);
        WebReferenceSupplier.ResultOfSupplierStatusContainer result = null;
        try
        {
            result = _supplier.GetSupplierStatus(_user.Get_Guid);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, _site);
            return null;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return null;

        return result.Value;
        
    }
    [WebMethod(true)]
    public string GetAccountStatusSupplier()
    {
        SiteSetting _site = (SiteSetting)Session["Site"];
        UserMangement _user = (UserMangement)Session["UserGuid"];

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(_site.GetUrlWebReference);
        WebReferenceSupplier.ResultOfSupplierStatusContainer result = null;
        try
        {
            result = _supplier.GetSupplierStatus(_user.Get_Guid);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, _site);
            return string.Empty;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return string.Empty;
        var _val = new { status = result.Value.SupplierStatus.ToString(), IsAvailableHours = result.Value.IsAvailableHours };

        JavaScriptSerializer jss = new JavaScriptSerializer();
        return jss.Serialize(_val);
    }
}
