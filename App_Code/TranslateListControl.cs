using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for TranslateListControl
/// </summary>
public class TranslateListControl : ScannerControls
{
    protected Dictionary<string, Dictionary<string, string>> dic;
    public TranslateListControl(Control page, Dictionary<string, Dictionary<string, string>> dic)
        : base(page)
	{
        this.dic = dic;
	}
    protected void TranslateLC(Control control)
    {
        if (!string.IsNullOrEmpty(control.ID) && dic.ContainsKey(control.ID)
            && control.GetType().IsSubclassOf(typeof(ListControl)))
        {       
/*            ListControl l_control = (ListControl)control;
            l_control.Items.Clear();
            foreach (KeyValuePair<string, string> kvp in dic[control.ID])
                l_control.Items.Add(new ListItem(kvp.Value, kvp.Key));
 * */
            ListControl l_control = (ListControl)control;
            foreach (KeyValuePair<string, string> kvp in dic[control.ID])
            {
                ListItem _li = l_control.Items.FindByValue(kvp.Key);
                if (_li != null)
                    _li.Text = kvp.Value;
     //           else
     //               l_control.Items.Add(new ListItem(kvp.Value, kvp.Key));
            }
        }
       
    }
    public void StartTo()
    {
        if (dic.Count == 0)
            return;
        myFunc func = new myFunc(TranslateLC);
        ApplyToControls( _control, func);
    }
    protected override bool SearchChilds(Control control)
    {
        if (!control.HasControls())
            return false;
        /*
        if (control == this._control)
            return true;
        if (control.GetType().IsSubclassOf(typeof(UserControl)))
            return false;
         * */
        return true;
    }
}
