﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AffiliatePage
/// </summary>
public class AffiliatePage : PageSetting
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!userManagement.IsAffiliate())
        {
            closeTheWindow("LogOut.aspx");
            return;
        }
        ScriptManager sm = ScriptManager.GetCurrent(this);
        if (sm == null || !sm.IsInAsyncPostBack)
        {
            HtmlGenericControl hgc = new HtmlGenericControl("script");
            hgc.Attributes["type"] = "text/javascript";
            hgc.Attributes["src"] = ResolveUrl("~/Publisher/PublisherScript.js");
            this.Header.Controls.Add(hgc);
            HtmlLink hl = new HtmlLink();
            hl.Href = ResolveUrl("~/UpdateProgress/updateProgress.css");
            hl.Attributes["rel"] = "stylesheet";
            hl.Attributes["type"] = "text/css";
            this.Header.Controls.Add(hl);
            Page.Controls.Add(Utilities.GetDivLoader());
            ClientScript.RegisterStartupScript(this.GetType(), "loaderInit", "appl_init();", true);
        }
        
    }
    
    public void closeTheWindow(string _pathLogout)
    {
        string _script = "if (window.opener) window.close(); else  window.location = '" + _pathLogout + "';";
        ScriptManager sm = ScriptManager.GetCurrent(this);
        if (sm == null || !sm.IsInAsyncPostBack)
            ClientScript.RegisterClientScriptBlock(this.GetType(), "CloseWindow", _script, true);
        else
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "CloseWindow", _script, true);
    }
}