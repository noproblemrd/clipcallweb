﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for ParamToAnchor
/// </summary>
public class ParamToAnchor : ScannerControls
{
    protected string _param;
    protected string _attribute;
    protected string UrlParams;
	public ParamToAnchor(Control _control, string _attribute, string _param):base(_control)
	{
        this._param = _param;
        this._attribute = _attribute;
        this.UrlParams = "?" + _attribute + "=" + HttpUtility.UrlEncode(_param);
	}
    protected void QueryStringAnchor(Control control)
    {
        
        if (control is HtmlAnchor)
        {
            HtmlAnchor _ha = (HtmlAnchor)control;
            _ha.HRef = GetCorrectLink(_ha.HRef);                
        }
        else if (control is HyperLink)
        {
            HyperLink _hl = (HyperLink)control;
            _hl.NavigateUrl = GetCorrectLink(_hl.NavigateUrl);
        }
        
    }
    public void StartTo()
    {
        myFunc func = new myFunc(QueryStringAnchor);
        ApplyToControls(_control, func);
    }
    string GetCorrectLink(string _link)
    {
        if (!_link.Contains("noproblemppc.com"))
            return _link;
        string[] _links = _link.Split('#');
        if (_links.Length == 1)
            return _link + UrlParams;
        if (_links.Length == 2)
            return _links[0] + UrlParams + "#" + _links[1];
        return _link;

    }
}