﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserControlTool
/// </summary>
public class UserControlTool : System.Web.UI.UserControl
{    

	public UserControlTool()
	{
		//
		
		//
//        
	}
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (Page.GetType() != typeof(PageSetting))
            return;
        string UserControlName = System.IO.Path.GetFileName(Server.MapPath(this.AppRelativeVirtualPath));
        GetTranslate(((PageSetting)Page).siteSetting.siteLangId, UserControlName);
    }
    void GetTranslate(int siteLangId, string UserControlName)
    {
        DBConnection.LoadTranslateToControl(this, UserControlName, siteLangId);
    }
}