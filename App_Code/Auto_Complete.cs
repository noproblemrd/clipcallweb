using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for Auto_Complete
/// </summary>
public class Auto_Complete
{
    public DateTime DateCreate;

    public SortedDictionary<string, string> list;
    public Auto_Complete(DateTime DateCreate, SortedDictionary<string, string> list)
	{
        
        this.list = list;
        this.DateCreate = DateCreate;
	}
}
