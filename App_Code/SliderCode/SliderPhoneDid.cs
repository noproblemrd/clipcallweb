﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for SliderPhoneDid
/// </summary>
public class SliderPhoneDid : SliderPage
{
    protected PpcSite _cache;
    protected HeadingCode _hc;
    protected string originId;
    protected string _tel;

    public SliderPhoneDid() { }
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        _cache = PpcSite.GetCurrent();
        _hc = _cache.headings[Slider_Data.ExpertiseCode];
        _tel = _hc.DidPhone.GetTel(Slider_Data);
        /*
        if(string.IsNullOrEmpty(_tel))
        {
            List<string> MessagesLog = new List<string>();
            MessagesLog.Add("Did phone Tel problem!!!!");
            MessagesLog.Add("ExposureId: " + Slider_Data._id.ToString());
            MessagesLog.Add("OriginId: " + Slider_Data.OriginId.ToString());
            MessagesLog.Add("Country: " + Slider_Data.Country);
            MessagesLog.Add("Expertise Code: " + Slider_Data.ExpertiseCode);
            dbug_log.MessageLog(MessagesLog.ToArray());
            ExecuteClodeSlider();
            return;
        }
        */
        originId = Slider_Data.OriginId.ToString();
        if(OpenDistinationPage)
            LoadDistinationPage();
        else
            HideSliderAfterDone();
        /*
        switch (Slider_Data.Type)
        {
            case(eExposureType.GoogleSterkly):
            case(eExposureType.Aducky):
            case(eExposureType.PastaLeads):
                HideSliderAfterDone();
                break;
            default:
                LoadDistinationPage();
                break;
        }
         * */
     
     //   LoadLinks();
    }
    /*
    private void LoadLinks()
    {
        string script = @"$(function(){
                $('.-whatThis').click(function(){
                    {0}
                });
            });";
        if(Slider_Data.OriginId == _cache.ConduitToolbarOriginId)

            script = script.Replace("{0}", GetOpenConduitWhatsThis());
        else if (Slider_Data.Type == eExposureType.Aducky)
        {
            /*
            string inner_script = @"openPopUp2('http://www.aducky.com/','settingWin',1000,600,'scrollbars=1,toolbar=1,menubar=1,titlebar=1',0,0);";
            script = script.Replace("{0}", inner_script);
             * * /
            return;
        }
        else
        {
            string inner_script = @"openPopUp2('http://noproblemppc.com/Consumer/SlidingWidgetFaq.aspx','settingWin',1000,600,'scrollbars=yes,toolbar=yes',0,0);";
            script = script.Replace("{0}", inner_script);
        }
        ClientScript.RegisterStartupScript(this.GetType(), "WhatsThisLink", script, true);
    }
*/
    protected void LoadHours(DropDownList ddl_CallBackHours)
    {
        Dictionary<string, string> dic = _hc.DidPhone.GetCallBackTimes(Slider_Data.TimeZone);
        ddl_CallBackHours.Items.Clear();
        foreach (KeyValuePair<string, string> kvp in dic)
            ddl_CallBackHours.Items.Add(new ListItem(kvp.Value, kvp.Key));
        ddl_CallBackHours.SelectedIndex = 0;
    }
    protected ListItem[] GetHours()
    {
        Dictionary<string, string> dic = _hc.DidPhone.GetCallBackTimes(Slider_Data.TimeZone);
        List<ListItem> list = new List<ListItem>();
        foreach (KeyValuePair<string, string> kvp in dic)
            list.Add(new ListItem(kvp.Value, kvp.Key));
        return list.ToArray();
    }
    private void LoadDistinationPage()
    {
        if (_cache.IsCJCampain(Slider_Data))
            ClientScript.RegisterStartupScript(this.GetType(), "SetDoneDistinationButton", "SetDoneDistinationButton('" + ((HeadingCampain)_hc).CampainDestination + "');", true);
        else
            ClientScript.RegisterStartupScript(this.GetType(), "SetDoneButton", "SetDoneButton('http://www.noproblem.me/main.aspx?keyword=" + Slider_Data.Keyword + "');", true);
    }
    private void HideSliderAfterDone()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "HideSliderAfterDone", "$('a.-doneButton').click(function () { _NpHideSlider(); });", true);
        
    }
    public void LoadRemoveSlider()
    {
        ClientScript.RegisterClientScriptBlock(this.GetType(), "CloseSlider", GetPostMessageScriptFunctionCloseSlider(), true);
    }
    protected bool ShowCallBack()
    {
        return _hc.DidPhone.IsMainCountry(Slider_Data.Country);
    }
    protected string GetCreateScheduledService
    {
        get { return ResolveUrl("~/npsb/SalesRequest.asmx/CreateScheduledService"); }
    }
    protected string PhoneRegulreExpression
    {
        get { return Utilities.RegularExpressionForJavascript(_cache.PhoneReg); }
    }
    protected string GetExposureId
    {
        get { return Slider_Data._id.ToString(); }
    }
    protected string GetHeadingCode
    {
        get { return Slider_Data.ExpertiseCode; }
    }
    protected string ClickEvent
    {
        get { return ResolveUrl("~/npsb/ClickRequest.ashx?ExposureId=" + Slider_Data._id.ToString()); }
    }
}