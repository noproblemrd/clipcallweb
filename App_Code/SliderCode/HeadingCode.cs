﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HeadingCode
/// </summary>
public enum eHeadingGroup
{
    none,
    attorney,
    IVR,
    mover,
    accountant,
    electrician,
    computerRepair,
    handyman,
    roofer,
    payday,
    creditCounselor,
    dentist,
    cleaner,
    hotel,
    flight,
    cruise,
    package,
    activity,
    food,
    personalInjuryAttorney,
  //  Immigration,
    dating,
    criminalAttorney,
    duiAttorney,
    divorceAttorney,
    bankruptcyAttorney,
    LifeInsurance,
    homeImprovement,
    Questions,
    locksmith,
    SsdAttorney,
    VehicleLoan,
    HomeSecurity,
    AutoInsurance,
    storage,
    RecruitingAdvertisers,
    BinaryOptions,
    healthInsurance
}
[Serializable()]
public class HeadingCode
{
    public HeadingCode(string heading, string code, string title, string SingularName, string WaterMark, string image, string SpeakToButtonName, string comment,
        Guid id, string slogan, string MobileTitle, string MobileDescription, bool intext, HeadingRelated[] headingRelated, bool IncludeGoogleAddon)
    {
        this.heading = heading;
        this.code = code;
        this.Title = title;
        this.SingularName = SingularName;
        this.WaterMark = WaterMark;
        this.image = image;
        this.SpeakToButtonName = SpeakToButtonName;
        this.Comment = comment;
        this.ID = id;
        this.Slogan = slogan;
        this.headingRelated = headingRelated;
        this.MobileDeviceTitle = MobileTitle;
        this.MobileDeviceDescription = MobileDescription;
        this.IncludeGoogleAddon = IncludeGoogleAddon;
        this.IsIntext = intext;
    }
    public HeadingCode(string heading, string code, string title, string SingularName, string WaterMark, string image, string SpeakToButtonName, string comment,
        Guid id, string slogan, string MobileTitle, string MobileDescription, bool intext, HeadingRelated[] headingRelated, bool IncludeGoogleAddon, HeadingDidPhone hdp)
        : this(heading, code, title, SingularName, WaterMark, image, SpeakToButtonName, comment, id, slogan,
            MobileTitle, MobileDescription, intext, headingRelated, IncludeGoogleAddon)
    {
        this.DidPhone = hdp;
    }
    public string heading { get; private set; }
    public string code { get; private set; }
    public string WaterMark { get; private set; }
    public string Title { get; private set; }
    public string SingularName { get; private set; }
    public string image { get; private set; }
    public string SpeakToButtonName { get; set; }
    public string Comment { get; private set; }
    public Guid ID { get; private set; }   
    public HeadingDidPhone DidPhone { get;  set; }
    public string Slogan { get; set; }
    public HeadingRelated[] headingRelated { get; set; }
    public List<string> CountryList { get; set; }
    public string MobileDeviceTitle { get; set; }
    public string MobileDeviceDescription { get; set; }
    public bool IncludeGoogleAddon { get; set; }
    public bool IsIntext { get; set; }

    //  public eHeadingGroup Group { get; set; }
    public void SetCountries(string country)
    {
        if (CountryList == null)
            CountryList = new List<string>();
        if (CountryList.Contains(country))
            return;
        CountryList.Add(country);
        CountryList.Sort();
    }
    public void SetCountries(string[] countries)
    {
        if (CountryList == null)
            CountryList = new List<string>();
        foreach (string country in countries)
        {
            if (CountryList.Contains(country))
                continue;
            CountryList.Add(country);
        }
        CountryList.Sort();
    }
    public bool IsServeCountry(string country, Guid OriginId)
    {
        if (CountryList == null)
            return false;
        bool HasCountry = CountryList.Contains(country);
        if(DidPhone == null || !HasCountry)
            return HasCountry;
        return DidPhone.IsWorkingTimeAndGoodOrigin(country, OriginId);
    }
    public string GetDescription(string OriginalDescription)
    {
        if (!string.IsNullOrEmpty(OriginalDescription) && OriginalDescription != WaterMark)
            return OriginalDescription;
        return GetDescription();
    }
    public string GetDescription()
    {
        return "I need " + (Utilities.ifVowel(SingularName) ? "an " : "a ") + SingularName.ToLower();
    }
    public string GetSingulareFoSentence
    {
        get { return (Utilities.ifVowel(SingularName) ? "an " : "a ") + SingularName.ToLower(); }
    }
    public virtual string GetTitle(SliderData sd, SliderViewData svd)
    {
        if (!string.IsNullOrEmpty(svd.AlternativeTitle))
            return svd.AlternativeTitle;
        return GetTitle(sd);
        
    }
    public virtual string GetTitle(SliderData sd)
    {        
        if (DidPhone == null)
            return this.Title;
        if (string.IsNullOrEmpty(DidPhone.Title))
            return this.Title;
        return DidPhone.Title;

    }
    public string GetDidPhoneSecondTitle
    {
        get
        {
            if (DidPhone == null)
                return this.SingularName;
            if (string.IsNullOrEmpty(DidPhone.SecondTitle))
                return this.SingularName;
            return DidPhone.SecondTitle;
        }
    }
    public virtual bool IsSupplierAvailable()
    {
        return true;
    }

    
}