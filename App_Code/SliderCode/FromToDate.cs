﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FromToDate
/// </summary>
[Serializable()]
public class FromToDate
{
    public DateTime From { get; private set; }
    public DateTime To { get; private set; }
	public FromToDate(int _from, int _to)
	{
        this.From = GetDateTime(_from);
        this.To = GetDateTime(_to);
	}
    DateTime GetDateTime(int hour)
    {
        if (hour > 23)
            return new DateTime(1, 1, 1, 23, 59, 59);
        return new DateTime(1, 1, 1, hour, 0, 0);
    }
}