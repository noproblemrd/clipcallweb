﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Supplier_Pricing_Component
/// </summary>
public class Supplier_Pricing_Component //: WebReferenceSupplier.SupplierPricingComponent
{
    public decimal Amount { get; set; }
    public decimal BaseAmount { get; set; }
    public ePaymentType PaymentType { get; set; }
    public string comment { get; set; }
	public Supplier_Pricing_Component()
	{
		//
		
		//
	}
    public Supplier_Pricing_Component(decimal amount, decimal BaseAmount, ePaymentType ept, string comment)
    {
        this.Amount = amount;
        this.BaseAmount = BaseAmount;
        this.comment = comment;
        this.PaymentType = ept;
    }
    public WebReferenceSupplier.SupplierPricingComponent GetCrmType()
    {
        WebReferenceSupplier.SupplierPricingComponent NewThis = new WebReferenceSupplier.SupplierPricingComponent();
        NewThis.AmountPayed = Amount;
        NewThis.BaseAmount = BaseAmount;
        NewThis.Type = GetCrmEnumType();
        return NewThis;
    }
    WebReferenceSupplier.eSupplierPricingComponentType GetCrmEnumType()
    {
        WebReferenceSupplier.eSupplierPricingComponentType espct;
        if (!Enum.TryParse(PaymentType.ToString(), out espct))
            espct = WebReferenceSupplier.eSupplierPricingComponentType.Manual;
        return espct;
    }
    public static List<WebReferenceSupplier.SupplierPricingComponent> GetListCrmType(List<Supplier_Pricing_Component> OldList, SupllierPayment sp)
    {
        List<WebReferenceSupplier.SupplierPricingComponent> list = new List<WebReferenceSupplier.SupplierPricingComponent>();
        if (OldList != null)
        {
            
            foreach (Supplier_Pricing_Component spc in OldList)
            {
                list.Add(spc.GetCrmType());
            }
        }
        if (sp.PaymentAmount > 0)
        {
            WebReferenceSupplier.SupplierPricingComponent NewSpc = sp.GetCrmType();
            if (NewSpc != null)
                list.Add(NewSpc);
        }
        return list;
    }
   
    
}