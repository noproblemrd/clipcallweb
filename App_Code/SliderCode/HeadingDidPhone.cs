﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HeadingDidPhone
/// </summary>
[Serializable()]
public class HeadingDidPhone 
{
    const string HOUR_FORMAT = "{0:h tt}";
    const string DAY_HOUR_FORMAT = "{0:dddd, h tt}";
    public int TimeZone { get; set; }
   // SortedList<Guid, string> Tel { get; set; }
    Dictionary<string, Dictionary<Guid, string>> CountryGuidTel { get; set; }
    public Dictionary<System.DayOfWeek, FromToDate> HoursWorks { get; set; }
    public string Title { get; set; }
    public string Image { get; set; }
    public string SecondTitle { get; set; }
    public bool IsRealTimeRouting { get; set; }
    public bool IsFormAndDid { get; set; }
//    bool _IsAlwaysAvailible;
   
    private volatile bool _IsValid;
    public bool IsAvailable
    {
        get { return _IsValid; }
        set { _IsValid = value; }
    }
    //for phone did intext only
    public HeadingDidPhone(Guid ExpertiseId, string UrlWebReference)
    {
       
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfPhoneDidSliderCampaignData result = null;
        try
        {
            result = _site.GetPhoneDidData(ExpertiseId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            return;
        }
        if (result.Value == null)
            return;
        string _title = (!result.Value.UseTitleOther || string.IsNullOrEmpty(result.Value.TitleOther)) ? string.Empty :
                   result.Value.TitleOther;
        //   Campaign.PhoneDid.IsRealTimeRouting
        string _titleSecond = (!result.Value.UseTitleOtherSecond || string.IsNullOrEmpty(result.Value.TitleOtherSecond)) ? string.Empty :
            result.Value.TitleOtherSecond;
        
        IsRealTimeRouting = false;
        HoursWorks = new Dictionary<System.DayOfWeek, FromToDate>();
        CountryGuidTel = new Dictionary<string, Dictionary<Guid, string>>();
        this.Image = result.Value.Image;
        this.Title = _title;
        this.SecondTitle = _titleSecond;
        this.TimeZone = ConvertToDateTime.GetTimeZoneInfo(result.Value.TimeZoneStandardName == null ? PpcSite.STANDARD_TIME_NAME_UK : result.Value.TimeZoneStandardName).GetUtcOffset(DateTime.Now).Hours;
    }
    public HeadingDidPhone(string timeZoneStandardName, Dictionary<string, WebReferenceSite.GuidStringPair[]> country_tel, string _image, string _title, string _SecondTitle,
        bool IsRealTimeRouting, WebReferenceSite.WorkingHoursData[] _WorkingHoursData, string HeadingCode, Guid PerionId)
	{
        this.TimeZone = ConvertToDateTime.GetTimeZoneInfo(timeZoneStandardName).GetUtcOffset(DateTime.Now).Hours;
        this.Image = _image;
        this.Title = _title;
        this.SecondTitle = _SecondTitle;
        /*
        this._IsAlwaysAvailible = IsAlwaysAvailible;
        if (IsAlwaysAvailible)
        {
            this.IsAvailable = true;
            this.IsRealTimeRouting = false;
        }
        else
        {
            this.IsRealTimeRouting = IsRealTimeRouting;
            this.IsAvailable = false;
        }
         * */
        this.IsRealTimeRouting = IsRealTimeRouting;
        this.IsAvailable = false;
        CountryGuidTel = new Dictionary<string, Dictionary<Guid, string>>();

        foreach (KeyValuePair<string, WebReferenceSite.GuidStringPair[]> kvp in country_tel)
        {
            CountryGuidTel.Add(kvp.Key, new Dictionary<Guid, string>());
            foreach (WebReferenceSite.GuidStringPair gsp in kvp.Value)
            {
                string _phone = GetShowPhone(gsp.Name);
                if (string.IsNullOrEmpty(_phone))
                {
                    PpcCacheException pce = new PpcCacheException("Direct phone empty");
                    Dictionary<string, string> _new = new Dictionary<string, string>();
                    _new.Add("phone", gsp.Name);
                    _new.Add("OriginId", gsp.Id.ToString());
                    _new.Add("Country", kvp.Key);
                    pce.NewValues = _new;
                    CacheLog.WriteException(pce);
                    continue;
                }
                if(CountryGuidTel[kvp.Key].ContainsKey(gsp.Id))
                {
                    PpcCacheException pce = new PpcCacheException("Origin phone exists");
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("phone", gsp.Name);
                    dic.Add("OriginId", gsp.Id.ToString());
                    dic.Add("Country", kvp.Key);
                    pce.NewValues = dic;
                    dic = new Dictionary<string, string>();
                    dic.Add("phone", CountryGuidTel[kvp.Key][gsp.Id]);
                    dic.Add("OriginId", gsp.Id.ToString());
                    dic.Add("Country", kvp.Key);
                    pce.OldValues = dic;
                    CacheLog.WriteException(pce);
                    continue;
                }
                
                CountryGuidTel[kvp.Key].Add(gsp.Id, _phone);
            }
            if (HeadingCode == "2289" && (kvp.Key == "UNITED STATES" || kvp.Key == "ISRAEL"))
            {
                if (!CountryGuidTel.ContainsKey(kvp.Key))
                    CountryGuidTel.Add(kvp.Key, new Dictionary<Guid, string>());
                if (CountryGuidTel[kvp.Key].ContainsKey(PerionId))
                    CountryGuidTel[kvp.Key][PerionId] = string.Empty;
                else
                    CountryGuidTel[kvp.Key].Add(PerionId, string.Empty);
            }
        }
        
     //   Tel = new SortedList<Guid, string>();
        

        HoursWorks = new Dictionary<System.DayOfWeek, FromToDate>();
        foreach (WebReferenceSite.WorkingHoursData whd in _WorkingHoursData)
        {
            System.DayOfWeek dow;
            if (!Enum.TryParse(whd.DayOfWeek.ToString(), true, out dow))
                continue;
            HoursWorks.Add(dow, new FromToDate(whd.From, whd.To));
        }
	}
    public bool IsMainCountry(string Country)
    {
        switch (Country)
        {
            case ("ISRAEL"):
            case ("UNITED STATES"):
                return true;
        }
        return false;
    }
    public bool IsAlwaysAvailible(string Country)
    {
        return !IsMainCountry(Country);
    }
    public string GetTelOrDefault(Guid OriginId, string _country)
    {
        string result = GetTel(OriginId, _country);
        if (!string.IsNullOrEmpty(result))
            return result;
    //    sd.OriginId = PpcSite.GetCurrent().UnknoenOriginId;
        return GetTel(PpcSite.GetCurrent().UnknoenOriginId, _country);
    }
    /*
    public string GetTel(SliderData sd)
    {
        if (!CountryGuidTel.ContainsKey(sd.Country))
            return null;
        if (CountryGuidTel[sd.Country].ContainsKey(sd.OriginId))
            return CountryGuidTel[sd.Country][sd.OriginId];
        if (CountryGuidTel[sd.Country].ContainsKey(Guid.Empty))
            return CountryGuidTel[sd.Country][Guid.Empty];       
        return null;
    }
    */
    public string GetTel(SliderData sd)
    {
        if(sd.SliderType == eSliderType.Intext)
            return GetDynamicPhone(sd.OriginId.ToString(), sd.GetHeadingGuid());
        if (!CountryGuidTel.ContainsKey(sd.Country))
            return null;
        string _tel = null;
        if (CountryGuidTel[sd.Country].ContainsKey(sd.OriginId))
            _tel = CountryGuidTel[sd.Country][sd.OriginId];
        else if(CountryGuidTel[sd.Country].ContainsKey(Guid.Empty))
            _tel = CountryGuidTel[sd.Country][Guid.Empty];
        if(_tel == null)
            return null;
        if (_tel == string.Empty)
            return GetDynamicPhone(sd.OriginId.ToString(), sd.GetHeadingGuid());
        return _tel;
    }
     
    public string GetDynamicPhone(string RSI, Guid ExpertiseId)
    {
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceCustomer.ResultOfString result = null;
        try
        {
            result = _customer.PerionGetDID(RSI, ExpertiseId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return null;
        }
        if(result.Type == WebReferenceCustomer.eResultType.Failure)
            return null;
        return GetShowPhone(result.Value);
        

    }
    /*
    public string GetTel(Guid OriginId, string _country, string RSI)
    {
        if (!CountryGuidTel.ContainsKey(_country))
            return null;
        string _tel = null;
        if (CountryGuidTel[_country].ContainsKey(OriginId))
            _tel = CountryGuidTel[_country][OriginId];
        else if (CountryGuidTel[_country].ContainsKey(Guid.Empty))
            _tel = CountryGuidTel[_country][Guid.Empty];
        if (_tel == null)
            return null;
        if (_tel == string.Empty)
            GetDynamicPhone(RSI);
        return _tel;
    }
    */
    public string GetTel(Guid OriginId, string _country)
    {        
        if (!CountryGuidTel.ContainsKey(_country))
            return null;
        if (CountryGuidTel[_country].ContainsKey(OriginId))
            return CountryGuidTel[_country][OriginId];
        if (CountryGuidTel[_country].ContainsKey(Guid.Empty))
            return CountryGuidTel[_country][Guid.Empty];
        return null;
    }
     
    private string GetShowPhone(string phone)
    {
        if (string.IsNullOrEmpty(phone) || (phone.Length != 10 && phone.Length != 11))
            return string.Empty;
        bool Is10 = (phone.Length == 10);
        string result = (Is10) ? "1-" : phone.Substring(0, 1) + "-";
        int StartIndex = (Is10) ? 0 : 1;
        result += phone.Substring(StartIndex, 3) + "-" + phone.Substring((StartIndex + 3), 3) + "-" + phone.Substring(StartIndex + 6);
        return result;
    }
    private string GetShowPhoneUK(string phone)
    {
        if (string.IsNullOrEmpty(phone) || (phone.Length != 10 && phone.Length != 11))
            return string.Empty;
        bool Is10 = (phone.Length == 10);
        string result = ((Is10) ? "0" + phone.Substring(0, 3) : string.Empty + phone.Substring(0, 4)) + " ";
        int StartIndex = (Is10) ? 3 : 4;
        result += phone.Substring(StartIndex, 3) + " " + phone.Substring((StartIndex + 3), 4);// +"-" + phone.Substring(StartIndex + 6);
        return result;
    }
    public void AddCountryPhons(string country, Dictionary<Guid, string> phones)
    {
        if (CountryGuidTel.ContainsKey(country))
            return;
        CountryGuidTel.Add(country, new Dictionary<Guid, string>());
        foreach (KeyValuePair<Guid, string> kvp in phones)
        {
            string _phone = GetShowPhoneUK(kvp.Value);
            if (string.IsNullOrEmpty(_phone) || CountryGuidTel[country].ContainsKey(kvp.Key))
                continue;
            CountryGuidTel[country].Add(kvp.Key, _phone);
        }
    }
    public bool IsWorkingTimeAndGoodOrigin(SliderData sd)
    {        
        return IsWorkingTimeAndGoodOrigin(sd.Country, sd.OriginId);
    }
    public bool IsWorkingTimeAndGoodOrigin(string _Country, Guid _OriginId)
    {
        if (!CountryGuidTel.ContainsKey(_Country))
            return false;
        if (!CountryGuidTel[_Country].ContainsKey(_OriginId) && !CountryGuidTel[_Country].ContainsKey(Guid.Empty))
            return false;
        if (IsAlwaysAvailible(_Country))
            return true;
        if (IsRealTimeRouting)
            return (IsAvailable);
        DateTime dt = DateTime.Now;
        dt = dt.AddHours(TimeZone);
        System.DayOfWeek _day = dt.DayOfWeek;
        dt = new DateTime(1, 1, 1, dt.Hour, dt.Minute, dt.Second);
        if (!HoursWorks.ContainsKey(_day))
            return false;
        return (dt > HoursWorks[_day].From && dt < HoursWorks[_day].To);
    }
    public Dictionary<string, string> GetCallBackTimes(int time_zone)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        DateTime dt = DateTime.UtcNow;
        DateTime dt_client = dt.AddHours(time_zone);
        DateTime dt_supplier = dt.AddHours(TimeZone);
        System.DayOfWeek dow = dt_supplier.DayOfWeek;
        int Diff_hours = time_zone - TimeZone;
        List<DateTime> list = GetCallBackTimes(dt_supplier);
        dic.Add("Now", "Now");
        foreach (DateTime date in list)
        {
            DateTime client_date = date.AddHours(Diff_hours);
            string show = string.Empty;
            if (client_date.Day - dt_client.Day == 0)
                show = "Today, " + string.Format(HOUR_FORMAT, client_date);
            else if (client_date.Day - dt_client.Day == 1)
                show = "Tomorrow, " + string.Format(HOUR_FORMAT, client_date);
            else
                show = string.Format(DAY_HOUR_FORMAT, client_date);
            dic.Add(date.AddHours(TimeZone * -1).ToString(), show);
        }
        
        return dic;
    }
    List<DateTime> GetCallBackTimes(DateTime dt_supplier)
    {
        List<DateTime> dic = new List<DateTime>();
        /*
        if (dt_supplier.Day == 23)
        {
            dt_supplier.AddDays(1);
            dt_supplier = new DateTime(dt_supplier.Year, dt_supplier.Month, dt_supplier.Day, 0, 0, 0);
        }
        else        
            dt_supplier = new DateTime(dt_supplier.Year, dt_supplier.Month, dt_supplier.Day, dt_supplier.Hour + 1, 0, 0);
         * */
        /*
       dt_supplier = dt_supplier.AddHours(1);
       dt_supplier = new DateTime(dt_supplier.Year, dt_supplier.Month, dt_supplier.Day, dt_supplier.Hour, 0, 0);
        */
        TimeSpan span = new TimeSpan(1, 0, 0);// one hour
        long ticks = (dt_supplier.Ticks + span.Ticks - 1) / span.Ticks;

        dt_supplier = new DateTime(ticks * span.Ticks);

        if (HoursWorks.Count == 0)
        {
            for (int _i = 0; _i < 11; _i++)
            {                
                dic.Add(dt_supplier);
                dt_supplier = dt_supplier.AddHours(1);
            }
            return dic;
        }
      //  System.DayOfWeek dow = dt_supplier.DayOfWeek;
      //  int _dayOfWeek = (int)dow;
        int i = 0;
        int j = 0;
        while (i < 11)
        {
            DateTime dt = new DateTime(1, 1, 1, dt_supplier.Hour, dt_supplier.Minute, dt_supplier.Second);
            if (!HoursWorks.ContainsKey(dt_supplier.DayOfWeek) || dt > HoursWorks[dt_supplier.DayOfWeek].To)
            {
                j++;
                if (j > 6)
                    break;
                TimeSpan spanDay = new TimeSpan(1, 0, 0, 0);// one day
                dt_supplier = dt_supplier.AddDays(1);
                long ticksDay = dt_supplier.Ticks / spanDay.Ticks;
                dt_supplier = new DateTime(ticksDay * spanDay.Ticks);
              //  dt_supplier = new DateTime(dt_supplier.Year, dt_supplier.Month, dt_supplier.Day + 1, 0, 0, 0);
                continue;
            }
            if (dt < HoursWorks[dt_supplier.DayOfWeek].From)
            {
                dt_supplier = dt_supplier.AddHours(1);
                continue;
            }            
            dic.Add(dt_supplier);
            dt_supplier = dt_supplier.AddHours(1);
            i++;
        }
        return dic;
    }
}