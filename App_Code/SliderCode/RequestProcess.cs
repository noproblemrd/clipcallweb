﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

/// <summary>
/// Summary description for RequestProcess
/// </summary>
public class RequestProcess
{
   // public WebReferenceCustomer.ServiceRequest _request { get; set; }
    public Guid ID { get; set; }
    public Guid RequestId { get; set; }
    public string Status { get; set; }
    public bool IsBroker { get; set; }
  //  string _RequestId;
    public RequestProcess(Guid _id, Guid RequestId, string status, bool IsBroker)
	{       
        this.ID = _id;
        this.RequestId = RequestId;
        this.Status = status;
        this.IsBroker = IsBroker;
	}
    
}