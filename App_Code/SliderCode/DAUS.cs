﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DAUS
/// </summary>
public class DAUS
{
    public Guid OriginId { get; set; }
    public int Hits { get; set; }
    public int Visitors { get; set; }
    public DateTime Date { get; set; }
    public int CountryId { get; set; }
    public string Page { get; set; }
	public DAUS()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}