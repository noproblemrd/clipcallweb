﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ResponseRequestCall
/// </summary>
public class ResponseRequestCall
{
    public string RequestId { get; set; }
    public string status { get; set; }
    public string plural { get; set; }
    public List<TitleTip> Tips { get; set; }
    public bool SentToBroker { get; set; }
    public string SuccessUrl { get; set; }

	public ResponseRequestCall()
	{
        Tips = new List<TitleTip>();
		//
		
		//
	}
}