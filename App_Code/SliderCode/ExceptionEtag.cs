﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ExceptionEtag
/// </summary>
public class ExceptionEtag : Exception
{
    string etag;
    string ModifiedSince;
    string _event;
    string QueryString;
    public ExceptionEtag(string etag, string ModifiedSince, string _event, string QueryString):
        base(string.Format("{0}: \r\netag: {1}\r\nModified-Since: {2}\r\nQueryString: {3}", _event, etag, ModifiedSince, QueryString))
    {
        this.etag = etag;
        this.ModifiedSince = ModifiedSince;
        this._event = _event;
        this.QueryString = QueryString;
    }
   
}