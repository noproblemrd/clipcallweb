﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConduitResponse
/// </summary>
public class ConduitResponse
{
    public bool HasIframe { get; set; }
    public string url { get; set; }
	public ConduitResponse()
	{
		
	}
    public ConduitResponse(bool HasIframe, string url)
    {
        this.HasIframe = HasIframe;
        this.url = url;
    }
}