﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Caching;

/// <summary>
/// Summary description for DataExposure
/// </summary>
public class DataExposure
{
    const int TableLength = 1000;
    static object _lock = new object();
//    static volatile bool IsExcuting = false;
    static int SiteId;
    static string SiteNameId;
    static SortedList<string, int> countries;
    
    DataTable data;
	DataExposure()
	{
        CreateTable();
        SiteNameId = PpcSite.GetCurrent().SiteId;
        SiteId = GetSiteId(SiteNameId);
        LoadCountriesList();
	}
   


   /*
    public static void AddExposure(Guid ID, SliderData sd, eExposureType ExposureType, bool IsAuthentic,
       eWelcomeExposureEvent ExposureEvent, HttpContext context)
    {

        AddExposure(ID, sd, ExposureType, IsAuthentic, ExposureEvent);
    }
    * */
    /*
    public static void AddExposure(Guid ID, SliderData sd, eExposureType ExposureType, bool IsAuthentic,
       eWelcomeExposureEvent ExposureEvent)
    {
        SalesUtility.WebExposure(ID, sd, ExposureType, IsAuthentic,
            ExposureEvent);
       
        
    }
    */
    void CreateTable()
    {
        data = new DataTable();
        data.Columns.Add("Id", typeof(Guid));
        data.PrimaryKey = new DataColumn[] { data.Columns["Id"] };
        data.Columns.Add("OriginId", typeof(Guid));
        data.Columns.Add("SiteId", typeof(Int32));
        data.Columns.Add("Domain", typeof(String));
        data.Columns.Add("Url", typeof(String));
        data.Columns.Add("HeadingCode", typeof(String));
        data.Columns.Add("ExposureType", typeof(String));
        data.Columns.Add("Channel", typeof(String));
        data.Columns.Add("PageName", typeof(String));
        data.Columns.Add("ControlName", typeof(String));
        data.Columns.Add("SessionId", typeof(String));
        data.Columns.Add("Keyword", typeof(String));
        data.Columns.Add("ToolbarId", typeof(String));
        data.Columns.Add("Title", typeof(String));
        data.Columns.Add("IP", typeof(String));
        data.Columns.Add("IsWelcomeScreenEvent", typeof(Boolean));
        data.Columns.Add("RegionCode", typeof(String));
        data.Columns.Add("Date", typeof(DateTime));
        data.Columns.Add("LoadedDate", typeof(DateTime));
        data.Columns.Add("Browser", typeof(String));
        data.Columns.Add("BrowserVersion", typeof(String));
        data.Columns.Add("PlaceInWebSite", typeof(String));
        data.Columns.Add("IsAuthentic", typeof(Boolean));
        data.Columns.Add("ClientScheme", typeof(String));
        data.Columns.Add("CountryId", typeof(Int32));
        data.Columns.Add("TimeZone", typeof(Int32));

    }
    /*
    public static SliderData GetExposure(Guid _id)
    {
        DataExposure de = GetDataExposure();
        SliderData sd = new SliderData();
        lock (_lock)
        {
            DataRow row = de._GetExposure(_id);
            if (row == null)
                return null;
            
            sd.SiteId = SiteNameId;
            sd.Browser = row["Browser"].ToString();
            sd.BrowserVersion = row["BrowserVersion"].ToString();
            sd.ClientDomain = row["Domain"].ToString();
            sd.ClientScheme = row["ClientScheme"].ToString();
            string cn = row["ControlName"].ToString();
            eFlavour efl;
            if (!Enum.TryParse(cn, out efl))
                efl = eFlavour.FlavorRegular0;
            sd.ControlName = efl;
            sd.Country = GetCountryName((int)row["CountryId"]);
            sd.ExpertiseCode = row["HeadingCode"].ToString();
            sd.ExposureId = _id;
            sd.Keyword = row["Keyword"].ToString();
            sd.OriginId = (Guid)row["OriginId"];
            sd.PageName = row["PageName"].ToString();
            sd.PlaceInWebSite = row["PlaceInWebSite"].ToString();
            sd.RegionCode = row["RegionCode"].ToString();
            sd.TimeZone = (int)row["TimeZone"];
            sd.title = row["Title"].ToString();
            sd.ToolbarId = row["ToolbarId"].ToString();
            sd.Url = row["Url"].ToString();
        }
        return sd;

    }
     * */
    public static bool UpdateHeadingExposure(Guid _id, string HeadingCode)
    {
        DataExposure de = GetDataExposure();
        DataRow row = de._GetExposure(_id);
        if (row == null)
            return false;
        row["HeadingCode"] = HeadingCode;
        return true;
    }
    static string GetCountryName(int Country_Id)
    {
        IEnumerable<string> query = from x in countries
                                    where x.Value == Country_Id
                                    select x.Key;
        if (query.Count() == 0)
            return string.Empty;
        return query.FirstOrDefault();
    }
    DataRow _GetExposure(Guid _id)
    {
        IEnumerable<DataRow> query = from x in data.AsEnumerable()
                                     where x.Field<Guid>("Id") == _id
                                     select x;
        if (query.Count() == 0)
            return null;
        return query.FirstOrDefault();
    }
    static DataExposure GetDataExposure()
    {
        
           
       
        return (DataExposure)HttpRuntime.Cache["Exposure"];
    }
    static void InsertRow(DataRow row)
    {
        
        DataTable data = GetDataExposure().data;
        try
        {
            data.Rows.Add(row);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (data.Rows.Count > TableLength)
        {
            GetDataExposure().BulkTable();
        }
    }
    /*
    private static int GetSiteId(string Country, out int CountryId)
    {
        DataExposure de =d; GetDataExposure();
        CountryId = de.countries.ContainsKey(Country) ? de.countries[Country] : 0;
        return de.SiteI
    }
     * */
    public static void FlushTable()
    {
        GetDataExposure().BulkTable();
    }
    private void BulkTable()
    {
        
        lock (_lock)
        {
            if (data.Rows.Count == 0)
                return;
            bool IsSuccess = true;
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                try
                {
                    conn.Open();
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);
                    bulkCopy.DestinationTableName = "dbo.SliderExposure";
                    bulkCopy.BulkCopyTimeout = 30;
                    bulkCopy.ColumnMappings.Add("Id", "Id");
                    bulkCopy.ColumnMappings.Add("OriginId", "OriginId");
                    bulkCopy.ColumnMappings.Add("SiteId", "SiteId");
                    bulkCopy.ColumnMappings.Add("Domain", "Domain");
                    bulkCopy.ColumnMappings.Add("Url", "Url");
                    bulkCopy.ColumnMappings.Add("HeadingCode", "HeadingCode");
                    bulkCopy.ColumnMappings.Add("ExposureType", "ExposureType");
                    bulkCopy.ColumnMappings.Add("Channel", "Channel");
                    bulkCopy.ColumnMappings.Add("PageName", "PageName");
                    bulkCopy.ColumnMappings.Add("ControlName", "ControlName");
                    bulkCopy.ColumnMappings.Add("SessionId", "SessionId");
                    bulkCopy.ColumnMappings.Add("Keyword", "Keyword");
                    bulkCopy.ColumnMappings.Add("ToolbarId", "ToolbarId");
                    bulkCopy.ColumnMappings.Add("Title", "Title");
                    bulkCopy.ColumnMappings.Add("IsWelcomeScreenEvent", "IsWelcomeScreenEvent");
                    bulkCopy.ColumnMappings.Add("RegionCode", "RegionCode");
                    bulkCopy.ColumnMappings.Add("Date", "Date");
                    bulkCopy.ColumnMappings.Add("LoadedDate", "LoadedDate");
                    bulkCopy.ColumnMappings.Add("Browser", "Browser");
                    bulkCopy.ColumnMappings.Add("BrowserVersion", "BrowserVersion");
                    bulkCopy.ColumnMappings.Add("PlaceInWebSite", "PlaceInWebSite");
                    bulkCopy.ColumnMappings.Add("IsAuthentic", "IsAuthentic");
                    bulkCopy.ColumnMappings.Add("ClientScheme", "ClientScheme");
                    bulkCopy.ColumnMappings.Add("CountryId", "CountryId");
                    bulkCopy.ColumnMappings.Add("TimeZone", "TimeZone");
                    bulkCopy.ColumnMappings.Add("IP", "IP");                     
                    bulkCopy.WriteToServer(data);
                    conn.Close();
                }
                catch (Exception exc)
                {
                    IsSuccess = false;
                    dbug_log.ExceptionLog(exc);
                    
                }
                if(IsSuccess)
                    data.Clear();
            }
            
        }
    }
    static DataRow GetRowSchema()
    {
        return GetDataExposure().data.NewRow();
    }
    
    public static void SetExposureCach(String key, Object item,
      CacheItemRemovedReason reason)
    {

        lock (_lock)
        {
            if (item != null && item.GetType() == typeof(DataExposure))
                ((DataExposure)item).BulkTable();
            DataExposure _data = new DataExposure();            
            HttpRuntime.Cache.Insert("Exposure", _data, null, Cache.NoAbsoluteExpiration,
                Cache.NoSlidingExpiration/* new TimeSpan(1, 0, 0)*/, CacheItemPriority.High, new CacheItemRemovedCallback(SetExposureCach));
            //     AddonTools.UpdateKeywordJs3(); 

        }

    }
    int GetSiteId(string _SiteNameId)
    {
        int _site = -1;
        string command = "select dbo.GetSiteIdBySiteName(@SiteNameId);";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", _SiteNameId);
            _site = (int)cmd.ExecuteScalar();
            conn.Close();
        }
        return _site;
    }
    void LoadCountriesList()
    {
        countries = new SortedList<string, int>();
        string command = "EXEC [dbo].[GetAllCountries]";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string name = (reader["IP2LocationName"] == DBNull.Value) ? null : (string)reader["IP2LocationName"];
                if (string.IsNullOrEmpty(name))
                    continue;
                if(!countries.ContainsKey(name))
                    countries.Add(name, (int)reader["Id"]);
            }
            conn.Close();
        }
    }
    static string SetNonNull(string s)
    {
        return (s == null) ? string.Empty : s;
    }
    static string SetNonNull(string s, int length)
    {
        if (s == null)
            return string.Empty;
        if (s.Length > length)
            return s.Substring(0, length);
        return s;
    }
}