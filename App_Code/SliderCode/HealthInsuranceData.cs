﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HealthInsuranceData
/// </summary>
public class HealthInsuranceData
{
    public bool? currentlyInsured { get; set; }
    public bool? tobbacoUse { get; set; }
    public int? peopleHousehold { get; set; }
    public int? income { get; set; }
    public bool? deniedCoverage { get; set; }
    public bool? pregnant { get; set; }
    public WebReferenceCustomer.eExistingCondition? currentMedicalConditions { get; set; }

	public HealthInsuranceData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public HealthInsuranceData(string data)
    {
        string[] strs = data.Split(new string[] {";;;"}, StringSplitOptions.RemoveEmptyEntries);

        bool _currentlyInsured;
        if (bool.TryParse(strs[0], out _currentlyInsured))
            this.currentlyInsured = _currentlyInsured;

        bool _tobbacoUse;
        if (bool.TryParse(strs[1], out _tobbacoUse))
            this.tobbacoUse = _tobbacoUse;

        int _peopleHousehold;
        if (int.TryParse(strs[2], out _peopleHousehold))
            this.peopleHousehold = _peopleHousehold;

        int _income;
        if (int.TryParse(strs[3], out _income))
            this.income = _income;

        bool _deniedCoverage;
        if (bool.TryParse(strs[4], out _deniedCoverage))
            this.deniedCoverage = _deniedCoverage;

        bool _pregnant;
        if (bool.TryParse(strs[5], out _pregnant))
            this.pregnant = _pregnant;

        WebReferenceCustomer.eExistingCondition _currentMedicalConditions;
        
        switch(strs[6].ToLower())
        {
            case "none":
              _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.None;
                break;

            case "aids/hiv":
                _currentMedicalConditions=  WebReferenceCustomer.eExistingCondition.AidsOrHiv;
                break;

            case "diabetes":
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Diabetes;
                break;

            case "liver disease":
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Liver;
                break;

            case "alzheimer's disease":
                 _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Alzheimers;
                break;

            case "lung disease":
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Lung;
                break;

            case "drug abuse":
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.DrugAbuse;
                break;

            case "mental illness":
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Mental;
                break;

            case "cancer":
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Cancer;
                break;

            case "heart disease":
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Heart;
                break;

            case "stroke":
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Stroke;
                break;         

            case "kidney disease":
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Kidney;
                break;         

            case "vascular disease":          
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.Vascular;
                break;

            default:
                _currentMedicalConditions= WebReferenceCustomer.eExistingCondition.None;
                break;
        }
            
        this.currentMedicalConditions=_currentMedicalConditions;
    }

    public WebReferenceCustomer.ServiceRequestHealthInsuranceData GetCrmData
    {

        get
        {
            WebReferenceCustomer.ServiceRequestHealthInsuranceData result = new WebReferenceCustomer.ServiceRequestHealthInsuranceData();
            result.IsInsured = this.currentlyInsured;
            result.IsSmoker = this.tobbacoUse;
            result.Household = this.peopleHousehold;
            result.Income = this.income;
            result.PreviouslyDenied = this.deniedCoverage;
            result.ExpectantParent = this.pregnant;
            result.ExistingCondition = this.currentMedicalConditions;

           
            return result;
        }
    }
}