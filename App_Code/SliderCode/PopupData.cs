﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for PopupData
/// </summary>
public class PopupData : ItemData
{
    

    
    public string ToolbarId { get; set; }   
    public string title { get; set; }
    
    public string ClientDomain { get; set; }
    public int TimeZone { get; set; }
    
    
   
 //   public string Domain { get; set; }
    public string Url { get; set; }
    public string Keyword { get; set; }
    public string OriginalKeywords { get; set; }
    public string RegionCode { get; set; }
    public string Browser { get; set; }
    public string BrowserVersion { get; set; }
    public string AppId { get; set; }
    public string IP { get; set; }       
    public eSliderType SliderType { get; set; }
    public Guid AdEngineCampaignId { get; set; }
    public string AdEngineCampaignName { get; set; }
    public string AdEngineCampaignUrl { get; set; }

    public DateTime expireAt
    {
        get { return Date.AddDays(7); }
    }
	public PopupData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public PopupData(HttpContext context)
    {
        this._id = MongoDB.Bson.ObjectId.GenerateNewId();
        Uri _UrlReferrer = null;
        try
        {
            _UrlReferrer = context.Request.UrlReferrer;
        }
        catch (UriFormatException ufexc)
        {
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        if (_UrlReferrer != null)
        {
            this.Url = _UrlReferrer.AbsoluteUri;
            this.ClientDomain = _UrlReferrer.Host;
            this.ClientScheme = _UrlReferrer.Scheme;
        }
        else
            this.ClientDomain = context.Request.QueryString["domain"];
        SetContextParams(context);
        string _ZipCode;

        this.Country = SalesUtility.GetCountryName(Utilities.GetIP(context.Request), context.Request.Url.Host, out _ZipCode);
        this.Date = DateTime.UtcNow;
        this.RegionCode = _ZipCode;

    }
    private void SetContextParams(HttpContext context)
    {
        PpcSite _cache = PpcSite.GetCurrent();
        this.Domain = context.Request.Url.Host;
        this.DomainName = GetDomainNameByDomainURL();
        this.title = context.Request.QueryString["title"];
        this.RegionCode = context.Request.QueryString["RegionCode"];
        this.AppId = context.Request.QueryString["appid"];
        string _tbid = context.Request.QueryString["mamid"];
        if (string.IsNullOrWhiteSpace(_tbid))
            _tbid = context.Request.QueryString["ToolbarId"];
        this.ToolbarId = _tbid;

        string _timezone = context.Request.QueryString["tz"];
        int _tz;
        if (!int.TryParse(_timezone, out _tz))
            _tz = GENERAL_TIME_ZONE;
        else
            _tz = (_tz / 60) * -1;
        this.TimeZone = _tz;
        string _OriginId = context.Request.QueryString["OriginId"];

        this.OriginId = _cache.GetOriginOrDefault(_OriginId);
        SetProductName(context);
        Guid _id;
        string _userid = context.Request.QueryString["UserUniqueId"];
        if (Guid.TryParse(_userid, out _id))
            this.UserId = _id;
        string cachesiteid = _cache.SiteId.ToLower();
        string querysiteid = (context.Request.QueryString["SiteId"] == null) ? "" : context.Request.QueryString["SiteId"].ToLower();
        querysiteid = querysiteid.ToLower();
        if (querysiteid != cachesiteid)
        {
            //        dbug_log.SliderLog(this, context, "SiteId missing: " + (string.IsNullOrEmpty(this.SiteId) ? "" : this.SiteId));
            this.SiteId = _cache.SiteId;
        }
        else
            this.SiteId = _cache.SiteId;

        this.SliderType = eSliderType.PopupApp;
        
        int num;
        if (!int.TryParse(context.Request.QueryString["apptype"], out num))
            this.AppType = eAppType.All;
        else
        {
            if (Enum.IsDefined(typeof(eAppType), num))
                this.AppType = (eAppType)num;
            else
                this.AppType = eAppType.All;
        }

        this.IP = Utilities.GetIP(context.Request);
        this.Browser = context.Request.Browser.Browser;
        this.BrowserVersion = context.Request.Browser.Version;
        this.Domain = context.Request.Url.Host;

    }
    public void SetPopupCampaignData(AdEngineCampaignData data)
    {
        this.AdEngineCampaignId = data.ID;
        this.AdEngineCampaignName = data.Name;
        this.AdEngineCampaignUrl = data.URL;
    }
    public override void InsertExposure()
    {
        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
        {
            using (System.Security.Principal.WindowsImpersonationContext wi = PpcSite.GetWindowsImpersonationContext)
            {
                try
                {
                    MongoDB.Driver.MongoCollection ColExposures = DBConnection.GetMongoPopupExposureCollection();
                    MongoDB.Driver.WriteConcernResult _result = ColExposures.Insert<PopupData>(this);
                    if (!_result.Ok)
                    {
                        var mess = new { Title = "Insert Faild", Message = _result.LastErrorMessage };
                        dbug_log.SliderLogUpdate(mess);

                    }
                }
                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc);
                }
            }

        }));

    }
    public static void UpdateExposure(MongoDB.Bson.ObjectId ExposureId)
    {
        DateTime LoadedDate = DateTime.UtcNow;
        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
        {
            using (System.Security.Principal.WindowsImpersonationContext wi = PpcSite.GetWindowsImpersonationContext)
            {
                try
                {

                    MongoDB.Driver.MongoCollection ColExposures = DBConnection.GetMongoPopupExposureCollection();
                    var query = MongoDB.Driver.Builders.Query.EQ("_id", ExposureId);
                    //     var sortBy = SortBy.Null;
                    var update = MongoDB.Driver.Builders.Update.Set("LoadedDate", LoadedDate); //("LoginCount",1).Set("LastLogin",DateTime.UtcNow);

                    MongoDB.Driver.WriteConcernResult _result = ColExposures.Update(query, update);
                    if (!_result.Ok)
                    {
                        var mess = new { Title = "Update Faild", Message = _result.LastErrorMessage };
                        dbug_log.SliderLogUpdate(mess);
                        return;
                    }
                    if (_result.DocumentsAffected < 1)
                    {
                        var mess = new { Title = "Update Faild", Message = "Update exposure don't take an affect", ExposureId = ExposureId.ToString() };
                        dbug_log.SliderLogUpdate(mess);

                    }


                }
                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc);
                }
            }

        }));
    }
    
}