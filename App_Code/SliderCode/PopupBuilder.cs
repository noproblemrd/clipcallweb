﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for PopupBuilder
/// </summary>
public class PopupBuilder
{
    const string THIRD_PARTY_POPUPS_HOW_MANY = "THIRD_PARTY_POPUPS_HOW_MANY";
    const string THIRD_PARTY_POPUPS_DELAY_INTERVAL_MINUTES = "THIRD_PARTY_POPUPS_DELAY_INTERVAL_MINUTES";
    const string THIRD_PARTY_POPUPS_TOTAL_DURATION_MINUTES = "THIRD_PARTY_POPUPS_TOTAL_DURATION_MINUTES";
    int _duration = 1;
    int _count = 10;
    int _delay = 1;
    public int Duration
    {
        get { return _duration * 60 * 1000; } //Minutes to millisecond
    }
    public int CountInDuration
    {
        get { return _count; }
    }
    public int Delay
    {
        get { return _delay * 60 * 1000; } //Minutes to millisecond
    }
    public PopupBuilder()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string GetPopupJs(PpcSite _cache, out string dataPopupScript)
    {

        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\popupProduct.1.1.js";
        string line;
        StringBuilder sbFile = new StringBuilder();

        if (File.Exists(path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    sbFile.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        sbFile.Replace("{HtmlPath}", "npsb/direct/data/datapop.html");
        sbFile.Replace("{PopupPath}", "npsb/direct/dp.aspx");
        sbFile.Replace("{IsLocal}", _cache.IsLocalMachine.ToString().ToLower());

        path = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\datapop.1.0.js";
        StringBuilder sbData = new StringBuilder();

        if (File.Exists(path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    sbData.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        XDocument xdoc = GetConfigurationSettings(_cache);
        if (xdoc == null)
            throw new Exception("Faild losd GetConfigurationSettings");
        string __interval = (from p in xdoc.Element("Configurations").Elements()
                             where p.Element("Key").Value == THIRD_PARTY_POPUPS_DELAY_INTERVAL_MINUTES
                         select p).FirstOrDefault().Element("Value").Value;
        string __HitPerDuration = (from p in xdoc.Element("Configurations").Elements()
                                  where p.Element("Key").Value == THIRD_PARTY_POPUPS_HOW_MANY
                            select p).FirstOrDefault().Element("Value").Value;
        string __Duration = (from p in xdoc.Element("Configurations").Elements()
                            where p.Element("Key").Value == THIRD_PARTY_POPUPS_TOTAL_DURATION_MINUTES
                            select p).FirstOrDefault().Element("Value").Value;

        if (!int.TryParse(__interval, out _delay))
            _delay = 0;
        if (!int.TryParse(__HitPerDuration, out _count))
            _count = 100;
        if (!int.TryParse(__Duration, out _duration))
            _duration = 0;

        sbData.Replace("{interval}", Delay.ToString());
        sbData.Replace("{HitPerDuration}", CountInDuration.ToString());
        sbData.Replace("{Duration}", Duration.ToString());

        GoogleClosure gc = new GoogleClosure();
        dataPopupScript = _cache.IsLocalMachine ? sbData.ToString() : gc.Compress(sbData.ToString(), false, _cache);        
        return _cache.IsLocalMachine ? sbFile.ToString() : gc.Compress(sbFile.ToString(), false, _cache);

    }
    private XDocument GetConfigurationSettings(PpcSite _cache)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_cache.UrlWebReference);
        string result = string.Empty;
        try
        {
            result = _site.GetConfigurationSettings();// GetUserTimeZone(userManagement.GetGuid);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            return null;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("Configurations") == null || xdd.Element("Configurations").Element("Error") != null)
        {
            Exception exc = new Exception("GetConfigurationSettings: CRM_FAILD");
            dbug_log.ExceptionLog(exc);
            return null;
        }
        return xdd;
    }
   
}