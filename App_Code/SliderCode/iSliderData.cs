﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for iSliderData
/// </summary>
public interface iSliderData
{
	SliderData Slider_Data { get; }
}