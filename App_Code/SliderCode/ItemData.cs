﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ItemData
/// </summary>
public abstract class ItemData
{
    protected const string GeneralScheme = "http";
    protected const string HTTPS_SCHEME = "https";
    protected const int GENERAL_TIME_ZONE = -5;
    protected const string EMPTY_PRODUCT_NAME = "$PRODUCT_NAME";

    protected const string NOPROBLEM_PRODUCT_NAME = "NoProblem";
    protected const string PASTALEADS_PRODUCT_NAME = "PastaLeads";
    protected const string DONUTLEADS_PRODUCT_NAME = "DonutsLeads";
    protected const string BBQLEADS_PRODUCT_NAME = "BbqLeads";
    protected const string SUSHILEADS_PRODUCT_NAME = "SushiLeads";

    string _Scheme;
    public MongoDB.Bson.ObjectId _id { get; set; }
    public eDomainName DomainName { get; set; }
    public string Domain { get; set; }
    public string ProductName { get; set; }
    public string SiteId { get; set; }
    public Guid OriginId { get; set; }
    public DateTime Date { get; set; }
    public DateTime LoadedDate { get; set; }
    public eAppType AppType { get; set; }
    public Guid UserId { get; set; }
    public string Country { get; set; }
    public string ClientScheme
    {
        get { return (string.IsNullOrEmpty(_Scheme)) ? GeneralScheme : _Scheme; }
        set { _Scheme = value; }
    }
	public ItemData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    protected eDomainName GetDomainNameByDomainURL()
    {
        if (this.Domain.EndsWith("noproblemppc.com"))
            return eDomainName.noproblemppc;
        if (this.Domain.EndsWith("donutleads.com"))
            return eDomainName.donutleads;
        if (this.Domain.EndsWith("pastaleads.com"))
            return eDomainName.pastaleads;
        if (this.Domain.EndsWith("bbqleads.com"))
            return eDomainName.bbqleads;
        if (this.Domain.EndsWith("shopwithboost.com"))
            return eDomainName.shopwithboost;
        if (this.Domain.EndsWith("sushileads.com"))
            return eDomainName.sushileads;
        return eDomainName.noproblemppc;
    }
    public void SetProductName(HttpContext context)
    {

        string _pn = context.Request.QueryString["ProductName"];
        if (string.IsNullOrEmpty(_pn) || _pn.Equals(EMPTY_PRODUCT_NAME))
        {
            SetProductName();
            return;
        }
        this.ProductName = _pn;
    }
    public void SetProductName()
    {
        string _pn = PpcSite.GetCurrent().GetOriginBroughtByName(this.OriginId);
        if (!string.IsNullOrEmpty(_pn))
        {
            this.ProductName = _pn;
            return;
        }
        this.ProductName = GetProductNameByDomain();


    }
    private string GetProductNameByDomain()
    {
        switch (this.DomainName)
        {
            case (eDomainName.noproblemppc):
                return NOPROBLEM_PRODUCT_NAME;
            case (eDomainName.pastaleads):
                return PASTALEADS_PRODUCT_NAME;
            case (eDomainName.donutleads):
                return DONUTLEADS_PRODUCT_NAME;
            case (eDomainName.bbqleads):
                return BBQLEADS_PRODUCT_NAME;
            case(eDomainName.sushileads):
                return SUSHILEADS_PRODUCT_NAME;
        }
        return PASTALEADS_PRODUCT_NAME;
    }
    public virtual string GetUrlParamsExposureId()
    {
        return "ExposureId=" + this._id.ToString();
    }
    public abstract void InsertExposure();
    public virtual bool IsDesktopAppOrigin(PpcSite _cache)
    {
        return _cache.IsDesktopOrigin(this.OriginId);
    }
    
}