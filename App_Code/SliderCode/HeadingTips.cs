﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HeadingTips
/// </summary>
[Serializable()]
public class HeadingTips
{
    public Guid Id { get; set; }
    public string Tip { get; set; }
    public string Title { get; set; }
	public HeadingTips(Guid id, string tip, string title)
	{
        this.Id = id;
        this.Tip = tip;
        this.Title = title;
	}
}
public class TitleTip
{
    public string Tip { get; set; }
    public string Title { get; set; }
    public TitleTip()
    {
    }
}