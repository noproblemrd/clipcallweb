﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EventArgsBuyCredit
/// </summary>

public class EventArgsBuyCredit : EventArgs
{
    public string type { get; set; }
    public int Amount { get; set; }
    public int BonusPercent { get; set; }
    public int BonusAmount { get; set; }
    public EventArgsBuyCredit()
    {
    }
}
