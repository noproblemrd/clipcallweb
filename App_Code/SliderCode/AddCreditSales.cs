﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Specialized;

/// <summary>
/// Summary description for AddCreditSales
/// </summary>
public class AddCreditSales
{
	public AddCreditSales()
	{
		//
		
		//
	}    
    public static string BuyCredit(Package _BuyCredit, SiteSetting siteSetting, Guid SupplierId)
    {
        

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(siteSetting.GetUrlWebReference);

        WebReferenceSupplier.GetSupplierChargingDataRequest request = new WebReferenceSupplier.GetSupplierChargingDataRequest();
        request.SupplierId = SupplierId;
        WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result;
        try
        {
            result = supplier.GetSupplierChargingData(request);
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            return string.Empty;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return string.Empty;
        WebReferenceSupplier.GetSupplierChargingDataResponse value = result.Value;
        //inside value is all the data...
        
        decimal CurrenBalance = value.CurrentBalance;
        //Response.Write(CurrenBalance);
      //  string Name = value.na
        string CompanyName = value.CompanyName;

        string FirstName = value.FirstName;
        string SecondName = value.LastName;
        string CompanyId = value.CompanyId;
        string Email = value.Email;
        string PhoneNumber = value.PhoneNumber;

        string Country = value.AddressCountry;
        string State = value.AddressState;
        string County = value.AddressCounty;
        string City = value.AddressCity;
        string Street = value.AddressStreet;
        string HouseNumber = value.AddressHouseNumber;
        string PostalCode = value.AddressPostalCode;
        string Currency = value.Currency;
        //Response.Write("Currency:" + Currency);
        string OtherPhone = value.OtherPhone;
        string WebSite = value.WebSite;
        string Fax = value.Fax;
        string CreditCardToken = value.CreditCardToken;
        string Last4DigitsCC = value.Last4DigitsCC;
        string  CardId = value.CreditCardOwnerId;
        string AccountNumber = value.AccountNumber;
        bool IsFirstTimeV = !string.IsNullOrEmpty(CreditCardToken);
        string StreetNumber = Street + " " + HouseNumber;
        WebReferenceSupplier.PaymentsKeyValuePair[]  ArrNumberPayments = value.PaymentsTable;
        /******* CreditCardToken ***********/


        /*
        int extra_bonus = value.RechargeBonusPercent;// string.IsNullOrEmpty(extra_bonus) ? "0" : extra_bonus;
        lbl_Title_AutoRecharge.Text = lbl_Title_AutoRecharge1.Text + " " + extra_bonus + "% " + lbl_Title_AutoRecharge2.Text;
        hf_ExtraBonus.Value = extra_bonus + "";
        */

     //   int Register_Stage = value.StageInRegistration;
        //Response.Write(Register_Stage);
        /*
        if (Register_Stage < 5)
        {
            string script = "parent.setInitRegistrationStep(" + Register_Stage + ");";
            ClientScript.RegisterStartupScript(this.GetType(), "GoToStep", script, true);
            IsFirstTimeV = true;
            //return;                   
        }
            * else if (Register_Stage == 5)
            IsFirstTimeV = true;
        else
            IsFirstTimeV = false;
            * */

        
        if (siteSetting.IfPaymentCharging == true)
        {
            if (siteSetting.CharchingCompany == eCompanyCharging.plimus)
            {
                int deposit = 0;
                int bonus = 0; ;
                decimal rate = 0;
                deposit = _BuyCredit.FromAmount;
                rate = (decimal)_BuyCredit.BonusPercent;
                bonus = _BuyCredit.BonusAmount;


                int RechargeAmount = -1;
                string auto_id = string.Empty;                
                string command = "EXEC dbo._SetPayment @site_id ,@company, @advertiser, @details, @RechargeAmount";
                using (SqlConnection conn = DBConnection.GetConnString())
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(command, conn);
                    cmd.Parameters.AddWithValue("@site_id", siteSetting.GetSiteID);
                    cmd.Parameters.AddWithValue("@company", "plimus");
                    cmd.Parameters.AddWithValue("@advertiser", CompanyName);
                    if (RechargeAmount == -1)
                        cmd.Parameters.AddWithValue("@RechargeAmount", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@RechargeAmount", RechargeAmount);
                    string paymentPlimusDetails = "";

                    //      int depositAgorot = int.Parse(deposit) * 100;

                    int _new_balance = Convert.ToInt32(CurrenBalance) + deposit;

                    string bonusType;

                    if (bonus <= 0)
                        bonusType = WebReferenceSupplier.BonusType.None.ToString();
                    else
                        bonusType = WebReferenceSupplier.BonusType.Package.ToString();

                    NameValueCollection nvServerVariables = (NameValueCollection)(HttpContext.Current.Request.ServerVariables);
                    string userAgent = nvServerVariables["HTTP_USER_AGENT"];
                    string hostIp = nvServerVariables["REMOTE_HOST"]; //Retrieves the remote host IP Address                
                    string clientIp = Utilities.GetIP(HttpContext.Current.Request);// nvServerVariables["REMOTE_ADDR"]; //Retrieves the user IP Address                
                    string acceptLanguage = nvServerVariables["HTTP_ACCEPT_LANGUAGE"];

                    paymentPlimusDetails = "supplierGuid=" + SupplierId +
                        "&supplierId=" + CompanyId +
                        "&advertiserName=" + HttpUtility.UrlEncode(CompanyName) +
                        "&balanceOld=" + CurrenBalance +
                        "&userId=" + SupplierId +
                        "&userName=" + HttpUtility.UrlEncode(CompanyName) +
                        "&siteId=" + siteSetting.GetSiteID +
                        "&siteLangId=" + siteSetting.siteLangId +
                        "&fieldId=" + " " + //lbl_YourBalance.ID +
                        "&fieldName=" + HttpUtility.UrlEncode(" ") + //lbl_YourBalance.Text) +
                        "&isFirstTimeV=" + IsFirstTimeV.ToString() +
                        "&bonusAmount=" + bonus +
                        "&balanceNew=" + _new_balance +
                        "&email=" + HttpUtility.UrlEncode(Email) +
                        "&phoneNumber=" + PhoneNumber +
                        "&otherPhone=" + OtherPhone +
                        "&country=" + Country +
                        "&city=" + City +
                        "&streetNumber=" + HttpUtility.UrlEncode(StreetNumber) +
                        "&postalCode=" + PostalCode +
                        "&webSite=" + HttpUtility.UrlEncode(WebSite) +
                        "&vat=" + siteSetting.Vat +
                        "&ifAccountingCharging=" + Convert.ToString(siteSetting.IfAccountingCharging) +
                        "&ppcPackage=" + //Server.UrlEncode(Hidden_PpcPackage.Value) +
                        "&currency=" + Currency +
                        "&accountNumber=" + AccountNumber +
                        "&fax=" + Fax + "&amount=" + deposit + "&bonusType=" + bonusType +
                        "&userAgent=" + HttpUtility.UrlEncode(userAgent) + "&hostIp=" + hostIp + "&clientIp=" +
                        clientIp + "&acceptLanguage=" + HttpUtility.UrlEncode(acceptLanguage);


                    cmd.Parameters.AddWithValue("@details", paymentPlimusDetails);

                    auto_id = Convert.ToString(cmd.ExecuteScalar());
                    conn.Close();
                }

                string __form = string.Empty;

                __form = (CompanyCharging.GetPlimusForm(siteSetting, deposit, auto_id, CompanyName, FirstName, SecondName, Email, StreetNumber, City, State, PostalCode, Country, PhoneNumber, Fax));

                return __form;


            }

        }
        return string.Empty;
    }



    public static string BuyCredit2(int deposit, SiteSetting siteSetting, Guid SupplierId, decimal RechargeAmount, string plan, bool isRecharge, string componentType, int bonusAmount)
    {


        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(siteSetting.GetUrlWebReference);

        WebReferenceSupplier.GetSupplierChargingDataRequest request = new WebReferenceSupplier.GetSupplierChargingDataRequest();
        request.SupplierId = SupplierId;
        WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result;
        try
        {
            result = supplier.GetSupplierChargingData(request);
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            return string.Empty;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return string.Empty;
        WebReferenceSupplier.GetSupplierChargingDataResponse value = result.Value;
        //inside value is all the data...

        decimal CurrenBalance = value.CurrentBalance;
        //Response.Write(CurrenBalance);
        //  string Name = value.na
        string CompanyName = value.CompanyName;
        string CreditCardOwnerName = value.CreditCardOwnerName;

        string FirstName="";
        string SecondName="";
        if (!string.IsNullOrEmpty(CreditCardOwnerName))
        {
            string[] arrSplitOwnerName = CreditCardOwnerName.Split(' ');
            if (arrSplitOwnerName.Length == 1)
                FirstName = arrSplitOwnerName[0];
            else
            {
                FirstName = arrSplitOwnerName[0];
                SecondName = arrSplitOwnerName[1];
            }
        }

        else
        {
            FirstName = value.FirstName;
            SecondName = value.LastName;
        }

        string CompanyId = value.CompanyId;
        string Email = value.Email;
        string PhoneNumber = value.PhoneNumber;

        string Country = value.AddressCountry;
        string State = value.AddressState;
        //HttpContext.Current.Response.Write("State:" + State);
        string County = value.AddressCounty;
        string City = value.AddressCity;
        string Street = value.AddressStreet;
        string HouseNumber = value.AddressHouseNumber;
        string PostalCode = value.AddressPostalCode;
        string Currency = value.Currency;
        //Response.Write("Currency:" + Currency);
        string OtherPhone = value.OtherPhone;
        string WebSite = value.WebSite;
        string Fax = value.Fax;
        string CreditCardToken = value.CreditCardToken;
        string Last4DigitsCC = value.Last4DigitsCC;
        string CardId = value.CreditCardOwnerId;
        string AccountNumber = value.AccountNumber;
        bool IsFirstTimeV = !string.IsNullOrEmpty(CreditCardToken);
        string StreetNumber = Street + " " + HouseNumber;
        WebReferenceSupplier.PaymentsKeyValuePair[] ArrNumberPayments = value.PaymentsTable;
        /******* CreditCardToken ***********/


        /*
        int extra_bonus = value.RechargeBonusPercent;// string.IsNullOrEmpty(extra_bonus) ? "0" : extra_bonus;
        lbl_Title_AutoRecharge.Text = lbl_Title_AutoRecharge1.Text + " " + extra_bonus + "% " + lbl_Title_AutoRecharge2.Text;
        hf_ExtraBonus.Value = extra_bonus + "";
        */

        //   int Register_Stage = value.StageInRegistration;
        //Response.Write(Register_Stage);
        /*
        if (Register_Stage < 5)
        {
            string script = "parent.setInitRegistrationStep(" + Register_Stage + ");";
            ClientScript.RegisterStartupScript(this.GetType(), "GoToStep", script, true);
            IsFirstTimeV = true;
            //return;                   
        }
            * else if (Register_Stage == 5)
            IsFirstTimeV = true;
        else
            IsFirstTimeV = false;
            * */


        if (siteSetting.IfPaymentCharging == true)
        {
            if (siteSetting.CharchingCompany == eCompanyCharging.plimus)
            {
                
                string auto_id = string.Empty;
                // sp SetPaymentZap with more details
                string command = "EXEC dbo._SetPayment @site_id ,@company, @advertiser, @details, @RechargeAmount";
                using (SqlConnection conn = DBConnection.GetConnString())
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(command, conn);
                    cmd.Parameters.AddWithValue("@site_id", siteSetting.GetSiteID);
                    cmd.Parameters.AddWithValue("@company", "plimus");
                    cmd.Parameters.AddWithValue("@advertiser", CompanyName);

                    if (RechargeAmount == -1)
                        cmd.Parameters.AddWithValue("@RechargeAmount", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@RechargeAmount", RechargeAmount);

                    string paymentPlimusDetails = "";            

                    NameValueCollection nvServerVariables = (NameValueCollection)(HttpContext.Current.Request.ServerVariables);
                    string userAgent = nvServerVariables["HTTP_USER_AGENT"];
                    string hostIp = nvServerVariables["REMOTE_HOST"]; //Retrieves the remote host IP Address                
                    string clientIp = Utilities.GetIP(HttpContext.Current.Request); //nvServerVariables["REMOTE_ADDR"]; //Retrieves the user IP Address                
                    string acceptLanguage = nvServerVariables["HTTP_ACCEPT_LANGUAGE"];

                    paymentPlimusDetails = "supplierGuid=" + SupplierId +
                        "&isRecharge=" + isRecharge.ToString() +
                        "&componentType=" + componentType +                         
                        "&supplierId=" + CompanyId +
                        "&advertiserName=" + HttpUtility.UrlEncode(CompanyName) +
                        "&balanceOld=" + CurrenBalance +
                        "&userId=" + SupplierId +
                        "&userName=" + HttpUtility.UrlEncode(CompanyName) +
                        "&siteId=" + siteSetting.GetSiteID +
                        "&siteLangId=" + siteSetting.siteLangId +
                        "&fieldId=" + " " + //lbl_YourBalance.ID +
                        "&fieldName=" + HttpUtility.UrlEncode(" ") + //lbl_YourBalance.Text) +
                        "&isFirstTimeV=" + IsFirstTimeV.ToString() +                                             
                        "&email=" + HttpUtility.UrlEncode(Email) +
                        "&phoneNumber=" + PhoneNumber +
                        "&otherPhone=" + OtherPhone +
                        "&country=" + Country +
                        "&city=" + City +
                        "&streetNumber=" + HttpUtility.UrlEncode(StreetNumber) +
                        "&postalCode=" + PostalCode +
                        "&webSite=" + HttpUtility.UrlEncode(WebSite) +
                        "&vat=" + siteSetting.Vat +
                        "&ifAccountingCharging=" + Convert.ToString(siteSetting.IfAccountingCharging) +
                        "&ppcPackage=" + //Server.UrlEncode(Hidden_PpcPackage.Value) +
                        "&currency=" + Currency +
                        "&accountNumber=" + AccountNumber +
                        "&fax=" + Fax +
                        "&amount=" + deposit +
                        "&bonusAmount=" + bonusAmount +
                        "&bonusType=" + WebReferenceSupplier.BonusType.Package.ToString() +                        
                        "&rechargeAmount=" + RechargeAmount + 
                        "&userAgent=" + HttpUtility.UrlEncode(userAgent) + "&hostIp=" + hostIp + "&clientIp=" +
                        clientIp + "&acceptLanguage=" + HttpUtility.UrlEncode(acceptLanguage);


                    cmd.Parameters.AddWithValue("@details", paymentPlimusDetails);

                    auto_id = Convert.ToString(cmd.ExecuteScalar());
                    conn.Close();
                }

                string __form = string.Empty;

                __form = (CompanyCharging.GetPlimusIframe(siteSetting, deposit, auto_id, CompanyName, FirstName, SecondName, Email, StreetNumber, City, State, PostalCode, Country, PhoneNumber, Fax, plan));

                return __form;


            }

        }
        return string.Empty;
    }
}