﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for Intext_XML_Builder
/// </summary>
public class Intext_XML_Builder
{
    PpcSite _cache;
    Guid OriginId;
    List<string> Headings;
    public Intext_XML_Builder(PpcSite _cache, Guid OriginId)
	{
        this._cache = _cache;
        this.OriginId = OriginId;
        Headings = new List<string>();
        Headings.Add("2289"); //Computer Repair
        Headings.Add("2158"); //Plumber
        Headings.Add("2137"); //Mover
	}
    public void BuildXML()
    {
        XDocument xdoc = new XDocument();
        XElement xelem_main = new XElement("feed");
        xelem_main.SetAttributeValue("update_time", DateTime.Now.ToString());

        var query = from x in _cache.headings
                    where  x.Value.IsIntext
                        && _cache.IntextFlavor.ContainsKey(x.Key)
             //           && Headings.Contains(x.Key)
                    select new { HeadingCode = x.Key, HeadingId = x.Value.ID, HeadingName = x.Value.heading };

        foreach(var q in query)
        {
            XElement xelem_category = new XElement("category");
            xelem_category.SetAttributeValue("name", HttpUtility.HtmlEncode(q.HeadingName));
            IEnumerable<KeywordsHeading> keywords = from x in _cache.KeywordsList
                                                    where x.IsPositiveKey &&
                                                        x.HeadingId == q.HeadingId
                                                    select x;

            SliderViewData svd = _cache.Flavors[_cache.IntextFlavor[q.HeadingCode]];
            foreach(KeywordsHeading kh in keywords)
            {
                XElement xelem_keyword = new XElement("keyword");
                xelem_keyword.SetAttributeValue("value", kh.Keyword);
                XElement xelem_intext = new XElement("intext");
                XElement xelem_url = new XElement("url");
                /*
                xelem_url.SetValue("http://app.pastaleads.com/npsb/IntextRout.ashx?OriginId=" + OriginId + "&keyword=" + HttpUtility.UrlEncode(kh.Keyword) + 
                    "&SliderType=" + ((int)eSliderType.Intext).ToString());
                 */
                XCData dataUrl = new XCData("http://app.pastaleads.com/npsb/IntextRout.ashx?OriginId=" + OriginId + "&keyword=" + HttpUtility.UrlEncode(kh.Keyword) +
                   "&SliderType=" + ((int)eSliderType.Intext).ToString());
                xelem_url.Add(dataUrl);
                XElement xelem_width = new XElement("width", svd.width.ToString());
                XElement xelem_height = new XElement("height", svd.height.ToString());

                xelem_intext.Add(xelem_url, xelem_width, xelem_height);
                xelem_keyword.Add(xelem_intext);
                xelem_category.Add(xelem_keyword);
            }
            xelem_main.Add(xelem_category);
        }
        xdoc.Add(xelem_main);
        string Path = System.Configuration.ConfigurationManager.AppSettings["IntextXml"] + OriginId.ToString();
        if (!Directory.Exists(Path))
            Directory.CreateDirectory(Path);
        Path += "\\intext.xml";
        xdoc.Save(Path);        
    }
}