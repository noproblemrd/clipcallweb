﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HomeSecurityData
/// </summary>
public class HomeSecurityData
{
   // WebReferenceCustomer.ServiceRequestHomeSecurityData sd;
    public static Dictionary<WebReferenceCustomer.eHomeSecurityTimeFrame, string> DicTimeFrame;

    public bool OwnProperty { get; set; }
    public WebReferenceCustomer.eHomeSecurityPropertyType PropertyType { get; set; }
    public WebReferenceCustomer.eHomeSecurityCreditRankings CreditRankings { get; set; }    
    public WebReferenceCustomer.eHomeSecurityTimeFrame TimeFrame { get; set; }

    static HomeSecurityData()
    {
        DicTimeFrame = new Dictionary<WebReferenceCustomer.eHomeSecurityTimeFrame, string>();
        DicTimeFrame.Add(WebReferenceCustomer.eHomeSecurityTimeFrame.now, "Now");
        DicTimeFrame.Add(WebReferenceCustomer.eHomeSecurityTimeFrame.one_two_weeks, "1-2 weeks");
        DicTimeFrame.Add(WebReferenceCustomer.eHomeSecurityTimeFrame.over_three_months, "Over 3 months");
        DicTimeFrame.Add(WebReferenceCustomer.eHomeSecurityTimeFrame.two_four_weeks, "2-4 weeks");
        DicTimeFrame.Add(WebReferenceCustomer.eHomeSecurityTimeFrame.two_three_months, "2-3 months");
        DicTimeFrame.Add(WebReferenceCustomer.eHomeSecurityTimeFrame.unknown, "unknown");
       
    }
	public HomeSecurityData(string _data)
	{
        string[] datas = _data.Split(new string[] { ";;;" }, StringSplitOptions.RemoveEmptyEntries);
        if (datas.Length != 4)
        {
            this.OwnProperty = true;
            return;
        }
        this.OwnProperty = datas[0].ToLower() == "yes";
        WebReferenceCustomer.eHomeSecurityPropertyType spt;
        if(Enum.TryParse(datas[1], out spt))
            this.PropertyType = spt;
        WebReferenceCustomer.eHomeSecurityCreditRankings scr;
        if (Enum.TryParse(datas[2], out scr))
            this.CreditRankings = scr;
        WebReferenceCustomer.eHomeSecurityTimeFrame stf;
        if (Enum.TryParse(datas[3], out stf))
            this.TimeFrame = stf;

	}
    public WebReferenceCustomer.ServiceRequestHomeSecurityData GetCrmData
    {

        get
        {
            WebReferenceCustomer.ServiceRequestHomeSecurityData result = new WebReferenceCustomer.ServiceRequestHomeSecurityData();
            result.CreditRanking = this.CreditRankings;
            result.OwnProperty = this.OwnProperty;
            result.PropertyType = this.PropertyType;
            result.TimeFrame = this.TimeFrame;
            return result;
        }
    }
}