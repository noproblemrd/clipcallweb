﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HeadingSpecialSupplier
/// </summary>
[Serializable()]
public class HeadingSpecialSupplier : HeadingCode
{
    public bool SupplierAvailable { get; set; }
    public string Email { get; set; }
	public HeadingSpecialSupplier(string heading, string code, string title, string SingularName, string WaterMark, string image, string SpeakToButtonName, string comment,
        Guid id, string slogan, string MobileTitle, string MobileDescription, bool intext, HeadingRelated[] headingRelated, bool _IsSupplierAvailable,
        string email, bool InluceGoogleAddon) :
        base(heading, code, title, SingularName, WaterMark, image, SpeakToButtonName, comment,
            id, slogan, MobileTitle, MobileDescription, intext, headingRelated, InluceGoogleAddon)
    {
        this.SupplierAvailable = _IsSupplierAvailable;
        this.Email = email;
    }
    /*
    public HeadingSpecialSupplier(string heading, string code, string title, string SingularName, string WaterMark, string image, string SpeakToButtonName, string comment,
        Guid id, string slogan, string MobileTitle, string MobileDescription, HeadingRelated[] headingRelated, HeadingDidPhone hdp,
        bool _IsSupplierAvailable, string email, bool InluceGoogleAddon)
        : base(heading, code, title, SingularName, WaterMark, image, SpeakToButtonName, comment,
            id, slogan, MobileTitle, MobileDescription, headingRelated, InluceGoogleAddon, hdp)
    {
        this.SupplierAvailable = _IsSupplierAvailable;
        this.Email = email;
    }
     * */
    public override bool IsSupplierAvailable()
    {
        return SupplierAvailable;
    }
}