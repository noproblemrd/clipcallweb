﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HeadingRelated
/// </summary>
[Serializable()]
public class HeadingRelated
{
    public string Code { get; set; }
    public string Name { get; set; }

	public HeadingRelated()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}