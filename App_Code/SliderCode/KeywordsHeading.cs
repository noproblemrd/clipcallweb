﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for KeywordsHeading
/// </summary>
[Serializable()]
public class KeywordsHeading
{
    public string Keyword { get; private set; }
    public Guid HeadingId { get; private set; }
    public bool IsPositiveKey { get; private set; }
    public int Priority { get; private set; }
    public KeywordsHeading(string _Keyword, Guid _HeadingId, bool _IsPositiveKey)
	{
        this.Keyword = _Keyword;
        this.HeadingId = _HeadingId;
        this.IsPositiveKey = _IsPositiveKey;
        this.Priority = 1;
	}
    public KeywordsHeading(string _Keyword, Guid _HeadingId, int _Priority)
    {
        this.Keyword = _Keyword;
        this.HeadingId = _HeadingId;
        this.IsPositiveKey = true;
        this.Priority = _Priority;
    }
}