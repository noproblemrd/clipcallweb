﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PpcCacheException
/// </summary>
public class PpcCacheException : Exception
{
   
    public Dictionary<string, string> OldValues { get; set; }
    public Dictionary<string, string> NewValues { get; set; }
    public PpcCacheException(string Message)
        : base(Message)
	{
		//
		// TODO: Add constructor logic here
		//
        
	}
}