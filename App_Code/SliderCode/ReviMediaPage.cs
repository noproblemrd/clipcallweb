﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for ReviMediaPage
/// </summary>
public class ReviMediaPage : SliderPage
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        setLinks();

    }
    protected void setLinks()
    {
        System.Web.UI.HtmlControls.HtmlGenericControl si = new System.Web.UI.HtmlControls.HtmlGenericControl();
        si = Utilities.setLinks(Slider_Data);

        this.Page.Header.Controls.AddAt(0, si);

        this.Page.Header.Controls.Add(new LiteralControl("\n"));

    }   
}