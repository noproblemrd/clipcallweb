﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RequestSupplier
/// </summary>
public class RequestSupplier
{
    /*
     * <Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" 
     * NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" />
     * */
    public string SupplierId { get; set; }
 //   Guid _SupplierId;
    public string SupplierNumber { get; set; }
    public string SupplierName { get; set; }
    public string SumSurvey { get; set; }
    public string SumAssistanceRequests { get; set; }
    public string NumberOfEmployees { get; set; }
    public string ShortDescription { get; set; }
    public string Status { get; set; }
    public string Likes { get; set; }
    public string Dislikes { get; set; }
    public string StreetNumber { get; set; }
    public string Street { get; set; }
    public string City { get; set; }
    public string Reviews { get; set; }
   
	public RequestSupplier()
	{
		//
		
		//
	}
}