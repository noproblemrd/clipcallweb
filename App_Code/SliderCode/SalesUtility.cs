﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.IO;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Xml.Linq;

/// <summary>
/// Summary description for SalesUtility
/// </summary>
public enum eLoginReferrer { earn_credit, add_credit, billing, invite_pro  }
public enum eWelcomeExposureEvent { none, Exposure, Enable, Disable, xEnable }
public class SalesUtility
{
    public const string COOKIE_NAME = "NoProblemCounterSession";
    public const string COOKIE_INTEXT_NAME = "IntextCountSess";
//    public const string COOKIE_NAME
 //   const string ZIPCODE_REGULARE_EXPRESSION = @"^\d{5}$";
    static List<eFlavour> ListFlavorBlockCRM;
    static Dictionary<Guid, string> AdvertiseCallBackExpertise;
    static readonly string InjEvent;
	public SalesUtility()
	{
		//
		
		//
	}
    static SalesUtility()
    {
        ListFlavorBlockCRM = new List<eFlavour>();
        ListFlavorBlockCRM.Add(eFlavour.NoProblemHomepage);
        ListFlavorBlockCRM.Add(eFlavour.Flavor_Directory_Business);
        ListFlavorBlockCRM.Add(eFlavour.Flavor_Half_Controls_Top_Cj);
        ListFlavorBlockCRM.Add(eFlavour.Flavor_Half_Controls_Top_Hotel_Booking);
        ListFlavorBlockCRM.Add(eFlavour.Flavor_Half_Controls_Top_Cruise_Expedia);
        ListFlavorBlockCRM.Add(eFlavour.Flavor_Half_Controls_Top_Package_Expedia);
        ListFlavorBlockCRM.Add(eFlavour.Flavor_Half_Controls_Top_Activity_Expedia);
        ListFlavorBlockCRM.Add(eFlavour.Flavor_Half_Controls_Top_Food_Artizone);
        ListFlavorBlockCRM.Add(eFlavour.Flavor_General_Search);
        ListFlavorBlockCRM.Add(eFlavour.WelcomeScreen);
        ListFlavorBlockCRM.Add(eFlavour.Conduit_General_Search);
        ListFlavorBlockCRM.Add(eFlavour.Flavor_Top_Questions);
        ListFlavorBlockCRM.Sort();

        AdvertiseCallBackExpertise = new Dictionary<Guid, string>();
        AdvertiseCallBackExpertise.Add(new Guid("8447D937-5E81-416A-B147-5CCBB11B4A2F"), "2006");//ReviMedia Home Security
        AdvertiseCallBackExpertise.Add(new Guid("2876D4B2-97B5-4242-8F7A-FDD411FEBF8D"), "2065");//1800 Dentist
        AdvertiseCallBackExpertise.Add(new Guid("122831FC-6083-488B-8198-093FE332CDE2"), "2298");//Roundsky I-Frame

        string line;
        StringBuilder sb = new StringBuilder();
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\InjEvent.js";
        if (File.Exists(path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    sb.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        InjEvent = sb.ToString();

    }
    public static void SetRegisterUser(HttpContext context, Guid Id, decimal balance, string phone)
    {
        context.Session["RegisterUser"] = Id;
        context.Session["SupplierType"] = eSupplierRegisterType.AAR;
        context.Session["_balance"] = (double)balance;
        context.Session["PhoneNum"] = phone;
    }
    public static string GetInjEventScript(bool ShowEvent)
    {
        string str = InjEvent.Replace("{TB}", ShowEvent.ToString().ToLower());
        return str;
    }
    /*
    public static string Exposure(string SiteId, string OriginId, string Domain,
      string Url, string HeadingCode, int HeadingLevel, string RegionCode,
      string RegionLevel, string Channel, string ExposureType,
      string PageName, string PlaceInWebSite, string ControlName,
      string SessionId, string Keyword, string ToolbarId, string UserId, string title, string Browser, string BrowserVersion,
      eWelcomeExposureEvent ExposureEvent, HttpContext context
      )
    {
        return Exposure(SiteId, OriginId, Domain,
                   Url, HeadingCode, HeadingLevel, RegionCode,
                   RegionLevel, Channel, ExposureType,
                   PageName, PlaceInWebSite, ControlName,
                   SessionId, Keyword, ToolbarId, UserId, title, Browser, BrowserVersion,
                  ExposureEvent, Utilities.GetIP(context.Request));
    }
     * */
    public static string Exposure(string SiteId, string OriginId, string Domain,
       string Url, string HeadingCode, int HeadingLevel, string RegionCode,
       string RegionLevel, string Channel, string ExposureType,
       string PageName, string PlaceInWebSite, string ControlName,
       string SessionId, string Keyword, string ToolbarId, string UserId, string title, string Browser, string BrowserVersion,
       eWelcomeExposureEvent ExposureEvent,  string _UserHostAddress
       )
    {
        /*Must this parameters SiteId,OriginId,HeadingCode,HeadingLevel,RegionCode,RegionLevel,ExposureType*/
        List<string> arrMust = new List<string>();
        arrMust.Add(SiteId);
        arrMust.Add(OriginId);
        arrMust.Add(ExposureType);
        //arrMust[4] = HeadingCode;
        //arrMust[5] = HeadingLevel;

        //      bool ifAnyNullOrEmpy = ifNullOrEmpty(arrMust);
        if (arrMust == null || arrMust.Count == 0)
        {
            eFlavour _ef;
            if (!Enum.TryParse(ControlName, out _ef))
                _ef = eFlavour.Flavor_General;
            dbug_log.SliderLog(new SliderData()
            {
                SiteId = SiteId,
                OriginId = new Guid(OriginId),
                Keyword = Keyword,
                ExpertiseCode = HeadingCode,
                ClientDomain = Domain,
                Url = Url,
                RegionCode = RegionCode,
                PageName = PageName,
                PlaceInWebSite = PlaceInWebSite,
                ControlName = _ef
            }, HttpContext.Current, eSliderLog.Missing_Params);
            //       return "Failure: Required parameter is missing";
        }

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        customer.Timeout = 300000;
        WebReferenceCustomer.CreateExposureRequest request = new WebReferenceCustomer.CreateExposureRequest();

        request.SiteId = SiteId;
        request.OriginId = new Guid(OriginId);
        request.Domain = Domain;
        request.Url = Url;
        request.IP = _UserHostAddress;

        request.ToolbarId = ToolbarId;
        request.Browser = Browser;
        request.BrowserVersion = BrowserVersion;

        if (!String.IsNullOrEmpty(HeadingCode))
        {
            request.HeadingCode = HeadingCode;
            request.HeadingLevel = HeadingLevel;
        }
        int _RegionLevel;
        if (int.TryParse(RegionLevel, out _RegionLevel))
            request.RegionLevel = _RegionLevel;
        /*
        if (!String.IsNullOrEmpty(RegionLevel))
            request.RegionLevel = Convert.ToInt32(RegionLevel);
        */
        if (!String.IsNullOrEmpty(RegionCode))
            request.RegionCode = RegionCode;
        if (string.IsNullOrEmpty(Channel))
            Channel = string.Empty;
        request.Keyword = Keyword;

        switch (Channel.ToLower())
        {
            case "call":
                request.Channel = WebReferenceCustomer.ChannelCode.Call;

                break;

            case "email":
                request.Channel = WebReferenceCustomer.ChannelCode.Email;
                break;

            case "fax":
                request.Channel = WebReferenceCustomer.ChannelCode.Fax;
                break;

            case "sms":
                request.Channel = WebReferenceCustomer.ChannelCode.SMS;
                break;
            default:
                /*
                eFlavour _ef;
                if (!Enum.TryParse(ControlName, out _ef))
                    _ef = eFlavour.Flavor_General;
                dbug_log.SliderLog(new SliderData()
                {
                    SiteId = SiteId,
                    OriginId = new Guid(OriginId),
                    Keyword = Keyword,
                    ExpertiseCode = HeadingCode,
                    ClientDomain = Domain,
                    Url = Url,
                    RegionCode = RegionCode,
                    PageName = PageName,
                    PlaceInWebSite = PlaceInWebSite,
                    ControlName = _ef
                }, context, "exposure: Missing Channel");
                 * */
                request.Channel = null;
                break;



        }

        switch (ExposureType.ToLower())
        {
            case "listing":
                request.Type = WebReferenceCustomer.ExposureType.Listing;
                break;

            case "widget":
                request.Type = WebReferenceCustomer.ExposureType.Widget;
                break;

            default:
                eFlavour _ef;
                if (!Enum.TryParse(ControlName, out _ef))
                    _ef = eFlavour.Flavor_General;
                dbug_log.SliderLog(new SliderData()
                {
                    SiteId = SiteId,
                    OriginId = new Guid(OriginId),
                    Keyword = Keyword,
                    ExpertiseCode = HeadingCode,
                    ClientDomain = Domain,
                    Url = Url,
                    RegionCode = RegionCode,
                    PageName = PageName,
                    PlaceInWebSite = PlaceInWebSite,
                    ControlName = _ef
                }, HttpContext.Current, eSliderLog.Missing_Exposure_Type);
                break;
            //return "Failure: wrong ExposureType value";

        }
        request.UserId = UserId;
        bool IsWelcomeScreenEvent = (ExposureEvent != eWelcomeExposureEvent.none);
        request.IsWelcomeScreenEvent = IsWelcomeScreenEvent;
        if (IsWelcomeScreenEvent)
        {
            switch (ExposureEvent)
            {
                case(eWelcomeExposureEvent.Disable):
                    request.WelcomeScreenEventType = "no";
                    break;
                case(eWelcomeExposureEvent.Enable):
                    request.WelcomeScreenEventType = "yes";
                    break;
                case(eWelcomeExposureEvent.Exposure):
                    request.WelcomeScreenEventType = "opened";
                    break;
                case(eWelcomeExposureEvent.xEnable):
                    request.WelcomeScreenEventType = "closeEnable";
                    break;
            }
        }

        request.SiteTitle = title;
        request.PageName = PageName;
        request.PlaceInWebSite = PlaceInWebSite;
        request.ControlName = ControlName;
        request.SessionId = SessionId;
        
        WebReferenceCustomer.Result result = new WebReferenceCustomer.Result();
        /*
        StringBuilder sbExposure = new StringBuilder();
        sbExposure.Append("SiteId:" + SiteId);
        sbExposure.Append(" OriginId:" + OriginId);
        sbExposure.Append(" Domain:" + Domain);
        sbExposure.Append(" Url:" + Url);
        sbExposure.Append(" HeadingCode:" + HeadingCode);
        sbExposure.Append(" HeadingLevel:" + HeadingLevel);
        sbExposure.Append(" RegionCode:" + RegionCode);
        sbExposure.Append(" RegionLevel:" + RegionLevel);
        sbExposure.Append(" Keyword:" + Keyword);
        */
        try
        {
            result = customer.CreateExposure(request);

            if (result.Type == WebReferenceCustomer.eResultType.Success)
            {
                return "Success";
            }

            else
            {
                eFlavour _ef;
                if (!Enum.TryParse(ControlName, out _ef))
                    _ef = eFlavour.Flavor_General;
                //          dbug_log.ExceptionGlobalLog(new System.ArgumentException("exposure", sbExposure.ToString()));
                dbug_log.SliderLog(new SliderData()
                {
                    SiteId = SiteId,
                    OriginId = new Guid(OriginId),
                    Keyword = Keyword,
                    ExpertiseCode = HeadingCode,
                    ClientDomain = Domain,
                    Url = Url,
                    RegionCode = RegionCode,
                    PageName = PageName,
                    PlaceInWebSite = PlaceInWebSite,
                    ControlName = _ef
                }, HttpContext.Current, eSliderLog.CRM_Error, result.Messages[0]);
                return "Failure: " + result.Messages[0];
            }
        }

        catch (Exception ex)
        {
            eFlavour _ef;
            if (!Enum.TryParse(ControlName, out _ef))
                _ef = eFlavour.Flavor_General;
            //            dbug_log.ExceptionGlobalLog(new System.ArgumentException("exposure " , sbExposure.ToString() + ex.Message));
            dbug_log.SliderLog(new SliderData()
            {
                SiteId = SiteId,
                OriginId = new Guid(OriginId),
                Keyword = Keyword,
                ExpertiseCode = HeadingCode,
                ClientDomain = Domain,
                Url = Url,
                RegionCode = RegionCode,
                PageName = PageName,
                PlaceInWebSite = PlaceInWebSite,
                ControlName = _ef
            }, HttpContext.Current, ex);
            return "Failure: " + ex.Message;
        }

    }
    /*
    public static string Exposure(string SiteId, string OriginId, string Domain,
        string Url, string HeadingCode, int HeadingLevel, string RegionCode,
        string RegionLevel, string Channel, string ExposureType,
        string PageName, string PlaceInWebSite, string ControlName,
        string SessionId, string Keyword, string ToolbarId, string Browser, string BrowserVersion
        )
    {
        return Exposure(SiteId, OriginId, Domain, Url, HeadingCode, HeadingLevel, RegionCode,
                    RegionLevel, Channel, ExposureType, PageName, PlaceInWebSite, ControlName, SessionId, Keyword, ToolbarId, 
                    null, null, Browser, BrowserVersion, eWelcomeExposureEvent.none, HttpContext.Current);
       
    }
    */
   /*
    public static string GetZipcodeOnRequest(HttpRequest request, string ZipCode)
    {
        return GetZipcodeOnRequest(Utilities.GetIP(request), ZipCode);
    }
    public static string GetZipcodeOnRequest(string IP, string ZipCode)
    {
        Regex reg_zipcode = new Regex(ZIPCODE_REGULARE_EXPRESSION);
        if (!string.IsNullOrEmpty(ZipCode) && reg_zipcode.IsMatch(ZipCode))
            return ZipCode;
        return GetZipcodeByIP(IP);
    }
    * */
    public static void InsertServiceRequest(string ExposureId, Guid RequestId, string status, bool IsBroker)
    {
        string command = "EXEC dbo.InsertServiceRequest @ExposureId, @RequestId, @Status, @IsBroker";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            if(ExposureId == null)
                cmd.Parameters.AddWithValue("@ExposureId", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@ExposureId", ExposureId);
            if (RequestId == Guid.Empty)
                cmd.Parameters.AddWithValue("@RequestId", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@RequestId", RequestId);
            cmd.Parameters.AddWithValue("@Status", status);
            cmd.Parameters.AddWithValue("@IsBroker", IsBroker);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
    public static RequestProcess GetServiceRequest(Guid ID)
    {
        RequestProcess rp = null;
        string command = "EXEC dbo.GetServiceRequest @ID";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                Guid RequestId = (reader["RequestId"] == DBNull.Value) ? Guid.Empty : (Guid)reader["RequestId"];
                rp = new RequestProcess(ID, RequestId, (string)reader["Status"], (bool)reader["IsBroker"]);
            }
            conn.Close();
        }
        return rp;
    }
    public static string GetEtagDateString(DateTime dt)
    {
        string date = string.Format("{0:MMddyyyy}", dt);
        return date;
    }
    public static bool IsInBlackDomain(string domain)
    {
     //   List<string> list = new List<string>();
        if (string.IsNullOrEmpty(domain))
            return false;
        domain = domain.ToLower();
        string domains = "";
        int start = -1;
        while ((start = domain.IndexOf('.')) > -1)
        {
           //list.Add(domain);
            domains += domain + ";";
            domain = domain.Substring(start + 1);
        }
        
        bool result = false;
        string command = "SELECT [dbo].[IsInBlackDomain](@Domains)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                conn.Open();
                cmd.Parameters.Add("@Domains", domains);
                result = (bool)cmd.ExecuteScalar();
                conn.Close();
            }
        }
        return result;
    }
    public static bool IsFromToday(string _etag, string ModifiedSince, HttpRequest request)
    {
        if (string.IsNullOrEmpty(_etag))
            return false;
        _etag = _etag.Replace("\"", "");
        string[] tags = _etag.Split(':');
        if (tags.Length != 2)
        {
          //  dbug_log.ExceptionLog(new ExceptionEtag(_etag, ModifiedSince, "Split", request.QueryString.ToString()));
            return false;
        }

        DateTime etagDate;
        if (!DateTime.TryParseExact(tags[1], "MMddyyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out etagDate))
        {
            //dbug_log.ExceptionLog(new ExceptionEtag(_etag, ModifiedSince, "TryParseExact", request.QueryString.ToString()));
            return false;
        }
        if (etagDate == DateTime.Today)
            return true;
        return false;
    }
    private static SliderViewData GetIntextSliderViewData(SliderData sd)
    {
        PpcSite _cache = PpcSite.GetCurrent();
        //if(!_cache.IntextFlavor.ContainsKey)
        return _cache.Flavors[_cache.IntextFlavor[sd.ExpertiseCode]];
    }
    public static SliderViewData GetSliderViewData(SliderData sd)
    {
        
        if (sd.SliderType == eSliderType.Intext)
            return GetIntextSliderViewData(sd);
        PpcSite _cache = PpcSite.GetCurrent();
        int _index;  
       

        if (!string.IsNullOrEmpty(sd.ExpertiseCode))
        {
            HeadingCampain hc = _cache.GetHeadingCampain(sd);
            HeadingCode _hc = _cache.headings[sd.ExpertiseCode];
            eHeadingGroup ehg = _cache.GetHeadingGroup(sd.ExpertiseCode);      
            if (hc != null)
            {
                if (hc.ShowBanner || !_cache.IsGoodLocationRegulareHeading(sd.Country, sd.ExpertiseCode))
                {
                    /* Enable banner campaign in https 24/02/2015
                    if (sd.IsHttpsScheme())
                        return null;
                     * */
                    if(ehg == eHeadingGroup.hotel && sd.Country != PpcSite.UNITED_STATES)
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Hotel_Agoda];
                    
                    SliderViewData _svd = _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Cj];
                    return _svd.ToCJCampain(hc);
                }
            }
            
            if (sd.ExpertiseCode == "2065")//dentist category
            {
                /*
                if (sd.Country == "UNITED KINGDOM")
                    return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_2];   
                 */
                return _cache.Flavors[eFlavour.Phone_Did_Top_Dentist_1_800_Company];
            }
             
            if (_hc.DidPhone != null && _hc.DidPhone.IsWorkingTimeAndGoodOrigin(sd))
            {
                
                sd.IsDidPhoneAvailable = true;
                if (!_hc.DidPhone.IsFormAndDid)
                {
                    /*
                    //_hc.DidPhone.Title
                    if (sd.ExpertiseCode == "2065")//dentist category
                    {
                       
                      //if(_hc.IsSupplierAvailable())
                            return _cache.Flavors[eFlavour.Phone_Did_Top_Dentist_A];
                    }
                    */
                   /*
                    else if (sd.ExpertiseCode == "2298")//payday category
                    {
                        
                        return (_hc.IsSupplierAvailable()) ? _cache.Flavors[eFlavour.Phone_Did_Top_RoundSky_Company] :                        
                           _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_PayDay_RoundSky];
                    }
                   */

                    return _cache.Flavors[eFlavour.Phone_Did_Top];
                }
            }          

                     

           
            // test expertise call for health insurance - 2268 , , credit counselor 2297 //// vehicle insurance - 2267(remove 27/04/2014 --> AutoInsurance group)
            if (sd.ExpertiseCode == "2297")//sd.ExpertiseCode == "2268" ||
            {
                 
               if(sd.SliderType == eSliderType.GoogleFrame)
                   return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_JustPhoneWithForm];
               
               if (!sd.IsHttpsScheme())
                   return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_CreditCounsellors_Offerweb];
                
               return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_JustPhoneWithForm];
                /*
                Random randomGeneral = new Random();
                int _indexGeneral = randomGeneral.Next(2);
                switch (_indexGeneral)
                {
                    case (0):
                        return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_JustPhoneWithForm]; 
                    case (1):
                        return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_JustPhoneWithForm_Disabled];                                  
                        
                }
                */
                 
            }
            //health insurance - 2268 
            if (sd.ExpertiseCode == "2268" )
            {

                if(sd.SliderType == eSliderType.XSaver)
                    return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_HealthInsurance_steps];
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_HealthInsurance_steps2];
                              
            }
            
            /*
            if (sd.ExpertiseCode == "2293")//Mortgage broker
            {
                if (sd.IsHttpsScheme())
                    return null;
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Mortgage_Bigcatmedia];
            }
             * */
            //quotebound new cars - 4000
            if (sd.ExpertiseCode == "4000")
            {
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_NewCar_Quotebound];
            }

            if (ehg == eHeadingGroup.RecruitingAdvertisers)
            {

                if (sd.SliderType == eSliderType.PopupRevizer)
                {
                    Random _randomRecruitingPop = new Random();
                    int _indexAdv = _randomRecruitingPop.Next(6);
                    switch (_indexAdv)
                    {
                        case (0):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Pop_dragged];
                        case (1):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Pop_recycling];
                        case (2):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Pop_ringing];
                        case (3):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Pop_clicks];
                        case (4):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Pop_dress];
                        case (5):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Pop_search];
                    }

                   

                }

                else
                {
                    Random _random = new Random();
                    int _indexAdv = _random.Next(6);
                    switch (_indexAdv)
                    {
                        case (0):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_clicks_dont];
                        case (1):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_dragged_down];
                        case (2):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_phone_ringing];
                        case (3):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_yellow_plane];
                        case (4):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_recyvling_bin];
                        case (5):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_google_search];

                        /*
                        case (0):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_operator_looking_for];
                        case (1):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_Operator_advertise_Here_get_call];
                        */
                        /*
                        case (2):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_Waiting_by_the_phone];
                        //New
                        case (3):
                        
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_plumber_customers_will];
                        case (4):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_Operator_customers_will];
                        case (5):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_Waiting_get_calls_now];
                        case (6):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_Plumber_advertise_with_us];
                        */
                        /*
                        case (1):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_operator_get_calls];
                        case (2):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_plumber_appear_here];
                        case (4):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_plumber_looking_for];
                        case (6):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Recruiting_Advertisers_operator_advertise_here];
                        * */
                    }
                }
            }

            // Attorney
            if (ehg == eHeadingGroup.attorney)
            {
                if (sd.ExpertiseCode == "2020")//undefined category//
                {
                    Random randomAttorney = new Random();
                    int _indexAttorney = randomAttorney.Next(2);
                    switch (_indexAttorney)
                    {
                        
                        case (0):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Attorney_AllExpertises_2_Till_3_Steps];
                        case (1):
                            return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Attorney_AllExpertises_2_Till_3_Steps_flickering];
                        /*
                        case (0):
                            return _cache.Flavors[eFlavour.Flavor_Attorney_2_Steps_withEmail_AutoCompleteKeywords];
                            //return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Attorney_AllExpertises];
                        case (1):
                            return _cache.Flavors[eFlavour.Flavor_Attorney_2_Steps_withEmail_AutoCompleteKeywords_flickering];
                         */
                    }                   
                }
                 
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Attorney];
            }
            if (ehg == eHeadingGroup.Questions)
                return _cache.Flavors[eFlavour.Flavor_Top_Questions];
            if (ehg == eHeadingGroup.mover)
            {
                
                Random randomMover = new Random();
                int _indexMover = randomMover.Next(2);
                switch (_indexMover)
                {

                    case (0):
                        return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Mover];
                    case (1):
                        return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Mover_flickering];
                }   

                
            }

            if (ehg == eHeadingGroup.personalInjuryAttorney)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_PersonalInjuryAttorney];
           
            if (ehg == eHeadingGroup.accountant)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Accountant];

            if (ehg == eHeadingGroup.electrician)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Electrician];

            
            if (ehg == eHeadingGroup.computerRepair)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_ComputerRepair];
            
            if (ehg == eHeadingGroup.storage)
            { 
              ////  return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Storage_Sparefoot];
         //       return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_StorageProvider_BuyerZone];

                if(sd.SliderType == eSliderType.Popup)
                    return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_StorageProviderOneStep];
                else
                    return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_StorageProvider];
            }

            if (ehg == eHeadingGroup.handyman)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_HandyMan2];

            if (ehg == eHeadingGroup.roofer)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Roofer];

            if (ehg == eHeadingGroup.payday)
            {
                if (sd.Country == "UNITED KINGDOM")
         //           return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Payday_LeadsMarket_UK];
                    return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Payday_Affiliaxe];
                /*
                Random randomPayday = new Random();
                int _indexPayday = randomPayday.Next(2);
                */
               // return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Payday_LeadsMarket];
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_PayDay_RoundSky3];

                /* 
                 switch (_indexPayday)
                 {
                     case (0):
                         return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Payday_LeadsMarket];
                     case (1):
                         return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_PayDay_RoundSky];
                 }               
                  */        
               
                 /*
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_PayDay];
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_PayDay_flickering];
                //return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_PayDay_3_Steps];
                  * */
            }
            if (ehg == eHeadingGroup.BinaryOptions)
            {
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Binary_Options_Sirwilliambot];
                /*
                switch(sd.Country)
                {
                    case (CountryNames.AUSTRALIA):
                    case (CountryNames.CANADA):
                    case (CountryNames.IRELAND):
                    case (CountryNames.SOUTH_AFRICA):
                    case (CountryNames.UNITED_KINGDOM):
                    case (CountryNames.UNITED_STATES):
                //    case (CountryNames.ISRAEL):
                        return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Binary_Options_Sirwilliambot];

                }
                    /*
                if (sd.Country == PpcSite.UNITED_STATES)// || sd.IsHttpsScheme())
                    return null;
                     * * /
                //     return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Binary_Options];
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Binary_Options_TigerOptions];
                 * */
            }

            if (ehg == eHeadingGroup.creditCounselor)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_CreditCounselor];

            if (ehg == eHeadingGroup.dentist)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Dentist];

            if (ehg == eHeadingGroup.cleaner)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Cleaner];

            if (ehg == eHeadingGroup.hotel)
            {
                //return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Hotel_Booking];
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Hotel_Agoda];                
            }
                
            /*
            if (ehg == eHeadingGroup.flight)
            {
                if (sd.IsHttpsScheme())
                    return null;
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Flight_Expedia];
            }
             * */
            /*
            if (ehg == eHeadingGroup.cruise)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Cruise_Expedia];
            */
            if (ehg == eHeadingGroup.package)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Package_Expedia];

            if (ehg == eHeadingGroup.activity)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Activity_Expedia];

            if (ehg == eHeadingGroup.food)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Food_Artizone];

            if (ehg == eHeadingGroup.homeImprovement)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_HomeImprovement];
          //      return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_HomeImprovement_Steps];

            if (ehg == eHeadingGroup.locksmith)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_Locksmith];

            /*
            if (ehg == eHeadingGroup.Immigration)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_GreenCard];
             * */
            if (ehg == eHeadingGroup.criminalAttorney)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_CriminalAttorney];
            if (ehg == eHeadingGroup.duiAttorney)
            {
                /*
                Random rnd = new Random();
                _index = rnd.Next(2);               
                if(_index == 0)
                    return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_DuiAttorney];                
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_DUI_Offerweb];
                */
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_DuiAttorney];        
            }
            if (ehg == eHeadingGroup.SsdAttorney)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_SsdAttorney];
            if (ehg == eHeadingGroup.divorceAttorney)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_DivorceAttorney];
            if (ehg == eHeadingGroup.bankruptcyAttorney)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_BankraptcyAttorney];
            if (ehg == eHeadingGroup.LifeInsurance)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_LifeInsurance];
            if (ehg == eHeadingGroup.VehicleLoan)
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_AutoLoanRndFrame];

            if (ehg == eHeadingGroup.HomeSecurity)// && !sd.IsHttpsScheme())//Home Security
            {
                /*
                Random rnd = new Random();
                _index = rnd.Next(2);
                if (_index == 0)
                       return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_HomeSecurity_Datalot];
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_HomeSecurity_BuyerZone];
                 * */
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_HomeSecurity_Datalot];
            }
           
            if (ehg == eHeadingGroup.AutoInsurance)
            {
                /*
               if (sd.IsHttpsScheme() || sd.OriginId == _cache.RevizerOriginId || sd.SliderType == eSliderType.Popup)
                       return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_JustPhoneWithForm];
               
                   Random rnd = new Random();
                   _index = rnd.Next(2);
                   if (_index == 0)
                       return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_VehicleInsurance_Leadnomics];
                * */
                    return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_JustPhoneWithForm];
                    //return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_AutoInsurance]; 
                
            }
            if (sd.ExpertiseCode == "2145" || sd.ExpertiseCode == "2128")//2145 Occupational Therapist //2128 Massage Therapist
               return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_2];

            if (sd.AppType != eAppType.DeskApp && !string.IsNullOrEmpty(sd.ClientDomain))
            {
    //            string _client_domain = (string.IsNullOrEmpty(sd.ClientDomain)) ? string.Empty : sd.ClientDomain;
                if (sd.ClientDomain.Contains(".yahoo.com"))
                    return _cache.Flavors[eFlavour.Flavor_Regular_Yahoo];
                if (sd.ClientDomain.Contains(".yellowpages.com"))
                    return _cache.Flavors[eFlavour.Flavor_Regular_Yellowpages];
                if (sd.ClientDomain.Contains(".bing.com"))
                    return _cache.Flavors[eFlavour.Flavor_Regular_Bing];
                if (sd.ClientDomain.Contains(".yellowbook.com"))
                    return _cache.Flavors[eFlavour.Flavor_Regular_Yellowbook];
            }


            if (_hc.DidPhone!=null && _hc.DidPhone.IsFormAndDid)
                 return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_2_FormDid];
            else
                return _cache.Flavors[eFlavour.Flavor_Half_Controls_Top_2_email];

         
        }
        // There is no keyword
        return _cache.Flavors[eFlavour.Flavor_General_Search];
    }
    public static string GetCleanKeyword(string keyword)
    {
        if (string.IsNullOrEmpty(keyword))
            return string.Empty;
        string NewKeyword = "";
        keyword = keyword.ToLower();
        for (int i = 0; i < keyword.Length; i++)
        {
            if (keyword[i] > 96 && keyword[i] < 123)
                NewKeyword += keyword[i];
            else
                if (NewKeyword.Length > 0 && NewKeyword[NewKeyword.Length - 1] != ' ' && keyword.Length > (i + 1))
                    NewKeyword += " ";
        }
        return NewKeyword;
    }
    public static string Get_NP_SRC()
    {
  //      const string PORT = "port:";
        string _domain = "http://" + DBConnection.GetDomainBySiteId(PpcSite.GetCurrent().SiteId);
        string path = @"npsb/_np.js";
        PpcSite _cache = PpcSite.GetCurrent();
     //   bool IsLocal = HttpRuntime.AppDomainAppVirtualPath != "/";
        string PathDestination;
        string _AppDomainAppVirtualPath = HttpRuntime.AppDomainAppVirtualPath.EndsWith("/") ? HttpRuntime.AppDomainAppVirtualPath : HttpRuntime.AppDomainAppVirtualPath + "/";       
        if (_cache.IsLocalMachine)
        {
            /*
            string _port = Environment.CommandLine;
            int _index = _port.IndexOf(PORT);
            if (_index < 0)
                _port = "80";
            else
            {
                string BuilderPort = "";
                for (int i = _index + PORT.Length; i < _port.Length; i++)
                {
                    if ("1234567890".Contains(_port[i]))
                        BuilderPort += _port[i];
                    else
                        break;
                }
                _port = BuilderPort;
            }
             * */
            PathDestination = _domain + ":" + _cache.Port + _AppDomainAppVirtualPath + path;
        }
        else
            PathDestination = _domain + _AppDomainAppVirtualPath + path;
        return PathDestination;
    }
    /*
    public static void WebExposure(Guid ID, SliderData sd, eExposureType ExposureType, bool IsAuthentic,
       eWelcomeExposureEvent ExposureEvent)
    {
        string command = "EXEC  [dbo].[CreateSliderExposure] @Id, @SiteNameId, @OriginId, @Domain, @Url, " +
            "@HeadingCode, @ExposureType, @Channel, @PageName, @ControlName, @SessionId, @Keyword, @ToolbarId, " +
            "@Title, @IP, @IsWelcomeScreenEvent, @RegionCode, @PlaceInWebSite, @Browser, @BrowserVersion, @IsAuthentic, @Scheme, @Country, "+
            "@TimeZone, @IsForReports";

        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@Id", ID);
            cmd.Parameters.AddWithValue("@SiteNameId", SetNonNull(sd.SiteId));
            cmd.Parameters.AddWithValue("@OriginId", sd.OriginId);
            cmd.Parameters.AddWithValue("@Domain", SetNonNull(sd.ClientDomain));
            cmd.Parameters.AddWithValue("@Url", SetNonNull(sd.Url));
            cmd.Parameters.AddWithValue("@HeadingCode", SetNonNull(sd.ExpertiseCode));
            cmd.Parameters.AddWithValue("@ExposureType", ExposureType.ToString());
            cmd.Parameters.AddWithValue("@Channel", string.Empty);
            cmd.Parameters.AddWithValue("@PageName", SetNonNull(sd.PageName));
            cmd.Parameters.AddWithValue("@ControlName", SetNonNull(sd.ControlName.ToString()));
            cmd.Parameters.AddWithValue("@SessionId", string.Empty);
            cmd.Parameters.AddWithValue("@Keyword", SetNonNull(sd.Keyword));
            cmd.Parameters.AddWithValue("@ToolbarId", SetNonNull(sd.ToolbarId));
            cmd.Parameters.AddWithValue("@Title", SetNonNull(sd.title));
            cmd.Parameters.AddWithValue("@IP", SetNonNull(sd.IP));
            cmd.Parameters.AddWithValue("@IsWelcomeScreenEvent", (ExposureEvent != eWelcomeExposureEvent.none));
            cmd.Parameters.AddWithValue("@RegionCode", SetNonNull(sd.RegionCode));
            cmd.Parameters.AddWithValue("@PlaceInWebSite", SetNonNull(sd.PlaceInWebSite));
            cmd.Parameters.AddWithValue("@Browser", SetNonNull(sd.Browser));
            cmd.Parameters.AddWithValue("@BrowserVersion", SetNonNull(sd.BrowserVersion));
            cmd.Parameters.AddWithValue("@IsAuthentic", IsAuthentic);
            cmd.Parameters.AddWithValue("@Scheme", sd.ClientScheme);
            cmd.Parameters.AddWithValue("@Country", SetNonNull(sd.Country));
            cmd.Parameters.AddWithValue("@TimeZone", sd.TimeZone);
            cmd.Parameters.AddWithValue("@IsForReports", ListFlavorBlockCRM.BinarySearch(sd.ControlName) < 0);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
     * */
    public static string SetNonNull(string s)
    {
        return (s == null) ? string.Empty : s;
    }
    /*
    public static void UpdateSliderExposureExpertiseCode(Guid ExposureId, string ExpertiseCode)
    {
        if (DataExposure.UpdateHeadingExposure(ExposureId, ExpertiseCode))
            return;
        string command = "EXEC [dbo].[UpdateSliderExposureExpertiseCode] @ID, @ExperriseCode";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@ID", ExposureId);
                cmd.Parameters.AddWithValue("@ExperriseCode", ExpertiseCode);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
        }
    }
    */
     
    public static void AddToCounter(int num)
    {
        string command = "EXEC [dbo].[AddToCounter] @num";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@num", num);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
    public static bool IsIpFromUSA_ISRAEL(string ip)
    {
        IP2Location.IPResult oIPResult = new IP2Location.IPResult();
        IP2Location.Component oIP2Location = new IP2Location.Component();
        
        //Set Database Path For IPv4 BIN file and IPv6 BIN file (only if you need to query IPv6 IP addresses)
        oIP2Location.IPDatabasePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE.BIN";
        //oIP2Location.IPDatabasePathIPv6 = @"C:\Program Files\IP2Location\IPV6-COUNTRY.BIN";
        //Set License Path
        oIP2Location.IPLicensePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\License.key";
        if (!string.IsNullOrEmpty(ip))
        {
            try
            {
                oIPResult = oIP2Location.IPQuery(ip);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return false;
            }
            switch (oIPResult.Status.ToString())
            {
                case ("OK"):
                    switch (oIPResult.CountryLong)
                    {
                        case ("UNITED STATES"):
                        case ("ISRAEL"):
                            return true;
                        default:
                            return false;
                    }
                case ("EMPTY_IP_ADDRESS"):
                case ("INVALID_IP_ADDRESS"):
                case ("MISSING_FILE"):
                    return false;
            }

        }
        return false;
    }
    public static string GetCountryName(string ip, string HostName)
    {
        if (HostName == "localhost")
            return "ISRAEL";
        IP2Location.IPResult oIPResult = new IP2Location.IPResult();
        IP2Location.Component oIP2Location = new IP2Location.Component();

        //Set Database Path For IPv4 BIN file and IPv6 BIN file (only if you need to query IPv6 IP addresses)
        oIP2Location.IPDatabasePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE.BIN";
        //oIP2Location.IPDatabasePathIPv6 = @"C:\Program Files\IP2Location\IPV6-COUNTRY.BIN";
        //Set License Path
        oIP2Location.IPLicensePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\License.key";
        if (!string.IsNullOrEmpty(ip))
        {
            try
            {
                oIPResult = oIP2Location.IPQuery(ip);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return string.Empty;
            }
            switch (oIPResult.Status.ToString())
            {
                case ("OK"):
                    return oIPResult.CountryLong.ToUpper();                    
                case ("EMPTY_IP_ADDRESS"):
                case ("INVALID_IP_ADDRESS"):
                case ("MISSING_FILE"):
                    dbug_log.SliderLog(null, null, eSliderLog.IP_Error, "IP: " + ip + "\r\noIPResult: " + oIPResult.CountryLong);
                    return string.Empty;
            }

        }
        return string.Empty;
    }
    //Sterkly-Google override country in case I got 'UNITED KINGDOM' override to 'UNITED STATES'
    public static string GetCountryName(string ip, string HostName, eSliderType Slider_Type, out string ZipCode)
    {
        string _country = GetCountryName(ip, HostName, out ZipCode);
        if (_country == "UNITED KINGDOM" && Slider_Type == eSliderType.GoogleFrame)
        {
            _country = "UNITED STATES";
            ZipCode = string.Empty;
        }
        return _country;
    }
    public static string GetCountryName(string ip, string HostName, out string ZipCode)
    {
        ZipCode = string.Empty;
        if (HostName == "localhost")
            return "ISRAEL";
        IP2Location.IPResult oIPResult = new IP2Location.IPResult();
        IP2Location.Component oIP2Location = new IP2Location.Component();

        //Set Database Path For IPv4 BIN file and IPv6 BIN file (only if you need to query IPv6 IP addresses)
        oIP2Location.IPDatabasePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE.BIN";
        //oIP2Location.IPDatabasePathIPv6 = @"C:\Program Files\IP2Location\IPV6-COUNTRY.BIN";
        //Set License Path
        oIP2Location.IPLicensePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\License.key";
        if (!string.IsNullOrEmpty(ip))
        {
            try
            {
                oIPResult = oIP2Location.IPQuery(ip);               
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return string.Empty;
            }
            switch (oIPResult.Status.ToString())
            {
                case ("OK"):                   
                    ZipCode = oIPResult.ZipCode;
                    return oIPResult.CountryLong.ToUpper();
                case ("EMPTY_IP_ADDRESS"):
                case ("INVALID_IP_ADDRESS"):
                case ("MISSING_FILE"):
                    dbug_log.SliderLog(null, null, eSliderLog.IP_Error, "IP: " + ip + "\r\noIPResult: " + oIPResult.CountryLong);
                    return string.Empty;
            }

        }
        return string.Empty;
    }
    public static GeoLocation GetGeoLocation(string ip)
    {        
        IP2Location.IPResult oIPResult = new IP2Location.IPResult();
        IP2Location.Component oIP2Location = new IP2Location.Component();

        //Set Database Path For IPv4 BIN file and IPv6 BIN file (only if you need to query IPv6 IP addresses)
        oIP2Location.IPDatabasePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE.BIN";
        //oIP2Location.IPDatabasePathIPv6 = @"C:\Program Files\IP2Location\IPV6-COUNTRY.BIN";
        //Set License Path
        oIP2Location.IPLicensePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\License.key";
        
        if (!string.IsNullOrEmpty(ip))
        {
            try
            {
                oIPResult = oIP2Location.IPQuery(ip);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return null;
            }
            switch (oIPResult.Status.ToString())
            {
                case ("OK"):
                    return new GeoLocation(oIPResult.CountryLong, oIPResult.Region, oIPResult.City, oIPResult.ZipCode);
                case ("EMPTY_IP_ADDRESS"):
                case ("INVALID_IP_ADDRESS"):
                case ("MISSING_FILE"):
                    dbug_log.SliderLog(null, HttpContext.Current, eSliderLog.IP_Error, "IP: " + ip + "\r\noIPResult: " + oIPResult.CountryLong);
                    return null;
            }

        }
        return null;
    }
    
    public static bool IsValidZipCode(string ZipCode, string _country)
    {
        if (string.IsNullOrEmpty(ZipCode))
            return false;
        PpcSite _ppc = PpcSite.GetCurrent();
        Regex reg_zipcode = new Regex(_ppc.GetZipCodeRegulareExpression(_country));
        return reg_zipcode.IsMatch(ZipCode);
    }
    /*
    public static string GetZipcodeByIP(string _ip, SliderData _sd)
    {
        Regex reg_zipcode = new Regex(ZIPCODE_REGULARE_EXPRESSION);
        if (!string.IsNullOrEmpty(_sd.RegionCode) && reg_zipcode.IsMatch(_sd.RegionCode))
            return _sd.RegionCode;
        return GetZipcodeByIP(_ip);
    }
     * */
    public static string GetZipcode(string _ip, SliderData _sd)
    {
        return GetZipcode(_ip, _sd, null);
    }
    public static string GetZipcode(string _ip, SliderData _sd, string _zipcode)
    {
        PpcSite _ppc = PpcSite.GetCurrent();
        Regex reg_zipcode = new Regex(_ppc.GetZipCodeRegulareExpression(_sd.Country));
        if (!string.IsNullOrEmpty(_zipcode) && reg_zipcode.IsMatch(_zipcode))
            return _zipcode;
        if (!string.IsNullOrEmpty(_sd.RegionCode) && reg_zipcode.IsMatch(_sd.RegionCode))
            return _sd.RegionCode;
        return GetZipcodeByIP(_ip);
    }
    public static string GetZipcodeByIP(string _ip)
    {
        /*
        string sURL = @"http://api.ipinfodb.com/v3/ip-city/?key=8b541a4c43e8944c3d7a3883e506649d7561715f0c888f0da95232227054995f&ip=" + _ip;// +context.Request.ServerVariables["REMOTE_ADDR"];
       */
        IP2Location.IPResult oIPResult = new IP2Location.IPResult();
        IP2Location.Component oIP2Location = new IP2Location.Component();

        //Set Database Path For IPv4 BIN file and IPv6 BIN file (only if you need to query IPv6 IP addresses)
        oIP2Location.IPDatabasePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE.BIN";
        //oIP2Location.IPDatabasePathIPv6 = @"C:\Program Files\IP2Location\IPV6-COUNTRY.BIN";
        //Set License Path
        oIP2Location.IPLicensePath = System.AppDomain.CurrentDomain.BaseDirectory + @"IPData\License.key";

        if (!string.IsNullOrEmpty(_ip))
        {
            try
            {
                oIPResult = oIP2Location.IPQuery(_ip);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return string.Empty;
            }
            switch (oIPResult.Status.ToString())
            {
                case ("OK"):
                    return oIPResult.ZipCode;
                case ("EMPTY_IP_ADDRESS"):
                case ("INVALID_IP_ADDRESS"):
                case ("MISSING_FILE"):
                    dbug_log.SliderLog(null, HttpContext.Current, eSliderLog.IP_Error, "IP: " + _ip + "\r\noIPResult: " + oIPResult.CountryLong);
                    return string.Empty;
            }

        }
        return string.Empty;

    }
    public static int CompareJavascriptString(string x, string y)
    {
        for (int i = 0; i < x.Length && i < y.Length; i++)
        {
            var num = x[i] - y[i];
            if (num != 0)
            {
                return num;
            }
        }
        if (x.Length > y.Length)
            return 1;
        else if (x.Length < y.Length)
            return -1;
        return 0;                
    }

    public static bool ClickRequest(MongoDB.Bson.ObjectId ExposureId)
    {
        return ClickRequest(ExposureId, null);
    }
    public static bool ClickRequest(MongoDB.Bson.ObjectId ExposureId, string description)
    {
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            string command = "EXEC [dbo].[InsertClick_Request] @ExposureId, @description";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@ExposureId", ExposureId.ToString());
                if (string.IsNullOrWhiteSpace(description))
                    cmd.Parameters.AddWithValue("@description", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@description", description);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return false;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                    conn.Close();
            }
        }
        return true;
    }

    public static bool ClickClaim(string company, string name, string email, string phone, string source, string version, string guid)
    {
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            string command = "EXEC dbo.InsertClaimRequest @company,@name,@email,@phone,@source,@version,@guid";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@company", company);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@phone", phone);
                cmd.Parameters.AddWithValue("@source", source);
                cmd.Parameters.AddWithValue("@version", version);
                cmd.Parameters.AddWithValue("@guid", guid);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return false;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                    conn.Close();
            }
        }
        return true;
    }

    public static bool ClickRelatedHeading(string heading, string siteId, string url, string flavor, string fromHeadingCode, string fromHeading, string exposureId, string originId, string keyword)
    {
       
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            string command = "EXEC dbo.InsertRelatedHeading @siteId,@heading,@fromHeadingCode,@fromHeading,@url,@flavor,@originId,@exposureId,@keyword";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@siteId", siteId);
                cmd.Parameters.AddWithValue("@heading", heading);
                cmd.Parameters.AddWithValue("@fromHeadingCode", fromHeadingCode);
                cmd.Parameters.AddWithValue("@fromHeading", fromHeading);
                cmd.Parameters.AddWithValue("@url", url);
                cmd.Parameters.AddWithValue("@flavor", flavor);
                cmd.Parameters.AddWithValue("@originId", originId);
                cmd.Parameters.AddWithValue("@exposureId", exposureId);                
                cmd.Parameters.AddWithValue("@keyword", keyword);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return false;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                    conn.Close();
            }
        }
        return true;
    }


    public static bool SetStepRegistration(string phone, string siteId, string step, string password, string page, string plan, string companyName, string companyContactPerson, string companyAddress, string heading, bool flagEnd)
    {
          using (SqlConnection conn = DBConnection.GetConnString())
        {
            string command = "EXEC dbo.InsertRegistration @phone,@siteId,@step,@password,@page,@plan,@companyName,@companyContactPerson,@companyAddress,@heading,@flagEnd";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@phone", phone);
                cmd.Parameters.AddWithValue("@siteId", siteId);
                cmd.Parameters.AddWithValue("@step", step);
                cmd.Parameters.AddWithValue("@password", password);                
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@plan", plan);
                cmd.Parameters.AddWithValue("@companyName", companyName);
                cmd.Parameters.AddWithValue("@companyContactPerson", companyContactPerson);                
                cmd.Parameters.AddWithValue("@companyAddress", companyAddress);
                cmd.Parameters.AddWithValue("@heading", heading);
                cmd.Parameters.AddWithValue("@flagEnd", flagEnd); 
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return false;
            }
            finally
            {
                if (conn.State != System.Data.ConnectionState.Closed)
                    conn.Close();
            }
        }
        return true;
    }
   

    public static bool SaveToFile(string destination, string source)
    {
        try
        {
            if (File.Exists(destination))
                File.Delete(destination);
            using (FileStream fs = File.Create(destination))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(source);
                fs.Write(info, 0, info.Length);
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false;
        }
        return true;
    }
    public static string FastJsReplacement(Dictionary<string, string> dic, string script)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < script.Length; i++)
        {
            if (script[i] == '{')
            {
                int _end = script.IndexOf('}', i);
                if (_end < 0)
                {
                    sb.Append(script[i]);
                    continue;
                }
                string str = script.Substring(i, _end - i + 1);
                if (dic.ContainsKey(str))
                {
                    sb.Append(dic[str]);
                    i = _end;
                }
                else
                    sb.Append(script[i]);

            }
            else
                sb.Append(script[i]);
        }
        return sb.ToString();
    }
    public static ResponseRequestCall CreateServiceRequest(string PhoneNum, int SupplierNum, string ZipCode, string description, string name, string keyword, SliderData sd, string IP)
    {
        ResponseRequestCall rrc = new ResponseRequestCall();
        
  //      SliderData sd = SliderData.GetSliderData(ExposureId);

     //   ZipCode = SalesUtility.GetZipcodeOnRequest(IP, ZipCode);
        PhoneNum = Utilities.GetCleanPhone(PhoneNum);
        PpcSite _ppc = PpcSite.GetCurrent();
        rrc.plural = _ppc.GetHeadingDetails(sd.ExpertiseCode).Title;
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, sd.SiteId);
        _customer.Timeout = 300000;
        WebReferenceCustomer.ServiceRequest _request = new WebReferenceCustomer.ServiceRequest();
        _request.ContactPhoneNumber = PhoneNum;
        _request.ControlName = sd.ControlName.ToString();
        _request.Domain = sd.Domain;
        _request.ExpertiseCode = sd.ExpertiseCode;
        _request.ExpertiseType = 1;
        _request.Keyword = keyword;
        _request.ExposureId = sd._id.ToString();
        
        _request.NumOfSuppliers = SupplierNum;
        _request.OriginId = sd.OriginId;
        _request.PageName = sd.PageName;
        _request.PlaceInWebSite = sd.PlaceInWebSite;
        _request.PrefferedCallTime = DateTime.UtcNow;
        _request.RequestDescription = _ppc.GetHeadingDetails(sd.ExpertiseCode).GetDescription(description);
        _request.RegionCode = ZipCode;
        _request.RegionLevel = 4;
        _request.ContactFullName = name;
        _request.SessionId = (HttpContext.Current != null) ? ((HttpContext.Current.Session != null) ? HttpContext.Current.Session.SessionID : string.Empty) : string.Empty;
        _request.SiteId = sd.SiteId;
        _request.ToolbarId = sd.ToolbarId;
        _request.IP =IP;
        _request.Url = sd.Url;
        _request.Browser = sd.Browser;
        _request.BrowserVersion = sd.BrowserVersion;
        WebReferenceCustomer.ServiceResponse _response = null;

        try
        {
            IAsyncResult ar = _customer.BeginCreateServiceRequestWithZipcodeUSA(_request, null, null);
            rrc.Tips = PpcSite.GetCurrent().GetTips(sd.ExpertiseCode);
            _response = _customer.EndCreateServiceRequestWithZipcodeUSA(ar);
        }
        catch (Exception exc)
        {
            dbug_log.SliderLog(_request, HttpContext.Current, exc);
            SalesUtility.InsertServiceRequest(null, Guid.Empty, "faild", false);
            //     LogRequest(_request, null);
            return rrc;
        }
        if (_response.Status == WebReferenceCustomer.StatusCode.Success)
        {
            rrc.status = "success";
            rrc.RequestId = _response.ServiceRequestId;
            SalesUtility.InsertServiceRequest(null, new Guid(_response.ServiceRequestId), "Success", _response.SentToBroker);
            /*
            LogEdit.SaveLog(sd.SiteId, "CreateServiceRequest status:" + _response.Status.ToString().ToLower() +
                "\r\nid: " + _response.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " +
                PhoneNum + "\r\nDescription: " + description +
                "\r\nServiceAreaCode: " + ZipCode +
                "\r\nExpertiseCode: " + ExpertiseCode +
                "\r\nSession: " + Session.SessionID + "\r\nip: " + Utilities.GetIP(context.Request));
             * */
        }
        else if (_response.Status == WebReferenceCustomer.StatusCode.BadParametersReceived)
        {
            rrc.RequestId = "";
            rrc.status = "BadParametersReceived";
            string message = _response.Message.ToLower();
            if (message.Contains("zip code"))
                rrc.RequestId += "zipcode;";
            /*
            if (message.Contains("phone"))
                rrc.RequestId += "phone;";
            if (message.Contains("expertise"))
                rrc.RequestId += "expertise;";
             * */
            if (string.IsNullOrEmpty(rrc.RequestId))
                rrc.status = "faild";
            Guid __id;
            if (!Guid.TryParse(_response.ServiceRequestId, out __id))
                __id = Guid.Empty;
            SalesUtility.InsertServiceRequest(null, __id, "BadParametersReceived", _response.SentToBroker);
        }
        else
        {
            rrc.status = "faild";
            Guid __id;
            if (!Guid.TryParse(_response.ServiceRequestId, out __id))
                __id = Guid.Empty;
            SalesUtility.InsertServiceRequest(null, __id, _response.Status.ToString(), _response.SentToBroker);
        }
        return rrc;
    }
    public static bool IsForReport(eFlavour ControlName)
    {
        return ListFlavorBlockCRM.BinarySearch(ControlName) < 0;
    }
    public static Dictionary<Guid, string> GetRegionsByLevel(string _WebReference, int level)
    {
        Dictionary<Guid, string> dic = new Dictionary<Guid, string>();

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_WebReference);
        
        WebReferenceSite.ResultOfListOfRegionData result = null;
        try
        {
            result = _site.GetRegionsOfLevel(level);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return null;

        foreach (WebReferenceSite.RegionData rd in result.Value)//  XmlNode node in xdd["Areas"].ChildNodes)
        {
            string name = rd.RegionName;
            Guid RegionId = rd.RegionId;
            dic.Add(RegionId, name);
        }
        return dic;
    }
    public static Dictionary<Guid, string> GetRegionsByLevel(string _WebReference, int level, Guid RegionParentId)
    {
        Dictionary<Guid, string> dic = new Dictionary<Guid, string>();

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_WebReference);
        WebReferenceSite.RegionRequest request = new WebReferenceSite.RegionRequest();
        request.level = level;
        request.RegionParentId = RegionParentId;
        WebReferenceSite.ResultOfListOfRegionData result = null;
        try
        {
            result = _site.GetRegionsOfParent(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return null;

        foreach (WebReferenceSite.RegionData rd in result.Value)//  XmlNode node in xdd["Areas"].ChildNodes)
        {
            string name = rd.RegionName;
            Guid RegionId = rd.RegionId;
            dic.Add(RegionId, name);
        }
        return dic;
    }
    public static bool CreateServiceRequestByCallBack(ServiceRequest serviceRequet)
    {        
        //LogEdit.SaveLog(serviceRequet.siteId, "try:" + serviceRequet.exposureId + "\r\nsurfer phone: " + serviceRequet.siteId.ToString());                   

        /*
        MongoDB.Bson.ObjectId ExposureId;

        if (!MongoDB.Bson.ObjectId.TryParse(serviceRequet.exposureId, out ExposureId))
        {
            LogEdit.SaveLog("general", "CreateServiceRequestLock2 Guid Exposure Id Faild:" + serviceRequet.exposureId + "\r\nsurfer phone: " +
                    serviceRequet.phone +
                    "\r\nSession: " + HttpContext.Current.Session.SessionID + "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));
            return false;
        }
        */
        /*
        Guid OriginId;

        if (!Guid.TryParse(serviceRequet.originId, out OriginId))
        {
            LogEdit.SaveLog(serviceRequet.siteId, "New CreateServiceRequestLock OriginId Faild:" + serviceRequet.exposureId + "\r\nsurfer phone: " +
                    serviceRequet.phone +
                    "\r\nSession: " + HttpContext.Current.Session.SessionID + "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));
            return false;
        }
        */
        if (string.IsNullOrEmpty(serviceRequet.phone))
        {
            Random randomDigit = new Random();
            int _index7Digit = randomDigit.Next(10);
            int _index8Digit = randomDigit.Next(10);
            int _index9Digit = randomDigit.Next(10);
            int _index10Digit = randomDigit.Next(10);
            serviceRequet.phone = "555555" + _index7Digit + _index8Digit + _index9Digit + _index10Digit;
        }
        SliderData sd = null;
        if (serviceRequet.exposureId != MongoDB.Bson.ObjectId.Empty)
            sd = SliderData.GetSliderData(serviceRequet.exposureId);
        if (sd == null)
        {
            /*
            sd = new SliderData();
            sd.SiteId = PpcSite.GetCurrent().SiteId;
            sd.ExpertiseCode = GetCallBackExpertise(serviceRequet.advertiserId);
             * */
            return false;
        }
        string _zip_code = SalesUtility.GetZipcode(string.Empty, sd, serviceRequet.zipCode);
        /*
        if (IsValidZipCode(serviceRequet.zipCode, serviceRequet.Country))
            sd.RegionCode = serviceRequet.zipCode;
        else if (string.IsNullOrEmpty(sd.RegionCode) || !IfZipCodeExists(sd.RegionCode, sd.SiteId))
            sd.RegionCode = "10019";
         * */
        if(string.IsNullOrEmpty(_zip_code))
            sd.RegionCode = "10019";

        if (string.IsNullOrEmpty(sd.Country) && !string.IsNullOrEmpty(serviceRequet.Country))
            sd.Country = serviceRequet.Country;

        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, sd.SiteId);

        _customer.Timeout = 300000;
        WebReferenceCustomer.ServiceRequest _request = new WebReferenceCustomer.ServiceRequest();

        _request.ExposureId = sd._id.ToString();
        _request.SiteId = sd.SiteId;
        _request.OriginId = sd.OriginId;
        _request.PreSoldToAdvertiserId = serviceRequet.advertiserId;
        
        string PhoneNum = Utilities.GetCleanPhone(serviceRequet.phone);
        _request.ContactPhoneNumber = PhoneNum;
        _request.ContactEmail = serviceRequet.email;
        _request.ContactFullName = serviceRequet.firstName + " " + serviceRequet.lastName;
        _request.RegionCode = sd.RegionCode;//.zipCode;
        _request.Country = sd.Country;
        _request.PreSoldPrice = serviceRequet.preSoldPrice;
        _request.ExpertiseCode = sd.ExpertiseCode;
        _request.ExpertiseType = 1;
        _request.NumOfSuppliers = serviceRequet.numOfSuppliers > 0 ? serviceRequet.numOfSuppliers : 3;
        _request.RequestDescription = serviceRequet.requestDescription;
        _request.IP = sd.IP;
//        _request.SessionId = HttpContext.Current.Session.SessionID;
        _request.Step = serviceRequet.step;
        _request.PrefferedCallTime = DateTime.UtcNow;
        _request.ControlName = sd.ControlName.ToString();
        _request.Domain = sd.ClientDomain;
        _request.Keyword = sd.Keyword;
        _request.ToolbarId = sd.ToolbarId;
        _request.SiteTitle = sd.title;
        _request.PageName = sd.PageName;
        _request.PlaceInWebSite = sd.PlaceInWebSite;
        _request.Url = sd.Url;
        _request.Browser = sd.Browser;
        _request.BrowserVersion = sd.BrowserVersion;

        _request.PreSoldMessage = serviceRequet.preSoldMessage;

        WebReferenceCustomer.ServiceResponse _response = null;
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, sd.SiteId);
        HttpContext Context = HttpContext.Current;
        try
        {
            _response = customer.CreateServiceRequestWithZipcodeUSA(_request);
        }
        catch (Exception exc)
        {
            dbug_log.SliderLog(_request, Context, exc);
            LogRequest(_request, _response, Context);
            return false;
        }

        if (_response.Status == WebReferenceCustomer.StatusCode.Success)
        {

            LogEdit.SaveLog(sd.SiteId, "CreateServiceRequest2 status:" + _response.Status.ToString().ToLower() +
                "\r\nid: " + _response.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " +
                PhoneNum + "\r\nDescription: " + _request.RequestDescription +
                "\r\nServiceAreaCode: " + _request.RegionCode +
                "\r\nExpertiseCode: " + sd.ExpertiseCode +
                "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request) +
                "\r\nURL: " + sd.Url);

        }
        else if (_response.Status == WebReferenceCustomer.StatusCode.BadParametersReceived)
        {
            LogRequest(_request, _response, Context);
            return false;
        }

        else
        {
            LogRequest(_request, _response, Context);
            return false;
        }

        return true;
    }
    static string GetCallBackExpertise(Guid AccountId)
    {
        if (!AdvertiseCallBackExpertise.ContainsKey(AccountId))
            return string.Empty;
        return AdvertiseCallBackExpertise[AccountId];
    }
    static void LogRequest(WebReferenceCustomer.ServiceRequest _request, WebReferenceCustomer.ServiceResponse _response, HttpContext context)
    {
        LogEdit.SaveLog(_request.SiteId, "CreateServiceRequest2 status:" + ((_response == null) ? "null" : _response.Status.ToString()) +
            "\r\nid: " + ((_response == null || _response.ServiceRequestId == null) ? "null" : _response.ServiceRequestId.ToString()) +
            "\r\nMessage: " + ((_response == null || string.IsNullOrEmpty(_response.Message)) ? "null" : _response.Message) +
                "\r\nsurfer phone: " +
                _request.ContactPhoneNumber + "\r\nDescription: " + _request.RequestDescription +
                "\r\nServiceAreaCode: " + _request.RegionCode +
                "\r\nExpertiseCode: " + _request.ExpertiseCode +
                "\r\nSession: " + ((context.Session == null) ? string.Empty : context.Session.SessionID) + "\r\nip: " + Utilities.GetIP(context.Request) +
                "\r\nURL: " + _request.Url);
    }
    public static bool IfZipCodeExists(string Zipcode, string SiteId)
    {

        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.ResultOfBoolean result = null;
        try
        {
            result = _customer.CheckUsaZipCode(Zipcode);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteId);
            return false;
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
            return false;
        return result.Value;
    }
    public static bool TryGetPublisherUserName(Guid UserId, string UrlRefference, out string name)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlRefference);
        WebReferenceSite.ResultOfString result = null;
        name = null;
        try
        {
            result = _site.GetPublisherUserName(UserId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false;
        }
        if (result.Type != WebReferenceSite.eResultType.Success)
            return false;
        if (string.IsNullOrEmpty(result.Value))
            return false;
        name = result.Value;
        return true;
    }
    /*
    public static string GetNumberForReportNonNegative<T>(Number<T> num, string _format)
    {
        if T is nu
    }
     * */
    public static string GetNumberForReportNonNegative(double num, string _format, string EndWith)
    {
        if (num < 0)
            return "-";
        return string.Format(_format, num) + EndWith;
    }
    public static string GetNumberForReportNonNegative(decimal num, string _format, string EndWith)
    {
        if (num < 0)
            return "-";
        return string.Format(_format, num) + EndWith;
    }
    public static string GetNumberForReportNonNegative(int num, string _format, string EndWith)
    {
        if (num < 0)
            return "-";
        return num.ToString() + EndWith;
    }
    public static bool CheckUS_PhoneValidation(string phone)
    {
        Regex rgx = new Regex(PpcSite.GetCurrent().PhoneReg);
        return rgx.IsMatch(phone);
    }
    

}
