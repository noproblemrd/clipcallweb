﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

/// <summary>
/// Summary description for LandedOnJoinPage
/// </summary>
public class LandedOnJoinPage
{
    public Guid ID { get; set; }
    public string IP { get; set; }
    public string UrlReferer { get; set; }
    public string Browser { get; set; }
    public string BrowserVersion { get; set; }
    public eFlavour? Flavor { get; set; }
    public DateTime date { get; set; }
    public WebReferenceSupplier.eAdvertiserRegistrationLandingPageType LandingPage { get; set; }
    /*
	public LandedOnJoinPage()
	{
        
	}
     * */
    public LandedOnJoinPage(string IP, string UrlReferer, string Browser, string BrowserVersion, string Flavor) : 
        this(IP, UrlReferer, Browser, BrowserVersion, Flavor, WebReferenceSupplier.eAdvertiserRegistrationLandingPageType.A)
    {
        
    }
    public LandedOnJoinPage(string IP, string UrlReferer, string Browser, string BrowserVersion, string Flavor, WebReferenceSupplier.eAdvertiserRegistrationLandingPageType LandingPage)        
    {
        this.IP = IP;
        this.UrlReferer = UrlReferer;
        this.Browser = Browser;
        this.BrowserVersion = BrowserVersion;
        date = DateTime.Now;
        eFlavour _flavor;
        if (Enum.TryParse(Flavor, out _flavor))
            this.Flavor = _flavor;
        else
            this.Flavor = null;
        this.LandingPage = LandingPage;
    }
    public Guid InsertHit()
    {
        Guid _id = Guid.NewGuid();
        WebReferenceSupplier.LandedRegistraionPageRequest request = new WebReferenceSupplier.LandedRegistraionPageRequest();
        request.ID = _id;
        request.BrowserVersion = this.BrowserVersion;
        request.Browser = this.Browser;
        request.date = this.date;
        request.IP = this.IP;
        request.UrlReferer = this.UrlReferer;
        request.Flavor = (this.Flavor == null) ? null : this.Flavor.ToString();
        request.LandingPage = this.LandingPage;

        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
        {
            using (System.Security.Principal.WindowsImpersonationContext wi = PpcSite.GetWindowsImpersonationContext)
            {
                WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
                WebReferenceSupplier.Result _result = null;
                try
                {
                    _result = _supplier.SetRegistrationHit(request);
                }
                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc);
                    //    return Guid.Empty;
                }
            }
            
        }));
        return _id;
         

    }
}