﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data;
using System.Text;
using System.IO;
using System.Xml.Linq;

//using Microsoft.Ajax.Utilities;


/// <summary>
/// Summary description for AddonTools
/// </summary>
public class AddonTools
{
 //   public static System.Timers.Timer _timer;
    const string PORT = "port:";
    const string VertiShoppingScript = @"window['__jBoostRan'] = true;
                window['__np__vbr'] = {
                    'partner': { 'id': '2001', 'subid': '1', 'url': 'http://partnerurl.com', 'name': 'Pastaleads' },
                    'inimage': { 'active': true, 'auto': true, 'hover': true },
                    'coupons': { 'active': true },
                    'nt': { 'active': true, 'nti': '10m' },
                    'gallery': { 'active': true, 'rt': true, 'rtd': 10, 'rsi': '10m', 'maxSize': 0.6, 'position': 'top' }
                };
                window['__boostConfig'] = window['__np__vbr'];
                var _element = document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0];
                var observer = new MutationObserver(function (mutations) {
                    mutations.forEach(function (mutation) {
                        for (var i = 0; i < mutation.addedNodes.length; i++) {
                            if (typeof mutation.addedNodes[i].tagName == 'string' && mutation.addedNodes[i].tagName.toLowerCase() == 'script') {
                                if (typeof window['__boostConfig'] != 'object' || typeof window['__boostConfig']['partner'] != 'object' ||
                                    !window['__boostConfig']['partner']['id'] || window['__boostConfig']['partner']['id'] != '2001') {
                                    window['__boostConfig'] = window['__np__vbr'];
                                    return;
                                }
                            }
                        }
                    });
                });
                var config = { attributes: true, childList: true, subtree: true };
                observer.observe(_element, config);";
    enum eOriginType
    {
        Distribution = 1,
        PartnerNoneInjections = 2
    }
	public AddonTools()
	{
		//
		
		//
	}
    public static string GetKeywordForSearch(string keyword)
    {
        string NewStr = "";
        keyword = keyword.ToLower();
        for (var i = 0; i < keyword.Length; i++)
        {
            if ((keyword[i] > 96 && keyword[i] < 123) ||
                (keyword[i] > 191 && keyword[i] < 592))
                NewStr += keyword[i];
            else
                if (NewStr.Length > 0 && NewStr[NewStr.Length - 1] != ' ')
                    NewStr += " ";
        }
        return NewStr.Trim();
    }
    static string GetVertiShoppingScript(PpcSite _cache)
    {
        if (_cache.IsLocalMachine)
            return VertiShoppingScript;
        GoogleClosure gc = new GoogleClosure();
        return gc.Compress(VertiShoppingScript, false, _cache);

    }
    /*
    public static List<string> GetBadDomain(PpcSite _cache)
    {
        List<string> list = new List<string>();
        WebReferenceSite.Site __site = WebServiceConfig.GetSiteReference(_cache.UrlWebReference);
        WebReferenceSite.ResultOfListOfStringBoolPair resultDomain = null;
        try
        {
            resultDomain = __site.GetAllBadDomains();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return list;
        }
        foreach (WebReferenceSite.StringBoolPair kvp in resultDomain.Value)
        {
            list.Add(kvp.Name);
        }
        return list;
    }
     * */
    public static List<string> GetClientBadDomain(PpcSite _cache)
    {
        List<string> list = new List<string>();
        string command = @"select Domain
                            FROM dbo.BlackDomain
                            where BlockInClient = 1";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    list.Add((string)reader["Domain"]);
                }
                conn.Close();
            }

        }
        return list;
    }
    static StringBuilder GetListForJsKeyword(List<KeywordsHeading> keyword_list, PpcSite _cache, out StringBuilder sbKeys)
    {
        List<KeywordStringJs> list_keywords_string = new List<KeywordStringJs>();
        List<string> list_Words = new List<string>();
        foreach (KeywordsHeading kvp in keyword_list)
        {
            KeywordStringJs KeywordString = new KeywordStringJs();
            List<KeyValuePair<string, bool>> list = new List<KeyValuePair<string, bool>>();
            //      list_keywords.Add(kvp.Key);
            string[] keys = kvp.Keyword.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in keys)
            {

                list.Add(new KeyValuePair<string, bool>(s, true));
                if (!list_Words.Contains(s))
                    list_Words.Add(s);
            }
            foreach (string s in _cache.GetNotKeywordsByHeadingIdForJs(kvp.HeadingId))
            {
                list.Add(new KeyValuePair<string, bool>(s, false));
                if (!list_Words.Contains(s))
                    list_Words.Add(s);
            }
            KeywordString.ListWord = list;
            KeywordString.Priority = kvp.Priority;
            list_keywords_string.Add(KeywordString);
        }


        list_Words.Sort(SalesUtility.CompareJavascriptString);
        Dictionary<string, List<int>> dic_words = new Dictionary<string, List<int>>();
        Dictionary<string, int> dic_words_index = new Dictionary<string, int>();
        List<KeywordIndexJs>  dic_keys = new List<KeywordIndexJs>();
        for (int i = 0; i < list_Words.Count; i++)
        {
            dic_words.Add(list_Words[i], new List<int>());
            dic_words_index.Add(list_Words[i], i);
        }
        for (int ik = 0; ik < list_keywords_string.Count; ik++)
        {
            KeywordIndexJs kij = new KeywordIndexJs();
            kij.ListWord = new List<KeyValuePair<int, bool>>();
            foreach (KeyValuePair<string, bool> _KeywordStringJs in list_keywords_string[ik].ListWord)
            {
                dic_words[_KeywordStringJs.Key].Add(ik);
                kij.ListWord.Add(new KeyValuePair<int, bool>(dic_words_index[_KeywordStringJs.Key], _KeywordStringJs.Value));
            }
            kij.Priority = list_keywords_string[ik].Priority;
            dic_keys.Add(kij);
        }
        StringBuilder sb = new StringBuilder();
        sb.Append("[");
        foreach (KeyValuePair<string, List<int>> pair in dic_words)
        {
            sb.Append("{word: '" + pair.Key + "', KeyId: [");
            foreach (int _int in pair.Value)
                sb.Append(_int + " ,");
            sb.Remove(sb.Length - 2, 2);
            sb.Append("]}, ");
        }
        sb.Remove(sb.Length - 2, 2);
        sb.Append("]");
        sbKeys = new StringBuilder();
        sbKeys.Append("[");
        foreach (KeywordIndexJs kij in dic_keys)
        {
            sbKeys.Append("{positive: [");
            foreach (KeyValuePair<int, bool> num in kij.ListWord)
                if (num.Value)
                    sbKeys.Append(num.Key + ", ");
            sbKeys.Remove(sbKeys.Length - 2, 2);
            sbKeys.Append("], ");
            sbKeys.Append("not: [");
            foreach (KeyValuePair<int, bool> num in kij.ListWord)
                if (!num.Value)
                    sbKeys.Append(num.Key + ", ");
            if (sbKeys[sbKeys.Length - 1] != '[')
                sbKeys.Remove(sbKeys.Length - 2, 2);
            sbKeys.Append("], ");
            sbKeys.Append("priority: " + kij.Priority);
            sbKeys.Append("}, ");
        }
        sbKeys.Remove(sbKeys.Length - 2, 2);
        sbKeys.Append("]");
        return sb;

    }
    public static JS_Files GetLogicJS(PpcSite _cache)
    {
        
     //   PpcSite _site = PpcSite.GetCurrent();
        //   Dictionary<string, Guid> dic_key = ;
        //     int ik = 0, iw = 0;
        IEnumerable<KeywordsHeading> keyword_query = from x in _cache.KeywordsList
                                                     where x.IsPositiveKey
                                                     select x;

     //   List<KeywordsHeading> list_key_temp = keyword_query.ToList();
        StringBuilder sbKeys;
        StringBuilder sb = GetListForJsKeyword(keyword_query.ToList(), _cache, out sbKeys);
        var GoogleHeading = from x in _cache.headings
                            where x.Value.IncludeGoogleAddon == true
                            select x.Value.ID;
        IEnumerable<KeywordsHeading> keyword_query_google = from x in _cache.KeywordsList
                                                            where x.IsPositiveKey &&
                                                               GoogleHeading.Contains(x.HeadingId)
                                                            select x;
       
        StringBuilder sbKeys_Google;
        StringBuilder sb_Google = GetListForJsKeyword(keyword_query_google.ToList(), _cache, out sbKeys_Google);
       
        List<string> bad_domains = GetClientBadDomain(_cache);
       // bad_domains.Sort();
        StringBuilder sb_domains = new StringBuilder();
        sb_domains.Append("[");
        if (bad_domains != null && bad_domains.Count > 0)
        {

            for(int i = 0; i < bad_domains.Count; i++)
            {               
                bad_domains[i] = (bad_domains[i][0] == '.' ? "" : ".") + bad_domains[i].ToLower();
       //         ListDomainName.Add(DomainName.ToLower());
            }
            bad_domains.Sort();
            foreach (string s in bad_domains)
                sb_domains.Append("'" + s + "', ");
            if (sb_domains.Length > 1)
                sb_domains.Remove(sb_domains.Length - 2, 2);
        }
        sb_domains.Append("]");

        StringBuilder sb_black_origins = new StringBuilder("['" + "984DAE08-1B8E-E111-9538-001517D1792A'".ToLower() +
            ",'06699A8C-7282-E311-B7DA-001517D1792A'".ToLower() + //Amonetize NoProblem AddOn
           ",'6E35F62E-F851-E311-A2C4-001517D1792A']".ToLower()); //SpeedBit

        StringBuilder sb_origin_type_list = new StringBuilder("[");
        List<Guid> ListOriginWithVertiShopping = _cache.GetOriginsWithVertiShopping();
        foreach (Guid OriginId in _cache.DistributionList)
        {
            bool with_verti = ListOriginWithVertiShopping.Contains(OriginId);
            sb_origin_type_list.Append("{id:'" + OriginId.ToString().ToLower() + "',type:" + 
                ((int)eOriginType.Distribution) + ",WithVertiShopping:" + with_verti.ToString().ToLower() + "},");
        }
        foreach(Guid OriginId in _cache.GetPartnerBlackInjections())
            sb_origin_type_list.Append("{id:'" + OriginId.ToString().ToLower() + "',type:" + 
                ((int)eOriginType.PartnerNoneInjections) + ",WithVertiShopping:false},");
        sb_origin_type_list.Remove(sb_origin_type_list.Length - 1, 1);
        sb_origin_type_list.Append("]");
        /*
        List<Guid> ListOriginNonDistribution = _cache.GetListOriginNonDistribution();
        
        foreach (Guid OriginNonDustribution in ListOriginNonDistribution)
        {
            var _query = from x in _cache.MonetizationOriginInjection.Keys
                         where x == OriginNonDustribution
                         select x;
            bool InjMon = _query.Count() > 0;
            sb_monetization_origin_inject.Append("{id:'" + OriginNonDustribution + "',ToInj:" + InjMon.ToString().ToLower() + "},");
            
        }
        
        
        */
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\addon7.6.js";
        string line;
        StringBuilder sbFile = new StringBuilder();
        

        if (File.Exists(path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    sbFile.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        
        StringBuilder sbGoogleAddon = new StringBuilder();
        string AddonGooglePath = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\addon_google3.0.js";
        if (File.Exists(AddonGooglePath))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(AddonGooglePath);
                while ((line = file.ReadLine()) != null)
                {
                    sbGoogleAddon.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        
        StringBuilder sbInj = new StringBuilder();
        /* inj1.+.js
        string InjPath = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\_inj.js";
        if (File.Exists(InjPath))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(InjPath);
                while ((line = file.ReadLine()) != null)
                {
                    sbInj.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        
         */
       // bool IsLocal = HttpRuntime.AppDomainAppVirtualPath != "/";

        string _domain = DBConnection.GetDomainBySiteId(_cache.SiteId);
        string _AppDomainAppVirtualPath = HttpRuntime.AppDomainAppVirtualPath.EndsWith("/") ? HttpRuntime.AppDomainAppVirtualPath : HttpRuntime.AppDomainAppVirtualPath + "/";
       
        string PathDestination;
        if (_cache.IsLocalMachine)
        {
            /*           
            string _port = Environment.CommandLine;
            int _index = _port.IndexOf(PORT);
            if (_index < 0)
                _port = "80";
            else
            {
                string BuilderPort = "";
                for (int i = _index + PORT.Length; i < _port.Length; i++)
                {
                    if ("1234567890".Contains(_port[i]))
                        BuilderPort += _port[i];
                    else
                        break;
                }
                _port = BuilderPort;
            }
             * */
            PathDestination = _domain + ":" + _cache.Port + _AppDomainAppVirtualPath;// +"/";
        }
        else
            PathDestination = _domain + _AppDomainAppVirtualPath;
        string[] PathDestinationSplit = PathDestination.Split('.');
        string pathLogic;
        string pathLogicShort, DomainCDN;
        if (PathDestinationSplit.Length > 1)
        {
            /*
            if (PathDestinationSplit[1] == "ppcnp")
            {
                pathLogic = PathDestination;
                DomainCDN = PathDestination;

            }
            else
            {
             */
                pathLogic = "app." + PathDestinationSplit[1] + "." + PathDestinationSplit[2];
                DomainCDN = "nps." + PathDestinationSplit[1] + "." + PathDestinationSplit[2];
            //}
            int startindex = PathDestination.IndexOf(PathDestinationSplit[0]) + PathDestinationSplit[0].Length;
            pathLogicShort = PathDestination.Substring(startindex, PathDestination.Length - startindex);
        }
        else
        {
            pathLogic = PathDestination;
            pathLogicShort = PathDestination;
            DomainCDN = PathDestination;
        }
        string pathGoogleLogic;
        string pathGoogleLogicShort;
        
        if (_cache.IsLocalMachine)
        {
            pathGoogleLogic = pathLogic;
            pathGoogleLogicShort = pathLogicShort;
        }
        else
        {
            pathGoogleLogic = _cache.GoogleAddonDomain + System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
            if(!pathGoogleLogic.EndsWith("/"))
                pathGoogleLogic += "/";
            string[] PathDestinationSplitGoogle = pathGoogleLogic.Split('.');
            //    pathLogic = PathDestination;
            int startindexGoogle = pathGoogleLogic.IndexOf(PathDestinationSplitGoogle[0]) + PathDestinationSplitGoogle[0].Length;
            pathGoogleLogicShort = pathGoogleLogic.Substring(startindexGoogle, pathGoogleLogic.Length - startindexGoogle);
            if (!DomainCDN.EndsWith("/"))
                DomainCDN += "/";
        }
        /*inj1.+.js
        sbInj.Replace("{Origins}", GetOriginsToInject());
        sbInj.Replace("{path_np}", DomainCDN);
        sbInj.Replace("{ProductName}", _cache.DefaultProductName);
        */

        string _last_update = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
        sbFile.Replace("{vals}", sb.ToString());
        sbFile.Replace("{keys}", sbKeys.ToString());
        sbFile.Replace("{domains}", sb_domains.ToString());
        sbFile.Replace("{IsLocal}", _cache.IsLocalMachine.ToString().ToLower());
        sbFile.Replace("{BlackOrigins}", sb_black_origins.ToString());
        sbFile.Replace("{RevizerId}", _cache.RevizerOriginId.ToString().ToLower());
        sbFile.Replace("{CrossriderId}", _cache.CrossriderId.ToString().ToLower());
        sbFile.Replace("{CrossriderTestId}", _cache.CrossriderTestId.ToString().ToLower());
        sbFile.Replace("{UnknownId}", _cache.UnknoenOriginId.ToString().ToLower());
        sbFile.Replace("{LastUpdate}", _last_update);
        
        sbFile.Replace("{VirtualPath}", _AppDomainAppVirtualPath);
        sbFile.Replace("{DefaultHost}", pathLogic);
        sbFile.Replace("{OriginTypes}", sb_origin_type_list.ToString());
        sbFile.Replace("{OriginTypeDistribution}", ((int)eOriginType.Distribution).ToString());
        sbFile.Replace("{InitVerti}", EncryptString.Base64Encode(GetVertiShoppingScript(_cache)));
     //   sbFile.Replace("{ThirdPartyPopupOrigins}", _cache.GetPopupEgineOriginsForJavascript());
        if (_cache.IsLocalMachine)
            sbFile.Replace("https:", "http:");


        sbGoogleAddon.Replace("{vals}", sb_Google.ToString());
        sbGoogleAddon.Replace("{keys}", sbKeys_Google.ToString());
        sbGoogleAddon.Replace("{LastUpdate}", _last_update);
        sbGoogleAddon.Replace("{IsLocal}", _cache.IsLocalMachine.ToString().ToLower());
        sbGoogleAddon.Replace("{BlackOrigins}", sb_black_origins.ToString());
        sbGoogleAddon.Replace("{SterklyG}", _cache.SterlyGoogleOriginId.ToString().ToLower());
        sbGoogleAddon.Replace("{VirtualPath}", _AppDomainAppVirtualPath);
        if (_cache.IsLocalMachine)
            sbGoogleAddon.Replace("https:", "http:");
        /*   GoogleClosure  
            GoogleClosure gc = new GoogleClosure();

            string _script = (_cache.IsLocalMachine) ? sbFile.ToString() : gc.Compress(sbFile.ToString(), false);
            string _GoogleScript = (_cache.IsLocalMachine) ? sbGoogleAddon.ToString() : gc.Compress(sbGoogleAddon.ToString(), false);
         *  * */
        string path_poc = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\poc1.1.js";
        string line_poc;
        StringBuilder sbFile_poc = new StringBuilder();

        if (File.Exists(path_poc))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(path_poc);
                while ((line_poc = file.ReadLine()) != null)
                {
                    sbFile_poc.AppendLine(line_poc);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        else
            dbug_log.ExceptionLog(new Exception("lo nimtza"));
        sbFile_poc.Replace("{vals}", sb.ToString());
        sbFile_poc.Replace("{keys}", sbKeys.ToString());
        sbFile_poc.Replace("{domains}", sb_domains.ToString());
  //      string poc = (_cache.IsLocalMachine) ? sbFile_poc.ToString() : gc.CompressSimple(sbFile_poc.ToString(), false);

        string _script, _GoogleScript, poc;
        if (!_cache.IsLocalMachine)
        {
           
      //      Minifier mf = new Minifier();
            /*
           _script = mf.MinifyJavaScript(sbFile.ToString());
           if (mf.ErrorList.Count > 0)
           {
               List<string> list_error = new List<string>();

               foreach (ContextError ce in mf.ErrorList)
                   list_error.Add(ce.Message);
               _cache.GetCacheLog.WriteCacheLog(new Exception("Minifier logic"), list_error.ToArray());
           }
            */
            GoogleClosure gc = new GoogleClosure();
            _script = gc.Compress(sbFile.ToString(), false, _cache);
            /*
            _GoogleScript = mf.MinifyJavaScript(sbGoogleAddon.ToString());
            if (mf.ErrorList.Count > 0)
            {
                List<string> list_error = new List<string>();

                foreach (ContextError ce in mf.ErrorList)
                    list_error.Add(ce.Message);
                _cache.GetCacheLog.WriteCacheLog(new Exception("Minifier GoogleAddon"), list_error.ToArray());
            }
            poc = mf.MinifyJavaScript(sbFile_poc.ToString());
            if (mf.ErrorList.Count > 0)
            {
                List<string> list_error = new List<string>();

                foreach (ContextError ce in mf.ErrorList)
                    list_error.Add(ce.Message);
                _cache.GetCacheLog.WriteCacheLog(new Exception("Minifier GoogleAddon"), list_error.ToArray());
            }
             * */
            _GoogleScript = gc.Compress(sbGoogleAddon.ToString(), false, _cache);
            poc = gc.Compress(sbFile_poc.ToString(), false, _cache);
        }
        else
        {
            _script = sbFile.ToString();
            _GoogleScript = sbGoogleAddon.ToString();
            poc = sbFile_poc.ToString();
        }

        
            
    //    string _ScriptInj = string.Empty;
        //inj1.+.js
        //_ScriptInj = (IsLocal) ? sbInj.ToString() : gc.Compress(sbInj.ToString(), false);

       
        return new JS_Files(_script, poc, _GoogleScript, null);
    }
    /*
    private static string GetOriginsToInject()
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        IEnumerable<XElement> origins = xdoc.Element("Sites").Element("SitePPC").Element("InjectOrigins").Elements("Origin");
        Guid _id;
        string result = "[";
        foreach (XElement xelem in origins)
        {
            if (!Guid.TryParse(xelem.Value, out _id))
            {
                PpcCacheException pce = new PpcCacheException("Origins To Inject FAILD!!!");
                pce.NewValues = new Dictionary<string, string>();
                string name = xelem.Attribute("name") == null ? string.Empty : xelem.Attribute("name").Value;
                pce.NewValues.Add("Origin name", name);
                pce.NewValues.Add("OriginId", xelem.Value);
                CacheLog.WriteException(pce);
                continue;
            }
            result += "'" + _id.ToString().ToLower() + "',";
        }
        if (result.Length > 1)
            result = result.Substring(0, result.Length - 1);
        result += "]";
        return result;

    }
     * */
    
}