﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ResultRequest
/// </summary>
[Serializable()]
public class ResultRequest<T>
{
    eResultType Result_Type;
    public string ResultType
    {
        get { return Result_Type.ToString(); }
    }
    public List<string> Messages { get; set; }
    public T Value { get; set; }
    enum eResultType
    {
        Success,
        Failure
    }
	public ResultRequest(T value)
	{
        Messages = new List<string>();
        Result_Type = eResultType.Success;
        this.Value = value;
	}
    public ResultRequest(List<string> messages)
    {
        this.Messages = messages;
        Result_Type = eResultType.Failure;        
    }
}