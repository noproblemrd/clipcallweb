﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for eSlider
/// </summary>
public enum eSliderLog 
{ 
    Keyword_Not_Exist, 
    Empty_Keyword_General_false, 
    Missing_Params, 
    Slider_Exception, 
    Missing_Exposure_Type, 
    Origin_Source, 
    CRM_Error, 
    Ivr_Number_Empty,
    Image_Exist,
    Js_Logic_Not_Find,
    Unrecognized_Log_From_Client,
    Session_Null_Wibiya,
    Logic_Error,
    IP_Error,
    LoadedDateError,
    UpdateExposureFaild,
    InsertExposureFaild
}

public class eSlider
{
	public eSlider()
	{
		//
		
		//
	}
}