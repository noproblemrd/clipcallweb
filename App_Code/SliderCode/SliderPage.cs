﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml.Linq;
using System.Net;
using System.IO;



/// <summary>
/// Summary description for SliderPage
/// </summary>
public abstract class SliderPage : Page, iSliderData
{
    const string FACEBOOK_SCRIPT = @"
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            }; if (!f._fbq) f._fbq = n;
            n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
        }(window,
        document, 'script', '//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '715905791849028');
        fbq('track', 'PageView');";
    const string FACEBOOK_NOSCRIPT = @"<img height=""1"" width=""1"" style=""display:none"" src=""https://www.facebook.com/tr?id=715905791849028&ev=PageView&noscript=1""/>";
    const string ADROLL_PIXEL_SCRIPT = @"adroll_adv_id = ""ZOM4NGOCW5HOBBGDDC5GBX"";    adroll_pix_id = ""B2PON6FEPBHEFOV4I6DINV"";    (function () {        var _onload = function(){            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}            var scr = document.createElement(""script"");            var host = ((""https:"" == document.location.protocol) ? ""https://s.adroll.com"" : ""http://a.adroll.com"");            scr.setAttribute('async', 'true');            scr.type = ""text/javascript"";            scr.src = host + ""/j/roundtrip.js"";            ((document.getElementsByTagName('head') || [null])[0] ||                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);        };        if (window.addEventListener) {window.addEventListener('load', _onload, false);}        else {window.attachEvent('onload', _onload)}    }());";
    static readonly List<string> ExpertiseCodeToInjectFacebool;

    static SliderPage()
    {
        ExpertiseCodeToInjectFacebool = new List<string>();
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc;
        try
        {
            xdoc = XDocument.Load(path);
        }
        catch (Exception exc)
        {
            CacheLog cl = new CacheLog();
            cl.WriteCacheLog(exc, new string[] { "SliderPage static initialize" }, true);
            return;
        }
        foreach(XElement xelem in xdoc.Element("Sites").Element("SitePPC").Element("FacebookCustomAudience").Elements("Categories"))
            ExpertiseCodeToInjectFacebool.Add(xelem.Value);
        
    }

    public const string NEW_WINDOW_ARGS = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=yes";
    public const string NEW_WINDOW_scrollbars = "scrollbars=yes";
    public const string REQUEST_COOKIE = "RequestCookie";
                                      
    int IndexPostMessage = 0;
    
    public SliderData Slider_Data { get; private set; }
   // protected string productName { get; private set; }
    public bool OpenDistinationPage { get; set; }
    protected string CurrencySymbole { get; set; }
    string _ZipcodeRegulareExpression = @"^\d{5}$";
    string _PhoneNumberExpression;
    string _date_format = "mm/dd/yy";
    string _date_format_UK = "dd/mm/yy";
    public string OpenDistinationPageForJavaScript
    {
        get
        {
            return OpenDistinationPage.ToString().ToLower();
        }
    }

    protected string ZipcodeRegulareExpression
    {
        get { return _ZipcodeRegulareExpression; }
    }
    protected string EmailRegulareExpression
    {
        get { return @"^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$"; }
    }
    protected string PhoneNumberExpression
    {
        get { return _PhoneNumberExpression; }
    }
    protected string ZipcodeRegulareExpressionJavascript
    {
        get { return Utilities.RegularExpressionForJavascript(_ZipcodeRegulareExpression); }
    }
    protected string EmailExpressionJavascript
    {
        get { return Utilities.RegularExpressionForJavascript(EmailRegulareExpression); }
    }
    protected string PhoneNumberExpressionJavascript
    {
        get { return Utilities.RegularExpressionForJavascript(_PhoneNumberExpression); }
    }
	public SliderPage()
	{
		//
		  
		//
	}
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Slider_Data = SliderData.GetSliderData(Context);
        if (Slider_Data == null || Slider_Data._id == MongoDB.Bson.ObjectId.Empty)
        {
          //  Context.ApplicationInstance.CompleteRequest();
      //      Response.SuppressContent = true;
            Response.Flush();
            Response.End();
        }
   
        //Security- for the browser not save in the cash memory
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");
        ////
        PpcSite _cache = PpcSite.GetCurrent();
     //   Slider_Data.Country = "UNITED KINGDOM";
        if (Slider_Data.Country == "UNITED KINGDOM")
        {
      //      lbl_ZipCode.Text = "Postcode";
            CurrencySymbole = "£";
            _ZipcodeRegulareExpression = _cache.ZipcodeRegularExpression_UK;
            _PhoneNumberExpression = _cache.PhoneRegularExpression_UK;
        }
        else
        {
            CurrencySymbole = "$";
            _ZipcodeRegulareExpression = _cache.ZipcodeReg;
            _PhoneNumberExpression = _cache.PhoneReg;
        }

        HtmlGenericControl _ScriptControl = new HtmlGenericControl("script");
        _ScriptControl.Attributes["type"] = "text/javascript";
        _ScriptControl.Attributes["src"] = ResolveUrl("~/npsb/ClickLogs.js");
        Header.Controls.Add(_ScriptControl);
        switch (Slider_Data.AppType)
        {
            case(eAppType.DeskApp):
            //default:
                InitDeskappCoverLoader();
                break;
        }

        switch (Slider_Data.SliderType)
        {
            case(eSliderType.GoogleFrame)://(eExposureType.GoogleSterkly):
                HtmlLink _link = new HtmlLink();
                _link.Href = ResolveUrl("~/npsb/images/favicon.ico");
                _link.Attributes["rel"] = "icon";
                _link.Attributes["type"] = "img/ico";
                Header.Controls.Add(_link);
                break;
            case(eSliderType.XSaver)://(eExposureType.XSaver):
                
                SendInteractionPerion();
                break;
            case(eSliderType.Intext):
                AddMouseClickEventForIntext();
                break;
            default:
                 eFlavour _flavor;
                 if (Enum.TryParse(Request.QueryString["ChangeSize"], out _flavor))
                 {
                     SliderViewData svd = PpcSite.GetCurrent().Flavors[_flavor];
                     SendReziseSliderToParent(svd.width, svd.height, svd.IframeWidth, svd.IframeHeight, svd.CssClass);
                 }
                break;
        }
        LoadPixelsScripts();
        
    }
    private void LoadPixelsScripts()
    {
        LoadFacebookCustomAudienceScript();
        LoadAdrollPixelScript();
    }

    private void LoadAdrollPixelScript()
    {
        HtmlGenericControl script = new HtmlGenericControl("script");
        script.Attributes.Add("type", "text/javascript");
        script.InnerHtml = ADROLL_PIXEL_SCRIPT;
        Header.Controls.Add(script);
    }
    private void LoadFacebookCustomAudienceScript()
    {
 //       if(ExpertiseCodeToInjectFacebool.Contains(Slider_Data.ExpertiseCode))
 //       {
            HtmlGenericControl script = new HtmlGenericControl("script");
            script.Attributes.Add("type", "text/javascript");
            script.InnerHtml = FACEBOOK_SCRIPT;

            HtmlGenericControl noscript = new HtmlGenericControl("noscript");
            noscript.InnerHtml = FACEBOOK_NOSCRIPT;

            Header.Controls.Add(script);
            Header.Controls.Add(noscript);
 //       }
    }
    /*
    protected virtual void SetGeneralSliderExposure()
    {
        Slider_Data = new SliderData(Context, eSliderType.General);
    }
     */
    protected override void OnPreLoad(EventArgs e)
    {
        base.OnPreLoad(e);
        /*
        switch (Slider_Data.Type)
        {
            case (eExposureType.GoogleSterkly):
            case (eExposureType.Aducky):
            case (eExposureType.PastaLeads):
            case (eExposureType.Regular):
            case(eExposureType.SpeedOptimizer_Guru):
            case(eExposureType.PicRec):
            case (eExposureType.Popup):
            case(eExposureType.DonutleadsDomain):
            case(eExposureType.DonutleadsPopup):
            case(eExposureType.PastaLeadsPopup):
                OpenDistinationPage = false;
                break;
            default:
                OpenDistinationPage = true;
                break;
        }
         * */
        OpenDistinationPage = (Slider_Data.AppType == eAppType.DeskApp) ? false : (Slider_Data.SliderType == eSliderType.Slider && Slider_Data.Type == eExposureType.Regular && Slider_Data.DomainName == eDomainName.noproblemppc);
        
        if (Slider_Data.SliderType != eSliderType.GoogleFrame)
            LoadGoogleAnalytic();
        LoadSentencesValidations();
        LoadCj();
        string ROOT_REQUEST = (Request.Url.Host == "app.noproblemppc.com") ? "http://www.noproblem.me/" : ResolveUrl("~");
       //a  npsb_flavours_affiliate
        
        ClientScript.RegisterClientScriptBlock(this.GetType(), "ROOT", "var ROOT='" + ResolveUrl("~") + "'; var ROOT_REQUEST='" + ROOT_REQUEST + "';", true);
        if(Slider_Data.SliderType != eSliderType.Popup)
            ClientScript.RegisterClientScriptBlock(this.GetType(), "CloseSliderFunction", GetPostMessageScriptFunctionCloseSlider(), true);
    }

    private void LoadCj()
    {        
        if (Slider_Data == null)
            return;
        PpcSite _cache = PpcSite.GetCurrent();
        if (Slider_Data.ControlName != eFlavour.Flavor_Half_Controls_Top_Cj && _cache.IsCJCampain(Slider_Data))
        {
            LiteralControl lc = new LiteralControl(@"<div style=""display:none;"">" + ((HeadingCampain)_cache.headings[Slider_Data.ExpertiseCode]).BannerHtml + "</div>");
            Page.Controls.Add(lc);
        }
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        SetLinks();


        ClientScript.RegisterClientScriptBlock(this.GetType(), "me", Slider_Data.GetDataScript(), true);
  //      if (Slider_Data.Type == eExposureType.GoogleSterkly || Slider_Data.Type == eExposureType.Aducky)
 //           ClientScript.RegisterStartupScript(this.GetType(), "RemoveLandingPage", "try{ RemoveLandingPage(); } catch(exc){}", true);

        ClientScript.RegisterStartupScript(this.GetType(), "LoadedFlag", GetLoadedFlagScript(), true);
    }
    protected string GetLoadedFlagScript()
    {
        string path = ResolveUrl("~/npsb/Callback/Exp.ashx") + "?" + Slider_Data.GetUrlParamsExposureId();
        string _script = @"var _i = new Image();
                _i.src = '" + path + "';";
        return _script;
    }
    private void SetLinks()
    {
        string _whatsThisLink = null, _AdvertiseLink = null, _SettingsLink = null;
        bool HasAdvertise = true;
        string privacy = null, terms = null;
        bool HideDisclaimer = false;
        switch (Slider_Data.Type)
        {
            case (eExposureType.Aducky):
                _whatsThisLink = "http://www.aducky.com/";
                HasAdvertise = false;

                privacy = "http://www.aducky.com/#!privacy-policy/c23l2";
                terms = "http://www.aducky.com/#!tandc/cg82";
                break;
            
            case(eExposureType.Shopwithboost):
                _whatsThisLink = "http://shopwithboost.com/";
                HasAdvertise = false;
                privacy = "http://shopwithboost.com/boost-privacy-policy.html​";
                terms = "http://shopwithboost.com/terms-of-use.html";
                break;
            case (eExposureType.SpeedOptimizer_Guru):
            case (eExposureType.PicRec):
            
      //      case (eExposureType.Popup):
      //      case (eExposureType.DonutleadsDomain):
     //       case(eExposureType.DonutleadsPopup):
     //       case(eExposureType.PastaLeadsPopup):
       //         string __domain = (Slider_Data.Type == eExposureType.DonutleadsDomain || Slider_Data.Type == eExposureType.DonutleadsPopup) ?
      //              "www.donutleads.com" : "www.pastaleads.com";
                SetLinks(ref _whatsThisLink, ref _AdvertiseLink, ref terms, ref privacy);                
                break;
            default:
                if(Slider_Data.SliderType == eSliderType.GoogleFrame)
                     HideDisclaimer = true;
          //      else if (Slider_Data.SliderType == eSliderType.Popup || Slider_Data.SliderType == eSliderType.PopupRevizer || Slider_Data.DomainName != eDomainName.noproblemppc)
                else if (Slider_Data.SliderType == eSliderType.PopupRevizer || Slider_Data.DomainName != eDomainName.noproblemppc)
   
                    SetLinks(ref _whatsThisLink, ref _AdvertiseLink, ref terms, ref privacy);           
                break;           
        }
        HasAdvertise = false; // Remove advertiser link from all the flavors
        FindControl fc = new FindControl(this, typeof(ISliderLinks));
        Control cnt = fc.StartTo();
        if (cnt != null)        
            ((ISliderLinks)cnt).SetSliderLinks(_whatsThisLink, _AdvertiseLink, _SettingsLink, HasAdvertise);
        
        fc = new FindControl(this, typeof(IDisclaimer));
        Control cnt_dis = fc.StartTo();
        if (cnt_dis == null)
           return;             
        ((IDisclaimer)cnt_dis).SetDisclaimerLinks(HideDisclaimer, privacy, terms);
    }
    private void SetLinks(ref string _whatsThisLink, ref string _AdvertiseLink, ref string terms, ref string privacy)
    {
        string __domain = (Slider_Data.DomainName == eDomainName.noproblemppc) ? "www.pastaleads.com" : GetHomeDomain();
        _whatsThisLink = (Slider_Data.Type == eExposureType._50OnRedId) ? "http://advertising-support.com/why.php" : "http://" + __domain + "/";
        //_AdvertiseLink = "http://" + __domain + "/contact-advertise";
        _AdvertiseLink = "http://app.noproblemppc.com/PPC/Register.aspx?comeFrom=Slider";
        terms = "http://" + __domain + "/eula/";
        privacy = "http://" + __domain + "/privacy/";
        /*
        switch (Slider_Data.SliderType)
        {
            case (eSliderType.Popup):
                break;
            default:
                {
                   
                }
                break;
        }
         * */
    }
    private string GetHomeDomain()
    {
        string[] _domain = Slider_Data.Domain.Split(new char[] {'.'}, StringSplitOptions.RemoveEmptyEntries);
        if (_domain.Length < 3)
            return Slider_Data.Domain + ":" + Request.Url.Port + HttpRuntime.AppDomainAppVirtualPath;
        string _result = "www";
        for (int i = 1; i < _domain.Length; i++)
            _result += "." + _domain[i];
        return _result;
        
    }

    private void LoadSentencesValidations()
    {
        string path = HttpContext.Current.Request.PhysicalApplicationPath + @"\Consumer\data\SliderMessages.xml";
        XDocument xdoc = XDocument.Load(path);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        foreach (XElement xelem in xdoc.Element("auction").Element("description").Elements())
            dic.Add(xelem.Name.LocalName, xelem.Value);
        description = dic;
        professional_valid = xdoc.Element("auction").Element("professional").Element("error").Value;
        phone_valid = xdoc.Element("auction").Element("phone").Element("error").Value;
        zipcode_valid = xdoc.Element("auction").Element("zipcode").Element("error").Value;
    }

    public virtual string GetUrlParams()
    {
        return "?" + Slider_Data.GetUrlParamsForFlavor();
    }
    public virtual string GetUrlParams(eFlavour _flavor)
    {
        string _UrlParams = GetUrlParams();        
        return _UrlParams + "&ChangeSize=" + _flavor.ToString();
    }
    private void SendMessageToParent(string message)
    {
    //    string _message = emtp.ToString() + ";" + message;
        string _script = GetSendMessageToParent(message);
        string ScriptName = "CallParent" + IndexPostMessage;
        if (ScriptManager.GetCurrent(this) != null && ScriptManager.GetCurrent(this).IsInAsyncPostBack)
            ScriptManager.RegisterStartupScript(this, this.GetType(), ScriptName, _script, true);
        else
            ClientScript.RegisterStartupScript(this.GetType(), ScriptName, _script, true);
        IndexPostMessage++;        
    }
    string GetSendMessageToParent(string message)
    {

        string _client_domain = (Slider_Data.AppType == eAppType.DeskApp) ? Slider_Data.GetClientDomainWithSchemaForMessage(Context)
            : Slider_Data.GetClientDomainWithSchemaForMessage(HttpContext.Current);
        string _script = @"var win = parent; win.postMessage('" + message + @"', '" + _client_domain + "');";
        return _script;
    }
    public string GetReziseLightSliderToParent(int heigth)
    {
        var message = new { heigth = heigth, selector = eMessageToParent.ResizeLight.ToString() };
        JavaScriptSerializer js = new JavaScriptSerializer();
        string str = js.Serialize(message);
       // SendMessageToParent(str);
        return GetSendMessageToParent(str);
    }
    public void SendReziseSliderToParent(int width, int heigth, int IframeWidth, int IframeHeigth, string CssClass)
    {
        var message = new { width = width, heigth = heigth, selector = eMessageToParent.Resize.ToString(), IframeWidth = IframeWidth, IframeHeigth = IframeHeigth, CssClass = CssClass };
        JavaScriptSerializer js = new JavaScriptSerializer();
        string str = js.Serialize(message);
        SendMessageToParent(str);
    }

    public void SendGeneralMessageToParent(string message, eMessageToParent selector)
    {
        if (Slider_Data.SliderType == eSliderType.Popup)
            return;
        if (selector == eMessageToParent.Title && Slider_Data.SliderType == eSliderType.Intext)
            return;
        var _params = new { message = message, selector = selector.ToString() };
        JavaScriptSerializer js = new JavaScriptSerializer();
        string str = js.Serialize(_params);
        SendMessageToParent(str);
    }
    string GetStringGeneralMessageToParent(string message, eMessageToParent selector)
    {
        var _params = new { message = message, selector = selector.ToString() };
        JavaScriptSerializer js = new JavaScriptSerializer();
        return js.Serialize(_params);
    }
    public void SendSizeToParentPerion()
    {
        SliderViewData svd = PpcSite.GetCurrent().Flavors[Slider_Data.ControlName];
        string _script = "parent.postMessage('iframeSize:" + svd.IframeWidth + "," + svd.IframeHeight + "','" + SliderData.GetPerionTemplateUrl + "');";
        string ScriptName = "SendSizeToParent";
        ClientScript.RegisterStartupScript(this.GetType(), ScriptName, _script, true);
    }
    public void SendInteractionPerion()
    {
        HtmlGenericControl _ScriptXsaver = new HtmlGenericControl("script");
        _ScriptXsaver.Attributes["type"] = "text/javascript";
        _ScriptXsaver.Attributes["src"] = ResolveUrl("~/npsb/flavours/XSaver/xsaver.js");
        Header.Controls.Add(_ScriptXsaver);
        string _script = "var _InteractiveSlider = new InteractiveSlider('" + SliderData.GetPerionTemplateUrl + "'); _InteractiveSlider.AddEvent();";
        string ScriptName = "InteractiveSlider";
        ClientScript.RegisterStartupScript(this.GetType(), ScriptName, _script, true);
    }
    public void LoadGoogleAnalytic()
    {
        string _script = @"<script type=""text/javascript"">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21441721-1']);
        _gaq.push(['_setDomainName', 'noproblemppc.com']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>";
        LiteralControl lc = new LiteralControl(_script);
        Header.Controls.Add(lc);
    }
    public string GetOpenConduitWhatsThis()
    {
        return GetSendMessageToParent(GetStringGeneralMessageToParent(Slider_Data.AppId, eMessageToParent.ConduitWhatsThis));
    }
    public string GetPostMessageScriptFunctionCloseSlider()
    {
        string message = "RemoveSlider";
        eMessageToParent selector = eMessageToParent.RemoveSlider;
        var _message = new { message = message, selector = selector.ToString() };
        string str = new JavaScriptSerializer().Serialize(_message);
        StringBuilder script = new StringBuilder();
        script.Append("function _NpHideSlider() {");
        script.Append("var win = parent;");
        script.Append("win.postMessage(");
        script.Append("'" + str + "',");
        script.Append("'"+Slider_Data.GetClientDomainWithSchemaForMessage(Context)+"'");
        script.Append(");}");
        return script.ToString();
    }
    protected void AddMouseClickEventForIntext()
    {
        var _message = new { message = "click", selector = eMessageToParent.IntextClick.ToString() };
        string str = new JavaScriptSerializer().Serialize(_message);
        StringBuilder script = new StringBuilder();
        script.Append("function SendIntextClick() {");
        script.Append("var win = parent;");
        script.Append("win.postMessage(");
        script.Append("'" + str + "',");
        script.Append("'" + Slider_Data.GetClientDomainWithSchemaForMessage(Context) + "'");
        script.Append(");}");
        script.Append("if (document.addEventListener) {document.addEventListener('mousedown', SendIntextClick, false);}");
        script.Append("else if (document.attachEvent) {document.attachEvent('onmousedown', SendIntextClick);}");
        HtmlGenericControl hgc = new HtmlGenericControl("script");
        hgc.Attributes["type"] = "text/javascript";
        hgc.InnerHtml = script.ToString();
        Header.Controls.Add(hgc);
    }
    public void ExecuteClodeSlider()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ExecuteClodeSlider", "_NpHideSlider();", true);
    }
    public string UrlSettings
    {
        get { return ResolveUrl("~/SliderSetting/Setting.aspx?" + Slider_Data.GetUrlParamsForFlavor()); }
    }
    public string PhoneRegJavascript
    {
        get { return Utilities.RegularExpressionForJavascript(PpcSite.GetCurrent().PhoneReg); }
    }
    public Dictionary<string, string> description { get; private set; }
    public string professional_valid { get; private set; }
    public string phone_valid { get; private set; }
    public string zipcode_valid { get; private set; }

    public string GetZipcodeByIP()
    {
        return SalesUtility.GetZipcode(Utilities.GetIP(Request),Slider_Data);// Request.ServerVariables["REMOTE_ADDR"], Slider_Data);
    }
    public string GetExposureId
    {
        get
        {
            return Slider_Data._id.ToString();
        }
    }
    protected void SetJqueryLink()
    {
        string srcJquery = Slider_Data.ClientScheme + "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js";
        //var srcJquery = sd.ClientScheme + "://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js";

        System.Web.UI.HtmlControls.HtmlGenericControl si = new System.Web.UI.HtmlControls.HtmlGenericControl();

        si.TagName = "script";
        si.Attributes.Add("type", "text/javascript");
        si.Attributes.Add("src", srcJquery);
        this.Page.Header.Controls.AddAt(0, si);

        this.Page.Header.Controls.Add(new LiteralControl("\n"));
                
    }
    protected string GetJqueryLink()
    {
        return Slider_Data.ClientScheme + "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js";        

    }
    protected string GetGoogleMapsJs()
    {
        return Slider_Data.ClientScheme + "://maps.googleapis.com/maps/api/js?key=AIzaSyByICNsdF00OXlUGTnzGsIfNmk0Mo7xqcw&sensor=false&language=en&libraries=places";

    }
    protected string getHeadingsRelatedHTML(string expertiseCode)
    {
        return getHeadingsRelatedHTML(expertiseCode, null);
    }
    protected string getHeadingsRelatedHTML(string expertiseCode, string CssClass)
    {
        if (string.IsNullOrEmpty(CssClass))
            CssClass = "relatedLink";
        PpcSite _cache = PpcSite.GetCurrent();
        HeadingRelated[] headingsRelated;
        headingsRelated = _cache.headings[expertiseCode].headingRelated;

        int headingRelatedLength = headingsRelated.Length;

        StringBuilder sbHeadingRelated = new StringBuilder();

        if (headingRelatedLength > 0)
        {
            for (int i = 0; i < headingRelatedLength; i++)
            {

                sbHeadingRelated.Append("<div class='" + CssClass + "'><a href='javascript:void(0);'");
                List<KeywordsHeading> listKeywords = _cache.GetKeywordsByCode(headingsRelated[i].Code);

                if (listKeywords != null && listKeywords.Count > 0)
                {
                    sbHeadingRelated.Append(" onclick='javascript:__np.OpenRelatedWin(\\\"" + listKeywords[0].Keyword + "\\\")' >");
                    sbHeadingRelated.Append(headingsRelated[i].Name);
                    sbHeadingRelated.Append("</a></div>");
                }

            }
        }

        return sbHeadingRelated.ToString();
    }
    public void InitDeskappCoverLoader()
    {
        HtmlGenericControl _script = new HtmlGenericControl("script");
        _script.Attributes["type"] = "text/javascript";
        _script.Attributes["src"] = "/npsb/flavours/DesktopAppScript.js";
        Header.Controls.Add(_script);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "InitDeskappCoverLoader", "DesktopApp.init();", true);
    }
    protected string GetServiceRequestLockUrl
    {
        get
        {
            
            return ResolveUrl("~/npsb/CreateServiceRequestLock.ashx");
        }
    }
    protected string ROOT_AFTER_REQUEST
    {
        get
        {
            return (Request.Url.Host == "app.noproblemppc.com") ? "http://www.noproblem.me/" : ResolveUrl("~");
        }
    }
    protected string COUNTRY_GOOGLE
    {
        get { return Slider_Data.Country == "UNITED KINGDOM" ? "uk" : "us"; }
    }
    protected string DATE_FORMAT
    {
        get { return Slider_Data.Country == "UNITED KINGDOM" ? _date_format_UK : _date_format; }
    }
    public string IsDesktopAppJavascript
    {

        get { return (Slider_Data.AppType == eAppType.DeskApp).ToString().ToLower(); }

    }
}