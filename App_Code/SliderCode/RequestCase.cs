﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RequestCase
/// </summary>
public class RequestCase
{
    public string ago { get; set; }
    public string description { get; set; }
    public string HeadingName { get; set; }
    public string city { get; set; }
    public bool IsPrivacy { get; set; }
    public decimal lng { get; set; }
    public decimal lat { get; set; }
    private DateTime date;
    public Guid ID
    {
        get;
        set;
    }
    
	public RequestCase()
	{
        IsPrivacy = false;
		//
		
		//
	}

    public void SetDate(DateTime date)
    {
        this.date = date;
    }
    public DateTime GetDate()
    {
        return date;
    }
}