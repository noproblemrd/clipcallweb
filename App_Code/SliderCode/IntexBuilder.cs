﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for IntexBuilder
/// </summary>
public class IntexBuilder
{
    const int EXTRA_HEIGHT = 20;
    public StringBuilder sb_vals { get; set; }
    public StringBuilder sb_keys { get; set; }
    public StringBuilder sb_headings { get; set; }
	public IntexBuilder(PpcSite _cache)
	{

        HeadingIntextHelpList listHeading = new HeadingIntextHelpList();
        /*
        listHeading.Add(_cache.headings["2137"].ID);
        listHeading.Add(_cache.headings["2268"].ID);
        IEnumerable<KeywordsHeading> keywords = from x in _cache.KeywordsList
                            where x.IsPositiveKey &&
                                listHeading.Contains(x.HeadingId)
                            select x;
        */
        var query = from x in _cache.headings
                    where x.Value.IsIntext
                    select new { HeadingCode = x.Key, HeadingId = x.Value.ID };

        if (query.Count() == 0)
            return;
        
        foreach(var v in query)
            listHeading.Add(new HeadingIntextHelp(v.HeadingId, v.HeadingCode));

        for (int i = listHeading.Count - 1; i > -1; i--)
        {
//            if (!_cache.Flavors.ContainsKey(_cache.IntextFlavor[listHeading[i].HeadingCode]))
            if (!_cache.IntextFlavor.ContainsKey(listHeading[i].HeadingCode))
            {
                listHeading.RemoveAt(i);
                continue;
            }
            HeadingCode _hc = _cache.headings[listHeading[i].HeadingCode];
            
            eFlavour flavor = _cache.IntextFlavor[listHeading[i].HeadingCode];
            if (flavor == eFlavour.Phone_Did_Top && _hc.DidPhone == null)
                _hc.DidPhone = new HeadingDidPhone(listHeading[i].HeadingId, _cache.UrlWebReference);
            listHeading[i].svd = _cache.Flavors[flavor];
        }

        IEnumerable<KeywordsHeading> keywords = from x in _cache.KeywordsList
                                                where x.IsPositiveKey &&
                                                    //listHeading.con(x.HeadingId)
                                                    listHeading.ContainsKey(x.HeadingId)
                                                select x;

        List<string> list_Words = new List<string>();
        List<KeywordStringJs> list_keywords_string = new List<KeywordStringJs>();
        foreach (KeywordsHeading kvp in keywords)
        {
           
            KeywordStringJs KeywordString = new KeywordStringJs();
            List<KeyValuePair<string, bool>> list = new List<KeyValuePair<string, bool>>();
            
            //      list_keywords.Add(kvp.Key);
            string[] keys = kvp.Keyword.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in keys)
            {

                list.Add(new KeyValuePair<string, bool>(s, true));
                if (!list_Words.Contains(s))
                    list_Words.Add(s);
            }
            
            KeywordString.ListWord = list;
        //    KeywordString.Priority = kvp.Priority;
            KeywordString.HeadingId = kvp.HeadingId;
            list_keywords_string.Add(KeywordString);
        }

        list_Words.Sort(SalesUtility.CompareJavascriptString);
        StringBuilder _sb_vals = new StringBuilder();
        StringBuilder _sb_keys = new StringBuilder();
        _sb_vals.Append("[");
        for (int i = 0; i < list_Words.Count; i++)
        {
            _sb_vals.Append("'" + list_Words[i] + "',");
        }
        if (_sb_vals.Length > 1)
            _sb_vals.Remove(_sb_vals.Length - 1, 1);
        _sb_vals.Append("]");

        _sb_keys.Append("[");
        //{heading, words[valsIndex1, valsIndex2,...]}       
        foreach (KeywordStringJs ksj in list_keywords_string)
        {
            _sb_keys.Append("{heading:" + listHeading.FindIndex(X => X.HeadingId == ksj.HeadingId) + ",words:[");
            foreach (KeyValuePair<string, bool> kvp in ksj.ListWord)
            {
                int num = list_Words.FindIndex(x => x.Equals(kvp.Key));
                _sb_keys.Append(num.ToString() + ",");
            }
            _sb_keys.Remove(_sb_keys.Length - 1, 1);
            _sb_keys.Append("]},");
        }
        _sb_keys.Remove(_sb_keys.Length - 1, 1);
        _sb_keys.Append("]");

        StringBuilder _sb_headings = new StringBuilder();
        _sb_headings.Append("[");
        foreach (HeadingIntextHelp hih in listHeading)
            _sb_headings.Append("{code:'" + hih.HeadingCode + "',width:" + hih.svd.width + ",height:" + (hih.svd.height + EXTRA_HEIGHT) + "},");
        _sb_headings.Remove(_sb_headings.Length - 1, 1);
        _sb_headings.Append("]");

        sb_headings = _sb_headings;
        sb_vals = _sb_vals;
        sb_keys = _sb_keys;
	}
    public string GetIntexJs(PpcSite _cache)
    {
        if (sb_headings == null || sb_vals == null || sb_keys == null)
            return string.Empty;
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\intext1.4.js";
        string line;
        StringBuilder sbFile = new StringBuilder();

        if (File.Exists(path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    sbFile.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        sbFile.Replace("{vals}", sb_vals.ToString());
        sbFile.Replace("{keys}", sb_keys.ToString());
        sbFile.Replace("{headings}", sb_headings.ToString());
        sbFile.Replace("{ExtraHeight}", (EXTRA_HEIGHT + 5).ToString());
        if (_cache.IsLocalMachine)
            sbFile.Replace("https:", "http:");
        GoogleClosure gc = new GoogleClosure();
        return _cache.IsLocalMachine ? sbFile.ToString() : gc.Compress(sbFile.ToString(), false, _cache);
        
    }
}
public class HeadingIntextHelp
{
    public string HeadingCode { get; set; }
    public Guid HeadingId { get; set; }
    public SliderViewData svd { get; set; }
    public HeadingIntextHelp(Guid ID, string HeadingCode)
    {
        this.HeadingId = ID;
        this.HeadingCode = HeadingCode;
    }
}
public class HeadingIntextHelpList : List<HeadingIntextHelp>
{
    public bool ContainsKey(Guid ID)
    {
        foreach(HeadingIntextHelp hih in this)
        {
            if (hih.HeadingId == ID)
                return true;
        }
        return false;
    }
}
