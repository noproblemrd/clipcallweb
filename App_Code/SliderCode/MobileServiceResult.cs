﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MobileServiceResult
/// </summary>
public class MobileServiceResult
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string Tel { get; set; }
	public MobileServiceResult()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public MobileServiceResult(string title, string description, string tel)
    {
        this.Title = title;
        this.Description = description;
        this.Tel = tel;
    }
}