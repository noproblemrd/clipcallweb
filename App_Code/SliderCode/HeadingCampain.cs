﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HeadingCampain
/// </summary>
[Serializable()]
public class HeadingCampain : HeadingCode
{
  //  Enum eCampaignMode { FormCampaign, Campaign, 
    public string CampainTitle { get; private set; }
    public int BannerHeight { get; private set; }
    public int BannerWidth { get; private set; }
    public string BannerHtml { get; private set; }
    public string CampainDestination { get; private set; }
    public bool ShowBanner { get; private set; }
    public HeadingCampain(HeadingCode hc, string CampainTitle, int BannerHeight, int BannerWidth, string BannerHtml, string CampainDestination, 
        bool _Slider_campaign):
        base(hc.heading, hc.code, hc.Title, hc.SingularName, hc.WaterMark, hc.image, hc.SpeakToButtonName, hc.Comment, hc.ID,hc.Slogan,
        hc.MobileDeviceTitle, hc.MobileDeviceDescription, hc.IsIntext, hc.headingRelated, hc.IncludeGoogleAddon, hc.DidPhone)
    {
        this.CampainTitle = CampainTitle;
        this.BannerHeight = BannerHeight;
        this.BannerWidth = BannerWidth;
        this.BannerHtml = BannerHtml;
        this.CampainDestination = CampainDestination;
        this.ShowBanner = _Slider_campaign;
	}
    public HeadingCampain(HeadingCode hc) :
        base(hc.heading, hc.code, hc.Title, hc.SingularName, hc.WaterMark, hc.image, hc.SpeakToButtonName, hc.Comment, hc.ID,
        hc.Slogan, hc.MobileDeviceTitle, hc.MobileDeviceDescription, hc.IsIntext, hc.headingRelated, hc.IncludeGoogleAddon, hc.DidPhone)
    {
        
    }
}