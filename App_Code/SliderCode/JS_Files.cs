﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for JS_Files
/// </summary>
public class JS_Files
{
    public string Addon { get; set; }
    public string poc { get; set; }
    public string AddonGoogle { get; set; }
    public string Inj { get; set; }
 //   public string Conduit_logic { get; set; }
	public JS_Files()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public JS_Files(string Addon, string poc, string AddonGoogle, string Inj)
    {
        this.Addon = Addon;
        this.poc = poc;
        this.AddonGoogle = AddonGoogle;
        this.Inj = Inj;
 //       this.Conduit_logic = Conduit_logic;
    }
}