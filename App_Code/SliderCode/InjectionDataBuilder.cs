﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InjectionDataBuilder
/// </summary>
public class InjectionDataBuilder
{
    public int Index { get; set; }
    public int DayCounter { get; set; }
    public int HitCounter { get; set; }
	public InjectionDataBuilder()
	{

	}
    public InjectionDataBuilder(int _Index, int _DayCounter, int _HitCounter)
    {
        this.Index = _Index;
        this.DayCounter = _DayCounter;
        this.HitCounter = _HitCounter;
    }
}