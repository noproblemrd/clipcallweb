﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for QuestionHeading
/// </summary>
[Serializable()]
public class QuestionHeading : HeadingCode
{
    const string BING = "bing";
    const string YAHOO = "yahoo";
    const string BING_DOMAIN = "www.bing.com";
    const string DEFAULT_KEY = "q";
    
    const string YAHOO_DOMAIN = ".yahoo.com";
/*
    const string CONDUIT_DOMAIN = "search.conduit.com";
    const string LOCALHOST = "localhost";
     * */
    const int MaxMainTitleStructure = 47;
   // const string StartMainTitleStructure = "Answers for: ";
    public QuestionHeading(string heading, string code, string title, string SingularName, string WaterMark, string image, string SpeakToButtonName,
        string comment, Guid id, string slogan, string MobileTitle, string MobileDescription, bool IncludeGoogleAddon, bool intext, HeadingRelated[] headingRelated) :
        base(heading, code, title, SingularName, WaterMark, image, SpeakToButtonName, comment, id, slogan, MobileTitle, MobileDescription, intext, headingRelated, IncludeGoogleAddon)

	{
		
	}
    /*
    public override string GetTitle(SliderData sd)
    {
        bool IsQuestion;
        return GetTitle(sd, out IsQuestion);
    }
     * */
    public string GetTitle(SliderData sd, out bool IsQuestion)
    {
        IsQuestion = false;        
        string result = null;
        /*
        if (sd.ClientDomain == BING_DOMAIN)
            result = SplitTitle(sd.title, BING);
        
    else if (sd.ClientDomain.EndsWith(YAHOO_DOMAIN))
        result = SplitTitle(sd.title, YAHOO);
    else if (sd.ClientDomain.EndsWith(CONDUIT_DOMAIN))
        result = GetQQuery(sd.Url);
    else if(sd.ClientDomain == LOCALHOST)
        result = GetQQuery(sd.Url);
         * */
        if (sd.ClientDomain.EndsWith(YAHOO_DOMAIN))
            result = GetQQuery(sd.Url, "p");
        else
            result = GetQQuery(sd.Url);
        if (string.IsNullOrWhiteSpace(result))
            return GetTitle(sd);              
        IsQuestion = true;
        return result;
    }
    public string GetTitleForMainTitle(SliderData sd)
    {
        /*
        string _result = GetQQuery(sd.Url);
        if (_result.Length > MaxMainTitleStructure)
            return GetQQuery(sd.Url) + "...";
        return _result;
         * */
      //  return GetQQuery(sd.Url);
        bool IsQuestion;
        string result = GetTitle(sd, out IsQuestion);
        if (!IsQuestion)
            return result;
        if (result.Length > MaxMainTitleStructure)
            result = result.Substring(0, MaxMainTitleStructure - 3) + "...";
        return result;
    }
    string SplitTitle(string _title, string _split)
    {
        if (string.IsNullOrWhiteSpace(_title))
            return null;
        string[] strs = _title.Split(new string[] { _split }, StringSplitOptions.RemoveEmptyEntries);
        return strs[0].Trim();
    }
    string GetQQuery(string _url)
    {
        return GetQQuery(_url, DEFAULT_KEY);
    }
    string GetQQuery(string _url, string key)
    {
        if (string.IsNullOrWhiteSpace(_url))
            return null;
        string[] urls = _url.Split(new char[] { '?' }, 2);
        if (urls.Length != 2)
            return null;
        string[] queries = urls[1].Split('&');
        foreach (string query in queries)
        {
            string[] key_val = query.Split(new char[] { '=' }, 2);
            if (key_val.Length != 2)
                continue;
            if (key_val[0] == key)
                return HttpUtility.UrlDecode(key_val[1]);
        }
        return null;
    }
  
   
}