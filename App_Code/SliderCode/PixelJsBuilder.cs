﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for PixelJsBuilder
/// </summary>
public class PixelJsBuilder
{
	public PixelJsBuilder()
	{
        
	}
    public string GetPixelJs()
    {
        string line;
        StringBuilder PixelSB = new StringBuilder();
        string InjectionPath = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\_pix1.0.js";

        if (File.Exists(InjectionPath))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(InjectionPath);
                while ((line = file.ReadLine()) != null)
                {
                    PixelSB.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        return PixelSB.ToString();
    }
}