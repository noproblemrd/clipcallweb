﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GeoLocation
/// </summary>
public class GeoLocation
{
    public string Country { get; set; }
    public string State { get; set; }
    public string City { get; set; }
    public string ZipCode { get; set; }
	public GeoLocation()
	{
		
	}
    public GeoLocation(string country, string state, string city, string ZipCode)
    {
        this.Country = country;
        this.State = state;
        this.City = city;
        this.ZipCode = ZipCode;
    }
}