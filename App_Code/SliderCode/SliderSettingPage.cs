﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SliderSettingPage
/// </summary>
public class SliderSettingPage : Page
{
    protected SliderData Slider_Data { get; set; }
    protected const string SLIDER_CLOSED = "_NP_Hide";
    protected const string POPUP_CLOSED = "_NP_Hide_PopUp";
    protected const string INTEXT_CLOSED = "_NP_Hide_Intext";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //    Slider_Data = SliderData.GetSliderData(Context);

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");

        MongoDB.Bson.ObjectId ExposureId;
        if (MongoDB.Bson.ObjectId.TryParse(Request.QueryString["ExposureId"], out ExposureId))
        {
            Slider_Data = SliderData.GetSliderData(ExposureId, Context);
        }
        if (Slider_Data == null)
            Response.Redirect(ResolveUrl("~/main.aspx"));


    }
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        HtmlLink _link;
        /*
        HtmlLink _lin2 = new HtmlLink();
        _link.Attributes.Add("rel", "Shortcut Icon");
        _link.Attributes.Add("type", "image/x-icon");
        _lin2.Href = ResolveUrl("~/npsb/images/Shopwithboost.ico");
        Header.Controls.AddAt(0, _lin2);
         * */

        switch (Slider_Data.Type)
        {
            case (eExposureType.Aducky):
                _link = new HtmlLink();
                _link.Attributes.Add("rel", "Shortcut Icon");
                _link.Attributes.Add("type", "image/x-icon");
                _link.Href = "favicon.ico";
                Header.Controls.AddAt(0, _link);
                break;
            case (eExposureType.SpeedOptimizer_Guru):
                _link = new HtmlLink();
                _link.Attributes.Add("rel", "Shortcut Icon");
                _link.Attributes.Add("type", "image/x-icon");
                _link.Href = ResolveUrl("~/npsb/images/SOG-logo.ico");
                Header.Controls.AddAt(0, _link);
                break;
            case (eExposureType.Shopwithboost):
                _link = new HtmlLink();
                _link.Attributes.Add("rel", "Shortcut Icon");
                _link.Attributes.Add("type", "image/x-icon");
                _link.Href = ResolveUrl("~/npsb/images/Shopwithboost.ico");
                Header.Controls.AddAt(0, _link);
                break;
            default:
                switch (Slider_Data.DomainName)
                {
                    case (eDomainName.pastaleads):
                        _link = new HtmlLink();
                        _link.Attributes.Add("rel", "Shortcut Icon");
                        _link.Attributes.Add("type", "image/x-icon");
                        _link.Href = ResolveUrl("~/npsb/images/PastaLeads.png");
                        Header.Controls.AddAt(0, _link);
                        break;
                    case (eDomainName.donutleads):
                        _link = new HtmlLink();
                        _link.Attributes.Add("rel", "Shortcut Icon");
                        _link.Attributes.Add("type", "image/x-icon");
                        _link.Href = ResolveUrl("~/npsb/images/Donutleads.png");
                        Header.Controls.AddAt(0, _link);
                        break;
                    case (eDomainName.bbqleads):
                        _link = new HtmlLink();
                        _link.Attributes.Add("rel", "Shortcut Icon");
                        _link.Attributes.Add("type", "image/x-icon");
                        _link.Href = ResolveUrl("~/npsb/images/icon_bbq32.png");
                        Header.Controls.Add(_link);
                        break;
                    case (eDomainName.sushileads):
                        _link = new HtmlLink();
                        _link.Attributes.Add("rel", "Shortcut Icon");
                        _link.Attributes.Add("type", "image/x-icon");
                        _link.Href = ResolveUrl("~/npsb/images/sushileads.png");
                        Header.Controls.Add(_link);
                        break;
                }
                break;

        }

    }
}