﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for KeywordIndexJs
/// </summary>
public class KeywordIndexJs
{
    public List<KeyValuePair<int, bool>> ListWord { get; set; }
    public int Priority { get; set; }
    public KeywordIndexJs(List<KeyValuePair<int, bool>> _ListWord, int _Priority)
	{
        this.ListWord = _ListWord;
        this.Priority = _Priority;
	}
    public KeywordIndexJs() { }

}
public class KeywordStringJs
{
    public List<KeyValuePair<string, bool>> ListWord { get; set; }
    public int Priority { get; set; }
    public Guid HeadingId { get; set; }
    public KeywordStringJs(List<KeyValuePair<string, bool>> _ListWord, int _Priority)
    {
        this.ListWord = _ListWord;
        this.Priority = _Priority;
    }
    public KeywordStringJs() { }

}
public class KeywordIndexList
{
    public List<int> ListWord { get; set; }
    public int Priority { get; set; }
    public KeywordIndexList(List<int> _ListWord, int _Priority)
    {
        this.ListWord = _ListWord;
        this.Priority = _Priority;
    }
    public KeywordIndexList() { }

}