﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Xml.Linq;
//using Microsoft.Ajax.Utilities;

/// <summary>
/// Summary description for InjectionBuilder
/// </summary>
public class InjectionBuilder
{
   
	public InjectionBuilder()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    static Guid GetSecondPriorityInjection()//firstofferzShopping
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        string inj = xdoc.Element("Sites").Element("SitePPC").Element("Injections").Element("InjectionfirstofferzShopping").Value;
        return Guid.Parse(inj);
    }
    public static string Build(PpcSite _cache)
    {
        string line;
        StringBuilder InjectionSB = new StringBuilder();
        string InjectionPath = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\_inj2.5.js";

        if (File.Exists(InjectionPath))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(InjectionPath);
                while ((line = file.ReadLine()) != null)
                {
                    InjectionSB.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_cache.UrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionData resultInjectionData = null;
        try
        {
            resultInjectionData = _site.GetInjectionData();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return string.Empty;
        }
        if (resultInjectionData.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;
        WebReferenceSite.ResultOfListOfInjectionCampaign resultInstallationCampaign = null;
        try
        {
            resultInstallationCampaign = _site.GetInjectionCampaign();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return string.Empty;
        }
        if (resultInstallationCampaign.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;
        Guid firstofferzShoppingId = GetSecondPriorityInjection();
        int SecondPriorityIndex = -1;
        StringBuilder InjectioCollectionSB = new StringBuilder();
        StringBuilder CampaignInjectionSB = new StringBuilder();
        Dictionary<Guid, int> IndexCampaign = new Dictionary<Guid, int>();
        InjectioCollectionSB.Append("[");
        //[{Name: 'name', FuncUrl: function(){ return url; }, FuncBefore: function() {}}]
        //for (int i = 0; i < resultInjectionData.Value.Length; i++)
        int _index = 0;
        foreach(WebReferenceSite.InjectionData InjData in resultInjectionData.Value)
        {
            var IsFind = false;//to prevent insert to js unused injections            
            if (firstofferzShoppingId == InjData.InjectionId)
            {
                IsFind = true;
                SecondPriorityIndex = _index;
            }
            else
            {
                foreach (WebReferenceSite.InjectionCampaign ic in resultInstallationCampaign.Value)
                {
                    if (ic.InjectionId == InjData.InjectionId)
                    {
                        IsFind = true;
                        break;
                    }
                }
            }            
            if (!IsFind)
                continue;

            IndexCampaign.Add(InjData.InjectionId, _index);
            string ScriptBefore, ScriptElementId;
            if (string.IsNullOrEmpty(InjData.ScriptBefore))
                ScriptBefore = "null";
            else
                ScriptBefore = "function() {" + InjData.ScriptBefore + "}";
            ScriptElementId = string.IsNullOrEmpty(InjData.ScriptElementId) ? "null" : "'" + InjData.ScriptElementId + "'";

            string ScriptUrl = "function() {" + InjData.ScriptUrl + "}";
            InjectioCollectionSB.Append("{id: '" + InjData.InjectionId.ToString() + "',FuncUrl: " + ScriptUrl +
                ",FuncBefore: " + ScriptBefore + ",BlockWithUs: " + InjData.BlockWithUs.ToString().ToLower() +
                ",ScriptElementId: " + ScriptElementId + "},");
            _index++;
        }
        InjectioCollectionSB.Remove(InjectioCollectionSB.Length - 1, 1);
        InjectioCollectionSB.Append("]");
        //[{Origin: OriginId, [InjIndex, ...]}]
        CampaignInjectionSB.Append("[");
        Dictionary<Guid, List<InjectionDataBuilder>> Campaigns = new Dictionary<Guid, List<InjectionDataBuilder>>();
        foreach (WebReferenceSite.InjectionCampaign ic in resultInstallationCampaign.Value)
        {
            if (!IndexCampaign.ContainsKey(ic.InjectionId))
                continue;
            if (!Campaigns.ContainsKey(ic.OriginId))
                Campaigns.Add(ic.OriginId, new List<InjectionDataBuilder>());
            Campaigns[ic.OriginId].Add(new InjectionDataBuilder(IndexCampaign[ic.InjectionId], ic.DayCounter, ic.HitCounter));
        }
        foreach (KeyValuePair<Guid, List<InjectionDataBuilder>> kvp in Campaigns)
        {
            CampaignInjectionSB.Append("{Origin: '" + kvp.Key.ToString().ToLower() + "', InjOriginData: [");
            foreach (InjectionDataBuilder idb in kvp.Value)
                CampaignInjectionSB.Append("{Index:" + idb.Index + ",HitCounter:" + idb.HitCounter + ",DayCounter:" + idb.DayCounter + "},");
            CampaignInjectionSB.Remove(CampaignInjectionSB.Length - 1, 1);
            CampaignInjectionSB.Append("]},");
        }
        CampaignInjectionSB.Remove(CampaignInjectionSB.Length - 1, 1);
        CampaignInjectionSB.Append("]");

      //  string _vp = HttpRuntime.AppDomainAppVirtualPath == "/" ? string.Empty : HttpRuntime.AppDomainAppVirtualPath;

        //InjectionSB.Replace("{VirtualPath}", _vp);
        InjectionSB.Replace("{IsLocal}", _cache.IsLocalMachine.ToString().ToLower());
        InjectionSB.Replace("{InjectionCollection}", InjectioCollectionSB.ToString());
        InjectionSB.Replace("{CampaignInjection}", CampaignInjectionSB.ToString());
        InjectionSB.Replace("{SecondPriorityIndex}", SecondPriorityIndex.ToString());
        /*
        Minifier mf = new Minifier();
        string script = null;
        if (!_cache.IsLocalMachine)
        {
            script = mf.MinifyJavaScript(InjectionSB.ToString());
            if (mf.ErrorList.Count > 0)
            {
                List<string> list_error = new List<string>();

                foreach (ContextError ce in mf.ErrorList)                   
                    list_error.Add(ce.Message);
                _cache.GetCacheLog.WriteCacheLog(new Exception("Minifier injection"), list_error.ToArray());
            }
        }
        else
            script = InjectionSB.ToString();
         * */
        
        GoogleClosure gc = new GoogleClosure();
        string script = (!_cache.IsLocalMachine) ? gc.Compress(InjectionSB.ToString(), false, _cache) : InjectionSB.ToString();
        return script; // InjJS;
    }
}