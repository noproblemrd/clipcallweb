﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for KeywordCalc
/// </summary>
public enum eKeywordLevel { URL, TITLE, META, QUERY, QUERY_STRING, H1 }
public class KeywordCalc
{
    double FactorUTM = 0.3;
    double FactorWords { get { return 1.0 - FactorUTM; } }

    double UrlGrade = 1.7;
    double TitleGrade = 0.8;
    double MetaGrade = 0.5;
    double QueryGrade = 10;
    double QueryString = 0.01;
    double H1Grade = 0.2;

   // Dictionary<eKeywordLevel, List<string>> keywords;
    Dictionary<string, List<eKeywordLevel>> keywords;
    string domain;
    double KeywordGrade(string keyword)
    {
        int num = keyword.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length;
        /*
        if (ekv == eKeywordLevel.QUERY_STRING)
            return 0.1 * (double)num;
         * */
        switch (num)
        {
            case (0): 
            case (1):
            case (2): return (double)num;
            default: return 3;
        }
    }
	public KeywordCalc(string QueryKeyword, string _domain)
	{
        this.domain = _domain;
        keywords = new Dictionary<string, List<eKeywordLevel>>();
        if (!QueryKeyword.Contains(':'))
        {
            List<eKeywordLevel> list = new List<eKeywordLevel>();
            list.Add(eKeywordLevel.QUERY);
            QueryKeyword = SalesUtility.GetCleanKeyword(QueryKeyword);
            keywords.Add(QueryKeyword, list);
            return;
        }
        string[] levels = QueryKeyword.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        foreach (string _level in levels)
        {
            string[] level_keys = _level.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (level_keys.Length != 2)
                continue;
            eKeywordLevel ekl;
            switch (level_keys[0])
            {
                case("t"):
                    ekl = eKeywordLevel.TITLE;
                    break;
                case("u"):
                    ekl = eKeywordLevel.URL;
                    break;
                case("h1"):
                    ekl = eKeywordLevel.H1;
                    break;
                case("uqs"):
                    ekl = eKeywordLevel.QUERY_STRING;
                    break;
                default:
                    ekl = eKeywordLevel.META;
                    break;
            }
        //    eKeywordLevel ekl = ((level_keys[0] == "u") ? eKeywordLevel.URL : ((level_keys[0] == "t") ? eKeywordLevel.TITLE : ((level_keys[0] == "uqs") ? eKeywordLevel.QUERY_STRING : eKeywordLevel.META)));
            string[] _keywords = level_keys[1].Split(',');            
            foreach (string str in _keywords)
            {
                string _str = HttpUtility.UrlDecode(str);
                if (!keywords.ContainsKey(_str))
                    keywords.Add(_str, new List<eKeywordLevel>());
                if (!keywords[_str].Contains(ekl))
                    keywords[_str].Add(ekl);
            }
      
        }
	}
    public string GetKeywordFirstRate(SliderData sd)
    {       
        PpcSite _ppc = PpcSite.GetCurrent();
        
        double rate = 0;
        string KeywordWin = "";
        if (!string.IsNullOrEmpty(this.domain) && _ppc.IsInBlackDomainByOrigin(sd.OriginId, this.domain))
            return string.Empty;
        foreach (KeyValuePair<string, List<eKeywordLevel>> kvp in keywords)
        {           
            if (!string.IsNullOrEmpty(this.domain) && _ppc.IsInBlackList(kvp.Key, this.domain))//Dynamic black list
                continue;             
            
            if (!_ppc.IsHeadingByOrigin(kvp.Key, sd.OriginId))
                continue;
            if (!_ppc.IsGoodLocationHeadingByKeyword(sd, kvp.Key))
                continue;            
            double KeyRate = 0;
            bool OnlyQueryString = false;
            foreach (eKeywordLevel ekv in kvp.Value)
            {
                KeyRate += (GetLevelRate(ekv) * FactorUTM);
                if (ekv == eKeywordLevel.QUERY_STRING)
                    OnlyQueryString = true;
            }
            if(kvp.Value.Count > 1 || !OnlyQueryString)
                KeyRate += (KeywordGrade(kvp.Key) * FactorWords);
            if (KeyRate > rate)
            {                
                rate = KeyRate;
                KeywordWin = kvp.Key;
            }
        }
        return KeywordWin;
    }
    double GetLevelRate(eKeywordLevel ekv)
    {
        switch (ekv)
        {
            case (eKeywordLevel.META): return MetaGrade;
            case (eKeywordLevel.QUERY): return QueryGrade;
            case (eKeywordLevel.TITLE): return TitleGrade;
            case (eKeywordLevel.URL): return UrlGrade;
            case (eKeywordLevel.QUERY_STRING): return QueryString;
            case (eKeywordLevel.H1): return H1Grade;
        }
        return 0;
    }
}