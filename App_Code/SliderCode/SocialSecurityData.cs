﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SocialSecurityData
/// </summary>
public class SocialSecurityData
{
    const string YES = "yes";
    public bool IsMissWork { get; set; }
    public bool IsReciveSocialSecurityBenefit { get; set; }
    public bool IsHaveAttorney { get; set; }
    public bool IsWorking { get; set; }
    public bool IsUseMedication { get; set; }
    public bool CurrentlySeeingDoctor { get; set; }
    public bool IsWork_5_0f_10 { get; set; }
    /*
    public SocialSecurityData(string _IsMissWork, string _IsReciveSocialSecurityBenefit, string _IsHaveAttorney,
                                string _IsWorking, string _IsUseMedication)
	{
        this.IsMissWork = _IsMissWork.ToLower() == YES;
        this.IsReciveSocialSecurityBenefit = _IsReciveSocialSecurityBenefit.ToLower() == YES;
        this.IsHaveAttorney = _IsHaveAttorney.ToLower() == YES;
        this.IsWorking = _IsWorking.ToLower() == YES;
        this.IsUseMedication = _IsUseMedication.ToLower() == YES;
	}
     * */
    public SocialSecurityData(string data)
    {
        string[] strs = data.Split(new string[] {";;;"}, StringSplitOptions.RemoveEmptyEntries);
        SetSocialSecurityData(strs[0], strs[1], strs[2], strs[3], strs[4], strs[5], strs[6]);
        
    }
    private void SetSocialSecurityData(string _IsMissWork, string _IsReciveSocialSecurityBenefit, string _IsHaveAttorney,
                               string _IsWorking, string _IsUseMedication, string _CurrentlySeeingDoctor, string _IsWork_5_0f_10)
    {
        this.IsMissWork = _IsMissWork.ToLower() == YES;
        this.IsReciveSocialSecurityBenefit = _IsReciveSocialSecurityBenefit.ToLower() == YES;
        this.IsHaveAttorney = _IsHaveAttorney.ToLower() == YES;
        this.IsWorking = _IsWorking.ToLower() == YES;
        this.IsUseMedication = _IsUseMedication.ToLower() == YES;
        this.CurrentlySeeingDoctor = _CurrentlySeeingDoctor.ToLower() == YES;
        this.IsWork_5_0f_10 = _IsWork_5_0f_10.ToLower() == YES;
    }

    public WebReferenceCustomer.ServiceRequestSocialSecurityDisabilityData GetCrmData
    {
        get
        {
            WebReferenceCustomer.ServiceRequestSocialSecurityDisabilityData data = new WebReferenceCustomer.ServiceRequestSocialSecurityDisabilityData();
            data.AbleToWork = IsWorking;
            data.AlreadyReceiveBenefits = IsReciveSocialSecurityBenefit;
            data.HaveAttorney = IsHaveAttorney;
            data.MissWork = IsMissWork;
            data.PrescribedMedication = IsUseMedication;
            data.HasDoctor = CurrentlySeeingDoctor;
            data.Work5Of10 = IsWork_5_0f_10;
            return data;
        }
    }
}