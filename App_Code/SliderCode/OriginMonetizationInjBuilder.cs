﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for OriginMonetizationInjBuilder
/// </summary>
public class OriginMonetizationInjBuilder
{
    PpcSite _cache;
    public OriginMonetizationInjBuilder(PpcSite _cache)
	{
		this._cache = _cache;
	}
    public string GetInjMonJs()
    {
        List<WebReferenceSite.InjectionCampaignParasite> listICP = GetInjectionParasiteCampaign();
        if (listICP == null)
            return string.Empty;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_cache.UrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionData resultInjectionData = null;
        try
        {
            resultInjectionData = _site.GetInjectionData();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return string.Empty;
        }
        if (resultInjectionData.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;
        Dictionary<Guid, List<int>> SpecialInjectionIndex = new Dictionary<Guid, List<int>>();

        StringBuilder InjectioCollectionSB = new StringBuilder();
        StringBuilder CampaignInjectionSB = new StringBuilder();
        InjectioCollectionSB.Append("[");
        //[{Name: 'name', FuncUrl: function(){ return url; }, FuncBefore: function() {}}]
        SpecialInjectionIndex.Add(Guid.Empty, new List<int>());
        int _index = 0;
        foreach (WebReferenceSite.InjectionData InjData in resultInjectionData.Value)
        {
            var IsFind = false;//to prevent insert to js unused injections
            foreach (WebReferenceSite.InjectionCampaignParasite icp in listICP)
            {
                if (icp.InjectionId == InjData.InjectionId)
                {
                    IsFind = true;
                    if (icp.IsDefault || icp.OriginId == Guid.Empty)
                        SpecialInjectionIndex[Guid.Empty].Add(_index);
                    else
                    {
                        if (!SpecialInjectionIndex.ContainsKey(icp.OriginId))
                            SpecialInjectionIndex.Add(icp.OriginId, new List<int>() { _index });
                        else
                            SpecialInjectionIndex[icp.OriginId].Add(_index);
                    }

                }
            }
            if (!IsFind)
                continue;

            InjectioCollectionSB.Append(GetJsInjectionData(InjData));
       //     SpecialInjectionIndex[Guid.Empty].Add(_index++);
            _index++;
        }
        /*
       

       
        foreach (WebReferenceSite.InjectionData InjData in resultInjectionData.Value)
        {
            var IsFind = false;//to prevent insert to js unused injections
            foreach (Guid InjectionId in injections)
            {
                if (InjectionId == InjData.InjectionId)
                {
                    IsFind = true;
                    break;
                }
            }
            if (!IsFind)
                continue;

            InjectioCollectionSB.Append(GetJsInjectionData(InjData));
            SpecialInjectionIndex[Guid.Empty].Add(_index++);
        }
        List<SpecialInjectionData> specialInjectionList = new List<SpecialInjectionData>();
        foreach (XElement xelem in xdoc.Element("Sites").Element("SitePPC").Element("Injections").Element("SpecialInjection").Elements("Origins"))
        {
            Guid oid = Guid.Parse(xelem.Element("origin").Value);
            Guid iid = Guid.Parse(xelem.Element("injection").Value);
            specialInjectionList.Add(new SpecialInjectionData(oid, iid));
        }
        foreach(SpecialInjectionData sid in specialInjectionList)
        {
            SpecialInjectionIndex.Add(sid.originId, new List<int>());
            foreach (WebReferenceSite.InjectionData InjData in resultInjectionData.Value)
            {
                if (sid.injectionId == InjData.InjectionId)
                {
                    InjectioCollectionSB.Append(GetJsInjectionData(InjData));
                    SpecialInjectionIndex[sid.originId].Add(_index++);
                }
            }            
        }
        */
        if (InjectioCollectionSB.Length > 1)
            InjectioCollectionSB.Remove(InjectioCollectionSB.Length - 1, 1);
        InjectioCollectionSB.Append("]");

        StringBuilder SpecialInjectionIndexSB = new StringBuilder("[");
        foreach(KeyValuePair<Guid, List<int>> kvp in SpecialInjectionIndex)
        {
            if (kvp.Value.Count == 0)
                continue;
            string originid = kvp.Key == Guid.Empty ? "" : kvp.Key.ToString();
            SpecialInjectionIndexSB.Append("{originid:'" + originid + "',injections:[");
            foreach (int num in kvp.Value)
                SpecialInjectionIndexSB.Append(num + ",");
            SpecialInjectionIndexSB.Remove(SpecialInjectionIndexSB.Length - 1, 1);
            SpecialInjectionIndexSB.Append("]},");
        }
        if (SpecialInjectionIndexSB.Length > 1)
            SpecialInjectionIndexSB.Remove(SpecialInjectionIndexSB.Length - 1, 1);
        SpecialInjectionIndexSB.Append("]");

        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\_injmon1.1.js";
        string line;
        StringBuilder sbFile = new StringBuilder();
        if (File.Exists(path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    sbFile.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return string.Empty;
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        sbFile.Replace("{HtmlPath}", "npsb/direct/data/mon.html");
        sbFile.Replace("{IsLocal}", _cache.IsLocalMachine.ToString().ToLower());
        sbFile.Replace("{InjectionCollection}", InjectioCollectionSB.ToString());
        sbFile.Replace("{SpecialInjection}", SpecialInjectionIndexSB.ToString());
        if (_cache.IsLocalMachine)
            sbFile.Replace("https:", "http:");
        GoogleClosure gc = new GoogleClosure();
        string script = (!_cache.IsLocalMachine) ? gc.Compress(sbFile.ToString(), false, _cache) : sbFile.ToString();
        return script;
    }
    private List<WebReferenceSite.InjectionCampaignParasite> GetInjectionParasiteCampaign()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_cache.UrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionCampaignParasite result = null;
        try
        {
            result = _site.GetInjectionParasiteCampaign();
        }
        catch (Exception exc)
        {
            _cache.GetCacheLog.WriteCacheLog(exc);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return null;
        //  Dictionary<Guid, int> dic = new Dictionary<Guid, int>();
        return result.Value.ToList();
    }
    public string GetMonHtmJs()
    {
        /*
        string _xml = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(_xml);
        int WaitHit = int.Parse(xdoc.Element("Sites").Element("SitePPC").Element("Injections").Element("OriginMonetizationInjection").Element("WaitHits").Value);
        Dictionary<Guid, int> dic = new Dictionary<Guid, int>();
        dic.Add(Guid.Empty, WaitHit);

        foreach(XElement xelem in xdoc.Element("Sites").Element("SitePPC").Element("Injections").Element("SpecialInjection").Elements("Origins"))
        {
            Guid originid = Guid.Parse(xelem.Element("origin").Value);
            int _wait = int.Parse(xelem.Element("WaitHits").Value);
            dic.Add(originid, _wait);
        }
         * */

        List<WebReferenceSite.InjectionCampaignParasite> listICP = GetInjectionParasiteCampaign();
        if (listICP == null)
            return string.Empty;
        
        StringBuilder WaitHitsSB = new StringBuilder("[");
        /*
        foreach(KeyValuePair<Guid, int> kvp in dic)
        {
            string origin = kvp.Key == Guid.Empty ? "" : kvp.Key.ToString();
            WaitHitsSB.Append("{originid:'" + origin + "',wh:" + kvp.Value + "},");
        }
         * */
        bool SetDefaultInjection = false;
        foreach(WebReferenceSite.InjectionCampaignParasite icp in listICP)
        {
            string origin;
            if(icp.IsDefault || icp.OriginId == Guid.Empty)
            {
                if (SetDefaultInjection)
                    continue;
                origin = "";
                SetDefaultInjection = true;
            }
            else
                origin = icp.OriginId.ToString();
            WaitHitsSB.Append("{originid:'" + origin + "',wh:" + icp.WaitHits + ",hi:" + icp.HourInterval + "},");
        }
        if(WaitHitsSB.Length > 1)
            WaitHitsSB.Remove(WaitHitsSB.Length - 1, 1);
        WaitHitsSB.Append("]");

        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\_monhtm1.4.js";
        string line;
        StringBuilder sbFile = new StringBuilder();
        if (File.Exists(path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    sbFile.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                _cache.GetCacheLog.WriteCacheLog(exc);
                return string.Empty;
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        sbFile.Replace("{InjectIn}", WaitHitsSB.ToString());//WaitHit.ToString());
     //   return sbFile.ToString();
        GoogleClosure gc = new GoogleClosure();
        string script = (!_cache.IsLocalMachine) ? gc.Compress(sbFile.ToString(), false, _cache) : sbFile.ToString();
     //   string script = gc.Compress(sbFile.ToString(), false, _cache);
        return script;
    }
    public string GetMonHtmlEventJs()
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\MonHtmlEvent.js";
        string line;
        StringBuilder sbFile = new StringBuilder();
        if (File.Exists(path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    sbFile.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                return string.Empty;
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }

        //   return sbFile.ToString();
   //     GoogleClosure gc = new GoogleClosure();
    //    string script = (!_cache.IsLocalMachine) ? gc.Compress(sbFile.ToString(), false, _cache) : sbFile.ToString();
        //   string script = gc.Compress(sbFile.ToString(), false, _cache);
        return sbFile.ToString();
    }
    private string GetJsInjectionData(WebReferenceSite.InjectionData InjData)
    {
        string ScriptBefore;
        if (string.IsNullOrEmpty(InjData.ScriptBefore))
            ScriptBefore = "null";
        else
            ScriptBefore = "function() {" + InjData.ScriptBefore + "}";
        string ScriptUrl = "function() {" + InjData.ScriptUrl + "}";
        return "{id: '" + InjData.InjectionId.ToString() + "',FuncUrl: " + ScriptUrl + ",FuncBefore: " + ScriptBefore + ",BlockWithUs: " + InjData.BlockWithUs.ToString().ToLower() + "},";
    }
}