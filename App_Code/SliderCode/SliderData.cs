﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Threading;
using System.Xml.Linq;



/// <summary>
/// Summary description for SliderData
/// </summary>
public enum eMessageToParent
{
    Title,
    Resize,
    RemoveSlider,
    ConduitWhatsThis,
    ResizeLight,
    IntextClick
}

public enum eFlavour
{
    
    Flavor_Regular = 1,
    Flavor_General = 2,
    Flavor_Attorney_2_Steps = 3,
    Flavor_IVR = 4,
    Flavor_IVR2 = 5,
    Flavor_General_Search = 6,
    NoProblemHomepage = 7,
    Flavor_2_Steps_A = 8,
    Flavor_2_Steps_B = 9,
    Flavor_2_Steps_C = 10,
    IVR_Hot_Line = 11,
    Flavor_Regular_LowDescFilter = 12,
    Flavor_Regular_Yahoo=13,
    Flavor_Regular_Yellowpages=14,
    Flavor_Regular_Bing=15,
    Flavor_Regular_Yellowbook=16,
    Flavor_Regular_Lawyers=17,
    Flavor_Regular_Automatic_ZIP = 18,
    Flavor_Regular_Name = 19,
    FlavorRegular0 = 20,
    Flavor_Regular_Half = 21,
    Flavor_Half_Controls_Left = 22,
    Flavor_Half_Controls_Right = 23,
    Flavor_Half_Controls_Top1 = 24,
    Flavor_Half_Controls_Top2 = 25,
    Flavor_Half_Controls_Top3 = 26,
    Flavor_Half_Controls_Top4 = 27,
    Flavor_Half_Controls_Top5 = 28,    
    Flavor_Half_Controls_Top_Narrow = 29,
    Flavor_Half_Controls_Top_Narrow_Num_Suppliers=30,
    Flavor_Half_Controls_Top_Narrow_PhoneOnly = 31,
    Flavor_Attorney_2_Steps_withEmail=32,
    Flavor_Half_Controls_Top_Mover = 33,
    Flavor_Half_Controls_Top_Accountant = 34,
    Flavor_Half_Controls_Top_Attorney = 35,
    Flavor_Half_Controls_Top_Electrician = 36,
    Flavor_Half_Controls_Top_Roofer = 37,
    Flavor_Half_Controls_Top_TileContractor = 38,
    Flavor_Half_Controls_Top_Carpenter = 39,
    Flavor_Half_Controls_Top_ComputerRepair = 40,
    Flavor_Half_Controls_Top_HandyMan = 41,
    Flavor_Half_Controls_Top_PayDay = 42,
    Flavor_Half_Controls_Top_CreditCounselor = 43,
    Flavor_Half_Controls_Top_Dentist = 44,
    Flavor_Half_Controls_Top_2 = 45,
    Flavor_Half_Controls_Top_3 = 46,
    Flavor_Half_Controls_Top_Cleaner= 47,
    Conduit_General_Search = 48,   
    Flavor_Half_Controls_Top_Hotel_Booking=49,
    Flavor_Half_Controls_Top_Flight_Expedia = 50,
    Flavor_Half_Controls_Top_Cruise_Expedia = 51,
    Flavor_Half_Controls_Top_Package_Expedia=52,
    Flavor_Half_Controls_Top_Activity_Expedia=53,
    Flavor_Half_Controls_Top_Food_Artizone = 54,
    Flavor_Half_Controls_Top_PersonalInjuryAttorney = 55,
    Flavor_Half_Controls_Top_GreenCard = 56,
    Flavor_Half_Controls_Top_Cj = 57,
    Flavor_Half_Controls_Top_BankraptcyAttorney = 58,
    Flavor_Half_Controls_Top_CriminalAttorney = 59,
    Flavor_Half_Controls_Top_DuiAttorney = 60,
    Flavor_Half_Controls_Top_DivorceAttorney = 61,
    Phone_Did_Top = 62,
    Phone_Did_Top_Dentist_A = 63,
    Phone_Did_Top_Dentist_B = 64,
    Flavor_Half_Controls_Top_LifeInsurance = 65,
    Flavor_Directory_Business=66,
    WelcomeScreen =67,
    Flavor_Half_Controls_Top_HomeImprovement = 68,
    Flavor_Attorney_2_Steps_withEmail_AutoCompleteKeywords=69,
    Flavor_Half_Controls_Top_Attorney_AllExpertises_1_Till_2_Steps = 70,
    Flavor_Top_Questions = 71,    
    Flavor_Half_Controls_Top_Attorney_AllExpertises_2_Till_3_Steps=72,
    Flavor_Half_Controls_Top_Locksmith=73,
    Flavor_Half_Controls_Top_Attorney_AllExpertises_2_Till_3_Steps_flickering=74,
    Flavor_Half_Controls_Top_PayDay_flickering = 75,
    Flavor_Attorney_2_Steps_withEmail_AutoCompleteKeywords_flickering = 76,
    Flavor_Half_Controls_Top_JustPhone=77,
    Flavor_Half_Controls_Top_JustPhoneWithForm = 78,
    Flavor_Half_Controls_Top_PopUp = 79,
    Flavor_Half_Controls_Top_HandyMan2 = 80,
    Flavor_Half_Controls_Top_Mover_flickering = 81,
    MobileDevice = 82,
    Phone_Did_Top_Dentist_1_800_Company = 83,
    Flavor_Half_Controls_Top_2_FormDid = 84,
    Flavor_Half_Controls_Top_PayDay_JustPhoneWithForm = 85,
    Flavor_Half_Controls_Top_JustPhoneWithForm_Disabled = 86,
    Flavor_Half_Controls_Top_PayDay_3_Steps=87,
    Phone_Did_Top_RoundSky_Company=88,
    Flavor_Half_Controls_Top_PayDay_RoundSky=89,
    Flavor_Half_Controls_Top_NewCar_Quotebound = 4000,
    Flavor_Half_Controls_Top_SsdAttorney = 90,
    Flavor_Half_Controls_Top_AutoLoanRndFrame = 91,
    Flavor_Half_Controls_Top_HomeImprovement_Steps = 92,
    Flavor_Half_Controls_Top_HomeSecurity = 93,
    Flavor_Half_Controls_Top_AutoInsurance = 94,
    Flavor_Half_Controls_Top_VehicleInsurance_Leadnomics = 95,
    Flavor_Half_Controls_Top_HealthInsurance = 96,
    Flavor_Half_Controls_Top_Payday_yosto = 97,
    Flavor_Half_Controls_Top_HomeSecurity_Ralleigh = 98,
    Flavor_Half_Controls_Top_Storage_Sparefoot = 99,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_operator_looking_for = 5000,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_operator_get_calls = 5001,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_plumber_appear_here = 5002,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_plumber_customers_will = 5003,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_plumber_looking_for = 5004,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_Waiting_by_the_phone = 5005,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_operator_advertise_here = 5006,
    Flavor_Half_Controls_Top_Binary_Options = 107,
    Flavor_Half_Controls_Top_HomeSecurity_Datalot = 108,
    Flavor_Half_Controls_Top_DUI_Offerweb = 109,
    Flavor_Half_Controls_Top_Payday_Affiliaxe = 110,
    Flavor_Half_Controls_Top_Mortgage_Bigcatmedia = 111,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_Operator_advertise_Here_get_call = 5007,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_Operator_customers_will = 5008,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_Waiting_get_calls_now = 5009,
    Flavor_Half_Controls_Top_Recruiting_Advertisers_Plumber_advertise_with_us = 5010,
    Flavor_Half_Controls_Top_CreditCounsellors_Offerweb = 116,
    Flavor_Half_Controls_Top_HomeSecurity_BuyerZone = 117,
    Flavor_Half_Controls_Top_StorageProvider_BuyerZone = 118,
    Flavor_Half_Controls_Top_Payday_LeadsMarket = 119,
    Flavor_Popup_PhoneDid = 120,
    Flavor_Half_Controls_Top_HealthInsurance_steps=121,
    Flavor_Half_Controls_Top_Binary_Options_TigerOptions = 122,
    GeneralPopup = 123,
    Flavor_Half_Controls_Top_Payday_LeadsMarket_UK = 124,
    Flavor_Half_Controls_Top_StorageProvider = 125,
    Flavor_Half_Controls_Top_Recruiting_clicks_dont = 5011,
    Flavor_Half_Controls_Top_Recruiting_dragged_down = 5012,
    Flavor_Half_Controls_Top_Recruiting_phone_ringing = 5013,
    Flavor_Half_Controls_Top_Recruiting_yellow_plane = 5014,
    Flavor_Half_Controls_Top_Recruiting_recyvling_bin = 5015,
    Flavor_Half_Controls_Top_Recruiting_google_search = 5016,
    Flavor_Half_Controls_Top_HealthInsurance_steps2=126,
    Flavor_Half_Controls_Top_Recruiting_Pop_dragged=127,
    Flavor_Half_Controls_Top_Recruiting_Pop_recycling = 128,
    Flavor_Half_Controls_Top_Recruiting_Pop_ringing = 129,
    Flavor_Half_Controls_Top_Recruiting_Pop_clicks = 130,
    Flavor_Half_Controls_Top_Recruiting_Pop_dress = 131,
    Flavor_Half_Controls_Top_Recruiting_Pop_search = 132,
    Email_LocksmithsAARWithStats=6000,
    Email_LocksmithsAARWithoutStats = 6001,
    Flavor_Half_Controls_Top_StorageProviderOneStep=133,
    Flavor_Half_Controls_Top_Hotel_Agoda=134,
    Flavor_InText_callPopUp = 135,
    Flavor_Half_Controls_Top_2_email = 136,
    Flavor_Half_Controls_Top_Binary_Options_Sirwilliambot = 137,
    Flavor_Half_Controls_Top_PayDay_RoundSky3 = 138,
}

public enum eBehaviour
{
    RightToLeft=1,
    RightToLeftToolBarLeft=2,
    RightToLeftToolBarRight=3,
    TopToBottom=4
}

public enum eSliderClickDestination
{
    Advertiser,
    NoProblem,
    Setting
}
public enum eSliderType
{    
    Slider = 1,
    Popup = 2,
    Wibiya = 3,
    XSaver = 4,
    GoogleFrame = 5,
    PopupRevizer = 6,
    Intext = 7,
    PopupApp = 8
}
public enum eAppType
{
    All = 1,
    Slider = 2,
    Intext= 3,
    PopApp = 4,
    DeskApp = 5,
    Pixel = 6
}
public enum eDesktopType
{
    None = 0,
    Driver = 1,
    Proxy = 2
}
public enum eDesktopAppCameFrom
{
    None = 1,
    Browser = 2,
    SearchFormApp = 3
}
public class SliderData : ItemData
{
    public const string DEFAULT_COUNTRY = "UNITED STATES";
    const string UK_COUNTRY = "UNITED KINGDOM";
    const string SALT = "KAYA";
    const string DATA_ID = "me";
  //  const string GeneralScheme = "http";
    
    //const string DEFAULT_PRODUCT_NAME = "";//"NoProblem";
  

    
    
    static readonly string PERION_TEMPLATE_URL;

    static SliderData()
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        string str = xdoc.Element("Sites").Element("SitePPC").Element("PerionData").Element("TamlataUrl").Value;
        PERION_TEMPLATE_URL = str.Trim();
        
    }

    string _headin_code;
    string _expertise_level;
    int _MaxResults = -1;
 //   string _Scheme;
    bool _general = false;
   // bool _IsConduit = false;
    public bool IsConduit()
    {
        return PpcSite.GetCurrent().ConduitToolbarOriginId == this.OriginId;
    }
    string _ToolbarId;
    public string ToolbarId
    {
        get { return _ToolbarId; }
        set { _ToolbarId = value; }
    }
    public bool General
    {
        get { return _general; }
        set { _general = value; }
    }
    string UrlGeneral
    {
        get { return (_general) ? "1" : "0"; }
    }
    public string title { get; set; }
    
    public string ClientDomain { get; set; }
    public int TimeZone { get; set; }
    public eExposureType Type { get; set; }
    public eSliderType SliderType { get; set; }
    public eDesktopType DesktopType { get; set; }
   // public eDomainName DomainName { get; set; }
//    public eAppType AppType { get; set; }
//    public Guid UserId { get; set; }
    /*
    public string ClientScheme 
    {
        get { return (string.IsNullOrEmpty(_Scheme)) ? GeneralScheme : _Scheme; }
        set { _Scheme = value; }
    }
     * */
    //public string Domain { get; set; }
    public string Url { get; set; }
    public string Keyword { get; set; }
    public string OriginalKeywords { get; set; }
    public string RegionCode { get; set; }
    /* not necesary */
    public string RegionLevel { get; set; }
    public eFlavour ControlName { get; set; }
  //  public string waterMarkComment { get; set; }
    public string PageName { get; set; }
    /* not necesary */
    public string PlaceInWebSite { get; set; }
//    public Guid ExposureId { get; set; }
   
    public string Browser { get; set; }
    public string BrowserVersion { get; set; }
    public string AppId { get; set; }
    /* not necesary */
     public string IP { get; set; }
    public bool IsAuthentic { get; set; }
    public eWelcomeExposureEvent ExposureEvent { get; set; }
   // public MongoDB.Bson.ObjectId _id { get; set; }
   
    public string RSI { get; set; }
    public string OS { get; set; }
    public bool IsWelcomeScreen { get { return false; } }
    
    public string OriginalExposure { get; set; }
    public eDesktopAppCameFrom DesktopAppCameFrom { get; set; }
    /*
    private string _ProductName;
    public string ProductName 
    { 
        private set{ _ProductName = value;}
        get
        {
            if (string.IsNullOrEmpty(_ProductName) || _ProductName[0] == '$')
                return DEFAULT_PRODUCT_NAME;
            return _ProductName;
        }
     //   set{ _ProductName = value;}
    }
     * */
    public bool IsForReport
    {
        get
        {
            return SalesUtility.IsForReport(this.ControlName);
        }
    }

    public bool IsDidPhoneAvailable { get; set; }
    
    public int MaxResults
    {
        get { return _MaxResults; }
        set { _MaxResults = value; }
    }
    /*
    public string ExpertiseLevel
    {
        set { _expertise_level = value; }
        get { return (string.IsNullOrEmpty(_expertise_level)) ? GeneralHeadingCode : _expertise_level; }
    }
     */
    public string ExpertiseCode 
    {
        get { return _headin_code; }
        set
        { 
            if(string.IsNullOrEmpty(value))
                return;
            this._headin_code = value;
        }
    }

    public string ClientDomainWithSchema(HttpRequest _request)
    {
       /*
        if (string.IsNullOrEmpty(ClientDomain))
            return string.Empty;
        return ClientScheme + "://" + ClientDomain;

        * */
       
        if (string.IsNullOrEmpty(ClientDomain))
            return string.Empty;
        string _domain = "";
        if (ClientDomain == "localhost" || _request != null)
        {
            _domain = _request.Url.Scheme + @"://" + _request.Url.Authority + _request.ApplicationPath + @"/";
        }
        else
            _domain = ClientScheme + @"://" + _domain + @"/";
        return _domain;        
        
    }
    public string ClientDomainWithSchema()
    {
        if (string.IsNullOrEmpty(ClientDomain))
            return string.Empty;
        return ClientScheme + @"://" + ClientDomain + @"/";
    }
	public SliderData()
	{
		//
		
		//
	}
    //For mobile
    /*
    public SliderData(string UserAgent, string keyword, string Origin, GeoLocation gl, string _IP)
    {
        PpcSite _cache = PpcSite.GetCurrent();
        this._id = MongoDB.Bson.ObjectId.GenerateNewId();
        this.Browser = UserAgent;
        this.Keyword = keyword;        
        this.ExpertiseCode = _cache.GetHeadingCodeByKeyword(keyword);
        this.ControlName = eFlavour.MobileDevice;
        this.Type = eExposureType.MobileDevice;
        this.SiteId = _cache.SiteId;
        this.Country = gl.Country;
        this.RegionCode = gl.ZipCode;
        this.IsAuthentic = true;
        this.IP = _IP;
        this.Date = DateTime.UtcNow;
        Guid _id;
        if (!Guid.TryParse(Origin, out _id))
            _id = Guid.Empty;
        this.OriginId = (_id == Guid.Empty) ? _cache.UnknoenOriginId : _id;
        
    }
     * */
    //For Perion
    public SliderData(string StrKeyword, string URL, string Title, string ToolbarId, 
        string OriginId, string OS, string RSI, string OurDomain, HttpRequest Request)
    {
        PpcSite _cache = PpcSite.GetCurrent();
        this._id = MongoDB.Bson.ObjectId.GenerateNewId();

        this.IP = Utilities.GetIP(Request);
        this.Browser = Request.Browser.Browser;
        this.BrowserVersion = Request.Browser.Version;

        string _ZipCode;
        this.Country = SalesUtility.GetCountryName(this.IP, string.Empty, out _ZipCode);
        if (this.Country != DEFAULT_COUNTRY)
            this.Country = DEFAULT_COUNTRY;

        
        Uri _uri;
        if (Uri.TryCreate(URL, UriKind.Absolute, out _uri))
        {
            this.ClientDomain = _uri.Host;
            this.Url = _uri.AbsolutePath;
        }
        else
            this.Url = URL;
       
        this.Domain = OurDomain;
        this.DomainName = GetDomainNameByDomainURL();
        this.title = Title;
        this.RSI = RSI;
        this.OS = OS;
        this.ToolbarId = ToolbarId;
        this.ClientScheme = "http";

        this.SliderType = eSliderType.XSaver;
        this.Type = eExposureType.Regular;
        this.SiteId = _cache.SiteId;
       
        this.RegionCode = _ZipCode;        
        this.IP = IP;
        this.Date = DateTime.UtcNow;
        Guid _id;
        if (Guid.TryParse(OriginId, out _id))
            this.OriginId = _id;
        else
            this.OriginId = _cache.PerionId;   
        SetKeyword(StrKeyword);

    }
    //For General - Revizer
    public SliderData(string StrKeyword, string OriginId, string ToolbarId, HttpContext context)
    {
        
        PpcSite _cache = PpcSite.GetCurrent();
        this._id = MongoDB.Bson.ObjectId.GenerateNewId();

        /*
        this.Browser = Browser;
        this.Url = URL;
        this.title = Title;
         * */
        this.ClientScheme = "http";
        this.IsAuthentic = true;
        /*
        Uri _uri;
        if (Uri.TryCreate(URL, UriKind.RelativeOrAbsolute, out _uri))
        {
            this.ClientDomain = _uri.Host;
        }
         * */
        //    this.Keyword = keyword;
        //    this.ExpertiseCode = _cache.GetHeadingCodeByKeyword(keyword);
        //     this.ControlName = eFlavour.MobileDevice;
        SetContextBaseParams(context);
        this.SliderType = eSliderType.PopupRevizer;
        this.Type = eExposureType.Regular;
        //this.Type = eType;
        
        this.SiteId = _cache.SiteId;
        this._general = false;
        
      //  this.Domain = _domain;
        /*
        string _ZipCode;
        this.Country = SalesUtility.GetCountryName(IP, string.Empty, out _ZipCode);
        this.RegionCode = _ZipCode;
         * */
        //this.IP = IP;
        this.Date = DateTime.UtcNow;
        
        Guid _id;
        if (Guid.TryParse(OriginId, out _id))
            this.OriginId = _id;
        else
            this.OriginId = _cache.RevizerPopup;                 
        this.ToolbarId = ToolbarId;
        if (string.IsNullOrEmpty(StrKeyword))
            return;
        this.DomainName = GetDomainNameByDomainURL();
        this.Keyword = StrKeyword.ToLower();
        this.ExpertiseCode = _cache.GetHeadingCodeByKeyword(this.Keyword);

    }
    
   
    
    /*
    //For popup phone did
    public SliderData(HttpContext context, eExposureType ex_type)
    {
        PpcSite _cache = PpcSite.GetCurrent();
        string strKeyword = context.Request.QueryString["keyword"];
        string oid = context.Request.QueryString["originid"];
       // string _domain = context.Request.Url.Authority;

        this.ToolbarId = context.Request.QueryString["toolbarid"];
        this.Type = eExposureType.Popup;
        this._id = MongoDB.Bson.ObjectId.GenerateNewId();
        this.IP = Utilities.GetIP(context.Request);
        this.Browser = context.Request.Browser.Browser;
        this.BrowserVersion = context.Request.Browser.Version;
        this.Domain = context.Request.Url.Authority;

        this.ClientScheme = "http";
        this.IsAuthentic = true;
        string _ZipCode;
        this.Country = SalesUtility.GetCountryName(this.IP, this.Domain, this.Type, out _ZipCode);
        this.RegionCode = _ZipCode;
       
        this.SiteId = _cache.SiteId;
        this._general = false;
      
        this.Date = DateTime.UtcNow;

        Guid _id;
        if (Guid.TryParse(oid, out _id))
            this.OriginId = _id;
        else
            this.OriginId = _cache.RevizerPopup;
        if (!string.IsNullOrEmpty(strKeyword))
        {
            this.Keyword = strKeyword.ToLower();
            this.ExpertiseCode = _cache.GetHeadingCodeByKeyword(this.Keyword);
        }

    }
     * */
    //For slider_type
    public SliderData(HttpContext context, eSliderType slider_type)
    {
        /*
       this._id = MongoDB.Bson.ObjectId.GenerateNewId();
       this.IP = Utilities.GetIP(context.Request);
       this.Browser = context.Request.Browser.Browser;
       this.BrowserVersion = context.Request.Browser.Version;
       this.Date = DateTime.UtcNow;
       
       if (slider_type == eSliderType.Slider)
       {
            
           this.ControlName = eFlavour.Conduit_General_Search;
           if (context.Request.UrlReferrer != null)
           {
               this.Url = context.Request.UrlReferrer.AbsoluteUri;
               this.ClientDomain = context.Request.UrlReferrer.Host;
               this.ClientScheme = context.Request.UrlReferrer.Scheme;
           }
           string _ZipCode;
       //    this.Country = SalesUtility.GetCountryName(Utilities.GetIP(context.Request), context.Request.Url.Host, out _ZipCode);
           this.Country = SalesUtility.GetCountryName(Utilities.GetIP(context.Request), context.Request.Url.Host, this.Type, out _ZipCode);
           this.RegionCode = _ZipCode;
           this.General = true;
           this.Type = GetSliderType(PpcSite.);

       }
        */
        if (slider_type == eSliderType.Popup)
        {
            SliderDataNpPopup(context, slider_type);
        }
        else if (slider_type == eSliderType.Intext)
        {
            PpcSite _cache = PpcSite.GetCurrent();
            this._id = MongoDB.Bson.ObjectId.GenerateNewId();
            Uri _UrlReferrer = null;
            try
            {
                _UrlReferrer = context.Request.UrlReferrer;
            }
            catch (UriFormatException ufexc)
            {
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            if (_UrlReferrer != null)
            {
                this.Url = _UrlReferrer.AbsoluteUri;
                this.ClientDomain = _UrlReferrer.Host;
                this.ClientScheme = _UrlReferrer.Scheme;
            }
            else
                this.ClientDomain = context.Request.QueryString["domain"];
            SetContextParams(context);
            this.SliderType = eSliderType.Intext;
           
            string _ZipCode;

            this.Country = SalesUtility.GetCountryName(Utilities.GetIP(context.Request), context.Request.Url.Host, this.SliderType, out _ZipCode);
            this.Date = DateTime.UtcNow;
            this.RegionCode = _ZipCode;
            this.Keyword = context.Request.QueryString["Keyword"];
            this.ExpertiseCode = _cache.GetHeadingCodeByKeyword(this.Keyword);
        }
        else if (slider_type == eSliderType.Wibiya)
        {
            // this.Domain = context.Request.Url.Host;
            this.SiteId = context.Request.QueryString["SiteId"];


            string OriginId = context.Request.QueryString["key"];
            Guid _origiid;
            if (!Guid.TryParse(OriginId, out _origiid))
                _origiid = Guid.Empty;
            this.OriginId = _origiid;
            this.PageName = context.Request.QueryString["PageName"];
            this.PlaceInWebSite = context.Request.QueryString["PlaceInWebSite"];
            string _ControlName = context.Request.QueryString["ControlName"];
            eFlavour ef;
            if (!Enum.TryParse(_ControlName, true, out ef))
                ef = eFlavour.Flavor_General_Search;
            this.ControlName = ef;
            if (context.Request.UrlReferrer != null)
            {
                this.Url = context.Request.UrlReferrer.AbsoluteUri;
                this.ClientDomain = context.Request.UrlReferrer.Host;
                this.ClientScheme = context.Request.UrlReferrer.Scheme;
            }
            SetContextBaseParams(context);
            /*
           string _ZipCode;           
           this.Country = SalesUtility.GetCountryName(Utilities.GetIP(context.Request), context.Request.Url.Host, out _ZipCode);
           this.RegionCode = _ZipCode;
           this.IP = Utilities.GetIP(context.Request);
           this.Browser = context.Request.Browser.Browser;
           this.BrowserVersion = context.Request.Browser.Version;
            */
            this.SliderType = eSliderType.Wibiya;
        }

    }
    //For NP popup
    private void SliderDataNpPopup(HttpContext context, eSliderType st)
    {
        string strKeyword = context.Request.QueryString["keyword"];
        string _toolbarid = context.Request.QueryString["toolbarid"];
        string originid = context.Request.QueryString["originid"];
        string _typeString = context.Request.QueryString["Type"];
        string _ok = context.Request.QueryString["ok"];
        this.OriginalExposure = context.Request.QueryString["OriginalExposure"];
        PpcSite _cache = PpcSite.GetCurrent();

        this.SliderType = st;
        this._id = MongoDB.Bson.ObjectId.GenerateNewId();

        this.ClientScheme = "http";
        this.IsAuthentic = true;

        int _type;
        if (!int.TryParse(_typeString, out _type))
            _type = -1;
        if (Enum.IsDefined(typeof(eExposureType), _type))
            this.Type = (eExposureType)_type;
        else
            this.Type = GetSliderType(_cache);       

        this.SiteId = _cache.SiteId;
        this._general = false;

        this.Date = DateTime.UtcNow;

        if (context.Request.UrlReferrer != null)
        {
            this.Url = context.Request.UrlReferrer.AbsoluteUri;
            this.ClientDomain = context.Request.UrlReferrer.Host;
            //  this.ClientScheme = context.Request.UrlReferrer.Scheme;
        }

        Guid _id;
        if (Guid.TryParse(originid, out _id))
            this.OriginId = _id;
        else
            this.OriginId = _cache.UnknoenOriginId;
        this.OriginalKeywords = _ok;
        this.ToolbarId = ToolbarId;
        if (string.IsNullOrEmpty(strKeyword))
            return;
        SetContextBaseParams(context);
        SetProductName(context);
        this.Keyword = strKeyword.ToLower();
        this.ExpertiseCode = _cache.GetHeadingCodeByKeyword(this.Keyword);

    }
    public SliderData(HttpContext context, eAppType app_type)
    {
        if(app_type== eAppType.DeskApp)
        {
            PpcSite _cache = PpcSite.GetCurrent();
            this._id = MongoDB.Bson.ObjectId.GenerateNewId();
            string HeadingName = context.Request.QueryString["headingname"];
            Guid uid;
            string _userid = context.Request.QueryString["UserUniqueId"];
            if (Guid.TryParse(_userid, out uid))
                this.UserId = uid;
            if (!string.IsNullOrEmpty(HeadingName))
            {
                this.ExpertiseCode = _cache.GetHeadinCode(ref HeadingName);
            }
            else
            {                
                this.Keyword = context.Request.QueryString["keyword"];              
                this.OriginalKeywords = context.Request.QueryString["ok"];
                this.ExpertiseCode = context.Request.QueryString["ExpertiseCode"];
            }
            SetDesktopAppCameFromByQueryString(context.Request.QueryString);
       //     SetDesktopTypeByQueryString(context.Request.QueryString);
       //     _cache.ha
            this.ToolbarId = context.Request.QueryString["toolbarid"];
            string originid = context.Request.QueryString["originid"];
            Guid oid;
            if (!Guid.TryParse(originid, out oid))
                oid = _cache.UnknoenOriginId;
            this.OriginId = oid;
           this.AppType = eAppType.DeskApp;
            SetContextBaseParams(context);
            SetProductName(context);
            this.Date = DateTime.UtcNow;
        }
    }
    void SetContextBaseParams(HttpContext context)
    {
        string _ZipCode;
        this.Country = SalesUtility.GetCountryName(Utilities.GetIP(context.Request), context.Request.Url.Host, out _ZipCode);
        this.RegionCode = _ZipCode;
        this.IP = Utilities.GetIP(context.Request);
        this.Browser = context.Request.Browser.Browser;
        this.BrowserVersion = context.Request.Browser.Version;
        this.Domain = Utilities.GetHostName(context.Request);// context.Request.Url.Authority;
        this.DomainName = GetDomainNameByDomainURL();
        SetProductName();
    }
    void SetDesktopTypeByQueryString(System.Collections.Specialized.NameValueCollection query_string)
    {
        int num;
        if (!int.TryParse(query_string["dapp"], out num))
            this.DesktopType = eDesktopType.None;       
        else if (Enum.IsDefined(typeof(eDesktopType), num))
            this.DesktopType = (eDesktopType)num;
        else this.DesktopType = eDesktopType.None;        
    }
    void SetDesktopAppCameFromByQueryString(System.Collections.Specialized.NameValueCollection query_string)
    {
        int num;
        if (!int.TryParse(query_string["camefrom"], out num))
            this.DesktopAppCameFrom = eDesktopAppCameFrom.None;
        else if (Enum.IsDefined(typeof(eDesktopAppCameFrom), num))
            this.DesktopAppCameFrom = (eDesktopAppCameFrom)num;
        else this.DesktopAppCameFrom = eDesktopAppCameFrom.None;
    }
     void SetAppTypeByQueryString(System.Collections.Specialized.NameValueCollection query_string)
    {
        int num;
        if (!int.TryParse(query_string["apptype"], out num))
            this.AppType = eAppType.All;
        else
        {
            if (Enum.IsDefined(typeof(eAppType), num))
                this.AppType = (eAppType)num;
            else
                this.AppType = eAppType.All;
        }
    }
    
    /*
    public SliderData(HttpContext context)
    {
       
    }
     * */
    public static SliderData GetSliderData(HttpContext context)
    {
        PpcSite ps = PpcSite.GetCurrent();
        SliderData _sd = new SliderData();
        /*
        
        if (!Guid.TryParse(context.Request.QueryString["ExposureId"], out _id))
            _id = Guid.Empty;
        _sd.ExposureId = _id;
         * */
        MongoDB.Bson.ObjectId eid;
        if (!MongoDB.Bson.ObjectId.TryParse(context.Request.QueryString["ExposureId"], out eid))
            eid = MongoDB.Bson.ObjectId.Empty;
        _sd._id = eid;
        _sd.Date = DateTime.UtcNow;

        Guid _id;
        if (!Guid.TryParse(context.Request.QueryString["OriginId"], out _id))
            _id = ps.UnknoenOriginId;
        _sd.OriginId = _id;

        string cn = context.Request.QueryString["ControlName"];
        eFlavour ef;
        if(!Enum.TryParse(cn, out ef))
            ef = eFlavour.Flavor_General;
        _sd.ControlName = ef;
        int num;
        if (!int.TryParse(context.Request.QueryString["type"], out num))
            _sd.Type = _sd.GetSliderType(ps);
        else
        {
            if(Enum.IsDefined(typeof(eExposureType), num))
                _sd.Type = (eExposureType)num;
            else
                _sd.Type = _sd.GetSliderType(ps);
        }
        if (!int.TryParse(context.Request.QueryString["slidertype"], out num))
            _sd.SliderType = eSliderType.Slider;
        else
        {
            if (Enum.IsDefined(typeof(eSliderType), num))
                _sd.SliderType = (eSliderType)num;
            else
                _sd.SliderType = eSliderType.Slider;
        }

  //      _sd.SetDesktopTypeByQueryString(context.Request.QueryString);


        _sd.ExpertiseCode = context.Request.QueryString["ExpertiseCode"];
        _sd.Keyword = context.Request.QueryString["Keyword"];
        if (string.IsNullOrEmpty(_sd.ExpertiseCode) && !string.IsNullOrEmpty(_sd.Keyword))
            _sd.ExpertiseCode = ps.GetHeadingCodeByKeyword(_sd.Keyword);
        _sd.AppId = context.Request.QueryString["AppId"];

        _sd.SiteId = context.Request.QueryString["SiteId"];
        if (string.IsNullOrEmpty(_sd.SiteId))
            _sd.SiteId = ps.SiteId;
        bool idpa = false;        
        _sd.IsDidPhoneAvailable = (bool.TryParse(context.Request.QueryString["IsDidPhoneAvailable"], out idpa)) ?  idpa : false;
        _sd.ToolbarId = context.Request.QueryString["ToolbarId"];
        /*
        if(string.IsNullOrEmpty(_sd.ToolbarId))
            _sd.ToolbarId = context.Request.QueryString["subid"];
         * */
        _sd.ClientScheme = context.Request.QueryString["ClientScheme"];
        if (!_sd.ClientScheme.StartsWith("http"))
            _sd.ClientScheme = "http";
        _sd.Country = context.Request.QueryString["Country"];
        _sd.ClientDomain = context.Request.QueryString["ClientDomain"];
        if (!int.TryParse(context.Request.QueryString["TimeZone"], out num))
            num = GENERAL_TIME_ZONE;
        _sd.TimeZone = num;
        _sd.SetDesktopAppCameFromByQueryString(context.Request.QueryString);
        _sd.SetAppTypeByQueryString(context.Request.QueryString);
        _sd.RegionCode = context.Request.QueryString["RegionCode"];
        _sd.SetProductName(context);// = context.Request.QueryString["ProductName"];
        _sd.RSI = context.Request.QueryString["rsi"];
        _sd.OS = context.Request.QueryString["os"];
        _sd.Domain = context.Request.Url.Host;
        _sd.DomainName = _sd.GetDomainNameByDomainURL();

       _sd.IP = Utilities.GetIP(context.Request);
       _sd.Browser = context.Request.Browser.Browser;
       _sd.BrowserVersion = context.Request.Browser.Version;
       _sd.Domain = context.Request.Url.Host;

        return _sd;
       
    }
    public string GetDefaultCountry()
    {       
        switch (this.Country)
        {
            case ("UNITED KINGDOM"):
            case ("UNITED STATES"):
                return this.Country;
            default:
                return "UNITED STATES";                
        }        
    }
    /*
    public static eExposureType GetSliderType(Guid OriginId, PpcSite _cache)
    {
        return _cache.IsNoProblemSlider(OriginId) ? eExposureType.NoProblemSlider : eExposureType.Regular;
    }
     * */
    public static SliderData GetSliderData(MongoDB.Bson.ObjectId ExposureId, HttpContext context)
    {
        SliderData sd = GetSliderData(ExposureId);
        if (sd == null)
            sd = GetSliderData(context);
        else
        {
            sd.Domain = context.Request.Url.Host;
        //    sd.Type = context.Request.QueryString["type"] == "3" ? eExposureType.GoogleSterkly : eExposureType.Regular;
            sd.AppId = context.Request.QueryString["appid"];
            sd.IsDidPhoneAvailable = Convert.ToBoolean(context.Request.QueryString["IsDidPhoneAvailable"]);
        }
        return sd;
    }
    public static SliderData GetSliderData(MongoDB.Bson.ObjectId ExposureId)
    {
        MongoDB.Driver.MongoCollection ColExposures = DBConnection.GetMongoExposureCollection();
        var query = MongoDB.Driver.Builders.Query<SliderData>.EQ(e => e._id, ExposureId);
        SliderData sd = null;
        try
        {
            sd = ColExposures.FindOneAs<SliderData>(query);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        return sd;
        
    }
    public SliderData(HttpContext context)
    {
        this._id = MongoDB.Bson.ObjectId.GenerateNewId();
        Uri _UrlReferrer = null;
        try
        {
            _UrlReferrer = context.Request.UrlReferrer;
        }
        catch (UriFormatException ufexc)
        {
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        if (_UrlReferrer != null)
        {
            this.Url = _UrlReferrer.AbsoluteUri;
            this.ClientDomain = _UrlReferrer.Host;
            this.ClientScheme = _UrlReferrer.Scheme;
        }
        else
            this.ClientDomain = context.Request.QueryString["domain"];
        SetContextParams(context);
        string _ZipCode;

        this.Country = SalesUtility.GetCountryName(Utilities.GetIP(context.Request), context.Request.Url.Host, this.SliderType, out _ZipCode);
        this.Date = DateTime.UtcNow;
        this.RegionCode = _ZipCode;
        string _keyword = context.Request.QueryString["Keyword"];

        SetKeyword(_keyword);
 //       if (!string.IsNullOrEmpty(this.Keyword))
 //           this.ExpertiseCode = PpcSite.GetCurrent().GetHeadingCodeByKeyword(this.Keyword);
    }
    
    private void SetKeyword(string _keyword)
    {
        this.OriginalKeywords = _keyword;
        if (string.IsNullOrEmpty(_keyword))
            return;
        KeywordCalc key_calc = new KeywordCalc(_keyword, this.ClientDomain);
        string keyword_win = key_calc.GetKeywordFirstRate(this);
        if (string.IsNullOrEmpty(keyword_win))
            return;
        this.Keyword = keyword_win;
        this.ExpertiseCode = PpcSite.GetCurrent().GetHeadingCodeByKeyword(this.Keyword);
       
    }
    /*
    private void NotFirstTime(HttpContext context)
    {
        
        this.Keyword = context.Request.QueryString["Keyword"];
        if (string.IsNullOrEmpty(this.ExpertiseCode) && !string.IsNullOrEmpty(this.Keyword))
            this.ExpertiseCode = PpcSite.GetCurrent().GetHeadingCodeByKeyword(this.Keyword);
        this.SetContextParams(context);
        if (string.IsNullOrEmpty(this.ClientDomain))
            this.ClientDomain = context.Request.QueryString["domain"];
        if (string.IsNullOrEmpty(this._Scheme))
            this.ClientScheme = context.Request.QueryString["Scheme"];
        string _HeadingCode = context.Request.QueryString["ExpertiseCode"];
        if (!string.IsNullOrEmpty(_HeadingCode))
            this.ExpertiseCode = _HeadingCode;
        string _cn = context.Request.QueryString["ControlName"];
        eFlavour _ef;
        if (!Enum.TryParse(_cn, out _ef))
            _ef = eFlavour.Flavor_General;
        this.ControlName = _ef;
        

    }
 * */
    /*
    private eDomainName GetDomainNameByDomainURL()
    {
        if (this.Domain.EndsWith("noproblemppc.com"))
            return eDomainName.noproblemppc;
        if (this.Domain.EndsWith("donutleads.com"))
            return eDomainName.donutleads;
        if (this.Domain.EndsWith("pastaleads.com"))
            return eDomainName.pastaleads;
        if (this.Domain.EndsWith("bbqleads.com"))
            return eDomainName.bbqleads;
        if (this.Domain.EndsWith("shopwithboost.com"))
            return eDomainName.shopwithboost;
        return eDomainName.noproblemppc;
    }
     * */
    private void SetContextParams(HttpContext context)
    {
        PpcSite _cache = PpcSite.GetCurrent();
        this.Domain = context.Request.Url.Host;
        this.DomainName = GetDomainNameByDomainURL();
        this.title = context.Request.QueryString["title"];
        this.RegionCode = context.Request.QueryString["RegionCode"];
        this.AppId = context.Request.QueryString["appid"];
        string _tbid = context.Request.QueryString["mamid"];
        if(string.IsNullOrWhiteSpace(_tbid))
            _tbid = context.Request.QueryString["ToolbarId"];
        this._ToolbarId = _tbid;
      //  this._IsConduit = context.Request.QueryString.AllKeys.Contains("toolbarid");
        this.RegionLevel = context.Request.QueryString["RegionLevel"];
       // this.ProductName = context.Request.QueryString["ProductName"];
        
        string _timezone = context.Request.QueryString["tz"];
        int _tz;
        if (!int.TryParse(_timezone, out _tz))
            _tz = GENERAL_TIME_ZONE;
        else
            _tz = (_tz / 60) * -1;
        this.TimeZone = _tz;
        string gen = context.Request.QueryString["General"];
        if (!string.IsNullOrEmpty(gen) && gen == "1")
            this.General = true;
        this.PageName = context.Request.QueryString["PageName"];
        this.PlaceInWebSite = context.Request.QueryString["PlaceInWebSite"];
        
       // this.Type = (_et == "3") ? eExposureType.GoogleSterkly : eExposureType.Regular;
        /*
        if (string.IsNullOrEmpty(this.Url))
            this.Url = (context.Session["client_url"] != null) ? (string)context.Session["client_url"] : string.Empty;
        else
            context.Session["client_url"] = this.Url;
         * */
        string _OriginId = context.Request.QueryString["OriginId"];

        this.OriginId = _cache.GetOriginOrDefault(_OriginId);
        SetProductName(context);
        Guid _id;
        string _userid = context.Request.QueryString["UserUniqueId"];
        if (Guid.TryParse(_userid, out _id))
            this.UserId = _id;
        string cachesiteid = _cache.SiteId.ToLower();
        string querysiteid = (context.Request.QueryString["SiteId"] == null) ? "" : context.Request.QueryString["SiteId"].ToLower();
        querysiteid = querysiteid.ToLower();
        if (querysiteid != cachesiteid)
        {
    //        dbug_log.SliderLog(this, context, "SiteId missing: " + (string.IsNullOrEmpty(this.SiteId) ? "" : this.SiteId));
            this.SiteId = _cache.SiteId;
        }
        else
            this.SiteId = _cache.SiteId;
        
        int _st;
        if (!int.TryParse(context.Request.QueryString["slidertype"], out _st))
            _st = -1;
        if (Enum.IsDefined(typeof(eSliderType), _st))
            this.SliderType = (eSliderType)_st;
        else
            this.SliderType = eSliderType.Slider;
        /*
        int num;
        if (!int.TryParse(context.Request.QueryString["apptype"], out num))
            this.AppType = eAppType.All;
        else
        {
            if (Enum.IsDefined(typeof(eAppType), num))
                this.AppType = (eAppType)num;
            else
                this.AppType = eAppType.All;
        }
         * */
        this.SetAppTypeByQueryString(context.Request.QueryString);
        

        this.Type = GetSliderType(_cache);       
       
        this.IP = Utilities.GetIP(context.Request);
        this.Browser = context.Request.Browser.Browser;
        this.BrowserVersion = context.Request.Browser.Version;
  //      this.Domain = context.Request.Url.Host;

       

    }
    eExposureType GetSliderType(PpcSite _cache)
    {
        //return (_cache.IsNoProblemSlider(this.OriginId)) ? eExposureType.NoProblemSlider : eExposureType.Regular;
        if (this.OriginId == _cache.AduckyId)
            return eExposureType.Aducky;

        else if (this.OriginId == _cache.SpeedOptimizer_Guru)
            return eExposureType.SpeedOptimizer_Guru;
        else if (this.OriginId == _cache.PicRecId)
            return eExposureType.PicRec;
        else if (this.OriginId == _cache._50OnRedId)
            return eExposureType._50OnRedId;
        else if (this.OriginId == _cache.ShopwithboostId)
            return eExposureType.Shopwithboost;
        return eExposureType.Regular;
    }
    string GetOurDomainWithoutSubDomain()
    {
        int _start = this.Domain.IndexOf('.');
        string _domain;
        if (_start > -1)
            _domain = this.Domain.Substring(_start + 1);
        else
            _domain = this.Domain;
        return _domain;
    }
    /*
    bool IsPastaLeadsDomain(PpcSite _cache)
    {        
        return (GetOurDomainWithoutSubDomain() == _cache.PastaLeadsDomain);
    }
    bool IsDonutleadsDomain(PpcSite _cache)
    {        
        return (GetOurDomainWithoutSubDomain() == _cache.DonutleadsDomain);
    }
     * */
    
    public string GetFullSliderPopupPath(SliderViewData svd, HttpContext context)
    {
        string _SliderPath = svd.SliderPath + "?" + this.GetUrlParamsForPopupFlavor();

     //   string _SchemeDomain = "http://" + context.Request.Url.Authority + HttpRuntime.AppDomainAppVirtualPath;//PpcSite.GetCurrent().MainDomain;
        string _SchemeDomain = "http://" + Utilities.GetHostName(context.Request) + HttpRuntime.AppDomainAppVirtualPath;//PpcSite.GetCurrent().MainDomain;

        if (!_SchemeDomain.EndsWith(@"/"))
            _SchemeDomain += @"/";
        _SliderPath = _SchemeDomain + _SliderPath;
        return _SliderPath;
    }
    public string GetUrlParamsForDesktopApp(PpcSite _cache)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        HeadingCode hd = _cache.GetHeadingDetails(this.ExpertiseCode);
        dic.Add("categoryid", hd.ID.ToString());
        dic.Add("categoryname", hd.heading);
        dic.Add("ExpertiseCode", ExpertiseCode); 
        dic.Add("Keyword", Keyword);
        dic.Add("ok", this.OriginalKeywords);
        dic.Add("ToolbarId", this.ToolbarId);
        dic.Add("OriginId", this.OriginId.ToString());
        if (this.UserId != Guid.Empty)
            dic.Add("UserUniqueId", this.UserId.ToString());
    //    dic.Add("SliderType", ((int)this.SliderType).ToString());
     //   dic.Add("Type", ((int)this.Type).ToString());
        if (!string.IsNullOrEmpty(this.ProductName))
            dic.Add("ProductName", this.ProductName);
    //    dic.Add("dapp", ((int)this.DesktopType).ToString());
        dic.Add("camefrom", ((int)this.DesktopAppCameFrom).ToString());
       // dic.Add("AppType", ((int)this.AppType).ToString());
        
        StringBuilder sb = new StringBuilder();
        foreach (KeyValuePair<string, string> kvp in dic)
        {
            sb.Append(kvp.Key + "=" + HttpUtility.UrlEncode(kvp.Value) + "&");
        }
        if (sb.Length > 0)
            return sb.ToString().Substring(0, sb.Length - 1);
        return "";
    }
    public string GetUrlParamsForPopupFlavor()
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Keyword", Keyword);
        dic.Add("ok", this.OriginalKeywords);
        dic.Add("ToolbarId", this.ToolbarId);
        dic.Add("OriginId", this.OriginId.ToString());
        dic.Add("SliderType", ((int)this.SliderType).ToString());
        dic.Add("Type", ((int)this.Type).ToString());
        if (!string.IsNullOrEmpty(this.ProductName))
            dic.Add("ProductName", this.ProductName);
        if (!string.IsNullOrEmpty(this.OriginalExposure))
            dic.Add("OriginalExposure", this.OriginalExposure);
        dic.Add("dapp", ((int)this.DesktopType).ToString());
        StringBuilder sb = new StringBuilder();
        foreach (KeyValuePair<string, string> kvp in dic)
        {
            sb.Append(kvp.Key + "=" + HttpUtility.UrlEncode(kvp.Value) + "&");
        }
        if (sb.Length > 0)
            return sb.ToString().Substring(0, sb.Length - 1);
        return "";
    }
    public string GetUrlParamsForIntextPopupFlavor()
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Keyword", Keyword);
        dic.Add("ok", this.OriginalKeywords);
        dic.Add("ToolbarId", this.ToolbarId);
        dic.Add("OriginId", this.OriginId.ToString());
        dic.Add("SliderType", ((int)this.SliderType).ToString());
        dic.Add("Type", ((int)this.Type).ToString());
        dic.Add("dapp", ((int)this.DesktopType).ToString());
        dic.Add("OriginalExposure", this._id.ToString());
        if (!string.IsNullOrEmpty(this.ProductName))
            dic.Add("ProductName", this.ProductName);
        StringBuilder sb = new StringBuilder();
        foreach (KeyValuePair<string, string> kvp in dic)
        {
            sb.Append(kvp.Key + "=" + HttpUtility.UrlEncode(kvp.Value) + "&");
        }
        if (sb.Length > 0)
            return sb.ToString().Substring(0, sb.Length - 1);
        return "";
    }
    public string GetUrlParamsForIntext()
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();        
        dic.Add("OriginId", this.OriginId.ToString());
        dic.Add("type", ((int)this.Type).ToString());
        if (!string.IsNullOrEmpty(ExpertiseCode))
            dic.Add("ExpertiseCode", ExpertiseCode);        
        if (!string.IsNullOrEmpty(AppId))
            dic.Add("appid", AppId);
        dic.Add("dapp", ((int)this.DesktopType).ToString());
        dic.Add("SiteId", this.SiteId);
        dic.Add("ToolbarId", this.ToolbarId);
        if (!string.IsNullOrEmpty(this.ProductName))
            dic.Add("ProductName", this.ProductName);
        if (!string.IsNullOrEmpty(this.OS))
            dic.Add("os", this.OS);
        if (!string.IsNullOrEmpty(this.RSI))
            dic.Add("rsi", this.RSI);
        if (UserId != Guid.Empty)
            dic.Add("UserUniqueId", this.UserId.ToString());

        StringBuilder sb = new StringBuilder();
        foreach (KeyValuePair<string, string> kvp in dic)
        {
            sb.Append(kvp.Key + "=" + HttpUtility.UrlEncode(kvp.Value) + "&");
        }
        if (sb.Length > 0)
            return sb.ToString().Substring(0, sb.Length - 1);
        return "";
    }
    public string GetUrlParamsForFlavor()
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("ExposureId", this._id.ToString());       
        dic.Add("ControlName", ControlName.ToString());
        dic.Add("SliderType", ((int)this.SliderType).ToString());
        dic.Add("type", ((int)this.Type).ToString());
        dic.Add("OriginId", this.OriginId.ToString());
        if (!string.IsNullOrEmpty(ExpertiseCode))
            dic.Add("ExpertiseCode", ExpertiseCode);
        if (!string.IsNullOrEmpty(Keyword))
            dic.Add("Keyword", Keyword);
        if (!string.IsNullOrEmpty(AppId))
            dic.Add("appid", AppId);
        if (UserId != Guid.Empty)
            dic.Add("UserUniqueId", this.UserId.ToString());
        dic.Add("dapp", ((int)this.DesktopType).ToString());
        dic.Add("IsDidPhoneAvailable", this.IsDidPhoneAvailable.ToString());
        dic.Add("SiteId", this.SiteId);
        dic.Add("ToolbarId", this.ToolbarId);
        dic.Add("ClientDomain", this.ClientDomain);
        dic.Add("TimeZone", this.TimeZone.ToString());
        if(!string.IsNullOrEmpty(this.RegionCode))
            dic.Add("RegionCode", this.RegionCode);
        dic.Add("AppType", ((int)this.AppType).ToString());
        dic.Add("Country", this.Country);
        dic.Add("ClientScheme", this.ClientScheme);
        if (this.DesktopAppCameFrom != eDesktopAppCameFrom.None)
            dic.Add("camefrom", ((int)this.DesktopAppCameFrom).ToString());
        if (!string.IsNullOrEmpty(this.ProductName))
            dic.Add("ProductName", this.ProductName);
        if (!string.IsNullOrEmpty(this.OS))
            dic.Add("os", this.OS);
        if (!string.IsNullOrEmpty(this.RSI))
            dic.Add("rsi", this.RSI);
       

        StringBuilder sb = new StringBuilder();
        foreach (KeyValuePair<string, string> kvp in dic)
        {
            sb.Append(kvp.Key + "=" + HttpUtility.UrlEncode(kvp.Value) + "&");
        }
        if (sb.Length > 0)
            return sb.ToString().Substring(0, sb.Length - 1);
        return "";
    }
    public string GetIntextScript(HttpContext context)
    {
        if (this.Country != PpcSite.UNITED_STATES && this.Country != PpcSite.ISRAEL)
            return string.Empty;
       // if (this.OriginId == PpcSite.GetCurrent().RevizerOriginId || this.OriginId == new Guid("02A9C962-B572-E311-B7DA-001517D1792A")) //Verti production
        if (PpcSite.GetCurrent().IsInIntextBlackList(this.OriginId))
            return string.Empty;
        
        if (PpcSite.GetCurrent().CrossriderId == this.OriginId && !string.IsNullOrEmpty(this.ClientDomain) && this.ClientDomain.Contains("google."))
            return string.Empty;
        
        StringBuilder _script = new StringBuilder();
        _script.Append(@"(function () {        
            var _script = document.createElement('script');
            _script.type = 'text/javascript';
            _script.async = true;");
        string path = this.GetServerDomainWithSchema(context) + "npsb/intext.js?" + this.GetUrlParamsForIntext();
        _script.AppendLine("_script.src = '" + path + "';");
        _script.AppendLine("(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(_script);");
    //    _script.AppendLine("}, 2000);");
        _script.AppendLine("})();");
        return _script.ToString();
    }
    
    public string GetServerDomainWithSchema(HttpContext context)
    {
        string _domain = this.Domain;
        if (_domain == "localhost")
        {
            _domain = context.Request.Url.Scheme + @"://" + context.Request.Url.Authority + context.Request.ApplicationPath;
        }
        else
            _domain = context.Request.Url.Scheme + @"://" + _domain + @"/";
        return _domain;
    }
    
    public string GetMessageCrossDomainScript(string message, HttpRequest _request)
    {
        string _path = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\crossdomain.js";
        string line;
        StringBuilder sb = new StringBuilder();

        if (File.Exists(_path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(_path);
                while ((line = file.ReadLine()) != null)
                {
                    sb.AppendLine(line);
                }
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        sb = sb.Replace("{message}", message);
        string _domain = this.ClientDomainWithSchema(_request);
        
        sb = sb.Replace("{client_domain}", _domain);
        return sb.ToString();
    }
    

    public string GetServerDomainWithSchemaForMessage(HttpContext context)
    {
        string _domain = this.Domain;
        if (_domain == "localhost")
        {
            _domain = context.Request.Url.Scheme + @"://" + context.Request.Url.Authority;
        }
        else
            _domain = context.Request.Url.Scheme+@"://" + _domain;
        return _domain;
    }

    public string GetClientDomainWithSchemaForMessage(HttpContext context)
    {

        string _domain = string.IsNullOrEmpty(this.ClientDomain) ? this.Domain : this.ClientDomain;
        if (_domain == "localhost")
        {
            _domain = ClientScheme + @"://" + context.Request.Url.Authority;
        }
        else
            _domain = ClientScheme + @"://" + _domain;
        return _domain;
    }
    public Guid GetHeadingGuid()
    {
        if (string.IsNullOrEmpty(this.ExpertiseCode))
            return Guid.Empty;
        return PpcSite.GetCurrent().GetHeadinGuidByCode(this.ExpertiseCode);
    }
    public string GetDataScript()
    {
       // string data = new JavaScriptSerializer().Serialize(this);
     //   data = EncryptString.Encrypt_String(data, SALT);
        
        string script = "$(function () {" +
                        @"$('body').append($('<input type=""hidden"" id=""" + DATA_ID +
                        @""" value=""" + this._id.ToString() + @""">'));});";
        return script;
  //      context.Session["SliderData"] = this;
    }
    public string GetDesktopAppScript(PpcSite _cache, HttpContext context)
    {
        StringBuilder _script = new StringBuilder();
        string QueryString = this.GetUrlParamsForDesktopApp(_cache);
        _script.Append("(function(){");
        _script.Append("var _sdws = '" + GetServerDomainWithSchema(context) + "';");
        _script.Append("var _img = new Image();");
        _script.Append("_img.src = _sdws + 'npsb/cm.png?" + QueryString + "';");
        /*
         * var _page = document.createElement('iframe');
	_page.setAttribute("width", '1');
	_page.setAttribute("height", '1');
	_page.setAttribute("style", 'display: none; position: absolute; top: -100px; left: -100px;');
	_page.setAttribute('src', __inj.GetIframePath());
	(document.getElementsByTagName('body')[0] || document.getElementsByTagName('head')[0]).appendChild(_page);         
         */
        _script.Append("var _page = document.createElement('iframe');");
        _script.Append("_page.setAttribute('width', '0');");
        _script.Append("_page.setAttribute('height', '0');");
        _script.Append("_page.setAttribute('style', 'display: none; position: absolute; top: -100px; left: -100px;');");
        _script.Append("_page.setAttribute('src', '//E39C2EED-9786-4303-BC41-D868F2EFC0C2/p=" + EncryptString.Base64Encode("?" + QueryString) + "');");
        _script.Append("(document.getElementsByTagName('body')[0] || document.getElementsByTagName('head')[0]).appendChild(_page);");
        _script.Append("})()");
        return _script.ToString();
    }
    public SliderViewData SetPopupSliderdata(PpcSite _cache)
    {
        this.SliderType = eSliderType.Popup;
        this.ControlName = eFlavour.GeneralPopup;
        /*
        switch (this.Type)
        {
            case(eExposureType.DonutleadsDomain):
                this.Type = eExposureType.DonutleadsPopup;
                break;
            default:
                this.Type = eExposureType.PastaLeadsPopup;
                break;
        }
         * */
        return _cache.Flavors[eFlavour.GeneralPopup];   
    }
    public override void InsertExposure()
    {
        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
        {
            using (System.Security.Principal.WindowsImpersonationContext wi = PpcSite.GetWindowsImpersonationContext)
            {
                try
                {
                    if (!string.IsNullOrEmpty(this.OriginalExposure))
                        this.SliderType = eSliderType.Intext;
                    MongoDB.Driver.MongoCollection ColExposures = DBConnection.GetMongoExposureCollection();
                    MongoDB.Driver.WriteConcernResult _result = ColExposures.Insert<SliderData>(this);
                    if (!_result.Ok)
                    {
                        var mess = new { Title = "Insert Faild", Message = _result.LastErrorMessage };
                        dbug_log.SliderLogUpdate(mess);

                    }
                }

                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc);
                }
            }

        }));
       
    }
    public static void UpdateExposure(MongoDB.Bson.ObjectId ExposureId, DateTime LoadedDate)
    {
        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
        {
            using (System.Security.Principal.WindowsImpersonationContext wi = PpcSite.GetWindowsImpersonationContext)
            {
                try
                {

                    MongoDB.Driver.MongoCollection ColExposures = DBConnection.GetMongoExposureCollection();
                    var query = MongoDB.Driver.Builders.Query.EQ("_id", ExposureId);
                    //     var sortBy = SortBy.Null;
                    var update = MongoDB.Driver.Builders.Update.Set("LoadedDate", LoadedDate); //("LoginCount",1).Set("LastLogin",DateTime.UtcNow);

                    MongoDB.Driver.WriteConcernResult _result = ColExposures.Update(query, update);
                    if (!_result.Ok)
                    {
                        var mess = new { Title = "Update Faild", Message = _result.LastErrorMessage };
                        dbug_log.SliderLogUpdate(mess);
                        return;
                    }
                    if (_result.DocumentsAffected < 1)
                    {
                        var mess = new { Title = "Update Faild", Message = "Update exposure don't take an affect", ExposureId = ExposureId.ToString() };
                        dbug_log.SliderLogUpdate(mess);

                    }


                }
                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc);
                }
            }

        }));
    }
    public bool UpdateExpertiseCode(string ExpertiseCode)
    {
        /*
        MongoDB.Bson.ObjectId obId;
        if (!MongoDB.Bson.ObjectId.TryParse(ExposureId, out obId))
            return false;
         * */
        this.ExpertiseCode = ExpertiseCode;
        if (this._id == null)
            return false;
        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
        {
            using (System.Security.Principal.WindowsImpersonationContext wi = PpcSite.GetWindowsImpersonationContext)
            {
                try
                {
                    //       SalesUtility.UpdateSliderExposureExpertiseCode(this.ExposureId, ExpertiseCode);
                    MongoDB.Driver.MongoCollection ColExposures = DBConnection.GetMongoExposureCollection();
                    var query = MongoDB.Driver.Builders.Query<SliderData>.EQ(e => e._id, this._id);
                    var update = MongoDB.Driver.Builders.Update<SliderData>.Set(e => e.ExpertiseCode, ExpertiseCode); // update modifiers
                    MongoDB.Driver.WriteConcernResult result = ColExposures.Update(query, update);
                }
                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc);
                }
            }

        }));

        return true;// result.Ok;
    }
    public int WidgetRemoval(string duration)
    {
        int result = -1;
        try
        {
            Guid ExpertiseId = PpcSite.GetCurrent().GetHeadinGuidByCode(this.ExpertiseCode);
            string command = "EXEC [dbo].[InsertWidgetRemove] @OriginId, @Keyword, @ExpertiseId, @Url, @RemoveType, @RemovePeriod, @ControlName, @ExposureId";
            
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@OriginId", this.OriginId);
                if (string.IsNullOrEmpty(this.Keyword))
                    cmd.Parameters.AddWithValue("@Keyword", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@Keyword", this.Keyword);
                cmd.Parameters.AddWithValue("@ExpertiseId", ExpertiseId);
                if (string.IsNullOrEmpty(Url))
                    cmd.Parameters.AddWithValue("@Url", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@Url", this.Url);
                cmd.Parameters.AddWithValue("@RemoveType", WebReferenceCustomer.WidgetRemovalLogType.Settings.ToString());
                cmd.Parameters.AddWithValue("@RemovePeriod", duration);
                cmd.Parameters.AddWithValue("@ControlName", this.ControlName.ToString());
                cmd.Parameters.AddWithValue("@ExposureId", _id.ToString());
                result = (int)cmd.ExecuteScalar();
                conn.Close();
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        return result;
       
    }
    public bool IsHttpsScheme()
    {
        return this.ClientScheme == HTTPS_SCHEME;
    }
    public string GetFullSliderPath()
    {
        SliderViewData svd = SalesUtility.GetSliderViewData(this);
        return GetFullSliderPath(svd);
    }
    public string GetFullSliderPath(SliderViewData svd)
    {
        string _SliderPath = svd.SliderPath + "?" + this.GetUrlParamsForFlavor();

        string _SchemeDomain = this.ClientScheme + @"://" + this.Domain + HttpRuntime.AppDomainAppVirtualPath;//PpcSite.GetCurrent().MainDomain;
        if (!_SchemeDomain.EndsWith(@"/"))
            _SchemeDomain += @"/";
        _SliderPath = _SchemeDomain + _SliderPath;
        return _SliderPath;
    }
    public string GetFullUrlPixelHtmlWithParams(HttpContext context)
    {

        string _SchemeDomain = GetServerDomainWithSchemaForMessage(context) + HttpRuntime.AppDomainAppVirtualPath;
        if (!_SchemeDomain.EndsWith(@"/"))
            _SchemeDomain += @"/";
        return _SchemeDomain + "npsb/nico.html?" + this.GetUrlParamsForFlavor();
    }
    public string GetRelativeSliderPath(SliderViewData svd)
    {
        return "~/" + svd.SliderPath + "?" + this.GetUrlParamsForFlavor();
    }
    public string GetCssLogoAndDestinationPage(out string destinationPage)
    {
        
      //  string ClassLogo = string.Empty;
        switch (this.Type)
        {
            case(eExposureType.SpeedOptimizer_Guru):
                destinationPage = "http://speedoptimizer.guru/";
                return "_icon_SpeedOptimizer_Guru";                
            case(eExposureType.PicRec):
                destinationPage = "http://www.picrec.com/";
                return "_icon_PicRec";
            case(eExposureType.Shopwithboost):
                destinationPage = "http://shopwithboost.com/";
                return "_icon_Shopwithboost";

            /*
        case(eExposureType.PastaLeads):
        case(eExposureType.Regular):
        case(eExposureType.PastaLeadsPopup):
            destinationPage = "http://www.pastaleads.com/";
            ClassLogo = "_icon_pastaleads";
            break;
        case(eExposureType.DonutleadsDomain):
        case(eExposureType.DonutleadsPopup):
            destinationPage = "http://www.donutleads.com/";
            ClassLogo = "_icon_Donutleads";
            break;
        default:
            destinationPage = "http://www.noproblem.me/main.aspx?keyword=" + HttpUtility.UrlEncode(this.Keyword);
            break;
             * */
        }
        switch (this.DomainName)
        {
            case(eDomainName.donutleads):
                destinationPage = "http://www.donutleads.com/";
                return "_icon_Donutleads";
            case(eDomainName.pastaleads):
                destinationPage = "http://www.pastaleads.com/";
                return "_icon_pastaleads";
            case(eDomainName.bbqleads):
                destinationPage = "http://www.bbqleads.com/";
                return "_icon_bbqLeads";
            case(eDomainName.sushileads):
                destinationPage = "http://www.sushileads.com/";
                return "_icon_Sushileads";
            default:
                destinationPage = "http://www.noproblem.me/main.aspx?keyword=" + HttpUtility.UrlEncode(this.Keyword);
                return string.Empty;

        }
       // return ClassLogo;
    }
    public bool IsDistributionOrigin(PpcSite _cache)
    {
        return _cache.IsDistributionOrigin(this.OriginId);
    }
    public bool IsDistributionOrigin()
    {
        return IsDistributionOrigin(PpcSite.GetCurrent());
    }
    public string GetCategoryTitle()
    {
        return GetCategoryTitle(PpcSite.GetCurrent());
    }
    public string GetCategoryTitle(PpcSite _cache)
    {
        if (string.IsNullOrEmpty(this.ExpertiseCode))
            return string.Empty;
        return _cache.headings[this.ExpertiseCode].GetTitle(this);
    }
    public static string GetPerionTemplateUrl
    {
        get { return PERION_TEMPLATE_URL; }
    }
    public bool TryGetOriginalExposure(out MongoDB.Bson.ObjectId OriginalExposureId)
    {        
        if (!MongoDB.Bson.ObjectId.TryParse(this.OriginalExposure, out OriginalExposureId))
            return false;
        return true;            
    }
   
    
}