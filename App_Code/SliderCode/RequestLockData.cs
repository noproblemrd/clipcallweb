﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.ComponentModel;

/// <summary>
/// Summary description for RequestLockData
/// </summary>
public class RequestLockData
{
    private int _step = 1;
    private string _description;
    private string _ContactFullName;
    private string _FirstName;
    private string _LastName;
    public string description 
    {
        get { return HttpUtility.UrlDecode(_description); }
        set { _description = value; } 
    }
    public string phone { get; set; }
    public int SupplierNum { get; set; }
    public string ZipCode { get; set; }
    public string toZipCode { get; set; }
    public string me { get; set; }
    public string ContactFullName
    {
        get { return HttpUtility.UrlDecode(_ContactFullName); }
        set { _ContactFullName = value; }
    }
    public string FirstName
    {
        get { return HttpUtility.UrlDecode(_FirstName); }
        set { _FirstName = value; }
    }
    public string LastName
    {
        get { return HttpUtility.UrlDecode(_LastName); }
        set { _LastName = value; }
    }
    public MongoDB.Bson.ObjectId ExposureId { get; set; }
    public string email { get; set; }
    public string siteId { get; set; }
    public string subTypeExpertise { get; set; }
    public string expertiseCodeFromList { get; set; }
    public bool? ifGenderFemale { get; set; }
    public string weight { get; set; }
    public string height { get; set; }
    public int heightFeet { get; set; }
    public int heightInches { get; set; }

    public bool? IsFinalStep { get; set; }
    /*
    [DefaultValue(1)]
    public int step { get; set; }
    */

    
    public int step
    {
        get { return _step; }
        set { _step = value; }
    }


    public string size { get; set; }
    public DateTime? date { get; set; }
    public DateTime? dateBirth { get; set; }
    
    public bool actionsCausedIt { get; set; }
    public string doesAnyoneHasVehicleInsurance { get; set; }
    public string estimatedMedicalBills { get; set; }
    public string accidentResult { get; set; }
    public string brokenBones { get; set; }
    public string whiplash { get; set; }
    public string lostLimb { get; set; }      
    public string spinalCordInjuryOrParalysis { get; set; }   
    public string brainInjury { get; set; }      
    public string lossOfLife { get; set; }            
    public string other { get; set; }
    public bool currentlyRepresented { get; set; }
    public bool HeIsFault { get; set; }
    public string status { get; set; }
    public string represented { get; set; }
    public bool represented2 { get; set; }

    public bool currentCharges { get; set; }
    public bool affordAttorney { get; set; }


    public string terms { get; set; }
    public string soon { get; set; }
    public bool live { get; set; }
    public bool beenFiled { get; set; }
    public string financing { get; set; }
    public string income { get; set; }
    public string spouseIncome { get; set; }
    public string property { get; set; }
    public string children { get; set; }        

    public string considering { get; set; } 
    public string bills { get; set; }   
    public string expences { get; set; }
    public bool behindRealEstate { get; set; }      
    public bool behindAutomobile { get; set; }
    public bool assets { get; set; }     
    public string incomeTypes { get; set; }
    public string StreetAddress { get; set; }
    public string budget { get; set; }



    /*********** pay day poperites *************/
    public string accountNumber { get; set; }
    public string accountType { get; set; }
    public string bankName { get; set; }
    public bool directDeposit { get; set; }
    public string drivingLicenseNumber { get; set; }
    public string drivingLicenseState { get; set; }
    public string employer { get; set; }
    public string incomeType { get; set; }
    public bool isActiveMilitary { get; set; }
    public bool? isRent { get; set; }
    public int? monthlyIncome { get; set; }
    public int? monthsAtResidence { get; set; }
    public int? monthsEmployed { get; set; }
    public int monthsWithBank { get; set; }
    public DateTime? nextPayDate { get; set; }
    public string occupation { get; set; }
    public string payPeriod { get; set; }
    public int? requestedLoanAmount { get; set; }
    public string routingNumber { get; set; }
    public DateTime? secondPayDate { get; set; }
    public string socialSecurityNumber { get; set; }
    public string workPhone { get; set; }



    public string StringData { get; set; }

    /*
      public string AccountNumber { get; set; }
        [XmlElement(IsNullable = true)]
        public eAccountType? AccountType { get; set; }
        public string BankName { get; set; }
        [XmlElement(IsNullable = true)]
        public bool? DirectDeposit { get; set; }
        public string DrivingLicenseNumber { get; set; }
        public string DrivingLicenseState { get; set; }
        public string Employer { get; set; }
        [XmlElement(IsNullable = true)]
        public eIncomeType? IncomeType { get; set; }
        [XmlElement(IsNullable = true)]
        public bool? IsActiveMilitary { get; set; }
        [XmlElement(IsNullable = true)]
        public bool? IsRent { get; set; }
        [XmlElement(IsNullable = true)]
        public int? MonthlyIncome { get; set; }
        [XmlElement(IsNullable = true)]
        public int? MonthsAtResidence { get; set; }
        [XmlElement(IsNullable = true)]
        public int? MonthsEmployed { get; set; }
        public int MonthsWithBank { get; set; }
        [XmlElement(IsNullable = true)]
        public DateTime? NextPayDate { get; set; }
        public string Ocupation { get; set; }
        [XmlElement(IsNullable = true)]
        public ePayPeriod? PayPeriod { get; set; }
        [XmlElement(IsNullable = true)]
        public int? RequestedLoanAmount { get; set; }
        public string RoutingNumber { get; set; }
        [XmlElement(IsNullable = true)]
        public DateTime? SecondPayDate { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string WorkPhone { get; set; }
    */







    public RequestLockData()
	{
		//
		
		//
	}
    /*
    public string GetZipCodeByIp(HttpRequest _request, SliderData sd)
    {
        Regex reg_zipcode = new Regex(@"^\d{5}$");
        if (!string.IsNullOrEmpty(ZipCode) && reg_zipcode.IsMatch(ZipCode))
            return ZipCode;
        return SalesUtility.GetZipcodeByIP(Utilities.GetIP(_request), sd);
    }
     * */
    public string GetZipCodeByDetails(HttpRequest _request, SliderData sd)
    {
        /*
        if (!string.IsNullOrEmpty(ZipCode))
            return ZipCode;
         * */
        /*
        if (!string.IsNullOrEmpty(sd.RegionCode))
            return sd.RegionCode;
         * */

        return SalesUtility.GetZipcode(Utilities.GetIP(_request), sd, ZipCode);
    }
    public string GetFullName()
    {
        if (!string.IsNullOrEmpty(FirstName) && !string.IsNullOrEmpty(LastName))
            return FirstName + " " + LastName;
        if (!string.IsNullOrEmpty(ContactFullName))
            return HttpUtility.UrlDecode(ContactFullName);
        if (!string.IsNullOrEmpty(FirstName))
            return FirstName;
        if (!string.IsNullOrEmpty(LastName))
            return LastName;
        return null;
    }
    
    public eHomeImprovmentBudget? eBudget
    {
        get
        {
            string _budget = HttpUtility.UrlDecode(budget);
            if (string.IsNullOrEmpty(_budget))
                return null;
            eHomeImprovmentBudget eResult;
            if (!Enum.TryParse(_budget, out eResult))
                return null;
            return eResult;
        }
    }
    
}