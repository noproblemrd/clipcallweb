﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StorageData
/// </summary>
public class StorageData
{
    public WebReferenceCustomer.eDeliveryStorage? DeliveryStorage { get; set; }
    public WebReferenceCustomer.eFinanceStorage? FinanceStorage { get; set; }
    public WebReferenceCustomer.eHowManyContainers? HowManyContainers { get; set; }
    public int? HowManyContainersSpecify { get; set; }
    public WebReferenceCustomer.ePlanningToUseStorage? PlanningToUseStorage { get; set; }
    public WebReferenceCustomer.eStorageLength? StorageLength { get; set; }
    public int? StorageLengthSpecify { get; set; }
    public WebReferenceCustomer.eTypeOfStorage? TypeOfStorage { get; set; }
    public string StorageZipcode { get; set; }
    public StorageData(string data)
	{
        string[] strs = data.Split(new string[] { ";;;" }, StringSplitOptions.RemoveEmptyEntries);
        if (strs.Length != 7)
            return;
        WebReferenceCustomer.eHowManyContainers _Containers;
        if (Enum.IsDefined(typeof(WebReferenceCustomer.eHowManyContainers), strs[0]))
            _Containers = (WebReferenceCustomer.eHowManyContainers)Enum.Parse(typeof(WebReferenceCustomer.eHowManyContainers), strs[0]);
        else
        {
            int num;
            if (int.TryParse(strs[0], out num))
            {
                HowManyContainersSpecify = num;
                _Containers = WebReferenceCustomer.eHowManyContainers.eight_plus;
            }
            else
                _Containers = WebReferenceCustomer.eHowManyContainers.two_to_four;
        }
        HowManyContainers = _Containers;

        WebReferenceCustomer.eStorageLength _Length;
        if (Enum.IsDefined(typeof(WebReferenceCustomer.eStorageLength), strs[1]))
            _Length = (WebReferenceCustomer.eStorageLength)Enum.Parse(typeof(WebReferenceCustomer.eStorageLength), strs[1]);
        else 
        {
            int num;
            if (int.TryParse(strs[1], out num))
            {
                StorageLengthSpecify = num;
                _Length = WebReferenceCustomer.eStorageLength.other;
            }
            else
                _Length = WebReferenceCustomer.eStorageLength.not_sure;
        }
        StorageLength = _Length;

        WebReferenceCustomer.eTypeOfStorage _StorageType;
        if(Enum.TryParse(strs[2], out _StorageType))
            TypeOfStorage = _StorageType;

        WebReferenceCustomer.ePlanningToUseStorage _Using;
        if(Enum.TryParse(strs[3], out _Using))
            PlanningToUseStorage = _Using;

        WebReferenceCustomer.eFinanceStorage _finance;
        if(Enum.TryParse(strs[4], out _finance))
            FinanceStorage = _finance;

        WebReferenceCustomer.eDeliveryStorage _When;
        if(Enum.TryParse(strs[5], out _When))
            DeliveryStorage = _When;
        StorageZipcode = strs[6];
	}
    public WebReferenceCustomer.ServiceRequestStorageData GetCrmData
    {
        get
        {
            WebReferenceCustomer.ServiceRequestStorageData data = new WebReferenceCustomer.ServiceRequestStorageData();
            data.DeliveryStorage = DeliveryStorage;
            data.FinanceStorage = FinanceStorage;
            data.HowManyContainers = HowManyContainers;
            data.HowManyContainersSpecify = HowManyContainersSpecify;
            data.PlanningToUseStorage = PlanningToUseStorage;
            data.StorageLength = StorageLength;
            data.StorageLengthSpecify = StorageLengthSpecify;
            data.TypeOfStorage = TypeOfStorage;
            data.ZipCodeOther = StorageZipcode;
            return data;
        }
    }
}