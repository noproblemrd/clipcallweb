﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SliderViewData
/// </summary>
[Serializable()]
public class SliderViewData
{
    public int height { get; private set; }
    public int width { get; private set; }
    public string CssClass { get; private set; }
    public int IframeWidth { get; private set; }
    public int IframeHeight { get; private set; }
    public string SliderPath { get; private set; }
    public string Script { get; private set; }
    public string StylePath { get; private set; }
    public eFlavour FlavorName { get; private set; }
    public eBehaviour Behaviour { get; private set; }
    public bool LoadScript { get; set; }
    public string GoogleStyle { get; set; }
    public int GoogleHeight { get; private set; }
    public int GoogleWidth { get; private set; }
    public string InnerClass { get; private set; }

    public string AlternativeTitle { get; private set; }

	public SliderViewData()
	{
       
	}

    public SliderViewData(eFlavour name)
    {
        string command = "EXEC dbo.GetSliderDetailsByName @Name";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@Name", name.ToString());
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                this.CssClass = (string)reader["class"];
                this.height = (int)reader["height"];
                this.width = (int)reader["width"];
                this.IframeWidth = (int)reader["IframeWidth"];
                this.IframeHeight = (int)reader["IframeHeigth"];
                this.SliderPath = (string)reader["Path"];
                this.Script = (string)reader["Script"];
                this.StylePath = (reader["Style"] == DBNull.Value) ? string.Empty : (string)reader["Style"];
                this.GoogleStyle = (reader["GoogleStyle"] == DBNull.Value) ? string.Empty : (string)reader["GoogleStyle"];
                int _GoogleHeigth = (reader["GoogleHeigth"] == DBNull.Value) ? 0 : (int)reader["GoogleHeigth"];
                int _GoogleWidth = (reader["GoogleWidth"] == DBNull.Value) ? 0 : (int)reader["GoogleWidth"];
                this.GoogleHeight = (_GoogleHeigth == 0) ? this.height : _GoogleHeigth;
                this.GoogleWidth = (_GoogleWidth == 0) ? this.width : _GoogleWidth;
                this.InnerClass = (reader["InnerClass"] == DBNull.Value) ? null : (string)reader["InnerClass"];
                this.FlavorName = name;
                ////this.Behaviour = (eBehaviour)Enum.Parse(typeof(eBehaviour), reader["behaviour"].ToString());
                //TODO HARDCORE
                if (name == eFlavour.Flavor_Half_Controls_Top_Payday_yosto)
                    this.AlternativeTitle = "Apply below for a loan from £200 to £1,000";

            }
            conn.Close();
        }
        
        //IsPhoneDid = (name == eFlavour.Phone_Did_Top || name == eFlavour.Phone_Did_Top_Dentist_A || name == eFlavour.Phone_Did_Top_Dentist_B);
        LoadScript = !string.IsNullOrEmpty(this.Script) && !this.Script.Contains(@"\flavours\");
    }
    public SliderViewData ToCJCampain(HeadingCampain hc)
    {
        SliderViewData newsvd = new SliderViewData();
        newsvd.height = hc.BannerHeight;
        newsvd.width = hc.BannerWidth;
        newsvd.IframeHeight = hc.BannerHeight;
        newsvd.IframeWidth = hc.BannerWidth;
        newsvd.Script = this.Script;
        newsvd.SliderPath = this.SliderPath;
        newsvd.Behaviour = this.Behaviour;
        newsvd.CssClass = this.CssClass;
        newsvd.FlavorName = this.FlavorName;
        return newsvd;
    }
}