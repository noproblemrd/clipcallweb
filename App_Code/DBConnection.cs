using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;

/// <summary>
/// Summary description for DBConnection
/// </summary>
public enum LiteralType { Regular, Explanation }
public class DBConnection
{    
    private const string RECORD_KEY = "RecordCalls";
    SqlConnection conn;
    SqlDataReader reader;
    static string MongoDB_ConnectionString;
    static string MongoDB_ExposureTable;
    static string MongoDB_PopupExposureTable;
    static string MongoDB_CacheTable;
    static string MongoDB_DataBase;
    static string MongoDB_LandedOnJoinPageCollection;
    static string CONNECTION_STRING;
    static string CONNECTION_STRING_SALES;
    static string CONNECTION_STRING_CLIPCALL;
	public DBConnection()
	{
        conn = GetConnString();
		
	}
    static DBConnection()
    {
        CONNECTION_STRING = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        CONNECTION_STRING_SALES = ConfigurationManager.ConnectionStrings["Sales"].ConnectionString;
        CONNECTION_STRING_CLIPCALL = ConfigurationManager.ConnectionStrings["ClipCall"].ConnectionString;
        MongoDB_ConnectionString = ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString;
        MongoDB_DataBase = ConfigurationManager.AppSettings["MongoDB"];
        MongoDB_ExposureTable = ConfigurationManager.AppSettings["ExposureTableMongoDB"];
        MongoDB_PopupExposureTable = ConfigurationManager.AppSettings["PopupExposureTableMongoDB"];
        MongoDB_CacheTable = ConfigurationManager.AppSettings["CacheCollection"];
        MongoDB_LandedOnJoinPageCollection = ConfigurationManager.AppSettings["JoinPageLandedMongoDB"];
        
    }
    
   

    public static SqlConnection GetConnString()
    {
        
     //   string connString = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;


        return new SqlConnection(CONNECTION_STRING);
    }
    public static SqlConnection GetClipCallConnString()
    {
        return new SqlConnection(CONNECTION_STRING_CLIPCALL);
    }
    public static SqlConnection GetSalesConnString()
    {
       // return new SqlConnection("Data Source=10.154.29.6;Initial Catalog=DynamicNoProblem;Persist Security Info=True;User ID=crmdb;Password=Pass@word1");
        return new SqlConnection(CONNECTION_STRING_SALES);
    }
    public static SqlConnection GetDynamicNoProblemQAConnString()
    {
        return new SqlConnection("Data Source=10.154.29.6;Initial Catalog=DynamicNoProblemQA;Persist Security Info=True;User ID=crmdb;Password=Pass@word1");
    }
    
    public static Dictionary<string, string> GetTranslate(string PageName, int SiteLangId, LiteralType lt)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        string command = "EXEC dbo.GetTranslateByPageAndType @PageName, @SiteLangId, @LiteralType";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PageName", PageName);
            cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
            cmd.Parameters.AddWithValue("@LiteralType", lt.ToString());
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string controlID = (string)reader["ControlId"];
                if (dic.ContainsKey(controlID) || !reader.IsDBNull(1))
                    continue;
                dic.Add(controlID, ((string)reader["Translate"]));
            }
            conn.Close();
        }
        return dic;
    }
     //public static void LoadTranslateExplanationToControl(Control _control, string PageName, int SiteLangId)
    public static void LoadTranslateToControl(Control _control, string PageName, int SiteLangId)
    {
        
        Dictionary<string, string> dic = new Dictionary<string, string>();
        Dictionary<string, Dictionary<string, string>> dicControl = new Dictionary<string, Dictionary<string, string>>();
        Dictionary<string, string> dic_explanation = new Dictionary<string, string>();
        
        string command = "EXEC dbo.GetTranslateByPage @PageName, @SiteLangId";

        try
        {
            using (SqlConnection conn = GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@PageName", PageName);
                cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string controlId = (string)reader["ControlId"];
                    string _LiteralType = (string)reader["LiteralType"];
                    if (_LiteralType == LiteralType.Regular.ToString())
                    {
                        if (!reader.IsDBNull(1))
                        {
                            if (!dicControl.ContainsKey(controlId))
                                dicControl.Add(controlId, new Dictionary<string, string>());
                            string controlValue = (reader.IsDBNull(3)) ? (string)reader["Translate"] :
                                (string)reader["ControlValue"];
                            dicControl[controlId].Add(controlValue, (string)reader["Translate"]);

                        }
                        else
                            dic.Add(controlId, ((string)reader["Translate"]));
                    }
                    else
                    {
                        string controlID = (string)reader["ControlId"];
                        if (dic.ContainsKey(controlID) || !reader.IsDBNull(1))
                            continue;
                        dic_explanation.Add(controlID, ((string)reader["Translate"]));
                    }
                }
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            return;
        }
//        if (PageName == "Mycalls.aspx")
//            dbug_log.UserLog((PageSetting)_control);
        Dictionary<string, bool> dic_Controls_explanation = GetLiteralsControls(PageName, LiteralType.Explanation);
        TranslateListControl tlc = new TranslateListControl(_control, dicControl);
        tlc.StartTo();
        InsertTranslate it = new InsertTranslate(_control, dic);
        it.StartTo();
    //    if (_control.GetType().IsSubclassOf(typeof(PageSetting)))
    //    {
            ExplanationControls exp = new ExplanationControls(_control, dic_explanation, dic_Controls_explanation);
            exp.StartInsertExplanation();
    //    }
      
    }
    public static Dictionary<string, string> GetTranslate(string PageName, int SiteLangId)
    {
        return GetTranslate(PageName, SiteLangId, LiteralType.Regular);
    }
    public static Dictionary<string, bool> GetLiteralsControls(string PageName, LiteralType lt)
    {
        Dictionary<string, bool> dic = new Dictionary<string, bool>();
        string command = "EXEC dbo.GetSentencesByPageNameId @pageNameId, @Type";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@pageNameId", PageName);
            cmd.Parameters.AddWithValue("@Type", lt.ToString());
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                dic.Add((string)reader["ControlId"], false);
            }
            conn.Close();
        }
        return dic;
    }
    public void CloseReaderTranslate()
    {
        conn.Close();
        
    }
    public SqlDataReader Reader
    {
        get { return reader; }
    }
    public SqlConnection Conn
    {
        get { return this.conn; }
    }
    public static string GetSymbol(string siteId)
    {
        string symb = "";
        string command = "SELECT dbo.GetCurrencySymbolBySiteNameId(@SiteId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteId", siteId);
            symb = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return symb;
    }
    public static string GetTextLogin(string siteId)
    {
        string textLogin = "";
        string command = "SELECT dbo.GetTextLoginBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteId);
            textLogin = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return textLogin;
    }
    public static string GetTextPaymentTransfer(string siteId)
    {
        string _text = "";
        string command = "SELECT dbo.GetTextPaymentTransferBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteId);
            _text = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return _text;
    }
    public static string GetTextCallPurchas(string siteId)
    {
        string _text = "";
        string command = "SELECT dbo.GetTextCallPurchasBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteId);
            _text = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return _text;
    }
    public static string GetTextCreditCard(string siteId)
    {
        string _text = "";
        string command = "SELECT dbo.GetTextPaymentCreditCardBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteId);
            _text = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return _text;
    }
    //to check if in used
    public static string GetLinkToSet(string ControlId, string sl, string siteId)
    {
        string result;
        string command = "SELECT dbo.GetFirstLinkToSetByControlIdSecurityName(@ControlId, "+
                    "@SecurityName, @SiteNameId)";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ControlId", ControlId);
            cmd.Parameters.AddWithValue("@SecurityName", sl);
            cmd.Parameters.AddWithValue("@SiteNameId", siteId);
            result = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return (string.IsNullOrEmpty(result))?"#":result;
    }
    public static Dictionary<string, string> GetLinksToSet(string siteId, string SecurityLevel)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        string command = "EXEC dbo.GetFirstLinkToSet @SecurityName, @SiteNameId";
        using(SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SecurityName", SecurityLevel);
            cmd.Parameters.AddWithValue("@SiteNameId", siteId);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string _controlId = (string)reader["ControlParent"];
                string _link = (string)reader["Link"];
                if (_controlId == "a_Advertisers" && string.IsNullOrEmpty(_link))
                    _link = "PublisherWelcome.aspx";
                if (string.IsNullOrEmpty(_link))
                    _link = "javascript:void(0);";
                dic.Add(_controlId, _link);
            }
            conn.Close();
        }
        return dic;
    }
    public static string GetSrc(string _host)
    {
        string _src = string.Empty;
        string siteId = string.Empty;
        string command = "EXEC dbo._GetSiteIframeDetailsByHostName @Host";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@Host", _host);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    siteId = (string)reader["SiteNameId"];
                    _src = (reader.IsDBNull(1)) ? "" : reader.GetString(1);
                }
                conn.Close();
            }
            catch (Exception ex) { }
        }
        if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(_src))
            return string.Empty;
        string result = siteId + _src;
        return result;
    }

    public static string GetSiteId(string _host)
    {        
        string siteId = string.Empty;
        string command = "EXEC dbo._GetSiteIframeDetailsByHostName @Host";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@Host", _host);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    siteId = (string)reader["SiteNameId"];
                }
                conn.Close();
            }
            catch (Exception ex) { }
        }
        if (string.IsNullOrEmpty(siteId))
            return string.Empty;
        string result = siteId;
        return result;
    }

    public static string GetSiteNameIdByHost(string _host)
    {
        string siteId = string.Empty;
 
        string command = "EXEC dbo._GetSiteIframeDetailsByHostName @Host";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@Host", _host);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    siteId = (string)reader["SiteNameId"];
                }
                conn.Close();
            }
            catch (Exception ex) { }
        }
        return siteId;

    }

    public static string GetAdvertiserLogo(Guid _id, string SiteNameId)
    {
        string User_Id;
        string command = "SELECT dbo.GetAdvertiserLogo(@UserId, @SiteNameId)";
        using (SqlConnection conn = GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@UserId", _id);
                cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
                User_Id = (string)cmd.ExecuteScalar();
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                return string.Empty;
            }
        }
        return User_Id;
    }
    public static Dictionary<string, PanelValidate> GetPanelListDisplay(string SiteNameId, int SiteLangId, string PageName)
    {
        Dictionary<string, PanelValidate> dic = new Dictionary<string, PanelValidate>();
  //      Dictionary<string, string> dic_temp = new Dictionary<string, string>();
        string command = "EXEC dbo.GetPanelShowBySiteNameIdPageName @SiteNameId, @SiteLangId, @PageName";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
            cmd.Parameters.AddWithValue("@PageName", PageName);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string _onblur = (reader.IsDBNull(8)) ? string.Empty : reader.GetString(8);
                int _level = (reader.IsDBNull(14)) ? -1 : (int)reader["AutoCompleteLevel"];
                string controlParentId = (reader.IsDBNull(15)) ? string.Empty : (string)reader["ParentControlId"];
                dic.Add((string)reader["ControlId"], new PanelValidate((bool)reader["Show"], (bool)reader["Valid"],
                    (bool)reader["AutoComplete"], _onblur, (bool)reader["HasAutoComplete"],
                    _level, controlParentId));

            }

            conn.Close();
        }
        return dic;
    }
    public static string GetUrlWebReference(string SiteNameId)
    {
        string result = "";
        string command = "SELECT dbo.GetUrlWebReferenceBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            result = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return result;
    }
    public static string GetPhoneRegularExpression(string SiteNameId)
    {
        string result = "";
        string command = "SELECT dbo.GetPhoneRegularExpressionBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
                result = (string)cmd.ExecuteScalar();
                conn.Close();
            }
            catch (Exception ex)
            {
                dbug_log.ExceptionLog(ex, SiteNameId);
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }
        }
        return result;
    }
    public static string GetZipcodeRegularExpression()
    {
        return @"^\d{5}$";
    }
    public static string GetPhoneRegularExpression_UK()
    {
        return @"^\(?(020[7,8]{1}\)?[ ]?[1-9]{1}[0-9]{2}[ ]?[0-9]{4})|(0[1-8]{1}[0-9]{3}\)?[ ]?[0-9]{3}[ ]?[0-9]{3})$";

    }
    public static string GetPhoneRegularExperssion_USA_and_UK()
    {
        return @"(^[+]?[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4})|(^\(?(020[7,8]{1}\)?[ ]?[1-9]{1}[0-9]{2}[ ]?[0-9]{4})|(0[1-8]{1}[0-9]{3}\)?[ ]?[0-9]{3}[ ]?[0-9]{3}))$";
    }
    public static string GetZipcodeRegularExpression_UK()
    {
        //return @"^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$";
       // return @"^?(([BEGLMNSWbeglmnsw][0-9][0-9]?)|(([A-PR-UWYZa-pr-uwyz][A-HK-Ya-hk-y][0-9][0-9]?)|(([ENWenw][0-9][A-HJKSTUWa-hjkstuw])|([ENWenw][A-HK-Ya-hk-y][0-9][ABEHMNPRVWXYabehmnprvwxy])))) ?[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$";
        return @"^[A-Z]{1}((\d{1})|([A-Z0-9]{2})|([A-Z]{1}[A-Z0-9]{2}))([ -]{1}\d{1}[A-Z]{2})?$";
    }
    public static bool LoadSiteId(string host, PageSetting ps)
    {
        bool res = false;  
        if (!string.IsNullOrEmpty(host))
        {
            string siteId = string.Empty;
            int SiteLangId = -1;
            string command = "EXEC dbo.GetSiteNameIdDefaultLangByHostName @Host";
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@Host", host);
                SqlDataReader reader = cmd.ExecuteReader();
                
                if (reader.Read())
                {
                    siteId = (string)reader["SiteNameId"];
                    SiteLangId = (int)reader["LanguageId"];
                }

                conn.Close();
            }
            if (!string.IsNullOrEmpty(siteId))
            {
                res = true;
                ps.siteSetting = new SiteSetting(siteId, SiteLangId);
                ps.siteSetting.SetSiteSetting(ps);
            }
        }
        return res;
    }
    public static SiteSetting LoadSiteId(string host)
    {
        if (!string.IsNullOrEmpty(host))
        {
            string siteId = string.Empty;
            int SiteLangId = -1;
            string command = "EXEC dbo.GetSiteNameIdDefaultLangByHostName @Host";
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@Host", host);
                SqlDataReader reader = cmd.ExecuteReader();
                
                if (reader.Read())
                {
                    siteId = (string)reader["SiteNameId"];
                    SiteLangId = (int)reader["LanguageId"];
                }

                conn.Close();
            }
            if (!string.IsNullOrEmpty(siteId))
            {
                return new SiteSetting(siteId, SiteLangId);
            }
        }
        return null;
    }
    public static int GetSiteLangIdByUserId(Guid _id, string SiteNameId)
    {
        int SiteLangId = -1;
        string command = "EXEC dbo.GetSiteLangIdByUserId @UserId, @SiteNameId";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", _id);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            SiteLangId = (int)cmd.ExecuteScalar();
            conn.Close();
        }
        return SiteLangId;
    }
    public static bool IsDashboardHomePage(Guid _id, string SiteNameId)
    {
        bool result = false;
        string command = "SELECT dbo.GetDashboardAsHomePage(@SiteNameId, @UserId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            cmd.Parameters.AddWithValue("@UserId", _id);
            result = (bool)cmd.ExecuteScalar();
            conn.Close();
        }
        return result;
    }
   
    public static string GetCultureBySiteLangId(int SiteLangId)
    {
        string command = "SELECT dbo.GetCultureBySiteLangId(@SiteLangId)";
        using (SqlConnection conn = GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
                string result = (cmd.ExecuteScalar() == DBNull.Value) ? string.Empty : (string)cmd.ExecuteScalar();
                conn.Close();
                return result;

            }
            catch (Exception Exc)
            {
                dbug_log.ExceptionLog(Exc, "SiteLangId= " + SiteLangId);
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }
        }
        return string.Empty;
    }
    public static bool IfSiteRecord(string WebReferenceURL)
    {
        bool CanRecord = false;
        string result = string.Empty;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(WebReferenceURL);
        try
        {
            result = _site.GetConfigurationSettings();
        }
        catch (Exception ex)
        {
            return false;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["Configurations"] == null || xdd["Configurations"]["Error"] != null)
        {
            return false;
        }
        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == RECORD_KEY)
            {
                CanRecord = (node["Value"].InnerText == "1") ? true : false;
                break;
            }
        }
        return CanRecord;
    }
    public static string GetMobileRegularExpression(string SiteNameId)
    {
        string mobile = string.Empty;
        string command = "SELECT dbo.GetMobileRegularExpression(@SiteNameId)";
        using (SqlConnection conn = GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
                if (cmd.ExecuteScalar() != DBNull.Value)
                    mobile = (string)cmd.ExecuteScalar();
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }
        }
        return mobile;
    }
    public static RegionTab GetRegionTab(string SiteNameId)
    {
        string result = string.Empty;
        string command = "SELECT dbo.GetRegionTabTypeBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
                if (cmd.ExecuteScalar() != DBNull.Value)
                    result = (string)cmd.ExecuteScalar();
                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }
        }
        foreach (RegionTab rt in Enum.GetValues(typeof(RegionTab)))
        {
            if (rt.ToString() == result)
                return rt;
        }
        return RegionTab.Map;
    }
    
   
    public static Guid GetOrigionId(string SiteNameId)
    {
        Guid _id = Guid.Empty;
        string command = "EXEC dbo.GetOrigionIdBySiteNameId @SiteNameId";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            _id = (Guid)cmd.ExecuteScalar();
            conn.Close();
        }
        return _id;
    }
    public static bool IfSiteLangIdExists(int SiteLangId, string SiteNameId)
    {
        bool result = false;
        string command = "SELECT dbo.IfSiteLangIdExists(@SiteLangId, @SiteNameId)";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            result = (bool)cmd.ExecuteScalar();
            conn.Close();
        }
        return result;
    }

    public static SortedDictionary<string, string> GetCities(string tableCities)
    {
        SortedDictionary<string, string> list = new SortedDictionary<string, string>();
        using (SqlConnection conn = GetConnString())
        {
            string tableYpCities = "";
            if (tableCities == "d")
                tableYpCities = "tbl_ypPublisher_codeCities";
            else if (tableCities == "noproblem")
                tableYpCities = "tbl_ypAffiliate_codeCities";
            else
                return GetCitiesYLP();// tableYpCities = "tbl_ypAffiliate_codeCities";
            string command = "EXEC dbo.getYpCities @tableCities";
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@tableCities", tableYpCities);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if (!reader.IsDBNull(1))
                {
                    list.Add((string)reader["City"], (string)reader["Guid"]);
                }
            }
            conn.Close();
        }
        return list;
    }
    public static SortedDictionary<string, string> GetCitiesYLP()
    {
        SortedDictionary<string, string> list = new SortedDictionary<string, string>();
        string command = "EXEC dbo.GetCities_ypAffiliate";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {

                list.Add((string)reader["City"], (string)reader["Id"]);

            }
            conn.Close();
        }
        return list;
    }
    public static string GetPaymentImage(string SiteId)
    {
        string _img;
        string command = "SELECT dbo.GetPaymentImageBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteId);
            _img = (cmd.ExecuteScalar() == DBNull.Value) ? string.Empty : (string)cmd.ExecuteScalar();
            conn.Close();
        }
        if(string.IsNullOrEmpty(_img))
            return _img;
        string _url = System.Configuration.ConfigurationManager.AppSettings["professionalRecordWeb"];
        _url += _img;
        return _url;       
    }
    public static string GetHomePage(String SiteId)
    {
        string _HomePage = string.Empty;
        string command = "SELECT dbo.GetHomePageBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteId);
            _HomePage = (cmd.ExecuteScalar() == DBNull.Value) ? string.Empty : (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return _HomePage;  
    }
    public static void InsertLogin(string email, Guid UserId, string SiteId, eUserType _type)
    {
        string command = "EXEC dbo.InsertLogin @UserName, @SiteNameId, @UserId, @IP, @Success, @AccountType";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserName", email);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteId);
            cmd.Parameters.AddWithValue("@IP", Utilities.GetIP(HttpContext.Current.Request));
            cmd.Parameters.AddWithValue("@AccountType", _type.ToString());
            if (UserId == Guid.Empty)
            {
                cmd.Parameters.AddWithValue("@UserId", DBNull.Value);
                cmd.Parameters.AddWithValue("@Success", false);
            }
            else
            {
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@Success", true);
            }
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
    
    public static void InsertLogin(string email, Guid _id, SiteSetting siteSetting, eUserType _type)
    {
        InsertLogin(email, _id, siteSetting.GetSiteID, _type);
        
    }
   
    public static bool IsLoginApproved(string UserName, string SiteNameId)
    {
        bool IsApprove = false;
        string command = "SELECT dbo.GetLogInApproved(@UserName, @SiteNameId, @IP)";
        using (SqlConnection _conn = DBConnection.GetConnString())
        {
            _conn.Open();
            SqlCommand _cmd = new SqlCommand(command, _conn);
            _cmd.Parameters.AddWithValue("@UserName", UserName);
            _cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            _cmd.Parameters.AddWithValue("@IP", Utilities.GetIP(HttpContext.Current.Request));
            IsApprove = (bool)_cmd.ExecuteScalar();
            _conn.Close();
        }
        return IsApprove;
    }
    public static bool IsLoginApproved(string email, SiteSetting siteSetting)
    {
        return IsLoginApproved(email, siteSetting.GetSiteID);
    }
    public static bool IsSiteExsist(string _siteId)
    {
        bool result = false;
        using (SqlConnection conn = GetConnString())
        {
            string command = "SELECT dbo.GetSiteIdBySiteName(@SiteNameId)";
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", _siteId);
            result = cmd.ExecuteScalar() != DBNull.Value;
            conn.Close();
        }
        return result;
    }


    public static Dictionary<string, string> GetExpertiseDetails(string Keyword)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        //      Dictionary<string, string> dic_temp = new Dictionary<string, string>();
        string command = "EXEC dbo.GetExpertiseCode @Keyword";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@Keyword", Keyword);

            SqlDataReader reader = cmd.ExecuteReader();
            string expertise = "";
            string expertiseSingular = "";
            string expertisePlural = "";
            string code = "";
            string watermark = "";
            string imageBack = "";

            while (reader.Read())
            {
                expertise = (DBNull.Value.Equals(reader["expertise"]) ? "" : (string)reader["expertise"]);
                expertiseSingular = (DBNull.Value.Equals(reader["expertiseSingular"]) ? "" : (string)reader["expertiseSingular"]);
                expertisePlural = (DBNull.Value.Equals(reader["expertisePlural"]) ? "" : (string)reader["expertisePlural"]);
                code = (DBNull.Value.Equals(reader["code"]) ? "" : (string)reader["code"]);
                watermark = (DBNull.Value.Equals(reader["watermark"]) ? "" : (string)reader["watermark"]);
                imageBack = (DBNull.Value.Equals(reader["imageBack"]) ? "" : (string)reader["imageBack"]);
            }

            dic.Add("expertise", expertise.Trim());
            dic.Add("expertiseSingular", expertiseSingular.Trim());
            dic.Add("expertisePlural", expertisePlural.Trim());
            dic.Add("code", code.Trim());
            dic.Add("watermark", watermark.Trim());
            dic.Add("imageBack", imageBack.Trim());

            conn.Close();
        }
        return dic;
    }

    public static Dictionary<string, string> GetExpertiseDetails2(string expertiseName)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        //      Dictionary<string, string> dic_temp = new Dictionary<string, string>();
        string command = "EXEC dbo.GetExpertiseDetails @expertise";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@expertise", expertiseName);

            SqlDataReader reader = cmd.ExecuteReader();
            string expertise = "";
            string expertiseSingular = "";
            string expertisePlural = "";
            string code = "";
            string watermark = "";
            string imageBack = "";

            while (reader.Read())
            {
                expertise = (DBNull.Value.Equals(reader["expertise"]) ? "" : (string)reader["expertise"]);
                expertiseSingular = (DBNull.Value.Equals(reader["expertiseSingular"]) ? "" : (string)reader["expertiseSingular"]);
                expertisePlural = (DBNull.Value.Equals(reader["expertisePlural"]) ? "" : (string)reader["expertisePlural"]);
                code = (DBNull.Value.Equals(reader["code"]) ? "" : (string)reader["code"]);
                watermark = (DBNull.Value.Equals(reader["watermark"]) ? "" : (string)reader["watermark"]);
                imageBack = (DBNull.Value.Equals(reader["imageBack"]) ? "" : (string)reader["imageBack"]);
            }

            dic.Add("expertise", expertise.Trim());
            dic.Add("expertiseSingular", expertiseSingular.Trim());
            dic.Add("expertisePlural", expertisePlural.Trim());
            dic.Add("code", code.Trim());
            dic.Add("watermark", watermark.Trim());
            dic.Add("imageBack", imageBack.Trim());

            conn.Close();
        }
        return dic;
    }


    public static Dictionary<string, string> GetExpertiseDetails3(string expertiseCode)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        //      Dictionary<string, string> dic_temp = new Dictionary<string, string>();
        string command = "EXEC dbo.GetExpertiseDetails2 @expertiseCode";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@expertiseCode", expertiseCode);

            SqlDataReader reader = cmd.ExecuteReader();
            string expertise = "";
            string expertiseSingular = "";
            string expertisePlural = "";
            string code = "";
            string watermark = "";
            string imageBack = "";

            while (reader.Read())
            {
                expertise = (DBNull.Value.Equals(reader["expertise"]) ? "" : (string)reader["expertise"]);
                expertiseSingular = (DBNull.Value.Equals(reader["expertiseSingular"]) ? "" : (string)reader["expertiseSingular"]);
                expertisePlural = (DBNull.Value.Equals(reader["expertisePlural"]) ? "" : (string)reader["expertisePlural"]);
                code = (DBNull.Value.Equals(reader["code"]) ? "" : (string)reader["code"]);
                watermark = (DBNull.Value.Equals(reader["watermark"]) ? "" : (string)reader["watermark"]);
                imageBack = (DBNull.Value.Equals(reader["imageBack"]) ? "" : (string)reader["imageBack"]);
            }

            dic.Add("expertise", expertise.Trim());
            dic.Add("expertiseSingular", expertiseSingular.Trim());
            dic.Add("expertisePlural", expertisePlural.Trim());
            dic.Add("code", code.Trim());
            dic.Add("watermark", watermark.Trim());
            dic.Add("imageBack", imageBack.Trim());

            conn.Close();
        }
        return dic;
    }

    
   
    public static string GetDomainBySiteId(string SiteId)
    {
        if (string.IsNullOrEmpty(SiteId))
            return string.Empty;
        string domain;
        string command = "SELECT dbo.GetUrlBySiteNameId(@SiteId)";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteId", SiteId);
            domain = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return domain;
    }
    public static bool IsValidSiteId(string SiteId)
    {
        bool result = false;
        string command = "SELECT dbo.IsSiteIdExist(@SiteNameId)";
        using (SqlConnection conn = GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteId);
            result = (bool)cmd.ExecuteScalar();
            conn.Close();
        }
        return result;
    }
    public static MongoDB.Driver.MongoDatabase GetMongoDatabase()
    {
        MongoDB.Driver.MongoClient client = new MongoDB.Driver.MongoClient(MongoDB_ConnectionString);

        MongoDB.Driver.MongoServer server = client.GetServer();
        return server.GetDatabase(MongoDB_DataBase);
    }
    public static MongoDB.Driver.MongoCollection GetMongoExposureCollection()
    {
 //       DBConnection _db = new DBConnection();
        return GetMongoDatabase().GetCollection(MongoDB_ExposureTable);
    }
    public static MongoDB.Driver.MongoCollection GetMongoPopupExposureCollection()
    {
        //       DBConnection _db = new DBConnection();
        return GetMongoDatabase().GetCollection(MongoDB_PopupExposureTable);
    }
    public static MongoDB.Driver.MongoCollection GetMongoCacheCollection()
    {
        //       DBConnection _db = new DBConnection();
        return GetMongoDatabase().GetCollection(MongoDB_CacheTable);
    }
    public static MongoDB.Driver.MongoCollection GetMongoLandedOnJoinPageCollection()
    {
        //       DBConnection _db = new DBConnection();
        return GetMongoDatabase().GetCollection(MongoDB_LandedOnJoinPageCollection);
    }
}
