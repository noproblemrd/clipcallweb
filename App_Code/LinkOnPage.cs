﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for LinkOnPage
/// </summary>
public class LinkOnPage : ScannerControls
{    
    Dictionary<string, string> controlsId;
    public LinkOnPage(Control _control, Dictionary<string, string> controlId) :
        base(_control)
	{
        this.controlsId = controlId;
	}
    protected void SetLinksToPage(Control control)
    {
        if (!string.IsNullOrEmpty(control.ID) && controlsId.ContainsKey(control.ID))
        {
            control.Visible = true;
            if (control.GetType() == (typeof(HtmlAnchor)))
                ((HtmlAnchor)control).HRef = controlsId[control.ID];
            else if (control.GetType() == (typeof(HyperLink)))
                ((HyperLink)control).NavigateUrl = controlsId[control.ID];
        }
        
    }
    public void StartTo()
    {
        if (controlsId == null || controlsId.Count == 0)
            return;
        myFunc func = new myFunc(SetLinksToPage);
        ApplyToControls(_control, func);
    }
}
