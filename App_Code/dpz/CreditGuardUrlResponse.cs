﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CreditGuardUrlResponse
/// </summary>
[Serializable()]
public class CreditGuardUrlResponse
{
    public string Url { get; set; }
    public string txId { private get; set; }
    public string Status { get; set; }
    public int PaymentId { private get; set; }
    public string Msg { get; set; }
	public CreditGuardUrlResponse()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string Get_txId()
    {
        return txId;
    }
    public int Get_PaymentId()
    {
        return PaymentId;
    }
}