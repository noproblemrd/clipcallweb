﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TokenAmount
/// </summary>
public enum eTokenResponse { InvalidBizId, InvalidtxId, ServerProblem }
public class TokenAmount
{
    private int SiteLangId;
    private string _msg;
    public string Token { get; set; }
    public int Amount { get; set; }
    public eTokenResponse eResponse { get; set; }
    public string Msg 
    {
        get
        {
            if (string.IsNullOrEmpty(_msg))
                return HttpUtility.JavaScriptStringEncode(EditTranslateDB.GetControlTranslate(SiteLangId, "eTokenResponse", eResponse.ToString()));
            return HttpUtility.JavaScriptStringEncode(_msg);
        }
        set
        {
            _msg = value;
        }
    }
	public TokenAmount(int SiteLangId)
	{
        this.SiteLangId = SiteLangId;
        Amount = -1;
	}
}