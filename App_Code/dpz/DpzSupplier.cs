﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DpzSupplier
/// </summary>
[Serializable()]
public class DpzSupplier
{
    public Guid SupplierId { get; set; }
    public string BizId { get; set; }
    public bool IsActive { get; set; }
    public string Name { get; set; }
    public string PhoneNumber { get; set; }
    public string SmsNumber { get; set; }
    public string VirtualNumber { get; set; }
    public string ResponseStatus { get; set; }
    public bool SendSMS { get; set; }
    public string Status { get; set; }
    public string FundsStatus { get; set; }
    public string HeadingName { get; set; }
    public string Region { get; set; }
    public List<string> MoveToStatuses { get; set; }
	public DpzSupplier()
	{
		//
		
		//
	}
}