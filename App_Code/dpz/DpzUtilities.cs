﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Globalization;
using System.Diagnostics;

/// <summary>
/// Summary description for DpzUtilities
/// </summary>
public class DpzUtilities
{
	public DpzUtilities()
	{
		//
		
		//
	}
    public static WebReferenceSite.RetrieveGoldenNumberAdvertiserResponse GetSupplierDetails(SiteSetting ss, Guid SupplierId)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(ss.GetUrlWebReference);
        WebReferenceSite.RetrieveGoldenNumberAdvertiserRequest request = new WebReferenceSite.RetrieveGoldenNumberAdvertiserRequest();
        request.SupplierId = SupplierId;
        WebReferenceSite.ResultOfRetrieveGoldenNumberAdvertiserResponse result = null;
        try
        {
            result = _site.RetrieveGoldenNumberAdvertiser(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            return null;
        }
       
        return result.Value;
    }
    public static WebReferenceSite.GetAvailableVirtualNumbersResponse GetAvailableVirtualNumbers(SiteSetting ss)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(ss.GetUrlWebReference);
        WebReferenceSite.ResultOfGetAvailableVirtualNumbersResponse result = null;
        try
        {
            result = _site.GetAvailableVirtualNumbers();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return null;
        return result.Value;

    }
    
    public static WebReferenceSite.UpdateGoldenNumberAdvertiserResponse SetSupplierDetails(HttpSessionState Session, Guid SupplierId, string BizId, bool IsActive, string name,
                        string PhoneNumber, string SmsNumber, string VirtualNumber, bool SendSMS, string MoveTo, string FundsStatus)
    {
        SiteSetting ss = (SiteSetting)Session["Site"];
        SupplierAudit audit = (Session["dpz_audit"] == null) ?
            new SupplierAudit(SupplierId, ((UserMangement)Session["UserGuid"]).Get_Guid, name, "SearchSuppliers.aspx", 
                ((UserMangement)Session["UserGuid"]).User_Name, ss.siteLangId, "GoldenNumberAdvertiser") :
            (SupplierAudit)Session["dpz_audit"];
        
        List<WebReferenceSite.AuditEntry> list_audit = new List<WebReferenceSite.AuditEntry>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(ss.GetUrlWebReference);
        WebReferenceSite.UpdateGoldenNumberAdvertiserRequest request = new WebReferenceSite.UpdateGoldenNumberAdvertiserRequest();
        request.SupplierId = SupplierId;
        request.BizId = BizId;
        audit.AddAudit("txt_BizId", "lbl_BizId", BizId, list_audit);        
        request.IsActive = IsActive;
        audit.AddAudit("cb_IsActive", "lbl_IsActive", IsActive.ToString(), list_audit); 
        request.Name = name;
        audit.AddAudit("txt_Name", "lbl_Name", name, list_audit); 
        request.PhoneNumber = PhoneNumber;
        audit.AddAudit("txt_PhoneNumber", "lbl_PhoneNumber", PhoneNumber, list_audit); 
        request.SmsNumber = SmsNumber;
        audit.AddAudit("txt_SmsNumber", "lbl_SmsNumber", SmsNumber, list_audit);
        request.VirtualNumber = VirtualNumber;
        audit.AddAudit("txt_VirtualNumber", "lbl_VirtualNumber", VirtualNumber, list_audit);
        request.DoNotSendSMS = !SendSMS;
        audit.AddAudit("cb_SendSms", "lbl_SendSms", (SendSMS).ToString(), list_audit);        
        if (!string.IsNullOrEmpty(MoveTo))
        {
            WebReferenceSite.DapazStatusChangeOptions dco;
            if (Enum.TryParse(MoveTo, out dco))
            {
                request.NewDapazStatus = dco;
                audit.AddAudit("txt_GoldenStatus", "lbl_Status", EditTranslateDB.GetControlTranslate(ss.siteLangId, "DapazStatus", dco.ToString()), list_audit);
            }
        }
        WebReferenceSite.FundsStatus fs;
        if (!Enum.TryParse(FundsStatus, out fs))
            fs = WebReferenceSite.FundsStatus.Normal;
        request.FundsStatus = fs;
        audit.AddAudit("ddl_FundsStatus", "lbl_MoneyStatus", EditTranslateDB.GetControlTranslate(ss.siteLangId, "FundsStatus", fs.ToString()), list_audit);

        if (list_audit.Count == 0)
            request.AuditOn = false;
        else
        {
            request.AuditOn = true;
            request.AuditEntries = list_audit.ToArray();
        }
        WebReferenceSite.ResultOfUpdateGoldenNumberAdvertiserResponse result = null;
        try
        {
            result = _site.UpdateGoldenNumberAdvertiser(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            return null;
        }

        return result.Value;
    }
    public static WebReferenceSite.CreateGoldenNumberAdvertiserResponse CreateSupplier(SiteSetting ss, string BizId, bool IsActive, string name,
                        string PhoneNumber, string SmsNumber, string VirtualNumber, bool SendSms, string MoveTo, string FundsStatus)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(ss.GetUrlWebReference);
        WebReferenceSite.CreateGoldenNumberAdvertiserRequest request = new WebReferenceSite.CreateGoldenNumberAdvertiserRequest();
        request.BizId = BizId;
        request.IsActive = IsActive;
        request.Name = name;
        request.PhoneNumber = PhoneNumber;
        request.SmsNumber = SmsNumber;
        request.VirtualNumber = VirtualNumber;
        request.DoNotSendSMS = !SendSms;
        WebReferenceSite.eTypeOfGolden etog;
        if(!Enum.TryParse(MoveTo, out etog))
            etog = WebReferenceSite.eTypeOfGolden.Fix;
        request.TypeOfGolden = etog;
        WebReferenceSite.FundsStatus fs;
        if (!Enum.TryParse(FundsStatus, out fs))
            fs = WebReferenceSite.FundsStatus.Normal;
        request.FundsStatus = fs;
        WebReferenceSite.ResultOfCreateGoldenNumberAdvertiserResponse result = null;
        try
        {
            result = _site.CreateGoldenNumberAdvertiser(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return null;
        return result.Value;
    }
    public static WebReferenceReports.DapazReportsSupplierContainer[] GetSupplierByStr(SiteSetting ss, string str)
    {
        //public Result<List<DataModel.Dapaz.Reports.DapazReportsSupplierContainer>> DapazReportFindSupplier(string searchValue)
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(ss.GetUrlWebReference);
        WebReferenceReports.ResultOfListOfDapazReportsSupplierContainer result = null;
        try
        {
            result = _report.DapazReportFindSupplier(str);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return null;
        }
        if(result.Type== WebReferenceReports.eResultType.Failure)
            return null;
        return result.Value;
    }
    public static WebReferenceReports.DapazReportsSupplierContainer GetSupplierByBizId(SiteSetting ss, string BizId)
    {
        WebReferenceReports.DapazReportsSupplierContainer[] result = GetSupplierByStr(ss, BizId);
        foreach (WebReferenceReports.DapazReportsSupplierContainer data in result)
        {
            if (data.BizId == BizId)
                return data;
        }
        return null;
    }
    public static bool GetPathTwillo(string UrlPath, string SavePath)
    {
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(UrlPath);

        webRequest.Method = "GET";

        WebResponse webResponse = webRequest.GetResponse();
        Stream webStream = webResponse.GetResponseStream();
        using (Stream _file = File.Create(SavePath))
        {
            CopyStream(webStream, _file);
        }
        return true;
    }
    static void CopyStream(Stream input, Stream output)
    {
        byte[] buffer = new byte[8 * 1024];
        int len;
        while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            output.Write(buffer, 0, len);
        }
    }
    public static string GetPathRecord(string RecordId, SiteSetting ss)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(ss.GetUrlWebReference);
        WebReferenceSite.ResultOfString result = null;
        try
        {
            result = _site.GetPathToRecordingByRecordId(RecordId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return string.Empty;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;
        return result.Value;
    }
    /*
    public static bool MoveSupplierToPPC(Guid SupplierId, SiteSetting ss)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ss.GetUrlWebReference);
        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.TurnSupplierToPPA(SupplierId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return false;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return false;
        return true;
    }
     * */
    public static ResultCharging ChargingSupplier(SupllierPayment supllierPayment, List<WebReferenceSupplier.SupplierPricingComponent> list_spc, HttpSessionState _session)
    {
        ResultCharging resultCharging = new ResultCharging();
       
        
        try
        {          

            /***************** set purchase crm ****************************/
            DateTime checkDate;
            //LogEdit.SaveLog("Accounting_" + supllierPayment.siteId, "numbers discount" + supllierPayment.discount + " deposit  " + supllierPayment.deposit);

            WebReferenceSupplier.CreateSupplierPricingRequest request = new WebReferenceSupplier.CreateSupplierPricingRequest();
            WebReferenceSupplier.BonusType bt = WebReferenceSupplier.BonusType.None;
            Enum.TryParse(supllierPayment.discountType, out bt);

            request.BonusType = bt;
            if (bt != WebReferenceSupplier.BonusType.None)
            {
                if (supllierPayment.Bonus > 0)
                    request.BonusAmount = supllierPayment.Bonus;
                else if (String.IsNullOrEmpty(supllierPayment.discount))
                    request.BonusAmount = 0;
                else
                    request.BonusAmount = Int32.Parse(supllierPayment.discount);
            }
            else            
                request.BonusAmount = 0;

            if (String.IsNullOrEmpty(supllierPayment.extraBonus))
                request.ExtraBonus = (supllierPayment.Bonus > 0 && bt == WebReferenceSupplier.BonusType.None) ?
                    supllierPayment.Bonus : 0;
            else
                request.ExtraBonus = Int32.Parse(supllierPayment.extraBonus);

            if (list_spc != null && list_spc.Count > 0)
                request.Components = list_spc.ToArray();

            if (!String.IsNullOrEmpty(supllierPayment.extraReasonBonusId))
                request.ExtraBonusReasonId = new Guid(supllierPayment.extraReasonBonusId);            
            
        //    decimal fDeposit = Math.Round(Convert.ToDecimal(supllierPayment.deposit));
            request.PaymentAmount = supllierPayment.PaymentAmount; //Convert.ToInt32(fDeposit); // it is in agorot
            
            // for if to check invoice number for refund
            if (supllierPayment.PaymentAmount < 0 && supllierPayment.HasInvoiceNumberRefund)
            {
                request.LookForInvoiceInRefund = true;
                request.InvoiceNumber = supllierPayment.InvoiceNumber;
            }
            else
                request.LookForInvoiceInRefund = false;
            
   //         request.IsAutoRenew = supllierPayment.isAutoRenew;
            request.SupplierId = new Guid(supllierPayment.guid);                    
            request.TransactionId = supllierPayment.transactionId;
            request.ChargingCompany = supllierPayment.chargingCompany;
            request.UserIP = supllierPayment.clientIp;
            request.UserRemoteHost = supllierPayment.hostIp;
            request.UserAcceptLanguage = supllierPayment.acceptLanguage;
            request.UserAgent = supllierPayment.userAgent;
            request.CreditCardToken = supllierPayment.creditCardToken;
            
            request.CreatedByUserId = new Guid(supllierPayment.userId);
            
            request.Bank = supllierPayment.bank;
            request.BankBranch = supllierPayment.bankBranch;
            request.BankAccount = supllierPayment.bankAccount;
         
            request.ChargingCompanyAccountNumber = supllierPayment.chargingCompanyCustomerId;
            request.ChargingCompanyContractId = supllierPayment.chargingCompanyContractId;
            request.CreditCardType = supllierPayment.creditCardCompany;
            request.Last4DigitsCC = supllierPayment.creditCard;
            request.CreditCardExpDate = supllierPayment.creditCardDateMmyy;
         //   WebReferenceSupplier.ePaymentMethod epm;
        //    if (Enum.TryParse(supllierPayment.paymentMethod, out epm))
            request.PaymentMethod = supllierPayment.paymentMethod;
            //TODO recive value in resultCharging

            bool Is_Publisher;
            UserMangement _um = null;
            if (_session != null && _session["UserGuid"] != null)
            {
               _um  = (UserMangement)_session["UserGuid"];
                Is_Publisher = _um.IsPublisher();
            }
            else
            {
                WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(null, supllierPayment.siteId);
                WebReferenceSite.ResultOfBoolean _result = null;
                try
                {
                    _result = _site.IsPublisherUser(new Guid(supllierPayment.userId));
                }
                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc);
                    return new ResultCharging { Result = ResultCharging.eResult.unsuccess, DepositStatus = eDepositStatusResponse.NotCompleted };
                }
                if (_result.Type == WebReferenceSite.eResultType.Failure)
                {
                    resultCharging.Result = ResultCharging.eResult.unsuccess;
                    resultCharging.DepositStatus = eDepositStatusResponse.NotCompleted;
                    return resultCharging;
                }
                Is_Publisher = _result.Value;                
            }
            request.CreatedByUserId = (_um != null) ? _um.Get_Guid : new Guid(supllierPayment.userId);
                
          //  string Page_name = (Is_Publisher) ? "PublisherPayment.aspx" : "professionalPayment.aspx";

            switch (supllierPayment.paymentMethod)
            {
                case WebReferenceSupplier.ePaymentMethod.Check:
                    request.CheckNumber = supllierPayment.checkNumber;

                    if (!string.IsNullOrEmpty(supllierPayment.checkDate))
                    {
                        CultureInfo culture;
                        culture = CultureInfo.CreateSpecificCulture("en-US");
                        culture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";

                        if (DateTime.TryParse(supllierPayment.checkDate, culture, DateTimeStyles.None, out checkDate))
                            request.CheckDate = checkDate;
                        else
                        {
                            resultCharging.Result = ResultCharging.eResult.unsuccess;
                            resultCharging.DepositStatus = eDepositStatusResponse.DateCheckNotValid;
                            return resultCharging;
                        }

                    }

                    break;

                case WebReferenceSupplier.ePaymentMethod.WireTransfer:

                    if (!string.IsNullOrEmpty(supllierPayment.transDate))
                    {
                        CultureInfo culture;
                        culture = CultureInfo.CreateSpecificCulture("en-US");
                        culture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";

                        if (DateTime.TryParse(supllierPayment.transDate, culture, DateTimeStyles.None, out checkDate))
                            request.TransferDate = checkDate;
                        else
                        {
                            resultCharging.Result = ResultCharging.eResult.unsuccess;
                            resultCharging.DepositStatus = eDepositStatusResponse.DateWireTransferNotValid;
                            return resultCharging;
                        }

                    }
/*
                    else if (!string.IsNullOrEmpty(supllierPayment.transDate))
                    {
                        CultureInfo culture;
                        culture = CultureInfo.CreateSpecificCulture("en-US");
                        culture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";

                        if (DateTime.TryParse(supllierPayment.transDate, culture, DateTimeStyles.None, out checkDate))
                            request.TransferDate = checkDate;
                        

                    }
*/
                    break;

                case WebReferenceSupplier.ePaymentMethod.Voucher:

                    request.VoucherNumber = supllierPayment.voucherNumber;

                    Guid voucherReasonId;
                    if (Guid.TryParse(supllierPayment.voucherReasonId, out voucherReasonId))
                        request.VoucherReasonId = voucherReasonId;

                    break;

            }

            request.CreditCardOwnerName = supllierPayment.cardName;
            request.BillingAddress = supllierPayment.cardAddress;
           
            /*
            request.CVV2 = supllierPayment.cvv2;
            request.CreditCardOwnerName = supllierPayment.cardName;
            string last4 = "";
            if (supllierPayment.creditCard.IndexOf("...") != -1)
                last4 = supllierPayment.creditCard.Replace("...", "");
            else
                last4 = supllierPayment.creditCard.Substring(supllierPayment.creditCard.Length - 4, 4);

            request.Last4DigitsCC = last4;
            request.CreditCardOwnerId = supllierPayment.cardId;
            */
            //TODO

            LogEdit.SaveLog("Accounting_" + supllierPayment.siteLangId, supllierPayment.name + " SupplierPricingComponentType: " + supllierPayment.SupplierPricingComponentType.ToString());

            if (supllierPayment.isRecharge) //
            {
                WebReferenceSupplier.RechargeTypeCode rtc;
                switch (supllierPayment.SupplierPricingComponentType)
                {
                    case(WebReferenceSupplier.eSupplierPricingComponentType.AutoLow):
                        rtc = WebReferenceSupplier.RechargeTypeCode.BalanceLow;
                        break;
                    
                    case (WebReferenceSupplier.eSupplierPricingComponentType.MonthlyFeePayment):
                        rtc = WebReferenceSupplier.RechargeTypeCode.Monthly;
                        request.RechargeDayOfMonth = DateTime.Now.Day;
                        break;
                    
                    default: rtc = WebReferenceSupplier.RechargeTypeCode.MaxMonthly;
                        break;
                    
                }
                request.UpdateRechargeFields = true;

                if (supllierPayment.RechargeAmount <= 0)
                    request.IsAutoRenew = false;
                else
                {
                    request.IsAutoRenew = true;
                    request.RechargeAmount = supllierPayment.RechargeAmount;
                    request.RechargeTypeCode = rtc;
           //         if (rtc == WebReferenceSupplier.RechargeTypeCode.Monthly)
        //                request.RechargeDayOfMonth = (supllierPayment.RechargeDayOfMonth < 1) ? 1 : supllierPayment.RechargeDayOfMonth;
                }
            }
            
            else
                request.UpdateRechargeFields = false;
            
            string pagename = "Credit.aspx";
            string page_translate = EditTranslateDB.GetPageNameTranslate(pagename, supllierPayment.siteLangId);

            WebReferenceSupplier.AuditEntry ae = new WebReferenceSupplier.AuditEntry();
            ae.AdvertiseId = new Guid(supllierPayment.guid); // who update could be supplier or publisher
            ae.AdvertiserName = supllierPayment.name; // who update could be supplier or publisher
            ae.AuditStatus = (supllierPayment.isFirstTimeV) ? WebReferenceSupplier.Status.Insert :
                WebReferenceSupplier.Status.Update;
            ae.FieldId = supllierPayment.fieldId;
            ae.FieldName = supllierPayment.fieldName;
            ae.NewValue = supllierPayment.balanceNew;
            ae.OldValue = supllierPayment.balanceOld;
            ae.PageId = "Credit.aspx";
            ae.UserId = new Guid(supllierPayment.userId);
            ae.UserName = supllierPayment.userName;
            ae.PageName = page_translate;
            
            List<WebReferenceSupplier.AuditEntry> arrAudit = new List<WebReferenceSupplier.AuditEntry>();
            arrAudit.Add(ae);
            request.AuditEntries = arrAudit.ToArray();
            request.AuditOn = true; // flag to inser all the details otherwise doesn't work at all even if yop send the details
            request.InvoiceUrl = supllierPayment.InvoiceUrlReference;
            request.InvoiceNumber = supllierPayment.InvoiceNumber;
            request.BillingPhone = String.IsNullOrEmpty(supllierPayment.tel1) ? supllierPayment.tel2 : supllierPayment.tel1;
            request.BillingEmail = supllierPayment.email;
            request.BillingCity = supllierPayment.city;
            request.BillingState = supllierPayment.state;

            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, supllierPayment.siteId);
            //LogEdit.SaveLog("Accounting_" + supllierPayment.siteLangId, "url: " + supplier.Url);
            WebReferenceSupplier.ResultOfCreateSupplierPricingResponse result = supplier.CreateSupplierPricing(request);        
            
            

       //     string DepositStatus = "";

            if (result.Type == WebReferenceSupplier.eResultType.Success)
            {
                resultCharging.Result = ResultCharging.eResult.success;
                resultCharging.AccountNumber = result.Value.AccountNumber;
                resultCharging.PricingId = result.Value.PricingId;
                resultCharging.SalesPerson = result.Value.AccountManagerId;

              //  DepositStatus = result.Value.DepositStatus.ToString().ToLower();
                switch (result.Value.DepositStatus)
                {
                    case WebReferenceSupplier.eDepositStatus.OK:
                        resultCharging.DepositStatus = eDepositStatusResponse.OK;
                        break;

                    case WebReferenceSupplier.eDepositStatus.Repeated:
                        resultCharging.DepositStatus = eDepositStatusResponse.Repeated;
                        break;

                    case WebReferenceSupplier.eDepositStatus.BadVoucherNumber:
                        resultCharging.DepositStatus = eDepositStatusResponse.BadVoucherNumber;
                        break;

                    case WebReferenceSupplier.eDepositStatus.NotEnoughCashInVoucher:
                        resultCharging.DepositStatus = eDepositStatusResponse.NotEnoughCashInVoucher;
                        break;

                    case WebReferenceSupplier.eDepositStatus.NotCompleted:
                        resultCharging.DepositStatus = eDepositStatusResponse.NotCompleted;
                        break;
                    case WebReferenceSupplier.eDepositStatus.InvalidInvoiceNumber:
                        resultCharging.DepositStatus = eDepositStatusResponse.InvalidInvoiceNumber;
                        break;
                    case WebReferenceSupplier.eDepositStatus.RefundBiggerThanBalance:
                        resultCharging.DepositStatus = eDepositStatusResponse.RefundBiggerThanBalance;
                        break;

                }
            }

            else
            {
                resultCharging.Result = ResultCharging.eResult.unsuccess;

                resultCharging.Msg = "Web Service code problem. Try later" ;
                resultCharging.Result = ResultCharging.eResult.unsuccess;
                string publisherEmail = Utilities.GetEmailNotification(null, supllierPayment.siteId);
                LogEdit.SaveLog("Accounting_" + supllierPayment.siteLangId, "Charging: Charging failed for " + supllierPayment.name + "\r\n" + result.Messages[0]);
                MailSender mail = new MailSender();
                mail.From = "rona@noproblem.co.il";
                mail.FromName = "NO PROBLEM billing";
                mail.To = "rona@noproblem.co.il";
                mail.Bcc = "shaip@onecall.co.il;rona@noproblem.co.il;shaim@onecall.co.il;" +
                    (!string.IsNullOrEmpty(publisherEmail)?publisherEmail + ";":"");    
                 

                mail.Subject = "Problem with transaction: " +
                    supllierPayment.transactionId;
                mail.IsText = true;
                
                mail.Body = "Charging company: " + supllierPayment.chargingCompany + "\r\n" +
                    "Site Id: " + supllierPayment.siteId + "\r\n" +
                    "Site Id: " + Convert.ToInt32(supllierPayment.siteLangId) + "\r\n" +
                    "Advertiser name: " + supllierPayment.name + "\r\n" +
                    "User name: " + supllierPayment.userName + "\r\n" +
                    "Transaction id: " + supllierPayment.transactionId + "\r\n" +
                    "Amount: " + supllierPayment.PaymentAmount + "\r\n" +
                    "Time: " + DateTime.Now.ToString() + "\r\n" +
                    "Message: " + result.Messages[0];
                    


                bool ifOkSentMail = mail.Send();
                LogEdit.SaveLog("Accounting_" + supllierPayment.siteLangId, supllierPayment.name + " Email: " + ifOkSentMail);
                resultCharging.Msg = result.Messages[0];                
            }


           
            LogEdit.SaveLog("Accounting_" + supllierPayment.siteLangId, "Crm: Create pricing  for " + supllierPayment.name
                + "\r\nType: " + result.Type.ToString()
                + "\r\nPricingId: " + resultCharging.PricingId.ToString()
                + "\r\nDepositStatus: " + resultCharging.DepositStatus.ToString()
                + "\r\nMsg: " + resultCharging.Msg);
            /***************** end set purchase crm ****************************/

        }

        catch (Exception ex)
        {
           
        
            resultCharging.Msg = "Server problem. Try later " + ex.Message;
            resultCharging.Result = ResultCharging.eResult.unsuccess;
            string publisherEmail = Utilities.GetEmailNotification(null, supllierPayment.siteId);

            //Get a StackTrace object for the exception
            StackTrace st = new StackTrace(ex, true);
            //Get the first stack frame
            StackFrame frame = st.GetFrame(0);
            //Get the file name
            string fileName = frame.GetFileName();
            //Get the method name
            string methodName = frame.GetMethod().Name;
            //Get the line number from the stack frame
            int line = frame.GetFileLineNumber();
            //Get the column number
            int col = frame.GetFileColumnNumber();

            LogEdit.SaveLog("Accounting_" + supllierPayment.siteLangId, "Charging: Charging failed for " + supllierPayment.name +
                "\r\n" + ex.Message + "\r\n" + ex.StackTrace + "\r\nline: " + line.ToString());
            MailSender mail = new MailSender();
            mail.From="rona@noproblem.co.il";
            mail.FromName="NO PROBLEM billing";
            mail.To = "rona@noproblem.co.il";
            mail.Bcc = "shaip@onecall.co.il;rona@noproblem.co.il;shaim@onecall.co.il;" +
                     (!string.IsNullOrEmpty(publisherEmail) ? publisherEmail + ";" : "");  

            mail.Subject = "Problem with transaction: " +
                supllierPayment.transactionId;
            mail.IsText = true;
            mail.Body = "Charging company: " + supllierPayment.chargingCompany + "\r\n" +
                "Site Id: " + supllierPayment.siteId + "\r\n" +
                "Site Id: " + Convert.ToInt32(supllierPayment.siteLangId) + "\r\n" +
                "Advertiser name: " + supllierPayment.name + "\r\n" +
                "User name: " + supllierPayment.userName + "\r\n" +
                "Transaction id: " + supllierPayment.transactionId + "\r\n" +
                "Amount: " + supllierPayment.PaymentAmount + "\r\n" +
                "Time: " + DateTime.Now.ToString() + "\r\n" +
                "Message: " + ex.Message + "\r\n" +                
                "StackTrace: " + ex.StackTrace; 
               

            bool ifOkSentMail = mail.Send();
            LogEdit.SaveLog("Accounting_" + supllierPayment.siteLangId,supllierPayment.name + " Email: " + ifOkSentMail);

            //mail.RemoteHost
        }

        return resultCharging;

    }
    public static ResultCharging ChargingZapSupplier(SupplierPaymentData PaymentData, UserMangement um, SiteSetting site_setting)
    {


        ResultCharging resultCharging = new ResultCharging();

        WebReferenceSupplier.CreateSupplierPricingRequest request = new WebReferenceSupplier.CreateSupplierPricingRequest();
        IEnumerable<Supplier_Pricing_Component> query = from x in PaymentData.list_component
                                                        where x.PaymentType == ePaymentType.Bonus
                                                        select x;
        request.CreatedByUserId = um.Get_Guid;
        resultCharging.Amount = (int)PaymentData.TotalAmount;

        if (query.Count() > 0)
        {
            Supplier_Pricing_Component s_p_c = query.FirstOrDefault();
            WebReferenceSupplier.BonusType _bt;
            if (!Enum.TryParse(s_p_c.comment, out _bt))
                _bt = WebReferenceSupplier.BonusType.None;
            request.BonusType = _bt;
            request.BonusAmount = (int)(from y in query
                                        select y.Amount).Sum();
        }
        else
        {
            request.BonusType = WebReferenceSupplier.BonusType.None;
            request.BonusAmount = 0;

        }
        query = from x in PaymentData.list_component
                where x.PaymentType == ePaymentType.BonusExtra
                select x;
        if (query.Count() > 0)
        {
            Supplier_Pricing_Component s_p_c = query.FirstOrDefault();
            Guid ExtraBonusReasonId;
            if (!Guid.TryParse(s_p_c.comment, out ExtraBonusReasonId))
                ExtraBonusReasonId = Guid.Empty;
            request.ExtraBonusReasonId = ExtraBonusReasonId;
            request.ExtraBonus = (int)(from y in query
                                       select y.Amount).Sum();
        }
        else
            request.ExtraBonus = 0;


        request.Components = PaymentData.GetListCrmType().ToArray();

        request.PaymentAmount = (int)PaymentData.TotalAmount;
        if (PaymentData.TotalAmount < 0 && !string.IsNullOrEmpty(PaymentData.InvoiceNumber))
        {
            request.LookForInvoiceInRefund = true;
            request.InvoiceNumber = PaymentData.InvoiceNumber;
        }
        else
            request.LookForInvoiceInRefund = false;

        request.SupplierId = PaymentData.SupplierId;
        request.IsFromDollar = PaymentData.IsFromDollar;

        //    request.UserIP = SupplierId;
        //      request.UserRemoteHost = supllierPayment.hostIp;
        //       request.UserAcceptLanguage = supllierPayment.acceptLanguage;
        //       request.UserAgent = supllierPayment.userAgent;

        request.CreatedByUserId = um.Get_Guid;




        request.PaymentMethod = PaymentData.GetCrmPaymentMethod();
        switch (PaymentData.PaymentMethod)
        {
            case (ePaymentMethod.CreditCard):
                request.CreditCardToken = PaymentData.CreditCardToken;
                request.ChargingCompanyContractId = PaymentData.PaymentId.ToString();//PaymentData.CreditCardTxId;
                request.CreditCardType = PaymentData.CreditCardCompany;
                request.Last4DigitsCC = PaymentData.Last4digitOfToken;
                request.CreditCardExpDate = PaymentData.CreditCard_Expier;
                request.CreditCardOwnerId = PaymentData.CreditCardPersonIdNumber;
                request.CreditCardOwnerName = PaymentData.CreditCarddHolderName;
                break;
            case (ePaymentMethod.Voucher):
                request.VoucherNumber = PaymentData.VoucherNumber;
                request.VoucherReasonId = PaymentData.VoucherReasonId;
                break;
            case (ePaymentMethod.Check):
                request.CheckNumber = PaymentData.CheckNumber;
                request.CheckDate = PaymentData.WireTransfeCheckDate;
                goto default;
            case (ePaymentMethod.WireTransfer):
                request.TransferDate = PaymentData.WireTransfeCheckDate;
                goto default;
            default:
                request.Bank = PaymentData.Bank;
                request.BankBranch = PaymentData.BankBranch;
                request.BankAccount = PaymentData.BankAccount;
                break;
        }

        decimal recharge = PaymentData.GetRecharge();

        if (recharge > 0) //
        {
            WebReferenceSupplier.RechargeTypeCode rtc;
            switch (PaymentData.GetRechargeTypeCode())
            {
                case (ePaymentType.AutoLow):
                    rtc = WebReferenceSupplier.RechargeTypeCode.BalanceLow;
                    break;
                default: rtc = WebReferenceSupplier.RechargeTypeCode.MaxMonthly;
                    break;

            }
            request.UpdateRechargeFields = true;

            request.IsAutoRenew = true;
            request.RechargeAmount = (int)recharge;
            request.RechargeTypeCode = rtc;
        }
        else
            request.UpdateRechargeFields = false;

        string pagename = "Credit.aspx";
        string page_translate = EditTranslateDB.GetPageNameTranslate(pagename, site_setting.siteLangId);

        WebReferenceSupplier.AuditEntry ae = new WebReferenceSupplier.AuditEntry();
        ae.AdvertiseId = PaymentData.SupplierId; // who update could be supplier or publisher
        ae.AdvertiserName = PaymentData.Name; // who update could be supplier or publisher
        ae.AuditStatus = (PaymentData.IsFirstTime) ? WebReferenceSupplier.Status.Insert :
            WebReferenceSupplier.Status.Update;
      //  ae.FieldId = supllierPayment.fieldId;
     //   ae.FieldName = supllierPayment.fieldName;
        ae.NewValue = PaymentData.BalanceNew.ToString();
        ae.OldValue = PaymentData.BalanceOld.ToString();
        ae.PageId = "Credit.aspx";
        ae.UserId = um.Get_Guid;
        ae.UserName = um.User_Name;
        ae.PageName = page_translate;

        List<WebReferenceSupplier.AuditEntry> arrAudit = new List<WebReferenceSupplier.AuditEntry>();
        arrAudit.Add(ae);
        request.AuditEntries = arrAudit.ToArray();
        request.AuditOn = true; // flag to inser all the details otherwise doesn't work at all even if yop send the details

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(site_setting.GetUrlWebReference);
        //LogEdit.SaveLog("Accounting_" + supllierPayment.siteLangId, "url: " + supplier.Url);
        WebReferenceSupplier.ResultOfCreateSupplierPricingResponse result = null;
        try
        {
            result = supplier.CreateSupplierPricing(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, PaymentData);
            resultCharging.Result = ResultCharging.eResult.unsuccess;
            resultCharging.Msg = "Web Service code problem. Try later";
            return resultCharging;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            resultCharging.Result = ResultCharging.eResult.unsuccess;
            foreach (string str in result.Messages)
                resultCharging.Msg += str + "\r\n";
            MailSender mail = new MailSender();
            mail.ZapMoneyMail(PaymentData);
            mail.Send();
            return resultCharging;
        }
        if (result.Value.DepositStatus == WebReferenceSupplier.eDepositStatus.OK)
            resultCharging.Result = ResultCharging.eResult.success;
        else
        {
            resultCharging.Result = ResultCharging.eResult.unsuccess;
            resultCharging.Msg = result.Value.ErrorDesc;
        }
        resultCharging.AccountNumber = result.Value.AccountNumber;
        resultCharging.PricingId = result.Value.PricingId;
        resultCharging.SalesPerson = result.Value.AccountManagerId;
        eDepositStatusResponse edsr;
        if(!Enum.TryParse(result.Value.DepositStatus.ToString(), true, out edsr))
            edsr = eDepositStatusResponse.NotCompleted;
        resultCharging.DepositStatus = edsr;

        return resultCharging;

    }
    //Old method
    public static int SetPayment(List<Supplier_Pricing_Component> list_spc, string SiteId, int amount, WebReferenceSupplier.eSupplierPricingComponentType PaymentType,
                        Guid SupplierId, string SupplierBizId, int bonus, int RechargeAmount, bool UpdateRecharge, string details)
    {
        int id = -1;
        string command = "EXEC [dbo].[SetPaymentZap] @site_id, @SupplierId, @PaymentType, @Amount, @BizId, @Bonus, @RechargeAmount, @UpdateRecharge, @details";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            SqlCommand cmd = new SqlCommand(command, conn);
            try
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@site_id", SiteId);
                cmd.Parameters.AddWithValue("@SupplierId", SupplierId);
                cmd.Parameters.AddWithValue("@PaymentType", PaymentType.ToString());
                cmd.Parameters.AddWithValue("@Amount", amount);
                if (string.IsNullOrEmpty(SupplierBizId))
                    cmd.Parameters.AddWithValue("@BizId", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@BizId", SupplierBizId);
                if (bonus > 0)
                    cmd.Parameters.AddWithValue("@Bonus", bonus);
                else
                    cmd.Parameters.AddWithValue("@Bonus", DBNull.Value);
                cmd.Parameters.AddWithValue("@UpdateRecharge", UpdateRecharge);
                if (UpdateRecharge && RechargeAmount > 0)
                    cmd.Parameters.AddWithValue("@RechargeAmount", RechargeAmount);
                else
                    cmd.Parameters.AddWithValue("@RechargeAmount", DBNull.Value);
                if (string.IsNullOrEmpty(details))
                    cmd.Parameters.AddWithValue("@details", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@details", details);
                id = (int)cmd.ExecuteScalar();
                if (list_spc != null)
                {
                    command = "EXEC [dbo].[SetPaymentHeadingZap] @PaymentId, @AmountPayed, @BaseAmount";
                    foreach (Supplier_Pricing_Component spc in list_spc)
                    {
                        cmd.Dispose();
                        cmd = new SqlCommand(command, conn);
                        cmd.Parameters.AddWithValue("@PaymentId", id);
                        cmd.Parameters.AddWithValue("@AmountPayed", spc.Amount);
                        cmd.Parameters.AddWithValue("@BaseAmount", spc.BaseAmount);
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, SiteId);
                if (conn.State != System.Data.ConnectionState.Closed)
                    conn.Close();
                return -1;
            }
        }
        return id;
    }
    
    public static int SetPayment(List<Supplier_Pricing_Component> list_spc, string SiteId, decimal amount, ePaymentMethod PaymentMethod,  
                        Guid SupplierId, string SupplierBizId, bool UpdateRecharge, string email, int NewBalance, int OldBalance, bool IsFromDollar)
    {
        SupplierBizId = SupplierBizId.Trim();
        string command = "EXEC [dbo].[InsertPaymentZap]	@Site, @BizId, @SupplierId, @payment_method, @PaymentAmount, @email, @OldBalance, @NewBalance, @IsFromDollar";
        int ID = -1;
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            SqlCommand cmd = new SqlCommand(command, conn);

            try
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@Site", SiteId);
                cmd.Parameters.AddWithValue("@SupplierId", SupplierId);
                if (string.IsNullOrEmpty(SupplierBizId))
                    cmd.Parameters.AddWithValue("@BizId", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@BizId", SupplierBizId);
                /*
                if (bonus > 0)
                    cmd.Parameters.AddWithValue("@Bonus", bonus);
                else
                    cmd.Parameters.AddWithValue("@Bonus", DBNull.Value);
                 * */
                cmd.Parameters.AddWithValue("@UpdateRecharge", UpdateRecharge);
                cmd.Parameters.AddWithValue("@payment_method", PaymentMethod.ToString());
                cmd.Parameters.AddWithValue("@PaymentAmount", amount);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@OldBalance", OldBalance);
                cmd.Parameters.AddWithValue("@NewBalance", NewBalance);
                cmd.Parameters.AddWithValue("@IsFromDollar", IsFromDollar);
                ID = (int)cmd.ExecuteScalar();
                if (list_spc != null)
                {
                    command = "EXEC [dbo].[InsertPaymentRecord] @PaymentId, @Amount, @BaseAmount, @PaymentType, @comment";
                    foreach (Supplier_Pricing_Component spc in list_spc)
                    {
                        cmd.Dispose();
                        cmd = new SqlCommand(command, conn);
                        cmd.Parameters.AddWithValue("@PaymentId", ID);
                        cmd.Parameters.AddWithValue("@Amount", spc.Amount);
                        if (spc.BaseAmount <= 0)
                            cmd.Parameters.AddWithValue("@BaseAmount", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@BaseAmount", spc.BaseAmount);
                        cmd.Parameters.AddWithValue("@PaymentType", spc.PaymentType.ToString());
                        if (string.IsNullOrEmpty(spc.comment))
                            cmd.Parameters.AddWithValue("@comment", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@comment", spc.comment);
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, SiteId);
                if (conn.State != System.Data.ConnectionState.Closed)
                    conn.Close();
                return -1;
            }
        }
        return ID;
    }
    /*
    public static Guid InsertPayment()
    {

    }
     * 
     * */
    public static List<string> GetStatusOptionsChange(WebReferenceSite.DapazStatus status)
    {
        List<string> list = new List<string>();
        switch (status)
        {
            case(WebReferenceSite.DapazStatus.Rarah):
            case (WebReferenceSite.DapazStatus.RarahGold):
                list.Add(WebReferenceSite.DapazStatus.RarahQuote.ToString());
                goto default;
            case (WebReferenceSite.DapazStatus.RarahQuote):
                list.Add(WebReferenceSite.DapazStatus.Rarah.ToString());
                list.Add(WebReferenceSite.DapazStatus.RarahGold.ToString());
                goto default;
            case (WebReferenceSite.DapazStatus.PPA):
            case (WebReferenceSite.DapazStatus.PPAHold):
                list.Add(WebReferenceSite.DapazStatus.Rarah.ToString());
                list.Add(WebReferenceSite.DapazStatus.RarahGold.ToString());
                list.Add(WebReferenceSite.DapazStatus.RarahQuote.ToString());
                break;
            case (WebReferenceSite.DapazStatus.FixGold):     
            case (WebReferenceSite.DapazStatus.Fix):
                list.Add(WebReferenceSite.DapazStatus.PPAHold.ToString());
                break;
            default:
                list.Add(WebReferenceSite.DapazStatus.PPA.ToString());
                break;
        }
        return list;
    }
    public static List<string> GetStatusOptionsChange(WebReferenceSupplier.DapazStatus status)
    {
        WebReferenceSite.DapazStatus _status;
        if (!Enum.TryParse(status.ToString(), out _status))
            return new List<string>();
        return GetStatusOptionsChange(_status);
    }
    public static bool ZapInvoice(SupllierPayment sp)
    {
        return ZapInvoice(sp, null, null, null, null, null);
    }
    public static bool ZapInvoice(SupllierPayment sp, List<Supplier_Pricing_Component> list, string resultTransuctionExpiration,
            string resultTransuctionCardNumber, string cardCompany, string resultTransuctionNumConfirm)
    {
        /*
        ZapPayment.NPCreateInvoiceERP client = new ZapPayment.NPCreateInvoiceERP();
        ZapPayment.NPCreateInvoiceERPProcessRequest _request = new ZapPayment.NPCreateInvoiceERPProcessRequest();

        ZapPayment.OrderItem_Type orderItem = new ZapPayment.OrderItem_Type();
        ZapPayment.NPOrderHeaderRec_Type _header = new ZapPayment.NPOrderHeaderRec_Type();
        ZapPayment.NPOrder_Type newOrder = new ZapPayment.NPOrder_Type();
        List<ZapPayment.OrderItem_Type> list_order_item = new List<ZapPayment.OrderItem_Type>();
        if (sp.Bonus > 0)
            orderItem.InvoiceDiscount = sp.Bonus.ToString();
        else
        {
            decimal _discount;
            if (decimal.TryParse(sp.discount, out _discount))
                orderItem.InvoiceDiscount = _discount.ToString();
            else
                orderItem.InvoiceDiscount = "0";
        }
        orderItem.InvoiceLineComments = sp.comments;
     //   if (sp.RechargeAmount > 0)
   //         orderItem.InvoiceTotalAmt = sp.RechargeAmount.ToString();
   //     else
        orderItem.InvoiceTotalAmt = sp.Total.ToString();
        orderItem.ItemCode = "";
        orderItem.ItemDescription = "CallBalanceCharge";// sp.SupplierPricingComponentType.ToString();
        orderItem.ItemPrice = sp.PaymentAmount.ToString();
        orderItem.ItemType = "CallBalanceCharge";
        orderItem.LineNum = "1";
        orderItem.Quantity = "1";
        list_order_item.Add(orderItem);
        int LineNumber = 2;
        if (list != null)
        {
            foreach (Supplier_Pricing_Component _spc in list)
            {
                if (_spc.AmountPayed <= 0)
                    continue;
                ZapPayment.OrderItem_Type _oit = new ZapPayment.OrderItem_Type();

                _oit.InvoiceTotalAmt = _spc.AmountPayed.ToString();
                _oit.ItemCode = "";
                _oit.ItemDescription = "HeadingPPACharge";// _spc.Type.ToString();
                _oit.ItemPrice = _spc.BaseAmount.ToString();
                _oit.ItemType = "HeadingPPACharge";
                _oit.LineNum = LineNumber.ToString();
                LineNumber++;
                _oit.Quantity = (_spc.AmountPayed / _spc.BaseAmount).ToString();
                list_order_item.Add(_oit);
            }
        }
        _header.CcAmount = sp.deposit;
        _header.CcDate = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
        _header.CcExpiration = resultTransuctionExpiration;
        _header.CcNum = resultTransuctionCardNumber;
        _header.CcType = cardCompany;
        _header.CcVerification = resultTransuctionNumConfirm;
        _header.CustomerEmail = sp.email;// sp.email;
        _header.CustomerNo = sp.SupplierBizId;
        _header.InvoiceComments = "";
        _header.SalesPerson = sp.SalesPerson;
        _header.TaxRegistrationNum = "";


        newOrder.ListOfOrderItem = list_order_item.ToArray();
        //  newOrder.ListOfOrderItem[0] = orderItem;
        newOrder.NPOrderHeaderRec = _header;
        _request.LoginUser = PpcSite.GetCurrent().UserZap;

        _request.NPOrder = newOrder;
        ZapPayment.NPCreateInvoiceERPProcessResponse _response = null;
        try
        {
            _response = client.process(_request);
        }
        catch (Exception exc)
        {
            dbug_log.PelecardPayment(exc, sp);
            return false;
        }
        string message = _response.ErrorDesc + "; " + _response.Result;
        dbug_log.PelecardPayment(null, sp, message);
        if (!string.IsNullOrEmpty(_response.ErrorDesc))
            return false;
         * */
        return true;
        
    }
    /*
    public static CreditGuardUrlResponse GetCreditGuardUrl(int PaymentId, string cc_name, string cc_expier, string cc_id)
    {
        CreditGuardUrlResponse response = new CreditGuardUrlResponse();
        string command = "EXEC [dbo].[GetPaymentDetailsForCreditGuardUrl] @PaymentId";
        string email, CustomerNo;
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PaymentId", PaymentId);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                CustomerNo = (reader["BizId"] == DBNull.Value) ? string.Empty : (string)reader["BizId"];
                email = (reader["email"] == DBNull.Value) ? string.Empty : (string)reader["email"];
            }
            else
            {
                response.Status = "faild";
                if (conn.State != System.Data.ConnectionState.Closed)
                    conn.Close();
                return response;
            }
            conn.Close();


            response.PaymentId = PaymentId;
            WebReferenceZapUrlCreditGuard.PMTGetPayPageURLCREDITGUARD guard = WebServiceConfig.GetUrlCreditGuard();
            WebReferenceZapUrlCreditGuard.PMTGetPayPageURLCREDITGUARDProcessRequest _request = new WebReferenceZapUrlCreditGuard.PMTGetPayPageURLCREDITGUARDProcessRequest();
            _request.LoginUser = PpcSite.GetCurrent().GetUserZapLoginUser(email);
            _request.TrxUniqueIdentifier = PaymentId.ToString();
            int _bizid;
            if (!int.TryParse(CustomerNo, out _bizid))
            {
                response.Status = "faild";
                response.Msg = "Invalid BizId";
                return response;
            }
            _request.CustomerNo = _bizid;
            _request.CallingSystem = WebReferenceZapUrlCreditGuard.PMTGetPayPageURLCREDITGUARDProcessRequestCallingSystem.NP;
            WebReferenceZapUrlCreditGuard.PMTGetPayPageURLCREDITGUARDProcessResponse result = null;
            try
            {
                result = guard.process(_request);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                response.Status = "faild";
                response.Msg = "Zap server problem";
                return response;
            }
            if (string.IsNullOrEmpty(result.mpiHostedPageUrl) || string.IsNullOrEmpty(result.mpiHostedPageUrl_txId))
            {
                response.Status = "faild";
                response.Msg = HttpUtility.JavaScriptStringEncode(result.StatusDesc);
                return response;
            }
            response.Status = "success";
            response.txId = result.mpiHostedPageUrl_txId;
            response.Url = result.mpiHostedPageUrl;
            cmd.Dispose();
            command = "EXEC [dbo].[UpdatePaymentDetailsUrl] @PaymentId, @CC_Url, @CC_txId, @CC_Expier, @CC_Id, @CC_Name";
            conn.Open();
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PaymentId", PaymentId);
            cmd.Parameters.AddWithValue("@CC_Url", result.mpiHostedPageUrl);
            cmd.Parameters.AddWithValue("@CC_txId", result.mpiHostedPageUrl_txId);
            cmd.Parameters.AddWithValue("@CC_Expier", GetExpierDateFormat(cc_expier));
            cmd.Parameters.AddWithValue("@CC_Id", cc_id);
            cmd.Parameters.AddWithValue("@CC_Name", cc_name);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        return response;

    }
    */
    static string GetExpierDateFormat(string _expier)
    {
        string[] exps = _expier.Split('-');
        if (exps.Length != 2)
            return _expier;
        if (exps[0].Length == 1)
            exps[0] = "0" + exps[0];
        if (exps[1].Length == 4)
            exps[1] = exps[1].Substring(2);
        return exps[0] + exps[1];
    }
    /*
    public static TokenAmount GetCreditGuardToken(int PaymentId, int SiteLangId)
    {
        TokenAmount ta = new TokenAmount(SiteLangId);
        string command = "EXEC [dbo].[GetPaymentDetailsForCreditGuard] @PaymentId";
        string email = null, CustomerNo = null, txId = null;
        decimal PaymentAmount = -1;
        WebReferenceZapQueryCreditGuard.PMTTrxQueryCREDITGUARDProcessResponse result = null;
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PaymentId", PaymentId);
            SqlDataReader reader = cmd.ExecuteReader();
            
            if (reader.Read())
            {
                CustomerNo = (reader["BizId"] == DBNull.Value) ? string.Empty : (string)reader["BizId"];
                email = (reader["email"] == DBNull.Value) ? string.Empty : (string)reader["email"];
                txId = (reader["CC_txId"] == DBNull.Value) ? string.Empty : (string)reader["CC_txId"];
                PaymentAmount = (decimal)reader["PaymentAmount"];
            }
            conn.Close();
            if (string.IsNullOrEmpty(txId))
            {
                ta.eResponse = eTokenResponse.InvalidtxId;
                return ta;
            }
            if (string.IsNullOrEmpty(CustomerNo))
            {
                ta.eResponse = eTokenResponse.InvalidBizId;
                return ta;
            }
            WebReferenceZapQueryCreditGuard.PMTTrxQueryCREDITGUARD guard = WebServiceConfig.GetQueryCreditGuard();
            WebReferenceZapQueryCreditGuard.PMTTrxQueryCREDITGUARDProcessRequest _request = new WebReferenceZapQueryCreditGuard.PMTTrxQueryCREDITGUARDProcessRequest();
            _request.CallingSystem = WebReferenceZapQueryCreditGuard.PMTTrxQueryCREDITGUARDProcessRequestCallingSystem.NP;
            _request.LoginUser = PpcSite.GetCurrent().GetUserZapLoginUser(email);
            _request.mpiHostedPageUrl_txId = txId;
            _request.CustomerNo = int.Parse(CustomerNo);
            
            try
            {
                result = guard.process(_request);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
                ta.eResponse = eTokenResponse.ServerProblem;
                return ta;
            }
            if (result.Status != "000")
            {
                ta.Msg = result.StatusDesc;
                return ta;
            }
            command = "EXEC [dbo].[UpdatePaymentDetailsToken] @PaymentId, @CC_Token, @CC_Company";
            cmd.Dispose();
            conn.Open();
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PaymentId", PaymentId);
            cmd.Parameters.AddWithValue("@CC_Token", result.CCId);
            cmd.Parameters.AddWithValue("@CC_Company", result.CCCompany);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        ta.Token = GetTokenForDisplay(result.CCId);
        ta.Amount = (int)PaymentAmount;
       
        return ta;

    }
    */
    public static string GetTokenForDisplay(string token)
    {
        string NewToken = "";
        for (var i = token.Length - 4; i < token.Length; i++)
            NewToken += token[i];
        NewToken = "********" + NewToken;
        return NewToken;
    }
    public static ResultCharging ExecZapPayment(int PaymentId, UserMangement _user, SiteSetting _site)
    {
        ResultCharging rc = new ResultCharging();
        SupplierPaymentData data = new SupplierPaymentData();
        data.PaymentId = PaymentId;
        string command = "EXEC [dbo].[GetPaymentDetailsForCreditGuard] @PaymentId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PaymentId", PaymentId);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                data.BizId = (reader["BizId"] == DBNull.Value) ? string.Empty : (string)reader["BizId"];
                data.email = (reader["email"] == DBNull.Value) ? string.Empty : (string)reader["email"];
                data.CreditCard_Expier = (reader["CC_Expier"] == DBNull.Value) ? string.Empty : (string)reader["CC_Expier"];
                data.SupplierId = (Guid)reader["SupplierId"];
                data.CreditCardTxId = (reader["CC_txId"] == DBNull.Value) ? string.Empty : (string)reader["CC_txId"];
                data.CreditCardPersonIdNumber = (reader["CC_Id"] == DBNull.Value) ? string.Empty : (string)reader["CC_Id"];
                data.CreditCarddHolderName = (reader["CC_Name"] == DBNull.Value) ? string.Empty : (string)reader["CC_Name"];
                data.TotalAmount = (reader["PaymentAmount"] == DBNull.Value) ? 0m : (decimal)reader["PaymentAmount"];
                data.BalanceNew = (reader["BalanceNew"] == DBNull.Value) ? 0 : (int)reader["BalanceNew"];
                data.BalanceOld = (reader["BalanceOld"] == DBNull.Value) ? 0 : (int)reader["BalanceOld"];
                data.CreditCardToken = (reader["CC_Token"] == DBNull.Value) ? string.Empty : (string)reader["CC_Token"];
                data.CreditCardCompany = (reader["CC_Company"] == DBNull.Value) ? string.Empty : (string)reader["CC_Company"];
                data.IsFromDollar = (bool)reader["IsFromDollar"];
            }
            else
            {
                rc.Result = ResultCharging.eResult.unsuccess;
                return rc;
            }
            reader.Dispose();
            cmd.Dispose();
            data.list_component = new List<Supplier_Pricing_Component>();
            command = "EXEC [dbo].[GetPaymentRecords] @PaymentId";
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PaymentId", PaymentId);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Supplier_Pricing_Component spc = new Supplier_Pricing_Component();
                spc.BaseAmount = (reader["BaseAmount"] == DBNull.Value) ? 0 : (decimal)reader["BaseAmount"];
                spc.Amount = (decimal)reader["Amount"];
                spc.PaymentType = (ePaymentType)Enum.Parse(typeof(ePaymentType), (string)reader["PaymentType"]);
                spc.comment = (reader["Comment"] == DBNull.Value) ? null : (string)reader["Comment"];
                data.list_component.Add(spc);
            }
            conn.Close();
        }
        return ChargingZapSupplier(data, _user, _site);
     //   return (rc.Result == ResultCharging.eResult.success);
    }
     
}