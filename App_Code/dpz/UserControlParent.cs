﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;

/// <summary>
/// Summary description for UserControlParent
/// </summary>
public class UserControlParent : System.Web.UI.UserControl
{
	public UserControlParent()
	{
		//
		
		//
	}
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        string _path = ResolveUrl("~/Publisher/DpzControls/DistributionReport.js");
        string _script = "var If_Loaded = IfJSLoaded('" + _path + "');" +
                "(function (If_Loaded) {" +
                "if(If_Loaded){ $(window).scroll(_scroll); return; }" +
                "var __script = document.createElement('script');" +
                "__script.type = 'text/javascript';" +
                "__script.async = true;" +
                    "__script.src = '" + _path + "';" +
                "(document.getElementsByTagName('head')[0]).appendChild(__script);" +
            "})(If_Loaded);";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "DistributionReport.js", _script, true);

        this.DataBind();
    }
   
   
}