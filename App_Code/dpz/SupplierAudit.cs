﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

/// <summary>
/// Summary description for SupplierAudit
/// </summary>
[Serializable()]
public class SupplierAudit : Dictionary<string, string>
{
    public Guid AccountId { get; set; }
    public Guid UserId { get; set; }
    public string PageId { get; set; }
    public string PageName { get; set; }
    public string UserName { get; set; }
    public string AccountName { get; set; }
    public int SiteLangId;
   
    public SupplierAudit(Guid SupplierId, Guid UserId, string SupplierName, string PageId, string UserName, int SiteLangId, string _Page_Name)
        : base()
	{
        this.AccountId = SupplierId;
        this.AccountName = SupplierName;
        this.UserId = UserId;
        this.PageId = PageId;
        this.UserName = UserName;
        this.SiteLangId = SiteLangId;
        this.PageName = EditTranslateDB.GetPageNameTranslate(_Page_Name, SiteLangId);
	}
    public void AddAudit(string ControlId, string lblId, string value, List<WebReferenceSite.AuditEntry> list_audit)
    {
        if (this.ContainsKey(ControlId) && this[ControlId] == value)
            return;
        WebReferenceSite.AuditEntry ae = new WebReferenceSite.AuditEntry();
        ae.AdvertiseId = AccountId;
        ae.AdvertiserName = AccountName;
        ae.FieldId = ControlId;
        ae.FieldName = EditTranslateDB.GetControlTranslate(SiteLangId, PageId, lblId);
        ae.NewValue = value;
        ae.OldValue = this.ContainsKey(ControlId) ? this[ControlId] : string.Empty;
        ae.PageId = PageId;
        ae.PageName = PageName;
        ae.UserId = UserId;
        ae.UserName = UserName;
        ae.AuditStatus = string.IsNullOrEmpty(value) ? WebReferenceSite.Status.Delete :
            WebReferenceSite.Status.Update;
        list_audit.Add(ae);
        
        
 //       list_audit.Add(new WebReferenceSite.AuditEntry(){ AdvertiseId=AccountId
    }
}