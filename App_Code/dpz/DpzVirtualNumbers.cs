﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DpzVirtualNumbers
/// </summary>
[Serializable()]
public class DpzVirtualNumbers
{
    const int NumbersInPage = 12;
    public string[] numbers { get; set; }
    public bool HasNext { get; set; }
    public bool HasPrevious { get; set; }
    public DpzVirtualNumbers(string[] _numbers, bool _HasNext, bool _HasPrevious)
	{
        numbers = _numbers;
        HasNext = _HasNext;
        HasPrevious = _HasPrevious;
	}
    public DpzVirtualNumbers(string[] _numbers, int PageIndex)
    {
        int end;
        int start = GetStartEndIndex(_numbers.Length, PageIndex, out end);
        List<string> list = new List<string>();
        for (int i = start; i < end; i++)
        {
            list.Add(_numbers[i]);
        }
        numbers = list.ToArray();
        HasNext = Has_Next(_numbers, PageIndex);
        HasPrevious = Has_Previous(_numbers, PageIndex);
       // DpzVirtualNumbers dvn = new DpzVirtualNumbers(list.ToArray(), HasNext(data.NumbersList, _PageIndex), HasPrevious(data.NumbersList, _PageIndex));
    }
    bool Has_Next(string[] numbers, int IndexPage)
    {
        int _numbers = ((int)(numbers.Length / NumbersInPage)) + 1;
        if (_numbers > IndexPage)
            return true;
        return false;
    }
    bool Has_Previous(string[] numbers, int IndexPage)
    {
        return IndexPage > 1;
    }
    int GetStartEndIndex(int length, int IndexPage, out int end)
    {
        int _end = IndexPage * NumbersInPage;
        if (length > _end)
            end = _end;
        else
            end = length;
        _end = _end - NumbersInPage;
        return _end;
    }
}