﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ContainerPages
/// </summary>
public class ContainerPages<T>
{
    const int Numbers_In_Page = 12;
    public List<T> Datas { get; set; }
   // public bool HasNext { get; set; }
  //  public bool HasPrevious { get; set; }
    int _NumbersInPage;
    public int NumbersInPage 
    {
        get
        {
            if (_NumbersInPage == 0)
                _NumbersInPage = Numbers_In_Page;
            return _NumbersInPage;
        }
        set { _NumbersInPage = value; }
    }
	public ContainerPages(T[] _datas)
	{
		Datas = new List<T>(_datas);
        
	}
    public ContainerPages(T[] _datas, int NumInPage)
    {
        Datas = new List<T>(_datas);
        NumbersInPage = NumInPage;
    }

    public ContainerPageResult<T> GetPage(int PageIndex)
    {
        int end;
        int start = GetStartEndIndex(PageIndex, out end);
        List<T> list = new List<T>();
        for (int i = start; i < end; i++)
        {
            list.Add(Datas[i]);
        }
        return new ContainerPageResult<T>(list, Has_Next(PageIndex), Has_Previous(PageIndex));       
    }
    bool Has_Next(int IndexPage)
    {
        int _numbers = ((int)(Datas.Count / NumbersInPage)) + 1;
        if (_numbers > IndexPage)
            return true;
        return false;
    }
    bool Has_Previous(int IndexPage)
    {
        return IndexPage > 1;
    }
    int GetStartEndIndex(int IndexPage, out int end)
    {
        int length = Datas.Count;
        int _end = IndexPage * NumbersInPage;
        if (length > _end)
            end = _end;
        else
            end = length;
        _end = _end - NumbersInPage;
        return _end;
    }
}
public class ContainerPageResult<T>
{
    public List<T> Datas { get; set; }
    public bool HasNext { get; set; }
    public bool HasPrevious { get; set; }
    public ContainerPageResult(List<T> _Datas, bool _HasNext, bool _HasPrevious)
    {
        Datas = _Datas;
        HasNext = _HasNext;
        HasPrevious = _HasPrevious;
    }
}