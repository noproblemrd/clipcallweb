﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for NeighborhoodControl
/// </summary>
public class NeighborhoodControl
{
    Dictionary<Guid, string> dic;
	public NeighborhoodControl(Dictionary<Guid, string> dic)
	{
        this.dic = dic;
	}
    public XElement GetControl()
    {
        XElement control = new XElement("div");
        /*
        string _func = @"function SetPlaces(_id, IsChoosen) {
             var hid_places = document.getElementById('hid_places');
             if (IsChoosen) 
                 hid_places.value += _id + ',';
             else 
                 hid_places.value = hid_places.value.replace(_id + ',', '');
         }";
        XElement _script = new XElement("script", _func);
        _script.Add(new XAttribute("type", @"text/javascript"));
        control.Add(_script);
         * */
        XElement table = new XElement("table");

        XElement tr = null;// new XElement("tr");
        int i = 0;

        foreach (KeyValuePair<Guid, string> kvp in dic)
        {
            if (i % 5 == 0)
            {
                if (tr != null)
                    table.Add(tr);
                tr = new XElement("tr");
            }
            XElement td = new XElement("td");
            XElement cb = new XElement("input");
            cb.Add(new XAttribute("type", "checkbox"));
            string run_script = "SetPlaces('" + kvp.Key.ToString() + "', this.checked);";
            cb.Add(new XAttribute("onclick", run_script));
            cb.Add(new XAttribute("value", kvp.Value));
            XElement lbl = new XElement("span", kvp.Value);

            td.Add(cb);
            td.Add(lbl);
            tr.Add(td);
            i++;
        }
        table.Add(tr);
        control.Add(table);
        XElement _hidden = new XElement("input");
        _hidden.Add(new XAttribute("type", "hidden"));
        _hidden.Add(new XAttribute("id", "hid_places"));
        control.Add(_hidden);
        return control;
    }
}