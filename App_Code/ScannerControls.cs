using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ScannerControls
/// </summary>
public delegate void myFunc(Control control);
public abstract class ScannerControls
{
    protected Control _control;
	public ScannerControls(Control _control)
	{
        this._control = _control;
	}
    protected void ApplyToControls(Control root, myFunc func)
    {
  //      if (root.HasControls())
        if(SearchChilds(root))
        {
            foreach (Control control in root.Controls)
            {
                func(control);
                ApplyToControls(control, func);
            }
        }
    }
    protected virtual bool SearchChilds(Control control)
    {
        return control.HasControls();
    }
}
