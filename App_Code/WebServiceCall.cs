﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;
/// <summary>
/// Summary description for WebServiceCall
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.None)] 
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceCall : System.Web.Services.WebService
{

    public WebServiceCall()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    private string unScramble(string phoneEncryption)
    {
        string returnStr = "";


        string[,] arrNumber = new string[10, 2];
        arrNumber[0, 0] = "0";
        arrNumber[0, 1] = "z";
        arrNumber[1, 0] = "1";
        arrNumber[1, 1] = "e";
        arrNumber[2, 0] = "2";
        arrNumber[2, 1] = "k";
        arrNumber[3, 0] = "3";
        arrNumber[3, 1] = "b";
        arrNumber[4, 0] = "4";
        arrNumber[4, 1] = "r";
        arrNumber[5, 0] = "5";
        arrNumber[5, 1] = "w";
        arrNumber[6, 0] = "6";
        arrNumber[6, 1] = "s";
        arrNumber[7, 0] = "7";
        arrNumber[7, 1] = "i";
        arrNumber[8, 0] = "8";
        arrNumber[8, 1] = "p";
        arrNumber[9, 0] = "9";
        arrNumber[9, 1] = "f";

        char[] charArrPhone = phoneEncryption.ToCharArray();
        bool ifFound = false;
        for (int indexPhone = 0; indexPhone < charArrPhone.Length; indexPhone++)
        {
            ifFound = false;
            for (int i = 0; i <= arrNumber.GetUpperBound(0); i++)
            {
                if (charArrPhone[indexPhone].ToString() == arrNumber[i, 1].ToString())
                {
                    returnStr += arrNumber[i, 0];
                    ifFound = true;
                    break;
                }
            }

            if (ifFound)
            {
            }
            else
                returnStr += "n";

        }


        return returnStr;
    }

    [WebMethod(EnableSession = true)]
    public string callNonPpc(string SiteId, string AdvertiserId, string AdvertiserName, string AdvertiserPhoneNumber1, string AdvertiserPhoneNumber2, string ContactPhoneNumber)
    {
        string replayStr = "";
        long resultPhoneValidation;

        string phone1AdvertiserDecryption = unScramble(AdvertiserPhoneNumber1);
        if (phone1AdvertiserDecryption == "" || !Int64.TryParse(phone1AdvertiserDecryption, out resultPhoneValidation))
        {
            LogEdit.SaveLog(SiteId, "CreateServiceNonppcService status: no valid advertiser phone1 " +
            "\r\nadvertiser phone 1: " + phone1AdvertiserDecryption +
            "\r\nsurfer phone: " + ContactPhoneNumber +
            "\r\nSession: " + HttpContext.Current.Session.SessionID +
            "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

            return replayStr = "unsuccess";
        }


        string phone2AdvertiserDecryption = unScramble(AdvertiserPhoneNumber2);
        if (AdvertiserPhoneNumber2.Length > 0 && !Int64.TryParse(phone2AdvertiserDecryption, out resultPhoneValidation))
        {
            LogEdit.SaveLog(SiteId, "CreateServiceNonppcService status: no valid advertiser phone2 " +
            "\r\nadvertiser phone 2: " + phone2AdvertiserDecryption +
            "\r\nsurfer phone: " + ContactPhoneNumber +
            "\r\nSession: " + HttpContext.Current.Session.SessionID +
            "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

            return replayStr = "unsuccess";
        }

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.NonPpcServiceRequest request = new WebReferenceCustomer.NonPpcServiceRequest();
        request.AdvertiserId = AdvertiserId; // must
        request.AdvertiserName = AdvertiserName;
        request.AdvertiserPhoneNumber = phone1AdvertiserDecryption;
        request.AdvertiserPhoneNumber2 = phone2AdvertiserDecryption;
        request.ContactPhoneNumber = ContactPhoneNumber;



        WebReferenceCustomer.ResultOfNonPpcServiceResponse response = new WebReferenceCustomer.ResultOfNonPpcServiceResponse();
        try
        {
            response = customer.CreateNonPpcService(request);

            if (response.Type == WebReferenceCustomer.eResultType.Success)
            {
                //Response.Write(serviceResponse.ServiceRequestId + " status:" + serviceResponse.Status.ToString().ToLower());
                //Response.Write(response.Value.ServiceRequestId);
                replayStr = response.Value.ServiceRequestId;

                LogEdit.SaveLog(SiteId, "CreateServiceNonppcService status:" + response.Value.Status.ToString() +
                    "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                    "\r\nsurfer phone: " +
                    ContactPhoneNumber +
                    "\r\nSession: " + HttpContext.Current.Session.SessionID + "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

            }

            else
            {

                replayStr = "unsuccess";

                //Response.Write("unsuccess");
                LogEdit.SaveLog(SiteId, "CreateServiceNonppcService status: " + response.Value.Status.ToString() +
                     "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                    "\r\nsurfer phone: " + ContactPhoneNumber +
                    "\r\nSession: " + HttpContext.Current.Session.SessionID +
                    "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request) + "\r\nmessage: " + response.Messages.ToArray().ToString());
            }
        }

        catch (Exception ex)
        {

            replayStr = "server problem " + ex.Message;
            LogEdit.SaveLog(SiteId, "CreateServiceNonppcService status: " +
                ex.Message + "\r\nsurfer phone: " + ContactPhoneNumber);
        }



        return replayStr;


    }

    [WebMethod(EnableSession = true)]
    public string callNonPpc2(string SiteId, string AdvertiserId, string AdvertiserName, string AdvertiserPhoneNumber1, string ContactPhoneNumber)
    {
        string replayStr = "";
        long resultPhoneValidation;

        string phone1AdvertiserDecryption = AdvertiserPhoneNumber1;
        if (phone1AdvertiserDecryption == "" || !Int64.TryParse(phone1AdvertiserDecryption, out resultPhoneValidation))
        {
            LogEdit.SaveLog(SiteId, "CreateServiceNonppcService status: no valid advertiser phone1 " +
            "\r\nadvertiser phone 1: " + phone1AdvertiserDecryption +
            "\r\nsurfer phone: " + ContactPhoneNumber +
            "\r\nSession: " + HttpContext.Current.Session.SessionID +
            "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

            return replayStr = "unsuccess";
        }


        string phone2AdvertiserDecryption = "";
       

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.NonPpcServiceRequest request = new WebReferenceCustomer.NonPpcServiceRequest();
        request.AdvertiserId = AdvertiserId; // must
        request.AdvertiserName = AdvertiserName;
        request.AdvertiserPhoneNumber = phone1AdvertiserDecryption;
        request.AdvertiserPhoneNumber2 = phone2AdvertiserDecryption;
        request.ContactPhoneNumber = ContactPhoneNumber;



        WebReferenceCustomer.ResultOfNonPpcServiceResponse response = new WebReferenceCustomer.ResultOfNonPpcServiceResponse();
        try
        {
            response = customer.CreateNonPpcService(request);

            if (response.Type == WebReferenceCustomer.eResultType.Success)
            {
                //Response.Write(serviceResponse.ServiceRequestId + " status:" + serviceResponse.Status.ToString().ToLower());
                //Response.Write(response.Value.ServiceRequestId);
                replayStr = response.Value.ServiceRequestId;

                LogEdit.SaveLog(SiteId, "CreateServiceNonppcService status:" + response.Value.Status.ToString() +
                    "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                    "\r\nsurfer phone: " +
                    ContactPhoneNumber +
                    "\r\nSession: " + HttpContext.Current.Session.SessionID + "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

            }

            else
            {

                replayStr = "unsuccess";

                //Response.Write("unsuccess");
                LogEdit.SaveLog(SiteId, "CreateServiceNonppcService status: " + response.Value.Status.ToString() +
                     "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                    "\r\nsurfer phone: " + ContactPhoneNumber +
                    "\r\nSession: " + HttpContext.Current.Session.SessionID +
                    "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request) + "\r\nmessage: " + response.Messages.ToArray().ToString());
            }
        }

        catch (Exception ex)
        {

            replayStr = "server problem " + ex.Message;
            LogEdit.SaveLog(SiteId, "CreateServiceNonppcService status: " +
                ex.Message + "\r\nsurfer phone: " + ContactPhoneNumber);
        }



        return replayStr;


    }

    [WebMethod]
    public WebReferenceReports.ResultOfListOfC2CData CreateC2cReport(DateTime date, string siteId)
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(null, siteId);
   //     return reports.CreateC2cReport(command, date, format, fields);
        WebReferenceReports.ResultOfListOfC2CData result = null;
        try
        {
            result = reports.CreateC2cReport(date);
        }
        catch (Exception xec)
        {
            dbug_log.ExceptionLog(xec);
        }

        return result;
    }

    [WebMethod]
    public WebReferenceReports.ResultOfListOfSimsCallData CreateSimsReport(DateTime date, string siteId)
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(null, siteId);
        //     return reports.CreateC2cReport(command, date, format, fields);
        WebReferenceReports.ResultOfListOfSimsCallData result = null;
        try
        {
            result = reports.CreateSimsReport(date);
        }
        catch (Exception xec)
        {
            dbug_log.ExceptionLog(xec);
        }
        return result;
    }

    [WebMethod]
    public WebReferenceReports.ResultOfListOfPotentialStatRow CreatePotentialStatsReport(string siteId)
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(null, siteId);
        //     return reports.CreateC2cReport(command, date, format, fields);
        WebReferenceReports.ResultOfListOfPotentialStatRow result = null;
        try
        {
            result = reports.ZgPotentialStatsReport();
        }
        catch (Exception xec)
        {
            dbug_log.ExceptionLog(xec);
        }
        return result;
    }
    

    [WebMethod(EnableSession = true)]
    public string callPpc(string ExpertiseCode, string ExpertiseType, string RegionCode, string RegionLevel, string SiteId, string SupplierId, string OriginId, string ContactPhoneNumber, string SessionID)
    {
        string replayStr = "";

        LogEdit.SaveLog(SiteId, "Start:" + "\r\nsurfer phone: " +
        ContactPhoneNumber +
        "\r\nSession: " + Session.SessionID +
        "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

        try
        {
            WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
            WebReferenceCustomer.SupplierServiceRequest request = new WebReferenceCustomer.SupplierServiceRequest();
            request.ExpertiseCode = ExpertiseCode;
            request.ExpertiseType = Convert.ToInt32(ExpertiseType);
            request.RegionCode = RegionCode;
            request.RegionLevel = Convert.ToInt32(RegionLevel);
            request.RequestDescription = "";
            request.SiteId = SiteId;
            request.SupplierId = new Guid(SupplierId);
            request.OriginId = new Guid(OriginId);
            request.ContactPhoneNumber = ContactPhoneNumber;
            request.SessionId = SessionID;

            WebReferenceCustomer.ResultOfSupplierServiceResponse response = new WebReferenceCustomer.ResultOfSupplierServiceResponse();

            response = customer.CreateSupplierSpecificService(request);

            if (response.Type == WebReferenceCustomer.eResultType.Success)
            {
                //Response.Write(serviceResponse.ServiceRequestId + " status:" + serviceResponse.Status.ToString().ToLower());

                replayStr = response.Value.ServiceRequestId;

                LogEdit.SaveLog(SiteId, "CreateServicePpcService status:" + response.Value.Status.ToString() +
                    "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                    "\r\nsurfer phone: " +
                    ContactPhoneNumber +
                    "\r\nSession: " + Session.SessionID + "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

            }

            else
            {

                replayStr = "unsuccess";

                LogEdit.SaveLog(SiteId, response.Value.Status.ToString() +
                     "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                    "\r\nsurfer phone: " + ContactPhoneNumber +
                    "\r\nSession: " + Session.SessionID +
                    "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request) + "\r\nmessage: " + response.Messages.ToArray().ToString());

            }
        }

        catch (Exception ex)
        {

            replayStr = "server problem " + ex.Message;
            LogEdit.SaveLog(SiteId, "CreateServicePpcService status: " +
                ex.Message + "\r\nsurfer phone: " + ContactPhoneNumber);
        }
        return replayStr;
    }

    [WebMethod(EnableSession = true)]
    public bool sendSms(string phone, string supplierId, string url, string siteId)
    {
        bool replayStr=false;

        WebReferenceSite.Site site = WebServiceConfig.GetSiteReference(null, siteId);
        WebReferenceSite.ResultOfBoolean result = new WebReferenceSite.ResultOfBoolean();

        try
        {            
            WebReferenceSite.SendToMyMobileRequest request=new WebReferenceSite.SendToMyMobileRequest();
            request.Phone = phone;
            request.SupplierId = new Guid(supplierId);
            request.Url = url;
            result = site.SendToMyMobile(request);

            if (result.Value)
            {
                replayStr = true;

            }
            else
            {
                replayStr = false;

                LogEdit.SaveLog(siteId, "problem with sms request: " + result.Messages[0].ToString() +
                     "\r\nphone: " + phone +
                    "\r\nsupplierId: " + supplierId +
                    "\r\nurl: " + url +
                    "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));
            }
        }

        catch(Exception ex)
        {

            LogEdit.SaveLog(siteId, "server problem " + ex.Message +
                 "\r\nphone: " + phone +
                    "\r\nsupplierId: " + supplierId +
                    "\r\nurl: " + url +
                    "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

            replayStr = false;
           
        }

         return replayStr;
        
    }

    [WebMethod(EnableSession = true)]
    public string CallPpcWithExposureData(
        string expertiseCode, string expertiseType, string regionCode, string regionLevel, string siteId, string supplierId, 
        string originId, string contactPhoneNumber, string sessionID,
        string domain, string url, string controlName, string pageName, string placeInWebSite)
    {
        string replayStr = "";
        LogEdit.SaveLog(siteId, "Start:" + "\r\nsurfer phone: " +
        contactPhoneNumber +
        "\r\nSession: " + Session.SessionID +
        "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));
        try
        {
            WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
            WebReferenceCustomer.SupplierServiceRequest request = new WebReferenceCustomer.SupplierServiceRequest();
            request.ExpertiseCode = expertiseCode;
            request.ExpertiseType = Convert.ToInt32(expertiseType);
            request.RegionCode = regionCode;
            request.RegionLevel = Convert.ToInt32(regionLevel);
            request.RequestDescription = "";
            request.SiteId = siteId;
            request.SupplierId = new Guid(supplierId);
            request.OriginId = new Guid(originId);
            request.ContactPhoneNumber = contactPhoneNumber;
            request.SessionId = sessionID;
            request.Domain = domain;
            request.Url = url;
            request.ControlName = controlName;
            request.PageName = pageName;
            request.PlaceInWebSite = placeInWebSite;
            WebReferenceCustomer.ResultOfSupplierServiceResponse response = new WebReferenceCustomer.ResultOfSupplierServiceResponse();
            response = customer.CreateSupplierSpecificService(request);
            if (response.Type == WebReferenceCustomer.eResultType.Success)
            {
                replayStr = response.Value.ServiceRequestId;
                LogEdit.SaveLog(siteId, "CreateServicePpcService status:" + response.Value.Status.ToString() +
                    "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                    "\r\nsurfer phone: " +
                    contactPhoneNumber +
                    "\r\nSession: " + Session.SessionID + "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));
            }
            else
            {

                replayStr = "unsuccess";
                LogEdit.SaveLog(siteId, response.Value.Status.ToString() +
                     "\r\nid: " + response.Value.ServiceRequestId.ToString() +
                    "\r\nsurfer phone: " + contactPhoneNumber +
                    "\r\nSession: " + Session.SessionID +
                    "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request) + "\r\nmessage: " + response.Messages.ToArray().ToString());
            }
        }
        catch (Exception ex)
        {

            replayStr = "server problem " + ex.Message;
            LogEdit.SaveLog(siteId, "CreateServicePpcService status: " +
                ex.Message + "\r\nsurfer phone: " + contactPhoneNumber);
        }
        return replayStr;
    }

    [WebMethod(EnableSession = true)]
    public string ClickTo(string ExpertiseCode, string ExpertiseType, string RegionCode,
        string RegionLevel, string SiteId, string SupplierId, string OriginId,
        string ContactPhoneNumber, string description, string ChannelCode, string SessionId)
    {
        description = description.Replace("%26", "&");
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.SupplierServiceRequest request = new WebReferenceCustomer.SupplierServiceRequest();
        request.ExpertiseCode = ExpertiseCode;
        request.ExpertiseType = Convert.ToInt32(ExpertiseType);
        request.RegionCode = RegionCode;
        request.RegionLevel = Convert.ToInt32(RegionLevel);
        request.RequestDescription = description;
        request.SiteId = SiteId;
        request.SupplierId = new Guid(SupplierId);
        request.OriginId = new Guid(OriginId);
        request.ContactPhoneNumber = ContactPhoneNumber;
        request.SessionId = SessionId;
        WebReferenceCustomer.ChannelCode cc = WebReferenceCustomer.ChannelCode.Email;
        foreach (WebReferenceCustomer.ChannelCode _cc in Enum.GetValues(typeof(WebReferenceCustomer.ChannelCode)))
        {
            if (_cc.ToString().ToLower() == ChannelCode.ToLower())
            {
                cc = _cc;
                break;
            }
        }
        request.Channel = cc;


        WebReferenceCustomer.ResultOfSupplierServiceResponse response = new WebReferenceCustomer.ResultOfSupplierServiceResponse();
        response = customer.CreateSupplierSpecificService(request);
        return response.Value.Status.ToString();


        //        WebReferenceCustomer.StatusCode.Success
    }

    /*
     var arrNumber=new Array();
    arrNumber[0]=new Array(2);
    arrNumber[0][0]="0";    
    arrNumber[0][1]="z";
    
    arrNumber[1]=new Array(2);
    arrNumber[1][0]="1";
    arrNumber[1][1]="e";
    
    arrNumber[2]=new Array(2);
    arrNumber[2][0]="2";
    arrNumber[2][1]="k";
    
    arrNumber[3]=new Array(2);
    arrNumber[3][0]="3";
    arrNumber[3][1]="b";
    
    arrNumber[4]=new Array(2);
    arrNumber[4][0]="4";
    arrNumber[4][1]="r";
    
    arrNumber[5]=new Array(2);
    arrNumber[5][0]="5";
    arrNumber[5][1]="w";
    
    arrNumber[6]=new Array(2);
    arrNumber[6][0]="6";
    arrNumber[6][1]="s";
    
    arrNumber[7]=new Array(2);
    arrNumber[7][0]="7";
    arrNumber[7][1]="i";
    
    arrNumber[8]=new Array(2);
    arrNumber[8][0]="8";
    arrNumber[8][1]="p";
    
    arrNumber[9]=new Array(2);
    arrNumber[9][0]="9";
    arrNumber[9][1]="f";
    */

    private bool ifNullOrEmpty(Object[] arr)
    {
        bool ifAnyNullOrEmpy = false;
        /*
        for (int i = 0; i < arr.Length; i++)
        {
            if (String.IsNullOrEmpty(arr.GetEnumerator())
                ifAnyNullOrEmpy = true;
        }
        */

        System.Collections.IEnumerator myEnumerator = arr.GetEnumerator();
        while ((myEnumerator.MoveNext()) && (myEnumerator.Current != null))
            if (String.IsNullOrEmpty(myEnumerator.Current.ToString()))
            {
                ifAnyNullOrEmpy = true;
                break;

            }

        return ifAnyNullOrEmpy;

    }
    private bool ifNullOrEmpty(List<string> arr)
    {
        bool IsNullOrEmpty = ((from x in arr
                             where string.IsNullOrEmpty(x)
                                   select x).Count() > 0);
        return IsNullOrEmpty;
    }
    
    [WebMethod(MessageName="Exposure")]
    public string Exposure(string SiteId, string OriginId, string Domain,
        string Url, string HeadingCode, int HeadingLevel, string RegionCode,
        int RegionLevel, string Channel, string ExposureType,
        string PageName, string PlaceInWebSite, string ControlName,
        string SessionId
        )
    {
        /*Must this parameters SiteId,OriginId,HeadingCode,HeadingLevel,RegionCode,RegionLevel,ExposureType*/
        Object[] arrMust = new Object[7];
        arrMust[0] = SiteId;
        arrMust[1] = OriginId;
        arrMust[2] = HeadingCode;
        arrMust[3] = HeadingLevel;
        arrMust[4] = RegionCode;
        arrMust[5] = RegionLevel;
        arrMust[6] = ExposureType;


        bool ifAnyNullOrEmpy = ifNullOrEmpty(arrMust);
        if (ifAnyNullOrEmpy)
        {
            return "Failure: Required parameter is missing";
        }

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.CreateExposureRequest request = new WebReferenceCustomer.CreateExposureRequest();

        request.SiteId = SiteId;
        request.OriginId = new Guid(OriginId);
        request.Domain = Domain;
        request.Url = Url;
        request.HeadingCode = HeadingCode;
        request.HeadingLevel = HeadingLevel;
        request.RegionCode = RegionCode;
        request.RegionLevel = RegionLevel;
        
        if (Context != null && Context.Request != null)
        {
            request.Browser = Context.Request.Browser.Browser;
            request.BrowserVersion = Context.Request.Browser.Version;
        }

        switch (Channel.ToLower())
        {
            case "call":
                request.Channel = WebReferenceCustomer.ChannelCode.Call;

                break;

            case "email":
                request.Channel = WebReferenceCustomer.ChannelCode.Email;
                break;

            case "fax":
                request.Channel = WebReferenceCustomer.ChannelCode.Fax;
                break;

            case "sms":
                request.Channel = WebReferenceCustomer.ChannelCode.SMS;
                break;



        }

        switch (ExposureType.ToLower())
        {
            case "listing":
                request.Type = WebReferenceCustomer.ExposureType.Listing;
                break;

            case "widget":
                request.Type = WebReferenceCustomer.ExposureType.Widget;
                break;

            default:
                return "Failure: wrong ExposureType value";

        }

        request.PageName = PageName;
        request.PlaceInWebSite = PlaceInWebSite;
        request.ControlName = ControlName;
        request.SessionId = SessionId;

        WebReferenceCustomer.Result result = new WebReferenceCustomer.Result();
        result = customer.CreateExposure(request);

        if (result.Type == WebReferenceCustomer.eResultType.Success)
            return "Success";


        else
            return "Failure: " + result.Messages[0];
    }
    
    [WebMethod(MessageName = "ExposureWithKeyword")]
    public string Exposure(string SiteId, string OriginId, string Domain,
        string Url, string HeadingCode, int HeadingLevel, string RegionCode,
        int RegionLevel, string Channel, string ExposureType,
        string PageName, string PlaceInWebSite, string ControlName,
        string SessionId, string Keyword
        )
    {
        /*Must this parameters SiteId,OriginId,HeadingCode,HeadingLevel,RegionCode,RegionLevel,ExposureType*/
        Object[] arrMust = new Object[7];
        arrMust[0] = SiteId;
        arrMust[1] = OriginId;
        arrMust[2] = HeadingCode;
        arrMust[3] = HeadingLevel;        
        arrMust[4] = ExposureType;
        arrMust[5] = RegionCode;
        arrMust[6] = RegionLevel;

        bool ifAnyNullOrEmpy = ifNullOrEmpty(arrMust);
        if (ifAnyNullOrEmpy)
        {
            return "Failure: Required parameter is missing";
        }

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        customer.Timeout = 300000;
        WebReferenceCustomer.CreateExposureRequest request = new WebReferenceCustomer.CreateExposureRequest();

        request.SiteId = SiteId;
        request.OriginId = new Guid(OriginId);
        request.Domain = Domain;
        request.Url = Url;
        request.HeadingCode = HeadingCode;
        request.HeadingLevel = HeadingLevel;        
        request.RegionLevel = RegionLevel;
        request.RegionCode = RegionCode;
        if (Context != null && Context.Request != null)
        {
            request.Browser = Context.Request.Browser.Browser;
            request.BrowserVersion = Context.Request.Browser.Version;
        }

        request.Keyword = Keyword;

        switch (Channel.ToLower())
        {
            case "call":
                request.Channel = WebReferenceCustomer.ChannelCode.Call;

                break;

            case "email":
                request.Channel = WebReferenceCustomer.ChannelCode.Email;
                break;

            case "fax":
                request.Channel = WebReferenceCustomer.ChannelCode.Fax;
                break;

            case "sms":
                request.Channel = WebReferenceCustomer.ChannelCode.SMS;
                break;



        }

        switch (ExposureType.ToLower())
        {
            case "listing":
                request.Type = WebReferenceCustomer.ExposureType.Listing;
                break;

            case "widget":
                request.Type = WebReferenceCustomer.ExposureType.Widget;
                break;

            default:
                return "Failure: wrong ExposureType value";

        }

        request.PageName = PageName;
        request.PlaceInWebSite = PlaceInWebSite;
        request.ControlName = ControlName;
        request.SessionId = SessionId;

        WebReferenceCustomer.Result result = new WebReferenceCustomer.Result();
        result = customer.CreateExposure(request);

        if (result.Type == WebReferenceCustomer.eResultType.Success)
        {
            return "Success";
        }

        else
        {
            StringBuilder sbExposure = new StringBuilder();
            sbExposure.Append("SiteId:" + SiteId);
            sbExposure.Append(" OriginId:" + OriginId);
            sbExposure.Append(" Domain:" + Domain);
            sbExposure.Append(" Url:" + Url);
            sbExposure.Append(" HeadingCode:" + HeadingCode);
            sbExposure.Append(" HeadingLevel:" + HeadingLevel);
            sbExposure.Append(" RegionCode:" + RegionCode);
            sbExposure.Append(" RegionLevel:" + RegionLevel);
            sbExposure.Append(" Keyword:" + Keyword);

            dbug_log.ExceptionGlobalLog(new System.ArgumentException("exposure", sbExposure.ToString()));

            return "Failure: " + result.Messages[0];
        }
    }
    /*
    [WebMethod(MessageName = "ExposureUsaWithKeyword")]
    public string Exposure(string SiteId, string OriginId, string Domain,
        string Url, string HeadingCode, int HeadingLevel, string RegionCode,
        string RegionLevel, string Channel, string ExposureType,
        string PageName, string PlaceInWebSite, string ControlName,
        string SessionId, string Keyword
        )
    {
        string Browser = null, BrowserVersion = null;
        if (Context != null && Context.Request != null)
        {
            Browser = Context.Request.Browser.Browser;
            BrowserVersion = Context.Request.Browser.Version;
        }
        return SalesUtility.Exposure(SiteId, OriginId, Domain, Url, HeadingCode, HeadingLevel, RegionCode,
                    RegionLevel, Channel, ExposureType, PageName, PlaceInWebSite, ControlName, SessionId, Keyword, null,
                    null, null, Browser, BrowserVersion, eWelcomeExposureEvent.none, HttpContext.Current);
    }   

    [WebMethod]
    public string ExposureWithIsAuction(string SiteId, string OriginId, string Domain,
        string Url, string HeadingCode, int HeadingLevel, string RegionCode,
        int RegionLevel, string Channel, string ExposureType,
        string PageName, string PlaceInWebSite, string ControlName,
        string SessionId
        )
    {
        string exposureResult = Exposure(SiteId, OriginId, Domain, Url, HeadingCode, HeadingLevel, RegionCode, RegionLevel, Channel,
            ExposureType, PageName, PlaceInWebSite, ControlName, SessionId);

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        var isAuctionResult = customer.IsHeadingForAuction(HeadingCode, HeadingLevel);
        if (isAuctionResult.Type == WebReferenceCustomer.eResultType.Success)
        {
            exposureResult = exposureResult + ";" + (isAuctionResult.Value ? "1" : "0");
        }
        else
        {
            exposureResult = exposureResult + ";1";
        }
        return exposureResult;
    }
     * */

}

