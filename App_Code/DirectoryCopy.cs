using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

/// <summary>
/// Summary description for DirectoryCopy
/// </summary>
public class DirectoryCopy
{
	public DirectoryCopy()
	{
		//
		
		//
	}
    public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
    {
        // Check if the target directory exists, if not, create it.
        if (Directory.Exists(target.FullName) == false)
        {
            Directory.CreateDirectory(target.FullName);
        }

        // Copy each file into it�s new directory.
        foreach (FileInfo fi in source.GetFiles())
        { 
            fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
        }

        // Copy each subdirectory using recursion.
        foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
        {
            DirectoryInfo nextTargetSubDir =
                target.CreateSubdirectory(diSourceSubDir.Name);
            CopyAll(diSourceSubDir, nextTargetSubDir);
        }
    }

    public static void DeleteAllFilesDirectory(DirectoryInfo source, DateTime date)
    {        
        foreach (DirectoryInfo di in source.GetDirectories())
        {
            if (di.LastWriteTimeUtc < date)
            {
                _deleteAllFiles(di);
                di.Delete();
            }
        }        
    }
    public static void _deleteAllFiles(DirectoryInfo source)
    {
        foreach (FileInfo fi in source.GetFiles())
        {            
                fi.Delete();
        }
        foreach (DirectoryInfo di in source.GetDirectories())
        {
            _deleteAllFiles(di);
            di.Delete();
        }
    }
}
