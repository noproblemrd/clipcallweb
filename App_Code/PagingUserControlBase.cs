﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for PagingUserControlBase
/// </summary>
public abstract class PagingUserControlBase<T> : System.Web.UI.UserControl
{
    protected const string BASE_PATH = "~/Publisher/ClipCallControls/";
    public PagingUserControlBase()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlGenericControl hgc = new HtmlGenericControl("script");
        hgc.Attributes["type"] = "text/javascript";
        hgc.Attributes["src"] = ResolveUrl(BASE_PATH + "PagingScript.js");
        Page.Header.Controls.Add(hgc);
   //     ClearSessions();
    }
    public abstract string SessionTableName { get; }
    public abstract void BindReport(T[] datas);
    protected abstract void Data_Bind(DataTable dt);
    
    public void ClearSessions()
    {
        PageIndex = 0;
        SetDataV(null);
    }

    public virtual bool SetDataByPage()
    {
        DataTable data = GetDataV();
        if (data == null)
            return false;
        PageIndex = PageIndex + 1;
        int _index = PageIndex;
        int _min = (_index * 20);
        IEnumerable<DataRow> data_row = (from x in data.AsEnumerable()
                                         select x).Skip(_min).Take(20);

        if (data_row.Count() == 0)
            return false;
        Data_Bind(data_row.CopyToDataTable());
       
        return true;
    }
    protected void SetDataV(DataTable dt)
    {
        Session[SessionTableName] = dt;
    }
    protected DataTable GetDataV()
    {
        return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName];
    }
    string SessionPage
    {
        get
        {
            return SessionTableName + "Paging";
        }
    }
    int PageIndex
    {
        get { return (Session[SessionPage] == null) ? 0 : (int)Session[SessionPage]; }
        set { Session[SessionPage] = value; }
    }
   
}