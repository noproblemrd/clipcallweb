﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using ClosedXML.Excel;

//using Excel = Microsoft.Office.Interop.Excel;

/// <summary>
/// Summary description for ToExcel
/// </summary>
public class ToExcel
{
    string EXCEL_NAME;
    Page _page;

    public ToExcel(Page _page, string name)
    {
        this._page = _page;
        this.EXCEL_NAME = name;
    }
    /*
    public string _CreateExcel(DataTable data)
    {
        if (data.Rows.Count == 0)
            return string.Empty;
        string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int rnd = new Random().Next();
        string filename = rnd + EXCEL_NAME + ".xls";
        path += filename;

        // Create an Excel object and add workbook...
    Excel.ApplicationClass excel = new Excel.ApplicationClass();
    Excel.Workbook workbook = excel.Application.Workbooks.Add(true);

        if (excel == null)
        {
            dbug_log.MessageLog("Excel is not properly installed!!");
            return null;
        }
        int iCol = 0;
        foreach (DataColumn c in data.Columns)
        {
            iCol++;
            excel.Cells[1, iCol] = c.ColumnName;
        }
        // for each row of data...
        int iRow = 0;
        foreach (DataRow r in data.Rows)
        {
            iRow++;

            // add each row's cell data...
            iCol = 0;
            foreach (DataColumn c in data.Columns)
            {
                iCol++;
                excel.Cells[iRow + 1, iCol] = r[c.ColumnName];
            }
        }

        object missing = System.Reflection.Missing.Value;

        // If wanting to Save the workbook...
        workbook.SaveAs(path,
        Excel.XlFileFormat.xlXMLSpreadsheet, missing, missing,
        false, false, Excel.XlSaveAsAccessMode.xlNoChange,
        missing, missing, missing, missing, missing);

        // If wanting to make Excel visible and activate the worksheet...
        excel.Visible = true;
        Excel.Worksheet worksheet = (Excel.Worksheet)excel.ActiveSheet;
        ((Excel._Worksheet)worksheet).Activate();

        // If wanting excel to shutdown...
        ((Excel._Application)excel).Quit();
        return filename;
    }
    */
    public string CreateExcel(DataTable data)
    {
        if (data.Rows.Count == 0)
            return string.Empty;
        string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int rnd = new Random().Next();
        string filename = rnd + EXCEL_NAME + ".xlsx";
        path += filename;
      
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path +
            ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\";";
        /*
              string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path +
                 ";Mode=ReadWrite;Extended Properties=\"Excel 8.0;HDR=Yes;\";";
             

        string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path +
           ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\";";
           */
        OleDbConnection objConn = new OleDbConnection(connString);
        List<KeyValuePair<string, string>> list = GetDataTable.GetColumnsNamesCaption(data);
        if (list.Count == 0)
            return string.Empty;
        string command = "CREATE TABLE [Sheet1] (";
        string CurrencySymbole = "";
        foreach (KeyValuePair<string, string> kvp in list)
        {
            string _type;
            if (kvp.Value.StartsWith("IsCurrency"))
            {
                CurrencySymbole = kvp.Value.Split(';')[1];
                _type = "CURRENCY";
            }
            else if (kvp.Value == "IsNumber")
                _type = "NUMERIC";
            else
            {
                if (data.Rows.Count > 0)
                {
                    _type = (IsNumber(data.Rows[0][kvp.Key])) ? "NUMERIC" : "MEMO";
                }
                else
                    _type = "MEMO";
            }
            //  command += "[" + kvp.Key + "] NTEXT,";
            command += "[" + kvp.Key + "] " + _type + ",";
            //        command += "[" + s + "] user-defined-type,";
            //           command += "[" + s + "] nvarchar(255),";
        }
        command = command.Substring(0, command.Length - 1) + ")";
        string SiteId = ((SiteSetting)(HttpContext.Current.Session["Site"])).GetSiteID;
        try
        {
            objConn.Open();
            OleDbCommand cmd = new OleDbCommand(command, objConn);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            string commandInsert = GetInsertString(list);
            
     //       int t = 0;
            foreach (DataRow row in data.Rows)
            {

                //       string commandInsert = "Insert into [Sheet1$] ([Code], [Segment], [Auction], [Min price], [Show certificate]) " +
                //           "VALUES (@code, @segment, @auction, @MinPrice, @certificate)";
                cmd = new OleDbCommand(commandInsert, objConn);
      //          dbug_log.MessageLog("index = " + t++);
                foreach (KeyValuePair<string, string> kvp in list)
                {
                    /*
                    string newS = s.Replace("'", SiteId);
                    cmd.Parameters.AddWithValue("@" + newS.Replace(" ", "_"), row[s].ToString());
                     * */
                    string _value;
                    if (kvp.Value.StartsWith("IsCurrency"))
                        _value = row[kvp.Key].ToString().Replace(CurrencySymbole, "");
                    /*
                else if(kvp.Value == "IsPercentage")
                    _value = row[kvp.Key].ToString().Replace("%", "");
                     * */
                    else
                        _value = row[kvp.Key].ToString();
                    if (string.IsNullOrEmpty(_value) && (kvp.Value.StartsWith("IsCurrency") || kvp.Value == "IsNumber"))
                        _value = "0";
                    if (_value.Length > 255)
                        _value = _value.Substring(0, 255);
                    cmd.Parameters.AddWithValue("@" + GetValidString(kvp.Key), _value);
                }

                int b = cmd.ExecuteNonQuery();
                cmd.Dispose();
            }

        }
        catch (Exception ex)
        {
   //         dbug_log.MessageLog("Exception ex");
            dbug_log.ExceptionLog(ex);
            return string.Empty;
        }
        finally
        {
   //         dbug_log.MessageLog("finally");
            if (objConn.State != ConnectionState.Closed)
                objConn.Close();
        }

        return filename;
    }
    private bool IsNumber(object value)
    {
        return
            value is short
            || value is ushort
            || value is int
            || value is uint
            || value is long
            || value is ulong
            || value is float
            || value is double
            || value is decimal;
    }
    public bool ExecExcel(DataTable data)
    {
        if (data.Rows.Count == 0)
            return false;
        string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int rnd = new Random().Next();
        path += rnd + EXCEL_NAME + ".xls";
        string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path +
            ";Mode=ReadWrite;Extended Properties=\"Excel 8.0;HDR=Yes;\";";
        //     string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path +
        //         ";Mode=ReadWrite;Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";
        OleDbConnection objConn = new OleDbConnection(connString);
        List<KeyValuePair<string, string>> list = GetDataTable.GetColumnsNamesCaption(data);
        if (list.Count == 0)
            return false;
        string CurrencySymbole = "";
        string command = "CREATE TABLE [Sheet1] (";
        foreach (KeyValuePair<string, string> kvp in list)
        {
            string _type;
            if (kvp.Value.StartsWith("IsCurrency"))
            {
                CurrencySymbole = kvp.Value.Split(';')[1];
                _type = "CURRENCY";
            }
            else
                _type = (kvp.Value == "IsNumber") ? "NUMBER" : "NTEXT";
            //  command += "[" + kvp.Key + "] NTEXT,";
            command += "[" + kvp.Key + "] " + _type + ",";
            //        command += "[" + s + "] user-defined-type,";
            //           command += "[" + s + "] nvarchar(255),";
        }
        command = command.Substring(0, command.Length - 1) + ")";
        string SiteId = ((SiteSetting)(HttpContext.Current.Session["Site"])).GetSiteID;
        try
        {
            objConn.Open();
            OleDbCommand cmd = new OleDbCommand(command, objConn);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            string commandInsert = GetInsertString(list);
            foreach (DataRow row in data.Rows)
            {

                //       string commandInsert = "Insert into [Sheet1$] ([Code], [Segment], [Auction], [Min price], [Show certificate]) " +
                //           "VALUES (@code, @segment, @auction, @MinPrice, @certificate)";

                cmd = new OleDbCommand(commandInsert, objConn);
                foreach (KeyValuePair<string, string> kvp in list)
                {
                    /*
                    string newS = s.Replace("'", SiteId);
                    cmd.Parameters.AddWithValue("@" + newS.Replace(" ", "_"), row[s].ToString());
                     * */
                    string _value;
                    if (kvp.Value.StartsWith("IsCurrency"))
                        _value = row[kvp.Key].ToString().Replace(CurrencySymbole, "");
                    /*
                else if(kvp.Value == "IsPercentage")
                    _value = row[kvp.Key].ToString().Replace("%", "");
                     * */
                    else
                        _value = row[kvp.Key].ToString();

                    cmd.Parameters.AddWithValue("@" + GetValidString(kvp.Key), _value);
                }

                int b = cmd.ExecuteNonQuery();
                cmd.Dispose();
            }

        }
        catch (Exception ex)
        {

            dbug_log.ExceptionLog(ex);
            return false;
        }
        finally
        {
            if (objConn.State != ConnectionState.Closed)
                objConn.Close();
        }

        _page.Response.ContentType = "application/vnd.ms-excel";// "application/ms-excel";// "Application/x-msexcel";       
        _page.Response.AppendHeader("Content-disposition", "attachment; filename=" + EXCEL_NAME + ".xls");

        //    _page.Response.WriteFile(path);
        _page.Response.BinaryWrite(File.ReadAllBytes(path));
        _page.Response.End();
        return true;
    }
    string GetInsertString(List<string> list)
    {
        string commandInsert = "Insert into [Sheet1$] (";
        foreach (string s in list)
        {
            commandInsert += "[" + s + "],";
        }
        commandInsert = commandInsert.Substring(0, commandInsert.Length - 1) + ") VALUES (";
        //     for (int i = 0; i < list.Count; i++)
        //         list[i] = list[i].Replace(" ", "_");
        foreach (string s in list)
        {
            /*
            string newS = s.Replace("'", "");
            commandInsert += "@" + newS.Replace(" ", "_") + ",";
             * */
            commandInsert += "@" + GetValidString(s) + ",";
        }
        commandInsert = commandInsert.Substring(0, commandInsert.Length - 1) + ")";
        return commandInsert;
    }
    string GetInsertString(List<KeyValuePair<string, string>> list)
    {
        string commandInsert = "Insert into [Sheet1$] (";
        foreach (KeyValuePair<string, string> kvp in list)
        {
            commandInsert += "[" + kvp.Key + "],";
        }
        commandInsert = commandInsert.Substring(0, commandInsert.Length - 1) + ") VALUES (";
        //     for (int i = 0; i < list.Count; i++)
        //         list[i] = list[i].Replace(" ", "_");
        foreach (KeyValuePair<string, string> kvp in list)
        {
            /*
            string newS = s.Replace("'", "");
            commandInsert += "@" + newS.Replace(" ", "_") + ",";
             * */
            commandInsert += "@" + GetValidString(kvp.Key) + ",";
        }
        commandInsert = commandInsert.Substring(0, commandInsert.Length - 1) + ")";
        return commandInsert;
    }

    string GetValidString(string str)
    {
        str = str.Replace("'", "");
        str = str.Replace(" ", "_");
        str = str.Replace("-", "");
        return str;
    }

    public bool ExecExcel(DataView data)
    {
        return ExecExcel(data.ToTable());
    }
    public bool ExecExcel(DataView data, GridView _grid)
    {





        /*
        string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int rnd = new Random().Next();
        path += rnd + EXCEL_NAME + ".xls";
         * */

        _grid.AllowPaging = false;
        _grid.DataSource = data;
        _grid.DataBind();

        //    Page pg = new Page();


        //     Response.ContentType = "application/vnd.xls";
        //      HtmlForm _form = new HtmlForm();
        //       _form.ID = "_form";
        //      _form.Attributes.Add("runat", "server");
        //      _form.Controls.Add(_grid);
        //   _page.Controls.Add(_form);

        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        _grid.RenderControl(hw);
        //style to format numbers to string

        //     _page.Response.AddHeader("content-disposition", "attachment;filename=" + EXCEL_NAME + ".xls");
        _page.Response.AppendHeader("content-disposition", "attachment;filename=" + EXCEL_NAME + ".xls");

        _page.Response.Charset = "";
        _page.Response.ContentType = "application/vnd.ms-excel";

        //   _page.Response.WriteFile(path);

        string style = @"<style>.textmode{mso-number-format:\@;}</style>";
        _page.Response.Write(style);
        _page.Response.Output.Write(sw.ToString());
        _page.Response.Flush();

        _page.Response.End();

        return true;

    }
    public bool ExecExcel(DataTable data, GridView _grid)
    {
        Dictionary<string, string> list = new Dictionary<string, string>();
        //   _grid.DataSource = data;
        //    _grid.DataBind();
        //       foreach( int t=_grid.;true;);
        for (int i = 0; i < _grid.HeaderRow.Cells.Count; i++)
        {
            string _bind = _grid.Columns[i].SortExpression;
            if (string.IsNullOrEmpty(_bind))
                continue;
            foreach (Control control in _grid.HeaderRow.Cells[i].Controls)
            {
                if (control.GetType() == typeof(Label))
                {
                    list.Add(_bind, ((Label)control).Text);
                    break;
                }
            }
        }
        DataTable dt = new DataTable();
        foreach (KeyValuePair<string, string> kvp in list)
        {
            dt.Columns.Add(kvp.Value, data.Columns[kvp.Key].DataType);
        }
        foreach (DataRow row in data.Rows)
        {
            DataRow newRow = dt.NewRow();
            foreach (KeyValuePair<string, string> kvp in list)
            {
                newRow[kvp.Value] = row[kvp.Key];
            }
            dt.Rows.Add(newRow);
        }
        return ExecExcel(dt);
    }
    /*
    public bool ExecExcel<T>(Dictionary<string, T> dic)
    {

        return true;
    }
     * */
    public bool ExecExcel(Repeater _repeater)
    {
        _repeater.EnableViewState = false;
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        _repeater.RenderControl(hw);
        //style to format numbers to string

        //     _page.Response.AddHeader("content-disposition", "attachment;filename=" + EXCEL_NAME + ".xls");
        _page.Response.AppendHeader("content-disposition", "attachment;filename=" + EXCEL_NAME + ".xls");

        //    _page.Response.Charset = "windows-1255";// "UTF-8"; //"";// System.Text.Encoding.UTF8.WebName;// "";
        _page.Response.ContentEncoding = System.Text.Encoding.Unicode;// System.Text.Encoding.GetEncoding("windows-1255");
        _page.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
        _page.Response.ContentType = "application/vnd.ms-excel";

        //   _page.Response.WriteFile(path);

        string style = @"<style>.textmode{mso-number-format:\@;}</style>";
        _page.Response.Write(style);
        _page.Response.Output.Write(sw.ToString());
        _page.Response.Flush();

        _page.Response.End();

        return true;
    }

    public XLWorkbook CreateExcelXLWorkBook(IEnumerable<IEnumerable<object>> data)
    {
        var workbook = new XLWorkbook();
        var worksheet = workbook.Worksheets.Add("Sheet1");
        for (int i = 0; i < data.Count(); i++)
        {
            IEnumerable<object> rowi = data.ElementAt(i);
            for (int j = 0; j < rowi.Count(); j++)
            {
                worksheet.Cell(i + 1, j + 1).Value = rowi.ElementAt(j);
            }
        }
        return workbook;
    }

    public void DownloadExcelXLWorkBook(XLWorkbook workbook)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            workbook.SaveAs(ms);
            byte[] bytesInStream = ms.GetBuffer();
            ms.Close();
            _page.Response.Clear();
            _page.Response.ContentType = "application/force-download";
            _page.Response.AddHeader("content-disposition", "attachment; filename=" + EXCEL_NAME + ".xlsx");
            _page.Response.BinaryWrite(bytesInStream);
            _page.Response.End();
        }
    }
}
