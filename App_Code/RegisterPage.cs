﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI;

/// <summary>
/// Summary description for RegisterPage
/// </summary>
public enum eSupplierRegisterType { none, AAR, New }
public class RegisterPage : System.Web.UI.Page
{
    protected const string NUMBER_FORMAT = "{0:#,0.##}";
    public XDocument xdoc { get; private set; }
    public SiteSetting siteSetting { get; set; }
    public double balance
    {
        get { return (Session["_balance"] == null) ? 0 : (double)Session["_balance"]; }
        
    }
    public bool HasReferrer
    {
        get { return (Session["HasReferrer"] == null) ? false : (bool)Session["HasReferrer"]; }
    }
    public string PhoneNum
    {
        get { return (Session["PhoneNum"] == null) ? string.Empty : (string)Session["PhoneNum"]; }
    }
    public string Password
    {
        get { return (Session["Password"] == null) ? string.Empty : (string)Session["Password"]; }
    }
    public string PhoneNumDisplay
    {
        get
        {
            return Utilities.americanFormatPhone(PhoneNum);
        }
    }
    public string email
    {
        get { return (Session["email"] == null) ? string.Empty : (string)Session["email"]; }
    }
    public Guid SupplierId
    {
        get { return (Session["RegisterUser"] == null) ? Guid.Empty : (Guid)Session["RegisterUser"]; }
    }
    public eSupplierRegisterType SupplierType
    {
        get { return (Session["SupplierType"] == null) ? eSupplierRegisterType.none : (eSupplierRegisterType)Session["SupplierType"]; }
        set { Session["SupplierType"] = value; }
    }
   

	public RegisterPage()
	{
		//
		
		//
	}
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        siteSetting = SiteSetting.GetSiteSetting(this);
    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //Security- for the browser not save in the cash memory
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");
        ////
      //  base.OnInit(e);
        if (this is IsInProcess)
            IsInRegisterProcess();
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                SiteSetting ss = DBConnection.LoadSiteId(host);
                if (ss == null)
                {
                   // Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
                    Response.Redirect("http://" + PpcSite.GetCurrent().MainDomain + Request.Url.PathAndQuery);
                    return;
                }
                siteSetting = ss;
                siteSetting.SetSiteSetting(this);
            }

            
            if (siteSetting.GetSiteID != PpcSite.GetCurrent().SiteId)
                Response.Redirect(ResolveUrl("~") + "Management/ProfessionLogin.aspx");
            
        }
        
    }
    protected override void OnPreLoad(EventArgs e)
    {
        base.OnPreLoad(e);
        if (!IsPostBack)
        {
            xdoc = Utilities.GetXmlSiteText();
            string _error;
            MissingInfo = Utilities.GetGeneralServerMissing(xdoc, out _error);
       //     MissingInfo = HttpUtility.JavaScriptStringEncode(MissingInfo);
            ServerError = _error;
        }
    }
    protected override void  OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(siteSetting.GetIcon))
            {
                HtmlLink _link = new HtmlLink();
                _link.Href = (siteSetting.GetIcon.Contains("http")) ? siteSetting.GetIcon : ResolveUrl(siteSetting.GetIcon);
                _link.Attributes.Add("rel", "Shortcut Icon");
                Page.Header.Controls.Add(_link);
            }
            Page.Title = siteSetting.GetPageTitle;
        }
        
    }
    public void GeneralServerError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "GeneralServerError", "GeneralServerError();", true);
    }
    public void IsInRegisterProcess()
    {
        
        if(SupplierId == Guid.Empty)
            Response.Redirect(ResolveUrl("~/PPC/Register.aspx"));
        
    }
    public bool _IsInRegisterProcess()
    {
        return (SupplierId != Guid.Empty);
    }
    
    public void SetSupplierType(eSupplierRegisterType esrt)
    {
        SupplierType = esrt;
    }
    /*
    public static void SetSupplierType(HttpContext context, eSupplierRegisterType esrt)
    {
        context.Session["SupplierType"] = esrt;
    }
    public static void SetSupplierId(HttpContext context, Guid _id)
    {
        context.Session["RegisterUser"] = _id;        
    }
    public static void SetPhoneNum(HttpContext context, string phone)
    {
        context.Session["PhoneNum"] = phone;
    }
     * */
    public static void SetSupplier(HttpContext context, Guid _id, eSupplierRegisterType esrt, string phone, string email, bool _has_referrer)
    {
        context.Session["SupplierType"] = esrt;
        context.Session["RegisterUser"] = _id;
        context.Session["PhoneNum"] = phone;
        context.Session["HasReferrer"] = _has_referrer;
        context.Session["email"] = email;
    }
    public static void SetDetailsAfterLogin(HttpContext context, double _balance, string password)
    {
        context.Session["_balance"] = _balance;
        context.Session["Password"] = password;
    }
    public static Guid GetSupplierId(HttpContext context)
    {
        return (context.Session["RegisterUser"] == null) ? Guid.Empty : (Guid)context.Session["RegisterUser"];
    }
    public void ClearDetails()
    {
        Session["SupplierType"] = null;
        Session["RegisterUser"] = null;
        Session["PhoneNum"] = null;
        Session["HasReferrer"] = null;
        Session["email"] = null;
        Session["_balance"] = null;
        Session["Password"] = null;
    }
    public void ClearDetails(bool ExceptPhone)
    {
        if (!ExceptPhone)
        {
            ClearDetails();
            return;
        }
        Session["SupplierType"] = null;
        Session["RegisterUser"] = null;        
        Session["HasReferrer"] = null;
        Session["PhoneNum"] = null;
        Session["_balance"] = null;
        Session["Password"] = null;
    }
    /*
    public void CheckRegisterProcess()
    {
        if (!IsInRegisterProcess() && string.IsNullOrEmpty(PhoneNum))
            Response.Redirect(ResolveUrl("~/PPC/RequestInvitation.aspx"));
    }
     * */
    public string ServerError
    {
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }
    public string MissingInfo
    {
        get { return (string)ViewState["MissingInfo"]; }
        set { ViewState["MissingInfo"] = value; }
    }
   
}