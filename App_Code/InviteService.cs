﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for InviteService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class InviteService : System.Web.Services.WebService {

    public InviteService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    //[ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public string create(Guid supplierId, string url) {

        JavaScriptSerializer jss = new JavaScriptSerializer();
        if(supplierId==Guid.Empty)
            return jss.Serialize(new { invitationId = "" });
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfGuid result = null;
        try
        {
            result = _supplier.CreateVideoChatInvitation(supplierId, url);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return jss.Serialize(new { invitationId = "" });
        }
        if(result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            return jss.Serialize(new { invitationId = "" });
        }
        if(result.Value==Guid.Empty)
            return jss.Serialize(new { invitationId = "" });
        return jss.Serialize(new { invitationId = result.Value.ToString() });
    }
    [WebMethod]
    public string send(Guid invitationId, string phone)
    {
        JavaScriptSerializer jss = new JavaScriptSerializer();
        if (invitationId == Guid.Empty)
            return jss.Serialize(new { success = false });
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfBoolean result = null;
        try
        {
            result = _supplier.SendVideoChatInvitation(invitationId, phone);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return jss.Serialize(new { success = false });
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            return jss.Serialize(new { success = false });
        }
        if (!result.Value)
            return jss.Serialize(new { success = false });
        return jss.Serialize(new { success = true });
    }
    
}
