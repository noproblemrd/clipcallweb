﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Xml;

/// <summary>
/// Summary description for ReportWizardService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class ReportWizardService : System.Web.Services.WebService {
    const int MAX_SUGGESTS = 10;
    private const string BASE_PATH = "~/Publisher/SalesControls/";
    public ReportWizardService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    
    [WebMethod(true)]
    public string GetOperators_InputValue(int _filter)
    {
        string _reference = "";
        string filterName = "";
        string type_name = "";
        StringBuilder sb = new StringBuilder();
        string command = "EXEC dbo.GetWizardFilterDetailsByWizardFilterId @WizardFilterId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@WizardFilterId", _filter);
            SqlDataReader reader = cmd.ExecuteReader();
            
            if (reader.Read())
            {
                _reference = reader.IsDBNull(0) ? string.Empty : (string)reader["Reference"];
                filterName = (string)reader["Name"];
                type_name = (string)reader["TypeName"];
            }
            reader.Dispose();
            cmd.Dispose();
            
            command = "EXEC dbo.GetWizardOperator_ByWizardFilterId @WizardFilterId";
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@WizardFilterId", _filter);
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string _name = EditTranslateDB.GetControlTranslate(((SiteSetting)Session["Site"]).siteLangId, "eOperators", (string)reader["Name"]);
                sb.Append(_name + "," + (int)reader["Id"] + ";");
            }
            conn.Close();
        }
        if (sb.Length == 0)
            return string.Empty;
        HtmlTableCell cell = GetInputCell(type_name, _reference, filterName, "");
        HtmlTableRow row = new HtmlTableRow();
        HtmlTable table = new HtmlTable();
        row.Controls.Add(cell);
        table.Controls.Add(row);

        StringBuilder _sb = new StringBuilder();
        StringWriter tw = new StringWriter(_sb);
        HtmlTextWriter hw = new HtmlTextWriter(tw);

        table.RenderControl(hw);
        return sb.ToString().Substring(0, sb.Length - 1) + "$$" + _sb.ToString(); 
       
    }
    [WebMethod(true)]
    public string GetColumns(int _issue)
    {
        Dictionary<int, string> dic = new Dictionary<int, string>();
        StringBuilder sb = new StringBuilder();
        string command = "EXEC dbo.GetColumnsByWizardIssueId @WizardFilterId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@WizardFilterId", _issue);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string PageName = (string)reader["PageName"];
                string _name = EditTranslateDB.GetControlTranslate(((SiteSetting)Session["Site"]).siteLangId, PageName, (string)reader["Name"]);
                dic.Add((int)reader["Id"], _name);
                //        sb.Append(_name + "," + (int)reader["Id"] + ";");
            }
            conn.Close();
        }
        IEnumerable<KeyValuePair<int, string>> _sort = dic.Select(x => x).OrderBy(x => x.Value);
        foreach (KeyValuePair<int, string> kvp in _sort)
        {
            sb.Append(kvp.Value + "," + kvp.Key + ";");
        }
                                                        
         if (sb.Length == 0)
            return string.Empty;
         return sb.ToString().Substring(0, sb.Length - 1);
    }
    HtmlTableCell GetInputCell(string _Type, string _reference, string _autocomplete, string _value)
    {
        HtmlTableCell cell3 = new HtmlTableCell();
        cell3.Attributes["class"] = "td_sel_value";
        HtmlControl _input = new HtmlInputText("text");
        switch (_Type)
        {
            case ("Autocomplete"):
                _input = new HtmlInputText("text");
                ((HtmlInputText)_input).Value = _value;              
                _input.Attributes["class"] = _autocomplete + " operator_value form-textw";
                _input.Attributes.Add("onblur", "OnBlurAutoComplete(this);");
                break;
            case ("Number"):
                _input = new HtmlInputText("text");
                ((HtmlInputText)_input).Value = _value;
                _input.Attributes["class"] = "_value operator_value form-textw";
                _input.Attributes.Add("onblur", "OnBlurNumber(this);");
                break;
            case ("Date"):
                _input = new HtmlInputText("text");
                ((HtmlInputText)_input).Value = _value;
                _input.Attributes["class"] = "_datepicker operator_value form-textw";
                _input.Attributes.Add("readonly", "readonly");
                break;
            case("Bool"):
            case ("Picklist"):
                _input = new HtmlSelect();
                HtmlSelect _select = (HtmlSelect)_input;
 //               _select.Attributes.Add("name", "operator_value");
                Type enum_type = null;
                if(_Type=="Bool")
                    enum_type = class_enum.GetEnumType("eYesNo");
                else
                    enum_type = class_enum.GetEnumType(_reference);
                if (enum_type != null && enum_type.IsEnum)
                    _select.Items.AddRange(GetPickListFromEnum(enum_type, _value));
                else
                    _select.Items.AddRange(GetPickListFromCrm(_reference, _value));
                _select.Attributes["class"] = "_value operator_value form-selectw";
                break;

        }
        cell3.Controls.Add(_input);
        return cell3;
    }
    ListItem[] GetPickListFromEnum(Type enum_type, string _value)
    {
        List<ListItem> list = new List<ListItem>();
        foreach (object o in Enum.GetValues(enum_type))
        {
 //           ListItem li = new ListItem(o.ToString(), (int)o + "");

  //          li.Selected = (li.Text == _value);
            ListItem li = new ListItem(EditTranslateDB.GetControlTranslate(((SiteSetting)Session["Site"]).siteLangId, enum_type.Name, o.ToString())
                , o.ToString());
            li.Selected = (li.Value == _value);
     //       _select.Items.Add(li);
            list.Add(li);
        }
        return list.ToArray();
    }
    ListItem[] GetPickListFromCrm(string _reference, string _value)
    {
        List<ListItem> list = new List<ListItem>();
        if (_reference == "UnavailabilityReason")
        {
            string Web_reference = ((SiteSetting)Session["Site"]).GetUrlWebReference;
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(Web_reference);
            string result = string.Empty;
            try
            {

                result = supplier.GetUnavailabilityReasons();

            }
            catch (Exception ex)
            {
                dbug_log.ExceptionLog(ex, ((SiteSetting)Session["Site"]));
                return list.ToArray();
            }
            XmlDataDocument xdd = new XmlDataDocument();
            xdd.LoadXml(result);
            if (xdd["UnavailabilityResons"] == null || xdd["UnavailabilityResons"]["Error"] != null)            
                return list.ToArray();
            
            XmlNodeList xnl = xdd["UnavailabilityResons"].ChildNodes;
           
            foreach (XmlNode node in xnl)
            {
                string reason = node.InnerText;
                string reasonId = node.Attributes["ID"].InnerText;
                ListItem _li = new ListItem(reason, reasonId);
                _li.Selected = (_li.Value == _value);
                list.Add(_li);
            }        
        }
        return list.ToArray();
    }
    public HtmlTableRow GetNewHTMLRow(int _issueId, FilterSelect fs)
    {
        int siteLangId = ((SiteSetting)Session["Site"]).siteLangId;
        Dictionary<int, string> dicFilters = new Dictionary<int, string>();
        Dictionary<int, string> dicOperators = new Dictionary<int, string>();
        int firstFilterId = -1;
        string FirstType = "";
        int FirstTypeId = -1;
        string FirstReference = "";
        string FirstFilterName = "";
        string FirstFilterAutocomplete = "";
        string command = "EXEC dbo.GetWizardFiltersFilterTypeByWizardIssueId @WizardIssueId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@WizardIssueId", _issueId);
            SqlDataReader reader = cmd.ExecuteReader();
            
            while (reader.Read())
            {
                int _id = (int)reader["FilterId"];
                string filterName = EditTranslateDB.GetControlTranslate(siteLangId, "eWizardFilters", (string)reader["FilterName"]);
                if ((fs == null && firstFilterId == -1) || (fs != null && fs.WizardFilterId == _id))
                {
                    firstFilterId = _id;
                    FirstType = (string)reader["TypeName"];
                    FirstTypeId = (int)reader["TypeId"];
                    FirstReference = reader.IsDBNull(2) ? "" : (string)reader["Reference"];
                    FirstFilterName = filterName;
                    FirstFilterAutocomplete = (string)reader["FilterName"];
                }

                dicFilters.Add(_id, filterName);

            }
            reader.Dispose();
            cmd.Dispose();
            
            command = "EXEC dbo.GetWizardOperator_ByWizardFilterId @WizardFilterId";


            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@WizardFilterId", firstFilterId);
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string _name = EditTranslateDB.GetControlTranslate(siteLangId, "eOperators", (string)reader["Name"]);
                int _id = (int)reader["Id"];
                dicOperators.Add(_id, _name);
            }

            conn.Close();
        }
        HtmlTableRow row = new HtmlTableRow();

        HtmlTableCell cell1 = new HtmlTableCell();
        HtmlSelect filter = new HtmlSelect();
        filter.Attributes["class"] = "filter form-selectw";
        filter.Attributes.Add("onchange", "javascript:SetOperators(this);");
        foreach (KeyValuePair<int, string> kvp in dicFilters)
        {
            ListItem li = new ListItem(kvp.Value, kvp.Key + "");
   //         li.Selected = (li.Value == fs.WizardFilterId.ToString());
            filter.Items.Add(li);

        }
        if (fs == null)
            filter.SelectedIndex = 0;
        else
        {
            foreach (ListItem li in filter.Items)            
                li.Selected = (li.Value == (fs.WizardFilterId + ""));
            
        }
        cell1.Controls.Add(filter);
        row.Controls.Add(cell1);

        HtmlTableCell cell2 = new HtmlTableCell();
        HtmlSelect _operator = new HtmlSelect();
        _operator.Attributes["class"] = "operator form-selectw";
        foreach (KeyValuePair<int, string> kvp in dicOperators)
        {
            ListItem li = new ListItem(kvp.Value, kvp.Key + "");
    //        li.Selected = (li.Value == fs.WizardOperatorId.ToString());
            _operator.Items.Add(li);
        }
        if (fs == null)
            _operator.SelectedIndex = 0;
        else
        {
            foreach (ListItem li in _operator.Items)
                li.Selected = (li.Value == (fs.WizardOperatorId + ""));
        }
        cell2.Controls.Add(_operator);
        row.Controls.Add(cell2);

        HtmlTableCell cell3 = GetInputCell(FirstType, FirstReference, FirstFilterAutocomplete, ((fs == null) ? string.Empty : fs._value));
        
        row.Controls.Add(cell3);

        HtmlTableCell cell4 = new HtmlTableCell();
        cell4.Attributes["class"] = "esteria";
        cell4.Attributes.Add("style", "display:none;");
        HtmlGenericControl _span = new HtmlGenericControl("span");
        _span.Attributes.Add("class", "span_star");
        _span.InnerHtml = "*";
        cell4.Controls.Add(_span);
        row.Controls.Add(cell4);

        HtmlTableCell cell5 = new HtmlTableCell();
        cell5.Attributes["class"] = "add_remove";
        HtmlAnchor _add = new HtmlAnchor();
        _add.Attributes["class"] = "add";
        _add.Attributes.Add("href", "javascript:void(0);");
        _add.Attributes.Add("onclick", "addRow();");
        cell5.Controls.Add(_add);
        HtmlAnchor _remove = new HtmlAnchor();
        _remove.Attributes["class"] = "remove";
        _remove.Attributes.Add("href", "javascript:void(0);");
        _remove.Attributes.Add("onclick", "DeleteRow(this);");
        cell5.Controls.Add(_remove);
        row.Controls.Add(cell5);
        return row;
        
        
    }
    [WebMethod(true)]
    public string GetRow(int _issueId)
    {
    //    if (!string.IsNullOrEmpty(DefaultRow))
    //        return DefaultRow;
        HtmlTableRow row = GetNewHTMLRow(_issueId, null);
        HtmlTable table = new HtmlTable();
        table.Controls.Add(row);
        StringBuilder sb = new StringBuilder();
        StringWriter tw = new StringWriter(sb);
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        
        table.RenderControl(hw);
   //     DefaultRow = sb.ToString();
        return sb.ToString();
    }
    [WebMethod(true)]
    public List<string> SuggestList(string str, string classList)
    {
        List<string> list = new List<string>();
        
        switch (classList)
        {
            case ("Region"):
                list = RegionV;                
                break;
            case ("Heading"):
                list = HeadingV;
                break;
            case ("Advertiser"):
                list = AdvertiserV;
                break;
            case ("AccountManager"):
                list = AccountManagerV;
                break;
            default: return SuggestNewList(str, classList);
            //'PageName', 'ControlName', 'PlaceInWebSite', 'Domain', 'URL'

        }
        IEnumerable<string> query = (from x in list
                                     where x != null &&  x.ToLower().StartsWith(str.ToLower())
                                        select x).Take(MAX_SUGGESTS);
        return query.ToList();
                                    
    }
    [WebMethod(true)]
    public List<string> SuggestNewList(string str, string classList)
    {
        List<string> list = new List<string>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(((SiteSetting)Session["Site"]).GetUrlWebReference);
        //WebReferenceSite.eAutoCompleteFromTable.Incident
        WebReferenceSite.eAtuoCompleteField _auto;
        if (!Enum.TryParse(classList, out _auto))
            return list;
        WebReferenceSite.ResultOfListOfString result = null;
        try
        {
            result = _site.AutoCompleter(str, WebReferenceSite.eAutoCompleteFromTable.Incident, _auto);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, (SiteSetting)Session["Site"]);
            return list;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return list;
        IEnumerable<string> query = (from x in result.Value
                                     select x).Take(MAX_SUGGESTS);
        return query.ToList();
        /*
        WebReferenceSite.eAtuoCompleteField.ControlName;
        WebReferenceSite.eAtuoCompleteField.Domain;
        WebReferenceSite.eAtuoCompleteField.PageName;
        WebReferenceSite.eAtuoCompleteField.PlaceInWebSite;         
        WebReferenceSite.eAtuoCompleteField.Url;
         * */
    }
    [WebMethod(true)]
    public string GetOneSuggest(string str, string classList)
    {
        List<string> list = new List<string>();

        switch (classList)
        {
            case ("Region"):
                list = RegionV;
                break;
            case ("Heading"):
                list = HeadingV;
                break;
            case ("Advertiser"):
                list = AdvertiserV;
                break;
            case ("AccountManager"):
                list = AccountManagerV;
                break;
            default: return GetNewOneSuggest(str, classList);
        }
        string query = (from x in list
                        where x != null && x.ToLower().StartsWith(str.ToLower())
                        select x).FirstOrDefault();
        return query;
    }
      [WebMethod(true)]
    public string GetNewOneSuggest(string str, string classList)
    {
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(((SiteSetting)Session["Site"]).GetUrlWebReference);
        //WebReferenceSite.eAutoCompleteFromTable.Incident
        WebReferenceSite.eAtuoCompleteField _auto;
        if (!Enum.TryParse(classList, out _auto))
            return string.Empty;
        WebReferenceSite.ResultOfListOfString result = null;
        try
        {
            result = _site.AutoCompleter(str, WebReferenceSite.eAutoCompleteFromTable.Incident, _auto);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, (SiteSetting)Session["Site"]);
            return string.Empty;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;
        string query = (from x in result.Value
                        select x).FirstOrDefault();
        return query;
    }
    /*
      [WebMethod(true)]
      public string GetPplDrillDown(string details)
      {
          PplReportRecord prr = PplReportRecord.GetDetails(details);
          SiteSetting ss = ((SiteSetting)Session["Site"]);
          WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(ss.GetUrlWebReference);
          WebReferenceReports.PplReportDrillDownRequest _request = new WebReferenceReports.PplReportDrillDownRequest();
          _request.ExpertiseId = prr.ExpertiseId;
          _request.RegionId = prr.RegionId;
          _request.From = prr.From;
          _request.To = prr.To;
          WebReferenceReports.ResultOfListOfPplReportDrillDownData result = null;
          try
          {
              result = _report.PplReportDrillDown(_request);
          }
          catch (Exception exc)
          {
              dbug_log.ExceptionLog(exc, ss);
              return string.Empty;
          }
          if (result.Type == WebReferenceReports.eResultType.Failure)
              return string.Empty;
          PageRenderin page = new PageRenderin();
          UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "PplReportDrillDown.ascx");
          MethodInfo LoadData = ctl.GetType().GetMethod("LoadData");
          LoadData.Invoke(ctl, new object[] { result.Value });
          ctl.EnableViewState = false;
          StringBuilder sb = new StringBuilder();
          using (StringWriter sw = new StringWriter(sb))
          {
              using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
              {
                  ctl.RenderControl(textWriter);
              }
          }
          return sb.ToString();
      }
     * */
    List<string> SetRegions()
    {
        string _WebService = ((SiteSetting)Session["Site"]).GetUrlWebReference;
        List<string> list = new List<string>();
        WebReferenceSupplier.Supplier _sp = WebServiceConfig.GetSupplierReference(_WebService);         
        string result_1 = string.Empty;
        string result_2 = string.Empty;
        try
        {
            result_1 = _sp.GetRegions(1);
            result_2 = _sp.GetRegions(2);
        }
        catch (SoapException ex)
        {
            return new List<string>();
        }
        XmlDataDocument xdd_1 = new XmlDataDocument();
        xdd_1.LoadXml(result_1);

        if (!(xdd_1["Areas"] == null || xdd_1["Areas"]["Error"] != null))
        {
            foreach (XmlNode node in xdd_1["Areas"].ChildNodes)
            {
                string name = node.Attributes["Name"].InnerText;
                if (string.IsNullOrEmpty(name))
                    continue;
                list.Add(name);
            }
        }
        XmlDataDocument xdd_2 = new XmlDataDocument();
        xdd_2.LoadXml(result_2);
        if (!(xdd_2["Areas"] == null || xdd_2["Areas"]["Error"] != null))
        {
            foreach (XmlNode node in xdd_2["Areas"].ChildNodes)
            {
                string name = node.Attributes["Name"].InnerText;
                if (string.IsNullOrEmpty(name))
                    continue;
                list.Add(name);
            }
        }
        list.Sort();
        return list;

    }
    List<string> SetHeading()
    {
        string _WebService = ((SiteSetting)Session["Site"]).GetUrlWebReference;
        List<string> list = new List<string>();
        WebReferenceSite.Site _sp = WebServiceConfig.GetSiteReference(_WebService);
        string result = string.Empty;
        result =  _sp.GetExpertise("", "");
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["Expertise"] == null || xdd["Expertise"]["Error"] != null)
        {
            return list;
        }
        foreach (XmlNode nodePrimary in xdd["Expertise"].ChildNodes)
        {
            string pName = nodePrimary.Attributes["Name"].InnerText;
            list.Add(pName);
        }
        list.Sort();
        return list;

    }
    
    List<string> SetAdvertisers()
    {
        string _WebService = ((SiteSetting)Session["Site"]).GetUrlWebReference;
        List<string> list = new List<string>();
        WebReferenceSite.Site _sp = WebServiceConfig.GetSiteReference(_WebService);
        WebReferenceSite.ResultOfListOfString result = new WebReferenceSite.ResultOfListOfString();
        result = _sp.GetAllSuppliersNames();
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return list;
        list = new List<string>(result.Value);
        return list;
    }
    List<string> SetAccountManager()
    {
        string _WebService = ((SiteSetting)Session["Site"]).GetUrlWebReference;
        List<string> list = new List<string>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_WebService);
        WebReferenceSite.ResultOfGetAllPublishersResponse result = new WebReferenceSite.ResultOfGetAllPublishersResponse();
        try
        {
            result = _site.GetAllPublishers();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ((SiteSetting)Session["Site"]));
            return list;
        }
       
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return list;
        
        foreach (WebReferenceSite.PublisherData pd in result.Value.Publishers)
        {
            list.Add(pd.Name);
        }
        list.Sort();
        return list;

    }
     
    List<string> HeadingV
    {
        get
        {
            if (Session["Heading"] == null)
                Session["Heading"] = SetHeading();
            return (List<string>)Session["Heading"];
        }
    }
    List<string> RegionV
    {
        get
        {
            if (Session["Region"] == null)
                Session["Region"] = SetRegions();
            return (List<string>)Session["Region"];
        }
     //   set { Session["Region"] = value; }
    }
    List<string> AdvertiserV
    {
        get
        {
            if (Session["Advertiser"] == null)
                Session["Advertiser"] = SetAdvertisers();
            return (List<string>)Session["Advertiser"];
        }
    }
    List<string> AccountManagerV
    {
        get
        {
            if (Session["AccountManager"] == null)
                Session["AccountManager"] = SetAccountManager();
            return (List<string>)Session["AccountManager"];
        }
    }
    
}

