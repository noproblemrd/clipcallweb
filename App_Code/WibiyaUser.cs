﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for WibiyaUser
/// </summary>
public class WibiyaUser
{
    public string Domain { get; set; }
    public string SiteId { get; set; }
    public Guid OriginId { get; set; }
    public string ClientDomain { get; set; }
    public string UrlReferrer { get; set; }

	public WibiyaUser()
	{
		//
		
		//
	}
    public static WibiyaUser GetDataForSafari(HttpRequest request)
    {
        WibiyaUser wu = new WibiyaUser();
        if(request.Cookies["domain"] != null)
            wu.Domain = request.Cookies["domain"].Value;
        if (request.Cookies["site"] != null)
            wu.SiteId = request.Cookies["site"].Value;
        string _OriginId;
        if (request.Cookies["origin"] == null)
            _OriginId = string.Empty;
        else
            _OriginId = request.Cookies["origin"].Value;
        if (string.IsNullOrEmpty(wu.Domain) || string.IsNullOrEmpty(wu.SiteId) || !Utilities.IsGUID(_OriginId))
            return null;
        wu.OriginId = new Guid(_OriginId);
        return wu;
    }
    public void SetDataForSafari(HttpResponse response)
    {
        response.Cookies["domain"].Value = this.Domain;
        
        response.Cookies["site"].Value = this.SiteId;
        
        response.Cookies["origin"].Value = this.OriginId.ToString();
    }
    public string GetServerDomainWithSchema(HttpContext context)
    {
        string _domain = this.Domain;
        if (_domain == "localhost")
        {
            _domain = context.Request.Url.Scheme + @"://" + context.Request.Url.Authority + context.Request.ApplicationPath + @"/";
        }
        else
            _domain = @"http://" + _domain + @"/";
        return _domain;
    }
    public string GetServerDomainWithSchemaForMessage(HttpContext context)
    {
        string _domain = this.Domain;
        if (_domain == "localhost")
        {
            _domain = context.Request.Url.Scheme + @"://" + context.Request.Url.Authority;
        }
        else
            _domain = @"http://" + _domain;
        return _domain;
    }
    public string GetCrossDomainScript(string message)
    {
        string _path = System.AppDomain.CurrentDomain.BaseDirectory + @"Wibiya\slider2\crossdomain.js";
        string line;
        StringBuilder sb = new StringBuilder();

        if (File.Exists(_path))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(_path);
                while ((line = file.ReadLine()) != null)
                {
                    sb.AppendLine(line);
                }
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        sb = sb.Replace("{message}", message);
        string _domain = this.ClientDomain;
        if (_domain == "http://localhost")
            _domain += ":" + HttpContext.Current.Request.Url.Port;
        sb = sb.Replace("{client_domain}", _domain);
   //     string _fake = this.GetServerDomainWithSchema(HttpContext.Current) + "Wibiya/slider2/stam.htm";
    //    sb = sb.Replace("{fake}", _fake);
        return sb.ToString();
    }
    
}