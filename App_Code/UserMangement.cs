using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;

/// <summary>
/// Summary description for UserMangement
/// </summary>
public enum SecurityLevel { ANONYMUS, SUPPLIER, PUBLISHER1, PUBLISHER2, PUBLISHER3, PUBLISHER4, PUBLISHER5, AFFILIATE, PRE_SUPPLIER, WIBIYA_AFFILIATE }
//public enum WebLang { CHINESE, HEBREW, NULL }
[Serializable()]
public class UserMangement
{ 
    string userGuid;
   
    SecurityLevel securityLevel;
    string UserName;
    string UserCode;
    double Balance;
    bool IsCompleteRegistration;
    Guid AffiliateId;
    string _email;
    public bool CanListenToRecords { get; set; }
    public int? TimeZone { get; set; }
    public UserMangement() {
        this.securityLevel = SecurityLevel.ANONYMUS;
    }
    public UserMangement(string userGuid, string securityLevel,
        string UserName)
	{
    //    HyperLink link;
    //    link.t
        this.userGuid = userGuid;
        this.UserName = UserName;
        this.Balance = 0;
       
        switch (securityLevel.ToUpper())
        {
            case ("1"): this.securityLevel = SecurityLevel.PUBLISHER1;
                break;
            case ("2"): this.securityLevel = SecurityLevel.PUBLISHER2;
                break;
            case ("3"): this.securityLevel = SecurityLevel.PUBLISHER3;
                break;
            case ("4"): this.securityLevel = SecurityLevel.PUBLISHER4;
                break;
            case ("5"): this.securityLevel = SecurityLevel.PUBLISHER5;
                break;
            case ("SUPPLIER"): this.securityLevel = SecurityLevel.SUPPLIER;
                break;
            default: this.securityLevel = SecurityLevel.ANONYMUS;
                break;
        }  
	}
    public UserMangement(string userGuid, string securityLevel,
        string UserName, string email, bool canListenRecord, int? timezone)
        : this(userGuid, securityLevel, UserName)
    {
        this._email = email;
        this.CanListenToRecords = canListenRecord;
        this.TimeZone = timezone;
    }
    public UserMangement(string userGuidAdvertisers, string UserName, double _balance, bool IsCompleteRegistration)
        :
            this(userGuidAdvertisers, UserName)
    {       
        this.Balance = _balance;
        this.IsCompleteRegistration = IsCompleteRegistration;
    }
    public UserMangement(string userGuidAdvertisers, string UserName)
    {
        this.userGuid = userGuidAdvertisers;
        this.UserName = UserName;
        this.securityLevel = SecurityLevel.SUPPLIER;
        this.Balance = 0;
    }
    public UserMangement(Guid _id, string UserName, string UserCode):this(_id.ToString(), UserName)
    {
        this.UserCode = UserCode;
    }
    public UserMangement(Guid _id, string UserName, string UserCode, string _email)
        : this(_id.ToString(), UserName)
    {
        this.UserCode = UserCode;
        this._email = _email;
    }
    public UserMangement(string userGuid, string UserName, Guid AffiliateId)
    {
        this.userGuid = userGuid;
        this.UserName = UserName;
        this.securityLevel = SecurityLevel.AFFILIATE;
        this.AffiliateId = AffiliateId;
    }
    public UserMangement(string userGuidAdvertisers, string UserName, double _balance, bool IsCompleteRegistration, string email)
        :
            this(userGuidAdvertisers, UserName)
    {
        this.Balance = _balance;
        this.IsCompleteRegistration = IsCompleteRegistration;
        this._email = email;
    }
    public UserMangement(string name, Guid AffiliateId)
    {
        this.AffiliateId = AffiliateId;
        this.UserName = name;
        this.securityLevel = SecurityLevel.WIBIYA_AFFILIATE;
    }
 /*   public static string GetGuid(Page page)
    {
        UserMangement um = GetUserObject(page);
        if (um == null)
            return string.Empty;
        return um.userGuid;
    }*/
    public UserMangement(Guid _id, SecurityLevel sl, string name)
    {
        this.userGuid = _id.ToString();
        this.securityLevel = sl;
        this.User_Name = name;
    }
    public void UpgradePreSupplier(string name, string email, double _balance)
    {
        if (this.securityLevel == SecurityLevel.PRE_SUPPLIER)
        {
            this.User_Name = name;
            this._email = email;
            this.Balance = _balance;
            this.securityLevel = SecurityLevel.SUPPLIER;
        }
    }
    public bool Is_CompleteRegistration
    {
        get { return this.IsCompleteRegistration; }
        set { this.IsCompleteRegistration = value; }
    }
    public string GetGuid
    {
        get { return this.userGuid; }
    }
    public Guid Get_Guid
    {
        get { return new Guid(this.userGuid); }
    }
    public static bool IsSupplier(Page page)
    {
        UserMangement um = GetUserObject(page);
        if (um == null)
            return false;
        if (um.securityLevel == SecurityLevel.SUPPLIER)
            return true;
        else
            return false;
    }
    public bool IsSupplier()
    {
        if (securityLevel == SecurityLevel.SUPPLIER)
            return true;
        else
            return false;
    }
    public bool IsAffiliate()
    {
        return (securityLevel == SecurityLevel.AFFILIATE);
    }
 
    public bool IsPublisher()
    {
        if (securityLevel == SecurityLevel.PUBLISHER1 || 
            securityLevel == SecurityLevel.PUBLISHER2 ||
            securityLevel == SecurityLevel.PUBLISHER3 ||
            securityLevel == SecurityLevel.PUBLISHER4 ||
            securityLevel == SecurityLevel.PUBLISHER5)
            return true;
        else
            return false;
    }
    public bool IsWibiyaAffiliate()
    {
        if (this.securityLevel == SecurityLevel.WIBIYA_AFFILIATE)
            return true;
        return false;
    }
    public bool IsAuthenticated()
    {
        return (this.securityLevel != SecurityLevel.ANONYMUS && this.securityLevel != SecurityLevel.PRE_SUPPLIER);
    }
    public bool IsPreSupplier()
    {
        return (this.securityLevel == SecurityLevel.PRE_SUPPLIER);
    }
    public string GetSecurityLevel
    {
        get { return this.securityLevel.ToString(); }
    }
    public SecurityLevel GetEnumSecurityLevel
    {
        get { return this.securityLevel; }
    }

    public static UserMangement GetUserObject(Page page)
    {
        return (page.Session["UserGuid"] == null) ? new UserMangement(): (UserMangement)page.Session["UserGuid"];
        //return ( new UserMangement() );
          
    }
    public static UserMangement GetUserObject(HttpContext context)
    {
        return (context.Session["UserGuid"] == null) ? new UserMangement() : (UserMangement)context.Session["UserGuid"];
        //return ( new UserMangement() );

    }
    public static UserMangement GetUserObject(System.Web.SessionState.HttpSessionState session)
    {
        return (session["UserGuid"] == null) ? new UserMangement() : (UserMangement)session["UserGuid"];
    }
    public DateTime GetDateTimeFromClient(string date, string format)
    {
        int tz = this.TimeZone.HasValue ? this.TimeZone.Value : 0;
        System.Globalization.CultureInfo enUS = new System.Globalization.CultureInfo("en-US");
        return DateTime.ParseExact(date, format, enUS).AddMinutes(tz * -1);
    }
    public void SetUserObject(Page page)
    {
        page.Session["UserGuid"] = this;
    }
    public void SetUserObject(HttpContext context)
    {
        context.Session["UserGuid"] = this;
    }
    public string User_Name
    {
        get { return this.UserName; }
        set { this.UserName = value; }
    }
    public double balance
    {
        get { return this.Balance; }
        set { this.Balance = value; }
    }
    public string User_Code
    {
        get { return (string.IsNullOrEmpty(this.UserCode)) ? "--" : this.UserCode; }
        set { this.UserCode = value; }
    }
    public Guid GetAffiliateId
    {
        get { return this.AffiliateId; }
    }
    public string Email
    {
        get { return _email; }
        set { _email = value; }
    }
    public static void SetTimeZone(HttpContext context, int timeZone)
    {
        UserMangement user = GetUserObject(context);
        if (!user.TimeZone.HasValue || user.TimeZone.Value != timeZone)
        {
            user.TimeZone = timeZone;
            user.SetUserObject(context);
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate (object state)
            {
                UserMangement _user = (UserMangement)state;
                _user.SetDbTimeZone();
            }), user);
        }
    }
    private void SetDbTimeZone()
    {
        if (!this.TimeZone.HasValue)
            return;
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.Result _response = null;
        try
        {
            _response = supplier.UpdateUserTimezone(this.Get_Guid, this.TimeZone.Value);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            return;
        }        
    }


}
