﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;

/// <summary>
/// Summary description for WebServiceReviews
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceReviews : System.Web.Services.WebService {

    public WebServiceReviews () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetReviews(string SiteId, string SupplierId)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(null, SiteId);
        WebReferenceReports.GetReviewsRequest _request = new WebReferenceReports.GetReviewsRequest();

        _request.SupplierId = new Guid(SupplierId);
        _request.ReviewStatus = WebReferenceReports.ReviewStatus.Approved;

        WebReferenceReports.ResultOfGetReviewsResponse result = null;
        try
        {
            result = _report.GetReviews(_request);
        }
        catch (Exception exc)
        {
            SiteSetting ss = new SiteSetting(SiteId);
            dbug_log.ExceptionLog(exc, ss);
            return "Faild";
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
            return "Faild";
       
        XElement xelem = new XElement("Reviews");
        foreach (WebReferenceReports.ReviewData rd in result.Value.Reviews)
        {
            XElement Cxelem = new XElement("Data");
            Cxelem.Add(new XAttribute("CreatedOn", rd.CreatedOn));
            Cxelem.Add(new XAttribute("Name", rd.Name));
            Cxelem.Add(new XAttribute("Description", rd.Description));
            Cxelem.Add(new XAttribute("IsLike", rd.IsLike));
            Cxelem.Add(new XAttribute("CostumerId", rd.CostumerId));
            Cxelem.Add(new XAttribute("IncidentId", rd.IncidentId));
            Cxelem.Add(new XAttribute("IncidentNumber", rd.IncidentNumber));
            Cxelem.Add(new XAttribute("Number", rd.Number));
            Cxelem.Add(new XAttribute("Phone", rd.Phone));
            Cxelem.Add(new XAttribute("ReviewId", rd.ReviewId));
            Cxelem.Add(new XAttribute("ReviewStatus", rd.ReviewStatus));
            Cxelem.Add(new XAttribute("SupplierName", rd.SupplierName));
            
            xelem.Add(Cxelem);
        }

        return xelem.ToString();
    }
    [WebMethod]
    public string SetReviews(string SiteId, string SupplierId, string Description, string IsLike,
                                string Name, string Phone)
    {
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.InsertReviewRequest _request = new WebReferenceCustomer.InsertReviewRequest();
        _request.Description = Description;
        _request.IsLike = (IsLike.ToLower() == "true" || IsLike == "1");
        _request.Name = Name;
        _request.Phone = Phone;
        _request.SupplierId = new Guid(SupplierId);
        WebReferenceCustomer.ResultOfInsertReviewResponse result = null;
        try
        {
            result = _customer.InsertReview(_request);
        }
        catch (Exception exc)
        {
            SiteSetting ss = new SiteSetting(SiteId);
            dbug_log.ExceptionLog(exc, ss);            
            return "Faild";
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
            return "Faild";
        
        return result.Value.InsertionStatus.ToString();
        /*
        WebReferenceCustomer.InsertReviewStatus.AlreadyExists;
        WebReferenceCustomer.InsertReviewStatus.MissingParams;
        WebReferenceCustomer.InsertReviewStatus.NoCaseFound;
        WebReferenceCustomer.InsertReviewStatus.OK;
         * */
    }
}

