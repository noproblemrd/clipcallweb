﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for SliderLogService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class SliderLogService : System.Web.Services.WebService {

    public SliderLogService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public string log(string QualityName, string _event, string desc, string step, string me) {
        MongoDB.Bson.ObjectId _id;
        if (!MongoDB.Bson.ObjectId.TryParse(me, out _id))
            return string.Empty;

        SliderData sd = SliderData.GetSliderData(_id);//, Context.Request.Url.Host);
        if (sd == null)
        {
            sd = new SliderData();
            sd.SiteId = PpcSite.GetCurrent().SiteId;
        }
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, sd.SiteId);
        WebReferenceCustomer.DescriptionValidationLogRequest _request = new WebReferenceCustomer.DescriptionValidationLogRequest();
        WebReferenceCustomer.enumQualityCheckName eqcn;
        if (!Enum.TryParse(QualityName, out eqcn))
            eqcn = WebReferenceCustomer.enumQualityCheckName.Error_nonAtoZ;
        _request.QualityCheckName = eqcn;
        
        _request.Description = desc;
        _request.Event = _event;
        _request.Heading = sd.ExpertiseCode;
        _request.Keyword = sd.Keyword;
        _request.OriginId = sd.OriginId;
        _request.Region = sd.RegionCode;
        _request.Session = Session.SessionID;
        _request.Step = step;
        _request.Control = sd.ControlName.ToString();
        _request.Flavor = sd.ControlName.ToString();
        _request.Url = sd.Url;
        WebReferenceCustomer.Result result = null;
        try
        {
            result = customer.DescriptionValidationLog(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);            
        }
        return string.Empty;
    }
    
}
