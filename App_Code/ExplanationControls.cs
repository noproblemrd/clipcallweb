using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for ExplanationControls
/// </summary>
public class ExplanationControls : InsertTranslate
{
    Dictionary<string, bool> dicControls;
    public ExplanationControls(Control page, Dictionary<string, string> dic, Dictionary<string, bool> dicControls):
        base(page,dic)
	{
        this.dicControls = dicControls;
	}
    protected void InsertExplanation(Control control)
    {

        if (string.IsNullOrEmpty(control.ID) || !dicControls.ContainsKey(control.ID) ||
            //linkbutton for toolbox
            !(control.GetType() == typeof(Image) || control.GetType() == typeof(LinkButton)))
            return;

        WebControl wc = (WebControl)control;
        if (!dic.ContainsKey(wc.ID))
        {
            if (wc.GetType() == typeof(Image))
                wc.Visible = false;
        }
        else
        {
            wc.Visible = true;
            //         _img.AlternateText = dic[_img.ID];
            wc.Attributes.Add("title", dic[wc.ID]);
            if (wc.GetType() == typeof(Image))
                ((Image)wc).AlternateText = dic[wc.ID];
        }
//        dicControls[_img.ID] = true;
    }
    public void StartInsertExplanation()
    {
        if (dicControls.Count == 0)
            return;
        
            myFunc func = new myFunc(InsertExplanation);
            ApplyToControls(_control, func);       
        
    }
    protected override bool SearchChilds(Control control)
    {
        if (!control.HasControls())
            return false;
        /*
        if (control == this._control)
            return true;
        if (control.GetType().IsSubclassOf(typeof(UserControl)))
            return false;
         * */
        return true;
    }
    
}
