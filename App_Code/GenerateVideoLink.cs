﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GenerateVideoLink
/// </summary>
public class GenerateVideoLink
{
    const string SCRAMBLE = "clipcall4321";
    const string VideoUrlKey = "v";
    const string PicPreviewUrlKey = "p";

    string videoUrl;
    string picPreviewUrl;
	public GenerateVideoLink(string videoUrl, string picPreviewUrl)
	{
		this.videoUrl = videoUrl;
        this.picPreviewUrl = picPreviewUrl;
	}
    public string GenerateQueryString()
    {
        string vu = EncryptString.Encrypt_String(videoUrl, SCRAMBLE);
        string pu = string.IsNullOrEmpty(picPreviewUrl) ? string.Empty : EncryptString.Encrypt_String(picPreviewUrl, SCRAMBLE);
        return VideoUrlKey + "=" + HttpUtility.UrlEncode(vu) + "&" + PicPreviewUrlKey + "=" + HttpUtility.UrlEncode(pu);
    }
    public string GenerateFullUrl(bool? isVideo = default(bool?))
    {
        if (string.IsNullOrEmpty(this.videoUrl))
            return null;
        Uri uri;
        try
        {
            uri = new Uri(this.videoUrl);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, "GenerateVideoLink videoUrl: {0}", this.videoUrl);
            return null;
        }
        string ext = System.IO.Path.GetExtension(uri.LocalPath);
        bool is_video = isVideo.HasValue && isVideo.Value;
        if (is_video && ext.Equals(""))
            return System.Web.VirtualPathUtility.ToAbsolute("~/Publisher/VideoPlayer.aspx?" + GenerateQueryString());
        if (ext.Equals(".mp3") || ext.Equals(".wav") || ext.Equals(""))
            return System.Web.VirtualPathUtility.ToAbsolute("~/Publisher/player.aspx?" + GenerateQueryString());

        //      return System.Web.VirtualPathUtility.ToAbsolute("~/player/GetVideo.ashx?" + GenerateQueryString());
        return System.Web.VirtualPathUtility.ToAbsolute("~/Publisher/jwplayer.aspx?" + GenerateQueryString());

    }
    public static GenerateVideoLink GetVideoLinkPrams(NameValueCollection queryString)
    {
        string videoUrl = null, picPreviewUrl = null;
        string v = queryString[VideoUrlKey];
        string p = queryString[PicPreviewUrlKey];
        if (!string.IsNullOrEmpty(v))
        {
            try
            {
                videoUrl = EncryptString.Decrypt_String(v, SCRAMBLE);
            }
            catch(Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
        }
        if(!string.IsNullOrEmpty(p))
        {
            try
            {
                picPreviewUrl = EncryptString.Decrypt_String(p, SCRAMBLE);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
        }           
        return new GenerateVideoLink(videoUrl, picPreviewUrl);
    }
    public string GetVideoUrl
    {
        get { return videoUrl; }
    }
    public string GetPicPreviewUrl
    {
        get { return picPreviewUrl; }
    }
    
}