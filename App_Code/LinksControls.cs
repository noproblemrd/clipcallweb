using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for LinksControls
/// </summary>
public class LinksControls : ScannerControls
{    
    protected List<LinksToPages> dic;
 //   protected string MasterID;
    public LinksControls(Control _control, string MasterID, string SiteNameId)
        : base(_control)
	{
        List<LinksToPages> list = new List<LinksToPages>();
        string command = "EXEC dbo.GetLinksByMasterNameSiteNameId @SiteNameId, @MasterName";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            cmd.Parameters.AddWithValue("@MasterName", MasterID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string _url = (reader.IsDBNull(1) ? string.Empty : (string)reader["url"]);
                LinksToPages ltp = new LinksToPages(_url, (string)reader["ControlId"],
                    (bool)reader["Visibility"]);
                list.Add(ltp);
            }
            conn.Close();
        }
        this.dic = list;
 
	}
    public LinksControls(PageSetting _control, string PageName, string SiteNameId)
        : base(_control)
    {
        List<LinksToPages> list = new List<LinksToPages>();
        string command = "EXEC dbo.GetLinksByPageNameSiteNameId @SiteNameId, @PageName";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            cmd.Parameters.AddWithValue("@PageName", PageName);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string _url = (reader.IsDBNull(1) ? string.Empty : (string)reader["url"]);
                LinksToPages ltp = new LinksToPages(_url, (string)reader["ControlId"],
                    (bool)reader["Visibility"]);
                list.Add(ltp);
            }
            conn.Close();
        }

        this.dic = list;

    }
    protected void SetLinksToPage(Control control)
    {
        if (!string.IsNullOrEmpty(control.ID))  
        {
            
            LinksToPages ltp = GetLinkOfControlId(control.ID);

            if (ltp == null)
                return;

            if (!string.IsNullOrEmpty(ltp.url))
            {

                control.Visible = true;
                if (control.GetType() == (typeof(HtmlAnchor)))
                    ((HtmlAnchor)control).HRef = ltp.url;
                else if (control.GetType() == (typeof(HyperLink)))
                    ((HyperLink)control).NavigateUrl = ltp.url;
            }
            else
            {
                if(ltp.Visibility)                
                    control.Parent.Visible = false;
            }
        
        }
    }
    public void StartTo()
    {
        if (dic==null || dic.Count == 0)
            return;
        myFunc func = new myFunc(SetLinksToPage);
        ApplyToControls(_control, func);
    }
    
    LinksToPages GetLinkOfControlId(string controlId)
    {
        foreach (LinksToPages ltp in dic)
        {
            if (ltp.controlId == controlId)
                return ltp;
        }
        return null;
    }
     
    
}
