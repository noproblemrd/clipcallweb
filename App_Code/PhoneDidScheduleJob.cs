﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PhoneDidScheduleJob
/// </summary>
public class PhoneDidScheduleJob : ScheduleJob
{
    static object didLock = new object();
    const int MINUTES_TO_WAIT = 15;
    public PhoneDidScheduleJob()
        : base(MINUTES_TO_WAIT * 60, "PhoneDidScheduleJob")
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public override void DoTask()
    {
        try
        {
            lock (didLock)
            {
                string _log = PpcSite.GetCurrent().DidPhoneJobUpdate();
                WriteLog(_log);
            }
        }
        catch (Exception exc)
        {
            //   _this.WriteLog(exc.Message);
            dbug_log.ExceptionLog(exc);
        }
    }
}