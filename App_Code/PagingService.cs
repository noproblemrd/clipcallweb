﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Text;
using System.Reflection;
using System.IO;
using System.Data;

/// <summary>
/// Summary description for PagingService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class PagingService : System.Web.Services.WebService {
    private const string BASE_PATH = "~/Publisher/SalesControls/";
    public PagingService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public string GetBillingReportPage()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl =
             (UserControl)page.LoadControl(BASE_PATH + "BillingTable.ascx");

        MethodInfo _translate = ctl.GetType().GetMethod("SetDataByPage");
        bool IsBindData = (bool)_translate.Invoke(ctl, null);
        if (!IsBindData)
            return "done";
        ctl.EnableViewState = false;

        //Render control to string
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }
    [WebMethod(true)]
    public string CreateExcel()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "BillingTable.ascx");
        MethodInfo GetCompleteTable = ctl.GetType().GetMethod("CreateExcel");
        return (string)GetCompleteTable.Invoke(ctl, null);
    }
    [WebMethod(true)]
    public string CreateExcelBillingTableAll()
    {
        DataTable dt = (Session["dataBR"] == null) ? new DataTable() : (DataTable)Session["dataBR"];
        if (dt.Rows.Count == 0)
            return string.Empty;
        ToExcel te = new ToExcel(null, "Report Billing");
        return te.CreateExcel(dt);
 //       if (!te.ExecExcel(dt))
  //          ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    [WebMethod(true)]
    public string CreateExcelByName(string SessionDatatableName, string ReportName)
    {
        DataTable dt = (Session[SessionDatatableName] == null) ? new DataTable() : (DataTable)Session[SessionDatatableName];
        if (dt.Rows.Count == 0)
            return string.Empty;
  //      ToExcel te = new ToExcel(null, ReportName);
  ToExcelV2 te = new ToExcelV2(null, ReportName);
        return te.CreateExcel(dt);
        //       if (!te.ExecExcel(dt))
        //          ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    /*
    [WebMethod(true)]
    public string GetConversionDetails(string date)
    {
        //    const string DATE_FORMAT = "yyyy-M-d HH:mm:ss";
        if (Session["Site"] == null || Session["ConversionReportRequestV"] == null)
            return string.Empty;
        //     System.Globalization.CultureInfo enUS = new System.Globalization.CultureInfo("en-US");
        SiteSetting ss = ((SiteSetting)Session["Site"]);
        long lng_date;
        if (!long.TryParse(date, out lng_date))
            return string.Empty;
        DateTime dt = new DateTime(lng_date);
        WebReferenceReports.ConversionReportRequest _ConversionReportRequest = (WebReferenceReports.ConversionReportRequest)Session["ConversionReportRequestV"];
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(ss.GetUrlWebReference);
        WebReferenceReports.ConversionDrillDownRequest _request = new WebReferenceReports.ConversionDrillDownRequest();
        _request.ControlName = _ConversionReportRequest.ControlName;
        _request.Domain = _ConversionReportRequest.Domain;
        _request.ExpertiseId = _ConversionReportRequest.ExpertiseId;
        _request.Date = dt;
        WebReferenceReports.ResultOfListOfConversionDrillDownRow result = null;
        try
        {
            result = _report.ConversionDrillDown(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return string.Empty;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
            return string.Empty;
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "ConversionDrillDown.ascx");
        MethodInfo LoadData = ctl.GetType().GetMethod("LoadData");
        LoadData.Invoke(ctl, new object[]{ result.Value });
        ctl.EnableViewState = false;
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }
     * */
    [WebMethod(true)]
    public string GetConversionDetails2(string date, string Country)
    {
    //    const string DATE_FORMAT = "yyyy-M-d HH:mm:ss";
        if (Session["Site"] == null || Session["ConversionReportRequestV"] == null)
            return string.Empty;
   //     System.Globalization.CultureInfo enUS = new System.Globalization.CultureInfo("en-US");
        SiteSetting ss = ((SiteSetting)Session["Site"]);
        
        int CountryId;
        if (!int.TryParse(Country, out CountryId))
            CountryId = -1;
        DateTime _from = DateTime.MinValue;
        DateTime _to = DateTime.MinValue;
        if (date.Contains(";"))
        {
            string[] dates = date.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries); 
            long lng_from, lng_to;
            if (!long.TryParse(dates[0], out lng_from))
                return string.Empty;
            if (!long.TryParse(dates[1], out lng_to))
                return string.Empty;
            _from = new DateTime(lng_from);
            _to = new DateTime(lng_to);
        }
        else
        {
            long lng_date;
            if (!long.TryParse(date, out lng_date))
                return string.Empty;
            _from = new DateTime(lng_date);
            _to = _from;
        }
        WebReferenceReports.ConversionReportRequest _ConversionReportRequest = (WebReferenceReports.ConversionReportRequest)Session["ConversionReportRequestV"];
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(ss.GetUrlWebReference);
        WebReferenceReports.ConversionDrillDownRequest _request = new WebReferenceReports.ConversionDrillDownRequest();
        _request.ControlName = _ConversionReportRequest.ControlName;
     //   _request.Domain = _ConversionReportRequest.Domain;
        _request.ExpertiseId = _ConversionReportRequest.ExpertiseId;
        _request.Interval = _ConversionReportRequest.Interval;
        _request.from = _from;
        _request.to = _to;
        _request.CountryId = CountryId;
        _request.SliderType = _ConversionReportRequest.SliderType;
        WebReferenceReports.ResultOfListOfConversionDrillDownRow result = null;
        try
        {
            result = _report.ConversionDrillDown2(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ss);
            return string.Empty;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
            return string.Empty;
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "ConversionDrillDown.ascx");
        MethodInfo LoadData = ctl.GetType().GetMethod("LoadData");
        LoadData.Invoke(ctl, new object[] { result.Value });
        ctl.EnableViewState = false;
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }
    
    [WebMethod(true)]
    public string GetInstallationDrillDown(string date)
    {
       
        DateTime _from = DateTime.MinValue;
        DateTime _to = DateTime.MinValue;
        List<Guid> Origins = new List<Guid>();
        WebReferenceReports.AddOnInstallationReportRequest SessionRequest = (Session["AddOnInstallationReportRequest"] == null) ? null : (WebReferenceReports.AddOnInstallationReportRequest)Session["AddOnInstallationReportRequest"];
        if (SessionRequest == null)
            return "Session expired! Please run the report again.";
        WebReferenceReports.AddOnInstallationReportRequest _request = new WebReferenceReports.AddOnInstallationReportRequest();
        long lng_date;
        if (!long.TryParse(date, out lng_date))
            lng_date = 0;
        if (lng_date == 0)
        {
            _request.FromDate = SessionRequest.FromDate;
            _request.ToDate = SessionRequest.ToDate;
        }
        else
        {
            _request.FromDate = new DateTime(lng_date);
            _request.ToDate = new DateTime(lng_date);
        }
        _request.Country = SessionRequest.Country;
        _request.Origins = SessionRequest.Origins;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(PpcSite.GetCurrent().UrlWebReference);

        WebReferenceReports.ResultOfDistributionInstallationReportResponseDrillDown result = null;
        try
        {
            result = _report.DistributionInstallationReportDrillDown(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return "Query failed";
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
            return "Query failed";
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "InstallationDrillDown.ascx");
        Type CtlType = ctl.GetType();
        MethodInfo LoadData = CtlType.GetMethod("LoadData");
        LoadData.Invoke(ctl, new object[] { result.Value });
        PropertyInfo RowNum = CtlType.GetProperty("row_num");
        int row_num = (int)(RowNum.GetValue(ctl, null));
        if (row_num == 0)
            return "There are no data!";
        ctl.EnableViewState = false;
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
     
    }
    [WebMethod(true)]
    public string NP_DistributionReport_CreateExcel()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "NP_DistributionReport.ascx");
        MethodInfo GetCompleteTable = ctl.GetType().GetMethod("CreateExcel");
        return (string)GetCompleteTable.Invoke(ctl, null);
    }
    [WebMethod(true)]
    public string NP_DistributionReportOrigin_CreateExcel()
    {
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "NP_DistributionReportOrigin.ascx");
        MethodInfo GetCompleteTable = ctl.GetType().GetMethod("CreateExcel");
        return (string)GetCompleteTable.Invoke(ctl, null);
    }
}
