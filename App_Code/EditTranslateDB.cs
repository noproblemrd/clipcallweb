using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

/// <summary>
/// Summary description for EditTranslateDB
/// </summary>
public class EditTranslateDB
{
	public EditTranslateDB()
	{
		//
		
		//
	}
    public static void SetNewEnumValue(int PageId, string _value)
    {
        string command = "EXEC dbo.InsertLiteralByPageId @PageId, " +
            "@Sentence, @ControlId, @IndexItem, @LiteralType, @ControlValue";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PageId", PageId);
            cmd.Parameters.AddWithValue("@Sentence", _value);
            cmd.Parameters.AddWithValue("@ControlId", _value);
            cmd.Parameters.AddWithValue("@IndexItem", DBNull.Value);
            cmd.Parameters.AddWithValue("@LiteralType", LiteralType.Regular.ToString());
            cmd.Parameters.AddWithValue("@ControlValue", DBNull.Value);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
    public static void DeleteEnumValue(int LiteralId)
    {
        string command = "EXEC dbo.DeleteLiteralEngByLiteralId @LiteralId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@LiteralId", LiteralId);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
    public static string GetControlTranslate(int SiteLangId, string PageName, string ControlId)
    {
        string command = "SELECT dbo.GetTranslateByControl(@SiteLangId, " +
                    "@PageName, @ControlId)";
        string result;
        using (SqlConnection conn = DBConnection.GetConnString())
        {

            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
                cmd.Parameters.AddWithValue("@PageName", PageName);
                cmd.Parameters.AddWithValue("@ControlId", ControlId);
                result = (string)cmd.ExecuteScalar();
            }
            catch (Exception exc)
            {
                dbug_log.TranslationException(exc, PageName, ControlId, SiteLangId);
                //If There is not value
                return ControlId;
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }
        }
        if (string.IsNullOrEmpty(result))
            return ControlId;
        return result;
    }
    public static string GetControlTranslate(int SiteLangId, int LiteralId)
    {
        string result = string.Empty;
        string command = "SELECT dbo.GetTranslateByLiteralId(@SiteLangId, @LiteralId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
            cmd.Parameters.AddWithValue("@LiteralId", LiteralId);
            result = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return result;
    }
    public static string GetPageNameTranslate(string PageName, int SiteLangId)
    {
        string result = "";
        string command = @"SELECT dbo.GetTranslateOfPageByPageName(@SiteLangId, @PageName)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
            cmd.Parameters.AddWithValue("@PageName", PageName);
            result = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return result;
    }
}
