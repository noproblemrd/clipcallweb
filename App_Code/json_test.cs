﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for json_test
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class json_test : System.Web.Services.WebService {

    public json_test () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetStam()
    {
        stamObj _stam = new stamObj();
        _stam.name = @"y""oav";
        _stam.age = "2'3";
        JavaScriptSerializer js = new JavaScriptSerializer();
        string str = js.Serialize(_stam);
        return str;
    }
    [WebMethod]
    public string SetStam(string str)
    {
        
        JavaScriptSerializer js = new JavaScriptSerializer();
        List<stamObj> stam = js.Deserialize<List<stamObj>>(str);
         
        return "";
    }
    [WebMethod]
    public List<stamObj> SetArrStam(string str)
    {
    JavaScriptSerializer js = new JavaScriptSerializer();
        List<stamObj> stam = js.Deserialize<List<stamObj>>(str);
        return stam;
    }
    
}
public class stamObj
{
    public stamObj() { }
    public string name { get; set; }
    public string age { get; set; }
}
