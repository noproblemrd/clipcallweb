﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Net;
using System.Xml;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.Threading;
using System.Security.Principal;
using System.Data;
using WebReferenceCustomer;

/// <summary>
/// Summary description for AddOnServices
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class AddOnServices : System.Web.Services.WebService
{
    private const string PROXY = "proxy";
    private const string HeartBeatInjectionTable = "[dbo].[HeartBeatInjection]";
    private const string HeartBeatLogicTable = "[dbo].[HeartBeatLogic]";

    public AddOnServices()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /// <summary>
    /// Missing method for driver app
    /// </summary>
    [WebMethod]
    public void UserDyn()
    {

    }

    /// <summary>
    /// Missing method for driver app
    /// </summary>
    [WebMethod]
    public void UserUpdate()
    {

    }

    /// <summary>
    /// Missing method for driver app
    /// </summary>
    [WebMethod]
    public void Supdate()
    {

    }

    [WebMethod]
    public void CheckForceInstallByIp()
    {
        int retVal = 0;
        string ip = Utilities.GetIP(Context.Request);
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(checkForceInstallByIpQuery, conn))
            {
                cmd.Parameters.AddWithValue("@ip", ip);
                object inject = cmd.ExecuteScalar();
                if (inject != null)
                {
                    if ((bool)inject)
                    {
                        retVal = 2;
                    }
                    else
                    {
                        retVal = 1;
                    }
                }
                conn.Close();
            }
        }
        Context.Response.Write(retVal);
    }

    private const string checkForceInstallByIpQuery =
        @"SELECT top 1 Inject
  FROM DesktopAppForceInstallConfig
  where IP = @ip
  and GETDATE() between FromDateTime and ToDateTime";

    [WebMethod]
    public void DeskAppLog(
        string type, //install or error
        string osn, //os name
        string ose, //os edition
        string sp, //service pack
        string osv, //os version
        string osb, //os bites (65 or 32)
        string dnetvs,//dot net versions intalled (; separated)
        bool pen, //proxy enabled in registry
        string pser,//proxy server in registry
        bool pspu,//proxySettginsPerUser in registry
        string uid,//userid
        string sid,//siteid
        string orid,//originid
        string nottxt,//notificationtext
        string conjse,//configuration javascript element
        string conmod,//configuration modified on
        string conver,//configuration version
        int por,//port used
        string instprogs//installed programs (json array)
        )
    {
        string retVal = "0,ok";
        try
        {
            string appType = Context.Request.Params["app_type"];
            if (String.IsNullOrEmpty(appType))
                appType = PROXY;
            var coreAppClient = WebServiceConfig.GetCustomerReference(null, sid);
            coreAppClient.Timeout = 300000;
            var result = coreAppClient.DeskAppLog(new WebReferenceCustomer.DeskAppLogRequest
                  {
                      Type = type,
                      OperatingSystemName = osn,
                      OperatingSystemEdition = ose,
                      ServicePack = sp,
                      OperatingSystemVersion = osv,
                      OperatingSystemBites = osb,
                      DotNetVersions = dnetvs,
                      ProxyEnabled = pen,
                      ProxyServer = pser,
                      ProxySettingPerUser = pspu,
                      UserId = uid,
                      OriginId = new Guid(orid),
                      NotificationText = nottxt,
                      ConfigurationScriptElement = conjse,
                      ConfigurationModifiedOn = conmod,
                      ConfigurationVersion = conver,
                      Port = por,
                      InstalledPrograms = instprogs,
                      AppType = appType
                  });
            if (result.Type == WebReferenceCustomer.eResultType.Failure)
            {
                retVal = "1,error in core application";
            }
        }
        catch (Exception exc)
        {
            retVal = "1,exception in web application";
            dbug_log.ExceptionLog(exc, "Exception in DeskAppLog");
        }
        Context.Response.StatusCode = 200;
        Context.Response.Write(retVal);
    }

    [WebMethod]
    public void UserInstallByUpdater()
    {
        HandleAddOnAction(true, true);
    }

    [WebMethod]
    public void UserInstall()
    {
        HandleAddOnAction(true);
    }


    [WebMethod]
    public void ReportInstallation()
    {        
        string ip = Utilities.GetIP(Context.Request);
        GeoLocation gl = SalesUtility.GetGeoLocation(ip);
        string appName = Context.Request.Params["appName"];

        string siteId = null;
        CustomersService customersService = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);
        customersService.ReportInstallation(new ReportInstallationRequest()
        {
            ApplicationName = appName,
            IpAddress = ip
        });
    }

    [WebMethod]
    public void UserUnInstall()
    {
        HandleAddOnAction(false);
    }

    private string HandleAddOnAction(bool isInstalltion)
    {
        return HandleAddOnAction(isInstalltion, false);
    }

    private string HandleAddOnAction(bool isInstallation, bool isUpdater)
    {
        string retVal;
        try
        {
            Guid originId;
            string[] macArr;
            string siteId;
            string appType;
            bool? isDuplicate;
            string subId;
            bool argsOk = GetArguments(out originId, out macArr, out retVal, out siteId, out appType, out isDuplicate, out subId);
            if (!argsOk)
            {
                Context.Response.Write(retVal);
                return retVal;
            }
            string ip = Utilities.GetIP(Context.Request);
            GeoLocation gl = SalesUtility.GetGeoLocation(ip);
            string country = null;
            if (gl != null)
                country = gl.Country;
            var coreAppClient = WebServiceConfig.GetCustomerReference(null, siteId);
            coreAppClient.Timeout = 300000;
            var result = coreAppClient.HandleAddOnAction(new WebReferenceCustomer.AddOnActionRequest()
            {
                IP = ip,
                IsInstallation = isInstallation,
                MacAddresses = macArr,
                OriginId = originId,
                AppType = appType,
                IsDuplicate = isDuplicate,
                SubId = subId,
                Country = country,
                IsUpdater = isUpdater
            });
            if (result.Type == WebReferenceCustomer.eResultType.Failure)
            {
                retVal = "1,exception in core application";
            }
        }
        catch (Exception exc)
        {
            retVal = "1,exception in web application";
            dbug_log.ExceptionLog(exc, "Exception in HandleAddOnAction");
        }
        Context.Response.Write(retVal);
        return retVal;
    }

    private bool GetArguments(out Guid originId, out string[] macArr, out string retVal, out string siteId, out string appType, out bool? isDuplicate, out string subId)
    {
        string uid = Context.Request.Params["uid"];
        if (String.IsNullOrEmpty(uid))
        {
            string macAddresses = Context.Request.Params["mac_addresses"];
            if (macAddresses != null)
                macArr = macAddresses.Split(';');
            else
                macArr = new string[0];
        }
        else
        {
            macArr = new string[] { uid };
        }
        string originIdStr = Context.Request.Params["origin_id"];
        siteId = Context.Request.Params["site_id"];

        appType = Context.Request.Params["app_type"];
        string isDuplicateParam = Context.Request.Params["is_duplicate"];
        isDuplicate = null;
        if (isDuplicateParam != null)
        {
            isDuplicate = isDuplicateParam == "true";
        }
        subId = Context.Request.Params["subid"];
        if (!Guid.TryParse(originIdStr, out originId))
        {
            originId = Guid.Empty;
            retVal = "1,argument origin_id is not valid";
            return false;
        }
        if (macArr == null || macArr.Length == 0)
        {
            retVal = "1,argument mac_addresses or uid cannot be empty";
            return false;
        }
        if (string.IsNullOrEmpty(siteId))
        {
            retVal = "1,argument site_id cannot be empty";
            return false;
        }
        retVal = "0,success";
        return true;
    }

    /*
    [WebMethod]
    public void UpdateKeywordJs()
    {
        AddonTools.UpdateKeywordJs();
    }
     * */
    /*
    [WebMethod]
    public void RecreateLogicJS()
    {
        string poc;
        string _script = AddonTools.GetLogicJS(out poc);
        string destination = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\_logic.js";
        string destination_poc = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\poc.js";
       
        bool IsSave = false;
        do
        {
            IsSave = SalesUtility.SaveToFile(destination, _script);
            if (!IsSave)
                System.Threading.Thread.Sleep(5000);
        } while (!IsSave);
        IsSave = false;
        do
        {
            IsSave = SalesUtility.SaveToFile(destination_poc, poc);
            if (!IsSave)
                System.Threading.Thread.Sleep(5000);
        } while (!IsSave);
        //FileInfo fi = new FileInfo(destination);
        PpcSite.GetCurrent().UpdateDateModified();//fi.LastWriteTime;
    }
     
    [WebMethod]
    public void UpdateDateModified()
    {
        PpcSite.GetCurrent().UpdateDateModified();
    }
     * * */
    /*
    [WebMethod]
    public bool RecreateServerLogicJS()
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        /**** Set js logic   ***
        try
        {
            for (int i = 0; i < xdoc.Element("Sites").Element("SitePPC").Element("WebServers").Elements("Server").Count(); i++)
            {
                string _value = (i == 0) ? xdoc.Element("Sites").Element("SitePPC").Element("WebServers").Elements("Server").ElementAt(i).Value + "RecreateLogicJS" :
                            xdoc.Element("Sites").Element("SitePPC").Element("WebServers").Elements("Server").ElementAt(i).Value + "UpdateDateModified";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_value);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false;
        }
        // RecreateLogicJS();
        return true;
    }
     * */

    [WebMethod]
    public string UpdateCache(Guid UserId)
    {
        /*
        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
        {
            PpcSite._UpdateCache(UserId);
        }));
         * */
        UpdateCacheResponse ucr = PpcSite._UpdateCache(UserId);
        JavaScriptSerializer jss = new JavaScriptSerializer();
        return jss.Serialize(ucr);
    }
   
   
    [WebMethod]
    public void UpdateSingleServerCache()
    {
   //     WindowsIdentity _Identity = System.Security.Principal.WindowsIdentity.GetCurrent();
   //     dbug_log.ExceptionLog(new Exception("_Identity: " + _Identity.Name));
    //    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
   //        {
       //        using (WindowsIdentity.Impersonate(IntPtr.Zero))
      //         {
            //   System.Security.Principal.WindowsImpersonationContext wi = _Identity.Impersonate();
               /*
               using (WindowsIdentity newId = new WindowsIdentity(IntPtr.Zero))
               {
                   using (WindowsImpersonationContext impersonatedUser = newId.Impersonate())
                   {
                * */
             //  System.Security.Principal.WindowsImpersonationContext wi =  _Identity.Impersonate();
           //    dbug_log.ExceptionLog(new Exception("_Identity: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name));
                       string _id = Context.Request.QueryString["UserId"];
                       string UserName = Context.Request.QueryString["UserName"];
                       Guid UserId;
                       if (!Guid.TryParse(_id, out UserId) || string.IsNullOrEmpty(UserName))
                       {
                           PpcCacheException pce = new PpcCacheException("UpdateSingleServerCache UserName Or UserId incorrect");
                           Dictionary<string, string> dic = new Dictionary<string, string>();
                           dic.Add("UserId", _id);
                           dic.Add("UserName", UserName);
                           CacheLog.WriteException(pce);
                           return;
                       }
                       CacheLog._StartUpdateCache(UserId, UserName);
                       PpcSite.SetCach(UserId, UserName);
               /*
                   }
               }
           */
  //         }));
    }

    [WebMethod]
    public void UpdateSingleBlackList()
    {
        PpcSite.ReloadKeywordDomainBlackList();
    }
    [WebMethod]
    public void InsertDAUS(DAUS[] users)
    {
        Guid UnknownOriginId = PpcSite.GetCurrent().UnknoenOriginId;
        string command = "EXEC [dbo].[InsertDAUS_WithCountry] @OriginId, @date, @hits, @visitors, @page, @CountryId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            foreach (DAUS d in users)
            {
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@OriginId", ((d.OriginId == Guid.Empty) ? UnknownOriginId : d.OriginId));
                cmd.Parameters.AddWithValue("@date", d.Date);
                cmd.Parameters.AddWithValue("@hits", d.Hits);
                cmd.Parameters.AddWithValue("@visitors", d.Visitors);
                string _page = string.IsNullOrEmpty(d.Page) ? "logic.js" : d.Page;
                cmd.Parameters.AddWithValue("@page", _page);
                cmd.Parameters.AddWithValue("@CountryId", d.CountryId);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            conn.Close();
        }
    }
    [WebMethod]
    public IntStringPair[] GetControlNames()
    {
        List<IntStringPair> list = new List<IntStringPair>();
        foreach (eFlavour flavor in Enum.GetValues(typeof(eFlavour)))
        {
            IntStringPair isp = new IntStringPair() { num = (int)flavor, str = flavor.ToString() };
            list.Add(isp);
        }
        return list.ToArray();
    }
    [WebMethod]
    public IntStringPair[] GetControlNamesNotForKeywordReport()
    {
        List<IntStringPair> list = new List<IntStringPair>();
        foreach (eFlavour flavor in Enum.GetValues(typeof(eFlavour)))
        {
            switch (flavor)
            {
                case (eFlavour.WelcomeScreen):
                case (eFlavour.NoProblemHomepage):
                case (eFlavour.Flavor_Directory_Business):
                    list.Add(new IntStringPair() { num = (int)flavor, str = flavor.ToString() });
                    break;
            }
        }
        return list.ToArray();
    }
    [WebMethod]
    public bool PublishInjection()
    {
        string _id = Context.Request.QueryString["UserId"];
        string UserName = Context.Request.QueryString["UserName"];
        Guid UserId;
        if (!Guid.TryParse(_id, out UserId))
            UserId = Guid.Empty;
        CacheLog._StartPublishInjection(UserId, UserName);
        PpcSite _cache = PpcSite.GetCurrent();
        string _inject = InjectionBuilder.Build(_cache);
        _cache.InjJs = _inject;
        return true;
    }
    [WebMethod]
    public bool PublishPopupEngine()
    {
        string _id = Context.Request.QueryString["UserId"];
        string UserName = Context.Request.QueryString["UserName"];
        Guid UserId;
        if (!Guid.TryParse(_id, out UserId))
            UserId = Guid.Empty;
     //   CacheLog._StartPublishInjection(UserId, UserName);
        string _dataPopup;
        PpcSite _cache = PpcSite.GetCurrent();
        PopupBuilder pb = new PopupBuilder();
        string _popup = pb.GetPopupJs(_cache, out _dataPopup);
        _cache.PopupProductScript = _popup;
        _cache.DataPopupProductScript = _dataPopup;
        return true;
    }
    [WebMethod]
    public string UpdateIntextXml(Guid UserId)
    {
         
        JavaScriptSerializer jss = new JavaScriptSerializer();
        UpdateIntextXmlResponse uixr = new UpdateIntextXmlResponse();
        CacheLog cl = new CacheLog();
        string UserName;
        if (!SalesUtility.TryGetPublisherUserName(UserId, PpcSite.GetCurrent().UrlWebReference, out UserName))
        {
            uixr.SetStatus(eIntextXmlUpdate.UserNotFound);
            PpcCacheException pce = new PpcCacheException(eServerUpdate.UserNotFound.ToString());
            pce.NewValues = new Dictionary<string, string>();
            pce.NewValues.Add("UserId", UserId.ToString());
            cl.WriteCacheLog(pce);
            return jss.Serialize(uixr);
        }
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        foreach (XElement xelem in xdoc.Element("Sites").Element("SitePPC").Element("OriginForIntext").Elements("Origin"))
    //    Parallel.ForEach(xdoc.Element("Sites").Element("SitePPC").Element("OriginForIntext").Elements("Origin"), xelem =>
        {
            try
            {
                Guid OriginId;
                if (!Guid.TryParse(xelem.Value, out OriginId))
                {
                    PpcCacheException pce = new PpcCacheException(eServerUpdate.Faild.ToString());
                    pce.NewValues = new Dictionary<string, string>();
                    pce.NewValues.Add("Origin", xelem.ToString());
                    cl.WriteCacheLog(pce);
                    uixr.OriginFailds.Add("Origin: " + xelem.ToString());
                    uixr.SetStatus(eIntextXmlUpdate.Faild);
                    continue;
                }
                Intext_XML_Builder IXB = new Intext_XML_Builder(PpcSite.GetCurrent(), OriginId);
                IXB.BuildXML();
            }
            catch(Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
        }
        return jss.Serialize(uixr);
      
    }
    [WebMethod]
    public bool InsertHeartBeat(HeartBeatData[] data)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("InjectionId", typeof(Guid));
        dt.Columns.Add("OriginId", typeof(Guid));
        dt.Columns.Add("Date", typeof(DateTime));
        dt.Columns.Add("Hits", typeof(int));
        dt.Columns.Add("CreatedOn", typeof(DateTime));
        foreach (HeartBeatData hbd in data)
        {
            DataRow row = dt.NewRow();
            row["InjectionId"] = hbd.InjectionId;
            row["OriginId"] = hbd.OriginId;
            row["Date"] = hbd.Date;
            row["Hits"] = hbd.Hits;
            row["CreatedOn"] = hbd.CreatedOn;
            dt.Rows.Add(row);
        }
        try
        {
            using (SqlConnection conn = DBConnection.GetSalesConnString())
            {
                conn.Open();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                {
                    bulkCopy.DestinationTableName = HeartBeatInjectionTable;
                    bulkCopy.BulkCopyTimeout = 300;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(dc.ColumnName, dc.ColumnName);
                    }
                    bulkCopy.WriteToServer(dt);
                }
                conn.Close();
            }
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false;
        }
        return true;
    }
    [WebMethod]
    public bool InsertLogicHeartBeat(LogicHeartBeatData[] data)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("OriginId", typeof(Guid));
        dt.Columns.Add("Date", typeof(DateTime));
        dt.Columns.Add("Hits", typeof(int));
        dt.Columns.Add("CreatedOn", typeof(DateTime));
        foreach (LogicHeartBeatData hbd in data)
        {
            DataRow row = dt.NewRow();
            row["OriginId"] = hbd.OriginId;
            row["Date"] = hbd.Date;
            row["Hits"] = hbd.Hits;
            row["CreatedOn"] = hbd.CreatedOn;
            dt.Rows.Add(row);
        }
        try
        {
            using (SqlConnection conn = DBConnection.GetSalesConnString())
            {
                conn.Open();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                {
                    bulkCopy.DestinationTableName = HeartBeatLogicTable;
                    bulkCopy.BulkCopyTimeout = 300;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        bulkCopy.ColumnMappings.Add(dc.ColumnName, dc.ColumnName);
                    }
                    bulkCopy.WriteToServer(dt);
                }
                conn.Close();
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false;
        }
        return true;
    }
    [WebMethod]
    public bool ClearPhoneMobileApp(string phone)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSite.ResultOfBoolean result = null;
        try
        {
            result = _site.ClearPhoneMobileApp(phone);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false;
        }
        return result.Type == WebReferenceSite.eResultType.Failure ? false : result.Value;
    }
}
