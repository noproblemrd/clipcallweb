using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.Script.Services;


/// <summary>
/// Summary description for RecordService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class RecordService : System.Web.Services.WebService
{

    public RecordService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public string GetPath(string _id)
    {
        if (Session["RecordList"] == null)
            return string.Empty;
        Dictionary<int, string> dic = (Dictionary<int, string>)Session["RecordList"];
        int id = int.Parse(_id);
        if (!dic.ContainsKey(id))
            return string.Empty;
        string fileName;
        string TempPath = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        string TempPathWeb = System.Configuration.ConfigurationManager.AppSettings["professionalRecordWeb"];
        string _path = dic[id];
        string p_record;
        if (!_path.Contains("http"))
        {
            if (string.IsNullOrEmpty(_path) || !File.Exists(_path))
            {
                if (!File.Exists(_path))
                    dbug_log.ExceptionLog(new Exception(_path));
                return string.Empty;
            }

            fileName = System.IO.Path.GetFileName(_path);
            File.Copy(_path, TempPath + fileName, true);
            p_record = TempPathWeb + fileName;
        }
        else
        {
            if (_path.EndsWith(".wav"))
                fileName = _id + ".wav";
            else
                fileName = _id + ".mp3";
            if (!GetPathTwillo(_path, TempPath + fileName))
                return string.Empty;
            p_record = TempPathWeb + fileName;
        }
        return p_record;
    }

    [WebMethod(true)]
    public string GetPath2(string _id)
    {
        if (Session["RecordList"] == null)
            return string.Empty;
        Guid guid_id;
        if (!Guid.TryParse(_id, out guid_id))
            return string.Empty;
        Dictionary<Guid, string> dic = (Dictionary<Guid, string>)Session["RecordList"];

        if (!dic.ContainsKey(guid_id))
            return string.Empty;
        string fileName;
        string TempPath = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        string TempPathWeb = System.Configuration.ConfigurationManager.AppSettings["professionalRecordWeb"];
        string CheckRecordPath = TempPath + _id + ".mp3";
        //if (File.Exists(CheckRecordPath))
        // return "exist&" + _id;
        string _path = dic[guid_id];
        string p_record;
        if (!_path.Contains("http"))
        {
            if (string.IsNullOrEmpty(_path) || !File.Exists(_path))
            {
                if (!File.Exists(_path))
                    dbug_log.ExceptionLog(new Exception(_path));
                return string.Empty;
            }

            fileName = System.IO.Path.GetFileName(_path);
            File.Copy(_path, TempPath + fileName, true);
            p_record = TempPathWeb + fileName;
        }
        else
        {
            if (_path.EndsWith(".wav"))
                fileName = _id + ".wav";
            else
                fileName = _id + ".mp3";
            if (!GetPathTwillo(_path, TempPath + fileName))
                return string.Empty;
            p_record = TempPathWeb + fileName;
        }
        return p_record + "&" + _id;
    }

    bool GetPathTwillo(string UrlPath, string SavePath)
    {
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(UrlPath);

        webRequest.Method = "GET";

        WebResponse webResponse = webRequest.GetResponse();
        Stream webStream = webResponse.GetResponseStream();
        using (Stream _file = File.Create(SavePath))
        {
            CopyStream(webStream, _file);
        }
        return true;
    }
    void CopyStream(Stream input, Stream output)
    {
        byte[] buffer = new byte[8 * 1024];
        int len;
        while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            output.Write(buffer, 0, len);
        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetTest(string callback)
    {
        dbug_log.ExceptionLog(new Exception(), "CROSS DOMAIN");
        //   return call_back+"({name:yoav})";
        TheTest _test = new TheTest() { age = "23", name = "yoav" };
        JavaScriptSerializer jss = new JavaScriptSerializer();
        return jss.Serialize(_test);

    }
}
class TheTest
{
    public string name;
    public string age;
}

