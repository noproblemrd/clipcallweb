using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Linq;
using System.Data.SqlClient;
using System.Net;
using System.Collections.Specialized;
using System.Collections;

/// <summary>
/// Summary description for Utilities
/// </summary>
public class Utilities
{
    public const string SUPPLIER_REQUEST_REPORT_URL = "~/Publisher/ClipCallRequestReport.aspx?supplierid={0}";
    public const string CUSTOMER_REQUEST_REPORT_URL = "~/Publisher/ClipCallRequestReport.aspx?customerid={0}";
    public const string CHAT_TRACE_URL = "~/Publisher/ClipCallLeadChat.aspx?ia={0}";
    public const string FOULS_MANAGMENT_URL = "~/Publisher/FoulsManagment.aspx?accountid={0}";

    public const string SURVEY_REPORT_URL = "~/Publisher/SurveyReport.aspx?ia={0}";
    public const string SUPPLIER_SURVEY_REPORT_URL = "~/Publisher/SurveyReport.aspx?sid={0}";

    public const string SUPPLIER_REVIEW_URL = @"~/Publisher/SupplierReviewReport.aspx?sid={0}";
    public const string CUSTOMER_REVIEW_URL = @"~/Publisher/SupplierReviewReport.aspx?cid={0}";

    public static string GET_CUSTOMER_REQUEST_REPORT_URL = CUSTOMER_REQUEST_REPORT_URL.Remove(0, 1);

    public const string V_ICON = "~/Publisher/images/icon-v.png";
    public const string X_ICON = "~/Publisher/images/icon-x.png";
    public const string NUM_FORMAT = "{0:#,0.##}";
    public const string DATE_TIME_FORMAT = "{0:MM/dd/yyyy HH:mm}";

    const string CdataStart = "<![CDATA[";
    const string CdataEnd = "]]>";
    const string REFUND_ALERT_USER = "RefundAlertUser";
    const string SCRAMBEL = "*4261np";
	public Utilities()
	{
		//
		
		//
	}
    public static string CleanCdata(string content)
    {
        if (!content.Contains(CdataStart))
            return content;
        content = content.Replace(CdataStart, "");
        content = content.Replace(CdataEnd, "");
        return content;
    }

    public static string badCharacters(string content)
    {        
        List<string> listBadChars = new List<string>();
        //listBadChars.Add("'");
        listBadChars.Add("''");        
        //listBadChars.Add("\"");
        listBadChars.Add("\"\"");
    //    listBadChars.Add("/");
        listBadChars.Add("\\");
        listBadChars.Add(";");
        listBadChars.Add("NULL");
        listBadChars.Add("crlf");
        listBadChars.Add("Select");
        listBadChars.Add("%");
        listBadChars.Add("exe");
        listBadChars.Add("^");
        listBadChars.Add("[,]");
        listBadChars.Add("<");
        listBadChars.Add(">");
        string temp=content;
         
        
        for (int i = 0; i < listBadChars.Count; i++)
        {
            temp=temp.Replace(listBadChars[i].ToLower(),"");

        }
       
        return temp;

    }

    public static string encodeSpecialCharacters(string content)
    {
        List<string> listBadChars = new List<string>();
        listBadChars.Add("'");
        listBadChars.Add("\"");
        listBadChars.Add("&");       

        string temp = content;


        for (int i = 0; i < listBadChars.Count; i++)
        {
            temp = temp.Replace(listBadChars[i], HttpContext.Current.Server.HtmlEncode(listBadChars[i]));

        }

        return temp;
    }
    /*
    public static string UmperToJavaScript(string str)
    {
      //  return str.Replace("&", "&amp;");
 //       str = str.Replace(";", "");
//      str = str.Replace("'", "");
        return str.Replace("&", "");
    }
     * */
    public static string ChangeCharacterForXml(string str)
    {
        str = str.Replace("&", "&amp;");
        str = str.Replace("<", "&lt;");
        str = str.Replace(">", "&gt;");
        str = str.Replace("\"", "&quot;");       
        return str.Replace("'", "&apos;");
    }
    public static string CleanStringGuid(string _guid)
    {
        _guid = _guid.Replace("{", "");
        _guid = _guid.Replace("}", "");
        return _guid;
    }

    public static bool HasStyleSheetLoaded(Page page, string cssClassRelativeUrl)
    {
        
        foreach (Control control in page.Header.Controls)
        {
            // Check all literal controls as that is how we add them to the header
            if (control is Literal)
            {
                string text = ((Literal)control).Text;
                if (text != null)
                {
                    if (text.IndexOf(cssClassRelativeUrl) != -1)
                    {
                        return true;                        
                    }
                }
            }
        }
        return false;
    }
    public static int LargeTest(string a, string b)
    {
        a = a.ToLower();
        b = b.ToLower();
        for (int i = 0; i < a.Length && i < b.Length; i++)
        {
            if (a[i] > b[i])
                return 1;
            if (a[i] < b[i])
                return -1;
        }
        if (a.Length == b.Length)
            return 0;        
        return -1;        
    }
    public static string RegularExpressionForJavascript(string _reg)
    {
        string result = string.Empty;
        for (int i = 0; i < _reg.Length; i++)
        {
            if (_reg[i] == '\\')
            {
                if (_reg.Length > i + 1 && _reg[i + 1] != '\\')
                    if (i - 1 < 0 || _reg[i - 1] != '\\')
                        result += '\\';
            }
            result += _reg[i];

        }
        return result;
    }
    public static string GetPathName(Page pg)
    {
        return pg.Request.ServerVariables["SCRIPT_NAME"];
        
    }
    public static string GetPageName(Page pg)
    {
        return System.IO.Path.GetFileName(pg.Request.ServerVariables["SCRIPT_NAME"]);

    }
    // Convert an object to a byte array
    public static byte[] ObjectToByteArray(Object obj)
    {
        if (obj == null)
            return null;
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);
        return ms.ToArray();
    }

    // Convert a byte array to an Object
    public static Object ByteArrayToObject(byte[] arrBytes)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(arrBytes, 0, arrBytes.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        Object obj = (Object)binForm.Deserialize(memStream);
        return obj;
    }
    public static string RandomString(int size)
    {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        char ch;
        for (int i = 0; i < size; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 97)));
            builder.Append(ch);
        }

        return builder.ToString();
    }
    public static string GetMimeType(FileInfo fileInfo)
    {
        string mimeType = "application/unknown";

        Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(
            fileInfo.Extension.ToLower()
            );

        if (regKey != null)
        {
            object contentType = regKey.GetValue("Content Type");

            if (contentType != null)
                mimeType = contentType.ToString();
        }

        return mimeType;
    }
    public static bool IsGUID(string expression)
    {
        /*
        if (!string.IsNullOrEmpty(expression))
        {
            Regex guidRegEx = new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");
            return guidRegEx.IsMatch(expression);
        }
        return false;
         * */
        Guid _id;
        return Guid.TryParse(expression, out _id);
    }
    public static string GetRamzorImage(WebReferenceSupplier.SupplierStatus _status, bool IsInactiveWithMoney)
    {
        string _resolveURL = string.Format("{0}://{1}{2}",
               HttpContext.Current.Request.Url.Scheme,
               HttpContext.Current.Request.ServerVariables["HTTP_HOST"],
               (HttpContext.Current.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Current.Request.ApplicationPath
               );
        if (IsInactiveWithMoney)
            return _resolveURL + "/images/InactiveWithMoney.png";
        if (_status == WebReferenceSupplier.SupplierStatus.Available)
            return _resolveURL + "/images/available.png";
        else if (_status == WebReferenceSupplier.SupplierStatus.Inactive)
            return _resolveURL + "/images/innactive.png";
        else if (_status == WebReferenceSupplier.SupplierStatus.Candidate)
            return _resolveURL + "/images/candidate.png";
        else if (_status == WebReferenceSupplier.SupplierStatus.Unavailable)
            return _resolveURL + "/images/unavailable.png";

        return string.Empty;
    }
    public static string EncodeJsString(string s)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("\"");
        foreach (char c in s)
        {
            switch (c)
            {
                case '\"':
                    sb.Append("\\\"");
                    break;
                case '\'':
                    sb.Append("\\'");
                    break;
                case '\\':
                    sb.Append("\\\\");
                    break;
                case '\b':
                    sb.Append("\\b");
                    break;
                case '\f':
                    sb.Append("\\f");
                    break;
                case '\n':
                    sb.Append("\\n");
                    break;
                case '\r':
                    sb.Append("\\r");
                    break;
                case '\t':
                    sb.Append("\\t");
                    break;
                default:
                    int i = (int)c;
                    if (i < 32 || i > 127)
                    {
                        sb.AppendFormat("\\u{0:X04}", i);
                    }
                    else
                    {
                        sb.Append(c);
                    }
                    break;
            }
        }
        sb.Append("\"");

        return sb.ToString();
    }
    public static void SetDropDownList(DropDownList _ddl, string _value)
    {
        for (int i = 0; i < _ddl.Items.Count; i++)
        {
            if (_ddl.Items[i].Value == _value)
            {
                _ddl.SelectedIndex = i;
                return;
            }
        }
        _ddl.SelectedIndex = 0;
    }
    public static string GetEmailNotification(string UrlWebReference)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfString result = null;
        
        try
        {
            result = _site.GetChargingErrorUserEmail();// GetUserTimeZone(userManagement.GetGuid);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, (SiteSetting)HttpContext.Current.Session["Site"]);
            return string.Empty;
        }


        if (result.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;

        return result.Value;
    }

    public static string GetEmailNotification(Page page,string siteId)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(page,siteId);
        WebReferenceSite.ResultOfString result = null;
        
        try
        {
            result = _site.GetChargingErrorUserEmail();// GetUserTimeZone(userManagement.GetGuid);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, (SiteSetting)HttpContext.Current.Session["Site"]);
            return string.Empty;
        }


        if (result.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;

        return result.Value;
    }
    public static string GetCleanPhone(string phone)
    {
        for (int i = phone.Length - 1; i >= 0; i--)
        {
            if (char.IsDigit(phone[i]) == false)
            {
                phone = phone.Remove(i, 1);
            }
        }
        if (phone.Length == 11)
            phone = phone.Substring(1, phone.Length - 1);
        return phone;
    }
    public static string GetDisplayPhone(string num)
    {
        if (string.IsNullOrEmpty(num))
            return string.Empty;
        if (num.Length == 9)
            return setnum9(num);
        return num.Substring(0, 1) + " " + setnum9(num.Substring(1, 9));
    }
    private static string setnum9(string num)
    {
        string newnum = "(" + num.Substring(0, 3) + ") " + num.Substring(3, 3) + "-" + num.Substring(6, 3);
        return newnum;
    }
    public static bool SaveHeadings(string SupplierId, string[] HeadingId, string WebReferenceUrl, Guid UserId)
    {
        XElement requestXml = new XElement("SupplierExpertise");
        requestXml.Add(new XAttribute("SiteId", ""));
        requestXml.Add(new XAttribute("SupplierId", SupplierId));
        foreach (string _id in HeadingId)
        {
            requestXml.Add(
                        new XElement("PrimaryExpertise",
                            new XAttribute("ID", _id),
                            new XAttribute("Certificate", false))
                            );
        }

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(WebReferenceUrl);

        string response = _supplier.CreateSupplierExpertise(
            new WebReferenceSupplier.CreateSupplierExpertiseRequest()
            {
                Request = requestXml.ToString(),
                IsFromAAR = false,
                UserId = UserId
            });

        //evaluate response:
        if (response == "<SupplierExpertise><Error>Failed</Error></SupplierExpertise>")
        {
            /*
            Console.WriteLine("Failed to create headings");
            var existing = crmContext.accounts.FirstOrDefault(x => x.accountid == supplier.Id);
            if (existing != null)
            {
                var accExps = crmContext.new_accountexpertises.Where(x => x.new_accountid == supplier.Id);
                foreach (var acExp in accExps)
                {
                    crmContext.DeleteObject(acExp);
                }
                crmContext.DeleteObject(existing);
                crmContext.SaveChanges();
            }
             * */
            return false;
        }
        return true;
    }
    public static List<string> GetCheckStatusUSPhoneRegularJavaScript()
    {
        List<string> list = new List<string>();
        list.Add(Utilities.RegularExpressionForJavascript(@"^[+]?[01]?[- .]?[(]?$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^[+]?[01]?[- .]?[(]?[2-9]$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^[+]?[01]?[- .]?[(]?[2-9]\d{1,2}[)]?[- .]?$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^[+]?[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{1,3}[- .]?$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^[+]?[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{1,3}$"));
        return list;
    }
    public static string CheckStatusUSPhoneJavaScript()
    {
         List<string> list = GetCheckStatusUSPhoneRegularJavaScript();
        StringBuilder sb = new StringBuilder();
        sb.Append("var reg_exp_arr = new Array();");
        sb.Append("function init_reg_phone() {");
        foreach(string str in list)
        {
            sb.Append(@"reg_exp_arr.push(new RegExp(""" + str + @"""));");
        }
        sb.Append("}");
        sb.Append(@"function checkPhoneStatus(_phone) {");
        sb.Append(@"_phone = _phone.RemoveSpace();");
        sb.Append(@"if (_phone.length == 0)");
        sb.Append(@"return ""empty"";");
        sb.Append(@"if (CheckPhone(_phone))");
        sb.Append(@"return ""match"";");
        sb.Append(@"for (var i = 0; i < reg_exp_arr.length; i++) {");
        sb.Append(@"if (reg_exp_arr[i].test(_phone))");
        sb.Append(@"return ""OnTheWay"";  }");
        sb.Append(@"return ""faild"";}");

     //   bool IsExplorer8 = HttpContext.Current.Request
    //    bool IsOldExplorer = (HttpContext.Current.Request.Browser.Browser == "IE" && HttpContext.Current.Request.Browser.MajorVersion < 9);
    //    if (IsOldExplorer)
        sb.Append(@"if(window.attachEvent)");
        sb.Append(@"window.attachEvent('onload', init_reg_phone);");
   //     else
        sb.Append(@"else if(window.addEventListener)");
        sb.Append(@"window.addEventListener('load', init_reg_phone, false);");
    
        return sb.ToString();   
    
    }
    static List<string> GetCheckStatusEmailRegularJavaScript()
    {
        List<string> list = new List<string>();
        list.Add(Utilities.RegularExpressionForJavascript(@"^\w+[-+.']?$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^\w+([-+.']\w+)*[-+.']?$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^\w+([-+.']\w+)*@$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^\w+([-+.']\w+)*@\w+[-.]?$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*[-.]?$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{1,2}$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,}[-.]?$"));
        list.Add(Utilities.RegularExpressionForJavascript(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,}([-.]\w+)*[-.]?$"));
        return list;
    }
    public static string CheckStatusEmailJavaScript()
    {
        List<string> list = GetCheckStatusEmailRegularJavaScript();
        StringBuilder sb = new StringBuilder();
        sb.Append("var reg_exp_arr_email = new Array();");
        sb.Append("function init_reg_email() {");
        foreach (string str in list)
        {
            sb.Append(@"reg_exp_arr_email.push(new RegExp(""" + str + @"""));");
        }
        sb.Append("}");
        sb.Append(@"function checkEmailStatus(_email) {");
        sb.Append(@"_email = $.trim(_email);");
        sb.Append(@"if (_email.length == 0)");
        sb.Append(@"return ""empty"";");
        sb.Append(@"if (CheckEmail(_email))");
        sb.Append(@"return ""match"";");
        sb.Append(@"for (var i = 0; i < reg_exp_arr_email.length; i++) {");
        sb.Append(@"if (reg_exp_arr_email[i].test(_email))");
        sb.Append(@"return ""OnTheWay"";  }");
        sb.Append(@"return ""faild"";}");

     //   bool IsOldExplorer = (HttpContext.Current.Request.Browser.Browser == "IE" && HttpContext.Current.Request.Browser.MajorVersion < 9);
    //    if (IsOldExplorer)
        sb.Append(@"if(window.attachEvent)");
        sb.Append(@"window.attachEvent('onload', init_reg_email);");
  //      else
        sb.Append(@"else if(window.addEventListener)");
        sb.Append(@"window.addEventListener('load', init_reg_email, false);");

        return sb.ToString();

    }
    public static XDocument GetXmlSiteText()
    {
        string path = HttpContext.Current.Request.PhysicalApplicationPath + @"\PPC\SentenceData.xml";
        XDocument xdoc = XDocument.Load(path);
        return xdoc;
    }
    public static string GetGeneralServerMissing(XDocument xdoc, out string _error)
    {
        string _missing = xdoc.Element("Data").Element("GeneralFields").Element("GeneralValidated").Element("Validated").Element("Instruction").Element("Missing").Value;
        _error = xdoc.Element("Data").Element("GeneralFields").Element("GeneralValidated").Element("Validated").Element("Error").Value;
        return _missing;
    }
    public static decimal GetBalance(Guid _id, string WebReferencePath)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(WebReferencePath);
        WebReferenceSupplier.GetSupplierChargingDataRequest request = new WebReferenceSupplier.GetSupplierChargingDataRequest();
        request.SupplierId = _id;
        WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result = null;
        try
        {
            result = _supplier.GetSupplierChargingData(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, (SiteSetting)HttpContext.Current.Session["Site"]);
            return 0;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return 0;

        return result.Value.CurrentBalance;

    }
    public static string GetWibiyaScrambel
    {
        get { return SCRAMBEL; }
    }
    public static bool AffiliateWibiyaLogin(PageSetting _page, string operation, string token)//, string _operator)
    {
        HttpRequest Request = HttpContext.Current.Request;
        if (Request.Cookies["AffiliateUser"] != null)
        {
            string _user = Request.Cookies["AffiliateUser"].Value;
            try
            {
                _user = EncryptString.Decrypt_String(_user, SCRAMBEL);
            }
            catch (Exception exc) 
            {
                return false;
            }
            string[] newPassword = _user.Split(';');
            if (newPassword.Length == 2)
            {
                switch(AffiliateWibiyaLogin(_page, newPassword[0], newPassword[1], operation, token))
                {
                    case(eWibiyaLoginStatus.AccountNotExist):
                    case(eWibiyaLoginStatus.LoginFaild):
                        return false;
                    case(eWibiyaLoginStatus.LoginSuccess):                       
                        return true;
                }
            }
           
        }
        return false;
    }
    public static eWibiyaLoginStatus AffiliateWibiyaLogin(PageSetting _page, string email, string password, string operation, string token)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_page);
        WebReferenceSite.ResultOfWibiyaLoginResponse result = null;
        WebReferenceSite.WibiyaLoginRequest _request = new WebReferenceSite.WibiyaLoginRequest();
        _request.Email = email;
        _request.Password = password;
        _request.SiteId = _page.siteSetting.GetSiteID;
        _request.UpdateXmlInWibiya = (operation == "add");
        _request.WibiyaToken = token;
        try
        {
            result = _site.WibiyaLogin(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, _page.siteSetting);
            return eWibiyaLoginStatus.LoginFaild;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return eWibiyaLoginStatus.LoginFaild;
        if (Guid.Empty == result.Value.OriginId)
            return eWibiyaLoginStatus.AccountNotExist;
        UserMangement um = new UserMangement(result.Value.FirstName, result.Value.OriginId);
        _page.userManagement = um;
        um.SetUserObject(_page);
        return eWibiyaLoginStatus.LoginSuccess;
    }
    public static string GetDateTimeView(DateTime date, string DateFormat, string TimeFormat)
    {
        return string.Format(DateFormat, date) + ", " + string.Format(TimeFormat, date);
    }
    public static LiteralControl GetDivLoader()
    {
        
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<div id=""divLoader"" class=""divLoader"">");
        sb.Append(@"<table><tr><td><img src=""../UpdateProgress/ajax-loader.gif"" alt=""Loading..."" /></td>");
        sb.Append(@"</tr></table></div>");
        LiteralControl lc = new LiteralControl(sb.ToString());
        return lc;
    }
    /*
    public static int WidgetRemovalLog(Guid OriginId, string HeadingCode, string Keyword, string ControlName, string Url, string WebReferenceUrl, WebReferenceCustomer.WidgetRemovalLogType LogType, string duration, MongoDB.Bson.ObjectId ExposureId)
    {
        
        Guid ExpertiseId = PpcSite.GetCurrent().GetHeadinGuidByCode(HeadingCode);
        string command = "EXEC [dbo].[InsertWidgetRemove] @OriginId, @Keyword, @ExpertiseId, @Url, @RemoveType, @RemovePeriod, @ControlName, @ExposureId";
        int result = -1;
        using(SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@OriginId", OriginId);
            if(string.IsNullOrEmpty(Keyword))
                cmd.Parameters.AddWithValue("@Keyword", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@Keyword", Keyword);
            cmd.Parameters.AddWithValue("@ExpertiseId", ExpertiseId);
            if(string.IsNullOrEmpty(Url))
                cmd.Parameters.AddWithValue("@Url", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@Url", Url);
            cmd.Parameters.AddWithValue("@RemoveType", LogType.ToString());
            cmd.Parameters.AddWithValue("@RemovePeriod", duration);
            cmd.Parameters.AddWithValue("@ControlName", ControlName);
            cmd.Parameters.AddWithValue("@ExposureId", ExposureId);
            result = (int)cmd.ExecuteScalar();
            conn.Close();
        }
        return result;
        
    }
    */
    public static string GetDateBefore(DateTime date)
    {
        string result;
        TimeSpan ts = DateTime.UtcNow - date;
        int _minutes = (int)ts.TotalMinutes;
        if (_minutes < 0)
            _minutes = 0;
        int _hours = (int)ts.TotalHours;
        if (_hours == 0)
            result = _minutes + " " + ((_minutes == 1) ? "minute" : "minutes") + " ago";
        else if (_hours < 24)
            result = _hours + " " + ((_hours == 1) ? "hour" : "hours") + " ago";
        else
            result = String.Format("{0:MMMM d, yyyy}", date);
        return result;
    }

    public static string GetTimePassed(DateTime Createon, DateTime CreatedOnSupplierTime)
    {
        string result="";
        TimeSpan ts = DateTime.UtcNow - Createon;
        int _minutes = (int)ts.TotalMinutes;       
        string strCreatedOnSupplierTime=CreatedOnSupplierTime.ToString("hh:mm tt");

        if (_minutes <= 0)
            result = strCreatedOnSupplierTime;
        else if (_minutes <= 59)
            result = _minutes + " min ago, " + strCreatedOnSupplierTime;
        else
        {
            int _hours = (int)ts.TotalHours;

            if(_hours<=24)
                result = _hours + " hrs ago, " + strCreatedOnSupplierTime;
            else if(_hours<48)
                result = "Yesterday, " + strCreatedOnSupplierTime;
            else
                result = CreatedOnSupplierTime.Day + " " + CreatedOnSupplierTime.ToString("MMMM")  + ", " + strCreatedOnSupplierTime;
        }       
     
        return result;
    }

    public static bool ifVowel(string str)
    {
        if (string.IsNullOrEmpty(str))
            return false;
        if (str.Length > 1)
            str = str.Substring(0, 1);

        string[] arrVowels = new string[] { "A", "E", "I", "O","U" };

        for (int i = 0; i < arrVowels.Length;i++ )
        {
            if (arrVowels[i] == str.ToUpper())
                return true;  
        }

        return false;
    }
    public static bool ifVowel(char c)
    {
        string arrVowels = "AEIOUaeiou";
        foreach(char ch in arrVowels)
        {
            if (ch == c)
                return true;
        }
        return false;
    }
    public static bool PublisherLogin(PageSetting ps, string user, string password)
    {
        if (string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
        {
            string host = ps.Request.ServerVariables["SERVER_NAME"];
            SiteSetting ss = DBConnection.LoadSiteId(host);
            if (ss == null)
                return false;
            ps.siteSetting = ss;
            ps.siteSetting.SetSiteSetting(ps);
        }
        int securityLevel;
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;
        try
        {
            _response = supplier.UserLogin(user, password);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, ps.siteSetting);
            return false;
        }
        if (_response.Type != WebReferenceSupplier.eResultType.Success)
            return false;
        if (_response.Value.UserLoginStatus == WebReferenceSupplier.eUserLoginStatus.NotFound ||
            _response.Value.AffiliateOrigin != Guid.Empty ||
            _response.Value.SecurityLevel < 1)
        {
            DBConnection.InsertLogin(user, Guid.Empty, ps.siteSetting.GetSiteID, eUserType.Publisher);
            return false;
        }
       
        securityLevel = _response.Value.SecurityLevel;

        DBConnection.InsertLogin(user, _response.Value.UserId, ps.siteSetting.GetSiteID, eUserType.Publisher);
        //       InsertLogin(email, _response.Value.UserId);
        ps.userManagement = new UserMangement(_response.Value.UserId.ToString(),
            securityLevel.ToString(), _response.Value.Name);
        ps.userManagement.SetUserObject(ps);

        int sitelangid = DBConnection.GetSiteLangIdByUserId(new Guid(ps.userManagement.GetGuid), ps.siteSetting.GetSiteID);
        bool IsDashboardHomePage = DBConnection.IsDashboardHomePage(new Guid(ps.userManagement.GetGuid), ps.siteSetting.GetSiteID);


        if (sitelangid > 0)
        {
            ps.siteSetting.siteLangId = sitelangid;
            ps.Response.Cookies["language"].Value = sitelangid.ToString();
            ps.Response.Cookies["language"].Expires = DateTime.Now.AddMonths(3);
        }


        string command = "EXEC dbo.GetPagesSecurityBySecurityLevel @SecurityLevel, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SecurityLevel", ps.userManagement.GetSecurityLevel);
            cmd.Parameters.AddWithValue("@SiteNameId", ps.siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string pageName = (!reader.IsDBNull(0)) ? (string)reader["PageName"] :
                    (string)reader["ControlId"];
                if (!ps.PageSecurity.ContainsKey(pageName))
                    ps.PageSecurity.Add(pageName,
                        (bool)reader["IsConfirm"]);
            }
            ps.SetPageSecurity = ps.PageSecurity;
            ps.LinkToSet.Clear();

            ps.LinkToSet = DBConnection.GetLinksToSet(ps.siteSetting.GetSiteID, ps.userManagement.GetSecurityLevel);

            ps.SaveLinkToSet();
            conn.Close();
        }
        return true;
    }
    public static bool IsPublisher(HttpRequest request, string user, string password, out SiteSetting ss)
    {

        string host = request.ServerVariables["SERVER_NAME"];
        ss = DBConnection.LoadSiteId(host);
        if (ss == null)
            return false;
            
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, ss.GetSiteID);
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;
        try
        {
            _response = supplier.UserLogin(user, password);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, ss);
            return false;
        }
        if (_response.Type != WebReferenceSupplier.eResultType.Success)
            return false;
        if (_response.Value.UserLoginStatus == WebReferenceSupplier.eUserLoginStatus.NotFound ||
            _response.Value.AffiliateOrigin != Guid.Empty ||
            _response.Value.SecurityLevel < 1)
        {
            return false;
        }

        return true;
    }
    public static byte[] GetTwillo(string UrlPath)
    {        
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(UrlPath);

        webRequest.Method = "GET";

        WebResponse webResponse = webRequest.GetResponse();
        Stream webStream = webResponse.GetResponseStream();
        byte[] buffer = new byte[16 * 1024];
        using (MemoryStream ms = new MemoryStream())
        {
            int read;
            while ((read = webStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            return ms.ToArray();
        }
        
    }
    public static NameValueCollection GetChargingCompanyDetails(SiteSetting siteSetting, string SchemeDomainPath)
    {
        string serverName = System.Web.HttpContext.Current.Request.Url.Host;
        string chargingCompany = siteSetting.CharchingCompany.ToString();
        NameValueCollection nvc = new NameValueCollection();

        string command = "EXEC dbo.GetCompanyChargingDetails @company";

        using (SqlConnection conn = DBConnection.GetConnString())
        {
            if (!String.IsNullOrEmpty(chargingCompany))
            {
                

                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@company", siteSetting.CharchingCompany.ToString());
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    string _value = ((reader["Value"] == DBNull.Value) ? "" : (string)reader["Value"]);
                    if (!string.IsNullOrEmpty(_value) && (string)reader["Type"] == eChargingKeyValueType.URL.ToString())
                        _value = SchemeDomainPath + _value;
                    nvc.Add((string)reader["Key"], _value);
                    
                }


                reader.Close();
                conn.Close();

            }
        }

        return nvc;

    }

    public static string setfirstCapital(string str)
    {
        if (!String.IsNullOrEmpty(str))
            return str.Substring(0, 1).ToUpper() + str.Substring(1);
        else
            return str;
    }


    public static string formatTime(int hour)
    {
        string tempHour = "";

       
        if (hour >= 12 && hour < 24)
            tempHour = hour + "pm";
        else
            tempHour = hour + "am";

        return tempHour;
    }

    public static string  shortHour(int hour)
    {
        string tempHour="";

        if (hour.GetType() == typeof(int) && hour >= 0 && hour<=24)
        {           

            if (hour == 24)
                tempHour = "Midnight";
            else if (hour == 23)
                tempHour = "11pm";
            else if (hour == 22)
                tempHour = "10pm";
            else if (hour == 21)
                tempHour = "9pm";
            else if (hour == 20)
                tempHour = "8pm";
            else if (hour == 19)
                tempHour = "7pm";
            else if (hour == 18)
                tempHour = "6pm";
            else if (hour == 17)
                tempHour = "5pm";
            else if (hour == 16)
                tempHour = "4pm";
            else if (hour == 15)
                tempHour = "3pm";
            else if (hour == 14)
                tempHour = "2pm";
            else if (hour == 13)
                tempHour = "1pm";
            else if (hour == 12)
                tempHour = "12pm";
            else if (hour == 0)
                tempHour = "12am";
            else
                tempHour = hour + "am";
        }

        else
        {
            tempHour = "invalid number";
        }

        return tempHour;
       
    }



    public static string shortHour2(int hour, int minutes)
    {
        string tempTime = "";

        if (hour.GetType() == typeof(int) && hour >= 0 && hour <= 24)
        {
            string strMinutes="";

            if (minutes == 0)
                strMinutes = "00";
            else
                strMinutes = minutes.ToString();

            if (hour == 24)
                tempTime = "Midnight";
            else if (hour == 23)
                tempTime = "11:" + strMinutes + "pm";
            else if (hour == 22)
                tempTime = "10:" + strMinutes + "pm";
            else if (hour == 21)
                tempTime = "9:" + strMinutes + "pm";
            else if (hour == 20)
                tempTime = "8:" + strMinutes + "pm";
            else if (hour == 19)
                tempTime = "7:" + strMinutes + "pm";
            else if (hour == 18)
                tempTime = "6:" + strMinutes + "pm";
            else if (hour == 17)
                tempTime = "5:" + strMinutes + "pm";
            else if (hour == 16)
                tempTime = "4:" + strMinutes + "pm";
            else if (hour == 15)
                tempTime = "3:" + strMinutes + "pm";
            else if (hour == 14)
                tempTime = "2:" + strMinutes + "pm";
            else if (hour == 13)
                tempTime = "1:" + strMinutes + "pm";
            else if (hour == 12)
                tempTime = "12:" + strMinutes + "pm";
            else if (hour == 0)
                tempTime = "12:" + strMinutes + "am";
            else
                tempTime = hour + ":" + strMinutes + "am";
        }

        else
        {
            tempTime = "invalid number";
        }

        return tempTime;

    }

    public static string formatTime2(int fromHour, int toHour)
    {
        string tempHour = "";
        string tempFromHour;
        string tempToHour;

        /*
         1. Hours format bug:
        - Correct use of am pm (17pm = 5pm)
        - 0am = 12am, 0am - 24pm = All day
        - 24 = Midnight
        */

        if (fromHour == 0 && toHour == 24)
            tempHour = "All day";
        else
        {
            tempFromHour = shortHour(fromHour);
            tempToHour = shortHour(toHour);
            tempHour = tempFromHour + " - " + tempToHour;
        }       

        return tempHour;
    }


    public static string formatTime3(int fromHour, int toHour, int fromMinute, int toMinute)
    {
        string tempHour = "";
        string tempFromHour;
        string tempToHour;

        /*
         1. Hours format bug:
        - Correct use of am pm (17pm = 5pm)
        - 0am = 12am, 0am - 24pm = All day
        - 24 = Midnight
        */

        if (fromHour == 0 && toHour == 24)
            tempHour = "All day";
        else
        {
            tempFromHour = shortHour2(fromHour, fromMinute);
            tempToHour = shortHour2(toHour,toMinute);
            tempHour = tempFromHour + " - " + tempToHour;
        }

        return tempHour;
    }

    public static string cleanSpecialCharsUrlFreindly(string param)
    {
       string tempParam = "";

       if (!string.IsNullOrEmpty(param))
       {
            tempParam = param.Replace("&", "and");
       }

       return tempParam;
    }

    public static string cleanSpecialCharsUrlFreindly2(string param)
    {
        string tempParam = "";

        if (!string.IsNullOrEmpty(param))
        {
            tempParam = param.Replace("&", "and");
            tempParam = tempParam.Replace("/", " or ");           
        }

        return tempParam;
    }

    public static string cleanSpecialCharsUrlFreindly3(string param)
    {
        string tempParam = "";

        if (!string.IsNullOrEmpty(param))
        {
            tempParam = param.Replace("&", "");
            tempParam = tempParam.Replace("/", "");
            tempParam = tempParam.Replace("\"", "");
            tempParam = tempParam.Replace("\'", "");
            tempParam = tempParam.Replace(".", "");
            tempParam = tempParam.Replace("+", "");
        }

        return tempParam;
    }


    public static string americanFormatPhone(string phone)
    {
        string tempPhone;
        if (phone.Length == 10)
            tempPhone = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6, 4);
        else
            tempPhone = phone;

        return tempPhone;

    }


    public static string daysShortcuts(string longDay)
    {
        string shortDay="";

        if (longDay.ToUpper() == "MONDAY")
            shortDay = "Mon";
        else if (longDay.ToUpper() == "TUESDAY")
            shortDay = "Tue";
        else if (longDay.ToUpper() == "WEDNESDAY")
            shortDay = "Wed";
        else if (longDay.ToUpper() == "THURSDAY")
            shortDay = "Thu";
        else if (longDay.ToUpper() == "FRIDAY")
            shortDay = "Fri";
        else if (longDay.ToUpper() == "SATURDAY")
            shortDay = "Sat";
        else if (longDay.ToUpper() == "SUNDAY")
            shortDay = "Sun";

        return shortDay;
    }



    public static List<T> getRandomList<T>(List<T> list, int cntFromList)
    {
        Random rnd = new Random();
        int length = list.Count;
        int rndNumber;

        List<T> rndTemp = new List<T>(list);       

        List<T> rndList = new List<T>();
        for (int i = 0; i < cntFromList; i++)
        {
            rndNumber = rnd.Next(0, rndTemp.Count - 1);
            rndList.Add(rndTemp[rndNumber]);
            rndTemp.RemoveAt(rndNumber);
           
        }

        return rndList;

    }

   

    public static List<string> getExcludedList(List<string> list, string listValue)
    {        

        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == listValue)
                list.RemoveAt(i);
        }

        return list;

    }
    public static string ConvertToStringNonCientificNotation(double d)
    {
        return d.ToString("0.################################################################");
    }
    public static string ConvertToStringNonCientificNotation2digit(double d)
    {
        if (d > 1)
            return d.ToString("0.##");
        string result = ConvertToStringNonCientificNotation(d);
        string[] strs = result.Split('.');
        if (strs.Length > 2)
            return result;
        bool IsFindOne = false;
        string AfterPoint="";
        for (int i = 0; i < strs[1].Length; i++)
        {
            if(IsFindOne && strs[1][i] == '0')
                return strs[0] +"." + AfterPoint;
            AfterPoint += strs[1][i];
            if(IsFindOne)
                return strs[0] + "." + AfterPoint;
            if (strs[1][i] == '0')            
                continue;
            IsFindOne = true;  
            
        }
        return strs[0];        
    }

    public static System.Web.UI.HtmlControls.HtmlGenericControl setLinks(SliderData sd)
    {
        var srcJquery = sd.ClientScheme + "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js";
        //var srcJquery = sd.ClientScheme + "://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js";

        System.Web.UI.HtmlControls.HtmlGenericControl si = new System.Web.UI.HtmlControls.HtmlGenericControl();

        si.TagName = "script";
        si.Attributes.Add("type", "text/javascript");
        si.Attributes.Add("src", srcJquery);

        return si;        

    }
    public static void SetUserPublisherLink(PageSetting _page)
    {
        string command = "EXEC dbo.GetPagesSecurityBySecurityLevel @SecurityLevel, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SecurityLevel", _page.userManagement.GetSecurityLevel);
            cmd.Parameters.AddWithValue("@SiteNameId", _page.siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string pageName = (!reader.IsDBNull(0)) ? (string)reader["PageName"] :
                    (string)reader["ControlId"];
                if (!_page.PageSecurity.ContainsKey(pageName))
                    _page.PageSecurity.Add(pageName,
                        (bool)reader["IsConfirm"]);
            }
            _page.SetPageSecurity = _page.PageSecurity;
            _page.LinkToSet.Clear();            
            _page.LinkToSet = DBConnection.GetLinksToSet(_page.siteSetting.GetSiteID, _page.userManagement.GetSecurityLevel);

            _page.SaveLinkToSet();
            conn.Close();
        }
    }
    public static string GenerateColor(Random random)
    {
        string letters = "0123456789ABCDEF";
        string color = "#";
      //  Random random = new Random();
        for (var i = 0; i < 6; i++ ) {
            color += letters[random.Next(letters.Length)];
        }
        return color;
    }
    public static string GenerateColor()
    {
        return GenerateColor(new Random());
    }

    public static int GetMaxValue(ArrayList arrList)
    {
        ArrayList copyList = new ArrayList(arrList);

        if (copyList == null || copyList.Count == 0)
        {
            return 0;
        }

        else
        {
            copyList.Sort();

            copyList.Reverse();

            return Convert.ToInt32(copyList[0]);
        }

    }

    public static string addZeroToOneDigit(string number)
    {
        if (number.Length == 1)
            return "0" + number;
        else
            return number.ToString();
    }
    public static string GetEnumStringFormat(Enum value)
    {
        string str = value.ToString();
        str = str.Replace("__", "/");
        str = str.Replace("_", " ");
        return str;
    }
    public static String GetIP(HttpRequest request)
    {
        String ip =
            request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (string.IsNullOrEmpty(ip))
        {
            ip = request.ServerVariables["REMOTE_ADDR"];
        }

        return ip;
    }
    public static string GetHostName(HttpRequest request)
    {
        return PpcSite.GetCurrent().IsLocalMachine ? request.Url.Authority : request.Url.DnsSafeHost;
    }
    public static Dictionary<Guid, string> GetCategories(string WebReferenceUrl, string siteId)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(WebReferenceUrl);
        string result = string.Empty;
        try
        {
            result = _site.GetPrimaryExpertise(siteId, "");
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteId);
            return null;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            return null;
        }
        Dictionary<Guid, string> dic = new Dictionary<Guid, string>();
        
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {
            string pName = nodePrimary.Attribute("Name").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            dic.Add(new Guid(_guid), pName);
        }
        return dic;
    }
   

    /*
    public static WebReferenceSupplier.ResultOfDeactivateSupplierResponse DeactivateSupplier(Guid accountId, Guid ReasonId, PageSetting _page)
    {

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(_page);
        WebReferenceSupplier.ResultOfDeactivateSupplierResponse result = null;
        try
        {
            result = _supplier.DeactivateSupplier(accountId, ReasonId, _page.userManagement.Get_Guid);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, _page.siteSetting.GetSiteID);            
            return null;
        }
        return result;
    }
     */
    
}
