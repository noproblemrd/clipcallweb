﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Result
/// </summary>
public class ResultCharging
{   
    
    private Guid pricingId=Guid.Empty;
    private string accountNumber="";

    private eResult result = eResult.unsuccess;
    public enum eResult { success, unsuccess };

    private eDepositStatusResponse depositStatus = eDepositStatusResponse.OK;
    

  
    private string msg = "";
    public string SalesPerson { get; set; }
    public int Amount { get; set; }
    public ResultCharging()
	{
        
		//
		
		//
	}
    

    public Guid PricingId
    {
        get
        {
            return pricingId;
        }

        set
        {
            pricingId = value;
        }
    }

    public string AccountNumber
    {
        get
        {
            return accountNumber;
        }

        set
        {
            accountNumber = value;
        }
    }
    public string _result
    {
        get { return result.ToString(); }
    }
    public eResult Result
    {
        get
        {
            return result;
        }

        set
        {
            result = value;
        }
    }

    public string  Msg
    {
        get
        {
            return msg;
        }

        set
        {
            msg = value;
        }
    }

    public eDepositStatusResponse DepositStatus
    {
        get
        {
            return depositStatus;
        }

        set
        {
            depositStatus = value;
        }
    }
   



}
