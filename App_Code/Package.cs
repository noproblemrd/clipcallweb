﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Package
/// </summary>
/// 
[System.Serializable]
public class Package
{
    private int _FromAmount;
    private int _BonusPercent;
    private string _Name;
    private string _SupplerType;
    private decimal _total;
    
	public Package()
	{
		//
		
		//
	}
    public int BonusAmount
    {
        get { return (_FromAmount * _BonusPercent) / 100; }
    }
    public int FromAmount
    {
        get {return _FromAmount;}
        set { _FromAmount = value; }
    }

    public int BonusPercent
    {
        get {return _BonusPercent;}
        set { _BonusPercent = value; }
    }

    public string Name
    {
        get {return _Name;}
        set { _Name = value; }
    }

    public string SupplerType
    {
        get { return _SupplerType; }
        set { _SupplerType = value; }
    }

    public decimal Total
    {
        get { return _total; }
        set { _total = value; }
    }
    public static List<Package> GetPackages(string UrlWebService, Guid SupplierId)
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(UrlWebService);
        WebReferenceSupplier.GetSupplierChargingDataRequest request = new WebReferenceSupplier.GetSupplierChargingDataRequest();
        request.SupplierId = SupplierId;

        WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result;
        try
        {
            result = supplier.GetSupplierChargingData(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, UrlWebService);
            return null;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return null;
        List<Package> list = new List<Package>();
        foreach (WebReferenceSupplier.PricingPackageData _package in result.Value.Packages)
        {
            Package pack = new Package();
            pack._BonusPercent = _package.BonusPercent;
            pack._FromAmount = _package.FromAmount;
            pack._Name = _package.Name.ToLower();
            pack._SupplerType = _package.SupplierType.ToString();
            //pack._total=_package.t
            list.Add(pack);
        }
        return list;
    }
    public Package(EventArgsBuyCredit e)
    {
        _FromAmount = e.Amount;
        _BonusPercent = e.BonusPercent;
        _SupplerType = e.type;
        _total = e.Amount;
    }
}