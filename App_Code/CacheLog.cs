﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.IO;
using System.Text;
using System.Configuration;
using System.Threading;

/// <summary>
/// Summary description for CacheLog
/// </summary>
[Serializable()]
public class CacheLog
{
    static object locker = new object();
    static object InjectionLocker = new object();
    static string PATH;
    static string PATH_INJECTION;
    string path;
    string pathInjection;
	
    static CacheLog()
	{
        PATH = ConfigurationManager.AppSettings["CacheLog"];
        PATH_INJECTION = PATH;
        PATH += "Cache_" + Environment.MachineName + "_";// +GetDateFileFormat(DateTime.Now) + ".log";
        PATH_INJECTION += "InjectionLog" + Environment.MachineName + "_";// +GetDateFileFormat(DateTime.Now) + ".log";

	}
    public CacheLog() 
    {
        path = PATH + GetDateFileFormat(DateTime.Now) + ".log";
        pathInjection = PATH_INJECTION + GetDateFileFormat(DateTime.Now) + ".log";
    }
    public static void _StartUpdateCache(Guid UserId, string userName)
    {
        CacheLog cl = new CacheLog();
        cl.StartUpdateCache(UserId, userName);
    }
    public void StartUpdateCache(Guid UserId, string userName)
    {
        try
        {
            lock (locker)
            {
                using (FileStream fs = new FileStream(path, FileMode.Append))
                {
                    using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        tw.WriteLine();
                        tw.WriteLine();
                        tw.WriteLine("Update start: " + DateTime.Now.ToString());
                        tw.WriteLine("User: " + userName + ", " + UserId.ToString());
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    public static void _StartPublishInjection(Guid UserId, string userName)
    {
        CacheLog cl = new CacheLog();
        cl.StartPublishInjection(UserId, userName);
    }
    public void StartPublishInjection(Guid UserId, string userName)
    {
        try
        {
            lock (locker)
            {
                using (FileStream fs = new FileStream(pathInjection, FileMode.Append))
                {
                    using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        tw.WriteLine();
                        tw.WriteLine();
                        tw.WriteLine("Publish Injection: " + DateTime.Now.ToString());
                        tw.WriteLine("User: " + userName + ", " + UserId.ToString());
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    public void UpdateSuccess()
    {
        try
        {
            lock (locker)
            {
                using (FileStream fs = new FileStream(path, FileMode.Append))
                {
                    using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        tw.WriteLine("Load cache success!!!");
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    public void UpdateFaild()
    {
        try
        {
            lock (locker)
            {
                using (FileStream fs = new FileStream(path, FileMode.Append))
                {
                    using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        tw.WriteLine("Update faild!!!");
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    public void WriteCacheLog(string Message)
    {
        WriteCacheLog(null, new string[] { Message }, true);
    }
    public void WriteCacheLog(Exception exc)
    {
        WriteCacheLog(exc, null, true);
    }
    public void WriteCacheLog(Exception exc, string[] Messages, bool SendMail)
    {
        
        //        System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"]);
        
        
        try
        {
            lock (locker)            
            {
                using (FileStream fs = new FileStream(path, FileMode.Append))
                {
                    using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Write at: " + DateTime.Now.ToString());
                        if (Messages != null)
                        {
                            foreach (string str in Messages)
                            {
                                sb.AppendLine(str);
                            }
                        }
                        int indx = 0;
                        Exception _ex = exc;
                        while (_ex != null)
                        {
                            WriteException(_ex, sb, indx);
                            indx++;
                            _ex = _ex.InnerException;
                        }

                        sb.AppendLine();
                        tw.Write(sb);
                        if (SendMail)
                            SendMailMessaage(sb);
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    void WriteException(Exception ex, StringBuilder sb, int indx)
    {
        if (indx > 0)
        {
            sb.AppendLine("Inner Exception: " + indx);
        }
        sb.AppendLine("HelpLink: " + ex.HelpLink);
        sb.AppendLine("Source: " + ex.Source);
        sb.AppendLine("StackTrace: " + ex.StackTrace);
        sb.AppendLine("TargetSite: " + ex.TargetSite);
        sb.AppendLine("Message: " + ex.Message);
        if (ex is PpcCacheException)
        {
            PpcCacheException pce = (PpcCacheException)ex;
            if (pce.NewValues != null)
            {
                sb.AppendLine("New Values:");
                foreach (KeyValuePair<string, string> kvp in pce.NewValues)
                    sb.AppendLine(kvp.Key + ": " + kvp.Value);                    
            }
            if (pce.OldValues != null)
            {
                sb.AppendLine("Old Values:");
                foreach (KeyValuePair<string, string> kvp in pce.OldValues)
                    sb.AppendLine(kvp.Key + ": " + kvp.Value);
            }
            
        }
    }
    private string GetDateFileFormat(DateTime dt)
    {
        return dt.Day + "-" + dt.Month + "-" + dt.Year;
    }
    public static void _ApplicationStart()
    {
        CacheLog cl = new CacheLog();
        cl.ApplicationStart();
    }
    public void ApplicationStart()
    {
        try
        {
            lock (locker)
            {
                using (FileStream fs = new FileStream(path, FileMode.Append))
                {
                    using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        tw.WriteLine("Application Start: " + DateTime.Now.ToString());
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    public static void WriteException(PpcCacheException exc)
    {
        CacheLog cl = new CacheLog();
        cl.WriteCacheLog(exc);
    }
    public void SendMailMessaage(StringBuilder sb)
    {
        MailSender _mail = new MailSender();
        _mail.Body = sb.ToString();
        _mail.CacheErrorMail();
        _mail.Send();
    }
    public void WriteInjectionLog(Guid UserId, string userName, StringBuilder sb)
    {
        try
        {
            lock (InjectionLocker)
            {
                using (FileStream fs = new FileStream(pathInjection, FileMode.Append))
                {
                    using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        tw.WriteLine();
                        tw.WriteLine();
                        tw.WriteLine("Injection pair saved: " + DateTime.Now.ToString());
                        tw.WriteLine("User: " + userName + ", " + UserId.ToString());
                        /*
                        string[] lines = sb.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                        foreach(string line in lines)
                            tw.WriteLine(line);
                         * */
                        tw.WriteLine(sb.ToString());
                        
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
}