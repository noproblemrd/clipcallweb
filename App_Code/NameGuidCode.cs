using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NameGuidCode
/// </summary>
public class NameGuidCode
{
    public string name;
    public Guid _guid;
    public string _code;
	public NameGuidCode(string name, Guid _guid, string _code)
	{
        this.name = name;
        this._guid = _guid;
        this._code = _code;
	}
}
