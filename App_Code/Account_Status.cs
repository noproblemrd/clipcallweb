﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Account_Status
/// </summary>
public class Account_Status
{
    string _Img_URL;
    string _status;
    WebReferenceSupplier.SupplierStatus _supplier_status;

	public Account_Status()
	{
		
		//
	}
    public string Img_URL { get; set; }
    public string Status { get; set; }
    public WebReferenceSupplier.SupplierStatus Supplier_status { get; set; }
}
