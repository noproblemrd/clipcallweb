﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NReco.VideoConverter;
using System.Configuration;

/// <summary>
/// Summary description for VideoConvertorManager
/// </summary>
public class VideoConvertorManager
{
    static readonly string VideoTempLocation;
    static readonly string VideoTempWebLocation;
    static VideoConvertorManager()
    {
        VideoTempWebLocation = ConfigurationManager.AppSettings["professionalRecordWeb"];
        VideoTempLocation = ConfigurationManager.AppSettings["professionalRecord"];
    }

    string url;
	public VideoConvertorManager(string url)
	{
        this.url = url;
	}
    public string GetVideoFilePath()
    {
        string path = VideoTempLocation + Guid.NewGuid() + ".mp4";
        var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
        ffMpeg.ExecutionTimeout = new TimeSpan(0,0,30);
   //     OutputSettings settings = new OutputSettings();
 //       ConvertSettings
 //       settings.
        try
        {
            ffMpeg.ConvertMedia(url, path, Format.mp4);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("VideoConvertorManager url: {0}", url));
            return null;
        }
        return path;
    }
    public byte[] GetVideoBytes()
    {
        string path = GetVideoFilePath();
        if (string.IsNullOrEmpty(path))
            return null;
        return System.IO.File.ReadAllBytes(path);
    }
}