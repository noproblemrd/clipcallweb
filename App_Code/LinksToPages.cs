﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for LinksToPages
/// </summary>
public class LinksToPages
{
//    string _MasterName; 
    string _url; 
    string _controlId;
    bool _Visibility;
    public LinksToPages( string url, string controlId, bool Visibility)
	{
        this._controlId = controlId;        
        this._url = url;
        this._Visibility = Visibility;
	}
//    public string MasterName { get { return _MasterName; } }
    public string url { get { return _url; } }
    public string controlId { get { return _controlId; } }
    public bool Visibility { get { return _Visibility; } }
}
