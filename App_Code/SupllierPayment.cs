﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SupllierPayment
/// </summary>
public class SupllierPayment
{
    private string _creditCard="";
    private string _creditCardCompany = "";
    private string _creditCardDateMmyy = "";
    private string _cvv2 = "";
   // private string _total = "";
    private string _discount = ""; // bonus
    private string  _discountType; // discount Type 
    private string _extraBonus = "";
    private string _extraReasonBonus = ""; 
    private string _extraReasonBonusId = "";    
    private string _currency = "";
    private string _cardId = "";
    private string _cardName = "";
    private string _cardAddress = "";    
    private string _myText = "";
    private string _name = "";
    private string _address = "";
    private string _city = "";
    private string _state = "";
    private string _zipCode = "";
    private string _tel1 = "";
    private string _tel2 = "";
    private string _cell = "";
    private string _fax = "";
    private string _email = "";
    private string _website = "";
    private string _comments = "";
    private string _tax = "";
    private string _id;
    private string _siteId;
    private int _siteLangId;    
    private string _guid;
    private bool _isRecharge=true;
    private bool _isAutoRenew;
    private string _deposit;
    private string _creditCardToken;
    private string _dateTimeFormat;
    private bool _ifAccountingCharging;
    private string _userId;    
    private string _userName;
    private string _balanceOld;   
    private string _balanceNew;
    private bool _isFirstTimeV;
    private string _fieldId;
    private string _fieldName;
    private string _transactionId;
    private string _chargingCompany;
    private string _chargingCompanyContractId;
    private string _chargingCompanyCustomerId;
    private string _ppcPackage;
    private int _rechargeAmount;
    private string _accountNumber;
  //  private string _paymentMethod;
    private string _bank="";
    private string _bankBranch="";
    private string _bankAccount = "";
    private int _numberPayments;
    private string _voucherNumber;
    private string _voucherReasonId;
    private string _checkNumber;
    private string _checkDate;   
    private string _transDate;
    private string _invoiceNumber;
    private string _shovar;
    private string _clientIp;
    private string _userAgent;
    private string _acceptLanguage;
    private string _hostIp;
    public WebReferenceSupplier.eSupplierPricingComponentType SupplierPricingComponentType { get; set; }

    public bool HasInvoiceNumberRefund
    {
        get;
        set;
    }
    public int Bonus { get; set; }
    public int Total { get; set; }
    public string total { get; set; }
    public int PaymentAmount { get; set; }
    public string SupplierBizId { get; set; }
    public string SalesPerson { get; set; }
    public int RechargeDayOfMonth { get; set; }
	public SupllierPayment()
	{
        HasInvoiceNumberRefund = false;
	}
    public int RechargeAmount
    {
        get { return _rechargeAmount; }
        set { _rechargeAmount = value; }
    }
    public string InvoiceNumber
    {
        get { return _invoiceNumber; }
        set { _invoiceNumber = value; }
    }

    public string creditCard
    {
        get
        {
            return _creditCard;
        }

        set
        {
            _creditCard=value;
        }

    }

    public string creditCardCompany
    {
        get
        {
            return _creditCardCompany;
        }

        set
        {
            _creditCardCompany = value;
        }

    }

    


    public string creditCardDateMmyy
    {
        get
        {
            return _creditCardDateMmyy;
        }

        set
        {
            _creditCardDateMmyy=value;
        }

    }

    public string cvv2
    {
        get
        {
            return _cvv2;
        }

        set
        {
            _cvv2=value;
        }
    }

   
    
    public string discount
    {
        get
        {
            return _discount;
        }

        set
        {
            _discount=value;
        }
    }

    public string discountType
    {
        get
        {
            return _discountType;
        }

        set
        {
            _discountType=value;
        }
    }

    
    public string extraBonus
    {
        get
        {
            return _extraBonus;
        }

        set
        {
            _extraBonus = value;
        }
    }

    public string extraReasonBonus
    {
        get
        {
            return _extraReasonBonus;
        }

        set
        {
            _extraReasonBonus = value;
        }
    }

    public string extraReasonBonusId
    {
        get
        {
            return _extraReasonBonusId;
        }

        set
        {
            _extraReasonBonusId = value;
        }
    }
    
   

    public string currency
    {
        get
        {
            return _currency;
        }

        set
        {
            _currency=value;
        }
    }
    
    public string cardId
    {
        get
        {
            return _cardId;
        }

        set
        {
            _cardId=value;
        }
    }

    public string cardName
    {
        get
        {
            return _cardName;
        }

        set
        {
            _cardName = value;
        }
    }

    public string cardAddress
    {
        get
        {
            return _cardAddress;
        }

        set
        {
            _cardAddress = value;
        }
    }
    

    public string myText
    {
        get
        {
            return _myText;
        }

        set
        {
            _myText=value;
        }
    }

    public string name
    {
        get
        {
            return _name;
        }

        set
        {
            _name=value;
        }
    }
    
    public string address
    {
        get
        {
            return _address;
        }

        set
        {
            _address=value;
        }
    }

    public string city
    {
        get
        {
            return _city;
        }

        set
        {
            _city=value;
        }
    }

    public string state
    {
        get
        {
            return _state;
        }

        set
        {
            _state=value;
        }
    }

    public string zipCode
    {
        get
        {
            return _zipCode;
        }

        set
        {
            _zipCode=value;
        }
    }

    public string tel1
    {
        get
        {
            return _tel1;
        }

        set
        {
            _tel1=value;
        }
    }
    
    public string tel2
    {
        get
        {
            return _tel2;
        }

        set
        {
            _tel2=value;
        }
    }
    
    public string cell
    {
        get
        {
            return _cell;
        }

        set
        {
            _cell=value;
        }
    }

    public string fax
    {
        get
        {
            return _fax;
        }

        set
        {
            _fax=value;
        }
    }

    public string email
    {
        get
        {
            return _email;
        }

        set
        {
            _email=value;
        }
    }
    
    public string website
    {
        get
        {
            return _website;
        }

        set
        {
            _website=value;
        }
    }
    
    public string comments
    {
        get
        {
            return _comments;
        }

        set
        {
            _comments=value;
        }
    }
    
    public string tax
    {
        get
        {
            return _tax;
        }

        set
        {
            _tax=value;
        }
    }

    public string id
    {
        get
        {
            return _id;
        }

        set
        {
            _id = value;
        }
    }

    public string siteId
    {
        get
        {
            return _siteId;
        }

        set
        {
            _siteId = value;
        }
    }

    public int siteLangId
    {
        get
        {
            return _siteLangId;
        }

        set
        {
            _siteLangId = value;
        }
    }

    public string guid
    {
        get
        {
            return _guid;
        }

        set
        {
            _guid = value;
        }
    }



    public bool isRecharge
    {
        get
        {
            return _isRecharge;
        }

        set
        {
            _isRecharge = value;
        }
    }

    public bool isAutoRenew
    {
        get
        {
            return _isAutoRenew;
        }

        set
        {
            _isAutoRenew = value;
        }
    }

    public string deposit
    {
        get
        {
            return _deposit;
        }

        set
        {
            _deposit = value;
        }
    }


    public string creditCardToken
    {
        get
        {
            return _creditCardToken;
        }

        set
        {
            _creditCardToken = value;
        }
    }

    public string dateTimeFormat
    {
        get
        {
            return _dateTimeFormat;
        }

        set
        {
            _dateTimeFormat = value;
        }
    }

    public bool ifAccountingCharging
    {
        get
        {
            return _ifAccountingCharging;
        }

        set
        {
            _ifAccountingCharging = value;
        }
    }



    public string userId
    {
        get
        {
            return _userId;
        }

        set
        {
            _userId = value;
        }
    }

    public string userName
    {
        get
        {
            return _userName;
        }

        set
        {
            _userName = value;
        }
    }

    public string balanceOld
    {
        get
        {
            return _balanceOld;
        }

        set
        {
            _balanceOld = value;
        }
    }

    public string balanceNew
    {
        get
        {
            return _balanceNew;
        }

        set
        {
            _balanceNew = value;
        }
    }

    public bool isFirstTimeV
    {
        get
        {
            return _isFirstTimeV;
        }

        set
        {
            _isFirstTimeV = value;
        }
    }

    public string fieldId
    {
        get
        {
            return _fieldId;
        }

        set
        {
            _fieldId = value;
        }
    }

    public string fieldName
    {
        get
        {
            return _fieldName;
        }

        set
        {
            _fieldName = value;
        }
    }

    public string transactionId
    {
        get
        {
            return _transactionId;
        }

        set
        {
            _transactionId = value;
        }
    }

    public string chargingCompany
    {
        get
        {
            return _chargingCompany;
        }

        set
        {
            _chargingCompany = value;
        }
    }

    public string chargingCompanyContractId
    {
        get
        {
            return _chargingCompanyContractId;
        }

        set
        {
            _chargingCompanyContractId = value;
        }
    }

    public string chargingCompanyCustomerId
    {
        get
        {
            return _chargingCompanyCustomerId;
        }

        set
        {
            _chargingCompanyCustomerId = value;
        }
    }   


    public string ppcPackage
    {
        get
        {
            return _ppcPackage;
        }

        set
        {
            _ppcPackage = value;
        }
    }

    public string accountNumber
    {
        get
        {
            return _accountNumber;
        }

        set
        {
            _accountNumber = value;
        }
    }
    /*
    public string paymentMethod
    {
        get
        {
            return _paymentMethod;
        }

        set
        {
            _paymentMethod = value;
        }
    }
     * */
    public WebReferenceSupplier.ePaymentMethod paymentMethod { get; set; }
    public string bank
    {
        get
        {
            return _bank;
        }

        set
        {
            _bank = value;
        }
    }

    public string bankBranch
    {
        get
        {
            return _bankBranch;
        }

        set
        {
            _bankBranch = value;
        }
    }

    public string bankAccount
    {
        get
        {
            return _bankAccount;
        }

        set
        {
            _bankAccount = value;
        }
    }

    public int numberPayments
    {
        get
        {
            return _numberPayments;
        }

        set
        {
            _numberPayments = value;
        }
    }

    public string voucherNumber
    {
        get
        {
            return _voucherNumber;
        }

        set
        {
            _voucherNumber = value;
        }
    }

    public string voucherReasonId
    {
        get
        {
            return _voucherReasonId;
        }

        set
        {
            _voucherReasonId = value;
        }
    }

    public string checkNumber
    {
        get
        {
            return _checkNumber;
        }

        set
        {
            _checkNumber = value;
        }
    }

    public string checkDate
    {
        get
        {
            return _checkDate;
        }

        set
        {
            _checkDate = value;
        }
    }
  

    public string transDate // wire transter date
    {
        get
        {
            return _transDate;
        }

        set
        {
            _transDate = value;
        }
    }

    public string shovar 
    {
        get
        {
            return _shovar;
        }

        set
        {
            _shovar = value;
        }
    }

    public string clientIp 
    {
        get
        {
            return _clientIp;
        }

        set
        {
            _clientIp = value;
        }
    }

    public string userAgent 
    {
        get
        {
            return _userAgent;
        }

        set
        {
            _userAgent = value;
        }
    }

    public string acceptLanguage 
    {
        get
        {
            return _acceptLanguage;
        }

        set
        {
            _acceptLanguage = value;
        }
    }

    public string hostIp 
    {
        get
        {
            return _hostIp;
        }

        set
        {
            _hostIp = value;
        }
    }
    public WebReferenceSupplier.SupplierPricingComponent GetCrmType()
    {
        WebReferenceSupplier.SupplierPricingComponent spc = new WebReferenceSupplier.SupplierPricingComponent();
        spc.AmountPayed = this.PaymentAmount;
        spc.BaseAmount = this.PaymentAmount;
        spc.Type = this.SupplierPricingComponentType;
        return spc;
    }

    public string InvoiceUrlReference { get; set; }  
}
