﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for BadWordService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class BadWordService : System.Web.Services.WebService {
    const int MAX_SUGGESTS = 10;
    public BadWordService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public List<string> SuggestList(string str)
    {
        List<string> list = BadWordsV;
        IEnumerable<string> query = (from x in list
                                     where x.ToLower().StartsWith(str.ToLower())
                                     select x).Take(MAX_SUGGESTS);
        return query.ToList();
    }
    [WebMethod(true)]
    public string CheckOne(string str)
    {
        List<string> list = BadWordsV;
        string query = (from x in list
                        where x.ToLower().StartsWith(str.ToLower())
                        select x).FirstOrDefault();
        return query;
    }
    public void SetBadWords(string _WebReference)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_WebReference);
        WebReferenceSite.ResultOfGetAllBadWordsResponse result = null;
        try
        {
            result = _site.GetAllBadWords();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, "WebReference: "+_WebReference);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {            
            return;
        }
        List<string> list = new List<string>();
        foreach (WebReferenceSite.BadWordData bwd in result.Value.BadWords)
        {
            if (!list.Contains(bwd.Word))
                list.Add(bwd.Word);
        }
        BadWordsV = list;
    }
    public void SetBadWords()
    {
        if (Session["Site"] == null)
            BadWordsV = new List<string>();
        string _WebReference = ((SiteSetting)Session["Site"]).GetUrlWebReference;
        SetBadWords(_WebReference);
        if(Session["_BadWords"]==null)
            BadWordsV = new List<string>();        
    }
    List<string> BadWordsV
    {
        get
        {
            if (Session["_BadWords"] == null)            
                SetBadWords();
            return (List<string>)Session["_BadWords"];
        }
        set
        {
            Session["_BadWords"] = value;
        }
    }
    
}

