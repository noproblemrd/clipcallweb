using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Reflection;
using System.Data.SqlClient;

/// <summary>
/// Summary description for GetDataTable
/// </summary>
public class GetDataTable
{
	public GetDataTable()
	{
		//
		
		//
	}
    public static DataTable GetDataTableFromList<T>(List<T> list)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            data.Columns.Add(_pi.Name, _pi.PropertyType);
            names.Add(_pi);            
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                row[name.Name] = name.GetValue(ngc, null);
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromList<T>(T[] list)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromList(_list);        
    }
    public static DataTable GetDataTableColums<T>(List<T> list)
    {
    //    List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            data.Columns.Add(_pi.Name, _pi.PropertyType);
  //          names.Add(_pi);
        }
        return data;
    }
    public static DataTable GetDataTableColums<T>(T[] list)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableColums(_list);
    }
    public static DataTable GetDataTableFromList(SqlDataReader reader)
    {
        Dictionary<string, Type> names = new Dictionary<string, Type>();
        DataTable data = new DataTable();
        for (int i = 0; i < reader.FieldCount; i++)
        {
            data.Columns.Add(reader.GetName(i), reader.GetFieldType(i));
            names.Add(reader.GetName(i), reader.GetFieldType(i));
        }
        
        while (reader.Read())
        {
            DataRow row = data.NewRow();
            foreach (KeyValuePair<string, Type> kvp in names)
            {
                row[kvp.Key] = reader[kvp.Key];
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableColumsString<T>(List<T> list)
    {
        //    List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            data.Columns.Add(_pi.Name, typeof(string));
            //          names.Add(_pi);
        }
        return data;
    }
    public static DataTable GetDataTableColumsString<T>(T[] list)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableColumsString(_list);
    }
    public static List<string> GetColumnsNames(DataTable data)
    {
        List<string> list = new List<string>();
        foreach (DataColumn dc in data.Columns)
        {
            if (dc.ColumnName.Contains("."))
                dc.ColumnName = dc.ColumnName.Replace(".", "");
            list.Add(dc.ColumnName);
        }
        return list;
    }
    public static List<KeyValuePair<string, string>> GetColumnsNamesCaption(DataTable data)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        foreach (DataColumn dc in data.Columns)
        {
            if (dc.ColumnName.Contains("."))
                dc.ColumnName = dc.ColumnName.Replace(".", "");            
            list.Add(new KeyValuePair<string, string>(dc.ColumnName, dc.Caption));
        }
        return list;
    }
    public static List<string> GetColumnsNames(DataView data)
    {
        return GetColumnsNames(data.ToTable());
    }
    public static DataTable GetDataTableFromListVisibleColums<T>(List<T> list, out Dictionary<string, bool> ToShow)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        ToShow = new Dictionary<string,bool>();
        
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            data.Columns.Add(_pi.Name, _pi.PropertyType);            
            names.Add(_pi);
            ToShow.Add(_pi.Name, false);
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                object o = name.GetValue(ngc, null);
                row[name.Name] = o;
                if (o != null && !string.IsNullOrEmpty(o.ToString()))
                    ToShow[name.Name] = true;
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListVisibleColums<T>(T[] list, out Dictionary<string, bool> ToShow)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListVisibleColums(_list, out ToShow);
    }
    public static DataTable GetDataTableFromListVisibleColumsDateFormat<T>(List<T> list, out Dictionary<string, bool> ToShow, string date_format)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        ToShow = new Dictionary<string, bool>();

        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            if (_pi.PropertyType == typeof(DateTime))
                data.Columns.Add(_pi.Name, typeof(string));
            else
                data.Columns.Add(_pi.Name, _pi.PropertyType);
            names.Add(_pi);
            ToShow.Add(_pi.Name, false);
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                bool IsOkDate = true;
                object o = name.GetValue(ngc, null);
                if (o != null && o.GetType() == typeof(DateTime))
                {
                    DateTime dt = (DateTime)o;
                    if (dt == DateTime.MaxValue || dt == DateTime.MinValue)
                        IsOkDate = false;
                    row[name.Name] = string.Format(date_format, dt);
                }
                else
                    row[name.Name] = o;
                if (o != null && !string.IsNullOrEmpty(o.ToString()) && o.ToString() != "0" && IsOkDate)
                    ToShow[name.Name] = true;
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListVisibleColumsDateFormat<T>(T[] list, out Dictionary<string, bool> ToShow, string date_format)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListVisibleColumsDateFormat(_list, out ToShow, date_format);
    }
    public static DataTable GetDataTableFromListCorrectNumber<T>(List<T> list)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            bool IsNullAble = false;
            Type _type = _pi.PropertyType;
            if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))            
                _type = Nullable.GetUnderlyingType(_type);


            if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float))
            {
                data.Columns.Add(_pi.Name, typeof(string));
                data.Columns[_pi.Name].Caption = "IsNumber";
            }
            else
            {                
                data.Columns.Add(_pi.Name, _type);
                if(_type == typeof(int))
                    data.Columns[_pi.Name].Caption = "IsNumber";
            }
            names.Add(_pi);
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                object Ovalue = name.GetValue(ngc, null);
                if (Ovalue == null)
                    continue;
                Type _type = name.PropertyType;
                if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    _type = Nullable.GetUnderlyingType(_type);
                if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float))
                    row[name.Name] = String.Format("{0:0.##}", Ovalue);
                else
                    row[name.Name] = Ovalue;
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListCorrectNumber<T>(T[] list)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListCorrectNumber(_list);
    }
    public static DataTable GetDataTableFromListCorrectNumber<T>(T[] list, string currency)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListCorrectNumber(_list, currency);
    }
    public static DataTable GetDataTableFromListCorrectNumber<T>(List<T> list, string currency)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            bool IsNullAble = false;
            Type _type = _pi.PropertyType;
            if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                _type = Nullable.GetUnderlyingType(_type);


            if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float))
                data.Columns.Add(_pi.Name, typeof(string));
            else
                data.Columns.Add(_pi.Name, _type);
            names.Add(_pi);
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                object Ovalue = name.GetValue(ngc, null);
                if (Ovalue == null)
                    continue;
                Type _type = name.PropertyType;
                if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    _type = Nullable.GetUnderlyingType(_type);
                if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float))
                {
                    string str = String.Format("{0:0.##}", Ovalue);
                    if (_type == typeof(decimal))
                        str = currency + str;
                    row[name.Name] = str;
                }
                else
                    row[name.Name] = Ovalue;
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListCorrectNumberInt<T>(List<T> list)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            bool IsNullAble = false;
            Type _type = _pi.PropertyType;
            if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                _type = Nullable.GetUnderlyingType(_type);


            if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float) || _type == typeof(int))
                data.Columns.Add(_pi.Name, typeof(string));
            else
                data.Columns.Add(_pi.Name, _type);
            names.Add(_pi);
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                object Ovalue = name.GetValue(ngc, null);
                if (Ovalue == null)
                    continue;
                Type _type = name.PropertyType;
                if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    _type = Nullable.GetUnderlyingType(_type);
                if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float) || _type == typeof(int))
                    row[name.Name] = String.Format("{0:#,0.##}", Ovalue);
                else
                    row[name.Name] = Ovalue;
            }
            data.Rows.Add(row);
        }
        return data;
    }
    
    public static DataTable GetDataTableFromListCorrectNumberInt<T>(T[] list)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListCorrectNumberInt(_list);
    }

    public static DataTable GetDataTableFromListCorrectNumberTrueFalse<T>(List<T> list, Dictionary<bool, string> YesNo)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            bool IsNullAble = false;
            Type _type = _pi.PropertyType;
            if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                _type = Nullable.GetUnderlyingType(_type);


            if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float) || _type == typeof(bool))
                data.Columns.Add(_pi.Name, typeof(string));
            else
                data.Columns.Add(_pi.Name, _type);
            names.Add(_pi);
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                object Ovalue = name.GetValue(ngc, null);
                if (Ovalue == null)
                    continue;
                Type _type = name.PropertyType;
                if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    _type = Nullable.GetUnderlyingType(_type);
                if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float))
                    row[name.Name] = String.Format("{0:0.##}", Ovalue);
                else if (_type == typeof(bool))
                    row[name.Name] = YesNo[(bool)Ovalue];
                else
                    row[name.Name] = Ovalue;
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListCorrectNumberTrueFalse<T>(T[] list, Dictionary<bool, string> YesNo)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListCorrectNumberTrueFalse(_list, YesNo);
    }


    public static DataTable GetdatatTableFromArrayTwoDimensional(string[][] str, List<KeyValuePair<string, eWizardSelectType>> list)
    {
        DataTable data = new DataTable();
        if (str.Length == 0)
            return data;

        for (int i = 0; i < list.Count; i++)
        {
            data.Columns.Add(list[i].Key);
        }
        for(int i=0;i<str.Length;i++)
        {
            int _index = 0;
            DataRow row = data.NewRow();
            for (int j = 0; j < str[i].Length; j++)
            {
                if (list[_index].Value == eWizardSelectType.None)
                {
                    
                    row[_index] = str[i][j];
                }
                else
                {
                    row[_index] = str[i][j++] + ";;;" + str[i][j] + ";;;" + list[_index].Value.ToString();
                }
                _index++;
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListDateTimeFormat<T>(List<T> list, string _format)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            Type _type = (_pi.PropertyType == typeof(DateTime)) ? typeof(string) : _pi.PropertyType;
            data.Columns.Add(_pi.Name, _type);
            names.Add(_pi);            
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                if (name.PropertyType == typeof(DateTime))
                    row[name.Name] = string.Format(_format, (DateTime)(name.GetValue(ngc, null)));
                else
                    row[name.Name] = name.GetValue(ngc, null);
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListDateTimeFormat<T>(T[] list, string _format)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListDateTimeFormat(_list, _format);
    }
    public static DataTable GetDataTableFromListDateTimeFormat<T>(List<T> list, string _DateFormat, string _TimeFormat)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            Type _type = (_pi.PropertyType == typeof(DateTime)) ? typeof(string) : _pi.PropertyType;
            data.Columns.Add(_pi.Name, _type);
            names.Add(_pi);
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                if (name.PropertyType == typeof(DateTime))
                {
                    string _date = string.Format(_DateFormat, (DateTime)(name.GetValue(ngc, null)));
                    _date += " " + string.Format(_TimeFormat, (DateTime)(name.GetValue(ngc, null)));
                    row[name.Name] = _date;
                }
                else
                    row[name.Name] = name.GetValue(ngc, null);
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListDateTimeFormat<T>(T[] list, string _DateFormat, string _TimeFormat)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListDateTimeFormat(_list, _DateFormat, _TimeFormat);
    }
    public static DataTable GetDataTableFromListCorrectNumberDateTimeFormat<T>(List<T> list, string _format)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            bool IsNullAble = false;
            Type _type = _pi.PropertyType;
            if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                _type = Nullable.GetUnderlyingType(_type);


            if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float) || 
                _type == typeof(DateTime))
                data.Columns.Add(_pi.Name, typeof(string));
            else
                data.Columns.Add(_pi.Name, _type);
            names.Add(_pi);
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                object Ovalue = name.GetValue(ngc, null);
                if (Ovalue == null)
                    continue;
                Type _type = name.PropertyType;
                if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    _type = Nullable.GetUnderlyingType(_type);
                if (_type == typeof(decimal) || _type == typeof(double) || _type == typeof(float))
                    row[name.Name] = String.Format("{0:0.##}", Ovalue);
                else if (_type == typeof(DateTime))
                    row[name.Name] = string.Format(_format, (DateTime)Ovalue);
                else
                    row[name.Name] = Ovalue;
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListCorrectNumberDateTimeFormat<T>(T[] list, string _format)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListCorrectNumberDateTimeFormat(_list, _format);
    }

    public static DataTable GetDataTableFromListEnumToString<T>(List<T> list)
    {
        List<PropertyInfo> names = new List<PropertyInfo>();
        DataTable data = new DataTable();
        PropertyInfo[] pi = typeof(T).GetProperties();
        foreach (PropertyInfo _pi in pi)
        {
            Type _type = _pi.PropertyType;
            if(_type.IsEnum)
                data.Columns.Add(_pi.Name, typeof(string));
            else
                data.Columns.Add(_pi.Name, _pi.PropertyType);
            names.Add(_pi);
        }
        foreach (T ngc in list)
        {
            DataRow row = data.NewRow();
            foreach (PropertyInfo name in names)
            {
                if(name.PropertyType.IsEnum)
                    row[name.Name] = name.GetValue(ngc, null).ToString();
                else
                    row[name.Name] = name.GetValue(ngc, null);
            }
            data.Rows.Add(row);
        }
        return data;
    }
    public static DataTable GetDataTableFromListEnumToString<T>(T[] list)
    {
        List<T> _list = new List<T>(list);
        return GetDataTableFromListEnumToString(_list);
    }
    public static int GetColumIndex(string name, GridView _GridView)
    {
        foreach (DataControlField field in _GridView.Columns)
        {            
            if (field.SortExpression == name)
            {
                return _GridView.Columns.IndexOf(field);
            }
        }
        return -1;
    }
    
    public static DataTable GetDataTableFromDataTableAndGridView(DataTable _data, GridView gv)
    {
        DataTable data = new DataTable();
        foreach (DataControlField dcf in gv.Columns)
        {
            foreach (DataColumn dc in _data.Columns)
            {
                if (dcf.SortExpression == dc.ColumnName)
                {                    
                    data.Columns.Add(dcf.SortExpression, dc.DataType);
                }
            }
        }
        foreach (DataRow row in _data.Rows)
        {
            DataRow newrow = data.NewRow();
            foreach (DataColumn dc in _data.Columns)
            {
                if(data.Columns.Contains(dc.ColumnName))
                {
                    newrow[dc.ColumnName] = row[dc];
                }
            }
            data.Rows.Add(newrow);
        }
        /*
        foreach (GridViewRow row in gv.Rows)
        {
            if (row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i < _data.Columns.Count && i < row.Cells.Count; i++)
                {
                    foreach (Control cnt in row.Cells[i].Controls)
                    {
                        if (cnt is Label)
                        {
                            _data.Columns[i].ColumnName =  ((Label)cnt).Text;
                            break;
                        }
                    }
                }
            }
        }
         */
        return data;
    }
    
}
