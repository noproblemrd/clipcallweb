﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BonusServiceData
/// </summary>
/// 
[Serializable()]
public class BonusServiceData
{
    public SortedDictionary<int, int> pricePercentList { get; private set; }
    public int MinDeposite { get; private set; }
    public int RechargeBonus { get; private set; }
    public BonusServiceData(SortedDictionary<int, int> pricePercentList, int MinDeposite, int RechargeBonus)
    {
        this.pricePercentList = pricePercentList;
        this.MinDeposite = MinDeposite;
        this.RechargeBonus = RechargeBonus;
    }
}