﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for State
/// </summary>
[Serializable()]
public class State
{
    public string abbreviation { get; set; }
    public string fullName { get; set; }
    public bool isPopular { get; set; }


	public State()
	{
		//
		
		//
	}
}