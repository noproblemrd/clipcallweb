﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;

/// <summary>
/// Summary description for EncryptString
/// </summary>
public class EncryptString
{
    const string RecordPathScramble = "simsim";
    const string SCRAMBEL_PUBLISHER = "*5106sp";
	public EncryptString()
	{
		//
		
		//
	}
    public static string Encrypt_String(string Message, string Passphrase)
    {
        byte[] Results;
        System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

        // Step 1. We hash the passphrase using MD5
        // We use the MD5 hash generator as the result is a 128 bit byte array
        // which is a valid length for the TripleDES encoder we use below

        MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
        byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

        // Step 2. Create a new TripleDESCryptoServiceProvider object
        TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

        // Step 3. Setup the encoder
        TDESAlgorithm.Key = TDESKey;
        TDESAlgorithm.Mode = CipherMode.ECB;
        TDESAlgorithm.Padding = PaddingMode.PKCS7;

        // Step 4. Convert the input string to a byte[]
        byte[] DataToEncrypt = UTF8.GetBytes(Message);

        // Step 5. Attempt to encrypt the string
        try
        {
            ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
            Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
        }
        finally
        {
            // Clear the TripleDes and Hashprovider services of any sensitive information
            TDESAlgorithm.Clear();
            HashProvider.Clear();
        }

        // Step 6. Return the encrypted string as a base64 encoded string
        return Convert.ToBase64String(Results);
    }

    public static string Decrypt_String(string Message, string Passphrase)
    {
        byte[] Results;
        System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

        // Step 1. We hash the passphrase using MD5
        // We use the MD5 hash generator as the result is a 128 bit byte array
        // which is a valid length for the TripleDES encoder we use below

        MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
        byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

        // Step 2. Create a new TripleDESCryptoServiceProvider object
        TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

        // Step 3. Setup the decoder
        TDESAlgorithm.Key = TDESKey;
        TDESAlgorithm.Mode = CipherMode.ECB;
        TDESAlgorithm.Padding = PaddingMode.PKCS7;

        // Step 4. Convert the input string to a byte[]
        byte[] DataToDecrypt = Convert.FromBase64String(Message);

        // Step 5. Attempt to decrypt the string
        try
        {
            ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
            Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
        }
        finally
        {
            // Clear the TripleDes and Hashprovider services of any sensitive information
            TDESAlgorithm.Clear();
            HashProvider.Clear();
        }

        // Step 6. Return the decrypted string in UTF8 format
        return UTF8.GetString(Results);
    }
    public static string DecodeRecordPath(string RecordPath)
    {
        RecordPath = RecordPath.Replace(' ', '+');
        return Decrypt_String(RecordPath, RecordPathScramble);
    }
    public static string EncodeRecordPath(string RecordPath)
    {
        return Encrypt_String(RecordPath, RecordPathScramble);
    }
    public static bool LogIn(PageSetting _page, HttpContext context)
    {
        if (context.Request.Cookies["PublishLogin"] == null)
            return false;
        string _password = context.Request.Cookies["PublishLogin"].Values["PublishPassword"];
    //    if (!context.Request.Cookies["PublishLogin"].HttpOnly) NOT WORKING
   //         return false;
        if (string.IsNullOrEmpty(_password))
        {
            LogInFaild(context, _page.siteSetting.GetSiteID, string.Empty, false);
            return false;
        }
        try
        {
            _password = EncryptString.Decrypt_String(_password, SCRAMBEL_PUBLISHER);
        }
        catch (Exception exc) 
        {
            LogInFaild(context, _page.siteSetting.GetSiteID, string.Empty, false);
            return false;
        }

        string[] newPassword = _password.Split(';');
        string pass, email;
        if (newPassword.Length == 2)
        {
            pass = newPassword[0];
            email = newPassword[1];
        }
        else
        {
            LogInFaild(context, _page.siteSetting.GetSiteID, string.Empty, false);
            return false;
        }
        bool IsApprove = DBConnection.IsLoginApproved(email, _page.siteSetting.GetSiteID);
        if (!IsApprove)
        {
            LogInFaild(context, _page.siteSetting.GetSiteID, email, false);
            return false;
        }
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(_page);
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;
        try
        {
            _response = supplier.UserLogin(email, pass);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, _page.siteSetting);
            LogInFaild(context, _page.siteSetting.GetSiteID, email, false);
            return false;
        }
        if (_response.Type != WebReferenceSupplier.eResultType.Success)
        {
            LogInFaild(context, _page.siteSetting.GetSiteID, email, false);
            return false;
        }
        if (_response.Value.UserLoginStatus == WebReferenceSupplier.eUserLoginStatus.NotFound)
        {
            LogInFaild(context, _page.siteSetting.GetSiteID, email, true);
            return false;
        }
        if (_response.Value.AffiliateOrigin != Guid.Empty)
        {
            LogInFaild(context, _page.siteSetting.GetSiteID, email, true);
            return false;
        }
  //      securityLevel = _response.Value.SecurityLevel;
        if (_response.Value.SecurityLevel < 1)
        {
            LogInFaild(context, _page.siteSetting.GetSiteID, email, true);
            return false;
        }
        DBConnection.InsertLogin(email, _response.Value.UserId, _page.siteSetting.GetSiteID, eUserType.Publisher);
        _page.userManagement = new UserMangement(_response.Value.UserId.ToString(),
           _response.Value.SecurityLevel.ToString(), _response.Value.Name, email, _response.Value.CanListenToRecords, _response.Value.TimeZoneMinutes);

        _page.userManagement.SetUserObject(_page);
        Utilities.SetUserPublisherLink(_page);
        return true;
    }
    private static void LogInFaild(HttpContext context, string SiteId, string email, bool IncludeInsertIntoDB)
    {
        context.Response.Cookies["PublishLogin"].Expires = DateTime.Now.AddDays(-1d);
        if (IncludeInsertIntoDB)
        {
            DBConnection.InsertLogin(email, Guid.Empty, SiteId, eUserType.Publisher);
        }        
    }
    public static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }



    public static string Base64Decode(string base64EncodedData)
    {
        var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    }
}
