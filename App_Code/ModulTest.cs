﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ModulTest
/// </summary>
public class ModulTest : IHttpModule
{
    const string LOGIC = "logic.js";
    const string GLOGIC = "glogic.js";
    const string INJ = "inj.js";
    const string INTEXT = "intext.js";
    const string POPUP_PRODUCT = "pprod.js";
    const string DATA_POPUP_PRODUCT = "datapop.js";
    const string INJ_MON = "injmon.js";
    const string MON_HTM = "monhtm.js";

    
	public ModulTest()
	{
		//
		
		//
	}

    public void Dispose()
    {
        
    }

    public void Init(HttpApplication context)
    {
        context.BeginRequest += new EventHandler(context_BeginRequest);
    }

    void context_BeginRequest(object sender, EventArgs e)
    {
        HttpRequest request = ((HttpApplication)sender).Request;
        HttpResponse response = ((HttpApplication)sender).Response;
        //     HttpContext context = ((HttpApplication)sender).Context;   

        if (request.Url.AbsolutePath.ToLower().Contains("/npsb/"))
        {
            string pagename =
                    System.IO.Path.GetFileName(request.Url.AbsolutePath).ToLower();
            switch (pagename)
            {
                case(LOGIC):
                    ((HttpApplication)sender).Context.RewritePath(request.ApplicationPath + "npsb/logic.ashx?" + request.QueryString.ToString());
                    return;
                case(GLOGIC):
                    ((HttpApplication)sender).Context.RewritePath(request.ApplicationPath + "npsb/glogic.ashx?" + request.QueryString.ToString());
                    return;
                case(INJ):
                    ((HttpApplication)sender).Context.RewritePath(request.ApplicationPath + "npsb/inj.ashx?" + request.QueryString.ToString());
                    return;
                case (INTEXT):
                    ((HttpApplication)sender).Context.RewritePath(request.ApplicationPath + "npsb/intext.ashx?" + request.QueryString.ToString());
                    return;
                case (POPUP_PRODUCT):
                    ((HttpApplication)sender).Context.RewritePath(request.ApplicationPath + "npsb/popupProduct.ashx?" + request.QueryString.ToString());
                    return;
                case (DATA_POPUP_PRODUCT):
                    ((HttpApplication)sender).Context.RewritePath(request.ApplicationPath + "npsb/datapop.ashx?" + request.QueryString.ToString());
                    return;
                case (INJ_MON):
                    ((HttpApplication)sender).Context.RewritePath(request.ApplicationPath + "npsb/injmon.ashx?" + request.QueryString.ToString());
                    return;
                case (MON_HTM):
                    ((HttpApplication)sender).Context.RewritePath(request.ApplicationPath + "npsb/monhtm.ashx?" + request.QueryString.ToString());
                    return;
            }
           
        }
        /*
        if (request.Url.Host == "www.noproblem.me")
        {
            
            switch (pagename)
            {
                case ("about.html"):
                case ("LegalTerms.htm"):
                case ("contact.html"):
                case ("adv.html"):
                case ("publisher.html"):
                case ("faq.htm"):
                    response.Redirect("http://www2.noproblemppc.com/" + pagename);
                    break;
            }

        }
         **/
        /*
        if (pagename == "Default26.aspx")// && !string.IsNullOrEmpty(request.QueryString["request"]))
        {
            HttpContext context = ((HttpApplication)sender).Context;
            context.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.ReadOnly);
        }
         */
        /*
        HttpRequest request = ((HttpApplication)sender).Request;
   //     HttpContext context = ((HttpApplication)sender).Context;       
        string _extension = request.CurrentExecutionFilePathExtension;
        if (!string.IsNullOrEmpty(_extension))
            return;
        string destination = request.Path;
        if (request.ApplicationPath != "/")
        {
            destination = destination.Substring(destination.IndexOf(request.ApplicationPath) + request.ApplicationPath.Length);
        }
        int t = destination.Where(x => x.Equals('/')).Count();
        if (destination.Where(x => x.Equals('/')).Count() != 1)
            return;
        if (destination.Contains("/i"))
        {
            string[] _path = destination.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            string _parms = _path[_path.Length-1];
            HttpContext context = ((HttpApplication)sender).Context;
           
//            context.Server.Transfer(request.ApplicationPath + "/Tests/dbug/ReWrite.aspx?id=" + _parms);
            context.RewritePath(request.ApplicationPath + "/Tests/dbug/ReWrite.aspx?id=" + _parms);
        }
         * */
    }
    
}