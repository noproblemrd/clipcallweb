﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MyEventRibbon
/// </summary>

public class EventArgsRibbon : EventArgs
{
    public EventArgsRibbon()
    { }

    public int Count { get; set; }
    public string Category { get; set; }
    public string State { get; set; }
    public string City { get; set; }
    public string ZipCode { get; set; }
}