﻿using System;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for PaymentsManager
/// </summary>
internal class PaymentsManager
{
    #region Internal Methods

    internal string CompletePaymentOperation(string siteId, string paymentId, string guid, string transactionId)
    {
        return null;
        /*
        string paymentDetails;
        int rechargeAmount;

        Guid validGuid;
        if (!Guid.TryParse(guid,out validGuid))
            return "1,invalid paymentId wrong guid";            
        
        string ifExistAndTrue = IfExistPaymentTrue(paymentId, guid);
        if (ifExistAndTrue.ToLower()=="true")
            return "1,invalid paymentId already exist";
        else if(string.IsNullOrEmpty(ifExistAndTrue))
            return "1,invalid paymentId there is not such record";

        GetDetailsFromDataBase(paymentId, out paymentDetails, out rechargeAmount);
        if (string.IsNullOrEmpty(paymentDetails))
        {
            LogEdit.SaveLog("Accounting_" + siteId, "api payment negative :" +
            "\r\nsiteId: " + siteId +
            "\r\npaymentId: " + paymentId +
            "\r\nretVal: 1, invalid paymentId");
            return "1,invalid paymentId";
        }

        SupllierPayment sp = ExtractPaymentDetails(paymentDetails, rechargeAmount);
        SiteSetting st = new SiteSetting(siteId);
        sp.chargingCompany = st.CharchingCompany.ToString();
        sp.transactionId = transactionId;
        sp.paymentMethod = WebReferenceSupplier.ePaymentMethod.CreditCard;
        WebServiceCharging wsCharging = new WebServiceCharging();
        ResultCharging resultCharging = DpzUtilities.ChargingSupplier(sp, null, HttpContext.Current.Session);
        string retVal;
        if (resultCharging.Result == ResultCharging.eResult.success && resultCharging.DepositStatus == eDepositStatusResponse.OK)
        {
            retVal = "0,success";

            LogEdit.SaveLog("Accounting_" + sp.siteLangId.ToString(), "api payment positive :" +
            "\r\nname: " + sp.name +
            "\r\nsiteId: " + siteId +
            "\r\npaymentId: " + paymentId +
            "\r\nretVal: " + retVal);
        }

        else if (resultCharging.Result == ResultCharging.eResult.success) // some problems
        {
            retVal = "1,invalid paymentId " + resultCharging.DepositStatus.ToString();

            LogEdit.SaveLog("Accounting_" + sp.siteLangId.ToString(), "api payment negative :" +               
            resultCharging.DepositStatus.ToString() +
            "\r\nname: " + sp.name +
            "\r\nsiteId: " + siteId +
            "\r\npaymentId: " + paymentId +
            "\r\nretVal: " + retVal);
        }

        else // failure
        {
            retVal = "1,invalid paymentId " + resultCharging.Msg;

            LogEdit.SaveLog("Accounting_" + sp.siteLangId.ToString(), "api payment negative :" +
            resultCharging.Msg +
            "\r\nname: " + sp.name +
            "\r\nsiteId: " + siteId +
            "\r\npaymentId: " + paymentId +
            "\r\nretVal: " + retVal);
        }

        
        return retVal;
        */
    }

    #endregion

    #region Private Methods

    private string IfExistPaymentTrue(string paymentId, string guid)
    {
        string command = "EXEC dbo._IfPaymentTrue @paymentId, @guid";        
        string ifExist;
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                cmd.Parameters.AddWithValue("@paymentId", paymentId);
                cmd.Parameters.AddWithValue("@guid", guid);
                ifExist = Convert.ToString(cmd.ExecuteScalar());                
            }            
        }
        
       return ifExist;        

    }

	private void GetDetailsFromDataBase(string paymentId, out string paymentDetails, out int rechargeAmount)
    {
        paymentDetails = string.Empty;
        rechargeAmount = -1;
        string command = "EXEC dbo._SetConfirmGetPayment @auto_id";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                cmd.Parameters.AddWithValue("@auto_id", paymentId);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    
                    if (reader.Read())
                    {
                        paymentDetails = (string)reader["details"];
                        rechargeAmount = reader.IsDBNull(1) ? -1 : (int)reader["RechargeAmount"];
                    }
                }
            }
        }
    }

    private SupllierPayment ExtractPaymentDetails(string paymentDetails, int rechargeAmount)
    {
        SupllierPayment sp = new SupllierPayment();
        var arrPaymentDetails = paymentDetails.Split('&');
        string[] nameValueCustom;
        sp.RechargeAmount = rechargeAmount;
        for (int i = 0; i < arrPaymentDetails.Length; i++)
        {
            nameValueCustom = arrPaymentDetails[i].Split('=');
            if (nameValueCustom[0] == "bonusAmount")
                sp.discount = nameValueCustom[1];

            if (nameValueCustom[0] == "bonusType")
                sp.discountType= nameValueCustom[1];

            if (nameValueCustom[0] == "bonusExtraAmount")
                sp.extraBonus = nameValueCustom[1];

            if (nameValueCustom[0] == "extraReasonBonusId")
                sp.extraReasonBonusId = nameValueCustom[1];

            if (nameValueCustom[0] == "supplierGuid")
                sp.guid = nameValueCustom[1];

            if (nameValueCustom[0] == "supplierId")
                sp.id = nameValueCustom[1];

            if (nameValueCustom[0] == "advertiserName")
                sp.name = HttpUtility.UrlDecode(nameValueCustom[1]);

            if (nameValueCustom[0] == "fieldId")
                sp.fieldId = nameValueCustom[1];

            if (nameValueCustom[0] == "fieldName")
                sp.fieldName = HttpUtility.UrlDecode(nameValueCustom[1]);

            if (nameValueCustom[0] == "balanceNew")
                sp.balanceNew = nameValueCustom[1];

            if (nameValueCustom[0] == "isFirstTimeV")
                sp.isFirstTimeV = Convert.ToBoolean(nameValueCustom[1]);

            if (nameValueCustom[0] == "balanceOld")
                sp.balanceOld = nameValueCustom[1];

            if (nameValueCustom[0] == "userId")
                sp.userId = nameValueCustom[1];

            if (nameValueCustom[0] == "userName")
                sp.userName = HttpUtility.UrlDecode(nameValueCustom[1]);

            if (nameValueCustom[0] == "siteLangId")
                sp.siteLangId = Convert.ToInt32(nameValueCustom[1]);

            if (nameValueCustom[0] == "siteId")
                sp.siteId = nameValueCustom[1];

            if (nameValueCustom[0] == "isAutoRenew")
                sp.isAutoRenew = Convert.ToBoolean(nameValueCustom[1]);

            if (nameValueCustom[0] == "email")
                sp.email = HttpUtility.UrlDecode(nameValueCustom[1]);

            if (nameValueCustom[0] == "phoneNumber")
                sp.tel1 = nameValueCustom[1];

            if (nameValueCustom[0] == "otherPhone")
                sp.tel2 = nameValueCustom[1];

            if (nameValueCustom[0] == "country")
                sp.state = nameValueCustom[1];

            if (nameValueCustom[0] == "city")
                sp.city = nameValueCustom[1];

            if (nameValueCustom[0] == "streetNumber")
                sp.address = HttpUtility.UrlDecode(nameValueCustom[1]);
            
            if (nameValueCustom[0] == "postalCode")
                sp.zipCode = nameValueCustom[1];

            if (nameValueCustom[0] == "webSite")
                sp.website = HttpUtility.UrlDecode(nameValueCustom[1]);

            if (nameValueCustom[0] == "fax")
                sp.fax = nameValueCustom[1];

            if (nameValueCustom[0] == "vat")
                sp.tax = nameValueCustom[1];

            if (nameValueCustom[0] == "ifAccountingCharging")
                sp.ifAccountingCharging = Convert.ToBoolean(nameValueCustom[1]);

            if (nameValueCustom[0] == "ppcPackage")
                sp.ppcPackage = HttpUtility.UrlDecode(nameValueCustom[1]);

            if (nameValueCustom[0] == "currency")
                sp.currency = nameValueCustom[1];

            if (nameValueCustom[0] == "accountNumber")
                sp.accountNumber = nameValueCustom[1];

            if (nameValueCustom[0] == "amount")
                sp.deposit = nameValueCustom[1];
        } // end for
        return sp;
    } 

	#endregion
}