﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for PopupPageBase
/// </summary>
public class PopupPageBase : System.Web.UI.Page
{
    protected SliderData sd;
    
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        string _script = "window['__np__flag'] = true;";
        HtmlGenericControl script_control = new HtmlGenericControl("script");
        script_control.Attributes["type"] = "text/javascript";
        script_control.InnerHtml = _script;
        Header.Controls.Add(script_control);

        sd = new SliderData(Context, eSliderType.Popup);
        //    sd.Country = SliderData.DEFAULT_COUNTRY;
        if (string.IsNullOrEmpty(sd.ExpertiseCode))
        {
            Response.Flush();
            Response.End();
            return;
        }        

        LoadIcon();
    }
    private void LoadIcon()
    {
        HtmlLink _link;
        switch (sd.Type)
        {
            case (eExposureType.PicRec):
                _link = new HtmlLink();
                _link.Attributes.Add("rel", "Shortcut Icon");
                _link.Href = ResolveUrl("~/npsb/images/favicon_picrec.ico");
                Header.Controls.Add(_link);
                break;
            case (eExposureType.SpeedOptimizer_Guru):
                _link = new HtmlLink();
                _link.Attributes.Add("rel", "Shortcut Icon");
                _link.Href = ResolveUrl("~/npsb/images/favicon_SpeedOptimizerGuru.ico");
                Header.Controls.Add(_link);
                break;
            case (eExposureType.Aducky):
                _link = new HtmlLink();
                _link.Attributes.Add("rel", "Shortcut Icon");
                _link.Href = ResolveUrl("~/npsb/images/favicon_aducky.ico");
                Header.Controls.Add(_link);
                break;
            case (eExposureType.Shopwithboost):
                _link = new HtmlLink();
                _link.Attributes.Add("rel", "Shortcut Icon");
                _link.Href = ResolveUrl("~/npsb/images/Shopwithboost.ico");
                Header.Controls.Add(_link);
                break;
            default:
                {
                    switch (sd.DomainName)
                    {
                        case (eDomainName.donutleads):
                            _link = new HtmlLink();
                            _link.Attributes.Add("rel", "Shortcut Icon");
                            _link.Href = ResolveUrl("~/npsb/images/Donutleads.png");
                            Header.Controls.Add(_link);
                            break;
                        case (eDomainName.bbqleads):
                            _link = new HtmlLink();
                            _link.Attributes.Add("rel", "Shortcut Icon");
                            _link.Href = ResolveUrl("~/npsb/images/icon_bbq32.png");
                            Header.Controls.Add(_link);
                            break;
                        case(eDomainName.sushileads):
                            _link = new HtmlLink();
                            _link.Attributes.Add("rel", "Shortcut Icon");
                            _link.Href = ResolveUrl("~/npsb/images/sushileads.png");
                            Header.Controls.Add(_link);
                            break;
                        case (eDomainName.noproblemppc):
                            break;
                        case (eDomainName.pastaleads):
                        default:
                            _link = new HtmlLink();
                            _link.Attributes.Add("rel", "Shortcut Icon");
                            _link.Href = ResolveUrl("~/npsb/images/PastaLeads.png");
                            Header.Controls.Add(_link);
                            break;
                    }
                }
                break;
        }
    }
}