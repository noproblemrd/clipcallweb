using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FindControl
/// </summary>
public delegate Control my_Func(Control control);
public class FindControl
{
    protected Control _control;
    protected Type _type;
    
	public FindControl(Control control, Type _type)
	{
        
		this._control = control;
        this._type = _type;
        

	}
    protected Control ApplyToControls(Control root, my_Func func)
    {
        //      if (root.HasControls())
        if (SearchChilds(root))
        {
            foreach (Control control in root.Controls)
            {
                Control cont = func(control);
                if (cont != null)
                    return cont;
                cont = ApplyToControls(control, func);
                if (cont != null)
                    return cont;
            }
        }
        return null;
    }
    protected Control find(Control cont)
    {
        if (cont.GetType() == _type)
            return cont;
        return null;
    }
    protected Control findInterface(Control cont)
    {
        if (cont.GetType().GetInterface(_type.Name) == _type)
            return cont;        
        return null;
    }
    protected bool SearchChilds(Control control)
    {
        return control.HasControls();
    }
    public Control StartTo()
    {
  //      if (!IsNullable())
  //          return null;
        my_Func func = _type.IsInterface ? new my_Func(findInterface) : new my_Func(find);
        return ApplyToControls(_control, func);
    }
    public  bool IsNullable()
    {
        if (!_type.IsValueType)
            return true;

        return (_type.IsGenericType && _type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)));
    }
}
