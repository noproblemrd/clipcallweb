﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for SalesNewRegistration
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class SalesNewRegistration : System.Web.Services.WebService {

    public SalesNewRegistration () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string NewUser(string SiteNameId, string email, string phone)
    {
        phone = Utilities.GetCleanPhone(phone);
        WebServiceLogIn _web = new WebServiceLogIn();
        int result = -10;
        try
        {
            result = _web.NewUserRegistration(SiteNameId, email, phone);
        }
        catch (Exception exc)
        {
            return result.ToString();
        }

        return result.ToString();
    }
    [WebMethod]
    public string LogIn(string SiteNameId, string email, string password)
    {
        WebServiceLogIn _web = new WebServiceLogIn();
        string _path = string.Empty;
        try
        {
            _path = (_web.LogIn(SiteNameId, email, password));
        }
        catch (Exception exc)
        {
            return "Faild";
        }
        if (_path.Contains(".aspx"))
            return _path;
        if (_path == "NotFound")
            return _path;
        return "Faild";
    }
   
    
}
