﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for SupplierStatusComprar
/// </summary>
public class SupplierStatusComprar : IComparer<WebReferenceReports.SupplierStatus>
{
	public SupplierStatusComprar()
	{
		//
		
		//
	}

    #region IComparer<SupplierStatus> Members

    public int Compare(WebReferenceReports.SupplierStatus x, WebReferenceReports.SupplierStatus y)
    {
        return Utilities.LargeTest(x.ToString(), y.ToString());
    }

    #endregion
}

public class InvalidRequestsStatusComprar : IComparer<WebReferenceReports.InvalidRequestStatus>
{
    public InvalidRequestsStatusComprar()
    {
        //
        
        //
    }

    #region IComparer<SupplierStatus> Members

    public int Compare(WebReferenceReports.InvalidRequestStatus x, WebReferenceReports.InvalidRequestStatus y)
    {
        return Utilities.LargeTest(x.ToString(), y.ToString());
    }

    #endregion
}