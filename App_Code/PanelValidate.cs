using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for PanelValidate
/// </summary>
[Serializable()]
public class PanelValidate
{
    public bool Show;
    public bool Valid;
    public bool HasAuto;
    public bool AutoCom;
    public string onblur;
    public int AutoCompleteLevel;
    public string txtBoxId;
    public string txtBoxParentId;
    public string ParentPanelId;
    public string txtValue;
    public string RegionGuid;
    public string RequiredFieldValidatorId;
    
    public PanelValidate(bool show, bool valid, bool AutoCom, string onblur, bool HasAuto,
        int AutoCompleteLevel, string ParentPanelId)
	{
        this.Show = show;
        this.Valid = valid;
        this.AutoCom = AutoCom;
        this.onblur = onblur;
        this.HasAuto = HasAuto;
        this.AutoCompleteLevel = AutoCompleteLevel;
    //    this.txtBoxParentId = txtBoxParentId;
        this.ParentPanelId = ParentPanelId;
    //    this.RequiredFieldValidatorId = RequiredFieldValidatorId;
	}
   
}
