﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EventArgsPageIndex
/// </summary>
public class EventArgsPageIndex : EventArgs
{
    
	public EventArgsPageIndex(int page_index, int page_items)
	{
        PageIndex = page_index;
        PageItems = page_items;
	}
    public int PageIndex { get; set; }
    public int PageItems { get; set; }
}