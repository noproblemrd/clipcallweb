using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for PageSetting
/// </summary>
public class PageSetting : System.Web.UI.Page
{
    protected const string V_ICON = "images/icon-v.png";
    protected const string X_ICON = "images/icon-x.png";
    protected const string VERSION = "Version 4.4.1";
    protected const string NUMBER_FORMAT = "{0:#,0.##}";
    protected const string FOULS_MANAGMENT_URL = "~/Publisher/FoulsManagment.aspx?accountid={0}";
    private const string REG_EXPRESSION_EMAIL = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,}([-.]\w+)*$";
    string errorPath;public UserMangement userManagement;
    public SiteSetting siteSetting;
    public Dictionary<string, bool> PageSecurity;
    public Dictionary<string, string> LinkToSet;
    //popup
//    public Dictionary<string, string> users_settings;
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        userManagement = UserMangement.GetUserObject(this);
        siteSetting = SiteSetting.GetSiteSetting(this);
        PageSecurity = PageSecurityV;
        LinkToSet = LinkToSetV;
        string _login = "";
        if (userManagement.IsPublisher())
            _login = ResolveUrl("~") + "Publisher/LogOut.aspx";
        else
            _login = ResolveUrl("~") + "Management/LogOut.aspx";
        errorPath = ResolveUrl("~") + "ErrorException.aspx?continue=" + _login;
        if (userManagement.IsPublisher() && !HasConfirmToThisPage())
        {
            //          Response.Redirect(ResolveUrl("~")+"Publisher/PublisherWelcome.aspx");

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "PublisherWelcome", "top.location='" + ResolveUrl("~") + "Publisher/PublisherWelcome.aspx';", true);
            return;
        }
        
        
    }
    /*
    protected void Page_PreInit(object sender, EventArgs e)
    {
 //       users_settings = users_settingsV;
       
    }
    */
    /*
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e); 
        
        if (userManagement.IsAuthenticated())
        {           
                ViewStateUserKey = Session.SessionID;            
            
        }    
         
       
    }
    */
    protected void Page_PreLoad(object sender, EventArgs e)
    {        
        if(!IsPostBack)
            LoadTranslate(siteSetting.siteLangId);
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        /*
        if (!IsPostBack)
        {
            if (Master != null && !string.IsNullOrEmpty(Master.ID))
                LoadSiteLinks(Master.ID);
        }
         * */
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(siteSetting.GetIcon))
            {
                if (siteSetting.GetIcon.Contains("http"))
                {
                    HtmlLink _link = new HtmlLink();
                    _link.Href = siteSetting.GetIcon;
                    _link.Attributes.Add("rel", "Shortcut Icon");
                    Page.Header.Controls.Add(_link);
                }
                else
                {
                    HtmlLink _link = new HtmlLink();
                    _link.Href = ResolveUrl(siteSetting.GetIcon);
                    _link.Attributes.Add("rel", "Shortcut Icon");
                    Page.Header.Controls.Add(_link);
                }
            }
            Page.Title = siteSetting.GetPageTitle;
           
        }
        if (userManagement.IsSupplier())
        {
  //          string script = "AddEndSession(" + Session.Timeout + ");";
            int _time = (Session.Timeout * 60000) - 30000;
            string script = "try { AddEndSession(" + _time.ToString() + "); }catch(ex){}";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AddEndSession",
                script, true);
            
        }
    }
    protected void LoadTranslate(int _siteLangId)
    {
        string pagename =
         System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        DBConnection.LoadTranslateToControl(this, pagename, _siteLangId);
    }
    public void LoadMasterSiteLinks(Control control)
    {
        LinksControls lc = new LinksControls(control,  control.ID, this.siteSetting.GetSiteID);
        lc.StartTo();
    }
    
    public void LoadPageLinks()
    {
        string pagename =
         System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        LinksControls lc = new LinksControls(this, pagename, this.siteSetting.GetSiteID);
        lc.StartTo();
    }
    public string ErrorPath
    {
        get { return errorPath; }
    }
    public void SaveLinkToSet()
    {
        LinkToSetV = LinkToSet;
    }
    public void SetGuidSetting(UserMangement um)
    {
        GuidSettingV = um;
        
    }
    public UserMangement GetUserSetting()
    {
        if (userManagement == null || string.IsNullOrEmpty(userManagement.GetGuid))
            return null;
        if (userManagement.IsSupplier())
            return userManagement;
        if (GuidSettingV == null)
            return null;
        return GuidSettingV;
    }
    public string GetGuidSetting()
    {
        if(userManagement==null || string.IsNullOrEmpty(userManagement.GetGuid))
            return string.Empty;
        if (userManagement.IsSupplier())
            return userManagement.GetGuid;
        if (GuidSettingV == null)
            return string.Empty;
        return GuidSettingV.GetGuid;
    }
    public void ClearGuidSetting()
    {
        GuidSettingV = null;
    }
    public string GetUserNameSetting()
    {
        if (GuidSettingV == null)
            return "NEW";
        return GuidSettingV.User_Name;
    }
    public void SetUserNameSetting(string UserName)
    {
        if (GuidSettingV == null)
            return;
        UserMangement um = GuidSettingV;
        um.User_Name = UserName;
    }
    public void ClearUserSite()
    {
        userManagement = new UserMangement();
        userManagement.SetUserObject(this);
        siteSetting = new SiteSetting();
        siteSetting.SetSiteSetting(this);
        SetGuidSetting(null);
        Session.Clear();
    }
    public void ClearExceptSiteSetting()
    {
        SiteSetting ss = siteSetting;
        string _reason_logout = (Session["ReasonLogOut"] == null) ? null : (string)Session["ReasonLogOut"];
        userManagement = new UserMangement();        
        userManagement.SetUserObject(this);
        siteSetting = new SiteSetting();
        siteSetting.SetSiteSetting(this);
        SetGuidSetting(null);
        Session.Clear();
        siteSetting = ss;
        siteSetting.SetSiteSetting(this);
        if(!string.IsNullOrEmpty(_reason_logout))
            Session["ReasonLogOut"] = _reason_logout;
    }
    public Dictionary<string, bool> SetPageSecurity
    {
        set { PageSecurityV = value; }
    }
    public bool HasConfirmToThisPage()
    {
        string pagename =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
  //      bool isConfirm = false;
        foreach (KeyValuePair<string, bool> kvp in PageSecurity)
        {
            if (kvp.Key == pagename)            
                return kvp.Value;            
        }
        return true;
    }
    
    UserMangement GuidSettingV
    {
        get { return (Session["GuidSetting"] == null) ? null : (UserMangement)Session["GuidSetting"]; }
        set { Session["GuidSetting"] = value; }
    }
 
    Dictionary<string, bool> PageSecurityV
    {
        get
        {
            return (Session["PageSecurity"] == null) ? new Dictionary<string, bool>()
            : (Dictionary<string, bool>)Session["PageSecurity"];
        }
        set { Session["PageSecurity"] = value; }
    }
    Dictionary<string, string> LinkToSetV
    {
        get
        {
            return (Session["LinkToSet"] == null) ? new Dictionary<string, string>()
            : (Dictionary<string, string>)Session["LinkToSet"];
        }
        set { Session["LinkToSet"] = value; }
    }
    public void SetNoIframe()
    {
        Response.AddHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
        string _script = "if (top!=self) top.location.href = self.location.href;";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "SetNoIframe", _script, true);
    }
    public void UnrecognizedClient(string _Message, string _path)
    {
        if (IsPostBack)
        {
            string script = @"<script type=""text/javascript"">alert('" +
                HttpUtility.JavaScriptStringEncode(_Message) + "'); window.location='" + _path + "';</script>";
            Response.Write(script);
            Response.Flush();
            Response.End();
        }
        else
            Response.Redirect(_path);

    }
    public void RemoveServerCommentPPC(){
        string _comment = Request.QueryString["comment"];
        if (_comment == "true")
            return;
        string _script = "try{ top.RemoveServerComment(); } catch(ex){}";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "RemoveServerComment", _script, true);
    }
    public string GetMoneyToDisplay(int num)
    {
        bool IsPositive = true;
        if (num < 0)
        {
            IsPositive = false;
            num = num * -1;
        }
        string result = "";
        if (!IsPositive)
            result += "-";
        result += siteSetting.CurrencySymbol;
        result += string.Format(NUMBER_FORMAT, num);
        return result;
    }

    public string GetMoneyToDisplay(decimal num)
    {
        bool IsPositive = true;
        if (num < 0)
        {
            IsPositive = false;
            num = num * -1;
        }
        string result = "";
        if (!IsPositive)
            result += "-";
        result += siteSetting.CurrencySymbol;
        result += string.Format(NUMBER_FORMAT, num);
        return result;
    }
    protected string GetDateTimeForClient(DateTime date)
    {
        int tz = userManagement.TimeZone.HasValue ? userManagement.TimeZone.Value : 0;
        return string.Format(siteSetting.DateTimeFormat, date.AddMinutes(tz));
    }

    protected DateTime GetDateTimeFromClient(string date)
    {
        int tz = userManagement.TimeZone.HasValue ? userManagement.TimeZone.Value : 0;
        System.Globalization.CultureInfo enUS = new System.Globalization.CultureInfo("en-US");
        return DateTime.ParseExact(date, siteSetting.DateTimeFormat, enUS).AddMinutes(tz * -1);
    }

    public void Update_Success()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
    }
    public virtual void Update_Faild()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    public void Update_Success_this()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "UpdateSuccess();", true);
    }
    public void Update_Faild_this()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
    }
    public string GetNumberFormat
    {
        get { return NUMBER_FORMAT; }
    }
    public bool IsCallback()
    {
        return (!string.IsNullOrEmpty(Request.Params["__CALLBACKPARAM"]));
    }
    public virtual Control CrossPageControl
    {
        get { return null; }
    }
    public string GetEmailRegExpresionJavaScript
    {
        get { return Utilities.RegularExpressionForJavascript(REG_EXPRESSION_EMAIL); }
    }
    public Regex GetEmailRegExpresion
    {
        get { return new Regex(REG_EXPRESSION_EMAIL); }
    }
    public string GetEmailRegulareExpresion
    {
        get { return REG_EXPRESSION_EMAIL; }
    }
    protected string DownloadExcelUrl
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
    protected string GetCreateExcelUrl
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    
}
