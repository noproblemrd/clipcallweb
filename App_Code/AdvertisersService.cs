﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Xml;

/// <summary>
/// Summary description for AdvertisersService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class AdvertisersService : System.Web.Services.WebService {

    public AdvertisersService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public List<string> GetSuggestionsAdvertisers(string prefixText)
    {
        Dictionary<Guid, string> list = AdvertisersListV;
        IEnumerable<string> query = (from x in list
                                     where x.Value.ToLower().StartsWith(prefixText.ToLower())
                                     select x.Value).Take(10);
        return query.ToList();
    }
    [WebMethod(true)]
    public string GetOneSuggestionsAdvertiser(string prefixText)
    {
        Dictionary<Guid, string> list = AdvertisersListV;
        string query = (from x in list
                        where x.Value.ToLower().StartsWith(prefixText.ToLower())
                        select x.Value).SingleOrDefault();
        return query;
    }
    [WebMethod(true)]
    public Guid GetGuidAdvertiser(string prefixText)
    {
        Dictionary<Guid, string> list = AdvertisersListV;
        Guid query = (from x in list
                      where x.Value.ToLower() == prefixText.ToLower()
                      select x.Key).SingleOrDefault();
        return query;
    }
    Dictionary<Guid, string> LoadGuidAdvertiser(string SiteId)
    {
        Dictionary<Guid, string> list = new Dictionary<Guid, string>();

        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(null, SiteId);
        WebReferenceSite.ResultOfListOfGuidStringPair result = sit.GetAllSupplierNamesAndIds();

        if(result.Type==WebReferenceSite.eResultType.Failure)
            return list;
        
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            list.Add(gsp.Id, gsp.Name);
        }
        return list;
    }
    Dictionary<Guid, string> AdvertisersListV
    {
        get
        {
            if (Session["AdvertisersAutoComplete"] == null || Session["AdvertisersAutoComplete"].GetType() != typeof(Dictionary<Guid, string>))
                Session["AdvertisersAutoComplete"] = LoadGuidAdvertiser(((SiteSetting)Session["Site"]).GetSiteID);
            return (Dictionary<Guid, string>)Session["AdvertisersAutoComplete"];
        }
        set { Session["AdvertisersAutoComplete"] = value; }
    }
    
}

