﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using Telerik.Web.UI;
using System.Collections.Generic;

/// <summary>
/// Summary description for ConsumerService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class ConsumerService : System.Web.Services.WebService {

    public ConsumerService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public RadComboBoxData GetItems(RadComboBoxContext context)
    {
        RadComboBoxData result = new RadComboBoxData();

        List<RadComboBoxItemData> items = new List<RadComboBoxItemData>();
        SiteSetting site_setting = (SiteSetting)Session["Site"];
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(site_setting.GetUrlWebReference);
        WebReferenceSite.GetInitiatorsRequest _request = new WebReferenceSite.GetInitiatorsRequest();
        _request.HelpDeskTypeId = (Guid)Session["HelpDeskTypeId"];
        _request.SearchInput = context.Text;
        WebReferenceSite.ResultOfListOfInitiatorsSearchData _result = null;
        try
        {
            _result = _site.GetInitiators(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, site_setting);
            return result;
        }
        if (_result.Type == WebReferenceSite.eResultType.Failure)
            return result;
        
        foreach (WebReferenceSite.InitiatorsSearchData isd in _result.Value)
        {
            if (string.IsNullOrEmpty(isd.Field3))
                continue;
            RadComboBoxItemData itemData = new RadComboBoxItemData();
  //          itemData.Text = isd.Field1 + " (" + isd.Field3 + ")";
            itemData.Text = isd.Field3;// +" (" + isd.Field1 + ")";
            itemData.Value = isd.Id.ToString();
            items.Add(itemData);
        }
        
        /*
        try
        {


            IEnumerable<DataRow> te = (from x in data.AsEnumerable()
                                       where x.Field<string>("FirstName").StartsWith(context.Text) ||
                                            x.Field<string>("MobilePhone").StartsWith(context.Text)
                                       select x);

            foreach (DataRow row in te)
            {
                RadComboBoxItemData itemData = new RadComboBoxItemData();
                itemData.Text = row["FirstName"].ToString();
                itemData.Value = row["ContactId"].ToString();
                itemData.Attributes.Add("Email", row["Email"].ToString());
                itemData.Attributes.Add("MobilePhone", row["MobilePhone"].ToString());
                items.Add(itemData);
            }
        }
        catch (Exception exc)
        {
            int ttt = 0;
        }
         * */
        result.Items = items.ToArray();
        return result;
    }
    [WebMethod(true)]
    public string GetOneItem(string _context)
    {
        int index = _context.LastIndexOf('(');
       
        if (index > 0)
        {
            _context = _context.Substring(0, index-1);
        }        
        WebReferenceSite.GetInitiatorsRequest _request = new WebReferenceSite.GetInitiatorsRequest();
        _request.HelpDeskTypeId = (Guid)Session["HelpDeskTypeId"];
        _request.SearchInput = _context;
        return _GetOneItem(_request);
    }
    [WebMethod(true)]
    public string GetOneItemByGuid(string _guid)
    {        
        WebReferenceSite.GetInitiatorsRequest _request = new WebReferenceSite.GetInitiatorsRequest();
        _request.HelpDeskTypeId = (Guid)Session["HelpDeskTypeId"];
        _request.InitiatorId = new Guid(_guid);
        if (_request.InitiatorId == Guid.Empty)
            return "";
        return _GetOneItem(_request);
       
    }
    string _GetOneItem(WebReferenceSite.GetInitiatorsRequest _request)
    {
        SiteSetting site_setting = (SiteSetting)Session["Site"];
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(site_setting.GetUrlWebReference);
        WebReferenceSite.ResultOfListOfInitiatorsSearchData _result = null;
        try
        {
            _result = _site.GetInitiators(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, site_setting);
            return "";
        }
        if (_result.Type == WebReferenceSite.eResultType.Failure)
            return "";

        if (_result.Value.Count() == 0)
            return "";
//        return _result.Value[0].Field1 + " (" + _result.Value[0].Field3 + ")" + ";;;;" + _result.Value[0].Id.ToString();
        var ifExists = _result.Value.Where(x => x.Field3 == _request.SearchInput).
            Select(x => new
            {
                Field = x.Field3 + ";;;;" + x.Id.ToString()
     //           Field = x.Field3 + " (" + x.Field1 + ")" + ";;;;" + x.Id.ToString()
            });//.FirstOrDefault().Field;
        string RetVal = (ifExists.Count() > 0) ? ifExists.FirstOrDefault().Field : string.Empty;
        if (!string.IsNullOrEmpty(RetVal))
            return RetVal;

        return _result.Value[0].Field3 + ";;;;" + _result.Value[0].Id.ToString();
    }
    public Guid GetGuidOneItem(string _context)
    {
        int index = _context.LastIndexOf('(');

        if (index > 0)
        {
            _context = _context.Substring(0, index - 1);
        }
        SiteSetting site_setting = (SiteSetting)Session["Site"];
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(site_setting.GetUrlWebReference);
        WebReferenceSite.GetInitiatorsRequest _request = new WebReferenceSite.GetInitiatorsRequest();
        _request.HelpDeskTypeId = (Guid)Session["HelpDeskTypeId"];
        _request.SearchInput = _context;
        WebReferenceSite.ResultOfListOfInitiatorsSearchData _result = null;
        try
        {
            _result = _site.GetInitiators(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, site_setting);
            return Guid.Empty;
        }
        if (_result.Type == WebReferenceSite.eResultType.Failure)
            return Guid.Empty;

        if (_result.Value.Count() == 0)
            return Guid.Empty;
        return _result.Value[0].Id;
    }
    
    
}

