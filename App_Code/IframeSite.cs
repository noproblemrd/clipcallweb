using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Xml;

/// <summary>
/// Summary description for IframeSite
/// </summary>
public class IframeSite
{
    const string DIGIT_REGULAREXPRESSION = @"\d+";
    const string MULTIPLE_AUCTION = "AuctionSuppliersMultiplier";
    string RegularExpressionPhone;
    string PhoneAreaCodeRegularExpression;
    string PhoneMainRegularExpression;
    int MaximimAdvertisers;
    int DefaultNumAdvertisers;
    int MultipleAuction;
    bool DisplayMinisite;

	public IframeSite(string SiteId)
	{
       
        string command = "EXEC dbo.GetSiteDetailBySiteNameId @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteId);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                RegularExpressionPhone = ((reader.IsDBNull(10)) ?
                    (reader.IsDBNull(8) ? DIGIT_REGULAREXPRESSION : reader.GetString(8)) : reader.GetString(10));
                PhoneAreaCodeRegularExpression = (reader.IsDBNull(34)) ? string.Empty : (string)reader["PhoneAreaCodeRegularExpression"];
                PhoneMainRegularExpression = (reader.IsDBNull(35)) ? string.Empty : (string)reader["PhoneMainRegularExpression"];

                MaximimAdvertisers = (int)reader["MaxAdvertisers"];
                DefaultNumAdvertisers = (int)reader["DefaultAdvertisers"];
                DisplayMinisite = (bool)reader["DisplayMinisite"];
            }
            conn.Close();
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(null, SiteId);
        string result = string.Empty;
        try
        {
            result = _site.GetConfigurationSettings();// GetUserTimeZone(userManagement.GetGuid);
        }
        catch (Exception ex) {      return;     }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["Configurations"] == null || xdd["Configurations"]["Error"] != null)
            return;
        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {           
            if (node["Key"].InnerText == MULTIPLE_AUCTION)
            {
                this.MultipleAuction = int.Parse(node["Value"].InnerText);
                break;
            }
        }
	}
    public string GetRegularExpressionPhone
    {
        get { return this.RegularExpressionPhone; }
    }

    public string GetPhoneAreaCodeRegularExpression
    {
        get { return this.PhoneAreaCodeRegularExpression; }
    }

    public string GetPhoneMainRegularExpression
    {
        get { return this.PhoneMainRegularExpression; }
    }

    public int GetMaximimAdvertisers
    {
        get { return this.MaximimAdvertisers; }
    }
    public int GetDefaultNumAdvertisers
    {
        get { return this.DefaultNumAdvertisers; }
    }
    public int GetMultipleAuction
    {
        get { return this.MultipleAuction; }
    }

    public bool GetDisplayMinisite
    {
        get { return this.DisplayMinisite; }
    }
}
