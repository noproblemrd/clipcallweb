﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Caching;

/// <summary>
/// Summary description for ScheduleJob
/// </summary>
public class ScheduleJob
{
    static readonly string LogPath;
    static readonly string FileName;
    static ScheduleJob()
    {
        LogPath = ConfigurationManager.AppSettings["Logs"] + "Services/";
        FileName = "PhoneDidJob_" + System.Net.Dns.GetHostName() + "_" + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName();
    }
    protected Timer timer;
    protected int durationSeconds;
    protected string name;
    protected CacheItemRemovedCallback OnCacheRemove = null;
	public ScheduleJob(int durationSec, string name)
	{
        this.durationSeconds = durationSec;
        this.name = name;
	}
    public void Start()
    {
        OnCacheRemove = new CacheItemRemovedCallback(CacheItemRemoved);
        HttpRuntime.Cache.Insert(name, this, null,
            DateTime.Now.AddSeconds(durationSeconds), Cache.NoSlidingExpiration,
            CacheItemPriority.NotRemovable, OnCacheRemove);
    }

    public void CacheItemRemoved(string k, object v, CacheItemRemovedReason r)
    {
        // do stuff here if it matches our taskname, like WebRequest
        // re-add our task so it recurs
        ScheduleJob sj = (ScheduleJob)v;
        sj.DoTask();
        sj.Start();
    }
    public virtual void DoTask()
    {
        
    }
    public void WriteLog(string message)
    {
        try
        {
            using (FileStream fs = new FileStream(GetFullPath, FileMode.Append))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine("date: " + DateTime.Now.ToString());
                    tw.WriteLine("Host Name:" + System.Net.Dns.GetHostName());
                    tw.WriteLine("Site Name:" + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName());
                    tw.WriteLine("Message: " + message);
                    tw.WriteLine("Environment.StackTrace: " + Environment.StackTrace);
                    tw.WriteLine();
                }
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
    }
    protected string GetFullPath
    {
        get
        {
            DateTime dt = DateTime.Now;
            return LogPath + FileName + GetFixNumber(dt.Day) + GetFixNumber(dt.Month) + dt.Year + ".log";
        }
    }
    protected string GetFixNumber(int num)
    {
        return ((num < 10) ? "0" + num : num.ToString());
    }
}