﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.UI;
using System.Text;
using System.Reflection;
using System.IO;

/// <summary>
/// Summary description for InjectionReportService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class InjectionReportService : System.Web.Services.WebService {
    private const string BASE_PATH = "~/Publisher/SalesControls/";
    public InjectionReportService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetCampainsOfInjection (Guid InjectionId) {
        //        public Result<List<GuidStringPair>> GetOriginsByInjection(Guid injectionId)
        /*
        Guid InjectionId;
        if (!Guid.TryParse(_InjectionId, out InjectionId))
            return string.Empty;
         * */
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionOriginData result = null;
        try
        {
            result = _site.GetOriginsByInjection(InjectionId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return string.Empty;
        }
        if(result.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;
        string campaigns = string.Empty;
        foreach (WebReferenceSite.InjectionOriginData iod in result.Value)
        {
            campaigns += iod.OriginId.ToString() + ";";
        }
        return campaigns;
    }
    [WebMethod]
    public string GetInjectionsByOrigin(Guid OriginId)
    {
        //        public Result<List<GuidStringPair>> GetOriginsByInjection(Guid injectionId)
        /*
        Guid InjectionId;
        if (!Guid.TryParse(_InjectionId, out InjectionId))
            return string.Empty;
         * */
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionOriginData result = null;
        try
        {
            result = _site.GetInjectionsByOrigin(OriginId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return string.Empty;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;
        StringBuilder _injection = new StringBuilder();
        foreach (WebReferenceSite.InjectionOriginData iod in result.Value)
        {
            _injection.Append(iod.InjectionId.ToString() + ";");
        }
        if (_injection.Length > 1)
            _injection.Remove(_injection.Length - 1, 1);
        return _injection.ToString();
    }
    [WebMethod]
    public string PublishInjection(Guid UserId)
    {
        CacheLog cl = new CacheLog();
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        
        bool IsSuccess = true;
        string UserName;
        if (!SalesUtility.TryGetPublisherUserName(UserId, PpcSite.GetCurrent().UrlWebReference, out UserName))
        {
            UserName = "USER NOT FOUND!";
        }
        Parallel.ForEach(xdoc.Element("Sites").Element("SitePPC").Element("WebServers").Elements("Server"),
            new ParallelOptions { MaxDegreeOfParallelism = 3 }, 
            xelem =>
        {
            using (System.Security.Principal.WindowsImpersonationContext wi = PpcSite.GetWindowsImpersonationContext)
            {
                HttpWebResponse response = null;
                //_web++;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(xelem.Value + "PublishInjection?UserId=" + UserId.ToString() + "&UserName=" + UserName);
                request.Timeout = 600000;
                try
                {
                    response = (HttpWebResponse)request.GetResponse();
                }
                catch (Exception exc)
                {
                    // ucr.WebServerFaild++;
                    dbug_log.ExceptionLog(exc);
                    string mess = "PublishInjection faild: " + xelem.Value;
                    //       sb.AppendLine(mess);
                    cl.WriteCacheLog(exc, new string[] { mess }, true);
                    IsSuccess = false;
                }
                HttpWebRequest request2 = (HttpWebRequest)WebRequest.Create(xelem.Value + "PublishPopupEngine?UserId=" + UserId.ToString() + "&UserName=" + UserName);
                try
                {
                    response = (HttpWebResponse)request2.GetResponse();
                }
                catch (Exception exc)
                {
                    // ucr.WebServerFaild++;
                    dbug_log.ExceptionLog(exc);
                    string mess = "PublishPopupEngine faild: " + xelem.Value;
                    //       sb.AppendLine(mess);
                    cl.WriteCacheLog(exc, new string[] { mess }, true);
                    IsSuccess = false;
                }
            }
        });
        return IsSuccess.ToString().ToLower();
        
    }
    [WebMethod(true)]
    public string InjectionReportDrillDown(string date)
    {

        DateTime _from = DateTime.MinValue;
        DateTime _to = DateTime.MinValue;
        List<Guid> Origins = new List<Guid>();
        WebReferenceReports.InjectionReportRequest SessionRequest = (Session["VInjectionReportRequest"] == null) ? null : (WebReferenceReports.InjectionReportRequest)Session["VInjectionReportRequest"];
        if (SessionRequest == null)
            return "Session expired! Please run the report again.";
        WebReferenceReports.InjectionReportRequest _request = new WebReferenceReports.InjectionReportRequest();
        long lng_date;
        if (!long.TryParse(date, out lng_date))
            lng_date = 0;
        if (lng_date == 0)
        {
            _request.FromDate = SessionRequest.FromDate;
            _request.ToDate = SessionRequest.ToDate;
        }
        else
        {
            _request.FromDate = new DateTime(lng_date);
            _request.ToDate = new DateTime(lng_date);
        }
        _request.Country = SessionRequest.Country;
        _request.Origins = SessionRequest.Origins;
        _request.InjectionId = SessionRequest.InjectionId;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(PpcSite.GetCurrent().UrlWebReference);

        WebReferenceReports.ResultOfListOfInjectionReportDrillDownResponse result = null;
        try
        {
            result = _report.InjectionReportDrillDown(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return "Query failed";
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
            return "Query failed";
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "InjectionDrillDown.ascx");
        Type CtlType = ctl.GetType();
        MethodInfo LoadData = CtlType.GetMethod("LoadData");
        LoadData.Invoke(ctl, new object[] { result.Value });
        PropertyInfo RowNum = CtlType.GetProperty("row_num");
        int row_num = (int)(RowNum.GetValue(ctl, null));
        if (row_num == 0)
            return "There are no data!";
        ctl.EnableViewState = false;
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();

    }    
}
