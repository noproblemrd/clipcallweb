﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Reflection;
using System.Text;

/// <summary>
/// Summary description for ErrorManagment
/// </summary>
public class ErrorManagment
{
    public string SiteId { get; set; }
    public Guid UserId { get; set; }
    public string UserName { get; set; }
    public string ClientIP { get; set; }
    public string Browser_Version { get; set; }
    public string Info { get; set; }
	public ErrorManagment()
	{
		//
		
		//
	}
    public static void CatchViewStateException(ViewStateException exc)
    {
        
        string DirectoryName =
         (System.IO.Path.GetDirectoryName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"])).ToLower();
        if (exc.IsConnected)
        {
            string URL = HttpContext.Current.Request.Url.Host;
            string _path = string.Format("{0}://{1}{2}",
                   HttpContext.Current.Request.Url.Scheme,
                   URL + ((HttpContext.Current.Request["SERVER_PORT"] != "" && URL == "localhost") ? (":" + HttpContext.Current.Request["SERVER_PORT"]) : string.Empty),
                   (HttpContext.Current.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Current.Request.ApplicationPath
                   );
            if (DirectoryName.Contains("management") || DirectoryName.Contains("ppc"))
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/Management/LogOut.aspx");
            else if (DirectoryName.Contains("affiliate"))
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/Affiliate/LogOut.aspx");
            
            else
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/Publisher/LogOut.aspx");
        }
         
    }
    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        foreach (PropertyInfo pi in this.GetType().GetProperties())
        {
            object o = pi.GetValue(this, null);
            if (o == null)
                continue;
            sb.AppendLine(pi.Name + " = " + o.ToString());
        }
        return sb.ToString();
        
    }
    
}
