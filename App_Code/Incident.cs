﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Incident
/// </summary>
public enum INCIDENT_STATUS {Init, Active, Completed, NULL}
public enum INCIDENT_ACCOUNT_STATUS { Init, Open, Initiated, Success, NULL }

public class Incident
{
    public string IncidentId { get; set; }
    public INCIDENT_STATUS IncidentStatus { get; set; }
    public string SupplierId { get; set; }
    public int count;
	public Incident(string IncidentId, string IncidentStatus)
	{
        this.IncidentId = IncidentId;
        this.IncidentStatus = GetIncidentStatusFromString(IncidentStatus);
        this.SupplierId = string.Empty;
        this.count = 0;
	}
    public static INCIDENT_STATUS GetIncidentStatusFromString(string _IncidentStatus)
    {
        foreach (INCIDENT_STATUS _is in Enum.GetValues(typeof(INCIDENT_STATUS)))
        {
            if (_is.ToString().ToLower() == _IncidentStatus.ToLower())           
                return _is;           
        }
        return INCIDENT_STATUS.NULL;
    }
    public void SetIncidentStatus(string _IncidentStatus)
    {
        this.IncidentStatus = GetIncidentStatusFromString(_IncidentStatus);
    }
    public static INCIDENT_ACCOUNT_STATUS GetIncidentAccountStatusFromString(string _IncidentStatus)
    {
        foreach (INCIDENT_ACCOUNT_STATUS _is in Enum.GetValues(typeof(INCIDENT_ACCOUNT_STATUS)))
        {
            if (_is.ToString().ToLower() == _IncidentStatus.ToLower())
                return _is;
        }
        return INCIDENT_ACCOUNT_STATUS.NULL;
    }
}
