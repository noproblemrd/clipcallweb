﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WorkingHoursUnit
/// </summary>
public class WorkingHoursUnit
{
    public int FromHour { get; set; }
    public int FromMinute { get; set; }
    public int ToHour { get; set; }
    public int ToMinute { get; set; }
    public bool AllDay { get; set; }
    public List<DayOfWeek> DaysOfWeek { get; set; }
    public int Order { get; set; }

    
}