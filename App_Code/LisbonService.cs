﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Xml;

/// <summary>
/// Summary description for LisbonService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class LisbonService : System.Web.Services.WebService {

    public LisbonService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetLisbonList(string SiteId, string ExpertiseCode, int ExpertiseLevel, string RegionCode,
                    int RegionLevel, string MaxResults, string origionId)
    {
        //SiteId=1&ExpertiseCode=1&ExpertiseLevel=1&RegionCode=3&RegionLevel=1
        //&MaxResults=-1&frames=2&origionId={CF879946-455C-DF11-80CD-0003FF727321}&parentDomain=&Description=&TypeOfBusiness=Mudanחas
        Panel _panel = new Panel();

        Label lbl_Calls = new Label();
        lbl_Calls.Text = "Calls:";
        lbl_Calls.ID = "lbl_Calls";
        _panel.Controls.Add(lbl_Calls);

        Label lbl_Employees = new Label();
        lbl_Employees.Text = "Employees:";
        lbl_Employees.ID = "lbl_Employees";
        _panel.Controls.Add(lbl_Employees);

        Label lbl_Certificate = new Label();
        lbl_Certificate.Text = "Certificate";
        lbl_Certificate.ID = "lbl_Certificate";
        _panel.Controls.Add(lbl_Certificate);

        Label lbl_CallNow = new Label();
        lbl_CallNow.Text = "CALL NOW";
        lbl_CallNow.ID = "lbl_CallNow";
        _panel.Controls.Add(lbl_CallNow);

        Label lbl_ShowAll = new Label();
        lbl_ShowAll.Text = "Show All List";
        lbl_ShowAll.ID = "lbl_ShowAll";
        _panel.Controls.Add(lbl_ShowAll);




        /*
        string SiteId;
        //SiteId = Request["SiteId"].ToString();
        SiteId = Request["SiteId"];
        //Response.Write("SiteId:" + SiteId);
   //     if (!string.IsNullOrEmpty(SiteId))
   //         GetTranslate(SiteId, _panel);

        string ExpertiseCode;
        ExpertiseCode = Request["ExpertiseCode"];

        int ExpertiseLevel;
        ExpertiseLevel = Convert.ToInt32(Request["ExpertiseLevel"]);

        string RegionCode;
        RegionCode = Request["RegionCode"];

        int RegionLevel;
        RegionLevel = Convert.ToInt32(Request["RegionLevel"]);

        */
        int _MaxResults;
        if (!int.TryParse(MaxResults, out _MaxResults))
            _MaxResults = -1;

        /*
        string origionId = "";
        origionId = Request["origionId"];
        //Response.Write("origionId" + origionId);
         * */
        string IfShowMinisite = "True";
        //Response.Write("IfShowMinisite:" + IfShowMinisite);
        
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.SearchRequest searchRequest = new WebReferenceCustomer.SearchRequest();
        searchRequest.ExpertiseCode = ExpertiseCode;
        searchRequest.ExpertiseLevel = ExpertiseLevel;
        searchRequest.MaxResults = _MaxResults;
        searchRequest.RegionCode = RegionCode;
        searchRequest.RegionLevel = RegionLevel;
        searchRequest.SiteId = SiteId;
        searchRequest.OriginId = origionId;

        string xmlSuppliers = customer.SearchSiteSuppliersExpanded(searchRequest);

        //WebReferenceSite.Site siteWebService = WebServiceConfig.GetSiteReference(this);
        //string xmlSuppliers = siteWebService.SearchSiteSuppliers(SiteId, ExpertiseGUID, CityGuid, ExpertiseType, MaxResults);

        //Response.Write("SiteId:" + SiteId + " ExpertiseGUID:" + ExpertiseGUID + " CityGuid:" + CityGuid + " ExpertiseType:" + ExpertiseType + " MaxResults:" + MaxResults);
        //Response.Write(xmlSuppliers);

        System.Text.UTF8Encoding myEncoder = new System.Text.UTF8Encoding();
        byte[] bytes = myEncoder.GetBytes(xmlSuppliers);
        MemoryStream ms = new MemoryStream(bytes);
        XmlTextReader xmlTextReader = new XmlTextReader(ms);
        string SupplierId = "";
        string SupplierNumber = "";
        string SupplierName = "";
        string SumSurvey = "";
        string SumAssistanceRequests = "";
        string NumberOfEmployees = "";
        string Certificate = "";
        string ShortDescription = "";
        string isAvailable = "";
        string DirectNumber = "";
        bool blnlogo = false;
        string ExtensionNumber = "";
        string professionalLogos = "";
        string professionalLogosWeb = "";
        bool ifError = false;
        string _like = "";
        string _dislike = "";

        StringBuilder sb = new StringBuilder();

        /*
         <Suppliers>
            <Supplier>
            <SupplierId />
            <SupplierNumber />
            <SupplierName />
            <SumSurvey />
            <SumAssistanceRequests />
            <NumberOfEmployees />
            <ShortDescription/ >
            <DirectNumber />
            </Supplier>
         </Suppliers>
        */

        //Likes="1" Dislikes="0"

        while (xmlTextReader.Read())
        {
            

            if (xmlTextReader.Name == "Suppliers")
            {

                

                if (xmlTextReader.GetAttribute("ExtensionNumber") != null)
                {
                    ExtensionNumber = xmlTextReader.GetAttribute("ExtensionNumber");
                                       
                }


                

                while (xmlTextReader.Read())
                {
                    if (xmlTextReader.Name == "Error" && xmlTextReader.NodeType == XmlNodeType.Element)
                    {
                        ifError = true;
                        sb.Append("");
                    }

                    if (xmlTextReader.Name == "Error" & xmlTextReader.NodeType == XmlNodeType.EndElement)
                    {                        
                        break;
                    }

                    if (xmlTextReader.Name == "Supplier")
                    {
                        sb.Append("<div class='box1'>");                   

                        SupplierId = xmlTextReader.GetAttribute("SupplierId");
                        SupplierNumber = xmlTextReader.GetAttribute("SupplierNumber");
                        SupplierName = xmlTextReader.GetAttribute("SupplierName");
                        DirectNumber = xmlTextReader.GetAttribute("DirectNumber");

                        _like = xmlTextReader.GetAttribute("Likes");
                        _dislike = xmlTextReader.GetAttribute("Dislikes");
                        
                        string directNumberSograim = "";
                        string directNumberRegular = "";

                        if (DirectNumber != "")
                        {
                            DirectNumber = DirectNumber.Substring(1, DirectNumber.Length - 1);
                            if (DirectNumber.Length > 3)
                            {
                                directNumberSograim = "(" + DirectNumber.Substring(0, 3) + ")";
                                directNumberRegular = DirectNumber.Substring(3, DirectNumber.Length - 3);

                            }
                            DirectNumber = directNumberSograim + " " + directNumberRegular;
                        }

                        else
                            DirectNumber = "";

                        ShortDescription = xmlTextReader.GetAttribute("ShortDescription");
                        //SumSurvey = xmlTextReader.GetAttribute("SumSurvey");
                        SumAssistanceRequests = xmlTextReader.GetAttribute("SumAssistanceRequests");
                        NumberOfEmployees = xmlTextReader.GetAttribute("NumberOfEmployees");
                        Certificate = xmlTextReader.GetAttribute("Certificate");
                        isAvailable = xmlTextReader.GetAttribute("isAvailable");                        

                        professionalLogos = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
                        professionalLogosWeb = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];

                        sb.Append("<div class='left'>");

                            blnlogo = File.Exists(professionalLogos +  SupplierId + ".jpg");
                                sb.Append("<div class='logo'>");
                                if (blnlogo)
                                {
                                    sb.Append("<img width=75 height=59 src='" + professionalLogosWeb +  SupplierId + @".jpg'>");
                                }
                                else
                                {
                                    sb.Append("<img width=75 height=59 src='images/logo.jpg'/>");
                                }
                                sb.Append("</div>");

                                sb.Append("<div class='name'>");

                                    sb.Append("<div class='company'>");                        
                                    if (IfShowMinisite == "True")
                                        sb.Append("<a href='../professional.aspx?supplierGUID={" + SupplierId + "}&SiteId=" + SiteId
                                            + "&DirectNumber=" + DirectNumber + "&ExtensionNumber=" + ExtensionNumber
                                            + "' target='_new'>" + SupplierName + "</a>");
                                    else
                                        sb.Append(SupplierName);
                                    sb.Append("</div>"); // close company

                                    sb.Append("<div class='phone'>");
                                        if (ExtensionNumber != "")
                                            sb.Append(ExtensionNumber + " " + DirectNumber);
                                        else
                                            sb.Append(DirectNumber);
                                    sb.Append("</div>"); // close phone

                                sb.Append("</div>"); // close name

                                sb.Append("<div class='description'>");
                                    sb.Append(ShortDescription);                        
                                sb.Append("</div>"); // close description

                                sb.Append("<div class='tools'>");
                                    sb.Append("<div class='lefttools'>");
                                        sb.Append("<div class='calls'>&nbsp;</div>");
                                        sb.Append("<div class='callstxt'>" + lbl_Calls.Text + ": " + SumAssistanceRequests + "</div>");
                                        sb.Append("<div class='employees'>&nbsp;</div>");
                                        sb.Append("<div class='employeestxt'>" + lbl_Employees.Text + ": " + NumberOfEmployees + "</div>");
                                        sb.Append("<div class='handup'>&nbsp;</div>");
                                        sb.Append("<div class='handuptxt'>" + _like + "</div>");
                                        sb.Append("<div class='handown'>&nbsp;</div>");
                                        sb.Append("<div class='handowntxt'>" + _dislike + "</div>");
                                        sb.Append("<div class='comments'>&nbsp;</div>");
                                        sb.Append("<div class='commentstxt'>Read/Add Reviews</div>");
                                    sb.Append("</div>");// close lefttools

                                    sb.Append("<div class='action'>");
                                             sb.Append("<div class='callto'><img src='images/freecall.jpg' width='51' height='36' /><img src='images/sms.jpg' width='42' height='36' /><img src='images/mail.jpg' width='42' height='36' /><img src='images/fax.jpg' width='42' height='36' /></div>");

                                    sb.Append("</div>"); // close action
                                sb.Append("</div>"); // close tools
                                
                        sb.Append("</div>");// close left                    

                        sb.Append("<div class='right'>");
                        if (ExtensionNumber != "")
                            sb.Append("<div class='bigphone'>" + ExtensionNumber + " " + DirectNumber + "</div>");
                        else
                            sb.Append("<div class='bigphone'>" + DirectNumber + "</div>");
                            sb.Append("<div class='clear'></div>");    
                        sb.Append("</div>"); // close right
                        

                        sb.Append("</div>"); // clocse box1
                    }

                    if (xmlTextReader.Name == "Suppliers" & xmlTextReader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }
                }                

            }


        }

        
        /*
        if (Request["MaxResults"] != null && !ifError)
        {
           
            sb.Append("<a class='showall' target='_new' href='supplierRegular.aspx?SiteId=" + SiteId +
                "&ExpertiseCode=" + ExpertiseCode + "&ExpertiseLevel=" +
                ExpertiseLevel + "&RegionCode=" + RegionCode +
                "&RegionLevel=" + RegionLevel + "'>" + lbl_ShowAll.Text + "</a>");
           
        }
        */
        //sb.Append("<div style='height:20px'>&nbsp;<div>");
        return sb.ToString();   

    }
    [WebMethod]
    public string CreateServicerequest(string SiteId, string ExpertiseCode, int ExpertiseLevel, string RegionCode,
                    int RegionLevel, string origionId, string ContactPhoneNumber, string Description, int NumOfSuppliers)
    {      
  
        LogEdit.SaveLog(SiteId, "Start:" + "\r\nsurfer phone: " +
            ContactPhoneNumber + "\r\nDescription: " + Description +
            "\r\nSession: " +  "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));
        
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.ServiceRequest serviceRequest = new WebReferenceCustomer.ServiceRequest();
        serviceRequest.RegionCode = RegionCode;      
        serviceRequest.RegionLevel = RegionLevel;       
        serviceRequest.ExpertiseCode = ExpertiseCode;       
        serviceRequest.ExpertiseType = ExpertiseLevel;       
        serviceRequest.ContactFullName = "";
        serviceRequest.ContactPhoneNumber = ContactPhoneNumber;     
        serviceRequest.RequestDescription = Description;      
        serviceRequest.PrefferedCallTime = DateTime.Now;
        serviceRequest.SiteId = SiteId;
        Guid MyGuid = new Guid(origionId); 
        serviceRequest.OriginId = MyGuid;

        serviceRequest.NumOfSuppliers = NumOfSuppliers;
        
        WebReferenceCustomer.ServiceResponse serviceResponse = new WebReferenceCustomer.ServiceResponse();
        serviceResponse = customer.CreateServiceRequest(serviceRequest);

        if (serviceResponse.Status.ToString().ToLower() == "success")
        {
            //Response.Write(serviceResponse.ServiceRequestId + " status:" + serviceResponse.Status.ToString().ToLower());
     //       Response.Write(serviceResponse.ServiceRequestId);


            LogEdit.SaveLog(SiteId, "CreateServiceRequest status:" + serviceResponse.Status.ToString().ToLower() +
                "\r\nid: " + serviceResponse.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " +
                ContactPhoneNumber + "\r\nDescription: " + Description +
                "\r\nSession: " +  "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));

            return serviceResponse.ServiceRequestId;

        }

        else
        {
           // Response.Write("unsuccess");
            LogEdit.SaveLog(SiteId, serviceResponse.Status.ToString().ToLower() +
                 "\r\nid: " + serviceResponse.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " +
                ContactPhoneNumber + "\r\nDescription: " + Description +
                "\r\nSession: " +  "\r\nip: " + Utilities.GetIP(HttpContext.Current.Request));
            return "unsuccess";
        }
    }
    [WebMethod]
    public string GetLisbonBubbleList(string SiteId, string incidentId)
    {
        Panel _panel = new Panel();

        Label lbl_Calls = new Label();
        lbl_Calls.Text = "Calls:";
        lbl_Calls.ID = "lbl_Calls";
        _panel.Controls.Add(lbl_Calls);

        Label lbl_Employees = new Label();
        lbl_Employees.Text = "Employees:";
        lbl_Employees.ID = "lbl_Employees";
        _panel.Controls.Add(lbl_Employees);

        Label lbl_Certificate = new Label();
        lbl_Certificate.Text = "Certificate:";
        lbl_Certificate.ID = "lbl_Certificate";
        _panel.Controls.Add(lbl_Certificate);

        Label lbl_CALLING = new Label();
        lbl_CALLING.Text = "CALLING...";
        lbl_CALLING.ID = "lbl_CALLING";
        _panel.Controls.Add(lbl_CALLING);


        Label lbl_CallNow = new Label();
        lbl_CallNow.Text = "CALL NOW";
        lbl_CallNow.ID = "lbl_CallNow";
        _panel.Controls.Add(lbl_CallNow);

        string IfShowMinisite = "True";

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);


        string xmlSuppliers = customer.GetSuppliersStatus(incidentId);
        //LogEdit.SaveLog(SiteId, "customer.GetSuppliersStatus:\r\n" + xmlSuppliers);
        //xmlSuppliers = "<Suppliers IncidentStatus=\"Init\" ><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";
        //Mode="Initiated"
        //xmlSuppliers="<Suppliers IncidentStatus=\"Active\" Mode=\"Open\"><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";
        //xmlSuppliers = "<Suppliers IncidentStatus=\"Active\" Mode=\"Initiated\"><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";
        //xmlSuppliers = "<Suppliers IncidentStatus=\"Completed\" ><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";

        //WebReferenceSite.Site siteWebService = WebServiceConfig.GetSiteReference(this);
        //string xmlSuppliers = siteWebService.SearchSiteSuppliers(SiteId, ExpertiseGUID, CityGuid, ExpertiseType, MaxResults);

        //Response.Write("SiteId:" + SiteId + " ExpertiseGUID:" + ExpertiseGUID + " CityGuid:" + CityGuid + " ExpertiseType:" + ExpertiseType + " MaxResults:" + MaxResults);
        //Response.Write(xmlSuppliers);

        System.Text.UTF8Encoding myEncoder = new System.Text.UTF8Encoding();
        byte[] bytes = myEncoder.GetBytes(xmlSuppliers);
        MemoryStream ms = new MemoryStream(bytes);
        XmlTextReader xmlTextReader = new XmlTextReader(ms);
        string SupplierId = "";
        string SupplierNumber = "";
        string SupplierName = "";
        string SumSurvey = "";
        string SumAssistanceRequests = "";
        string NumberOfEmployees = "";
        string Certificate = "";
        string ShortDescription = "";
        string isAvailable = "";
        string DirectNumber = "";
        bool blnlogo = false;
        int index = 0;
        string classIndex = "";
        string Status = "";
        string ExtensionNumber = "";
        string professionalLogos = "";
        string professionalLogosWeb = "";
        string _like = "";
        string _dislike = "";

        StringBuilder sb = new StringBuilder();

        /*
         <Suppliers>
            <Supplier>
            <SupplierId />
            <SupplierNumber />
            <SupplierName />
            <SumSurvey />
            <SumAssistanceRequests />
            <NumberOfEmployees />
            <ShortDescription/ >
            <DirectNumber />
            </Supplier>
         </Suppliers>
        */



        while (xmlTextReader.Read())
        {
            if (xmlTextReader.Name == "Suppliers")
            {
                sb.Append(@"<input type=""hidden"" id=""_mode"" value=""" + xmlTextReader.GetAttribute("Mode") + @""" />");
                sb.Append(@"<input type=""hidden"" id=""_IncidentStatus"" value=""" + xmlTextReader.GetAttribute("IncidentStatus") + @""" />");

                /*
                sb.Append("<div style='display:none;'>mode=" +
                    xmlTextReader.GetAttribute("Mode") + "mode2;IncidentStatus=" +
                    xmlTextReader.GetAttribute("IncidentStatus") + "IncidentStatus2</div>");
                 * */

                LogEdit.SaveLog(SiteId, "Session: " + " incidentId:" + incidentId + " Mode:" +
                    xmlTextReader.GetAttribute("Mode") +
                    " IncidentStatus:" + xmlTextReader.GetAttribute("IncidentStatus"));


                if (xmlTextReader.GetAttribute("ExtensionNumber") != null)
                {
                    ExtensionNumber = xmlTextReader.GetAttribute("ExtensionNumber");

                }

                while (xmlTextReader.Read())
                {
                    index++;
                    if (xmlTextReader.Name == "Supplier")
                    {
                        sb.Append("<div class='box1'>");

                        SupplierId = xmlTextReader.GetAttribute("SupplierId");
                        SupplierNumber = xmlTextReader.GetAttribute("SupplierNumber");
                        SupplierName = xmlTextReader.GetAttribute("SupplierName");
                        DirectNumber = xmlTextReader.GetAttribute("DirectNumber");
                        ShortDescription = xmlTextReader.GetAttribute("ShortDescription");
                        //SumSurvey = xmlTextReader.GetAttribute("SumSurvey");
                        SumAssistanceRequests = xmlTextReader.GetAttribute("SumAssistanceRequests");
                        NumberOfEmployees = xmlTextReader.GetAttribute("NumberOfEmployees");
                        Certificate = xmlTextReader.GetAttribute("Certificate");
                        isAvailable = xmlTextReader.GetAttribute("isAvailable");

                        Status = xmlTextReader.GetAttribute("Status");
                        _like = xmlTextReader.GetAttribute("Likes");
                        _dislike = xmlTextReader.GetAttribute("Dislikes");

                        professionalLogos = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
                        professionalLogosWeb = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];

                        sb.Append("<div class='left1'>");

                        blnlogo = File.Exists(professionalLogos + SupplierId + ".jpg");

                        sb.Append("<div class='logo'>");
                        if (blnlogo)
                        {
                            sb.Append("<img  width=75 height=59 src='" + professionalLogosWeb + SupplierId + @".jpg'>");
                        }
                        else
                        {
                            sb.Append("<img  width=75 height=59 src='images/logo.jpg'/>");
                        }
                        sb.Append("</div>"); // close logo


                        sb.Append("<div class='name'>");
                        sb.Append("<div class='company'>");
                        if (IfShowMinisite == "True")
                            sb.Append("<a href='../professional.aspx?supplierGUID={" + SupplierId + "}&SiteId=" + SiteId
                                + "&DirectNumber=" + DirectNumber + "&ExtensionNumber=" + ExtensionNumber
                                + "' target='_new'>" + SupplierName + "</a>");
                        else
                            sb.Append(SupplierName);
                        sb.Append("</div>"); // close company
                        sb.Append("</div>"); // close name

                        sb.Append("<div class='description1'>");
                        sb.Append(ShortDescription);
                        sb.Append("</div>"); // close description


                        sb.Append("<div class='tools'>");
                        sb.Append("<div class='calls1'>&nbsp;</div>");
                        sb.Append("<div class='callstxt'1>" + lbl_Calls.Text + ": " + SumAssistanceRequests + "</div>");
                        sb.Append("<div class='employees1'>&nbsp;</div>");
                        sb.Append("<div class='employeestxt1'>" + lbl_Employees.Text + ": " + NumberOfEmployees + "</div>");
                        sb.Append("<div class='handup1'>&nbsp;</div>");
                        sb.Append("<div class='handuptxt1'>" + _like + "</div>");
                        sb.Append("<div class='handown1'>&nbsp;</div>");
                        sb.Append("<div class='handowntxt1'>" + _dislike + "</div>");
                        sb.Append("<div class='comments1'>&nbsp;</div>");
                        sb.Append("<div class='commentstxt1'>Read/Add Reviews</div>");
                        sb.Append("</div>"); // close tools

                        sb.Append("</div>");// close left1 


                        sb.Append("<div class='right1'>");
                        sb.Append("<div class='status'>");
                        if (Status == "Initiated")
                            sb.Append("<img src='images/calling.png' width='92' height='85' />");
                        else if (Status == "Succeed")
                            sb.Append("<img src='images/done.png' width='92' height='85' />");
                        else
                            sb.Append("<img src='images/waiting.png' width='92' height='85' />");
                        sb.Append("</div>");
                        sb.Append("</div>"); // close right1                        

                        sb.Append("</div>"); // clocse box1
                    }

                    if (xmlTextReader.Name == "Suppliers" & xmlTextReader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }
                }

            }


        }


        //sb.Append("<div style='height:30px'>&nbsp;<div>");
        return sb.ToString();
    }

       
     
    
}

