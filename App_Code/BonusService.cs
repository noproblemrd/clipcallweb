﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;


/// <summary>
/// Summary description for BonusService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class BonusService : System.Web.Services.WebService {

    public BonusService()
    {
    }

    [WebMethod(true)]
    public string GetBonus(int _deposite, string BonusType) {
        if(Session["BonusServiceData"] == null || Session["BonusServiceData"].GetType() != typeof(BonusServiceData))
            return "0";
        BonusServiceData bsd = (BonusServiceData)Session["BonusServiceData"];
        if (_deposite < bsd.MinDeposite)
            return "0";
        WebReferenceSupplier.BonusType bt;
        if (!Enum.TryParse(BonusType, out bt))
            return "0";
        if (bt == WebReferenceSupplier.BonusType.None)
            return "0";
        if(bt==WebReferenceSupplier.BonusType.Recharge)
            return CalcBonus(_deposite, bsd.RechargeBonus) + "";
        int BonusPercent = 0;
        int _bonus;
        foreach (KeyValuePair<int, int> kvp in bsd.pricePercentList)
        {
            if (kvp.Key > _deposite)
                break;
            BonusPercent = kvp.Value;            
        }
        _bonus = CalcBonus(_deposite, BonusPercent);
        if (bt == WebReferenceSupplier.BonusType.Package)
            return _bonus + "";
        return (_bonus + CalcBonus(_deposite, bsd.RechargeBonus)) + "";
    }
    int CalcBonus(int _deposite, int BonusPercent)
    {
        double num = (((double)_deposite * (double)BonusPercent) / 100.0);
        return (int)(Math.Round(num, MidpointRounding.AwayFromZero));       
    }
    
}
