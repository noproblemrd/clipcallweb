using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Reflection;

/// <summary>
/// Summary description for InsertTranslate
/// </summary>
//public delegate void myFunc(Control control);
public class InsertTranslate : ScannerControls
{
  //  protected Control _control;
    
    protected Dictionary<string, string> dic;
    /*
	public InsertTranslate(PageSetting page, Dictionary<string, string> dic): base(page)
	{
     //   this.page = page;
        this.dic = dic;
	}
     * */
    public InsertTranslate(PageSetting page):base(page)
    {
 //     this.page = page;
    }
    public InsertTranslate(Control control, Dictionary<string, string> dic):base(control)
    {        
        this.dic = dic;
    }
  
    protected void TranslateIt(Control control)
    {
        
        if (string.IsNullOrEmpty(control.ID) || !dic.ContainsKey(control.ID))
            return;       

        Type controlType = control.GetType();

  //      PropertyInfo[] properties = controlType.GetProperties();
        
        List<PropertyInfo> properties = new List<PropertyInfo>();
        properties.AddRange(controlType.GetProperties());
        int i = 0;
        foreach (PropertyInfo controlProperty in properties)
        {
            i++;
            if (controlProperty.PropertyType != typeof(String))
                continue;
            if (controlProperty.Name == "Text")
            {
                if (HavePropertyName("ErrorMessage", controlType))
                {
                    string stringValue = (string)controlProperty.GetValue(control, null);
                    if (string.IsNullOrEmpty(stringValue) || stringValue == "*")
                        continue;
                }
            }
            if (controlProperty.Name == "ErrorMessage")
            {
                if (HavePropertyName("Text", controlType))
                {
                    string stringValue = (string)controlProperty.GetValue(control, null);
                    if (string.IsNullOrEmpty(stringValue) || stringValue == "*")
                        continue;
                }
            }
            if (controlProperty.Name == "ErrorMessage" || controlProperty.Name == "Text" ||
                controlProperty.Name == "InnerText" || controlProperty.Name == "Value" ||
                controlProperty.Name == "WatermarkText")
                
            {                
                controlProperty.SetValue(control, dic[control.ID], null);
                return;
            }
           
        }

    }
    bool HavePropertyName(string name, Type controlType)
    {
   //      Type controlType = control.GetType();
        List<PropertyInfo> properties = new List<PropertyInfo>();
        properties.AddRange(controlType.GetProperties());
        foreach (PropertyInfo controlProperty in properties)
        {
            if (controlProperty.Name == name && controlProperty.PropertyType == typeof(string))
                return true; //return (string)controlProperty.GetValue(control, null);
            
        }
        return false;
      //  return string.Empty;
    }
  //  void SaveValue(string val, Control control, 
    public void StartTo()
    {
        if (dic.Count == 0)
            return;
        myFunc func = new myFunc(TranslateIt);
        ApplyToControls(_control, func);
    }
    protected override bool SearchChilds(Control control)
    {
        if (!control.HasControls())
            return false;
        /*
        if (control == this._control)
            return true;
        if (control.GetType().IsSubclassOf(typeof(UserControl)))
            return false;
         * */
        return true; 
    }
   
}
