﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PhoneCallCreatedBody
/// </summary>
public class MediaChatBody
{
    [JsonProperty("url")]
    public string url { get; set; }
    [JsonProperty("duration")]
    public int duration { get; set; }
    [JsonProperty("did_call")]
    public bool did_call { get; set; }
    [JsonProperty("id")]
    public Guid id { get; set; }
    public MediaChatBody()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}