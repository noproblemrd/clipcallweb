using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for SiteSetting
/// </summary>
[Serializable()]
public class SiteSetting
{
    /*
    static SiteSetting()
    {
        URL_REFERENCE = (PpcSite.GetCurrent() == null || !PpcSite.GetCurrent().IsYoav) ? _URL_REFERENCE : _URL_ME;
    }
    const string _URL_REFERENCE = @"http://212.143.223.177:1127/OnecallInterfaces/Core/";
    const string _URL_ME = @"http://10.0.0.101/NoProblem.Core.Interfaces/Core/";
     * */
    static string URL_REFERENCE;
    const string DIGIT_REGULAREXPRESSION = @"\d+";
    const string STYLE_NAME = "style.css";
    const string TITLE_PAGE = "NO PROBLEM";
    const string DATE_FORMAT = "{0:MM/dd/yyyy}";
    const string TIME_FORMAT = "{0:HH:mm}";
    const string DATE_TIME_FORMAT = "{0:MM/dd/yyyy HH:mm}";
 //   const string URL_REFERENCE = @"http://10.154.29.254:4444/NoProblemQA/Interfaces/Core/";
    string SiteId;
    string UICulture;
    string encoding;
    string culture;
    string Country;
    string currencySymbol;
    string logoPath;
    bool ifEmailCharging;
    bool ifPaymentCharging;
    bool ifAccountingCharging;
    eCompanyCharging charchingCompany;
	double vat;
    bool ifvatincluded;
    string companyPayPalAccount;
	string payPalEnvironment;
    string payPalReturnPage;
    string payPalCountry;
    string platnosciEnvironment;

    Guid OriginId;
    string dateFormat;
    string timeFormat;

    string UrlWebReference;
    string PhoneRegularExpression;

    string phoneAreaCodeRegularExpression;
    string phoneMainRegularExpression;

    string MobileRegularExpression;
    bool DisplayMinisite;
    string StyleName;
   
    string ZipCodeRegularExpression;

    string platnosciPos_id;
    string platnosciPos_auth_key;

    string Icon;
    string PageTitle;
    string GoogleAnalytics;
    string GoogleCampain;
    int SiteLangId;
    bool IsValidSite;

    public SiteSetting() { }
    
	public SiteSetting(string SiteId)
	{
        IsValidSite = false;
        if (string.IsNullOrEmpty(SiteId))
            return;
        string command = "EXEC dbo.GetSiteDetailBySiteNameId @SiteId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteId", SiteId);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                //      this.UICulture = (reader.IsDBNull(2)) ? string.Empty : reader.GetString(2);
                //      this.culture = (reader.IsDBNull(1)) ? string.Empty : reader.GetString(1);
                //        this.encoding = (reader.IsDBNull(0)) ? string.Empty : reader.GetString(0);
                //        this.Country = (reader.IsDBNull(3)) ? string.Empty : reader.GetString(3);
                this.currencySymbol = (reader.IsDBNull(3)) ? "$" : reader.GetString(3);
                this.logoPath = (reader.IsDBNull(5)) ? string.Empty : reader.GetString(5);
                this.UrlWebReference = (reader.IsDBNull(7)) ? URL_REFERENCE : reader.GetString(7);
                //this.UrlWebReference = URL_REFERENCE;// (reader.IsDBNull(7)) ? URL_REFERENCE : reader.GetString(7);
                this.PhoneRegularExpression = (reader.IsDBNull(8)) ? DIGIT_REGULAREXPRESSION : reader.GetString(8);
                this.ZipCodeRegularExpression = (reader.IsDBNull(9)) ? DIGIT_REGULAREXPRESSION : reader.GetString(9);
                this.MobileRegularExpression = (reader.IsDBNull(10)) ? DIGIT_REGULAREXPRESSION : reader.GetString(10);
                this.OriginId = (reader.IsDBNull(14)) ? Guid.Empty : (Guid)reader["OrigionId"];
                this.DisplayMinisite = (bool)reader["DisplayMinisite"];
                this.StyleName = (reader.IsDBNull(16)) ? STYLE_NAME : (string)reader["StyleName"];
                this.Icon = (reader.IsDBNull(19)) ? string.Empty : (string)reader["Icon"];
                this.PageTitle = (reader.IsDBNull(18)) ? TITLE_PAGE : (string)reader["PageTitle"];
                this.GoogleAnalytics = (reader.IsDBNull(20)) ? string.Empty : (string)reader["GoogleAnalytics"];
                this.IfEmailCharging = (reader.IsDBNull(21)) ? false : (bool)reader["ifEmailCharging"];
                this.IfPaymentCharging = (reader.IsDBNull(22)) ? false : (bool)reader["IfPaymentCharging"];
                this.IfAccountingCharging = (reader.IsDBNull(23)) ? false : (bool)reader["IfAccountingCharging"];
                this.siteLangId = (reader.IsDBNull(24)) ? 0 : (int)reader["DefaultLanguageId"];

                this.Vat = (reader.IsDBNull(26)) ? 0 : Convert.ToDouble(reader["Vat"]);
                this.CompanyPayPalAccount = (reader.IsDBNull(27)) ? string.Empty : (string)reader["CompanyPayPalAccount"];
                this.PayPalEnvironment = (reader.IsDBNull(28)) ? string.Empty : (string)reader["PayPalEnvironment"];
                this.PayPalReturnPage = (reader.IsDBNull(29)) ? string.Empty : (string)reader["PayPalReturnPage"];
                this.PayPalCountry = (reader.IsDBNull(30)) ? string.Empty : (string)reader["PayPalCountry"];
                this.PlatnosciEnvironment = (reader.IsDBNull(31)) ? string.Empty : (string)reader["PlatnosciEnvironment"];
                this.GoogleCampain = (reader.IsDBNull(32)) ? null : (string)reader["GoogleCampain"];
                this.culture = (reader.IsDBNull(33)) ? string.Empty : (string)reader["Culture"];
                this.PhoneAreaCodeRegularExpression = (reader.IsDBNull(34)) ? string.Empty : (string)reader["PhoneAreaCodeRegularExpression"];
                this.PhoneMainRegularExpression = (reader.IsDBNull(35)) ? string.Empty : (string)reader["PhoneMainRegularExpression"];
                this.PlatnosciPos_id = (reader.IsDBNull(36)) ? string.Empty : (string)reader["PlatnosciPos_id"];
                this.PlatnosciPos_auth_key = (reader.IsDBNull(37)) ? string.Empty : (string)reader["PlatnosciPos_auth_key"];
                this.ifVatIncluded = (bool)reader["ifVatIncluded"];
                //  this.CharchingCompany = (reader.IsDBNull(25)) ? string.Empty : (string)reader["CharchingCompany"];
                string cc = (reader.IsDBNull(25)) ? string.Empty : (string)reader["CharchingCompany"];
                eCompanyCharging ecc;
                if (!Enum.TryParse(cc, out ecc))
                    ecc = eCompanyCharging.none;
                charchingCompany = ecc;



                this.dateFormat = DATE_FORMAT;
                this.timeFormat = TIME_FORMAT;
                IsValidSite = true;
            }
            reader.Dispose();
            cmd.Dispose();

            conn.Close();
        }
        this.SiteId = SiteId;		
        
	}
    public SiteSetting(string SiteId, int SiteLangId):this(SiteId)
    {
        this.SiteLangId = SiteLangId;
    }
    public static bool TryGetSiteSetting(string SiteId, out SiteSetting ss)
    {
        ss = new SiteSetting(SiteId);
        return (ss.IsValidSite);
    }
    public int siteLangId
    {
        get { return this.SiteLangId; }
        set 
        { 
            SiteLangId = value;
            this.culture = DBConnection.GetCultureBySiteLangId(value);
        }
    }
    /*
    public void SetSiteLangId(int _SiteLangId)
    {
        this.SiteLangId = _SiteLangId;
        this.culture = DBConnection.GetCultureBySiteLangId(_SiteLangId);
    }
     * */
    public string GetIcon
    {
        get { return this.Icon; }
    }
    public string GetPageTitle
    {
        get { return this.PageTitle; }
    }
    public string GetGoogleAnalytics
    {
        get { return this.GoogleAnalytics; }
    }
    public string GetGoogleCampain
    {
        get { return this.GoogleCampain; }
    }
    public Guid GetOrigionId
    {
        get { return this.OriginId; }
    }
    public string GetStyle
    {
        get { return this.StyleName; }
    }
    public bool HasMiniSite
    {
        get { return this.DisplayMinisite; }
        set { this.DisplayMinisite = value; }
    }
    
    public string GetZipCodeRegularExpression
    {
        get { return this.ZipCodeRegularExpression; }
    }

    public string GetPhoneRegularExpression
    {
        get { return this.PhoneRegularExpression; }
    }


    public string GetMobileRegularExpression
    {
        get { return this.MobileRegularExpression; }
    }
    public string GetMobileAndRegularPhoneExpression()
    {
        string result = "";
        if (!string.IsNullOrEmpty(GetMobileRegularExpression) &&
            GetMobileRegularExpression != DIGIT_REGULAREXPRESSION)
        {
            result = GetMobileRegularExpression;
        }
       
        if (!string.IsNullOrEmpty(GetPhoneRegularExpression) &&
            GetPhoneRegularExpression != DIGIT_REGULAREXPRESSION)
        {
            if (string.IsNullOrEmpty(result))
                result = GetPhoneRegularExpression;
            else
                result = "(" + result + ")|(" + GetPhoneRegularExpression + ")";
        }
        return string.IsNullOrEmpty(result) ? DIGIT_REGULAREXPRESSION : result;
    }
    public string GetUrlWebReference
    {
        get { return UrlWebReference; }
    }
    public string DateFormat//"{0:MM/dd/yyyy}"
    {
        get { return this.dateFormat; }
        set { this.dateFormat = value; }
    }
    public string DateTimeFormat//"{0:MM/dd/yyyy}"
    {
        get { return DATE_TIME_FORMAT; }
       
    }
    public string DateTimeFormatEmpty
    {
        get { return DATE_TIME_FORMAT.Replace("{0:", "").Replace("}", ""); }
    }
    public string TimeFormat
    {
        get { return this.timeFormat; }
        set { this.timeFormat = value; }
    }
    public string DateFormatClean//"{0:MM/dd/yyyy}"
    {
        get 
        {
            string _date = this.dateFormat;
            _date = _date.Replace("{0:", "");
            _date = _date.Replace("}", "");
            return _date; 
        }        
    }
    public string TimeFormatClean
    {
        get 
        {
            string _time = this.timeFormat;
            _time = _time.Replace("{0:", "");
            _time = _time.Replace("}", "");
            return _time; 
         //   return this.timeFormat; 
        }        
    }
    public string LogoPath
    {
        get { return this.logoPath; }
        set { this.logoPath = value; }
    }
   
    public bool IfEmailCharging
    {
        get { return this.ifEmailCharging; }
        set { this.ifEmailCharging = value; }
    }

    public bool IfPaymentCharging
    {
        get { return this.ifPaymentCharging; }
        set { this.ifPaymentCharging = value; }
    }

    public bool IfAccountingCharging
    {
        get { return this.ifAccountingCharging; }
        set { this.ifAccountingCharging = value; }
    }

    public eCompanyCharging CharchingCompany
    {
        get { return this.charchingCompany; }
        set { this.charchingCompany = value; }
    }

    public double Vat
    {
        get { return this.vat; }
        set { this.vat = value; }
    }

    public bool ifVatIncluded
    {
        get { return this.ifvatincluded; }
        set { this.ifvatincluded = value; }
    }

    public string CompanyPayPalAccount
    {
        get { return this.companyPayPalAccount; }
        set { this.companyPayPalAccount = value; }
    }

    public string PayPalEnvironment
    {
        get { return this.payPalEnvironment; }
        set { this.payPalEnvironment = value; }
    }

    public string PayPalReturnPage
    {
        get { return this.payPalReturnPage; }
        set { this.payPalReturnPage = value; }
    }

    public string PayPalCountry
    {
        get { return this.payPalCountry; }
        set { this.payPalCountry = value; }
    }

    public string PlatnosciEnvironment
    {
        get { return this.platnosciEnvironment; }
        set { this.platnosciEnvironment = value; }
    }

   

    public void SetLang(Page page)
    {        
        if (!string.IsNullOrEmpty(encoding))
        {
            page.Request.ContentEncoding = System.Text.Encoding.GetEncoding(encoding);
            page.Response.ContentEncoding = System.Text.Encoding.GetEncoding(encoding);
        }
        if (!string.IsNullOrEmpty(culture))
            page.Culture = culture;
        if (!string.IsNullOrEmpty(UICulture))
            page.UICulture = UICulture;
        
    }
    public static bool LoadSiteId(PageSetting ps, string host)
    {
        bool res = false;
        string siteId;
    //    string host = Request.ServerVariables["SERVER_NAME"];
        if (!string.IsNullOrEmpty(host))
        {
            string command = "SELECT dbo.GetSiteNameIdByHostName(@Host)";
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@Host", host);
                siteId = (string)cmd.ExecuteScalar();
                conn.Close();
            }
            if (!string.IsNullOrEmpty(siteId))
            {
                res = true;
                ps.siteSetting = new SiteSetting(siteId);
                ps.siteSetting.SetSiteSetting(ps);
            }
        }
        return res;
    }
    public static SiteSetting LoadSiteId(string host)
    {
        SiteSetting ss = null;
        string siteId;
        if (!string.IsNullOrEmpty(host))
        {
            string command = "SELECT dbo.GetSiteNameIdByHostName(@Host)";
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@Host", host);
                siteId = (string)cmd.ExecuteScalar();
                conn.Close();
            }
            if (!string.IsNullOrEmpty(siteId))
            {     
                ss = new SiteSetting(siteId);                
            }
        }
        return ss;
    }
    public static string GetStylePage(string SiteId)
    {
        string _style;
        string command = "SELECT dbo.GetStyleBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.Add("@SiteNameId", SiteId);
            _style = (string)cmd.ExecuteScalar();
            conn.Close();
        }
        return string.IsNullOrEmpty(_style) ? STYLE_NAME : _style;
    }
    public static SiteSetting GetSiteSetting(Page page)
    {
        return (page.Session["Site"] == null) ? new SiteSetting() : (SiteSetting)page.Session["Site"];
    }
    public static SiteSetting GetSiteSetting(System.Web.SessionState.HttpSessionState session)
    {
        return (session["Site"] == null) ? new SiteSetting() : (SiteSetting)session["Site"];
    }
    public static string GetDateTimeFormat//"{0:MM/dd/yyyy}"
    {
        get { return DATE_TIME_FORMAT; }

    }
    public string CurrencySymbol
    {
        get { return (string.IsNullOrEmpty(currencySymbol)) ? "$" : currencySymbol; }
        set { currencySymbol = value; }
    }
 
    public void SetSiteSetting(Page page)
    {
        page.Session["Site"] = this;
    }
   
    public string GetSiteID
    {
        get { return this.SiteId; }
    }

    public string Culture
    {
        get { return this.culture; }
        set { this.culture = value; }
    }

    public string PhoneAreaCodeRegularExpression
    {
        get { return this.phoneAreaCodeRegularExpression; }
        set { this.phoneAreaCodeRegularExpression = value; }
    }

    public string PhoneMainRegularExpression
    {
        get { return this.phoneMainRegularExpression; }
        set { this.phoneMainRegularExpression = value; }
    }

    public string PlatnosciPos_id
    {
        get { return this.platnosciPos_id; }
        set { this.platnosciPos_id = value; }
    }
    public string GetDefaultTimeFormat
    {
        get { return TIME_FORMAT; }
    }

    public string PlatnosciPos_auth_key
    {
        get { return this.platnosciPos_auth_key; }
        set { this.platnosciPos_auth_key = value; }
    }
    public string CrmDateFormat
    {
        get { return DATE_FORMAT; }
    }
    
    
}
