﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Xml;

/// <summary>
/// Summary description for RegionService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class RegionService : System.Web.Services.WebService {

    public RegionService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public string SetList(int _level, string _Guid) {
   //     Guid _id = Guid.NewGuid();
        Dictionary<Guid, string> list = new Dictionary<Guid, string>();
        string siteId = ((SiteSetting)Session["Site"]).GetSiteID;
        WebReferenceSupplier.Supplier _sp = WebServiceConfig.GetSupplierReference(null, siteId);
        if (string.IsNullOrEmpty(_Guid))
        {
            
            string result = string.Empty;
            try
            {
                result = _sp.GetRegions(_level);
            }
            catch (SoapException ex)
            {
                return "false";
            }
            XmlDataDocument xdd = new XmlDataDocument();
            xdd.LoadXml(result);

            if (xdd["Areas"] == null || xdd["Areas"]["Error"] != null)
                return "false";
           
            foreach (XmlNode node in xdd["Areas"].ChildNodes)
            {
                string name = node.Attributes["Name"].InnerText;
       //         string engName = node.Attributes["EnglishName"].InnerText;
                string RegionId = node.Attributes["RegionId"].InnerText;
                if (string.IsNullOrEmpty(name))
                    continue;
                list.Add(new Guid(RegionId), name);
            }
        }
        else
        {
            WebReferenceSupplier.GetRegionsInLevelByAncestorRequest request = new WebReferenceSupplier.GetRegionsInLevelByAncestorRequest();
            request.AncestorId = (new Guid(_Guid));
            request.Level = _level;
            WebReferenceSupplier.ResultOfGetRegionsInLevelByAncestorResponse _response = null;
            try
            {
                _response = _sp.GetRegionsInLevelByAncestor(request);
            }
            catch (Exception ex)
            {
                return "false";
            }
            if(_response.Type == WebReferenceSupplier.eResultType.Failure)
                return "false";
            foreach (WebReferenceSupplier.RegionData rd in _response.Value.RegionDataList)
            {
                list.Add(rd.RegionId, rd.RegionName);
            }
        }
        if (list.Count == 0)
            return "false";
        list = SortDic(list);
        Session["region" + _level] = list;
        return "true";

    }
    [WebMethod(true)]
    public string SetListCode(int _level)
    {
        string siteId = ((SiteSetting)Session["Site"]).GetSiteID;
        WebReferenceSupplier.Supplier _sp = WebServiceConfig.GetSupplierReference(null, siteId);
        Dictionary<string, string> list = new Dictionary<string, string>();
        string result = string.Empty;
        try
        {
            result = _sp.GetRegions(_level);
        }
        catch (SoapException ex)
        {
            return "false";
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);

        if (xdd["Areas"] == null || xdd["Areas"]["Error"] != null)
            return "false";

        foreach (XmlNode node in xdd["Areas"].ChildNodes)
        {
            string name = node.Attributes["Name"].InnerText;
            //         string engName = node.Attributes["EnglishName"].InnerText;
            string RegionCode = node.Attributes["RegionCode"].InnerText;
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(RegionCode))
                continue;
            list.Add(RegionCode, name);
        }
        Session["region" + _level] = list;
        return "true";
    }
    [WebMethod(true)]
    public string GetGuidRegion(int _level, string name, int childLevel)
    {
        if (Session["region" + _level] == null)
            return "false";
        Dictionary<Guid, string> list = (Dictionary<Guid, string>)Session["region" + _level];
        if (list.ContainsValue(name))
        {     
            var _key = list.Where(x => x.Value == name).Select(x => new { key = x.Key }).First();
            string result = SetList(childLevel, _key.key.ToString());
            if (result == "false")
                return "false";
            return _key.key.ToString();           
        }
        return "false";
    }
    /*
    Guid GetGuidByName(Dictionary<Guid, string> list)
    {
        foreach (KeyValuePair<Guid, string> kvp in list)
        {

        }
    }
     * */
    [WebMethod(EnableSession = true)]
    public string[] GetSuggestions(string prefixText, int count, string contextKey)
    {
        prefixText = prefixText.ToLower();
        Dictionary<Guid, string> list = (Dictionary<Guid, string>)Session["region" + contextKey];
        List<string> responses = new List<string>();
        foreach (KeyValuePair<Guid, string> kvp in list)
        {

            if (kvp.Value.ToLower().StartsWith(prefixText))
            {

                responses.Add(kvp.Value);
                if (responses.Count >= count)
                    break;
            }
        }
        return responses.ToArray();
    }
    [WebMethod(EnableSession = true)]
    public string[] GetSuggestionsCode(string prefixText, int count, string contextKey)
    {
        prefixText = prefixText.ToLower();
        Dictionary<string, string> list = (Dictionary<string, string>)Session["region" + contextKey];
        List<string> responses = new List<string>();
        foreach (KeyValuePair<string, string> kvp in list)
        {

            if (kvp.Value.ToLower().StartsWith(prefixText))
            {

                responses.Add(kvp.Value);
                if (responses.Count >= count)
                    break;
            }
        }
        return responses.ToArray();
    }
    [WebMethod(EnableSession = true)]
    public bool chkMatcReturnCorrect(string prefixText, string _level)
    {
        Dictionary<Guid, string> list = (Dictionary<Guid, string>)Session["region" + _level];
        return list.ContainsValue(prefixText);
    }
    public string GetGuid(string name, string _level)
    {
        Dictionary<Guid, string> list = (Dictionary<Guid, string>)Session["region" + _level];
        if (list == null || list.Count == 0)
            return string.Empty;
        var _key = list.Where(x => x.Value == name).Select(x => new { key = x.Key }).FirstOrDefault();
        if (_key == null || _key.key == null)
            return string.Empty;
        return _key.key.ToString();
    }
    
    [WebMethod(EnableSession = true)]
    public string chkMatcReturnCorrectCity(string prefixText, string _level)
    {
        Dictionary<string, string> list = (Dictionary<string, string>)Session["region" + _level];
        if (list.ContainsValue(prefixText))
            return prefixText + ";" + list.Where(x=>x.Value==prefixText).First().Key;
        string TempCity = string.Empty;
        for(int i=0;i<prefixText.Length;i++)
        {
            
            
            var query = list.Where(x => x.Value.StartsWith(prefixText.Substring(0, i + 1)));

            if (query.Count() == 0)
                return TempCity;
            TempCity = query.First().Value + ";" + query.First().Key;
               
        }
        return TempCity;
    }
    Dictionary<Guid, string> SortDic(Dictionary<Guid, string> dic)
    {
        Dictionary<Guid, string> newDic = new Dictionary<Guid, string>();
        List<KeyValuePair<Guid, string>> l = new List<KeyValuePair<Guid, string>>(dic);
        l.Sort(delegate(KeyValuePair<Guid, string> first, KeyValuePair<Guid, string> next)
        {
            return first.Value.CompareTo(next.Value); // ascending
          //  return next.Value.CompareTo(first.Value); // descending
        });
   //     dic.Clear();
        foreach (KeyValuePair<Guid, string> item in l)
        {
            newDic.Add(item.Key, item.Value);
        }
        return newDic;
    }

    
}

