﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CompanyCharging
/// </summary>
public class CompanyCharging
{
	public CompanyCharging()
	{
		//
		
		//
	}
    public static NameValueCollection GetChargingCompanyDetails(SiteSetting ss)
    {
        string serverName = System.Web.HttpContext.Current.Request.Url.Host;
        string chargingCompany =  ss.CharchingCompany.ToString();
        NameValueCollection nvc = new NameValueCollection();
        string command = "EXEC dbo.GetCompanyChargingDetails @company";

        using (SqlConnection conn = DBConnection.GetConnString())
        {
            if (!String.IsNullOrEmpty(chargingCompany))
            {
                /*
                eCompanyCharging ecc;
                if (!Enum.TryParse(ss.CharchingCompany, out ecc))
                    return nvc;
                */
                //   string PageUrl = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + ResolveUrl("~");
                string PageUrl = string.Format("{0}://{1}{2}",
                       HttpContext.Current.Request.Url.Scheme,
                       serverName + ((HttpContext.Current.Request["SERVER_PORT"] != "" && serverName == "localhost") ? (":" + HttpContext.Current.Request["SERVER_PORT"]) : string.Empty),
                       (HttpContext.Current.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Current.Request.ApplicationPath
                       );
                PageUrl += @"/";
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@company", ss.CharchingCompany.ToString());
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    string _value = ((reader["Value"] == DBNull.Value) ? "" : (string)reader["Value"]);
                    if (!string.IsNullOrEmpty(_value) && (string)reader["Type"] == eChargingKeyValueType.URL.ToString())
                        _value = PageUrl + _value;
                    nvc.Add((string)reader["Key"], _value);

                }

                reader.Close();
                conn.Close();

            }
        }
        return nvc;

    }

    public static string GetPelecardForm(PageSetting ps, int depositAgorot, string auto_id, int maxPayments)
    {
        NameValueCollection nvc = GetChargingCompanyDetails(ps.siteSetting);

        nvc.Add("total", depositAgorot + "");
        nvc.Add("Parmx", auto_id);
        nvc.Add("maxPayments", maxPayments.ToString());
        return HttpHelper.PreparePOSTForm(@"https://gateway.pelecard.biz/", nvc);
    }
    public static string GetPelecardFormQA(PageSetting ps, int depositAgorot, string auto_id, int maxPayments)
    {
        NameValueCollection nvc = GetChargingCompanyDetails(ps.siteSetting);

        nvc.Add("total", depositAgorot + "");
        nvc.Add("Parmx", auto_id);
        nvc.Add("maxPayments", maxPayments.ToString());
        nvc["authNum"] = "123456";
        return HttpHelper.PreparePOSTForm(@"https://gateway.pelecard.biz/", nvc);
    }

    
    public static string GetPlimusForm(PageSetting ps, int deposit , string auto_id , string CompanyName, string FirstName, string SecondName, string Email, string StreetNumber,string City,string State,string PostalCode,string Country, string PhoneNumber,string Fax)
    {
       return GetPlimusForm(ps.siteSetting, deposit , auto_id , CompanyName, FirstName, SecondName, Email, StreetNumber,City, State, PostalCode, Country, PhoneNumber, Fax);
        
    }
    public static string GetPlimusForm(SiteSetting ss, int deposit, string auto_id, string CompanyName, string FirstName, string SecondName, string Email, string StreetNumber, string City, string State, string PostalCode, string Country, string PhoneNumber, string Fax)
    {
        NameValueCollection nvc = GetChargingCompanyDetails(ss);

        nvc.Add("overridePrice", deposit + "");
        nvc.Add("custom1", auto_id); // send custom1 and recieve from ipn supplierid. the connection is made in the plimus administration 
        nvc.Add("companyName", CompanyName);
        nvc.Add("firstName", FirstName);        
        nvc.Add("lastName", SecondName);
        nvc.Add("email", Email);
        nvc.Add("validateEmail", Email);
        nvc.Add("address1", StreetNumber);
        nvc.Add("city", City);
        nvc.Add("state", State);
        nvc.Add("zipCode", PostalCode);
        nvc.Add("country", Country); // link reference for full country list) Country http://www.plimus.com/jsp/buynow_countries.jsp        
        nvc.Add("workPhone", PhoneNumber);
        nvc.Add("faxNumber", Fax);

        /*     
            nvc["url"]
            test environment https://sandbox.plimus.com/jsp/buynow.jsp 
            production environment  https://www.plimus.com/jsp/buynow.jsp                                         
                          
        */


        return HttpHelper.PreparePOSTForm(nvc["url"], nvc);

    }


    public static string GetPlimusIframe(SiteSetting ss, decimal deposit, string auto_id, string CompanyName, string FirstName, string SecondName, string Email, string StreetNumber, string City, string State, string PostalCode, string Country, string PhoneNumber, string Fax, string plan)
    {
        NameValueCollection nvc = GetChargingCompanyDetails(ss);

        if (plan == "FEATURED_LISTING")
        {
            nvc["contractId"] = nvc["contractId_FEATURED_LISTING"];
        }

        else if (plan == "LEAD_BOOSTER_AND_FEATURED_LISTING")
        {
            nvc["contractId"] = nvc["contractId_LEAD_BOOSTER_AND_FEATURED_LISTING"];
        }

        else if (plan == "FEATURED_LISTING_dashboard")
        {
            nvc["contractId"] = nvc["contractId_FEATURED_LISTING_dashboard"];
        }

        else if (plan == "LEAD_BOOSTER_AND_FEATURED_LISTING_dashboard")
        {
            nvc["contractId"] = nvc["contractId_LEAD_BOOSTER_AND_FEATURED_LISTING_dashboard"];
        }

        

        nvc.Remove("contractId_FEATURED_LISTING");
        nvc.Remove("contractId_LEAD_BOOSTER_AND_FEATURED_LISTING");
        nvc.Remove("contractId_FEATURED_LISTING_dashboard");
        nvc.Remove("contractId_LEAD_BOOSTER_AND_FEATURED_LISTING_dashboard");

        nvc.Add("overridePrice", deposit + "");
        nvc.Add("custom1", auto_id); // send custom1 and recieve from ipn supplierid. the connection is made in the plimus administration 
        nvc.Add("companyName", CompanyName);
        nvc.Add("firstName", FirstName);        
        nvc.Add("lastName", SecondName);         
        nvc.Add("email", Email);        
        nvc.Add("validateEmail", Email);
        nvc.Add("address1", StreetNumber);
        nvc.Add("city", City);
        nvc.Add("state", State);        
        //nvc.Add("zipCode", PostalCode);        
        if (!String.IsNullOrEmpty(Country) && Country.ToLower() == "usa")
            Country = "us";
        nvc.Add("country", Country); // link reference for full country list) Country http://www.plimus.com/jsp/buynow_countries.jsp 
        nvc.Add("workPhone", PhoneNumber);
        nvc.Add("faxNumber", Fax);



        /*     
            nvc["url"]
            test environment https://sandbox.plimus.com/jsp/buynow.jsp 
            production environment  https://www.plimus.com/jsp/buynow.jsp                                         
                          
        */


        return HttpHelper.PrepareQueryStringIframe(nvc["url"], nvc);

    }


    public static string GetInterInfoForm(PageSetting ps, int deposit, string auto_id ,string guid)
    {
        NameValueCollection nvc = GetChargingCompanyDetails(ps.siteSetting);

        nvc.Add("amount", deposit.ToString());
        nvc.Add("order_nr", auto_id);
        nvc.Add("guid", guid);

        return HttpHelper.PreparePOSTForm(@"http://www.1188.ee/noproblem_payment.php", nvc);

    }

    public static string GetPaypalForm(PageSetting ps, int depositAgorot,
        string FirstName, string SecondName, string Email, string Currency,
        string lbl_CurrentBalanceID, string lbl_CurrentBalanceText, string CurrenBalance,
        bool IsFirstTime)
    {
        NameValueCollection nvc = GetChargingCompanyDetails(ps.siteSetting);

        nvc.Add("business", ps.siteSetting.CompanyPayPalAccount);
        nvc.Add("first_name", FirstName);
        nvc.Add("last_name", SecondName);
        nvc.Add("email", Email);
        nvc.Add("country", ps.siteSetting.PayPalCountry);
        nvc.Add("lc", ps.siteSetting.PayPalCountry);
        nvc.Add("currency_code", Currency);
        nvc.Add("tax_rate", ps.siteSetting.Vat + "");
        nvc.Add("os0", lbl_CurrentBalanceID);
        nvc.Add("os1", lbl_CurrentBalanceText);
        nvc.Add("os2", CurrenBalance);
        nvc.Add("os3", ps.userManagement.GetGuid);
        nvc.Add("os4", ps.userManagement.User_Name);
        nvc.Add("os5", IsFirstTime.ToString());
        nvc.Add("os6", ps.siteSetting.GetSiteID);
        nvc.Add("os7", ps.siteSetting.siteLangId + "");
        nvc.Add("os8", ps.siteSetting.PayPalEnvironment);
        nvc.Add("amount", depositAgorot + "");
        
        return HttpHelper.PreparePOSTForm(ps.siteSetting.PayPalEnvironment, nvc);
    }
    public static string GetPlatnosciForm(PageSetting ps, int depositAgorot,
        string FirstName, string SecondName, string Email, string PhoneNumber,
        string lbl_CurrentBalanceID, string lbl_CurrentBalance, string balanceNew,
        bool IsFirstTime, string Street, string HouseNumber,
        string City, string PostalCode, string CompanyName, string _type,
        string PpcPackage, string bonus, string lbl_YourBalanceText, string bonusType)

         /*
        __form = CompanyCharging.GetPlatnosciForm(this, _deposit, FirstName, SecondName,
               Email, PhoneNumber, lbl_CurrentBalance.ID, lbl_CurrentBalance.Text, _new_balance,
               IsFirstTimeV, dicValueV[lbl_YourBalance.ID], Street, HouseNumber, City, PostalCode,
               CompanyName, "", Hidden_PpcPackage.Value, Int32.Parse(txt_AddDeposite.Text).ToString());
        */
    {
        NameValueCollection nvc = GetChargingCompanyDetails(ps.siteSetting);

        
        nvc.Add("first_name", FirstName);
        nvc.Add("last_name", SecondName);
        nvc.Add("email", Email);
        nvc.Add("phone", PhoneNumber);
        nvc.Add("street", Street);
        nvc.Add("HouseNumber", HouseNumber);
        nvc.Add("city", City);
        nvc.Add("post_code", PostalCode);
        nvc.Add("desc", PpcPackage);
        
 //       nvc.Add("pos_id", ps.siteSetting.PlatnosciPos_id);
  //      nvc.Add("pos_auth_key", ps.siteSetting.PlatnosciPos_auth_key);
        nvc.Add("session_id", ps.GetGuidSetting() + DateTime.Now.ToString());
        nvc.Add("client_ip", Utilities.GetIP(HttpContext.Current.Request));
        if (!string.IsNullOrEmpty(_type))
        {
            if(nvc.AllKeys.Contains("pay_type")) /* t for test environment. c for credit card environment . empty for all kinds of opportunities */
                nvc.Remove("pay_type");
            nvc.Add("pay_type", _type);
        }// ps.siteSetting.PlatnosciEnvironment); /* t for test environment. c for credit card environment . empty for all kinds of opportunities */
        // desc2 is limited to 1024 chars 
        nvc.Add("desc2", "supplierId=" + ps.GetGuidSetting() + "&advertiserName=" + CompanyName.Replace("\"", "&quot;") +
            "&balanceOld=" + lbl_CurrentBalance + "&userId=" + ps.userManagement.GetGuid + "&userName=" +
            ps.userManagement.User_Name.Replace("\"", "&quot;") + "&siteId=" + ps.siteSetting.GetSiteID + "&siteLangId=" +
            ps.siteSetting.siteLangId + "&filedId=" + lbl_CurrentBalanceID + "&fieldName=" +
            lbl_YourBalanceText.Replace("\"", "&quot;") +
            "&isFirstTimeV=" + IsFirstTime.ToString() + "&bonusAmount=" + bonus +
            "&balanceNew=" + balanceNew + "&bonusType=" + bonusType);

     
        string depositGrosz = (depositAgorot * 100).ToString(); /*amount in grosz Grosz, a coin used in Poland as a hundredth part of 1 złoty. Złoty is Polish currency*/
        nvc.Add("amount", depositGrosz);
        return HttpHelper.PreparePOSTForm(@"https://www.platnosci.pl/paygw/UTF/NewPayment", nvc);
        
    }
}