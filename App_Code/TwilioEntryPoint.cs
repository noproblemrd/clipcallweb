﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.IO;

/// <summary>
/// Summary description for TwilioEntryPoint
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
//[System.Web.Script.Services.ScriptService]
public class TwilioEntryPoint : System.Web.Services.WebService
{

    public TwilioEntryPoint()
    {
    }

    #region AAR Calls Methods

    //[WebMethod]
    //public void OutboundCallFallback()
    //{
    //    string CallSid = Context.Request.Params["CallSid"];
    //    string SiteId = Context.Request.Params["SiteId"];
    //    string AccountSid = Context.Request.Params["AccountSid"];
    //    string From = Context.Request.Params["From"];
    //    string To = Context.Request.Params["To"];
    //    string CallStatus = Context.Request.Params["CallStatus"];
    //    string ApiVersion = Context.Request.Params["ApiVersion"];
    //    string Direction = Context.Request.Params["Direction"];
    //    string IncidentAccountId = Context.Request.Params["IncidentAccountId"];
    //    string ErrorCode = Context.Request.Params["ErrorCode"];
    //    string ErrorUrl = Context.Request.Params["ErrorUrl"];
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.OutboundCallFallback(
    //            CallSid, AccountSid, From, To,
    //        CallStatus, ApiVersion, Direction, IncidentAccountId,
    //         ErrorCode, ErrorUrl);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in OutboundCallFallback. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, IncidentAccountId:{8}, ErrorCode:{9}, ErrorUrl:{10}",
    //            CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, IncidentAccountId, ErrorCode, ErrorUrl));
    //    }
    //}

    //[WebMethod]
    //public void OutboundCallStatusCallback(
    //    string CallDuration, string CallSid, string AccountSid, string From, string To,
    //    string CallStatus, string ApiVersion, string Direction, string SiteId, string IncidentAccountId
    //    )
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        twilioClient.OutboundCallStatusCallback(CallDuration, CallSid, AccountSid, From, To,
    //          CallStatus, ApiVersion, Direction, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write("<Response />");
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in OutboundCallStatusCallback. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, IncidentAccountId:{8}",
    //            CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, IncidentAccountId));
    //    }
    //}

    //[WebMethod]
    //public void OutboundCallConsultation(
    //    string CallSid, string AccountSid, string From, string To,
    //    string CallStatus, string ApiVersion, string Direction, string SiteId, string IncidentAccountId
    //    )
    //{
    //    string answeredBy = null;
    //    try
    //    {
    //        answeredBy = Context.Request.Params["AnsweredBy"];
    //        if (string.IsNullOrEmpty(answeredBy))
    //        {
    //            answeredBy = "unknown";
    //        }
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.OutboundCallConsultation(CallSid, AccountSid, From, To,
    //         CallStatus, ApiVersion, Direction, IncidentAccountId, answeredBy);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in OutboundCallConsultation. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, IncidentAccountId:{8}, AnsweredBy:{9}",
    //            CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, IncidentAccountId, answeredBy));
    //    }
    //}

    //[WebMethod]
    //public void OutboundCallClickHandler(string Digits, string CallSid, string IncidentAccountId, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.OutboundCallClickHandler(Digits, CallSid, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in OutboundCallClickHandler. CallSid:{0}, Digits:{1}, IncidentAccountId:{2}, SiteId:{3}",
    //            CallSid, Digits, IncidentAccountId, SiteId));
    //    }
    //}

    //[WebMethod]
    //public void ConsumerCallStatusCallback(
    //   string DialCallStatus, string IncidentAccountId, string SiteId
    //   )
    //{
    //    string duration = string.Empty;
    //    string recordUrl = string.Empty;
    //    string dialCallSid = string.Empty;
    //    try
    //    {
    //        duration = Context.Request.Params["DialCallDuration"];
    //        if (string.IsNullOrEmpty(duration))
    //        {
    //            duration = "0";
    //        }
    //        recordUrl = Context.Request.Params["RecordingUrl"];
    //        if (recordUrl == null)
    //        {
    //            recordUrl = string.Empty;
    //        }
    //        dialCallSid = Context.Request.Params["DialCallSid"];
    //        if (dialCallSid == null)
    //        {
    //            dialCallSid = "---";
    //        }
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        twilioClient.ConsumerCallStatusCallback(DialCallStatus, dialCallSid, duration, recordUrl, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write("<Response />");
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in ConsumerCallStatusCallback. DialCallStatus:{0}, DialCallSid:{1}, DialCallDuration:{2}, SiteId:{3}, RecordingUrl:{4}, IncidentAccountId:{5}",
    //            DialCallStatus, dialCallSid, duration, SiteId, recordUrl, IncidentAccountId));
    //    }
    //}

    //[WebMethod]
    //public void RepresentativeStatusCallback(
    //   string DialCallStatus, string DialCallSid, string IncidentAccountId, string SiteId
    //   )
    //{
    //    string duration = string.Empty;
    //    string recordUrl = string.Empty;
    //    bool isIncoming = false;
    //    try
    //    {
    //        duration = Context.Request.Params["DialCallDuration"];
    //        if (string.IsNullOrEmpty(duration))
    //        {
    //            duration = "0";
    //        }
    //        recordUrl = Context.Request.Params["RecordingUrl"];
    //        if (recordUrl == null)
    //        {
    //            recordUrl = string.Empty;
    //        }
    //        string isIncomingStr = Context.Request.Params["IsIncoming"];
    //        if (string.IsNullOrEmpty(isIncomingStr) == false && isIncomingStr.ToLower() == "true")
    //        {
    //            isIncoming = true;
    //        }
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.RepresentativeStatusCallback(DialCallStatus, DialCallSid, duration, recordUrl, IncidentAccountId, isIncoming);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in RepresentativeStatusCallback. DialCallStatus:{0}, DialCallSid:{1}, DialCallDuration:{2}, SiteId:{3}, RecordingUrl:{4}, IncidentAccountId:{5}, IsIncoming:{6}",
    //            DialCallStatus, DialCallSid, duration, SiteId, recordUrl, IncidentAccountId, isIncoming.ToString()));
    //    }
    //}

    //[WebMethod]
    //public void SupplierRecordHandler(
    //    string RecordingUrl, string CallSid, string IncidentAccountId, string SiteId)
    //{
    //    bool isIncoming = false;
    //    try
    //    {
    //        string isIncomingStr = Context.Request.Params["IsIncoming"];
    //        if (string.IsNullOrEmpty(isIncomingStr) == false && isIncomingStr.ToLower() == "true")
    //        {
    //            isIncoming = true;
    //        }
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.SupplierRecordHandler(RecordingUrl, CallSid, IncidentAccountId, isIncoming);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in SupplierRecordHandler. RecordingUrl:{0}, CallSid:{1}, IncidentAccountId:{2}, SiteId:{3}, IsIncoming:{4}",
    //            RecordingUrl, CallSid, IncidentAccountId, SiteId, isIncoming.ToString()));
    //    }
    //}

    #endregion

    #region Incoming Calls Methods

    //[WebMethod]
    //public void IncomingCallConsultation(
    //    string CallSid, string From, string To, string CallStatus, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.IncomingCallConsultation(CallSid, From, To, CallStatus);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in OutboundCallConsultation. CallSid:{0}, From:{1}, To:{2}, CallStatus:{3}, SiteId:{4}",
    //            CallSid, From, To, CallStatus, SiteId));
    //    }
    //}

    //[WebMethod]
    //public void IncomingCallFallBack(
    //    string CallSid, string From, string To,
    //    string CallStatus, string ErrorCode, string ErrorUrl, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.IncomingCallFallBack(CallSid, From, To, CallStatus, ErrorCode, ErrorUrl);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in IncomingCallFallBack. CallSid:{0}, From:{1}, To:{2}, CallStatus:{3}, SiteId:{4}",
    //            CallSid, From, To, CallStatus, SiteId));
    //    }
    //}

    //[WebMethod]
    //public void IncomingCallStatusCallback(
    //    string CallDuration, string CallSid, string CallStatus, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        twilioClient.IncomingCallStatusCallback(CallDuration, CallSid, CallStatus);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write("<Response />");
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in IncomingCallStatusCallback. CallSid:{0}, CallDuration, CallStatus:{3}, SiteId:{4}",
    //            CallSid, CallDuration, CallStatus, SiteId));
    //    }
    //}

    //[WebMethod]
    //public void IncomingCallClickHandler(string Digits, string CallSid, string IncomingCallId, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.IncomingCallClickHandler(Digits, CallSid, IncomingCallId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in IncomingCallClickHandler. CallSid:{0}, Digits:{1}, IncidentAccountId:{2}, SiteId:{3}",
    //            CallSid, Digits, IncomingCallId, SiteId));
    //    }
    //}

    #endregion

    #region Machine Test Methods

    //[WebMethod]
    //public void MachineCheckFallback()
    //{
    //    string CallSid = Context.Request.Params["CallSid"];
    //    string SiteId = Context.Request.Params["SiteId"];
    //    string AccountSid = Context.Request.Params["AccountSid"];
    //    string From = Context.Request.Params["From"];
    //    string To = Context.Request.Params["To"];
    //    string CallStatus = Context.Request.Params["CallStatus"];
    //    string ApiVersion = Context.Request.Params["ApiVersion"];
    //    string Direction = Context.Request.Params["Direction"];
    //    string SupplierId = Context.Request.Params["SupplierId"];
    //    string ErrorCode = Context.Request.Params["ErrorCode"];
    //    string ErrorUrl = Context.Request.Params["ErrorUrl"];
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.MachineCheckFallback(CallSid, AccountSid, From, To,
    //         CallStatus, ApiVersion, Direction, SupplierId,
    //         ErrorCode, ErrorUrl);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in MachineCheckFallback. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, ErrorCode:{8}, ErrorUrl:{9}, SupplierId:{10}",
    //             CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, ErrorCode, ErrorUrl, SupplierId));
    //    }
    //}

    //[WebMethod]
    //public void MachineCheckStatusCallback(
    //    string CallSid, string AccountSid, string From, string To,
    //    string CallStatus, string ApiVersion, string Direction, string SiteId, string SupplierId
    //    )
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        twilioClient.MachineCheckStatusCallback(CallSid, AccountSid, From, To,
    //          CallStatus, ApiVersion, Direction, SupplierId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write("<Response />");
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in MachineCheckStatusCallback. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, SupplierId:{8}",
    //             CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, SupplierId));
    //    }
    //}

    //[WebMethod]
    //public void MachineCheckConsultation(
    //   string CallSid, string AccountSid, string From, string To,
    //    string CallStatus, string ApiVersion, string Direction, string SiteId, string SupplierId, string AnsweredBy
    //    )
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.MachineCheckConsultation(CallSid, AccountSid, From, To,
    //         CallStatus, ApiVersion, Direction, SupplierId, AnsweredBy);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in MachineCheckStatusCallback. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, SupplierId:{8}, AnsweredBy:{9}",
    //             CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, SupplierId, AnsweredBy));
    //    }
    //}

    #endregion

    #region IvrWidget Methods

    [WebMethod]
    public void IvrWidgetConsultation(
        string CallSid, string From, string To, string CallStatus, string SiteId)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.IvrWidgetConsultation(CallSid, From, To, CallStatus);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in IvrWidgetConsultation. CallSid:{0}, From:{1}, To:{2}, CallStatus:{3}, SiteId:{4}",
                CallSid, From, To, CallStatus, SiteId));
        }
    }

    [WebMethod]
    public void IvrWidgetDataMethods(
        string Digits, string CallSid, string IvrWidgetCallId, string SiteId, string Method, int tryCount)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            var methodInfo = typeof(WebReferenceTwilio.TwilioEntryPoint).GetMethod(Method);
            var ans = methodInfo.Invoke(twilioClient, new object[] { Digits, CallSid, IvrWidgetCallId, tryCount });
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in IvrWidgetDataMethods. CallSid:{0}, Digits:{1}, To:{2}, IvrWidgetCallId:{3}, SiteId:{4}, Method:{5}",
                CallSid, Digits, IvrWidgetCallId, SiteId, Method));
        }
    }

    [WebMethod]
    public void IvrWidgetStatusCallback(
        string CallDuration, string CallSid, string CallStatus, string SiteId
        )
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.IvrWidgetStatusCallback(CallDuration, CallSid, CallStatus);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in IvrWidgetStatusCallback. CallSid:{0}, CallDuration, CallStatus:{3}, SiteId:{4}",
                CallSid, CallDuration, CallStatus, SiteId));
        }
    }

    [WebMethod]
    public void IvrWidgetFallBack(
        string CallSid, string From, string To,
        string CallStatus, string ErrorCode, string ErrorUrl, string SiteId)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.IvrWidgetFallBack(CallSid, From, To, CallStatus, ErrorCode, ErrorUrl);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in IvrWidgetFallBack. CallSid:{0}, From:{1}, To:{2}, CallStatus:{3}, SiteId:{4}",
                CallSid, From, To, CallStatus, SiteId));
        }
    }

    #endregion

    #region Auto Upsale Call Back Methods

    [WebMethod]
    public void AutoUpsaleCallBackFallback()
    {
        string CallSid = Context.Request.Params["CallSid"];
        string SiteId = Context.Request.Params["SiteId"];
        string AccountSid = Context.Request.Params["AccountSid"];
        string From = Context.Request.Params["From"];
        string To = Context.Request.Params["To"];
        string CallStatus = Context.Request.Params["CallStatus"];
        string ApiVersion = Context.Request.Params["ApiVersion"];
        string Direction = Context.Request.Params["Direction"];
        string Id = Context.Request.Params["Id"];
        string ErrorCode = Context.Request.Params["ErrorCode"];
        string ErrorUrl = Context.Request.Params["ErrorUrl"];
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.AutoUpsaleCallBackFallback(
                CallSid, AccountSid, From, To,
            CallStatus, ApiVersion, Direction, Id,
             ErrorCode, ErrorUrl);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in AutoUpsaleCallBackFallback. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, Id:{8}, ErrorCode:{9}, ErrorUrl:{10}",
                CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, Id, ErrorCode, ErrorUrl));
        }
    }

    [WebMethod]
    public void AutoUpsaleCallBackConsultation(
        string CallSid, string AccountSid, string From, string To,
        string CallStatus, string ApiVersion, string Direction, string SiteId, string Id, string HeadingIvrName
        )
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.AutoUpsaleCallBackConsultation(CallSid, AccountSid, From, To,
             CallStatus, ApiVersion, Direction, Id, HeadingIvrName);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in AutoUpsaleCallBackConsultation. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, Id:{8}, , HeadingIvrName:{9}",
                CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, Id, HeadingIvrName));
        }
    }

    [WebMethod]
    public void AutoUpsaleCallBackStatusCallback(
     string CallDuration, string CallSid, string AccountSid, string From, string To,
        string CallStatus, string ApiVersion, string Direction, string SiteId, string Id
        )
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.AutoUpsaleCallBackStatusCallback(CallDuration, CallSid, AccountSid, From, To,
              CallStatus, ApiVersion, Direction, Id);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in AutoUpsaleCallBackStatusCallback. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, Id:{8}",
                CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, Id));
        }
    }

    [WebMethod]
    public void AutoUpsaleCallBackMainClickHandler(string Digits, string CallSid, string Id, string SiteId, string HeadingIvrName)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.AutoUpsaleCallBackMainClickHandler(Digits, CallSid, Id, HeadingIvrName);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in AutoUpsaleCallBackMainClickHandler. CallSid:{0}, Digits:{1}, Id:{2}, SiteId:{3}, HeadingIvrName:{4}",
                CallSid, Digits, Id, SiteId, HeadingIvrName));
        }
    }

    [WebMethod]
    public void AutoUpsaleCallBackSecondaryClickHandler(string Digits, string CallSid, string Id, string SiteId, string HeadingIvrName)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.AutoUpsaleCallBackSecondaryClickHandler(Digits, CallSid, Id, HeadingIvrName);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in AutoUpsaleCallBackSecondaryClickHandler. CallSid:{0}, Digits:{1}, Id:{2}, SiteId:{3}, HeadingIvrName:{4}",
                CallSid, Digits, Id, SiteId, HeadingIvrName));
        }
    }

    #endregion

    #region Lead Buyers Methods

    [WebMethod]
    public void LeadBuyersCustomerCallAnswered(string CallSid, string AccountSid, string From, string To,
        string CallStatus, string ApiVersion, string Direction, string ids, string SiteId)
    {
        string answeredBy = null;
        try
        {
            //answeredBy = Context.Request.Params["AnsweredBy"];
            //if (string.IsNullOrEmpty(answeredBy))
            //{
            //    answeredBy = "unknown";
            //}
            answeredBy = "human";
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.LeadBuyersCustomerCallAnswered(CallSid, AccountSid, From, To,
                CallStatus, ApiVersion, Direction, answeredBy, ids);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in LeadBuyersCustomerCallAnswered. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, ids:{8}, AnsweredBy:{9}",
                CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, ids, answeredBy));
        }
    }

    [WebMethod]
    public void LeadBuyersCallConferenceCallBack(string CallSid, string ids, string SiteId)
    {
        string recordingUrl = string.Empty;
        try
        {
            recordingUrl = Context.Request.Params["RecordingUrl"];
            if (recordingUrl == null)
            {
                recordingUrl = string.Empty;
            }
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.LeadBuyersCallConferenceCallBack(CallSid, recordingUrl, ids);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in LeadBuyersCallConferenceCallBack. CallSid:{0}, ids:{1}, SiteId:{2}, recordingUrl = {3}",
                CallSid, ids, SiteId, recordingUrl));
        }
    }

    [WebMethod]
    public void LeadBuyersCustomerCallCallback(string CallDuration, string CallSid, string AccountSid, string From, string To,
        string CallStatus, string ApiVersion, string Direction, string ids, string SiteId)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.LeadBuyersCustomerCallCallback(CallDuration, CallSid, AccountSid, From, To,
              CallStatus, ApiVersion, Direction, ids);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in LeadBuyersCustomerCallCallback. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, ids:{8}",
                CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, ids));
        }
    }

    [WebMethod]
    public void LeadBuyersBrokerCallAnswered(string CallSid, string AccountSid, string From, string To,
        string CallStatus, string ApiVersion, string Direction, string ids, string SiteId)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.LeadBuyersBrokerCallAnswered(CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, ids);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format(
                "Exception in LeadBuyersBrokerCallAnswered. CallSid = {0}, AccountSid = {1}, From = {2}, To = {3}, CallStatus = {4}, ApiVersion = {5}, Direction = {6}, ids = {7}, SiteId = {8}",
                CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, ids, SiteId));
        }
    }

    [WebMethod]
    public void LeadBuyersBrokerCallCallback(string CallDuration, string CallSid, string AccountSid, string From, string To,
        string CallStatus, string ApiVersion, string Direction, string ids, string SiteId)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.LeadBuyersBrokerCallCallback(CallDuration, CallSid, AccountSid, From, To,
              CallStatus, ApiVersion, Direction, ids);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in LeadBuyersBrokerCallCallback. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, ids:{8}",
                CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, ids));
        }
    }

    [WebMethod]
    public void LeadBuyersCallClickHandler(string Digits, string CallSid, string ids, string SiteId)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.LeadBuyersCallClickHandler(ids, Digits, CallSid);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in LeadBuyersCallClickHandler. CallSid:{0}, Digits:{1}, ids:{2}, SiteId:{3}",
                CallSid, Digits, ids, SiteId));
        }
    }

    #endregion

    #region New AAR Methods

    //[WebMethod]
    //public void AarAdvertiserConsultation(string CallSid, string AccountSid, string From, string To,
    //    string CallStatus, string ApiVersion, string Direction, string AnsweredBy, string IncidentAccountId, string SiteId)
    //{
    //    string answeredBy = null;
    //    try
    //    {
    //        answeredBy = Context.Request.Params["AnsweredBy"];
    //        if (string.IsNullOrEmpty(answeredBy))
    //        {
    //            answeredBy = "unknown";
    //        }
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.AarAdvertiserConsultation(CallSid, AccountSid, From, To,
    //            CallStatus, ApiVersion, Direction, answeredBy, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in AarAdvertiserConsultation. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, IncidentAccountId:{8}, AnsweredBy:{9}",
    //            CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, IncidentAccountId, answeredBy));
    //    }
    //}

    //[WebMethod]
    //public void AarClickHandler(string Digits, string CallSid, string IncidentAccountId, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.AarClickHandler(Digits, CallSid, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in AarClickHandler. CallSid:{0}, Digits:{1}, IncidentAccountId:{2}, SiteId:{3}",
    //            CallSid, Digits, IncidentAccountId, SiteId));
    //    }
    //}

    //[WebMethod]
    //public void AarConferenceCallBack(string CallSid, string IncidentAccountId, string SiteId)
    //{
    //    string recordingUrl = string.Empty;
    //    try
    //    {
    //        recordingUrl = Context.Request.Params["RecordingUrl"];
    //        if (recordingUrl == null)
    //        {
    //            recordingUrl = string.Empty;
    //        }
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        twilioClient.AarConferenceCallBack(CallSid, recordingUrl, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write("<Response />");
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in AarConferenceCallBack. CallSid:{0}, IncidentAccountId:{1}, SiteId:{2}, recordingUrl = {3}",
    //            CallSid, IncidentAccountId, SiteId, recordingUrl));
    //    }
    //}

    //[WebMethod]
    //public void AarConsumerConsultation(string CallSid, string AccountSid, string From, string To,
    //    string CallStatus, string ApiVersion, string Direction, string IncidentAccountId, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.AarConsumerConsultation(CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format(
    //            "Exception in AarConsumerConsultation. CallSid = {0}, AccountSid = {1}, From = {2}, To = {3}, CallStatus = {4}, ApiVersion = {5}, Direction = {6}, IncidentAccountId = {7}, SiteId = {8}",
    //            CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, IncidentAccountId, SiteId));
    //    }
    //}

    //[WebMethod]
    //public void AarFallBackAdvertiser()
    //{
    //    string CallSid = Context.Request.Params["CallSid"];
    //    string SiteId = Context.Request.Params["SiteId"];
    //    string AccountSid = Context.Request.Params["AccountSid"];
    //    string From = Context.Request.Params["From"];
    //    string To = Context.Request.Params["To"];
    //    string CallStatus = Context.Request.Params["CallStatus"];
    //    string ApiVersion = Context.Request.Params["ApiVersion"];
    //    string Direction = Context.Request.Params["Direction"];
    //    string IncidentAccountId = Context.Request.Params["IncidentAccountId"];
    //    string ErrorCode = Context.Request.Params["ErrorCode"];
    //    string ErrorUrl = Context.Request.Params["ErrorUrl"];
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.AarFallBackAdvertiser(
    //            CallSid, AccountSid, From, To,
    //        CallStatus, ApiVersion, Direction, ErrorCode,
    //         ErrorUrl, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in AarFallBackAdvertiser. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, IncidentAccountId:{8}, ErrorCode:{9}, ErrorUrl:{10}",
    //            CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, IncidentAccountId, ErrorCode, ErrorUrl));
    //    }
    //}

    //[WebMethod]
    //public void AarFallBackConsumer()
    //{
    //    string CallSid = Context.Request.Params["CallSid"];
    //    string SiteId = Context.Request.Params["SiteId"];
    //    string AccountSid = Context.Request.Params["AccountSid"];
    //    string From = Context.Request.Params["From"];
    //    string To = Context.Request.Params["To"];
    //    string CallStatus = Context.Request.Params["CallStatus"];
    //    string ApiVersion = Context.Request.Params["ApiVersion"];
    //    string Direction = Context.Request.Params["Direction"];
    //    string IncidentAccountId = Context.Request.Params["IncidentAccountId"];
    //    string ErrorCode = Context.Request.Params["ErrorCode"];
    //    string ErrorUrl = Context.Request.Params["ErrorUrl"];
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.AarFallBackConsumer(
    //            CallSid, AccountSid, From, To,
    //        CallStatus, ApiVersion, Direction, ErrorCode,
    //         ErrorUrl, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in AarFallBackConsumer. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, IncidentAccountId:{8}, ErrorCode:{9}, ErrorUrl:{10}",
    //            CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, IncidentAccountId, ErrorCode, ErrorUrl));
    //    }
    //}

    //[WebMethod]
    //public void AarStatusCallBackAdvertiser(string CallDuration, string CallSid, string AccountSid, string From, string To,
    //    string CallStatus, string ApiVersion, string Direction, string IncidentAccountId, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        twilioClient.AarStatusCallBackAdvertiser(CallDuration, CallSid, AccountSid, From, To,
    //          CallStatus, ApiVersion, Direction, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write("<Response />");
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in AarStatusCallBackAdvertiser. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, IncidentAccountId:{8}",
    //            CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, IncidentAccountId));
    //    }
    //}

    //[WebMethod]
    //public void AarStatusCallBackConsumer(string CallDuration, string CallSid, string AccountSid, string From, string To,
    //    string CallStatus, string ApiVersion, string Direction, string IncidentAccountId, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        twilioClient.AarStatusCallBackConsumer(CallDuration, CallSid, AccountSid, From, To,
    //          CallStatus, ApiVersion, Direction, IncidentAccountId);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write("<Response />");
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in AarStatusCallBackConsumer. CallSid:{0}, AccountSid:{1}, From:{2}, To:{3}, CallStatus:{4}, ApiVersion:{5}, Direction:{6}, SiteId:{7}, IncidentAccountId:{8}",
    //            CallSid, AccountSid, From, To, CallStatus, ApiVersion, Direction, SiteId, IncidentAccountId));
    //    }
    //}

    //[WebMethod]
    //public void ConsumerHangUpRedirect(string CallSid, string SiteId)
    //{
    //    try
    //    {
    //        var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
    //        string ans = twilioClient.ConsumerHangUpRedirect(CallSid);
    //        Context.Response.ContentType = "application/xml";
    //        Context.Response.Output.Write(ans);
    //    }
    //    catch (Exception exc)
    //    {
    //        dbug_log.ExceptionLog(exc, string.Format("Exception in ConsumerHangUpRedirect. CallSid:{0}",
    //            CallSid));
    //    }
    //}

    #endregion

    #region Password Call Methods

    [WebMethod]
    public void PasswordCallAnswered()
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            string id = Context.Request.Params["id"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.PasswordCallAnswered(id);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in PasswordCallAnswered"));
        }
    }

    [WebMethod]
    public void PasswordCallClickHandler(string Digits, string CallSid, string id)
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.PasswordCallClickHandler(Digits, CallSid, id);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in PasswordCallClickHandler"));
        }
    }

    #endregion

    #region Expired Call Methods

    [WebMethod]
    public void ExpiredCallAnswered()
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            string callId = Context.Request.Params["callId"];
            string againStr = Context.Request.Params["again"];
            bool again = false;
            if (!string.IsNullOrEmpty(againStr))
            {
                again = bool.Parse(againStr);
            }
            string callStatus = Context.Request.Params["CallStatus"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.ExpiredCallAnswered(callId, callStatus, again);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in ExpiredCallAnswered"));
        }
    }

    [WebMethod]
    public void ExpiredCallClickHandler(string Digits, string CallSid, string callId)
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.ExpiredCallClickHandler(callId, Digits);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in ExpiredCallClickHandler"));
        }
    }

    [WebMethod]
    public void ExpiredCallCallback(string CallStatus, string callId)
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.ExpiredCallCallback(CallStatus, callId);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in ExpiredCallClickHandler"));
        }
    }

    [WebMethod]
    public void ExpiredRepresentativeCallback(string callId)
    {
        try
        {
            string recordUrl = Context.Request.Params["RecordingUrl"];
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.ExpiredRepresentativeCallback(recordUrl, callId);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in ExpiredRepresentativeCallback"));
        }
    }

    #endregion

    [WebMethod]
    public void GetStartedCallTestFakeCustomer()
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.GetStartedCallTestFakeConsumer();
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in GetStartedCallTestFakeCustomer"));
        }
    }

    [WebMethod]
    public void TestCallAnswered(
            string CallSid, string AccountSid, string From, string To,
            string CallStatus, string Direction, string TestCallId)
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.TestCallAnswered(CallSid, AccountSid, From, To, CallStatus, Direction, TestCallId);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in TestCallAnswered"));
        }
    }

    [WebMethod]
    public void TestCallClickHandler(
        string Digits, string CallSid, string TestCallId)
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.TestCallClickHandler(Digits, CallSid, TestCallId);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in TestCallClickHandler"));
        }
    }

    [WebMethod]
    public void TestCallCallback(
        string CallDuration, string CallSid, string AccountSid, string From, string To,
           string CallStatus, string TestCallId)
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.TestCallCallback(CallDuration, CallSid, AccountSid, From, To, CallStatus, TestCallId);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in TestCallCallback"));
        }
    }

    [WebMethod]
    public void TestCallConsumerCallback(
            string DialCallStatus,
            string TestCallId)
    {
        string duration = string.Empty;
        string recordUrl = string.Empty;
        string dialCallSid = string.Empty;
        try
        {
            duration = Context.Request.Params["DialCallDuration"];
            if (string.IsNullOrEmpty(duration))
            {
                duration = "0";
            }
            recordUrl = Context.Request.Params["RecordingUrl"];
            if (recordUrl == null)
            {
                recordUrl = string.Empty;
            }
            dialCallSid = Context.Request.Params["DialCallSid"];
            if (dialCallSid == null)
            {
                dialCallSid = "---";
            }
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.TestCallConsumerCallback(DialCallStatus, dialCallSid, duration, recordUrl, TestCallId);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in TestCallConsumerCallback"));
        }
    }

    [WebMethod]
    public void GetRequests(int n, string siteId)
    {
        Context.Response.ContentType = "application/xml";
        var reference = WebServiceConfig.GetSupplierReference(null, siteId);
        var result = reference.GetLastCases(n);
        string utf8EncodedXml;
        var serializer = new System.Xml.Serialization.XmlSerializer(typeof(WebReferenceSupplier.ResultOfListOfCustomerPageCaseData));
        using (var memoryStream = new MemoryStream())
        {
            using (var streamWriter = new StreamWriter(memoryStream, System.Text.Encoding.UTF8))
            {
                serializer.Serialize(streamWriter, result);
                memoryStream.Seek(0, SeekOrigin.Begin);
                using (var streamReader = new StreamReader(memoryStream, System.Text.Encoding.UTF8))
                {
                    utf8EncodedXml = streamReader.ReadToEnd();
                }
            }
        }
        Context.Response.Output.Write(utf8EncodedXml);
    }

    #region Vienna

    [WebMethod]
    public void ViennaConsultation(
        string CallSid, string From, string To, string CallStatus, string SiteId)
    {
        try
        {
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            string ans = twilioClient.ViennaConsultation(CallSid, From, To, CallStatus);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in OutboundCallConsultation. CallSid:{0}, From:{1}, To:{2}, CallStatus:{3}, SiteId:{4}",
                CallSid, From, To, CallStatus, SiteId));
        }
    }

    [WebMethod]
    public void ViennaDialStatusCallback(
       string DialCallStatus, string DialCallSid, string SiteId, string IsUnmapped,
                    string IncomingCallSid, string CustomerPhone, string VirtualNumber, string AdvertiserPhone
       )
    {
        string duration = string.Empty;
        string recordUrl = string.Empty;
        try
        {
            duration = Context.Request.Params["DialCallDuration"];
            if (string.IsNullOrEmpty(duration))
            {
                duration = "0";
            }
            recordUrl = Context.Request.Params["RecordingUrl"];
            if (recordUrl == null)
            {
                recordUrl = string.Empty;
            }
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.ViennaDialStatusCallback(DialCallStatus, DialCallSid, duration, recordUrl, IsUnmapped.ToLower() == "true", IncomingCallSid, CustomerPhone, VirtualNumber, AdvertiserPhone);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, "Exception in ViennaDialStatusCallback.");
        }
    }

    [WebMethod]
    public void ViennaIncomingCallStatusCallBack(
        string CallDuration, string CallSid, string CallStatus, string From, string To)
    {
        try
        {
            string SiteId = Context.Request.Params["SiteId"];
            var twilioClient = WebServiceConfig.GetTwilioReference(null, SiteId);
            twilioClient.ViennaIncomingCallStatusCallback(CallDuration, CallSid, CallStatus, From, To);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write("<Response />");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in ViennaIncomingCallStatusCallBack"));
        }
    }
    [WebMethod]
    public void IvrGetApp()
    {
        try
        {
            /*
            string str = "";
            System.Collections.Specialized.NameValueCollection nv = Context.Request.Params;
            foreach (string key in nv)
            {
                str += key + "=" + nv[key] + "; ";
            }
            
            dbug_log.ExceptionLog(new Exception(), str);
             */
            
            string from = Context.Request.QueryString["From"];
            string to = Context.Request.QueryString["To"];
            var twilioClient = WebServiceConfig.GetTwilioReference(PpcSite.GetCurrent().UrlWebReference);
            string ans = twilioClient.IvrGetTheApp(from, to);
            Context.Response.ContentType = "application/xml";
            Context.Response.Output.Write(ans);
             
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, string.Format("Exception in GetStartedCallTestFakeCustomer"));
        }
    }
    #endregion
}
