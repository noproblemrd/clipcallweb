﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Reflection;
using System.Web.UI;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Data;

/// <summary>
/// Summary description for AuctionService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class AuctionService : System.Web.Services.WebService {
    readonly string professionalLogos = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
    readonly string professionalLogosWeb = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];

    public AuctionService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    
    [WebMethod(true)]
    public string createServiceRequest(string _phone, string _desc, string _expertise, string ExpCode, string RegionCode, int NumOfSuppliers)
    {
        
            SiteSetting ss = (SiteSetting)Session["Site"];
            if(ss==null)
            {
                ss= SiteSetting.LoadSiteId(Context.Request.ServerVariables["SERVER_NAME"]);
                if(ss==null)
                    return "Failed";
                Session["Site"] = ss;
            }
            //       WebReferenceCustomer.SearchRequest searchRequest = (WebReferenceCustomer.SearchRequest)Session["searchRequest"];
            WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(ss.GetUrlWebReference);
            WebReferenceCustomer.ServiceRequest serviceRequest = new WebReferenceCustomer.ServiceRequest();
            serviceRequest.RegionCode = RegionCode;
     //       serviceRequest.ServiceAreaCode = RegionCode;// 1;
            serviceRequest.RegionLevel = 2;
    //        serviceRequest.ServiceAreaType = 2;
            serviceRequest.ExpertiseCode = ExpCode;
            //serviceRequest.ExpertiseCode = 7;
            serviceRequest.ExpertiseType = 1;
            //serviceRequest.ExpertiseType = 1;
            serviceRequest.ContactFullName = "";
            serviceRequest.ContactPhoneNumber = _phone;
            //serviceRequest.ContactPhoneNumber = "0526135849";
            serviceRequest.RequestDescription = _desc;
            //serviceRequest.RequestDescription = "some Text";
            serviceRequest.PrefferedCallTime = DateTime.Now;
            serviceRequest.SiteId = ss.GetSiteID;
            serviceRequest.OriginId = new Guid("{A543E7BB-C606-E011-BADF-001517D1792A}"); //NoProblemIsrael
    //        serviceRequest.OriginId = new Guid("{35779B9E-759D-DF11-92C8-A4BADB37A26F}"); ///qa
      //      serviceRequest.OriginId = new Guid("{CF879946-455C-DF11-80CD-0003FF727321}");       //dev          
 //           serviceRequest.OriginId = ss.GetOrigionId;

            serviceRequest.NumOfSuppliers = NumOfSuppliers;

            WebReferenceCustomer.ServiceResponse serviceResponse = new WebReferenceCustomer.ServiceResponse();
            //      serviceResponse = customer.CreateServiceRequest(serviceRequest);


            //      IAsyncResult ar = customer.BeginCreateServiceRequest(serviceRequest, null, null);
            //       serviceResponse = customer.EndCreateServiceRequest(ar);
            //      try
            //       {
            serviceResponse = customer.CreateServiceRequest(serviceRequest);
            //     }
            //     catch (Exception exc)
            //     {
            //          return exc.Message;
            //      }
      
            if (serviceResponse.Status == WebReferenceCustomer.StatusCode.NoSuppliers)
                return "NoSuppliers";
            if (serviceResponse.Status != WebReferenceCustomer.StatusCode.Success)
                return "Failed";
            string _incidentId = serviceResponse.ServiceRequestId;
            Page page = new Page();
            UserControl ctl =
                (UserControl)page.LoadControl("~/LP_Israel/Message.ascx");
            //    MethodInfo _translate = ctl.GetType().GetMethod("SetTranslate");
            //    _translate.Invoke(ctl, new object[] { ss.GetSiteID });
            MethodInfo m_load = ctl.GetType().GetMethod("LoadData");
            m_load.Invoke(ctl, new object[] { _expertise, _incidentId });
            ctl.EnableViewState = false;

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
                {
                    ctl.RenderControl(textWriter);
                }
            }
            Dictionary<string, Incident> dic = IncidentV;
            dic.Add(_incidentId, new Incident(_incidentId, "Init"));
            IncidentV = dic;
            return sb.ToString();
        

    }
    [WebMethod(true)]
    public string CheckAuction(string _incidentId)
    {

        string _path = string.Format("{0}://{1}{2}",
                HttpContext.Current.Request.Url.Scheme,
                HttpContext.Current.Request.ServerVariables["HTTP_HOST"],
                (HttpContext.Current.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Current.Request.ApplicationPath
                );  
            SiteSetting ss = (SiteSetting)Session["Site"];            
            if (ss == null)
            {
                ss = SiteSetting.LoadSiteId(Context.Request.ServerVariables["SERVER_NAME"]);
                if (ss == null)
                    return INCIDENT_STATUS.Completed.ToString();
                Session["Site"] = ss;
            }
            Dictionary<string, Incident> dic = IncidentV;
            Incident _incident = null;
        if(dic.ContainsKey(_incidentId))       
            _incident = dic[_incidentId];
            if (_incident == null)            
                _incident = new Incident(_incidentId, "NULL");
            
            //    string incidentId = _guid.ToString();
            WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(ss.GetUrlWebReference);
            string xmlSuppliers = customer.GetSuppliersStatus(_incidentId);

            XmlDataDocument xdd = new XmlDataDocument();
            xdd.LoadXml(xmlSuppliers);
            if (!xdd["Suppliers"].HasChildNodes)
                return "NULL";
        //    return xmlSuppliers;
            if (xdd["Suppliers"] == null || xdd["Suppliers"]["Error"] != null)
                return "NULL";
            //     dbug_log.SaveAuctionUpsale(xmlSuppliers, _guid);
            INCIDENT_STATUS _IncidentStatus = Incident.GetIncidentStatusFromString(xdd["Suppliers"].Attributes["IncidentStatus"].Value);
            string _mode = xdd["Suppliers"].Attributes["Mode"].Value;
            if (_IncidentStatus == INCIDENT_STATUS.Init)
                return "NULL";

            //      _incident.SetIncidentStatus(_IncidentStatus);
            //    Session["Incident"] = _incident;
            if (_IncidentStatus == INCIDENT_STATUS.Completed)
            {
                WebReferenceCustomer.SearchRequest searchRequest = (WebReferenceCustomer.SearchRequest)Session["searchRequest"];
                return INCIDENT_STATUS.Completed.ToString();
            }

            DataTable data = new DataTable();
            data.Columns.Add("SupplierId");
            data.Columns.Add("SupplierNumber");
            data.Columns.Add("SupplierName");
            data.Columns.Add("MiniSite");
            data.Columns.Add("ShortDescription");
            data.Columns.Add("SumAssistanceRequests");
            data.Columns.Add("NumberOfEmployees");
            data.Columns.Add("Certificate");
            data.Columns.Add("isAvailable");
            data.Columns.Add("Logo");
            data.Columns.Add("Calling", typeof(bool));
            data.Columns.Add("NotCalling", typeof(bool));
            //   data.Columns.Add("CallIcon");
            //    data.Columns.Add("CallIconClass");
            bool IsNewActive = true;
            bool IsInCalling = false;
            string NewSupplierId = string.Empty;
            int _count = 0;
            foreach (XmlNode node in xdd["Suppliers"].ChildNodes)
            {
                _count++;
                string SupplierId = node.Attributes["SupplierId"].Value;
                string SupplierNumber = node.Attributes["SupplierNumber"].Value;
                string SupplierName = node.Attributes["SupplierName"].Value;
                //    string DirectNumber = node.Attributes["DirectNumber"].Value;
                string ShortDescription = node.Attributes["ShortDescription"].Value;
           //     string DirectNumber = node.Attributes["DirectNumber"].Value;
                string SumAssistanceRequests = node.Attributes["SumAssistanceRequests"].Value;
                string NumberOfEmployees = node.Attributes["NumberOfEmployees"].Value;
                string Certificate = node.Attributes["Certificate"].Value;
                //    string isAvailable = node.Attributes["isAvailable"].Value;
                INCIDENT_ACCOUNT_STATUS Status = Incident.GetIncidentAccountStatusFromString(node.Attributes["Status"].Value);

                DataRow row = data.NewRow();
                row["SupplierId"] = SupplierId;
                row["SupplierName"] = SupplierName;
                row["SupplierNumber"] = SupplierNumber;
                //    row["DirectNumber"] = DirectNumber;
                row["ShortDescription"] = ShortDescription;
                row["SumAssistanceRequests"] = SumAssistanceRequests;
                row["NumberOfEmployees"] = NumberOfEmployees;
                row["Certificate"] = Certificate;

                string SupplierUrl = _path + "/professional.aspx?supplierGUID={" + SupplierId + "}&SiteId=" + ss.GetSiteID;
                      //          + "&DirectNumber=" + DirectNumber + "&ExtensionNumber=" + ExtensionNumber;
                row["MiniSite"] = SupplierUrl;
                string Logo = (File.Exists(professionalLogos + SupplierId + ".jpg")) ?
                    professionalLogosWeb + SupplierId + @".jpg" :
                     _path + @"/LP_Israel/images/logo-supplier.gif";
                row["Logo"] = Logo;
                
                if (Status == INCIDENT_ACCOUNT_STATUS.Initiated)
                {
                    NewSupplierId = SupplierId;
                    IsInCalling = true;
                    row["Calling"] = true;
                    row["NotCalling"] = false;
                }
                else                {
                    
                    row["Calling"] = false;
                    row["NotCalling"] = true;
                }
                data.Rows.Add(row);
            }
            if (_IncidentStatus == _incident.IncidentStatus && _count == _incident.count)
            {
                if (NewSupplierId == _incident.SupplierId)
                    IsNewActive = false;
            }
            _incident.SupplierId = (string.IsNullOrEmpty(NewSupplierId)) ? string.Empty : NewSupplierId;
            _incident.IncidentStatus = _IncidentStatus;
            _incident.count = _count;
            dic[_incident.IncidentId] = _incident;
            IncidentV = dic;
            if (!IsNewActive)
                return "NULL";
            Page page = new Page();
            UserControl ctl = (UserControl)page.LoadControl("~/LP_Israel/SupplierList.ascx");
            //      MethodInfo _translate = ctl.GetType().GetMethod("SetTranslate");
            //       _translate.Invoke(ctl, new object[] { ss.GetSiteID });
            MethodInfo m_load = ctl.GetType().GetMethod("LoadData");
            m_load.Invoke(ctl, new object[] { data,  _incident.IncidentId, IsInCalling });
            ctl.EnableViewState = false;

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
                {
                    ctl.RenderControl(textWriter);
                }
            }
            
            return sb.ToString();
        
    }
    
    [WebMethod(true)]
    public string CreateFutureRequest(string SiteId, string _phone, string _desc, int _expertise, string ExpCode, string RegionCode,
        int RegionLevel, int NumOfSuppliers, string _date, string origionId)
    //"&SiteId=<%=SiteId%>&ExpertiseCode=<%=ExpertiseCode%>&ExpertiseType=<%=ExpertiseLevel%>
 //       &ServiceAreaCode=<%=RegionCode%>&ServiceAreaType=<%=RegionLevel%>&origionId=<%=origionId%>";
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.ScheduledServiceRequest serviceRequest = new WebReferenceCustomer.ScheduledServiceRequest();
        serviceRequest.RegionCode = RegionCode;

        serviceRequest.RegionLevel = RegionLevel;
        
        serviceRequest.ExpertiseCode = ExpCode;

        serviceRequest.ExpertiseType = _expertise;
        
     //   serviceRequest.ContactFullName = "";
        serviceRequest.ContactPhoneNumber = _phone;
       
        serviceRequest.RequestDescription = _desc;
      
    //    serviceRequest.PrefferedCallTime = DateTime.Now;
        serviceRequest.SiteId = SiteId;
        serviceRequest.OriginId = new Guid(origionId); //NoProblemIsrael
      
        serviceRequest.NumOfSuppliers = NumOfSuppliers;
        string[] str = _date.Split(';');
        DateTime dt = new DateTime(int.Parse(str[0]), int.Parse(str[1]), int.Parse(str[2]), int.Parse(str[3]), int.Parse(str[4]), 0,DateTimeKind.Utc);
        
        serviceRequest.PrefferedCallTime = dt;

        WebReferenceCustomer.ResultOfScheduledServiceResponse result = customer.CreateScheduledService(serviceRequest);
        
        LogEdit.SaveLog(SiteId, "CreateFutureRequest type:" + result.Type.ToString() +        
        "\r\nsurfer phone: " +             
        _phone + "\r\nDescription: " + _desc +
        "\r\nTime to call utc: " + _date +  
        "\r\nSession: " + Session.SessionID + "\r\nip: " +   Utilities.GetIP(HttpContext.Current.Request));


        return result.Type.ToString();
    }

    private Dictionary<string, Incident> IncidentV
    {
        get { return (Session["Incident"] == null) ? new Dictionary<string, Incident>() : (Dictionary<string, Incident>)Session["Incident"]; }
        set { Session["Incident"] = value; }
    }
    
}

