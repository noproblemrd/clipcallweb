using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.SessionState;

/// <summary>
/// Summary description for PrintHelper
/// </summary>
public class PrintHelper
{
	public PrintHelper()
	{
		
	}
    public static void PrintWebControl(Control ctrl)
    {
        PrintWebControl(ctrl, string.Empty);
    }

    public static void PrintWebControl(Control ctrl, string Script)
    {
        StringWriter stringWrite = new StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
        if (ctrl is WebControl)
        {
            Unit w = new Unit(100, UnitType.Percentage); 
            ((WebControl)ctrl).Width = w;
        }
        Page pg = new Page();
        
        pg.EnableEventValidation = false;
        if (Script != string.Empty)
        {
            pg.ClientScript.RegisterStartupScript(pg.GetType(), "PrintJavaScript", Script);
        }
       
        HtmlForm frm = new HtmlForm();
        pg.Controls.Add(frm);
        frm.Attributes.Add("runat", "server");
        frm.Controls.Add(ctrl);
        pg.DesignerInitialize();

        pg.RenderControl(htmlWrite);
        string strHTML = stringWrite.ToString();
        

       

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Write(strHTML);
    //    HttpContext.Current.Response.Write("<script type='text/javascript'>window.print();setTimeout('window.close()',4000);</script>");
        string _script = "<script type='text/javascript'>"+
            "window.onload = function() {"+
            "window.print();setTimeout('self.close()', 10000);}"+
            "</script>";
        HttpContext.Current.Response.Write(_script);
     //   HttpContext.Current.Response.Write("<script>window.print();</script>");

        /*
        pg.Response.Clear();
        pg.Response.Buffer = true;
        pg.Response.ContentType = "application/html";
        pg.Response.ContentEncoding = System.Text.Encoding.UTF8;
        pg.Response.AddHeader("Content-Disposition", "attachment;filename=Test.html");
        pg.Response.Charset = "";
        EnableViewState = false;
      //  System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
      //  System.Web.UI.HtmlTextWriter oHtmlTextWriter  = new System.Web.UI.HtmlTextWriter(oStringWriter);
        pg.RenderControl(oHtmlTextWriter)
        Response.Write(oHtmlTextWriter.ToString())
        Response.Close()
         * */
    //
        HttpContext.Current.Response.Flush();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }
    public static void PrintWebControl(GridView ctrl, DataTable data)
    {
        ctrl.AllowPaging = false;
        ctrl.DataSource = data;
        ctrl.DataBind();
        PrintWebControl(ctrl);
    }
    public static void PrintWebControl(GridView ctrl, DataView data)
    {
        ctrl.AllowPaging = false;
        ctrl.DataSource = data;
        ctrl.DataBind();
        PrintWebControl(ctrl);
    }
    public static void PrintWebControl(Repeater ctrl, DataTable data)
    {
       
        ctrl.DataSource = data;
        ctrl.DataBind();
        PrintWebControl(ctrl);
    }
}
