﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ISliderLinks
/// </summary>
public interface ISliderLinks
{   
    void SetSliderLinks(string _whatsThisLink, string _AdvertiseLink, string _SettingsLink, bool HasAdvertise);
}