﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IToolBoxControl
/// </summary>
public interface IToolBoxControl
{
    void SetToolBox(System.Web.UI.UserControl ControlSender);
    void SetToolBoxEvents(System.Web.UI.UserControl ControlSender);
}