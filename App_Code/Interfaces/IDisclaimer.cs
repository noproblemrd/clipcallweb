﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IDisclaimer
/// </summary>
public interface IDisclaimer
{
    void SetDisclaimerLinks(bool HideDisclaimer, string privacy, string terms);
}