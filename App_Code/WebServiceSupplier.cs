using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;

/// <summary>
/// Summary description for WebServiceSupplier
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WebServiceSupplier : System.Web.Services.WebService {

    public WebServiceSupplier () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string sendCitiesTree(string strCities) {
        //return "Hello World";
        //WebReferenceSupplier.Supplier supplier = new WebReferenceSupplier.Supplier();
        SiteSetting ss = (SiteSetting)Session["Site"];
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, ss.GetSiteID);

        WebReferenceSupplier.UpdateSupplierRegionsRequest updateRequest = new WebReferenceSupplier.UpdateSupplierRegionsRequest();

        List<WebReferenceSupplier.RegionDataForUpdate> list = new List<WebReferenceSupplier.RegionDataForUpdate>();
        
        WebReferenceSupplier.RegionDataForUpdate regionDataForUpdate;

        string[] splitCiteis;
        string[] splitCity=null;
        string[] splitCityProperty = null;
        string[] splitParents = null;
        string RegionIdValue = "";
        string RegionStateValue="";
        string IsDirtyValue = "";
        string LevelValue = "";
        string ParentsValue = "";
        string msg = "";

        if (strCities.Length > 0)
        {
            splitCiteis = strCities.Split('&');
            string strReturn = "";
            for (int indexCities = 0; indexCities < splitCiteis.Length; indexCities++)
            {
                regionDataForUpdate = new WebReferenceSupplier.RegionDataForUpdate();
                splitCity = splitCiteis[indexCities].Split(',');
                for (int indexCity = 0; indexCity < splitCity.Length; indexCity++)
                {
                    splitCityProperty = splitCity[indexCity].Split('=');
                    if (splitCityProperty[0] == "RegionId")
                    {
                        RegionIdValue = splitCityProperty[1];
                        regionDataForUpdate.RegionId = new Guid(RegionIdValue);
                    }
                    else if (splitCityProperty[0] == "RegionState")
                    {
                        RegionStateValue = splitCityProperty[1];
                        if (RegionStateValue == "1") // all childs working
                            regionDataForUpdate.RegionState = WebReferenceSupplier.RegionState.Working;
                        else if (RegionStateValue == "2") // partial childs working
                            regionDataForUpdate.RegionState = WebReferenceSupplier.RegionState.HasNotWorkingChild;
                        else if (RegionStateValue == "0") // all childs not working
                            regionDataForUpdate.RegionState = WebReferenceSupplier.RegionState.NotWorking;

                    }

                    else if (splitCityProperty[0] == "IsDirty")
                    {
                        IsDirtyValue = splitCityProperty[1];
                        if (IsDirtyValue == "True")
                            regionDataForUpdate.IsDirty = true;
                        else
                            regionDataForUpdate.IsDirty = false;
                    }

                    else if (splitCityProperty[0] == "Level")
                    {
                        LevelValue = splitCityProperty[1];
                        regionDataForUpdate.Level = Convert.ToInt32(LevelValue) + 1;
                    }

                    else if (splitCityProperty[0] == "Parents")
                    {
                        ParentsValue = splitCityProperty[1];
                        splitParents = ParentsValue.Split(new string[] { "**" }, StringSplitOptions.RemoveEmptyEntries);
                        for (int indexParent = 0; indexParent < splitParents.Length; indexParent++)
                        {
                            if (indexParent == 0)
                                regionDataForUpdate.Parent1 = new Guid(splitParents[indexParent]);
                            else if (indexParent == 1)
                                regionDataForUpdate.Parent2 = new Guid(splitParents[indexParent]);
                            else if (indexParent == 2)
                                regionDataForUpdate.Parent3 = new Guid(splitParents[indexParent]);
                            else if (indexParent == 3)
                                regionDataForUpdate.Parent4 = new Guid(splitParents[indexParent]);
                            else if (indexParent == 4)
                                regionDataForUpdate.Parent5 = new Guid(splitParents[indexParent]);
                        }

                    }

                    //"RegionId="
                    //",RegionState="
                    //",IsDirty="  
                    //",Level=" 
                    //",Parents=" 





                }

                list.Add(regionDataForUpdate);
            }
        }

        

        updateRequest.RegionDataList = list.ToArray();

        updateRequest.SupplierId = new Guid(Session["supllierId"].ToString()); // the id of the supplier.

        WebReferenceSupplier.Result updateresult = supplier.UpdateSupplierRegionsInTree(updateRequest); //call to the service.


        string error = "";


        if (updateresult.Type == WebReferenceSupplier.eResultType.Failure)//if true, an error ocurred inside the server.
        {
            error = updateresult.Messages[0].ToString(); ;//you can see the error message here if you want.
            msg = error;
        }

        else if (updateresult.Type == WebReferenceSupplier.eResultType.Success)
        {
            msg = "success";
        }

        return msg;
    }
    
}

