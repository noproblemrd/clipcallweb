﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Xml;

/// <summary>
/// Summary description for iframeParent2
/// </summary>
public class iframeParent2 : System.Web.UI.Page
{
    /* needed to be protected for childs*/
    protected string xmlSuppliers = "";
    protected string IfShowMinisite = "";
    protected XmlTextReader xmlTextReader;

    protected string SiteId = "";
    protected string SupplierId = "";
    protected string SupplierNumber = "";
    protected string SupplierName = "";
    protected string SumSurvey = "";
    protected string SumAssistanceRequests = "";
    protected string NumberOfEmployees = "";
    protected string Certificate = "";
    protected string ShortDescription = "";
    protected string isAvailable = "";
    protected string DirectNumber = "";
    protected bool blnlogo = false;
    protected string Logo = "";
    protected string ExtensionNumber = "";
    protected string professionalLogos = "";
    protected string professionalLogosWeb = "";
    protected bool ifError = false;
    protected string _like = "";
    protected string _dislike = "";
    protected string City = "";
    protected string Street = "";
    protected string StreetNumber = "";
    protected string ZipCode = "";
    protected string District = "";
    protected string Address = "";
    protected string TypeOfBusiness = "";
    protected string ExpertiseCode = "";
    protected int ExpertiseLevel;
    protected string RegionCode = "";
    protected int RegionLevel;
    protected string origionId = "";

    protected StringBuilder sb;

	public iframeParent2()
	{
		//
		
		//
	}

    protected void Page_Init(object sender, EventArgs e)
    {
        //
        
        //





    }

    protected void Page_Load(object sender, EventArgs e)
    {       

        
        //SiteId = Request["SiteId"].ToString();
        SiteId = Request["SiteId"];
        //Response.Write("SiteId:" + SiteId);
        //     if (!string.IsNullOrEmpty(SiteId))
        //         GetTranslate(SiteId, _panel);

        
        ExpertiseCode = Request["ExpertiseCode"];       
        ExpertiseLevel = Convert.ToInt32(Request["ExpertiseLevel"]);        
        RegionCode = Request["RegionCode"];        
        RegionLevel = Convert.ToInt32(Request["RegionLevel"]);


        int MaxResults;
        if (Request["MaxResults"] != null && Request["MaxResults"].ToString() != "")
        {
            MaxResults = Convert.ToInt32(Request["MaxResults"].ToString());
        }
        else
            MaxResults = -1;

        
        origionId = Request["origionId"];
        //Response.Write("origionId" + origionId);
        IfShowMinisite = Request["IfShowMinisite"];
        //Response.Write("IfShowMinisite:" + IfShowMinisite);

        string Domain = "";
        Domain = Request["Domain"];

        string Url = "";
        Url = Request["Url"];

        string PageName = "";
        PageName = Request["PageName"];

        string PlaceInWebSite = "";
        PlaceInWebSite = Request["PlaceInWebSite"];

        string ControlName = "";
        ControlName = Request["ControlName"];

        string SessionId = "";
        SessionId = Request["SessionId"];

   //     Encoding enc = Encoding.GetEncoding("Windows-1255");

  //      TypeOfBusiness = HttpUtility.UrlDecode(Request["TypeOfBusiness"], enc);
        TypeOfBusiness = Request["TypeOfBusiness"];

        /*
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.SearchRequestWithExposure searchRequest = new WebReferenceCustomer.SearchRequestWithExposure();
        searchRequest.ExpertiseCode = ExpertiseCode;
        searchRequest.ExpertiseLevel = ExpertiseLevel;
        searchRequest.MaxResults = MaxResults;
        searchRequest.RegionCode = RegionCode;
        searchRequest.RegionLevel = RegionLevel;
        searchRequest.SiteId = SiteId;
        searchRequest.OriginId = origionId;
        searchRequest.Domain = Domain;
        searchRequest.Url = Url;
        searchRequest.PageName = PageName;
        searchRequest.PlaceInWebSite = PlaceInWebSite;
        searchRequest.ControlName = ControlName;
        searchRequest.SessionId = SessionId;
        */

        //xmlSuppliers = customer.SearchSiteSuppliersExpandedWithExposure(searchRequest);
        WebServiceAuction wsAuction=new  WebServiceAuction();
        string xmlSuppliers=wsAuction.SearchSuppliersExpandedWithExposure(RegionCode,RegionLevel,ExpertiseCode,ExpertiseLevel,origionId,SiteId,-1,MaxResults,-1,Domain,Url,ControlName,PageName,PlaceInWebSite,SessionId);
        //WebReferenceSite.Site siteWebService = WebServiceConfig.GetSiteReference(this);
        //string xmlSuppliers = siteWebService.SearchSiteSuppliers(SiteId, ExpertiseGUID, CityGuid, ExpertiseType, MaxResults);

        //Response.Write("SiteId:" + SiteId + " ExpertiseGUID:" + ExpertiseGUID + " CityGuid:" + CityGuid + " ExpertiseType:" + ExpertiseType + " MaxResults:" + MaxResults);
        //Response.Write(xmlSuppliers);

        System.Text.UTF8Encoding myEncoder = new System.Text.UTF8Encoding();
        byte[] bytes = myEncoder.GetBytes(xmlSuppliers);
        MemoryStream ms = new MemoryStream(bytes);
        xmlTextReader = new XmlTextReader(ms);

        sb = new StringBuilder();

        /*
         <Suppliers>
            <Supplier>
            <SupplierId />
            <SupplierNumber />
            <SupplierName />
            <SumSurvey />
            <SumAssistanceRequests />
            <NumberOfEmployees />
            <ShortDescription/ >
            <DirectNumber />
            </Supplier>
         </Suppliers>
        */

    }
}