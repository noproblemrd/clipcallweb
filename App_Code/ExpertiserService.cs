using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Text;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;


/// <summary>
/// Summary description for ExpertiserService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ExpertiserService : System.Web.Services.WebService {

    public ExpertiserService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    [WebMethod(EnableSession = true)]
    public string[] GetSuggestionsUpsale(string prefixText, int count)
    {
       SortedDictionary<string, string> list = new SortedDictionary<string, string>();
  //      List<string> responses = new List<string>();
        if (Session == null || Session["AutoCompleteListExp"] == null)
            return new string[]{};
        else
            list = (SortedDictionary<string, string>)Session["AutoCompleteListExp"];
        /*        contextKey = contextKey.Substring(0, contextKey.Length - 1);

                string[] items = contextKey.Split(';');
                foreach (string str in items)
                {
                    string[] itemssub = str.Split('&');
                    this.list.Add(itemssub[0], itemssub[1]);
                }
         * */
         IEnumerable<string> query = (from x in list
                                      where x.Key.ToLower().StartsWith(prefixText.ToLower())
                                      select x.Key).Take(count);
        /*
        foreach (string str in list.Keys)
        {
            if (str.ToLower().StartsWith(prefixText.ToLower()))
            {
                responses.Add(str);
                count--;
                if (count == 0 || Utilities.LargeTest(str, prefixText) > 0)
                    return responses.ToArray();
            }
        }
         * */
    //    return responses.ToArray();
         return query.ToArray();
    }
    [WebMethod(true)]
    public string CheckExpertise(string Expertise)
    {
        SortedDictionary<string, string> list = new SortedDictionary<string, string>();
//        List<string> responses = new List<string>();
        if (Session == null || Session["AutoCompleteListExp"] == null)
            return "false";
        else
            list = (SortedDictionary<string, string>)Session["AutoCompleteListExp"];
        if (list.ContainsKey(Expertise))
            return "true";
        return "false";
    }
    [WebMethod(true)]
    public string GetSecExpertisers(string ExpertiseId)
    {
        SiteSetting ss = (SiteSetting)Session["Site"];
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(ss.GetUrlWebReference);
        List<NameGuidCode> list = (List<NameGuidCode>)Session["UpsaleExpertise"];
        string _guid = "";
        foreach (NameGuidCode ngc in list)
        {
            if (ngc._code == ExpertiseId)
            {
                _guid = ngc._guid.ToString();
                break;
            }
        }
        if (string.IsNullOrEmpty(_guid))
            return string.Empty;
        string result = _site.GetSecondaryExpertise(ss.GetSiteID, _guid);
       
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if(xdd["PrimaryExpertise"] == null || xdd["PrimaryExpertise"]["Error"] != null)
            return "";
        StringBuilder sb = new StringBuilder();
        foreach (XmlNode node in xdd["PrimaryExpertise"].ChildNodes)
        {
            sb.Append(node.InnerText + "," + node.Attributes["Code"].Value + ";");
        }
        if(sb.Length > 0)
            return sb.ToString().Substring(0,sb.Length-1);
        return string.Empty;       
    }
    [WebMethod(true)]
    public List<string> GetSuggestionsExpertise(string prefixText)
    {
        Dictionary<Guid, string> list = ExpertisersListV;
        IEnumerable<string> query = (from x in list
                                     where x.Value.ToLower().StartsWith(prefixText.ToLower())
                                     select x.Value).Take(10);
        return query.ToList();
    }
    [WebMethod(true)]
    public string GetOneSuggestionsExpertise(string prefixText)
    {
        if (string.IsNullOrEmpty(prefixText))
            return string.Empty;
        Dictionary<Guid, string> list = ExpertisersListV;
        string query = (from x in list
                        where x.Value.ToLower().StartsWith(prefixText.ToLower())
                        select x.Value).SingleOrDefault();
        return query;
    }
    [WebMethod(true)]
    public Guid GetGuidExpertise(string prefixText)
    {
        if (string.IsNullOrEmpty(prefixText))
            return Guid.Empty;
        Dictionary<Guid, string> list = ExpertisersListV;
        Guid query = (from x in list
                        where x.Value.ToLower() == prefixText.ToLower()
                        select x.Key).SingleOrDefault();
        return query;
    }
    Dictionary<Guid, string> LoadGuidExpertise(string SiteId)
    {
        Dictionary<Guid, string> list = new Dictionary<Guid, string>();

        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(null, SiteId);
        string result = sit.GetPrimaryExpertise(SiteId, "");
        
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["PrimaryExpertise"] == null || xdd["PrimaryExpertise"]["Error"] != null)
        {
            return list;
        }
        
        foreach (XmlNode nodePrimary in xdd["PrimaryExpertise"].ChildNodes)
        {
            string pName = nodePrimary.Attributes["Name"].InnerText;
            string _id = nodePrimary.Attributes["Code"].Value;
            string _guid = nodePrimary.Attributes["ID"].Value;
            list.Add(new Guid(_guid), pName);           
        }
        return list;
    }
    Dictionary<Guid, string> ExpertisersListV
    {
        get
        {
            if (Session["ExpertiseAutoComplete"] == null || Session["ExpertiseAutoComplete"].GetType() != typeof(Dictionary<Guid, string>))
                Session["ExpertiseAutoComplete"] = LoadGuidExpertise(((SiteSetting)Session["Site"]).GetSiteID);
            return (Dictionary<Guid, string>)Session["ExpertiseAutoComplete"];
        }
        set { Session["ExpertiseAutoComplete"] = value; }
    }
    
}

