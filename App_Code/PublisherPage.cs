﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for PublisherPage
/// </summary>
public class PublisherPage : PageSetting
{
    protected const string STRIPE_CHARGE_URL = @"payments/";
    protected const string CONNECT_ACCOUNT_URL = @"applications/users/";
    protected const string STRIPE_TRANSFER_URL = @"applications/transfers/";
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //Security- for the browser not save in the cash memory
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");
        ////

        if (Session.IsNewSession || !this.userManagement.IsPublisher())
        {
            if (this.siteSetting == null || string.IsNullOrEmpty(this.siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, this))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            }
            if (!EncryptString.LogIn(this, HttpContext.Current))
            {
                Response.Redirect("LogOut.aspx");
                return;
            }
        }
          
    }
    public void closeTheWindow(string _pathLogout)
    {
        string _script = "function CloseWindow() { if (window.opener) window.close(); else  window.location = '" + _pathLogout + "';}";
        ScriptManager sm = ScriptManager.GetCurrent(this);
        if(sm==null || !sm.IsInAsyncPostBack)
            ClientScript.RegisterStartupScript(this.GetType(), "CloseWindow", _script, true);
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseWindow", _script, true);
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }

    protected string ProjectViewUrl
    {
        get { return ResolveUrl("~/Publisher/ProjectView.aspx?ia="); }
    }

}