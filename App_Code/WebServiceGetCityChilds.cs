using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Telerik.Web.UI;
using System.Collections.Generic;

/// <summary>
/// Summary description for WebServiceGetCityChilds
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class WebServiceGetCityChilds : System.Web.Services.WebService {

    protected string partial="";

    public WebServiceGetCityChilds()
    {
        
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public WebServiceGetCityChilds (string partialTranslate) {
        partial = partialTranslate;
        Session["partial"] = partial;
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod (EnableSession=true)]
    public RadTreeNodeData[] getCityChilds(RadTreeNodeData node, object context)
    {
        SiteSetting ss = (SiteSetting)Session["Site"];

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, ss.GetSiteID);

        Telerik.Web.UI.RadTreeNodeData ds = new Telerik.Web.UI.RadTreeNodeData();

        

        WebReferenceSupplier.GetSupplierRegionsRequest request = new WebReferenceSupplier.GetSupplierRegionsRequest();
        request.ParentRegionId = new Guid(node.Value);// the parent of the node you want to open. send Guid.Empty for level 1.
        request.SupplierId = new Guid(Session["supllierId"].ToString());// the id of the supplier.
        WebReferenceSupplier.ResultOfGetSupplierRegionsResponse result = supplier.GetSupplierRegionsInTree(request);//call to the service.

        List<RadTreeNodeData> nodesList = new List<RadTreeNodeData>();

        if (result.Type == WebReferenceSupplier.eResultType.Success)
        {
            //iterate the results:
            foreach (WebReferenceSupplier.RegionDataForRetrieve regionData in result.Value.RegionDataList)
            {
                //retrieve data:
                Guid id = regionData.RegionId;
                
                string name = regionData.RegionName;
                WebReferenceSupplier.RegionState state = regionData.RegionState;
                
                RadTreeNodeData itemData = new RadTreeNodeData();
                itemData.Text = name;
                itemData.Value = id.ToString();                

                itemData.Attributes = new System.Collections.Generic.Dictionary<string, string>();
                
                

                if (node.Attributes["ifChecked"] == "Working") // if parent is checked 
                {
                    itemData.Checked = true;
                    itemData.Attributes.Add("ifChecked", "Working");
                    itemData.Attributes.Add("ifExpand", "False");
                }
                else if (node.Attributes["ifChecked"] == "HasNotWorkingChild") // if parent is checked
                {

                    if (state.ToString() == "Working")
                    {
                        itemData.Checked = true;
                        itemData.Attributes.Add("ifChecked", "Working");
                        itemData.Attributes.Add("ifExpand", "False");
                    }

                    else if (state.ToString() == "HasNotWorkingChild")
                    {
                        itemData.Text += " (" + Session["partial"] + ")";
                        itemData.Checked = true;
                        itemData.Attributes.Add("ifChecked", "HasNotWorkingChild");
                        itemData.Attributes.Add("ifExpand", "False");
                    }

                    else if (state.ToString() == "NotWorking")
                    {
                        itemData.Checked = false;
                        itemData.Attributes.Add("ifChecked", "NotWorking");
                        itemData.Attributes.Add("ifExpand", "True");
                    }



                }
                else if (node.Attributes["ifChecked"] == "NotWorking") // if parent is not checked
                {
                    itemData.Checked = false;
                    itemData.Attributes.Add("ifChecked", "NotWorking");
                    itemData.Attributes.Add("ifExpand", "True");
                }

                
                /*
                else if (state.ToString() == "Working" || state.ToString() == "HasNotWorkingChild")// if parent is partial it itself is working and partial working
                    {

                        itemData.Checked = true;
                    }

                    else
                    {

                        itemData.Checked = false;
                    }
                */
                //itemData.ifExpand = "False";
                //itemData.Attributes.Add("ifExpand", "False");
                itemData.ExpandMode = TreeNodeExpandMode.WebService;
                
                nodesList.Add(itemData);
                //buildTree(name, id, state.ToString());
                //use data...
            }


        }

        return nodesList.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string,string> getCityChildsLevelOne(RadTreeView RadTreeView1)
    {
        //WebReferenceSupplier.Supplier supplier = new WebReferenceSupplier.Supplier();
        SiteSetting ss = (SiteSetting)Session["Site"];
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, ss.GetSiteID);
       
        WebReferenceSupplier.GetSupplierRegionsRequest request = new WebReferenceSupplier.GetSupplierRegionsRequest();
        request.ParentRegionId = Guid.Empty;// the parent of the node you want to open. send Guid.Empty for level 1.
        request.SupplierId = new Guid(Session["supllierId"].ToString());// the id of the supplier.
        WebReferenceSupplier.ResultOfGetSupplierRegionsResponse result = supplier.GetSupplierRegionsInTree(request);//call to the service.
        
        int registrationStage = result.Value.StageInRegistration;
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("RegistrationStep", registrationStage.ToString());
        dic.Add("ResultType", result.Type.ToString().ToLower());        
        
        List<RadTreeNodeData> nodesList = new List<RadTreeNodeData>();

        if (result.Type == WebReferenceSupplier.eResultType.Success)
        {
            //iterate the results:
            foreach (WebReferenceSupplier.RegionDataForRetrieve regionData in result.Value.RegionDataList)
            {
                //retrieve data:
                Guid id = regionData.RegionId;
                string name = regionData.RegionName;
                WebReferenceSupplier.RegionState state = regionData.RegionState;

                /*
                RadTreeNodeData itemData = new RadTreeNodeData();
                itemData.Text = name;
                itemData.Value = id.ToString();
                

                if (state.ToString() == "Working" || state.ToString() == "HasNotWorkingChild")
                    itemData.ExpandMode = TreeNodeExpandMode.WebService;
                */


                //nodesList.Add(itemData);

                RadTreeNode node = new RadTreeNode();
                node.Text = name;
                node.Value = id.ToString();
                node.ExpandMode = TreeNodeExpandMode.WebService;
                //node.CssClass="internalNodeClass";
                
                node.Attributes.Add("ifChecked", state.ToString());

                if (state.ToString() == "Working")
                {
                    node.Checked = true;
                   
                    //HttpContext.Current.Response.Write("t" + node.Attributes["Text"]);
                }

                else if(state.ToString() == "HasNotWorkingChild")
                {


                    /*
                    RadTreeNode rdNodeNotWork = new RadTreeNode();
                    rdNodeNotWork.Text = "NotWork";
                    rdNodeNotWork.Expanded = false;
                    
                    //node.Nodes.Add(rdNodeNotWork);
                    
                    

                    RadTreeNode rdNodeWork = new RadTreeNode();
                    rdNodeWork.Text = "Work";
                    rdNodeWork.Value = "555";
                    rdNodeWork.Expanded = true;
                    */

                    //node.Nodes.Add(rdNodeWork);

                    node.Text += " (" + partial +")";
                    node.Checked = true;
                    
                }

                else // not working
                {
                    node.Checked = false;
                    
                }

                node.Attributes.Add("ifExpand","False");
                
                RadTreeView1.Nodes.Add(node);
                //buildTree(name, id, state.ToString());
                //use data...
               
            }


        }

        return dic;
    }

    /*
    [WebMethod(EnableSession = true)]
    public Dictionary<string, string> getCitiesBySearch(string strSearch)
    {
        
        SiteSetting ss = (SiteSetting)Session["Site"];
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, ss.GetSiteID);
        
        WebReferenceSupplier.GetSupplierRegionsSearchRequest request=new  WebReferenceSupplier.GetSupplierRegionsSearchRequest();
        request.RegionName=strSearch;
        request.SupplierId=new Guid(Session["supllierId"].ToString());        

        WebReferenceSupplier.ResultOfGetSupplierRegionsSearchResponse response=new  WebReferenceSupplier.ResultOfGetSupplierRegionsSearchResponse(); 
        response=supplier.GetSupplierRegionsSearchInTree(request);
        

        Dictionary<string, string> dic = new Dictionary<string, string>();

        //dic.Add("ResultType", response.Type.ToString().ToLower());
        //dic.Add("Message", response.Messages[0].ToString());

        return dic;
    }
     * */
}

    public class customRadTreeNodeData : Telerik.Web.UI.RadTreeNodeData
    {
        private bool mChecked;

        public bool Checked
        {
            get { return mChecked; }
            set { mChecked = value; }
        }
    }

    public class RadTreeNodeData
    {
        private string text;
        private string mvalue;
        private TreeNodeExpandMode expandMode;
        private bool mChecked;
        private string mifExpand;
        private System.Collections.Generic.IDictionary<string, string> mAttributes;
        

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public string Value
        {
            get { return mvalue; }
            set { mvalue = value; }
        }

        public TreeNodeExpandMode ExpandMode
        {
            get { return expandMode; }
            set { expandMode = value; }
        }

        public bool Checked
        {
            get { return mChecked; }
            set { mChecked = value; }
        }

        public string ifExpand
        {
            get { return mifExpand; }
            set { mifExpand = value; }
        }

        public System.Collections.Generic.IDictionary<string, string> Attributes
        {
            get { return mAttributes; }
            set { mAttributes = value; }
        }
    }

    


