﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Data.SqlClient;

/// <summary>
/// Summary description for WebServiceMiniSite
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceMiniSite : System.Web.Services.WebService
{

    public WebServiceMiniSite()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public MiniSiteData GetMiniSiteData(string siteId, string supplier)
    {
        OneCallUtilities.OneCallUtilities client = WebServiceConfig.GetOneCallUtilitiesReference(null, siteId);
        var data = client.GetMiniSiteSupplierData(supplier);
        MiniSiteData retVal = new MiniSiteData();
        retVal.Expertises = data.Expertises.ToList();
        retVal.Regions = data.Regions.ToList();
        retVal.CallsNumber = data.CallsNumber;
        retVal.DirectNumber = data.DirectNumber;
        retVal.EmployeeNumber = data.EmployeeNumber;
        retVal.LongDescription = data.LongDescription;
        retVal.ShortDescription = data.ShortDescription;
        retVal.Video = data.Video;
        retVal.SurveyNumber = data.SurveyNumber;
        retVal.Certificate = data.Certificate;
        retVal.SupplierId = data.AccountId;
        
        if (data.AccountId != Guid.Empty)
        {
            retVal.Logo = GetLogo(data.AccountId);
        }
        return retVal;
    }

    private string GetLogo(Guid supplierId)
    {
        string logosUrl = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];
        string logosFile = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
        string siteAddress = GetSiteAddress("np_Israel");
        string supplierIdStr = supplierId.ToString();
        string logoUrlReady = logosUrl + supplierId + ".jpg";
        string logoFile = logosFile + supplierId + ".jpg";
        if (File.Exists(logoFile))
            return logoUrlReady;
        else
            return string.Empty;
    }

    private string GetSiteAddress(string siteId)
    {        
        string retVal = string.Empty;
        string command = "SELECT dbo.GetUrlBySiteNameId(@SiteNameId)";
        using (SqlConnection connection = DBConnection.GetConnString())
        {
            connection.Open();
            SqlCommand cmd = new SqlCommand(command, connection);

            cmd.Parameters.AddWithValue("@SiteNameId", siteId);
            retVal = (string)cmd.ExecuteScalar();
           
            connection.Close();
        }
        return retVal;
    }

    public class MiniSiteData
    {
        public List<string> Images { get; set; }
        public string Video { get; set; }
        public string Logo { get; set; }
        public string DirectNumber { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public int SurveyNumber { get; set; }
        public int EmployeeNumber { get; set; }
        public int CallsNumber { get; set; }
        public List<string> Expertises { get; set; }
        public List<string> Regions { get; set; }
        public bool Certificate { get; set; }
        public Guid SupplierId { get; set; }
    }
}
