﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DefaultIntervalReport
/// </summary>
[Serializable()]
public class DefaultIntervalReport
{
  /*  public DayOfWeek DAY { get; private set; }*/
    public string DAY_OF_WEEK { get; private set; }
    public int FROM_HOUR { get; private set; }
    public int TO_HOUR { get; private set; }
	public DefaultIntervalReport(string _day, int _from, int _to)
	{
        DAY_OF_WEEK = _day;
        FROM_HOUR = _from;
        TO_HOUR = _to;
	}
    public bool IsDefault(string _DayOfWeek, int _from, int _to)
    {
        if (DAY_OF_WEEK != _DayOfWeek)
            return false;
        if (FROM_HOUR != _from)
            return false;
        return TO_HOUR == _to;
    }
}