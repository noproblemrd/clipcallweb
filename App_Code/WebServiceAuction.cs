using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Xml;
using System.Xml.Linq;
using System.IO;


/// <summary>
/// Summary description for WebServiceAuction
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

[System.Web.Script.Services.ScriptService()]
public class WebServiceAuction : System.Web.Services.WebService
{
    const string _FALSE = "false";
    public WebServiceAuction()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    //   [ScriptMethod(UseHttpGet = true)]
    [WebMethod]
    public string createServiceRequest(string ServiceAreaCode, int ServiceAreaType,
        string ExpertiseCode, int ExpertiseType, string ContactPhoneNumber,
        string Description, int NumOfSuppliers, string SiteId, string origionId, string SessionID)
    {
    //    dbug_log.ExceptionLog(new Exception(), "1= " + SiteId);
        string badParam = string.Empty;
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.ServiceRequest serviceRequest = new WebReferenceCustomer.ServiceRequest();
        serviceRequest.RegionCode = ServiceAreaCode;
        serviceRequest.RegionLevel = ServiceAreaType;
        serviceRequest.ExpertiseCode = ExpertiseCode;
        serviceRequest.ExpertiseType = ExpertiseType;
        serviceRequest.ContactFullName = "";
        serviceRequest.ContactPhoneNumber = ContactPhoneNumber;
        serviceRequest.RequestDescription = Description;
        serviceRequest.PrefferedCallTime = DateTime.Now;
        serviceRequest.SiteId = SiteId;
        serviceRequest.SessionId = SessionID;
        if (IsGuid(origionId) == false)
        {
            return WebReferenceCustomer.StatusCode.BadParametersReceived.ToString() + ", OriginId";
        }
        serviceRequest.OriginId = new Guid(origionId);
        serviceRequest.NumOfSuppliers = NumOfSuppliers;

        WebReferenceCustomer.ServiceResponse serviceResponse = new WebReferenceCustomer.ServiceResponse();
        ////WebReferenceCustomer.CustomersService.CreateServiceRequest(WebReferenceCustomer.ServiceRequest1)'
        try
        {
            serviceResponse = customer.CreateServiceRequest(serviceRequest);
        }
        catch (Exception ex)
        {
            return _FALSE;
        }

        if (serviceResponse.Status == WebReferenceCustomer.StatusCode.Success)
        {

            //     Response.Write("CreateServiceRequest: no suppliers");
            //     LogEdit.SaveLog(SiteId, "no suppliers");
            return serviceResponse.ServiceRequestId.ToString();
        }
        else
        {
            //       Response.Write(serviceResponse.ServiceRequestId);
            //        LogEdit.SaveLog(SiteId, "CreateServiceRequest:" + serviceResponse.ServiceRequestId.ToString());
            return serviceResponse.Status.ToString() + ", " + serviceResponse.Message;
        }

    }

    [WebMethod]
    public string createServiceRequestExposure(string ServiceAreaCode, int ServiceAreaType,
        string ExpertiseCode, int ExpertiseType, string ContactPhoneNumber,
        string Description, int NumOfSuppliers, string SiteId, string origionId, string SessionID,
        string Domain, string Url, string ControlName, string PageName, string PlaceInWebSite)
    {
        //    dbug_log.ExceptionLog(new Exception(), "1= " + SiteId);
        string badParam = string.Empty;
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        WebReferenceCustomer.ServiceRequest serviceRequest = new WebReferenceCustomer.ServiceRequest();
        serviceRequest.RegionCode = ServiceAreaCode;
        serviceRequest.RegionLevel = ServiceAreaType;
        serviceRequest.ExpertiseCode = ExpertiseCode;
        serviceRequest.ExpertiseType = ExpertiseType;
        serviceRequest.ContactFullName = "";
        serviceRequest.ContactPhoneNumber = ContactPhoneNumber;
        serviceRequest.RequestDescription = Description;
        serviceRequest.PrefferedCallTime = DateTime.Now;
        serviceRequest.SiteId = SiteId;
        serviceRequest.SessionId = SessionID;
        serviceRequest.Domain=Domain;
        serviceRequest.Url = Url;
        serviceRequest.ControlName=ControlName;
        serviceRequest.PageName = PageName;
        serviceRequest.PlaceInWebSite = PlaceInWebSite;
        
        if (IsGuid(origionId) == false)
        {
            return WebReferenceCustomer.StatusCode.BadParametersReceived.ToString() + ", OriginId";
        }
        serviceRequest.OriginId = new Guid(origionId);
        serviceRequest.NumOfSuppliers = NumOfSuppliers;

        WebReferenceCustomer.ServiceResponse serviceResponse = new WebReferenceCustomer.ServiceResponse();
        try
        {
            serviceResponse = customer.CreateServiceRequest(serviceRequest);
        }
        catch (Exception ex)
        {
            return _FALSE;
        }

        if (serviceResponse.Status == WebReferenceCustomer.StatusCode.Success)
        {

            //     Response.Write("CreateServiceRequest: no suppliers");
            //     LogEdit.SaveLog(SiteId, "no suppliers");
            return serviceResponse.ServiceRequestId.ToString();
        }
        else
        {
            //       Response.Write(serviceResponse.ServiceRequestId);
            //        LogEdit.SaveLog(SiteId, "CreateServiceRequest:" + serviceResponse.ServiceRequestId.ToString());
            return serviceResponse.Status.ToString() + ", " + serviceResponse.Message;
        }

    }

    private bool IsGuid(string expression)
    {
        if (expression != null)
        {
            System.Text.RegularExpressions.Regex guidRegEx = 
                new System.Text.RegularExpressions.Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");
            return guidRegEx.IsMatch(expression);
        }
        return false;
        
    }

    /* not in used
    [WebMethod]
    public bool HasBadWords(string SiteId, string sentence)
    {
        List<string> list = new List<string>();
        string command = "EXEC dbo.GetBadWordsBySiteNameId @SiteId";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@SiteId", SiteId);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            list.Add(((string)reader["Word"]).ToLower());
        }
        conn.Close();
        if (list.Count == 0)
            return false;
        list.Sort();
        sentence.ToLower();
        string[] words = sentence.Split(' ', ',', '.', ';', '\'', '"', '-', '?', '!');
        for (int i = 0; i < words.Length; i++)
        {
            if (words[i].Length > 0 && list.BinarySearch(words[i]) == 0)
                return true;
        }
        return false;
    }
     * */
    [WebMethod]
    public string CheckAuctionStatus(string SiteId, string IncidentId)
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        string xmlSuppliers = customer.GetSuppliersStatus(IncidentId);
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(xmlSuppliers);
        if (xdd["Suppliers"] == null || xdd["Suppliers"].Attributes.Count == 0 || xdd["Suppliers"].Attributes["IncidentStatus"] == null || xdd["Suppliers"]["Error"] != null)
            return _FALSE;
        return xdd["Suppliers"].Attributes["IncidentStatus"].Value;
    }

    [WebMethod]
    public string CheckAuctionStatusWithSuppliers(string siteId, string incidentId)
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
        string xmlSuppliers = customer.GetSuppliersStatus(incidentId);
        string xml = AddSiteAndLogoToSuppliers(siteId, xmlSuppliers);
        return xml;
    }

    private string AddSiteAndLogoToSuppliers(string siteId, string xmlSuppliers)
    {
        XDocument doc = XDocument.Parse(xmlSuppliers);
        string logosUrl = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];
        string logosFile = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
        string siteAddress = GetSiteAddress(siteId);
        string miniSiteUrl = "http://" + siteAddress + "/Professional.aspx?supplierGUID=";
        IEnumerable<XElement> supps = doc.Elements("Suppliers").Elements("Supplier");
        foreach (var supplier in supps)
        {
            string supplierId = supplier.Attribute("SupplierId").Value;
            string logoUrlReady = logosUrl + supplierId + ".jpg";
            string logoFile = logosFile + supplierId + ".jpg";
            if (File.Exists(logoFile))
                supplier.SetAttributeValue("Logo", logoUrlReady);
            else
                supplier.SetAttributeValue("Logo", "");
            string miniSiteUrlReady = miniSiteUrl + supplierId + "&" + "SiteId=" + siteId;
            supplier.SetAttributeValue("Site", miniSiteUrlReady);
        }
 //       string xml = System.Web.HttpUtility.HtmlDecode(doc.ToString());
        return doc.ToString();
    }

    private string GetSiteAddress(string siteId)
    {
        string retVal = string.Empty;
        string command = "SELECT dbo.GetUrlBySiteNameId(@SiteNameId)";
        using (SqlConnection connection = DBConnection.GetConnString())
        {
            connection.Open();
            SqlCommand cmd = new SqlCommand(command, connection);

            /*
            command.CommandText = string.Format("select URL from tbl_Site where SiteNameId = '{0}'", siteId);
            command.Connection = connection;
            command.Connection.Open();
                * */
            cmd.Parameters.AddWithValue("@SiteNameId", siteId);
            retVal = (string)cmd.ExecuteScalar();
            /*
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    retVal = reader["URL"].ToString();
                }
            }
                * */
            connection.Close();
        }
        return retVal;
    }

    [WebMethod]
    public string SearchSuppliersExpanded(string serviceAreaCode, int serviceAreaType,
        string expertiseCode, int expertiseType, string originId, string siteId, int maxResults, int pageNumber, int pageSize)
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
        WebReferenceCustomer.SearchRequest request = new WebReferenceCustomer.SearchRequest();
        request.ExpertiseCode = expertiseCode;
        request.ExpertiseLevel = expertiseType;
        request.MaxResults = maxResults;
        request.OriginId = originId;
        request.RegionCode = serviceAreaCode;
        request.RegionLevel = serviceAreaType;
        request.SiteId = siteId;
        request.PageNumber = pageNumber;
        request.PageSize = pageSize;
        string result = customer.SearchSiteSuppliersExpanded(request);
        result = AddSiteAndLogoToSuppliers(siteId, result);
        return result;
    }

    [WebMethod]
    public string SearchSuppliers(string serviceAreaCode, int serviceAreaType,
        string expertiseCode, int expertiseType, string originId, string siteId, int maxResults, int pageNumber, int pageSize)
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
        WebReferenceCustomer.SearchRequest request = new WebReferenceCustomer.SearchRequest();
        request.ExpertiseCode = expertiseCode;
        request.ExpertiseLevel = expertiseType;
        request.MaxResults = maxResults;
        request.OriginId = originId;
        request.RegionCode = serviceAreaCode;
        request.RegionLevel = serviceAreaType;
        request.SiteId = siteId;
        request.PageNumber = pageNumber;
        request.PageSize = pageSize;
        string result = customer.SearchSiteSuppliers(request);
        return result;
    }

    [WebMethod]
    public string SearchSuppliersDapazPoc(string headingCodeDapaz, string mekomonCode, string originId, string siteId)
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
        string result = customer.SearchSuppliersDapazPoc(headingCodeDapaz, mekomonCode, new Guid(originId));
        return result;
    }

    [WebMethod]
    public string SearchSuppliersWithExposure(string serviceAreaCode, int serviceAreaType,
        string expertiseCode, int expertiseType, string originId, string siteId, int maxResults, int pageNumber, int pageSize, string Domain, string Url, string controlName, string pageName, string placeInWebSite, string sessionId)
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
        WebReferenceCustomer.SearchRequestWithExposure request = new WebReferenceCustomer.SearchRequestWithExposure();
        request.ExpertiseCode = expertiseCode;
        request.ExpertiseLevel = expertiseType;
        request.MaxResults = maxResults;
        request.OriginId = originId;
        request.RegionCode = serviceAreaCode;
        request.RegionLevel = serviceAreaType;
        request.SiteId = siteId;
        request.PageNumber = pageNumber;
        request.PageSize = pageSize;
        request.Domain = Domain;
        request.Url = Url;
        request.ControlName = controlName;
        request.PageName = pageName;
        request.PlaceInWebSite = placeInWebSite;        
        request.SessionId = sessionId;
       
        string result = customer.SearchSiteSuppliersWithExposure(request);
        return result;
    }

    [WebMethod]
    public string SearchSuppliersExpandedWithExposure(string serviceAreaCode, int serviceAreaType,
        string expertiseCode, int expertiseType, string originId, string siteId, int maxResults, int pageNumber, int pageSize, string Domain, string Url, string controlName, string pageName, string placeInWebSite, string sessionId)
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
        WebReferenceCustomer.SearchRequestWithExposure request = new WebReferenceCustomer.SearchRequestWithExposure();
        request.ExpertiseCode = expertiseCode;
        request.ExpertiseLevel = expertiseType;
        request.MaxResults = maxResults;
        request.OriginId = originId;
        request.RegionCode = serviceAreaCode;
        request.RegionLevel = serviceAreaType;
        request.SiteId = siteId;
        request.PageNumber = pageNumber;
        request.PageSize = pageSize;
        request.Domain = Domain;
        request.Url = Url;
        request.ControlName = controlName;
        request.PageName = pageName;
        request.PlaceInWebSite = placeInWebSite;
        request.SessionId = sessionId;
       
        string result = customer.SearchSiteSuppliersExpandedWithExposure(request);
        result = AddSiteAndLogoToSuppliers(siteId, result);
        return result;
    }

    [WebMethod]
    public string SearchAvailableAndUnavailableSuppliers(string serviceAreaCode, int serviceAreaType,
        string expertiseCode, int expertiseType, string originId, string siteId, int maxResults, int pageNumber, int pageSize, string Domain, string Url, string controlName, string pageName, string placeInWebSite, string sessionId)
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
        WebReferenceCustomer.SearchRequestWithExposure request = new WebReferenceCustomer.SearchRequestWithExposure();
        request.ExpertiseCode = expertiseCode;
        request.ExpertiseLevel = expertiseType;
        request.MaxResults = maxResults;
        request.OriginId = originId;
        request.RegionCode = serviceAreaCode;
        request.RegionLevel = serviceAreaType;
        request.SiteId = siteId;
        request.PageNumber = pageNumber;
        request.PageSize = pageSize;
        request.Domain = Domain;
        request.Url = Url;
        request.ControlName = controlName;
        request.PageName = pageName;
        request.PlaceInWebSite = placeInWebSite;
        request.SessionId = sessionId;

        string result = customer.SearchAvailableAndUnavailableSuppliers(request);        
        return result;
    }

    [WebMethod]
    public string SearchAvailableAndUnavailableSuppliersIncludeLogo(string serviceAreaCode, int serviceAreaType,
        string expertiseCode, int expertiseType, string originId, string siteId, int maxResults, int pageNumber, int pageSize, string Domain, string Url, string controlName, string pageName, string placeInWebSite, string sessionId)
    {
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
        WebReferenceCustomer.SearchRequestWithExposure request = new WebReferenceCustomer.SearchRequestWithExposure();
        request.ExpertiseCode = expertiseCode;
        request.ExpertiseLevel = expertiseType;
        request.MaxResults = maxResults;
        request.OriginId = originId;
        request.RegionCode = serviceAreaCode;
        request.RegionLevel = serviceAreaType;
        request.SiteId = siteId;
        request.PageNumber = pageNumber;
        request.PageSize = pageSize;
        request.Domain = Domain;
        request.Url = Url;
        request.ControlName = controlName;
        request.PageName = pageName;
        request.PlaceInWebSite = placeInWebSite;
        request.SessionId = sessionId;

        string result = customer.SearchAvailableAndUnavailableSuppliers(request);
        result = AddSiteAndLogoToSuppliers(siteId, result);
        return result;
    }

    [WebMethod]
    public WebReferenceCustomer.ResultOfListOfExpertiseData GetSuppliersCountPerExpertise(string areaCode, int areaType,
        string expertiseName, string siteId)
    {
        WebReferenceCustomer.ResultOfListOfExpertiseData result = new WebReferenceCustomer.ResultOfListOfExpertiseData();
        try
        {
            WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, siteId);
            result = customer.GetSuppliersCount(expertiseName, areaCode, areaType);
        }
        catch (Exception exc)
        {
            result.Type = WebReferenceCustomer.eResultType.Failure;
            result.Messages = new string[] { "Unexpected exception was thrown at web level.", exc.Message };
        }
        return result;
    }

    [WebMethod]
    public WebReferenceCustomer.ResultOfListOfRegionCodeNamePair
        GetRegionsByParentCode(string parentCode, string siteId)
    {
        WebReferenceCustomer.GetRegionsByParentCodeRequest request = new WebReferenceCustomer.GetRegionsByParentCodeRequest();
        request.ParentCode = parentCode;
        request.SiteId = siteId;
        WebReferenceCustomer.CustomersService customerClient = WebServiceConfig.GetCustomerReference(null, siteId);
        var result = customerClient.GetRegionsByParentCode(request);
        return result;
    }

    [WebMethod]
    public string GetAllExpertisesWithIsActive(string siteId)
    {
        WebReferenceSite.Site client = WebServiceConfig.GetSiteReference(null, siteId);
        var response = client.GetAllExpertisesWithIsActive();
        string retVal = string.Empty;
        if (response.Type == WebReferenceSite.eResultType.Success)
        {
            XElement doc = new XElement("AllExpertises");
            
            foreach (var item in response.Value)
            {
                XElement element = new XElement("Expertise");
                element.Add(new XAttribute("Code", item.Code));
                element.Add(new XAttribute("Name", item.Name));
                element.Add(new XAttribute("IsActive", item.IsActive.ToString()));
                doc.Add(element);
            }
            retVal = doc.ToString(SaveOptions.DisableFormatting);
        }
        else
        {
            retVal = "Error";
        }

        
        return retVal;
    }
}

