using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Globalization;
using System.Text;

/// <summary>
/// Summary description for WebServiceAccounting
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WebServiceAccounting : System.Web.Services.WebService {

    private string _companyName="";

    public WebServiceAccounting () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
     
  

    [WebMethod]
    public void accounting(string paymentMehod,string invoice4youUsername,string invoice4youKey,string CompanyGuid,
        string CompanyId, string AccountNumber, string CompanyName, string CompanyAddress, string CompanyCity,
        string CompanyState, string CompanyZipcode, string CompanyTel1, string CompanyTel2, string CompanyCell,
        string CompanyFax, string CompanyEmail, string CompanyWebsite, string CompanyComments, string vat,
        string total, string discount, string siteId,string siteLangId, Guid PricingId, string dateTimeFormat,
        string last4, string currency, string deposit, string ppcPackage, string extraBonus, string checkDate,  
        string checkNumber, string checkAccount, string checkBranch, string checkBank,
        string ccNumber, string ccPaymentNum, string ccExpiration, string ccType, string ccVerification, string ccTicket,
        string ccDate, string transDate, string transBank, string transBranch, string transAccount)
    {

        StringBuilder sb = new StringBuilder();
        LogEdit.SaveLog("Accounting_" + siteLangId, "Accounting entered to supplier " + CompanyName);

            string responseCode = "";
            string DocNumber = "";
            ////bool ifOkToSendEmail = false;


            ////string Username = "a4u_heb";    // a4u_eng for test, noproblem for production
            string Username = invoice4youUsername;
            //string Username = CompanyId;
            ////string Key = "";
            string Key = invoice4youKey; // empty for test, /ncRTWVpO/JJOjGzYLfa8H0vU//mgGlD for production , 
            //string CompanyCode = "111111111";  // can start from 1 for constant customer
            string CompanyCode = ""; // for temp customer
            /*
            CompanyName = "shai marekevitch";
            string CompanyAddress = "nachal 23";
            string CompanyCity = "modiin";
            string CompanyState = "israel";
            string CompanyZipcode = "71701";
            string CompanyTel1 = "0545350800";
            string CompanyTel2 = "035678987";
            string CompanyCell = "0543456567";
            string CompanyFax = "034567898";
            string CompanyEmail = "shaim@onecall.co.il";
            string CompanyWebsite = "www.a.co.il";
            string CompanyComments = "first try";
            string CompanyTax = "3-2"; // limited to 10 chars and valid no' just to israel
            */

            DataSet ds;

            try
            {
               
                /********** for constant customer *************/
                
                il.co.invoice4u.account.w_company companyInvoice4you = new il.co.invoice4u.account.w_company();
                ////CompanyCode = Convert.ToString(50000 + Convert.ToInt32(AccountNumber)); // for known customer
                CompanyCode = AccountNumber; // for known customer
                ds = companyInvoice4you.RETRIEVE(Username, Key, CompanyCode);
                
                //HttpContext.Current.Response.Write("****** retrieve company ******************<br>");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //HttpContext.Current.Response.Write(ds.Tables[0].Rows[i].ItemArray[0].ToString() + ": " + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "<br>");
                    if (ds.Tables[0].Rows[i].ItemArray[0].ToString() == "ResponseCode")
                        responseCode = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                }

                LogEdit.SaveLog("Accounting_" + siteLangId, "invoice4you - Retrieve " + CompanyName + "\r\nCompanyCode: " + CompanyCode + " Response code: " + responseCode );

                if (responseCode != "100") // company not exist
                {
                    LogEdit.SaveLog("Accounting_" + siteLangId, "invoice4you - first time " + CompanyName);

                    sb.Append("Username:" + Username);
                    sb.AppendLine("Key:" + Key);
                    sb.AppendLine("CompanyCode:" + CompanyCode);
                    sb.AppendLine("CompanyName:" + CompanyName);
                    sb.AppendLine("CompanyAddress:" + CompanyAddress);
                    sb.AppendLine("CompanyCity:" + CompanyCity);
                    sb.AppendLine("CompanyState:" + CompanyState);
                    sb.AppendLine("CompanyZipcode:" + CompanyZipcode);
                    sb.AppendLine("CompanyTel1:" + CompanyTel1);
                    sb.AppendLine("CompanyTel2:" + CompanyTel2);
                    sb.AppendLine("CompanyCell:" + CompanyCell);
                    sb.AppendLine("CompanyFax:" + CompanyFax);
                    sb.AppendLine("CompanyEmail:" + CompanyEmail);
                    sb.AppendLine("CompanyWebsite:" + CompanyWebsite);
                    sb.AppendLine("CompanyComments:" + CompanyComments);
                    sb.AppendLine("CompanyId:" + CompanyId);

                    ds = companyInvoice4you.INSERT(Username, Key, CompanyCode, CompanyName,
                    CompanyAddress, CompanyCity, CompanyState, CompanyZipcode, CompanyTel1, CompanyTel2,
                    CompanyCell, CompanyFax, CompanyEmail, CompanyWebsite, CompanyComments, CompanyId);

                    //HttpContext.Current.Response.Write("****** insert company ******************<br>");

                    LogEdit.SaveLog("Accounting_" + siteLangId, "invoice4you - After send to method create  " + CompanyName + "\r\n" + sb.ToString());

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        //HttpContext.Current.Response.Write(ds.Tables[0].Rows[i].ItemArray[0].ToString() + ": " + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "<br>");
                        if (ds.Tables[0].Rows[i].ItemArray[0].ToString() == "ResponseCode")
                        {
                            responseCode = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                            LogEdit.SaveLog("Accounting_" + siteLangId, "invoice4you - insert  " + CompanyName + "\r\nResponse code: " + responseCode);
                        }

                       
                        
                    }

                    if (responseCode == "100") // company not exist
                    {
                        ////ifOkToSendEmail = true;
                    }
                }

                else
                {
                    ds=companyInvoice4you.UPDATE(Username, Key, CompanyCode, CompanyName,
                    CompanyAddress, CompanyCity, CompanyState, CompanyZipcode, CompanyTel1, CompanyTel2,
                    CompanyCell, CompanyFax, CompanyEmail, CompanyWebsite, CompanyComments, CompanyId);

                    sb.Append("Username:" + Username);
                    sb.AppendLine("Key:" + Key);
                    sb.AppendLine("CompanyCode:" + CompanyCode);
                    sb.AppendLine("CompanyName:" + CompanyName);
                    sb.AppendLine("CompanyAddress:" + CompanyAddress);
                    sb.AppendLine("CompanyCity:" + CompanyCity);
                    sb.AppendLine("CompanyState:" + CompanyState);
                    sb.AppendLine("CompanyZipcode:" + CompanyZipcode);
                    sb.AppendLine("CompanyTel1:" + CompanyTel1);
                    sb.AppendLine("CompanyTel2:" + CompanyTel2);
                    sb.AppendLine("CompanyCell:" + CompanyCell);
                    sb.AppendLine("CompanyFax:" + CompanyFax);
                    sb.AppendLine("CompanyEmail:" + CompanyEmail);
                    sb.AppendLine("CompanyWebsite:" + CompanyWebsite);
                    sb.AppendLine("CompanyComments:" + CompanyComments);
                    sb.AppendLine("CompanyId:" + CompanyId);

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        //HttpContext.Current.Response.Write(ds.Tables[0].Rows[i].ItemArray[0].ToString() + ": " + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "<br>");
                        if (ds.Tables[0].Rows[i].ItemArray[0].ToString() == "ResponseCode")
                        {
                            responseCode = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                            LogEdit.SaveLog("Accounting_" + siteLangId, "invoice4you - update  " + CompanyName + "\r\nResponse code: " + responseCode);
                        }
                    }

                    LogEdit.SaveLog("Accounting_" + siteLangId, "invoice4you - not first time " + CompanyName + "\r\n" + sb.ToString());

                    ////ifOkToSendEmail = true;
                }
               
               
                /********** end for constant customer *************/
                //if (ifOkToSendEmail)
                //{
                    //HttpContext.Current.Response.Write("******** create invoice ****************<br>");
                    /*
                    string InvoiceDate="";

                    string Day;
                    Day = DateTime.Now.Day.ToString().Length == 1 ? "0"
                        + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();


                    string Month;
                    Month = DateTime.Now.Month.ToString().Length == 1 ? "0"
                        + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();

                    string Year;
                    Year = DateTime.Now.Year.ToString().Length == 2 ? "20"
                        + DateTime.Now.Year.ToString() : DateTime.Now.Year.ToString();

                    if (invoice4youUsername == "a4u_heb") // test hebrew
                        InvoiceDate = Day + @"/" + Month + @"/" + Year; // ??? always sent the date as dd/mm/yyyy. if the user site language in invoice4you is set to english it will convert it to mm/dd/yyyy auto
                    else if (invoice4youUsername == "a4u_eng") // test english
                        InvoiceDate = Month + @"/" + Day + @"/" + Year; 
                    else if (currency.ToLower() == "ils") // israel                    
                        InvoiceDate = Day + @"/" + Month + @"/" + Year; // ??? always sent the date as dd/mm/yyyy. if the user site language in invoice4you is set to english it will convert it to mm/dd/yyyy auto                    
                    else
                        InvoiceDate = Month + @"/" + Day + @"/" + Year; 
                    */

                    //string vat=System.Configuration.ConfigurationManager.AppSettings["vat"];
                    double vat2 = (Convert.ToDouble(vat) + 100)/Convert.ToDouble(100);
                    double payBeforeVat = double.Parse(deposit) / vat2;

                    //string InvoiceSubject = ppcPackage;
                    string InvoiceSubject = "���� ���: " + AccountNumber;
                    string InvoiceDiscount = "0";
                    //float discountTax = float.Parse(discount) / float.Parse(total);
                    string InvoiceDiscountRate = "0";
                    string InvoiceItemCode = "--";
                    string InvoiceItemDescription = "����� ���������";
                    string InvoiceItemQuantity = "1";
                    string InvoiceItemPrice = payBeforeVat.ToString();

                    string InvoiceTaxRate=vat;
                    string InvoiceComments="";

                    int bonus;
                    if (String.IsNullOrEmpty(discount))
                        bonus = 0;
                    else
                        bonus = int.Parse(discount);

                    int ExtraBonus;
                    if (String.IsNullOrEmpty(extraBonus))
                        ExtraBonus = 0;
                    else
                        ExtraBonus = int.Parse(extraBonus);

                    string allBonus;
                    allBonus = (bonus + ExtraBonus).ToString();

                    if (currency.ToLower() == "ils") // israel
                    {
                        //InvoiceComments = "������ ���� ����� ����� ����� �������� � " + last4 + "<br>���� ����� ��� �� " + discount + " �\"�";
                        InvoiceComments = "������ �� ����� ���� ����� ��� " + allBonus + " �\"�" + " ������ ������!";
                        
                    }
                    else
                    {
                        //InvoiceComments = "Payment made with a credit card that ends on " + last4 + "<br>incloding bunus of $" + discount;
                        InvoiceComments = "Incloding bunus of $" + allBonus;

                    }

                    /* for temo users
                    string CompanyInfo =  CompanyName + ", �����: " + CompanyAddress +
                        " " + CompanyCity + " " + CompanyState + " " + CompanyZipcode +
                        ", ���� ���� �����: " + CompanyId + ", ���� ���: " + AccountNumber; // for temp customer
                    */

                    string CompanyInfo = "";

                    string MailTo = CompanyEmail + ",invoices@noproblem.co.il";

                    //// string MailTo = CompanyEmail + ",invoices@noproblem.co.il,daniel@noproblem.co.il,einath@noproblem.co.il,lilacht@noproblem.co.il";

                    il.co.invoice4u.account.w_invoicereceipt acoountInvoice4you = new il.co.invoice4u.account.w_invoicereceipt();
                    /*  for connstant customer *****/
                    /*
                    ds = acoountInvoice4you.CREATE(Username, Key, InvoiceDate, InvoiceSubject, InvoiceDiscount,
                           InvoiceDiscountRate, InvoiceItemCode, InvoiceItemDescription, InvoiceItemQuantity,
                           InvoiceItemPrice, InvoiceTaxRate, InvoiceComments, CompanyCode, CompanyInfo,
                           MailTo);
                    */

                    LogEdit.SaveLog("Accounting_" + siteLangId, "invoice4you - Before send to method create  " + CompanyName);
                     
                    /*
                        ds = acoountInvoice4you.CREATE(Username, Key, InvoiceDate, InvoiceSubject, InvoiceDiscount,
                              InvoiceDiscountRate, InvoiceItemCode, InvoiceItemDescription, InvoiceItemQuantity,
                              InvoiceItemPrice, InvoiceTaxRate, InvoiceComments, CompanyCode, CompanyInfo,
                              MailTo);
                    */

                    /*
                    string Bank="";
                    string BankBranch="";
                    string BankAccount = "";
                    */

                    
                    string IsItemPriceWithTax = ""; // 1 for yes                   
                    
                    string Cash="";  
                    string CheckDate="";  
                    string CheckNumber=""; 
                    string CheckAccount="";  
                    string CheckBranch="";  
                    string CheckBank="";  
                    string CheckAmount="";  
                    string CcNumber="";  
                    string CcExpiration="";
                    string CcPaymentNum = "";
                    string CcType="";  
                    string CcVerification="";  
                    string CcTicket="";  
                    string CcAmount="";  
                    string CcDate="";  
                    string TransDate="";  
                    string TransBank="";  
                    string TransBranch="";  
                    string TransAccount="";
                    string TransAmount = ""; 
                           
                    switch (paymentMehod.ToLower())
                    {
                        case "credit card":

                            CcNumber = ccNumber;
                            CcExpiration = ccExpiration;
                            CcPaymentNum = ccPaymentNum;
                            CcType = ccType;
                            CcVerification = ccVerification;
                            CcTicket = ccTicket;
                            CcAmount = deposit;
                            CcDate = ccDate;  

                            break;

                        case "cash":
                            Cash = deposit;
                            break;

                        case "check":
                            CheckDate = checkDate;
                            
                            DateTime CheckDateParse;

                            if (!string.IsNullOrEmpty(CheckDate))
                            {     
                                
                                CultureInfo culture;
                                culture = CultureInfo.CreateSpecificCulture("en-US");
                                culture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";

                                if (!DateTime.TryParse(CheckDate, culture, DateTimeStyles.None, out CheckDateParse))
                                    CheckDate = "";

                            }

                            CheckNumber = checkNumber;
                            CheckBank = checkBank;
                            CheckBranch = checkBranch;
                            CheckAccount = checkAccount;
                            CheckAmount = deposit;
                            break;

                        case "wiretransfer":
                            TransDate = transDate;
                            DateTime TransDateParse;

                            if (!string.IsNullOrEmpty(TransDate))
                            {
                                // need to convert to dd/mm/yyyy format
                                CultureInfo culture;
                                culture = CultureInfo.CreateSpecificCulture("en-US");
                                culture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";

                                if (DateTime.TryParse(TransDate, culture, DateTimeStyles.None, out TransDateParse))
                                    TransDate = TransDateParse.Day.ToString() + "/" + TransDateParse.Month.ToString() + "/" + TransDateParse.Year.ToString(); 

                            }

                            TransBank = transBank;
                            TransBranch = transBranch;
                            TransAccount = transAccount;
                            TransAmount = deposit; 
                            break;

                        default:
                            break;
                    }


                    
                    ds = acoountInvoice4you.CreateWithPaymentMethods2("", Username, Key, InvoiceSubject, InvoiceDiscount,
                        InvoiceDiscountRate, InvoiceItemCode, InvoiceItemDescription, InvoiceItemQuantity,
                        InvoiceItemPrice, InvoiceTaxRate, InvoiceComments, CompanyCode, CompanyInfo,
                        MailTo, IsItemPriceWithTax, Cash, CheckDate, CheckNumber, CheckAccount,
                        CheckBranch, CheckBank, CheckAmount, CcNumber, CcPaymentNum, CcExpiration, CcType,
                        CcVerification, CcTicket, CcAmount, CcDate, TransDate, TransBank, TransBranch,
                        TransAccount, TransAmount);                        

                        sb.Clear();
                        sb.Append("Username: " + Username);
                        sb.AppendLine("Key: " + Key);
                        sb.AppendLine("InvoiceSubject: " + InvoiceSubject);
                        sb.AppendLine("InvoiceDiscount: " + InvoiceDiscount);
                        sb.AppendLine("InvoiceDiscountRate: " + InvoiceDiscountRate);
                        sb.AppendLine("InvoiceItemCode: " + InvoiceItemCode);
                        sb.AppendLine("InvoiceItemDescription: " + InvoiceItemDescription);
                        sb.AppendLine("InvoiceItemQuantity: " + InvoiceItemQuantity);
                        sb.AppendLine("InvoiceItemPrice: " + InvoiceItemPrice);
                        sb.AppendLine("InvoiceTaxRate: " + InvoiceTaxRate);
                        sb.AppendLine("InvoiceComments: " + InvoiceComments);
                        sb.AppendLine("CompanyCode: " + CompanyCode);
                        sb.AppendLine("CompanyInfo: " + CompanyInfo);
                        sb.AppendLine("MailTo: " + MailTo);
                        sb.AppendLine("IsItemPriceWithTax: " + IsItemPriceWithTax);
                        sb.AppendLine("Cash: " + Cash);
                        sb.AppendLine("CheckDate: " + CheckDate);
                        sb.AppendLine("CheckNumber: " + CheckNumber);
                        sb.AppendLine("CheckAccount: " + CheckAccount);
                        sb.AppendLine("CheckBranch: " + CheckBranch);
                        sb.AppendLine("CheckBank: " + CheckBank);
                        sb.AppendLine("CheckAmount: " + CheckAmount);
                        sb.AppendLine("CcNumber: " + CcNumber);
                        sb.AppendLine("CcPaymentNum: " + CcPaymentNum);
                        sb.AppendLine("CcExpiration: " + CcExpiration);
                        sb.AppendLine("CcType: " + CcType);
                        sb.AppendLine("CcVerification: " + CcVerification);
                        sb.AppendLine("CcTicket: " + CcTicket);
                        sb.AppendLine("CcAmount: " + CcAmount);
                        sb.AppendLine("CcDate: " + CcDate);
                        sb.AppendLine("TransDate: " + TransDate);
                        sb.AppendLine("TransBank: " + TransBank);
                        sb.AppendLine("TransBranch: " + TransBranch);
                        sb.AppendLine("TransAccount: " + TransAccount);
                        sb.AppendLine("TransAmount: " + TransAmount);

                        LogEdit.SaveLog("Accounting_" + siteLangId, "invoice4you - After send to method create  " + CompanyName + "\r\n" + sb.ToString());
                        /*
                        ResponseCode: 100
                        ResponseName: Success
                        DocNumber: 73490
                        */

                        string DocURL = "";

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            //HttpContext.Current.Response.Write(ds.Tables[0].Rows[i].ItemArray[0].ToString() + ": " + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "<br>");
                            if (ds.Tables[0].Rows[i].ItemArray[0].ToString() == "ResponseCode")
                                responseCode = ds.Tables[0].Rows[i].ItemArray[1].ToString();

                            if (ds.Tables[0].Rows[i].ItemArray[0].ToString() == "DocURL")
                                DocURL = ds.Tables[0].Rows[i].ItemArray[1].ToString();

                            if (ds.Tables[0].Rows[i].ItemArray[0].ToString() == "DocNumber")                            
                                DocNumber = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                                
                           
                        }

                        //HttpContext.Current.Response.Write("******** pdf details ****************<br>");
                        /*
                         html https://account.invoice4u.co.il/User/Print/print_invoice_receipt.aspx?x=cHMy5JUPyl3QvurVlvIIXy%2fdpYYaVqd2lpS2QpwDmoaWuxycRN5MGX2YRPb85pp7gktJCg8WBohtkGoAl3yJUaTOQPFTLt3onDsVLHcgaJF80c4%2fDPWUrOZsUM73FG5f0JMrFD4WcwKd%2ftZxVzayvw%3d%3d
                         pdf  http://account.invoice4u.co.il/pdfprint.aspx?x=cHMy5JUPyl3QvurVlvIIXy%2fdpYYaVqd2lpS2QpwDmoaWuxycRN5MGX2YRPb85pp7gktJCg8WBohtkGoAl3yJUaTOQPFTLt3onDsVLHcgaJF80c4%2fDPWUrOZsUM73FG5f0JMrFD4WcwKd%2ftZxVzayvw%3d%3d&type=1
                        */

                        string preDocURL = "https://account.invoice4u.co.il/User/Print/print_invoice_receipt.aspx?";
                        int preDocURLLength = preDocURL.Length;

                        string suffixDocURL = DocURL.Substring(preDocURLLength);

                        string DocURLPdf = "http://account.invoice4u.co.il/pdfprint.aspx?" +
                            suffixDocURL + "&type=1";

                        //HttpContext.Current.Response.Write(DocURLPdf);
                        if (responseCode != "100")
                            LogEdit.SaveLog("Accounting_" + siteLangId, "Some response problem in Refund process for " + CompanyName + "\r\nResponseCode" + responseCode);
                        else
                            LogEdit.SaveLog("Accounting_" + siteLangId, "Accounting Invoice4You succeeded for " + CompanyName + "\r\n" + DocURLPdf);

                        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, siteId);
                        WebReferenceSupplier.UpdateInvoicePdfLinkRequest invoiceLink = new WebReferenceSupplier.UpdateInvoicePdfLinkRequest();
                        invoiceLink.InvoicePdfLink = DocURLPdf;
                        invoiceLink.PricingId = PricingId;
                        invoiceLink.InvoiceNumber = DocNumber;

                        WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
                        result = supplier.UpdateInvoicePdfLink(invoiceLink);

                        if (result.Type == WebReferenceSupplier.eResultType.Success)
                            LogEdit.SaveLog("Accounting_" + siteLangId, "Crm: Update pdf link " + CompanyName + "\r\n" + result.Type.ToString());

                        else
                            LogEdit.SaveLog("Accounting_" + siteLangId, "Accounting succeed crm  UpdateInvoicePdfLink "
                                + CompanyName + "\r\n" + result.Messages[0]);
                    
                //}
            }

            catch (Exception ex)
            {
                
                LogEdit.SaveLog("Accounting_" + siteLangId, "Some problem in Accounting process for " + CompanyName + "\r\n" + ex.Message);

             
                string publisherEmail = Utilities.GetEmailNotification(null, siteId);
                MailSender mail = new MailSender();
                mail.From = "rona@noproblem.co.il";
                mail.FromName = "NO PROBLEM billing";
                mail.To = "rona@noproblem.co.il";
                mail.Bcc = "shaip@onecall.co.il;rona@onecall.co.il;shaim@onecall.co.il;" +
                         (!string.IsNullOrEmpty(publisherEmail) ? publisherEmail + ";" : "");


                mail.Subject = "Problem with invoive4you : " +  CompanyName;
                mail.IsText = true;
                mail.Body = "Accounting company: " + CompanyName + "\r\n" +
                    "Site Id: " + siteId + "\r\n" +
                    "Site Lang Id: " + siteLangId + "\r\n" +
                    "Advertiser id: " + AccountNumber + "\r\n" +                    
                    "Amount: " + deposit + "\r\n" +
                    "Time: " + DateTime.Now.ToString() + "\r\n" +
                    "Message: " + ex.Message + "\r\n" +
                    "StackTrace: " + ex.StackTrace;

                bool ifOkSentMail = mail.Send();
            }


    }

    [WebMethod]
    public void setInvoice(string siteId, string siteLangId ,Guid PricingId, string DocURLPdf, string DocNumber)
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, siteId);
        WebReferenceSupplier.UpdateInvoicePdfLinkRequest invoiceLink = new WebReferenceSupplier.UpdateInvoicePdfLinkRequest();
        invoiceLink.InvoicePdfLink = DocURLPdf;
        invoiceLink.PricingId = PricingId;
        invoiceLink.InvoiceNumber = DocNumber;

        WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
        result = supplier.UpdateInvoicePdfLink(invoiceLink);

        if (result.Type == WebReferenceSupplier.eResultType.Success)
            LogEdit.SaveLog("Accounting_" + siteLangId, "Crm: Update invoice html link " + CompanyName + "\r\n" + result.Type.ToString());

        else
            LogEdit.SaveLog("Accounting_" + siteLangId, "Accounting succeed crm  UpdateInvoiceHtmlLink "
                + CompanyName + "\r\n" + result.Messages[0]);
    }

    [WebMethod]
    public void refund(string siteId,string siteLangId, string CompanyName, string invoice4youUsername, string invoice4youKey, string InvoiceSubject, string InvoiceReceiptNumber, string InvoiceReceiptAmount, string InvoiceTaxRate, string InvoiceComments, string CompanyCode, string MailTo ,Guid PricinigId)
    {
        LogEdit.SaveLog("Refund_" + siteLangId, "Refund entered to supplier " + CompanyName);
        string Username = invoice4youUsername;
        string Key = invoice4youKey; // empty for test, /ncRTWVpO/JJOjGzYLfa8H0vU//mgGlD for production , 
        
        //string InvoiceSubject = "���� ���: " + AccountNumber;
        try
        {
            il.co.invoice4u.account.w_invoicecredit companyInvoice4you = new il.co.invoice4u.account.w_invoicecredit();
            DataSet ds;
            ds=companyInvoice4you.CREATE(Username, Key, InvoiceSubject, "", "", InvoiceReceiptNumber, InvoiceReceiptAmount, InvoiceTaxRate, InvoiceComments, CompanyCode, MailTo);

            string DocURL = "";
            string DocNumber = "";
            string ResponseCode="";

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //HttpContext.Current.Response.Write(ds.Tables[0].Rows[i].ItemArray[0].ToString() + ": " + ds.Tables[0].Rows[i].ItemArray[1].ToString() + "<br>");
                if (ds.Tables[0].Rows[i].ItemArray[0].ToString() == "ResponseCode")
                    ResponseCode = ds.Tables[0].Rows[i].ItemArray[1].ToString();

                if (ds.Tables[0].Rows[i].ItemArray[0].ToString() == "DocURL")
                    DocURL = ds.Tables[0].Rows[i].ItemArray[1].ToString();

                if (ds.Tables[0].Rows[i].ItemArray[0].ToString() == "DocNumber")
                    DocNumber = ds.Tables[0].Rows[i].ItemArray[1].ToString();

            }

            if (ResponseCode!="100")
                LogEdit.SaveLog("Refund_" + siteLangId, "Some response problem in Refund process for " + CompanyName + "\r\nResponseCode" +ResponseCode);
            else
                LogEdit.SaveLog("Refund_" + siteLangId, "Refund Invoice4You succeeded for " + CompanyName);

            string preDocURL = "https://account.invoice4u.co.il/User/Print/print_invoice_credit.aspx?";
            int preDocURLLength = preDocURL.Length;

            string suffixDocURL = DocURL.Substring(preDocURLLength);

            string DocURLPdf = "http://account.invoice4u.co.il/pdfprint.aspx?" +
                suffixDocURL + "&type=3";
         

            try
            {
                WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, siteId);
                WebReferenceSupplier.UpdateInvoicePdfLinkRequest invoiceLink = new WebReferenceSupplier.UpdateInvoicePdfLinkRequest();
                invoiceLink.InvoicePdfLink = DocURLPdf;
                invoiceLink.InvoiceNumber = DocNumber;
                invoiceLink.InvoiceNumberRefund = InvoiceReceiptNumber;
                invoiceLink.PricingId = PricinigId;
                
                WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
                result = supplier.UpdateInvoicePdfLink(invoiceLink);

                if(result.Type==WebReferenceSupplier.eResultType.Success)
                    LogEdit.SaveLog("Refund_" + siteLangId, "Refund succeeded crm  UpdateInvoicePdfLink " + CompanyName);
                else
                    LogEdit.SaveLog("Refund_" + siteLangId, "Refund failed crm  UpdateInvoicePdfLink "
                        + CompanyName + "\r\n" + result.Messages[0]);
                
            }

            catch(Exception ex)
            {
                LogEdit.SaveLog("Refund_" + siteLangId, "Some response problem in Refund crm UpdateInvoicePdfLink " + CompanyName + "\r\n" + ex.Message);
            }

                
        }

        catch (Exception ex)
        {
            LogEdit.SaveLog("Refund_" + siteLangId, "Some Server problem in Refund process for " + CompanyName + "\r\n" + ex.Message);
        }
    }

    [WebMethod]
    public string getCompanyName()
    {
        return CompanyName;
    }

   
    public string CompanyName
    {
        get
        {
            return _companyName;
        }

        set
        {
            _companyName = value;
        }

    }


}

