﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;

/// <summary>
/// Summary description for class_enum
/// </summary>
/// 

public enum PagesNames
{
    Publisher, PrePublisher, Advertiser, PreAdvertiser, NewUser, MiniSite, Refund,
    professionalDetails, professionalDates, professionalCallPurchase, professionalPayment, professionChooseProfessions,
    ProfessionalSetting, CreditCard, RefundCall, MyCallDetails, RefundReport, PublisherPayment, GoldenNumberAdvertiser
}
public enum RegionTab { Map, Tree }
public enum DayOfWeek { Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6, Saturday = 7 }
public enum PageType { Page = 1, Enum = 2 }
public enum eChartType { pie = 1, Column = 2, Line = 3 }

public enum eReportIssue { Advertisers = 1, Deposits = 2, Calls = 3, Requests = 4 }
public enum eAdvertiserStatus 
{ 
    Available = 1, 
    Candidate = 2, 
    Inactive = 3, 
    Unavailable = 4,
    FreshlyImported = 5,
    Machine = 6,
    Accessible = 7,
    ITC = 8,
    RemoveMe = 9,
    Expired = 10,
    InRegistration = 11,
    DoNotCallList = 12

}
public enum ePaymentType { MonthlyFeePayment, AutoMaxMonthly, AutoLow, Bonus, BonusExtra, Manual, Recharge }
public enum eCallStatus { WON, LOST }
public enum eSortType { Ascending, Descending }
public enum eRegionLevel { RegionLevel1 = 1, RegionLevel2 = 2, RegionLevel3 = 3, RegionLevel4 = 4, RegionLevel5 = 5 }
public enum eChannelCode
{
    Call = 1,
    Email = 2,
    SMS = 3,
    Fax = 4
}

public enum eOperators
{
    BiggerThan,
    EndBy,
    Equals,
    NotEquals,
    SmallerThan,
    StartAt
}
public enum eConversionChartType
{
    Regular_view,
    Users,
    User_Value,
    EXP__KDAUS,
    Requests__KDAUS
}
    
public enum eWizardFilters
{
    CreatedOn,
    AdvertiserStatus,
    Balance,
    Heading,
    Region,
    AccountManager,
    UnavailabilityReason,
    NumberOfCalls,
    CallStatus,
    Advertiser,
    Amount,
    PricePerCall,
    NumOfRequestedAdvertisers,
    NumOfPayingAdvertisers,
    Revenue,

    PreDefinedPPC,
    PriceChanged,
    UnavailableFrom,
    UnavailableTo,

    ControlName,
    Domain,
    PageName,
    PlaceInWebSite,
    Url,
    Keyword,
    Duration
}

public enum eAdvertiserColumns
{
    CompanyName,
    CompanyId,
    ContactPersonName,
    MainPhone,
    Email,
    Balance,
    Status,
    AccountManager,
    UnavailabilityReason,
    UnavailableFrom,
    UnavailableTo,
    Custom2,
    IncidentPrice,
    Heading,
    VirtualNumber
}
public enum eCallColumns
{
    RequestId,
    CreatedOn,
    ConsumerPhoneNumber,
    Status,
    PricePerCall,
    Advertiser,
    PreDefinedPPC,
    PriceChanged,
    Duration
}
public enum eRequestColumns
{
    RequestId,
    CreatedOn,
    ConsumerPhoneNumber,
    NumOfRequestedAdvertisers,
    NumOfPayingAdvertisers,
    Status,
    Revenue,
    ControlName,
    Domain,
    PageName,
    PlaceInWebSite,
    Url,
    Keyword
}
public enum eDepositColumns
{
    CreatedOn,
    Amount,
    Advertiser
}
public enum eYesNo
{
    Yes,
    No
}
public enum eHelpDeskType
{
    Complaint
}
public enum eGroupDashboard
{
    CallsAndRequests = 1,
    Deposits = 2,
    Advertisers = 3,
    Conversions = 4,
    Refund = 5,
    AAR = 6,
    Reroute = 7
}
public enum eUnavailabilityReasonGroup
{
    OutOfMoney = 1,
    CustomerService = 2,
    TemporaryAvailability = 3
}

public enum eCompanyCharging
{
    pelecard,
    invoice4u,
    paypal,
    platnosci,
    interinfo,
    plimus,
    none
}
public enum eGeneralAuction
{
    Results
}
public enum eChargingKeyValueType
{
    String,
    URL
}
public enum ePaymentMethodSIte { CreditCard, WireTransfer, Voucher, Cash, Check }
public enum ePaymentMethod { CreditCard, WireTransfer, Voucher, Cash, Check }
/*
 Cash = 0,
        Check = 1,
        CreditCard = 2,
        WireTransfer = 3,
        Voucher = 4,
 * */
public enum eWizardSelectType { Advertiser, Consumer, None }
public enum eFileType { NONE, EXCEL, IMAGE }

public enum eDepositStatusResponse
{
    OK,
    Repeated,
    BadVoucherNumber,
    NotEnoughCashInVoucher,
    NotCompleted,
    DateCheckNotValid,
    DateWireTransferNotValid,
    InvalidInvoiceNumber,
    RefundBiggerThanBalance,
    ChargingWithExternalSystemFailed
}
public enum eReasonLogout
{
    SessionEnd
}
public enum eGroupBySoldCallReport
{
    Without,
    RevenuePerHeading,
    RevenuePerDate,
    CountPerHeading,
    CountPerDates
};
public enum eStatusWibiyaSignup
{
    AlreadyRegister,
    PasswordSent
}
public enum eWebUserRegStatus
{
    All,
    WebRegistrant,
    NoneWebRegistrant    
}
public enum eHideSlider
{
    PRESS_X = 0,
    This_session = 1,
    _1_day = 2,
    _2_days = 3,
    _1_week = 4,
    Forever = 5
        //,    This_website = 6
}
public enum eUserType
{
    Affiliate,
    Publisher,
    Supplier,
    Wibiya
}
public enum eFinancialDashboardGroup
{
    Potential = 1,
    Actual = 2,
    NetActual = 3,
   
    RevenuesDirectedToRecruitment = 5,
     RevenuePerLead = 8
}
public enum eFinancialDashboard
{
    AverageDau = 1,
    Exposures = 2,
    Requests = 3,
    Upsales = 4,
    AverageDailyExposures = 5,
    AverageDailyExposuresDividedByAverageDau = 6,
    CostOfAddOnInstallation = 7,
    Conversion = 8,   

    FullFullfilmentRevenue = 11,
    RequestsThatGeneratedFullFullfilmentRevenues = 12,
    DailyFullFullfilmentRevenue = 13,
    FullFullfilmentARDAU = 14,
    LifetimeTimesFullFullfilmentARDAU = 15,
    RoiFullFullfilment = 16, 

    ActualRevenue = 21,
    RequestsThatGeneratedActualRevenues = 22,
    DailyActualRevenue = 23,
    ActualARDAU = 24,
    LifetimeTimesActualARDAU = 25,
    RoiActual = 26,   

      

    NetActualRevenue = 31,
    NetActualRevenueUpsales = 32,
    UpsalesAgain = 33,
    RequestsThatGeneratedNetActualRevenue = 34,
    DailyNetActualRevenue = 35,
    NetActualARDAU = 36,
    LifetimeTimesNetActualRevenueARDAU = 37,        
    RoiNetActual = 38,
   

    PotentialRevenue = 51,
    DailyPotentialRevenue = 52,
    PotentialARDAU = 53,
    LifetimeTimesPotentialARDAU = 54,
    RoiPotential = 55,

    AverageFullFullfilmentPPL = 81,
    AverageActualPPL = 82,
    AverageNetActualPPL = 83,   
    AveragePotentialPPL = 84,
        
}
public enum eFinancialDashboardInjection
{
    AverageDau = 1,
    AverageDailyExposuresDividedByAverageDau = 2,  
    Exposures = 3,
    Requests = 4,
    Upsales = 6,
    Conversion = 7,
    

    FullFullfilmentRevenue = 101,
    TotalCostOfRequestsFullFullfilment = 102,
    CostOfRevenueGeneratingLeadsFullFullfilment = 103,
    CostPerRequestFullFullfilment = 104,
    CostPerMilliaFullFullfilment = 105,
    RevenuePerMilliaFullFullfilment = 106,
    FullFullfilmentARDAU = 107,
    LifetimeTimesFullFullfilmentRevenueARDAU = 108,

    ActualRevenue = 210,
    TotalCostOfRequestsActual = 212,
    CostOfRevenueGeneratingLeadsActual = 213,
    CostPerRequestActual = 214,
    CostPerMilliaActual = 215,
    RevenuePerMilliaActual = 216,
    ActualARDAU = 217,
    LifetimeTimesActualARDAU = 218,
    
    NetActualRevenue = 310,
    NetActualRevenueUpsales = 311,
    UpsalesAgain = 320,
    TotalCostOfRequestsNetActual = 322,
    TotalCostOfRequestsNetActualUpsales = 323,
    CostOfRevenueGeneratingLeadsNetActual = 324,
    CostPerRequestNetActual = 325,
    CostPerMilliaNetActual = 326,
    RevenuePerMilliaNetActual = 327,
    NetActualARDAU = 328,
    LifetimeTimesNetActualRevenueARDAU = 329,
   

    PotentialRevenue = 511,
    TotalCostOfRequestsPotential = 512,
    CostOfRevenueGeneratingLeadsPotential = 513,
    CostPerRequestPotential = 514,
    CostPerMilliaPotential = 515,
    RevenuePerMilliaPotential = 516,
    PotentialARDAU = 517,
    LifetimeTimesPotentialARDAU = 518,
    
    AverageNetActualPPL = 813,
    AverageActualPPL = 812,
    AveragePotentialPPL = 811,
    AverageFullFullfilmentPPL = 814 

}
public enum eFinancialDashboardAddon_Tooltip
{
    AverageDau = 1,
    AverageDailyExposuresDividedByAverageDau = 2,
    Exposures = 3,
    Requests = 4,
    Upsales = 6,
    Conversion = 7,


    FullFullfilmentRevenue = 101,
    TotalCostOfRequestsFullFullfilment = 102,
    CostOfRevenueGeneratingLeadsFullFullfilment = 103,
    CostPerRequestFullFullfilment = 104,
    CostPerMilliaFullFullfilment = 105,
    RevenuePerMilliaFullFullfilment = 106,
    FullFullfilmentARDAU = 107,
    LifetimeTimesFullFullfilmentRevenueARDAU = 108,

    ActualRevenue = 210,
    TotalCostOfRequestsActual = 212,
    CostOfRevenueGeneratingLeadsActual = 213,
    CostPerRequestActual = 214,
    CostPerMilliaActual = 215,
    RevenuePerMilliaActual = 216,
    ActualARDAU = 217,
    LifetimeTimesActualARDAU = 218,

    NetActualRevenue = 310,
    NetActualRevenueUpsales = 311,
    UpsalesAgain = 320,
    TotalCostOfRequestsNetActual = 322,
    TotalCostOfRequestsNetActualUpsales = 323,
    CostOfRevenueGeneratingLeadsNetActual = 324,
    CostPerRequestNetActual = 325,
    CostPerMilliaNetActual = 326,
    RevenuePerMilliaNetActual = 327,
    NetActualARDAU = 328,
    LifetimeTimesNetActualRevenueARDAU = 329,


    PotentialRevenue = 511,
    TotalCostOfRequestsPotential = 512,
    CostOfRevenueGeneratingLeadsPotential = 513,
    CostPerRequestPotential = 514,
    CostPerMilliaPotential = 515,
    RevenuePerMilliaPotential = 516,
    PotentialARDAU = 517,
    LifetimeTimesPotentialARDAU = 518,

    AverageNetActualPPL = 813,
    AverageActualPPL = 812,
    AveragePotentialPPL = 811,
    AverageFullFullfilmentPPL = 814 

}
public enum eFinancialDashboardInjection_Tooltip
{
    AverageDau = 1,
    AverageDailyExposuresDividedByAverageDau = 2,
    Exposures = 3,
    Requests = 4,
    Upsales = 6,
    Conversion = 7,   

    FullFullfilmentRevenue = 11,
    TotalCostOfRequestsFullFullfilment = 12,
    CostOfRevenueGeneratingLeadsFullFullfilment = 13,
    CostPerRequestFullFullfilment = 14,
    CostPerMilliaFullFullfilment = 15,
    RevenuePerMilliaFullFullfilment = 16,
    FullFullfilmentARDAU = 17,
    LifetimeTimesFullFullfilmentRevenueARDAU = 18,

    ActualRevenue = 21,
    TotalCostOfRequestsActual = 22,
    CostOfRevenueGeneratingLeadsActual = 23,
    CostPerRequestActual = 24,
    CostPerMilliaActual = 25,
    RevenuePerMilliaActual = 26,
    ActualARDAU = 27,
    LifetimeTimesActualARDAU = 28,

    NetActualRevenue = 30,
    NetActualRevenueUpsales = 31,
    TotalCostOfRequestsNetActual = 32,
    TotalCostOfRequestsNetActualUpsales = 33,
    CostOfRevenueGeneratingLeadsNetActual = 34,
    CostPerRequestNetActual = 35,
    CostPerMilliaNetActual = 36,
    RevenuePerMilliaNetActual = 37,
    NetActualARDAU = 38,
    LifetimeTimesNetActualRevenueARDAU = 39,
    

    PotentialRevenue = 51,
    TotalCostOfRequestsPotential = 52,
    CostOfRevenueGeneratingLeadsPotential = 53,
    CostPerRequestPotential = 54,
    CostPerMilliaPotential = 55,
    RevenuePerMilliaPotential = 56,
    PotentialARDAU = 57,
    LifetimeTimesPotentialARDAU = 58,

    AverageNetActualPPL = 83,
    AverageActualPPL = 82,
    AveragePotentialPPL = 81,
    AverageFullFullfilmentPPL = 84

}

public enum eDpzReportType
{
    CallReport = 1,

    CallDistributionReportByHour = 2,
    CallDistributionReportByDay = 3,
    CallDistributionReportByMonth = 4,

    ConversationSourceReport = 5,
    NULL = 6

}
public enum eDpzStatusCall
{
    Success,
    Failed,
    AllCalls
}
public enum eMonths
{
    January = 1,
    February = 2,
    March = 3,
    April = 4,
    May = 5,
    June = 6,
    July = 7,
    August = 8,
    September = 9,
    October = 10,
    November = 11,
    December = 12
}
public enum eExposureType
{
 //   unknown = 0,
    Regular = 1,
   // GoogleSterkly = 3,
 //   WelcomwScreen = 5,
//    Wibiya = 6,
//    NoProblem = 7,
 //   MobileDevice = 8,
    Aducky = 9,
 //   PastaLeads = 10,
 //   NoProblemSlider = 11,
    SpeedOptimizer_Guru = 12,
    PicRec = 13,
    Conduit = 14,
    _50OnRedId = 15,
    Shopwithboost = 16
 //   Popup = 15,
 //   XSaver = 16,
 //   DonutleadsDomain = 17,
 //   DonutleadsPopup = 18,
//    PastaLeadsPopup = 19
}
public enum eDomainName
{
    noproblemppc = 1,
    pastaleads = 2,
    donutleads = 3,    
    bbqleads = 4,
    shopwithboost = 5,
    sushileads = 6
}
public enum eInterval
{
    HOUR = 0,
    DAY = 1,
    WEEK = 2,
    MONTH = 3,
    YEAR = 4
}
public enum eHomeImprovmentBudget
{
    _upTo5,
    _5To10,
    _10upTo25,
    _25To50,  
     _50To100,             
    _100To150,                 
    _150To250,                 
   _250To_500,                    
    _500To1000,                   
    _1000To2000,
    _2000More   
}
public static class CountryNames
{
    public const string UNITED_STATES = "UNITED STATES";
    public const string UNITED_KINGDOM = "UNITED KINGDOM";
    public const string CANADA = "CANADA";
    public const string ISRAEL = "ISRAEL";
    public const string IRELAND = "IRELAND";
    public const string SOUTH_AFRICA = "SOUTH AFRICA";
    public const string AUSTRALIA = "AUSTRALIA";
}
/*
public enum eAdvertiserRegistrationLandingPageType
{
    A,
    B
}
 * */
/*
public enum eCompletedRegistrationStep
{
    LandedOnJoinPage = 0,
    UserCreation = 1,
    EmailVerification = 2,
    ChoosePlan = 3,
    BusinessDetails = 4,
    Categories = 5,
    CoverArea = 6,
    Checkout = 7,
    PaymentRegistered = 8
}
*/
public class class_enum
{
	public class_enum()
	{
		//
		
		//
	}
    public static Type GetEnumType(string name)
    {
        
        Assembly assembly = typeof(WebReferenceReports.SupplierStatus).Assembly;
        Type _type = assembly.GetType(name);
        if (_type == null)
        {
            assembly = typeof(eChartType).Assembly;
            _type = assembly.GetType(name);
            if (_type == null)
            {                
                return null;
            }
        }
        if (!_type.IsEnum)
        {            
            return null;
        }
        return _type;
     //   WebReferenceSupplier.SupplierStatus.;
    //        WebReferenceReports.SupplierStatus.
    }
    public static object GetValue(Type EnumType, string s)
    {
        if (!EnumType.IsEnum)
            return null;
        foreach(object o in Enum.GetValues(EnumType))
        {
            if (o.ToString() == s)
                return o;
        }
        return null;
    }
   
   
}
