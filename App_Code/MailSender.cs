using System;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using System.Collections.Generic;


	public class MailSender {
		private string from = "";//=Utils.GetConfigValue("Mail.From");
        private string fromName = "";//=Utils.GetConfigValue("Mail.FromName");
        private string to = "";//=Utils.GetConfigValue("Mail.From");
        private string toName = "";//=Utils.GetConfigValue("Mail.FromName");
        private string bcc="";
		private string subject;
		private string body;
        private string replyTo = "";//=Utils.GetConfigValue("Mail.From");
		private bool isText;
        private string remoteHost = "";//=Utils.GetConfigValue("Mail.RemoteHost");
        private string loginUser = "";//=Utils.GetConfigValue("Mail.LoginUser"),loginPass=Utils.GetConfigValue("Mail.LoginPass");

		public string From { get { return from; } set { from=value; } }
		public string FromName { get { return fromName; } set { fromName=value; } }
		public string To { get { return to; } set { to=value; } }
		public string ToName { get { return toName; } set { toName=value; } }
        public string Bcc { get { return bcc; } set { bcc = value; } }
		public string Subject { get { return subject; } set { subject=value; } }
		public string Body { get { return body; } set { body=value; } }
		public string ReplyTo { get { return replyTo; } set { replyTo=value; } }
		public bool IsText { get { return isText; } set { isText=value; } }
		public string RemoteHost { get { return remoteHost; } set { remoteHost=value; } }

		public MailSender() { }

		#region Constructors
		public MailSender(
			string to,
			string toName,
			string subject,
			string body
		) {
			To=to;
			ToName=toName;
			Subject=subject;
			Body=body;
		}
		public MailSender(
			string to,
			string toName,
			string subject,
			string body,
			string from,
			string fromName
		):this(to,toName,subject,body) {
			From=from;
			FromName=fromName;
		}
		public MailSender(
			string to,
			string toName,
			string subject,
			string body,
			string from,
			string fromName,
			string replyTo
		):this(to,toName,subject,body,from,fromName) {
			ReplyTo=replyTo;
		}
		public MailSender(
			string to,
			string toName,
			string subject,
			string body,
			string from,
			string fromName,
			string replyTo,
			bool isText
		):this(to,toName,subject,body,from,fromName,replyTo) {
			IsText=isText;
		}
		#endregion

		public bool Send() {
			MailMessage msg=new MailMessage();
			string[] toArr=To.Split(';');            
			

			foreach (string toItem in toArr)
			{
				if (!String.IsNullOrEmpty(toItem))
				{
					if (String.IsNullOrEmpty(ToName)) { ToName = toItem; }
					msg.To.Add(new MailAddress(toItem, toItem));
				}
			}

            string[] bccArr = Bcc.Split(';');

            if (bccArr.Length > 0)
            {
                foreach (string bccItem in bccArr)
                {
                    if (!String.IsNullOrEmpty(bccItem))
                    {
                        msg.Bcc.Add(new MailAddress(bccItem, bccItem));
                    }
                }
            }

			if (!String.IsNullOrEmpty(From)) msg.From=new MailAddress(From,FromName);
			if (!String.IsNullOrEmpty(ReplyTo)) msg.Headers["Reply-To"]=ReplyTo;
			msg.Subject=Subject;
			msg.IsBodyHtml=!IsText;
			msg.Body=body;

			SmtpClient client=new SmtpClient();

			//if (RemoteHost!=String.Empty) client.Host=RemoteHost;
			//if (loginUser!=String.Empty && loginPass!=String.Empty) client.Credentials=new NetworkCredential(loginUser,loginPass);
            /*
            client.Credentials = new System.Net.NetworkCredential("noproblemreports@gmail.com", "npr123456");
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
             * */

			try {
				client.Send(msg);
				return true;
			}
			catch (Exception ex) {                
				//throw new SiteException("Error while seding mail: ",ex);
		//		Logger.File(ex, "common");
                ////LogEdit.SaveLog("email_" , ex.Message);

				return false;
			}
		}
        public void ZapMoneyMail(SupplierPaymentData data)
        {
            this.From = "rona@noproblem.co.il";
            this.FromName = "NO PROBLEM billing";
            this.To = "rona@noproblem.co.il";
            this.Bcc = "shaip@onecall.co.il;shaim@onecall.co.il;yoave@noproblemppc.com";
            this.Subject = "Problem with transaction: " + data.PaymentId;
            this.IsText = true;
            this.body = GetAllPropertyObject(data);

        }
        public void CacheErrorMail()
        {
            this.From = "yoave@noproblemppc.com";
            this.FromName = "NO PROBLEM ERROR!!!";
            this.To = GetMailCacheErrorNotification();
          //  this.Bcc = "alain@noproblemppc.com";
            this.Subject = "Cache problem!!!";
            this.IsText = true;
           // this.body = GetAllPropertyObject(data);

        }
        string GetAllPropertyObject(object obj)
        {
            StringBuilder sb = new StringBuilder();
            foreach (PropertyInfo pi in obj.GetType().GetProperties())
            {
                object _value = pi.GetValue(obj, null);
                if (_value == null)
                    continue;
                sb.AppendLine(pi.Name + ": " + _value.ToString());
            }
            return sb.ToString();
        }
        string GetMailCacheErrorNotification()
        {
            string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
            XDocument xdoc = XDocument.Load(path);
            IEnumerable<XElement> elements = xdoc.Element("Sites").Element("SitePPC").Element("MailNotification").Elements("Mail");
            StringBuilder sb = new StringBuilder();
            foreach (XElement xelem in elements)
            {
                sb.Append(xelem.Value + ";");
            }
            if (sb.Length > 0)
                sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
	}

