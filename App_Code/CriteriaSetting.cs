﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for CriteriaSetting
/// </summary>
public class CriteriaSetting
{
    
    public CriteriaSetting()
	{
        
	}
    public static string GetSymbolCriteria(string criteria, string CurrencySymbol)
    {
        WebReferenceReports.Criteria car;
        foreach (WebReferenceReports.Criteria cri in Enum.GetValues(typeof(WebReferenceReports.Criteria)))
        {
            if (cri.ToString() == criteria)
                return GetSymbolCriteria(cri, CurrencySymbol);
        }
        return string.Empty;
    }
    public static string GetSymbolCriteria(WebReferenceReports.Criteria criteria, string CurrencySymbol)
    {
        switch (criteria)
        {
            case(WebReferenceReports.Criteria.ConversionRateDN):
            case(WebReferenceReports.Criteria.ConversionRateSR):
            case(WebReferenceReports.Criteria.PercentageOfRefunds):
            case(WebReferenceReports.Criteria.RerouteCallsUnansweredPercent):
            case (WebReferenceReports.Criteria.RerouteCallsReroutedPercent):
                return "%";                
            case(WebReferenceReports.Criteria.AmountOfCalls):
            case(WebReferenceReports.Criteria.AmountOfAdvertisers):
            case(WebReferenceReports.Criteria.AmountOfDeposits):
            case(WebReferenceReports.Criteria.SoldCallsPerRequets):
            case (WebReferenceReports.Criteria.AmountOfRefundedCalls):
            case (WebReferenceReports.Criteria.OutOfMoneyCount):
            case (WebReferenceReports.Criteria.AmountOfRecurringDeposits):
            case (WebReferenceReports.Criteria.AmountOfRequests):
            case(WebReferenceReports.Criteria.AvailableAdvertisersCount):
            case(WebReferenceReports.Criteria.CustomerServiceUnavailableCount):
            case(WebReferenceReports.Criteria.TemporaryUnavailableCount):
            case(WebReferenceReports.Criteria.CandidatesCount):

            case (WebReferenceReports.Criteria.RerouteCallsReroutedCount):
            case (WebReferenceReports.Criteria.RerouteCallsUnansweredCount):
            case (WebReferenceReports.Criteria.RerouteCallsCount):
                return string.Empty;
            default:
                return CurrencySymbol;
        }
        
    }
}
