using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for SecuritControls
/// </summary>
public class SecuritControls : ScannerControls
{
    PageSetting page;
    Dictionary<string, string> _dic;
	public SecuritControls(PageSetting page, Dictionary<string, string> dic):base(page)
	{
        this.page = page;
        this._dic = dic;
	}
    //security
    //Set Link enabled true or false//
    protected void findLinks(Control control)
    {
        if (string.IsNullOrEmpty(control.ID))
            return;
        if (_dic.ContainsKey(control.ID) && control.GetType() == typeof(HtmlAnchor))
        {
            ((HtmlAnchor)control).HRef = _dic[control.ID];
            return;
        }
        if (control.GetType() == typeof(HyperLink))
        {
            HyperLink hl = (HyperLink)control;
            foreach (KeyValuePair<string, bool> kvp in page.PageSecurity)
            {
                if (kvp.Key == hl.NavigateUrl || kvp.Key == hl.ID)
                {
                    hl.Enabled = kvp.Value;
                    break;
                }
            }
        }
        else if (control.GetType() == typeof(HtmlAnchor))
        {
            HtmlAnchor ha = (HtmlAnchor)control;
            foreach (KeyValuePair<string, bool> kvp in page.PageSecurity)
            {
                if (kvp.Key == ha.HRef || kvp.Key == ha.ID)
                {
                    if (!kvp.Value)
                        ha.Attributes.Remove("href");
                    break;
                }
            }
        }
        else if (control.GetType() == typeof(LinkButton))
        {
            LinkButton lb = (LinkButton)control;
            foreach (KeyValuePair<string, bool> kvp in page.PageSecurity)
            {
                if (kvp.Key == lb.ID)
                {
                    lb.Enabled = kvp.Value;
                    break;
                }
            }
        }

    }
    public void SetLinks()
    {
        if (page.PageSecurity == null || page.PageSecurity.Count == 0)
            return;
        myFunc func = new myFunc(findLinks);
        ApplyToControls(page, func);
    }
    ////
}
