﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for DesktopAppWebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class DesktopAppWebService : System.Web.Services.WebService {

    public DesktopAppWebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string RegisterConsumer(string phone) {

        WebReferenceDeskapp.ConsumerSignUpDesktopRequest _request = new WebReferenceDeskapp.ConsumerSignUpDesktopRequest();
        _request.phone = Utilities.GetCleanPhone(phone);
        JavaScriptSerializer jss = new JavaScriptSerializer();
        if (Context != null && Context.Request != null)
        {
            _request.IP = Utilities.GetIP(Context.Request);
            _request.Browser = Context.Request.Browser.Browser;
  //          _request.Country = SalesUtility.GetCountryName(_request.IP, Context.Request.Url.Authority);
            _request.Country = SalesUtility.GetCountryName(_request.IP, Utilities.GetHostName(Context.Request));
        }
        WebReferenceDeskapp.Deskapp _customer = WebServiceConfig.GetDeskappServicesReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceDeskapp.ResultOfConsumerSignUpDesktopResponse _response = null;
        try
        {
            _response = _customer.ConsumerSignUpDesktop(_request);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return jss.Serialize(new { status = "faild", message = "ServerFaild" });
        }
        if (_response.Type == WebReferenceDeskapp.eResultType.Failure)
            return jss.Serialize(new { status = "faild", message = "ServerFaild" });
        if (_response.Value.Status == WebReferenceDeskapp.eConsumerSignUpDesktopStatus.OK)
            return jss.Serialize(new { status = "OK", phone = _request.phone });
        if (_response.Value.Status == WebReferenceDeskapp.eConsumerSignUpDesktopStatus.PhoneValidationFaild)
            return jss.Serialize(new { status = "faild", message = "PhoneValidation" });
        return jss.Serialize(new { status = "faild", message = "GeneralProblem" });

    }
    [WebMethod]
    public string RegisterConsumerPinCode(string phone, string PinCode)
    {
        WebReferenceDeskapp.ConsumerRegisterPincodeRequest _request = new WebReferenceDeskapp.ConsumerRegisterPincodeRequest();
        _request.phone = Utilities.GetCleanPhone(phone);
        _request.PinCode = PinCode;
        WebReferenceDeskapp.Deskapp _customer = WebServiceConfig.GetDeskappServicesReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceDeskapp.ResultOfGuid _result = null;
        JavaScriptSerializer jss = new JavaScriptSerializer();
        try
        {
            _result = _customer.ConsumerRegisterPincode(_request);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return jss.Serialize(new { status = "Faild" });
        }
        if (_result.Type == WebReferenceDeskapp.eResultType.Failure)
            return jss.Serialize(new { status = "Faild" });
        if(_result.Value == Guid.Empty)
            return jss.Serialize(new { status = "Faild" });
        return jss.Serialize(new { status = "OK", account = _result.Value.ToString() });
    }
    [WebMethod]
    public string CallRecord(Guid ContactId, string ToPhoneNumber, int TimezoneOffset, bool BlockCallerId = false)
    {        
        JavaScriptSerializer jss = new JavaScriptSerializer();
        ToPhoneNumber = Utilities.GetCleanPhone(ToPhoneNumber);
        if(!SalesUtility.CheckUS_PhoneValidation(ToPhoneNumber))
            return jss.Serialize(new { status = "PhoneNotValid" });
        WebReferenceDeskapp.Deskapp _Deskapp = WebServiceConfig.GetDeskappServicesReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceDeskapp.CallRecordRequest request = new WebReferenceDeskapp.CallRecordRequest();
        request.BlockCallerId = BlockCallerId;
        request.BobPhone = ToPhoneNumber;
        request.ContactId = ContactId;
        request.TimezoneOffset = TimezoneOffset;
        WebReferenceDeskapp.ResultOfCallRecordResponse result = null;
        try
        {
            result = _Deskapp.RecordCall(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return jss.Serialize(new { status = "ServerFaild" });
        }
        if (result.Type == WebReferenceDeskapp.eResultType.Failure)
            return jss.Serialize(new { status = "ServerFaild" });
        return jss.Serialize(new { status = result.Value.ResponseStatus.ToString(), callid = result.Value.CallId.ToString() });
    }
    [WebMethod]
    public string GetSecondRecordLeft(Guid ContactId)
    {
        WebReferenceDeskapp.Deskapp _Deskapp = WebServiceConfig.GetDeskappServicesReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceDeskapp.ResultOfInt32 result = null;
        JavaScriptSerializer jss = new JavaScriptSerializer();
        try
        {
            result = _Deskapp.GetSecondRecordLeft(ContactId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return jss.Serialize(new { status = "ServerFaild" });
        }
        if (result.Type == WebReferenceDeskapp.eResultType.Failure)
            return jss.Serialize(new { status = "ServerFaild" });
        return jss.Serialize(new { status = "Success", duration = result.Value.ToString() });
    }
    [WebMethod]
    public string getDeskAppHeadingTips(string incidentId)    {
        WebReferenceCustomer.ResultOfListOfTipBasicData _response = null;
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string status;

        try
        { 
            _response = customer.GetTipsForIncident(new Guid(incidentId));

            if (_response.Type == WebReferenceCustomer.eResultType.Success)
                status = "success";
            else
                status = "failure";            
        } 
        catch(Exception exc)
        {
            status = "failure";   
            dbug_log.ExceptionLog(exc);            
        }

        return jss.Serialize(new { status = status, value = _response.Value });
    }

    [WebMethod]
    public string startDesktopAppCase(string incidentId)
    {
        WebReferenceCustomer.Result _response = null;
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);

        string status;

        try
        { 
            _response = customer.StartDesktopAppCase(new Guid(incidentId));

            if (_response.Type == WebReferenceCustomer.eResultType.Success)
                status = "success";
            else
                status = "failure";
        }
        catch(Exception exc)
        {
            status = "failure";
            dbug_log.ExceptionLog(exc);
        }     

        JavaScriptSerializer jss = new JavaScriptSerializer();
        return jss.Serialize(new { status = status });
    }

    [WebMethod]
    public string getCaseDataForDesktopApp(string incidentId)
    {
        
        WebReferenceCustomer.ResultOfCaseAllDataContainer _response = null;
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);        

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string status;

        try
        {
            _response = customer.GetCaseDataForDesktopApp(new Guid(incidentId));

            if (_response.Type == WebReferenceCustomer.eResultType.Success)
                status = "success";
            else
                status = "failure";
        }
        catch (Exception exc)
        {
            status = "failure";
            dbug_log.ExceptionLog(exc);
        }

        return jss.Serialize(new { status = status, value = _response.Value });
        
    }


    [WebMethod]
    public string continueDesktopAppCase(string incidentId, bool wantsToContinue)
    {
        WebReferenceCustomer.Result _response = null;
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string status;

        try
        {
            _response = customer.ContinueDesktopAppCase(new Guid(incidentId), wantsToContinue);

            if (_response.Type == WebReferenceCustomer.eResultType.Success)
                status = "success";
            else
                status = "failure";
        }
        catch(Exception exc)
        {
            status = "failure";
            dbug_log.ExceptionLog(exc);
        }

        return jss.Serialize(new { status = status });
    }

    
    [WebMethod]
    public string setCustomerTipFeedback(string incidentId, string tipId, string supplierId, bool isPositiveFeedBack)
    {
        WebReferenceCustomer.Result _response = null;
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string status;

        WebReferenceCustomer.TipFeedback feedback=new WebReferenceCustomer.TipFeedback();
        feedback.IncidentId=new Guid(incidentId);
        feedback.TipId=new Guid(tipId);
        feedback.SupplierId=new Guid(supplierId);
        feedback.IsPositiveFeedback=isPositiveFeedBack;

        try
        {
            _response=customer.SetCustomerTipFeedback(feedback);

            if (_response.Type == WebReferenceCustomer.eResultType.Success)
                status = "success";
            else
            {
                dbug_log.ExceptionLog(new Exception("setCustomerTipFeedback failed: incidentId: " + incidentId + " tipId:" + tipId + " supplierId:" + supplierId + " message:" + _response.Messages[0]));
                status = "failure";
            }
                
        }

        catch(Exception exc)
        {
            status = "failure";
            dbug_log.ExceptionLog(exc);
        }
        
        return jss.Serialize(new { status = status });
    }
    
    
    [WebMethod]
    public string sendEmailToCustomerAfterCase(string incidentId, string email)
    {
        WebReferenceCustomer.Result _response = null;
        WebReferenceCustomer.SendEmailToCustomerAfterCaseRequest sendEmailToCustomerAfterCaseRequest = new WebReferenceCustomer.SendEmailToCustomerAfterCaseRequest();
        sendEmailToCustomerAfterCaseRequest.Email = email;
        sendEmailToCustomerAfterCaseRequest.IncidentId = new Guid(incidentId);
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string status;

        try
        {

            _response = customer.SendEmailToCustomerAfterCase(sendEmailToCustomerAfterCaseRequest);

            if (_response.Type == WebReferenceCustomer.eResultType.Success)
                status = "success";
            else
            {
                dbug_log.ExceptionLog(new Exception("sendEmailToCustomerAfterCase failed: incidentId: " + incidentId + " email:" + email + " message:" + _response.Messages[0]));
                status = "failure";
            }
                
        }

        catch (Exception exc)
        {
            status = "failure";
            dbug_log.ExceptionLog(exc);
        }

        return jss.Serialize(new { status = status });
    }

    [WebMethod]
    public string sendEmailAfterCallAndRecordDesktopApp(string callId, string email)
    {
        WebReferenceCustomer.ResultOfBoolean _response = null;
        
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string status;

        try
        {

            _response = customer.SendEmailAfterCallAndRecordDesktopApp(new Guid(callId),email);

            if (_response.Type == WebReferenceCustomer.eResultType.Success)
            {
                if(_response.Value)
                    status = "success";
                else
                    status = "failure";
            }
                
            else
            {
                dbug_log.ExceptionLog(new Exception("sendEmailAfterCallAndRecordDesktopApp failed: incidentId: " + callId + " email:" + email + " message:" + _response.Messages[0]));
                status = "failure";
            }

        }

        catch (Exception exc)
        {
            status = "failure";
            dbug_log.ExceptionLog(exc);
        }

        return jss.Serialize(new { status = status });
    }

    /*
     public Result<bool> SendEmailAfterCallAndRecordDesktopApp(Guid callId, string email)
    */

    [WebMethod]
    public string cancelRecordForSupplier(string incidentId,string supplierId)
    {

        //public Result CustomerDisallowesRecording(Guid incidentId, Guid supplierId)

        WebReferenceCustomer.Result _response = null;      
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);

        JavaScriptSerializer jss = new JavaScriptSerializer();
        string status;

        try
        {
            _response = customer.CustomerDisallowesRecording(new Guid(incidentId), new Guid(supplierId));

            
            if (_response.Type == WebReferenceCustomer.eResultType.Success)
                status = "success";
            else
            {
                dbug_log.ExceptionLog(new Exception("cancelRecordFroSupplier failed: incidentId: " + incidentId + " supplierId:" + supplierId));
                status = "failure";
            }
                
        }

        catch (Exception exc)
        {
            status = "failure";
            dbug_log.ExceptionLog(exc);
        }
        
        return jss.Serialize(new { status = status });
    }


    
/*
    [WebMethod]
public Result SendEmailToCustomerAfterCase(SendEmailToCustomerAfterCaseRequest request)
*/

public class SendEmailToCustomerAfterCaseRequest
    {
        public string Email { get; set; }
        public Guid IncidentId { get; set; }
    }






  public class TipFeedback
    {
        public Guid TipId { get; set; }
        public Guid SupplierId { get; set; }
        public Guid IncidentId { get; set; }
        public bool IsPositiveFeedback { get; set; }
    }

}
