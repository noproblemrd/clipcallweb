﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Web.Caching;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;
using Microsoft.Web.Administration;
using System.Net;
using System.Threading.Tasks;

/// <summary>
/// Summary description for Heading
/// </summary>
[Serializable()]
public class PpcSite
{
    #region Static-const properties
    static object _lock = new object();
    static object _lockLoad = new object();
    static volatile bool IsLoading = false;
    //static volatile bool IsReloadCacheLoading = false;
    static WaitHandle waitHandles = new AutoResetEvent(false);
    public static string STANDARD_TIME_NAME_UK = "GMT Standard Time";
    

    private const string PLACE_HOLDER = "Describe what you need...";
    private const string SLIDER_CLOSED_CONFIGURATION = "SLIDER_CLOSED_BUTTON_COOCKIE_EXPIRE_DURATION";
    private const string SLIDER_MINIMIZE_CONFIGURATION = "SLIDER_MINIMIZE_BUTTON_COOCKIE_EXPIRE_DURATION";
    public const string SITE_ID_SALES = "1";
    const string CRM_FAILD = "CRM faild";
    readonly string DESTINATION = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\_logic.js";

    public const string UNITED_STATES = "UNITED STATES";
    public const string UNITED_KINGDOM = "UNITED KINGDOM";
    public const string CANADA = "CANADA";
    public const string ISRAEL = "ISRAEL";

    static System.Security.Principal.WindowsIdentity _identity;
    public static System.Security.Principal.WindowsImpersonationContext GetWindowsImpersonationContext
    {
        get
        {
            return _identity.Impersonate();
        }
    }

    static PpcSite()
    {
        _identity = System.Security.Principal.WindowsIdentity.GetCurrent();
    }
    #endregion
    #region properties
    CacheLog cl;    
    public bool IsLocalMachine { get; set; }
    public int Port { get; set; }
    public Dictionary<string, HeadingCode> headings { get; set; }
    public string SiteId { get; set; }
    public string UrlWebReference { get; private set; }
    public Guid GeneralOrigin { get; set; }
    public Dictionary<eFlavour, SliderViewData> Flavors { get; set; }
    public Dictionary<string, eFlavour> IntextFlavor { get; private set; }
  /*  public Dictionary<string, Guid> Keywords { get; private set; }*/
  //  public List<HeadingGroup_Heading> heading_group { get; private set; }
    public Dictionary<string, eHeadingGroup> heading_group { get; set; }   
    public List<HeadingTips> Tips { get; set; }
   
    public List<KeywordsHeading> KeywordsList { get; set; }
    public Dictionary<string, string> keywords { get; set; }
    public Dictionary<Guid, string> Categories { get; set; }
    public List<State> states { get; set; }
    public List<State> popularStates { get; set; }
    public List<City> cities { get; set; }
 //   public List<KeywordsHeading> QueryStringKeywords { get; private set; }
    public string Logic { get; set; }
    public string GoogleLogic { get; set; }
  //  public string ConduitLogic { get; set; }
    public string InjJs { get; set; }
    public string intext { get; set; }
    public string PopupProductScript { get; set; }
    public string DataPopupProductScript { get; set; }
    public string InjMonScript { get; set; }
    public string MonHtmScript { get; set; }
    public string MonHtmlEventJs { get; set; }
    public DateTime DateModified { get; set; }
    public Guid ConduitToolbarOriginId { get; set; }
    public Guid UnknoenOriginId { get; set; }
    public Guid RevizerOriginId { get; set; }
    public Guid DentistAdvertiser { get; set; }
    public Guid RevizerPopup { get; set; }
    public Guid RoundSky { get; set; }
    public Guid SterlyGoogleOriginId { get; set; }
    public Guid StartAppOriginId { get; set; }
    public Guid DealPLYOriginId { get; set; }
    public Guid CrossriderId { get; set; }
    public Guid CrossriderTestId { get; set; }
    public Guid AduckyId { get; set; }
    public Guid ReviMedia { get; private set; }
    public Guid _50OnRedId { get; private set; }
    public Guid SpeedOptimizer_Guru { get; private set; }
    public Guid PicRecId { get; private set; }
    public Guid PerionId { get; private set; }
    public Guid ShopwithboostId { get; private set; }
    public string PastaLeadsDomain{ get; set; }
    public string DonutleadsDomain { get; set; }
    public string MainDomain { get; set; }
    
    public string ZapSiteId { get; set; }
    public string UserZap { get; set; }
    public string ZapService { get; set; }
    public string GoogleAddonDomain { get; set; }
    public Dictionary<Guid, List<string>> BlackDomainByOrigin { get; set; }
    public Dictionary<string, HeadingDidPhone> PhoneDid { get; set; }
    //public List<string> BadDomain { get; private set; }
    Dictionary<string, List<string>> CategoryDisplay { get; set; }
    public List<Guid> BlackOrigin { get; set; }
    public List<Guid> AllOrigins { get; set; }
    public List<Guid> DesktopOrigins { get; set; }
    public Dictionary<Guid, string> OriginBroughtBy { get; set; }
    public List<Guid> NoProblemSliderOrigins { get; set; }
    private List<string> _BlackKeywordDomain;
    public Dictionary<Guid, List<string>> MonetizationOriginInjection { get; set; }
   // public bool IsYoav { set; get; }
  //  public string DefaultProductName { get; private set; }
    public string PhoneReg { get; private set; }
    public string ZipcodeReg { get; private set; }
    public string PhoneRegularExpression_UK { get; private set; }
    public string PhoneRegularExpression_USA_and_UK { get; private set; }
    public string ZipcodeRegularExpression_UK { get; private set; }
    public List<AdEngineCampaignData> AdEnginePopupCampaign { get; set; }
    public List<Guid> IntextBlackOrigins { get; set; }
    public string pixJs { get; set; }

    public string StripeBaseUrl { get; set; }

#region ClipCall property
    /*
    public string ClipCallDomain { get; set; }
     *  */
    public Guid InviteDefaultAccountId { get; set; }
    
    public string ClipCallWebInvite { get; set; }
    public string ClipCallWebInvite2 { get; set; }
#endregion


    public List<string> BlackKeywordDomain {
        get
        {
            /*
            if (IsLoading)
            {
                lock (_lock)
                {
                }
            }
             * */
            return _BlackKeywordDomain;
        }
        private set { _BlackKeywordDomain = value; } 
    }
    public Dictionary<Guid, List<string>> BlackHeadingByOrigin { get; set; }
    public List<Guid> DistributionList { get; set; }
    int _SliderCloseButtonExpire { get; set; }
    int _SliderMinimizeButtonExpire { get; set; }
    public string SliderCloseButtonExpire
    {
        get
        {
            return (_SliderCloseButtonExpire > -1) ? _SliderCloseButtonExpire.ToString() : "null";
        }
    }
    public string SliderMinimizeButtonExpire
    {
        get
        {
            return (_SliderMinimizeButtonExpire > -1) ? _SliderMinimizeButtonExpire.ToString() : "null";
        }
    }
    #endregion
    #region Constractor
    public PpcSite()
    {
        cl = new CacheLog();
    }
    public static PpcSite _PpcSite()//(string SiteId, Guid GeneralOrigin)
    {
    //    Stopwatch sw_general = new Stopwatch();
    //    sw_general.Start();
        
        PpcSite _ppc = new PpcSite();
        
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc;
        try
        {
            xdoc = XDocument.Load(path);
        }
        catch (Exception exc)
        {
            _ppc.cl.WriteCacheLog(exc, new string[] { _ppc.UrlWebReference }, true);
            _ppc = LoadCacheFromFile();
            return _ppc;
        }
        string SiteId = xdoc.Element("Sites").Element("SitePPC").Element("SiteId").Value;
        Guid GeneralOrigin = new Guid(xdoc.Element("Sites").Element("SitePPC").Element("GeneralOrigin").Value);
        lock (_lockLoad)
        {
            
            try
            {
            //    throw new Exception();
                _ppc.GeneralOrigin = GeneralOrigin;
                if (!DBConnection.IsSiteExsist(SiteId))
                {
                    PpcCacheException pce = new PpcCacheException("Faild to Load siteId");
                    pce.NewValues = new Dictionary<string, string>();
                    pce.NewValues.Add("SiteId", SiteId);
                    CacheLog.WriteException(pce);
                    _ppc = LoadCacheFromFile();
                    return _ppc;
                }
            //    _ppc.IsYoav = Environment.MachineName == "YOAVNEW";
                _ppc.SiteId = SiteId;
                _ppc.LoadUrlWebReference();
                _ppc.LoadGeneralOrigins();
 //               _ppc.LoadZapData();                
                _ppc.LoadHeadings();
                _ppc.LoadCategories();
 //               _ppc.LoadHeadingCampains();
//                _ppc.LoadBroughtByNames();
                _ppc.LoadCategoryHeadingGroup();
                _ppc.LoadKeywords();
 //               _ppc.LoadSliderViewData();
 //               _ppc.LoadIntextFlavors();
 //               _ppc.LoadKeywordDomainBlackList();
                _ppc.LoadTips();
                _ppc.LoadRegularExpressions();
                _ppc.LoadStripeBaseUrl();

//                _ppc.LoadSliderButtonsDurationExpires();
//                _ppc.LoadAllOriginList();
//                _ppc.LoadAdEngineCampaign();
//                _ppc.LoadIntextBlackOrigins();
//                _ppc.LoadDesktopOrigins();

//                _ppc.LoadBlackHeadingDomain_Origin();
//                _ppc.LoadNoProblemSliderOrigins();
//                _ppc.LoadDistributionOrigins();
//                _ppc.loadSates();
//                _ppc.loadPopularCities();
//                _ppc.LoadDisplayCategories();
//                _ppc.DidPhoneJobUpdate();
//                _ppc.LoadBlackDomain();
//                _ppc.LoadMonetizationOriginInjection();
//                _ppc.LoadLogicJs();
//                _ppc.LoadPixJs();

//                _ppc.LoadIviteMobileUser();
            }
            catch (Exception exc)
            {
                
                _ppc.cl.WriteCacheLog(exc, new string[] { _ppc.UrlWebReference }, true);
                _ppc = LoadCacheFromFile();
                return _ppc;
            }
            _ppc.SaveCache();
            _ppc.cl.UpdateSuccess();
        }
        return _ppc;
     
    }

   

    

    
    #endregion   
    #region Load functions
    private void LoadStripeBaseUrl()
    {
        StripeBaseUrl = System.Configuration.ConfigurationManager.AppSettings["StripeBaseUrl"];
    }
    private void LoadCategories()
    {
        Dictionary<Guid, string> dic = Utilities.GetCategories(this.UrlWebReference, this.SiteId);
        var myList = dic.ToList();

        myList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
        this.Categories = myList.ToDictionary(x => x.Key, x => x.Value);
    }
    private void LoadIviteMobileUser()
    {        
        InviteBuilder ib = new InviteBuilder();
        this.InviteDefaultAccountId = ib.SetInviteJs(this);
        this.ClipCallWebInvite = ib.inviteJS;
        this.ClipCallWebInvite2 = ib.invite2JS;        
    }
    private void LoadPixJs()
    {
        PixelJsBuilder pjb = new PixelJsBuilder();
        this.pixJs = pjb.GetPixelJs();
    }
    public List<Guid> GetListOriginNonDistribution()
    {
        List<Guid> ListOriginNonDistribution = new List<Guid>();
        using (SqlConnection connSales = DBConnection.GetSalesConnString())
        {
            string commandSales = @"SELECT o.New_originId
 FROM dbo.New_originBase ob
	INNER JOIN dbo.New_originExtensionBase o on o.New_originId = ob.New_originId
WHERE ob.deletionstatecode = 0
  and ISNULL(o.new_isdistribution, 0) = 0";
            using (SqlCommand cmdSales = new SqlCommand(commandSales, connSales))
            {
                connSales.Open();
                SqlDataReader readerSales = cmdSales.ExecuteReader();
                while (readerSales.Read())
                    ListOriginNonDistribution.Add((Guid)readerSales["New_originId"]);
                connSales.Close();
            }
        }
        return ListOriginNonDistribution;
    }
    
    private void LoadMonetizationOriginInjection()
    {       
        List<Guid> listNotIncludeOrigins = GetPartnerBlackInjections();
        MonetizationOriginInjection = new Dictionary<Guid, List<string>>();
        
        List<Guid> ListOriginNonDistribution = GetListOriginNonDistribution();
        DateTime dt;
        using(SqlConnection conn = DBConnection.GetConnString())
        {
            string command = @"SELECT MAX([Date]) FROM [dbo].[Exposure]";
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                dt = (DateTime)cmd.ExecuteScalar();
                cmd.Dispose();
            }
            command = @"SELECT c.IP2LocationName, SUM(e.Visitor) DAUs
FROM dbo.Exposure e
	INNER JOIN dbo.Country c on e.CountryId = c.Id
WHERE e.[Date] = @date
	AND e.OriginId = @OriginId
GROUP BY c.IP2LocationName
ORDER BY SUM(e.Visitor) desc";
            foreach(Guid OriginId in ListOriginNonDistribution)
            {
                if (listNotIncludeOrigins.Contains(OriginId))
                    continue;
                List<string> ListCountries = new List<string>();
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@date", dt);
                    cmd.Parameters.AddWithValue("@OriginId", OriginId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    int i = 0;
                    while(reader.Read())
                    {
                        /*// remove 12-05-2015
                        i++;
                        if (i < 4)
                            continue;
                        
                        
                        int DAUs = (int)reader["DAUs"];
                        if (DAUs > 10000)
                            continue;
                         * */
                       
                        string _country = (string)reader["IP2LocationName"];
                        /*remove 12-05-2015
                       if (_country == "ISRAEL")
                           continue;
                        */
                        ListCountries.Add(_country);
                    }
                    reader.Dispose();
                    cmd.Dispose();
                }
                if (ListCountries.Count > 0)
                    MonetizationOriginInjection.Add(OriginId, ListCountries);
            }
            conn.Close();
        }
       

        OriginMonetizationInjBuilder InjBuilder = new OriginMonetizationInjBuilder(this);
        this.InjMonScript = InjBuilder.GetInjMonJs();
        this.MonHtmScript = InjBuilder.GetMonHtmJs();
        this.MonHtmlEventJs = InjBuilder.GetMonHtmlEventJs();

    }
    private void LoadDesktopOrigins()
    {
        this.DesktopOrigins = new List<Guid>();
        this.DesktopOrigins.Add(new Guid("AE9E553F-4FC2-E411-840C-001517D1792A"));
        this.DesktopOrigins.Sort();
    }
    private void LoadIntextBlackOrigins()
    {
        this.IntextBlackOrigins = new List<Guid>();
        this.IntextBlackOrigins.Add(RevizerOriginId);
        this.IntextBlackOrigins.Add(new Guid("02A9C962-B572-E311-B7DA-001517D1792A")); //Verti production)
        this.IntextBlackOrigins.Add(new Guid("5FB64BDB-AA6D-E411-B45D-001517D1792A")); //Verti-Shopwithboost
        this.IntextBlackOrigins.Add(new Guid("F0C8EE84-B42E-E411-87EC-001517D1792A"));// VI_P_1.2.1.1_N_280814 
        this.IntextBlackOrigins.Add(new Guid("ED792582-BBAA-E411-840C-001517D1792A"));// webpic_js
        this.IntextBlackOrigins.Add(new Guid("FDC91A32-AC5A-E411-B45D-001517D1792A"));//Admedia
        this.IntextBlackOrigins.Add(new Guid("4DB88F93-A1B6-E411-840C-001517D1792A"));//speedbit_js_slider_only
        this.IntextBlackOrigins.Add(new Guid("85790A9D-1885-4A9E-A27F-2272B258AFBC"));//IS_D_PL_25215
        // this.IntextBlackOrigins.Add(new Guid("E8A4A23A-B034-E211-A9A0-001517D10F6E"));//CrossriderE8A4A23A-B034-E211-A9A0-001517D10F6E
        this.IntextBlackOrigins.Sort();
    }

    private void LoadAdEngineCampaign()
    {
        this.AdEnginePopupCampaign = new List<AdEngineCampaignData>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfListOfAdEngineCampaignData result = null;
        try
        {
            result = _site.GetAllAdEngineCampaigns();
        }
        catch(Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("site.GetAllOrigins " + CRM_FAILD);
            return;
        }
        foreach(WebReferenceSite.AdEngineCampaignData row in result.Value)
            AdEnginePopupCampaign.Add(new AdEngineCampaignData(row.Id, row.URL, row.Name, row.Width, row.Height));
    }
    private void LoadAllOriginList()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetAllOrigins(true);// GetUserTimeZone(userManagement.GetGuid);
        }
        catch (Exception ex)
        {
            cl.WriteCacheLog(ex);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("site.GetAllOrigins " + CRM_FAILD);
            return;
        }
        AllOrigins = new List<Guid>();
        foreach(WebReferenceSite.GuidStringPair gsp in result.Value)
            AllOrigins.Add(gsp.Id);
        AllOrigins.Sort();
    }
    private void LoadSliderButtonsDurationExpires()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        string result = string.Empty;
        try
        {
            result = _site.GetConfigurationSettings();// GetUserTimeZone(userManagement.GetGuid);
        }
        catch (Exception ex)
        {
            cl.WriteCacheLog(ex);
            return;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("Configurations") == null || xdd.Element("Configurations").Element("Error") != null)
        {
            string mess = "GetConfigurationSettings " + CRM_FAILD;
            cl.WriteCacheLog(mess);
            return;
        }
        string _value = (from p in xdd.Element("Configurations").Elements()
                         where p.Element("Key").Value == SLIDER_CLOSED_CONFIGURATION
                         select p).FirstOrDefault().Element("Value").Value;
        int temp;
        this._SliderCloseButtonExpire = (int.TryParse(_value, out temp)) ? temp : 0;

        _value = (from p in xdd.Element("Configurations").Elements()
                         where p.Element("Key").Value == SLIDER_MINIMIZE_CONFIGURATION
                         select p).FirstOrDefault().Element("Value").Value;
        this._SliderMinimizeButtonExpire = (int.TryParse(_value, out temp)) ? temp : 0;

    }

    private void LoadNoProblemSliderOrigins()
    {
       // throw new NotImplementedException();
        NoProblemSliderOrigins = new List<Guid>();
        WebReferenceSite.Site site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfListOfGuid result = null;
        try
        {
            result = site.GetNoProblemSliderOrigins();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetNoProblemSliderOrigins " + CRM_FAILD);
            return;
        }
        foreach (Guid id in result.Value)
        {
            NoProblemSliderOrigins.Add(id);
        }
        NoProblemSliderOrigins.Sort();
    }
    private void LoadDistributionOrigins()
    {
        DistributionList = new List<Guid>();
        WebReferenceSite.Site site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfListOfGuid result = null;
        try
        {
            result = site.GetDistributionOrigins();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetDistributionOrigins " + CRM_FAILD);
            return;
        }
        foreach (Guid id in result.Value)
        {
            DistributionList.Add(id);
        }
        DistributionList.Sort();
    }
    private void LoadBroughtByNames()
    {
        OriginBroughtBy = new Dictionary<Guid, string>();
        WebReferenceSite.Site site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;

        try
        {
            result = site.GetOriginBroughtByName();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetOriginBroughtByName " + CRM_FAILD);
            return;
        }
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            OriginBroughtBy.Add(gsp.Id, gsp.Name);
        }
    }
    private void LoadBlackDomain()
    {
        BlackOrigin = new List<Guid>();
        string command = "SELECT Id FROM dbo.BlackOrigin";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                BlackOrigin.Add((Guid)reader["Id"]);
            }
            conn.Close();
        }
        BlackOrigin.Sort();
    }
    private void LoadDisplayCategories()
    {
        CategoryDisplay = new Dictionary<string, List<string>>();
        WebReferenceSite.Site site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfHashSetOfCategoryGroupForDisplay result = null;

        try
        {
            result = site.GetCategoryGroupsForDisplay();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetCategoryGroupsForDisplay " + CRM_FAILD);
            return;
        }
        foreach (WebReferenceSite.CategoryGroupForDisplay cgfd in result.Value)
        {
            List<string> list = new List<string>();
            foreach (string s in cgfd.HeadingCodes)
                list.Add(s);
            CategoryDisplay.Add(cgfd.Name, list);
        }


    }
    private void LoadLogicJs()
    {
        JS_Files jsfiles = AddonTools.GetLogicJS(this);
        this.Logic = jsfiles.Addon;
        this.GoogleLogic = jsfiles.AddonGoogle;
        this.InjJs = InjectionBuilder.Build(this);
        
        IntexBuilder _ib = new IntexBuilder(this);
        this.intext = _ib.GetIntexJs(this);

        string data_script;
        PopupBuilder _pb = new PopupBuilder();
        this.PopupProductScript = _pb.GetPopupJs(this, out data_script);
        this.DataPopupProductScript = data_script;
        
        this.DateModified = DateTime.Now;
        string destination_poc = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\poc.js";

        bool IsSave = false;   
        /* Remove 15-02-2016
        do
        {
            IsSave = SalesUtility.SaveToFile(destination_poc, jsfiles.poc);
            if (!IsSave)
            {
                int num = new Random().Next(5) + 1;
                System.Threading.Thread.Sleep(num * 1000);
            }
        } while (!IsSave);
         */
    }
    private void LoadBlackHeadingDomain_Origin()
    {
        BlackHeadingByOrigin = new Dictionary<Guid, List<string>>();
        List<string> list = new List<string>("3002,3005,3004,3003,3006".Split(','));
      //  list.AddRange(GetAllHeadingOnlyCampainCodes());
        foreach (string s in GetAllHeadingOnlyCampainCodes())
        {
            if (!list.Contains(s))
                list.Add(s);
        }
        list.Sort();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this.UrlWebReference);
        WebReferenceSite.ResultOfListOfGuid result = null;
        try
        {
            result = _site.GetOriginsBlockCampaigns();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetOriginsBlockCampaigns " + CRM_FAILD);
            return;
        }
        foreach (Guid OriginId in result.Value)
        {
            BlackHeadingByOrigin.Add(OriginId, list);
        }
        /*
        BlackHeadingByOrigin.Add(ConduitToolbarOriginId, list);//Conduit
        BlackHeadingByOrigin.Add(new Guid("F0AB6155-8751-E111-BE62-001517D10F6E"), list); //Widdit
        BlackHeadingByOrigin.Add(new Guid("A01BE54D-1287-E211-B20F-001517D10F6E"), list); //WidditPartner1
        BlackHeadingByOrigin.Add(new Guid("7C1C775F-D454-E211-9708-001517D10F6E"), list); //WidditWW        
        BlackHeadingByOrigin.Add(new Guid("FDF56E64-B034-E211-A9A0-001517D10F6E"), list); //Sterkly       
        BlackHeadingByOrigin.Add(new Guid("616679CE-FEDE-E211-B7F6-001517D1792A"), list); //Perion       
        BlackHeadingByOrigin.Add(new Guid("08E2FFBE-9894-E211-AAD1-001517D1792A"), list); //web-pick
        BlackHeadingByOrigin.Add(new Guid("25C7729B-B6E4-E211-8B81-001517D1792A"), list); //NCISOFT_Injection
        BlackHeadingByOrigin.Add(new Guid("B31D352D-E9C6-E211-B7F6-001517D1792A"), list); //AmonetizeInjection1 
        BlackHeadingByOrigin.Add(new Guid("0B6A9290-31F0-E111-AF7A-001517D1792A"), list); //softpublisher1
        BlackHeadingByOrigin.Add(new Guid("E165D603-B034-E211-A9A0-001517D10F6E"), list); //Softpublisher12 
        BlackHeadingByOrigin.Add(new Guid("340B1290-011E-E311-A28E-001517D10F6E"), list); //Revizer
        BlackHeadingByOrigin.Add(new Guid("8C524FE6-46F4-E211-BEB6-001517D10F6E"), list); //DealPlyInjection
        BlackHeadingByOrigin.Add(new Guid("8143692D-C40F-E311-A28E-001517D10F6E"), list); //Wajamu
        BlackHeadingByOrigin.Add(new Guid("E214549C-00F8-E111-AC13-001517D10F6E"), list); //Outbrowse
        BlackHeadingByOrigin.Add(new Guid("FA2A279C-AC3C-E311-B6DC-001517D1792A"), list); //Dollario
        BlackHeadingByOrigin.Add(new Guid("F28B3440-7836-E311-AD59-001517D1792A"), list); //InstallDaddy
        BlackHeadingByOrigin.Add(new Guid("B45FD2E5-4042-E311-B6DC-001517D1792A"), list); //keydownload
        BlackHeadingByOrigin.Add(new Guid("E8A4A23A-B034-E211-A9A0-001517D10F6E"), list);//Crossrider
        */
        
        BlackDomainByOrigin = new Dictionary<Guid, List<string>>();
        List<string> listDomain = new List<string>("austin.citysearch.com,free.avg.com,hoover.com,isearch.avg.com,oldnavy.gap.com,philadelphia.citysearch.com,search.avg.com,shop.advanceautoparts.com,sportsillustrated.cnn.com,store1.adobe.com,techguru.aol.com,usa.kaspersky.com".Split(','));
        listDomain.Sort();
        BlackDomainByOrigin.Add(ConduitToolbarOriginId, listDomain);

    }
    private void LoadKeywordDomainBlackList()
    {
        _BlackKeywordDomain = new List<string>();
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this.UrlWebReference);
        WebReferenceSite.ResultOfArrayOfArrayOfString result = null;
        try
        {
            result = _site.GetAllBlackList();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetAllBlackList " + CRM_FAILD);
            return;
        }        
        foreach (string[] ArrStr in result.Value)
        {
            string _keyword = ArrStr[0];
            string _domain = (ArrStr[1]).ToLower();
            _keyword = AddonTools.GetKeywordForSearch(_keyword);
            string KeyDom = _keyword +";" + _domain;
            if (!_BlackKeywordDomain.Contains(KeyDom))
                _BlackKeywordDomain.Add(KeyDom);
        }
        _BlackKeywordDomain.Sort();
         
    }
    private void LoadGeneralOrigins()
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        string origin = xdoc.Element("Sites").Element("SitePPC").Element("ConduitOriginId").Value;
        Guid _id;
        if (!Guid.TryParse(origin, out _id))
            _id = Guid.Empty;
        this.ConduitToolbarOriginId = _id;

        string UnknownOrigin = xdoc.Element("Sites").Element("SitePPC").Element("UnknownOriginId").Value;
        if (!Guid.TryParse(UnknownOrigin, out _id))
            _id = Guid.Empty;
        this.UnknoenOriginId = _id;

        string dentistAdvertiser = xdoc.Element("Sites").Element("SitePPC").Element("dentistAdvertiserId").Value;
        if (!Guid.TryParse(dentistAdvertiser, out _id))
            _id = Guid.Empty;
        this.DentistAdvertiser = _id;

        string roundSky = xdoc.Element("Sites").Element("SitePPC").Element("roundSkyId").Value;
        if (!Guid.TryParse(roundSky, out _id))
            _id = Guid.Empty;
        this.RoundSky = _id;

        this.GoogleAddonDomain = xdoc.Element("Sites").Element("SitePPC").Element("GoogleAddonDomain").Value.Trim();

        string RevizerOrigin = xdoc.Element("Sites").Element("SitePPC").Element("RevizerOriginId").Value;
        if (!Guid.TryParse(RevizerOrigin, out _id))
            _id = Guid.Empty;
        this.RevizerOriginId = _id;

        string SterklyG = xdoc.Element("Sites").Element("SitePPC").Element("SterklyG").Value;
        if (!Guid.TryParse(SterklyG, out _id))
            _id = Guid.Empty;
        this.SterlyGoogleOriginId = _id;

        string StartApp = xdoc.Element("Sites").Element("SitePPC").Element("StartAppOriginId").Value;
        if (!Guid.TryParse(StartApp, out _id))
            _id = Guid.Empty;
        this.StartAppOriginId = _id;

        string DealPly = xdoc.Element("Sites").Element("SitePPC").Element("DealPLYOriginId").Value;
        if (!Guid.TryParse(DealPly, out _id))
            _id = Guid.Empty;
        this.DealPLYOriginId = _id;

        string crossrider = xdoc.Element("Sites").Element("SitePPC").Element("CrossriderId").Value;
        if (!Guid.TryParse(crossrider, out _id))
            _id = Guid.Empty;
        this.CrossriderId = _id;

        string aducky = xdoc.Element("Sites").Element("SitePPC").Element("AduckyId").Value;
        if (!Guid.TryParse(aducky, out _id))
            _id = Guid.Empty;
        this.AduckyId = _id;

        string _revi_media = xdoc.Element("Sites").Element("SitePPC").Element("ReviMedia").Value;
        if (!Guid.TryParse(_revi_media, out _id))
            _id = Guid.Empty;
        this.ReviMedia = _id;

        string _50OnRed = xdoc.Element("Sites").Element("SitePPC").Element("_50onRedId").Value;
        if (!Guid.TryParse(_50OnRed, out _id))
            _id = Guid.Empty;
        this._50OnRedId = _id;

        string _SpeedOptimizer_Guru = xdoc.Element("Sites").Element("SitePPC").Element("SpeedOptimizer_Guru").Value;
        if (!Guid.TryParse(_SpeedOptimizer_Guru, out _id))
            _id = Guid.Empty;
        this.SpeedOptimizer_Guru = _id;

        string _PicRec = xdoc.Element("Sites").Element("SitePPC").Element("PicRec").Value;
        if (!Guid.TryParse(_PicRec, out _id))
            _id = Guid.Empty;
        this.PicRecId = _id;

        string _RevizerPopUp = xdoc.Element("Sites").Element("SitePPC").Element("RevizerPopup").Value;
        if (!Guid.TryParse(_RevizerPopUp, out _id))
            _id = Guid.Empty;
        this.RevizerPopup = _id;

        string _Perion = xdoc.Element("Sites").Element("SitePPC").Element("PerionId").Value;
        if (!Guid.TryParse(_Perion, out _id))
            _id = Guid.Empty;
        this.PerionId = _id;

        string _ShopwithboostId = xdoc.Element("Sites").Element("SitePPC").Element("ShopwithboostId").Value;
        if (!Guid.TryParse(_ShopwithboostId, out _id))
            _id = Guid.Empty;
        this.ShopwithboostId = _id;

        this.PastaLeadsDomain = xdoc.Element("Sites").Element("SitePPC").Element("PastaLeadsDomain").Value;
        PastaLeadsDomain = PastaLeadsDomain.Trim();

        this.DonutleadsDomain = xdoc.Element("Sites").Element("SitePPC").Element("donutleadsDomain").Value;
        this.DonutleadsDomain = DonutleadsDomain.Trim();


        this.MainDomain = xdoc.Element("Sites").Element("SitePPC").Element("MainDomain").Value;
        this.MainDomain = this.MainDomain.Trim();

    //    this.DefaultProductName = xdoc.Element("Sites").Element("SitePPC").Element("DefaultProductName").Value.Trim();
        
    }
    private void LoadZapData()
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        this.ZapSiteId = xdoc.Element("Sites").Element("ZapSite").Element("ZapProduction").Element("ZapSiteId").Value;
        this.UserZap = xdoc.Element("Sites").Element("ZapSite").Element("ZapProduction").Element("UserZap").Value;
        this.ZapService = xdoc.Element("Sites").Element("ZapSite").Element("ZapProduction").Element("ZapService").Value;
    }
    private void LoadHeadingCampains()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfHashSetOfCampaignData result = null;
        try
        {
            result = _site.GetAllCampaignsData();
        }
        catch (Exception exc)
        {
           cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetAllCampaignsData " + CRM_FAILD);
            return;
        }   
        foreach (WebReferenceSite.CampaignData Campaign in result.Value)
        {
            if (Campaign.Campaign == WebReferenceSite.eCampaignMode.PhoneDidSlider || Campaign.Campaign == WebReferenceSite.eCampaignMode.FormAndDid)
            {
                string _title = (!Campaign.PhoneDid.UseTitleOther || string.IsNullOrEmpty(Campaign.PhoneDid.TitleOther)) ? string.Empty :
                    Campaign.PhoneDid.TitleOther;
             //   Campaign.PhoneDid.IsRealTimeRouting
                string _titleSecond = (!Campaign.PhoneDid.UseTitleOtherSecond || string.IsNullOrEmpty(Campaign.PhoneDid.TitleOtherSecond)) ? string.Empty :
                    Campaign.PhoneDid.TitleOtherSecond;
                Dictionary<string, WebReferenceSite.GuidStringPair[]> dic_phones = new Dictionary<string,WebReferenceSite.GuidStringPair[]>();
                dic_phones.Add("UNITED STATES", Campaign.PhoneDid.DIDs);
                dic_phones.Add("ISRAEL", Campaign.PhoneDid.DIDs);
                /*
                var rrrr = from x in Campaign.PhoneDid.DIDs
                           where x.Id == new Guid("3B4FEF8D-FDD5-E311-B491-001517D1792A")
                           select x;
                */
                HeadingDidPhone hdp = new HeadingDidPhone(Campaign.PhoneDid.TimeZoneStandardName, dic_phones,
                    Campaign.PhoneDid.Image, _title, _titleSecond, Campaign.PhoneDid.IsRealTimeRouting, Campaign.PhoneDid.WorkingHoursData, Campaign.HeadingCode, this.PerionId);
                if (Campaign.Campaign == WebReferenceSite.eCampaignMode.FormAndDid)
                    hdp.IsFormAndDid = true;
                headings[Campaign.HeadingCode].DidPhone = hdp;                

            }


            if (Campaign.Campaign == WebReferenceSite.eCampaignMode.BannerCampaign || Campaign.UseBannerCampaignsLandingPage)
            {
                bool ShowCampaign = Campaign.Campaign == WebReferenceSite.eCampaignMode.BannerCampaign;
                headings[Campaign.HeadingCode] = new HeadingCampain(headings[Campaign.HeadingCode], Campaign.Banner.Title,
                    Campaign.Banner.BannerHeight, Campaign.Banner.BannerWidth, Campaign.Banner.HtmlCode, Campaign.Banner.DestinationPage, ShowCampaign);
            }
        }
        /*
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        foreach(XElement elem in xdoc.Element("Sites").Element("SitePPC").Element("PhoneDidCountries").Elements("Expertise"))
        {
            string country = elem.Attribute("country").Value;
            string exp = elem.Attribute("headingcode").Value;
            Dictionary<Guid, string> dic_country_phone = new Dictionary<Guid, string>();
            foreach (XElement in_elem in elem.Elements("phone"))
            {
                Guid origin_id;
                if (!Guid.TryParse(in_elem.Attribute("origin").Value, out origin_id))
                    origin_id = Guid.Empty;
                string tel = in_elem.Value;
                dic_country_phone.Add(origin_id, tel);
            }
            if (dic_country_phone.Count == 0)
                continue;
            if (!headings.ContainsKey(exp))// || headings[exp].DidPhone == null)
                continue;
            if (headings[exp].DidPhone == null)
            {
                HeadingDidPhone hdp = LoadPhoneDid(headings[exp].ID);
                if (hdp != null)
                    headings[exp].DidPhone = hdp;
                else
                    continue;
            }
            headings[exp].DidPhone.AddCountryPhons(country, dic_country_phone);
            headings[exp].SetCountries(country);
            
        }
         * */
        WebReferenceSite.ResultOfListOfPhonedidOutsideUsResponse _responsePhonedidOutsideUs = null;
        try
        {
            _responsePhonedidOutsideUs = _site.GetPhoneDidOutsideUS();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetPhoneDidOutsideUS " + CRM_FAILD);
            return;
        }
        var queryPhonedid = from x in _responsePhonedidOutsideUs.Value
                             group x by x.Country into g
                             select new { Country = g.Key, dataCountry = 
                                 from c in g
                                 group c by c.ExpertiseCode into exp
                                 select new { ExpertiseCode = exp.Key, data = exp }
                             };
        foreach (var c in queryPhonedid)
        {
            foreach(var exp in c.dataCountry)
            {
                Dictionary<Guid, string> dic_country_phone = new Dictionary<Guid, string>();
                foreach (var phoneData in exp.data)
                    dic_country_phone.Add(phoneData.OriginId, phoneData.number);
                if (dic_country_phone.Count == 0)
                    continue;
                if (!headings.ContainsKey(exp.ExpertiseCode))// || headings[exp].DidPhone == null)
                    continue;
                if (headings[exp.ExpertiseCode].DidPhone == null)
                {
                    HeadingDidPhone hdp = LoadPhoneDid(headings[exp.ExpertiseCode].ID);
                    if (hdp != null)
                        headings[exp.ExpertiseCode].DidPhone = hdp;
                    else
                        continue;
                }
                headings[exp.ExpertiseCode].DidPhone.AddCountryPhons(c.Country, dic_country_phone);
                headings[exp.ExpertiseCode].SetCountries(c.Country);
            }
            
        }
                             

    }
    private HeadingDidPhone LoadPhoneDid(Guid ExpertiseId)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfPhoneDidSliderCampaignData result = null;
        try
        {
            result = _site.GetPhoneDidData(ExpertiseId);
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("LoadPhoneDid " + CRM_FAILD);
            return null;
        }
        if (result.Value == null)
            return null;
        string _title = (!result.Value.UseTitleOther || string.IsNullOrEmpty(result.Value.TitleOther)) ? string.Empty :
                   result.Value.TitleOther;
        //   Campaign.PhoneDid.IsRealTimeRouting
        string _titleSecond = (!result.Value.UseTitleOtherSecond || string.IsNullOrEmpty(result.Value.TitleOtherSecond)) ? string.Empty :
            result.Value.TitleOtherSecond;
        HeadingDidPhone hdp = new HeadingDidPhone(STANDARD_TIME_NAME_UK, new Dictionary<string, WebReferenceSite.GuidStringPair[]>(),
                   result.Value.Image, _title, _titleSecond, true, new WebReferenceSite.WorkingHoursData[0], string.Empty, this.PerionId);
       // hdp.IsFormAndDid = true;
        return hdp;
    }
    
    private void LoadUrlWebReference()
    {
        this.UrlWebReference = DBConnection.GetUrlWebReference(this.SiteId);
        this.IsLocalMachine = bool.Parse(ConfigurationManager.AppSettings["IsLocal"]);
        /*  PROBLEM WITH IIS express not store on global application config 31-01-2018
        if (this.IsLocalMachine)
        {
            using (ServerManager sm = new ServerManager())
            {
                var bindings = sm.Sites[System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName()].Bindings;
                foreach (Binding b in bindings)
                {
                    if (b.IsIPPortHostBinding)
                    {
                        if (b.Protocol.Equals("http"))
                        {
                            this.Port = b.EndPoint.Port;
                            break;
                        }
                        
                    }

                }
            }
        }
        */
    }

    private void LoadRegularExpressions()
    {
        PhoneReg = DBConnection.GetPhoneRegularExpression(SiteId);
        ZipcodeReg = DBConnection.GetZipcodeRegularExpression();
        PhoneRegularExpression_UK = DBConnection.GetPhoneRegularExpression_UK();
        ZipcodeRegularExpression_UK = DBConnection.GetZipcodeRegularExpression_UK();
        PhoneRegularExpression_USA_and_UK = DBConnection.GetPhoneRegularExperssion_USA_and_UK();
    }

    private void LoadTips()
    {

        Tips = new List<HeadingTips>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfListOfTipData result = null;
        try
        {
            result = _site.GetAllTips();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetAllTips " + CRM_FAILD);
            return;
        }  
        Tips = new List<HeadingTips>();
        foreach (WebReferenceSite.TipData td in result.Value)
            Tips.Add(new HeadingTips(td.HeadingId, td.Subtitle, td.Title));
    }
    private void LoadCategoryHeadingGroup()
    {
        heading_group = new Dictionary<string, eHeadingGroup>();//new List<HeadingGroup_Heading>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfHashSetOfSpecialCategoryGroup result = null;
        try
        {
            result = _site.GetAllSpecialCategroyGroups();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetAllSpecialCategroyGroups " + CRM_FAILD);
            return;
        }  
        foreach (WebReferenceSite.SpecialCategoryGroup scg in result.Value)
        {
            eHeadingGroup ehg;
            if (!Enum.TryParse(scg.ConnectivityName, out ehg))
                continue;
            /*
            eHeadingGroup ehg = eHeadingGroup.none;
            
            switch (scg.ConnectivityName)
            {
                    
                case ("personalInjuryattorney"): ehg = eHeadingGroup.personalInjuryAttorney;
                    break;
                
                case ("attorneys"): ehg = eHeadingGroup.attorney;
                    break;
                case ("IVR"): ehg = eHeadingGroup.IVR;
                    break;
                case ("mover"): ehg = eHeadingGroup.mover;
                    break;
                case ("accountant"): ehg = eHeadingGroup.accountant;
                    break;
                case ("electrician"): ehg = eHeadingGroup.electrician;
                    break;
                case ("computerRepair"): ehg = eHeadingGroup.computerRepair;
                    break;
                case ("handyman"): ehg = eHeadingGroup.handyman;
                    break;
                case ("roofer"): ehg = eHeadingGroup.roofer;
                    break;
                case ("payday"): ehg = eHeadingGroup.payday;
                    break;
                case ("creditCounselor"): ehg = eHeadingGroup.creditCounselor;
                    break;
                case ("dentist"): ehg = eHeadingGroup.dentist;
                    break;
                case ("cleaner"): ehg = eHeadingGroup.cleaner;
                    break;
                case ("hotel"): ehg = eHeadingGroup.hotel;
                    break;
                case ("flight"): ehg = eHeadingGroup.flight;
                    break;
                case ("cruise"): ehg = eHeadingGroup.cruise;
                    break;
                case ("activity"): ehg = eHeadingGroup.activity;
                    break;
                case ("package"): ehg = eHeadingGroup.package;
                    break;
                case ("food"): ehg = eHeadingGroup.food;
                    break;
                case ("Immigration"): ehg = eHeadingGroup.Immigration;
                    break;
               
               
            }
            */
            foreach(Guid _id in scg.HeadingIds)
            {
                string _code = GetHeadingCodeByGuid(_id);
                if (!string.IsNullOrEmpty(_code) && !heading_group.ContainsKey(_code))
                    heading_group.Add(_code, ehg);//new HeadingGroup_Heading(ehg, _id));
                if (ehg == eHeadingGroup.Questions)
                {
                    HeadingCode hc = headings[_code];
                    headings[_code] = new QuestionHeading(hc.heading, hc.code, hc.Title, hc.SingularName, hc.WaterMark, hc.image, hc.SpeakToButtonName,
                        hc.Comment, hc.ID, hc.Slogan, hc.MobileDeviceTitle, hc.MobileDeviceDescription, hc.IncludeGoogleAddon, hc.IsIntext, hc.headingRelated);
                }
            }
           
            
            
        }
    }

    

    private void LoadKeywords()
    {
    //    Keywords = new Dictionary<string, Guid>();
        KeywordsList = new List<KeywordsHeading>();
        keywords = new Dictionary<string, string>();
    //    QueryStringKeywords = new List<KeywordsHeading>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfKeywordsContainer result = null;
        try
        {
            result = _site.GetAllSliderKeywords();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            return;
        }
        
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetAllSliderKeywords " + CRM_FAILD);
            return;
        }  
        
        foreach (WebReferenceSite.KeywordHeadingIdPair khip in result.Value.Keywords)
        {
            string _key = AddonTools.GetKeywordForSearch(khip.Keyword);
            if (string.IsNullOrEmpty(_key) || keywords.ContainsKey(_key))
                continue;
            KeywordsHeading kh = new KeywordsHeading(_key, khip.HeadingId, khip.Priority);
            KeywordsList.Add(kh);
            keywords.Add(_key, GetHeadingCodeByGuid(khip.HeadingId));
            /*
            if (khip.Priority == 1)
                Keywords.Add(kh);
            else if (khip.Priority == 2)
                QueryStringKeywords.Add(kh);
             * */
        }
        foreach (WebReferenceSite.KeywordHeadingIdPair khip in result.Value.NotKeywords)
        {
            string _key = AddonTools.GetKeywordForSearch(khip.Keyword);
            KeywordsHeading kh = new KeywordsHeading(_key, khip.HeadingId, false);
            KeywordsList.Add(kh);            
        }

    }

    private void LoadSliderViewData()
    {
        Flavors = new Dictionary<eFlavour, SliderViewData>();
        foreach (eFlavour ef in Enum.GetValues(typeof(eFlavour)))
        {
            Flavors.Add(ef, new SliderViewData(ef));
        }
    }
    private void LoadIntextFlavors()
    {
        IntextFlavor = new Dictionary<string, eFlavour>();
        string command = "EXEC [dbo].[GetIntextSliderDetails]";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string _code = GetHeadingCodeByGuid((Guid)reader["ExpertiseId"]);
                eFlavour _flavor = (eFlavour)(Enum.Parse(typeof(eFlavour), (string)reader["Name"]));
                if (!IntextFlavor.ContainsKey(_code))                    
                    IntextFlavor.Add(_code, _flavor);
            }
            conn.Close();
        }
        
    }

   
    void LoadHeadings()
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        Dictionary<string, KeyValuePair<string, bool>> dic = new Dictionary<string, KeyValuePair<string, bool>>();
        List<string> emails = new List<string>();
        foreach (XElement X in xdoc.Element("Sites").Element("SitePPC").Element("SpecialSupplierAvailable").Elements())//.Element("ConduitOriginId").Value;
        {
            dic.Add(X.Attribute("headingcode").Value, new KeyValuePair<string, bool>(X.Value.Trim(), true));            
            emails.Add(X.Value.Trim());
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this.UrlWebReference);
        WebReferenceSite.EmailAndIsAvailable[] result_available = GetResultOfListOfEmailAndIsAvailable(emails.ToArray());
        if (result_available != null)
        {
            foreach (string key in dic.Keys.ToArray())
            {
                var query = from x in result_available
                            where x.Email == dic[key].Key
                            select x;
                if (query.Count() == 0)
                    continue;
                dic[key] = new KeyValuePair<string, bool>(dic[key].Key, query.FirstOrDefault().IsAvailable);
            }
        }
        
        WebReferenceSite.ResultOfListOfExpertiseData result = null;
        try
        {
            result = _site.GetAllExpertiseSliderData();
        }
        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
      //      RestartHeadingCache();
            return;
        }

        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            cl.WriteCacheLog("GetAllExpertiseSliderData " + CRM_FAILD);
            return;
        }  

        headings = new Dictionary<string, HeadingCode>();
        foreach (WebReferenceSite.ExpertiseData ed in result.Value)
        {                        
            if (headings.ContainsKey(ed.Code))
                continue;

            WebReferenceSite.RelatedExpertiseData[] relatedExpertiseData = ed.RelatedExpertises;

           
            List<HeadingRelated> headingsRelated = new List<HeadingRelated>();
            HeadingRelated headingRelated;

            for(int i=0;i<relatedExpertiseData.Length;i++)
            {
                headingRelated=new HeadingRelated();
                headingRelated.Code=relatedExpertiseData[i].Code;
                headingRelated.Name=relatedExpertiseData[i].Name;

                headingsRelated.Add(headingRelated);                
            }
            HeadingCode _HeadingCode;
            
            if (dic.ContainsKey(ed.Code))
                _HeadingCode = new HeadingSpecialSupplier(ed.Name, ed.Code, ed.Plural, ed.Singular, PLACE_HOLDER, ed.ImageBack, ed.SpeakToButtonName, ed.Watermark, ed.Id,
                ed.Slogan, ed.MobileDeviceTitel, ed.MobileDeviceDescription, ed.Intext, headingsRelated.ToArray(), dic[ed.Code].Value, dic[ed.Code].Key, ed.InluceGoogleAddon);
            else
                _HeadingCode = new HeadingCode(ed.Name, ed.Code, ed.Plural, ed.Singular, PLACE_HOLDER, ed.ImageBack, ed.SpeakToButtonName, ed.Watermark, ed.Id,
                ed.Slogan, ed.MobileDeviceTitel, ed.MobileDeviceDescription, ed.Intext, headingsRelated.ToArray(), ed.InluceGoogleAddon);
            headings.Add(ed.Code, _HeadingCode);
        }
    }

    private void loadSates()
    {        
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this.UrlWebReference);
        WebReferenceSupplier.ResultOfListOfSEOStateData resultStates= null;
        states = new List<State>();
        try
        {
            resultStates=_supplier.SEOGetAllStates();
            WebReferenceSupplier.SEOStateData[] statesSeo = resultStates.Value;            
            State state;  
            foreach(WebReferenceSupplier.SEOStateData seoData in statesSeo)
            {
                state=new State();
                state.abbreviation=seoData.Abbreviation;
                state.fullName = seoData.FullName;
                state.isPopular = seoData.IsPopular;
                states.Add(state);
            }


        }

        catch(Exception exc)
        {
            cl.WriteCacheLog(exc);            
            return;
        }
       
    }

   

    private void loadPopularCities()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this.UrlWebReference);
        WebReferenceSupplier.ResultOfListOfSEOPopularCityData result = null;
        cities = new List<City>();
        try
        {
            result = _supplier.SEOGetPopularCities();
            WebReferenceSupplier.SEOPopularCityData[] popularCity = result.Value;

            City city;

            foreach (WebReferenceSupplier.SEOPopularCityData seoPopularCityData in popularCity)
            {
                city = new City();
                city.CityName=seoPopularCityData.CityName;
                city.StateAbbreviation = seoPopularCityData.StateAbbreviation;
                city.StateFullName=seoPopularCityData.StateFullName;

                cities.Add(city);
            }
            
        }

        catch (Exception exc)
        {
            cl.WriteCacheLog(exc);
            //      RestartHeadingCache();
            return;
        }
    }

    #endregion
    #region functions
    public List<Guid> GetPartnerBlackInjections()
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        List<Guid> listNotIncludeOrigins = new List<Guid>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this.UrlWebReference);
        WebReferenceSite.ResultOfListOfGuid result = null;
        try
        {
            result = _site.GetParasiteOriginBlackList();
        }
        catch(Exception exc)
        {
            cl.WriteCacheLog(exc);
            return null;
        }
        if(result.Type == WebReferenceSite.eResultType.Failure)
        {
            return null;
        }
        /*
        foreach (XElement xelem in xdoc.Element("Sites").Element("SitePPC").Element("Injections").Element("OriginMonetizationInjection").Element("NotIncludeOrigins").Elements("origin"))
            listNotIncludeOrigins.Add(new Guid(xelem.Value));
         * */
     //   foreach(Guid originId in result.Value)
        listNotIncludeOrigins = result.Value.ToList();
        return listNotIncludeOrigins;
    }
    public bool IsDesktopOrigin(Guid OriginId)
    {
        return (this.DesktopOrigins.BinarySearch(OriginId) > -1);
    }
    public bool IsInIntextBlackList(Guid OriginId)
    {
        return (IntextBlackOrigins.BinarySearch(OriginId) > -1);
    }
    public CacheLog GetCacheLog
    {
        get { return cl; }
    }
    public bool IsNoProblemSlider(Guid OriginId)
    {
        return NoProblemSliderOrigins.BinarySearch(OriginId) > -1;
    }
    public bool IsBadDomain(string domain)
    {
        return SalesUtility.IsInBlackDomain(domain);
    }
    public bool IsBlackOrigin(Guid OriginId)
    {
        return BlackOrigin.BinarySearch(OriginId) > -1;
    }
    public bool IsBadDomain(string domain, eSliderType SliderType, Guid OriginId)
    {
        if (string.IsNullOrEmpty(domain) || SliderType == eSliderType.GoogleFrame)
            return false;
        if (domain.EndsWith(".google.com") && (OriginId == RevizerOriginId || OriginId == CrossriderId || OriginId == CrossriderTestId))
            return false;
        return IsBadDomain(domain);
    }
    public string GetPhoneRegForJavascript
    {
        get { return Utilities.RegularExpressionForJavascript(PhoneReg); }
    }
    public string GetZipcodeRegForJavascript
    {
        get { return Utilities.RegularExpressionForJavascript(ZipcodeReg); }
    }
    public string GetUK_PhoneRegForJavascript
    {
        get { return Utilities.RegularExpressionForJavascript(PhoneRegularExpression_UK); }
    }
    public string GetUK_ZipcoseRegForJavascript
    {
        get { return Utilities.RegularExpressionForJavascript(ZipcodeRegularExpression_UK); }
    }

    public List<string> GetHeadingByChar(string _char)
    {
        if (string.IsNullOrEmpty(_char) || _char.Length != 1)
            _char = "A";
        if ((int)_char[0] > 90 || (int)_char[0] < 65)
            _char = "A";
        _char = _char.ToLower();
        IEnumerable<string> query = (from x in headings
                                     where x.Value.heading.ToLower().StartsWith(_char)
                                     select x.Value.heading);
        return query.ToList();
    }
    public string[] GetAllCategoriesDisplay()
    {
        IEnumerable<string> query = from x in CategoryDisplay
                                    select x.Key;
        return query.ToArray();
    }
    public string[] GetCategoryDisplay(string HeadingCode)
    {
        IEnumerable<string> query = from x in CategoryDisplay
                                    where x.Value.Contains(HeadingCode)
                                    select x.Key;
        if(query.Count() == 0)
            return null;
        return query.ToArray();
    }


    public IEnumerable<KeyValuePair<string, List<string>>> GetCategoryDisplay2(string HeadingCode)
    {
        IEnumerable<KeyValuePair<string, List<string>>> query = from x in CategoryDisplay
                                    where x.Value.Contains(HeadingCode)
                                    select x;
                         
        if (query.Count() == 0)
            return null;
        return query.ToList();
        
    }

    public string[] GetHeadingCodeByCategoryNameDisplay(string CategoryNameDisplay)
    {
        if (!CategoryDisplay.ContainsKey(CategoryNameDisplay))
            return null;
        return CategoryDisplay[CategoryNameDisplay].ToArray();
    }

    public List<string> GetHeadingNameByCategoryNameDisplay(string[] listHeadingCode)
    {
        List<string> listHeadingName = new List<string>();

        for (int i = 0; i < listHeadingCode.Length; i++)
        {
            listHeadingName.Add(GetHeadingName(listHeadingCode[i]));
        }

        
        return listHeadingName;
    }

    public string[] GetHeadingByTagPerfix(string TagPrefix)
    {
        TagPrefix = TagPrefix.ToLower();
        IEnumerable<string> query = (from x in headings
                                     where x.Value.heading.ToLower().Contains(TagPrefix)
                                     select x.Value.heading).Take(10);

        return query.ToArray();
    }
    public string[] GetHeadingByTagPerfix(string TagPrefix, int count)
    {
        TagPrefix = TagPrefix.ToLower();
        IEnumerable<string> query = (from x in headings
                                     where x.Value.heading.ToLower().Contains(TagPrefix)
                                     select x.Value.heading).Take(count);

        return query.ToArray();
    }

    public string[] GetHeadingByTagPerfixStartWith(string TagPrefix, int count)
    {
        TagPrefix = TagPrefix.ToLower();
        IEnumerable<string> query = (from x in headings
                                     where x.Value.heading.ToLower().StartsWith(TagPrefix)
                                     select x.Value.heading).Take(count);

        return query.ToArray();
    }

    public string[] GetHeadingByTagPerfixStartWithAny(string TagPrefix, int count)
    {
        TagPrefix = TagPrefix.ToLower();

        IEnumerable<string> query;

        if (TagPrefix.Split(' ').Length == 1)
        {          

            query = (from x in headings
                     where x.Value.heading.ToLower().Split(' ').Any(searchTerm => searchTerm.StartsWith(TagPrefix))  
                     select x.Value.heading).Take(count);
        }

        else
        {
            query = (from x in headings
                     where x.Value.heading.ToLower().StartsWith(TagPrefix)  
                     select x.Value.heading).Take(count);
        }

        return query.ToArray();
        
    }

    public IEnumerable<KeyValuePair<string, string>> GetHeadingCodeByTagPerfix(string TagPrefix, int count)
    {
        TagPrefix = TagPrefix.ToLower();
        IEnumerable<KeyValuePair<string, string>> query = (from x in headings
                                     where x.Value.heading.ToLower().Contains(TagPrefix)
                                     select new KeyValuePair<string, string>(x.Value.code, x.Value.heading)).Take(count);

        return query;
    }
    public Dictionary<string, string> GetClosestHeadings(string TagPrefix, int sum)
    {
        Dictionary<string, string> list = new Dictionary<string, string>();
        int count = sum;
        while (list.Count < sum && TagPrefix.Length >= 0)
        {
            IEnumerable<KeyValuePair<string, string>> ie = GetHeadingCodeByTagPerfix(TagPrefix, count);
            foreach (KeyValuePair<string, string> kvp in ie)
            {
                if(!list.ContainsKey(kvp.Key))
                    list.Add(kvp.Key, kvp.Value);
            }
            
            count = sum - list.Count;
            if (TagPrefix.Length > 0)
                TagPrefix = TagPrefix.Substring(0, TagPrefix.Length - 1);
        }
        return list;
    }
    public List<HeadingCode> GetCodeNameByGuids(List<Guid> _ids)
    {

        IEnumerable<HeadingCode> query = from x in this.headings
                                         where _ids.Contains(x.Value.ID)
                                         select x.Value;
        return query.ToList();
    }
    public bool IsHeadingCodeExists(string heading_code)
    {
        return headings.ContainsKey(heading_code);
    }
    public string GetHeadingCodeByGuid(Guid _id)
    {

        IEnumerable<string> query = from x in this.headings
                                    where x.Value.ID == _id
                                    select x.Value.code;
        if (query.Count() == 0)
            return string.Empty;
        return query.FirstOrDefault();
    }

    public bool IsHeadingByName(string name)
    {
        name = name.ToLower();
        int result = (from x in headings
                      where x.Value.heading.ToLower().Equals(name)
                      select x.Value).Count();
        return (result > 0);
    }
    //GetHeadingCodeByKeyword
    public string GetHeadingCodeByNameKeyword(string key)
    {
        key = key.ToLower();
        var query = (from x in headings
                     where x.Value.heading.ToLower().Equals(key)
                     select x.Value);
        if(query.Count() > 0)
            return query.FirstOrDefault().code;
        return GetHeadingCodeByKeyword(key);
    }
    public bool IsHeadingByName(string name, out string region)
    {
        region = string.Empty;
        name = name.ToLower();
        int _count = 0;

        //      bool IsFirstTime = true;

        _count = (from x in headings
                  where x.Value.heading.ToLower().Equals(name)
                  select x.Value).Count();


        if (_count > 0)
            return true;
        while (_count == 0 && name.Length > 0)
        {
            _count = (from x in headings
                      where x.Value.heading.ToLower().Contains(name)
                      select x.Value).Count();
            if (_count == 0)
            {
                name = name.Substring(0, name.Length - 1);
                //        IsFirstTime = false;
            }
        }
        if (_count == 0)
            return (false);
        region = (from x in headings
                  where x.Value.heading.ToLower().Contains(name)
                  select x.Value.heading).FirstOrDefault();
        return (false);
    }
    public HeadingCode GetHeadingDetails(string code)
    {
        if (string.IsNullOrEmpty(code) || !headings.ContainsKey(code))
            return null;
        return headings[code];
    }
    public HeadingCode GetHeadingDetails(Guid Heading_Id)
    {
        return GetHeadingDetails(GetHeadingCodeByGuid(Heading_Id));
    }
    public Guid GetHeadinGuid(string name)
    {
        name = name.ToLower();
        Guid _id = Guid.Empty;
        _id = (from x in headings
               where x.Value.heading.ToLower().Equals(name)
                 select x.Value.ID).SingleOrDefault();
        return _id;
    }
    public Guid GetHeadinGuidByCode(string _code)
    {
        return headings[_code].ID;
    }
    public List<string> GetHeadingsByGuids(Guid[] guids)
    {
        List<Guid> list = new List<Guid>(guids);
        IEnumerable<string> query = (from x in headings
                                     where list.Contains(x.Value.ID)
                                     select x.Value.heading);
        return query.ToList();
    }
    public string GetHeadinCode(ref string name)
    {
        string _name = name.ToLower();
        HeadingCode hc = null;
        hc = (from x in headings
                where x.Value.heading.ToLower().Equals(_name)
               select x.Value).SingleOrDefault();
        if (hc == null || string.IsNullOrEmpty(hc.code))
        {
            return string.Empty;
        }
        name = hc.heading;
        return hc.code;
    }
    public string GetHeadingName(string code)
    {
        string name = (from x in headings
                       where x.Value.code.Equals(code)
              select x.Value.heading).SingleOrDefault();
        return name;
    }
    public string GetHeadingCodeByKeyword(string keyword)
    {
        if (string.IsNullOrEmpty(keyword))
            return string.Empty;
        if (!keywords.ContainsKey(keyword))
            return string.Empty;
        return keywords[keyword];
    }

    public string[] GetHeadingNamesByKeywordStartWith(string keyword, int count)
    {
        string[] temp;

        if (string.IsNullOrEmpty(keyword))
        {
            temp = new string[0];
            return temp;
        }

        IEnumerable<string> query;

        if (keyword.Split(' ').Length == 1)
        {
            query = (from x in keywords
                     where x.Key.ToLower().Split(' ').Any(searchTerm => searchTerm.StartsWith(keyword.ToLower()))
                     select GetHeadingName(x.Value)).Distinct().Take(count);
        }

        else
        {
            query = (from x in keywords
                     where x.Key.ToLower().StartsWith(keyword.ToLower())
                     select GetHeadingName(x.Value)).Distinct().Take(count);
        }
       
        return query.ToArray();

    }

    public string[] GetHeadingNamesByKeywordStartWith(string keyword, int count, string profession)
    {
        string[] temp;

        if (string.IsNullOrEmpty(keyword))
        {
            temp = new string[0];
            return temp;
        }

        IEnumerable<string> query;

        if (keyword.Split(' ').Length == 1)
        {
            query = (from x in keywords
                     where x.Key.ToLower().Split(' ').Any(searchTerm => searchTerm.StartsWith(keyword.ToLower()) )
                     where GetHeadingName(x.Value).ToLower().Contains(profession.ToLower())
                     select GetHeadingName(x.Value).Replace("attorney", "").Trim()).Distinct().Take(count);
        }

        else
        {
            query = (from x in keywords
                     where x.Key.ToLower().StartsWith(keyword.ToLower())
                     where GetHeadingName(x.Value).ToLower().Contains(profession.ToLower())
                     select GetHeadingName(x.Value).Replace("attorney","").Trim()).Distinct().Take(count);
        }

        return query.ToArray();

    }

    public string GetHeadingCodeByKeywordHeadingName(string keyword)
    {
        keyword = keyword.ToLower();
        IEnumerable<HeadingCode> hc = from x in headings
                                      where x.Value.heading.ToLower() == keyword
                                      select x.Value;

        if (hc.Count() > 0)
            return hc.FirstOrDefault().code;

        return GetHeadingCodeByKeyword(keyword);
    }
    
    public eHeadingGroup GetHeadingGroup(string Expertiser_code)
    {
        if (!heading_group.ContainsKey(Expertiser_code))
            return eHeadingGroup.none;
        return heading_group[Expertiser_code];
    }

    public bool HeadingOfGroup(string  _HeadingCode, eHeadingGroup _group)
    {
        if (!heading_group.ContainsKey(_HeadingCode))
            return false;
        return (heading_group[_HeadingCode] == _group);
    }
    public bool HeadingOfGroup(Guid HeadingId, eHeadingGroup _group)
    {
        return HeadingOfGroup(GetHeadingCodeByGuid(HeadingId), _group);
    }

    public List<string> getSiblingsHeadings(eHeadingGroup headinGroup)
    {
        List<string> listHeadings=new List<string>();

        foreach (KeyValuePair<string, eHeadingGroup> kvp in heading_group)
        {
            if (kvp.Value == headinGroup)
            {

                listHeadings.Add(GetHeadingName(kvp.Key));
            }

        }

        return listHeadings;       
    }
   

    public List<TitleTip> GetTips(string code)
    {
        Guid _id = GetHeadinGuidByCode(code);
        if (_id == Guid.Empty)
            return new List<TitleTip>();
        IEnumerable<TitleTip> query = from x in this.Tips
                                    where x.Id == _id || x.Id == Guid.Empty
                                    select new TitleTip() { Tip = x.Tip, Title = x.Title };
        return new List<TitleTip>(query);
    }
    public List<KeywordsHeading> GetKeywordsByCode(string code)
    {
        Guid _id = this.GetHeadinGuidByCode(code);
        IEnumerable<KeywordsHeading> query = from x in this.KeywordsList
                                             where x.HeadingId == _id
                                             select x;
        return query.ToList();
    }
    public List<string> GetNotKeywordsByHeadingIdForJs(Guid HeadinId)
    {
        IEnumerable<string> query = from x in this.KeywordsList
                                    where x.HeadingId == HeadinId && !x.IsPositiveKey
                                    select x.Keyword;
        if (query == null || query.Count() == 0)
            return new List<string>();

        List<string> list = new List<string>();
        foreach (string str in query)
        {
            string[] keys = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (keys.Count() > 0)
                list.Add(keys[0]);
        }
        return list;
    }
    public Dictionary<string, bool> GetKeywordsByHeadingIdForJs(Guid HeadinId)
    {
        IEnumerable<KeyValuePair<string, bool>> query = from x in this.KeywordsList
                                                        where x.HeadingId == HeadinId
                                                        select new KeyValuePair<string, bool>(x.Keyword, x.IsPositiveKey);

        bool ThereIsPositive = (from x in query
                                where x.Value
                                select x).Count() > 0;
        if (query == null || query.Count() == 0 || !ThereIsPositive)
            return new Dictionary<string, bool>();

        Dictionary<string, bool> dic = new Dictionary<string, bool>();

        foreach (KeyValuePair<string, bool> kvp in query)
        {
            string[] keys = kvp.Key.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (kvp.Value)
            {
                foreach (string s in keys)
                {
                    if (!dic.ContainsKey(s))
                        dic.Add(s, true);
                }
            }
            else
            {
                if (keys.Length > 0 && !dic.ContainsKey(keys[0]))
                    dic.Add(keys[0], false);
            }
        }
        return dic;
    }
    public bool IsInBlackList(string keyword, string domain)
    {
        string kd = keyword + ";" + domain;
        return BlackKeywordDomain.BinarySearch(kd) > -1;
    }
    public bool IsHeadingByOrigin(string keyword, Guid OriginId)
    {
        string __HeadingCode = GetHeadingCodeByKeyword(keyword);
        if (string.IsNullOrEmpty(__HeadingCode))
            return false;
        if (!BlackHeadingByOrigin.ContainsKey(OriginId))
            return true;
        if (BlackHeadingByOrigin[OriginId].BinarySearch(__HeadingCode) > -1)
            return false;
        return true;
    }
    public bool IsInBlackDomainByOrigin(Guid OriginId, string domain)
    {
        if (!BlackDomainByOrigin.ContainsKey(OriginId))
            return false;
        if (BlackDomainByOrigin[OriginId].BinarySearch(domain) > -1)
            return true;
        return false;
    }
    /*
    public bool IsGoodLocationHeadingByKeyword(SliderData sd, string _keyword)
    {
        return IsGoodLocationHeading(IPCountryName, GetHeadingCodeByKeyword(_keyword), _OriginId);
    }
     * */
    public bool IsGoodLocationHeadingByKeyword(SliderData sd, string _keyword)//(string IPCountryName, string _HeadingCode, Guid _OriginId)
    {
        string _HeadingCode = GetHeadingCodeByKeyword(_keyword);
        if (string.IsNullOrEmpty(_HeadingCode))
            return false;
        eHeadingGroup ehg;
        if (heading_group.ContainsKey(_HeadingCode))
            ehg = heading_group[_HeadingCode];
        else
            ehg = eHeadingGroup.none;
        /****** TODO ********/
        if (_HeadingCode == "2289" && sd.Country == CANADA)//Computer repair
      //  if (_HeadingCode == "2289")//Computer repair
        
            return true;
        
        //ONLY IN UK
        /* remove on dev: 12/01/2015
        if (_HeadingCode == "2145" || _HeadingCode == "2128")//2145 Occupational Therapist //2128 Massage Therapist

        {
            if (sd.Country == UNITED_KINGDOM)
                return true;
            return false;
        }
         * */
       
        if (sd.Country == UNITED_KINGDOM)
        {
            /* remove on dev: 12/01/2015
           switch (_HeadingCode)
           {
               //case ("2268")://Health Insurance

               case ("2294")://HVAC
               case ("2158")://Plumber
               case ("2079")://Electrician
               case ("2085")://Fence Installer / Repair
               case ("2057")://Concrete Installer/ Repair
               case ("2216")://Window Installer / Repair
               case ("2094")://General Contractor
               case ("2174")://Roofer

               case ("2065")://Dentist
               //case("
                   return true;
           }
             * * */
            switch (ehg)
            {
                //case(eHeadingGroup.HomeSecurity):
                case(eHeadingGroup.payday):
                    return true;
            }
            
        }
         
        if (ehg == eHeadingGroup.BinaryOptions)
            return true;
        if (_HeadingCode == "3020" && sd.Country != UNITED_STATES && !sd.IsHttpsScheme())
            return true;        
        /******************/
        if (headings[_HeadingCode].IsServeCountry(sd.Country, sd.OriginId))
            return true;
        
        if (ehg == eHeadingGroup.hotel)
            return true;        
       /*
        if (IsCJCampain(sd))
           return true;
        */
        switch (sd.Country)
        {
            case (UNITED_STATES):
            case (ISRAEL):
                return true;

        }
        return false;
    }
    public bool IsGoodLocationRegulareHeading(string IPCountryName, string _HeadingCode)
    {
        if (string.IsNullOrEmpty(_HeadingCode))
            return false;
        /****** TODO ********/
        if (_HeadingCode == "2289" && IPCountryName == "CANADA")
            return true;
        /******************/
        eHeadingGroup ehg;
        if (heading_group.ContainsKey(_HeadingCode))
            ehg = heading_group[_HeadingCode];
        else
            ehg = eHeadingGroup.none;
        if (ehg == eHeadingGroup.hotel)
            return true;
       
        if (ehg == eHeadingGroup.dating)
        {
            switch (IPCountryName)
            {
                case ("UNITED STATES"):
                case ("CANADA"):
                case ("ISRAEL"):
                    return true;
                default:
                    return false;
            }
        }        
        switch (IPCountryName)
        {
            case ("UNITED STATES"):
            case ("ISRAEL"):
                return true;
        }
        return false;
    }


    string[] GetAllHeadingCampainCodes()
    {
        IEnumerable<string> query = from x in headings
                                    where x.Value.GetType() == typeof(HeadingCampain)
                                    select x.Value.code;

        return query.ToArray();
    }
    string[] GetAllHeadingOnlyCampainCodes()
    {
        IEnumerable<string> query = from x in headings
                                    where x.Value.GetType() == typeof(HeadingCampain) && ((HeadingCampain)x.Value).ShowBanner
                                    select x.Value.code;

        return query.ToArray();
    }
    public bool IsCJCampain(SliderData sd)
    {
        if (string.IsNullOrEmpty(sd.ExpertiseCode))
            return false;
        if (!headings.ContainsKey(sd.ExpertiseCode))
            return false;
        if (sd.SliderType == eSliderType.Intext)
            return this.IntextFlavor[sd.ExpertiseCode] == eFlavour.Flavor_Half_Controls_Top_Cj;
        return headings[sd.ExpertiseCode] is HeadingCampain;
    }
    public HeadingCampain GetHeadingCampain(SliderData sd)
    {
        if (!IsCJCampain(sd))
            return null;
        return (HeadingCampain)headings[sd.ExpertiseCode];
    }
    public Guid[] GetAllHeadingDidPhoneCampainInRealTimeRouting()
    {
        IEnumerable<Guid> query = from x in headings
                                    where x.Value.DidPhone != null && x.Value.DidPhone.IsRealTimeRouting
                                    select x.Value.ID;

        return query.ToArray();
    }
    public string SetHeadingDidPhoneCampainInRealTimeAvailable(WebReferenceSite.HeadingIdAndIsAvailable[] list)
    {
        StringBuilder _log = new StringBuilder();
        foreach (WebReferenceSite.HeadingIdAndIsAvailable hiia in list)
        {
            HeadingCode hc = (from x in headings
             where x.Value.ID == hiia.HeadingId
             select x.Value).FirstOrDefault();
            hc.DidPhone.IsAvailable = hiia.IsSomeoneAvailable;
            _log.AppendLine(hc.heading + "(" + hc.code + "): " + hiia.IsSomeoneAvailable.ToString());
          //  hd.DidPhone.IsRealTimeRouting = hiia.IsSomeoneAvailable;
        }
        return _log.ToString();
    }
    /*
    public void UpdateDateModified()
    {
        lock (_lock)
        {
            IsLoading = true;
            FileInfo fi = new FileInfo(DESTINATION);
            this.DateModified = fi.LastWriteTime;
            IsLoading = false;
        }
    }
    */
    public bool IsPrivacyHeading(string _HeadingCode)
    {
        return HeadingOfGroup(_HeadingCode, eHeadingGroup.attorney) || HeadingOfGroup(_HeadingCode, eHeadingGroup.personalInjuryAttorney);
    }
    public bool IsZapSite(string _siteId)
    {
        return this.ZapSiteId == _siteId;
    }
    public string GetUserZapLoginUser(string _email)
    {
        return this.UserZap + _email;
    }
    public string UpdateSpecialSupplierAvailable()
    {
        IEnumerable<HeadingSpecialSupplier> query = from X in headings
                                                    where X.Value is HeadingSpecialSupplier
                                                    select (HeadingSpecialSupplier)X.Value;
        IEnumerable<string> queryEmails = from X in query
                                          select X.Email;

        WebReferenceSite.EmailAndIsAvailable[] result_available = GetResultOfListOfEmailAndIsAvailable(queryEmails.ToArray());
        if (result_available == null)
            return "FAILD: result_available=null";
        int i = 0;
        foreach (WebReferenceSite.EmailAndIsAvailable email_available in result_available)
        {
            IEnumerable<HeadingSpecialSupplier> query_available = from x in query
                                                                  where x.Email == email_available.Email
                                                                  select x;
            if (query_available.Count() == 0)
                continue;
            HeadingSpecialSupplier hss = query_available.FirstOrDefault();
            hss.SupplierAvailable = email_available.IsAvailable;
            i++;
        }
        return "SUCCESS " + i + " updates";
    }
    private WebReferenceSite.EmailAndIsAvailable[] GetResultOfListOfEmailAndIsAvailable(string[] emails)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this.UrlWebReference);
        WebReferenceSite.ResultOfListOfEmailAndIsAvailable result_available;
        try
        {
            result_available = _site.CheckSpecialAdvertisersAvailability(emails.ToArray());
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            //      RestartHeadingCache();
            return null;
        }
        if (result_available.Type == WebReferenceSite.eResultType.Failure)
            return null;
        return result_available.Value;
    }
    public string DidPhoneJobUpdate()
    {
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this.UrlWebReference);
        WebReferenceSite.ResultOfListOfHeadingIdAndIsAvailable result = null;
        try
        {
            result = _site.DidSliderCheckAvailability(this.GetAllHeadingDidPhoneCampainInRealTimeRouting());
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return string.Empty;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;
        return this.SetHeadingDidPhoneCampainInRealTimeAvailable(result.Value);
    }
    public string GetOriginBroughtByName(Guid OriginId)
    {
        if(!OriginBroughtBy.ContainsKey(OriginId))
            return null;
       // new SliderData(
        return OriginBroughtBy[OriginId];
    }
    public bool IsDistributionOrigin(Guid OriginId)
    {
        return DistributionList.BinarySearch(OriginId) > -1;
    }
    public string GetSliderCloseButtonExpire(Guid OriginId)
    {
        if (!IsDistributionOrigin(OriginId))
            return "null";
        return SliderCloseButtonExpire;        
    }
    public string GetSliderMinimizeButtonExpire(Guid OriginId)
    {
        if (!IsDistributionOrigin(OriginId))
            return "null";
        return SliderMinimizeButtonExpire;
    }
    public string GetZipCodeRegulareExpression(string country)
    {
        if (country == "UNITED KINGDOM")
            return ZipcodeRegularExpression_UK;
        return ZipcodeReg;
    }
    public Guid GetOriginOrDefault(string origin_id)
    {
        Guid _id;
        if (!Guid.TryParse(origin_id, out _id))
            return this.UnknoenOriginId;
        if (AllOrigins.BinarySearch(_id) > -1)
            return _id;
        return this.UnknoenOriginId;

    }
    public string GetPopupEgineOriginsForJavascript()
    {
        StringBuilder sb = new StringBuilder("[");
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(UrlWebReference);
        WebReferenceSite.ResultOfListOfGuid result = null;
        try
        {
            result = _site.GetOriginsWithAdEngineEnabled();
        }
        catch (Exception Exc)
        {
            dbug_log.ExceptionLog(Exc);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Exception exc = new Exception("GetOriginsWithAdEngineEnabled: CRM_FAILD");
            dbug_log.ExceptionLog(exc);
            return null;
        }
        foreach (Guid id in result.Value)
            sb.Append("'" + id.ToString().ToLower() + "',");
        if (sb.Length > 1)
            sb.Remove(sb.Length - 1, 1);
        sb.Append("]");
        return sb.ToString();
    }
    public bool IsMonitizationOrigin(string origin_id, string Country)
    {
        Guid OriginId;
        if (!Guid.TryParse(origin_id, out OriginId))
            OriginId = this.UnknoenOriginId;
        if (!this.MonetizationOriginInjection.ContainsKey(OriginId))
            return false;
        return this.MonetizationOriginInjection[OriginId].Contains(Country);
    }
    public string GetMonHtmlEventJs(string origin_id, string Country)
    {
        return this.MonHtmlEventJs.Replace("{TI}", IsMonitizationOrigin(origin_id, Country).ToString().ToLower());
    }
    #endregion
    #region Static functions
    public static void SetCach(String key, Object item,
      CacheItemRemovedReason reason)
    {
        if (!IsLoading)
        {
            PpcSite _head = PpcSite._PpcSite();
            _SetCache(_head);
        }
       /*
        lock (_lock)
        {
            IsLoading = true;
            string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
            XDocument xdoc = XDocument.Load(path);
            string SiteId = xdoc.Element("Sites").Element("SitePPC").Element("SiteId").Value;
            Guid GeneralOrigin = new Guid(xdoc.Element("Sites").Element("SitePPC").Element("GeneralOrigin").Value);
            PpcSite _head = PpcSite._PpcSite(SiteId, GeneralOrigin);
         //   if(item != null)
         //       Thread.Sleep(90000);
            
            CacheDependency cd = new CacheDependency(path);
            HttpRuntime.Cache.Insert("Heading", _head, cd, Cache.NoAbsoluteExpiration,
                 new TimeSpan(1, 0, 0), CacheItemPriority.High, new CacheItemRemovedCallback(SetCach));
            //     AddonTools.UpdateKeywordJs3(); 

            IsLoading = false;
        }  
    */
        
    }
    private static void _SetCache(PpcSite _head)
    {
        lock (_lock)
        {
            IsLoading = true;
   //         string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";          
            /*
           
            XDocument xdoc = XDocument.Load(path);
            string SiteId = xdoc.Element("Sites").Element("SitePPC").Element("SiteId").Value;
            Guid GeneralOrigin = new Guid(xdoc.Element("Sites").Element("SitePPC").Element("GeneralOrigin").Value);
            PpcSite _head = PpcSite._PpcSite(SiteId, GeneralOrigin);
           */
            CacheDependency cd = null;// new CacheDependency(path);
            HttpRuntime.Cache.Insert("Heading", _head, cd, Cache.NoAbsoluteExpiration,
                /*Cache.NoSlidingExpiration*/ new TimeSpan(1, 0, 0), CacheItemPriority.High, new CacheItemRemovedCallback(SetCach));
            //     AddonTools.UpdateKeywordJs3(); 

            IsLoading = false;
        }    
    }
    public static void SetCachOnAppStart()
    {
        CacheLog._ApplicationStart();
        PpcSite _head = PpcSite._PpcSite();
    //    PpcSite.SetCach("Heading", null, CacheItemRemovedReason.DependencyChanged);
        PpcSite._SetCache(_head);
    }
    public static void SetCach(Guid UserId, string UserName)
    {
        PpcSite _head = PpcSite._PpcSite();
        IsLoading = true;
        HttpRuntime.Cache.Remove("Heading");
        PpcSite._SetCache(_head);
    }
    
    public static PpcSite GetCurrent()
    {
        if (IsLoading)
        {
            lock (_lock)
            {
            }
        }
        return ((PpcSite)HttpRuntime.Cache["Heading"]);
    }
    
   
    public static UserMangement SupplierLogin(string mobile, string password)
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;
        string _siteid = PpcSite.GetCurrent().SiteId;

        try
        {
            _response = supplier.AdvertiserLogin(mobile, password);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, _siteid);
            return null;
        }
        if (_response.Type != WebReferenceSupplier.eResultType.Success)
            return null;
        if (_response.Value.UserLoginStatus != WebReferenceSupplier.eUserLoginStatus.OK || _response.Value.UserId == Guid.Empty)
        {
            DBConnection.InsertLogin(mobile, Guid.Empty, _siteid, eUserType.Supplier);
            return null;
        }
        DBConnection.InsertLogin(mobile, _response.Value.UserId, _siteid, eUserType.Supplier);
        bool RegistrationStage = _response.Value.StageInRegistration > 5;
        UserMangement um = new UserMangement(_response.Value.UserId.ToString(), _response.Value.Name, (double)_response.Value.Balance,
            RegistrationStage);

        return um;
        
    }    
    
    public static void ReloadKeywordDomainBlackList()
    {
        PpcSite _ps = PpcSite.GetCurrent();
        if (_ps == null)
            return;
        lock (_lock)
        {
            IsLoading = true;
            _ps.LoadKeywordDomainBlackList();
            IsLoading = false;                
        }
    }

    public string getFullStateByAbbreviation(string abbreviationState)
    {
        string tempFullState = abbreviationState;

        for (int i = 0; i < states.Count; i++)
        {
            if (abbreviationState.ToLower() == states[i].abbreviation.ToString().ToLower())
            {
                tempFullState = states[i].fullName;
            }
        }

        return tempFullState;
    }
    public string GetDistributionOriginListForJavascript()
    {
        StringBuilder result = new StringBuilder("[");
        foreach(Guid id in DistributionList)
            result.Append("'" + id.ToString().ToLower() + "',");
        if (result.Length > 1)
            result.Remove(result.Length - 1, 1);
        result.Append("]");
        return result.ToString();
    }
    public List<Guid> GetOriginsWithVertiShopping()
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
        string origin = xdoc.Element("Sites").Element("SitePPC").Element("Injections").Element("InjectionVertiShopping").Value;
        Guid VertiShoppingId = Guid.Parse(origin);
        List<Guid> list = new List<Guid>();
        string command = @"SELECT I.New_OriginId    
FROM [dbo].[New_injectionoriginExtensionBase] I WITH(NOLOCK)
	INNER JOIN [dbo].[New_injectionoriginBase] IB WITH(NOLOCK) on I.New_injectionoriginId = IB.New_injectionoriginId
WHERE I.New_InjectionId = @InjectionId
	AND IB.DeletionStateCode = 0";
        using(SqlConnection conn = DBConnection.GetSalesConnString())
        {
            using(SqlCommand cmd = new SqlCommand(command, conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@InjectionId", VertiShoppingId);
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                    list.Add((Guid)reader["New_OriginId"]);
                conn.Close();
            }
        }
        return list;
    }
    void SaveCache()
    {
        /*
        try
        {
            MongoDB.Driver.MongoCollection collection = DBConnection.GetMongoCacheCollection();
            MongoDB.Driver.WriteConcernResult _result = collection.Insert<PpcSite>(this);
            if (!_result.Ok)
            {
                PpcCacheException pce = new PpcCacheException("Faild to save cache in MongoDB");
                pce.NewValues = new Dictionary<string, string>();
                pce.NewValues.Add("ErrorMessage", _result.LastErrorMessage);
                cl.WriteCacheLog(pce);

            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        */
     
        

        try
        {
            DateTime _now = DateTime.Now;
            /*
            DriveInfo dis = new DriveInfo(ConfigurationManager.AppSettings["CacheData"]);
            System.IO.FileInfo[] fileNames = dis.RootDirectory.GetFiles("*.*");
            
            var query = from x in fileNames
                        orderby x.CreationTime descending
                        select x;
             * */
            System.IO.FileInfo _file = GetFileInfoLastDate();
            Random rnd = new Random();
            if (_file == null || (_now - _file.CreationTime).Minutes > 10)
            {
                string _dir = ConfigurationManager.AppSettings["CacheData"];
                string full_path = string.Empty;
                string file_name = GetDateFileFormat();
                do
                {

                    int num = rnd.Next(10000);
                    full_path = _dir + file_name + num + ".log";
                } while (File.Exists(full_path));

                Stream stream = File.Open(full_path, FileMode.Create);
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                bFormatter.Serialize(stream, this);
                stream.Close();
            }
   

           
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
        }  


    }
    static System.IO.FileInfo GetFileInfoLastDate()
    {
        //DriveInfo dis = new DriveInfo(ConfigurationManager.AppSettings["CacheData"]);
        DirectoryInfo dis = new DirectoryInfo(ConfigurationManager.AppSettings["CacheData"]);
        System.IO.FileInfo[] fileNames = dis.GetFiles();// RootDirectory.GetFiles("*.*");
        DateTime _now = DateTime.Now;
        var query = from x in fileNames
                    orderby x.CreationTime descending
                    select x;
        if (query.Count() == 0)
            return null;
        return query.First();
    }
    static PpcSite LoadCacheFromFile()
    {
       
            PpcSite ps;
            FileInfo fi = GetFileInfoLastDate();
            if (fi == null)
                return null;
            Stream stream = File.Open(fi.FullName, FileMode.Open);
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            ps = (PpcSite)bFormatter.Deserialize(stream);
            stream.Close();
            return ps;
        
        /*
        MongoDB.Driver.MongoCollection CacheColl = DBConnection.GetMongoCacheCollection();
        var sortBy = MongoDB.Driver.Builders.SortBy.Descending("DateModified");

        PpcSite ps = null;
    
        try
        {
            var result = CacheColl.FindAllAs<PpcSite>().SetSortOrder(sortBy).SetLimit(1);
            ps = result.FirstOrDefault();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        return ps;
         * */
    }
    private static string GetDateFileFormat()
    {
        DateTime dt = DateTime.Now;
        return dt.Day + "-" + dt.Month + "-" + dt.Year + "-";
    }
    public static UpdateCacheResponse _UpdateCache(Guid UserId)
    {
 //       System.Security.Principal.WindowsIdentity _Identity = System.Security.Principal.WindowsIdentity.GetCurrent();
//        dbug_log.ExceptionLog(new Exception("_Identity: " + _Identity.Name));
        UpdateCacheResponse ucr = new UpdateCacheResponse();
        
                string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
                XDocument xdoc = XDocument.Load(path);
                //     JavaScriptSerializer jss = new JavaScriptSerializer();

                CacheLog cl = new CacheLog();
                string UserName;
                if (!SalesUtility.TryGetPublisherUserName(UserId, PpcSite.GetCurrent().UrlWebReference, out UserName))
                {
                    ucr.SetStatus(eServerUpdate.UserNotFound);
                    PpcCacheException pce = new PpcCacheException(eServerUpdate.UserNotFound.ToString());
                    pce.NewValues = new Dictionary<string, string>();
                    pce.NewValues.Add("UserId", UserId.ToString());
                    cl.WriteCacheLog(pce);
                    return ucr;
                    //      return ucr;
                }

                //    cl.StartUpdateCache();


                //    StringBuilder sb = new StringBuilder();

         //       int _crm = 0;
         //       int _web = 0;

                /**** Set CRM Cache   ***/
                Parallel.ForEach(xdoc.Element("Sites").Element("SitePPC").Element("CrmServers").Elements("Server"),
                    new ParallelOptions { MaxDegreeOfParallelism = 3 },
                    xelem =>
                    {
                        //            _crm++;
                        XmlDocument soapEnvelopeXml = HttpHelper.CreateSoapEnvelope();
                        HttpWebRequest webRequest = HttpHelper.CreateWebRequest(xelem.Value, "http://tempuri.org/ClearCache");
                        webRequest.Timeout = 600000;
                        HttpHelper.InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);
                        try
                        {
                            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
                        }
                        catch (Exception exc)
                        {
                            //             ucr.CrmFailds++;
                            string mess = "CRM faild: " + xelem.Value;
                            //           sb.AppendLine(mess);
                            cl.WriteCacheLog(exc, new string[] { mess }, true);
                        }
                    });

                /**** Set Web Cache   ***/
                Parallel.ForEach(xdoc.Element("Sites").Element("SitePPC").Element("WebServers").Elements("Server"),
                    new ParallelOptions { MaxDegreeOfParallelism = 3 },
                    xelem =>{
           //     foreach (XElement xelem in xdoc.Element("Sites").Element("SitePPC").Element("WebServers").Elements("Server"))
        //        {
                    //          _web++;
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(xelem.Value + "UpdateSingleServerCache?UserId=" + UserId.ToString() + "&UserName=" + UserName);
                    request.Timeout = 600000;
                    try
                    {
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    }
                    catch (Exception exc)
                    {
                        //             ucr.WebServerFaild++;
                        dbug_log.ExceptionLog(exc);
                        string mess = "WebServer faild: " + xelem.Value;
                        //         sb.AppendLine(mess);
                        cl.WriteCacheLog(exc, new string[] { mess }, true);
                    }
          //      }
                   });
            

        ucr.SetStatus(eServerUpdate.UpdateInProcessing);
        return ucr;
      /*
        if (ucr.CrmFailds == 0 && ucr.WebServerFaild == 0)
        {
            ucr.SetStatus(eServerUpdate.Success);
            //   cl.UpdateSuccess();
            return ucr;
        }
        if (ucr.WebServerFaild == _web)
        {
            ucr.SetStatus(eServerUpdate.Faild);
            //       cl.UpdateFaild();
            return ucr;
        }
        ucr.SetStatus(eServerUpdate.PartialSuccess);
        //       cl.UpdateFaild();
        return ucr;
        */
    }
    #endregion

}
