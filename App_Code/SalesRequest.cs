﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;
using System.Web.Script.Serialization;
using System.Threading;

/// <summary>
/// Summary description for SalesRequest
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class SalesRequest : System.Web.Services.WebService {

    public SalesRequest () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public string CreateServiceRequest(string PhoneNum, string SupplierNum, string ZipCode, string description, string name, string keyword, string me)
    {
        ResponseRequestCall rrc = new ResponseRequestCall();
        MongoDB.Bson.ObjectId _id;
        if (!MongoDB.Bson.ObjectId.TryParse(me, out _id))
        {
            rrc.status = "WrongExposureId";
            SalesUtility.InsertServiceRequest(null, Guid.Empty, "WrongExposureId=" + me, false);
            return new JavaScriptSerializer().Serialize(rrc);
        }
        SliderData sd = SliderData.GetSliderData(_id);
        if (sd == null)
        {
            sd = new SliderData();
            sd.ExpertiseCode = PpcSite.GetCurrent().GetHeadingCodeByKeyword(keyword);
            sd.OriginId = PpcSite.GetCurrent().UnknoenOriginId;
            sd._id = _id;
        }
        int num;
        if (!int.TryParse(SupplierNum, out num))
            num = 3;
        rrc = SalesUtility.CreateServiceRequest(PhoneNum, num, ZipCode, description, name, keyword, sd, Utilities.GetIP(Context.Request));
        return new JavaScriptSerializer().Serialize(rrc);
       
    }

    [WebMethod]
    public string CreateServiceRequestNoProblem(string PhoneNum, string SupplierNum, string ZipCode, string description, string name, string HeadingCode, string flavor, string pageName)
    {
        PpcSite _cache = PpcSite.GetCurrent();
        SliderData sd = new SliderData();
        sd.ControlName = (eFlavour)Enum.Parse(typeof(eFlavour), flavor); 
        sd.Browser = Context.Request.Browser.Browser;
        sd.BrowserVersion = Context.Request.Browser.Version;
        sd.ClientDomain = Context.Request.Url.Host;
        sd.Country = "";
        sd.Domain = sd.ClientDomain;
        sd.ExpertiseCode = HeadingCode;
        sd.General = false;
        sd.OriginId = _cache.GeneralOrigin;
        sd.RegionCode = ZipCode;
        sd.SiteId = _cache.SiteId;
        sd.TimeZone = -5;
        sd.PageName = pageName;
        int num;
        if (!int.TryParse(SupplierNum, out num))
            num = 3;
        sd.MaxResults = num;
        sd.Url = Context.Request.UrlReferrer.PathAndQuery;
        ResponseRequestCall rrc = SalesUtility.CreateServiceRequest(PhoneNum, num, ZipCode, description, name, "", sd, Utilities.GetIP(Context.Request));
        return new JavaScriptSerializer().Serialize(rrc);        
    }
    [WebMethod(true)]
    public string GetSuppliersStatus(string SiteId, string IncidentId)
    {
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, SiteId);
        string xmlSupplier = "";
        try
        {
            xmlSupplier = _customer.GetSuppliersStatus(IncidentId);
        }
        catch (Exception exc)
        {
            // To Put Log //
            dbug_log.ExceptionLog(exc);
            return string.Empty;
        }       
        XDocument xdoc = XDocument.Parse(xmlSupplier);
        if(xdoc.Element("Suppliers")==null || xdoc.Element("Suppliers").Element("Error") != null)
            return string.Empty;
        if (true)//(Session["xmlSupplier"] == null || (string)Session["xmlSupplier"] != xmlSupplier)
        {
       //     dbug_log.WriteLog(xmlSupplier, IncidentId);
            Session["xmlSupplier"] = xmlSupplier;
            OnRequest or = new OnRequest();
            or.IncidentStatus = xdoc.Element("Suppliers").Attribute("IncidentStatus").Value;
            List<RequestSupplier> list = new List<RequestSupplier>();
            IEnumerable<XElement> querySucceed = from x in xdoc.Element("Suppliers").Elements()
                        where x.Attribute("Status").Value == "Succeed"
                        orderby x.Attribute("SupplierName").Value
                        select x;
            IEnumerable<XElement> queryInit = from x in xdoc.Element("Suppliers").Elements()
                                              where x.Attribute("Status").Value == "Initiated"
                                              orderby x.Attribute("SupplierName").Value
                                              select x;
            List<XElement> listXelement = new List<XElement>(querySucceed);
            listXelement.AddRange(queryInit);
          //  foreach (XElement elem in xdoc.Element("Suppliers").Elements("Supplier"))
            foreach(XElement elem in listXelement)
            {
           //     if(rs.Status != "
                RequestSupplier rs = new RequestSupplier();
                rs.NumberOfEmployees = elem.Attribute("NumberOfEmployees").Value;
                rs.ShortDescription = elem.Attribute("ShortDescription").Value;
                rs.Status = elem.Attribute("Status").Value;
                rs.SumAssistanceRequests = elem.Attribute("SumAssistanceRequests").Value;
                rs.SumSurvey = elem.Attribute("SumSurvey").Value;
                rs.SupplierId = elem.Attribute("SupplierId").Value;
                rs.SupplierName = elem.Attribute("SupplierName").Value;
                rs.SupplierNumber = elem.Attribute("SupplierNumber").Value;
                rs.Likes = elem.Attribute("Likes").Value;
                rs.Dislikes = elem.Attribute("Dislikes").Value;
                rs.City = elem.Attribute("City").Value;
                rs.StreetNumber = elem.Attribute("StreetNumber").Value;
                rs.Street = elem.Attribute("Street").Value;
                int _like, _dislike;
                if (!int.TryParse(rs.Likes, out _like))
                    _like = 0;
                if (!int.TryParse(rs.Dislikes, out _dislike))
                    _dislike = 0;
                rs.Reviews = (_like + _dislike).ToString();
                list.Add(rs);
            }
            or.Suppliers = list;
            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Serialize(or);
        }
        if (xdoc.Element("Suppliers").Attribute("IncidentStatus").Value == "Completed")
        {
            OnRequest or = new OnRequest();
            or.IncidentStatus = "Completed";
            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Serialize(or);
        }
        return string.Empty;
    }
    /*
    [WebMethod(true)]
    public string CreateServiceRequestLock(string PhoneNum, string SupplierNum, string ZipCode, string description, string ID, string me)
    {
        
        SliderData sd = SliderData.GetSliderData(me);
        ResponseRequestCall rrc = new ResponseRequestCall();

        PhoneNum = Utilities.GetCleanPhone(PhoneNum);
        rrc.plural = PpcSite.GetCurrent().GetHeadingDetails(sd.ExpertiseCode).PluralName;
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, sd.SiteId);
        _customer.Timeout = 300000;
        WebReferenceCustomer.ServiceRequest _request = new WebReferenceCustomer.ServiceRequest();
        _request.ContactPhoneNumber = PhoneNum;
        _request.ControlName = sd.ControlName.ToString();
        _request.Domain = sd.ClientDomain;
        _request.ExpertiseCode = sd.ExpertiseCode;
        _request.ExpertiseType = 1;
        _request.Keyword = sd.Keyword;
        int _num;
        if (!int.TryParse(SupplierNum, out _num))
            _num = 3;
        _request.NumOfSuppliers = _num;
        _request.OriginId = sd.OriginId;
        _request.PageName = sd.PageName;
        _request.PlaceInWebSite = sd.PlaceInWebSite;
        _request.PrefferedCallTime = DateTime.UtcNow;
        _request.RequestDescription = description;
        _request.RegionCode = ZipCode;
        _request.RegionLevel = 4;

        _request.SessionId = Session.SessionID;
        _request.SiteId = sd.SiteId;
        _request.Url = (Context.Session["client_url"] == null) ? ((Context.Request.UrlReferrer != null) ? Context.Request.UrlReferrer.AbsoluteUri : "") : (string)Context.Session["client_url"];
        RequestProcess rp = new RequestProcess(new Guid(ID), _request);
        rp.SetRequest(Session);
        WebReferenceCustomer.ServiceResponse _response = null;

        _response = rp.CreateServiceRequestWithZipcodeUSA(_customer);
        if(_response==null)
        {
            LogRequest(_request, null);
            return new JavaScriptSerializer().Serialize(rrc);
        }
        rrc.Tips = PpcSite.GetCurrent().GetTips(sd.ExpertiseCode);
        if (_response.Status == WebReferenceCustomer.StatusCode.Success)
        {
            rrc.status = "success";
            rrc.RequestId = _response.ServiceRequestId;
            LogEdit.SaveLog(sd.SiteId, "CreateServiceRequest status:" + _response.Status.ToString().ToLower() +
                "\r\nid: " + _response.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " +
                PhoneNum + "\r\nDescription: " + description +
                "\r\nServiceAreaCode: " + ZipCode +
                "\r\nExpertiseCode: " + sd.ExpertiseCode +
                "\r\nSession: " + Session.SessionID + "\r\nip: " + Utilities.GetIP(context.Request));
        }
        else if (_response.Status == WebReferenceCustomer.StatusCode.BadParametersReceived)
        {
            rrc.RequestId = "";
            rrc.status = "BadParametersReceived";
            string message = _response.Message.ToLower();
            if (message.Contains("zip code"))
                rrc.RequestId += "zipcode;";
            /*
            if (message.Contains("phone"))
                rrc.RequestId += "phone;";
            if (message.Contains("expertise"))
                rrc.RequestId += "expertise;";
             * * /
            if (string.IsNullOrEmpty(rrc.RequestId))
                rrc.status = "faild";
            LogRequest(_request, _response);
        }
        else
        {
            rrc.status = "faild";
            LogRequest(_request, _response);
        }
        return new JavaScriptSerializer().Serialize(rrc);

    }
    */
    /*
    [WebMethod(true)]
    public string GetRequestDone(string id, string ExpertiseCode)
    {
        Guid _id = new Guid(id);
        RequestProcess rp = RequestProcess.GetCurrent(Session, _id);
        for (int i = 0; i < 15 && rp == null; i++)
        {
            Thread.Sleep(3000);
            rp = RequestProcess.GetCurrent(Session, _id);
        }
        
        
        ResponseRequestCall rrc = new ResponseRequestCall();
        if (rp == null)
            rrc.status = "faild";
        else
        {
            PpcSite _cache = PpcSite.GetCurrent();
            switch (rp.Status)
            {
                case ("BadParametersReceived"):
                case ("faild"):
                    rrc.status = rp.Status;
                    break;
                case ("success"):
                    rrc.status = rp.Status;
                    rrc.Tips = _cache.GetTips(ExpertiseCode);
                    rrc.RequestId = rp.RequestId.ToString();
                    rrc.plural = _cache.GetHeadingDetails(ExpertiseCode).PluralName;
                    break;
            }
        }
        
        return new JavaScriptSerializer().Serialize(rrc);
         
    }
     * */
    [WebMethod(true)]
    public bool CheckZipCode(string Zipcode, string SiteId)
    {

        return SalesUtility.IfZipCodeExists(Zipcode, SiteId);
    }
    [WebMethod(true)]
    public string ClickLog(string me, string destination)
    {
        MongoDB.Bson.ObjectId _id;
        if (!MongoDB.Bson.ObjectId.TryParse(me, out _id))
            return string.Empty;
        SliderData sd = SliderData.GetSliderData(_id, Context);
        eSliderClickDestination escd;
        if (!Enum.TryParse(destination, out escd))
        {
            dbug_log.SliderLog(sd, Context, eSliderLog.Origin_Source);
            escd = eSliderClickDestination.NoProblem;
        }
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, sd.SiteId);
        WebReferenceCustomer.LogFlavorUsabilityRequest _request = new WebReferenceCustomer.LogFlavorUsabilityRequest();
        _request.Action = escd.ToString();
        _request.Flavor = sd.ControlName.ToString();
        WebReferenceCustomer.Result result;
        try
        {
            result = _customer.LogFlavorUsability(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, sd.SiteId);
        }
        return string.Empty;
    }
    /*
    [WebMethod(true)]
    public string SetSession(string ID, string RequestId, string status, bool IsBroker)
    {
        Guid r_id, id;
        if (!Guid.TryParse(RequestId, out r_id))
            r_id = Guid.Empty;
        if (!Guid.TryParse(ID, out id))
            return string.Empty;
        RequestProcess rp = new RequestProcess(id, r_id, status, IsBroker);
        rp.SetRequest(Session);
        return string.Empty;
    }
    */
    [WebMethod]
    public string CreateScheduledService(string phone, string date, string HeadingCode, string exposure_id)
    {
        ResponseRequestCall rrc = new ResponseRequestCall();
        MongoDB.Bson.ObjectId ExposureId;
        SliderData sd = null;
        PpcSite _cache = PpcSite.GetCurrent();
        if (!MongoDB.Bson.ObjectId.TryParse(exposure_id, out ExposureId))
            ExposureId = MongoDB.Bson.ObjectId.Empty;
        if (ExposureId != MongoDB.Bson.ObjectId.Empty)
            sd = SliderData.GetSliderData(ExposureId);
        if (sd == null)
        {
            sd = new SliderData();
            sd.ExpertiseCode = HeadingCode;
            sd.OriginId = _cache.UnknoenOriginId;
        }
        DateTime dt;
        if (date == "Now" || !DateTime.TryParse(date, out dt))
        {
            
            rrc = SalesUtility.CreateServiceRequest(Utilities.GetCleanPhone(phone), 3, sd.RegionCode,
                string.Empty, "*", sd.Keyword, sd, Utilities.GetIP(Context.Request));
            return new JavaScriptSerializer().Serialize(rrc);
        }
        
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(_cache.UrlWebReference);
        WebReferenceCustomer.ScheduledServiceRequest request = new WebReferenceCustomer.ScheduledServiceRequest();
            
        request.ControlName = sd.ControlName.ToString();
        request.Domain = sd.ClientDomain;
        request.ExpertiseCode = sd.ExpertiseCode;
        request.ExpertiseType = 1;
        request.Keyword = sd.Keyword;
        request.OriginId = sd.OriginId;
        request.PageName = sd.PageName;
        request.SessionId = string.Empty;
        request.SiteId = sd.SiteId;
        request.SiteTitle = sd.title;
        request.ToolbarId = sd.ToolbarId;
        request.Url = sd.Url;
        request.IP = Utilities.GetIP(Context.Request);
        request.RegionCode = sd.RegionCode;// SalesUtility.GetZipcodeByIP(request.IP, sd);
        request.NumOfSuppliers = 3;
        request.RegionLevel = 4;
        request.ContactPhoneNumber = Utilities.GetCleanPhone(phone);
        request.PrefferedCallTime = dt;
        request.Browser = sd.Browser;
        request.BrowserVersion = sd.BrowserVersion;
        request.ExposureId = ExposureId.ToString();
        
        

        WebReferenceCustomer.ResultOfScheduledServiceResponse result = null;
        try
        {
            result = customer.CreateScheduledServiceWithZipCodeUsa(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, phone);
            rrc.status = "faild";
            return new JavaScriptSerializer().Serialize(rrc);
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
        {
            rrc.status = "faild";
            return new JavaScriptSerializer().Serialize(rrc);
        }
        if (result.Value == null)
        {
            rrc.status = "success";
            return new JavaScriptSerializer().Serialize(rrc);
        }
        rrc.status = (result.Value.Status == WebReferenceCustomer.StatusCode.Success) ? "faild" : "success";
        return new JavaScriptSerializer().Serialize(rrc);
    }
    /*
    [WebMethod]
    public bool IsMonitizationOrigin(string origin)
    {
        PpcSite _cache = PpcSite.GetCurrent();
        Guid OriginId;
        if (!Guid.TryParse(origin, out OriginId))
            OriginId = _cache.UnknoenOriginId;        
        string country = SalesUtility.GetCountryName(Utilities.GetIP(context.Request), Context.Request.Url.DnsSafeHost);
        if (!_cache.MonetizationOriginInjection.ContainsKey(OriginId))
            return false;
        return _cache.MonetizationOriginInjection[OriginId].Contains(country);
    } 
     * */

   
}
