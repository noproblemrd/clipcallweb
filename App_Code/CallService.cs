﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using Telerik.Web.UI;
using System.Collections.Generic;

/// <summary>
/// Summary description for CallService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class CallService : System.Web.Services.WebService {

    public CallService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public RadComboBoxData GetItems(RadComboBoxContext context)
    {
        RadComboBoxData result = new RadComboBoxData();
     //   context.
        List<RadComboBoxItemData> items = new List<RadComboBoxItemData>();
        SiteSetting site_setting = (SiteSetting)Session["Site"];
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(site_setting.GetUrlWebReference);
        WebReferenceSite.GetRegardingObjectsRequest _request = new WebReferenceSite.GetRegardingObjectsRequest();
        _request.HelpDeskTypeId = (Guid)Session["HelpDeskTypeId"];
        _request.SearchInput = context.Text;
      //  Guid _id;
        try
        {
            _request.InitiatorId = new Guid((string)context["Consumer"]);
        }
        catch (Exception exc)
        {
            return result;
        }
        
        WebReferenceSite.ResultOfListOfRegardingObjectSearchData _result = null;
        try
        {
            _result = _site.GetRegardingObjects(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, site_setting);
            return result;
        }
        if (_result.Type == WebReferenceSite.eResultType.Failure)
            return result;

        foreach (WebReferenceSite.RegardingObjectSearchData rosd in _result.Value)
        {
            RadComboBoxItemData itemData = new RadComboBoxItemData();
            itemData.Text = rosd.Field2 + ", " + rosd.Field1 + ", " + rosd.Field3;
            itemData.Value = rosd.Id.ToString();
            items.Add(itemData);
        }
        result.Items = items.ToArray();
        return result;
    }

    [WebMethod(true)]
    public string GetOneItem(string _context, string Consumer)
    {
        string the_context = "";
        string[] contexts = _context.Split(',');
        if (contexts[0].Length > 0)
            the_context = contexts[0];
        else
        {
            if (contexts.Length < 2)
                return "";
            the_context = contexts[1];
        }                
        
        WebReferenceSite.GetRegardingObjectsRequest _request = new WebReferenceSite.GetRegardingObjectsRequest();
        _request.HelpDeskTypeId = (Guid)Session["HelpDeskTypeId"];
        _request.SearchInput = the_context;
        try
        {
            _request.InitiatorId = new Guid(Consumer);
        }
        catch (Exception exc)
        {
            return "";
        }
        return _GetOneItem(_request);
    }
    [WebMethod(true)]
    public string GetOneItemByGuid(string _guid)
    {        
        WebReferenceSite.GetRegardingObjectsRequest _request = new WebReferenceSite.GetRegardingObjectsRequest();
        _request.HelpDeskTypeId = (Guid)Session["HelpDeskTypeId"];
        _request.RegardingObjectId = new Guid(_guid);
        if (_request.RegardingObjectId == Guid.Empty)
            return "";
        return _GetOneItem(_request);
    }
    string _GetOneItem(WebReferenceSite.GetRegardingObjectsRequest _request)
    {
        SiteSetting site_setting = (SiteSetting)Session["Site"];
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(site_setting.GetUrlWebReference);        
        WebReferenceSite.ResultOfListOfRegardingObjectSearchData _result = null;
        try
        {
            _result = _site.GetRegardingObjects(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, site_setting);
            return "";
        }
        if (_result.Type == WebReferenceSite.eResultType.Failure)
            return "";

        if (_result.Value.Count() == 0)
            return "";
        return _result.Value[0].Field2 + ", " + _result.Value[0].Field1 + ", " + _result.Value[0].Field3 +
    //    return _result.Value[0].Field1 +
            ";;;;" + _result.Value[0].Id.ToString();
    }
}

