using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for LogEdit
/// </summary>
public class LogEdit
{
    static readonly string _path = System.Configuration.ConfigurationManager.AppSettings["Logs"];
	public LogEdit()
	{
		//
		
		//
	}

    public string checkFileSize(string fileName)
    {
        string fileSize = "";
        try
        {
            FileInfo f = new FileInfo(fileName);
            fileSize = Convert.ToString(f.Length);
            
        }
        catch (Exception ex)
        {
            fileSize = "0";
        };

        return fileSize;

    }

    public static void SaveLog(string SiteNameId, string details)
    {
        
        DateTime _date = DateTime.Now;
        string date = string.Format("{0:MM-dd-yyyy}", _date);
        string file_name = SiteNameId + "_" + date + ".log";
        
        if (!Directory.Exists(_path + SiteNameId))
            Directory.CreateDirectory(_path + SiteNameId);
        
        string newPath = _path + SiteNameId + @"\";
       
        string file_path = newPath + file_name;
        
        StringBuilder sb = new StringBuilder();

        sb.AppendLine("********* Create: " + "SiteId:" + SiteNameId  + " Date:" + _date.ToShortDateString() + " " + _date.ToLongTimeString() + " *********");
        sb.AppendLine();
        sb.AppendLine(details);
        sb.AppendLine("\r\n");
        try
        {
            using (FileStream fs = new FileStream(file_path, FileMode.Append))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) { }
    }
}
