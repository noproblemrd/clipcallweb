﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ResultSteps
/// </summary>
public class ResultSteps
{
    /*
        steps:
        VerifyYourEmail_1 = 1,
        ChoosePlan_2,
        BusinessDetails_General_3,
        BusinessDetails_Category_4,
        BusinessDetails_Area_5,
        Checkout_6,
        Dashboard_7
    */

    public string result { get; set; }
    public string supplierId { get; set; }
    public int step { get; set; }
    public string message { get; set; }

	public ResultSteps()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    
}