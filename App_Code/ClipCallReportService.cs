﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for ClipCallReportService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class ClipCallReportService : System.Web.Services.WebService {

    public ClipCallReportService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string ConfirmRejectIncident(Guid incidentId, bool toConfirm, int rejectReason)
    {
        WebReferenceReports.ReviewRejectConfirmRequest _request = new WebReferenceReports.ReviewRejectConfirmRequest();
        _request.incidentId = incidentId;
        _request.toConfirm = toConfirm;
   //     WebReferenceReports.ePortalReviewRejectReason rr;
  //      if (Enum.TryParse(rejectReason, out rr))
            _request.rejectReason = rejectReason;
        WebReferenceReports.ResultOfReviewRejectConfirmResponse _response = null;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(PpcSite.GetCurrent().UrlWebReference);
        JavaScriptSerializer jss = new JavaScriptSerializer();
        try
        {
            _response = _report.RejectConfirmReviewIncident(_request);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            var _res = new { isSuccess = false, message = "Server failed" };
            return jss.Serialize(_res);
        }
        if(_response.Type == WebReferenceReports.eResultType.Failure)
        {
            var _res = new { isSuccess = false, message = "Server failed" };
            return jss.Serialize(_res);
        }
        return jss.Serialize(new { isSuccess = _response.Value.IsSuccess, message = _response.Value.message.ToString() });

    }
    [WebMethod]
    public string SendAarToIncident(Guid incidentId, string supplierName, string phone)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceReports.ResultOfAddAarSupplierToLeadResponse _response = null;
        WebReferenceReports.AddAarSupplierToLeadRequest request = new WebReferenceReports.AddAarSupplierToLeadRequest();
        request.incidentId = incidentId;
        request.name = supplierName;
        request.phoneNumber = phone;
        JavaScriptSerializer jss = new JavaScriptSerializer();
        try
        {
            _response = _report.AddAarSupplierToLead(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            var _res = new { isSuccess = false, message = "Server failed" };
            return jss.Serialize(_res);
        }
        if (_response.Type == WebReferenceReports.eResultType.Failure)
        {
            var _res = new { isSuccess = false, message = "Server failed" };
            return jss.Serialize(_res);
        }
        return jss.Serialize(new { isSuccess = _response.Value.isSuccess, message = _response.Value.status.ToString().Replace("_", " ") });
    }
    [WebMethod]
    public string RelaunchIncident(Guid incidentId)
    {
        WebReferenceCustomer.CustomersService cs = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceCustomer.ResultOfRelaunchLeadResponse _response = null;
        JavaScriptSerializer jss = new JavaScriptSerializer();
        try
        {
            _response = cs.RelaunchIncidentByPublisher(incidentId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            var _res = new { isSuccess = false, message = "Server failed" };
            return jss.Serialize(_res);
        }
        if (_response.Type == WebReferenceCustomer.eResultType.Failure)
        {
            var _res = new { isSuccess = false, message = "Server failed" };
            return jss.Serialize(_res);
        }
        return jss.Serialize(new { isSuccess = _response.Value.IsSuccess, status = _response.Value.requestStatus.ToString() });
    }
 //{"incidentId": "' + _incidentId + '", "expertiseId": "' + ExpertiseCode + '", "regionName": "' + regionName +
 //               '", "regionLevel": ' + RegionLevel + ',"desc": "' + _desc + '", "numExper": ' +
  //                          _num + ', "name": "' + name + '", "email": "' + _email + '"}'
    [WebMethod]
    public string RelaunchCustomIncident(Guid incidentId, Guid expertiseId, string regionName,
        int regionLevel, string desc, int numExper, string name, string email)
    {
        ClipCallReport.ClipCallReport cc = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.RelaunchClipCallIncidentRequest _request = new ClipCallReport.RelaunchClipCallIncidentRequest();
        _request.desc = desc;
        _request.email = email;
        _request.expertiseId = expertiseId;
        _request.IncidentId = incidentId;
        _request.name = name;
        _request.numExper = numExper;
        _request.regionLevel = regionLevel;
        _request.regionName = regionName;
        ClipCallReport.ResultOfRelaunchClipCallIncidentResponse result = null;
        try
        {
            result = cc.RelaunchCustomIncident(_request);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return "failed";
        }
        if(result.Type == ClipCallReport.eResultType.Failure)
            return "failed";
        return result.Value.IsSuccess ? "success" : "failed";
    }
    [WebMethod]
    public string ExecuteManualAar(Guid incidentId)
    {
        ClipCallReport.ClipCallReport cc = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = cc.ExecuteManualAar(incidentId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return "failed";
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
            return "failed";
        return result.Value ? "success" : "failed";
    }
    
}
