﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for PageAdmin
/// </summary>
public class PageAdmin : System.Web.UI.Page
{
	public PageAdmin()
	{
		//
		
		//
	}
    protected void Page_PreInit(object sender, EventArgs e)
    {
        /*
        if (!IsPostBack)
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            if (!(host == "localhost" || host == "qa.ppcnp.com" || IsAdminV))
                Response.Redirect("LogInAdmin.aspx");
        }
         * */
    }
    bool IsAdminV
    {
        get { return (Session["IsAdmin"] == null) ? false : (bool)Session["IsAdmin"]; }        
    }
}
