﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Timers;
using System.Configuration;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for PhoneDidJob
/// </summary>
public class PhoneDidJob
{
    const int MINUTES_TO_WAIT = 1;
    const int MINUTES_TO_WAIT_SUPPLIER_AVAILABLE = 10;
//    System.Timers.Timer timer;
//    System.Timers.Timer timer_supplier_available;
    
 //   System.Threading.Timer timer_supplier_available;
    static object didLock = new object();
    static object supplierAvailableLock = new object();
 //   HttpApplication _application;
    static readonly string LogPath;
    static readonly string FileName;
    static PhoneDidJob()
    {
        LogPath = ConfigurationManager.AppSettings["Logs"] + "Services/";
        FileName = "PhoneDidJob_" + System.Net.Dns.GetHostName() + "_" + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName();
    }

    System.Threading.Timer timer;
    bool TimerIsRunning
    {
        get { return HttpRuntime.Cache["TimerRunning"] == null ? false : (bool)HttpRuntime.Cache["TimerRunning"]; }
        set { HttpRuntime.Cache["TimerRunning"] = value; }
    }
    public PhoneDidJob()
	{
     //   this._application = _application;
        
	}
    public void StartPhoneDidJobTimer()
    {
        dbug_log.MessageLog("StartTimer phone did");
        int duration = MINUTES_TO_WAIT * 60 * 1000;
        timer = new System.Threading.Timer(new System.Threading.TimerCallback(timer_Elapsed), this, 0, duration);
        dbug_log.MessageLog("End StartTimer StartPhoneDidJobTimer " + System.Net.Dns.GetHostName() + "_" + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName());

        /*
 //       if (!TimerIsRunning) // don't want multiple timers
 //       {
   //     dbug_log.MessageLog("StartTimer phone did");
        
            timer = new System.Timers.Timer(MINUTES_TO_WAIT * 60 * 1000);
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            timer.Start();
   //         dbug_log.MessageLog("StartTimer MINUTES_TO_WAIT_SUPPLIER_AVAILABLE");
            timer_supplier_available = new System.Timers.Timer(MINUTES_TO_WAIT_SUPPLIER_AVAILABLE * 60 * 1000);
            timer_supplier_available.AutoReset = true;
            timer_supplier_available.Enabled = true;
            timer_supplier_available.Elapsed += new ElapsedEventHandler(timer_Elapsed_supplier_available);
            timer_supplier_available.Start();
 //           dbug_log.MessageLog("End start Timer phone did");
    //        TimerIsRunning = true;
  //      }
         * */
    }
    public void StartSupplierAvailbleTimer()
    {
        int duration2 = MINUTES_TO_WAIT_SUPPLIER_AVAILABLE * 60 * 1000;
        timer = new System.Threading.Timer(new System.Threading.TimerCallback(timer_Elapsed_supplier_available), this, duration2, duration2);
        dbug_log.MessageLog("End StartTimer StartSupplierAvailbleTimer " + System.Net.Dns.GetHostName() + "_" + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName());
    }

    //void timer_Elapsed(object sender, ElapsedEventArgs e)
    void timer_Elapsed(object stateObj)
    {
        try
            {
        dbug_log.MessageLog("start job phone did");
        lock (didLock)
        {
            
            PhoneDidJob _this = (PhoneDidJob)stateObj;
            
                string _log = PpcSite.GetCurrent().DidPhoneJobUpdate();
                _this.WriteLog(_log);
            
        }
            }
        catch (Exception exc)
        {
         //   _this.WriteLog(exc.Message);
            dbug_log.ExceptionLog(exc);
        }
    }
   // void timer_Elapsed_supplier_available(object sender, ElapsedEventArgs e)
    void timer_Elapsed_supplier_available(object stateObj)
    {
        lock (supplierAvailableLock)
        {
            PhoneDidJob _this = (PhoneDidJob)stateObj;
            try
            {
                string _log = PpcSite.GetCurrent().UpdateSpecialSupplierAvailable();
                _this.WriteLog(_log);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
        }
    }
    public void WriteLog(string message)
    {
        
        try
        {
            using (FileStream fs = new FileStream(GetFullPath, FileMode.Append))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine("date: " + DateTime.Now.ToString());
                    tw.WriteLine("Host Name:" + System.Net.Dns.GetHostName());
                    tw.WriteLine("Site Name:" + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName());
                    tw.WriteLine("Message: " + message);
                    tw.WriteLine("Environment.StackTrace: " + Environment.StackTrace);
                }
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
    }
    string GetFullPath
    {
        get
        {
            DateTime dt = DateTime.Now;
            return LogPath + FileName + GetFixNumber(dt.Day) + GetFixNumber(dt.Month) + dt.Year + ".log";
        }
    }
    string GetFixNumber(int num)
    {
        return ((num < 10) ? "0" + num : num.ToString());
    }
    
}
/*
public class PhoneDidJobArgument
{
    public System.Threading.Timer timer { get; set; }
    public PhoneDidJob phoneDidJob { get; set; }
    public PhoneDidJobArgument(PhoneDidJob phoneDidJob)
    {
        this.phoneDidJob = phoneDidJob;
    }
}
*/