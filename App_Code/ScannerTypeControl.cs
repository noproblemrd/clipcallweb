﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ScannerTypeControl
/// </summary>
public class ScannerTypeControl<T> : ScannerControls where T : Control
{
    List<T> list;
    public ScannerTypeControl(Control control)
        : base(control)
	{
        list = new List<T>();
		//
		
		//
	}
    public void SetControl(Control ctr)
    {
        if (ctr.GetType() == typeof(T))
        {
            T _item = (T)ctr;
            list.Add(_item);
        }

    }
    public void StartTo()
    {

        myFunc func = new myFunc(SetControl);
        ApplyToControls(_control, func);
    }
    public List<T> GetControls()
    {
        return list;
    }

}