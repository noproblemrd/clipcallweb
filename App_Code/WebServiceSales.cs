﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for WebServiceSales
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WebServiceSales : System.Web.Services.WebService {

    public WebServiceSales () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    [WebMethod]
    public string[] GetHeadingList(string TagPrefix)
    {
        PpcSite _heading = PpcSite.GetCurrent();
        return _heading.GetHeadingByTagPerfix(TagPrefix);

    }
    [WebMethod]
    public string[] GetHeadingListIframe(string TagPrefix)
    {
        PpcSite _heading = PpcSite.GetCurrent();
        return _heading.GetHeadingByTagPerfix(TagPrefix, 4);

    }
    [WebMethod]
    public string[] GetHeadingListIframeStartWith(string TagPrefix)
    {
        PpcSite _heading = PpcSite.GetCurrent();
        return _heading.GetHeadingByTagPerfixStartWith(TagPrefix, 4);

    }

    [WebMethod]
    public string[] GetHeadingByTagPerfixStartWithAny(string TagPrefix)
    {
        PpcSite _heading = PpcSite.GetCurrent();
        return _heading.GetHeadingByTagPerfixStartWithAny(TagPrefix, 4);

    }

    [WebMethod]
    public string[] GetHeadingByTagPerfixStartWithAny2(string TagPrefix)
    {
        PpcSite _heading = PpcSite.GetCurrent();        

        string[] attornies = new string[]{
            "Personla Injury attorney",
            "DUI attorney",
            "Divorce attorney",
            "Bankruptcy attorney"
        };       

        string[] byHeading=_heading.GetHeadingByTagPerfixStartWithAny(TagPrefix, 4);

        if (byHeading.Length == 0) // can't find in headings look in keywords
        {
            string[] headingkewords = _heading.GetHeadingNamesByKeywordStartWith(TagPrefix, 4);
            if (headingkewords.Contains("undefined category"))
            {
                return headingkewords.Where(searchText => searchText != "undefined category").Concat(attornies).ToArray();
            }

            else
            {
                return headingkewords;
            }
        }
        else
            return byHeading.Where(searchText => searchText != "undefined category").ToArray();

        

    }

    [WebMethod]
    public bool Is_Heading(string name)
    {
        PpcSite head = PpcSite.GetCurrent();
        return head.IsHeadingByName(name);
    }
    [WebMethod]
    public string GetHeadingCodeByNameKeyword(string name)
    {
        PpcSite head = PpcSite.GetCurrent();
        return head.GetHeadingCodeByNameKeyword(name);
    }
    [WebMethod(EnableSession = true)]
    public string SendMobile(string Mobile)
    {
        Mobile = Utilities.GetCleanPhone(Mobile);
        if (SiteSettingV == null)
            return "error";
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        WebReferenceSupplier.SupplierSignUpRequest _request = new WebReferenceSupplier.SupplierSignUpRequest();
        WebReferenceSupplier.ResultOfSupplierSignUpResponse result = null;
        _request.MobilePhone = Mobile;
        try
        {
            result = _supplier.SupplierSignUpNew(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return "error";
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            return "error";
        }
        switch (result.Value.SignUpStatus)
        {
            case (WebReferenceSupplier.SupplierSignUpStatus.OkIsAar):               
            case (WebReferenceSupplier.SupplierSignUpStatus.OkIsNew):
                _user = new UserMangement(result.Value.SupplierId, SecurityLevel.PRE_SUPPLIER, "");
                Session["RegPhone"] = Mobile;
                return "true";
            case (WebReferenceSupplier.SupplierSignUpStatus.TransferToLogin):
                return "redirect";
        }
        //Never reach to this point
        return "error";
    }
    
    [WebMethod(true)]
    public string Login(string password)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        WebReferenceSupplier.ResultOfApprovePasswordResponse result = null;
        WebReferenceSupplier.ApprovePasswordRequest approvePasswordRequest = new WebReferenceSupplier.ApprovePasswordRequest();
        approvePasswordRequest.Password = password;
        if(Session["EmailRequestInvitation"] != null)
            approvePasswordRequest.Email = (string)Session["EmailRequestInvitation"];
        Guid _id = RegisterPage.GetSupplierId(HttpContext.Current);
        if (_id == Guid.Empty)
            return new JavaScriptSerializer().Serialize(new { status = "error" });
        approvePasswordRequest.SupplierId = _id;
        try
        {
            result = _supplier.ApprovePassword(approvePasswordRequest);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return new JavaScriptSerializer().Serialize(new{status="error"});
        }
        
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return new JavaScriptSerializer().Serialize(new{status="error"});
        string status = "";
    //     var _response=null;
        if (!result.Value.IsApproved)
        {            
            return new JavaScriptSerializer().Serialize(new { status = "false" });
        }
        else
        {
            Session["RegistrationPassword"] = password;
            //  _user.balance = (double)result.Value.Balance;   
            RegisterPage.SetDetailsAfterLogin(HttpContext.Current, (double)result.Value.Balance, password);
            status = "true";
            bool IsTrial = (result.Value.TrialStatusReasonCode == WebReferenceSupplier.TrialStatusReasonCode.Expired || result.Value.TrialStatusReasonCode == WebReferenceSupplier.TrialStatusReasonCode.InTrial);
            var _response = new { status = status, IsTrial = IsTrial.ToString().ToLower() };
            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Serialize(_response);
        }
    }
    
    [WebMethod(true)]
    public string AffiliateSignIn(string WebSite, string email)
    {
        return "";
    }
    [WebMethod(true)]
    public string SetService(string _service)
    {
        PpcSite _head = PpcSite.GetCurrent();
        Guid _id = _head.GetHeadinGuid(_service);
        if (_id == Guid.Empty)
            return "not exists";
        bool IsSuccess = false;
        try
        {
            IsSuccess = Utilities.SaveHeadings(RegisterPage.GetSupplierId(HttpContext.Current).ToString() , new string[] { _id.ToString() }, PpcSite.GetCurrent().UrlWebReference, ((UserMangement)Session["UserGuid"]).Get_Guid);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return "faild";
        }
        if (IsSuccess)
            return "success";
        return "faild";
    }
    [WebMethod(true)]
    public string MobileExists(string mobile)
    {
        mobile = Utilities.GetCleanPhone(mobile);
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        WebReferenceSupplier.ResultOfBoolean result = null;
        try
        {
            result = _supplier.IsAdvertiserExists(mobile);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return "faild";
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return "faild";
        return result.Value.ToString().ToLower();
    }
    [WebMethod(true)]
    public bool ToAvailability()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.MakeSupplierAvailable(_user.Get_Guid);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return false;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return false;

        return true;
    }
    [WebMethod(true)]
    public bool TurnOnRecordCalls()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.TurnOnRecordings(_user.Get_Guid);
            Session["ifEnableRecord"] = true;
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return false;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return false;
        return true;
    }

    [WebMethod(true)]
    public bool TurnOffRecordCalls()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(SiteSettingV.GetUrlWebReference);
        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.TurnOffRecordings(_user.Get_Guid);
            Session["ifEnableRecord"] = false;
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return false;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return false;
        return true;
    }


    [WebMethod(true)]
    public string CheckEmailInRegistrationRequest(string email)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfCheckEmailInRegistrationRequestResponse result;
        try
        {
            result = _supplier.CheckEmailInRegistrationRequest(email);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, PpcSite.GetCurrent().SiteId);
            return "faild";
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return "faild";
        if (result.Value.EmailCheckResult == WebReferenceSupplier.eEmailCheck.Continue)
            Session["EmailRequestInvitation"] = email;
        return result.Value.EmailCheckResult.ToString();
        /*
        WebReferenceSupplier.eEmailCheck.Continue
        WebReferenceSupplier.eEmailCheck.Login
         */

    }
    
    [WebMethod(true)]
    public string SupplierRequestInvitation(string phone, string email)
    {
        if (Session["EmailRequestInvitation"] == null || (string)Session["EmailRequestInvitation"] != email)
            return "faild";
        return RegisterSupplier(phone, email, string.Empty);
    }
    [WebMethod(true)]
    public string SupplierReferrerInvitation(string phone, string referrer)
    {        
        return RegisterSupplier(phone, string.Empty, referrer);
    }
    string RegisterSupplier(string phone, string email, string referrer)
    {
        phone = Utilities.GetCleanPhone(phone);
        Session["RegistrationPhone"] = phone;
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.SupplierSignUpRequest _request = new WebReferenceSupplier.SupplierSignUpRequest();
        _request.Email = email;
        _request.MobilePhone = phone;
        _request.ReferralCode = referrer;
        WebReferenceSupplier.ResultOfSupplierSignUpResponse result;
        try
        {
            result = _supplier.SupplierSignUpNew(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, PpcSite.GetCurrent().SiteId);
            return "faild";
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return "faild";

        if (result.Value.SignUpStatus == WebReferenceSupplier.SupplierSignUpStatus.OkIsAar ||
            result.Value.SignUpStatus == WebReferenceSupplier.SupplierSignUpStatus.OkIsNew)
        {
            bool HasReferral = !string.IsNullOrEmpty(referrer);
            eSupplierRegisterType _type = ((result.Value.SignUpStatus == WebReferenceSupplier.SupplierSignUpStatus.OkIsAar) ? eSupplierRegisterType.AAR : eSupplierRegisterType.New);
            RegisterPage.SetSupplier(HttpContext.Current, result.Value.SupplierId, _type, phone, email, HasReferral);

        }
        WebReferenceSupplier.SupplierSignUpStatus sss = (result.Value.SignUpStatus == WebReferenceSupplier.SupplierSignUpStatus.ReferralCodeAlreadyUsed) ? WebReferenceSupplier.SupplierSignUpStatus.ReferralCodeNotFound : result.Value.SignUpStatus;
        return sss.ToString();
        /*
        WebReferenceSupplier.SupplierSignUpStatus.EmailDuplicate;
        WebReferenceSupplier.SupplierSignUpStatus.OkIsAar;
        WebReferenceSupplier.SupplierSignUpStatus.OkIsNew;
        WebReferenceSupplier.SupplierSignUpStatus.ReferralCodeAlreadyUsed;
        WebReferenceSupplier.SupplierSignUpStatus.ReferralCodeNotFound;
        WebReferenceSupplier.SupplierSignUpStatus.TransferToLogin;
         * */
    }

    /*
    [WebMethod(true)]
    public string RegisterSupplierSocialNetworks(string email, string id, string name, string source, string referrer, string comeFrom , Guid registrationHitId)
    {

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.SocialMediaLogOnRequest socialMediaLogOnRequest = new WebReferenceSupplier.SocialMediaLogOnRequest();

        socialMediaLogOnRequest.Email = email;

        if (source == "facebook")
            socialMediaLogOnRequest.FacebookId = id;
        else if (source == "googlePlus")
            socialMediaLogOnRequest.GoogleId = id;
        
        Guid myGuid;

        if (!Guid.TryParse(referrer, out myGuid))
            myGuid = Guid.Empty;

        socialMediaLogOnRequest.ReferrerId = myGuid;
        socialMediaLogOnRequest.ContactName = name;
        socialMediaLogOnRequest.ComeFrom = comeFrom;
        socialMediaLogOnRequest.RegistrationHitId = registrationHitId;
        
        WebReferenceSupplier.ResultOfLogOnResponse logOnResponse = new WebReferenceSupplier.ResultOfLogOnResponse();
         
        try
        {
            logOnResponse = _supplier.SocialMediaLogOn_Registration2014(socialMediaLogOnRequest);

            string supplierId = logOnResponse.Value.SupplierId.ToString();
            int step= logOnResponse.Value.Step;
            string statusCode = logOnResponse.Value.StatusCode;
            
            /* status options
            OK = "OK";
            public const string WRONG_EMAIL_PASSWORD_COMBINATION = "WRONG_EMAIL_PASSWORD_COMBINATION";
            public const string ANOTHER_ACCOUNT_ALREADY_HAS_THIS_EMAIL = "ANOTHER_ACCOUNT_ALREADY_HAS_
            * /

            string result="";

            if (statusCode == "OK")
            {
                result = "ok";
                HttpContext.Current.Session["RegisterUser2014"] = new Guid(supplierId);               
                HttpContext.Current.Session["Username2014"] = name;
                HttpContext.Current.Session["Source2014"] = source;
                HttpContext.Current.Session["legacy"] = false;

                HttpCookie cookieAllredyProcess = new HttpCookie("npRegisterAllredyProcess");
                cookieAllredyProcess.Value = registrationHitId.ToString();
                cookieAllredyProcess.Expires = DateTime.Now.AddYears(10);
                HttpContext.Current.Response.Cookies.Add(cookieAllredyProcess);
                
            }
            else
            {
                result = "failed";
                dbug_log.ExceptionLog(new Exception("failed " + supplierId + " to register reason " + statusCode));
            }

            
            var resultParams = new { result = result, supplierId = supplierId, step = step, statusCode = statusCode };

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            return (jss.Serialize(resultParams));
        }

        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);

            var resultParams = new { result = "failed", supplierId = "", step = "" , statusCode = "server page or server problem" };

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            return (jss.Serialize(resultParams));
        }

        
        
    }
    */
    /*
    [WebMethod(true)]
    public string RegisterSupplierRegular(string email, string password, string referrer, string comeFrom, Guid registrationHitId)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.NormalLogOnRequest normalLogOnRequest = new WebReferenceSupplier.NormalLogOnRequest();

        Guid myGuidReferrer;

        if (!Guid.TryParse(referrer, out myGuidReferrer))
            myGuidReferrer = Guid.Empty;

        normalLogOnRequest.Email = email;
        normalLogOnRequest.Password=password;
        normalLogOnRequest.ReferrerId = myGuidReferrer;
        normalLogOnRequest.ComeFrom = comeFrom;
        normalLogOnRequest.RegistrationHitId = registrationHitId;
        //RegistrationHitId 

        WebReferenceSupplier.ResultOfLogOnResponse logOnResponse = new WebReferenceSupplier.ResultOfLogOnResponse();
        
        try
        {
            logOnResponse = _supplier.CreateUserNormal_Registration2014(normalLogOnRequest);

            string supplierId = logOnResponse.Value.SupplierId.ToString(); 
            int step = logOnResponse.Value.Step;
            string statusCode = logOnResponse.Value.StatusCode;
            
            /* status options
            OK = "OK";
            public const string WRONG_EMAIL_PASSWORD_COMBINATION = "WRONG_EMAIL_PASSWORD_COMBINATION";
            public const string ANOTHER_ACCOUNT_ALREADY_HAS_THIS_EMAIL = "ANOTHER_ACCOUNT_ALREADY_HAS_
            * /

            string result = "";

            if (statusCode == "OK")
            {
                result = "ok";
                HttpContext.Current.Session["RegisterUser2014"] = new Guid(supplierId);
                HttpContext.Current.Session["Username2014"] = email;
                HttpContext.Current.Session["Source2014"] = "website";
                HttpContext.Current.Session["legacy"] = false;

                HttpCookie cookieAllredyProcess = new HttpCookie("npRegisterAllredyProcess");
                cookieAllredyProcess.Value = registrationHitId.ToString();
                cookieAllredyProcess.Expires = DateTime.Now.AddYears(10);
                HttpContext.Current.Response.Cookies.Add(cookieAllredyProcess);
            }
            else
            {
                result = "failed";
                dbug_log.ExceptionLog(new Exception("failed " + supplierId + " to register reason " + statusCode));
                

                if(statusCode=="SEND_TO_LEGACY_PROCESS")
                    HttpContext.Current.Session["legacy"] = true;
            }

            var resultParams = new { result = result, supplierId = supplierId, step = step, statusCode = statusCode, email = email };

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            return (jss.Serialize(resultParams));
        }

        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);

            var resultParams = new { result = "failed", supplierId = "", step = "", statusCode = "server page or server problem" };

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            return (jss.Serialize(resultParams));
        }
        //CreateUserNormal_Registration2014
    }
    */
    [WebMethod(true)]
    public string LoginSupplierRegular(string email, string password)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.NormalLogOnRequest normalLogOnRequest = new WebReferenceSupplier.NormalLogOnRequest();
       
        normalLogOnRequest.Email = email;
        normalLogOnRequest.Password=password;              

        WebReferenceSupplier.ResultOfLogOnResponse logOnResponse = new WebReferenceSupplier.ResultOfLogOnResponse();


        try
        {
            logOnResponse = _supplier.LogInNormal_Registration2014(normalLogOnRequest);

            string supplierId = logOnResponse.Value.SupplierId.ToString();
            int step = logOnResponse.Value.Step;
            string statusCode = logOnResponse.Value.StatusCode;
            decimal balance = logOnResponse.Value.Balance;
            string copmanyName = logOnResponse.Value.Name;
            bool facebook = logOnResponse.Value.HasFacebook;
            bool googlePlus = logOnResponse.Value.HasGoogle;

            /* status options
            OK = "OK";
            public const string WRONG_EMAIL_PASSWORD_COMBINATION = "WRONG_EMAIL_PASSWORD_COMBINATION";
            public const string ANOTHER_ACCOUNT_ALREADY_HAS_THIS_EMAIL = "ANOTHER_ACCOUNT_ALREADY_HAS_
             SEND_TO_LEGACY_PROCESS
            */

            string result = "";

            if (statusCode == "OK")
            {
                result = "ok";
                
                HttpContext.Current.Session["RegisterUser2014"] = new Guid(supplierId);

                if (string.IsNullOrEmpty(copmanyName))
                    HttpContext.Current.Session["Username2014"] = email;
                else
                    HttpContext.Current.Session["Username2014"] = copmanyName;

                string source;
                source = "website";

                HttpContext.Current.Session["Source2014"] = source;
                HttpContext.Current.Session["Balance2014"] = balance;
                
                
                
            }
            else if (statusCode == "SEND_TO_LEGACY_PROCESS")
            {
                result = "failed";
                dbug_log.ExceptionLog(new Exception("legacy user logged in with email and password for supplierId:" + supplierId + " statusCode " + statusCode));

                HttpContext.Current.Session["RegisterUser2014"] = new Guid(supplierId);
                HttpContext.Current.Session["Username2014"] = copmanyName;
                HttpContext.Current.Session["Source2014"] = "website";
                HttpContext.Current.Session["Balance2014"] = balance;
            }

            else
            {
                result = "failed";
                dbug_log.ExceptionLog(new Exception("failed " + supplierId + " to login reason " + statusCode));
            }

            var resultParams = new { result = result, supplierId = supplierId, step = step, statusCode = statusCode, email = email };

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            return (jss.Serialize(resultParams));
        }

        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);

            var resultParams = new { result = "failed", supplierId = "", step = "", statusCode = "server page or server problem" };

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            return (jss.Serialize(resultParams));
        }
        //CreateUserNormal_Registration2014
    }

    public bool resendRegistrationEmail(Guid guidId)
    {

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        //public Result ResendEmailVerificationEmail_Registration2014(Guid supplierId)
        WebReferenceSupplier.ResultOfBoolean result= new WebReferenceSupplier.ResultOfBoolean();

        bool resendOk=true;

        try
        {
            result = _supplier.ResendEmailVerificationEmail_Registration2014(guidId);

            if (result.Type == WebReferenceSupplier.eResultType.Failure)
            {
                resendOk = false;
                dbug_log.ExceptionLog(new Exception("resendRegistrationEmail failed by result.Type guid:" + guidId.ToString()));
            }
            else if (result.Value == false)
            {
                resendOk = false;
                dbug_log.ExceptionLog(new Exception("resendRegistrationEmail failed by result.Value guid:" + guidId.ToString()));
            }
        }

        catch(Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            resendOk = false;
        }

        return resendOk;
    }
    
    public int verifyRegistrationEmail(Guid guidId)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfInt32 result = new WebReferenceSupplier.ResultOfInt32();

        int step;

        try
        {
            result = _supplier.EmailVerified_Registration2014(guidId);

            step = result.Value;

            if (step == -1)
            {
                dbug_log.ExceptionLog(new Exception("verifyRegistrationEmail failed by result.Value step -1 guid:" + guidId.ToString()));
            }

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            step = -2;
        }

        return step;
    }

    public WebReferenceSupplier.EmailVerifiedResponse verifyRegistrationEmailLegacy(Guid guidId)
    {
        //public Result<EmailVerifiedResponse> EmailVerified_Legacy2014(Guid supplierId)
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfEmailVerifiedResponse result = new WebReferenceSupplier.ResultOfEmailVerifiedResponse();
        WebReferenceSupplier.EmailVerifiedResponse emailVerifiedResponse = null;        

        try
        {
            result = _supplier.EmailVerified_Legacy2014(guidId);

            if(result.Type== WebReferenceSupplier.eResultType.Failure)
                dbug_log.ExceptionLog(new Exception("failed method verifyRegistrationEmailLegacy for supllier " + guidId.ToString() + " message: " + result.Messages[0]));
            else
                emailVerifiedResponse = result.Value;           

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            
        }

        return emailVerifiedResponse;
    }


    public string legacyRedirectMap(Guid supplierId, string stepLegacy, int step, string legacyStatus, string username2014, decimal balance2014)
    {

       /*
       public static class StepsLegacy
       {
           public const string CONFIRM_EMAIL_1 = "1_CONFIRM_EMAIL";
           public const string PAYMENT_METHOD_2 = "2_PAYMENT_METHOD";
           public const string FINISHED_3 = "3_FINISHED";
       }

       public static class LegacyStatuses
       {
           public const string SEND_TO_LEGACY_STEP = "SEND_TO_LEGACY_STEP";
           public const string SEND_TO_NORMAL_STEP = "SEND_TO_NORMAL_STEP";
           public const string PHONE_PASSWORD_COMBINATION_NOT_FOUND = "PHONE_PASSWORD_COMBINATION_NOT_FOUND";
           public const string MORE_THAN_ONE_ACCOUNT_FOUND_SEND_TO_EMAIL_LOGIN = "MORE_THAN_ONE_ACCOUNT_FOUND_SEND_TO_EMAIL_LOGIN";
           public const string NOT_LEGACY_SEND_TO_EMAIL_LOGIN = "NOT_LEGACY_SEND_TO_EMAIL_LOGIN";
           public const string LEGACY_FINISHED_SEND_TO_EMAIL_LOGIN = "LEGACY_FINISHED_SEND_TO_EMAIL_LOGIN";
       }
       */

        string redirectPath="";        

        if (legacyStatus == "PHONE_PASSWORD_COMBINATION_NOT_FOUND")
        {
            ////SetComment(HttpUtility.HtmlEncode(WrongPassword));
            
            ////return;
            redirectPath = "PHONE_PASSWORD_COMBINATION_NOT_FOUND";
        }

        else if (legacyStatus == "MORE_THAN_ONE_ACCOUNT_FOUND_SEND_TO_EMAIL_LOGIN")
        {
            dbug_log.ExceptionLog(new Exception("failed login for mobile supplier " + username2014 + " guid: " + supplierId.ToString() +
                " error: " + legacyStatus));

            redirectPath="ppcLogin.aspx";
        }

        else if (legacyStatus == "NOT_LEGACY_SEND_TO_EMAIL_LOGIN") // new suppliers plus old suppliers that not registered
        {
            dbug_log.ExceptionLog(new Exception("failed login for mobile supplier " + username2014 + " guid: " + supplierId.ToString() +
                " error: " + legacyStatus));

            redirectPath="ppcLogin.aspx?error=joinByEmail";
        }

        else if (legacyStatus == "LEGACY_FINISHED_SEND_TO_EMAIL_LOGIN")
        {
            dbug_log.ExceptionLog(new Exception("failed login for mobile supplier " + username2014 + " guid: " + supplierId.ToString() +
                " error: " + legacyStatus));

            redirectPath="ppcLogin.aspx?error=alreadyRegisteredByEmail";
        }

        else if (legacyStatus == "SEND_TO_LEGACY_STEP")
        {

            HttpContext.Current.Session["RegisterUser2014"] = supplierId;
            HttpContext.Current.Session["Username2014"] = username2014;
            HttpContext.Current.Session["Source2014"] = "website";
            HttpContext.Current.Session["Balance2014"] = balance2014;
            HttpContext.Current.Session["legacy"] = true;

            switch (stepLegacy)
            {
                case "1_CONFIRM_EMAIL":
                    redirectPath="verifyYourEmailManually.aspx";
                    break;

                case "2_PAYMENT_METHOD":
                    redirectPath="paymentLegacy.aspx";
                    break;

                case "3_FINISHED":
                    redirectPath="beforeEnterance.aspx";
                    break;

                default:
                    break;

            }

        }


        else if (legacyStatus == "SEND_TO_NORMAL_STEP")
        {

            HttpContext.Current.Session["RegisterUser2014"] = supplierId;
            HttpContext.Current.Session["Username2014"] = username2014;
            HttpContext.Current.Session["Source2014"] = "website";
            HttpContext.Current.Session["Balance2014"] = balance2014;
            HttpContext.Current.Session["legacy"] = true;

            /*
            1 = VerifyYourEmail
            2 = ChoosePlan
            3 = BusinessDetails_General
            4 = BusinessDetails_Category
            5 = BusinessDetails_Area
            6 = Checkout
            7 = Dashboard
            */

            switch (step)
            {

                case 1:
                    //Response.Redirect("ResendYourEmail.aspx?id=" + _response.Value.SupplierId.ToString() + "&email=" + _response.Value. );                       
                    break;

                case 2:
                    redirectPath="choosePlan.aspx";
                    break;

                case 3:
                    redirectPath="businessMainDetails.aspx";
                    break;

                case 4:
                    redirectPath="categories.aspx";
                    break;

                case 5:
                    redirectPath="location2.aspx";
                    break;

                case 6:
                    redirectPath="checkout.aspx";
                    break;

                case 7:
                    redirectPath="beforeEnterance.aspx";
                    break;

                default:
                    break;


            }

        }

        return redirectPath;
    }




    public ResultSteps ChoosePlan_Registration2014(Guid guidId, string planType)
    {
        //planType
        //  "FEATURED_LISTING";
        //  "LEAD_BOOSTER_AND_FEATURED_LISTING";

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfChoosePlanResponse resultOfChoosePlanResponse = new WebReferenceSupplier.ResultOfChoosePlanResponse();
        WebReferenceSupplier.ChoosePlanRequest choosePlanRequest = new WebReferenceSupplier.ChoosePlanRequest();

        choosePlanRequest.SupplierId = guidId;
        choosePlanRequest.PlanSelected = planType;

        ResultSteps resultSteps = new ResultSteps();

        try
        {
            resultOfChoosePlanResponse = _supplier.ChoosePlan_Registration2014(choosePlanRequest);

            if (resultOfChoosePlanResponse.Type == WebReferenceSupplier.eResultType.Failure)
            {
                resultSteps.result = "failed";
                resultSteps.supplierId = guidId.ToString();
                resultSteps.step = 2;
                resultSteps.message = resultOfChoosePlanResponse.Messages[0].ToString();                
            }

            else
            {
                resultSteps.result = "success";
                resultSteps.supplierId = guidId.ToString();
                resultSteps.step = resultOfChoosePlanResponse.Value.Step;
                resultSteps.message ="";                
            }

            
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);

            resultSteps.result = "failed";
            resultSteps.supplierId = guidId.ToString();
            resultSteps.step = 2;
            resultSteps.message = "server page or server problem";      

        }

        return resultSteps;
    }

    public ResultSteps businessDetails_Registration2014(Guid guidId, string businessName, string businessAddress, string mobile, decimal lat, decimal lot, string contactPersonName, string country, string state)
    {
        //planType
        //  "FEATURED_LISTING";
        //  "LEAD_BOOSTER_AND_FEATURED_LISTING";
        dbug_log.ExceptionLog(new Exception("business details parameters guid" + guidId.ToString() + " businessName " + businessName + " businessAddress:" + businessAddress + " mobile:" + mobile + " lat:" + lat + " lot:" + lot ));

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfChoosePlanResponse resultOfChoosePlanResponse = new WebReferenceSupplier.ResultOfChoosePlanResponse();

        WebReferenceSupplier.EnterBusinessDetailsRequest enterBusinessDetailsRequest = new WebReferenceSupplier.EnterBusinessDetailsRequest();
        enterBusinessDetailsRequest.SupplierId = guidId;
        enterBusinessDetailsRequest.Name = businessName;
        enterBusinessDetailsRequest.Address = businessAddress;        
        enterBusinessDetailsRequest.Phone = mobile;
        enterBusinessDetailsRequest.Latitude = lat;
        enterBusinessDetailsRequest.Longitude = lot;
        enterBusinessDetailsRequest.ContactPersonName=contactPersonName;
        enterBusinessDetailsRequest.Country=country;
        enterBusinessDetailsRequest.State = state;
        WebReferenceSupplier.ResultOfEnterBusinessDetailsResponse resultOfEnterBusinessDetailsResponse = new WebReferenceSupplier.ResultOfEnterBusinessDetailsResponse();
        ResultSteps resultSteps = new ResultSteps();

        try
        {
            resultOfEnterBusinessDetailsResponse=_supplier.EnterBusinessDetails_Registration2014(enterBusinessDetailsRequest);

            if (resultOfEnterBusinessDetailsResponse.Type == WebReferenceSupplier.eResultType.Failure)
            {
                resultSteps.result = "failed";
                resultSteps.supplierId = guidId.ToString();
                resultSteps.step = 2;
                resultSteps.message = resultOfEnterBusinessDetailsResponse.Messages[0].ToString();

                dbug_log.ExceptionLog(new Exception("failed to update business details " + guidId.ToString() + " " + resultSteps.message));
            }

            else
            {
                resultSteps.result = "success";
                resultSteps.supplierId = guidId.ToString();
                resultSteps.step = resultOfEnterBusinessDetailsResponse.Value.Step;
                resultSteps.message ="";                
            }
        }

        catch(Exception ex)
        {
            dbug_log.ExceptionLog(ex);

            resultSteps.result = "failed";
            resultSteps.supplierId = guidId.ToString();
            resultSteps.step = 2;
            resultSteps.message = ex.Message;
        }

        return resultSteps;
    }

    [WebMethod(true)]
    public string SetAvailability(SetWorkingHoursRequest workingHoursRequest)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.Result result= new WebReferenceSupplier.Result();
      
        WebReferenceSupplier.SetWorkingHoursRequest setWorkingHoursRequest = new WebReferenceSupplier.SetWorkingHoursRequest();
        setWorkingHoursRequest.SupplierId = workingHoursRequest.SupplierId;
        setWorkingHoursRequest.VacationFrom = workingHoursRequest.VacationFrom;
        setWorkingHoursRequest.VacationTo = workingHoursRequest.VacationTo;
        setWorkingHoursRequest.Units = workingHoursRequest.ToWebServiceUnitsArray();

        result = _supplier.SetWorkingHours_Registration2014(setWorkingHoursRequest);
        string status;
        try
        {
            if (result.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed SetAvailability " + result.Messages[0]));
                status = "failed";
            }

            else
                status = "success";

        }
        catch(Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            status = "failed";
        }


        var resultParams = new { status = status };

        System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

        return (jss.Serialize(resultParams));       

    }


    
    public string forgotPassword(string email)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfBoolean result = new WebReferenceSupplier.ResultOfBoolean();
        result=_supplier.SendResetEmail(email);
       
        string status;
        try
        {
            if (result.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed SendResetEmail for email: " + email  + " message: " + result.Messages[0]));
                status = "serverError";
            }

            else
            {
                if(result.Value)
                    status = "success";
                else
                    status = "failed";
            }

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            status = "serverError";
        }

        /*
        var resultParams = new { status = status };

        System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
        
        return (jss.Serialize(resultParams));
        */
        return status;
    }


     public string CheckResetPasswordToken(string token)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfBoolean result = new WebReferenceSupplier.ResultOfBoolean();
        result = _supplier.CheckResetPasswordToken(token);
       
        string status;
        try
        {
            if (result.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed CheckResetPasswordToken for token: " + token + " message: " + result.Messages[0]));
                status = "serverError";
            }

            else
            {
                if(result.Value)
                    status = "success";
                else
                    status = "failed";
            }

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            status = "serverError";
        }
      
        return status;
    }
   
    public string CheckResetPasswordToken(string password, string token)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
        result = _supplier.NewPasswordEntered(password,token);
       
        string status;
        try
        {
            if (result.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed CheckResetPasswordToken for password: " + password + " token: " + token + " message: "  + result.Messages[0]));
                status = "failed";
            }

            else
            {               
                status = "success";
            }

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            status = "serverError";
        }
      
        return status;
    }   



    [WebMethod(true)]
    public string ChangePhoneInRegistration()
    {
        Guid RegisterId = RegisterPage.GetSupplierId(HttpContext.Current);
        if (Guid.Empty == RegisterId)
            return "faild";
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.Result result;
        try
        {
            result = _supplier.ChangePhoneInRegistration(RegisterId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return "faild";
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return "faild";
        return "success";
    }
    [WebMethod(true)]
    public string SendSupplierSignUpNotification()
    {
        Guid RegisterId = RegisterPage.GetSupplierId(HttpContext.Current);
        if (Guid.Empty == RegisterId)
            return "faild";
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfBoolean result;
        try
        {
            result = _supplier.SendSupplierSignUpNotification(RegisterId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return "faild";
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return "faild";
        return result.Value.ToString().ToLower();
    }
    [WebMethod(true)]
    public string GetPasswordByPhone()
    {
        Guid RegisterId = RegisterPage.GetSupplierId(HttpContext.Current);
        if (Guid.Empty == RegisterId)
            return "faild";
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfBoolean result;
        try
        {
            result = _supplier.GetPasswordByPhone(RegisterId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, SiteSettingV);
            return "faild";
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return "faild";
        return result.Value.ToString().ToLower();
    }
    UserMangement _user
    {
        get { return (Session["UserGuid"] == null) ? new UserMangement() : (UserMangement)Session["UserGuid"]; }
        set { Session["UserGuid"] = value; }
    }
    SiteSetting SiteSettingV
    {
        get { return (Session["Site"] == null) ? null : (SiteSetting)Session["Site"]; }
        set { Session["Site"] = value; }
    }
   
}

