﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for WibiyaData
/// </summary>
[Serializable()]
public class WibiyaData
{
    public string token { get; set; }
    public string operation { get; set; }
    public string callback { get; set; }
    
	public WibiyaData(string token, string operation, string callback)
	{
        this.callback = callback;
        this.operation = operation;
        this.token = token;
	}
    public void SaveToSession()
    {
        data = this;
    }
    public static WibiyaData curren
    {
        get { return data; }
    }
    
    static WibiyaData data
    {
        get { return (HttpContext.Current.Session["WibiyaData"] == null) ? null : (WibiyaData)HttpContext.Current.Session["WibiyaData"]; }
        set { HttpContext.Current.Session["WibiyaData"] = value; }
    }
     
    
}
public enum eWibiyaLoginStatus
{
    AccountNotExist,
    LoginFaild,
    LoginSuccess
}