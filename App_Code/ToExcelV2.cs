﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using OfficeOpenXml;

/// <summary>
/// Summary description for ToExcelV2
/// </summary>
public class ToExcelV2
{
    string EXCEL_NAME;
    Page _page;

    public ToExcelV2(Page _page, string name)
    {
        this._page = _page;
        this.EXCEL_NAME = name;
    }

    public string CreateExcel(DataTable data)
    {
        if (data.Rows.Count == 0)
            return string.Empty;
        string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int rnd = new Random().Next();
        string filename = rnd + EXCEL_NAME + ".xlsx";
        path += filename;
        FileInfo newFile = new FileInfo(path);

        using (ExcelPackage package = new ExcelPackage(newFile))
        {
            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");
            List<KeyValuePair<string, string>> list = GetDataTable.GetColumnsNamesCaption(data);

            // worksheet.Column(1).StyleName
            for (int i = 0; i < list.Count; i++)
            {
                worksheet.Cells[1, (i + 1)].Value = list[i].Key;
            }
            int y = 2;
            foreach (DataRow row in data.Rows)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    worksheet.Cells[y, (i + 1)].Value = row[list[i].Key];
                }
                y++;
            }
            using (var range = worksheet.Cells[1, 1, 1, list.Count])
            {
                // Setting bold font
                range.Style.Font.Bold = true;
                // Setting fill type solid
           //     range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                // Setting background color dark blue
              //  range.Style.Fill.BackgroundColor.SetColor(Color.DarkBlue);
                // Setting font color
          //      range.Style.Font.Color.SetColor(Color.White);
            }
            //worksheet.Cells["A1:E4"].AutoFilter = true;
            using (var range = worksheet.Cells[1, 1, (y - 1), list.Count])//int FromRow, int FromCol, int ToRow, int ToCol]
            {
                range.AutoFilter = true;
            }
            package.Workbook.Properties.Title = "EXCEL_NAME";
            package.Workbook.Properties.Author = "Yoav Elizedek";
         //   package.Workbook.Properties.Comments = "This sample demonstrates how to create an Excel 2007 workbook using EPPlus";

            // set some extended property values
            package.Workbook.Properties.Company = "ClipCall Inc.";
            package.Save();
        }

     
        return filename;
    }
}