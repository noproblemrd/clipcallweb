﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for sliderParent
/// </summary>
public class sliderParent : SliderPage
{
    protected string SiteId = "";
    protected string Keyword = "";
    protected string KeywordOriginal = "";
    protected string ExpertiseCode = "";
    protected string expertiseCodeFromList = "";
    protected string ExpertiseLevel = "";

    protected string ExpertiseName = "";
    protected string ExpertiseAbbreviation = "";
    protected string ExpertiseAbbreviationUpper = "";
    protected string ExpertiseNameUpper = "";
    protected string ExpertiseNameLower = "";
    protected string ExpertiseNameShortLower = "";
    protected string ExpertiseNamePlural = "";
    protected string ExpertiseNamePluralUpper = "";
    protected string ExpertiseNamePluralLower = "";
    protected string ExpertiseNamePluralFirstLetterUpper = "";
    protected string msgIvrDefault = "";

    protected string RegionCode = "";
    protected string RegionLevel = "";
    protected string RegionName = "";
    protected string MaxResults = "";
    protected string root = "";

    protected string origionId = "";
    protected string regFormatPhone = "";
    protected string IfShowMinisite = "";
    protected string waterMarkComment = "";
    protected string waterMarkComment2 = "";
    protected string Domain = "";
    protected string Url = "";
    protected string Channel = "";
    protected string PageName = "";
    protected string PlaceInWebSite = "";
    protected string ControlName = "";
    protected string showNotConnected = "false";
    protected string noproblemDomain = "";
    protected string expertiseServiceList = "";
    protected bool ifJquery = false;
    protected string expertiseComment;
    protected string requestGuid;
    protected string clientDomain;
    

	public sliderParent()
	{
		//
		
		//
	}
   
    protected virtual void Page_Load(object sender, EventArgs e)
    {        
        
        if (!IsPostBack)
        {

            Response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

            root = ResolveUrl("~");
            noproblemDomain = Request.Url.Host;
            
            SiteId = Slider_Data.SiteId;            

            RegionCode = Slider_Data.RegionCode;
            RegionLevel = Slider_Data.RegionLevel;
            if (String.IsNullOrEmpty(RegionLevel))
                RegionLevel = "4";

            expertiseCodeFromList = Request["expertiseCodeFromList"];

            if (string.IsNullOrEmpty(expertiseCodeFromList))
                ExpertiseCode = Slider_Data.ExpertiseCode;
            else
                ExpertiseCode = Request["expertiseCodeFromList"];

            ExpertiseLevel = "1";
            
            HeadingCode headingCode = PpcSite.GetCurrent().GetHeadingDetails(ExpertiseCode);
            //Response.Write(headingCode.code);
            ExpertiseName = (string.IsNullOrEmpty(headingCode.SingularName)) ? headingCode.heading : headingCode.SingularName;
         
            if (string.IsNullOrEmpty(ExpertiseName))
                ExpertiseName = string.Empty;

            ExpertiseNameUpper = ExpertiseName.ToUpper();
            ExpertiseNameLower = ExpertiseName.ToLower();
            
            ExpertiseNamePlural = headingCode.Title;
            ExpertiseNamePluralUpper = ExpertiseNamePlural.ToUpper();
            ExpertiseNamePluralLower = ExpertiseNamePlural.ToLower();
            ExpertiseNamePluralFirstLetterUpper = char.ToUpper(ExpertiseNamePluralLower[0]) + ExpertiseNamePluralLower.Substring(1);

            ExpertiseAbbreviation = headingCode.SpeakToButtonName;
            ExpertiseAbbreviationUpper = ExpertiseAbbreviation.ToUpper();

            if (Utilities.ifVowel(ExpertiseNameLower.Substring(0, 1)))
                msgIvrDefault = "I need an " + ExpertiseNameLower;
            else
                msgIvrDefault = "I need a " + ExpertiseNameLower;


            waterMarkComment = headingCode.WaterMark;
            expertiseComment = headingCode.Comment;            
            expertiseComment = expertiseComment.Replace("'", "\\'");
            Keyword = Slider_Data.Keyword;

            /*
            Dictionary<string, string> dic;

            if (String.IsNullOrEmpty(Keyword))
                Keyword = "";

            if (Keyword.Length > 0 && Keyword!="general")
            {                
                dic = DBConnection.GetExpertiseDetails(Keyword);

                if (!String.IsNullOrEmpty(dic["code"]))
                    ExpertiseCode = dic["code"];
                else
                    ExpertiseCode = "3453454534";

                if (!String.IsNullOrEmpty(dic["expertisePlural"]))
                {
                    ExpertiseName = dic["expertisePlural"];
                    ExpertiseNameUpper = ExpertiseName.ToUpper();
                    ExpertiseNameLower = ExpertiseName.ToLower();
                }

                if (!String.IsNullOrEmpty(dic["expertiseSingular"]))
                {
                    ExpertiseNameShortLower = dic["expertiseSingular"];
                    
                }

                
                waterMarkComment = dic["watermark"];
            }
            
            else if (string.IsNullOrEmpty(ExpertiseCode))
            {
                ExpertiseCode = "3453454539";
            }

            else // ExpertiseCode is positive. there is no keyword
            {
                dic = DBConnection.GetExpertiseDetails3(ExpertiseCode); //this will bring more accurate than the parameter recieved ExpertiseName
                if (!String.IsNullOrEmpty(dic["expertisePlural"]))
                {
                    ExpertiseName = dic["expertisePlural"];
                    ExpertiseNameUpper = ExpertiseName.ToUpper();
                    ExpertiseNameLower = ExpertiseName.ToLower();
                    
                }
               
                if (!String.IsNullOrEmpty(dic["expertiseSingular"]))
                {
                    ExpertiseNameShortLower = dic["expertiseSingular"];
                    
                }

                if (!String.IsNullOrEmpty(dic["watermark"]))
                {
                    waterMarkComment = dic["watermark"];
                }

                if (!String.IsNullOrEmpty(KeywordOriginal))
                    Keyword = KeywordOriginal;

            }
            */

            //SendGeneralMessageToParent(ExpertiseNamePluralUpper, eMessageToParent.Title);
           

            MaxResults = Convert.ToString(Slider_Data.MaxResults);
            origionId = Slider_Data.OriginId.ToString();
            showNotConnected = Request.QueryString["showNotConnected"];

            Domain = Slider_Data.Domain;
            if (String.IsNullOrEmpty(Domain))
            {
                Domain = Request.Url.Host;
                //Domain = Utilities.GetIP(Request); // the ip of the client
            }

            clientDomain = Slider_Data.ClientDomain;

            regFormatPhone = PpcSite.GetCurrent().PhoneReg;// DBConnection.GetPhoneRegularExpression(SiteId); // for usa ^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$ 
            regFormatPhone = regFormatPhone.Replace("\\", "\\\\");


         //   if (Slider_Data.Country.ToLower() != "united states" && Slider_Data.Country.ToLower() != "israel" 
       //         && Domain != "127.0.0.1" && Domain != "localhost" && Domain != "qa.ppcnp.com")
            if(string.IsNullOrEmpty(Slider_Data.Country) ||
                (Slider_Data.Country.ToLower() != "united states" && Slider_Data.Country.ToLower() != "israel" 
                && Domain != "127.0.0.1" && Domain != "localhost" && Domain != "qa.ppcnp.com"))
                regFormatPhone = ""; // can do amything

            /*

            Url = string.IsNullOrEmpty(Slider_Data.Url) ? string.Empty : Slider_Data.Url;

            if (Url.Length > 300)
                Url = Url.Substring(0, 300);
                
            else
                Url = Url.Substring(0, Url.Length);
                 * * /

            //Url = "A.ASPX?<GDFGF>";
            //Response.Write("Url:" + Url);

            if (String.IsNullOrEmpty(Url))
                Url = Request.Url.AbsoluteUri;
            */
            Channel = "";

            PageName = Slider_Data.PageName;
            PlaceInWebSite = Slider_Data.PlaceInWebSite;
            ControlName = Slider_Data.ControlName.ToString();
            

            string headingCodeStr;
            headingCodeStr = headingCode.heading.ToLower();

            requestGuid = Guid.NewGuid().ToString();
            /*******************

            if (ControlName.ToLower() == "flavor_regular_yellowpages")
            {
                btnCall.Attributes.Add("style", "background-image:url('images/buttonblue.png');");
                form1.Attributes.Add("style", "font-family:Arial,Helvetica,sans-serif");
            }
            else if (ControlName.ToLower() == "flavor_regular_bing")
            {
                btnCall.Attributes.Add("style", "background-image:url('images/buttonblue2.png');color:white;");
                form1.Attributes.Add("style", "font-family:small Arial,Helvetica,Sans-Serif");
            }
            else if (ControlName.ToLower() == "flavor_regular_yahoo")
            {
                btnCall.Attributes.Add("style", "background-image:url('images/buttonyellow.png');color:black;");
                form1.Attributes.Add("style", "font-family:arial,helvetica,clean,sans-serif");
            }
            else if (ControlName.ToLower() == "flavor_regular_yellowbook")
            {
                btnCall.Attributes.Add("style", "background-image:url('images/buttonGray.png');color:white;");
                form1.Attributes.Add("style", "font-family:Arial,Helvetica,Sans-Serif");
            }

            //if (headingCodeStr.IndexOf("attorney") != -1)
            if (ControlName.ToLower() == "flavor_attorney_2_steps")
            {
                ifJquery = true;
                TextBox tbZipCode = new TextBox();
                tbZipCode.ID = "formZipCode";
                tbZipCode.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                tbZipCode.CssClass = "waterMarkZipCode2";
                tbZipCode.Attributes.Add("onfocus", "setCleaZipCode();");
                tbZipCode.Attributes.Add("onblur", "validEmpty(this.id);");
                tbZipCode.Text = HiddenWaterMarkZipCode.Value;
                //tbZipCode.
                //<asp:TextBox  ID="formZipCode" runat="server" CssClass="waterMarkZipCode" onfocus="setCleaZipCode();" onblur="validEmpty(this.id);"></asp:TextBox>
                PlaceHolderZipCode2.Controls.Add(tbZipCode);
                setJavaSciptSourceExpand();

                btnCall.Value = "SPEAK TO LAWYERS";
                btnCall.Attributes.Add("onclick", "stepExpertises()");

                WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null, SiteId);
                WebReferenceCustomer.ResultOfListOfExpertiseInSpecialCategoryContainer rsListExprtises = customer.GetSpecialCategoryGroup("attorneys");
                WebReferenceCustomer.ExpertiseInSpecialCategoryContainer[] objCategory = rsListExprtises.Value;

                //List li = new List();
                ArrayList listExpertises = new ArrayList();

                int indexCategory = 0;

                for (int i = 0; i < objCategory.Length; i++)
                {
                    expertiseServiceList += objCategory[i].Name.Replace("attorney", "").Trim() + "=" + objCategory[i].Code + "&";

                    if (objCategory[i].DisplayInSlider && indexCategory < 12)
                    {
                        indexCategory++;
                        listExpertises.Add(new expertises(objCategory[i].Name.Replace("attorney", "").Trim(), objCategory[i].Code));
                    }
                }

                if (expertiseServiceList.Length > 0)
                    expertiseServiceList = expertiseServiceList.Remove(expertiseServiceList.Length - 1);

                dataListExpertises.DataSource = listExpertises;
                dataListExpertises.DataBind();

            }

            else
            {
                btnCall.Value = "CALL ME NOW";
                btnCall.Attributes.Add("onclick", "makeCall(1,'call me now')");

                TextBox tbZipCode = new TextBox();
                tbZipCode.ID = "formZipCode";
                tbZipCode.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                tbZipCode.CssClass = "waterMarkZipCode";
                tbZipCode.Attributes.Add("onfocus", "setCleaZipCode();");
                tbZipCode.Attributes.Add("onblur", "validEmpty(this.id);");
                tbZipCode.Text = HiddenWaterMarkZipCode.Value;
                //<asp:TextBox  ID="formZipCode" runat="server" CssClass="waterMarkZipCode" onfocus="setCleaZipCode();" onblur="validEmpty(this.id);"></asp:TextBox>
                PlaceHolderZipCode.Controls.Add(tbZipCode);

                setJavaSciptSource();
            }

            ***********************/

           

            // Code By Yoav
            /*
            if (Slider_Data.General)
            {
                SendGeneralMessageToParent((PpcSite.GetCurrent().GetHeadingDetails(Slider_Data.ExpertiseCode)).PluralName, eMessageToParent.Title);
            }
            
            string IsWibiyaUrl = Request.QueryString["IsWibiyaUrl"];
            if (Session["WibiyaUser"] != null || !string.IsNullOrEmpty(IsWibiyaUrl))
            {
                WibiyaUser wu;
                if (Session["WibiyaUser"] == null)
                {
                    string _ClientDomain = IsWibiyaUrl;// Request.UrlReferrer.DnsSafeHost;
                  //  _ClientDomain = @"http://" + _ClientDomain;
                    wu = new WibiyaUser() { OriginId = new Guid(origionId), SiteId = SiteId, ClientDomain = _ClientDomain };
                }
                else
                    wu = (WibiyaUser)Session["WibiyaUser"];

       //         string script = wu.GetCrossDomainScript("resize");
                
      //           ClientScript.RegisterStartupScript(this.GetType(), "resizeiframe", script, true);                
            }
             * */
            //     this.SendGeneralMessageToParent(headingCode.PluralName
            // Code By Yoav


            ClientScript.RegisterClientScriptBlock(this.GetType(), "loadParams", "loadParams('" + ExpertiseNamePluralLower + "');", true);


        }
    }
}