using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Linq;

/// <summary>
/// Summary description for ConvertToDateTime
/// </summary>
public static class ConvertToDateTime
{
    const string DEFAULT_DATE_FORMAT = "{0:MM/dd/yyyy}";
    /*
	public ConvertToDateTime()
	{
		//
		
		//
	}
     * */
    public static double GetDateForClient(this DateTime dt)
    {
        return dt.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
    }
    /*
    public static DateTime StringToDateTime(string date)
    {        
        string[] str = date.Split('.', ' ', '/');
        return new DateTime(int.Parse(str[2]), int.Parse(str[1]), int.Parse(str[0]));
    }
     * */
    public static DateTime CalanderToDateTime(string date)
    {
        return CalanderToDateTime(date, DEFAULT_DATE_FORMAT);
        /*
        string[] str = date.Split('/', ' ');
        if (str.Length < 3)
            return DateTime.MinValue;
        if (str[2].Length != 4)
            return DateTime.MinValue;
        int temp;
        for(int i = 0; i < 3; i++)
            if(!int.TryParse(str[i], out temp))
                return DateTime.MinValue;
        return new DateTime(int.Parse(str[2]), int.Parse(str[0]), int.Parse(str[1]));
         * */
    }
    public static string CalanderToString(DateTime date)
    {
        
        return date.Month + "/" + date.Day + "/" + date.Year;
    }
     public static DateTime CalanderToDateTime(string date, string DateFormat)
    {
        if (string.IsNullOrEmpty(DateFormat))
            DateFormat = DEFAULT_DATE_FORMAT;
        string[] formats = (GetDateFormatDisplay(DateFormat)).Split('/');
        string[] str = date.Split('/', ' ');
        if (str.Length < 3)
            return DateTime.MinValue;
         if(str[2].Length != 4)
             return DateTime.MinValue;
         List<int> nums = new List<int>();
        for (int i = 0; i < 3; i++)
        {
            int temp;
            
            if (!int.TryParse(str[i], out temp) || !IsNumOk(temp, formats[i]))
                return DateTime.MinValue;
            nums.Add(temp);
        }
        DateTime _date;
       
        if (DateFormat == DEFAULT_DATE_FORMAT)
            _date = new DateTime(nums[2], nums[0], nums[1]);
        else
            _date = new DateTime(nums[2], nums[1], nums[0]);
    //    CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");
   //     _date = DateTime.Parse(_date.ToString(), ci);
        return _date;
         
    }
    
    private static bool IsNumOk(int num, string period)
    {
        if (period == "MM")
            return (num > 0 && num < 13);
        if (period == "dd")
            return (num > 0 && num < 32);
        if (period == "yyyy")
            return (num > 2005 && num < 2030);
        return false;
    }
    public static DateTime CRM_ToDateTime(string date)
    {
  //      date = date.Substring(0, 10);
        string[] str = date.Split('-', ' ', '/');
        return new DateTime(int.Parse(str[0]), int.Parse(str[1]), int.Parse(str[2]));
    }
    public static string GetCalenderFormat(string DateFormat)
    {
        DateFormat = DateFormat.Split(':')[1];
        return DateFormat.Substring(0, DateFormat.Length - 1);
    }
    public static DateTime GetDateTimeByTimeValue(string val)
    {
        string[] values = val.Split(':');
        return new DateTime(1900, 1, 1, int.Parse(values[0]), int.Parse(values[1]), 0);
    }
    public static string GetDateFormatDisplay(string DateFormat)
    {
       // {0:MM/dd/yyyy}
        DateFormat=DateFormat.Replace("}", "");
        return DateFormat.Split(':')[1];
    }
    
    public static double MilliTimeStampToJavascript(DateTime d2)
    {
        DateTime d1 = new DateTime(1970, 1, 1);
     //   DateTime d2 = DateTime.UtcNow;
        TimeSpan ts = new TimeSpan(d2.Ticks - d1.Ticks);

        return ts.TotalMilliseconds;
    }
    public static DateTime GetLocalFromUtc(DateTime utcTime, string localTimeZoneStandardName)
    {
        var timeZoneInfo = GetTimeZoneInfo(localTimeZoneStandardName);
        DateTime local = utcTime;
        local = DateTime.SpecifyKind(local, DateTimeKind.Unspecified);
        local = TimeZoneInfo.ConvertTimeFromUtc(local, timeZoneInfo);
        return local;
    }

    /// <summary>
    /// Converts the given datetime to UTC according to the given time zone name. This is fast (does not use Crm context).
    /// </summary>
    /// <param name="localTime"></param>
    /// <param name="localTimeZoneStandardName"></param>
    /// <returns></returns>
    public static DateTime GetUtcFromLocal(DateTime localTime, string localTimeZoneStandardName)
    {
        var timeZoneInfo = GetTimeZoneInfo(localTimeZoneStandardName);
        DateTime utcTime = localTime;
        utcTime = DateTime.SpecifyKind(utcTime, DateTimeKind.Unspecified);
        utcTime = TimeZoneInfo.ConvertTimeToUtc(utcTime, timeZoneInfo);
        return utcTime;
    }

    public static TimeZoneInfo GetTimeZoneInfo(string timeZoneStandardName)
    {
        TimeZoneInfo timeZoneInfo =
           (from info in TimeZoneInfo.GetSystemTimeZones()
            where string.Equals(info.StandardName, timeZoneStandardName, StringComparison.InvariantCultureIgnoreCase)
            select info).First();        
        return timeZoneInfo;
    }

     
}
