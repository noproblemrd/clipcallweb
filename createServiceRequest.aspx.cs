using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;

public partial class createServiceRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Guid MyGuid = new Guid(System.Configuration.ConfigurationManager.AppSettings["origionId"]); // dev test        
        //Response.Write("unsuccess");
        //Response.End();
        Server.ScriptTimeout = 300; // 5 minutes
        string ServiceAreaCode = Request["ServiceAreaCode"];
        int ServiceAreaType = Convert.ToInt32(Request["ServiceAreaType"]);
        string ExpertiseCode = Request["ExpertiseCode"];
        int ExpertiseType = Convert.ToInt32(Request["ExpertiseType"]);
        string ContactPhoneNumber = Request["ContactPhoneNumber"];
        string Description = Request["Description"];
        if (!String.IsNullOrEmpty(Description))
            Utilities.badCharacters(Request["Description"]);
        int NumOfSuppliers=Convert.ToInt32(Request["NumOfSuppliers"]);
        string SiteId = Request["SiteId"];
        string myGuid = Request["origionId"];
        string PageName = Request["PageName"];
        string PlaceInWebSite = Request["PlaceInWebSite"];
        string ControlName = Request["ControlName"];
        string Domain = Request["Domain"];
        string Url = Request["Url"];
        string Keyword = Request["Keyword"];

        LogEdit.SaveLog(SiteId, "Start:" + "\r\nsurfer phone: " +
            ContactPhoneNumber + "\r\nDescription: " + Description +
            "\r\nSession: " + Session.SessionID + "\r\nip: " + Utilities.GetIP(Request));

         //var params;
	        //params="PhoneNumber=" + phoneNumber + "&Description=" + escape(document.getElementById("form-msg").value)  + "&NumOfSuppliers=" + document.getElementById("form-suppliers").value;
        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(this, SiteId);
        WebReferenceCustomer.ServiceRequest serviceRequest = new WebReferenceCustomer.ServiceRequest();
        serviceRequest.RegionCode = ServiceAreaCode;
        //serviceRequest.ServiceAreaCode = 1;
        serviceRequest.RegionLevel = ServiceAreaType;
        //serviceRequest.ServiceAreaType = 3;
        serviceRequest.ExpertiseCode = ExpertiseCode;
        //serviceRequest.ExpertiseCode = 7;
        serviceRequest.ExpertiseType = ExpertiseType;
        //serviceRequest.ExpertiseType = 1;
        serviceRequest.ContactFullName = "";
        serviceRequest.ContactPhoneNumber = ContactPhoneNumber;
        //serviceRequest.ContactPhoneNumber = "0526135849";
        serviceRequest.RequestDescription = Description;
        //serviceRequest.RequestDescription = "some Text";
        serviceRequest.PrefferedCallTime = DateTime.Now;
        serviceRequest.SiteId = SiteId;
        serviceRequest.PageName = PageName;
        serviceRequest.PlaceInWebSite = PlaceInWebSite;
        serviceRequest.ControlName = ControlName;
        serviceRequest.Domain = Domain;
        serviceRequest.Url = Url;
        serviceRequest.Keyword = Keyword;
        
        //serviceRequest.SiteId = "0";
        serviceRequest.SessionId = Session.SessionID;


        Guid MyGuid = new Guid(myGuid); // production

        serviceRequest.OriginId = MyGuid;
       
        serviceRequest.NumOfSuppliers = NumOfSuppliers;
        //serviceRequest.NumOfSuppliers = 3;

        WebReferenceCustomer.ServiceResponse serviceResponse = new WebReferenceCustomer.ServiceResponse();
        serviceResponse=customer.CreateServiceRequest(serviceRequest);
        
        
        //Response.Write(serviceResponse.Status);
        //WebReferenceCustomer.StatusCode statusEnum=serviceResponse.Status;
        

        if (serviceResponse.Status.ToString().ToLower() == "success")
        {
            //Response.Write(serviceResponse.ServiceRequestId + " status:" + serviceResponse.Status.ToString().ToLower());
            Response.Write(serviceResponse.ServiceRequestId);


            LogEdit.SaveLog(SiteId, "CreateServiceRequest status:" + serviceResponse.Status.ToString().ToLower() +
                "\r\nid: " + serviceResponse.ServiceRequestId.ToString() +
                "\r\nsurfer phone: " +
                ContactPhoneNumber + "\r\nDescription: " + Description +
                "\r\nSession: " + Session.SessionID +
                "\r\nBrowser: " + Request.Browser.Browser + " MajorVersion: " +
                Request.Browser.MajorVersion + " MinorVersion: " +  Request.Browser.MinorVersion +
                "\r\nIsMobileDevice: " + Request.Browser.IsMobileDevice +
                "\r\nip: " + Utilities.GetIP(Request));

       }

       else
       {
            Response.Write("unsuccess");
            LogEdit.SaveLog(SiteId, serviceResponse.Status.ToString().ToLower() +
                 "\r\nid: " + serviceResponse.ServiceRequestId +
                "\r\nsurfer phone: " +
                ContactPhoneNumber + "\r\nDescription: " + Description +
                "\r\nBrowser: " + Request.Browser.Browser + " MajorVersion: " +
                Request.Browser.MajorVersion + " MinorVersion: " + Request.Browser.MinorVersion +
                "\r\nIsMobileDevice: " + Request.Browser.IsMobileDevice +
                "\r\nSession: " + Session.SessionID + "\r\nip: " +
                Utilities.GetIP(Request) + "\r\n Messages:" + serviceResponse.Message);
            
            if(serviceResponse.Status == WebReferenceCustomer.StatusCode.NoSuppliers)
            {

                //if (MyGuid.ToString().ToUpper() == "CF879946-455C-DF11-80CD-0003FF727321") // local dev
                if (MyGuid.ToString().ToUpper() == "69F8ABEA-95F9-E011-8CC5-001517D10F6E") // mars                
                {
                    //Response.Write(",no suppliers," + serviceResponse.ServiceRequestId);
                    /*                     
                   http://track.right-ads.com/tags_conv/?n=156&t=img&ta_creative_id=28496&ta_camp_id=4071&px_c=${URL}&li_id=${PageName}&vurlid=${PlaceInWebSite}&pub_id=${PUBLISHERID}&amount=${LEAD_PRICE}&commission=${LEAD_COMMISSION}&curency=USD&nconv_id=${LEAD_ID}&data=${LEAD_PHONE}&d_vchar_1=${LEAD_NAME}&d_vchar_2=${LEAD_IP}
                   */ 
                    
                    string strWebRquest = "http://track.right-ads.com/tags_conv/?n=156&t=img&ta_creative_id=28496&ta_camp_id=4071&px_c=" + Url +
                        "&li_id=" + PageName + "&vurlid=" + PlaceInWebSite +
                        "&pub_id=" + Domain + "&amount=0&commission=0&curency=USD&nconv_id=" + serviceResponse.ServiceRequestId + "&data=" +
                        ContactPhoneNumber + "&d_vchar_1=&d_vchar_2=" + Utilities.GetIP(Request);
                  

                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(strWebRquest);
                    
                    webRequest.Method = "POST";
                    
                    Stream webStream = webRequest.GetRequestStream();
                    webStream.Close();
                   

                    WebResponse webResponse = webRequest.GetResponse();
                    webStream = webResponse.GetResponseStream();

                    
                    TextReader reader = new StreamReader(webStream);
                    string strResponse = reader.ReadToEnd();                     

              
                }
            }
            
            
       }

       
        
    }
}
