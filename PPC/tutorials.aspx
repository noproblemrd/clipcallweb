﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tutorials.aspx.cs" Inherits="PPC_tutorials" MasterPageFile="~/Controls/MasterPagePPC2.master"%>
<%@ Register Src="~/Controls/menuVerticalFaq.ascx" TagName="menuVertical" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server"> 
    <title></title>
    <script type="text/javascript" src="../management/swfobject.js"></script>
    <script>
        /********************************** embed swf player **********************/
        

        function showVideo(videoSrc, containerVideoIframe, index) {

            $("#" + containerVideoIframe).show();
            $("#containerContent" + index).hide();

            var params = { allowScriptAccess: "always" };
            var atts = { id: "myytplayer" + index };

            // https://developers.google.com/youtube/js_api_reference

            var c = document.getElementById("ytapiplayer" + index);

            if (!c) {
                var d = document.createElement("div");
                d.setAttribute("id", "ytapiplayer" + index);
                document.getElementById(containerVideoIframe).appendChild(d);
            }

            /*
            swfobject.embedSWF("http://www.youtube.com/v/2lnZoIF2sVE?enablejsapi=1&playerapiid=ytplayer&version=3&autoplay=1&autohide=1&controls=1;",
            "ytapiplayer", "637", "359", "8", null, null, params, atts);
            */


            if (swfobject.hasFlashPlayerVersion("8")) {
                // has Flash
            }
            else {

                //if (!document.getElementById("commentDownloadFlash")) {

                if (document.getElementById("commentDownloadFlash" + index) == null)
                {
                    var commentDownloadFlash = document.createElement("div");


                    commentDownloadFlash.setAttribute("id", "commentDownloadFlash" + index);
                    //d.innerHTML = ""; 
                    commentDownloadFlash.innerHTML = "<br> You need to install Adobe Flash Player or check other solutions. See <a style='font-size:16px;' href='http://helpx.adobe.com/flash-player.html' target='_blank'>instructions.</a><br><br>";
                    document.getElementById("ytapiplayer" + index).appendChild(commentDownloadFlash);
                }

                //}
            }

            swfobject.embedSWF(videoSrc,
                    "ytapiplayer" + index, "571", "320.224", "8", null, null, params, atts);

            //alert(p);
            //http://www.youtube.com/embed/2lnZoIF2sVE?enablejsapi=1&playerapiid=ytplayer&version=3&autoplay=1 // see it's defferent from the formal whrer the link is



        }

        function closeVideo(containerVideoIframe,index) {

            $("#" + containerVideoIframe).hide();
            $("#containerContent" + index).show();
            
            swfobject.removeSWF('myytplayer' + index);

            var commentDownloadFlash = document.getElementById('commentDownloadFlash');
            if (commentDownloadFlash)
                document.getElementById("ytapiplayer").removeChild(commentDownloadFlash);
        }


        function onYouTubePlayerReady(playerId) {            
            ytplayer = document.getElementById("myytplayer");
            ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
        }

        function onytplayerStateChange(newState) {

            //Possible values are unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5)
            //_trackEvent(category, action, opt_label, opt_value, opt_noninteraction)
            var flavor;
            flavor = document.getElementById("hdn_flavor");
            if (flavor)
                flavor = document.getElementById("hdn_flavor").value;
            else
                flavor = "";

            if (newState == 1) // palying
            {
                _gaq.push(['_trackEvent', 'Join us: Video', 'playing', flavor]);
            }

            if (newState == 0) { // end
                _gaq.push(['_trackEvent', 'Join us: Video', 'end play', flavor]);
            }
        }

        /********************************** end embed swf player **********************/

        function showHideVideo2(videoSrc, containerVideoIframe, index) {
            //alert(videoSrc);
            //alert("showHideVideo" + $('#registrationContainerVideoIframe').attr('id') + $('#registrationContainerVideoIframe').is(":visible"));
            ////closeVideo();

            if ($("#" + containerVideoIframe).is(":visible")) {
                /*
                $('#registrationContainerVideoIframe').hide();

                $('#registrationContainerVideoIframe').css('display', 'none');

                $("#iframeVideo").attr({ src: "", height: "0px" });

                $(".seret").show();
                $(".registrationContainerVideo").show();

                $(".x-close").hide();

                $(".x-close").css('display', 'none');

                if (ifExplorer7()) {
                    $('.registration').css('height', '666px');
                }

                closeVideo();
                */
                showVideo(videoSrc, containerVideoIframe, index);
            }
            else {

                $("#" + containerVideoIframe).show();
                //$('#registrationContainerVideoIframe').hide();

                /*
                if (ifExplorer7()) {
                    $('#registrationContainerVideoIframe').css({ 'display': 'block', 'height': '500px' });
                    $('.registration').css('height', '1200px');

                }
                else {
                    $('#registrationContainerVideoIframe').css({ 'display': 'table-caption', 'height': '500px' });
                }

                //$("#iframeVideo").attr({ 'src': 'http://www.youtube.com/embed/97q0JRDvuik?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0', 'height': '485.761' });

                $(".seret").hide();
                $(".registrationContainerVideo").hide();
                $(".x-close").css('display', 'block');
                */

                showVideo(videoSrc, containerVideoIframe, index);
            }

        }

    </script>
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server"> 
  
     <uc1:menuVertical  ID="menuVertical" runat="server" whoActive="tutorials"/>
   
     
    <div class="faq dashboardIframe tutorial">
        <div class="titleFirst">
            <span>Video tutorials</span>
        </div>

        <div class="tutorialContent">

          <div class="tutorialItem">

               <div id="containerContent1">
                   <div class="tutorialVideo tutorialVideo1">                  
                       <div class="play" onclick="javascript:showVideo('http://www.youtube.com/v/qG_PVFk2NmQ?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1','containerVideoIframe1',1);"></div>
                   </div>
                   <div class="tutorialDesc">
                       <div class="tutorialDescTitle">Here's How NoProblem Works</div>
                       <div class="tutorialDescContent">
                           Watch this video to see how NoProblem
                           <br />
                           connects you in real-time with ready-to-spend
                           <br />
                           consumers in your area over the phone.
                       </div>
                   </div>
              </div>

              <div class="clear"></div> 

              <div id="containerVideoIframe1" class="containerVideoIframe">
                  <div>
                    <a class="x-close" href="javascript:closeVideo('containerVideoIframe1',1);" style="display: block;"></a>
                  </div>
              </div>

          </div>
          
          <div class="clear"></div> 
            
          <div class="tutorialItem">

              <div id="containerContent2">
               <div class="tutorialVideo tutorialVideo2">                  
                  <div class="play" onclick="javascript:showHideVideo2('http://www.youtube.com/v/ed5D4vcLuic?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1','containerVideoIframe2',2);"></div>
               </div>
               <div class="tutorialDesc">
                   <div class="tutorialDescTitle">Directory Listing Overview</div>
                   <div class="tutorialDescContent">
                      This video explains the benefits of your
                       <br />
                       Premium Listing at the <a id="linkBusiness" href="http://www.noproblem.me" runat="server" target="_blank">NoProblem Local 
                       <br />
                       Business Directory</a>.
                   </div>
               </div>
               </div> 

              <div class="clear"></div> 

              <div id="containerVideoIframe2" class="containerVideoIframe">
                  <div>
                    <a class="x-close" href="javascript:closeVideo('containerVideoIframe2',2);" style="display: block;"></a>
                  </div>
              </div>

          </div>
          
          <div class="clear"></div>  
         
          <div class="tutorialItem">

              <div id="containerContent3">
               <div class="tutorialVideo tutorialVideo3">
                    <div class="play" onclick="javascript:showHideVideo2('http://www.youtube.com/v/-e80MZ-lV4I?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1','containerVideoIframe3',3);"></div>
               </div>
               <div class="tutorialDesc">
                   <div class="tutorialDescTitle">Advertiser Portal Overview</div>
                   <div class="tutorialDescContent">
                      This video gives you an overview of your
                       <br />
                       Advertiser Portal and our lead history.
                   </div>
               </div>
                </div>

               <div class="clear"></div> 

              <div id="containerVideoIframe3" class="containerVideoIframe">
                  <div>
                    <a class="x-close" href="javascript:closeVideo('containerVideoIframe3',3);" style="display: block;"></a>
                  </div>
              </div>
          </div>
          
          <div class="clear"></div>             
          
          
         <div class="tutorialItem">

             <div id="containerContent4">
               <div class="tutorialVideo tutorialVideo4">
                  <div class="play" onclick="javascript:showHideVideo2('http://www.youtube.com/v/60AgAcrezwY?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1','containerVideoIframe4',4);"></div>
               </div>
               <div class="tutorialDesc">
                   <div class="tutorialDescTitle">My Business Details</div>
                   <div class="tutorialDescContent">
                      A tutorial on how to complete the Business
                       <br />
                      Details section in your Advertiser Portal. Edit 
                       <br />
                       your business profile, service category, cover
                       <br />
                       area and availability hours.
                   </div>
               </div>
             </div>
                
             <div class="clear"></div> 

              <div id="containerVideoIframe4" class="containerVideoIframe">
                  <div>
                    <a class="x-close" href="javascript:closeVideo('containerVideoIframe4',4);" style="display: block;"></a>
                  </div>

              </div>
          </div>  
            
        <div class="clear"></div>    
         
         <div class="tutorialItem">

             <div id="containerContent5">
               <div class="tutorialVideo tutorialVideo5">
                  <div class="play" onclick="javascript:showHideVideo2('http://www.youtube.com/v/bnF7mHQLp9M?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1','containerVideoIframe5',5);"></div>
               </div>
               <div class="tutorialDesc">
                   <div class="tutorialDescTitle">How to Set Your Default Price Per Call</div>
                   <div class="tutorialDescContent">
                      This is how you set and adjust your Default
                       <br />
                      Price Per Call in the Advertiser Portal.
                       <br />
                      Increasing your Default Price is one of several
                       <br />
                       ways to increase the number of high quality
                       <br />
                       phone leads you receive.
                   </div>
               </div>
             </div>
                
             <div class="clear"></div> 

              <div id="containerVideoIframe5" class="containerVideoIframe">
                  <div>
                    <a class="x-close" href="javascript:closeVideo('containerVideoIframe5',5);" style="display: block;"></a>
                  </div>

              </div>
          </div>  
        
        <div class="clear"></div> 

         <div class="tutorialItem">

             <div id="containerContent6">
               <div class="tutorialVideo tutorialVideo6">
                  <div class="play" onclick="javascript:showHideVideo2('http://www.youtube.com/v/AMNW8K0CopE?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1&enablejsapi=1&playerapiid=ytplayer&version=3&controls=1','containerVideoIframe6',6);"></div>
               </div>
               <div class="tutorialDesc">
                   <div class="tutorialDescTitle">How To Report a Lead</div>
                   <div class="tutorialDescContent">
                      We do our best to ensure you receive the
                       <br />
                      highest quality leads. Occasionally, a low 
                       <br />
                      quality lead may slip through our filters. This
                       <br />
                       video explains how to report a lead.
                       <br />
                       <b>
                       <span style="font-weight:600;">Remember: You <u>must</u> have call recording 
                       <br />
                        enabled to report a lead.</span>
                       </b>
                   </div>
               </div>
             </div>
                
             <div class="clear"></div> 

              <div id="containerVideoIframe6" class="containerVideoIframe">
                  <div>
                    <a class="x-close" href="javascript:closeVideo('containerVideoIframe6',6);" style="display: block;"></a>
                  </div>

              </div>
          </div>  

        </div>

    </div>
 </asp:Content>
