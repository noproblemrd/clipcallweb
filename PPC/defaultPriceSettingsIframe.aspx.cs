﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Web.Services.Protocols;
using System.Reflection;

public partial class PPC_defaultPriceSettingsIframe : PageSetting
{   

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(btn_SendService);
        if (!IsPostBack)
        {
            if (Request["source"]!=null)
                source=Request["source"];
            
            if (string.IsNullOrEmpty(GetGuidSetting()))
            {
                Response.Redirect("LogOut.aspx");
                return;
            }

            _GridView.GridLines = GridLines.Horizontal;

            LoadSymbolMoney();
            LoadDetail();
            //    LoadAvgPrice();
            LoadTextCallPurchas();
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
            ////RemoveServerCommentPPC();
        }
        Header.DataBind();
    }
    /*
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        int _inx = 0;
        foreach (GridViewRow row in _GridView.Rows)
        {
            UserControl ctl =
            (UserControl)row.FindControl("On_Off1");
            PropertyInfo pi = ctl.GetType().GetProperty("_InAuction");
            if (_inx == 0)
            {
            }
            bool InAuction = (bool)pi.GetValue(ctl, new object[]{});
            if (!InAuction)
                continue;
            MethodInfo mi = ctl.GetType().GetMethod("SetElements");
            mi.Invoke(ctl, new object[] { _inx });
            _inx++;
        }
    }
     * */
    private void LoadTextCallPurchas()
    {
        string _txt = DBConnection.GetTextCallPurchas(siteSetting.GetSiteID);
        if (string.IsNullOrEmpty(_txt))
        {
            ////div_LearnMore.Visible = false;
        }
        else
        {
            ////div_LearnMore.Visible = true;
            lbl_TextCallPurchas.Text = _txt;
        }
    }

    private void LoadSymbolMoney()
    {

        //      lbl_money.Text = siteSetting.CurrencySymbol;
        lbl_money.Text = siteSetting.CurrencySymbol;        
        //   lbl_money3.Text = siteSetting.CurrencySymbol;
    }

    void LoadDetail()
    {
        //     double currenBalance, minPrice, incidentPrice;
        //    decimal minPrice;
        //      bool isBid;
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.GetSupplierPricesRequest _request = new WebReferenceSupplier.GetSupplierPricesRequest();
        _request.SupplierId = new Guid(GetGuidSetting());
        WebReferenceSupplier.ResultOfGetSupplierPricesResponse result = null;
        try
        {
            result = supplier.GetSupplierPrices(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "parent.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "parent.UpdateFailed();", true);
            return;
        }
        int RegistrationStage = result.Value.StageInRegistration;
        //     BalanceDetailV = xdd;
        bool IsFirstTime = !((RegistrationStage) >= 5);
        // if (IsFirstTime)// xdd["SupplierPricingDetails"].Attributes["RegistrationStage"] != null)
        //  {
        //      string RegistrationStage = xdd["SupplierPricingDetails"].Attributes["RegistrationStage"].InnerText;
        IsFirstTimeV = IsFirstTime;
        string csnameStep = "setStep";
        Type cstypeStep = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager csStep = this.Page.ClientScript;

        if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
        {
            string csTextStep = "witchAlreadyStep(" + RegistrationStage + ");";
            csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
        }

        DataTable data = GetDataTable.GetDataTableFromListCorrectNumber(result.Value.Expertises);
        Dictionary<string, string> dicVal = new Dictionary<string, string>();

        foreach (DataRow row in data.Rows)
        {
            string expertise_name = row["ExpertiseName"].ToString();
            string _is_bid = row["IsBid"].ToString();
            string _price = row["Price"].ToString();
            dicVal.Add(expertise_name, _price);
            dicVal.Add(expertise_name + "-" + lbl_allowBidAudit.Text, _is_bid);
        }

        /*
        if (result.Value.RankingNotification == 0)
        {
            txt_Notify.Enabled = false;
            txt_Notify.Attributes.Add("readonly", "readonly");
            txt_Notify.CssClass = "notifytextoff";
            cb_Notify.Checked = false;
            dicVal.Add(cb_Notify.ID, "");
        }
        else
        {
            txt_Notify.Enabled = true;
            txt_Notify.Attributes.Remove("readonly");
            txt_Notify.CssClass = "notifytext";
            cb_Notify.Checked = true;
            txt_Notify.Text = result.Value.RankingNotification + "";
            dicVal.Add(cb_Notify.ID, result.Value.RankingNotification + "");
        }
        */


        dataV = data;
        

        _GridView.PageIndex = 0;
        SetGridView();
        dicValueV = dicVal;

    }
    void SetGridView()
    {
        DataTable data = dataV;
        _GridView.DataSource = data;
        _GridView.DataBind();

        if (dataV.Rows.Count > 0)
        {
            /*
            Image _image = (Image)_GridView.HeaderRow.FindControl("img_AllowBid");
            _image.Visible = _img_AllowBid.Visible;
            _image.AlternateText = _img_AllowBid.AlternateText;
            _image.Attributes.Add("title", _img_AllowBid.AlternateText);
            _img_AllowBid.Visible = false;
            */
        }

        foreach (GridViewRow row in _GridView.Rows)
        {
            //       HiddenField hf_rankingId = ((HiddenField)row.FindControl("hf_ranking"));
            string expertiseId = ((Label)row.FindControl("lbl_HeadingId")).Text;
            Label rankingId = ((Label)row.FindControl("lbl_ranking"));
            Label _minPrice = ((Label)row.FindControl("lbl_minPrice"));

            string _span = ((HtmlGenericControl)row.FindControl("span_minPrice")).ClientID;
            string _script = "GetRanking(this, '" + expertiseId + "', '" + GetGuidSetting() + "', '" + rankingId.ClientID + "', '" +
                _minPrice.Text + "', '" + _span + "');";
            TextBox _tb = (TextBox)row.FindControl("txt_MyPrice");
            _tb.Attributes.Add("onblur", _script);
            double _ranking;
            if (!double.TryParse(rankingId.Text, out _ranking))
                _ranking = 10;
            rankingId.CssClass = (_ranking < 4) ? "Lead _ranking" : "NotLead _ranking";
            RangeValidator _rv = (RangeValidator)row.FindControl("RangeValidator_minPrice");
            _rv.MaximumValue = int.MaxValue + "";
            /*
            UserControl ctl =
                (UserControl)row.FindControl("On_Off1");
            PropertyInfo pi = ctl.GetType().GetProperty("_InAuction");
            if (_inx == 0)
            {
            }
            bool InAuction = (bool)pi.GetValue(ctl, new object[] { });
            if (!InAuction)
                continue;
            MethodInfo mi = ctl.GetType().GetMethod("SetElements");
            mi.Invoke(ctl, new object[] { _inx });
            _inx++;
             * */

        }
        //to load max price that not in the GridView
        int _start = (_GridView.PageIndex * _GridView.PageSize);
        int _end = (_GridView.PageIndex + 1) * _GridView.PageSize;
        int max_value = 0;
        for (int i = 0; i < data.Rows.Count; i++)
        {
            if (i >= _start && i < _end)
                continue;
            string price = data.Rows[i]["Ranking"].ToString();
            if (string.IsNullOrEmpty(price))
                continue;
            int _price = int.Parse(price);
            if (_price > max_value)
                max_value = _price;
        }
        hf_HiddenValue.Value = max_value + "";
        //***************************************
        _UpdatePanelBid.Update();
    }

    void notValidPriceCall(string message)
    {
        string script = "alert('" + message + "');";
        ClientScript.RegisterStartupScript(this.GetType(), "notValidCall", script, true);
    }


    protected void btnSend_Click(object sender, EventArgs e)
    {
        _UpdatePanelBid.Update();
        decimal priceCall;

        string page_name = System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        //string page_translate = EditTranslateDB.GetPageNameTranslate(page_name, siteSetting.siteLangId);
        string page_translate = "My credits";

        UserMangement um = GetUserSetting();
        List<WebReferenceSupplier.AuditEntry> list = new List<WebReferenceSupplier.AuditEntry>();
        List<WebReferenceSupplier.ExpertiseContainer> listExp = new List<WebReferenceSupplier.ExpertiseContainer>();
        bool is_ok = true;
        bool IsFirstTime = IsFirstTimeV;


        int max_ranking_grid = 0;


        foreach (GridViewRow row in _GridView.Rows)
        {
            string _temp = ((TextBox)row.FindControl("txt_MyPrice")).Text;
            if (!decimal.TryParse(_temp, out priceCall))
            {
                notValidPriceCall(lbl_mesValidCallPrice.Text);
                return;
            }
            Decimal MinVlue = Decimal.Parse(((Label)row.FindControl("lbl_minPrice")).Text);
            HtmlGenericControl hgc = (HtmlGenericControl)row.FindControl("span_minPrice");
            if (MinVlue > priceCall)
            {
                hgc.Style["display"] = "inline";
                is_ok = false;
            }
            else
                hgc.Style["display"] = "none";
            string expertiseName = ((Label)row.FindControl("lbl_expertiseName")).Text;
            string HeadingId = (((Label)row.FindControl("lbl_HeadingId")).Text);
            WebServiceSite wss = new WebServiceSite();
            string _ranking = wss.GetExpertiseRanking(HeadingId, _temp, GetGuidSetting());
            ((Label)row.FindControl("lbl_ranking")).Text = _ranking;

            Guid itemId = new Guid(((Label)row.FindControl("lbl_ItemId")).Text);

            /*
            UserControl ctl =
            (UserControl)row.FindControl("On_Off1");
            PropertyInfo _isbid = ctl.GetType().GetProperty("_ON");
            bool IsBid = (bool)(_isbid.GetValue(ctl, null));
            _isbid.SetValue(ctl, IsBid, null);
            */

            bool IsBid=true;

            //    string _min_price = ((HiddenField)row.FindControl("hf_ranking")).Value;

            if (string.Format("{0:0.##}", priceCall) != dicValueV[expertiseName] || IsFirstTime || source == "dashboardHome")
            {
                WebReferenceSupplier.AuditEntry ae = new WebReferenceSupplier.AuditEntry();
                ae.AdvertiseId = new Guid(um.GetGuid);
                ae.AdvertiserName = um.User_Name;
                ae.AuditStatus = (IsFirstTime) ? WebReferenceSupplier.Status.Insert : WebReferenceSupplier.Status.Update;
                ae.FieldId = lbl_heading.ID;
                ae.FieldName = expertiseName;
                ae.NewValue = string.Format("{0:0.##}", priceCall);
                ae.OldValue = (IsFirstTime) ? string.Empty : dicValueV[expertiseName];
                ae.PageId = page_name;
                ae.UserId = new Guid(userManagement.GetGuid);
                ae.UserName = userManagement.User_Name;
                ae.PageName = page_translate;
                list.Add(ae);
            }

            /*
            if (IsBid.ToString() != dicValueV[expertiseName + "-" + lbl_allowBidAudit.Text] || IsFirstTime)
            {
                WebReferenceSupplier.AuditEntry ae = new WebReferenceSupplier.AuditEntry();
                ae.AdvertiseId = new Guid(um.GetGuid);
                ae.AdvertiserName = um.User_Name;
                ae.AuditStatus = (IsFirstTime) ? WebReferenceSupplier.Status.Insert : WebReferenceSupplier.Status.Update;
                ae.FieldId = lbl_allowBidAudit.ID;
                ae.FieldName = expertiseName + "-" + lbl_allowBidAudit.Text;
                ////ae.NewValue = IsBid.ToString();
                ae.NewValue = true.ToString();
                ae.OldValue = (IsFirstTime) ? string.Empty : dicValueV[expertiseName + "-" + lbl_allowBidAudit.Text];
                ae.PageId = page_name;
                ae.UserId = new Guid(userManagement.GetGuid);
                ae.UserName = userManagement.User_Name;
                ae.PageName = page_translate;
                list.Add(ae);
            }
            */


            WebReferenceSupplier.ExpertiseContainer ec = new WebReferenceSupplier.ExpertiseContainer();
            ec.Id = itemId;
            ////ec.IsBid = IsBid;
            ec.IsBid = IsBid;
            ec.Price = priceCall;
            listExp.Add(ec);

            //ranking check

            /*
            if (cb_Notify.Checked)
            {
                //          WebServiceSite _site = new WebServiceSite();

                //            string HeadingId = ((Label)row.FindControl("lbl_HeadingId")).Text;
                int __ranking = int.Parse(_ranking);//int.Parse(_site.GetExpertiseRanking(HeadingId, priceCall + "", GetGuidSetting()));
                if (__ranking > max_ranking_grid)
                    max_ranking_grid = __ranking;
            }
            */

        }
        if (!is_ok)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "invalidPrice", "stopActiveButtonAnim();alert('" + HttpUtility.JavaScriptStringEncode(lbl_minPriceMessage.Text) + "');", true);
            return;
        }
        int max_ranking = 0;

        /*
        if (cb_Notify.Checked)
        {

            if (!int.TryParse(txt_Notify.Text, out max_ranking))
            {
                lbl_NotifyMissing.Style[HtmlTextWriterStyle.Display] = "inline";
                _UpdatePanel_notify.Update();
                return;
            }
            if (max_ranking < max_ranking_grid)
            {
                lbl_NotifyMust.Style[HtmlTextWriterStyle.Display] = "inline";
                _UpdatePanel_notify.Update();
                return;
            }
        }
        */

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.UpdateSupplierPricesRequest _request = new WebReferenceSupplier.UpdateSupplierPricesRequest();

        ////_request.RankingNotification = max_ranking;
        _request.RankingNotification = 0;
        string MaxRanking = (max_ranking == 0) ? "" : max_ranking + "";

        /*
        if (MaxRanking != dicValueV[cb_Notify.ID])
        {
            WebReferenceSupplier.AuditEntry ae = new WebReferenceSupplier.AuditEntry();
            ae.AdvertiseId = new Guid(um.GetGuid);
            ae.AdvertiserName = um.User_Name;
            ae.AuditStatus = (IsFirstTime) ? WebReferenceSupplier.Status.Insert : WebReferenceSupplier.Status.Update;
            ae.FieldId = cb_Notify.ID;
            ae.FieldName = cb_Notify.Text;
            ae.NewValue = MaxRanking;
            ae.OldValue = (IsFirstTime) ? string.Empty : dicValueV[cb_Notify.ID];
            ae.PageId = page_name;
            ae.UserId = new Guid(userManagement.GetGuid);
            ae.UserName = userManagement.User_Name;
            ae.PageName = page_translate;
            list.Add(ae);
        }
        */

        if (list.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noNewData", "stopActiveButtonAnim();parent.NoNewDataDefaultPrice();", true);
            return;
        }

        _request.AuditOn = true;
        _request.AuditEntries = list.ToArray();
        _request.Expertises = listExp.ToArray();
        _request.SupplierId = new Guid(GetGuidSetting());
        WebReferenceSupplier.ResultOfUpdateSupplierPricesResponse result = null;

        try
        {
            result = supplier.UpdateSupplierPrices(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "stopActiveButtonAnim();parent.UpdateFailed();", true);
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "stopActiveButtonAnim();parent.UpdateFailed();", true);
            return;
        }
        if (result.Value.Status == WebReferenceSupplier.UpdateSupplierPricesStatus.PriceOverRechargePrice)
        {
            string _script = "alert('" + lbl_RechargeAmountShouldBigger.Text + " " + result.Value.RechargePrice + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PriceOverRechargePrice", _script, true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "stopActiveButtonAnim();parent.UpdateDefaultPriceSuccess();", true);

        /*
        if (btnSend.Text == lbl_NEXT.Text)
        {
            userManagement.Is_CompleteRegistration = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setStep6", "setStep6();", true);
            _UpdatePanelBid.Update();
            return;
        }
        */
        //Refresh the search advertiser parent page
        if (userManagement.IsPublisher())
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "run_report", "try{top.run_report();}catch(ex){}", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Set_Status", "parent.SetAccountStatus();", true);
        }
        LoadDetail();

    }

    

    public string GetMyRankingSentence
    {
        get { return lbl_MyRanking.Text; }
    }
    public string GetInvalidNumber
    {
        get { return lbl_invalid.Value; }
    }


    Dictionary<string, string> dicValueV
    {
        get { return (ViewState["dicValue"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["dicValue"]; }
        set { ViewState["dicValue"] = value; }
    }
    bool IsFirstTimeV
    {
        get { return (ViewState["IsFirstTime"] == null) ? false : (bool)ViewState["IsFirstTime"]; }
        set { ViewState["IsFirstTime"] = value; }
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }

    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView();
    }

    protected string source
    {
        get
        {
            return (ViewState["source"] == null) ? "" : ViewState["source"].ToString();
        }

        set
        {
            ViewState["source"] = value;
        }
    }
    
}