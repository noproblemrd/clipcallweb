﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="faq.aspx.cs" Inherits="PPC_faq" MasterPageFile="~/Controls/MasterPagePPC2.master"%>
<%@ Register Src="~/Controls/menuVerticalFaq.ascx" TagName="menuVertical" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server"> 
    <title></title>
   
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server"> 
  
     <uc1:menuVertical  ID="menuVertical" runat="server" whoActive="faq"/>
   
     
    <div class="faq dashboardIframe">
        <div class="titleFirst">
            <span>Frequently Asked Questions</span>
        </div>

        <div class="faqContent">
            <div class="containerFaq containerFaqFirst">

                <div class="asq">
                    How do I get leads from NoProblem? 
                </div>

                <div class="answer">
                        <div class="answerParagraph">
                            <div class="answerParagraphNumber">1.</div>
                            <div class="answerParagraphText">
                                We are constantly expanding our extensive network of web publishers who promote NoProblem advertisers. We currently reach tens of millions of web users every day, and our directory is viewed by two million daily visitors. 
                                <br />
                            </div>
                        </div>


                        <div class="answerParagraph">
                            <div class="answerParagraphNumber">2.</div>
                            <div class="answerParagraphText">
                                Our Call Generator Online Form appears only to customers who are actively looking for a service provider on one of our affiliated sites.
                            </div>
                        </div>

                        <div class="answerParagraph">
                            <div class="answerParagraphNumber">3.</div>
                            <div class="answerParagraphText">
                                The customer fills out the form describing the service they need. 
                                <br />
                            </div>
                        </div>

                       <div class="answerParagraph">
                            <div class="answerParagraphNumber">4.</div>
                            <div class="answerParagraphText">
                              Our system matches the customer’s request with your services and calls you immediately.   
                                <br />
                            </div>
                        </div>

                        <div class="answerParagraph">
                            <div class="answerParagraphNumber">5.</div>
                            <div class="answerParagraphText">
                               When you answer the call you will hear a description of the customer’s requirements, which will help you determine the potential value of the job.
                            </div>
                        </div>                      
                        
                        <div class="answerParagraph">
                            <div class="answerParagraphNumber">6.</div>
                            <div class="answerParagraphText">
                               You may use your pre-set default price per call or change the bid once you’ve listened to the customer’s job description. If you are motivated to get the lead, you can enter your bid based on what you think that particular call is worth. It’s completely up to you. After all, who better than you to decide what a particular lead is worth to your business!
                            </div>
                        </div>
                        
                       <div class="answerParagraph">
                            <div class="answerParagraphNumber">7.</div>
                            <div class="answerParagraphText">
                             Our real time system determines the winning bid, calls the service provider and then “bridges” the call with the customer within 30 seconds of winning the bid. You will only pay for the leads that you bid on and win.
                            </div>
                        </div>

                        <div class="answerParagraph">
                            <div class="answerParagraphNumber">8.</div>
                            <div class="answerParagraphText">
                               All the leads delivered to you are archived on your Advertiser Portal dashboard where you can generate useful reports to improve the effectiveness of your marketing.
                            </div>
                        </div>
                       
                </div>

            </div>

            <div class="containerFaq">
                <div class="asq">
                    I bid on a lead but never got a call back. Do I pay for the leads?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                       No. You only pay for leads when you are connected to a customer.
                    </div>                       
                </div>

            </div>

             <div class="containerFaq">
                <div class="asq">
                    Do I need to be in front of a computer to respond to leads?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                       No. All you need is a phone. Our service connects you with motivated customers through your cell phone. If the lead doesn’t sound good to you, you can reject it.
                    </div>                       
                </div>

            </div>

             <div class="containerFaq">
                <div class="asq">
                    How do I pay for leads that I accept?
                </div>
                <div class="answer">
                    <div class="answerParagraph">
                       We charge the credit card associated with your NoProblem account. Your card is only charged once you’ve accepted the lead, won the auction and spoken to a customer. 
                    </div>                       
                </div>

            </div>

             <div class="containerFaq">
                <div class="asq">
                    Will I be charged for every lead I get?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                       You will be charged for every lead you accept, bid on and win.
                    </div>                       
                </div>

            </div>

            <div class="containerFaq">
                <div class="asq">
                   How much do I pay per lead?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                            <div class="answerParagraphNumber">1.</div>
                            <div class="answerParagraphText">
                                <span class="answerParagraphTextTitle">Default pricing</span>
                                <br />
                                     You determine in advance the amount you are willing to pay for each lead. To set your default price per call, go to <a href="defaultPriceSettings.aspx">Billing > Default price settings</a> and set your price per service category. Don’t forget that you are competing for leads with other service providers. We’ll suggest a price range to bid within. The higher your bid, the greater the chance you’ll be connected to motivated consumers looking for your products and services.
                                <br />
                            </div>
                        </div>


                        <div class="answerParagraph">
                            <div class="answerParagraphNumber">2.</div>
                            <div class="answerParagraphText">
                                <span class="answerParagraphTextTitle">Real time pricing</span>
                                <br />
                                When you are called with a lead, you will hear the job description. Based on this information, you decide how much you want to pay for the call (For example, if it’s a smaller job, you might bid only $25. If it’s a larger job, that lead might be worth $50. You choose what the lead is worth to you.).
                            </div>
                        </div>                       
                </div>

            </div>
            
            <div class="containerFaq">
                <div class="asq">
                    How do I know what others paid?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                       If you lose a bid, we recommend you visit the Overview Page. Hover your mouse over the Losing Bid icon to see the winning price.
                    </div>                       
                </div>

            </div>

             <div class="containerFaq">
                <div class="asq">
                   Does the highest bidder always wins?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                       Not always. Leads are awarded to the advertisers with the highest Business Quality Rank (BQR). While important, bid price is only one of several factors that determine BQR.  
                    </div>                       
                </div>

            </div>

              <div class="containerFaq">
                <div class="asq">
                   What is Business Quality Rank (BQR) and how is it determined?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                       Business Quality Rank is a measure of your availability, motivation and customer service. BQR is determined by a weighted average of the following factors:
                       <ul>
                        <li>
                            Answer rate – How often you answer your phone when our system calls you with a lead
                        </li>
                         <li>
                           Acceptance rate – The percentage of calls you accept
                        </li>
                         <li>
                           Winning rate – The percentage of bids you win
                        </li>
                         <li>
                           Refund rate – How often you request refunds
                        </li>
                         <li>
                          Offered bid – How much you are willing to pay for the particular call
                        </li>
                         <li>
                            Reviews and complaints – How positively you are reviewed by your customers
                        </li>
                         <li>
                           Your cover area – The size of your geographic service area
                        </li>
                         <li>
                           Your phone availability hours – The listed number of hours you are available to answer calls
                        </li>
                       </ul>
                    </div>                       
                </div>

            </div>


            <div class="containerFaq">
                <div class="asq">
                   How do I maximize the number of leads I receive from NoProblem?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                       <ul>
                        <li>
                            When you set your <a href="defaultPriceSettings.aspx">default price per lead</a>, you will immediately see your ranking among advertisers in your business category. You can increase your price to boost your ranking.
                        </li>
                         <li>
                            Make sure you cover the largest possible geographic service area. Go to <a href="FramePpc2.aspx?step=3">Business details > Cover area</a> to change your current settings.
                        </li>
                         <li>
                           Increase the number of hours you are available to accept calls. Go to <a href="FramePpc2.aspx?step=4">Business details > Availability</a> to change our current settings.
                        </li>
                         <li>
                           Make sure you maximize your Business Quality Rank (BQR).
                        </li>                        
                       </ul>
                    </div>                       
                </div>
                
                
            </div>

             <div class="containerFaq">
                <div class="asq">
                   Can I set different prices for different types of jobs?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                       Yes. You are able to set different default prices for different categories of work. You can also decide to override your default price and evaluate the lead in real time based on the customer’s job description. You can then bid based on how much the lead is worth to you. For example, if it’s a smaller job, you might bid only $25. If it’s a larger job, it might be worth paying $50 or more to be immediately connected to the customer on your phone.
                    </div>                       
                </div>

            </div>

               <div class="containerFaq">
                <div class="asq">
                  Can I have more than one business category?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                        Yes. If you did not add all your service categories during the registration process, go to <a href="FramePpc2.aspx?step=2">Business details > Categories</a> and add the missing categories. You can modify or update these categories at any time.
                    </div>                       
                </div>

            </div>

              <div class="containerFaq">
                <div class="asq">
                   How do I track my leads?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                        We maintain a list of every lead you have received on your <a href="../Management/overview2.aspx">Overview page</a> where you can access powerful reports tracking how many calls you've received, the details of each call and how many potential customers you've won or lost. You can even listen back to calls and retrieve important information you may have missed, like the customer's address or the amount of your bid for the lead.
                    </div>                       
                </div>

            </div>

              <div class="containerFaq">
                <div class="asq">
                   What is the NoProblem directory?
                </div>

                <div class="answer">
                    <div class="answerParagraph">
                      NoProblem is an online local business directory. Users can search and browse through the directory listings or fill in a form describing what they need, and immediately get connected with ready-to-serve professionals. As a NoProblem advertiser, you are automatically listed as a premium advertiser in the NoProblem directory. You can enhance your business page with videos, pictures and information about your company. As a Premium Advertiser, you benefit from higher placement in search results, no competition on your page and no outside ads.

                    </div>                       
                </div>

            </div>

               <div class="containerFaq">
                <div class="asq">
                   How do I improve my directory placement?
                </div>

                <div class="answer">
                       
                       <div class="answerParagraph">
                            <div class="answerParagraphNumber">1.</div>
                            <div class="answerParagraphText">                                
                                Make sure you are a Premium Advertiser.
                                <br />
                            </div>
                        </div>

                       <div class="answerParagraph">
                            <div class="answerParagraphNumber">2.</div>
                            <div class="answerParagraphText">                                
                                Maximize your Business Quality Rank (BQR).
                                <br />
                            </div>
                        </div>

                    </div>                       
                </div>

               <div class="containerFaq">
                    <div class="asq">
                       How can I view my business page?
                    </div>

                    <div class="answer">
                        <div class="answerParagraph">
                            Go to the <a href="FramePpc2.aspx?step=1">Business details</a> section and click the View Your Business Page button.
                        </div>                       
                    </div>

                </div>
            
              
                <div class="containerFaq">
                    <div class="asq">
                       How do I edit my business page?
                    </div>

                    <div class="answer">
                        <div class="answerParagraph">
                            All changes you make in the <a href="FramePpc2.aspx?step=1">Business details</a> section will show up on your directory business page.
                        </div>                       
                    </div>

                </div>

                 <div class="containerFaq">
                    <div class="asq">
                      Will I receive calls from customers 24/7?
                    </div>

                    <div class="answer">
                        <div class="answerParagraph">
                            You decide when you want to receive calls from customers. Go to <a href="FramePpc2.aspx?step=4">Business details > Availability</a> to set your hours for each day of the week.
                        </div>                       
                    </div>

                </div>


                 <div class="containerFaq">
                    <div class="asq">
                       I’m going on vacation next month. How can I make sure I won’t get calls while I am away?
                    </div>

                    <div class="answer">
                        <div class="answerParagraph">                          
                            NoProblem allows you to stop receiving calls for a period of time. Go to <a href="FramePpc2.aspx?step=4">Business details > Availability</a> and click on "Go on Vacation" to suspend your service while you are away.
                        </div>                       
                    </div>

                </div>

                <div class="containerFaq">
                    <div class="asq">
                      How do I change my credit card information?
                    </div>

                    <div class="answer">
                        <div class="answerParagraph">                           
                            Go to <a href="PaymentMethod.aspx">Billing > Payment method</a> and click on the "Edit" link next to the card number.
                        </div>                       
                    </div>

                </div>

                <div class="containerFaq">
                    <div class="asq">
                      How do I track my expenses?
                    </div>

                    <div class="answer">
                        <div class="answerParagraph">
                            Go to <a href="transactions2014.aspx">Billing > My transactions</a>. You will find a log of all your account activity: leads, bonuses, refunds etc.
                        </div>                       
                    </div>

                </div>


                 <div class="containerFaq">
                    <div class="asq">
                     What happens if I get a bogus or irrelevant call?
                    </div>

                    <div class="answer">
                        <div class="answerParagraph">

                        Our quality control team is constantly working to ensure that you receive only the highest quality leads. Your feedback is appreciated and helps us improve your advertising experience. 
                        <br />
                        Despite these efforts, a low quality lead may occasionally slip through our filters.
                         Should this occur, you can request a refund for any of the following reasons: Out of the Area, Wrong Category, Automatic Call (not a real person) or a Prank Call.
                         If you are uncertain about the quality of the call, you can listen to a recording of the call on your Overview Page.
                         Remember that you are always free to reject a call in real time after hearing the job description.
                         If you are repeatedly receiving irrelevant leads, please review your service description to see if it needs tweaking.
                         If you do accept an irrelevant call, as defined in our Refund Policy, simply report the lead.
                         Please note: We can only issue credits for recorded calls. This is one of several reasons <a href="#anchorWhyRecordCalls">why should you record your calls</a>.
                         Go to My Leads in the Overview page and click “Record Calls” to make sure all your calls are recorded. 
                       
                        <br />
                        
                        As set out in our <a href="HonorCode2014.aspx">Honor Code</a>, we trust our advertisers to be honest and we do our best to honor your refund requests.
                         However, when an advertiser is exceeding an acceptable refund rate, our system automatically flags the account for review to ensure compliance with our <a href="http://www2.noproblemppc.com/LegalRefund.htm" target="_blank">Refund Policy</a>.
                      
                        </div>                       
                    </div>

                </div>


                 <div class="containerFaq">
                    <div class="asq">
                      Why should I record my calls?
                      <br />
                      <a name="anchorWhyRecordCalls"></a>
                    </div>

                    <div class="answer">
                        <div class="answerParagraph">
                           Recording your calls enables you to keep a record of all your leads and your communication with them. This is especially helpful when accepting calls on the road. This feature allows you to record the customer’s name, address and phone number for later use. 
                            <br />
                           By reviewing your call recordings you can learn a lot about your customers needs and how to improve your sales pitch. 
                            To be eligible to request refunds, Call Recording must be enabled. Go to My Leads in the Overview page and click “Record Calls” to ensure all your calls are recorded.

                        </div>                       
                    </div>

                </div>


                <div class="containerFaq">
                    <div class="asq">   
                      
                      
                    </div>

                     <div class="answer">
                        <div class="answerParagraph">
                            If you have any additional questions for us, please feel free to <a href="mailto:hi@noproblemppc.com">contact us</a>.
                        </div>                       
                    </div>

                </div>

                

            </div>        

    </div>
 </asp:Content>
