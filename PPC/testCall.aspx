﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testCall.aspx.cs" Inherits="PPC_testCall" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="samplepPpc.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
	<script type="text/javascript">	    try { Typekit.load(); } catch (e) { }</script>

    <script type="text/javascript">
        function setp(index) {
            document.getElementById("step" + index).style.display = 'block';
            var prevIndex = parseInt(index) - 1;
            document.getElementById("step" + prevIndex).style.display = 'none';
        }

        var intervalGetLiveTestCallStatus;

        function GetLiveTestCallStatus(testCallId) {
            var params;
            params = "supplierId=<%#supplierId%>&mode=live&testCallId=" + testCallId;

            var result;
            var showInReport;
            //alert("params:" + params);           

            var url;

            //url = "http://" + location.hostname + ":" + location.port + "<%#root%>createServiceRequest.aspx";
            url = "testCall.ashx";
            // alert(url)

            var objHttp;
            var ua = navigator.userAgent.toLowerCase();

            if (ua.indexOf("msie") != -1) {
                objHttp = new ActiveXObject("Msxml2.XMLHTTP");
            }
            else {
                objHttp = new XMLHttpRequest();
            }



            if (objHttp == null) {
                //alert("Unable to create DOM document!");
                return;
            }


            objHttp.open("POST", url, true);

            objHttp.onreadystatechange = function () {

                if (objHttp.readyState == 4) {

                    //alert(objHttp.responseText);

                    result = objHttp.responseText;
                    arrSplit = result.split(',');
                    showInReport = arrSplit[0]; //  showInReport true or false. true means end of task, fulfill task
                    liveTestCallStage = arrSplit[1]; //ringing , advertiser answered, advertiser confirmed, completed, busy, failed,no answer

                    if (objHttp.status == 200 || objHttp.status == 304) {
                        if (objHttp.responseText == "failed") {
                            clearInterval(intervalGetLiveTestCallStatus);
                        }
                        else // success
                        {
                            switch (liveTestCallStage) {

                                case "ringing":
                                    break;

                                case "advertiser answered":
                                    setp(2);
                                    break;

                                case "advertiser confirmed": // now showInReport becomes true 
                                    setp(3);
                                    break;

                                case "completed":
                                    clearInterval(intervalGetLiveTestCallStatus);
                                    parent.showHideTestCall();
                                    break;

                                case "busy":
                                    clearInterval(intervalGetLiveTestCallStatus);
                                    break;

                                case "failed":
                                    clearInterval(intervalGetLiveTestCallStatus);
                                    break;

                                case "no answer":
                                    clearInterval(intervalGetLiveTestCallStatus);
                                    break;
                            }

                        }
                    }

                    else {

                    }


                }
            }

            objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            objHttp.setRequestHeader("Content-Length", params.length);
            objHttp.setRequestHeader("Connection", "close");

            objHttp.send(params);


            return true;

        }

        function StartGetStartedTestCall() {
           
            var params;
            params = "supplierId=<%#supplierId%>&mode=start";

            //alert("params:" + params);           

            var url;
            
            //url = "http://" + location.hostname + ":" + location.port + "<%#root%>createServiceRequest.aspx";
            url = "testCall.ashx";
            // alert(url)

            var objHttp;
            var ua = navigator.userAgent.toLowerCase();

            if (ua.indexOf("msie") != -1) {                
                objHttp = new ActiveXObject("Msxml2.XMLHTTP");                
            }
            else {                
                objHttp = new XMLHttpRequest();
            }



            if (objHttp == null) {
                //alert("Unable to create DOM document!");
                return;
            }


            objHttp.open("POST", url, true);

            objHttp.onreadystatechange = function () {

                if (objHttp.readyState == 4) {
                    //alert(objHttp.responseText);
                    if (objHttp.status == 200 || objHttp.status == 304) {
                        if (objHttp.responseText == "failed") {
                            alert(objHttp.responseText);
                        }
                        else // success
                        {
                            testCallId = objHttp.responseText;
                            intervalGetLiveTestCallStatus=setInterval("GetLiveTestCallStatus('" + testCallId + "')",1000);
                        }
                    }

                    else {

                    }


                }
            }

            objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            objHttp.setRequestHeader("Content-Length", params.length);
            objHttp.setRequestHeader("Connection", "close");

            objHttp.send(params);


            return true;

        }

        window.onload = StartGetStartedTestCall;
       
    </script>
</head>
<body class="bodyTestCall">
    <form id="form1" runat="server">

    <div> 
               
        <div class="title">Test Call</div>

            <div id="step1" class="step1"  >
                <div class="subTitle">Your mobile phone should ring at any moment now. Make sure it’s available.</div>
                <div class="animation">
                    <img src="images/overview/ani-NObg.gif" />
                    <div class="callerId"><%=callerId%></div>
                    <div class="supplierPhone"><%=supplierPhone%></div>
                </div>
                
            </div>
        
            <div id="step2" class="step2" >
             
                <div class="subTitle">
                    <img src="images/overview/phone.gif" style="vertical-align:middle;"/>
                    <span class="spanSubTitle">Call in session...</span>
                </div>

                <div class="description">
                      You are listening now to the automatic announcement that we play each time we find a
                            customer in your area.   
                </div>

                <div class="description2">
                    When you done, press "1" to talk to the customer.
                </div>
                
            </div>

            <div id="step3" class="step3" >
                <div class="subTitle">
                    <img src="images/overview/phone.gif" style="vertical-align:middle;"/>
                    <span class="spanSubTitle">You are now being redirected to the customer.</span>
                </div>

                <div class="description">
                    <div class="descriptionTitle">Introduce yourself.</div>                
                
                    Say your name, your profession, and why you are calling.
                    <br />
                    You’ll be able to listen to your conversation immediately after the call ends.
                </div>

                <div class="description2">
                     When you’re done, hang up.
                </div>

            </div>
        </div>

    </div>

    </form>
</body>
</html>
