﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;

public partial class PPC_addLogo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            

    }

    void LoadPic(string _FileName)
    {

        //string path2 = ConfigurationManager.AppSettings["professionalLogos"] + @"aspjpeg\" + _FileName;
        string path2 = ConfigurationManager.AppSettings["professionalLogos"] + @"\" + _FileName;
        string _path = ConfigurationManager.AppSettings["PublisherLogoWeb"] + @"aspjpeg/" + _FileName;
        if (File.Exists(path2))
        {
            //tr_pic.Attributes.Remove("style");
            //_ImagePic.ImageUrl = _path;
            //FileNameV = _FileName;
        }
        
    }

    protected void btn_uploadLogo_Click(object sender, EventArgs e)
    {

        commentUploadImages.Visible = false;
        string _path = ConfigurationManager.AppSettings["professionalLogos"];
        string UploadedFile = _FileImages.PostedFile.FileName;
        //      string UploadedFile = Hidden_FilePath.Value;
        if (string.IsNullOrEmpty(UploadedFile))
        {
            commentUploadImages.Visible = true;
            LoadPic(FileNameV);
            return;
        }
        //     string test = System.IO.Path.GetFileName(UploadedFile);
        string FullName = System.IO.Path.GetFileName(UploadedFile);
        string _name = System.IO.Path.GetFileNameWithoutExtension(UploadedFile);
        //      string[] fileName = FullName.Split('.');
        string _extension = System.IO.Path.GetExtension(UploadedFile);

        switch (_extension.ToLower())
        {
            case ".jpg":
                break;
            case ".png":
                break;
            case ".gif":
                break;
            case ".jpeg":
                break;
            default:
                Response.Write("this file type is not allowed");
                return;
        }


        if (string.IsNullOrEmpty(_extension))
        {
            InvalidImage();
            return;
        }

        /*
        int i = 0;
        while (File.Exists(_path + _name + (i++) + _extension)) ;
        _name = _name + (i - 1);
        */


        string str_path_standard = _path + _name + _extension;

        _FileImages.PostedFile.SaveAs(str_path_standard);

        ASPJPEGLib.IASPJpeg objJpeg;
        objJpeg = new ASPJPEGLib.ASPJpeg();
        try
        {

            objJpeg.Open(str_path_standard);
        }
        catch (Exception exc)
        {
            InvalidImage();
            return;
        }

        // Decrease image size by 50%
        objJpeg.Width = 136;
        objJpeg.Height = 146;
        string new_name = _name + "_icon" + _extension;
        string str_path_thumbNail = _path +  new_name;
        objJpeg.Save(str_path_thumbNail);

        //insert path to DB  
        /*
        string command = "EXEC dbo.InsertLogoBySiteNameId @SiteNameId, @LogoPath ";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            ////cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@LogoPath", new_name);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
        */
        LoadPic(new_name);
        ////siteSetting.LogoPath = new_name;
        /////siteSetting.SetSiteSetting(this);
        ////Master.setLogo(siteSetting.LogoPath);

    }

    string FileNameV
    {
        get { return (ViewState["pic"] == null) ? string.Empty : (string)ViewState["pic"]; }
        set { ViewState["pic"] = value; }
    }

    void InvalidImage()
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "InvalidImage", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_ErrorImage.Text) + "');", true);
    }

    /*
    max
    height: 136px;
    width: 146px;
    */
}