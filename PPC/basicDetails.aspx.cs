﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_basicDetails : RegisterPage, IsInProcess
{
    protected string nameHolder;
    protected string companyAddressHolder;
    protected string contactPersonHolder;
    protected string regFormatPhone;   
    protected string businessAdress;
    protected string businessNameError;
    protected string contactPersonError;
    protected string GetServerError;
    protected string ServicePlaceHolder;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Master.removeSideBar(); 

            LoadPageText();

            txt_name.Attributes["place_holder"] = nameHolder;
            txt_Category.Attributes["place_holder"] = ServicePlaceHolder;
            txt_contactPerson.Attributes["place_holder"] = contactPersonHolder;
            SendEmail();
            
        }
        else
        {


        }

        LoadMaster();

        Header.DataBind();
        Page.DataBind();

    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;


        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "BusinessDetails"
                          select x).FirstOrDefault();

        nameHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("CompanyName").Value;
        contactPersonHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("ContactPersonName").Value;
        companyAddressHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("CompanyAddress").Value;
        

        businessNameError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("BusinessName").Value;
        contactPersonError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("ContactPersonName").Value;
        businessAdress = xelem.Element("Validated").Element("Instruction").Element("Error").Element("BusinessAdress").Value;

        xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "DetailsService"
                          select x).FirstOrDefault();
        XElement Service_element = xelem.Element("Service").Element("Validated");
        ServicePlaceHolder = Service_element.Element("Instruction").Element("General").Value;
       
    }

    private void SendEmail()
    {
        MailSender mailSender = new MailSender();

        mailSender.From = "registration@noproblem.co.il";
        mailSender.FromName = "NoProblem Registration";
        mailSender.To = "registration@onecall.co.il";

        mailSender.IsText = false;
        mailSender.Subject = "Newly " + Session["plan"] + " potential advertiser";

        string phone = Session["RegistrationPhone"] != null ? Session["RegistrationPhone"].ToString() : "";
        string password = Session["RegistrationPassword"] != null ? Session["RegistrationPassword"].ToString() : "";

        string body = "<bold>A potential advertiser clicked on " + Session["plan"] + "</bold><br><br>" +
                    "Phone: " + phone + "<br>" +
                    "Password: " + password;

        mailSender.Body = body;
        //mailSender.RemoteHost = "127.0.0.1";
        bool ifOkSentNoProblemBilling = mailSender.Send();

        if (ifOkSentNoProblemBilling)
        {
            // status = "true";
        }

        else
        {
            // status = "false";
            dbug_log.ExceptionLog(new Exception("Registration failed to send email to " + mailSender.To + " from " + phone), Site_Id);

        }
    }

    private void LoadMaster()
    {
        Master.SetBenefitFreeText("Get leads from ready-to-hire customers directly to your mobile phone.</br></br>That's NoProblem!");
        Master.SetStatusBar("statusBar2");
    }    


    protected string Site_Id
    {
        get { return siteSetting.GetSiteID; }
    }

    protected string GetHeadingServiceList
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/GetHeadingList"; }
    }
   

   
//    SalesUtility.SetStepRegistration(Session["RegistrationPhone"] != null ? Session["RegistrationPhone"].ToString() : "", Site_Id, "PassedChoosePlanPage_CompletedAll", Session["RegistrationPassword"] != null ? Session["RegistrationPassword"].ToString() : "", page, plan, true);

  //      Response.Redirect("done.aspx");

    protected void lb_Next_Click(object sender, EventArgs e)
    {
        Session["name"] = txt_name.Value;
        Session["contactPerson"] = txt_contactPerson.Value;
        Session["region"] = searchGoogle1.region;
        Session["category"] = txt_Category.Text;

        SalesUtility.SetStepRegistration(Session["RegistrationPhone"] != null ? Session["RegistrationPhone"].ToString() : "", Site_Id, "PassedEnterDetails_CompletedAll", Session["RegistrationPassword"] != null ? Session["RegistrationPassword"].ToString() : "", Session["page"].ToString(), Session["plan"].ToString(), txt_name.Value,txt_contactPerson.Value, searchGoogle1.region,txt_Category.Text, true);

        Response.Redirect("done.aspx");
    }
}