﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HonorCode.aspx.cs" Inherits="PPC_HonorCode" MasterPageFile="~/Management/MasterPageAdvertiser.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <script type="text/javascript">
        //window.onload = appl_init;
        function appl_init() {
            //hideDiv();
            //alert("baaaa");
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);
            
        }
        function BeginHandler() {
            //alert("showDiv");
            showDiv();
        }
        function EndHandler() {
            hideDiv();
        }

        function updateSystemMessage(str) {

            document.getElementById("<%#systemMessage.ClientID%>").style.display = 'block';
            document.getElementById("<%#systemMessageText.ClientID%>").innerHTML = str;

        }

        
        function OnBeginRequest(sender, args) {
            // to destroy this function in UpdateProgress\UpdateProgress.js
            //document.getElementById('divLoader').style.display = 'block';
        }

        function endRequest(sender, args) {
            // to destroy this function in UpdateProgress\UpdateProgress.js
            //document.getElementById('divLoader').style.display = 'none';
        }

        function updateBalance(balance) {
            document.getElementById("<%#clientIdBalance%>").innerHTML = balance;
        }

        function allTasksDone(bonusAmount){
           
            var balance = document.getElementById("<%#clientIdBalance%>").innerHTML;
            
            balance = balance.replace(',', '');
            balance = parseFloat(balance);           
            
            balance += bonusAmount * 1;

            updateBalance(addCommas(balance));
            
        }       

       
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">

    <div class="honorCode">
        <div class="left">
            <div id="systemMessage" class="systemMessage" runat="server" >
                <div id="systemMessageText" class="systemMessageText" runat="server">                                      
                </div>
                <div class="systemMessageClose" onclick="closeDiv('<%=systemMessage.ClientID%>')"></div>            
            </div>

            <div id="stepHonorContent" class="stepHonorContent" style="display:block;">

                    <div class="honorContent">
                        <asp:UpdatePanel runat="server" ID="updatePanel1">
                            <ContentTemplate>                            
                            <div class="ribbon" runat="server"  id="honorRibon" visible="false"><div class="ribbonText">NP</div></div>
                            <div class="honorTitle">Honor Code</div>         
                        
                                <div  id="stepHonorValues" class="stepHonorValues" runat="server"></div>
                        
                            </ContentTemplate>            
                        </asp:UpdatePanel> 
                    </div>  
            </div>   
        </div>
    </div>




</asp:Content>