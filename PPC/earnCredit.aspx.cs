﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_earnCredit : PageSetting
{    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadaccountCompletness();
        }
        Header.DataBind();
    }
    private void LoadaccountCompletness()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfGetStartedTasksStatusResponse result;
        try
        {
            result = _supplier.RetrieveGetStartedTasksStatus(new Guid(GetGuidSetting()));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if (result.Value.TasksLeft == 0)
            EarnCredit_CompleteGetStarted.Visible = false;
    }
    protected string OverView
    {
        get { return ResolveUrl("~/Management/OverView.aspx"); }
    }
    protected string BonusCredit
    {
        get { return ResolveUrl("~/PPC/AddCredit.aspx"); }
    }
    protected string Invite
    {
        get { return ResolveUrl("~/PPC/invitePro.aspx"); }
    }
}