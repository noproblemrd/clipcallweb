﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNP.master" AutoEventWireup="true" CodeFile="RequestInvitation.aspx.cs" Inherits="PPC_RequestInvitation" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNP.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<!--
<script type="text/javascript" src="../general.js"></script>
-->
<script type="text/javascript" src="../scripts/EmailValidation.js"></script>
<script type="text/javascript" src="../scripts/US-Phone.js"></script>
<script type="text/javascript" src="../scripts/HashBackForward.js"></script>
 <script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript">
    var _email = new np_email();
    _email.init();
    _email.OnKeyUpEmpty = function (txtId) {
        ClearEmailValidate();
    };
    _email.OnMatch = function (txtId) {
        ClearEmailValidate();
        $('#div_EmailSmile').show();
        $('#<%# txt_email.ClientID %>').addClass('textValid');
    };
    _email.OnKeyUpOnTheWay = function (txtId) {
        ClearEmailValidate();
    };
    _email.OnFaild = function (txtId) {
        ClearEmailValidate();
        $('#div_EmailComment').show();
        $('#div_EmailError').show();
        $('#<%# txt_email.ClientID %>').addClass('textError');
    };
    _email.OnEmpty = function (txtId) {
        ClearEmailValidate();
       
    };
    _email.OnFocus = function (txtId) {
        ClearEmailValidate();
    };
    _email.OnClickFaild = function (txtId, _status) {
        switch (_status) {
            case ("empty"):
                SetServerComment('<%# EmailMissing %>');
                break;
            case ("OnTheWay"):
            case ("faild"):
                SetServerComment('<%# InvalidEmail %>');
                break;
        }
    };
    _email.OnClickMatch = function (txtId) {
        var email = $('#' + txtId).val();
        $('#Request_Invitation').hide();
        $('.span-Request-Invitation').show();
        $.ajax({
            url: "<%# CheckEmailInRegistrationRequest %>",
            data: "{ 'email': '" + email + "' }",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if (data.d == 'Continue') {
                    $('#<%# hf_email.ClientID %>').val(email);
                    ToPhoneNum();
                    window.location.hash = 'p';
                }
                else if (data.d == 'Login') {
                    window.location.href = '<%# login %>';
                }
                else {

                    GeneralServerError();
                    $('#Request_Invitation').show();
                    $('.span-Request-Invitation').hide();
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert('error');
            }

        });

    };
    function ToPhoneNum()
    {
          $('#<%# div_a.ClientID %>').hide();
        $('#<%# div_b.ClientID %>').show();
        RemoveServerComment();
    }
    function ClearEmailValidate() {
        $('#div_EmailSmile').hide();
        $('#div_EmailComment').hide();
        $('#div_EmailError').hide();
        $('#<%# txt_email.ClientID %>').removeClass('textError');
        $('#<%# txt_email.ClientID %>').removeClass('textValid');
    }
    var _phone = new np_phone('<%# PhoneRegExpression %>');
    _phone.init();
    _phone.OnKeyUpEmpty = function (txtId) {
        ClearPhoneValidate();
    };
    _phone.OnMatch = function (txtId) {
        ClearPhoneValidate();
        $('#div_PhoneSmile').show();
        $('#<%# txt_phone.ClientID %>').addClass('textValid');
    };
    _phone.OnKeyUpOnTheWay = function (txtId) {
        ClearPhoneValidate();
    };
    _phone.OnFaild = function (txtId) {
        ClearPhoneValidate();
        $('#div_PhoneComment').show();
        $('#div_PhoneError').show();
        $('#<%# txt_phone.ClientID %>').addClass('textError');
    };
    _phone.OnClickFaild = function (txtId) {
        var __message = ($('#'+txtId).val().length > 0) ? '<%# InvalidMobile %>' : '<%# MissingMobile %>';
        SetServerComment(__message);
        _phone.OnFaild(txtId);
    };
    _phone.OnEmpty = function (txtId) {
        ClearPhoneValidate();

    };
    _phone.OnFocus = function (txtId) {
        ClearPhoneValidate();
    };
    _phone.OnClickMatch = function (txtId) {
        $('span.btn_next').show();
        $('#btn_next').hide();
        var phone = $('#' + txtId).val();
        var email = $('#<%# hf_email.ClientID %>').val();
        $.ajax({
            url: "<%# SupplierRequestInvitation %>",
            data: "{ 'phone': '" + phone + "', 'email': '" + email + "' }",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                switch (data.d) {
                    case ('EmailDuplicate'):
                        SetServerComment(EmailDuplicate);
                        $('span.btn_next').hide();
                        $('#btn_next').show();
                       
                        break;
                    case ('faild'):
                        GeneralServerError();
                        $('span.btn_next').hide();
                        $('#btn_next').show();
                        break;
                    case ('TransferToLogin'):
                        window.location.href = '<%# login %>?register=true';
                        break;
                    case ('OkIsAar'):
                    case ('OkIsNew'):
                        window.location.href = '<%# ConfirmPhone %>';
                        break;
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert('error');
            }

        });
    };
    var EmailDuplicate;
    $(function () {
        EmailDuplicate = '<%# EmailDuplicate %>';
    });
    function ClearPhoneValidate() {
        $('#div_PhoneSmile').hide();
        $('#div_PhoneError').hide();
        $('#div_PhoneComment').hide();
        $('#<%# txt_phone.ClientID %>').removeClass('textError');
        $('#<%# txt_phone.ClientID %>').removeClass('textValid');
    }
    function BackToEmail() {
        $('#<%# hf_email.ClientID %>').val('');
        BackToEmailHash();
    }
    function BackToEmailHash(){ 
        $('#<%# div_a.ClientID %>').show();
        $('#<%# div_b.ClientID %>').hide();
        $('#Request_Invitation').show();
        $('.span-Request-Invitation').hide();
        RemoveServerComment();
    }


    var _BackForward = new HashBackForward(BackToEmailHash);
          
    $(function () {
      <%# ForwardBackScript() %>
      _BackForward.SetStep('p', ToPhoneNum); 
       _BackForward.start();
    });
    

    /*
    $(function(){
        setTimeout(function(){
            SetServerComment('wef wefeg rth rt hr th r t ertgwertwe tfwergtwert weegtfwet wet w e we t wet w et w et  wet fwetgf');
        }, 2000);
    });
    */
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="hf_email" runat="server" />
<div class="RequestInvitation">
    <div class="titleFirst">
            <span>Request an Invitation to Join NoProblem</span>
        </div>
    <div runat="server" id="div_a" class="div_TopSpace div_email_request_invitation"  >
        
        
        <span class="span-block bottom-higth"> 
NoProblem is currently in beta, so we’re only inviting a number of select service professionals to join. We occasionally release invitations, so please register to have your name added to our waiting list. We’d love to have you on board! 
       </span>
        <span class="span-block">Got an invitation from a friend?<a class="general" href="RegisterReferrer.aspx">Register with a referral code.</a></span>
        <span class="span-block">Already a beta user?<a class="general" href="<%= ResolveUrl("~/PPC/PpcLogin.aspx") %>">Log in</a>to your account.</span>

        <div class="titleSecond emailTitle"><asp:Label ID="lbl_email" runat="server" Text="My email is..."></asp:Label></div>
        <div class="inputParent">
            <input id="txt_email" type="text" class="textActive txtEmail" runat="server" HolderClass="place-holder" />
            <div class="div-Request-Invitation">
                <a href="javascript:void(0);" button-mode="email" class="Request-Invitation" id="Request_Invitation">Request Invitation</a>
                <span class="span-Request-Invitation" style="display:none;">Processing...</span>
            </div>
             <div class="inputValid" style="display:none;"  id="div_EmailSmile">

                
            </div>
            <div class="inputError" style="display:none;"  id="div_EmailError">
               
            </div>
            <div class="inputComment" style="display:none;" id="div_EmailComment">
                     <%= EmailPlaceHplder %>
                <div class="chat-bubble-arrow-border"></div>
                <div class="chat-bubble-arrow"></div>
            </div>
            <div>
                
            </div>
        </div>
        <div class="manWithPC"></div>
    </div>
    <div runat="server" id="div_b" class="div_TopSpace" style="display:none;">
        <span class="span-block bottom-higth">
Please provide us with some more information now, so that you won’t need to register when we invite more professionals to join our beta.
        </span>
        <div class="titleSecond emailTitle"><asp:Label ID="lbl_phone" runat="server" Text="My mobile phone number is..."></asp:Label></div>
         <div class="inputParent">
         <input id="txt_phone" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="phone" />

             <div class="inputValid" style="display:none;"  id="div_PhoneSmile">
                
            </div>
            <div class="inputError" style="display:none;"  id="div_PhoneError">
               
            </div>
            <div class="inputComment" style="display:none;" id="div_PhoneComment">
                     <%= PhonePlaceHplder %>
                <div class="chat-bubble-arrow-border"></div>
                <div class="chat-bubble-arrow"></div>
            </div>
            
        </div>
        <div>
                <a href="javascript:void(0);" button-mode="phone" class="btn_next" id="btn_next" >Next ></a>
                <span class="btn_next" style="display:none;">Processing...</span>
            </div>
            
            <div class="underNext">
                <span>We’ll text the password to the phone number above.</span>
            </div>
            <div class="underNextText">
               
                <span>
                    * Please note that to use No Problem’s phone lead advertising service, it is essential to have a mobile phone that’s able to receive text messages. We will not share your phone number with anyone, nor use it for anything other than bringing you new customers. We take your <a href="javascript:openWin('http://www2.noproblemppc.com/LegalPrivacy.htm');"  class="general">privacy</a> very seriously.
                </span>
            </div>
    </div>
</div>


</asp:Content>

