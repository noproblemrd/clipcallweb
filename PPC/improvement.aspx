﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Controls/MasterPageNP2.master" CodeFile="improvement.aspx.cs" Inherits="PPC_improvement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<div class="improvement">
    <div class="titleFirst">
        <span>New and improved NoProblem</span>
    </div>
   
    
    <div class="dear">
    Dear NoProblem advertiser, 
    </div>

    <div class="promo">
    Following the great feedback we received from you, our advertiser community, NoProblem
    <br />
    improves existing features and adds new features. Take a moment to learn what’s new and
    <br />
    how it can enrich your experience as an advertiser.
    </div>

    <div class="whatsNew">
    What’s new?
    </div>

    <div class="points">

    <div class="pointsLeft">

        <div class="point">

            <div class="pointIcon"><img src="images/pay.jpg"></div>

            <div class="pointContent pointContent2line">
                <div class="pointContentTitle">Pay only for leads you accept</div>

                <div class="pointContentText">
                    <div>We wont charge you when you are not</div>
                    <div>available to accept the lead.</div>
                </div>
            </div>
       
        </div>

        <div class="clear"></div>

        <div class="point">
            <div class="pointIcon"><img src="images/50.jpg"></div>

            <div class="pointContent pointContent2line">
                <div class="pointContentTitle">No need to purchase credit anymore</div>
                <div class="pointContentText">
                    <div>You pay per each lead you accept, your</div>
                    <div>auto-recharge has been canceled.</div>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="point">
            <div class="pointIcon"><img src="images/cover-area.jpg"></div>

            <div class="pointContent pointContent1line">
                <div class="pointContentTitle">Multiple cover area</div>

                <div class="pointContentText">
                    <div>Be available on more than one location.</div>            
                </div>
            </div>
        </div>

    </div>

     <div class="pointsRight">

        <div class="point">
            <div class="pointIcon"><img src="images/hours.jpg"></div>

            <div class="pointContent pointContent1line">
                <div class="pointContentTitle">Working hours enhancements</div>

                <div class="pointContentText">
                    <div>You can now split your day into shifts.</div>            
                </div>
            </div>

        </div>

        <div class="clear"></div>

        <div class="point point4line">
            <div class="pointIcon"><img src="images/business-page.jpg"></div>

            <div class="pointContent pointContent4line">
                <div class="pointContentTitle">Premium business page</div>

                <div class="pointContentText">
                    <div>Appear in NoProblem revamped directory</div> 
                    <div>for free: featured exposure, no competitor</div> 
                    <div>ads, link to your website, photo and video</div>
                    <div>gallery and more.</div>              
                </div>
            </div>

        </div>

        <div class="clear"></div>

     </div>


     </div>


     <div class="clear"></div>


     <div class="pleaseClick">
        In order to start using the new features, please click the button below.
     </div>

     <div class="send">
      <section class="progress-demo">
        <a id="linkSetup" runat="server" href="ppcloginMobile.aspx" data-style="expand-right" data-color="blue" class="ladda-button" button-mode="phone" onclick="javascript:activeButtonAnim('.progress-demo #linkSetup', 200);">
        <span class="ladda-label">Setup your account</span><span class="ladda-spinner">
        </span><div class="ladda-progress" style="width: 138px;"></div>
        </a>
      </section>
      </div>


</div>

</asp:Content>

