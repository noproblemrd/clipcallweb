﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_testCall : PageSetting
{
    protected string callerId="";
    protected string supplierPhone="";
    protected string supplierId = "";
    protected string root = "";

    protected void Page_Load(object sender, EventArgs e)
    {
       
        callerId=Request.QueryString["callerId"];
        if (callerId.IndexOf("+") != -1) // like +15756362489
            callerId = callerId.Replace("+", string.Empty);

        if (callerId.Length == 10)
            callerId = callerId.Substring(0, 3) + "-" + callerId.Substring(3, 3) + "-" + callerId.Substring(6, 4);
        else if (callerId.Length == 11)
            callerId=callerId.Substring(0,1) + "-" + callerId.Substring(1,3) + "-" + callerId.Substring(4,3) + "-" + callerId.Substring(7,4);
       
        supplierPhone=Request.QueryString["supplierPhone"];
        supplierPhone = supplierPhone.Substring(0, 3) + "-" + supplierPhone.Substring(3, 3) + "-" + supplierPhone.Substring(6, 4);
        
        supplierId = Request.QueryString["supplierId"];

        Page.Header.DataBind();
    }
}