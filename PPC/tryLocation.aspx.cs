﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class PPC_tryLocation : RegisterPage
{
    Guid _RegionId;
    bool _selectAll = false;
    protected string GetServerError; 

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(lbAddButton);

        if (!IsPostBack)
        {
            LoadPageText();            
            LoadMaster();

            arrayWhoStay = new ArrayList();
            listLocatinData = new List<LocationData>();
            LoadLocation();
            LoadPanelLocation(false);
            bindMapToTextBox();

            /*
            string _script = "initializeMulti(" + index + ")";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "initializeMulti", _script, true);
            */
        }

        else
        {
            index++;
            //Response.Write("index before: " + index);
            LoadPanelLocation(true);


        }


        //Response.Write("index: " + index);

        this.Page.Header.DataBind();

        this.DataBind();

    }

    private void LoadMaster()

    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

        masterPage.SetStatusBar("past", "current", "pre");
        masterPage.SetBlackMenu();
        masterPage.SetPlanType("Lead Booster plan");
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        string FieldMissing = Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

       

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "Location"
                          select x).FirstOrDefault();



        XElement t_element = xelem.Element("BusinessAddress").Element("Validated");
        LocationPlaceHplder = t_element.Element("Instruction").Element("General").Value;
        SelectFromList = t_element.Element("Error").Value;
        //    _map.ChooseLargerArea = t_element.Element("Instruction").Element("LargerArea").Value;
        ServerError = _error;

    }

    public void SetFullAddress(string _address, int index)
    {
        string _script = "SetFullAddress('" + _address + "'," + index + ");";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "SetFullAddress" + index, _script, true);
    }

    public void SetMap(string radius, string lat, string lng, int index)
    {
        if (index == 1)
        {
            string _script = "SetMapOnLoad('" + radius + "', '" + lat + "', '" + lng + "'," + index + ");";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SetMapOnLoad" + index, _script, true);
        }

        /*
        if (IsMoreSelected)
        {
            XElement xarea = _areas.Element("SelectedAreas");
            div_places.Style[HtmlTextWriterStyle.Display] = "block";
            foreach (XElement _xel in xarea.Elements("Area"))
            {
                string _level = _xel.Attribute("Level").Value;
                string RegionId = _xel.Attribute("RegionId").Value;
                HtmlAnchor _ha = new HtmlAnchor();
                bool __selectedall = false;
                switch (_level)
                {

                    case ("ALL"): _ha = a_Region0;
                        __selectedall = true;
                        break;
                    case ("1"): _ha = a_Region1;
                        goto default;
                    case ("2"): _ha = a_Region2;
                        goto default;
                    case ("3"): _ha = a_Region3;
                        goto default;
                    default:
                        _ha.InnerHtml = _xel.Attribute("Name").Value;
                        _ha.Attributes.Add("_id", RegionId);
                        break;
                }

                if (_xel.Attribute("Selected").Value == "true")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SetLargeArea", "_SetLargeArea(document.getElementById('" + _ha.ClientID + "'));", true);
                    Guid.TryParse(RegionId, out _RegionId);
                    _selectAll = __selectedall;
                    hf_SavedRegionId.Value = (__selectedall) ? "ALL" : _RegionId.ToString();
                }
            }
        }
        else
            div_places.Style[HtmlTextWriterStyle.Display] = "none";
        */
    }


    private void LoadLocation()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        string strWorkAreas;

        /*
        try
        {
            
            WebReferenceSupplier.GetCoverAreaRequest getCoverAreaRequest=new WebReferenceSupplier.GetCoverAreaRequest();
            getCoverAreaRequest.SupplierId=new Guid("3E796B9A-7A51-493C-A371-FE5B5A63BC63");
            
            WebReferenceSupplier.ResultOfString resultOfString=new WebReferenceSupplier.ResultOfString();
            resultOfString = _supplier.GetCoverAreas_Registration2014(getCoverAreaRequest);

            if (resultOfString.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed retrieve GetCoverAreas_Registration2014 for " + getCoverAreaRequest.SupplierId.ToString() + " message: " + resultOfString.Messages[0]));
                return;
            }

            else
            {
                strWorkAreas = resultOfString.Value;
            }

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
            return;
        }
        */

        /*
        string strWorkAreas = @"<Areas IsMoreSelected=""false"" RegistrationStage=""6"" 
            Longitude=""-73.99293419999998"" Latitude=""40.742716""
            Radius=""16"" FullAddress=""6th Ave, ניו יורק, ארצות הברית"" 
            Email=""hi@noproblemppc.com"">
              <SelectedAreas />
            </Areas>";
        */

        /*
        string strWorkAreas = @"<Areas IsMoreSelected=""false"" RegistrationStage=""6"" 
            Longitude=""-73.99293419999998"" Latitude=""41.742716""
            Radius=""32"" FullAddress=""6th Ave, קוקי, ארצות הברית"" 
            Email=""hi@noproblemppc.com"">
              <SelectedAreas />
            </Areas>
            <Areas IsMoreSelected=""false"" RegistrationStage=""6"" 
            Longitude=""-73.99293419999998"" Latitude=""40.742716""
            Radius=""16"" FullAddress=""6th Ave, ניו יורק, ארצות הברית"" 
            Email=""hi@noproblemppc.com"">
              <SelectedAreas />
            </Areas>";
        */

        /*
        strWorkAreas= @"<Areas>
        <Area IsMoreSelected=""true"" RegistrationStage=""6"" 
        Longitude=""-73.99293419999998"" Latitude=""40.742716"" 
        Radius=""ALL"" FullAddress=""6th Ave, ניו יורק, ארצות הברית"" 
        Email=""hi@noproblemppc.com"">
          <SelectedAreas>
            <Area RegionId=""ALL"" Name=""ALL"" Level=""ALL"" Selected=""true"" />
            <Area RegionId=""02b5cbe3-3a2a-e111-bd7e-001517d10f6e"" Name=""New York"" Level=""3"" Selected=""false"" />
            <Area RegionId=""cf41637c-392a-e111-bd7e-001517d10f6e"" Name=""Manhattan"" Level=""2"" Selected=""false"" />
            <Area RegionId=""f031ce5b-392a-e111-bd7e-001517d10f6e"" Name=""NY"" Level=""1"" Selected=""false"" />
          </SelectedAreas>
        </Area>

        <Area IsMoreSelected=""true"" RegistrationStage=""6"" 
        Longitude=""-75.99293419999998"" Latitude=""43.742716"" 
        Radius=""32"" FullAddress=""סין"" 
        Email=""hi@noproblemppc.com"">
          <SelectedAreas>
            <Area RegionId=""ALL"" Name=""ALL"" Level=""ALL"" Selected=""true"" />
            <Area RegionId=""02b5cbe3-3a2a-e111-bd7e-001517d10f6e"" Name=""New York"" Level=""3"" Selected=""false"" />
            <Area RegionId=""cf41637c-392a-e111-bd7e-001517d10f6e"" Name=""Manhattan"" Level=""2"" Selected=""false"" />
            <Area RegionId=""f031ce5b-392a-e111-bd7e-001517d10f6e"" Name=""NY"" Level=""1"" Selected=""false"" />
          </SelectedAreas>
        </Area>

        <Area IsMoreSelected=""true"" RegistrationStage=""6"" 
        Longitude=""-75.99293419999998"" Latitude=""43.742716"" 
        Radius=""4.800000000000001"" FullAddress=""סין"" 
        Email=""hi@noproblemppc.com"">
          <SelectedAreas>
            <Area RegionId=""ALL"" Name=""ALL"" Level=""ALL"" Selected=""true"" />
            <Area RegionId=""02b5cbe3-3a2a-e111-bd7e-001517d10f6e"" Name=""New York"" Level=""3"" Selected=""false"" />
            <Area RegionId=""cf41637c-392a-e111-bd7e-001517d10f6e"" Name=""Manhattan"" Level=""2"" Selected=""false"" />
            <Area RegionId=""f031ce5b-392a-e111-bd7e-001517d10f6e"" Name=""NY"" Level=""1"" Selected=""false"" />
          </SelectedAreas>
        </Area>

        
        </Areas>";
        */


        strWorkAreas = @"<?xml version=""1.0"" encoding=""utf-16""?>
        <Areas xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
          <Area Longitude=""-112.07403729999999"" Latitude=""33.4483771"" Radius=""6dffe2a3-3a2a-e111-bd7e-001517d10f6e"" FullAddress=""Phoenix, AZ, United States"">
            <SelectedAreas>
              <LargeArea RegionId=""6dffe2a3-3a2a-e111-bd7e-001517d10f6e"" Name=""Phoenix"" Level=""3"" />
              <LargeArea RegionId=""f23a637c-392a-e111-bd7e-001517d10f6e"" Name=""Maricopa"" Level=""2"" />
              <LargeArea RegionId=""cf31ce5b-392a-e111-bd7e-001517d10f6e"" Name=""AZ"" Level=""1"" />
            </SelectedAreas>
          </Area>
        </Areas>";




        XDocument doc = XDocument.Parse(strWorkAreas);


        if (doc.Element("WorkArea") != null)
            return;
        if (!doc.Element("Areas").HasElements)
        {
            // if there isn't childs it is the beginning
            index++;
            return;
        }

        /*
        index++;
        return;
        */

        // must be at least one area from fulfill business detials in step 3
        foreach (XElement elem in doc.Element("Areas").Elements("Area"))
        {
            index++;

            string radius = elem.Attribute("Radius").Value;//.Replace(',', '.');


            //  double _radius = Math.Round(double.Parse(radius), 2);
            string selectedArea;
            selectedArea = radius;
            decimal _radiusM = (Utilities.IsGUID(radius) || radius == "ALL") ? 35m : decimal.Parse(radius.Replace(',', '.'));
            double _radius = (Utilities.IsGUID(radius) || radius == "ALL") ? 35 : double.Parse(radius.Replace(',', '.')) / 1.6; // in the xml , crm the radius is real the radius and not miles
            //  double _radius = (double)_radiusM;
            _radius = Math.Round(_radius, 2);
            radius = _radius.ToString();

            string lat = elem.Attribute("Latitude").Value.Replace(',', '.');
            string lng = elem.Attribute("Longitude").Value.Replace(',', '.');

            string FullAddress = elem.Attribute("FullAddress").Value;

            XElement namedAreas = elem.Element("SelectedAreas");

            List<LocationDataNamedRegions> listNamedRegions = new List<LocationDataNamedRegions>();
            if (namedAreas.HasElements)
            {
                int indexNamedRegions = 0;
                foreach (XElement _xel in namedAreas.Elements("LargeArea"))
                {

                    string Level = _xel.Attribute("Level").Value;
                    string Name = _xel.Attribute("Name").Value;
                    string RegionId = _xel.Attribute("RegionId").Value;

                    LocationDataNamedRegions namedRegion = new LocationDataNamedRegions();
                    namedRegion.RegionId = RegionId;
                    namedRegion.Name = Name;
                    namedRegion.Level = Level;

                    listNamedRegions.Add(namedRegion);

                    indexNamedRegions++;

                }
            }

            LocationData ld = new LocationData();

            ld.Radius = _radiusM;
            ld.Lat = decimal.Parse(lat);
            ld.Lng = decimal.Parse(lng);
            ld.RegionId = Region_ID;
            ld.SelectAll = Selected_All;
            ld.FullAddress = FullAddress;
            ld.namedRegions = listNamedRegions.ToArray();
            ld.SelectedArea = selectedArea;
            listLocatinData.Add(ld);

            LocationDataV = ld;
        }






    }

    private void LoadPanelLocation(bool isPostback)
    {
        //index++;        

        pnlAddLocation.Controls.Clear();

        HtmlGenericControl pnlAddServicesTitle = new HtmlGenericControl("div");
        pnlAddServicesTitle.Attributes["class"] = "pnlAddServicesTitle";

        pnlAddLocation.Controls.Add(pnlAddServicesTitle);


        for (int i = 0; i < index; i++)
        {

            /* example how it looks in html
             <div class="inputContainer">         
    
                <input id="searchTextField1" type="text" size="50"   class="textActive" place_holder="<%# LocationPlaceHplder %>"
                onkeyup="ChkLocationOnKeyPress(this, event);" onkeydown="return (event.keyCode!=13);" HolderClass="place-holder"  />
   

                <div class="inputValid" style="display:none;"  id="div_LocationSmile1">
                
                </div>
                <div class="inputError" style="display:none;"  id="div_LocationError1">
               
                </div>
                <div class="inputComment" style="display:none;" id="div_LocationComment1">
                        <%= LocationPlaceHplder %>
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

                <asp:DropDownList runat="server" ID="ddlCoverArea1" class="coverArea">
                    <asp:ListItem>0.5mi</asp:ListItem>
                    <asp:ListItem>1mi</asp:ListItem>
                    <asp:ListItem>2mi</asp:ListItem>
                    <asp:ListItem>3mi</asp:ListItem>
                    <asp:ListItem>10mi</asp:ListItem>
                    <asp:ListItem>20mi</asp:ListItem>
                    <asp:ListItem>30mi</asp:ListItem>

                </asp:DropDownList>
    
                <asp:Label runat="server" ID="lblZipCode" class="numberZipCodes">100</asp:Label>
               <div class="clear"></div>
    
            </div>
            */

            /* must set here in page load and not in click events to make view state for the controls !!!*/

            //if (!arrayWhoStay.Contains(i)) // because ther isn't value zero all time in the beginnig of loop add max number to arrayWhoStay
            arrayWhoStay.Add(Convert.ToInt32(GetMaxValue(arrayWhoStay) + 1));

            HtmlGenericControl inputCategoryContainer = new HtmlGenericControl("div");
            inputCategoryContainer.ID = "inputCategoryContainer" + arrayWhoStay[i];
            inputCategoryContainer.Attributes["class"] = "inputContainer";


            TextBox tb = new TextBox();
            tb.ID = "searchTextField" + arrayWhoStay[i];
            tb.CssClass = "textActive";
            tb.Attributes.Add("place_holder", LocationPlaceHplder);
            tb.Attributes.Add("holderClass", "place-holder");
            tb.Attributes.Add("type", "text");
            tb.Attributes.Add("onkeyup", "ChkLocationOnKeyPress(this, event);");
            tb.Attributes.Add("onkeydown", "return (event.keyCode!=13);");


            if (!isPostback)
            {
                if (listLocatinData.Count() > 0)
                {
                    tb.Text = listLocatinData[i].FullAddress;
                }

            }


            HtmlGenericControl serviceSmile = new HtmlGenericControl("div");
            serviceSmile.ID = "div_LocationSmile" + arrayWhoStay[i];
            serviceSmile.Attributes["class"] = "inputValid";
            serviceSmile.Attributes.Add("style", "display:none;");

            HtmlGenericControl serviceError = new HtmlGenericControl("div");
            serviceError.ID = "div_LocationError" + arrayWhoStay[i];
            serviceError.Attributes["class"] = "inputError";
            serviceError.Attributes.Add("style", "display:none;");

            HtmlGenericControl serviceComment = new HtmlGenericControl("div");
            serviceComment.ID = "div_LocationComment" + arrayWhoStay[i];
            serviceComment.Attributes["class"] = "inputComment";
            serviceComment.Attributes.Add("style", "display:none;");
            serviceComment.InnerText = LocationPlaceHplder;


            HtmlGenericControl chatBubbleArrowBorder = new HtmlGenericControl("div");
            chatBubbleArrowBorder.ID = "chatBubbleArrowBorder" + arrayWhoStay[i];
            chatBubbleArrowBorder.Attributes["class"] = "chat-bubble-arrow-border";

            serviceComment.Controls.Add(chatBubbleArrowBorder);

            HtmlGenericControl chatBubbleArrow = new HtmlGenericControl("div");
            chatBubbleArrow.ID = "chatBubbleArrow" + arrayWhoStay[i];
            chatBubbleArrow.Attributes["class"] = "chat-bubble-arrow";

            serviceComment.Controls.Add(chatBubbleArrow);

            inputCategoryContainer.Controls.Add(tb);
            inputCategoryContainer.Controls.Add(serviceSmile);
            inputCategoryContainer.Controls.Add(serviceError);
            inputCategoryContainer.Controls.Add(serviceComment);

            DropDownList ddlCoverArea = new DropDownList();


            ddlCoverArea.ID = "ddlCoverArea" + arrayWhoStay[i];

            ddlCoverArea.Attributes.Add("class", "coverArea");

            ListItem listItemCoverArea = new ListItem();
            string hiddenCoverArea = "";




            try
            {
                if (listLocatinData[i].namedRegions != null)
                {
                    for (int indexNamedRegions = 0; indexNamedRegions < listLocatinData[i].namedRegions.Length; indexNamedRegions++)
                    {
                        listItemCoverArea = new ListItem();
                        listItemCoverArea.Value = listLocatinData[i].namedRegions[indexNamedRegions].RegionId;
                        listItemCoverArea.Text = listLocatinData[i].namedRegions[indexNamedRegions].Name;
                        ddlCoverArea.Items.Add(listItemCoverArea);
                        hiddenCoverArea += listItemCoverArea.Value + "$";
                    }

                }
            }

            catch (Exception Ex)
            {
                // after post back there isn't alreay details
                // there isn't 
            }




            listItemCoverArea = new ListItem();

            listItemCoverArea.Value = "0.5";
            listItemCoverArea.Text = "0.5mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "1";
            listItemCoverArea.Text = "1mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "2";
            listItemCoverArea.Text = "2mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "3";
            listItemCoverArea.Text = "3mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "10";
            listItemCoverArea.Text = "10mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "20";
            listItemCoverArea.Text = "20mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            listItemCoverArea = new ListItem();
            listItemCoverArea.Value = "30";
            listItemCoverArea.Text = "30mi";
            ddlCoverArea.Items.Add(listItemCoverArea);
            hiddenCoverArea += listItemCoverArea.Value + "$";

            try
            {
                ListItem li = ddlCoverArea.Items.FindByValue(listLocatinData[i].SelectedArea);
                if (li != null)
                    li.Selected = true;
            }
            catch (Exception Ex)
            {
                // after post back there isn't alreay details
                // there isn't 
            }

            inputCategoryContainer.Controls.Add(ddlCoverArea);

            Label lblZipCode = new Label();
            lblZipCode.ID = "lblZipCode" + arrayWhoStay[i];
            lblZipCode.CssClass = "numberZipCodes";
            lblZipCode.Text = "100";

            inputCategoryContainer.Controls.Add(lblZipCode);


            if (i > 0)
            {
                /*
                    <a href=""><i class="fa fa-times"></i> fa-times</a>
                */

                LinkButton lb = new LinkButton();
                lb.ID = "minus" + arrayWhoStay[i];
                lb.Command += new CommandEventHandler(lb_click_minus);
                lb.CommandArgument = arrayWhoStay[i].ToString();
                lb.Text = "<i class='fa fa-times fa-2'></i>";

                inputCategoryContainer.Controls.Add(lb);
                ////ScriptManager1.RegisterAsyncPostBackControl(lb);
            }

            HtmlGenericControl divClear = new HtmlGenericControl("div");
            divClear.ID = "clear" + arrayWhoStay[i];
            divClear.Attributes["class"] = "clear";


            inputCategoryContainer.Controls.Add(divClear);


            HiddenField hiddenField = new HiddenField();
            hiddenField.ID = "hf_areas" + arrayWhoStay[i];
            //Response.Write("hidden " + hiddenField.Value);
            inputCategoryContainer.Controls.Add(hiddenField);


            pnlAddLocation.Controls.Add(inputCategoryContainer);

            try
            {
                SetFullAddress(listLocatinData[i].FullAddress, (i + 1));
            }

            catch
            {
                // add new will fell here cause there isn't class locationData for it
            }

            if (i == 0)
                SetMap(listLocatinData[i].Radius.ToString(), listLocatinData[i].Lat.ToString(), listLocatinData[i].Lng.ToString(), 1);

        }




    }

    protected void lbAddButton_Click(object sender, EventArgs e)
    {
        bindMapToTextBox();


        string _script = "initializeMulti(" + index + ")";
        //_script = "alert('sfdfdf');";
        /*
        Page.ClientScript.RegisterStartupScript(this.GetType(), "initializeMulti", _script, true);
        */

        ScriptManager.RegisterStartupScript(this, this.GetType(), "initializeMulti", _script, true);


        updatePanel1.Update();

        /*
        string _script = "initializeMulti(" + index + ")";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "initializeMulti", _script, true);
        */
    }

    protected void lb_click_minus(object sender, CommandEventArgs e)
    {
        /************  remove the specific control *************/
        Control myControl1 = pnlAddLocation.FindControl("inputCategoryContainer" + e.CommandArgument);
        pnlAddLocation.Controls.Remove(myControl1);

        Control myControlClear1 = pnlAddLocation.FindControl("clear" + e.CommandArgument);
        pnlAddLocation.Controls.Remove(myControlClear1);


        index--;

        arrayWhoStay.Remove(Convert.ToInt32(e.CommandArgument));

        /************  remove the last control added in page load *************/
        pnlAddLocation.Controls.RemoveAt(pnlAddLocation.Controls.Count - 1);
        index--;

        arrayWhoStay.RemoveAt(arrayWhoStay.Count - 1);

        foreach (Control cn in pnlAddLocation.Controls)
        {

        }


        bindMapToTextBox();


        this.DataBind();
        updatePanel1.Update();

    }

    private void bindMapToTextBox()
    {
        for (int i = 0; i < index; i++)
        {
            string _script = "initializeMap(" + arrayWhoStay[i] + ");";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "initializeMap" + arrayWhoStay[i], _script, true);
        }
    }

    protected void btn_SetLocation_Click(object sender, EventArgs e)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        string result = "";
        decimal _lat, _lng, _radius;
        if (!decimal.TryParse(hf_lat.Value, out _lat) || !decimal.TryParse(hf_lng.Value, out _lng) || !decimal.TryParse(hf_radius.Value, out _radius))
        {
            ////Update_Faild();
            return;
        }
        Guid RegionId = Guid.Empty;
        bool SelectAll = false;
        string LargePlace = hf_LargePlace.Value;
        if (Utilities.IsGUID(LargePlace))
            RegionId = new Guid(LargePlace);
        else if (LargePlace == "all")
            SelectAll = true;

        if (IfSameDetails(_radius, _lng, _lat, RegionId, SelectAll))
        {
            /*
            string _script = "top.SetServerComment('" + HttpUtility.JavaScriptStringEncode("Nothing has been changed. It's all the same info as before.") + "'); location.href='" + ResolveUrl("~/Management/LocationMap.aspx?comment=true") + "';";
            ClientScript.RegisterStartupScript(this.GetType(), "SetComment", _script, true);
            */

            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem("Nothing has been changed. It's all the same info as before.");
            LoadLocation();

            return;
        }

        try
        {
            result = _supplier.SetCitiesByCoordinate(new Guid("3E796B9A-7A51-493C-A371-FE5B5A63BC63"), _radius, _lng, _lat, hf_FullAddress.Value, "", "", RegionId, SelectAll, false, "", false);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ////Update_Faild();
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);

            LoadLocation();

            return;
        }
        XDocument doc = XDocument.Parse(result);
        if (doc.Element("SetCitiesByCoordinate").Element("Error") != null)
        {
            ////Update_Faild();
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);

            LoadLocation();
            return;
        }
        string _status = doc.Element("SetCitiesByCoordinate").Element("Status").Value;
        if (_status == "Success")
        {
            /*
            if (userManagement.IsPublisher() && IsFirstTime)
                ClientScript.RegisterStartupScript(this.GetType(), "UpdateSuccessStep4", "UpdateSuccessStep4();", true);
            else
                ClientScript.RegisterStartupScript(this.GetType(), "SetComment", "top.SetServerComment('Update success');", true);
            */


        }
        else
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "SetComment", "top.SetServerComment('Update faild');", true);
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);

            LoadLocation();
        }
        LoadLocation();
    }

    public int GetMaxValue(ArrayList arrList)
    {
        ArrayList copyList = new ArrayList(arrList);

        if (copyList == null || copyList.Count == 0)
        {
            return 0;
        }

        else
        {
            copyList.Sort();

            copyList.Reverse();

            return Convert.ToInt32(copyList[0]);
        }

    }

    bool IfSameDetails(decimal _radius, decimal _lng, decimal _lat, Guid region_id, bool select_all)
    {
        LocationData ld = LocationDataV;
        if (ld == null)
            return false;
        if (ld.Lat != _lat)
            return false;
        if (ld.Lng != _lng)
            return false;
        if (ld.RegionId != region_id)
            return false;
        if (ld.SelectAll != select_all)
            return false;
        if (region_id != Guid.Empty)
            return true;
        else
            if (ld.Radius != _radius)
                return false;
        return true;

    }

    public Guid Region_ID
    {
        get { return _RegionId; }
    }
    public bool Selected_All
    {
        get { return _selectAll; }
    }
    protected string GetLargeAreas
    {
        get { return ResolveUrl("~") + "PPC/MapService.asmx/GetLargeAreas"; }
    }
    public string LocationPlaceHplder
    {
        //       get { return "Please enter a location:"; }
        get { return (string)ViewState["LocationPlaceHplder"]; }
        set { ViewState["LocationPlaceHplder"] = value; }
    }
    public string SelectFromList
    {
        //       get { return "Select location from the list"; }
        get { return (string)ViewState["SelectFromList"]; }
        set { ViewState["SelectFromList"] = value; }
    }

    public string FieldMissing
    {
        get { return (string)ViewState["FieldMissing"]; }
        set { ViewState["FieldMissing"] = value; }
    }
    /*
    public string ChooseLargerArea
    {
        get { return (string)ViewState["ChooseLargerArea"]; }
        set { ViewState["ChooseLargerArea"] = value; }
    }
     * */
    public string ServerError
    {
        get { return (string)ViewState["ServerError"]; }
        set { ViewState["ServerError"] = value; }
    }


    protected int index
    {
        get
        {
            return (ViewState["index"] == null ? 0 : (int)ViewState["index"]);
        }
        set
        {

            ViewState["index"] = value;
        }
    }


    protected ArrayList arrayWhoStay
    {
        get
        {
            return (ViewState["arrayWhoStay"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoStay"]);
        }
        set
        {

            ViewState["arrayWhoStay"] = value;
        }
    }

    protected ArrayList arrayWhoInInit
    {
        get
        {
            return (ViewState["arrayWhoInInit"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoInInit"]);
        }
        set
        {

            ViewState["arrayWhoInInit"] = value;
        }
    }


    bool IsFirstTime
    {
        get { return (ViewState["IsFirstTime"] == null) ? false : (bool)ViewState["IsFirstTime"]; }
        set { ViewState["IsFirstTime"] = value; }
    }
    LocationData LocationDataV
    {
        get { return (ViewState["LocationData"] == null) ? null : (LocationData)ViewState["LocationData"]; }
        set { ViewState["LocationData"] = value; }
    }

    public string getArrayWhoStay(int i)
    {
        return "werwer";
    }


    List<LocationData> listLocatinData
    {
        get { return (ViewState["listLocatinData"] == null) ? null : (List<LocationData>)ViewState["listLocatinData"]; }
        set { ViewState["listLocatinData"] = value; }
    }

}