﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_RequestApproved : RegisterPage, IsInProcess
{
    protected void Page_Load(object sender, EventArgs e)
    {
        loadMaster();
    }

    private void loadMaster()
    {
        Master.SetStatusBar("statusBar3");
    }
    protected void lb_GetStarted_Click(object sender, EventArgs e)
    {
        UserMangement um = PpcSite.SupplierLogin(PhoneNum, Password);
        if (um == null)
        {
            GeneralServerError();
            return;
        }
        Session["UserGuid"] = um;
        ClearDetails();
        Response.Redirect(ResolveUrl("~/Management/overview.aspx"));
    }
}