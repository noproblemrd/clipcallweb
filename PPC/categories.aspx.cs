﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Web.UI.HtmlControls;
using System.Collections;

public partial class PPC_categories : RegisterPage2, IsInProcess
{
    protected string GetServerError;
    
  
    protected void Page_Load(object sender, EventArgs e)
    {
        
        ScriptManager1.RegisterAsyncPostBackControl(lbAddButton);
        //ScriptManager1.RegisterAsyncPostBackControl(btn_SendService); 
        
        if (!IsPostBack)
        {
            arrayWhoStay = new ArrayList();
            arrayWhoInInit = new ArrayList();

            LoadMaster();
            LoadPageText();
            LoadService();            
            LoadPanelServices(false);
        }

        else
        {           
            index++;
            LoadPanelServices(true);
        }        

        Page.DataBind();       
       
    }

    private void LoadPanelServices(bool isPostback)
    {
        //index++;        
        
        pnlAddServices.Controls.Clear();

        HtmlGenericControl pnlAddServicesTitle = new HtmlGenericControl("div");
        pnlAddServicesTitle.Attributes["class"] = "pnlAddServicesTitle";
        pnlAddServicesTitle.InnerText = "Service category";

        pnlAddServices.Controls.Add(pnlAddServicesTitle);


        for (int i = 0; i < index; i++)
        {

            /* example how it looks in html
              <div class="inputCategoryContainer" id="inputCategoryContainer1">
                  <input type="text" id="pt0" style="display: inline;" class="_Heading textActive place-holder ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
                  <input type="text" holderclass="place-holder" place_holder="e.g., plumber, computer technician, etc." class="_Heading textActive ui-autocomplete-input" id="txt_Category1" name="ctl00$ContentPlaceHolder1$txt_Category1" style="display: none;" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
                  <div style="display:none;" class="inputValid" id="div_ServiceSmile1"></div>
                  <div style="display:none;" class="inputError" id="div_ServiceError1"></div>
                  <div style="display:none;" class="inputComment" id="div_ServiceComment1">e.g., plumber, computer technician, etc.
                      <div class="chat-bubble-arrow-border" id="chatBubbleArrowBorder1"></div>
                      <div class="chat-bubble-arrow" id="chatBubbleArrow1"></div>
                 </div>
                 <div class="clear" id="clear1"></div>
             </div>
            */

            /* must set here in page load and not in click events to make view state for the controls !!!*/

            //if (!arrayWhoStay.Contains(i)) // because ther isn't value zero all time in the beginnig of loop add max number to arrayWhoStay
            arrayWhoStay.Add(Convert.ToInt32(GetMaxValue(arrayWhoStay) + 1));

            HtmlGenericControl inputCategoryContainer = new HtmlGenericControl("div");
            inputCategoryContainer.ID = "inputCategoryContainer" + arrayWhoStay[i];
            inputCategoryContainer.Attributes["class"] = "inputCategoryContainer";

           
            TextBox tb = new TextBox();
            tb.ID = "txt_Category" + arrayWhoStay[i];
            tb.CssClass = "_Heading textActive";
            tb.Attributes.Add("place_holder", ServicePlaceHolder);
            tb.Attributes.Add("holderClass", "place-holder");
            if (!isPostback)
            {
                if (arrayWhoInInit.Count>0)
                    tb.Text = arrayWhoInInit[i].ToString();
            }

            HtmlGenericControl serviceSmile = new HtmlGenericControl("div");
            serviceSmile.ID = "div_ServiceSmile" + arrayWhoStay[i];
            serviceSmile.Attributes["class"] = "inputValid";
            serviceSmile.Attributes.Add("style", "display:none;");                      

            HtmlGenericControl serviceError = new HtmlGenericControl("div");
            serviceError.ID = "div_ServiceError" + arrayWhoStay[i];
            serviceError.Attributes["class"] = "inputError";
            serviceError.Attributes.Add("style", "display:none;");

            HtmlGenericControl serviceComment = new HtmlGenericControl("div");
            serviceComment.ID = "div_ServiceComment" + arrayWhoStay[i];
            serviceComment.Attributes["class"] = "inputComment";
            serviceComment.Attributes.Add("style", "display:none;");
            serviceComment.InnerText = ServicePlaceHolder;

            HtmlGenericControl chatBubbleArrowBorder = new HtmlGenericControl("div");
            chatBubbleArrowBorder.ID = "chatBubbleArrowBorder" + arrayWhoStay[i];
            chatBubbleArrowBorder.Attributes["class"] = "chat-bubble-arrow-border";

            serviceComment.Controls.Add(chatBubbleArrowBorder);

            HtmlGenericControl chatBubbleArrow = new HtmlGenericControl("div");
            chatBubbleArrow.ID = "chatBubbleArrow" + arrayWhoStay[i];
            chatBubbleArrow.Attributes["class"] = "chat-bubble-arrow";

            serviceComment.Controls.Add(chatBubbleArrow);

            inputCategoryContainer.Controls.Add(tb);
            inputCategoryContainer.Controls.Add(serviceSmile);
            inputCategoryContainer.Controls.Add(serviceError);
            inputCategoryContainer.Controls.Add(serviceComment);

            if (i > 0)
            {
                /*
                 <a href=""><i class="fa fa-times"></i> fa-times</a>
                */

                LinkButton lb = new LinkButton();
                lb.ID = "minus" + arrayWhoStay[i];
                lb.Command += new CommandEventHandler(lb_click_minus);
                lb.CommandArgument = arrayWhoStay[i].ToString();
                lb.Text = "<i class='fa fa-times fa-2'></i>";

                inputCategoryContainer.Controls.Add(lb);
                ScriptManager1.RegisterAsyncPostBackControl(lb);
            }


            HtmlGenericControl divClear = new HtmlGenericControl("div");
            divClear.ID = "clear" + arrayWhoStay[i];
            divClear.Attributes["class"] = "clear";

            inputCategoryContainer.Controls.Add(divClear);

            pnlAddServices.Controls.Add(inputCategoryContainer);

           

        }

        
    }

    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

        masterPage.SetStatusBar("past", "current", "pre");
        masterPage.SetBlackMenu();
       
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        FieldMissing = Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;


        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "DetailsService"
                          select x).FirstOrDefault();
        XElement Service_element = xelem.Element("Service").Element("Validated");
        ServicePlaceHolder = Service_element.Element("Instruction").Element("General").Value;


        xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                 where x.Attribute("name").Value == "MasterPage"
                 select x).FirstOrDefault();
        
        invalidData = xelem.Element("FormMessages").Element("autoCompleteMissing").Value;

        /*
        ChooseCategory = Service_element.Element("Instruction").Element("Category").Value;
        ChooseSkill = Service_element.Element("Instruction").Element("Skill").Value;
        TrialV = Service_element.Element("Instruction").Element("Trial").Value;
        ErrorService = Service_element.Element("Error").Value;
        lbl_ServiceInstruction.DataBind();
        */
    }

    private bool LoadService()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        string result = "";
        try
        {
            //result = _supplier.GetSupplierExpertise(siteSetting.GetSiteID, SupplierId.ToString());
            result = _supplier.GetSupplierExpertise(siteSetting.GetSiteID, SupplierId.ToString());

            /*
             <SupplierExpertise RegistrationStage="2" SubscriptionPlan="LEAD_BOOSTER_AND_FEATURED_LISTING">
              <PrimaryExpertise Name="Devil Worshipper" ID="f6f38910-1058-e311-8fb9-001517d10f6e" Certificate="False" />
             </SupplierExpertise>
            */

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);

            
            masterPage.ServerProblem(GetServerError);           

            return false;
        }

        XDocument xdoc = XDocument.Parse(result);
        if (xdoc.Element("SupplierExpertise") == null || xdoc.Element("SupplierExpertise").Element("Error") != null)
        {            
            masterPage.ServerProblem(GetServerError);    
            return false;
        }

        

        if (!xdoc.Element("SupplierExpertise").HasElements)
        {
            index=1; 
            return false;
        }

       
        

        // for each child category
        string exp_name;
        foreach (XElement childCategory in xdoc.Element("SupplierExpertise").Elements("PrimaryExpertise"))
        {
            exp_name = childCategory.Attribute("Name").Value;
            arrayWhoInInit.Add(exp_name);            
            index++;
        }


        masterPage.SetPlanType(xdoc.Element("SupplierExpertise").Attribute("SubscriptionPlan").Value);
        


        //string exp_name = xdoc.Element("SupplierExpertise").Element("PrimaryExpertise").Attribute("Name").Value;
        //txt_Category.Text = exp_name;
        //  txt_Category.CssClass = txt_Category.CssClass.Replace("place-holder", "").Trim();

        return true;
    }

    void SetService()
    {
        string _service = hf_Heading.Value.Substring(0, hf_Heading.Value.Length-1); // subtracr the comma in the end
        
        if (string.IsNullOrEmpty(_service))
        {
            /*
            HeadingProblem(_service);
            return;
            */
        }

        string[] headingSplitComma;
        headingSplitComma=_service.Split(',');

        List<string> goodHeading = new List<string>();
        PpcSite _head = PpcSite.GetCurrent();
        Guid _id;
 
        for (int i = 0; i < headingSplitComma.Length; i++)
        {
            _id = _head.GetHeadinGuid(headingSplitComma[i]);
            if (_id == Guid.Empty)
            {
                ////HeadingProblem(_service); 
            }
            
            else if(headingSplitComma[i].ToLower()=="computer repair" )
            {
               
            }
            
            else
            {
                goodHeading.Add(_id.ToString());
            }

        }

        String[] myArr = (String[])goodHeading.ToArray();

        if (myArr.Length == 0)
        {            
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(invalidData);
        }

        else
        {
            if (Utilities.SaveHeadings(SupplierId.ToString(), myArr, siteSetting.GetUrlWebReference, SupplierId))
            {
                Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
                masterPage.stepRedirect(5); // go to location                 
                //return;
            }
            else
            {
                Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
                masterPage.ServerProblem(GetServerError);
                //ClientScript.RegisterStartupScript(this.GetType(), "StopButtonAnimation", "stopActiveButtonAnim();", true);
            }
        }

        /*
        a_categories.Style[HtmlTextWriterStyle.Display] = "none";
        a_FreeSearch.Style[HtmlTextWriterStyle.Display] = "none";
        div_HeadingCategoryTable.Style[HtmlTextWriterStyle.Display] = "block";
        div_HeadingText.Style[HtmlTextWriterStyle.Display] = "block";

        SetCategoryList();
        SetServiceError();
        */
    }

    protected void btn_SendService_click(object sender, EventArgs e)
    {
        /************  remove the last control added in page load *************/
        pnlAddServices.Controls.RemoveAt(pnlAddServices.Controls.Count - 1);
        index--;

        SetService();
    }

    protected void lb_click_minus(object sender, CommandEventArgs e)
    {      
        /************  remove the specific control *************/
        Control myControl1 = pnlAddServices.FindControl("inputCategoryContainer" + e.CommandArgument);
        pnlAddServices.Controls.Remove(myControl1);

        Control myControlClear1 = pnlAddServices.FindControl("clear" + e.CommandArgument);
        pnlAddServices.Controls.Remove(myControlClear1);


        index--;              
        
        arrayWhoStay.Remove(Convert.ToInt32(e.CommandArgument));
       
        /************  remove the last control added in page load *************/
        pnlAddServices.Controls.RemoveAt(pnlAddServices.Controls.Count-1);
        index--;
        
        arrayWhoStay.RemoveAt(arrayWhoStay.Count-1);

        foreach(Control cn in pnlAddServices.Controls)
        {

        }

        updatePanel1.Update();
       
    }

    public int GetMaxValue(ArrayList arrList)
    {
        ArrayList copyList = new ArrayList(arrList);

        if (copyList == null || copyList.Count == 0)
        {
            return 0;
        }

        else
        {
            copyList.Sort();

            copyList.Reverse();

            return Convert.ToInt32(copyList[0]);
        }

    }

   

    protected void lbAddButton_Click(object sender, EventArgs e)
    {
        updatePanel1.Update();
    }


    protected string GetIsHeadingService
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/Is_Heading"; }
    }

    protected string ServicePlaceHolder
    {
        get { return (string)ViewState["ServicePlaceHolder"]; }
        set { ViewState["ServicePlaceHolder"] = value; }
    }

    protected string GetHeadingServiceList
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/GetHeadingList"; }
    }

    protected string FieldMissing
    {
        get { return (string)ViewState["FieldMissing"]; }
        set { ViewState["FieldMissing"] = value; }
    }

  
    protected int index
    {
        get
        {
            return (ViewState["index"] == null ? 0 : (int)ViewState["index"]);
        }
        set
        {

            ViewState["index"] = value;
        }
    }

    
    protected ArrayList arrayWhoStay
    {
        get
        {
            return (ViewState["arrayWhoStay"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoStay"]);
        }
        set
        {

            ViewState["arrayWhoStay"] = value;
        }
    }

    protected ArrayList arrayWhoInInit
    {
        get
        {
            return (ViewState["arrayWhoInInit"] == null ? new ArrayList() : (ArrayList)ViewState["arrayWhoInInit"]);
        }
        set
        {

            ViewState["arrayWhoInInit"] = value;
        }
    }


    protected string invalidData
    {
        get { return (string)ViewState["invalidData"]; }
        set { ViewState["invalidData"] = value; }
    }
   
  
}