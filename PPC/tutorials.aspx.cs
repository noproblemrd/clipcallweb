﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_tutorials : PageSetting
{
    protected string url;
    protected string root;

    protected void Page_Load(object sender, EventArgs e)
    {
        root = ResolveUrl("~");

        if (Request.Url.Host == "localhost")
        {
            url = Request.Url.Host + ":" + Request.Url.Port + root;
        }

        else if (Request.Url.Host == "qa.ppcnp.com")
        {
            url = "http://" + Request.Url.Host + root;
        }

        else
        {
            url = "http://www.noproblem.me" + root;
        }

        getCompanyLink();
    }


    private void getCompanyLink()
    {
        //<a href="<%#root%>business/<%#Utilities.cleanSpecialCharsUrlFreindly3(Eval("Name").ToString().Trim())%>/<%#Eval("Id")%>"><%#Utilities.setfirstCapital(Eval("Name").ToString().Trim())%></a>

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfGetBusinessProfileResponse resultOfGetBusinessProfileResponse = new WebReferenceSupplier.ResultOfGetBusinessProfileResponse();

        try
        {
            resultOfGetBusinessProfileResponse = _supplier.GetBusinessProfile(new Guid(GetGuidSetting()));
            linkBusiness.HRef = url + "business/" + Utilities.cleanSpecialCharsUrlFreindly3(resultOfGetBusinessProfileResponse.Value.Name.Trim()) + "/" + GetGuidSetting();

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex);
        }
    }
}