﻿<%@ WebHandler Language="C#" Class="inviteMorePro" %>

using System;
using System.Web;

public class inviteMorePro : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "application/json";
        //context.Response.Write("Hello World");

        string siteId = context.Request["siteId"];
        string supplierId = context.Request["supplierId"];
        string strResult = "";
        
        try
        {
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, siteId);
            WebReferenceSupplier.CreateInvitationRequest request = new WebReferenceSupplier.CreateInvitationRequest();

            WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
            result=supplier.RequestMoreInvites(new Guid(supplierId));

            if (result.Type == WebReferenceSupplier.eResultType.Success)
            {
                strResult = "success";
            }

            else
            {
                strResult = "fauilre";
            }
        }


        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteId);
            strResult = "server problem";            
        }

        var resultParams = new { result = strResult };
        
        System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();        
        
        context.Response.Write(jss.Serialize(resultParams));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}