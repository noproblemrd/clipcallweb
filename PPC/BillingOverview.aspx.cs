﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_BillingOverview :PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            LoadDetails();
            LoadPackages();
            LoadTransaction();
            LoadActivity();
        }
    }

    private void LoadActivity()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfGetAccountActivityResponse result;
        try
        {
            result = _supplier.GetAccountActivity(new Guid(GetGuidSetting()));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        lbl_LeadsAmount.Text = result.Value.LeadCount.ToString();
        lbl_LeadsSpent.Text = GetMoneyToDisplay(result.Value.LeadMoney);
        lbl_RefundsAmount.Text = result.Value.RefundCount.ToString();
        lbl_RefundsSpent.Text = GetMoneyToDisplay(result.Value.RefundMoney);
        lbl_friendsAmount.Text = result.Value.EarnedCount.ToString();
        lbl_friendsEarned.Text = GetMoneyToDisplay(result.Value.EarnedMoney);
        lbl_BonusCreditAmount.Text = result.Value.BonusCount.ToString();
        lbl_BonusCreditEarned.Text = GetMoneyToDisplay(result.Value.BonusMoney);
        lbl_TotalSpent.Text = GetMoneyToDisplay(result.Value.TotalUsed);
        lbl_BonusEarnedEarned.Text = GetMoneyToDisplay(result.Value.TotalBonus);
        
    }
    private void LoadPackages()
    {
        List<Package> list = (PackageV == null) ?
            Package.GetPackages(siteSetting.GetUrlWebReference, new Guid(GetGuidSetting())) :
            PackageV;
        foreach (Package p in list)
        {
            switch (p.Name)
            {
                case ("bronze"): lbl_AmountBronze.Text = string.Format(GetNumberFormat, p.FromAmount);
                    lbl_BonusBronze.Text = string.Format(GetNumberFormat, p.BonusAmount);
                    break;
                case ("silver"): lbl_AmountSilver.Text = string.Format(GetNumberFormat, p.FromAmount);
                    lbl_BonusSilver.Text = string.Format(GetNumberFormat, p.BonusAmount);
                    break;
                case ("gold"): lbl_AmountGold.Text = string.Format(GetNumberFormat, p.FromAmount);
                    lbl_BonusGold.Text = string.Format(GetNumberFormat, p.BonusAmount);
                    break;
            }
        }
        PackageV = list;

    }
    private void LoadTransaction()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfListOfTransactionRow result;
        try
        {
            result = _supplier.GetTransactions(new Guid(GetGuidSetting()), true);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        /*
        result.Value[0].Amount;
        result.Value[0].Balance;
        result.Value[0].DateLocal;
        result.Value[0].DateServer;
        result.Value[0].Type;
         * */
        DataTable data = new DataTable();
        data.Columns.Add("Amount", typeof(string));
        data.Columns.Add("Date", typeof(string));
        data.Columns.Add("Type", typeof(string));
        foreach (WebReferenceSupplier.TransactionRow tr in result.Value)
        {
            DataRow row = data.NewRow();
            row["Amount"] = GetMoneyToDisplay(tr.Amount);
            row["Type"] = tr.Type;
            row["Date"] = Utilities.GetTimePassed(tr.DateServer, tr.DateLocal);
            data.Rows.Add(row);
        }
        _GridView_Transaction.DataSource = data;
        _GridView_Transaction.DataBind();
    }

    private void LoadDetails()
    {
        lbl_Balance.Text = string.Format(NUMBER_FORMAT, userManagement.balance);
        lbl_month.Text = string.Format("{0:MMMM yyyy}", DateTime.Now);
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfGetRechargeDataResponse result;
        try
        {
            result = _supplier.GetRechargeData(new Guid(GetGuidSetting()));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        
        if (result.Value.MinPrice == 0)
        {
            div_TitleDetails.Visible = false;
            a_add_credit.Attributes["class"] = "a-add-credit-empty";
            return;
        }
         
        
        lbl_PricePerLead.Text = string.Format(NUMBER_FORMAT, result.Value.MinPrice);
        lbl_RechargeBalance.Text = string.Format(NUMBER_FORMAT, result.Value.RechargeAmount);
    }
    /*
    protected void _GridView_Transaction_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == 5)
            e.Row.CssClass = "_rs _fs";
    }
     * */
    protected void lb_plan_Click(object sender, EventArgs e)
    {
        Package _pack = GetPackageByType(((LinkButton)sender).CommandArgument);
        if (_pack == null)
        {
            Update_Faild();
            return;
        }
        string __form = AddCreditSales.BuyCredit(_pack, siteSetting, new Guid(GetGuidSetting()));
        Response.Write(__form);
        Response.Flush();
        Context.ApplicationInstance.CompleteRequest();
    }

    private Package GetPackageByType(string _type)
    {
        foreach (Package p in PackageV)
        {
            if (p.Name == _type)
                return p;
        }
        return null;
    }
    
    List<Package> PackageV
    {
        get { return (Session["Package"] == null) ? null : (List<Package>)Session["Package"]; }
        set { Session["Package"] = value; }
    }
}