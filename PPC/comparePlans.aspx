﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="comparePlans.aspx.cs" MasterPageFile="~/Controls/MasterPageNP2.master" Inherits="PPC_comparePlans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>

<!--[if IE 7]>      
      <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome-ie7.min.css" />
<![endif]--> 
 
 
<style>
    .fa
    {
        /*padding-top:20px;*/
    }
</style>   
   
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="ComparePlan">

  

    <div class="titleFirst">
        <span>Choose advertising plan</span>
    </div>
   
    <div class="comparePlanTbl">
        <div class="comparePlanCaption">
            <div class="comparePlanCaptionEmpty">
            </div>

            <div class="comparePlanCaptionPlan">
               <div class="comparePlanCaptionPlanBackground">
                   
                    <div class="comparePlanCaptionPlanTiltle">
                        Featured Listing
                        
                    </div>

                    <div class="comparePlanCaptionPlanSubTiltle">
                        $<span id="featureListingTopSum" class="cost" runat="server"></span> /month
                    </div>

                    <div class="comparePlanCaptionContainerButton">                        
                        <asp:LinkButton id="linkButtonPlanFeauredListing" runat="server" 
                            CssClass="comparePlanButton" onclick="linkButtonPlanFeauredListing_Click">Select</asp:LinkButton>
                    </div>
                  
              </div>              
            </div>

            
            <div class="comparePlanCellSpace">
            </div>

           <div class="comparePlanCaptionPlan">
               <div class="comparePlanCaptionPlanBackground">
                
                    
                    <div class="comparePlanCaptionPlanTiltle2">
                        Featured Listing
                        
                    </div>
                    
                    <div class="comparePlanCaptionPlanTiltle2_2">
                        + Lead Booster                    
                    </div>

                    <div class="comparePlanCaptionPlanSubTiltle2">
                        Pay per lead
                    </div>

                    <div class="comparePlanCaptionContainerButton">                        
                        <asp:LinkButton id="linkButtonPlanLeadsBooster" runat="server" 
                            CssClass="comparePlanButton" onclick="linkButtonPlanLeadsBooster_Click">Select</asp:LinkButton>
                    </div>
                
              </div>              
            </div>

        </div>

        <div class="comparePlanRow">
            <div class="comparePlanCellDesc">
                <span class="descTitle">Profile traffic</span>
                <br />
                Appear in NoProblem search results
            </div>
            <div class="comparePlanCellPlan1">                
                <i class="fa fa-check"></i> 
                <!--<i class="icon-check"></i> ei 7 -->                
            </div>

            <div class="comparePlanCellSpace">
            </div>

            <div class="comparePlanCellPlan2">                
               <i class="fa fa-check"></i>               
            </div>
        </div>

         <div class="comparePlanRow">
            <div class="comparePlanCellDesc">                
                <span class="descTitle">Edit your business page</span>
                <br />
                Working hours, coverage, business information
            </div>
            <div class="comparePlanCellPlan1">                
                <i class="fa fa-check"></i>           
            </div>

             <div class="comparePlanCellSpace">
            </div>

            <div class="comparePlanCellPlan2">               
               <i class="fa fa-check"></i>               
            </div>
        </div>

         <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleLightGreen">
                <span class="descTitle">Multiple categories</span>                
                <br />
                Appear in multiple categories and in multiple per zip code
            </div>
            <div class="comparePlanCellPlan1">                
               <i class="fa fa-check"></i>             
            </div>

            <div class="comparePlanCellSpace">
            </div>

            <div class="comparePlanCellPlan2">                
                <i class="fa fa-check"></i>              
            </div>
        </div>


        <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleLightGreen">               
               <span class="descTitle">Preferred Placement</span>
               <br />
               Top positions in search results for 1 category in your zip code
            </div>
            <div class="comparePlanCellPlan1">               
                <i class="fa fa-check"></i>              
            </div>

            <div class="comparePlanCellSpace">
            </div>


            <div class="comparePlanCellPlan2">               
                <i class="fa fa-check"></i>       
            </div>
        </div>

        <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleLightGreen">
               <span class="descTitle">No Competitor Ads and No Adst</span>
               <br />
               Remove other featured advertisers and any distractions from your
               <br />
               business page
            </div>
            <div class="comparePlanCellPlan1">               
               <i class="fa fa-check"></i>              
            </div>

            <div class="comparePlanCellSpace">
            </div>

            <div class="comparePlanCellPlan2">              
                <i class="fa fa-check"></i>           
            </div>
        </div>

         <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleLightGreen">              
              <span class="descTitle">Featured Appearance</span>
              <br />
              Appear in sponsored placement on pages of other businesses
            </div>
            <div class="comparePlanCellPlan1">                
               <i class="fa fa-check"></i>               
            </div>

            <div class="comparePlanCellSpace">
            </div>


            <div class="comparePlanCellPlan2">                
                <i class="fa fa-check"></i>              
            </div>
        </div>

        <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleLightGreen">             
             <span class="descTitle">Upgraded Business Page</span>
             <br />
             Add photos, links to your website and social pages
            </div>
            <div class="comparePlanCellPlan1">               
                <i class="fa fa-check"></i>          
            </div>

            <div class="comparePlanCellSpace">
            </div>


            <div class="comparePlanCellPlan2">               
                <i class="fa fa-check"></i>               
            </div>
        </div>

         <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleGreen">            
            <span class="descTitle">Calls Booster</span>
            <br />
            Get leads from clients off your listing and from our affiliates across
            <br />
            the web (over 5 million daily)
            </div>
            <div class="comparePlanCellPlan1">               
               &nbsp;          
            </div>

            <div class="comparePlanCellSpace">
            </div>


            <div class="comparePlanCellPlan2">             
                <i class="fa fa-check"></i>             
            </div>
        </div>

        <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleGreen">            
            <span class="descTitle">Quote Request Program</span>
            <br />
            Get matched with consumers who ask for quotes in your area, and
            <br />
            get leads with description.
            </div>
            <div class="comparePlanCellPlan1">               
                &nbsp;     
            </div>

            <div class="comparePlanCellSpace">
            </div>


            <div class="comparePlanCellPlan2">               
                <i class="fa fa-check"></i>              
            </div>
        </div>

         <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleGreen">            
            <span class="descTitle">Pay Only for Leads You Want</span>
            <br />
            Listen to the consumer description and accept only the relevant
            </div>
            <div class="comparePlanCellPlan1">                
                &nbsp;       
            </div>

            <div class="comparePlanCellSpace">
            </div>


            <div class="comparePlanCellPlan2">                
                <i class="fa fa-check"></i>               
            </div>
        </div>

         <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleGreen">            
            <span class="descTitle">Pause / Resume campaign</span>
            <br />
            Decide when you want to receive calls and when you are off duty
            </div>
            <div class="comparePlanCellPlan1">                
               &nbsp;         
            </div>

            <div class="comparePlanCellSpace">
            </div>

            <div class="comparePlanCellPlan2">                
                <i class="fa fa-check"></i>             
            </div>
        </div>

         <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleGreen">            
            <span class="descTitle">Realtime Leads</span>
            <br />
            Get the leads when they’re still hot - while the consumer is on the
            <br />
            line
            </div>
            <div class="comparePlanCellPlan1">              
               &nbsp;      
            </div>

            <div class="comparePlanCellSpace">
            </div>

            <div class="comparePlanCellPlan2">              
                <i class="fa fa-check"></i>              
            </div>
        </div>

         <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleGreen">            
            <span class="descTitle">Management Dashboard</span>
            <br />
            Track leads (descriptions, location, client names), manage budget
            <br />
            and set price per lead
            </div>
            <div class="comparePlanCellPlan1">               
                &nbsp;     
            </div>

            <div class="comparePlanCellSpace">
            </div>

            <div class="comparePlanCellPlan2">                
                <i class="fa fa-check"></i>              
            </div>
        </div>

          <div class="comparePlanRow">
            <div class="comparePlanCellDesc descTitleGreen">            
            <span class="descTitle">Listen to Phone Leads</span>
            <br />
            All calls from clients are recorded
            </div>
            <div class="comparePlanCellPlan1">              
                &nbsp;            
            </div>

            <div class="comparePlanCellSpace">
            </div>


            <div class="comparePlanCellPlan2">                
                <i class="fa fa-check"></i>               
            </div>
        </div>


        <div class="comparePlanCaption comparePlanCaptionBottom">
            <div class="comparePlanCaptionEmpty">
            </div>

            <div class="comparePlanCaptionPlan">
               <div class="comparePlanCaptionPlanBackground">
                   
                    <div class="comparePlanCaptionPlanTiltle">
                        Featured Listing
                        
                    </div>

                    <div class="comparePlanCaptionPlanSubTiltle">
                        $<span id="featureListingBottomSum" runat="server" class="cost"></span> /month
                    </div>

                    <div class="comparePlanCaptionContainerButton">                        
                        <asp:LinkButton id="linkButtonPlanFeauredListing2" runat="server" CssClass="comparePlanButton" 
                            onclick="linkButtonPlanFeauredListing2_Click">Select</asp:LinkButton>
                    </div>
                  
              </div>              
            </div>

            
            <div class="comparePlanCellSpace">
            </div>

           <div class="comparePlanCaptionPlan">
               <div class="comparePlanCaptionPlanBackground">
                
                    
                    <div class="comparePlanCaptionPlanTiltle2">
                        Featured Listing
                        
                    </div>
                    
                    <div class="comparePlanCaptionPlanTiltle2_2">
                        + Lead Booster                    
                    </div>

                    <div class="comparePlanCaptionPlanSubTiltle2">
                        Pay per lead
                    </div>

                    <div class="comparePlanCaptionContainerButton">                        
                        <asp:LinkButton id="linkButtonPlanLeadsBooster2" runat="server" CssClass="comparePlanButton" 
                            onclick="linkButtonPlanLeadsBooster2_Click">Select</asp:LinkButton>
                    </div>
                
              </div>              
            </div>

        </div>

         
    </div>
</div>

</asp:Content>
