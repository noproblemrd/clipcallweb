﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="businessMainDetails.aspx.cs" MasterPageFile="~/Controls/MasterPageNP2.master" Inherits="PPC_businessMainDetails" ClientIDMode="Static"  %>
<%@ Register Src="~/Controls/searchGoogle.ascx" TagName="searchGoogle" TagPrefix="businessDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="../PlaceHolder.js"></script>

<!--
<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&amp;sensor=false&language=en"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyByICNsdF00OXlUGTnzGsIfNmk0Mo7xqcw&sensor=false&language=en"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places" type="text/javascript"></script>
-->

<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&amp;sensor=false&language=en"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyByICNsdF00OXlUGTnzGsIfNmk0Mo7xqcw&sensor=false&language=en"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places" type="text/javascript"></script>
                                    
 <script type="text/javascript" src="script/JSGeneral.js"></script>
 <script type="text/javascript" src="../general.js"></script>

 <script>
     
 </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="businessMainDetails">

   

    <div class="titleFirst">
        <span>Enter your business details</span>
    </div>   
    
    <div class="businessMainContent">

            <div class="containerName">
                <div>Company name</div>  
                <div class="inputParent">
                    <input id="txt_name" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="name" />
                    <div id="div_NameSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_NameError" class="inputError" style=""> </div>

                     <div id="div_NameComment" style="display:none;" class="inputComment"><%#businessNameError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                </div>
            </div>

            <div class="containerAddress">
                <div>Business address</div>

                <div class="inputParent inputParentAddress">                
                    <businessDetails:searchGoogle runat="server" ID="searchGoogle1"></businessDetails:searchGoogle>

                    <div id="div_AddressSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_AddressError" class="inputError" style=""> </div>             

                    <div id="div_AddressComment" style="display:none;" class="inputComment"><%#businessAdressError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                
                </div>
            </div>

             <div class="containerAddress">
                <div>Contact person</div>

                <div class="inputParent inputParentAddress">           
                    
                    <input id="txt_contactPerson" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="contact person" />
                    <div id="div_ContactPersonSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_ContactPersonError" class="inputError" style=""> </div>             

                    <div id="div_ContactPersonComment" style="display:none;" class="inputComment"><%#contactPersonError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                
                </div>
            </div>

            <div class="containerMobile">
                <div>Mobile number</div>  

                <div class="inputParent inputParentMobile">
                    <input id="txt_mobile" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="mobile number" />
                    <div id="div_MobileSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_MobileError" class="inputError" style=""> </div>             

                    <div id="div_MobileComment" style="display:none;" class="inputComment"><%#mobileError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>

                </div>
            </div>

            
            <div id="containerNext" class="containerNext">
             <section class="progress-demo">
                <asp:LinkButton ID="NextBusinessDetails" runat="server" data-style="expand-right" data-color="blue" class="ladda-button" OnClientClick="javascript:return businessDetailsValidation();" OnClick="NextBusinessDetails_Click" >
                    <span class="ladda-label">Next</span><span class="ladda-spinner">
                    </span><div class="ladda-progress" style="width: 138px;"></div>
                  </asp:LinkButton>

                  <span class="back"><a href="choosePlan.aspx">&lt; Back</a></span>
                </section>      
            </div>


            <asp:HiddenField id="hidden_phoneFormat" runat="server"/>
    </div> 
    
   

</div>

</asp:Content>
