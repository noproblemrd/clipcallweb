﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="location2.aspx.cs" MasterPageFile="~/Controls/MasterPageNP2.master" enableEventValidation="false" Inherits="PPC_location2" %>
<%@ Register Src="~/Controls/MapControlMulti.ascx" TagName="map" TagPrefix="ma" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="../PlaceHolder.js"></script>
    <script type="text/javascript" src="script/jquery-ui-1.8.16.custom.autocomplete.min.js"></script>

     <script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&amp;sensor=false&language=en"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyByICNsdF00OXlUGTnzGsIfNmk0Mo7xqcw&sensor=false&libraries=places&language=en"></script>
    <script type="text/javascript" src="script/JSGeneral.js"></script>
    <script type="text/javascript" src="../CallService.js"></script>
    <script type="text/javascript" src="../general.js"></script>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>

    <script type="text/javascript">
       
        function CheckLocationDetails() {
            var index;
            var boolValid=true;

            $('input[id^="searchTextField"]').each(function () {

                index = $(this).attr('id').charAt($(this).attr('id').length - 1);

                if ($(this).siblings('.inputValid').css('display') == 'none') {
                    $(this).siblings('.inputComment').css('display', 'block');
                    SetServerComment("<%# _map.SelectFromList %>");
                    boolValid = false;
                }
            }
            )


            if (!boolValid)
                return false;

            activeButtonAnim('.location #linkSetDetails', 200);

            GetAllDetailsMulti('<%#urlSuccees%>', "<%#GetServerError%>",true);
            
            /*
            var LPlace = GetSelectLargeAreaId();
            var FullAddress = GetFullAddress();
            if (FullAddress.length == 0 || LPlace == "false") {
            var _comment = (!LPlace) ? "<# _map.ChooseLargerArea %>" : "<# _map.SelectFromList %>";
            SetServerComment(_comment);
            SetInvalidLocation();
            return false;
            }
            */

            /*
            var _details = GetAllDetailsMulti();
            if (_details.length == 0) {
                SetServerComment("<%# _map.SelectFromList %>");
                ////SetInvalidLocation();
                return false;
            }

            document.getElementById("<%# hf_lat.ClientID %>").value = _details.lat_lng.lat();
            document.getElementById("<%# hf_lng.ClientID %>").value = _details.lat_lng.lng();
            document.getElementById("<%# hf_radius.ClientID %>").value = (_details.radius * 1.6);
            document.getElementById("<%# hf_LargePlace.ClientID %>").value = _details.AreaId;
            document.getElementById("<%# hf_FullAddress.ClientID %>").value = _details.FullAddress;
            */


            ////parent.showDiv();
            return true;
        }

        function SendFaild() {
            stopActiveButtonAnim();
        }

    </script>






</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="location"> 
        <div class="titleFirst">
            <span>Set cover area</span>
        </div> 
        

        <ma:map ID="_map" runat="server" />
        
        <div class="clear"></div>

        <div id="containerNext" class="containerNext">
                <section class="progress-demo">               

                 <a id="linkSetDetails" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" onclick="javascript:return CheckLocationDetails();">
                  <span class="ladda-label">Next</span><span class="ladda-spinner">
                  </span><div class="ladda-progress" style="width: 138px;"></div>
                 </a>

                  <span class="back"><a href="categories.aspx">&lt; Back</a></span>

                </section>      
         </div>  
         
         
        <asp:HiddenField ID="hf_lat" runat="server" />
        <asp:HiddenField ID="hf_lng" runat="server" />
        <asp:HiddenField ID="hf_radius" runat="server" />
        <asp:HiddenField ID="hf_LargePlace" runat="server" />
        <asp:HiddenField ID="hf_FullAddress" runat="server" /> 

        <asp:HiddenField ID="HiddenPlaceDetails" runat="server" /> 
        

    </div>
</asp:Content>