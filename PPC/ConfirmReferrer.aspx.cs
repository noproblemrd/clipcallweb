﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_ConfirmReferrer : RegisterPage, IsInProcess
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {           
            LoadPageText();
            LoadPhonDetails();
            SalesUtility.SetStepRegistration(Session["RegistrationPhone"] != null ? Session["RegistrationPhone"].ToString() : "", Site_Id, "PassedEnterPhonePage", "", "", "","","","","", false);

            Master.SetServerComment(PasswordSent);
        }

        LoadMaster();

        txt_password.Attributes["place_holder"] = PasswordPlaceHplder;
        Header.DataBind();
    }

    private void LoadMaster()
    {
        Master.SetStatusBar("statusBar1");
    }

    private void LoadPhonDetails()
    {
        lbl_ClientPhone.Text = PhoneNumDisplay;
    }
    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;
        //    _map.FieldMissing = FieldMissing;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "DetailsService"
                          select x).FirstOrDefault();

        // FieldMissing = xelem.Element("BusinessName").Element("Validated").Element("Missing").Value;
        //   XElement t_element = xelem.Element("BusinessAddress").Element("Validated");

        //    t_element = xelem.Element("Email").Element("Validated");
        //    EmailPlaceHplder = t_element.Element("Instruction").Element("General").Value;
        //    EmailDuplicate = t_element.Element("Instruction").Element("EmailDuplicate").Value;
        //    InvalidEmail = t_element.Element("Error").Value;
        //    EmailMissing = t_element.Element("Missing").Value;
        PasswordSent = xelem.Element("FormMessages").Element("PasswordSent").Value;
       
        PasswordPlaceHplder = xelem.Element("PasswordCode").Element("Validated").Element("Instruction").Element("PlaceHolder").Value;
        WrongCode = xelem.Element("PasswordCode").Element("Validated").Element("Instruction").Element("WrongCode").Value;
        GetServerErrorSendToPhone = xelem.Element("MobilePhone").Element("Validated").Element("ServerError").Value;
        
        Master.SetBenefitFreeText("Get leads from ready-to-hire customers directly to your mobile phone.<br /><br />That's NoProblem!");
    }

   

    protected string PasswordPlaceHplder
    {
        get { return (string)ViewState["PasswordPlaceHplder"]; }
        set { ViewState["PasswordPlaceHplder"] = value; }
    }
    protected string GetServerError
    {
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }
    protected string PasswordSent
    {
        get { return (string)ViewState["PasswordSent"]; }
        set { ViewState["PasswordSent"] = value; }
    }
    protected string GetServerErrorSendToPhone
    {
        get { return HttpUtility.JavaScriptStringEncode((string)ViewState["GetServerErrorSendToPhone"]); }
        set { ViewState["GetServerErrorSendToPhone"] = value; }
    }
    protected string WrongCode
    {
        get { return HttpUtility.JavaScriptStringEncode((string)ViewState["WrongCode"]); }
        set { ViewState["WrongCode"] = value; }
    }
    
    
    protected string GetSendMobileService
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/SendSupplierSignUpNotification"); }
    }
    protected string ChangePhoneInRegistration
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/ChangePhoneInRegistration"); }
    }
    protected string ApprovePassword
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/Login"); }
    }
    protected string GetPasswordByPhone
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/GetPasswordByPhone"); }
    }
    protected string ServicePage
    {
        get { return ResolveUrl("~/PPC/Service2.aspx"); }
    }

    protected string PlansPage
    {
        get { return ResolveUrl("~/PPC/Plans.aspx"); }
    }

    protected string GetChangeNumDestination
    {
        //get { return (HasReferrer ? "RegisterReferrer.aspx" : "RequestInvitation.aspx?changephone=true"); }
        get { return "Register.aspx"; }
    }

    protected string Site_Id
    {
        get { return siteSetting.GetSiteID; }
    }
}