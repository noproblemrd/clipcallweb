﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNP.master" AutoEventWireup="true" CodeFile="Service.aspx.cs" Inherits="PPC_Service"  %>
  <%@ MasterType VirtualPath="~/Controls/MasterPageNP.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
<script type="text/javascript" src="script/jquery-ui-1.8.16.custom.autocomplete.min.js"></script>
<script type="text/javascript" src="../CallService.js"></script>
<script type="text/javascript" src="../general.js"></script>

<script type="text/javascript" src="script/JSGeneral.js"></script>

<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript" src="script/HistoryManager.js"></script>        
<link href="../Formal/jquery-ui-1.8.16.custom.css" type="text/css" rel="Stylesheet" />
<script type="text/javascript">
      
    window.onload = init;

    function init() {

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndHandler);
    }
    function startRequest(sender, e) {
         $('a.btn_next').hide();
        $('span.btn_next').show();
  //      switch (e.get_postBackElement().id) {
    }
    function EndHandler(sender, e){
         $('a.btn_next').show();
        $('span.btn_next').hide();
    }
       
    /*****    general           *******/
    /*
    function RemoveServerComment() {
    
        document.getElementById("lbl_ServerComment").innerHTML = "";
        document.getElementById("div_ServerComment").style.display = "none";
    }
    function SetServerComment(_comment) {
        document.getElementById("lbl_ServerComment").innerHTML = _comment;
        document.getElementById("div_ServerComment").style.display = "block";
    }
    */
    function ResetScrollPosition() {
        //   window.scrollTo(0, 0);
        setTimeout("window.scrollTo(0,0)", 0);
    }
    /*
    function SetServicePlaceHolder() {
        document.getElementById("<%# txt_Category.ClientID %>").value = "<%# ServicePlaceHolder %>";
    }
    */
    /********** Service  ****************/
    var service_place_holder;
    var IsSelected = false;
    $(function () {
        $("._Heading").autocomplete({

            source: function (request, response) {
                var _txt = document.getElementById("<%# txt_Category.ClientID  %>");
                SourceText = request.term;
                $.ajax({
                    url: "<%# GetHeadingServiceList %>",
                    data: "{ 'TagPrefix': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {

                        response($.map(data.d, function (item) {
                            return {
                                value: item
                            }
                        }))
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //              alert(textStatus);
                    },
                    complete: function (jqXHR, textStatus) {
                        //  HasInHeadingRequest = false;
                    }

                });
            },
            minLength: 1,
            close: function (event, ui) {
                if (!IsSelected && event.target.value.length != 0){
                    SlideElement("div_ServiceComment", true);// document.getElementById("div_ServiceComment").style.display = "block";
                    $('#div_ServiceError').show();
                }
                IsSelected = false;
            },
            open: function (event, ui) {
                SlideElement("div_ServiceComment", false);// document.getElementById("div_ServiceComment").style.display = "none";
                $('#div_ServiceError').hide();
            },
            select: function (event, ui) {
                SlideElement("div_ServiceComment", false);// document.getElementById("div_ServiceComment").style.display = "none";
                $("#div_ServiceSmile").show();
                $('#div_ServiceError').hide();
                IsSelected = true;
            }
        });
    });

    function CheckHeading(_elem) {

      //  Text_Blur(_elem, service_place_holder);
        setTimeout(function (){
       // SlideElement("div_ServiceComment", false);// document.getElementById("div_ServiceComment").style.display = "none";
        var div_ServiceSmile = document.getElementById("div_ServiceSmile");
        var div_ServiceError = document.getElementById("div_ServiceError");
        //   var div_ServiceComment = document.getElementById("div_ServiceComment");
        
        var name = _elem.value;//GetText_PlaceHolder(_elem, service_place_holder);
        if (name.length == 0) {
            div_ServiceSmile.style.display = "none";
            div_ServiceError.style.display = "none";
            RemoveCssClass(_elem, "textError");
            return;
        }
        var prms = "name=" + name;
        var url = "<%# GetIsHeadingService %>";

        CallWebService(prms, url,
                function (arg) {
                    
                    if (arg.toLowerCase() == "true") {
                        div_ServiceSmile.style.display = "block";

                        div_ServiceError.style.display = "none";
                        RemoveCssClass(_elem, "textError");
                        AddCssClass(_elem, "textValid");
                        IsSelected = true;
                        SlideElement("div_ServiceComment", false);
                    }
                    else {
                        div_ServiceSmile.style.display = "none";

                        div_ServiceError.style.display = "block";
                        AddCssClass(_elem, "textError");
                        SlideElement("div_ServiceComment", true);
                    }
                },
                function () {
                    SlideElement("div_ServiceComment", false);
                    div_ServiceSmile.style.display = "none";

                    div_ServiceError.style.display = "block";
                    AddCssClass(_elem, "textError");
                    SlideElement("div_ServiceComment", true);
                });

        }, 250);
    }
    /*
    function CategoryFocus(_elem) {
        //        document.getElementById("div_service_error").style.visibility = "hidden";
    //    Text_Focus(_elem, service_place_holder);
    //    RemoveCssClass(_elem, "textValid");
    }
    */
    function CategoryPress(obj, e) {
        document.getElementById("<%# a_categories.ClientID %>").style.display = (obj.value.length == 0) ?
                "none" : "inline";
     /*   document.getElementById("div_ServiceError").style.display = "none";*/
        document.getElementById("div_ServiceSmile").style.display = "none";
        if(obj.value.length == 0){
            SlideElement("div_ServiceComment", false);// document.getElementById("div_ServiceComment").style.display = "none";
            $('#div_ServiceError').hide();
        }
        var keynum;
        if (e.keyCode) // IE            
            keynum = e.keyCode
        else if (e.which) // Netscape/Firefox/Opera            
            keynum = e.which
        if (keynum == 13){
            $("._Heading").autocomplete("close");
            document.getElementById("<%# hf_Heading.ClientID %>").value = obj.value;
           // toggleDisabledDivSearchService(true);
           startRequest();
            <%# setEnterPostBack() %>;
        }
    }
   
    function Set_Service() {
        
        var txt_Category = document.getElementById("<%# txt_Category.ClientID %>");
        var _service = txt_Category.value;
        var div_ServiceError = document.getElementById("div_ServiceError");
        RemoveCssClass(txt_Category, "textError");
        RemoveCssClass(txt_Category, "textValid");
        if (_service.length == 0) {
            _SetServerComment("<%# FieldMissing %>");
            div_ServiceError.style.display = "block";
            AddCssClass(txt_Category, "textError");
            document.getElementById("<%# a_categories.ClientID %>").style.display = "block";
            return false;
        }
        div_ServiceError.style.display = "none";
        document.getElementById("<%# hf_Heading.ClientID %>").value = _service;
       
        return true;
    }
    
    function SetDivCategories() {
       
        _SetDivCategories();
        $.history.load("2"); 
    }
    function _SetDivCategories()
    {
        document.getElementById("<%# div_HeadingText.ClientID %>").style.display = "none";
        document.getElementById("<%# div_HeadingCategoryTable.ClientID %>").style.display = "block";
        document.getElementById("<%# a_categories.ClientID %>").style.display = "none";
        document.getElementById("<%# a_FreeSearch.ClientID %>").style.display = "block";
        document.getElementById("<%# div_SendServiceList.ClientID %>").style.display = "none";
        document.getElementById("div_btn_FreeSearch2").style.display = "none";   
    }
    function SetEmptyFreeSearch() {
        txt_Category = document.getElementById("<%# txt_Category.ClientID %>");
        document.getElementById("div_ServiceError").style.display = "none";
        document.getElementById("div_ServiceSmile").style.display = "none";
        SlideElement("div_ServiceComment", false);
        RemoveCssClass(txt_Category, "textError");
        
    }
    function SetFreeSearch() {
        document.getElementById("<%# div_HeadingText.ClientID %>").style.display = "block";
        document.getElementById("<%# div_HeadingCategoryTable.ClientID %>").style.display = "none";
        document.getElementById("<%# a_FreeSearch.ClientID %>").style.display = "none";
        document.getElementById("<%# a_categories.ClientID %>").style.display = "none";
        document.getElementById("<%# div_SendServiceList.ClientID %>").style.display = "none";
        document.getElementById("div_btn_FreeSearch").style.display = "block";
        //         document.getElementById("div_SendServiceList").style.display = "none";
        SetEmptyFreeSearch();
    }
    function SetCategoryButton(elem) {
  //      RemoveServerComment();
        elem.className = "selected";
        SetDivCategories();
     //   setTimeout(function(){$.history.load("3");}, 0);
    //    document.getElementById("div_SendServiceList").style.display = "block";
        return true;
    }
    var dont_ToGo;
    function set_step_3(ToGo){
        dont_ToGo = (ToGo=="true");
        $.history.load("3");
    }
    function SetButton(elem, _id) {
   //     RemoveServerComment();
        var div_HeadingCategoryTable = document.getElementById("<%# div_HeadingCategoryTable.ClientID %>");
        var buttons = div_HeadingCategoryTable.getElementsByTagName("a");
        for (var i = 0; i < buttons.length; i++) {
            RemoveCssClass(buttons[i], "selected");
        }
        elem.className = "selected";
        document.getElementById("<%# lbl_Heading.ClientID %>").innerHTML = elem.innerHTML;
        document.getElementById("<%# hf_Heading.ClientID %>").value = elem.innerHTML;
        document.getElementById("<%# hf_ParentId.ClientID %>").value = _id;
        //    document.getElementById("<%# lbl_ServiceInstruction.ClientID %>").innerHTML = "<%# ChooseSkill %>";
        document.getElementById("<%# div_inputComment.ClientID %>").style.display = "none";
        document.getElementById("<%# div_SendServiceList.ClientID %>").style.display = "block";
    }
    function Set_Service_list() {
        var _exp = document.getElementById("<%# hf_Heading.ClientID %>").value;
        if (_exp.length == 0) {
            _SetServerComment("Please retype or browes categories");
            return false;
        }
        return true;

    }
/*
    function toggleDisabledDivSearchService(ToDisable) {
        var SearchService = document.getElementById("<%# div_HeadingText.ClientID  %>");

        var btn_SendService = document.getElementById("<%# btn_SendService.ClientID %>");
        toggleDisabled(SearchService, ToDisable);
        if (ToDisable) {

            btn_SendService.className = "btn_next_active";
      //      btn_SendService.innerHTML = "Sending...";

        }
        else {

            btn_SendService.className = "btn_next";
            //        btn_SendService.innerHTML = "Next &#62;";

        }
    }
    function toggleDisabledDivListService(ToDisable) {
        var CategoryTable = document.getElementById("<%# div_HeadingCategoryTable.ClientID %>");

        var btn_SendService = document.getElementById("<%# btn_SendServiceList.ClientID %>");
        toggleDisabled(CategoryTable, ToDisable);
        if (ToDisable) {
            btn_SendService.className = "btn_next_active";
 //           btn_SendService.innerHTML = "Sending...";
        }
        else {
            btn_SendService.className = "btn_next";
            //           btn_SendService.innerHTML = "Next &#62;";
        }
    }
    */
    function SetServiceError() {
        var txt_Category = document.getElementById("<%# txt_Category.ClientID %>");
        document.getElementById("div_ServiceError").style.display = "block";
        AddCssClass(txt_Category, "textError");
       EndHandler();
        document.getElementById("div_btn_FreeSearch").style.display = "none";
        
        document.getElementById("<%# a_categories.ClientID %>").style.display = "none";
        document.getElementById("<%# a_FreeSearch.ClientID %>").style.display = "none";
        
        document.getElementById("<%# div_SendServiceList.ClientID %>").style.display = "none";
        document.getElementById("div_btn_FreeSearch2").style.display = "block";
        document.getElementById("<%# div_HeadingCategoryTable.ClientID %>").style.display = "block";
        document.getElementById("<%# div_HeadingText.ClientID  %>").style.display = "block";
        
    }
    jQuery(document).ready(function ($) {
            
            $.history.init(function (url) {
                if(url=="")
                    url="1";
                set_step(url);
            });

        });
    function set_step(_step){
        if(dont_ToGo == true)
        {
            dont_ToGo=false;
            return;
        }
        if(_step=="1"){
            SetFreeSearch();
            <%# setStep("1") %>;
        }
        else if(_step=="2"){
            SetDivCategories();
            <%# setStep("2") %>;
        }
        else if(_step=="3"){
           
            <%# setStep("3") %>;
        }
            
    }


</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <div runat="server" id="div_c" class="div_TopSpace">
   
        
        <div class="titleFirst">What do you do?</div>
        <div class="clear"></div>
        <div class="UnderContentSpace">
            <asp:Label ID="lbl_NonITC" runat="server" Text="Please enter the type of service you provide in the space below."></asp:Label>
            <asp:Label ID="lbl_ITC" Visible="false" runat="server" Text="Complete this brief form to access your account and all the advanced features in our Advertiser Dashboard. We've managed to fill in some of your information based on public records to make the registration process quicker for you. Please review and edit if necessary, and provide the missing information."></asp:Label>
            
        </div>
         <div class="spaceAbove" >
        <div  class="div_brows_freesearch">
            <asp:LinkButton ID="a_categories" runat="server" OnClick="a_categories_click" OnClientClick="SetDivCategories();" style="display:none;" class="general"
             Text="Browse Categories"></asp:LinkButton>
            
        </div>   
       
         
         
        <div id="div_HeadingText" runat="server">
            <div class="titleSecond">The service I provide...</div>
        
            <div class="clear"></div>
            <div class="inputParent">          
                <div class="inputSun">
                    <asp:TextBox ID="txt_Category" runat="server" CssClass="_Heading textActive" onblur="CheckHeading(this);"                         
                                         onkeyup="CategoryPress(this, event);" HolderClass="place-holder"></asp:TextBox>
           
                </div>
                 <div class="inputValid" style="display:none;"  id="div_ServiceSmile">
                
                </div>
                <div class="inputError" style="display:none;"  id="div_ServiceError">
               
                </div>
                <div class="inputComment" style="display:none;" id="div_ServiceComment">
                    
                        <%= ServicePlaceHolder%>
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>
                
                 
               
               
               
            </div>                   
            
            <div id="div_btn_FreeSearch">
                <asp:LinkButton ID="btn_SendService" runat="server" CssClass="btn_next"  OnClientClick="return Set_Service();" OnClick="btn_SendService_click"
                  Text="Next"></asp:LinkButton>
                <span class="btn_next" style="display:none;">Processing...</span>

             
            </div>
        </div>
       
        <div class="detailsService">  
            
        <asp:UpdatePanel runat="server" ID="_UpdatePanel" UpdateMode="Conditional">
        <ContentTemplate>
          <asp:HiddenField ID="hf_ParentId" runat="server" />
             
            <div id="div_HeadingCategoryTable" runat="server" class="category" style="display:none;">
               
                <div class="div_FreeSearch">
                    <asp:LinkButton ID="a_FreeSearch" runat="server" OnClick="a_FreeSearch_click" OnClientClick="SetFreeSearch();" style="display:none;" class="general" Text="< Search"></asp:LinkButton>
            </div>
                
                <asp:HiddenField ID="hf_Heading" runat="server" />
                

                  <div class="inputParent">
                    
                    <div class="inputComment"  id="div_inputComment" runat="server">
                        <asp:Label ID="lbl_ServiceInstruction" runat="server" Text="<%# ChooseCategory %>"></asp:Label>
                     
                      <div class="chat-bubble-arrow-border"></div>
                      <div class="chat-bubble-arrow"></div>
                    </div>
	            </div>


                <asp:DataList runat="server" ID="_dataList" RepeatColumns="3" RepeatDirection="Horizontal">
                <HeaderTemplate>
                        <div class="titleSecond title_service" runat="server" id="div_CategoryTitle">
                            <asp:Label runat="server" ID="lbl_CategoryTitle" Text="<%# GetListTitle %>"></asp:Label>
                        </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="item">
                        <asp:LinkButton ID="lb_Category" runat="server" Text="<%# Bind('Name') %>" CommandArgument="<%# Bind('arg') %>" OnClientClick="<%# Bind('script') %>"
                            onclick="btn_Category_Click"></asp:LinkButton>
                  
                    </div>
               </ItemTemplate>
                </asp:DataList>
                <div runat="server" id="div_CategoryTitle" style="display:none;">
                    
                    
                  <asp:LinkButton class="categoryTest btnRegionBackground" ID="a_back" runat="server" onclick="a_back_click">
                    <asp:Label ID="lbl_Category" runat="server"></asp:Label><span class="detailsService categoryX">X</span>
                </asp:LinkButton>
                </div>
                <div class="WrapSkill">
                
                    <asp:Label ID="lbl_Heading" runat="server" class="ChoosenSkill"></asp:Label>
                </div>
                <div id="div_SendServiceList" runat="server" class="div_SendServiceList">
               
                    <asp:LinkButton ID="btn_SendServiceList" runat="server" CssClass="btn_next" OnClientClick="return Set_Service_list();" OnClick="btn_SendServiceList_click" Text="Next"></asp:LinkButton>
       
                </div>
            </div>
         </ContentTemplate>
        </asp:UpdatePanel>
        </div>
        <div id="div_btn_FreeSearch2" style="display:none;">
            <asp:LinkButton ID="btn_SendService2" runat="server" CssClass="btn_next"  OnClientClick="return Set_Service();" OnClick="btn_SendService_click" Text="Next"></asp:LinkButton>
            <span class="btn_next" style="display:none;">Processing...</span>
        </div>
        </div>
    </div>
    <asp:Button ID="btn_virtual" runat="server" Text="Button" style="display:none;" />

</asp:Content>

