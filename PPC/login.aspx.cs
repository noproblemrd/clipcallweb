﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_login : RegisterPage
{
    protected string EmailDuplicate = "";

    protected void Page_Load(object sender, EventArgs e)
    {       

        if (!IsPostBack)
        {
            LoadPageText();

            string err = "";
            err=Request["error"];

            if (err!=null && err.ToLower() == "emailduplicate")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "SetServerComment", "SetServerComment('" + EmailDuplicate + "');", true);
            }
        }

        LoadMaster();

        txt_email.Attributes["place_holder"] = EmailPlaceHolder;
        txt_password.Attributes["place_holder"] = PasswordPlaceHolder;
        ////txt_referrer.Attributes["place_holder"] = "The code from the email invitation you received";
        Header.DataBind();
    }

    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();       

    }
    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

        //Response.Write(GetServerError);
        //    _map.FieldMissing = FieldMissing;       

        // FieldMissing = xelem.Element("BusinessName").Element("Validated").Element("Missing").Value;
        //   XElement t_element = xelem.Element("BusinessAddress").Element("Validated");

        //    t_element = xelem.Element("Email").Element("Validated");
        //    EmailPlaceHplder = t_element.Element("Instruction").Element("General").Value;
        //    EmailDuplicate = t_element.Element("Instruction").Element("EmailDuplicate").Value;
        //    InvalidEmail = t_element.Element("Error").Value;
        //    EmailMissing = t_element.Element("Missing").Value;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "login"
                          select x).FirstOrDefault();

        EmailPlaceHolder = xelem.Element("Email").Element("Validated").Element("Instruction").Element("General").Value;
        PasswordPlaceHolder = xelem.Element("password").Element("Validated").Element("Instruction").Element("General").Value;
        EmailError = xelem.Element("Email").Element("Validated").Element("Error").Value;
        EmailError2 = xelem.Element("Email").Element("Validated").Element("Error2").Value;
        PasswordError = xelem.Element("password").Element("Validated").Element("Instruction").Element("Error").Value;
        EmailDuplicate = xelem.Element("Email").Element("Validated").Element("Instruction").Element("EmailDuplicate2").Value;

    }

    protected string EmailPlaceHolder
    {
        get { return (string)ViewState["EmailPlaceHolder"]; }
        set { ViewState["EmailPlaceHolder"] = value; }
    }

    protected string PasswordPlaceHolder
    {
        get { return (string)ViewState["PasswordPlaceHolder"]; }
        set { ViewState["PasswordPlaceHolder"] = value; }
    }

    protected string EmailError
    {
        get { return (string)ViewState["EmailError"]; }
        set { ViewState["EmailError"] = value; }
    }

    protected string EmailError2
    {
        get { return (string)ViewState["EmailError2"]; }
        set { ViewState["EmailError2"] = value; }
    }
    

    protected string PasswordError
    {
        get { return (string)ViewState["PasswordError"]; }
        set { ViewState["PasswordError"] = value; }
    }

    protected string GetServerError
    {
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }

    protected string RegisterReferrer
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/SupplierReferrerInvitation"); }
    }

    protected string Root
    {
        get
        { return ResolveUrl("~");
        }
    }




}