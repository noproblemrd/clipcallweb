﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_businessMainDetails : RegisterPage2, IsInProcess
{
   
    protected string nameHolder;
    protected string companyAddressHolder;
    protected string mobileNumberHolder;
    protected string contactPersonNameHolder;    
    protected string regFormatPhone;
    protected string mobileError;
    protected string businessAdressError;
    protected string businessNameError;
    protected string contactPersonError;    
    protected string mobile;
    protected string companyName;
    protected string businessAddress;
    protected string contactPerson;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadMaster();
            LoadPageText();
            loadDetails();

            txt_name.Attributes["place_holder"] = nameHolder;
            txt_mobile.Attributes["place_holder"] = mobileNumberHolder;
            txt_contactPerson.Attributes["place_holder"] = contactPersonNameHolder;
            if (Request.Url.Host == "localhost" || Request.Url.Host == "qa.ppcnp.com" || Request.Url.Host == "10.0.0.90")
            {
                regFormatPhone = PpcSite.GetCurrent().PhoneReg;
            }
            else
            {
                regFormatPhone = PpcSite.GetCurrent().PhoneRegularExpression_USA_and_UK;// DBConnection.GetPhoneRegularExpression(SiteId); // for usa ^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$ 
            }
            //regFormatPhone = regFormatPhone.Replace("\\", "\\\\");
            hidden_phoneFormat.Value = regFormatPhone;
        }

        Page.DataBind();
    }


    private void loadDetails()
    {
       
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfGetBusinessDetailsResponse resultOfGetBusinessDetailsResponse=new WebReferenceSupplier.ResultOfGetBusinessDetailsResponse();
        
        try
        {
            //Response.Write("SupplierId:" + SupplierId.ToString());

            resultOfGetBusinessDetailsResponse = _supplier.GetBusinessDetails_Registration2014(SupplierId);

            if (resultOfGetBusinessDetailsResponse.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed GetBusinessDetails_Registration2014 for " + SupplierId + " " + resultOfGetBusinessDetailsResponse.Messages[0]));

                Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
                masterPage.ServerProblem(GetServerError);
            }

            else
            {
                mobile = resultOfGetBusinessDetailsResponse.Value.Phone;
                txt_mobile.Value = mobile;

                companyName = resultOfGetBusinessDetailsResponse.Value.Name;
                txt_name.Value = companyName;

                contactPerson = resultOfGetBusinessDetailsResponse.Value.ContactPersonName;
                txt_contactPerson.Value = contactPerson;

                businessAddress = resultOfGetBusinessDetailsResponse.Value.Address;

                

                if (!String.IsNullOrEmpty(businessAddress))
                    searchGoogle1.region = businessAddress;

                decimal lat = resultOfGetBusinessDetailsResponse.Value.Latitude.Value;
                searchGoogle1.lat = lat;

                decimal lng = resultOfGetBusinessDetailsResponse.Value.Longitude.Value;
                searchGoogle1.lng = lng;

               
                Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
                masterPage.SetPlanType(resultOfGetBusinessDetailsResponse.Value.SubscriptionPlan);
              
            }
            


        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);

            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);             
        }


    }


    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

        masterPage.SetStatusBar("past", "current", "pre");
        masterPage.SetBlackMenu();        
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

        
        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "BusinessDetails"
                          select x).FirstOrDefault();

        nameHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("CompanyName").Value;
        companyAddressHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("CompanyAddress").Value;
        mobileNumberHolder = xelem.Element("Validated").Element("Instruction").Element("Watermark").Element("MobileNumber").Value;
        contactPersonNameHolder=xelem.Element("Validated").Element("Instruction").Element("Error").Element("ContactPersonName").Value;

        businessNameError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("BusinessName").Value;
        mobileError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("MobileNumber").Value;
        businessAdressError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("BusinessAdress").Value;
        contactPersonError = xelem.Element("Validated").Element("Instruction").Element("Error").Element("ContactPersonName").Value;
        
    }

    protected void NextBusinessDetails_Click(object sender, EventArgs e)
    {
        WebServiceSales webServiceSales=new WebServiceSales();


        try
        {
            ResultSteps resultSteps = webServiceSales.businessDetails_Registration2014(SupplierId, txt_name.Value, searchGoogle1.region, Utilities.GetCleanPhone(txt_mobile.Value), searchGoogle1.lat, searchGoogle1.lng, txt_contactPerson.Value, "usa", searchGoogle1.state);

            if (resultSteps.result == "success")
            {
                Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
                masterPage.stepRedirect(4); // go to categories              
            }

            else
            {
                dbug_log.ExceptionLog(new Exception("failed businessDetails_Registration2014 " + resultSteps.message));

                Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
                masterPage.ServerProblem(GetServerError);
            }
        }

        catch(Exception ex)
        {
            dbug_log.ExceptionLog(ex);
        }
      
    }

    protected string GetServerError
    {
        get {return (ViewState["GetServerError"]==null) ? " " : ViewState["GetServerError"].ToString();}
        set { ViewState["GetServerError"] = value; }
    }
}