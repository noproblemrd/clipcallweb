﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="changePassword.aspx.cs" Inherits="PPC_changePassword" MasterPageFile="~/Controls/MasterPageNP2.master"  ClientIDMode="Static"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="../PlaceHolder.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>
<script src="../scripts/sjcl.js" type="text/javascript"></script>
<link href="samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />
<!--[if IE 7]>      
      <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome-ie7.min.css" />
    <![endif]-->  
    
    
<script>
    function validation() {
       
         if (!checkPassword($("#txt_passwordFirstOfTwo").val())) {
             SetServerComment("<%#PasswordError%>");
             return false;
             
         }

         else if (!checkPasswordsEquel($("#txt_passwordFirstOfTwo").val(), $("#txt_passwordSecondOfTwo").val())) {
             SetServerComment("<%#PasswordMutch%>");
             return false;
         }

         setPasswordCookieFirstOfTwo();
         
         activeButtonAnim('.progress-demo #btn_SendService', 200);

         return true;
         
    }
</script>  
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="registration login changePassword">   

    <div class="registrationLeft">

       <div  id="titleFirst" runat="server" class="titleFirst">
            <span>Change your password</span>
        </div>      

        <div id="subTitle" runat="server" class="subTitle">
            Enter your new password information. 
            <br />
            Use at least 6 characters. Don’t use a password from another site or something too obvious.
        </div>

       <div  id="registrationInputs" runat="server" class="registrationInputs" >            

           <asp:ScriptManager ID="ScriptManager1" runat="server">
           </asp:ScriptManager> 

           <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
           <ContentTemplate>
           <div class="emailContainer">
                <div>New password</div>  
                <div class="inputParent">
                    <input id="txt_passwordFirstOfTwo" type="password" class="textActive" runat="server" HolderClass="place-holder" text-mode="password" />
                    <div id="div_PasswordSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_PasswordError" class="inputError" style=""> </div>

                    <div class="inputValid" style="display:none;"  id="div1"></div>

                    <div id="div_PasswordComment" style="display:none;" class="inputComment"><%#PasswordError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>

                </div>

                <div class="secondPasswordTitle">Confirm new password</div> 
                <div class="inputParent">
                    <input id="txt_passwordSecondOfTwo" type="password" class="textActive" runat="server" HolderClass="place-holder" text-mode="password" />
                    <div id="div_PasswordSmile2" class="inputValid" style="display:none;"> </div>
                    <div id="div_PasswordError2" class="inputError" style=""> </div>

                    <div class="inputValid" style="display:none;"  id="div4"></div>

                    <div id="div_PasswordComment2" style="display:none;" class="inputComment"><%#PasswordError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>

                </div>

                 <div id="containerNext" class="containerNext">
                        <section class="progress-demo">
                        <asp:LinkButton ID="btn_SendService" runat="server" data-style="expand-right" data-color="blue" class="ladda-button" OnClientClick="javascript:return validation();" OnClick="btnSend_Click" >
                            <span class="ladda-label">Save</span><span class="ladda-spinner">
                            </span><div class="ladda-progress" style="width: 138px;"></div>
                          </asp:LinkButton>                 
                        </section>      
                 </div>  
            </div>        
           </ContentTemplate>
           </asp:UpdatePanel>

            <div class="clear"></div>     

 
           
       </div>  

      
   
   
    
    </div>

    <div class="registrationRight">
        No account? <a href="register.aspx">Join NoProblem</a>
        
    </div>
    
</div>
</asp:Content>
