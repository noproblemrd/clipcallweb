﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Controls/MasterPageNP2.master" CodeFile="paymentLegacy.aspx.cs" Inherits="PPC_paymentLegacy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="paymentLegacy">    
        <div class="titleFirst">
            <span>Update payment method</span>
        </div>  

        <div class="subTitle">
            In order to benefit from the new improvements, you will need to confirm your payment method. 
        </div>

        <div class="whatsNew">
            What’s new? 
        </div>

        <div class="points">
            <div class="point">
                ✓ <span class="pointText">Get charged after you accept a lead (instead of buying a pre-paid credit). </span>
            </div>

            <div class="point">
                ✓ <span class="pointText">No charges for leads you miss.</span>
            </div>
        </div>

        <div class="payment">
                <div class="containerIframePayment">
                    <iframe id="iframePayment" src="" runat="server" width="860" height="1456" frameborder="0"></iframe>
                </div>           
        </div>
    </div>   
</asp:Content>
