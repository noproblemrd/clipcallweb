﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_improvement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.hideNameAndLogout();
            masterPage.RemoveRibbon();


            string mode;
            mode=Request["redirect"];

            if(!string.IsNullOrEmpty(mode))
            {
                if(mode.ToLower()=="verifyyouremailmanually")
                    linkSetup.HRef = "verifyYourEmailManually.aspx";
            }           


        }
    }
}