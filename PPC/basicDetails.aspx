﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="basicDetails.aspx.cs" MasterPageFile="~/Controls/MasterPageNPTemp.master" Inherits="PPC_basicDetails"  ClientIDMode="Static"%>
<%@ MasterType VirtualPath="~/Controls/MasterPageNPTemp.master" %>
<%@ Register Src="~/Controls/searchGoogle.ascx" TagName="searchGoogle" TagPrefix="businessDetails" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


  
<script type="text/javascript" src="script/jquery-ui-1.8.16.custom.autocomplete.min.js"></script>
<script type="text/javascript" src="../CallService.js"></script>
<script type="text/javascript" src="../general.js"></script>

<script type="text/javascript" src="script/JSGeneral.js"></script>

<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript" src="script/HistoryManager.js"></script>        
<link href="../Formal/jquery-ui-1.8.16.custom.css" type="text/css" rel="Stylesheet" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
<script type="text/javascript" src="ppc.js"></script>
<script type="text/javascript"> 
var service_place_holder;
var IsSelected = false;





   function businessDetailsValidation() {

        if (nameValidation() == true & contactPersonValidation() & regionValidation() == true & categoryValidation()) {                
            return true;
        }
        else {        
            return false;
        }   
    
   } 

   
   
   $(function () {     
   $("._Heading").autocomplete({
                
             source: function (request, response) {
                <%--
                 var _txt = document.getElementById("<%# txt_Category.ClientID  %>");
                 --%>
                                
                 SourceText = request.term;
                
                 $.ajax({
                     url: "<%# GetHeadingServiceList %>",
                     data: "{ 'TagPrefix': '" + request.term + "'}",
                     dataType: "json",
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     dataFilter: function (data) { return data; },
                     success: function (data) {
                       
                         response($.map(data.d, function (item) {
                             return {
                                 value: item
                             }
                         }))
                     },
                     error: function (XMLHttpRequest, textStatus, errorThrown) {
                                       alert(textStatus);
                     },
                     complete: function (jqXHR, textStatus) {
                         //  HasInHeadingRequest = false;
                     }

                 });
             },
             minLength: 1,
             close: function (event, ui) {              
               
                 //alert("sfsd");
                 if (!IsSelected && event.target.value.length != 0) {
                     //SlideElement($(this).siblings('.inputComment').attr('id'), true);
                     $(this).siblings('.inputComment').show();
                     $(this).siblings('.inputError').show();
                 }

                 else
                 {
                    $(this).siblings('.inputComment').hide();
                 }

                 IsSelected = false;
                 
             },
             open: function (event, ui) {
                
                 SlideElement($(this).siblings('.inputComment').attr('id'), false); 
                 $(this).siblings('.inputError').hide();
                
             },
             select: function (event, ui) {                
              
                 //SlideElement($(this).siblings('.inputComment').attr('id'), false); 
                 $(this).siblings('.inputComment').show();

                 $(this).siblings('.inputValid').show();
                 $(this).siblings('.inputError').hide();
                 IsSelected = true;
             }
         });
         });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <div runat="server" id="div_c" class="div_TopSpace businessDetails">
   
        
        <div class="titleFirst">Enter your business details</div>
        <div class="clear"></div>

        <div style="width:922px;" class="businessContent" >
             <div class="businessCategory containerName">
                <div class="titleSecond">Company name</div>  
                <div class="inputParent">
                    <input id="txt_name" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="name" />
                    <div id="div_NameSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_NameError" class="inputError" style="display:none;"> </div>

                     <div id="div_NameComment" style="display:none;" class="inputComment"><%#businessNameError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                </div>
            </div>

             <div class="businessCategory containerContactPerson">
                <div class="titleSecond">Contact person</div>  
                <div class="inputParent">
                    <input id="txt_contactPerson" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="name" />
                    <div id="div_ContactPersonSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_ContactPersonError" class="inputError" style="display:none;"> </div>

                     <div id="div_ContactPersonComment" style="display:none;" class="inputComment"><%#contactPersonError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                </div>
            </div>

            <div class="businessCategory containerAddress">
                <div class="titleSecond">Business address</div>

                <div class="inputParent inputParentAddress">                
                    <businessDetails:searchGoogle runat="server" ID="searchGoogle1"></businessDetails:searchGoogle>

                    <div id="div_AddressSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_AddressError" class="inputError" style="display:none;"> </div>             

                    <div id="div_AddressComment" style="display:none;" class="inputComment"><%#businessAdress%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                
                </div>
            </div>

                
            <div class="businessCategory containerCtategory">
                <div class="titleSecond">Service category</div>

                <div class="inputParent inputParentAddress">
                    <asp:TextBox ID="txt_Category" runat="server" CssClass="_Heading textActive"  HolderClass="place-holder"></asp:TextBox>           
               
                    <div class="inputValid" style="display:none;"  id="div_ServiceSmile"></div>
                    <div class="inputError" style="display:none;"  id="div_ServiceError"></div>
                    <div class="inputComment" style="display:none;" id="div_ServiceComment">                    
                            <%= ServicePlaceHolder%>
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>
                </div>
             </div>            

            <div class="containerNext">               
                <asp:LinkButton runat="server" ID="lb_Next" CssClass="btn_next" 
                    OnClientClick="return businessDetailsValidation();" onclick="lb_Next_Click">Next</asp:LinkButton>
            </div> 
        </div>
      
        
    </div>

 
   
   
</asp:Content>
