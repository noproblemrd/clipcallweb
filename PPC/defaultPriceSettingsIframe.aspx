﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="defaultPriceSettingsIframe.aspx.cs" Inherits="PPC_defaultPriceSettingsIframe" %>
<%@ Register Src="~/Controls/On_Off.ascx" TagName="On_Off" TagPrefix="uc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title>No Problem</title>

<!--
<link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
-->

<script type="text/javascript" src="//use.typekit.com/kxs8umo.js"></script>
<script type="text/javascript">        try { Typekit.load(); } catch (e) { }</script>

<link href="samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>

<script src="../CallService.js" type="text/javascript" ></script>  
<script type="text/javascript" src="../general.js"></script>
<script type="text/javascript" src="../ppc/ppc.js"></script>

<link href="../Ladda-master/dist/ladda.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../Ladda-master/dist/spin.min.js"></script>
<script type="text/javascript" src="../Ladda-master/dist/ladda.min.js"></script>
    
<script type="text/javascript" >
        var RankingToUpdate
        function GetRanking(elm, expertiseId, UserId, rankingId, minPrice, _span) {
            //    alert(elm.value);     
            document.getElementById(_span).style.display = "none";
            RankingToUpdate = rankingId; // + ";" + _hf;
            var price = elm.value;
            var ranking = document.getElementById(rankingId)
            var num = new Number();

            if (price.length == 0) {
                //        alert(ranking.value);
                alert("<%# GetInvalidNumber %>");
                ranking.innerHTML = "";
                return;
            }
            if (!isNaN(price) && price.indexOf(".") == -1)
                num = parseInt(price);
            else {
                alert('<%# GetInvalidNumber %>');
                ranking.innerHTML = "";
                return;
            }

            var _minprice = new Number();
            _minprice = parseInt(minPrice);
            //      alert(_minprice+"; "+num);
            if (num < _minprice) {
                document.getElementById(_span).style.display = "inline";
                CleanPageValidators();
                alert("<%# lbl_minPriceMessage.Text %>" + " " + _minprice);
                ranking.innerHTML = "";
                return;
            }
            elm.value = num;
            CleanPageValidators();
            var params = "price=" + price + "&expertiseId=" + expertiseId + "&UserId=" + UserId;
            var url = "../WebServiceSite.asmx/GetExpertiseRanking"
            CallWebService(params, url, OnCompleteRanking, OnCompleteError);
            //     parent.showDiv();
        }
        function CleanPageValidators() {
            var validators = Page_Validators;
            for (var i = 0; i < Page_Validators.length; i++) {
                Page_Validators[i].style.display = "none";
            }
        }
        function OnCompleteRanking(arg) {
            //  elmUpdate = RankingToUpdate.split(";");

            if (arg.length == 0)
                alert('00');
            else {
                var ranking = document.getElementById(RankingToUpdate);
                ranking.innerHTML = arg;
                ranking.className = (arg < 4) ? "Lead _ranking" : "NotLead _ranking";

            }
            //    parent.hideDiv();
        }
        function OnCompleteError() {
            //         top.UpdateFailed();
        }

        function witchAlreadyStep(level) {
            try {
                parent.window.witchAlreadyStep(level);
                parent.window.setLinkStepActive('linkStep5');
            }
            catch (ex) { }
        }

        
        function focusTxtBox(_txt) {
            document.getElementById(_txt).focus();
        }
        function ShowLead() {
            if (document.getElementById('leadBox').style.visibility == 'hidden')
            { document.getElementById('leadBox').style.visibility = 'visible'; }
            else
            { document.getElementById('leadBox').style.visibility = 'hidden'; }
        }

        function showDetail() {
            var a_learnMore = document.getElementById("a_learnMore");
            var _div = document.getElementById('div_detail');
            if (_div.style.visibility == 'hidden') {

                _div.style.visibility = 'visible';
                a_learnMore.className = "learn-more-closed";
                //        alert(a_learnMore.className);
            }
            else {

                _div.style.visibility = 'hidden';
                a_learnMore.className = "learn-more";
                ///         alert(a_learnMore.className);
            }
        }


        //remove update progress
        /*
        function pageLoad(sender, args) { 
        try{
        parent.hideDiv();
        }catch(ex){}
            
        }
        */

        function checkValidate() {
            //alert("checkValidate");
            var IsValid = true;
            if (!Page_ClientValidate('MyPrice')) {
                //alert("<%# GetInvalidNumber %>");
                IsValid = false;

                if (!ifExplorer8AndLess())
                {                   
                    stopActiveButtonAnim();
                }
            }

           
           if (!ifExplorer8AndLess())
           {            
             activeButtonAnim('.progress-demo #btn_SendService',200);
           }

            <%--
            if (document.getElementById("<%# cb_Notify.ClientID %>").checked) {
                var num = document.getElementById("<%# txt_Notify.ClientID %>").value;
                if (!Is_Integer(num)) {
                    document.getElementById("<%# lbl_NotifyMissing.ClientID %>").style.display = "inline";
                    IsValid = false;
                }
                else {
                    var max_ranking = GetMaxValue();
                    if (num < max_ranking) {
                        document.getElementById("<%# lbl_NotifyMust.ClientID %>").style.display = "inline";
                        IsValid = false;
                    }
                }
            }
            --%>
            /*
            if (IsValid)
            parent.showDiv();
            */
            return IsValid;
        }

        function RemoveZero(elm) {
            if (elm.value == "0")
                elm.value = "";
        }
        function init() {            
            appl_init();
            parent.hideDiv();
        }

        window.onload = init;
        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);
        }
        function BeginHandler() {
            //parent.showDiv();

        }
        function EndHandler() {
            ////_SetOnOff();
            //parent.hideDiv();
        }
        <%--
        function notify(elem) {
            var txt_notify = document.getElementById("<%# txt_Notify.ClientID %>");
            if (elem.checked) {

                txt_notify.disabled = false;
                txt_notify.readOnly = false;
                txt_notify.value = GetMaxValue();
                txt_notify.className = "notifytext";
                //      txt_notify.focus();
            }
            else {

                txt_notify.disabled = true;
                txt_notify.value = "";
                txt_notify.readOnly = true;
                txt_notify.className = "notifytextoff";
                RemoveError();

            }
        }
        

        function RemoveError() {
            document.getElementById("<%# lbl_NotifyMissing.ClientID %>").style.display = "none";
            document.getElementById("<%# lbl_NotifyMust.ClientID %>").style.display = "none";

        }
        --%>
        function GetMaxValue() {
            var _val = 0;
            var _table = document.getElementById("<%# _GridView.ClientID %>");
            var elements = getElementsWithMultiClassesByClass("_ranking", _table, "span");
            var values = new Array();

            for (var i = 0; i < elements.length; i++) {
                var RankValue = elements[i].innerHTML;

                if (RankValue.length == 0)
                    continue;
                var rank_value = parseInt(RankValue);
                //        alert("ran= "+rank_value+" val= "+_val+(rank_value > _val));
                if (rank_value > _val)
                    _val = rank_value;
            }
            var hidden_value = parseInt(document.getElementById("<%# hf_HiddenValue.ClientID %>").value);
            if (hidden_value > _val)
                return hidden_value;
            return _val;
        }

        function showHideRankingBubble()
        {            
            if(document.getElementById('div_RankingComment').style.display=='none')
                document.getElementById('div_RankingComment').style.display='block';
            else
                document.getElementById('div_RankingComment').style.display='none';
        }

    </script>

    
</head>


<body style="background-color:transparent;">


<form id="form_credits" runat="server">
<div class="defaultPriceSettings">
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
        </asp:ScriptManager>   
        <div class="subtitle">
            <asp:Label ID="lbl_ManageBalance" CssClass="title" runat="server" Text="Set your price below, and see how it affects your ranking."></asp:Label>
        </div> 
        <div class="gridContainer">
    <asp:UpdatePanel ID="_UpdatePanelBid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div class="inputParent">
            <div class="inputComment" id="div_RankingComment" style="display:none;">
                Calls from customers are dispatched to service
                <br />
                providers based on their ranking. Ranking also
                <br />
                varies across regions.                   
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                        <div class="closeX" onclick="javascript:showHideRankingBubble();">x</div>
                    </div>
            </div>

            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                CssClass="data-table" PageSize="10"
              AllowPaging="true" onpageindexchanging="_GridView_PageIndexChanging">
            <RowStyle CssClass="even" />
            <AlternatingRowStyle CssClass="odd" />
            <Columns>
            
            <asp:TemplateField SortExpression="Heading"  HeaderStyle-Width="230" ItemStyle-Width="230">
            <HeaderTemplate>
                <asp:Label ID="Label1" runat="server" Text="<%# lbl_heading.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lbl_expertiseName" runat="server" Text="<%# Bind('ExpertiseName') %>"></asp:Label>                
                <asp:Label ID="lbl_HeadingId" runat="server" Text="<%# Bind('ExpertiseId') %>" Visible="false"></asp:Label>
                <asp:Label ID="lbl_ItemId" runat="server" Text="<%# Bind('Id') %>" Visible="false"></asp:Label>
                
            </ItemTemplate>
            </asp:TemplateField>
            
             <asp:TemplateField SortExpression="MyPrice"  HeaderStyle-Width="187" ItemStyle-Width="187">
            <HeaderTemplate>
                <asp:Label ID="Label3" runat="server" Text="<%# lbl_MyPrice.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lbl_sym" runat="server" Text="<%# lbl_money.Text %>"></asp:Label>
                <asp:TextBox ID="txt_MyPrice" runat="server" Text="<%# Bind('Price') %>"></asp:TextBox>
                
                
                <span id="span_minPrice" runat="server" style="display:none;" class="error-msg">
                    <asp:Label ID="Label2" runat="server" Text="*"></asp:Label>                    
                    <asp:Label ID="lbl_minPrice" runat="server" Text="<%# Bind('MinimumPrice') %>" Visible="false"></asp:Label>
                </span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_minPrice" runat="server" Display="Dynamic" CssClass="error-msg"
                ErrorMessage="*" ValidationGroup="MyPrice" ControlToValidate="txt_MyPrice"></asp:RequiredFieldValidator>
                
                <asp:RangeValidator ID="RangeValidator_minPrice" runat="server" ControlToValidate="txt_MyPrice"
                 CssClass="error-msg"
                Display="Dynamic" ValidationGroup="MyPrice" MinimumValue="1" MaximumValue="100"
                Type="Integer" ErrorMessage="*"></asp:RangeValidator>
               
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField SortExpression="Ranking"  HeaderStyle-Width="132" ItemStyle-Width="132">
            <HeaderTemplate>
                <asp:Label ID="Label5" runat="server" Text="<%# GetMyRankingSentence %>"></asp:Label>&nbsp;<span id="bubbleRanking" class="bubbleRanking"><a href="javascript:void(0);" onclick="showHideRankingBubble();">?</a></span>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lbl_ranking" runat="server" Text="<%# Bind('Ranking') %>" CssClass="_ranking"></asp:Label>
               
            </ItemTemplate>
            </asp:TemplateField>
            
            <%-- 
            <asp:TemplateField SortExpression="IsBid" HeaderStyle-Width="300" ItemStyle-Width="300">
            <HeaderTemplate>
                <asp:Label ID="Label7" runat="server" Text="<%# lbl_allowBid.Text %>"></asp:Label>
                <a href="javascript:showDetail();">
                <asp:Image ID="img_AllowBid" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
                </a>
            </HeaderTemplate>
            <ItemTemplate>
                <uc1:On_Off ID="On_Off1" runat="server" _ON="<%# Bind('IsBid') %>" _InAuction="<%# Bind('ExpertiseIsAuction') %>" />
            </ItemTemplate>
            </asp:TemplateField>
            --%>

            </Columns>
            <PagerStyle HorizontalAlign="Center" CssClass="pager" />
            </asp:GridView>
        
        
        <asp:HiddenField ID="hf_HiddenValue" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <%--
    <div style="margin: 10px 0px 10px 13px;">*<asp:Label ID="lbl_comments" runat="server" Text="The ranking can be different according to region"></asp:Label></div>              
    --%>

    <%--
    <div class="notify">
        <asp:UpdatePanel ID="_UpdatePanel_notify" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
            <div style="width:422px;">
            <asp:CheckBox ID="cb_Notify" CssClass="checkbox" runat="server" Text="Notify me when my ranking is getting below" onclick="notify(this);" />
            <asp:TextBox ID="txt_Notify" runat="server" onkeypress="RemoveError();"></asp:TextBox>        
            <asp:Label ID="lbl_NotifyMissing" runat="server" Text="Invalid" style="display:none;" CssClass="error-msg"></asp:Label><br />
            <asp:Label ID="lbl_NotifyMust" runat="server" Text="The value must be equal or lower than the lower value" style="display:none;" CssClass="error-msg"></asp:Label>
        </ContentTemplate>
        </asp:UpdatePanel>
        </div>
        
    </div>
    --%>


        <div id="containerNext" class="containerNext">
                <section class="progress-demo">
                <asp:LinkButton ID="btn_SendService" runat="server" data-style="expand-right" data-color="blue" class="ladda-button" OnClientClick="javascript:return checkValidate();" OnClick="btnSend_Click" ValidationGroup="MyPrice">
                    <span class="ladda-label">Update</span><span class="ladda-spinner">
                    </span><div class="ladda-progress" style="width: 138px;"></div>
                  </asp:LinkButton>                 
                </section>      
         </div>  
    
        <%--
        <div  style="border-bottom: solid 2px #ccc; width: 490px; padding: 0 0 35px; margin-left: 10px;" id="div_LearnMore" runat="server">

        
        <a href="javascript:showDetail();" class="learn-more" id="a_learnMore">
        <asp:Label ID="lbl_learn" runat="server" Text="Learn More"></asp:Label>
        </a>
        </div>
        --%>
          
      <div class="step5-more-details" id="div_detail" style="visibility: hidden;">
    <span class="top"></span>
    <p class="p_credit">
    <asp:Label ID="lbl_TextCallPurchas" runat="server" ></asp:Label></p>
    <span class="bottom"></span>
</div>  
    
          

        
<asp:Label ID="lbl_mesValidCallPrice" runat="server" Text="Not a valid price-per-call" Visible="false"></asp:Label>

<asp:Label ID="lbl_NEXT" runat="server" Text="NEXT" Visible="false"></asp:Label>
<asp:Label ID="lbl_UPDATE" runat="server" Text="UPDATE" Visible="false"></asp:Label>

<asp:Label ID="lbl_heading" runat="server" Text="Category" Visible="false"></asp:Label>
<asp:Label ID="lbl_MyPrice" runat="server" Text="Default price" Visible="false"></asp:Label>
<asp:Label ID="lbl_MyRanking" runat="server" Text="Ranking" Visible="false"></asp:Label>
<asp:Label ID="lbl_allowBid" runat="server" Text="Allow bid improvement in real time" Visible="false"></asp:Label>
<asp:Label ID="lbl_allowBidAudit" runat="server" Text="Allow bid" Visible="false"></asp:Label>


<asp:HiddenField ID="lbl_invalid" runat="server" Value="The value is not valid or missing. Please enter a valid number" />
<asp:Label ID="lbl_minPriceMessage" runat="server" Text="The bid is lower than allowed" Visible="false"></asp:Label>

<asp:Label ID="lbl_money" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lbl_RechargeAmountShouldBigger" runat="server" Visible="false" Text="Recharge amount should be bigger or equal to pre define price"></asp:Label>
<%--
    <asp:Image ID="_img_AllowBid" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
    --%>

 </div>   
</form>   
</body>
</html>