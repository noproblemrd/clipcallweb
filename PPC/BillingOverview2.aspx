﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BillingOverview2.aspx.cs" Inherits="PPC_BillingOverview2" MasterPageFile="~/Controls/MasterPagePPC2.master"%>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/menuVerticalBilling.ascx" TagName="menuVertical" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server"> 
    <title></title>
   
    
    
 <script>

        function showBubble() {
            document.getElementById("ContainerBubble").style.display = 'block';
        }

        function closeBubble() {
            document.getElementById("ContainerBubble").style.display = 'none';
        }

        function serverError() {
            SetServerComment2("<%#GetServerError%>");

        }

        function init() {        
            
            parent.hideDiv();
        }
        

        window.onload = init;

    </script>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server"> 
  
     <uc1:menuVertical  ID="menuVertical" runat="server" whoActive="billingOverview"/>

    <div class="BillingOverview checkout dashboardIframe iframe-inner step5">
    <uc1:PpcComment runat="server" ID="_comment"  />
     <div class="titleFirst">
        <span>Billing Overview</span>
     </div>
     
              
     <div class="tables">
     <table class="tow-tables">
     <tr>
     <td class="Media-left">

            <div class="BillingOverviewTitle">
                <table class="tg __tg">
                <tr class="tr_SubTitle">
                    <td class="first subTitle"><asp:Label ID="lbl_plan" runat="server" Text="Lead Booster"></asp:Label></td>               
                    <td class="changeYourPlan"><!--<a href="">Change your plan</a>--></td>
                </tr>
                </table>
            </div>

            <div class="orderPreview">
            <div class="Table-Title">
                <span class="_table_title">Order preview</span>                
            </div>

            <table class="tg __tg">
                <tr class="_hs ">
                    <th class="first"><asp:Label ID="Label8" runat="server" Text="Item"></asp:Label></th>
                    <th class="second">&nbsp;</th>                 
                    <th><asp:Label ID="Label12" runat="server" Text="Price"></asp:Label></th>
                </tr>

                <tr  id="tr_LeadBooster" class="_rs billingCategoryTitle billingCategory" runat="server" Visible="true" >                
                    <td class="billingCategoryTitleText"><asp:Label runat="server">Lead Booster plan</asp:Label></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr id="tr_LeadBooster2" runat="server" class="_rs" Visible="true">                
                    <td class="orderPreviewContentTitle">Leads from new customers</td>
                    <td></td>
                    <td class="payPerLead">                   
                    <a href="javascript:void(0);" onclick="showBubble();">Pay per lead</a>
                    </td>
                </tr>

                <tr id="tr_LeadBooster3" runat="server" class="_rs" Visible="true">                
                    <td class="orderPreviewContentTitle">Monthly fee</td>
                    <td></td>
                    <td>$0.00</td>
                </tr>

                <tr id="tr_FeaturedListing" runat="server" class="_rs billingCategoryTitle billingCategory2"  Visible="true">                
                    <td class="billingCategoryTitleText"><asp:Label ID="Label10" runat="server">Featured Listing</asp:Label></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr id="tr_FeaturedListing2" runat="server" class="_rs" Visible="true">                
                    <td class="orderPreviewContentTitle">Monthly fee</td>
                    <td></td>
                    <td>$<asp:Label ID="lbl_MonthlyFee" runat="server"></asp:Label></td>
                </tr>

                 <tr id="tr_FeaturedListing3" runat="server" class="_rs billingCategory" Visible="true">                
                    <td class="orderPreviewContentTitle discount">Discount*</td>
                    <td></td>
                    <td class="discount">-$<asp:Label ID="lbl_discount" runat="server"></asp:Label></td>
                </tr>

                
                <tr class="_rs billingCategoryEnd" >                
                    <td></td>
                    <td class="totalPayment">Total payment</td>
                    <td class="totalPaymentSum"><span id="totalPaymentSum" runat="server" ></span></td>
                </tr>

            </table>
            </div>


            <div id="ContainerBubble" class="ContainerBubblePayPerLead">

                <div class="bubblePerLead">
                    When you get a lead from us, you decide how
                    <br />
                    much you want to pay for the call. We then
                    <br />
                    charge the amount you set.
                </div>
                    
                <div class="bubbleTriangular">
                </div>

                <div class="close" onclick="closeBubble();">x</div>   
                                    
            </div>

            <div class="accountActivity">
            <div class="Table-Title ">
                <span class="_table_title">Account activity</span>
                <asp:Label ID="lbl_month" runat="server" CssClass="AccountActivityMonth"></asp:Label>
            </div>
            <table class="tg __tg">
            <tr class="_hs">
                <th class="first"><asp:Label ID="Label1" runat="server" Text="Activity"></asp:Label></th>
                <th ><asp:Label ID="Label3" runat="server" Text="Amount"></asp:Label></th>
                <th ><asp:Label ID="Label5" runat="server" Text="Spent"></asp:Label></th>
                <th ><asp:Label ID="Label7" runat="server" Text="Earned"></asp:Label></th>
            </tr>

            <tr id="tr_Leads" class="_rs" runat="server" >
                <td class="first"><asp:Label ID="Label9" runat="server" Text="Leads" ></asp:Label></td>
                <td><asp:Label ID="lbl_LeadsAmount" runat="server"  ></asp:Label></td>
                <td><asp:Label ID="lbl_LeadsSpent" runat="server" ></asp:Label></td>
                <td><asp:Label ID="lbl_LeadsEarned" runat="server" ></asp:Label></td>
            </tr>

            <tr id="tr_Refunds" class="_rs" runat="server">
                <td class="first"><asp:Label ID="Label2" runat="server" Text="Refunds"></asp:Label></td>
                <td><asp:Label ID="lbl_RefundsAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_RefundsSpent" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_RefundsEarned" runat="server"></asp:Label></td>
            </tr>

            <tr id="tr_MonthlyFee" class="_rs" runat="server">
                <td class="first"><asp:Label ID="Label4" runat="server" Text="Monthly fee"></asp:Label></td>
                <td><asp:Label ID="lbl_MonthlyFeeAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_MonthlyFeeSpent" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_MonthlyFeeEarned" runat="server"></asp:Label></td>
            </tr>

            <tr id="tr_TenLeadsBonus" class="_rs" runat="server">
                <td class="first"><asp:Label ID="Label6" runat="server" Text="Cash-back bonus for 10 leads"></asp:Label></td>
                <td><asp:Label ID="lbl_TenLeadsBonusAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_TenLeadsBonusSpent" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_TenLeadsBonusEarned" runat="server"></asp:Label></td>
            </tr>

            <tr id="tr_Friends" class="_rs" runat="server">
                <td class="first"><asp:Label ID="Label19" runat="server" Text="Credit earned from friends"></asp:Label></td>
                <td><asp:Label ID="lbl_friendsAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_friendsSpent" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_friendsEarned" runat="server"></asp:Label></td>
            </tr>

            <tr id="tr_BonusCredit" class="_rs" runat="server">
                <td class="first"><asp:Label ID="Label23" runat="server" Text="Bonus credit"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusCreditAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusCreditSpent" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusCreditEarned" runat="server"></asp:Label></td>
            </tr>

            <tr class="_fs">
                <td class="first"><asp:Label ID="Label27" runat="server" Text="Total credit used"></asp:Label></td>
                <td><asp:Label ID="lbl_TotalAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_TotalSpent" runat="server" ></asp:Label></td>
                <td><asp:Label ID="lbl_TotalEarned" runat="server"></asp:Label></td>
            </tr>

            <tr class="_rr">
                <td class="first"><asp:Label ID="Label31" runat="server" Text="Bonus earned"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusEarnedAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusEarnedSpent" runat="server" ></asp:Label></td>
                <td><asp:Label ID="lbl_BonusEarnedEarned" runat="server" ></asp:Label></td>
            </tr>

            </table>
            <div class="TransactionTableShadow"></div>
            </div>
     </td>     
         
     </tr>
     </table>
     </div>
 </div>
 </asp:Content>
