﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNP.master" AutoEventWireup="true" CodeFile="RequestApproved.aspx.cs" Inherits="PPC_RequestApproved" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNP.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="RequestApproved">
    

    <div class="titleFirst">
        <span>Your request to join NoProblem is approved!</span>
    </div>
    <span class="span-block">
       Since we already worked with you, we know you, and we think you are awesome – we decided
        to approve your invitation instantly. 
    </span>
    <div class="div-set-started">
        <asp:LinkButton ID="lb_GetStarted" runat="server" CssClass="Request-Invitation" 
            onclick="lb_GetStarted_Click">Get Started</asp:LinkButton>

    </div>
</div>

</asp:Content>

