﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="invitepro2014.aspx.cs" Inherits="PPC_invitepro2014" MasterPageFile="~/Management/MasterPageAdvertiser.master"%>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="ppcComment" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script type="text/javascript" src="../PlaceHolder.js"></script>

  <script type="text/javascript">

      function validation() {
          CleanEmailValidate();
          var email = $("#<%#inviteFriendEmail.ClientID%>").val();
          //alert(email);
          var ifValidEmail = true;
          //var regFormatEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
          var regFormatEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

          if (email.search(regFormatEmail) == -1) //if match failed
          {
              errorEmail('<%#emailError%>');
              ifValidEmail = false;
              /*border: 1px solid #91CEEC;*/
              //alert('faield');
              /*
              ifValidEmail = false;

              document.getElementById("validationText").className = '';

              if (mode == 'blur') {
              if (email == "") {
              document.getElementById("validation").style.display = 'none';
              document.getElementById("validationText").className = '';
              }
              else {
              document.getElementById("validation").style.display = 'block';
              document.getElementById("validationIcon").innerHTML = "<img src='" + imgRedError.src + "'>";
              document.getElementById("validationText").innerHTML = 'Enter valid email address';
              //document.getElementById("validationText").style.color = 'red';
              document.getElementById("validationText").className = 'validationTextErrorSubmit';
              }
              }

              else if (mode == 'keyup') {
              document.getElementById("validation").style.display = 'block';
              document.getElementById("validationIcon").innerHTML = "<img src='" + imgOrangeError.src + "'>";
              document.getElementById("validationText").innerHTML = 'Enter valid email address';
              document.getElementById("validationText").className = 'validationTextErrorKeyUp'; 
              //document.getElementById("validationText").style.color = 'orange';
              }

              if (mode == 'submit') {
              document.getElementById("validation").style.display = 'block';
              document.getElementById("validationIcon").innerHTML = "<img src='" + imgRedError.src + "'>";
              document.getElementById("validationText").innerHTML = 'Enter valid email address';
              //document.getElementById("validationText").style.color = 'red';
              document.getElementById("validationText").className = 'validationTextErrorSubmit';
              }
              */
          }

          else {
              //alert("true");
              $("#<%#inviteFriendEmail.ClientID%>").addClass("textValid");
              $("#emailInstruction").hide();
              ifValidEmail = true;
              /*
              document.getElementById("validation").style.display = 'block';
              document.getElementById("validationIcon").innerHTML = "<img src='" + imgGreenError.src + "'>";
              document.getElementById("validationText").innerHTML = '';
              */
          }


          return ifValidEmail;

      }
      function CleanEmailValidate() {
          $("#<%#inviteFriendEmail.ClientID%>").removeClass("textValid");
          $("#<%#inviteFriendEmail.ClientID%>").removeClass("textError");
          $("#emailInstruction").hide();
      }
      function cleanEmail() {
          var email = $("#<%#inviteFriendEmail.ClientID%>").val();
          if (email == "<%#hiddenEmail.Value%>")
              $("#<%#inviteFriendEmail.ClientID%>").val("");
      }

      function focusEmail() {
          //  var email = $("#<%#inviteFriendEmail.ClientID%>").val();
          /*
          if(email!="<%#hiddenEmail.Value%>")
          {
          $("#<%#inviteFriendEmail.ClientID%>").css('color','#8EBB03');
          $("#<%#inviteFriendEmail.ClientID%>").css('font-style','normal');
          }
          */
          CleanEmailValidate();
      }

      function blurEmail() {
          /*
          var email = $("#<%#inviteFriendEmail.ClientID%>").val();
          if(email=="")
          {
          $("#<%#inviteFriendEmail.ClientID%>").val("<%#hiddenEmail.Value%>");
          $("#<%#inviteFriendEmail.ClientID%>").css('color','#ccc');
          }
          else if(email!="<%#hiddenEmail.Value%>")
          {
          $("#<%#inviteFriendEmail.ClientID%>").css('color','#408EB4');
          $("#<%#inviteFriendEmail.ClientID%>").css('font-style','normal');
          }
          */
          validation();
      }

      function cleanMessage() {
          var message = $("#inviteFriendMessage").val();
          if (message == "<%#HiddenMessage.Value%>")
              $("#inviteFriendMessage").val("");
      }

      function focusMessage() {
          var message = $("#inviteFriendMessage").val();
          if (message != "<%#HiddenMessage.Value%>") {
              $("#inviteFriendMessage").css('color', '#8EBB03');
              $("#inviteFriendMessage").css('font-style', 'normal');
          }
      }

      function blurMessage() {
          var message = $("#inviteFriendMessage").val();
          if (message != "<%#HiddenMessage.Value%>") {
              $("#inviteFriendMessage").css('color', '#408EB4');
              $("#inviteFriendMessage").css('font-style', 'normal');
          }
      }

      function showMessage() {
          document.getElementById("inviteEmailMessage").style.display = 'block';
          document.getElementById("sendInvitationAddMessage").style.display = 'none';
      }

      function errorEmail(txt) {
          $("#<%#inviteFriendEmail.ClientID%>").addClass("textError");
          $("#emailInstruction").show();
          $("#inputCommentText").html(txt);
      }



      function inviteFriend() {
          if ($("#<%#inviteFriendEmail.ClientID%>").val() == "") {
              errorEmail('<%#emailMissingFriend%>');
              return false;
          }

          if (!validation())
              return false;

          RemoveServerComment();

          var inviteFriendEmail = $("#<%#inviteFriendEmail.ClientID%>").val();
          var inviteFriendMessage = $.trim($("#inviteFriendMessage").val());

          if (encodeURIComponent(inviteFriendMessage) == encodeURIComponent("<%#HiddenMessage.Value%>"))
              inviteFriendMessage = '';

          $.ajax({
              url: "invitePro.ashx",
              data: { invitedEmail: inviteFriendEmail, invitedMessage: inviteFriendMessage, siteId: '<%#SiteId%>', supplierId: "<%#SupplierId%>" },
              dataType: "json",
              type: "POST",
              //dataFilter: function (data) { return data; },
              success: function (data) {
                  //alert(data);
                  //alert(data.result);
                  //alert(data.status);
                  //alert(data.invitationsLeft);

                  switch (data.result.toLowerCase()) {
                      case "pass":

                          switch (data.status.toLowerCase()) {
                              case "aleradyregistered": // the supplier is already registered. remember you can invite just new ones
                                  errorEmail("This email is already exists in our system");
                                  break;

                              case "alreadyinvited":
                                  errorEmail("This email is already exists in our system");
                                  break;

                              case "emailfailed":
                                  errorEmail("Ooops, something went wrong :( Please try again )");
                                  break;

                              case "noinvitationsleft":
                                  errorEmail("You have no more invitaions left");
                                  break;

                              case "needsemailverification":
                                  showHideFormVerification();

                                  //Thanks for verifying your email address. Now you can send the invite to Ido@noproblem.co.il.
                                  break;

                              case "ok":
                                  $("#emailInstruction").hide();
                                  $("#inputCommentText").html('');
                                  $("#<%#div_FriendsPic.ClientID%>").css('display', 'block');
                                  $("#<%#inviteFriendEmail.ClientID%>").val('<%#hiddenEmail.Value%>');


                                  switch (data.invitationsLeft) {
                                      case "0":

                                          $("#<%#friend1.ClientID%>").addClass("inviteProStatusRegistered");
                                          $("#<%#friend2.ClientID%>").addClass("inviteProStatusRegistered");
                                          $("#<%#friend3.ClientID%>").addClass("inviteProStatusRegistered");

                                          SetServerComment("The invitation has been sent. You have no more invitations left.");
                                          $('#<%#moreFriends.ClientID%>').css("visibility", "visible");

                                          break;

                                      case "1":
                                          $("#<%#friend1.ClientID%>").addClass("inviteProStatusRegistered");
                                          $("#<%#friend2.ClientID%>").addClass("inviteProStatusRegistered");
                                          $("#<%#friend3.ClientID%>").addClass("inviteProStatusUnRegistered");

                                          SetServerComment("The invitation has been sent. You have 1 more invitations left.");
                                          break;

                                      case "2":
                                          $("#<%#friend1.ClientID%>").addClass("inviteProStatusRegistered");
                                          $("#<%#friend2.ClientID%>").addClass("inviteProStatusUnRegistered");
                                          $("#<%#friend3.ClientID%>").addClass("inviteProStatusUnRegistered");

                                          SetServerComment("The invitation has been sent. You have 2 more invitations left.");
                                          break;
                                  }

                                  location.hash = "#CommentPlace";

                                  break;

                              default:
                                  break;
                          }

                          break;

                      case "notpass":
                          errorEmail("Ooops, something wen wrong :(Please try later");
                          break;

                      default:
                          break;
                  }





                  /*
                  if (data.d == true) {
                  updateSystemMessage("Great decision! From now on, all your customer calls will be recorded.");

                  $('a.-TurnOnRecordCalls').each(function () {
                  $(this).hide();
                  });
                  //GoToCommentPlace();
                     
                  }
                  */
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                  //                alert('faild');
              }
          });

      }

      function showHideFormVerification() {
          $("#inviteEmailMiddleStepVerification").show();
          $("#inviteEmailMiddleStepForm").hide();

          /*
          if($("#inviteEmailMiddleStepForm").is(":visible"))
          {
          $("#inviteEmailMiddleStepVerification").show();
          $("#inviteEmailMiddleStepForm").hide();
          }

          else
          {
          $("#inviteEmailMiddleStepVerification").hide();
          $("#inviteEmailMiddleStepForm").show();
          }
          */

      }

      function askMoreInvites() {

          $.ajax({
              url: "inviteMorePro.ashx",
              data: { siteId: "<%#SiteId%>", supplierId: "<%#SupplierId%>" },
              dataType: "json",
              type: "POST",
              //dataFilter: function (data) { return data; },
              success: function (data) {
                  //alert(data);

                  //alert(data.result);

                  if (data.result == "success") {
                      //Request for more invites sent.
                      $("#<%#moreFriends.ClientID%>").html('Request for more invites sent.');
                      $("#<%#div_FriendsPic.ClientID%>").css('margin-left', '600px');


                  }

              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                  //                alert('faild');
              }
          });

      }

      function init() {
          //$("#<%#moreFriends.ClientID%>").show();

          //SetServerComment('Thanks for verifying your email address. Now you can send the invite to <%#supplieremail%>');               
      }

      //window.onload=init;
  </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">


<div class="inviteProPage">
<div class="clear"></div>

<div class="clear"></div> 
    <div class="inviteProTitle">
        <div class="invite-icon"></div>
        <div class="inviteProTitleText">Invite a Pro</div> 
        <div id="div_FriendsPic" class="div_FriendsPic" runat="server">
            <div id="moreFriends" class="moreFriends" runat="server"><a href="javascript:void(0);" onclick="javascript:askMoreInvites();">Request more invites</a></div>
            <div id="friend1" class="inviteProStatusUnRegistered" runat="server">
            </div>
    
            <div id="friend2" class="inviteProStatusUnRegistered" runat="server">
            </div>

            <div id="friend3" class="inviteProStatusUnRegistered" runat="server">
            </div>
        </div>       
    </div>

    <div class="clear"></div>

    <div class="inviteProDesc">
        If you’ve got friends who are also hard-working, talented professionals, you can invite them to NoProblem and start earning money!
        When your professional friend joins NoProblem, you’ll make $25 of NoProblem credit plus you accrue additional credit for as long as
        that friend works with NoProblem!
    </div>

    <div class="bonuses">
        
        <div class="bonusesLeft">

            <div class="bonusesLeftIcon">
                <div class="bonusesLeftIconInner">
                    $<%=InvitationSuccessBonus%>
                </div>                
            </div>            

            <div class="bonusesLeftText">
              <div class="bonusesLeftText1">on your friend’s</div>
                
              <div class="bonusesLeftText2">first payment to</div>
               
              <div class="bonusesLeftText3">NoProblem</div>
            </div>
            
        </div>

        <div class="bonusesMiddle">
            <div class="bonusesMiddleNumber">+<%=InvitationLifetimeBonus%>%</div>
            
            <div class="bonusesMiddleText">
                <div class="bonusesMiddleText1">on your friend’s</div> 
                <div class="bonusesMiddleText2">future payments</div> 
            </div>            
        </div> 

        

    </div>

     <div class="clear"></div> 

    
    <div class="inviteEmail">            
        
        <div class="inviteEmailTable">

            <div class="inviteEmailLeft">
                <div class="inviteEmailLeftInner"></div>
            </div>

            <div class="inviteEmailMiddle">
            
                <div id="inviteEmailMiddleStepForm" class="inviteEmailMiddleStepForm" style="display:block;">
                <div class="inviteEmailMiddleTitle">
                    Invite by email
                </div>

                <div class="inviteEmailMiddleSubTitle">
                    Friend’s email
                </div>

                <div class="inviteEmailMiddleInput">                    

                <div class="inputParent">          
                        <div class="inputSun">
                           <input id="inviteFriendEmail" type="text"  runat="server" class="inviteEmailInput" onkeyup="validation('blur');" onfocus="focusEmail();" onblur="blurEmail()"/>
                        </div>

                        <!--
                        <div class="inputValid" style="display:none;"  id="MobileValid">
                
                        </div>

                        <div class="inputError" id="MobileError" style="display:none;">
          
                        </div>
                        -->
                          
                        <div class="inputComment" id="emailInstruction" style="display:none;">
                                    <span id="inputCommentText"></span>
                                <div class="chat-bubble-arrow-border"></div>
                                <div class="chat-bubble-arrow"></div>
                        </div>
                        
                            
                    </div>                

                <div class="inviteEmailMiddleDisclaimer">
                    We <a onclick="javascript:openWin('http://www2.noproblemppc.com/LegalPrivacy.htm#Legal');" href="javascript:void(0);">won’t share</a> this email address with anyone.
                </div>

                <div id="inviteEmailMessage" class="inviteEmailMessage">
                    <div class="inviteEmailMessageTitle">
                        Personal Message
                    </div>

                    <div class="inviteEmailMessageContent">
                        <textarea id="inviteFriendMessage"  class="inviteEmailMessageTextarea" onfocus="cleanMessage();focusMessage();" onblur="blurMessage();"><%=HiddenMessage.Value%></textarea>
                    </div>
                </div>

                <div class="inviteEmailMiddleSend">                   
                    <span id="sendInvitationBtn" class="sendInvitationBtn" class="inviteButton" onclick="javascript:inviteFriend();">SEND INVITATION</span>
                    <a id="sendInvitationAddMessage" class="sendInvitationAddMessage" href="javascript:showMessage();">+ Add a personal message</a>
                    
                </div>

                </div>
                </div>

                <div id="inviteEmailMiddleStepVerification" class="inviteEmailMiddleStepVerification" style="display:none;">
                    <div class="inviteEmailMiddleTitle">
                        Verify your email address
                    </div>

                    <div class="inviteEmailMiddleSubTitle">  

                         We needs to verify your email address to invite friends. Just click the verification
                         <br />
                         link in the email we send to you.

                    </div>
        
                    <div class="inviteEmailContent">
                        <div class="inviteEmailContent1">You didn’t get an email from us?</div>                        
                        <div class="inviteEmailContent2">Is your email address <%=supplieremail%></div>
                        <div class="inviteEmailContent3">• No, I need to <a href="FramePpc.aspx">update my email address.</a></div>
                        <div class="inviteEmailContent4">• Yes, please <a href="javascript:void(0);" onclick="javasceipt:inviteFriend();">resend</a> the email.</div>
                    </div>

                </div>
                
            </div>
           
            <div class="inviteEmailRight">
                <div class="inviteEmailRightText">
                    <div class="inviteEmailRightText1">Choose your friends wisely;</div>
                    <div class="inviteEmailRightText2">you only have few invites!</div>  
                </div>

                <div class="inviteEmailRightTextIcon">
                </div>
                              
            </div>
        </div>        
         
    </div>
   
    <div class="bonusesFooter">
    </div>  
   
   <asp:HiddenField  ID="hiddenEmail" runat="server" Value="e.g friend@email.com"/>
   <asp:HiddenField  ID="HiddenMessage" runat="server" Value="What's on your mind..."/>
   
  
</div>
</asp:Content>
