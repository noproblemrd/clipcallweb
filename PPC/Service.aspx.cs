﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;

public partial class PPC_Service : RegisterPage, IPostBackEventHandler, IsInProcess
{
  //  string BackArg;
    const string SET_SERVICE = "SET_SERVICE";
    const string SET_STEP = "STEP";
    protected List<string> PhoneReg { get; private set; }
    //   protected int PhoneRegLength { get { return PhoneReg.Count; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        ScriptManager1.RegisterAsyncPostBackControl(a_categories);
        ScriptManager1.RegisterAsyncPostBackControl(a_FreeSearch);
        ScriptManager1.RegisterAsyncPostBackControl(btn_SendService);
        ScriptManager1.RegisterAsyncPostBackControl(btn_SendServiceList);
        ScriptManager1.RegisterAsyncPostBackControl(btn_SendService2);
        ScriptManager1.RegisterAsyncPostBackControl(btn_virtual);
        PhoneReg = Utilities.GetCheckStatusUSPhoneRegularJavaScript();
        if (!IsPostBack)
        {            
            LoadPageText();
            LoadService();
            /*
            if (!LoadService())
                ClientScript.RegisterStartupScript(this.GetType(), "SetServicePlaceHolder", "SetServicePlaceHolder();", true);
            */
            string trial = Request.QueryString["trial"];
            if (trial == "true")
            {
                SetServerComment(string.Format(TrialV, string.Format(NUMBER_FORMAT, balance)));
                lbl_ITC.Visible = true;
                lbl_NonITC.Visible = false;
            }
        }
        else
        {

            string __EVENTARGUMENT = Request["__EVENTARGUMENT"];
            if (__EVENTARGUMENT == SET_SERVICE)
                SetService();
            else if (__EVENTARGUMENT == SET_STEP + "1")
                SetFreeSearch();
            else if (__EVENTARGUMENT == SET_STEP + "2")
                SetCategoryList();
            else if (__EVENTARGUMENT == SET_STEP + "3")
                LoadCategoriesByBack();
        }
        LoadMaster();
        txt_Category.Attributes.Add("place_holder", ServicePlaceHolder);
        Header.DataBind();
    }

    private void LoadMaster()
    {
        Master.SetBenefitFreeText("Get leads from ready-to-hire customers directly to your mobile phone.</br></br>That's NoProblem!");
        Master.SetStatusBar("statusBar1");
    }

   

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        FieldMissing = Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "DetailsService"
                          select x).FirstOrDefault();
        XElement Service_element = xelem.Element("Service").Element("Validated");
        ServicePlaceHolder = Service_element.Element("Instruction").Element("General").Value;
        ChooseCategory = Service_element.Element("Instruction").Element("Category").Value;
        ChooseSkill = Service_element.Element("Instruction").Element("Skill").Value;
        TrialV = Service_element.Element("Instruction").Element("Trial").Value;
        ErrorService = Service_element.Element("Error").Value;
        lbl_ServiceInstruction.DataBind();
    }
    bool LoadService()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        string result = "";
        try
        {
            result = _supplier.GetSupplierExpertise(siteSetting.GetSiteID, SupplierId.ToString());
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            return false;
        }
        XDocument xdoc = XDocument.Parse(result);
        if (xdoc.Element("SupplierExpertise") == null || xdoc.Element("SupplierExpertise").Element("Error") != null)
            return false;
       
        if (!xdoc.Element("SupplierExpertise").HasElements)
            return false;
        string exp_name = xdoc.Element("SupplierExpertise").Element("PrimaryExpertise").Attribute("Name").Value;
        txt_Category.Text = exp_name;
      //  txt_Category.CssClass = txt_Category.CssClass.Replace("place-holder", "").Trim();

        return true;
    }
    /*
    void SetStartScript()
    {
        string _script = "SetPlaceHolder();";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "SetStartScript", _script, true);
    }
     */
    protected void a_back_click(object sender, EventArgs e)
    {
        
        
        if (hf_ParentId.Value == "")
        {
            lbl_Category.Text = "";
            SetCategoryList();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDivCategories", "SetDivCategories();", true);
        }
        else
        {
            Guid CategoryId = new Guid(hf_ParentId.Value);
            LoadCategories(CategoryId, lbl_Category.Text);
        }
    }
    protected void a_categories_click(object sender, EventArgs e)
    {
        
        /*
        LinkButton _lb = (LinkButton)sender;
        if (_lb.CommandArgument == "Category")
        {
            SetFreeSearch();
            return;
        }
         * */
        SetCategoryList();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetDivCategories", "SetDivCategories();", true);
    }
    void SetCategoryList()
    {
        /*
        div_HeadingCategoryTable.Style[HtmlTextWriterStyle.Display] = "block";
         *  * */
        hf_ParentId.Value = string.Empty;
        div_CategoryTitle.Style[HtmlTextWriterStyle.Display] = "none";
        
        lbl_Category.Text = string.Empty;
        lbl_Heading.Text = string.Empty;

        div_c.Style[HtmlTextWriterStyle.Display] = "block";

        //    div_HeadingCategoryTable.Style[HtmlTextWriterStyle.Display] = "block";

        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("arg");
        data.Columns.Add("script");

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _supplier.GetAllCategories();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            SetServerComment(GetServerError);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            SetServerComment(GetServerError);
            return;
        }
    //    BackArg = "";
        lbl_ServiceInstruction.Text = ChooseCategory;
        div_inputComment.Style[HtmlTextWriterStyle.Display] = "block";
        div_HeadingCategoryTable.Style[HtmlTextWriterStyle.Display] = "block";
        a_FreeSearch.Style[HtmlTextWriterStyle.Display] = "block";
        foreach (WebReferenceSupplier.GuidStringPair gsp in result.Value)
        {
            DataRow row = data.NewRow();
            row["Name"] = gsp.Name;
            row["arg"] = gsp.Id.ToString();
            row["script"] = "SetCategoryButton(this);";
            data.Rows.Add(row);
        }
        GetListTitle = "Service categories";
        div_SendServiceList.Style[HtmlTextWriterStyle.Display] = "none";
        _dataList.DataSource = data;
        _dataList.DataBind();
   //     a_back.DataBind();
        _UpdatePanel.Update();
        
    }
    protected void a_FreeSearch_click(object sender, EventArgs e)
    {
        SetFreeSearch();
    }
    void SetFreeSearch()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFreeSearch", "SetFreeSearch();", true);
        div_HeadingCategoryTable.Style[HtmlTextWriterStyle.Display] = "none";
        _dataList.DataSource = null;
        _dataList.DataBind();

        _UpdatePanel.Update();
    }
    /*
    protected string BackArgV
    {
        get { return BackArg; }
    }
     */
    void SetServerComment(string str)
    {
        if (!ClientScript.IsStartupScriptRegistered("ServerComment"))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerComment", "SetServerComment('" + HttpUtility.HtmlEncode(str) + "'); ResetScrollPosition();", true);
    }
    protected void btn_Category_Click(object sender, EventArgs e)
    {
        ///   string args = ((Button)sender).CommandArgument;
        LinkButton btn = ((LinkButton)sender);
        Guid _id = new Guid(btn.CommandArgument);
        LastServicesList = new KeyValuePair<Guid, string>(_id, btn.Text);
        LoadCategories(_id, btn.Text);
        
    }
    void LoadCategoriesByBack()
    {
        LoadCategories(LastServicesList.Key, LastServicesList.Value);
    }
    void LoadCategories(Guid _id, string name)
    {
        hf_ParentId.Value = string.Empty;
        hf_Heading.Value = string.Empty;
        div_CategoryTitle.Style[HtmlTextWriterStyle.Display] = "block";
        lbl_Category.Text = name;
        lbl_Heading.Text = string.Empty;
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("arg");
        data.Columns.Add("script");
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _supplier.GetCategoryGroup(_id);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            SetServerComment(GetServerError);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            SetServerComment(GetServerError);
            return;
        }
   //     BackArg = _id.ToString();
        lbl_ServiceInstruction.Text = ChooseSkill;
        div_inputComment.Style[HtmlTextWriterStyle.Display] = "block";
        a_FreeSearch.Style[HtmlTextWriterStyle.Display] = "block";
        foreach (WebReferenceSupplier.GuidStringPair gsp in result.Value)
        {
            DataRow row = data.NewRow();
            row["Name"] = gsp.Name;
            row["arg"] = gsp.Id.ToString();
            row["script"] = "SetButton(this, '" + _id.ToString() + "'); return false;";
            data.Rows.Add(row);
        }
        GetListTitle = "The service I provide";
        div_SendServiceList.Style[HtmlTextWriterStyle.Display] = "none";
        _dataList.DataSource = data;
        _dataList.DataBind();
        
        ScriptManager.RegisterStartupScript(this, this.GetType(), "set_step_3", "set_step_3('true');", true);
    //    a_back.DataBind();
        _UpdatePanel.Update();
    }
    protected void btn_SendService_click(object sender, EventArgs e)
    {               
        SetService();
    }
    protected void btn_SendServiceList_click(object sender, EventArgs e)
    {
        SetService();
    }
    void HeadingProblem(string _service)
    {
        string _mess = string.Format(ErrorService, _service);
        string _script = @"SetServerComment('" + HttpUtility.HtmlEncode(_mess) + "'); ResetScrollPosition();";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "HeadingProblem", _script, true);
    }
    void ServerProblem(string _mess)
    {
        string _script = @"SetServerComment(""" + HttpUtility.HtmlEncode(_mess) + @"""); ResetScrollPosition();";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerProblem", _script, true);
    }
    void SetService()
    {
        string _service = hf_Heading.Value;
        if (string.IsNullOrEmpty(_service))
        {
            HeadingProblem(_service);
            return;
        }
        PpcSite _head = PpcSite.GetCurrent();
        Guid _id = _head.GetHeadinGuid(_service);
        if (_id == Guid.Empty)
            HeadingProblem(_service);
        else 
        {
            if (Utilities.SaveHeadings(SupplierId.ToString(), new string[] { _id.ToString() }, siteSetting.GetUrlWebReference, SupplierId))
            {
                Response.Redirect("Location.aspx");
                return;
            }
            else
                ServerProblem(GetServerError);
        }
        
        
         a_categories.Style[HtmlTextWriterStyle.Display] = "none";
         a_FreeSearch.Style[HtmlTextWriterStyle.Display] = "none";
         div_HeadingCategoryTable.Style[HtmlTextWriterStyle.Display] = "block";
         div_HeadingText.Style[HtmlTextWriterStyle.Display] = "block";
        
        SetCategoryList();
        SetServiceError();
    }
    void SetServiceError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetServiceError", "SetServiceError();", true);       
    }
    protected string GetHeadingServiceList
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/GetHeadingList"; }
    }
   
    
    protected string GetIsHeadingService
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/Is_Heading"; }
    }
    /*
    protected string GetSetServiceService
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/SetService"; }
    }
    */
    
    protected string Site_Id
    {
        get { return siteSetting.GetSiteID; }
    }
    
    protected string LoginPage
    {
        get { return ResolveUrl("~") + "Management/LogOut.aspx"; }
    }

    protected string RedirectLocation
    {
        get { return ResolveUrl("~") + "PPC/Location.aspx"; }
    }
    protected string ErrorService
    {
        get { return (string)ViewState["ErrorService"]; }
        set { ViewState["ErrorService"] = value; }
    }
    protected string ServicePlaceHolder
    {
        get { return (string)ViewState["ServicePlaceHolder"]; }
        set { ViewState["ServicePlaceHolder"] = value; }
    }
    protected string ChooseCategory
    {
        get { return (string)ViewState["ChooseCategory"]; }
        set { ViewState["ChooseCategory"] = value; }
    }
    protected string ChooseSkill
    {
        get { return (string)ViewState["ChooseSkill"]; }
        set { ViewState["ChooseSkill"] = value; }
    }
    protected string GetServerError
    {       
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }
    protected string FieldMissing
    {
        get { return (string)ViewState["FieldMissing"]; }
        set { ViewState["FieldMissing"] = value; }
    }
    protected string TrialV
    {
        get { return (string)ViewState["Trial"]; }
        set { ViewState["Trial"] = value; }
    }
    protected string GetListTitle { get; private set; }
    
    public void RaisePostBackEvent(string eventArgument)
    {
        return;
    }
    protected string setEnterPostBack()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(btn_SendService, SET_SERVICE);
        myPostBackOptions.PerformValidation = false;

        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
    }
    protected string setStep(string _num)
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(btn_virtual, SET_STEP + _num);
        myPostBackOptions.PerformValidation = false;

        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
    }
    protected override void Render(HtmlTextWriter writer)
    {
        Page.ClientScript.RegisterForEventValidation(btn_SendService.UniqueID, SET_SERVICE);
        for (int i = 1; i < 4; i++)
            Page.ClientScript.RegisterForEventValidation(btn_virtual.UniqueID, SET_STEP + i);
        base.Render(writer);
    }
    
    KeyValuePair<Guid, string> LastServicesList
    {
        get { return (ViewState["LastServicesList"] == null) ? new KeyValuePair<Guid, string>() : (KeyValuePair<Guid, string>)ViewState["LastServicesList"]; }
        set { ViewState["LastServicesList"] = value; }
    }
     
}