﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_afterPayment : PageSetting
{
    protected string beforeEnterancePage;
    protected void Page_Load(object sender, EventArgs e)
    {
        string host;
        host = Request.Url.Host;
       
        if (host == "localhost")
            beforeEnterancePage = "http://" + host + ":" + Request.Url.Port + ResolveUrl("~") + "ppc/beforeEnterance.aspx";
        else
            beforeEnterancePage = "http://" + host + "/ppc/beforeEnterance.aspx";

        Page.Header.DataBind();
        
    }
}