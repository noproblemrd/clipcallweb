﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="defaultPriceSettings.aspx.cs" Inherits="PPC_defaultPriceSettings" MasterPageFile="~/Controls/MasterPagePPC2.master"%>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/menuVerticalBilling.ascx" TagName="menuVertical" TagPrefix="uc1" %>

<%@ Register Src="~/Controls/On_Off.ascx" TagName="On_Off" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server"> 
    <title></title>
    <script type="text/javascript" >
      

        function UpdateDefaultPriceSuccess() {
            SetServerComment2("<%#UpdateSuccess%>");
        }

        function UpdateFailed() {
            alert("UpdateFailed");
            SetServerComment2("<%#UpdateFaild%>");

        }

        function NoNewDataDefaultPrice() {
            alert("<%#NoNewData%>");
            SetServerComment2("<%#NoNewData%>");
        }

        function serverError() {
            SetServerComment2("<%#GetServerError%>");

        }  
         
    </script>

   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server"> 
  
     <uc1:menuVertical  ID="menuVertical" runat="server" whoActive="defaultPriceSettings"/>

     <div class="paymentMethod dashboardIframe">
         <uc1:PpcComment runat="server" ID="_comment"  />
           <div class="titleFirst">
                <span>Default price settings</span>
            </div>

            <iframe src="defaultPriceSettingsIframe.aspx" width="587px" height="1200px" frameborder="0" scrolling="auto"></iframe>

     </div>
     
    
 </asp:Content>