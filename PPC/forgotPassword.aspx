﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="forgotPassword.aspx.cs" MasterPageFile="~/Controls/MasterPageNP2.master" Inherits="PPC_forgotPassword"  ClientIDMode="Static"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="../PlaceHolder.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>
<link href="samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />
<!--[if IE 7]>      
      <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome-ie7.min.css" />
    <![endif]-->  
    
    
<script>
   

</script>  
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="registration login forgotPassword">   

    <div class="registrationLeft">

       <div  id="titleFirst" runat="server" class="titleFirst">
            <span>Forgot password</span>
        </div>      

        <div id="subTitle" runat="server" class="subTitle">
            To reset your password, please enter the email address you registered with.
        </div>
        
       <div  id="registrationInputs" runat="server" class="registrationInputs" >            

           <div class="emailContainer">
                <div>Enter your email address</div>  
                <div class="inputParent">
                    <input id="txt_email" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="email" />
                    <div id="div_EmailSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_EmailError" class="inputError" style=""> </div>

                

                    <div class="inputValid" style="display:none;"  id="div1"></div>

                    <div id="div_EmailComment" style="display:none;" class="inputComment"><%#EmailError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>

                     <div id="containerNext" class="containerNext">
                        <section class="progress-demo">
                        <asp:LinkButton ID="btn_SendService" runat="server" data-style="expand-right" data-color="blue" class="ladda-button" OnClientClick="javascript:return forgotPassword();" OnClick="btnSend_Click" >
                            <span class="ladda-label">Reset my password</span><span class="ladda-spinner">
                            </span><div class="ladda-progress" style="width: 138px;"></div>
                          </asp:LinkButton>                 
                        </section>      
                    </div> 
                </div>
            </div>        

            <div class="clear"></div>     

 
            
       </div>  

      
   
   
    
    </div>

    <div class="registrationRight">
        No account? <a href="register.aspx">Join NoProblem</a>
        
    </div>
    
</div>
</asp:Content>
