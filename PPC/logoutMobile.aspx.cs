﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_logoutMobile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["RegisterUser2014"] = null;
        Session["Username2014"] = null;
        Response.Redirect("ppcLoginMobile.aspx");
    }
}