﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="choosePlan.aspx.cs" MasterPageFile="~/Controls/MasterPageNP2.master" Inherits="PPC_choosePlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="../scripts/BidCounter.js" type="text/javascript"></script>
    <script>
        var ROOT = '<%# ROOT %>';

        _counter.format = "regular";
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="ChoosePlan">

   <div class="choosePlanCaption">

    <div class="titleFirst">
        <span>Choose advertising plan</span>
    </div>
   
    <div class="comparePlans"><a href="compareplans.aspx">Compare plans</a></div>
   </div> 

   <div class="choosePlanLeft">
         <div class="planTitle">
             5,000,000 daily visits to NoProblem directory
         </div>
           
         <div class="planSubTitle planSubTitleLeft">
            Featured Listing
         </div>  
            
        <div class="planSum planSumleft">
            $<span id="featureListingLeftSum" runat="Server" class="planSumNumber"></span>&nbsp; <span class="planSumText">/month</span>
        </div>
        
        <div class="planContent"> 
            <div>
                ✓ Premium business page ✓ Photo and video gallery 
            </div>    
            
            <div>
                ✓ Get rid of competitor ads ✓ Featured exposure
            </div>
            
        </div>  

        <div class="containerFeauterdButton">
            <asp:Button ID="featuredListingPerZipCode"  runat="server"  onclick="featuredListingPerZipCode_Click"  Text="Select"/>
        </div>   
    </div>

    <div class="choosePlanRight">

         <div class="yellowLine">

         </div>

         <div class="seret" runat="server" id="ribbon">

         </div>
         


         <div class="planTitle">
             <span class="counterBox"></span>&nbsp; leads served so far…
         </div>        

         <div class="planSubTitle">
            Lead Booster
         </div>  
            
        <div class="planSum">
            $<span id="Span1" runat="Server" class="planSumNumber">0</span> <span class="planSumText">/month</span>
        </div>

        <div class="planSumUnder">
            Pay only for calls you accept
        </div>

        
        <div class="planContent"> 
            <div>
                ✓ You set the price ✓ Lead volume via partners  
            </div>    
            
            <div>
                ✓ Rich leads: Description + address ✓ Call recording 
            </div> 
        </div>  


        <div class="planSubTitle planSubTitleRight2">
            Featured Listing
        </div>  
            
        <div class="planSum planSumRight2">
            $<span id="featureListingRightSum" runat="Server" class="planSumNumber"></span>&nbsp; <span class="planSumText">/month</span>
        </div>
        
        <div class="planContent planContentRight2"> 
            <div>
                ✓ Premium business page ✓ Photo and video gallery 
            </div>    
            
            <div>
                ✓ Get rid of competitor ads ✓ Featured exposure
            </div>
            
        </div>  

        <div class="containerFeauterdButton">
         <asp:Button ID="leadBoosterPerZipCode"  runat="server" onclick="leadBoosterPerZipCode_Click" Text="Select"/> 
        </div> 
         
    </div>

</div>

</asp:Content>
