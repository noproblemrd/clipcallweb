﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPagePPC.master" AutoEventWireup="true" CodeFile="AddCredit.aspx.cs" Inherits="PPC_AddCredit" %>

<%@ Register src="../Controls/AddCredit.ascx" tagname="AddCredit" tagprefix="uc1" %>
<%@ MasterType VirtualPath="~/Controls/MasterPagePPC.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script type="text/javascript">

   

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<div>
<div class="title">
    <asp:Label ID="lbl_YourBalance" runat="server" Text="Add Credit"></asp:Label>
</div>
<div class="div-TitleDetails" >
    <span class="span-block">Choose the prepaid plan that best supports your activity as a NoProblem service professional. The bigger the plan, the more value for your money.</span>
    <div class="div-parent-Packages">
<uc1:AddCredit ID="_AddCredit" runat="server" />
</div>
</div>
   
    </div>
   
</asp:Content>

