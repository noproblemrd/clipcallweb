﻿<%@ Page Language="C#" MasterPageFile="~/Controls/MasterPageNPTemp.master" AutoEventWireup="true" CodeFile="Register2_28_5_2014.aspx.cs" Inherits="PPC_Register2_28_5_2014" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNPTemp.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" src="../general.js"></script>

<script type="text/javascript" src="../scripts/US-Phone.js"></script>
<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript">
    var _phone = new np_phone('<%# PhoneRegExpression %>');
    _phone.init();
    _phone.OnKeyUpEmpty = function (txtId) {
        ClearPhoneValidate();
    };
    _phone.OnMatch = function (txtId) {
        ClearPhoneValidate();
        $('#div_PhoneSmile').show();
        $('#<%# txt_phone.ClientID %>').addClass('textValid');
    };
    _phone.OnKeyUpOnTheWay = function (txtId) {
        ClearPhoneValidate();
    };
    _phone.OnFaild = function (txtId) {
        ClearPhoneValidate();
        $('#div_PhoneComment').show();
        $('#div_PhoneError').show();
        $('#<%# txt_phone.ClientID %>').addClass('textError');
    };
    _phone.OnClickFaild = function (txtId) {
        _phone.OnFaild(txtId);
    };
    _phone.OnEmpty = function (txtId) {
        ClearPhoneValidate();

    };
    _phone.OnFocus = function (txtId) {
        ClearPhoneValidate();
    };
    _phone.OnClickMatch = function (txtId) {
        SendDetails(txtId);
    };
    function SendDetails(phoneId) {

        var _ref = "ThEkNiGhTrIdEr";

        /*
        if (_ref.length == 0)
        return;
        */


        $('span.btn_next').show();
        $('a.btn_next').hide();
        var phone = $('#' + phoneId).val();
        $.ajax({
            url: "<%# RegisterReferrer %>",
            data: "{'phone': '" + phone + "', 'referrer': '" + _ref + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                switch (data.d) {
                    case ('OkIsAar'):
                    case ('OkIsNew'):
                        window.location.href = 'ConfirmReferrer.aspx';
                        break;
                    case ('ReferralCodeNotFound'):

                        $('span.btn_next').hide();
                        $('a.btn_next').show();
                        SetServerComment('<%# ReferralCodeNotFound %>');
                        break;

                    case ('TransferToLogin'):
                        window.location.href = 'PpcLogin.aspx?register=true';
                        break;
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

                $('span.btn_next').hide();
                $('a.btn_next').show();
                GeneralServerError();
            }
        });
    }

    function ClearPhoneValidate() {
        $('#div_PhoneSmile').hide();
        $('#div_PhoneError').hide();
        $('#div_PhoneComment').hide();
        $('#<%# txt_phone.ClientID %>').removeClass('textError');
        $('#<%# txt_phone.ClientID %>').removeClass('textValid');
    }
    
   
   
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="step1">
    <div class="titleFirst">
        <span>Register to NoProblem</span>
    </div>

    <span class="span-block bottom-higth">
        Are you ready? You’re only a few quick steps from getting quality phone leads directly to your mobile
        phone! Start by entering your mobile phone number below. We’ll send a password via text message
        to the number you provide to verify it.
    </span>
    
   
    <div class="titleSecond"><asp:Label ID="lbl_phone" runat="server" Text="My mobile phone number is..."></asp:Label></div>

    <div class="inputParent" >
        <input id="txt_phone" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="phone" />

            <div class="inputValid" style="display:none;"  id="div_PhoneSmile">
                
        </div>
        <div class="inputError" style="display:none;"  id="div_PhoneError">
               
        </div>
        <div class="inputComment" style="display:none;" id="div_PhoneComment">
                    <%= PhonePlaceHplder %>
            <div class="chat-bubble-arrow-border"></div>
            <div class="chat-bubble-arrow"></div>
        </div>
            
    </div>
   
    <%-- 
    <div class="titleSecond emailTitle"><asp:Label ID="lbl_Referrer" runat="server" Text="Referral code"></asp:Label></div>

    <div class="inputParent">
        <input id="txt_referrer" type="password" class="textActive" runat="server" HolderClass="place-holder" />

       <div class="inputValid" style="display:none;"  id="div_ReferreSmile">
                
        </div>
        <div class="inputError" style="display:none;"  id="div_ReferreError">
               
        </div>
        
            
    </div>
    --%>

    <div>
        <a href="javascript:void(0);" button-mode="phone" class="btn_next">Next ></a>
        <span class="btn_next" style="display:none;">Processing...</span>
    </div>
    <div>
        <div class="underNext">
            <span>We’ll text the password to the phone number above.</span>
        </div>
         <div class="underNextText">
               
            <span>
                * Please note that to use No Problem’s phone lead advertising service, it is essential to have a mobile phone that’s able to receive text messages. We will not share your phone number with anyone, nor use it for anything other than bringing you new customers. We take your <a href="javascript:openWin('http://www2.noproblemppc.com/LegalPrivacy.htm');"  class="general">privacy</a> very seriously.
            </span>
        </div>
        
        
    </div>
</div>
</asp:Content>

