﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNP.master" AutoEventWireup="true" CodeFile="WaitingForApproved.aspx.cs" Inherits="PPC_WaitingForApproved" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNP.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="RequestApproved">
    

    <div class="titleFirst">
        <span>Thanks!</span>
    </div>
    <span class="span-block">
       Your request has been submitted successfully. In the meantime, feel free to <a href="http://www2.noproblemppc.com/" class="general">explore our website</a> and learn about phone lead advertising.
    </span>
</div>
</asp:Content>

