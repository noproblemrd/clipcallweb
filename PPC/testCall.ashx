﻿<%@ WebHandler Language="C#" Class="testCall" %>

using System;
using System.Web;

public class testCall : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        
        string supplierId = context.Request["supplierId"];
        string mode = context.Request["mode"];
        string testCallIdLive = context.Request["testCallId"];
        
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null,PpcSite.GetCurrent().SiteId);

        if (mode == "start")
        {
            WebReferenceSupplier.ResultOfGuid resultOfGuid = new WebReferenceSupplier.ResultOfGuid();

            try
            {
                resultOfGuid = supplier.StartGetStartedTestCall(new Guid(supplierId));


                LogEdit.SaveLog(PpcSite.GetCurrent().SiteId, "Start Call Test:" + "\r\nsupplier id: " +
                supplierId + "\r\nSession: " + HttpContext.Current.Session.SessionID +
                "\r\nip: " + Utilities.GetIP(context.Request));


                if (resultOfGuid.Type == WebReferenceSupplier.eResultType.Success)
                {
                    Guid testCallId = resultOfGuid.Value;
                    if (testCallId == Guid.Empty)
                    {
                        context.Response.Write("failed");
                    }

                    else
                    {
                        context.Response.Write(testCallId.ToString());
                    }

                }

                else
                {

                }
            }

            catch (Exception e)
            {
                dbug_log.ExceptionLog(e, PpcSite.GetCurrent().SiteId);
            }
        }

        else if (mode == "live")
        {            
            WebReferenceSupplier.ResultOfLiveTestCallStatus resultOfLiveTestCallStatus = new WebReferenceSupplier.ResultOfLiveTestCallStatus();
            try
            {
                resultOfLiveTestCallStatus = supplier.GetLiveTestCallStatus(new Guid(testCallIdLive));
                bool showInReport = resultOfLiveTestCallStatus.Value.ShowInReport;
                WebReferenceSupplier.LiveTestCallStage liveTestCallStage = resultOfLiveTestCallStatus.Value.Stage;
                
                string strliveTestCallStage="";
                
                switch (liveTestCallStage)
                {
                    case WebReferenceSupplier.LiveTestCallStage.ringing:
                        {
                            strliveTestCallStage = "ringing";
                            break;
                        } 
                        
                    case WebReferenceSupplier.LiveTestCallStage.advertiser_answered:
                        {
                            strliveTestCallStage = "advertiser answered";
                            break;
                        }
                        
                    case WebReferenceSupplier.LiveTestCallStage.advertiser_confirmed:
                        {
                            strliveTestCallStage = "advertiser confirmed"; // AFTER THE SUPPLIER CLICKED 1
                            break;
                        }

                    case WebReferenceSupplier.LiveTestCallStage.completed: // AFTER THE SUPPLIER LISTEN TO THE AUTOMATIC CALL
                        {
                            strliveTestCallStage = "completed"; // IT MEANS THAT THE CONNECTION STOPED OR AT THE END OR EVEN DURING THE CONNECTION. COMPLETED AND showInReport TRUE MAENS IT STOPED AT THE END AND THE ALL TASK FINISHED
                            break;
                        }
                            
                    case WebReferenceSupplier.LiveTestCallStage.busy:
                        {
                            strliveTestCallStage = "busy";
                            break;
                        }                   

                    case WebReferenceSupplier.LiveTestCallStage.failed:
                        {
                            strliveTestCallStage = "failed";
                            break;
                        }

                    case WebReferenceSupplier.LiveTestCallStage.no_answer:
                        {
                            strliveTestCallStage = "no answer";
                            break;
                        }

                    
                                                 
                    default:
                        break;
                }

                LogEdit.SaveLog(PpcSite.GetCurrent().SiteId, "During Call Test:" + "\r\nsupplier id: " +
                supplierId + "\r\nSession: " + HttpContext.Current.Session.SessionID +
                "\r\nip: " + Utilities.GetIP(context.Request) +
                "\r\nshowInReport: " + showInReport.ToString() + "\r\nliveTestCallStage: " + liveTestCallStage);
                
                string strResult = showInReport.ToString() + "," + strliveTestCallStage;
                context.Response.Write(strResult);
            }

            catch (Exception e)
            {
                dbug_log.ExceptionLog(e, PpcSite.GetCurrent().SiteId);
            }
            
            
        }
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}