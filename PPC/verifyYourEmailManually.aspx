﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="verifyYourEmailManually.aspx.cs" Inherits="PPC_verifyYourEmailManually" MasterPageFile="~/Controls/MasterPageNP2.master"  ClientIDMode="Static"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="../PlaceHolder.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>
<link href="samplepPpc2.css?version=3" rel="stylesheet" type="text/css" />
<!--[if IE 7]>      
      <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome-ie7.min.css" />
    <![endif]-->  
    
    
<script>
    function validation() {

        if (!checkEmail($('#txt_email').val())) {
            $("#div_EmailComment").show();         
            return false;
        }
        
        activeButtonAnim('.progress-demo #btn_SendService', 200);

        return true;

    }
</script>  
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="registration emailVerificationManually">   

    <div class="registrationLeft">

       <div  id="titleFirst" runat="server" class="titleFirst">
            <span>Verify your email</span>
        </div>      

        <div id="subTitle" runat="server" class="subTitle">
            Please review the email address below, we will send you a verification message to it. You will use this email address for
            <br />
            your future logins.
        </div>

       <div  id="registrationInputs" runat="server" class="registrationInputs" >            

           <asp:ScriptManager ID="ScriptManager1" runat="server">
           </asp:ScriptManager> 

           <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
           <ContentTemplate>
               <div class="emailContainer">
                    <div>Email</div>  
                    <div class="inputParent">
                    <input id="txt_email" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="email" />
                    <div id="div_EmailSmile" class="inputValid" style="display:none;"> </div>
                    <div id="div_EmailError" class="inputError" style=""> </div>

                

                    <div class="inputValid" style="display:none;"  id="div1"></div>

                    <div id="div_EmailComment" style="display:none;" class="inputComment"><%#EmailError%>                    
                        <div class="chat-bubble-arrow-border"></div>
                        <div class="chat-bubble-arrow"></div>
                    </div>

                </div>               

                 <div id="containerNext" class="containerNext">
                        <section class="progress-demo">
                        <asp:LinkButton ID="btn_SendService" runat="server" data-style="expand-right" data-color="blue" class="ladda-button" OnClientClick="javascript:return validation();" OnClick="btnSend_Click" >
                            <span class="ladda-label">Verify</span><span class="ladda-spinner">
                            </span><div class="ladda-progress" style="width: 138px;"></div>
                          </asp:LinkButton>                 
                        </section>      
                 </div>  
            </div>        
           </ContentTemplate>
           </asp:UpdatePanel>

            <div class="clear"></div>     

 
           
       </div>  

      
   
   
    
    </div>
   
    
</div>
</asp:Content>