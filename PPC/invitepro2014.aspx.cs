﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_invitepro2014 : PageSetting
{
    private int InvitationsLeft;
    protected int InvitationSuccessBonus;
    protected int InvitationLifetimeBonus;
    protected string SiteId;
    protected string SupplierId;
    protected string friendemail;
    protected string supplieremail;
    protected bool RequestedMoreInvites;
    protected bool JustGotMoreInvites;
    protected string emailMissingFriend;
    protected string emailError;

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        if (userManagement.IsPublisher())
            this.MasterPageFile = "../Publisher/MasterPageFrame.master";
        else if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
        {
            this.MasterPageFile = ResolveUrl("~") + "Controls/MasterPagePPC2.master";

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadFriendsData();
            SiteId = siteSetting.GetSiteID;
            //Response.Write(SiteId + "<br>");
            SupplierId = GetGuidSetting();
            friendemail = Request["friendemail"];
            //friendemail = "sdfsdfsd@sdfdf.com";

            if (!string.IsNullOrEmpty(friendemail))
            {
                inviteFriendEmail.Value = friendemail.Trim();
                //inviteSystemMessage.InnerHtml = "Thanks for verifying your email address. Now you can send the invite to <span style='color:black;'>" + friendemail + "</span>";
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "SetServerComment"))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetServerComment", "SetServerComment('Thanks for verifying your email address. Now you can send the invite to <span style=\"color:black;\">" + friendemail + "</span>');", true);
            }

            inviteFriendEmail.Attributes["place_holder"] =  hiddenEmail.Value;
            inviteFriendEmail.Attributes["HolderClass"] = "place-holder";
                 
            //Response.Write(SupplierId);

            LoadPageText();

        }
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();


        XElement xLocation = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "Location"
                          select x).FirstOrDefault();

        XElement phone_element = xLocation.Element("Email").Element("Validated");
        emailMissingFriend = phone_element.Element("MissingFriend").Value;
        emailError=phone_element.Element("Error").Value;  

    }

    private void LoadFriendsData()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfInvitationData result = null;
        try
        {
            result = _supplier.GetInvitationsData(new Guid(GetGuidSetting()));
            InvitationsLeft = result.Value.InvitationsLeft;
            InvitationSuccessBonus = result.Value.InvitationSuccessBonus;
            InvitationLifetimeBonus=result.Value.InvitationLifetimeBonus;
            RequestedMoreInvites = result.Value.RequestedMoreInvites;
            supplieremail = result.Value.Email;            
            JustGotMoreInvites = result.Value.JustGotMoreInvites;

            //InvitationsLeft = 4;
            //Response.Write("leeeeeeeeft" + InvitationsLeft);

            if (InvitationsLeft > 0)
            {
                int rest = InvitationsLeft % 3;

                switch (rest)
                {
                    case 0: // InvitationsLeft 3
                        div_FriendsPic.Attributes.Add("style", "display:none;");
                        friend1.Attributes.Add("class", "inviteProStatusUnRegistered");
                        friend2.Attributes.Add("class", "inviteProStatusUnRegistered");
                        friend3.Attributes.Add("class", "inviteProStatusUnRegistered");
                        
                        break;

                    case 1:
                        friend1.Attributes.Add("class", "inviteProStatusRegistered");
                        friend2.Attributes.Add("class", "inviteProStatusRegistered");
                        friend3.Attributes.Add("class", "inviteProStatusUnRegistered");
                        break;

                    case 2:
                        friend1.Attributes.Add("class", "inviteProStatusRegistered");
                        friend2.Attributes.Add("class", "inviteProStatusUnRegistered");
                        friend3.Attributes.Add("class", "inviteProStatusUnRegistered");
                        break;

                    default:
                        friend1.Attributes.Add("class", "inviteProStatusUnRegistered");
                        friend2.Attributes.Add("class", "inviteProStatusUnRegistered");
                        friend3.Attributes.Add("class", "inviteProStatusUnRegistered");
                        break;
                }
            }

            else // 0 left invites
            {
                friend1.Attributes.Add("class", "inviteProStatusRegistered");
                friend2.Attributes.Add("class", "inviteProStatusRegistered");
                friend3.Attributes.Add("class", "inviteProStatusRegistered");

                if (RequestedMoreInvites) // if ask
                {
                    moreFriends.InnerHtml = "Request for more invites sent.";
                    moreFriends.Attributes.Add("style", "visibility:visible");
                    div_FriendsPic.Attributes.Add("style", "margin-left:600px");
                }

                else
                {
                    moreFriends.InnerHtml = "<a href='javascript:void(0);' onclick='javascript:askMoreInvites();'>Request more invites</a>";
                    moreFriends.Attributes.Add("style", "visibility:visible");
                }
                        
               
            }

            if (JustGotMoreInvites)
            {
                if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "SetServerComment2"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetServerComment2", "SetServerComment('You have been given 3 more NoProblem invitations. Invite your friends and earn $" + InvitationSuccessBonus + " and more in credit!');", true);
                }
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            return;

        /*
        var ans = _supplier.GetInvitationsData(Guid.Empty);

        var asn2 = _supplier.InviteYourFriend(
            new WebReferenceSupplier.CreateInvitationRequest()
            {
                
            });
        asn2.Value.Status == WebReferenceSupplier.CreateInvitationStatus.
        */


    }
    protected string EmailPlaceHolder
    {
        get { return hiddenEmail.Value; }
    }

}
