﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Controls/MasterPageNP2.master" CodeFile="PpcLoginMobileTest.aspx.cs" Inherits="PPC_PpcLoginMobile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="../PlaceHolder.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="registration loginMobile">

    <div id="registrationContainerVideoIframe" class="registrationContainerVideoIframe">       
        <div style="" class="registrationVideoIframe" id="registrationVideoIframe">
	        <div>
                <a href="javascript:showHideVideo();" class="x-close"></a>  
            </div>          
            <iframe height="" frameborder="0" width="870" allowfullscreen="" src="" name="iframeVideo" id="iframeVideo"></iframe>
        </div>
    </div>

    <div class="clear"></div>

    <div class="registrationLeft">

        <div class="titleFirst">
        <span>Login</span>
    </div>

        <div class="connectWith">
            <span>Connect with</span>
        </div>          

       <div class="clear"></div>          

       <div class="registrationInputs" >    
        
            

             <div class="emailContainer">
            <div>Email</div>  
            <div class="inputParent">
                <input id="txt_email" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="email" />
                <div id="div_EmailSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_EmailError" class="inputError" style=""> </div>

                

                <div class="inputValid" style="display:none;"  id="div1"></div>

                <div id="div_EmailComment" style="display:none;" class="inputComment"><%#EmailError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        <div class="passwordContainer">
            <div>Password</div>
            <div class="inputParent">
                <input id="txt_password" type="password" class="textActive" runat="server" HolderClass="place-holder" text-mode="pelephone" />

                <div id="div_PasswordSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_PasswordError" class="inputError" style=""> </div>

                <div class="inputValid" style="display:none;"  id="div2"></div>

                <div id="div_PasswordComment" style="display:none;" class="inputComment"><%#PasswordError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        </div>
   
      

        <div> 
  </div>
   
       <div class="clear"></div>

       <div class="containerLogin">
            <div class="loginCell alreadyRegistered">
                
                <span id="spn_checkbox"><input id="chk_remember" type="checkbox" /></span>

                <span id="spn_remember">Remember me</span>

                <div class="forgotPassword">
                    <a href="forgotPassword.aspx">Forgot password?</a>
                </div>        
                

            </div>
        
            <div class="loginCell registerLogin">
                <section class="progress-demo">
                 <a id="linkRegistration" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" button-mode="phone" class="btn_next2" onclick="javascript:preCheckRegistrationStep('regularLogin');">
                  <span class="ladda-label">Login</span><span class="ladda-spinner">
                  </span><div class="ladda-progress" style="width: 138px;"></div>
                 </a>
                </section>
            </div>
        </div>
    
    </div>
   
    <div class="registrationRight">
        <div class="">
            NoProblem got better! What’s new?
        </div>
        
    </div>   
   
       
</div>
</asp:Content>