﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_VerifyYourEmail : RegisterPage2
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        string id=Request["id"];
        Guid guidId;
        Guid.TryParse(id, out guidId);

        WebServiceSales webServiceSales = new WebServiceSales();
        int step=webServiceSales.verifyRegistrationEmail(guidId);


        if (step < 1)
            verifyContent.InnerText = "Can't verify you";
        else
        {
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.stepRedirect(step);
        }
      


    }    

}