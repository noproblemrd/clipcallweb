﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPPre.master" AutoEventWireup="true" CodeFile="LegalAdvTerms.aspx.cs" Inherits="PPC_Privacy_LegalAdvTerms" %>

<%@ MasterType VirtualPath="~/Controls/MasterPageNPPre.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="Legal-Privacy">
    <div class="navigation">
        <ul>
            <li>
                <a href="javascript:void(0);" class="active">TERMS OF SERVICE</a>
            </li>
            <li>
                <a href="<%= ResolveUrl("~/PPC/Privacy/LegalPrivacy.aspx")  %>">PRIVACY POLICY</a>
            </li>
            
        </ul>
    </div>
    <div class="clear"></div>
    <div class="menu-terms">
        <h5>Advertiser</h5>
        <a href="javascript:void(0);">Advertiser terms of service</a>
        <a href="<%= ResolveUrl("~/PPC/Privacy/LegalRefund.aspx")  %>">Refund policy</a>
        <h5>Publisher</h5>
        <a href="<%= ResolveUrl("~/PPC/Privacy/LegalTerms.aspx")  %>">Publisher terms of service</a>
    </div>
    <div class="term-text">
    </div>
</div>
</asp:Content>

