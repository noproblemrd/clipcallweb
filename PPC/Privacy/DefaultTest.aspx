﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DefaultTest.aspx.cs" Inherits="PPC_DefaultTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
  <!--   <script type="text/javascript" src="script/HistoryManager.js"></script>-->
     <script type="text/javascript">

         jQuery(document).ready(function ($) {
             $('a').click(function () {

                 $('#div1').hide();
                 $('#div2').show();
                 var url = $(this).attr('title');
                 $.history.load(url);
                 return false;
             });
             $.history.init(function (url) {
                 setBack(url == "" ? "1" : url);
             });
             

         });
         function setBack(num) {
        //     if (num == '1')
             GoBack(num);
         }
         function GoBack(num) {
             if (num == "1") {
                 $('#div1').hide();
                 $('#div2').show();
             }
             else if (num == "2") {
                 $('#div1').show();
                 $('#div2').hide();
             }
         }
         
     
     
     </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <a href="javascript:void(0);" title="2">go</a>
        <a href="javascript:void(0);" title="1">back</a>
        <div id="div1" style="width:400px; height:400px; background-color:Red;"></div>
        <div id="div2" style="width:600px; height:700px; background-color:Green; display:none;" ></div>
    </div>
    </form>
</body>
</html>
