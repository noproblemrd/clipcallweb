﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_Privacy_LegalAdvTerms : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Master.SetTitle1 = "";
            Master.SetTitle2 = "Legal";
            Master.SetCssClass = "privacy";
        }
    }
}