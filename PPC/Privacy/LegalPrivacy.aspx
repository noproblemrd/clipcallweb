﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPPre.master" AutoEventWireup="true" CodeFile="LegalPrivacy.aspx.cs" Inherits="PPC_Privacy_LegalPrivacy" %>

<%@ MasterType VirtualPath="~/Controls/MasterPageNPPre.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="Legal-Privacy">
    <div class="navigation">
        <ul>
            <li>
                <a href="<%= ResolveUrl("~/PPC/Privacy/LegalAdvTerms.aspx")  %>">TERMS OF SERVICE</a>
            </li>
            <li>
                <a href="javascript:void(0);" class="active">PRIVACY POLICY</a>
            </li>
            
        </ul>
    </div>
    <div class="term-text totali">
        <h3>Privacy Policy</h3>
        <p>OneCall Contact Centers Ltd. (the "Company” or "No Problem" or "We”) is committed to protect the privacy of users who access its Website (the "Site”). This Privacy Policy Statement (the "Privacy Policy”) explains the types of information We gather and what We do with it. By using the Site, you agree to the terms and policies described in this Privacy Policy.</p>

        <h5>1. Collecting Personally Identifiable Information "Personally Identifiable Information"</h5>
        <p>means any information obtained through your use of the Site which might reasonably be used to specifically identify you and may include, among other, your name, title, company, address, email. To the extent Personally Identifiable Information has been collected from you, you acknowledge such collection is done on a voluntary basis to enable the Company to provide you efficient access to features, functions, promotions, products and services.</p>
        <p>In addition, We collect information from you for the purpose of opening an account, billing payment and payment instruments from you for products and services you order or purchase from the Company.</p>

        <h5>2. Non-Personal Information</h5>
        <p>We automatically collect non-personal information such as IP addresses during your access and use of the Site to help diagnose problems with our server, and to identify ways of making our Site better. This information is not linked to anything personal.</p>

        <h5>3. Use of Digital Identifiers</h5>
        <p>A digital identifier is information that a Site puts on your hard disk so that it can remember something about you at a later time (More technically, it is information for future use that is stored by the server on the client side of a client/server communication, such as “cookies”.) We automatically use ‘digital identifiers’ for tracking transactions, identification, security and the user characteristics as set forth herein. No Personally Identifiable Information is collected by use of digital identifier and We use the information collected through such digital identifier to analyze trends, administer the Site, track movements on the Site and gather aggregate demographic information about visitors so we can continually improve the Site.</p>

        <h5>4. Security</h5>
        <p>We employ industry standard security measures to ensure the security of all data. Any data that is stored on our servers is treated as proprietary and confidential and is not available to the public. We maintain an internal security policy with respect to the confidentiality of your data, allowing access only to those employees or third parties who have a need to know such information.</p>
        <p>While Company has taken measures to protect your information, please know that (i) no computer system is immune from intrusion; and (ii) information transmitted to the Site may need to pass through many systems in order to process such information. Therefore, We cannot guarantee the absolute security or confidentiality of information transmitted to the Site and you hereby acknowledge and assume the risks relating to any such communication.</p>

        <h5>5. Information Sharing</h5>
        <p>We may use the collected Personally Identifiable Information and non-personal information for any legally permissible purpose in our sole discretion, including but not limited to those detailed below:</p>
        <ul>
            <li>We may share information We collect with affiliated companies, for use in their ad serving and marketing programs. This Privacy Policy does not apply to affiliated companies and their collection, use and sharing of information.</li>
            <li>We may share your information with various third party vendors that are not part of the Company.</li>
            <li>When you participate in an in our Programs, we will transfer the information you submitted on the form to the applicable advertisers with whom we cooperate.</li>
            <li>We may use third party ad networks or ad serving companies to serve advertisements on our websites. We pass information about you to these companies so that they can deliver targeted advertisements that they believe will be of interest to you.</li>
            <li>We may use third party service providers to enhance our database with additional elements of our services.</li>
        </ul>

        <h5>6. Third Party Links</h5>
        <p>The Site may offer links to other websites. Other websites have their own terms of use and privacy and security policies. If you choose to visit one of these sites, you should review the policies that govern that particular site.</p>

        <h5>7. Permitted Disclosures</h5>
        <p>Other than as described in this Privacy Policy or as otherwise specifically communicated to you by Company, Company does not provide information supplied by users to any third parties, except where law, court order, or governmental authority requires or based upon the good faith belief that disclosure is necessary including, without limitation, to protect the rights of Company when we have reason to believe that disclosing the information is necessary to identify, contact or bring legal action against someone who may be causing interference with our rights or causing harm to third parties.</p>

        <h5>8. Revision</h5>
        <p>Company may alter, update or otherwise change this Privacy Policy at any time to reflect material changes in the way Company uses your information. Any such changes will be posted on this page. You should periodically check this Privacy Policy to review the current terms and guidelines applicable to your use. Your continued use of the Site following the posting of such changes will indicate your full acceptance of those changes.</p>

        <h5>9. Updating Your Personal Information</h5>
        <p>At any time, you can access and correct your personal information and privacy preferences on “my account” in your dash board in the Company’s system or by sending an e-mail to No Problem <a href="mailto:advertiserservices@noprobemppc.com">advertiserservices@noprobemppc.com</a> or <a href="mailto:publisherservices@oproblemppc.com">publisherservices@oproblemppc.com</a>. To the extent permissible under the applicable law, you hereby acknowledge and consent to the Company occasionally sending you various e-mail messages and updates. If you would prefer not to receive such messages you may opt out by following the instructions within the e-mail message or by contacting us.</p>
        
        <h5>10. Contact Us</h5>
        <p>If you have questions about this Privacy Policy, please send an e-mail to <a href="mailto:info@noproblemppc.com">info@noproblemppc.com</a>. If you believe that we have not complied with this Privacy Policy with respect to your personal information or you have other related inquiries or concerns, you may write to Company at the following address: 10 Zarchin St. Ra'anana P.O. Box 2421 Israel</p>
        <p><br /></p>
        <p>YOU ACKNOWLEDGE AND AGREE THAT ANY DISPUTE OVER PRIVACY IS SUBJECT TO THE TERMS AND CONDITIONS OF THIS PRIVACY POLICY, AS WELL AS THE GENERAL TERMS AND CONDITIONS BETWEEN YOU AND THE COMPANY AND THIS SITE (INCLUDING LIMITATIONS ON DAMAGES).</p>
    </div>
</div>
</asp:Content>

