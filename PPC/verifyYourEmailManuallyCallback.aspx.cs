﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_verifyYourEmailManuallyCallback : RegisterPage2
{
     
    protected void Page_Load(object sender, EventArgs e)
    {
        string id = Request["id"];
        Guid supplierId;
        bool ifGuid=Guid.TryParse(id, out supplierId);
        if (!ifGuid)
        {
            verifyContent.InnerText = "Can't verify you";
            dbug_log.ExceptionLog(new Exception("failed verifyYourEmailManuallyCallback for id=: " + id));
        }

        else
        {       
            
            WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
            WebReferenceSupplier.ResultOfEmailVerifiedResponse result = new WebReferenceSupplier.ResultOfEmailVerifiedResponse();                  

            WebServiceSales webServiceSales = new WebServiceSales();

            try
            {
                result = _supplier.EmailVerified_Legacy2014(supplierId);

                if(result.Type== WebReferenceSupplier.eResultType.Failure)
                    dbug_log.ExceptionLog(new Exception("failed method EmailVerified_Legacy2014 for supllier " + supplierId.ToString() + " message: " + result.Messages[0]));
                else
                {
                    string redirectPath=webServiceSales.legacyRedirectMap(supplierId,result.Value.StepLegacy, result.Value.Step,
                    result.Value.LegacyStatus, result.Value.Name, result.Value.Balance);
                    Response.Redirect(redirectPath);
                }

            }

            catch (Exception ex)
            {
                dbug_log.ExceptionLog(ex);
            
            }           

        }      


    }

    
}