﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" MasterPageFile="~/Controls/MasterPageNP2.master"  Inherits="PPC_login" ClientIDMode="Static" %>

<%@ Register Src="~/Controls/socialNetworks.ascx" TagName="socialNetworks" TagPrefix="Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="../PlaceHolder.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>
<!--
<script src="../scripts/sjcl-master/sjcl.js" type="text/javascript"></script>
-->

<script src="sjcl.js" type="text/javascript"></script>

<!--[if IE 7]>      
      <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome-ie7.min.css" />
<![endif]-->  



</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<Registration:socialNetworks  ID="socialNetworks1" runat="server"/>

<div class="registration login">

    <div id="registrationContainerVideoIframe" class="registrationContainerVideoIframe">       
        <div style="" class="registrationVideoIframe" id="registrationVideoIframe">
	        <div>
                <a href="javascript:showHideVideo();" class="x-close"></a>  
            </div>          
            <iframe height="" frameborder="0" width="870" allowfullscreen="" src="" name="iframeVideo" id="iframeVideo"></iframe>
        </div>
    </div>

    <div class="clear"></div>

    <div class="registrationLeft">

        <div class="titleFirst">
        <span>Login</span>
    </div>

        <div class="connectWith">
            <span>Connect with</span>
        </div>
        
    
       <div class="socialNetworks">

        <span id="googleCustomSignIn" class="icon-stack icon-stackGooglePlus">
            <a href="javascript:void(0);">
                <i class="icon-circle icon-stack-base" style="color: rgb(210, 71, 55);"></i>
                <i class="icon-google-plus"></i>
            </a>
        </span>

        <span class="icon-stack icon-stackFacebook">
            <a onclick="faceBookLogIn2();" href="javascript:;">
                <i class="icon-circle icon-stack-base" style="color: rgb(59, 89, 153);"></i>
                <i class="icon-facebook"></i>
            </a>
        </span>      

    </div>

       <div class="clear"></div>

       <div class="or">Or</div>      

       <div class="registrationInputs" >    
        
            

             <div class="emailContainer">
            <div>Email</div>  
            <div class="inputParent">
                <input id="txt_email" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="email" />
                <div id="div_EmailSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_EmailError" class="inputError" style=""> </div>

                

                <div class="inputValid" style="display:none;"  id="div1"></div>

                <div id="div_EmailComment" style="display:none;" class="inputComment"><%#EmailError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

                <div id="div_EmailComment2" style="display:none;" class="inputComment inputComment2"><%#EmailError2%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>
            </div>
        </div>

        <div class="passwordContainer">
            <div>Password</div>
            <div class="inputParent">
                <input id="txt_password" type="password" class="textActive" runat="server" HolderClass="place-holder" text-mode="pelephone" />

                <div id="div_PasswordSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_PasswordError" class="inputError" style=""> </div>

                <div class="inputValid" style="display:none;"  id="div2"></div>

                <div id="div_PasswordComment" style="display:none;" class="inputComment"><%#PasswordError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        </div>
   
      

        <div> 
  </div>
   
       <div class="clear"></div>

       <div class="containerLogin">
            <div class="loginCell alreadyRegistered">
                
                <span id="spn_checkbox"><input id="chk_remember" type="checkbox" /></span>

                <span id="spn_remember">Remember me</span>

                <div class="forgotPassword">
                    <a href="forgotPassword.aspx">Forgot password?</a>
                </div>        
                

            </div>
        
            <div class="loginCell registerLogin">
                <section class="progress-demo">
                 <a id="linkRegistration" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" button-mode="phone" class="btn_next2" onclick="javascript:preCheckRegistrationStep('regularLogin');">
                  <span class="ladda-label">Login</span><span class="ladda-spinner">
                  </span><div class="ladda-progress" style="width: 138px;"></div>
                 </a>
                </section>
            </div>
        </div>
    
    </div>
   
    <div class="registrationRight">
        No account? <a href="register.aspx">Join NoProblem</a>
        
    </div>
    
    <script>
         initLogin();
    </script>
       
</div>
</asp:Content>
