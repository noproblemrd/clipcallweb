﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Web.Services.Protocols;
using System.Reflection;
using System.Linq;
using System.Xml.Linq;

public partial class PPC_defaultPriceSettings : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            LoadPageText();    
            
           
        }


        Page.DataBind();
    }   


    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        string FieldMissing = Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;  


        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                 where x.Attribute("name").Value == "MasterPage"
                 select x).FirstOrDefault();


        UpdateSuccess = xelem.Element("FormMessages").Element("UpdateSuccess").Value;
        UpdateFaild = xelem.Element("FormMessages").Element("UpdateFaild").Value;
        NoNewData = xelem.Element("FormMessages").Element("NoNewData").Value;

    }    


    protected string GetServerError
    {
        get { return (ViewState["GetServerError"] == null) ? " " : ViewState["GetServerError"].ToString(); }
        set { ViewState["GetServerError"] = value; }
    }   


    protected string UpdateSuccess
    {
        get { return ViewState["UpdateSuccess"] == null ? "" : ViewState["UpdateSuccess"].ToString(); }
        set { ViewState["UpdateSuccess"] = value; }
    }

    protected string UpdateFaild
    {
        get { return ViewState["UpdateFaild"] == null ? "" : ViewState["UpdateFaild"].ToString(); }
        set { ViewState["UpdateFaild"] = value; }
    }

    protected string NoNewData
    {
        get { return ViewState["NoNewData"] == null ? "" : ViewState["NoNewData"].ToString(); }
        set { ViewState["NoNewData"] = value; }
    }



    
}