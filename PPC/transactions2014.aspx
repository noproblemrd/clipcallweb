﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="transactions2014.aspx.cs" Inherits="PPC_transactions2014" MasterPageFile="~/Controls/MasterPagePPC2.master" %>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/menuVerticalBilling.ascx" TagName="menuVertical" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<script type="text/javascript">

    function serverError() {
        SetServerComment2("<%#GetServerError%>");

    }

    function OnBeginRequest(sender, args) {
        // to destroy this function in UpdateProgress\UpdateProgress.js
        //document.getElementById('divLoader').style.display = 'block';
    }

    function endRequest(sender, args) {
        // to destroy this function in UpdateProgress\UpdateProgress.js
        //document.getElementById('divLoader').style.display = 'none';
    }

    var intervalScroll;
    var enableNextList = false;

    function resetScroll() {
        //alert("resetScroll");
        window.onscroll = scroll;
        clearInterval(intervalScroll);
    }

    function getPaging() {
        //alert("getPaging");
        //__doPostBack('ctl00$ContentPlaceHolderBody$lnkNextPage', '');
        __doPostBack('ctl00$ContentPlaceHolderBody$_GridView_Transaction', 'Page$Next')
        window.onscroll = null;
        intervalScroll = setTimeout('resetScroll()', 250);
    }

    function enableNext(mode) {
        //alert("mode:" + mode);          
        enableNextList = mode;
    }

    //alert(window.innerHeight + ' ' + window.pageYOffset);

    function scroll() {

        //if (ifRunReport == true && enableNextList==true) {
        
        if (enableNextList == true) {
            if (navigator.appName == "Microsoft Internet Explorer")
                scrollPosition = document.documentElement.scrollTop;
            else
                scrollPosition = window.pageYOffset;

            
            //Get the window's height and width: (NOT including toolbars/scrollbars)


            var listHeight = document.getElementById("<%=_GridView_Transaction.ClientID%>").offsetHeight;
            //document.getElementById("myText").innerHTML = "ifNextEnabled:" + enableNextList + " scrollPosition:" + scrollPosition + " contentHeight:" + document.getElementById("list").offsetHeight;

            if (scrollPosition > 0.4 * listHeight){
            //if (scrollPosition > 0.5 * window.innerHeight) {
                //alert(scrollPosition);
                getPaging();
            }

        }

        else {
            //alert(enableNextList);
        }

        ////}

    }

    window.onscroll = scroll;
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<uc1:menuVertical  ID="menuVertical" runat="server" whoActive="transactions"/>

<div class="transactionsPage dashboardIframe">
    <uc1:PpcComment runat="server" ID="_comment"  />
    <div class="titleFirst">
        <span>Transactions</span>

    </div>

    <div class="transactionsList">

        <asp:UpdatePanel ID="updatePanelTrans" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    
            <asp:GridView ID="_GridView_Transaction" runat="server"  GridLines="None"
                  AutoGenerateColumns="false" CssClass="tg" 
                  onrowdatabound="_GridView_Transaction_RowDataBound" 
            AllowPaging="true" onpageindexchanging="trans_PageIndexChanging"  >
               <HeaderStyle CssClass="_hs" />
              <RowStyle CssClass="_rs" />
              <Columns>
              <asp:TemplateField HeaderStyle-CssClass="transDate" ControlStyle-CssClass="transDateData">
                <HeaderTemplate >
                    <asp:Label ID="Label10" runat="server" Text="Date"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text="<%# Bind('Date') %>"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField HeaderStyle-CssClass="transType">
                <HeaderTemplate>
                    <asp:Label ID="Label13" runat="server" Text="Type"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label14" runat="server" Text="<%# Bind('Type') %>"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField HeaderStyle-CssClass="transAmount">
                <HeaderTemplate>
                    <asp:Label ID="Label15" runat="server" Text="Amount"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label16" runat="server" Text="<%# Bind('Amount') %>"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>              

              </Columns>
              <PagerSettings  Mode="NextPreviousFirstLast" FirstPageText="First" PreviousPageText="Previous" NextPageText="Next" LastPageText="Last"      />
              <PagerStyle  CssClass="transPagerStyle"/>
          
             </asp:GridView>  
             <div class="TransactionTableShadowWide"></div>
        </ContentTemplate>

        </asp:UpdatePanel>
    
    </div>

</div>

</asp:Content>