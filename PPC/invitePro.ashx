﻿<%@ WebHandler Language="C#" Class="invitePro" %>

using System;
using System.Web;


public class invitePro : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {
        /*context.Response.ContentType = "text/plain";*/
        context.Response.ContentType = "application/json";
        

       
        string invitedEmail=context.Request["invitedEmail"];
        string invitedMessage=context.Request["InvitedMessage"];
        string siteId = context.Request["siteId"];
        string supplierId = context.Request["supplierId"];

        string result;
        string status;
        string invitationsLeft="";
        
        try
        {

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(null, siteId);
        WebReferenceSupplier.CreateInvitationRequest request = new WebReferenceSupplier.CreateInvitationRequest();

        request.InvitedEmail = invitedEmail;
        request.PersonalMessage = invitedMessage;
        request.SupplierId = new Guid(supplierId);

        WebReferenceSupplier.ResultOfCreateInvitationResponse responseInvite = new WebReferenceSupplier.ResultOfCreateInvitationResponse();

       
            responseInvite = supplier.InviteYourFriend(request);

            if (responseInvite.Type == WebReferenceSupplier.eResultType.Success)
            {
                /*
                    public enum CreateInvitationStatus
                    {
                        OK,
                        NoInvitationsLeft,
                        AlreadyInvited,
                        AleradyRegistered,
                        EmailFailed
                    }
                */

                result = "pass";
                status = responseInvite.Value.Status.ToString();
                invitationsLeft = responseInvite.Value.InvitationsLeft.ToString();
                
            }

            else
            {
                dbug_log.ExceptionLog(new Exception("crm: failed InviteYourFriend "), siteId);
                result = "notpass";
                status = "crm failed";
            }
            
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteId);
            result = "notpass";
            status = "server failed";
        }

        var resultParams = new { result = result, status = status, invitationsLeft = invitationsLeft };
        
        System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();        
        
        context.Response.Write(jss.Serialize(resultParams));

       
      
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}