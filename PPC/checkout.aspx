﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="checkout.aspx.cs"  MasterPageFile="~/Controls/MasterPageNP2.master" Inherits="PPC_checkout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>

 <script>
     function showBubble() {
         document.getElementById("ContainerBubble").style.display = 'block';
     }

     function closeBubble() {
         document.getElementById("ContainerBubble").style.display = 'none';
     }
 </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="checkout">

   <div class="checkoutLeft">

    <div class="titleFirst">
        <span>Checkout - Preview order</span>
    </div>   
    
    <div class="checkoutLeftContent">
        <div class="titleTable">
            <div class="titleTableLeft">
                Item
            </div>

            <div class="titleTableRight">
                Price
            </div>

            <div class="clear"></div>

            <%if (plan == "LEAD_BOOSTER_AND_FEATURED_LISTING")%>
            <%{ %>
            <div class="titleTableLeft rowTableLeft">
                Lead Booster plan
            </div>

            <div class="titleTableRight rowTableRight rowTablePrice">
               &nbsp;
            </div>

            <div class="clear"></div>

            <div class="titleTableLeft rowTableLeft rowSub">
                 &nbsp;&nbsp;&nbsp; Leads from new customers
            </div>

            <div class="titleTableRight rowTableRight rowTablePayPerLead rowSub">
                 <a href="javascript:void(0);" onclick="showBubble();">Pay per lead</a>

                <div id="ContainerBubble" class="ContainerBubblePayPerLead">
                    <div class="bubblePerLead">
                        When you get a lead from us, you decide how
                        <br />
                        much you want to pay for the call. We then
                        <br />
                        charge the amount you set.
                    </div>
                    
                    <div class="bubbleTriangular">
                    </div>

                    <div class="close" onclick="closeBubble();">x</div>   
                                    
                </div>
            </div>

            <div class="clear"></div>

            <div class="titleTableLeft rowSub rowTableLeft" style="border-bottom: 1px solid #000;">
                &nbsp;&nbsp;&nbsp; Monthly fee
            </div>

            <div class="titleTableRight rowTableRight">
               $0.00
            </div>

            <div class="titleTableLeft rowTableLeft">
               Featured listing
            </div>

            <div class="titleTableRight rowTableRight">
                &nbsp;
            </div>

            <div class="clear"></div>

            <div class="titleTableLeft rowTableLeft rowSub rowSubLeft">
               &nbsp;&nbsp;&nbsp; Monthly fee
            </div>

            <div class="titleTableRight rowTableRight rowSub">
                $<%#fee%>
            </div>

            <div class="clear"></div>

            <div class="titleTableLeft rowTableLeft rowDiscount rowSubEnd rowSubLeft">
                &nbsp;&nbsp;&nbsp; Discount*
            </div>

            <div class="titleTableRight rowTableRight rowDiscount rowSubEnd">
                -$<%#fee%>
            </div>

            <div class="clear"></div>           

            <div class="titleTableLeft rowTableLeft rowTableSumLeft rowTableCreditFeaturedListing">
               &nbsp;
            </div>

            <div class="titleTableLeft rowTableLeft rowTableSumMiddle rowTableCreditFeaturedListing">
                Total
            </div>

            <div class="titleTableRight rowTableRight rowTableSumRight rowTableCreditFeaturedListing">
                $0.00
            </div>

            <div class="clear"></div>

            <div class="checkoutComment">
                * Accepting at least 50% of the leads, will grant you with a full monthly fee credit
            </div>
            <%} %>

            <%else if (plan == "FEATURED_LISTING")%>
            <%{ %>
            <div class="titleTableLeft rowTableLeft">
                Featured Listing plan
            </div>

            <div class="titleTableRight rowTableRight rowTablePrice">
               &nbsp;
            </div>

            <div class="clear"></div>


            <div class="titleTableLeft rowTableLeft">
                &nbsp;&nbsp;&nbsp;Monthly fee
            </div>

            <div class="titleTableRight rowTableRight rowTablePrice">
                $<%#fee%>
            </div>

            <div class="clear"></div>

            <div class="titleTableLeft rowTableLeft rowTableSumLeft rowTableCreditFeaturedListing">
               &nbsp;
            </div>

            <div class="titleTableLeft rowTableLeft rowTableSumMiddle rowTableCreditFeaturedListing">
                Total
            </div>

            <div class="titleTableRight rowTableRight rowTableSumRight rowTableCreditFeaturedListing">
                $<%#fee%>
            </div>

            <div class="clear"></div>

            <%} %>
        </div>
    </div>

    <div class="clear"></div>

    <div id="containerNext" class="containerNext">
                <section class="progress-demo">               

                 <asp:LinkButton  ID="lbCheckout"  runat="server" data-style="expand-right" 
                    data-color="blue" class="ladda-button" onclick="lbCheckout_Click" >
                    Checkout
                 </asp:LinkButton>

                 <!--
                 <a id="linkSetDetails" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" onclick="javascript:return CheckLocationDetails();">
                  <span class="ladda-label">Checkout</span><span class="ladda-spinner">
                  </span><div class="ladda-progress" style="width: 138px;"></div>
                 </a>
                 -->
                  <span class="back"><a href="location2.aspx">&lt; Back</a></span>

                </section>      
     </div> 

   </div> 

   

   <div class="checkoutRight">
    <%if (plan == "LEAD_BOOSTER_AND_FEATURED_LISTING")%>
    <%{ %>
    <div class="checkoutRightTitle">
        <span>No fixed fees</span>
    </div>   
    
    <div class="checkoutExplain">
        <div class="checkoutExplainRow">
            <div class="checkoutExplainRowIcon ">
                <span class="hide-xs">
                    <i class="fa fa-clock-o fa-3"></i>
                </span>
            </div>

            <div class="checkoutExplainRowContent checkoutExplainRowOneLine">
                You are charged after the call
                <br />
                is dispatched to you
            </div>
        </div>

        <div class="clear"></div>

         <div class="checkoutExplainRow">
            <div class="checkoutExplainRowIcon">
                <span class="hide-xs">
                 <i class="fa fa-usd fa-2"></i>
                 </span>
            </div>

            <div class="checkoutExplainRowContent checkoutExplainRowTwoLine">
                You decide how much you pay
            </div>
        </div>

        <div class="clear"></div>

         <div class="checkoutExplainRow">
            <div class="checkoutExplainRowIcon">
            <span class="hide-xs">
                 <i class="fa fa-smile-o fa-3"></i>
            </span>
            </div>

            <div class="checkoutExplainRowContent checkoutExplainRowTwoLine">
                No other charges
            </div>
        </div>
        
        <div class="clear"></div>

        <div class="checkoutSlogan">
            That’s pay-per-lead.
        </div>

        <div class="checkoutVideoContainer">
            <div class="checkoutVideoTitle">
                How it works?
            </div>

            <div class="checkoutVideo">
                <iframe width="266" height="150" src="//www.youtube.com/embed/iUbjJtXuVFU?autoplay=0&autohide=1&hd=1&vq=hd720&rel=0&cc_load_policy=1" frameborder="0" allowfullscreen></iframe>
            </div>

        </div>

    </div>
    <%} %>
   </div> 

   
</div>

</asp:Content>
