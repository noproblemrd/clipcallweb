﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Web.UI.HtmlControls;

public partial class PPC_Location : RegisterPage, IsInProcess
{
   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadPageText();
            txt_email.Value = email;
            if (LoadName())// || (Guid)Session["InvitationId"] != Guid.Empty)
                LoadLoation();
                
            
        }
        ScriptManager sm = ScriptManager.GetCurrent(this);
        if (sm == null || !sm.IsInAsyncPostBack)
        {
            string _script = Utilities.CheckStatusEmailJavaScript();
            ClientScript.RegisterClientScriptBlock(this.GetType(), "SetEmailValidation", _script, true);
        }
        LoadMaster();
        txt_email.Attributes.Add("place_holder", EmailPlaceHplder);
        txt_name.Attributes.Add("place_holder", NamePlaceHolder);
        Header.DataBind();

    }

    private void LoadMaster()
    {
        Master.SetBenefits("Track calls, manage your budget, control your location parameters, set your working hours and more on our Advertiser Dashboard.");
        Master.SetStatusBar("statusBar2");
    }
   
    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;
    //    _map.FieldMissing = FieldMissing;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "Location"
                          select x).FirstOrDefault();

        NamePlaceHolder = xelem.Element("BusinessName").Element("Validated").Element("Instruction").Element("General").Value;
        FieldMissing = xelem.Element("BusinessName").Element("Validated").Element("Missing").Value;
        XElement t_element = xelem.Element("BusinessAddress").Element("Validated");
        _map.FieldMissing = t_element.Element("Missing").Value;
        _map.LocationPlaceHplder = t_element.Element("Instruction").Element("General").Value;
        _map.SelectFromList = t_element.Element("Error").Value;
   //     _map.ChooseLargerArea = t_element.Element("Instruction").Element("LargerArea").Value;
        _map.ServerError = _error;
        TermsService = t_element.Element("Instruction").Element("Terms").Value;
        
        
        t_element = xelem.Element("Email").Element("Validated");
        EmailPlaceHplder = t_element.Element("Instruction").Element("General").Value;
        EmailDuplicate = t_element.Element("Instruction").Element("EmailDuplicate").Value;
        InvalidEmail = t_element.Element("Error").Value;
        EmailMissing = t_element.Element("Missing").Value;
        /*
        
    
    <Email>
      <Validated>
        <Instruction>
          <General>e.g.you@superman.com</General>
          <EmailDuplicate>Email allready exist</EmailDuplicate>
        </Instruction>
        <Error>Invalid email</Error>
      </Validated>
    </Email>
         * */
    }
    private void LoadLoation()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        string result = null;
        try
        {
            result = _supplier.GetWorkArea(siteSetting.GetSiteID, SupplierId.ToString());
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            return;
        }
        XDocument doc = XDocument.Parse(result);
        if (doc.Element("WorkArea") != null)
            return;
        XElement elem = doc.Element("Areas");
        /*
        string email = elem.Attribute("Email").Value;
        if (!string.IsNullOrEmpty(email))
            txt_email.Value = email;
         * */
        string radius = elem.Attribute("Radius").Value.Replace(',', '.');
        double _radius;
        if (!double.TryParse(radius, out _radius))// / 1.6;
            _radius = 50;
        else
            _radius = _radius / 1.6;
        if (_radius == 0)
            return;
        _radius = Math.Round(_radius, 2);
        radius = _radius.ToString();
        string lat = elem.Attribute("Latitude").Value.Replace(',','.');
        string lng = elem.Attribute("Longitude").Value.Replace(',', '.');
        bool IsMoreSelected = (elem.Attribute("Longitude").Value == "true");
        string FullAddress = elem.Attribute("FullAddress").Value;

        _map.SetFullAddress(FullAddress);
        _map.SetMap(radius, lat, lng, IsMoreSelected, elem);        
       
       
    }
    /*
    private void SetEmailPlaceHolder()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "SetEmailPlaceHolder", "SetEmailPlaceHolder();", true);
    }
     * */
    private bool LoadName()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfAdvertiserGeneralInfo result = null;
        try
        {
            result = _supplier.GetSupplierGeneralInfo(SupplierId);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);           
            return false;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)                    
            return false;
        if(!string.IsNullOrEmpty(result.Value.Email))
            txt_email.Value = result.Value.Email;
        if (!string.IsNullOrEmpty(result.Value.Name) && result.Value.Name != "***")
        {
            txt_name.Value = result.Value.Name;
            txt_name.Attributes["class"] = "textActive textValid";
        }
            /*
        else
            ClientScript.RegisterStartupScript(this.GetType(), "SetNamePlaceHolder", "SetNamePlaceHolder();", true);
             * */
        
        return !string.IsNullOrEmpty(result.Value.Name);
    }
    /*
    protected string GetNeighborhood
    {
        get { return ResolveUrl("~")+"PPC/MapService.asmx/GetNeighborhood"; }
    }
     *  * */
   
    protected string GetEmailExpresion
    {
        get { return Utilities.RegularExpressionForJavascript(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,}([-.]\w+)*$"); }
    }
    
   
    protected string FinalStepIsApproved
    {
        get { return ResolveUrl("~") + "PPC/RequestApproved.aspx"; }
    }
    protected string WaitingForApproved
    {
        get 
        {
            return ResolveUrl("~/PPC/WaitingForApproved.aspx");
        }
    }
    protected string FinalStep
    {
        get {
            if (this.SupplierType == eSupplierRegisterType.AAR)
                return FinalStepIsApproved;
            return ResolveUrl("~/PPC/RegisterAddCredit.aspx"); 
        }
    }
    protected string SetDetails
    {
        get { return ResolveUrl("~") + "PPC/MapService.asmx/SetDetails"; }
    }
    protected string has_referral
    {
        get { return HasReferrer.ToString().ToLower(); }
    }
    protected string NamePlaceHolder
    {
 //       get { return "Your Business name"; }
        get { return (string)ViewState["NamePlaceHolder"]; }
        set { ViewState["NamePlaceHolder"] = value; }
    }
    /*
    protected string LocationPlaceHolder
    {
//        get { return "Your Business Location"; }
        get { return (string)ViewState["LocationPlaceHolder"]; }
        set { ViewState["LocationPlaceHolder"] = value; }
    }
    */
    protected string EmailPlaceHplder
    {
//        get { return "e.g.you@superman.com"; }
        get { return (string)ViewState["EmailPlaceHplder"]; }
        set { ViewState["EmailPlaceHplder"] = value; }
    }
    protected string EmailDuplicate
    {
//        get { return "e.g.you@superman.com"; }
        get { return (string)ViewState["EmailDuplicate"]; }
        set { ViewState["EmailDuplicate"] = value; }
    }
    protected string InvalidEmail
    {
        get { return (string)ViewState["InvalidEmail"]; }
        set { ViewState["InvalidEmail"] = value; }
    }
    
    protected string GetServerError
    {
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }
    protected string FieldMissing
    {
        get { return (string)ViewState["FieldMissing"]; }
        set { ViewState["FieldMissing"] = value; }
    }
    protected string EmailMissing
    {
        get { return (string)ViewState["EmailMissing"]; }
        set { ViewState["EmailMissing"] = value; }
    }
    protected string TermsService
    {
        get { return (string)ViewState["TermsService"]; }
        set { ViewState["TermsService"] = value; }
    }
}