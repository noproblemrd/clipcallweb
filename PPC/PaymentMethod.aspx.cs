﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_PaymentMethod : PageSetting
{  

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           LoadPageText();
           getBalanceStatus();
           LoadDetails();     
        }

        Page.DataBind();
    }


    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

    }

    protected void getBalanceStatus()
    {
        //_GetAccountStatus

        try
        {
            WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
            WebReferenceSupplier.ResultOfSupplierStatusContainer resultOfSupplierStatusContainer = null;

            resultOfSupplierStatusContainer=_supplier.GetSupplierStatus(new Guid(GetGuidSetting()));

            balance = resultOfSupplierStatusContainer.Value.Balance;
            paymentMethodFailed = resultOfSupplierStatusContainer.Value.PaymentMethodFailed;

            /*
            if (paymentMethodFailed)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
            }
            */

        }

        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
        }



    }

    protected void LoadDetails()
    {
        try
        {
            WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
            WebReferenceSupplier.ResultOfDecimal resultOfDecimal = _supplier.GetSubscriptionPlanCost(new Guid(GetGuidSetting())); // must send supplierid as parameter in case already registered
            fee = Convert.ToInt32(resultOfDecimal.Value);
            
            WebReferenceSupplier.ResultOfGetPaymentMethodDataResponse resultOfGetPaymentMethodDataResponse = _supplier.GetPaymentMethodData(new Guid(GetGuidSetting()));
            last4Digits = resultOfGetPaymentMethodDataResponse.Value.Data.Last4Digits;
            creditCardOwnerName = resultOfGetPaymentMethodDataResponse.Value.Data.CcOwnerName;
            billingAddress = resultOfGetPaymentMethodDataResponse.Value.Data.BillingAddress;
            expDate = resultOfGetPaymentMethodDataResponse.Value.Data.ExpDate;

            lbl_last4Digits.InnerText = last4Digits;

            if (expDate.Length == 3)
                lbl_expirationDate.InnerText = Utilities.addZeroToOneDigit(expDate.Substring(0, 1)) + "/20" + expDate.Substring(1, 2);
            else if (expDate.Length == 4)
                lbl_expirationDate.InnerText = Utilities.addZeroToOneDigit(expDate.Substring(0, 2)) + "/20" + expDate.Substring(2, 2);
            else
                lbl_expirationDate.InnerText = expDate;

            lbl_CarHolderName.InnerText = creditCardOwnerName;
            //lbl_CarHolderAddress.InnerText = billingAddress;

            
        }

        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
        }
    }

    protected void LoadPlan()
    {
        
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfString resultOfString = new WebReferenceSupplier.ResultOfString();

        try
        {
            resultOfString = _supplier.Checkout_GetPlan_Registration2014(new Guid(GetGuidSetting()));
            plan = resultOfString.Value;
            //plan = "LEAD_BOOSTER_AND_FEATURED_LISTING";            
            
            string __form;            

            if (plan == "FEATURED_LISTING")
            {
                if (paymentMethodFailed)
                {
                    int amount;
                    amount = Convert.ToInt32(decimal.Floor(balance));
                    if (Math.Sign(balance) == -1)
                        __form = AddCreditSales.BuyCredit2(Math.Abs(amount), siteSetting, new Guid(GetGuidSetting()), fee, plan + "_dashboard", false, "manual",0);
                    else
                        __form = AddCreditSales.BuyCredit2(0, siteSetting, new Guid(GetGuidSetting()), fee, plan + "_dashboard", false, "manual",0);
                }
                else
                    __form = AddCreditSales.BuyCredit2(0, siteSetting, new Guid(GetGuidSetting()), fee, plan + "_dashboard", false, "monthlyFeePayment",0);                    
                //Response.Write(__form);
                iframePayment.Attributes["src"] = __form;
            }

            else if (plan == "LEAD_BOOSTER_AND_FEATURED_LISTING")
            {
                if (paymentMethodFailed)
                {
                    //Math.Round(balance, MidpointRounding.AwayFromZero)
                    int amount;
                    amount = Convert.ToInt32(decimal.Floor(balance));
                    if(Math.Sign(balance)==-1)
                        __form = AddCreditSales.BuyCredit2(Math.Abs(amount), siteSetting, new Guid(GetGuidSetting()), fee, plan + "_dashboard", false, "manual",0);
                    else
                        __form = AddCreditSales.BuyCredit2(0, siteSetting, new Guid(GetGuidSetting()), fee, plan + "_dashboard", false, "manual",0);
                }
                else
                {
                    __form = AddCreditSales.BuyCredit2(0, siteSetting, new Guid(GetGuidSetting()), fee, plan + "_dashboard", false, "monthlyFeePayment",0);
                }
                //Response.Write(__form);
                iframePayment.Attributes["src"] = __form;
            }


        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);

        }
        
    }

    protected void lbEdit_Click(object sender, EventArgs e)
    {
       
        paymentMethodContent.Visible = false;
        LoadPlan();
        
    }

    protected string plan
    {
        get
        {
            return (ViewState["plan"] != null) ? ViewState["plan"].ToString() : "";
        }

        set
        {
            ViewState["plan"] = value;
        }

    }


    protected int fee
    {
        get
        {
            return (ViewState["fee"] != null) ? (int)ViewState["fee"] : 0;
        }

        set
        {
            ViewState["fee"] = value;
        }

    }

    protected string last4Digits
    {
        get
        {
            return (ViewState["last4Digits"] != null) ? ViewState["last4Digits"].ToString() : "";
        }

        set
        {
            ViewState["last4Digits"] = value;
        }

    }

    protected string creditCardOwnerName
    {
        get
        {
            return (ViewState["creditCardOwnerName"] != null) ? ViewState["creditCardOwnerName"].ToString() : "";
        }

        set
        {
            ViewState["creditCardOwnerName"] = value;
        }

    }

    protected string expDate
    {
        get
        {
            return (ViewState["expDate"] != null) ? ViewState["expDate"].ToString() : "";
        }

        set
        {
            ViewState["expDate"] = value;
        }

    }

    protected string billingAddress
    {
        get
        {
            return (ViewState["billingAddress"] != null) ? ViewState["billingAddress"].ToString() : "";
        }

        set
        {
            ViewState["billingAddress"] = value;
        }

    }

    protected string GetServerError
    {
        get { return (ViewState["GetServerError"] == null) ? " " : ViewState["GetServerError"].ToString(); }
        set { ViewState["GetServerError"] = value; }
    }

    protected decimal balance
    {
        get
        {
            return (ViewState["balance"] != null) ? (decimal)ViewState["balance"] : 0;
        }

        set
        {
            ViewState["balance"] = value;
        }

    }

    protected bool paymentMethodFailed
    {
        get
        {
            return (ViewState["paymentMethodFailed"] != null) ? (bool)ViewState["paymentMethodFailed"] : false;
        }

        set
        {
            ViewState["paymentMethodFailed"] = value;
        }

    }
    
}