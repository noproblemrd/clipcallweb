﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_location2 : RegisterPage2, IsInProcess
{
    protected string GetServerError;   

    protected void Page_Load(object sender, EventArgs e)
    {
        _map.SendStr += new Controls_MapControlMulti.SendEventHandler(setPlan);

        if (!IsPostBack)
        {
            _map.SupplierId = SupplierId;
            LoadPageText();
            //LoadLocation();
            LoadMaster();

            /*
            LoadPageText();
            LoadService();
            LoadPanelServices(false);
            */

        }
    }

    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

        masterPage.SetStatusBar("past", "current", "pre");
        masterPage.SetBlackMenu();
        
    }

    public void callServerProblem(string str)
    {
         Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
         masterPage.ServerProblem(GetServerError);
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        string FieldMissing = Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

        _map.FieldMissing = FieldMissing;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "Location"
                          select x).FirstOrDefault();



        XElement t_element = xelem.Element("BusinessAddress").Element("Validated");
        _map.LocationPlaceHplder = t_element.Element("Error2").Value;
        _map.SelectFromList = t_element.Element("Error2").Value;
        //    _map.ChooseLargerArea = t_element.Element("Instruction").Element("LargerArea").Value;
        _map.ServerError = _error;

    }

    private void LoadLocation()
    {        
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        string result = null;
        try
        {
            //result = _supplier.GetWorkArea(siteSetting.GetSiteID, SupplierId.ToString());
            result = _supplier.GetWorkArea(siteSetting.GetSiteID, SupplierId.ToString());
            
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            return;
        }
        XDocument doc = XDocument.Parse(result);
        if (doc.Element("WorkArea") != null)
            return;
        XElement elem = doc.Element("Areas");
        string radius = elem.Attribute("Radius").Value;//.Replace(',', '.');
        
        //  double _radius = Math.Round(double.Parse(radius), 2);
        decimal _radiusM = (Utilities.IsGUID(radius) || radius == "ALL") ? 35m : decimal.Parse(radius.Replace(',', '.'));
        double _radius = (Utilities.IsGUID(radius) || radius == "ALL") ? 35 : double.Parse(radius.Replace(',', '.')) / 1.6;
        //  double _radius = (double)_radiusM;
        _radius = Math.Round(_radius, 2);
        radius = _radius.ToString();
        string lat = elem.Attribute("Latitude").Value.Replace(',', '.');
        string lng = elem.Attribute("Longitude").Value.Replace(',', '.');
        if (lat == "0" && lng == "0")
        {
            IsFirstTime = true;
            return;
        }
        bool IsMoreSelected = (elem.Attribute("IsMoreSelected").Value == "true");
        string FullAddress = elem.Attribute("FullAddress").Value;       

        ////_map.SetFullAddress(FullAddress);
        ////_map.SetMap(radius, lat, lng, IsMoreSelected, elem);
        //save data
        LocationData ld = new LocationData();
        ld.Radius = _radiusM;
        ld.Lat = decimal.Parse(lat);
        ld.Lng = decimal.Parse(lng);
        ld.RegionId = _map.Region_ID;
        ld.IsMoreSelected = IsMoreSelected;
        ld.SelectAll = _map.Selected_All;
        LocationDataV = ld;


    }


    protected void btn_SetLocation_Click(object sender, EventArgs e)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        string result = "";
        decimal _lat, _lng, _radius;
        if (!decimal.TryParse(hf_lat.Value, out _lat) || !decimal.TryParse(hf_lng.Value, out _lng) || !decimal.TryParse(hf_radius.Value, out _radius))
        {
            ////Update_Faild();
            return;
        }
        Guid RegionId = Guid.Empty;
        bool SelectAll = false;
        string LargePlace = hf_LargePlace.Value;
        if (Utilities.IsGUID(LargePlace))
            RegionId = new Guid(LargePlace);
        else if (LargePlace == "all")
            SelectAll = true;

        if (IfSameDetails(_radius, _lng, _lat, RegionId, SelectAll))
        {
            /*
            string _script = "top.SetServerComment('" + HttpUtility.JavaScriptStringEncode("Nothing has been changed. It's all the same info as before.") + "'); location.href='" + ResolveUrl("~/Management/LocationMap.aspx?comment=true") + "';";
            ClientScript.RegisterStartupScript(this.GetType(), "SetComment", _script, true);
            */

            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem("Nothing has been changed. It's all the same info as before.");
            LoadLocation();

            return;
        }

        try
        {
            result = _supplier.SetCitiesByCoordinate(SupplierId, _radius, _lng, _lat, hf_FullAddress.Value, "", "", RegionId, SelectAll, false, "", false);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ////Update_Faild();
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);

            LoadLocation();

            return;
        }
        XDocument doc = XDocument.Parse(result);
        if (doc.Element("SetCitiesByCoordinate").Element("Error") != null)
        {
            ////Update_Faild();
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);

            LoadLocation();
            return;
        }
        string _status = doc.Element("SetCitiesByCoordinate").Element("Status").Value;
        if (_status == "Success")
        {
            /*
            if (userManagement.IsPublisher() && IsFirstTime)
                ClientScript.RegisterStartupScript(this.GetType(), "UpdateSuccessStep4", "UpdateSuccessStep4();", true);
            else
                ClientScript.RegisterStartupScript(this.GetType(), "SetComment", "top.SetServerComment('Update success');", true);
            */


        }
        else
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "SetComment", "top.SetServerComment('Update faild');", true);
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);

            LoadLocation();
        }
        LoadLocation();
    }


    bool IfSameDetails(decimal _radius, decimal _lng, decimal _lat, Guid region_id, bool select_all)
    {
        LocationData ld = LocationDataV;
        if (ld == null)
            return false;
        if (ld.Lat != _lat)
            return false;
        if (ld.Lng != _lng)
            return false;
        if (ld.RegionId != region_id)
            return false;
        if (ld.SelectAll != select_all)
            return false;
        if (region_id != Guid.Empty)
            return true;
        else
            if (ld.Radius != _radius)
                return false;
        return true;

    }

    bool IsFirstTime
    {
        get { return (ViewState["IsFirstTime"] == null) ? false : (bool)ViewState["IsFirstTime"]; }
        set { ViewState["IsFirstTime"] = value; }
    }
    LocationData LocationDataV
    {
        get { return (ViewState["LocationData"] == null) ? null : (LocationData)ViewState["LocationData"]; }
        set { ViewState["LocationData"] = value; }
    }

    protected string urlSuccees
    {
        get {return ResolveUrl("~") + "PPC/checkout.aspx"; }
    }




    public void setPlan(object sender, Controls_MapControlMulti.SendEventArgs e)
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.SetPlanType(e.StrSend);
    }
}