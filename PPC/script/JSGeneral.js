﻿function Text_Focus(_txt, __holder) {

    RemoveCssClass(_txt, "textError");
    if (_txt.className.indexOf("place-holder") != -1 && (_txt.value == __holder || _txt.value.length == 0)) {
        RemoveCssClass(_txt, "place-holder"); // txt_Mobile.className = "";
        _txt.value = "";
    }
}
function Text_Blur(_txt, __holder) {
    RemoveCssClass(_txt, "textError");
    var _value = _txt.value;
    if (_value.length == 0) {
        _txt.value = __holder;
        if(_txt.className.indexOf("place-holder") == -1)
            _txt.className += " place-holder";
    }
    else  {
        RemoveCssClass(_txt, "place-holder");        
    }
    
}
function GetText_PlaceHolder(_txt, __holder) {
    if (_txt.value == __holder && _txt.className.indexOf("place-holder") > -1)
        return "";
    return _txt.value;
}

function Text_Focus_pass(_txt, __holder) {

    RemoveCssClass(_txt, "textError");
    if (_txt.className.indexOf("place-holder") != -1 && _txt.value == __holder) {

         _txt.setAttribute("type", "password");
  //      $("#" + _txt.id).attr("type", "password");
       // _txt.type = "password";
        RemoveCssClass(_txt, "place-holder"); // txt_Mobile.className = "";
        _txt.value = "";
    }
}
function Text_Blur_pass(_txt, __holder) {
    RemoveCssClass(_txt, "textError");
    var _value = _txt.value;
    if (_value.length == 0) {
        _txt.setAttribute("type", "text");
        _txt.value = __holder;
        if (_txt.className.indexOf("place-holder") == -1)
            _txt.className += " place-holder";
    }
    else {
        RemoveCssClass(_txt, "place-holder");
    }

}
/********* Blind comment *************/
function RemoveServerComment() {
    try {
        if ($("#div_ServerComment").data('slide') != true)
            return;
        $("#div_ServerComment").data('slide', false);
        document.getElementById("lbl_ServerComment").innerHTML = "";
        $("#div_ServerComment").hide('drop', { direction: "up" }, 500);
    }
    catch (exc) { }
}
/*
function SetServerComment(_comment) {
    
    try {
        document.getElementById("lbl_ServerComment").innerHTML = _comment;
        if ($("#div_ServerComment").data('slide') == true)
            return;
        $("#div_ServerComment").data('slide', true);
        
        $("#div_ServerComment").show('drop', { direction: "up" }, 500);
       
    }
    catch (exc) { }
}

function AddServerComment(_comment) {   
    try {
       
        var _com = document.getElementById("lbl_ServerComment").innerHTML;
        if (_com.length == 0)
            _com = _comment;
        else
            _com += " " + _comment;
        document.getElementById("lbl_ServerComment").innerHTML = _com;
        if ($("#div_ServerComment").data('slide') == true) 
            return;
        $("#div_ServerComment").data('slide', true);
        $("#div_ServerComment:hidden").show('drop', { direction: "up" }, 500);
        
    }
    catch (exc) { }
}

function ClearTextServerComment() {
    document.getElementById("lbl_ServerComment").innerHTML = "";
}
/******* slide instruction  ********/
function SlideElement(_elemId, ToSlide) {
    if (typeof ($("#" + _elemId).data('sliding')) == 'undefined')
        $("#" + _elemId).data('sliding', false);
    if ($("#" + _elemId).data('sliding') == ToSlide)
        return;
    $("#" + _elemId).data('sliding', ToSlide)
    if (ToSlide) {
        //if ($("#" + _elemId + ":hidden").length == 0)
        $("#" + _elemId).show('slide', { direction: "down" }, 500);
    }
    else {
      //  if (!$("#" + _elemId + ":visible").data('sliding')) {
            $("#" + _elemId).hide('slide', { direction: "down" }, 500);
    //        $("#" + _elemId + ":visible").data('sliding', true);
    //    }
       
    }
}
