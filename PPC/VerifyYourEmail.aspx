﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Controls/MasterPageNP2.master" CodeFile="VerifyYourEmail.aspx.cs" Inherits="PPC_VerifyYourEmail" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNP2.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="verifyYourEmail">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div class="titleFirst">
        <span>Account verification</span>
    </div>

    <div id="verifyContent" class="verifyContent" runat="server">        
        
    </div>

</div>

</asp:Content>

