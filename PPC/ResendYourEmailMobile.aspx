﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Controls/MasterPageNP2.master" CodeFile="ResendYourEmailMobile.aspx.cs" Inherits="PPC_ResendYourEmailMobile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="resendYourEmail">   

    <div class="titleFirst">
        <span>Check your email</span>
    </div>

    <div class="resendContent">
        <div class="resendContent1">
             Please check your inbox <span id="registerEmail" runat="server"></span> and click the link in the message to complete your account
             
             setup.
        </div>
       

        <div class="resendContent2">
           <span class="emphasize">Didn’t receive an email from us?</span>
            <br />
            We can <a href="verifyYourEmailManually.aspx?error=resend&myEmail=<%#myEmail%>">resend</a> the email or try your registration again.
           
           
        </div>     
      
        
        
        
    </div>

</div>

</asp:Content>
