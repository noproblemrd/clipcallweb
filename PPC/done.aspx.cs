﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;

public partial class PPC_done : RegisterPage, IsInProcess
{    
    protected void Page_Load(object sender, EventArgs e)
    {      
       
        if (!IsPostBack)
        {           
            SendEmail();            
        }
        else
        {


        }
        LoadMaster();

        Header.DataBind();
    }

    private void LoadMaster()
    {
        Master.SetBenefitFreeText("Get leads from ready-to-hire customers directly to your mobile phone.</br></br>That's NoProblem!");
        Master.SetStatusBar("statusBar3");
    }

    private void SendEmail()
    {
        MailSender mailSender = new MailSender();

        mailSender.From = "registration@noproblem.co.il";
        mailSender.FromName = "NoProblem Registration";
        mailSender.To = "registration@onecall.co.il";        

        mailSender.IsText = false;
        mailSender.Subject = "Newly " + Session["plan"] + " potential advertiser plus details";

        string phone=Session["RegistrationPhone"]!=null? Session["RegistrationPhone"].ToString() : "";
        string password = Session["RegistrationPassword"] != null ? Session["RegistrationPassword"].ToString() : "";
        string compnayName = Session["name"] != null ? Session["name"].ToString() : "";
        string compnayContactPerson = Session["contactPerson"] != null ? Session["contactPerson"].ToString() : "";
        string region = Session["region"] != null ? Session["region"].ToString() : "";
        string category = Session["category"] != null ? Session["category"].ToString() : ""; 


        string body = "<bold>A potential advertiser clicked on " + Session["plan"] + " </bold><br><br>" +
                    "Phone: " + phone + "<br>" +
                    "Password: " + password + "<br>" +
                    "Company name: " + compnayName + "<br>" +
                    "Company Contact Person: " + compnayContactPerson + "<br>" +                    
                    "Business address: " + region + "<br>" +
                    "Category: " + category; 


        mailSender.Body = body;
        //mailSender.RemoteHost = "127.0.0.1";
        bool ifOkSentNoProblemBilling = mailSender.Send();

        if (ifOkSentNoProblemBilling)
        {
            // status = "true";
        }

        else
        {
            // status = "false";
            dbug_log.ExceptionLog(new Exception("Registration failed to send email to " + mailSender.To + " from " + phone), Site_Id);

        }
    }

   
    protected string Site_Id
    {
        get { return siteSetting.GetSiteID; }
    }   
}