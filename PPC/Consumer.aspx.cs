﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_Consumer : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Header.DataBind();
    }
    protected string GetHeadingServiceList
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/GetHeadingListIframe"); }
    }
}