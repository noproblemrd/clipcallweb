﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;

public partial class PPC_transactions2014 : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadPageText();
            LoadTransaction();
        }
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

    }

    private void LoadTransaction()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfListOfTransactionRow2014 result;
        try
        {
            // GetTransactions_Registration2014
            result = _supplier.GetTransactions_Registration2014(new Guid(GetGuidSetting()));            
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
            return;
        }
        /*
        result.Value[0].Amount;
        result.Value[0].Balance;
        result.Value[0].DateLocal;
        result.Value[0].DateServer;
        result.Value[0].Type;
         * */
        DataTable data = new DataTable();
        data.Columns.Add("Amount", typeof(string));
        data.Columns.Add("Date", typeof(string));
        data.Columns.Add("Type", typeof(string));        

        foreach (WebReferenceSupplier.TransactionRow2014 tr in result.Value)
        {
            DataRow row = data.NewRow();
            row["Amount"] = GetMoneyToDisplay(tr.Amount);
            row["Type"] = tr.Type;
            row["Date"] = Utilities.GetTimePassed(tr.DateServer, tr.DateLocal);            
            data.Rows.Add(row);
        }

        if (data != null )
        {
            if(data.Rows.Count <= 10)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(false);", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);
        }

        _GridView_Transaction.PageSize = 10;
        _GridView_Transaction.DataSource = data;
        _GridView_Transaction.DataBind();
        _GridView_Transaction.PageIndex = 0;

        Session["data"] = data;
    }

    protected void _GridView_Transaction_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        /*
        if (e.Row.RowIndex == 5)
            e.Row.CssClass = "_rs _fs";
       */
    }


    protected void trans_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        //Response.Write(_GridView_Transaction.PageCount + " " + _GridView_Transaction.PageIndex);

        _GridView_Transaction.PageSize += 10; // use this instead of below
        //_GridView_Transaction.PageIndex = e.NewPageIndex;

        _GridView_Transaction.DataSource = Session["data"];
        _GridView_Transaction.DataBind();

        // pageindex start from 0
        if (_GridView_Transaction.PageIndex != _GridView_Transaction.PageCount - 1)
        {

            //updatePanelTrans.Update();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);
        }

        else // after it there are no leads
        {

            ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(false);", true);
        }

        //Response.Write(_GridView_Transaction.PageCount + " " + _GridView_Transaction.PageIndex);

    }

    protected string GetServerError
    {
        get { return (ViewState["GetServerError"] == null) ? " " : ViewState["GetServerError"].ToString(); }
        set { ViewState["GetServerError"] = value; }
    }
}