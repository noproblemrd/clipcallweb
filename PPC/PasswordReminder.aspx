﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNP.master" AutoEventWireup="true"
    CodeFile="PasswordReminder.aspx.cs" Inherits="PPC_PasswordReminder" %>

<%@ MasterType VirtualPath="~/Controls/MasterPageNP.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="../scripts/US-Phone.js"></script>
    <script type="text/javascript" src="../PlaceHolder.js"></script>
    <%/*  
    <script type="text/javascript" src="../general.js"></script>

<script type="text/javascript" src="script/JSGeneral.js"></script>
<script type="text/javascript" src="../CallService.js"></script>
*/ %>
    <script type="text/javascript">
        /***  Mobile  ****/
        var _phone = new np_phone('<%# GetPhoneExpresion %>');
        _phone.init();
        _phone.OnKeyUpEmpty = function (txtId) {
            ClearPhoneValidate();
        };
        _phone.OnMatch = function (txtId) {
            ClearPhoneValidate();
            $('#MobileValid').show();
            $('#<%# txt_Mobile.ClientID %>').addClass('textValid');
        };
        _phone.OnKeyUpOnTheWay = function (txtId) {
            ClearPhoneValidate();
        };
        _phone.OnFaild = function (txtId) {
            ClearPhoneValidate();
            $('#MobileInstruction').show();
            $('#MobileError').show();
            $('#<%# txt_Mobile.ClientID %>').addClass('textError');
        };
        _phone.OnClickFaild = function (txtId) {
            _phone.OnFaild(txtId);
            var _status = _phone.checkPhoneStatus(txtId);
            switch (_status) {
                case ("OnTheWay"):
                case ("faild"):
                    SetServerComment('<%# PhoneInvalid %>');
                    break;
                case ("empty"):
                    SetServerComment('<%# PhoneMissing %>');
                    break;
            }
            return false;
        };
        _phone.OnEmpty = function (txtId) {
            ClearPhoneValidate();

        };
        _phone.OnFocus = function (txtId) {
            ClearPhoneValidate();
        };
        _phone.OnClickMatch = function (txtId) {
            ExecDetails(txtId);
            return true;
        };

        function ClearPhoneValidate() {
            $('#MobileValid').hide();
            $('#MobileError').hide();
            $('#MobileInstruction').hide();
            $('#<%# txt_Mobile.ClientID %>').removeClass('textError');
            $('#<%# txt_Mobile.ClientID %>').removeClass('textValid');
        }

        /*****/
        function ExecDetails(txtId) {
            var _mobile = $('#' + txtId).val();
            RemoveServerComment();
            $.ajax({
                url: "<%# IfPhoneExists %>",
                data: "{ 'mobile': '" + _mobile + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d == 'true') {
                        return true;
                    }
                    else if (data.d == "false") {
                        SetServerComment("<%# PhoneNotFind %>");
                        return false;
                    }
                    else {
                        GeneralServerError();
                        return false;
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    GeneralServerError();
                }

            });

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main_login">
        <div id="wrapadv">
            <div class="titleFirst titleForgotPassword">
                <span class="TitleRight">Forgot password</span>
                <div class="titleRight">
                    <span>Don't have an account? <a href="Register.aspx" class="general">Register</a></span></div>
            </div>
            <div class="clear">
            </div>
            <div class="regnav" id="lbl_EnterMobile">
                <p>
                    Please enter the mobile number you registered with.</p>
            </div>
            <div id="div_PasswordReminder">
                <div class="regoform">
                    <label>
                        My mobile number is</label>
                    <div class="inputform">
                        <div class="inputParent">
                            <div class="inputSun">
                                <input type="text" id="txt_Mobile" runat="server" />
                            </div>
                            <div class="inputValid" style="display: none;" id="MobileValid">
                            </div>
                            <div class="inputError" id="MobileError" style="display: none;">
                            </div>
                            <div class="inputComment" id="MobileInstruction" style="display: none;">
                                <%: PhoneInstruction %>
                                <div class="chat-bubble-arrow-border">
                                </div>
                                <div class="chat-bubble-arrow">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <asp:LinkButton ID="a_submit" runat="server" class="a_submit" OnClientClick="return _phone.onclick();"
                        OnClick="lb_send_Click">Send My Password</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
