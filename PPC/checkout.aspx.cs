﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_checkout : RegisterPage2, IsInProcess
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            plan = "FEATURED_LISTING"; // LEAD_BOOSTER_AND_FEATURED_LISTING , FEATURED_LISTING  for testing mark method LoadPlan()


            LoadMaster();
            LoadPlan();
            LoadFee();
        }

        Page.DataBind();
    }

    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

        masterPage.SetStatusBar("past", "past", "current");
        masterPage.SetBlackMenu();
        
    }

    protected void LoadPlan()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfString resultOfString = new WebReferenceSupplier.ResultOfString();
        
        try
        {            
            resultOfString = _supplier.Checkout_GetPlan_Registration2014(SupplierId);
            plan = resultOfString.Value;
           
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.SetPlanType(plan);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);

            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            //masterPage.ServerProblem(GetServerError);
           
        }

    }

    protected void LoadFee()
    {
        
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfSubscriptionCostAndBonus result = new WebReferenceSupplier.ResultOfSubscriptionCostAndBonus();
        try
        {
            result = _supplier.GetSubscriptionPlanCost();
            fee = result.Value.Cost;           
            
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            //masterPage.ServerProblem(GetServerError);  
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            //masterPage.ServerProblem(GetServerError);  
            return;
        }

        
   
    }

    

    /*
    Supplier.asmx

[WebMethod]
public Result<string> Checkout_GetPlan_Registration2014(Guid supplierId)

the string will be one of the following constants:
public static class Plans
{
         public const string FEATURED_LISTING = "FEATURED_LISTING";
         public const string LEAD_BOOSTER_AND_FEATURED_LISTING = "LEAD_BOOSTER_AND_FEATURED_LISTING";
}
*/

    protected void lbCheckout_Click(object sender, EventArgs e)
    {
        //public static string BuyCredit2(int deposit, SiteSetting siteSetting, Guid SupplierId)

        string urlPayment;

        string host;
        host = Request.Url.Host;


        if (host == "localhost")
            urlPayment = "http://" + host + ":" + Request.Url.Port + ResolveUrl("~") + "ppc/" + "payment.aspx?plan=" + plan;
        else
            urlPayment = "http://" + host + "/ppc/" + "payment.aspx?plan=" + plan;


        //urlPayment = "payment.aspx?plan=" + plan;

        Response.Redirect(urlPayment);
        
    }

    protected string plan
    {
        get
        {
            return (ViewState["plan"]!=null)? ViewState["plan"].ToString(): ""; 
        }

        set
        {
            ViewState["plan"] = value;
        }

    }

    protected decimal fee
    {
        get
        {
            return (ViewState["fee"] != null) ? (decimal)ViewState["fee"] : 0;
        }

        set
        {
            ViewState["fee"] = value;
        }

    }
}