﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentMethod.aspx.cs" Inherits="PPC_PaymentMethod" MasterPageFile="~/Controls/MasterPagePPC2.master"%>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/menuVerticalBilling.ascx" TagName="menuVertical" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">    

     <script>
        

         function serverError() {
             SetServerComment2("<%#GetServerError%>");

         }


         function init() {             
             parent.hideDiv();
         }

         window.onload = init;
    </script>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

        <uc1:menuVertical  ID="menuVertical" runat="server" whoActive="paymentMethod"/>

        <div class="paymentMethod dashboardIframe">
         <uc1:PpcComment runat="server" ID="_comment"  />
           <div class="titleFirst">
                <span>Payment method</span>
            </div>


            <div class="paymentMethodContent" runat="server" id="paymentMethodContent">

                <div class="paymentMethodContentItem">
                    <div class="paymentItemLeft">Card number:</div>
                    <div class="paymentItemLeft paymentItemRight">**** **** **** <span id="lbl_last4Digits" runat="server"></span> </div>
                    <div class="paymentItemEdit"><asp:LinkButton  ID="lbEdit" runat="server" 
                            onclick="lbEdit_Click">Change</asp:LinkButton></div>
                </div>

                <div class="clear"></div>

                <div class="paymentMethodContentItem">
                    <div class="paymentItemLeft">Expiration date:</div>
                    <div class="paymentItemRight"><span id="lbl_expirationDate" runat="server"></span></div>
                </div>
             
                <div class="clear"></div>

                <div class="paymentMethodContentItem">
                    <div class="paymentItemLeft">Cardholders name:</div>
                    <div class="paymentItemRight"><span id="lbl_CarHolderName" runat="server"></span></div>
                </div>

                <div class="clear"></div>

                <!--
                <div class="paymentMethodContentItem">
                    <div class="paymentItemLeft">Billing address:</div>
                    <div class="paymentItemRight"><span id="lbl_CarHolderAddress" runat="server"></span></div>
                </div>

                <div class="clear"></div>
                -->

            </div>

            <div class="containerIframePayment">
                <iframe id="iframePayment" src="" runat="server" width="587" height="1500" frameborder="0"></iframe>
            </div>    


        </div>
</asp:Content>
