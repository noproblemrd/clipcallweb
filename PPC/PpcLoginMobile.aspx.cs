﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Xml.Linq;

public partial class PPC_PpcLoginMobile : PageSetting, IPostBackEventHandler
{
    const string POST_BACK = "POST_BACK";
    const string SCRAMBEL = "*4261ppc";
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.RemoveRibbon();

        if (!IsPostBack)
        {
            //Master.SetBenefitFreeText("Concentrate on the work at hand rather than drumming up new business with NoProblem.</br></br>We bring quality leads directly to you!");
            LoadPageText();
            SetLoginOptions();
            if (Request.Cookies["PpcLogin"] != null)
            {
                string _password = Request.Cookies["PpcLogin"].Values["Login"];
                if (!string.IsNullOrEmpty(_password))
                {
                    try
                    {
                        _password = EncryptString.Decrypt_String(_password, SCRAMBEL);
                    }
                    catch (Exception exc) { }
                    string[] newPassword = _password.Split(';');
                    if (newPassword.Length == 2)
                    {
                        txt_Mobile.Value = newPassword[1];
                        txt_Mobile.Attributes.Remove("class");
                        txt_Password.Attributes.Add("value", newPassword[0]);
                        txt_Password.Style[HtmlTextWriterStyle.Display] = "block";
                    }

                }

            }

            Master.RemoveSiteMap();
            txt_Mobile.Attributes["place_holder"] = PhonePlaceHolder;
            txt_Mobile.Attributes["HolderClass"] = "place-holder";
            txt_Mobile.Attributes["text-mode"] = "phone";
            txt_Mobile.Attributes["class"] = "textActive";

            btn_Login.Attributes["button-mode"] = "phone";
            txt_Password.Attributes["place_holder"] = CodePlaceHolder;
            txt_Password.Attributes["HolderClass"] = "place-holder";
            lt_PhoneInstruction.Text = PhoneInstruction;
            ////cb_RememberMe.Checked = (Request.Cookies["rememberme"] == null || Request.Cookies["rememberme"].Value != "false");

        }

        //   ClientScript.RegisterClientScriptBlock(this.GetType(), "PhoneValidation", Utilities.CheckStatusUSPhoneJavaScript(), true);
        Header.DataBind();

    }
    void SetLoginOptions()
    {
        string verification = Request.QueryString["verification"];
        string HisEmail = Request.QueryString["verify"];
        string FriendEmail = Request.QueryString["friendemail"];
        if (!string.IsNullOrEmpty(verification) && !string.IsNullOrEmpty(HisEmail) && !string.IsNullOrEmpty(FriendEmail))
        {
            GotoInvitation(verification, HisEmail, FriendEmail);
            return;
        }
        // to check
        string _sent = Request.QueryString["sent"];
        if (!string.IsNullOrEmpty(_sent))
        {
            string _comm = null;
            if (_sent == "mobile")
                _comm = PasswordSentMobile;
            else if (_sent == "email")
                _comm = PasswordSentEmail;
            if (!string.IsNullOrEmpty(_comm))
                SetComment(_comm);
        }
        string _register = Request.QueryString["register"];

        if (!string.IsNullOrEmpty(_register) && _register == "true")
            SetCommentAllreadyRegister();
        /*
        if (ReasonLogOutV == eReasonLogout.SessionEnd.ToString())
            SetComment(SessionEndV);
         * */
        string _refer = Request.QueryString["refer"];
        _refer = (string.IsNullOrEmpty(_refer)) ? string.Empty : _refer.ToLower();
        eLoginReferrer elr;
        if (Enum.TryParse(_refer, out elr))
        {
            switch (elr)
            {
                case (eLoginReferrer.add_credit):
                    LoginDestination = ResolveUrl("~/PPC/AddCredit.aspx");
                    break;
                case (eLoginReferrer.earn_credit):
                    LoginDestination = ResolveUrl("~/PPC/earnCredit.aspx");
                    break;
                case (eLoginReferrer.billing):
                    LoginDestination = ResolveUrl("~/PPC/BillingOverview.aspx");
                    break;
                case (eLoginReferrer.invite_pro):
                    LoginDestination = ResolveUrl("~/PPC/invitePro.aspx");
                    break;
                default:
                    LoginDestination = ResolveUrl("~/Management/overview.aspx");
                    break;
            }
        }
        if (userManagement.IsSupplier())
            Response.Redirect(LoginDestination);



    }

    private void GotoInvitation(string verification, string HisEmail, string FriendEmail)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfBoolean result;
        try
        {
            result = _supplier.VerifyEmail(verification, HisEmail);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            //         Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            //          Update_Faild();
            return;
        }
        if (!result.Value)
            return;
        LoginDestination = ResolveUrl("~/PPC/invitePro.aspx?friendemail=" + FriendEmail);
    }
    private void SetCommentAllreadyRegister()
    {
        string _link = @"<a href='" + ResolveUrl("~/PPC/PasswordReminder.aspx") + @"'>" + AllreadyRegisterLink + "</a>";
        string mess = AllreadyRegister + " " + _link;
        SetComment(mess);
    }
    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();


        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "login"
                          select x).FirstOrDefault();


        XElement phone_element = xelem.Element("mobile").Element("Validated");
        AllreadyRegister = phone_element.Element("Instruction").Element("AllreadyRegister").Value;
        AllreadyRegisterLink = phone_element.Element("Instruction").Element("AllreadyRegisterLink").Value;
        PasswordSentEmail = phone_element.Element("Instruction").Element("PasswordSentEmail").Value;
        PasswordSentMobile = phone_element.Element("Instruction").Element("PasswordSentMobile").Value;
        AccountInactive = phone_element.Element("Instruction").Element("AccountInactive").Value;
        AccountLocked = phone_element.Element("Instruction").Element("AccountLocked").Value;
        PhoneInstruction = phone_element.Element("Instruction").Element("General").Value;
        PhonePlaceHolder = phone_element.Element("Instruction").Element("PhonePlaceHolder").Value;
        SessionEndV = phone_element.Element("Instruction").Element("SessionEnd").Value;
        PhoneMissing = phone_element.Element("Instruction").Element("PhoneMissing").Value;
        PhoneInvalid = phone_element.Element("Instruction").Element("PhoneInvalid").Value;
        PasswordMissing = phone_element.Element("Instruction").Element("PasswordMissing").Value;
        AccountNotExist = phone_element.Element("Instruction").Element("AccountNotExist").Value;
        WrongPassword = phone_element.Element("Instruction").Element("WrongPassword").Value;
        InvitaionNotAproved = phone_element.Element("Instruction").Element("InvitaionNotAproved").Value;
        LoginFaild = phone_element.Element("Error").Element("LoginFaild").Value;

        XElement password_element = xelem.Element("password").Element("Validated");
        CodePlaceHolder = password_element.Element("Instruction").Element("CodePlaceHolder").Value;
        MissingMultiFields = HttpUtility.JavaScriptStringEncode(xdoc.Element("Data").Element("GeneralFields").Element("GeneralValidated").Element("Validated").Element("Instruction").Element("Missing").Value);

    }
    void SetComment(string comment)
    {
        //ClientScript.RegisterStartupScript(this.GetType(), "SetComment", @"SetServerComment(""" + _comment + @""");", true);
        Master.SetServerComment(comment);
    }
    /*
    void SetPlaceHolder()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "SetPlaceHolder", "SetPlaceHolder();", true);
    }
     * */
    protected void btn_Login_Click(object sender, EventArgs e)
    {
        log_in();
    }
    void log_in()
    {
        if (siteSetting == null || string.IsNullOrEmpty(siteSetting.GetSiteID))
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            if (!DBConnection.LoadSiteId(host, this))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
        }
        string mobile = txt_Mobile.Value; 
        string passWord = txt_Password.Text;



        mobile = Utilities.GetCleanPhone(mobile);

        /*
        if (cb_RememberMe.Checked)
        {
            string newPassword = passWord + ";" + mobile;
            newPassword = EncryptString.Encrypt_String(newPassword, SCRAMBEL);
            //       Response.Cookies["professionalLogin"]["professionalEmail"] = email;Cookies["PpcLogin"].Values["Login"];
            Response.Cookies["PpcLogin"]["Login"] = newPassword;
            Response.Cookies["PpcLogin"].Expires = DateTime.Now.AddMonths(3);
            Response.Cookies["rememberme"].Values.Clear();
        }
        else
        {
            Response.Cookies["PpcLogin"].Values.Clear();
            Response.Cookies["rememberme"].Value = "false";
            Response.Cookies["rememberme"].Expires = DateTime.Now.AddMonths(3);
        }
        */
   

        //Check if try to access multi times

        /*
        bool IsApprove = DBConnection.IsLoginApproved(mobile, siteSetting.GetSiteID);
        if (!IsApprove)
        {
            SetComment(HttpUtility.HtmlEncode(AccountLocked));
            return;
        }
        */

        /**********/

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfPhoneLogInResponse _response = null;
        WebServiceSales webServiceSales = new WebServiceSales();

        WebReferenceSupplier.PhoneLogInRequest phoneLogInRequest=new WebReferenceSupplier.PhoneLogInRequest();
        phoneLogInRequest.Password=passWord;
        phoneLogInRequest.Phone=mobile;

        try
        {

            _response = supplier.PhoneLogIn_Legacy2014(phoneLogInRequest);

            if (_response.Type == WebReferenceSupplier.eResultType.Success)
            {
                string redirectPath = webServiceSales.legacyRedirectMap(_response.Value.SupplierId,
                        _response.Value.StepLegacy, _response.Value.Step,
                        _response.Value.LegacyStatus, _response.Value.Name, _response.Value.Balance);

                if (redirectPath == "PHONE_PASSWORD_COMBINATION_NOT_FOUND")
                {
                    SetComment(HttpUtility.HtmlEncode(WrongPassword));
                    //ClientScript.RegisterStartupScript(this.GetType(), "SetPasswordError", "PasswordInvalid();", true);
                    return;
                }

                else
                {
                    Response.Redirect(redirectPath);
                }
            }

            else
            {     
                dbug_log.ExceptionLog(new Exception("failed login for mobile for supplier mobile " + mobile + " error: " + _response.Messages[0]));        
            }

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            SetComment(LoginFaild);
            return;
        }        

        
    }

    private void LoadDateTimeFormat()
    {
        string command = "EXEC dbo.GetDateTimeFormatByUserId @UserId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@UserId", new Guid(userManagement.GetGuid));
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                if (!reader.IsDBNull(0))
                    siteSetting.DateFormat = (string)reader["DateFormat"];

                if (!reader.IsDBNull(1))
                    siteSetting.TimeFormat = (string)reader["TimeFormat"];
            }
            conn.Close();
        }
    }
    #region IPostBackEventHandler Members
    public void RaisePostBackEvent(string eventArgument)
    {
        if (eventArgument == POST_BACK)
        {
            log_in();
        }
    }
    protected string GetPostBackEnter()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(this, POST_BACK);
        myPostBackOptions.PerformValidation = false;
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
    }
    #endregion
    protected string PhoneInstruction
    {
        get { return (string)ViewState["PhoneInstruction"]; }
        set { ViewState["PhoneInstruction"] = value; }
    }
    protected string PhonePlaceHolder
    {
        get { return (string)ViewState["PhonePlaceHolder"]; }
        set { ViewState["PhonePlaceHolder"] = value; }
    }
    protected string CodePlaceHolder
    {
        get { return (string)ViewState["CodePlaceHolder"]; }
        set { ViewState["CodePlaceHolder"] = value; }
    }
    protected string GetPhoneExpresion
    {
        //        get { return Utilities.RegularExpressionForJavascript(@"^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"); }
        get { return Utilities.RegularExpressionForJavascript(siteSetting.GetPhoneRegularExpression); }
        //    get { return Utilities.RegularExpressionForJavascript(@"^\d+$"); }
    }
    protected string AllreadyRegister
    {
        get { return (string)ViewState["AllreadyRegister"]; }
        set { ViewState["AllreadyRegister"] = value; }
    }

    protected string PasswordSentEmail
    {
        get { return (string)ViewState["PasswordSentEmail"]; }
        set { ViewState["PasswordSentEmail"] = value; }
    }
    protected string PasswordSentMobile
    {
        get { return (string)ViewState["PasswordSentMobile"]; }
        set { ViewState["PasswordSentMobile"] = value; }
    }
    protected string LoginFaild
    {
        get { return (string)ViewState["LoginFaild"]; }
        set { ViewState["LoginFaild"] = value; }
    }
    protected string AccountLocked
    {
        get { return (string)ViewState["AccountLocked"]; }
        set { ViewState["AccountLocked"] = value; }
    }
    protected string AccountInactive
    {
        get { return (string)ViewState["AccountInactive"]; }
        set { ViewState["AccountInactive"] = value; }
    }
    protected string SessionEndV
    {
        get { return (string)ViewState["SessionEnd"]; }
        set { ViewState["SessionEnd"] = value; }
    }
    string ReasonLogOutV
    {
        get { return (Session["ReasonLogOut"] == null) ? string.Empty : (string)Session["ReasonLogOut"]; }
        set { Session["ReasonLogOut"] = value; }

    }
    protected string AllreadyRegisterLink
    {
        get { return (string)ViewState["AllreadyRegisterLink"]; }
        set { ViewState["AllreadyRegisterLink"] = value; }
    }
    /*
     * <EmailMissing>Email missing</EmailMissing>
          <EmailInvalid>The email you entered is invalid. Please check for typos.</EmailInvalid>
          <PasswordMissing>Password missing</PasswordMissing>
     * */
    protected string PhoneMissing
    {
        get { return (string)ViewState["PhoneMissing"]; }
        set { ViewState["PhoneMissing"] = value; }
    }
    protected string PhoneInvalid
    {
        get { return (string)ViewState["PhoneInvalid"]; }
        set { ViewState["PhoneInvalid"] = value; }
    }
    protected string PasswordMissing
    {
        get { return (string)ViewState["PasswordMissing"]; }
        set { ViewState["PasswordMissing"] = value; }
    }
    protected string AccountNotExist
    {
        get { return (string)ViewState["AccountNotExist"]; }
        set { ViewState["AccountNotExist"] = value; }
    }
    protected string WrongPassword
    {
        get { return (string)ViewState["WrongPassword"]; }
        set { ViewState["WrongPassword"] = value; }
    }
    protected string LoginDestination
    {
        get { return (ViewState["LoginDestination"] == null) ? string.Empty : (string)ViewState["LoginDestination"]; }
        set { ViewState["LoginDestination"] = value; }
    }
    protected string InvitaionNotAproved
    {
        get { return (ViewState["InvitaionNotAproved"] == null) ? string.Empty : (string)ViewState["InvitaionNotAproved"]; }
        set { ViewState["InvitaionNotAproved"] = value; }
    }
    protected string MissingMultiFields
    {
        get { return (ViewState["MissingMultiFields"] == null) ? string.Empty : (string)ViewState["MissingMultiFields"]; }
        set { ViewState["MissingMultiFields"] = value; }
    }
}