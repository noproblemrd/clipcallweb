﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_RegisterBefore : PageSetting
{


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadPageText();

            //    lbl_CodePlaceHolder.Text = CodePlaceHolder;
            /*
            siteSetting = new SiteSetting("1");
            siteSetting.SetSiteSetting(this);
            */
            //    txt_pass.Value = CodePlaceHolder;
            //      pass_place.DataBind();
            txt_Mobile.Attributes.Add("place_holder", PhonePlaceHolder);
            txt_pass.Attributes.Add("place_holder", CodePlaceHolder);
            SetBenefit();
            hf_IsFirstTime.Value = "true";
            //         txt_Mobile.Text = PhonePlaceHolder;
            //      ClientScript.RegisterStartupScript(this.GetType(), "SetPhonePlaceHolder", "SetPhonePlaceHolder();", true);
            SetInvitationId();
        }

        ClientScript.RegisterClientScriptBlock(this.GetType(), "PhoneValidation", Utilities.CheckStatusUSPhoneJavaScript(), true);
        Header.DataBind();
    }

    private void SetInvitationId()
    {
        string invitationStr = Context.Request.QueryString["invitationid"];
        Guid invitationId;
        if (!Guid.TryParse(invitationStr, out invitationId))
        {
            invitationId = Guid.Empty;
        }
        InvitationId = invitationId;
        Session["InvitationId"] = invitationId;
    }

    private void SetBenefit()
    {
        Master.SetBenefits("<h3>Performance Advertising</h3><br/><br/>Sign up now to start connecting to ready-to-hire customers in real time to your mobile phone.");
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        FieldMissing = Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "DetailsService"
                          select x).FirstOrDefault();
        CodeSent = xelem.Element("FormMessages").Element("PasswordSent").Value;

        XElement phone_element = xelem.Element("MobilePhone").Element("Validated");
        PhonePlaceHolder = phone_element.Element("Instruction").Element("General").Value;
        InvalidPhoneNumber = phone_element.Element("Error").Value;
        ServerErrorPhoneNumber = phone_element.Element("ServerError").Value;
        MissingPhone = phone_element.Element("Missing").Value;

        XElement code_element = xelem.Element("PasswordCode").Element("Validated");
        CodePlaceHolder = code_element.Element("Instruction").Element("General").Value;
        WrongPassword = code_element.Element("Instruction").Element("WrongCode").Value;



    }



    void SetServerComment(string str)
    {
        if (!ClientScript.IsStartupScriptRegistered("ServerComment"))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerComment", "SetServerComment('" + HttpUtility.HtmlEncode(str) + "');", true);
    }

    protected string GetSendMobileService
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/SendMobile"; }
    }
    protected string GetLoginService
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/Login"; }
    }

    protected Guid InvitationId { get; private set; }

    protected string GetPhoneExpresion
    {
        //     get { return Utilities.RegularExpressionForJavascript(@"^[+]?[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"); }
        get { return Utilities.RegularExpressionForJavascript(siteSetting.GetPhoneRegularExpression); }
    }

    protected string Site_Id
    {
        get { return siteSetting.GetSiteID; }
    }

    protected string LoginPage
    {
        get { return ResolveUrl("~") + "PPC/PpcLogin.aspx?register=true"; }
    }

    protected string RedirectService
    {
        get { return ResolveUrl("~") + "PPC/Service.aspx"; }
    }
    protected string PhonePlaceHolder
    {
        get { return (string)ViewState["PhonePlaceHolder"]; }//{ return "e.g. 1(646)1234567"; }
        set { ViewState["PhonePlaceHolder"] = value; }
    }
    protected string CodePlaceHolder
    {
        //       get { return "It's in the text message we sent to tour phone"; }
        get { return (string)ViewState["CodePlaceHolder"]; }
        set { ViewState["CodePlaceHolder"] = value; }
    }

    protected string InvalidPhoneNumber
    {
        get { return (string)ViewState["InvalidPhoneNumber"]; }
        set { ViewState["InvalidPhoneNumber"] = value; }
    }
    protected string ServerErrorPhoneNumber
    {
        get { return (string)ViewState["ServerErrorPhoneNumber"]; }
        set { ViewState["ServerErrorPhoneNumber"] = value; }
    }
    protected string CodeSent
    {
        get { return (string)ViewState["CodeSent"]; }
        set { ViewState["CodeSent"] = value; }
    }

    protected string WrongPassword
    {
        get { return (string)ViewState["WrongPassword"]; }
        set { ViewState["WrongPassword"] = value; }
    }
    protected string GetServerError
    {
        /*
        get
        {
            return "The code could not be sent, please try again later";
        }
         * */
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }
    protected string FieldMissing
    {
        get { return (string)ViewState["FieldMissing"]; }
        set { ViewState["FieldMissing"] = value; }
    }
    protected string MissingPhone
    {
        get { return (string)ViewState["MissingPhone"]; }
        set { ViewState["MissingPhone"] = value; }
    }
}