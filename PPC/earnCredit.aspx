﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="earnCredit.aspx.cs" Inherits="PPC_earnCredit" MasterPageFile="~/Controls/MasterPagePPC.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script type="text/javascript">
    function CompleteStart() {
        window.location.href = '<%# OverView %>';
    }
    function BonusCredit() {
        window.location.href = '<%# BonusCredit %>';
    }
    function Invite() {
        window.location.href = '<%# Invite %>';
    }



</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">
<div class="EarnCredit">
    

     <div class="title">
        <span>Earn Credit</span>
     </div>
     <span class="description-page">There are lots of easy ways to earn NoProblem bonus credit—that means more money to spend on calls from qualified leads! Remember, it all adds up!</span>
     <div id="EarnCreditContainer" class="EarnCreditContainer">
        <div class="div-EarnCreditAction" runat="server" id="EarnCredit_CompleteGetStarted" onclick="CompleteStart();">
            <div class="EarnCredit-icon icon_CompleteGetStarted"></div>
            <div class="EarnCreditText">
                <a href="javascript:void(0);">Complete our Get Started Guide</a>
                <span>Familiarize yourself with NoProblem.</span>
            </div>
            <div class="EarnCredit-right">                   
                <span class="money">25</span>
                <span class="currency">$</span>
            </div>
        </div>

        <div class="div-EarnCreditAction" runat="server" id="EarnCredit_Invite" onclick="Invite();">
            <div class="EarnCredit-icon icon_InviteAPro"></div>
            <div class="EarnCreditText">
                <a href="javascript:void(0);">Invite a Pro</a>
                <span>Invite your professional friends and start earning money!</span>
            </div>
            <div class="EarnCredit-right right-invite">                   
                <span class="money">25</span>
                <span class="currency">$</span>
                <span class="free_text">+1% on your friend’s future payments</span>
            </div>
        </div>

        <div class="div-EarnCreditAction Last" runat="server" id="EarnCredit_Bonus" onclick="BonusCredit();">
            <div class="EarnCredit-icon icon_BonusCredit"></div>
            <div class="EarnCreditText">
                <a href="javascript:void(0);">Bonus credit</a>
                <span>Buy a prepaid plan, earn bonus credit. The bigger the plan, the better the value.</span>
            </div>
            <div class="EarnCredit-right right-Bonus">
                <span class="free_text">Up to</span>
                <span style="clear:both;"></span>                   
                <span class="money">25</span>
                <span class="currency">$</span>
                
            </div>
        </div>
     </div>
</div>
</asp:Content>
