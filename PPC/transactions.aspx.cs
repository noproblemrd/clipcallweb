﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PPC_transactions : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadTransaction();
        }      
    }

    private void LoadTransaction()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfListOfTransactionRow result;
        try
        {
            result = _supplier.GetTransactions(new Guid(GetGuidSetting()), false);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        /*
        result.Value[0].Amount;
        result.Value[0].Balance;
        result.Value[0].DateLocal;
        result.Value[0].DateServer;
        result.Value[0].Type;
         * */
        DataTable data = new DataTable();
        data.Columns.Add("Amount", typeof(string));
        data.Columns.Add("Date", typeof(string));
        data.Columns.Add("Type", typeof(string));
        data.Columns.Add("Balance", typeof(string));

        foreach (WebReferenceSupplier.TransactionRow tr in result.Value)
        {
            DataRow row = data.NewRow();
            row["Amount"] = GetMoneyToDisplay(tr.Amount);
            row["Type"] = tr.Type;
            row["Date"] = Utilities.GetTimePassed(tr.DateServer, tr.DateLocal);
            row["Balance"] = GetMoneyToDisplay(tr.Balance);
            data.Rows.Add(row);
        }

        if(data!=null)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);

        _GridView_Transaction.PageSize = 10;
        _GridView_Transaction.DataSource = data;        
        _GridView_Transaction.DataBind();
        _GridView_Transaction.PageIndex = 0;
        
        Session["data"] = data;
    }

    protected void _GridView_Transaction_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        /*
        if (e.Row.RowIndex == 5)
            e.Row.CssClass = "_rs _fs";
       */
    }

        
    protected void trans_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {    

        //Response.Write(_GridView_Transaction.PageCount + " " + _GridView_Transaction.PageIndex);

        _GridView_Transaction.PageSize += 10; // use this instead of below
        //_GridView_Transaction.PageIndex = e.NewPageIndex;

        _GridView_Transaction.DataSource = Session["data"];
        _GridView_Transaction.DataBind();

        // pageindex start from 0
        if (_GridView_Transaction.PageIndex != _GridView_Transaction.PageCount - 1)
        {
           
            //updatePanelTrans.Update();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(true);", true);
        }

        else // after it there are no leads
        {
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "disabledNext", "enableNext(false);", true);
        }

        //Response.Write(_GridView_Transaction.PageCount + " " + _GridView_Transaction.PageIndex);
        
    }
    
}