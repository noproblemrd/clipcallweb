﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_checkYourEmail : PageSetting
{
    protected override void OnPreInit(EventArgs e)
    {
        
        base.OnPreInit(e);
        if (userManagement.IsPublisher())
            this.MasterPageFile = "../Publisher/MasterPageFrame.master";
        else if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
        {
            this.MasterPageFile = ResolveUrl("~") + "Controls/MasterPagePPC2.master";

        }
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            

            supplierGuid = GetGuidSetting();
            WebServiceSales webServiceSales = new WebServiceSales();
            bool resendOk = webServiceSales.resendRegistrationEmail(new Guid(supplierGuid));

            loadDetails();
        }
    }


    private void loadDetails()
    {       

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfGetBusinessProfileResponse resultOfGetBusinessProfileResponse = new WebReferenceSupplier.ResultOfGetBusinessProfileResponse();

        try
        {
            resultOfGetBusinessProfileResponse = _supplier.GetBusinessProfile(new Guid(supplierGuid));           

            if (resultOfGetBusinessProfileResponse.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed GetBusinessDetails_Registration2014 " + resultOfGetBusinessProfileResponse.Messages[0]));

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
            }

            else
            {
                registerEmail.InnerText = resultOfGetBusinessProfileResponse.Value.Email;
            }

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "UpdateFailed();", true);
        }


    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
       

        WebServiceSales webServiceSales = new WebServiceSales();
        bool resendOk = webServiceSales.resendRegistrationEmail(new Guid(supplierGuid));

        //UpdatePanelVerifyResult.Update();
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerComment", "SetServerComment('" + Sent + "');", true);
        /*
        if (resendOk)
        {
           
            if (!ClientScript.IsStartupScriptRegistered("ServerComment"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerComment", "SetServerComment('" + Sent + "');", true);
                
        }
        else
        {
            
            if (!ClientScript.IsStartupScriptRegistered("ServerComment"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerComment", "SetServerComment('" + SentFailed + "');", true);
        }
        */
        //UpdatePanelVerifyResult.Update();
    }

    public string supplierGuid
    {
        get
        {
            return (ViewState["supplierGuid"] == null ? "" : (string)ViewState["supplierGuid"]);
        }

        set
        {
            ViewState["supplierGuid"] = value;
        }
    }
}