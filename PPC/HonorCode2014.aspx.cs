﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class PPC_HonorCode2014 : PageSetting
{
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        if (userManagement.IsPublisher())
            this.MasterPageFile = "../Publisher/MasterPageFrame.master";
        else if (siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
        {
            this.MasterPageFile = ResolveUrl("~") + "Controls/MasterPagePPC2.master";

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setValues();
        }

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        supplierGuid = GetGuidSetting();
        honorCodeValues(supplier, supplierGuid);
        //honorCodeValues(supplier, supplierGuid);
        clientIdBalance = this.Master.FindControl("lbl_Balance").ClientID;



        Page.Header.DataBind();

    }

    protected void setValues()
    {
        List<string> list = new List<string>();
        /*
        list.Add("We believe that you shouldn’t have to pay through the nose to grow your business.");
        list.Add("We believe that you should pay only for real leads.");
        list.Add("We believe in putting control in the hands of the advertiser.");
        list.Add("We believe in honesty and transparency.");
        list.Add("We believe that your time is better spent focusing on the work at hand rather than drumming up new business.");
        list.Add("We strive to bring you more customers and help you make money.");
        list.Add("We use superior technology to create a seamless and enjoyable experience for you.");
        list.Add("We believe in bringing you true value for your marketing dollars.");
        list.Add("We believe in open communication.");
        list.Add("We believe in listening to our advertisers and continuously evolving our service to meet your needs.");
        */
        list.Add("We believe in one big universe; as long as you are happy and grow your business, we will grow ours.");
        list.Add("We respect your time, and will never call you while you are off duty. In exchange, we expect you to be available while you are on duty.");
        list.Add("All your customers are basically good. You should treat them with respect and courtesy--the same way you want to be treated.");
        //list.Add("We know that you are honest, so we let you decide if leads are irrelevant and we automatically credit you for those calls.");
        list.Add("In terms of leads, there are busy periods and there are slow periods. Please be patient and know we are on your side.");
        listValues = list;
    }

    protected void honorCodeValues(WebReferenceSupplier.Supplier supplier, string supplierGuid)
    {
        WebReferenceSupplier.ResultOfHonorCodeReadingStatus resultOfHonorCodeReadingStatus;
        try
        {
            resultOfHonorCodeReadingStatus = supplier.GetHonorCodeReadingStatus(new Guid(supplierGuid));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            string _systemMessage = "Server problem. Try later";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "updateSystemMessage", "updateSystemMessage('" + _systemMessage + "',1);", true);
            return;
        }
        if (resultOfHonorCodeReadingStatus.Type == WebReferenceSupplier.eResultType.Failure)
        {
            string _systemMessage = "Server problem. Try later";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "updateSystemMessage", "updateSystemMessage('" + _systemMessage + "',1);", true);
            return;
        }
        int counter = resultOfHonorCodeReadingStatus.Value.Counter;       

        bool isClickedDone = resultOfHonorCodeReadingStatus.Value.IsClickedDone;

        if (isClickedDone)
            honorRibon.Visible = true;

        stepHonorValues.Controls.Clear();
        counter++;
        for (int i = 0; i < listValues.Count; i++)
        {

            HtmlGenericControl divStepHonorValue = new HtmlGenericControl("div");
            divStepHonorValue.Attributes.Add("class", "honorValue");

            //HtmlGenericControl spanStepHonorCounter = new HtmlGenericControl("span");
            //spanStepHonorCounter.InnerText = (i).ToString();
            //spanStepHonorCounter.Attributes.Add("class", "honorCounter");



            //divStepHonorValue.Controls.Add(spanStepHonorCounter);

            HtmlGenericControl spanValue = new HtmlGenericControl("div");


            divStepHonorValue.Controls.Add(spanValue);

            HtmlGenericControl spanValueInner = new HtmlGenericControl("div");
            spanValue.Controls.Add(spanValueInner);

            if (i < counter)
            {

                spanValueInner.InnerText = listValues[i];
                spanValueInner.Attributes.Add("class", "honorValueTextInner");

                spanValue.Attributes.Add("class", "honorValueText honorValueTextAfterReveal");
            }

            if (i == (counter))
            {
                LinkButton linkValue = new LinkButton();
                linkValue.Text = "Reveal next item...";
                linkValue.ID = "lbValue" + i;
                linkValue.CommandArgument = i.ToString();
                linkValue.Command += new CommandEventHandler(lbValue_Command);
                spanValueInner.Attributes.Add("class", "honorValueTextReveal");
                spanValueInner.Controls.Add(linkValue);

                spanValue.Attributes.Add("class", "honorValueText honorValueTextBeforeReveal");
            }

            stepHonorValues.Controls.Add(divStepHonorValue);


        }


    }

    protected void lbValue_Command(object sender, CommandEventArgs e)
    {

        LinkButton lb = (LinkButton)sender;
        lb.Enabled = false;

        string valueNumber = (string)e.CommandArgument;

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);

        WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
        result = supplier.HonorCodePlusOne(new Guid(supplierGuid));

        if (result.Type == WebReferenceSupplier.eResultType.Success)
        {
            honorCodeValues(supplier, supplierGuid);
            if (valueNumber == (listValues.Count - 1).ToString())
                btnDone_Click();
        }

        else
        {
            string systemMessage = "Server problem. Try later";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "updateSystemMessage", "updateSystemMessage('" + systemMessage + "',1);", true);
            lb.Enabled = true;
        }
    }

    /*
    public WebReferenceSupplier.Supplier supplier
    {
        get
        {
            return (ViewState["supplier"]==null ? null : (WebReferenceSupplier.Supplier)ViewState["supplier"]);           
        }

        set
        {
            ViewState["supplier"] = value;
        }
    }
    */

    protected void btnDone_Click()
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.GetStartedTaskType getStartedTaskType = WebReferenceSupplier.GetStartedTaskType.register;
        getStartedTaskType = WebReferenceSupplier.GetStartedTaskType.honor_code;

        //WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
        WebReferenceSupplier.ResultOfCrossOutGetStartedTaskResponse resultOfCrossOutGetStartedTaskResponse = new WebReferenceSupplier.ResultOfCrossOutGetStartedTaskResponse();
        resultOfCrossOutGetStartedTaskResponse = supplier.CrossOutGetStartedTask(new Guid(supplierGuid), getStartedTaskType);

        string systemMessage = "";

        if (resultOfCrossOutGetStartedTaskResponse.Type == WebReferenceSupplier.eResultType.Success)
        {
            honorCodeValues(supplier, supplierGuid);
            honorRibon.Visible = true;
            decimal balance = resultOfCrossOutGetStartedTaskResponse.Value.Balance;
            int tasksLeft = resultOfCrossOutGetStartedTaskResponse.Value.TasksLeft;
            int bonusAmount = resultOfCrossOutGetStartedTaskResponse.Value.BonusAmount;

            if (tasksLeft > 0)
            {
                /*
                systemMessage = "Thank you for reading our honor code. You are now only $link$ from getting $" + bonusAmount + " worth of leads.";

                string _script = "top.FirstUpdate('" + systemMessage + "', '" + ResolveUrl("~/Management/overview2.aspx") + "', '" + tasksLeft + ((tasksLeft == 1) ? " step" : " steps") + " away');";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "FirstUpdate", _script, true);
                */

            }
            else
            {
                /*
                systemMessage = "Hero! You righteously earned $" + bonusAmount + " to spend getting phone leads!";

                string strBalance = string.Format(NUMBER_FORMAT, balance);
                Session["balance"] = strBalance;

               
                string _script = "top.FirstUpdate('" + systemMessage + "', '" + ResolveUrl("~/Management/overview2.aspx") + "', '" + tasksLeft + " steps away');allTasksDone(" + bonusAmount + ");";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "FirstUpdate", _script, true);
                */

            }
        }

        else
        {
            systemMessage = "Server problem. Try later";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "updateSystemMessage", "updateSystemMessage('" + systemMessage + "',1);", true);
        }
    }

    public string supplierGuid
    {
        get
        {
            return (ViewState["supplierGuid"] == null ? "" : (string)ViewState["supplierGuid"]);
        }

        set
        {
            ViewState["supplierGuid"] = value;
        }
    }

    public List<string> listValues
    {
        get
        {
            return (ViewState["listValues"] == null ? null : (List<string>)ViewState["listValues"]);
        }
        set
        {
            ViewState["listValues"] = value;
        }
    }

    protected string clientIdBalance
    {
        get { return (ViewState["clientIdBalance"] == null ? "" : (string)ViewState["clientIdBalance"]); }
        set { ViewState["clientIdBalance"] = value; }
    }



}