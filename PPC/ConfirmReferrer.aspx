﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPTemp.master" AutoEventWireup="true" CodeFile="ConfirmReferrer.aspx.cs" Inherits="PPC_ConfirmReferrer" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNPTemp.master" %>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript">
    $(function () {
        var _inrterval = setInterval(function () {
            var _html = $('#<%# lbl_seconds.ClientID %>').html();
            var sec = parseInt(_html);
            if (sec == 0) {
                $('.div_confirm_right').addClass('step-2');
                clearInterval(_inrterval);
                setTimeout(function () {
                    $('#mobile_active').show();
                    $('#mobile_waiting').hide();
                }, 300);
                return;
            }
            sec--;
            $('#<%# lbl_seconds.ClientID %>').html(sec);
        }, 1000);
    });
    function ResendCode() {
        var phone = $('#<%# hf_Phone.ClientID %>').val();
        $.ajax({
            url: "<%# GetSendMobileService %>",
            data: "{}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                switch (data.d) {
                    case ('false'):
                    case ('faild'):
                        SetServerComment('<%# GetServerErrorSendToPhone %>');
                        break;
                    case ('true'):
                        SetServerComment('<%# PasswordSent %>');
                        break;

                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                GeneralServerError();
            }

        });
    }
    function ChangeNum() {
        $.ajax({
            url: "<%# ChangePhoneInRegistration %>",
            data: "{}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                switch (data.d) {
                    case ('faild'):
                        GeneralServerError();
                        break;
                    case ('success'):
                        window.location.href = '<%# GetChangeNumDestination %>';
                        break;

                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                GeneralServerError();
            }

        });
    }
    $(function () {
        $('#<%# txt_password.ClientID %>').keyup(function (e) {
            var keynum;
            if (window.event) // IE            
                keynum = e.keyCode
            else if (e.which) // Netscape/Firefox/Opera            
                keynum = e.which
            if (keynum != 13)
                return true;
            SendCode();
        });
    });
    function SendCode() {
        var _code = $.trim($('#<%# txt_password.ClientID %>').val());
        $('#<%# txt_password.ClientID %>').val(_code);
        $('span.btn_next').show();
        $('a.btn_next').hide();
        $.ajax({
            url: "<%# ApprovePassword %>",
            data: "{'password': '" + escape(_code) + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var result = eval('(' + data.d + ')');
                switch (result.status) {
                    case ('error'):
                        $('span.btn_next').hide();
                        $('a.btn_next').show();
                        GeneralServerError();
                        break;
                    case ('false'):
                        $('span.btn_next').hide();
                        $('a.btn_next').show();
                        SetServerComment('<%# WrongCode %>');
                        break;
                    case ('true'):
                        //window.location.href = '<%# ServicePage %>';
                        window.location.href = '<%# PlansPage %>';                        
                        break;
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('span.btn_next').hide();
                $('a.btn_next').show();
                GeneralServerError();
            }
        });
    }
    function GetPasswordByPhone() {
        $('a.call-my-phone').hide();
        $('span.call-my-phone').show();
        $.ajax({
            url: "<%# GetPasswordByPhone %>",
            data: "{}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {

                switch (data.d) {
                    case ('false'):
                    case ('faild'):
                        $('a.call-my-phone').show();
                        $('span.call-my-phone').hide();
                        SetServerComment('<%# GetServerErrorSendToPhone %>');
                        break;
                    case ('true'):
                        window.location.href = 'ConfirmVoice.aspx';
                        break;
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('a.call-my-phone').show();
                $('span.call-my-phone').hide();
                GeneralServerError();
            }
        });
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="hf_Phone" runat="server" />
<div class="ConfirmReferrer">
   
    <div class="titleFirst">
        <span>Confirm Your Phone Number</span>
    </div>
    <div class="div_confirm_left">
        <h3>by Text Message</h3>
        <div class="titleSecond emailTitle"><asp:Label ID="lbl_phone" runat="server" Text="My password is..."></asp:Label></div>

        <div class="inputParent">
            <input id="txt_password" type="password" class="textActive" runat="server" HolderClass="place-holder" />

           <div class="inputValid" style="display:none;"  id="div_ReferreSmile">
                
            </div>
            <div class="inputError" style="display:none;"  id="div_ReferreError">
               
            </div>
        
            
        </div>
         <div>
            <a href="javascript:SendCode();" button-mode="phone" class="btn_next">Next ></a>
            <span class="btn_next" style="display:none;">Processing...</span>
        </div>
        <div class="didnt-get">
            <span class="title-didnt-get">Didn’t get the text message?</span>
            <span class="-block">
                Is your mobile number 
                <asp:Label ID="lbl_ClientPhone" runat="server" ></asp:Label>?
            </span>
            <ul>
                <li>No, I need to <a class="general" id="a_ChangeNum" href="javascript:void(0);" onclick="ChangeNum();">change the number</a></li>
                <li>Yes, please <a href="javascript:ResendCode();" class="general">resend</a> the text.</li>
            </ul>
        
        </div>
    </div>
    <div class="div_confirm_right">
        <h3>by Phone Call</h3>
        <div id="mobile_waiting" >
            <div>
                <span>
                    <span class="mobile_waiting_light">If you aren’t using a mobile,</span> we can
                    verify your number by calling your
                    landline. Wait <asp:Label ID="lbl_seconds" runat="server" Text="120" CssClass="counter-calling"></asp:Label> seconds and
                    then click the button below
                </span>
            </div>
            <div id="div_callMyPhone" class="div_callMyPhone">
                <span class="call-my-phone-disable">Call My Phone</span>
            </div>
        </div>
        <div id="mobile_active" style="display:none;">
            <div>
                <span>
                    <span class="mobile_waiting_light">By clicking</span> “Call My Phone,” <span class="mobile_waiting_light">I authorize NoProblem to make this call to my number.</span>
                </span>
            </div>
            <div class="div-call-my-phone">
                <a href="javascript:GetPasswordByPhone();" class="call-my-phone">Call My Phone</a>
                <span class="call-my-phone" style="display:none;">Processing</span>
            </div>
            <div>
                <span class="mobile_waiting_light_13">Our automated system will call you and read a password.</span>
            </div>
        </div>
        <div class="shadowUnder288"></div>
    </div>
</div>
</asp:Content>

