﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Controls/MasterPageNP2.master" CodeFile="PpcLoginMobile.aspx.cs" Inherits="PPC_PpcLoginMobile"  %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNP2.master" %>
<%@ Register Src="~/Controls/PpcComment.ascx" TagName="PpcComment" TagPrefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="../scripts/US-Phone.js"></script>
<script type="text/javascript" src="../PlaceHolder.js"></script>

<script type="text/javascript">
     var _phone = new np_phone('<%# GetPhoneExpresion %>');
    _phone.init();
    _phone.OnKeyUpEmpty = function (txtId) {
        ClearPhoneValidate();
    };
    _phone.OnMatch = function (txtId) {
        ClearPhoneValidate();
        $('#MobileValid').show();
        $('#<%# txt_Mobile.ClientID %>').addClass('textValid');
    };
    _phone.OnKeyUpOnTheWay = function (txtId) {
        ClearPhoneValidate();
    };
    _phone.OnFaild = function (txtId) {
        ClearPhoneValidate();
        $('#MobileInstruction').show();
        $('#MobileError').show();
        $('#<%# txt_Mobile.ClientID %>').addClass('textError');
    };
    _phone.OnClickFaild = function (txtId) {
        _phone.OnFaild(txtId);
        return false;
    };
    _phone.OnEmpty = function (txtId) {
        ClearPhoneValidate();

    };
    _phone.OnFocus = function (txtId) {
        ClearPhoneValidate();
    };
    _phone.OnClickMatch = function (txtId) {
        
        return true;
    };
   
    function ClearPhoneValidate() {
        $('#MobileValid').hide();
        $('#MobileError').hide();
        $('#MobileInstruction').hide();
        $('#<%# txt_Mobile.ClientID %>').removeClass('textError');
        $('#<%# txt_Mobile.ClientID %>').removeClass('textValid');
    }                       
    
    /******* Password ****/
    $(function () {
        var $txt = $('#<%# txt_Password.ClientID %>');
        $txt.keyup(function (e) {
            var keynum;
            if (window.event) // IE            
                keynum = e.keyCode
            else if (e.which) // Netscape/Firefox/Opera            
                keynum = e.which
            if (keynum == 13) {
                if(CheckDetails())
                      <%# GetPostBackEnter() %>;
                return;
            }
            if (CeckPassword())
                PasswordValid();
            else
                PasswordInvalid();
        });
        $txt.focus(function () {
            ClearPasswordValidation();
        });
        $txt.blur(function () {
            if (CeckPassword())
                PasswordValid();
            else
                ClearPasswordValidation();
        });
    });
    function CeckPassword() {
        if ($('#<%# txt_Password.ClientID %>').val().length > 0)
            return true;
        return false;
    }
    function PasswordValid() {
        ClearPasswordValidation();
        $('#PasswordValid').show();
        $('#<%# txt_Password.ClientID %>').addClass("textValid");
    }
    function PasswordInvalid() {
        ClearPasswordValidation();
        $('#PasswordError').show();
        $('#<%# txt_Password.ClientID %>').addClass("textError");
    }
    function ClearPasswordValidation(){
        $('#PasswordValid').hide();
        $('#PasswordError').hide();
        $('#<%# txt_Password.ClientID %>').removeClass("textError textValid");    
    }

    /******/
    function CheckDetails() {
        if (CeckPassword() && _phone.check_phone()) {
            //$('span.btn_next').show();
            //$('#<%# btn_Login.ClientID %>').hide();
            activeButtonAnim('.progress-demo #<%#btn_Login.ClientID%>', 200);
            return true;
        }
        var IsMulti = false;
        if (!CeckPassword()){
            PasswordInvalid();
            IsMulti = true;
            SetServerComment('<%# PasswordMissing %>');
        }
        if(!_phone.check_phone()){
            if(IsMulti)
                SetServerComment('<%# MissingMultiFields %>');
            else
                SetServerComment('<%# PhoneInvalid %>');
        }

        
        return false;
    }


    function backToLogin() {
 //       SetPlaceHolder();
        document.getElementById("div_login_faild").style.display = "none";
        document.getElementById("div_login").style.display = "block";
    }
    function LoginNotFound() {
        document.getElementById("div_login_faild").style.display = "block";
        document.getElementById("div_login").style.display = "none";
    }
    function RedirectTo(_path) {
        window.location = _path;
    }


   
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<%--
    <div class="main_login">
<div id="wrapadv">
    <div class="titleFirst titleLogin">
        <span class="TitleRight">Log in to your account</span>
        <div class="titleRight"><span>Don't have an account? <a href="Register.aspx" class="general">Register</a></span></div>
    </div>
	<div class="clear"></div>
    
    
    <div id="div_login">
        

        <div class="regoform" >
		    <label>My mobile number is...</label>
 		    <div class="inputform">
 		       <div class="inputParent">          
                    <div class="inputSun">
                        <input type="text" id="txt_Mobile"  runat="server"/>
                    </div>
                    <div class="inputValid" style="display:none;"  id="MobileValid">
                
                    </div>
                    <div class="inputError" id="MobileError" style="display:none;">
          
                    </div>
                          
                    <div class="inputComment" id="MobileInstruction" style="display:none;">
                        <asp:Literal ID="lt_PhoneInstruction" runat="server"></asp:Literal>
                            <div class="chat-bubble-arrow-border"></div>
                            <div class="chat-bubble-arrow"></div>
                    </div>
                
                </div>         
            </div>
		
		    <label>My password is...</label>
 		    <div class="inputform">
 		       <div class="inputParent">          
                    <div class="inputSun">
                        <asp:TextBox ID="txt_Password" runat="server" TextMode="Password"></asp:TextBox>
                         
                    </div>

                    <div class="inputValid" style="display:none;"  id="PasswordValid">
                
                    </div>
                    <div class="inputError" id="PasswordError" style="display:none;">
          
                    </div>
                          
                    <div class="inputComment" id="PasswordInstruction" style="display:none;">
                        
                            <div class="chat-bubble-arrow-border"></div>
                            <div class="chat-bubble-arrow"></div>
                    </div>
                    
                
                </div>         
            
            </div>
           
            <div class="div_btn_Login">
                <asp:LinkButton ID="btn_Login" runat="server" class="btn_next" 
                    onclick="btn_Login_Click" OnClientClick="return CheckDetails();">Login</asp:LinkButton>
                <span class="btn_next" style="display: none;">Process...</span>
  		    </div>
            <div class="div_Forgot_Password">
                <a href="PasswordReminder.aspx" class="general">Forgot password?</a>
            </div>
            <div class="clear"></div>
            <asp:CheckBox ID="cb_RememberMe" runat="server" Text="Remember me"  CssClass="rememberCheckbox"/>
	    </div>
    </div>
    
</div>
</div>
--%>





<div class="registration loginMobile">
   

    <div class="registrationLeft">

        <div class="titleFirst">
            <span>Login</span>
        </div>           

       <div class="clear"></div>          

       <div class="registrationInputs" >    
               
        <div id="wrapadv">
    
	        <div class="clear"></div>
    
    
            <div id="div_login">
        

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div class="regoform" >
		    
 		    <div class="inputform">
               <label>My mobile number is...</label>
 		       <div class="inputParent">          
                    
                    <input type="text" id="txt_Mobile"  runat="server" />
                    
                    <div class="inputValid" style="display:none;"  id="MobileValid">
                
                    </div>
                    <div class="inputError" id="MobileError" style="display:none;">
          
                    </div>
                          
                    <div class="inputComment" id="MobileInstruction" style="display:none;">
                        <asp:Literal ID="lt_PhoneInstruction" runat="server"></asp:Literal>
                            <div class="chat-bubble-arrow-border"></div>
                            <div class="chat-bubble-arrow"></div>
                    </div>
                
                </div>         
            </div>
		
		    
 		    <div class="inputform">
                <label>My password is...</label>
 		       <div class="inputParent">          
                   
                    <asp:TextBox ID="txt_Password" runat="server" TextMode="Password" class="textActive"></asp:TextBox>
                         
                    

                    <div class="inputValid" style="display:none;"  id="PasswordValid">
                
                    </div>
                    <div class="inputError" id="PasswordError" style="display:none;">
          
                    </div>
                          
                    <div class="inputComment" id="PasswordInstruction" style="display:none;">
                        
                            <div class="chat-bubble-arrow-border"></div>
                            <div class="chat-bubble-arrow"></div>
                    </div>
                    
                
                </div>         
            
            </div>
           
            <div class="div_btn_Login">
                <a href="PasswordReminder.aspx" >Forgot password?</a>
                <!--
                <asp:CheckBox ID="cb_RememberMe" runat="server" Text="Remember me"  CssClass="rememberCheckbox"/>
                -->
                
                <section class="progress-demo">
                <asp:LinkButton ID="btn_Login" runat="server" class="ladda-button" data-style="expand-right" data-color="blue" button-mode="phone" 
                    onclick="btn_Login_Click" OnClientClick="return CheckDetails();"><span class="ladda-label">Login</span><span class="ladda-spinner">
                  </span><div class="ladda-progress" style="width: 138px;"></div></asp:LinkButton>
                </section>

                <span class="btn_next" style="display: none;">Process...</span>
  		    </div>

            <div class="div_Forgot_Password">
                
            </div>
            <div class="clear"></div>
            
	    </div>
            </div>
    
        </div>

      
        </div>
    
    </div>
   
    <div class="registrationRight">
        <div class="loginMobileContent">
            <div class="loginMobileContentTitle">NoProblem got better! What’s new?</div>
            
            <div class="loginMobileContentPoints">

                <div class="loginMobileContentUnit">
                <div class="loginMobileContentIcon">
                    <img src="images/pay.jpg" />
                </div>

                <div class="loginMobileContentPoint">
                    <div class="loginMobileContentPoint1">Pay only for leads you accept</div>
                    <div class="loginMobileContentPoint2">
                         We wont charge you when you are not
                         <br />
                        available to accept the lead. 
                    </div>                   
                </div>
                </div>

                <div class="clear"></div>

                <div class="loginMobileContentUnit">
                <div class="loginMobileContentIcon">
                    <img src="images/50.jpg" />
                </div>

                <div class="loginMobileContentPoint">
                    <div class="loginMobileContentPoint1">No need to purchase credit anymore</div>
                    <div class="loginMobileContentPoint2">
                        You pay per each lead you accept, your auto-
                        <br />
                        recharge has been canceled.
                    </div>                   
                </div>
                </div>

                <div class="clear"></div>

                <div class="loginMobileContentUnit">
                <div class="loginMobileContentIcon">
                    <img src="images/cover-area.jpg" />
                </div>

                <div class="loginMobileContentPoint loginMobileContentPointOneLine">
                    <div class="loginMobileContentPoint1">Multiple cover area</div>
                    <div class="loginMobileContentPoint2 ">
                        Be available on more than one location.                        
                    </div>                   
                </div>
                </div>

                <div class="clear"></div>

                <div class="loginMobileContentUnit">
                <div class="loginMobileContentIcon">
                    <img src="images/hours.jpg" />
                </div>

                <div class="loginMobileContentPoint loginMobileContentPointOneLine">
                    <div class="loginMobileContentPoint1">Working hours enhancements</div>
                    <div class="loginMobileContentPoint2 ">
                        You can now split your day into shifts.                       
                    </div>                   
                </div>
                </div>

                <div class="clear"></div>

                <div class="loginMobileContentUnit">
                <div class="loginMobileContentIcon">
                    <img src="images/business-page.jpg" />
                </div>

                <div class="loginMobileContentPoint loginMobileContentPointThreeLine">
                    <div class="loginMobileContentPoint1">Premium business page</div>
                    <div class="loginMobileContentPoint2 ">
                        Appear in NoProblem revamped directory for free:
                        <br />
                        featured exposure, no competitor ads, link to your
                        <br />
                        website, photo and video gallery and more.                      
                    </div>                   
                </div>
                </div>

            </div>

            


        </div>
        
    </div>   
   
       
</div>



</asp:Content>



