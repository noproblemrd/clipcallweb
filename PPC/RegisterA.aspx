﻿<%@ Page Language="C#" MasterPageFile="~/Controls/MasterPageNP2.master" AutoEventWireup="true" CodeFile="RegisterA.aspx.cs" Inherits="PPC_RegisterA" ClientIDMode="Static" %>
<%@ Register Src="~/Controls/socialNetworks.ascx" TagName="socialNetworks" TagPrefix="Registration" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript" src="../management/swfobject.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>

<!--[if IE 7]>      
      <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome-ie7.min.css" />
    <![endif]-->    
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="homeheaderLandingPage" class="homeheaderLandingPage" runat="server" visible="true">
            <a  class="_logo"></a>  
    </div>


    <Registration:socialNetworks  ID="socialNetworks1" runat="server"/>

    <asp:PlaceHolder runat="server" ID="PlaceHolderFlavor"></asp:PlaceHolder>


</asp:Content>
