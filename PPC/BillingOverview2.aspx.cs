﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;

public partial class PPC_BillingOverview2 : PageSetting
{
    protected decimal fee;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadPageText();
            LoadFee();
            LoadPlan();
            LoadDetails();        
            LoadActivity();
        }
        
        Page.DataBind();
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

    }


    protected void LoadFee()
    {

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfSubscriptionCostAndBonus result = new WebReferenceSupplier.ResultOfSubscriptionCostAndBonus();
        try
        {
            result = _supplier.GetSubscriptionPlanCost();
            fee = result.Value.Cost;          

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
            return;
        }



    }

    protected void LoadPlan()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfString resultOfString = new WebReferenceSupplier.ResultOfString();

        try
        {
            resultOfString = _supplier.Checkout_GetPlan_Registration2014(new Guid(GetGuidSetting()));
            plan = resultOfString.Value;
            //plan = "FEATURED_LISTING";
            if (plan == "FEATURED_LISTING") // LEAD_BOOSTER_AND_FEATURED_LISTING , FEATURED_LISTING
            {
                lbl_plan.Text = "Featured Listing";
                tr_LeadBooster.Visible = false;
                tr_LeadBooster2.Visible = false;
                tr_LeadBooster3.Visible = false;
                tr_FeaturedListing3.Visible = false;
                totalPaymentSum.InnerHtml = "$" + fee;
                lbl_MonthlyFee.Text = fee.ToString();
                
            }
            
            else if(plan=="LEAD_BOOSTER_AND_FEATURED_LISTING")
            {
                lbl_plan.Text = "Lead Booster";
                totalPaymentSum.InnerHtml = "$0.00";
                lbl_MonthlyFee.Text = fee.ToString();
                lbl_discount.Text = fee.ToString();
            }

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
        }

    }

    private void LoadDetails()
    {        
        lbl_month.Text = string.Format("{0:MMMM yyyy}", DateTime.Now);
    }

    private void LoadActivity()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfGetAccountActivityResponse2014 result;
        try
        {
            result = _supplier.GetAccountActivity_Registration2014(new Guid(GetGuidSetting()));           
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "serverError();", true);
            return;
        }

        lbl_LeadsAmount.Text = result.Value.LeadCount.ToString();
        lbl_LeadsSpent.Text = GetMoneyToDisplay(result.Value.LeadMoney);

        if (lbl_LeadsAmount.Text == "0")
            tr_Leads.Visible = false;

        lbl_RefundsAmount.Text = result.Value.RefundCount.ToString();
        lbl_RefundsSpent.Text = GetMoneyToDisplay(result.Value.RefundMoney);

        if (lbl_RefundsAmount.Text == "0")
            tr_Refunds.Visible = false;

        lbl_MonthlyFeeSpent.Text = GetMoneyToDisplay(result.Value.MonthlyFee);
        lbl_MonthlyFeeEarned.Text = GetMoneyToDisplay(result.Value.MonthlyFeeRefund);        

        lbl_TenLeadsBonusAmount.Text=result.Value.TenLeadsBonusCount.ToString();
        lbl_TenLeadsBonusEarned.Text=GetMoneyToDisplay(result.Value.TenLeadsBonusMoney);

        if (lbl_TenLeadsBonusAmount.Text == "0")
            tr_TenLeadsBonus.Visible = false;

        lbl_friendsAmount.Text = result.Value.EarnedFromFriendCount.ToString();
        lbl_friendsEarned.Text = GetMoneyToDisplay(result.Value.EarnedFromFriendMoney);

        if (lbl_friendsAmount.Text == "0")
            tr_Friends.Visible = false;

        lbl_BonusCreditAmount.Text = result.Value.GetStartedBonusCount.ToString();
        lbl_BonusCreditEarned.Text = GetMoneyToDisplay(result.Value.GetStartedBonusMoney);

        if (lbl_BonusCreditAmount.Text == "0")
            tr_BonusCredit.Visible = false;

        lbl_TotalSpent.Text = GetMoneyToDisplay(result.Value.TotalUsed);
        lbl_BonusEarnedEarned.Text = GetMoneyToDisplay(result.Value.TotalBonus);
              


    }
    
    

    
   
    protected void lb_plan_Click(object sender, EventArgs e)
    {
        Package _pack = GetPackageByType(((LinkButton)sender).CommandArgument);
        if (_pack == null)
        {
            Update_Faild();
            return;
        }
        string __form = AddCreditSales.BuyCredit(_pack, siteSetting, new Guid(GetGuidSetting()));
        Response.Write(__form);
        Response.Flush();
        Context.ApplicationInstance.CompleteRequest();
    }

    private Package GetPackageByType(string _type)
    {
        foreach (Package p in PackageV)
        {
            if (p.Name == _type)
                return p;
        }
        return null;
    }

    List<Package> PackageV
    {
        get { return (Session["Package"] == null) ? null : (List<Package>)Session["Package"]; }
        set { Session["Package"] = value; }
    }

    protected string plan
    {
        get
        {
            return (ViewState["plan"] != null) ? ViewState["plan"].ToString() : "";
        }

        set
        {
            ViewState["plan"] = value;
        }

    }

    protected string GetServerError
    {
        get { return (ViewState["GetServerError"] == null) ? " " : ViewState["GetServerError"].ToString(); }
        set { ViewState["GetServerError"] = value; }
    }
}