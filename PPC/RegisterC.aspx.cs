﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;


public partial class PPC_RegisterC : RegisterPage
{
    public string blnLandingpage;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            string registerFormat;
            registerFormat = (Request["format"] == null) ? "" : Request["format"].ToString();

            bool landingPage;

            if (registerFormat.ToLower() == "landingpage")
                landingPage = true;
            else
                landingPage = false;

            if (landingPage)
                homeheaderLandingPage.Visible = true;
            else
                homeheaderLandingPage.Visible = false;

            LoadFlavor();

            LoadMaster();
        }
        //LoadMaster();
        
        //txt_referrer.Attributes["place_holder"] = "The code from the email invitation you received";
              
        Header.DataBind();
    }


    protected void LoadFlavor()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;        
        UserControl ctlFlavor;       
       
        ctlFlavor = (UserControl)LoadControl("~/Controls/Registration/RegisterC.ascx");
        socialNetworks1.LandingPage = WebReferenceSupplier.eAdvertiserRegistrationLandingPageType.C;               

        Session["flavorRegistration"] = socialNetworks1.LandingPage;

        masterPage.setRibbon2(socialNetworks1.LandingPage);

        PlaceHolderFlavor.Controls.Add(ctlFlavor);
    }



    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        //masterPage.SetContentPaddingBottom("100px");
        //masterPage.SetStatusBar("statusBar1");

    } 

    

    protected string RegisterReferrer
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/SupplierReferrerInvitation"); }
    }
}