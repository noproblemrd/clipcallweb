﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Data.SqlClient;

public partial class PPC_AddCredit : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _AddCredit.BuyCredit += new EventHandler(_AddCredit_BuyCredit);
        _AddCredit.SetCreditDetails(siteSetting.GetUrlWebReference, new Guid(GetGuidSetting()), NUMBER_FORMAT);
        if (!IsPostBack)
        {
            RemoveServerCommentPPC();
        }
        Page.Header.DataBind();
    }

   

    void _AddCredit_BuyCredit(object sender, EventArgs e)
    {
        EventArgsBuyCredit _BuyCredit = (EventArgsBuyCredit)e;
        Package _pack = new Package(_BuyCredit);
        string __form = AddCreditSales.BuyCredit(_pack, siteSetting, new Guid(GetGuidSetting()));
        Response.Write(__form);
        Response.Flush();
        Context.ApplicationInstance.CompleteRequest();
    }

}