﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_ConfirmVoice : RegisterPage, IsInProcess
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadPageText();
            LoadDetails();
        }
        txt_password.Attributes["place_holder"] = PasswordPlaceHplder;
        Header.DataBind();
    }

    private void LoadDetails()
    {
        lbl_ClientPhone2.Text = PhoneNumDisplay;
        lbl_ClientPhone.Text = PhoneNumDisplay;
        Master.SetStatusBar("statusBar1");
    }
    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;
        //    _map.FieldMissing = FieldMissing;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "DetailsService"
                          select x).FirstOrDefault();

        WrongCode = xelem.Element("PasswordCode").Element("Validated").Element("Instruction").Element("WrongCode").Value;
        PasswordPlaceHplder = xelem.Element("PasswordCode").Element("Validated").Element("Instruction").Element("PlaceHolderVoice").Value;
        GetServerErrorSendToPhone = xelem.Element("MobilePhone").Element("Validated").Element("ServerError").Value;

        PasswordSent = xelem.Element("FormMessages").Element("PasswordSent").Value;
        xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                 where x.Attribute("name").Value == "DetailsService"
                 select x).FirstOrDefault();
        
        Master.SetBenefitFreeText("Get leads from ready-to-hire customers directly to your mobile phone.<br /><br />That's NoProblem!");
    }
    protected string PasswordPlaceHplder
    {
        get { return (string)ViewState["PasswordPlaceHplder"]; }
        set { ViewState["PasswordPlaceHplder"] = value; }
    }
    protected string GetServerError
    {
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }
    protected string GetChangeNumDestination
    {
        get { return (HasReferrer ? "RegisterReferrer.aspx" : "RequestInvitation.aspx"); }
    }
    protected string ChangePhoneInRegistration
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/ChangePhoneInRegistration"); }
    }
    protected string ServicePage
    {
        get { return ResolveUrl("~/PPC/Service.aspx"); }
    }

    protected string PlansPage
    {
        get { return ResolveUrl("~/PPC/Plans.aspx"); }
    }

    protected string ApprovePassword
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/Login"); }
    }
    protected string GetSendMobileService
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/SendSupplierSignUpNotification"); }
    }
    protected string WrongCode
    {
        get { return HttpUtility.JavaScriptStringEncode((string)ViewState["WrongCode"]); }
        set { ViewState["WrongCode"] = value; }
    }
    protected string PasswordSent
    {
        get { return (string)ViewState["PasswordSent"]; }
        set { ViewState["PasswordSent"] = value; }
    }
    protected string GetServerErrorSendToPhone
    {
        get { return HttpUtility.JavaScriptStringEncode((string)ViewState["GetServerErrorSendToPhone"]); }
        set { ViewState["GetServerErrorSendToPhone"] = value; }
    }
}