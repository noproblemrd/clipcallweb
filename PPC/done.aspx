﻿<%@ Page Language="C#" MasterPageFile="~/Controls/MasterPageNPTemp.master" AutoEventWireup="true" CodeFile="done.aspx.cs" Inherits="PPC_done" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNPTemp.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
<script type="text/javascript" src="script/jquery-ui-1.8.16.custom.autocomplete.min.js"></script>
<script type="text/javascript" src="../CallService.js"></script>
<script type="text/javascript" src="../general.js"></script>

<script type="text/javascript" src="script/JSGeneral.js"></script>

<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript" src="script/HistoryManager.js"></script>        
<link href="../Formal/jquery-ui-1.8.16.custom.css" type="text/css" rel="Stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <div runat="server" id="div_c" class="div_TopSpace">
   
        
        <div class="titleFirst">Thank you!</div>
        <div class="clear"></div>

        <div style="font-size: 16px;font-weight:600;padding-top:16px;width:922px;" >
            A sales representative will contact you shortly.
        </div>
       
    </div>

 
    <asp:Button ID="btn_virtual" runat="server" Text="Button" style="display:none;" />
   
</asp:Content>

