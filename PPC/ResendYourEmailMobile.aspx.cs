﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_ResendYourEmailMobile : RegisterPage2
{
    protected string myEmail;    

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            myEmail = Request["myEmail"];           

            registerEmail.InnerText = myEmail;
            
            LoadMaster();
        }

        Page.DataBind();
    }
  

    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

        masterPage.SetBlackMenu();

    }    

    
}