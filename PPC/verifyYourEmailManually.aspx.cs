﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.IO;

public partial class PPC_verifyYourEmailManually : RegisterPage2
{
    protected string EmailResend;
    

    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (!IsPostBack)
        {
            ifAuthorized(SupplierId);
            //ifAuthorized(new Guid("748BB4AB-4BB6-43E3-9646-BAC2452B2D40"));
            
            LoadPageText();

            string err = "";
            err = Request["error"];

            if (err != null && err.ToLower() == "resend")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "SetServerComment", "SetServerComment('" + EmailResend + "');", true);
            }

            string email = "";
            email = Request["myEmail"];            

            if (!string.IsNullOrEmpty(Request["myEmail"]))
                txt_email.Value = email;

            else
                LoadEmail();           


            txt_email.Attributes["place_holder"] = EmailPlaceHolder;

            LoadMaster();
            
            
        }

        
        
        
        

        Page.DataBind();
    }
        

    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

        masterPage.SetBlackMenu();
    }

    private void LoadEmail()
    {
        
        //public Result<EmailVerifiyLegacyPreLoadResponse> EmailVerifiyLegacyPreLoad_Legacy2014(Guid supplierId)

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfEmailVerifiyLegacyPreLoadResponse result;
        try
        {
            result = _supplier.EmailVerifiyLegacyPreLoad_Legacy2014(SupplierId);
            txt_email.Value=result.Value.Email;

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            
            return;
        }

    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;
        
        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "login"
                          select x).FirstOrDefault();


        EmailPlaceHolder = xelem.Element("Email").Element("Validated").Element("Instruction").Element("General").Value;
        
        EmailError = xelem.Element("Email").Element("Validated").Element("Error").Value;
        AlreadyExist = xelem.Element("Email").Element("Validated").Element("Instruction").Element("alreadyRegisteredByEmail2").Value;
        EmailResend = xelem.Element("Email").Element("Validated").Element("Instruction").Element("resendEmail").Value;
        SentFailed = xelem.Element("Email").Element("Validated").Element("SentFailed").Value;
    }


    private void ifAuthorized(Guid supplierId)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);

        WebReferenceSupplier.ResultOfEmailVerifiyLegacyPreLoadResponse resultOfEmailVerifiyLegacyPreLoadResponse = null;

        WebServiceSales webServiceSales = new WebServiceSales();

       

        try
        {
            resultOfEmailVerifiyLegacyPreLoadResponse = _supplier.EmailVerifiyLegacyPreLoad_Legacy2014(supplierId);
            if (resultOfEmailVerifiyLegacyPreLoadResponse.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed method EmailVerifiyLegacyPreLoad_Legacy2014 for guid: " + supplierId.ToString()));
            }

            else
            {
                string redirectPath = webServiceSales.legacyRedirectMap(supplierId, 
                    resultOfEmailVerifiyLegacyPreLoadResponse.Value.StepLegacy, resultOfEmailVerifiyLegacyPreLoadResponse.Value.Step,
                    resultOfEmailVerifiyLegacyPreLoadResponse.Value.LegacyStatus, resultOfEmailVerifiyLegacyPreLoadResponse.Value.Name, resultOfEmailVerifiyLegacyPreLoadResponse.Value.Balance);

                string urlPage = this.Request.Url.AbsolutePath;
                FileInfo fileInfo = new FileInfo(urlPage);

                if (fileInfo.Name.ToLower() != redirectPath.ToLower())
                    Response.Redirect(redirectPath.ToLower());

                
            }
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
        }

    }

    protected void btnSend_Click(object sender, EventArgs e)
    {       
        
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);

        WebServiceSales webServiceSales = new WebServiceSales();        

        try
        {
            WebReferenceSupplier.ResultOfSendVerificationEmailResponse resultOfSendVerificationEmailResponse = _supplier.SendVerificationEmail_Legacy2014(SupplierId, txt_email.Value);

            if (resultOfSendVerificationEmailResponse.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed method SendVerificationEmail_Legacy2014 for supplier " + SupplierId + " message: " + resultOfSendVerificationEmailResponse.Messages[0]));
                ClientScript.RegisterStartupScript(this.GetType(), "SetComment", @"stopActiveButtonAnim();SetServerComment(""" + GetServerError + @""");", true);
            }

            else
            {
                string SendEmailStatus = resultOfSendVerificationEmailResponse.Value.SendEmailStatus;                

                /*
                public static class SendEmailStatuses
                {
                    public const string SENT = "SENT";
                    public const string FAILED_TO_SEND = "FAILED_TO_SEND";
                    public const string DUPLICATE_EMAIL = "DUPLICATE_EMAIL";
                }
                */

                if (SendEmailStatus.ToUpper() == "SENT")
                {
                    Response.Redirect("ResendYourEmailMobile.aspx?myEmail=" + txt_email.Value);                    
                }
                else if (SendEmailStatus.ToUpper() == "FAILED_TO_SEND")
                    ClientScript.RegisterStartupScript(this.GetType(), "SetComment", @"stopActiveButtonAnim();SetServerComment(""" + SentFailed + @""");", true);
                else if (SendEmailStatus.ToUpper() == "DUPLICATE_EMAIL")
                    ClientScript.RegisterStartupScript(this.GetType(), "SetComment", @"stopActiveButtonAnim();SetServerComment(""" + HttpUtility.JavaScriptStringEncode(AlreadyExist) + @""");", true);

            }

        }

        catch(Exception ex)
        {
            dbug_log.ExceptionLog(ex,siteSetting);
            ClientScript.RegisterStartupScript(this.GetType(), "SetComment", @"stopActiveButtonAnim();SetServerComment(""" + GetServerError + @""");", true);

        }
        

        /*
        if(1==2)
        {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);

        WebServiceSales webServiceSales = new WebServiceSales();
        Guid SupllierId = (Guid)Session["RegisterUser2014"];

        WebReferenceSupplier.EmailVerifiedResponse emailVerifiedResponse = webServiceSales.verifyRegistrationEmailLegacy(SupllierId);


        if (emailVerifiedResponse == null)
            ClientScript.RegisterStartupScript(this.GetType(), "SetComment", @"stopActiveButtonAnim();SetServerComment(""" + GetServerError + @""");", true);
        else
        {

            string legacyStatus = emailVerifiedResponse.LegacyStatus;

            if (legacyStatus == "PHONE_PASSWORD_COMBINATION_NOT_FOUND")
            {
                //SetComment(HttpUtility.HtmlEncode(WrongPassword));
                //ClientScript.RegisterStartupScript(this.GetType(), "SetPasswordError", "PasswordInvalid();", true);
                return;
            }
               
            else if (legacyStatus == "MORE_THAN_ONE_ACCOUNT_FOUND_SEND_TO_EMAIL_LOGIN")
            {
                dbug_log.ExceptionLog(new Exception("failed login for mobile supplier guid: " + Session["RegisterUser2014"].ToString() +
                    " error: " + legacyStatus));

                Response.Redirect("ppcLogin.aspx");
            }

            else if (legacyStatus == "NOT_LEGACY_SEND_TO_EMAIL_LOGIN") // new suppliers plus old suppliers that not registered
            {
                dbug_log.ExceptionLog(new Exception("failed login for mobile supplier  guid: " + Session["RegisterUser2014"].ToString() +
                    " error: " + legacyStatus));

                Response.Redirect("ppcLogin.aspx");
            }

            else if (legacyStatus == "LEGACY_FINISHED_SEND_TO_EMAIL_LOGIN")
            {
                dbug_log.ExceptionLog(new Exception("failed login for mobile supplier  guid: " + Session["RegisterUser2014"].ToString() +
                    " error: " + legacyStatus));

                Response.Redirect("ppcLogin.aspx");
            }

            else if (legacyStatus == "SEND_TO_LEGACY_STEP")
            {
                string stepsLegacy;
                stepsLegacy = emailVerifiedResponse.StepLegacy;

                switch (stepsLegacy)
                {
                    case "1_CONFIRM_EMAIL":
                        Response.Redirect("verifyYourEmailManually.aspx");
                        break;

                    case "2_PAYMENT_METHOD":
                        Response.Redirect("paymentMobile.aspx");
                        break;

                    case "3_FINISHED":
                        Response.Redirect("../management/overview2.aspx");
                        break;

                    default:
                        break;

                }

            }


            else if (legacyStatus == "SEND_TO_NORMAL_STEP")
            {
                int step;
                step = emailVerifiedResponse.Step;

                switch (step)
                {

                    case 1:
                        //Response.Redirect("ResendYourEmail.aspx?id=" + _response.Value.SupplierId.ToString() + "&email=" + _response.Value. );                       
                        break;

                    case 2:
                        Response.Redirect("choosePlan.aspx");
                        break;

                    case 3:
                        Response.Redirect("businessMainDetails.aspx");
                        break;

                    case 4:
                        Response.Redirect("categories.aspx");
                        break;

                    case 5:
                        Response.Redirect("location2.aspx");
                        break;

                    case 6:
                        Response.Redirect("checkout.aspx");
                        break;

                    case 7:
                        Response.Redirect("checkout.aspx");
                        break;

                    default:
                        break;


                }

            }

        }
        
        }
        */

       
        
    }

    protected string EmailError
    {
        get { return (string)ViewState["EmailError"]; }
        set { ViewState["EmailError"] = value; }
    }

    protected string AlreadyExist
    {
        get { return (string)ViewState["AlreadyExist"]; }
        set { ViewState["AlreadyExist"] = value; }
    }

    protected string SentFailed
    {
        get { return (string)ViewState["SentFailed"]; }
        set { ViewState["SentFailed"] = value; }    }   

    
    protected string GetServerError
    {
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }

    protected string EmailPlaceHolder
    {
        get { return (string)ViewState["EmailPlaceHolder"]; }
        set { ViewState["EmailPlaceHolder"] = value; }
    }
}