﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="checkYourEmail.aspx.cs" Inherits="PPC_checkYourEmail" MasterPageFile="~/Management/MasterPageAdvertiser.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">

<div class="checkYourEmail">   

    <div class="titleFirst">
        <span>Check your email</span>
    </div>

    <div class="resendContent">
        <div class="resendContent1">
             Please check your inbox (<span id="registerEmail" runat="server"></span>) and click the link in the message to start getting actual
             <br />
             phone calls from potential customers in your area actively look for your services online!
        </div>
       
        <div class="resendContent2">
            If you don't see the email indoor inbox, make sure to check your junk mail folder
        </div>

        <div class="resendContent3">
           <span class="emphasize">Didn’t receive an email from us?</span>
            <br />
            We can <asp:LinkButton ID="LinkButtonVerify" runat="server" onclick="LinkButton1_Click">resend</asp:LinkButton> the email or contact us on <a href="mailto:support@noproblemppc.com">support@noproblemppc.com</a>.
           
        </div>       
    </div>

</div>
</asp:Content>

