﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.IO;

public partial class PPC_paymentLegacy : RegisterPage2//, IsInProcess
{
    protected decimal fee;

    protected void Page_Load(object sender, EventArgs e)
    {

        plan = "LEAD_BOOSTER_AND_FEATURED_LISTING";

        if (!IsPostBack)
        {
            ifAuthorized(SupplierId);
            //ifAuthorized(new Guid("748BB4AB-4BB6-43E3-9646-BAC2452B2D40"));
            LoadMaster();
            LoadPlan();
            LoadIframe();
        }
    }
   

    private void ifAuthorized(Guid supplierId)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);

        WebReferenceSupplier.ResultOfUpdatePaymentMethodResponse resultOfUpdatePaymentMethodResponse = null;

        WebServiceSales webServiceSales = new WebServiceSales();

        try
        {
            resultOfUpdatePaymentMethodResponse = _supplier.UpdatePaymentMethodOnLoad_Legacy2014(supplierId);
            if (resultOfUpdatePaymentMethodResponse.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed method UpdatePaymentMethodOnLoad_Legacy2014 for guid: " + supplierId.ToString()));
            }

            else
            {
                string redirectPath = webServiceSales.legacyRedirectMap(supplierId,
                    resultOfUpdatePaymentMethodResponse.Value.StepLegacy, resultOfUpdatePaymentMethodResponse.Value.Step,
                    resultOfUpdatePaymentMethodResponse.Value.LegacyStatus, resultOfUpdatePaymentMethodResponse.Value.Name, resultOfUpdatePaymentMethodResponse.Value.Balance);

                string urlPage = this.Request.Url.AbsolutePath;
                FileInfo fileInfo = new FileInfo(urlPage);

                if (fileInfo.Name.ToLower() != redirectPath.ToLower())
                    Response.Redirect(redirectPath.ToLower());
                
            }
        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
        }

    }

    private void LoadIframe()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfSubscriptionCostAndBonus result = new WebReferenceSupplier.ResultOfSubscriptionCostAndBonus();
        try
        {
            result = _supplier.GetSubscriptionPlanCost();
            fee = result.Value.Cost;

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);
            return;
        }

        
        string __form = AddCreditSales.BuyCredit2(0, siteSetting, SupplierId, Convert.ToInt32(fee), plan, true,"monthlyFeePayment",0);
        iframePayment.Attributes["src"] = __form;
        
    }


    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();
        
        masterPage.SetBlackMenu();

    }

    protected void LoadPlan()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.SetPlanType(plan);

    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

    }

    protected string plan
    {
        get
        {
            return (ViewState["plan"] != null) ? ViewState["plan"].ToString() : "";
        }

        set
        {
            ViewState["plan"] = value;
        }

    }

    protected string GetServerError
    {
        get { return (ViewState["GetServerError"] == null) ? " " : ViewState["GetServerError"].ToString(); }
        set { ViewState["GetServerError"] = value; }
    }
}