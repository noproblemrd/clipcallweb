﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_RequestInvitation : RegisterPage
{
    bool _SaveEmail = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            _SaveEmail = (Request.QueryString["changephone"] == "true" && !string.IsNullOrEmpty(email));
            ClearDetails(_SaveEmail);
            if (_SaveEmail)
            {
                txt_email.Value = email;
                hf_email.Value = email;
                ClientScript.RegisterStartupScript(this.GetType(), "ToPhoneNum", "ToPhoneNum();", true);
                
            }

            Form.Action = Request.Url.AbsolutePath + "#p";
            LoadPageText();            
        }
        LoadMaster();
        txt_email.Attributes["place_holder"] = EmailPlaceHplder;
        txt_email.Attributes["text-mode"] = "email";
        txt_phone.Attributes["place_holder"] = PhonePlaceHplder;
    //    GeneralServerError();
        Header.DataBind();
    }

    private void LoadMaster()
    {
        Master.SetStatusBar("statusBar1");
    }
    protected string ForwardBackScript()
    {
        if (!_SaveEmail)
            return string.Empty;
        string _script = "window.location.hash='p';";
        return _script;
    }
    private void LoadPageText()
    {
        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "Location"
                          select x).FirstOrDefault();

        FieldMissing = xelem.Element("BusinessName").Element("Validated").Element("Missing").Value;
        XElement t_element = xelem.Element("BusinessAddress").Element("Validated");

        t_element = xelem.Element("Email").Element("Validated");
        EmailPlaceHplder = t_element.Element("Instruction").Element("General").Value;

        InvalidEmail = HttpUtility.JavaScriptStringEncode(t_element.Element("Error").Value);
        EmailMissing = t_element.Element("Missing").Value;
        
        xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "DetailsService"
                          select x).FirstOrDefault();
        PhonePlaceHplder = xelem.Element("MobilePhone").Element("Validated").Element("Instruction").Element("General").Value;
        XElement xel = xelem.Element("FormMessages").Element("EmailDuplicate");
        ServerMessage sm = new ServerMessage(xel);
        EmailDuplicate = HttpUtility.JavaScriptStringEncode(sm.GetHtmlMessage("javascript:BackToEmailHash();", ResolveUrl("~/PPC/PpcLogin.aspx")));

        InvalidMobile = xelem.Element("MobilePhone").Element("Validated").Element("Error").Value;
        MissingMobile = xelem.Element("MobilePhone").Element("Validated").Element("Missing").Value;
        Master.SetBenefitFreeText("Get leads from ready-to-hire customers directly to your mobile phone.<br /><br />That's NoProblem!");
    }
    protected string InvalidEmail
    {
        get { return (string)ViewState["InvalidEmail"]; }
        set { ViewState["InvalidEmail"] = value; }
    }
    protected string InvalidMobile
    {
        get { return
            HttpUtility.JavaScriptStringEncode((string)ViewState["InvalidMobile"]); }
        set { ViewState["InvalidMobile"] = value; }
    }
    protected string MissingMobile
    {
        get { return 
            HttpUtility.JavaScriptStringEncode((string)ViewState["MissingMobile"]); }
        set { ViewState["MissingMobile"] = value; }
    }

    
    protected string FieldMissing
    {
        get { return (string)ViewState["FieldMissing"]; }
        set { ViewState["FieldMissing"] = value; }
    }
    protected string EmailMissing
    {
        get { return (string)ViewState["EmailMissing"]; }
        set { ViewState["EmailMissing"] = value; }
    }
    protected string EmailPlaceHplder
    {
        get { return (string)ViewState["EmailPlaceHplder"]; }
        set { ViewState["EmailPlaceHplder"] = value; }
    }
    protected string PhonePlaceHplder
    {
        get { return (string)ViewState["PhonePlaceHplder"]; }
        set { ViewState["PhonePlaceHplder"] = value; }
    }
    protected string EmailDuplicate
    {
        get { return (string)ViewState["EmailDuplicate"]; }
        set { ViewState["EmailDuplicate"] = value; }
    }
    protected string PhoneRegExpression
    {
        get { return Utilities.RegularExpressionForJavascript(PpcSite.GetCurrent().PhoneReg); }
    }
    protected string CheckEmailInRegistrationRequest
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/CheckEmailInRegistrationRequest"); }
    }
    protected string SupplierRequestInvitation
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/SupplierRequestInvitation"); }
    }
    protected string login
    {
        get { return ResolveUrl("~/PPC/PpcLogin.aspx"); }
    }
    protected string ConfirmPhone
    {
        get { return ResolveUrl("~/PPC/ConfirmReferrer.aspx"); }
    }
}