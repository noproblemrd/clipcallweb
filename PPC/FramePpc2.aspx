﻿<%@ Page Language="C#" MasterPageFile="~/Controls/MasterPagePPC2.master" AutoEventWireup="true" CodeFile="FramePpc2.aspx.cs" Inherits="PPC_FramePpc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script type="text/javascript">

    var arrStepsNames = new Array();



    function SentenceInit() {
        arrStepsNames[0] = "";
        arrStepsNames[1] = "<%# Get_GeneralInfo %>";
        arrStepsNames[2] = "<%#GetSegmentsArea%>";
        arrStepsNames[3] = "<%#GetCities%>";
        arrStepsNames[4] = "<%#Get_Time%>";
        arrStepsNames[5] = "<%#Get_AccountSettings%>";
        //arrStepsNames[5] = "<%#Get_MyCredits%>";
        //      arrStepsNames[6]= "<%#GetPaymentInfo%>"; 
    }

    function witchAlreadyStep(level) {
        //alert("level:" + level);
        if (arrStepsNames.length == 0)
            SentenceInit();
        var steps = document.getElementById("ulSteps");
        var alreadyStep = level * 1 + 1;

        var stepsBtn = steps.getElementsByTagName("li");
        for (var i = 0; i < stepsBtn.length; i++) {
            var liNumber = parseInt(stepsBtn[i].id.replace('liStep', ''));

            if(i==0)
            {
                stepsBtn[i].innerHTML = "<a id='linkStep" +
                liNumber + "' href='javascript:void(0);' style='cursor:hand;' class='mouseover' onclick='javascript:step" + liNumber + "();'>" +
                "<span class='mouseover' >" + liNumber +
                    "</span><span class='titleNavigation titleNavigationFirst'>" + arrStepsNames[liNumber] + "</span></a>";
            }
            else if (i == stepsBtn.length - 1) {
                stepsBtn[i].innerHTML = "<a id='linkStep" +
                liNumber + "' href='javascript:void(0);' style='cursor:hand;' class='mouseover' onclick='javascript:step" + liNumber + "();'>" +
                "<span class='mouseover' >" + liNumber +
                    "</span><span class='titleNavigation titleNavigationLast'>" + arrStepsNames[liNumber] + "</span></a>";
            }

            else {
                stepsBtn[i].innerHTML = "<a id='linkStep" +
                liNumber + "' href='javascript:void(0);' style='cursor:hand;' class='mouseover' onclick='javascript:step" + liNumber + "();'>" +
                "<span class='mouseover' >" + liNumber +
                    "</span><span class='titleNavigation'>" + arrStepsNames[liNumber] + "</span></a>";
            }

        }
        setLinkStepActive('linkStep1');
        SetIframeHeight();

        //step5();         

    }
    function SetStep(_num) {
        setLinkStepActive('linkStep' + _num);
        SetIframeHeight();
    }

    function setInitRegistrationStep(level) {
        //setLinkStepActive('linkStep' + (level*1+1));
        //alert("level " + level);
        return;
        RemoveServerComment();
        switch (level) {
            case 0:
                step1();
                break;

            case 1:
                step2();
                break;

            case 2:
                step3();
                break;

            case 3:
                step4();
                break;

            case 4:
                step5();
                break;

            case 5:
                step6();
                break;
            /* 
            case 6:
            step();
            break;    
            */ 
            default:
                break;
        }
    }


    function setLinkStepActive(linkId) {
        var steps = document.getElementById("ulSteps");
        var A_Btn = steps.getElementsByTagName("A");
        if (A_Btn.length == 0) {
            witchAlreadyStep(6);
            A_Btn = steps.getElementsByTagName("A");
        }
        for (var i = 0; i < A_Btn.length; i++) {
            RemoveCssClass(A_Btn[i], "active");
        }
        document.getElementById(linkId).className = "active mouseover";
    }

    function goUp() {
        window.scrollTo(0, 0);
    }

    function step1() {
        //alert("step1");
        showDiv();
        //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src = '<%#ResolveUrl("~")%>Management/professionalDetails2014.aspx';
        //document.getElementById("<%# iframe.ClientID %>").height = '1050px';
        document.getElementById("<%# iframe.ClientID %>").style.height = '1650px';
        
        //document.getElementById("<%# iframe.ClientID %>").style.overflowX = "hidden";
        
    }


    function step2() {
        showDiv();
        //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src = '<%#ResolveUrl("~")%>Management/categories2014.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '670px';
    }

    function step3() {
        showDiv();
        //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src = '<%#ResolveUrl("~")%>Management/<%#cityPage%>';
        document.getElementById("<%# iframe.ClientID %>").height = '900px';
    }

    function step4() {
        
        showDiv();
        //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src = '<%#ResolveUrl("~")%>Management/professionalDates2014.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '850px';
    }

    function step5() {
        showDiv();
        //window.scrollTo(0,0);      
        document.getElementById("<%# iframe.ClientID %>").src = '<%#ResolveUrl("~")%>Management/accountSettings.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '670px';
    }
    /*
    function step6()
    {
    showDiv(); 
    document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/professionalPaymentrRunWest.aspx';
    document.getElementById("<%# iframe.ClientID %>").height = '860px';
       
    }
    */



    //  window.onload = init;
    function SetIframeHeight() {

        var _iframe = document.getElementById("<%# iframe.ClientID %>");
        var _nav = document.getElementById("tab_navigation");
        //var _heigth = _iframe.offsetHeight - _nav.offsetHeight;
        var _heigth = _iframe.offsetHeight;       
        if (_heigth > 0)
            document.getElementById("<%# div_height.ClientID %>").style.height = (_heigth) + "px";

    }

    function GetIframeStep() {
        var _src = document.getElementById("<%# iframe.ClientID %>").src;
        if (_src.indexOf("professionalDetails") > -1)
            return 1;
        if (_src.indexOf("professionchooseprofessions") > -1)
            return 2;
        if (_src.indexOf("<%#cityPage%>") > -1)
            return 3;
        if (_src.indexOf("professionalDates") > -1)
            return 4;
        if (_src.indexOf("professionalCallPurchase") > -1)
            return 5;
        /*
        if (_src.indexOf("professionalPaymentrRunWest") > -1)
        return 6;
        */
        return
    }
    function SetHeaderUpdate(_step) {
        var path = window.location.href.split('?')[0];
        if (_step > 0)
            path = path + "?step=" + _step;
        window.location = path;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<div class="navigationContainer">
<div class="tab-navigation" id="tab_navigation">
	<ul id="ulSteps">
		<li id="liStep1"></li>
		<li id="liStep2"></li>
        <li id="liStep3"></li>
		<li id="liStep4"></li>
        <!--
		<li id="liStep5"></li>
        -->
	</ul>
</div>

<div class="containerProfile">
    <a id="linkBusiness" class="linkProfile" runat="server" target="_blank">View your business page</a>
</div>

</div>

<div class="iframe-content">
    <iframe id="iframe" class="iframe"  frameborder="0" scrolling="auto" runat="server"  ></iframe>
</div>


<div id="div_height" runat="server"></div>

</asp:Content>
