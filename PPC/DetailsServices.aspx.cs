﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PPC_DetailsServices : PageSetting
{
    string _GetBackHeadings;
    protected List<string> PhoneReg { get; private set; }
 //   protected int PhoneRegLength { get { return PhoneReg.Count; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(a_categories);
        ScriptManager1.RegisterAsyncPostBackControl(a_FreeSearch);
        PhoneReg = Utilities.GetCheckStatusUSPhoneRegularJavaScript();        
        if (!IsPostBack)
        {
            
            siteSetting = new SiteSetting("1");
            siteSetting.SetSiteSetting(this);
            
            txt_pass.Value = CodePlaceHolder;
            txt_Category.Text = ServicePlaceHolder;
      //      SetStartScript();
        }
        Header.DataBind();
    }
    /*
    void SetStartScript()
    {
        string _script = "SetPlaceHolder();";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "SetStartScript", _script, true);
    }
     */
    
    protected void a_categories_click(object sender, EventArgs e)
    {
        div_CategoryTitle.Style[HtmlTextWriterStyle.Display] = "none";
        lbl_Category.Text = string.Empty;
        lbl_Heading.Text = string.Empty;

        LinkButton _lb = (LinkButton)sender;
        if(_lb.CommandArgument=="Category")
        {
            SetFreeSearch();
            return;
        }

        div_c.Style[HtmlTextWriterStyle.Display] = "block";
        div_b.Style[HtmlTextWriterStyle.Display] = "none";
        div_a.Style[HtmlTextWriterStyle.Display] = "none";
        div_HeadingCategoryTable.Style[HtmlTextWriterStyle.Display] = "block";
        
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("arg");
        data.Columns.Add("script");

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _supplier.GetAllCategories();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            SetServerComment("Server error");
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            SetServerComment("Server error");
            return;
        }
        _GetBackHeadings = "Category";
        foreach (WebReferenceSupplier.GuidStringPair gsp in result.Value)
        {
            DataRow row = data.NewRow();
            row["Name"] = gsp.Name;
            row["arg"] = gsp.Id.ToString();
            row["script"] = "SetButton(this);";
            data.Rows.Add(row);
        }
         
        _dataList.DataSource=data;
        _dataList.DataBind();
       
        _UpdatePanel.Update();
    }
    protected void a_FreeSearch_click(object sender, EventArgs e)
    {
        SetFreeSearch();
    }
    void SetFreeSearch()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFreeSearch", "SetFreeSearch();", true);
        div_c.Style[HtmlTextWriterStyle.Display] = "block";
        _dataList.DataSource = null;
        _dataList.DataBind();

        _UpdatePanel.Update();
    }
    protected string GetBackHeadings
    {
        get { return _GetBackHeadings; }
    }
    void SetServerComment(string str)
    {
        if(!ClientScript.IsStartupScriptRegistered("ServerComment"))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerComment", "SetServerComment('" + HttpUtility.HtmlEncode(str) + "');", true);
    }
    protected void btn_Category_Click(object sender, EventArgs e)
    {
     ///   string args = ((Button)sender).CommandArgument;
        LinkButton btn = ((LinkButton)sender);
        Guid _id = new Guid(btn.CommandArgument);

        div_CategoryTitle.Style[HtmlTextWriterStyle.Display] = "block";
        lbl_Category.Text = btn.Text;
        lbl_Heading.Text = string.Empty;
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("arg");
        data.Columns.Add("script");
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
         WebReferenceSupplier.ResultOfListOfGuidStringPair result = null;
         try
         {
             result = _supplier.GetCategoryGroup(_id);
         }
         catch (Exception exc)
         {
             dbug_log.ExceptionLog(exc, siteSetting);
             SetServerComment("Server error");
             return;
         }
         if (result.Type == WebReferenceSupplier.eResultType.Failure)
         {
             SetServerComment("Server error");
             return;
         }
         _GetBackHeadings = "Headings";
         foreach (WebReferenceSupplier.GuidStringPair gsp in result.Value)
        {           
            DataRow row = data.NewRow();
            row["Name"] = gsp.Name;
            row["arg"] = gsp.Id.ToString();
            row["script"] = "SetButton(this); return false;";
            data.Rows.Add(row);           
        }
        _dataList.DataSource = data;
        _dataList.DataBind();

        _UpdatePanel.Update();
    }
    
    protected string GetHeadingServiceList
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/GetHeadingList"; }
    }
    protected string GetSendMobileService
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/SendMobile"; }
    }
    protected string GetLoginService
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/Login"; }
    }
    protected string GetIsHeadingService
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/Is_Heading"; }
    }
    protected string GetSetServiceService
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/SetService"; }
    }
    protected string GetPhoneExpresion
    {
        get { return Utilities.RegularExpressionForJavascript(@"^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"); }
  //      get { return Utilities.RegularExpressionForJavascript(@"^\d+$"); }
    }
    protected string PhonePlaceHolder
    {
        get { return "e.g. 1(646)1234567"; }
    }
    protected string CodePlaceHolder
    {
        get { return "It's in the text message we sent to tour phone"; }
    }
    protected string ServicePlaceHolder
    {
        get { return "e.g. plumber, heating, etc."; }
    }
    protected string Site_Id
    {
        get { return siteSetting.GetSiteID; }
    }
    protected string GetServerError
    {
        get
        {
            return "The code could not be sent, please try again later";
        }
    }
    protected string LoginPage
    {
        get { return ResolveUrl("~") + "Management/LogOut.aspx"; }
    }

    protected string RedirectLocation
    {
        get { return ResolveUrl("~") + "PPC/Location.aspx"; }
    }

   
}