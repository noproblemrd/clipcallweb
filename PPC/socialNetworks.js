﻿
    /*********** social networks ************/
    /*********** facebook ************/
    window.fbAsyncInit = function () {
        // appId: 392131104255963 dev
        // appId: 486879858091228 qa
        FB.init({
            appId: '<%#facebookAppId%>',
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });

        // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
        // for any authentication related change, such as login, logout or session refresh. This means that
        // whenever someone who was previously logged out tries to log in again, the correct case below 
        // will be handled. 
        FB.Event.subscribe('auth.authResponseChange', function (response) {
            //alert(response.status);
            // Here we specify what we do with the response anytime this event occurs. 
            // see ex[lanation https://developers.facebook.com/docs/facebook-login/login-flow-for-web/
            /* response include

            {
            status: 'connected',
            authResponse:
            {
            accessToken: '...',
            expiresIn:'...',
            signedRequest:'...',
            userID:'...'
            }
            }

            */

            if (response.status === 'connected') {
                // here he also entered after  he cliced on "ok" buttom in facebook process when he aske him for approve
                // The response object is returned with a status field that lets the app know the current
                // login status of the person. In this case, we're handling the situation where they 
                // have logged in to the app.
            
                //faceBookLogIn2();
            } else if (response.status === 'not_authorized') {
                // In this case, the person is logged into Facebook, but not into the app, so we call
                // FB.login() to prompt them to do so. 
                // In real-life usage, you wouldn't want to immediately prompt someone to login 
                // like this, for two reasons:
                // (1) JavaScript created popup windows are blocked by most browsers unless they 
                // result from direct interaction from people using the app (such as a mouse click)
                // (2) it is a bad experience to be continually prompted to login upon page load.
                ////FB.login();

            } else {
                // In this case, the person is not logged into Facebook, so we call the login() 
                // function to prompt them to do so. Note that at this stage there is no indication
                // of whether they are logged into the app. If they aren't then they'll see the Login
                // dialog right after they log in to Facebook. 
                // The same caveats as above apply to the FB.login() call here.
                ////FB.login();
            }

            if (window.console)
                console.log("status:" + response.status);
        });
    };


    // Load the SDK asynchronously
    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    } (document));

    function faceBookLogOut() {
        alert("g");
        FB.logout(function (response) {
            alert("g2");         
            location.href='logout.aspx';
            // Person is now logged out
        });

        return false;
    }





    /*********************** google sign in **************************/ 

    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/client:plusone.js?onload=render';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();

    function render() {
        gapi.signin.render('googleCustomSignIn', {
            'callback': 'signinCallback',
            'clientid': '868196622582-vl6pg174p48t8chura0dsoa0r84opvgt.apps.googleusercontent.com',
            'cookiepolicy': 'single_host_origin',
            'requestvisibleactions': 'http://schemas.google.com/AddActivity',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
        });
    }

    /**
    * Uses the JavaScript API to request the user's profile, which includes
    * their basic information. When the plus.profile.emails.read scope is
    * requested, the response will also include the user's primary email address
    * and any other email addresses that the user made public.
    */
    function loadProfile() {
        var request = gapi.client.plus.people.get({ 'userId': 'me' });
        request.execute(loadProfileCallback);
    }

    /**
    * Callback for the asynchronous request to the people.get method. The profile
    * and email are set to global variables. Triggers the user's basic profile
    * to display when called.
    */
    function loadProfileCallback(obj) {
        profile = obj;

        // Filter the emails object to find the user's primary account, which might
        // not always be the first in the array. The filter() method supports IE9+.
        email = obj['emails'].filter(function (v) {
            return v.type === 'account'; // Filter out the primary email
        })[0].value; // get the email from the filtered results, should always be defined.

        var id = obj.id
        var name = obj.name.givenName + ' ' + obj.name.familyName;

        checkRegistrationStep(email, id, name, '', 'googlePlus');
        if (window.console)
            console.log('email:' + email + ' id:' + id + ' name:' + name);
    }

    function signinCallback(authResult) {
        //alert("baaaaa " + authResult['status']['signed_in']);
        if (authResult['status']['signed_in']) {
            // Update the app to reflect a signed in user
            // Hide the sign-in button now that the user is authorized, for example:
            ////document.getElementById('customBtn').setAttribute('style', 'display: none');
            ////gapi.client.load('plus', 'v1', loadProfile);  // Trigger request to get the email address.
        } else {
            //alert(authResult['error']);
            // Update the app to reflect a signed out user
            // Possible error values:
            //   "user_signed_out" - User is signed-out
            //   "access_denied" - User denied access to your app
            //   "immediate_failed" - Could not automatically log in the user
            if (window.console)
                console.log('Sign-in state: ' + authResult['error']);
        }
    }

    function signOutGooglePlus() {
        // Note that sign out will not work if you are running from localhost.
        //alert("baaa");
        gapi.auth.signOut(function () { alert("baaaa"); });
        //location.href = 'logout.aspx';
        return false;
    }
      
