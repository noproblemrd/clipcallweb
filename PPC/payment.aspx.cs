﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_payment : RegisterPage2, IsInProcess
{
    protected decimal fee;
    protected decimal registrationBonus;

    protected void Page_Load(object sender, EventArgs e)
    {

        plan = Request.QueryString["plan"].ToString();

        if (!IsPostBack)
        {
            LoadMaster();
            SetStep();
            LoadPlan();
            LoadIframe();            
        }
    }

    private void SetStep()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.Result result = new WebReferenceSupplier.Result();
        try
        {
            result = _supplier.CheckoutButtonClicked_Registration2014(SupplierId);

            if (result.Type == WebReferenceSupplier.eResultType.Failure)
            {
                dbug_log.ExceptionLog(new Exception("failed CheckoutButtonClicked_Registration2014 for " + SupplierId.ToString() + " message: " + result.Messages[0]));
                return;
            }

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);
            return;
        }
    }

    private void LoadIframe()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfSubscriptionCostAndBonus result = new WebReferenceSupplier.ResultOfSubscriptionCostAndBonus();
        try
        {
            result = _supplier.GetSubscriptionPlanCost();
            fee = result.Value.Cost;
            registrationBonus = result.Value.Bonus;
            //result.
            
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);  
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            masterPage.ServerProblem(GetServerError);  
            return;
        }

        if (plan == "FEATURED_LISTING")
        {
            string __form = AddCreditSales.BuyCredit2(Convert.ToInt32(fee), siteSetting, SupplierId, Convert.ToInt32(fee), plan, true,"monthlyFeePayment",0);
            iframePayment.Attributes["src"] = __form;
        }

        else if (plan == "LEAD_BOOSTER_AND_FEATURED_LISTING")
        {
            string __form = AddCreditSales.BuyCredit2(0, siteSetting, SupplierId, Convert.ToInt32(fee), plan, true, "monthlyFeePayment", 0);
            iframePayment.Attributes["src"] = __form;
        }
    }


    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

       
        masterPage.SetStatusBar("past", "past", "current");
        masterPage.SetBlackMenu();        
       
    }

    protected void LoadPlan()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.SetPlanType(plan);       

    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

    }

    protected string plan
    {
        get
        {
            return (ViewState["plan"] != null) ? ViewState["plan"].ToString() : "";
        }

        set
        {
            ViewState["plan"] = value;
        }

    }

    protected string GetServerError
    {
        get { return (ViewState["GetServerError"] == null) ? " " : ViewState["GetServerError"].ToString(); }
        set { ViewState["GetServerError"] = value; }
    }
}