﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;


public partial class PPC_Register : RegisterPage
{
    public string blnLandingpage;
    protected string QueryStringParams;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            //Response.Write(Request.QueryString);

            QueryStringParams = Request.QueryString.ToString();
            if (!string.IsNullOrEmpty(QueryStringParams))
                QueryStringParams = "?" + QueryStringParams;
                    
            string registerFormat;
            registerFormat = (Request["format"] == null) ? "" : Request["format"].ToString();

            bool landingPage;

            if (registerFormat.ToLower() == "landingpage")
                landingPage = true;
            else
                landingPage = false;

            if (landingPage)
                homeheaderLandingPage.Visible = true;
            else
                homeheaderLandingPage.Visible = false;

            LoadFlavor();

            LoadMaster();
        }
        //LoadMaster();
        
        //txt_referrer.Attributes["place_holder"] = "The code from the email invitation you received";
              
        Header.DataBind();
    }


    protected void LoadFlavor()
    {
        Response.Redirect("RegisterC.aspx" + QueryStringParams);

        Random rnd = new Random();
        
        int rndNumber;
        rndNumber=rnd.Next(2);
        switch (rndNumber)
        {
            case 0:
                Response.Redirect("RegisterB.aspx" + QueryStringParams);
                break;

            case 1:
                Response.Redirect("RegisterC.aspx" + QueryStringParams);
                break;
            /*
            case 2:
                Response.Redirect("RegisterD.aspx" + QueryStringParams);
                break;
            */

            /*
            case 2:
                ctlFlavor = (UserControl)LoadControl("~/Controls/Registration/RegisterC.ascx");
                //masterPage.SetContentMinHeight("1677px");
                socialNetworks1.LandingPage = WebReferenceSupplier.eAdvertiserRegistrationLandingPageType.C;
                break;
            */

            default:
                Response.Redirect("RegisterA.aspx" + QueryStringParams);         
                break;
        }

       
    }



    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        //masterPage.SetContentPaddingBottom("100px");
        //masterPage.SetStatusBar("statusBar1");

    } 

    

    protected string RegisterReferrer
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/SupplierReferrerInvitation"); }
    }
}