﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_FinalStep : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbl_phone.Text = GetPhone;
            SetBenefit();
            /*
            siteSetting = new SiteSetting("1");
            siteSetting.SetSiteSetting(this);
            userManagement = new UserMangement(new Guid("c641851a-3199-4bc1-9c81-ffb4faf41238"), SecurityLevel.PRE_SUPPLIER, "");
            userManagement.SetUserObject(this);
            */
            
            if (!userManagement.IsSupplier())
                Response.Redirect(ResolveUrl("~") + "PPC/RequestInvitation.aspx");
            
            
        }
    }
    private void SetBenefit()
    {
        Master.SetBenefits("<h3>Get started!</h3><br/>");
        Master.BenefitsLinks(true);
    }
    string GetPhone
    {
        get { return (Session["RegPhone"] == null) ? string.Empty : (string)Session["RegPhone"]; }
        set { Session["RegPhone"] = value; }
    }
}