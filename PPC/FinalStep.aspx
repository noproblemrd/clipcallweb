﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNP.master" AutoEventWireup="true" CodeFile="FinalStep.aspx.cs" Inherits="PPC_FinalStep" %>
  <%@ MasterType VirtualPath="~/Controls/MasterPageNP.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" src="../general.js"></script>
<script type="text/javascript">
    function LoadVideo() {
        $('#table_video').slideToggle("fast", function () {
            $('#iframeVideo').attr('src', 'http://www.youtube.com/embed/2lnZoIF2sVE?autoplay=1&autohide=1');
            $('#div_video').slideToggle("fast");
        });
    /*
        document.getElementById("table_video").style.display = "none";
        document.getElementById("iframeVideo").src = "http://www.youtube.com/embed/2lnZoIF2sVE?autoplay=1&autohide=1";
        document.getElementById("div_video").style.display = "block";
        */
    }
    function HideVideo() {
        $('#div_video').slideToggle("fast", function () {
            $('#iframeVideo').removeAttr('src');
            $('#table_video').slideToggle("fast");
        });
    /*
        document.getElementById("table_video").style.display = "block";
        document.getElementById("iframeVideo").removeAttribute('src');
        document.getElementById("div_video").style.display = "none";
        */
    }
    

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="final_step">
     <div class="statusBar statusBarBackground3">
        <div class="detailsService shadow">Step 1</div>
        <div class="location location-done shadow">Step 2</div> 
        <div class="getFreeLeads getFreeLeads-done shadow">Get Started!</div>  
                    
    </div>
    <div class="final_step titleFirst">That’s it! You’re all set.</div>
    <span>
        You are now registered to No Problem’s phone lead advertising service. Your profile is being propagated across the Internet via our network of partners and affiliates. As soon as a customer within your area requests your service, you will receive a call at
         <asp:Label ID="lbl_phone" runat="server"></asp:Label>. (<a href="<%= ResolveUrl("~") %>PPC/FramePpc.aspx" class="general">change this number</a>).
    </span>
    <div class="clear"></div>
    <div class="titleFirst titleFirstFinalStep">Here’s how it works</div>
    <div class="clear"></div>
    <table width="680px" id="table_video">
    <tr>
    <td>
        <div id="div_img_video" class="div_img_video">
            <a href="javascript:void(0);" onclick="LoadVideo();" ></a>
        </div>
        
    </td>
    <td>
        <div>
            <div class="adv01">We find ready-to-hire customers through our online affiliates and partner network.</div>
	        <div class="adv02">You decide how much the call is worth to you after hearing the request.</div>
			<div class="adv03"> We put you through to the customer. You pay only for this call.</div>
        </div>
    </td>
    </tr>
    </table>
    <div id="div_video" style="display:none;" class="div_video">
        <a href="javascript:void(0);" onclick="HideVideo();"></a>
            
        <iframe id="iframeVideo" name="iframeVideo" width="666" height="375" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="clear"></div>
    <div class="titleFirst titleFirstFinalStep">What should you do next?</div>
    <div class="clear"></div>
    <span>
        Nothing! We’ll call you as soon as we have a customer within your area that requires your service. In the meantime, you can concentrate on the work at hand without worrying about where the next job will come from. Or, you can complete your No Problem PPC profile while you wait for your first call. 
    </span>
    <div  class="btn_visit">
        <a href="<%= ResolveUrl("~") %>Management/OverView.aspx" >Visit Your Account</a>
    </div>
</div>
</asp:Content>

