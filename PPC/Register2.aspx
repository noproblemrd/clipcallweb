﻿<%@ Page Language="C#" MasterPageFile="~/Controls/MasterPageNP2.master" AutoEventWireup="true" CodeFile="Register2.aspx.cs" Inherits="PPC_Register2" ClientIDMode="Static" %>
<%@ Register Src="~/Controls/socialNetworks.ascx" TagName="socialNetworks" TagPrefix="Registration" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="../PlaceHolder.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>

<!--[if IE 7]>      
      <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome-ie7.min.css" />
    <![endif]-->    
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<Registration:socialNetworks  ID="socialNetworks1" runat="server"/>

<div class="registration">

    <div id="registrationContainerVideoIframe" class="registrationContainerVideoIframe">       
        <div style="" class="registrationVideoIframe" id="registrationVideoIframe">
	        <div>
                <a href="javascript:showHideVideo();" class="x-close"></a>  
            </div>          
            <iframe height="" frameborder="0" width="870" allowfullscreen="" src="" name="iframeVideo" id="iframeVideo"></iframe>
        </div>
    </div>

    <div class="clear"></div>

    <div class="registrationLeft">

        <div class="titleFirst">
        <span>Join NoProblem</span>
    </div>

        <div class="connectWith">
            <span>Connect with</span>
        </div>
        
    
       <div class="socialNetworks">

        <span id="googleCustomSignIn" class="icon-stack icon-stackGooglePlus">
            <a href="javascript:void(0);">
                <i class="icon-circle icon-stack-base" style="color: rgb(210, 71, 55);"></i>
                <i class="icon-google-plus"></i>
            </a>
        </span>

        <span class="icon-stack icon-stackFacebook">
            <a onclick="faceBookLogIn2();" href="javascript:;">
                <i class="icon-circle icon-stack-base" style="color: rgb(59, 89, 153);"></i>
                <i class="icon-facebook"></i>
            </a>
        </span>      

    </div>

       <div class="clear"></div>

       <div class="or">Or</div>      

       <div class="registrationInputs" >    
        
            

             <div class="emailContainer">
            <div>Email</div>  
            <div class="inputParent">
                <input id="txt_email" type="text" class="textActive" runat="server" HolderClass="place-holder" text-mode="email" />
                <div id="div_EmailSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_EmailError" class="inputError" style=""> </div>

                

                <div class="inputValid" style="display:none;"  id="div1"></div>

                <div id="div_EmailComment" style="display:none;" class="inputComment"><%#EmailError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        <div class="passwordContainer">
            <div>Password</div>
            <div class="inputParent">
                <input id="txt_password" type="password" class="textActive" runat="server" HolderClass="place-holder" text-mode="pelephone" />

                <div id="div_PasswordSmile" class="inputValid" style="display:none;"> </div>
                <div id="div_PasswordError" class="inputError" style=""> </div>

                <div class="inputValid" style="display:none;"  id="div2"></div>

                <div id="div_PasswordComment" style="display:none;" class="inputComment"><%#PasswordError%>                    
                    <div class="chat-bubble-arrow-border"></div>
                    <div class="chat-bubble-arrow"></div>
                </div>

            </div>
        </div>

        </div>
   
       
       

        <div> 
  </div>
   
       <div class="clear"></div>

       <div class="containerLogin">
            <div class="loginCell alreadyRegistered">
                Already registered? <span class="spanLinkLogin"><a href="ppcLogin.aspx">Login</a></span>
            </div>
        
            <div class="loginCell registerLogin">
                 <section class="progress-demo">
                    <a id="linkRegistration" href="javascript:void(0);" data-style="expand-right" data-color="blue" class="ladda-button" button-mode="phone" onclick="javascript:preCheckRegistrationStep('regularRegister');">
                        <span class="ladda-label">Next</span><span class="ladda-spinner">
                        </span><div class="ladda-progress" style="width: 138px;"></div>
                    </a>
                 </section>               
            </div>            

        </div>
    
    </div>
   
    <div class="registrationRight">

        <div class="registrationContainerVideo">
            <div onclick="javascript:showHideVideo();" class="registrationVideo"></div>            
        </div>

        <div class="registrationDescriptions">
            <div class="regDesc regDesc1">
                <div class="regDescNumber">
                    1
                </div>

                <div class="regDescContent">
                    We find ready-to-spend customers through our
                    <br />
                    online affiliates and partner network.  
                </div>
            </div>

            <div class="regDesc regDesc2">
                <div class="regDescNumber">
                    2
                </div>

                <div class="regDescContent">
                    You decide how much the call is worth to you after
                    <br />
                    hearing the request. 
                </div>
            </div>

             <div class="regDesc regDesc3">
                <div class="regDescNumber">
                    3
                </div>

                <div class="regDescContent">
                    You decide how much the call is worth to you after
                    <br />
                    hearing the request. 
                </div>
            </div>
        </div>
    </div>
    
</div>
</asp:Content>
