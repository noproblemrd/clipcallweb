﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageConsumer.master" AutoEventWireup="true" CodeFile="Consumer.aspx.cs" Inherits="PPC_Consumer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <link href="../Formal/jquery-ui-1.8.16.custom.css" type="text/css" rel="Stylesheet" />
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
     <script type="text/javascript" src="../scripts/jquery-ui-1.8.17.custom.autocomplete.min.js"></script>
  <script type="text/javascript" src="../PlaceHolder.js"></script>

  <script type="text/javascript">
      var _noproblem = _noproblem || {};
      _noproblem.IsSelected = false;
      $(function () {
          $("._Heading").autocomplete({

              source: function (request, response) {
     //             var _txt = $("#<%# txt_service.ClientID  %>").val();
      //            SourceText = request.term;
                  $.ajax({
                      url: "<%# GetHeadingServiceList %>",
                      data: "{ 'TagPrefix': '" + request.term + "'}",
                      dataType: "json",
                      type: "POST",
                      contentType: "application/json; charset=utf-8",
                      dataFilter: function (data) { return data; },
                      success: function (data) {

                          response($.map(data.d, function (item) {
                              return {
                                  value: item
                              }
                          }))
                      },
                      error: function (XMLHttpRequest, textStatus, errorThrown) {
                          //              alert(textStatus);
                      },
                      complete: function (jqXHR, textStatus) {
                          //  HasInHeadingRequest = false;
                      }

                  });
              },
              minLength: 1,
              close: function (event, ui) {
                  if (!_noproblem.IsSelected && event.target.value.length != 0) {
                      //             SlideElement("div_ServiceComment", true); // document.getElementById("div_ServiceComment").style.display = "block";
                      //               $('#div_ServiceError').show();
                  }
                  _noproblem.IsSelected = false;
              },
              /*
              open: function (event, ui) {
              SlideElement("div_ServiceComment", false); // document.getElementById("div_ServiceComment").style.display = "none";
              $('#div_ServiceError').hide();
              },
              */
              select: function (event, ui) {
                  //                SlideElement("div_ServiceComment", false); // document.getElementById("div_ServiceComment").style.display = "none";
                  //                 $("#div_ServiceSmile").show();
                  //                 $('#div_ServiceError').hide();
                  _noproblem.IsSelected = true;
              }
          });
      });
  
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div>
    <asp:TextBox ID="txt_service" runat="server" place_holder="Type a service provider" HolderClass="place-holder"
     CssClass="_Heading textActive" holder-position="none"></asp:TextBox>
    <asp:LinkButton ID="lb_Search" runat="server">Call me now</asp:LinkButton>

</div>
    


</asp:Content>

