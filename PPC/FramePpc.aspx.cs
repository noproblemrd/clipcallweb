﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class PPC_FramePpc : PageSetting
{
    RegionTab result;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {

            whatCityPage(siteSetting.GetSiteID);

            

            string _step = Request.QueryString["step"];
            Regex _rg = new Regex("^[1-6]$");
            iframe.Attributes.Add("height", @"900px");
            if (!string.IsNullOrEmpty(_step) && _rg.IsMatch(_step))
            {
                if (_step == "6")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"PPC/AddCredit.aspx");
                else if (_step == "4")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalDates.aspx?mode=init");
                else if (_step == "1")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalDetails.aspx?mode=init");
                else if (_step == "2")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionChooseProfessions.aspx?mode=init");
                else if (_step == "3")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/" + cityPage);
                else if (_step == "5")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalCallPurchase.aspx?mode=init");
            }
            else
                iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalDetails.aspx?mode=init");
                
            ClientScript.RegisterStartupScript(this.GetType(), "show_div", "showDiv();", true);
            
            div_height.Style.Add(HtmlTextWriterStyle.Height, "900px");

        }
        Page.Header.DataBind();
        //    iframe.Attributes.Add("onload", "OnIframeLoad();");
    }


    private void whatCityPage(string SiteId)
    {
        result = (RegionTab)DBConnection.GetRegionTab(SiteId);

    }


    protected string cityPage
    {
        get
        {
            if (result == RegionTab.Map)
                return "LocationMap.aspx";
            else if (result == RegionTab.Tree)
                return "professionalCities3.aspx";
            else
                return "professionalCities3.aspx";

        }


    }
    protected string GetPaymentInfo
    {
        get { return Server.HtmlEncode("Add Credit"); }
    }
    protected string GetSegmentsArea
    {
        get { return Server.HtmlEncode("What I Do"); }
    }
    protected string GetCities
    {
        get { return Server.HtmlEncode("Area I Cover"); }
    }
    protected string Get_Time
    {
        get { return Server.HtmlEncode("When I Work"); }
    }
    protected string Get_MyCredits
    {
        get { return Server.HtmlEncode("Set Price"); }
    }

    protected string Get_GeneralInfo
    {
        get { return Server.HtmlEncode("My Details"); }
    }
    
   
}