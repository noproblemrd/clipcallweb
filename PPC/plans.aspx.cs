﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;

public partial class PPC_plans : RegisterPage, IsInProcess
{   
    
    protected void Page_Load(object sender, EventArgs e)
    {      
       
        if (!IsPostBack)
        {            
            Master.removeSideBar();
            ChoosePlan();

            SalesUtility.SetStepRegistration(Session["RegistrationPhone"] != null ? Session["RegistrationPhone"].ToString() : "", Site_Id, "PassedEnterPasswordPage", Session["RegistrationPassword"] != null ? Session["RegistrationPassword"].ToString() : "", "", "","", "", "", "", false);
        }
        else
        {


        }
        LoadMaster();

        Header.DataBind();
    }

    private void LoadMaster()
    {
        Master.SetBenefitFreeText("Get leads from ready-to-hire customers directly to your mobile phone.</br></br>That's NoProblem!");
        Master.SetStatusBar("statusBar2");
    }    
   
    protected void ChoosePlan()
    {
        Random rndPlan = new Random();

        int indexRnd = rndPlan.Next(2);

        if (indexRnd == 0)
        {
            featuredListingPerZipCode.ImageUrl = "images/featuredListingFixed.png";
            leadBoosterPerZipCode.ImageUrl = "images/leadBoosterFixed.png";

            
        }
        else
        {
            featuredListingPerZipCode.ImageUrl = "images/featuredListingPerZipCode.png";
            leadBoosterPerZipCode.ImageUrl = "images/leadBoosterPerZipCode.png";            
        }

    }
        

    protected string Site_Id
    {
        get { return siteSetting.GetSiteID; }
    } 
    

    protected void featuredListingPerZipCode_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        string page = "";
        string plan = "";

        if (btn.ImageUrl == "images/featuredListingPerZipCode.png")
        {
            page = "pageFeaturedListingZipCode";
            plan = "Featured Listing plan (20c per zip)";
           
        }
        else if (btn.ImageUrl == "images/featuredListingFixed.png")
        {
            page = "pageFeaturedListingFixed";
            plan = "Featured Listing plan (3.99 a month)";         
           
        }

        Session["plan"]=plan;
        Session["page"] = page;

        SalesUtility.SetStepRegistration(Session["RegistrationPhone"] != null ? Session["RegistrationPhone"].ToString() : "", Site_Id, "PassedChoosePlanPage_CompletedAll", Session["RegistrationPassword"] != null ? Session["RegistrationPassword"].ToString() : "", page, plan,"","","","", false);

        Response.Redirect("basicDetails.aspx");
    }

    protected void leadBoosterPerZipCode_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        string page = "";
        string plan = "";

        if (btn.ImageUrl == "images/leadBoosterPerZipCode.png")
        {
            page = "pageFeaturedListingZipCode";
            plan = "Lead booster plan (FREE Featured Listing plan (20c per zip))";           
             
        }
        else if (btn.ImageUrl == "images/leadBoosterFixed.png")
        {
            page = "pageFeaturedListingFixed";
            plan = "Lead booster (FREE Featured Listing plan (3.99 a month))";          

        }

        Session["plan"]=plan;
        Session["page"] = page;

        SalesUtility.SetStepRegistration(Session["RegistrationPhone"] != null ? Session["RegistrationPhone"].ToString() : "", Site_Id, "PassedChoosePlanPage", Session["RegistrationPassword"] != null ? Session["RegistrationPassword"].ToString() : "", page, plan,"","","","", false);

        Response.Redirect("basicDetails.aspx");
    }
}