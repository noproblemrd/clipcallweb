﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_RegisterReferrer : RegisterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("Register.aspx");

        if (!IsPostBack)
        {
            LoadPageText();
        }
        LoadMaster();
        txt_phone.Attributes["place_holder"] = PhonePlaceHplder;
        txt_referrer.Attributes["place_holder"] = "The code from the email invitation you received";
        Header.DataBind();
    }

    private void LoadMaster()
    {
        Master.SetStatusBar("statusBar1");
    }
    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;
        //    _map.FieldMissing = FieldMissing;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "Location"
                          select x).FirstOrDefault();

       // FieldMissing = xelem.Element("BusinessName").Element("Validated").Element("Missing").Value;
     //   XElement t_element = xelem.Element("BusinessAddress").Element("Validated");

    //    t_element = xelem.Element("Email").Element("Validated");
    //    EmailPlaceHplder = t_element.Element("Instruction").Element("General").Value;
    //    EmailDuplicate = t_element.Element("Instruction").Element("EmailDuplicate").Value;
    //    InvalidEmail = t_element.Element("Error").Value;
    //    EmailMissing = t_element.Element("Missing").Value;

        xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                 where x.Attribute("name").Value == "DetailsService"
                 select x).FirstOrDefault();
        PhonePlaceHplder = xelem.Element("MobilePhone").Element("Validated").Element("Instruction").Element("General").Value;
        ReferralCodeNotFound = xelem.Element("ReferralCode").Element("ReferralCodeWrong").Value;
        ReferralCodeMissing = xelem.Element("ReferralCode").Element("Missing").Value;
        Master.SetBenefitFreeText("Get leads from ready-to-hire customers directly to your mobile phone.<br /><br />That's NoProblem!");
    }
    protected string PhonePlaceHplder
    {
        get { return (string)ViewState["PhonePlaceHplder"]; }
        set { ViewState["PhonePlaceHplder"] = value; }
    }
    protected string GetServerError
    {
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }
    protected string ReferralCodeNotFound
    {
        get { return HttpUtility.JavaScriptStringEncode((string)ViewState["ReferralCodeNotFound"]); }
        set { ViewState["ReferralCodeNotFound"] = value; }
    }
    protected string ReferralCodeMissing
    {
        get { return HttpUtility.JavaScriptStringEncode((string)ViewState["ReferralCodeMissing"]); }
        set { ViewState["ReferralCodeMissing"] = value; }
    }
    protected string PhoneRegExpression
    {
        get { return Utilities.RegularExpressionForJavascript(PpcSite.GetCurrent().PhoneReg); }
    }
    protected string RegisterReferrer
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/SupplierReferrerInvitation"); }
    }
}