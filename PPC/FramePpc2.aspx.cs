﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class PPC_FramePpc2 : PageSetting
{
    RegionTab result;
    protected string root;
    protected string url;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            
            whatCityPage(siteSetting.GetSiteID);

            // /business/melech hafalafel/16e6c8e8-a34a-4703-a9b7-470e6812d492

            string _step = Request.QueryString["step"];
            Regex _rg = new Regex("^[1-6]$");
            iframe.Attributes.Add("height", @"1650px");
            if (!string.IsNullOrEmpty(_step) && _rg.IsMatch(_step))
            {
                if (_step == "6")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"PPC/AddCredit.aspx");
                else if (_step == "4")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalDates2014.aspx?mode=init");
                else if (_step == "1")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalDetails2014.aspx?mode=init");
                else if (_step == "2")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/categories2014.aspx?mode=init");
                else if (_step == "3")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/" + cityPage);
                else if (_step == "5")
                    iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalCallPurchase.aspx?mode=init");
            }
            else
                iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalDetails2014.aspx?mode=init");

            ClientScript.RegisterStartupScript(this.GetType(), "show_div", "showDiv();", true);

            div_height.Style.Add(HtmlTextWriterStyle.Height, "900px");

            root = ResolveUrl("~");

            if (Request.Url.Host == "localhost")
            {
                url = Request.Url.Host + ":" + Request.Url.Port + root;
            }

            else if (Request.Url.Host == "qa.ppcnp.com")
            {
                url = "http://" + Request.Url.Host + root;
            }

            else
            {
                url = "http://www.noproblem.me" + root;
            }

           getCompanyLink();

            Page.Header.DataBind();
            //    iframe.Attributes.Add("onload", "OnIframeLoad();");
            
        }
    }

    private void getCompanyLink()
    {
        //<a href="<%#root%>business/<%#Utilities.cleanSpecialCharsUrlFreindly3(Eval("Name").ToString().Trim())%>/<%#Eval("Id")%>"><%#Utilities.setfirstCapital(Eval("Name").ToString().Trim())%></a>

         WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
            WebReferenceSupplier.ResultOfGetBusinessProfileResponse resultOfGetBusinessProfileResponse = new WebReferenceSupplier.ResultOfGetBusinessProfileResponse();

            try
            {            
                resultOfGetBusinessProfileResponse = _supplier.GetBusinessProfile(new Guid(GetGuidSetting()));
                linkBusiness.HRef = url + "business/" + Utilities.cleanSpecialCharsUrlFreindly3(resultOfGetBusinessProfileResponse.Value.Name.Trim()) + "/" + GetGuidSetting();            

            }

            catch(Exception ex)
            {
                dbug_log.ExceptionLog(ex);
            }
    }

    private void whatCityPage(string SiteId)
    {
        result = (RegionTab)DBConnection.GetRegionTab(SiteId);

    }


    protected string cityPage
    {
        get
        {
            /*
            if (result == RegionTab.Map)
            {
                //return "LocationMap.aspx";
                return "location2014.aspx";
                
                
            }
            else if (result == RegionTab.Tree)
                return "professionalCities3.aspx";
            else
                return "professionalCities3.aspx";
            */

            return "location2014.aspx";
        }


    }
    protected string GetPaymentInfo
    {
        get { return Server.HtmlEncode("Add Credit"); }
    }
    protected string GetSegmentsArea
    {
        get { return Server.HtmlEncode("Categories"); }
    }
    protected string GetCities
    {
        get { return Server.HtmlEncode("Cover area"); }
    }
    protected string Get_Time
    {
        get { return Server.HtmlEncode("Availability"); }
    }
    protected string Get_MyCredits
    {
        get { return Server.HtmlEncode("Set Price"); }
    }

    protected string Get_AccountSettings
    {
        get { return Server.HtmlEncode("Account Settings"); }
    }

    


    protected string Get_GeneralInfo
    {
        get { return Server.HtmlEncode("Business profile"); }
    }
    
}