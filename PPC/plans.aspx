﻿<%@ Page Language="C#" MasterPageFile="~/Controls/MasterPageNPTemp.master" AutoEventWireup="true" CodeFile="plans.aspx.cs" Inherits="PPC_plans" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNPTemp.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
<script type="text/javascript" src="script/jquery-ui-1.8.16.custom.autocomplete.min.js"></script>
<script type="text/javascript" src="../CallService.js"></script>
<script type="text/javascript" src="../general.js"></script>

<script type="text/javascript" src="script/JSGeneral.js"></script>

<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript" src="script/HistoryManager.js"></script>        
<link href="../Formal/jquery-ui-1.8.16.custom.css" type="text/css" rel="Stylesheet" />
<script type="text/javascript">    
  
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <div runat="server" id="div_c" class="div_TopSpace">
   
        
        <div class="titleFirst">Choose a plan</div>
        <div class="clear"></div>

        <div style="width:922px;" >
            <div style="float:left;">               
                <asp:ImageButton ID="featuredListingPerZipCode"  runat="server" 
                    ImageUrl="images/featuredListingPerZipCode.png" 
                    onclick="featuredListingPerZipCode_Click"/>
            </div>
        
            <div style="display:inline-block;margin-left:11px;">
                
                <asp:ImageButton ID="leadBoosterPerZipCode"  runat="server" 
                    ImageUrl="images/leadBoosterPerZipCode.png" 
                    onclick="leadBoosterPerZipCode_Click"/> 
            </div>
        </div>
      
    </div>

 
   
   
</asp:Content>

