﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_beforeEnterance : PageSetting
{
    

    protected void Page_Load(object sender, EventArgs e)
    {

        double balance;
        balance = (Session["Balance2014"] == null) ? 0 : Convert.ToDouble(Session["Balance2014"]);

        try
        {
            userManagement = new UserMangement(Session["RegisterUser2014"].ToString(), Session["Username2014"].ToString(), balance, true);
        }

        catch(Exception ex) // may be lost session
        {
            dbug_log.ExceptionLog(ex);

            Response.Redirect("../ppc/ppcLogin.aspx");
           
        }

        userManagement.SetUserObject(this);
        
        string plan=LoadPlan((Guid)Session["RegisterUser2014"]);

        if (plan == null)
            Session["plan"] = "LEAD_BOOSTER_AND_FEATURED_LISTING";
        else
            Session["plan"] = plan;       

        if (plan == "FEATURED_LISTING")
            Response.Redirect("../ppc/FramePpc2.aspx");
        else
            Response.Redirect("../Management/overview2.aspx");
        

    }

    private string LoadPlan(Guid SupplierId)
    {
        string plan="";

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfString resultOfString = new WebReferenceSupplier.ResultOfString();

        try
        {
            resultOfString = _supplier.Checkout_GetPlan_Registration2014(SupplierId);
            plan = resultOfString.Value;
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);

        }

        return plan;

    }
}