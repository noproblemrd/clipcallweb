﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class PPC_FramePpcBilling : PageSetting
{
    RegionTab result;    

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {       

            string _step = Request.QueryString["step"];
           
            iframe.Attributes.Add("src", ResolveUrl("~") + @"ppc/BillingOverview2.aspx");

            ClientScript.RegisterStartupScript(this.GetType(), "show_div", "showDiv();", true);

            div_height.Style.Add(HtmlTextWriterStyle.Height, "900px");          

            Page.Header.DataBind();
           

        }
    }    
    

    protected string Get_Overview
    {
        get { return Server.HtmlEncode("Billing overview"); }
    }
    
    protected string GetPriceSettings
    {
        get { return Server.HtmlEncode("Default price settings"); }
    }

    protected string GetPaymentMethod
    {
        get { return Server.HtmlEncode("Payment method"); }
    }

    protected string GetTransactions
    {
        get { return Server.HtmlEncode("Transactions"); }
    }
    
    
}