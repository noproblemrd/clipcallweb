﻿
function ifExplorer() {
    var boolExplorer = false;
    if (navigator.userAgent.search("MSIE") >= 0 || navigator.userAgent.search("Trident/") >= 0) {
        boolExplorer = true;
    }

    return boolExplorer;
}

function ifExplorer7() {
    var boolExplorer7 = false;

    if (navigator.userAgent.search("MSIE") >= 0) {
        var position = navigator.userAgent.search("MSIE") + 5;
        var end = navigator.userAgent.search("; Windows");
        var version = navigator.userAgent.substring(position, end);

        if (version == 7)
            boolExplorer7 = true;
    }

    return boolExplorer7;
}

function ifExplorer8AndLess() {
   
    var boolExplorer8AndLess = false;
    
    if (navigator.userAgent.search("MSIE") >= 0) {
        var position = navigator.userAgent.search("MSIE") + 5;
        var end = navigator.userAgent.search("; Windows");
        var version = navigator.userAgent.substring(position, end);       

        if (parseInt(version) <= 8)
            boolExplorer8AndLess = true;
    }   

    return boolExplorer8AndLess;
}

function showHideVideo() {
    //alert("showHideVideo" + $('#registrationContainerVideoIframe').attr('id') + $('#registrationContainerVideoIframe').is(":visible"));
    
    if ($('#registrationContainerVideoIframe').is(":visible")) {

        $('#registrationContainerVideoIframe').hide();

        $('#registrationContainerVideoIframe').css('display', 'none');

        $("#iframeVideo").attr({ src: "", height: "0px" });

        $(".seret").show();
        $(".registrationContainerVideo").show();

        $(".x-close").hide();

        $(".x-close").css('display', 'none');

        if (ifExplorer7()) {
            $('.registration').css('height', '666px');
        }

    }
    else {

        //$('#registrationContainerVideoIframe').hide();
        
        if (ifExplorer7()) {            
            $('#registrationContainerVideoIframe').css({ 'display': 'block', 'height': '500px' });
            $('.registration').css('height', '1200px');

        }
        else {           
            $('#registrationContainerVideoIframe').css({ 'display': 'table-caption', 'height': '500px' });
        }

        $("#iframeVideo").attr({ 'src': 'http://www.youtube.com/embed/97q0JRDvuik?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0', 'height': '485.761' });

        $(".seret").hide();
        $(".registrationContainerVideo").hide();
        $(".x-close").css('display', 'block');

        
    }

}

function showHideVideo2(videoSrc) {
    //alert(videoSrc);
    //alert("showHideVideo" + $('#registrationContainerVideoIframe').attr('id') + $('#registrationContainerVideoIframe').is(":visible"));

    if ($('#registrationContainerVideoIframe').is(":visible")) {

        $('#registrationContainerVideoIframe').hide();

        $('#registrationContainerVideoIframe').css('display', 'none');

        $("#iframeVideo").attr({ src: "", height: "0px" });

        $(".seret").show();
        $(".registrationContainerVideo").show();

        $(".x-close").hide();

        $(".x-close").css('display', 'none');

        if (ifExplorer7()) {
            $('.registration').css('height', '666px');
        }

        closeVideo();
    }
    else {

        //$('#registrationContainerVideoIframe').hide();

        if (ifExplorer7()) {
            $('#registrationContainerVideoIframe').css({ 'display': 'block', 'height': '500px' });
            $('.registration').css('height', '1200px');

        }
        else {
            $('#registrationContainerVideoIframe').css({ 'display': 'table-caption', 'height': '500px' });
        }

        //$("#iframeVideo").attr({ 'src': 'http://www.youtube.com/embed/97q0JRDvuik?autoplay=1&autohide=1&hd=1&vq=hd720&rel=0', 'height': '485.761' });

        $(".seret").hide();
        $(".registrationContainerVideo").hide();
        $(".x-close").css('display', 'block');        
        
        
        if (document.getElementById("videoClose") != null)
        {            
            //window.location.hash = "videoAnchor";

            $('html, body').animate({
                scrollTop: $("#videoClose").offset().top - 50
            }, 1000);

            
        }
            
        
        showVideo(videoSrc);
    }

}

function get_cookie(cookie_name) {
    var results = document.cookie.match('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

    if (results)
        return (unescape(results[2]));
    else
        return null;
}

function set_cookie(name, value, exp_y, exp_m, exp_d, exp_hh, exp_mm, exp_ss, path, domain, secure) {

    var cookie_string = name + "=" + escape(value);

    if (exp_y) {
        var expires = new Date(exp_y, exp_m, exp_d, exp_hh, exp_mm, exp_ss);

        cookie_string += ";expires=" + expires.toGMTString();

    }

    if (path)
        cookie_string += "; path=" + escape(path);

    if (domain)
        cookie_string += "; domain=" + escape(domain);

    if (secure)
        cookie_string += "; secure";


    document.cookie = cookie_string;

}

var current_date = new Date();
var cookie_year = current_date.getFullYear();
cookie_year = cookie_year + 3;
var cookie_month = current_date.getMonth();
var cookie_day = current_date.getDate();
var cookie_hh = current_date.getHours();
var cookie_mm = current_date.getMinutes();
var cookie_ss = current_date.getSeconds(); 


function setLoginCookies() { 

    if ($('#chk_remember').attr('checked')) {
        var email = sjcl.encrypt("email", $('#txt_email').attr("value"));        
        var password = sjcl.encrypt("password", $('#txt_password').attr("value"));
        // set cookies to expire in 14 days
        set_cookie('email', email, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);
        set_cookie('password', password, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);
        set_cookie('remember', true, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);
    } else {
        // reset cookies
        set_cookie('email', null, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);
        set_cookie('password', null, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);
        set_cookie('remember', null, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);
    }
}

function setPasswordCookie() {        
        var password = sjcl.encrypt("password", $('#txt_password').attr("value"));
        set_cookie('password', password, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);    
    
}

function setPasswordCookieFirstOfTwo() {
    var password = sjcl.encrypt("password", $('#txt_passwordFirstOfTwo').attr("value"));
    set_cookie('password', password, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);

}


function preCheckRegistrationStep(source) {
    
    var email = document.getElementById("txt_email").value;
    var password = document.getElementById("txt_password").value;

    var flavor;
    flavor = document.getElementById("hdn_flavor");
    if (flavor)
        flavor = document.getElementById("hdn_flavor").value;
    else
        flavor = "";

    if ($.trim(email) == "") {

        $("#div_EmailComment2").show();

        if (source == "regularRegister")
            _gaq.push(['_trackEvent', 'Join us: Validation', 'empty email',flavor]);
        else if (source == "regularLogin")
            _gaq.push(['_trackEvent', 'Login: Validation', 'empty email', flavor]); 
            
        return;
    }


    else if (!checkEmail(email)) {
        $("#div_EmailComment").show();

        if (source == "regularRegister")
            _gaq.push(['_trackEvent', 'Join us: Validation', 'wrong email format', flavor]);
        else if (source == "regularLogin")
            _gaq.push(['_trackEvent', 'Login: Validation', 'wrong email format', flavor]); 

        return;
    }


    if (!checkPassword(password)) {
        $("#div_PasswordComment").show();

        if (source == "regularRegister")
            _gaq.push(['_trackEvent', 'Join us: Validation', 'wrong password', flavor]);
        else if (source == "regularLogin")
            _gaq.push(['_trackEvent', 'Login: Validation', 'wrong password', flavor]); 

        return;
    }

    if(source!="regularRegister")
        setLoginCookies();
    
    checkRegistrationStep(email, '', '', password, source);    
    activeButtonAnim('.progress-demo #linkRegistration', 200);
    
}



function preCheckRegistrationStep_another(source) {

    var email = document.getElementById("txt_email_another").value;
    var password = document.getElementById("txt_password_another").value;

    var flavor;
    flavor = document.getElementById("hdn_flavor");
    if (flavor)
        flavor = document.getElementById("hdn_flavor").value;
    else
        flavor = "";

    if ($.trim(email) == "") {

        $("#div_EmailComment2_another").show();

        if (source == "regularRegister")
            _gaq.push(['_trackEvent', 'Join us: Validation', 'empty email', flavor]);
        else if (source == "regularLogin")
            _gaq.push(['_trackEvent', 'Login: Validation', 'empty email', flavor]);

        return;
    }


    else if (!checkEmail(email)) {
        $("#div_EmailComment_another").show();

        if (source == "regularRegister")
            _gaq.push(['_trackEvent', 'Join us: Validation', 'wrong email format', flavor]);
        else if (source == "regularLogin")
            _gaq.push(['_trackEvent', 'Login: Validation', 'wrong email format', flavor]);

        return;
    }


    if (!checkPassword(password)) {
        $("#div_PasswordComment_another").show();

        if (source == "regularRegister")
            _gaq.push(['_trackEvent', 'Join us: Validation', 'wrong password', flavor]);
        else if (source == "regularLogin")
            _gaq.push(['_trackEvent', 'Login: Validation', 'wrong password', flavor]);

        return;
    }

    if (source != "regularRegister")
        setLoginCookies();

    checkRegistrationStep(email, '', '', password, source);
    activeButtonAnim('.progress-demo #linkRegistration_another', 200);

}


function checkEmail(strEmail) {
    var ifValidEmail = true;
    //var regFormatEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    var regFormatEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (strEmail.search(regFormatEmail) == -1) //if match failed
    {
        ifValidEmail = false;
    }

    return ifValidEmail;
}

function checkPassword(password) {

    var ifCheckPassword = $("#txt_password").attr('ifCheckPassword');

    var ifValidPassword = true;

    if (ifCheckPassword != "no") {
        if ($.trim(password).length < 6)
            ifValidPassword = false;
    }

    else {
            if($.trim(password).length==0)
                ifValidPassword = false;
        
    }

    return ifValidPassword;
}

function checkPassword_another(password) {

    var ifCheckPassword = $("#txt_password_another").attr('ifCheckPassword');

    var ifValidPassword = true;

    if (ifCheckPassword != "no") {
        if ($.trim(password).length < 6)
            ifValidPassword = false;
    }

    else {
        if ($.trim(password).length == 0)
            ifValidPassword = false;

    }

    return ifValidPassword;
}

function checkPasswordsEquel(firstPassword, secondPassword) {
    if (firstPassword == secondPassword)
        return true;
    else
        return false;
}

function forgotPassword() {
    
    var email = document.getElementById("txt_email").value;    

    if (!checkEmail(email)) {
        $("#div_EmailComment").show();
        return false;
    }

    activeButtonAnim('.progress-demo #btn_SendService', 200);

    //alert("return");
    return true;

}

/******* slide instruction ********/
/*
function SlideElement(_elemId, ToSlide) {
    if (typeof ($("#" + _elemId).data('sliding')) == 'undefined')
        $("#" + _elemId).data('sliding', false);
    if ($("#" + _elemId).data('sliding') == ToSlide)
        return;
    $("#" + _elemId).data('sliding', ToSlide)
    if (ToSlide) {
        //if ($("#" + _elemId + ":hidden").length == 0)
        $("#" + _elemId).show('slide', { direction: "down" }, 500);
    }
    else {
        // if (!$("#" + _elemId + ":visible").data('sliding')) {
        $("#" + _elemId).hide('slide', { direction: "down" }, 500);
        // $("#" + _elemId + ":visible").data('sliding', true);
        // }
    }
} 
*/

function slideElement(_elemId) {
    $("#" + _elemId).show('slide', { direction: "down" }, 500);
}


function checkPhone(phoneNumber) {
    var blnPhone = false;
    var regFormatPhone = document.getElementById("hidden_phoneFormat").value;
    if (phoneNumber.search(regFormatPhone) == -1) //if match failed
    {
        blnPhone = false;
    }

    else
        blnPhone = true;

    return blnPhone;
}


function initLogin() {
    var email = sjcl.decrypt("email", get_cookie('email'));
    var password = sjcl.decrypt("password", get_cookie('password'));
    var prePopulatedEmail = $("#hdn_prePopulatedEmail").val();
    var remember = get_cookie('remember');   

    if (prePopulatedEmail.length > 0) {
       
        if (email != prePopulatedEmail) { // the current cookie email is different from the prepopulated email            
            // the email comes from c# query string
            $('#txt_password').attr("value", "");
            $('#chk_remember').attr('checked', false);
        }

        else { // the current cookie email is equel to the prepopulated email
            // the email comes from c# query string
           
            $('#txt_password').attr("value", password); // get the cookie
            $('#chk_remember').attr('checked', true); //
        }
    }

    else if (remember == 'true') {       
       

        // autofill the fields
        //alert($('#txt_email').attr("value"));
        $('#txt_email').attr("value", email);
        //alert($('#txt_email').attr("value"));
       
        $('#txt_password').attr("value", password);       
        $('#chk_remember').attr('checked', true);
        
    }
}

function nameValidation() {
    
    var ifValidName = false;
    if ($.trim($("#txt_name").val()).length > 0) {
        $("#div_NameSmile").show();
        $("#div_NameError").hide();

        $("#div_NameComment").hide();

        ifValidName = true;
    }
    else {
        $("#div_NameSmile").hide();
        $("#div_NameError").show();

        $("#div_NameComment").show();

        ifValidName = false;
    }
    
    return ifValidName;
}

function contactPersonValidation() {

    var ifValidName = false;
    if ($.trim($("#txt_contactPerson").val()).length > 0) {
        $("#div_ContactPersonSmile").show();
        $("#div_ContactPersonError").hide();

        $("#div_ContactPersonComment").hide();

        ifValidName = true;
    }
    else {
        $("#div_ContactPersonSmile").hide();
        $("#div_ContactPersonError").show();

        $("#div_ContactPersonComment").show();

        ifValidName = false;
    }

    return ifValidName;
}


function regionValidation() {
    var ifValidRegion = false;
    if (clickedFromList == true && $.trim($("#txt_Region").val()).length > 0) {
        $("#div_AddressSmile").show();
        $("#div_AddressError").hide();
        $("#div_AddressComment").hide();

        ifValidRegion = true;
    }

    else {
        $("#div_AddressSmile").hide();
        $("#div_AddressError").show();
        $("#div_AddressComment").show();

        ifValidRegion = false;
    }

    return ifValidRegion;
}

function mobileValidation() {
    
    var ifValidMobile = false;

    if (checkPhone($.trim($("#txt_mobile").val()))) {
        $("#div_MobileSmile").show();
        $("#div_MobileError").hide();
        $("#div_MobileComment").hide();

        ifValidMobile = true;
    }

    else {
        $("#div_MobileSmile").hide();
        $("#div_MobileError").show();
        $("#div_MobileComment").show();

        ifValidMobile = false;
    }

    return ifValidMobile;
}

function categoryValidation() {
    var ifValidCategory = false;
    if ($.trim($("#txt_Category").val()).length > 0 && !$('#div_ServiceComment').is(":visible")) {
        $("#div_ServiceSmile").show();
        $("#div_ServiceError").hide();

        $("#div_ServiceComment").hide();

        ifValidCategory = true;
    }
    else {
        $("#div_ServiceSmile").hide();
        $("#div_ServiceError").show();

        $("#div_ServiceComment").show();

        ifValidCategory = false;
    }

    return ifValidCategory;

}


function businessDetailsValidation() {

    if (nameValidation() == true & regionValidation() == true & mobileValidation() == true & contactPersonValidation()==true) {
        activeButtonAnim('.progress-demo #NextBusinessDetails', 200);      
        return true;
    }
    else {        
        return false;
    }

    
    
}

function businessDetailsValidationAll(linkObj) {

    if (nameValidation() == true & regionValidation() == true & mobileValidation() == true & emailValidation(document.getElementById("txt_email").value) == true & paymentsValidation()== true) {
        activeButtonAnim(linkObj, 200);
        return true;
    }
    else {
        return false;
    }

}

var l;
var interval;


function stopActiveButtonAnim() {

    if (!ifExplorer()) {
        if (l) {
            clearInterval(interval);
            l.stop();
        }
    }
}


function activeButtonAnim(obj, intervalProgressTime) {
    if (!ifExplorer()) {
        //  var l = Ladda.create(document.querySelector());
        l = Ladda.create(document.querySelector(obj));
        // Start loading
        l.start();

        var progress = 0;
        interval = setInterval(function () {
            progress = Math.min(progress + Math.random() * 0.1, 1);
            l.setProgress(progress);

            if (progress === 1) {
                progress = 0;
                l.start();
                //clearInterval(interval);
            }
        }, intervalProgressTime);
    }
}


/*
function activeButtonAnim(obj, intervalProgressTime) {
    if (!ifExplorer()) {
        //alert("dfgdf");
        var l = Ladda.create(document.querySelector());

        l = Ladda.create(document.querySelector(obj));

        // Start loading

        l.start();

        var progress = 0;


        interval = setInterval(function () {

            progress = Math.min(progress + Math.random() * 0.1, 1);

            l.setProgress(progress);


            if (progress == 1) {
                progress = 0;
                l.start();


                //clearInterval(interval);
            }


        }, intervalProgressTime);

    }
}
*/




function emailValidation(email) {
    
    if (checkEmail(email)) {

        $("#div_EmailSmile").show();
        $("#div_EmailError").hide();

        //$($(this)).css({ 'color': '#000', 'font-weight': '600', 'box-shadow': 'none' });

        return true;
    }

    else {
        $("#div_EmailSmile").hide();
        $("#div_EmailError").show();

        //$($(this)).css({ 'color': '#FB9024', 'font-weight': '600', 'box-shadow': '0 0 5px 1px #FB9024' });

        return false;
    }
}

function paymentsValidation() {
    if (!$('#chkAmex').is(':checked') && !$('#chkMasterCard').is(':checked') && !$('#chkVisa').is(':checked') && !$('#chkCash').is(':checked')) {
        $("#commentPayments").show();
        return false;
    }
    else {
        $("#commentPayments").hide();
        return true;
    }      

}

function checkToDateToLaterFromDate(fromHour, fromMinute, toHour, toMinute) {
    //alert(fromHour + " " + toHour);
    if (fromHour == 24 && toHour==24)
    {
        fromHour = "0";
        toHour= "0";
    }

    else if (fromHour == 24)
        fromHour = "0";

    //alert(fromHour + " " + toHour);
    var tempFromHour;
    var tempFromMinute;
    var tempToHour;
    var tempToMinute;
    var fromTime;
    var toTime;

    if (fromHour.length == 1)
        fromHour = "0" + fromHour;

    if (fromMinute.length == 1)
        fromMinute = "0" + fromMinute;

    if (toHour.length == 1)
        toHour = "0" + toHour;

    if (toMinute.length == 1)
        toMinute = "0" + toMinute;

    fromTime = fromHour + ":" + fromMinute;
    
    toTime = toHour + ":" + toMinute;


    if (fromTime < toTime)
        return true;
    else { 
        if (fromTime == toTime && fromMinute < toMinute)
            return true;
        else
            return false;
    }   
}

/********************************** embed swf player **********************/
var params = { allowScriptAccess: "always" };
var atts = { id: "myytplayer" };

function showVideo(videoSrc) {

        // https://developers.google.com/youtube/js_api_reference

        var c = document.getElementById("ytapiplayer");

        if (!c) {
            var d = document.createElement("div");
            d.setAttribute("id", "ytapiplayer");
            document.getElementById("registrationContainerVideoIframe").appendChild(d);
        }

        /*
        swfobject.embedSWF("http://www.youtube.com/v/2lnZoIF2sVE?enablejsapi=1&playerapiid=ytplayer&version=3&autoplay=1&autohide=1&controls=1;",
        "ytapiplayer", "637", "359", "8", null, null, params, atts);
        */


        if (swfobject.hasFlashPlayerVersion("8")) {
            // has Flash
        }
        else {

            //if (!document.getElementById("commentDownloadFlash")) {
               
                var commentDownloadFlash = document.createElement("div");

                
                    commentDownloadFlash.setAttribute("id", "commentDownloadFlash");
                    //d.innerHTML = ""; 
                    commentDownloadFlash.innerHTML = "<br> You need to install Adobe Flash Player or check other solutions. See <a style='font-size:16px;' href='http://helpx.adobe.com/flash-player.html' target='_blank'>instructions.</a><br><br>";
                    document.getElementById("ytapiplayer").appendChild(commentDownloadFlash);
                
            //}
        }

        swfobject.embedSWF(videoSrc,
                "ytapiplayer", "870", "485.761", "8", null, null, params, atts);
                    
        //alert(p);
        //http://www.youtube.com/embed/2lnZoIF2sVE?enablejsapi=1&playerapiid=ytplayer&version=3&autoplay=1 // see it's defferent from the formal whrer the link is

             

}

function closeVideo() {    
    swfobject.removeSWF('myytplayer');    

    var commentDownloadFlash = document.getElementById('commentDownloadFlash');
    if (commentDownloadFlash)
        document.getElementById("ytapiplayer").removeChild(commentDownloadFlash);
}


function onYouTubePlayerReady(playerId) {
    //alert("onit");
    ytplayer = document.getElementById("myytplayer");
    ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
}

function onytplayerStateChange(newState) {

    //Possible values are unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5)
    //_trackEvent(category, action, opt_label, opt_value, opt_noninteraction)
    var flavor;
    flavor = document.getElementById("hdn_flavor");
    if (flavor)
        flavor = document.getElementById("hdn_flavor").value;
    else
        flavor = "";

    if (newState == 1) // palying
    {
        _gaq.push(['_trackEvent', 'Join us: Video', 'playing', flavor]);  
    }

    if (newState == 0) { // end
        _gaq.push(['_trackEvent', 'Join us: Video', 'end play', flavor]);             
    }
}

/********************************** end embed swf player **********************/


$(document).ready(function () {
    $("#txt_email").blur(function () {

        if ($.trim($(this).val()) != "") {
            $("#div_EmailComment2").hide();


            if (checkEmail($(this).val())) {

                $("#div_EmailSmile").show();
                $("#div_EmailError").hide();

                $("#div_EmailComment").hide();
                //$($(this)).css({ 'color': '#000', 'font-weight': '600', 'box-shadow': 'none' });

            }

            else {
                $("#div_EmailSmile").hide();
                $("#div_EmailError").show();

                $("#div_EmailComment").show();

                //$($(this)).css({ 'color': '#FB9024', 'font-weight': '600', 'box-shadow': '0 0 5px 1px #FB9024' });
            }
        }


        else
            $("#div_EmailComment2").show();


    }
    )


    $("#txt_email_another").blur(function () {

        if ($.trim($(this).val()) != "") {
            $("#div_EmailComment2_another").hide();


            if (checkEmail($(this).val())) {

                $("#div_EmailSmile_another").show();
                $("#div_EmailError_another").hide();

                $("#div_EmailComment_another").hide();
                //$($(this)).css({ 'color': '#000', 'font-weight': '600', 'box-shadow': 'none' });

            }

            else {
                $("#div_EmailSmile_another").hide();
                $("#div_EmailError_another").show();


                if ($("#div_EmailComment_another").height() > 30)
                    $("#div_EmailComment_another").addClass('inputComment2Lines');
                else
                    $("#div_EmailComment_another").removeClass('inputComment2Lines');

                $("#div_EmailComment_another").show();
                //$("#div_EmailComment_another").css('display','block');
                //$($(this)).css({ 'color': '#FB9024', 'font-weight': '600', 'box-shadow': '0 0 5px 1px #FB9024' });

                //alert($("#div_EmailComment_another").height());


                
            }
        }


        else
            $("#div_EmailComment2_another").show();


    }
    )



    $("#txt_email").focus(function () {
        $("#div_EmailSmile").hide();
        $("#div_EmailError").hide();
        $("#div_EmailComment").hide();    
        
           
    }
    )
   
    $("#txt_email_another").focus(function () {
        $("#div_EmailSmile_another").hide();
        $("#div_EmailError_another").hide();
        $("#div_EmailComment_another").hide();


    }
    )

    $("#txt_password").blur(function () {        

        var ifCheckPassword = $("#txt_password").attr('ifCheckPassword');

        if (ifCheckPassword != "no") {
            if (checkPassword($(this).val())) {

                $("#div_PasswordSmile").show();
                $("#div_PasswordError").hide();

                $($(this)).css({ 'color': '#000', 'font-weight': '600', 'box-shadow': 'none' });

                $("#div_PasswordComment").hide();
            }



            else {

                $("#div_PasswordSmile").hide();
                $("#div_PasswordError").show();

                $($(this)).css({ 'color': '#FB9024', 'font-weight': '600', 'box-shadow': '0 0 5px 1px #FB9024' });

                $("#div_PasswordComment").show();


            }
        }

        else
            $("#div_PasswordComment").hide();
    }
    )

    $("#txt_password_another").blur(function () {

        var ifCheckPassword = $("#txt_password_another").attr('ifCheckPassword');

        if (ifCheckPassword != "no") {
            if (checkPassword_another($(this).val())) {

                $("#div_PasswordSmile_another").show();
                $("#div_PasswordError_another").hide();

                $($(this)).css({ 'color': '#000', 'font-weight': '600', 'box-shadow': 'none' });

                $("#div_PasswordComment_another").hide();
            }



            else {

                //$("#div_PasswordSmile_another").hide();
                //$("#div_PasswordError_another").show();

                $($(this)).css({ 'color': '#FB9024', 'font-weight': '600', 'box-shadow': '0 0 5px 1px #FB9024' });

                
                if ($("#div_PasswordComment_another").height() > 30)
                    $("#div_PasswordComment_another").addClass('inputComment2Lines');
                else
                    $("#div_PasswordComment_another").removeClass('inputComment2Lines');
                

                $("#div_PasswordComment_another").show();
                //alert(1);

                /*
                $("#div_PasswordComment_another").css('display','block');
                $("#div_PasswordComment_another").css('top','-61px');
                */
                
                
            }
        }

        else
            $("#div_PasswordComment_another").hide();
    }
    )

    $("#txt_passwordFirstOfTwo").blur(function () {

        if (checkPassword($(this).val())) {
            $("#div_PasswordSmile").show();
            $("#div_PasswordError").hide();

            //$($(this)).css({ 'color': '#000', 'font-weight': '600', 'box-shadow': 'none' });
        }

        else if (!checkPasswordsEquel($(this).val(), $("#txt_passwordSecondOfTwo").val())) {

            $("#div_PasswordSmile").hide();
            $("#div_PasswordError").show();

            //$($(this)).css({ 'color': '#000', 'font-weight': '600', 'box-shadow': 'none' });
        }

        else {

            $("#div_PasswordSmile").hide();
            $("#div_PasswordError").show();

            //$($(this)).css({ 'color': '#FB9024', 'font-weight': '600', 'box-shadow': '0 0 5px 1px #FB9024' });
        }
    }
    )

    $("#txt_passwordSecondOfTwo").blur(function () {

        if (!checkPassword($(this).val())) {
            $("#div_PasswordSmile2").hide();
            $("#div_PasswordError2").show();
        }

        else if (checkPasswordsEquel($(this).val(), $("#txt_passwordFirstOfTwo").val())) {

            $("#div_PasswordSmile2").show();
            $("#div_PasswordError2").hide();

            //$($(this)).css({ 'color': '#000', 'font-weight': '600', 'box-shadow': 'none' });
        }

        else {

            $("#div_PasswordSmile2").hide();
            $("#div_PasswordError2").show();

            //$($(this)).css({ 'color': '#FB9024', 'font-weight': '600', 'box-shadow': '0 0 5px 1px #FB9024' });
        }
    }
    )

    /*
    $("#txt_email").keyup(function () {
    $("#div_EmailSmile").hide();
    if (!checkEmail($(this).val())) {
    $("#div_EmailComment").show();
    }

    else
    $("#div_EmailComment").hide();
    }
    )
    */


    $("#txt_password").keyup(function (e) {
        //alert(e.keyCode);
        var ifCheckPassword = $("#txt_password").attr('ifCheckPassword');
        //var mode = $("#txt_password").attr('mode');
        //alert(mode);
        if (ifCheckPassword != "no") { // register mode
            $("#div_PasswordSmile").hide();
            $("#div_PasswordError").hide();

            if (!checkPassword($(this).val())) {
                $("#div_PasswordComment").show();
                $("#div_PasswordError").show();
            }

            else {
                $("#div_PasswordComment").hide();
                $("#div_PasswordSmile").show();
                if (e.keyCode == 13)
                    preCheckRegistrationStep('regularRegister');

            }
        }

        else { // login mode
            $("#div_PasswordComment").hide();
            if (e.keyCode == 13)
                preCheckRegistrationStep('regularLogin');
        }
    }
    )

    $("#txt_password_another").keyup(function (e) {
        //alert(e.keyCode);
        var ifCheckPassword = $("#txt_password_another").attr('ifCheckPassword');
        //var mode = $("#txt_password").attr('mode');
        //alert(mode);
        if (ifCheckPassword != "no") { // register mode
            $("#div_PasswordSmile_another").hide();
            $("#div_PasswordError_another").hide();

            if (!checkPassword_another($(this).val())) {

                if ($("#div_PasswordComment_another").height() > 30)
                    $("#div_PasswordComment_another").addClass('inputComment2Lines');
                else
                    $("#div_PasswordComment_another").removeClass('inputComment2Lines');

                $("#div_PasswordComment_another").show();
                $("#div_PasswordError_another").show();

               
            }

            else {
                $("#div_PasswordComment_another").hide();
                $("#div_PasswordSmile_another").show();

               

                if (e.keyCode == 13)
                    preCheckRegistrationStep('regularRegister');

            }
        }

        else { // login mode
            $("#div_PasswordComment_another").hide();
            if (e.keyCode == 13)
                preCheckRegistrationStep('regularLogin');
        }
    }
    )

    $("#txt_passwordFirstOfTwo").keyup(function () {

        if (!checkPassword($(this).val())) {
            $("#div_PasswordComment").show();
        }

        else
            $("#div_PasswordComment").hide();
    }
    )


    $("#txt_name").keyup(function () {
        nameValidation();
    }
    )


    $("#txt_contactPerson").keyup(function () {

        contactPersonValidation();
    }
    )


    $("#txt_contactPerson").keyup(function () {


    }
    )

    $("#txt_Region").blur(function () {
        regionValidation();
    }
    )

    $("#txt_mobile").keyup(function () {

        if (checkPhone($.trim($(this).val()))) {
            $("#div_MobileSmile").show();
            $("#div_MobileComment").hide();
            $("#div_MobileError").hide();

            ifValidMobile = true;
        }

        else {
            $("#div_MobileSmile").hide();
            $("#div_MobileComment").show();

            ifValidMobile = false;
        }

    }
    )

    $("#txt_mobile").blur(function () {
        mobileValidation();
    }
    )

    $('#chkAmex, #chkMasterCard, #chkVisa, #chkCash').click(function () {
        $("#commentPayments").hide();

        if (!$('#chkAmex').is(':checked') && !$('#chkMasterCard').is(':checked') && !$('#chkVisa').is(':checked') && !$('#chkCash').is(':checked')) {
            $("#commentPayments").show();
        }
    }
    )

}
)