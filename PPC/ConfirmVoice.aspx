﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNP.master" AutoEventWireup="true" CodeFile="ConfirmVoice.aspx.cs" Inherits="PPC_ConfirmVoice" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNP.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" src="../general.js"></script>
<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript">
    function ResendCode() {
        $.ajax({
            url: "<%# GetSendMobileService %>",
            data: "{}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                switch (data.d) {
                    case ('false'):
                    case ('faild'):
                        SetServerComment('<%# GetServerErrorSendToPhone %>');
                        break;
                    case ('true'):
                        SetServerComment('<%# PasswordSent %>');
                        break;

                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                GeneralServerError();
            }

        });
    }
    function SendCode() {
        var _code = $.trim($('#<%# txt_password.ClientID %>').val());
        $('#<%# txt_password.ClientID %>').val(_code);
        $('span.btn_next').show();
        $('a.btn_next').hide();
        $.ajax({
            url: "<%# ApprovePassword %>",
            data: "{'password': '" + _code + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var result = eval('(' + data.d + ')');
                switch (result.status) {
                    case ('error'):
                        $('span.btn_next').hide();
                        $('a.btn_next').show();
                        GeneralServerError();
                        break;
                    case ('false'):
                        $('span.btn_next').hide();
                        $('a.btn_next').show();
                        SetServerComment('<%# WrongCode %>');
                        break;
                    case ('true'):
                        //window.location.href = '<%# ServicePage %>';
                        window.location.href = '<%# PlansPage %>';                        
                        break;
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('span.btn_next').hide();
                $('a.btn_next').show();
                GeneralServerError();
            }
        });
    }
    function ChangeNum() {
        $.ajax({
            url: "<%# ChangePhoneInRegistration %>",
            data: "{}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                switch (data.d) {
                    case ('faild'):
                        GeneralServerError();
                        break;
                    case ('success'):
                        window.location.href = '<%# GetChangeNumDestination %>';
                        break;

                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                GeneralServerError();
            }

        });
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="ConfirmReferrer">
    
    <div class="titleFirst">
        <span>Confirm Your Phone Number Via Voice Call</span>
    </div>
    <div class="div_Top_Sentence">
     <span class="span-block bottom-higth">
       We will be calling you at <asp:Label ID="lbl_ClientPhone2" runat="server" ></asp:Label>. To verify your phone please enter the digits we read to you.
    </span>
    </div>
    <div class="div_confirm_left">
        <h3>by Text Message</h3>
        <div class="titleSecond emailTitle"><asp:Label ID="lbl_phone" runat="server" Text="My password is..."></asp:Label></div>

        <div class="inputParent">
            <input id="txt_password" type="text" class="textActive" runat="server" HolderClass="place-holder" />

           <div class="inputValid" style="display:none;"  id="div_ReferreSmile">
                
            </div>
            <div class="inputError" style="display:none;"  id="div_ReferreError">
               
            </div>
        
            
        </div>
         <div>
            <a href="javascript:SendCode();" button-mode="phone" class="btn_next">Next ></a>
            <span class="btn_next" style="display:none;">Processing...</span>
        </div>
        <div class="didnt-get">
            <span class="title-didnt-get">Didn’t get the text message?</span>
            <span class="-block">
                Is your mobile number 
                <asp:Label ID="lbl_ClientPhone" runat="server" ></asp:Label>?
            </span>
            <ul>
                <li>No, I need to <a href="javascript:ChangeNum();" class="general">change the number</a></li>
                <li>Yes, please <a href="javascript:ResendCode();" class="general">resend</a> the text.</li>
            </ul>
        
        </div>
    </div>
</div>
</asp:Content>

