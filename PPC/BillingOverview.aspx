﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPagePPC.master" AutoEventWireup="true" CodeFile="BillingOverview.aspx.cs" Inherits="PPC_BillingOverview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
              
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <div class="BillingOverview">
     <div class="title">
        <span>Billing Overview</span>
     </div>
     <div class="title-billing">
        <div class="div-balance">
            <span class="span-balance">BALANCE</span>
            <span class="span-currency">$</span>               
            <asp:Label ID="lbl_Balance" runat="server" CssClass="span-amount">50</asp:Label>
        </div>
        <div class="div-TitleDetails" >
            <div runat="server" id="div_TitleDetails">
                <span class="span-block">If the balance falls below <a href="FramePpc.aspx?step=5">my price per lead</a> ($<asp:Label ID="lbl_PricePerLead" runat="server"></asp:Label>)</span>
                <span class="span-block">please recharge my balance to $<asp:Label ID="lbl_RechargeBalance" runat="server"></asp:Label></span>
            </div>
            <a href="AddCredit.aspx" class="a-add-credit" runat="server" id="a_add_credit">+ Add Credit ></a>
        </div>
        <div class="div-plans">
            <asp:LinkButton ID="lb_bronze" runat="server" CssClass="div-plan small-bronze" 
                onclick="lb_plan_Click" CommandArgument="bronze">
                <div class="plan-money">
                    <span class="plan-currency">$</span>
                    <asp:Label ID="lbl_AmountBronze" runat="server" CssClass="plan-money"></asp:Label>
                </div>
                <div class="clear"></div>
                <div class="bonus-container">
                    <span class="currency">$</span>
                    <asp:Label ID="lbl_BonusBronze" runat="server" CssClass="bonus"></asp:Label>
                    <span class="free-bonus">FREE BONUS</span>
                </div>
                <div class="tapaHover tapaHoverBronze"></div>
            </asp:LinkButton>
            <asp:LinkButton ID="lb_silver" runat="server" CssClass="div-plan small-silver" 
                onclick="lb_plan_Click" CommandArgument="silver">
                <div class="plan-money">
                    <span class="plan-currency">$</span>
                    <asp:Label ID="lbl_AmountSilver" runat="server" CssClass="plan-money"></asp:Label>
                </div>
                <div class="clear"></div>
                <div class="bonus-container">
                    <span class="currency">$</span>
                    <asp:Label ID="lbl_BonusSilver" runat="server" CssClass="bonus"></asp:Label>
                    <span class="free-bonus">FREE BONUS</span>
                </div>
                <div class="tapaHover"></div>
            </asp:LinkButton>
            <asp:LinkButton ID="lb_gold" runat="server" CssClass="div-plan small-gold" 
                onclick="lb_plan_Click" CommandArgument="gold">
                <div class="plan-money">
                    <span class="plan-currency">$</span>
                    <asp:Label ID="lbl_AmountGold" runat="server" CssClass="plan-money"></asp:Label>
                </div>
                <div class="clear"></div>
                <div class="bonus-container">
                    <span class="currency">$</span>
                    <asp:Label ID="lbl_BonusGold" runat="server" CssClass="bonus"></asp:Label>
                    <span class="free-bonus">FREE BONUS</span>
                </div>
                <div class="tapaHover"></div>
            </asp:LinkButton>
        </div>
        <div class="clear"></div>
        <div class="UnderLine"></div>
     </div>
     <div class="clear"></div>
     <div class="tables">
     <table class="tow-tables">
     <tr>
     <td class="Media-left">
            <div class="Table-Title">
                <span class="_table_title">Account Activity</span>
                <asp:Label ID="lbl_month" runat="server" CssClass="AccountActivityMonth"></asp:Label>
            </div>
            <table class="tg __tg">
            <tr class="_hs">
                <th><asp:Label ID="Label1" runat="server" Text="Activity"></asp:Label></th>
                <th ><asp:Label ID="Label3" runat="server" Text="Amount"></asp:Label></th>
                <th ><asp:Label ID="Label5" runat="server" Text="Spent"></asp:Label></th>
                <th ><asp:Label ID="Label7" runat="server" Text="Earned"></asp:Label></th>
            </tr>

            <tr class="_rs">
                <td class="first"><asp:Label ID="Label9" runat="server" Text="Leads"></asp:Label></td>
                <td><asp:Label ID="lbl_LeadsAmount" runat="server" ></asp:Label></td>
                <td><asp:Label ID="lbl_LeadsSpent" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_LeadsEarned" runat="server"></asp:Label></td>
            </tr>

            <tr class="_rs">
                <td><asp:Label ID="Label2" runat="server" Text="Refunds"></asp:Label></td>
                <td><asp:Label ID="lbl_RefundsAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_RefundsSpent" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_RefundsEarned" runat="server"></asp:Label></td>
            </tr>

            <tr class="_rs">
                <td><asp:Label ID="Label19" runat="server" Text="Credit earned from friends"></asp:Label></td>
                <td><asp:Label ID="lbl_friendsAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_friendsSpent" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_friendsEarned" runat="server"></asp:Label></td>
            </tr>

            <tr class="_rs">
                <td><asp:Label ID="Label23" runat="server" Text="Bonus credit"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusCreditAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusCreditSpent" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusCreditEarned" runat="server"></asp:Label></td>
            </tr>

            <tr class="_fs">
                <td><asp:Label ID="Label27" runat="server" Text="Total Credit Used"></asp:Label></td>
                <td><asp:Label ID="lbl_TotalAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_TotalSpent" runat="server" ></asp:Label></td>
                <td><asp:Label ID="lbl_TotalEarned" runat="server"></asp:Label></td>
            </tr>

            <tr class="_rr">
                <td><asp:Label ID="Label31" runat="server" Text="Bonus Earned"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusEarnedAmount" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_BonusEarnedSpent" runat="server" ></asp:Label></td>
                <td><asp:Label ID="lbl_BonusEarnedEarned" runat="server" ></asp:Label></td>
            </tr>

            </table>
            <div class="TransactionTableShadow"></div>
         
     </td>
     <td class="Media-right">
        <div class="Table-Title">
                <span class="_table_title">Transactions</span>
                <a href="transactions.aspx" class="AccountActivityMonth">View all</a>
            </div>
          <asp:GridView ID="_GridView_Transaction" runat="server"  GridLines="None"
              AutoGenerateColumns="false" CssClass="tg">
           <HeaderStyle CssClass="_hs" />
          <RowStyle CssClass="_rs" />
          <Columns>
          <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label10" runat="server" Text="Date"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label12" runat="server" Text="<%# Bind('Date') %>"></asp:Label>
            </ItemTemplate>
          </asp:TemplateField>

          <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label13" runat="server" Text="Type"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label14" runat="server" Text="<%# Bind('Type') %>"></asp:Label>
            </ItemTemplate>
          </asp:TemplateField>

          <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label15" runat="server" Text="Amount"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label16" runat="server" Text="<%# Bind('Amount') %>"></asp:Label>
            </ItemTemplate>
          </asp:TemplateField>
          </Columns>
         </asp:GridView>
         <div class="TransactionTableShadow"></div>
     </td>
         
     </tr>
     </table>
     </div>
 </div>
</asp:Content>

