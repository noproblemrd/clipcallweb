﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Controls/MasterPageNP2.master" CodeFile="ResendYourEmail.aspx.cs" Inherits="PPC_ResendYourEmail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="resendYourEmail">   

    <div class="titleFirst">
        <span>Check your email</span>
    </div>

    <div class="resendContent">
        <div class="resendContent1">
             Please check your inbox (<span id="registerEmail" runat="server"></span>) and click the link in the message to
            <br />

            <span class="emphasize">setup your account</span>
            and start getting leads.
        </div>
       

        <div class="resendContent2">
           <span class="emphasize">Didn’t receive an email from us?</span>
            <br />
            We can <asp:LinkButton ID="LinkButtonVerify" runat="server" onclick="LinkButton1_Click">resend</asp:LinkButton> the email or try your registration again.
           
           
        </div>     
      
        
        
        
    </div>

</div>

</asp:Content>
