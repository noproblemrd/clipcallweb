﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNP.master" AutoEventWireup="true" CodeFile="RegisterAddCredit.aspx.cs" Inherits="PPC_RegisterAddCredit" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNP.master" %>


<%@ Register src="../Controls/AddCredit.ascx" tagname="AddCredit" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="RegisterAddCredit">

    
    <div class="titleFirst">
        <span>Add Credit</span>
    </div>
    <asp:LinkButton ID="lb_DoItLater" runat="server" 
        CssClass="ill_DoItLater general" onclick="lb_DoItLater_Click">I'll do it later</asp:LinkButton>
    <span class="span-block bottom-higth">
       Choose one of the value offers below.
    </span>
    <div class="AddCreditPanel">
        <uc2:AddCredit ID="_AddCredit" runat="server" />
    </div>
</div>
</asp:Content>

