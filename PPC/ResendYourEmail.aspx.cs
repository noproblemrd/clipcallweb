﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_ResendYourEmail : RegisterPage2
{
    protected string email;    

    protected void Page_Load(object sender, EventArgs e)
    {       

        if (!IsPostBack)
        {
            email = Request["email"];
            id = Request["id"];

            registerEmail.InnerText = Request["email"];

            LoadMaster();
            LoadPageText();
        }
    }

    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        //masterPage.setRibbon();
    }


    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText(); 
       

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "login"
                          select x).FirstOrDefault();

        SentFailed = xelem.Element("Email").Element("Validated").Element("SentFailed").Value;        

        Sent = xelem.Element("Email").Element("Validated").Element("Sent").Value;        
    }   

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Guid guidId;
        Guid.TryParse(id, out guidId);

        WebServiceSales webServiceSales = new WebServiceSales();
        bool resendOk = webServiceSales.resendRegistrationEmail(guidId);

        //UpdatePanelVerifyResult.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerComment", "SetServerComment('" + Sent + "');", true);
        /*
        if (resendOk)
        {
           
            if (!ClientScript.IsStartupScriptRegistered("ServerComment"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerComment", "SetServerComment('" + Sent + "');", true);
                
        }
        else
        {
            
            if (!ClientScript.IsStartupScriptRegistered("ServerComment"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerComment", "SetServerComment('" + SentFailed + "');", true);
        }
        */
        //UpdatePanelVerifyResult.Update();
    }

    protected string id
    {
        get { return (ViewState["id"] != null) ? (string)ViewState["id"] : ""; }
        set { ViewState["id"] = value; }
    }

    protected string Sent
    {
        get {return (ViewState["Sent"]!=null) ? (string)ViewState["Sent"]: ""; }
        set {ViewState["Sent"]=value; }
    }

    protected string SentFailed
    {
        get { return (ViewState["SentFailed"] != null) ? (string)ViewState["SentFailed"] : ""; }
        set { ViewState["SentFailed"] = value; }
    }
    
}