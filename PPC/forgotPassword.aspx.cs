﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_forgotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadPageText();

            string token;
            token=Request["token"];

            if (token == "expired")
                SetComment(tokenExpired);
        }

        LoadMaster();


        

        txt_email.Attributes["place_holder"] = EmailPlaceHolder;
        
        ////txt_referrer.Attributes["place_holder"] = "The code from the email invitation you received";
        Page.DataBind();
    }

    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

    }
    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;
        //Response.Write(GetServerError);
        //Response.Write(GetServerError);
        //    _map.FieldMissing = FieldMissing;       

        // FieldMissing = xelem.Element("BusinessName").Element("Validated").Element("Missing").Value;
        //   XElement t_element = xelem.Element("BusinessAddress").Element("Validated");

        //    t_element = xelem.Element("Email").Element("Validated");
        //    EmailPlaceHplder = t_element.Element("Instruction").Element("General").Value;
        //    EmailDuplicate = t_element.Element("Instruction").Element("EmailDuplicate").Value;
        //    InvalidEmail = t_element.Element("Error").Value;
        //    EmailMissing = t_element.Element("Missing").Value;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "login"
                          select x).FirstOrDefault();

        EmailPlaceHolder = xelem.Element("Email").Element("Validated").Element("Instruction").Element("General").Value;        
        EmailError = xelem.Element("Email").Element("Validated").Element("Error").Value;        
        NotFound = xelem.Element("Email").Element("Validated").Element("NotFound2").Value;
        tokenExpired = xelem.Element("password").Element("Validated").Element("Instruction").Element("Expired").Value;
    }


    void SetComment(string comment)
    {
        
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.SetServerComment(comment);
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {

        WebServiceSales webServiceSales = new WebServiceSales();
        string status=webServiceSales.forgotPassword(txt_email.Value);
        
        if (status == "serverError")
        {
            SetComment(GetServerError);
        }
        else if (status == "failed")
        {
            SetComment(NotFound);
        }
        else
        {
            titleFirst.InnerText = "Reset your password ";
            subTitle.InnerText = "Please check your inbox, and follow the instructions in the email to set your new password.";

            registrationInputs.Visible = false;
        }

        
    }

    protected string EmailPlaceHolder
    {
        get { return (string)ViewState["EmailPlaceHolder"]; }
        set { ViewState["EmailPlaceHolder"] = value; }
    }

    protected string tokenExpired
    {
        get { return (string)ViewState["tokenExpired"]; }
        set { ViewState["tokenExpired"] = value; }
    }

    protected string EmailError
    {
        get { return (string)ViewState["EmailError"]; }
        set { ViewState["EmailError"] = value; }
    }

    protected string NotFound
    {
        get { return (string)ViewState["NotFound"]; }
        set { ViewState["NotFound"] = value; }
    }


    protected string GetServerError
    {
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }

    protected string RegisterReferrer
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/SupplierReferrerInvitation"); }
    }


   
}