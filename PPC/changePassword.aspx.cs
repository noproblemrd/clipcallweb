﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_changePassword : System.Web.UI.Page
{  

    protected void Page_Load(object sender, EventArgs e)
    {
        //ScriptManager1.RegisterAsyncPostBackControl(btn_SendService);

        if (!IsPostBack)
        {
            LoadPageText();
            token = Request["code"];
            checkToken(token);
        }

        LoadMaster();

        txt_passwordFirstOfTwo.Attributes["place_holder"] = "Enter password";
        txt_passwordSecondOfTwo.Attributes["place_holder"] = "Enter password";

        ////txt_referrer.Attributes["place_holder"] = "The code from the email invitation you received";
        Page.DataBind();
    }

    private void checkToken(string token)
    {
        WebServiceSales webServiceSales = new WebServiceSales();
        string result=webServiceSales.CheckResetPasswordToken(token);
        if (result != "success")
            SetComment(tokenExpired);


    }
    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.RemoveRibbon();

    }
    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

        //Response.Write(GetServerError);
        //    _map.FieldMissing = FieldMissing;       

        // FieldMissing = xelem.Element("BusinessName").Element("Validated").Element("Missing").Value;
        //   XElement t_element = xelem.Element("BusinessAddress").Element("Validated");

        //    t_element = xelem.Element("Email").Element("Validated");
        //    EmailPlaceHplder = t_element.Element("Instruction").Element("General").Value;
        //    EmailDuplicate = t_element.Element("Instruction").Element("EmailDuplicate").Value;
        //    InvalidEmail = t_element.Element("Error").Value;
        //    EmailMissing = t_element.Element("Missing").Value;

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "login"
                          select x).FirstOrDefault();
        tokenExpired=xelem.Element("password").Element("Validated").Element("Instruction").Element("Expired").Value;
        PasswordError = xelem.Element("password").Element("Validated").Element("Instruction").Element("Error").Value;
        PasswordMutch = xelem.Element("password").Element("Validated").Element("Instruction").Element("Mutch").Value;
    }


    void SetComment(string comment)
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.SetServerComment(comment);
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {

        WebServiceSales webServiceSales = new WebServiceSales();
        string status = webServiceSales.CheckResetPasswordToken(txt_passwordFirstOfTwo.Value, token);       
        
        if (status == "success")        
             Response.Redirect("ppcLogin.aspx?error=changedPassword");
        else if (status == "failed")
        {
            Response.Redirect("forgotPassword.aspx?token=expired");
            //SetComment(tokenExpired);
        }
        else
        {
            //SetComment(GetServerError);
            ClientScript.RegisterStartupScript(this.GetType(), "SetComment", @"stopActiveButtonAnim();SetServerComment(""" + GetServerError + @""");", true);            
        }
        //UpdatePanel1.Update();
    }




    protected string token
    {
        get { return (string)ViewState["token"]; }
        set { ViewState["token"] = value; }
    }

    protected string tokenExpired
    {
        get { return (string)ViewState["tokenExpired"]; }
        set { ViewState["tokenExpired"] = value; }
    }

    protected string PasswordError
    {
        get { return (string)ViewState["PasswordError"]; }
        set { ViewState["PasswordError"] = value; }
    }

    protected string PasswordMutch
    {
        get { return (string)ViewState["PasswordMutch"]; }
        set { ViewState["PasswordMutch"] = value; }
    }

    
    protected string GetServerError
    {
        get { return (string)ViewState["GetServerError"]; }
        set { ViewState["GetServerError"] = value; }
    }

    
   

}