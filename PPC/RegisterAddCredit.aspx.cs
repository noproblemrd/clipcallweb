﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PPC_RegisterAddCredit : RegisterPage, IsInProcess
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _AddCredit.BuyCredit += new EventHandler(_AddCredit_BuyCredit);
        _AddCredit.SetCreditDetails(siteSetting.GetUrlWebReference, SupplierId, NUMBER_FORMAT);
        LoadMaster();
        
    }

    private void LoadMaster()
    {
        Master.SetBenefitFreeText("There are lots of ways to earn NoProblem bonus credit!</br><br/>We’ll tell you how in the Earn Credit section of our Advertiser Dashboard.");
        Master.SetStatusBar("statusBar3");
    }

    void _AddCredit_BuyCredit(object sender, EventArgs e)
    {
        EventArgsBuyCredit _BuyCredit = (EventArgsBuyCredit)e;
        Package _pack = new Package(_BuyCredit);
        string __form = AddCreditSales.BuyCredit(_pack, siteSetting, SupplierId);
        Response.Write(__form);
        Response.Flush();
        Context.ApplicationInstance.CompleteRequest();
    }
    protected void lb_DoItLater_Click(object sender, EventArgs e)
    {
        UserMangement um = PpcSite.SupplierLogin(PhoneNum, Password);
        if (um == null)
        {
            GeneralServerError();
            return;
        }
        Session["UserGuid"] = um;
        ClearDetails();
        Response.Redirect(ResolveUrl("~/Management/overview.aspx"));
    }
}