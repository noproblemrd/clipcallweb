﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_comparePlans : RegisterPage2, IsInProcess
{
    protected decimal fee;
    protected string GetServerError;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadPageText();

        if (!IsPostBack)
        {            
            LoadMaster();
            LoadFee();
        }
    }


    private void LoadMaster()
    {
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        masterPage.SetStatusBar("current","pre","pre");

        masterPage.SetBlackMenu();

        if (Session["legacy"] != null)
        {
            bool ifLegacy = (bool)Session["legacy"];

            if (ifLegacy)
                masterPage.RemoveRibbon();
        }

        //masterPage.setRibbon();
    }

    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();
        string _error;
        FieldMissing = Utilities.GetGeneralServerMissing(xdoc, out _error);
        GetServerError = _error;

    }

    protected void LoadFee()
    {

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSupplier.ResultOfSubscriptionCostAndBonus result = new WebReferenceSupplier.ResultOfSubscriptionCostAndBonus();
        try
        {
            result = _supplier.GetSubscriptionPlanCost();
            fee = result.Value.Cost;
            featureListingTopSum.InnerText = fee.ToString();
            featureListingBottomSum.InnerText = fee.ToString();

        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            //masterPage.ServerProblem(GetServerError);  
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
            //masterPage.ServerProblem(GetServerError);  
            return;
        }



    }

    protected void linkButtonPlanFeauredListing_Click(object sender, EventArgs e)
    {
        WebServiceSales webServiceSales = new WebServiceSales();
        Dictionary<string, string> dic = new Dictionary<string, string>();

        ResultSteps resultSteps = webServiceSales.ChoosePlan_Registration2014(SupplierId, "FEATURED_LISTING");
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        if (resultSteps.result != "failed")
            masterPage.stepRedirect(3); // go to business details general including after use button "back"
        else
            masterPage.ServerProblem(GetServerError);   
    }

    protected void linkButtonPlanLeadsBooster_Click(object sender, EventArgs e)
    {
        WebServiceSales webServiceSales = new WebServiceSales();
        Dictionary<string, string> dic = new Dictionary<string, string>();

        ResultSteps resultSteps = webServiceSales.ChoosePlan_Registration2014(SupplierId, "LEAD_BOOSTER_AND_FEATURED_LISTING");
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        if (resultSteps.result != "failed")
            masterPage.stepRedirect(3); // go to business details general including after use button "back"
        else
            masterPage.ServerProblem(GetServerError);     
    }

    protected void linkButtonPlanFeauredListing2_Click(object sender, EventArgs e)
    {
        WebServiceSales webServiceSales = new WebServiceSales();
        Dictionary<string, string> dic = new Dictionary<string, string>();

        ResultSteps resultSteps = webServiceSales.ChoosePlan_Registration2014(SupplierId, "FEATURED_LISTING");
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        if (resultSteps.result != "failed")
            masterPage.stepRedirect(3); // go to business details general including after use button "back"
        else
            masterPage.ServerProblem(GetServerError);   
    }

    protected void linkButtonPlanLeadsBooster2_Click(object sender, EventArgs e)
    {
        WebServiceSales webServiceSales = new WebServiceSales();
        Dictionary<string, string> dic = new Dictionary<string, string>();

        ResultSteps resultSteps = webServiceSales.ChoosePlan_Registration2014(SupplierId, "LEAD_BOOSTER_AND_FEATURED_LISTING");
        Controls_MasterPageNP2 masterPage = (Controls_MasterPageNP2)Page.Master;
        if (resultSteps.result != "failed")
            masterPage.stepRedirect(3); // go to business details general including after use button "back"
        else
            masterPage.ServerProblem(GetServerError);   
    }


    protected string FieldMissing
    {
        get { return (string)ViewState["FieldMissing"]; }
        set { ViewState["FieldMissing"] = value; }
    }
}