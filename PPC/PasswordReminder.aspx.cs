﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class PPC_PasswordReminder : PageSetting
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
        //    Master.SetTitle1 = " &nbsp;";
       //     Master.SetTitle2 = "Login";
            SetMaster();
            
            LoadPageText();
        }
        else
        {
            if (txt_Mobile.Value == PhonePlaceHolder || string.IsNullOrEmpty(txt_Mobile.Value))
            {
                txt_Mobile.Value = PhonePlaceHolder;
                txt_Mobile.Attributes["class"] = "place-holder";
            }

        }
        SetPlaceHolder();
   //     Master.SetCssClass = "login";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "PhoneValidation", Utilities.CheckStatusUSPhoneJavaScript(), true);
        Header.DataBind();
    }
    private void SetMaster()
    {
        Master.RemoveRibbon();
        Master.SetBenefitFreeText("Concentrate on the work at hand rather than drumming up new business with NoProblem.</br></br>We bring quality leads directly to you!");
    }
    private void LoadPageText()
    {
        XDocument xdoc = Utilities.GetXmlSiteText();

        XElement xelem = (from x in xdoc.Element("Data").Elements("FormPage")
                          where x.Attribute("name").Value == "PasswordReminder"
                          select x).FirstOrDefault();

        XElement phone_element = xelem.Element("mobile").Element("Validated");
        PhonePlaceHolder = phone_element.Element("Instruction").Element("PhonePlaceHolder").Value;
        PhoneInstruction = phone_element.Element("Instruction").Element("General").Value;        
    //    PhoneNotFind = phone_element.Element("Instruction").Element("PhoneNotFind").Value;
        ServerMessage sm = new ServerMessage(phone_element.Element("Instruction").Element("PhoneNotFind"));
        PhoneNotFind = sm.GetHtmlMessageJavascriptEncode("RequestInvitation.aspx");
        ServerFaild = phone_element.Element("Error").Element("SendFaild").Value;
    //    PhoneNotFindAction = phone_element.Element("Instruction").Element("PhoneNotFindAction").Value;
        PhoneMissing = phone_element.Element("Instruction").Element("PhoneMissing").Value;
        PhoneInvalid = phone_element.Element("Instruction").Element("PhoneInvalid").Value;
    }
    protected void lb_send_Click(object sender, EventArgs e)
    {
        string _phone = txt_Mobile.Value;
        if (String.IsNullOrEmpty(_phone))
        {
            SetServerComment(PhoneNotFind);
            return;
        }
        var service = new WebServiceSales();
        bool phoneExists = service.MobileExists(_phone) == "true";
        if (!phoneExists)
        {
            SetServerComment(PhoneNotFind);
            return;
        }
        _phone=Utilities.GetCleanPhone(_phone);
        bool UseEmail = false;
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.SendPasswordReminder(_phone, UseEmail);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            SetServerComment(ServerFaild);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            SetServerComment(ServerFaild);
            return;
        }
        string _login = "PpcLoginMobile.aspx?sent=" + ((UseEmail) ? "email" : "mobile");
        Response.Redirect(_login);
    }
   
    protected string PhoneInstruction
    {
        get { return (string)ViewState["PhoneInstruction"]; }
        set { ViewState["PhoneInstruction"] = value; }
    }
    protected string PhonePlaceHolder
    {
        get { return (string)ViewState["PhonePlaceHolder"]; }
        set { ViewState["PhonePlaceHolder"] = value; }
    }
    
    void SetPlaceHolder()
    {
        txt_Mobile.Attributes["place_holder"] = PhonePlaceHolder;
        txt_Mobile.Attributes["HolderClass"] = "place-holder";
        txt_Mobile.Attributes["text-mode"] = "phone";
    }
    
    void SetServerComment(string _comment)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "SetComment", "SetServerComment('" + HttpUtility.HtmlEncode(_comment) + "');", true);
    }
    protected string GetPhoneExpresion
    {
        get { return Utilities.RegularExpressionForJavascript(siteSetting.GetPhoneRegularExpression); }
    }
    protected string IfPhoneExists
    {
        get { return ResolveUrl("~") + "PPC/WebServiceSales.asmx/MobileExists"; }    
    }
    protected string PhoneNotFind
    {
        get { return (string)ViewState["PhoneNotFind"]; }
        set { ViewState["PhoneNotFind"] = value; }
    }
    protected string ServerFaild
    {
        get { return (string)ViewState["ServerFaild"]; }
        set { ViewState["ServerFaild"] = value; }
    }
    /*
    protected string PhoneNotFindAction
    {
        get { return (string)ViewState["PhoneNotFindAction"]; }
        set { ViewState["PhoneNotFindAction"] = value; }
    }
     * */
    protected string PhoneMissing
    {
        get { return (string)ViewState["PhoneMissing"]; }
        set { ViewState["PhoneMissing"] = value; }
    }
    protected string PhoneInvalid
    {
        get { return (string)ViewState["PhoneInvalid"]; }
        set { ViewState["PhoneInvalid"] = value; }
    }
    
}