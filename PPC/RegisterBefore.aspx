﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNP.master" AutoEventWireup="true" CodeFile="RegisterBefore.aspx.cs" Inherits="PPC_RegisterBefore" %>
<%@ MasterType VirtualPath="~/Controls/MasterPageNP.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="../CallService.js"></script>
<script type="text/javascript" src="../general.js"></script>
<script type="text/javascript" src="script/JSGeneral.js"></script>
 <script type="text/javascript" src="script/HistoryManager.js"></script>
 <script type="text/javascript" src="../PlaceHolder.js"></script>

<script type="text/javascript">

    function GotoStep1() {
        document.getElementById("<%# div_a.ClientID %>").style.display = "block";
        document.getElementById("<%# div_b.ClientID %>").style.display = "none";
        RemoveServerComment();
        var txtCode = document.getElementById("<%# txt_pass.ClientID %>");
        txtCode.value = "";
        $(txtCode).focus();
        $(txtCode).blur();
        toggleDisabledDiv1(false);
        $.history.load("1");
    }

    /******   Phone validation   *****/
    var place_holder_phone;
    function ChekValidPhone_KeyPress(_value, e) {
        var div_MobileError = document.getElementById("div_MobileError");
        var div_MobileSmile = document.getElementById("div_MobileSmile");
   //     var div_MobileComment = document.getElementById("div_MobileComment");
        var txt_Mobile = document.getElementById("<%# txt_Mobile.ClientID %>");
        RemoveCssClass(txt_Mobile, "textError");
        var _status = checkPhoneStatus(_value);
        if (_status == "empty") {
            div_MobileError.style.display = "none";
            div_MobileSmile.style.display = "none";
            SlideElement('div_MobileComment', false);
            return;
        }
       
        if (_status == "match") {
            div_MobileError.style.display = "none";
            div_MobileSmile.style.display = "block";
            SlideElement('div_MobileComment', false);
        }
        else if (_status == "OnTheWay") {
            div_MobileError.style.display = "none";
            div_MobileSmile.style.display = "none";
            SlideElement('div_MobileComment', true);
        }
        else {
            div_MobileError.style.display = "block";
            AddCssClass(txt_Mobile, "textError");            
            div_MobileSmile.style.display = "none";
            SlideElement('div_MobileComment', true);
        }
        var keynum;
        if (window.event) // IE            
            keynum = e.keyCode            
        else if (e.which) // Netscape/Firefox/Opera            
            keynum = e.which
        if (keynum == 13)
            SendMobile();
    }

    function ChekValidPhone(elem) {
        var _value = elem.value; //GetText_PlaceHolder(elem, place_holder_phone);
        var div_MobileError = document.getElementById("div_MobileError");
        var div_MobileSmile = document.getElementById("div_MobileSmile");
      //  var div_MobileComment = document.getElementById("div_MobileComment");
        var txt_Mobile = document.getElementById("<%# txt_Mobile.ClientID %>");
        _value = _value.RemoveSpace();
        if (_value.length == 0) {
            div_MobileError.style.display = "none";
            div_MobileSmile.style.display = "none";
            SlideElement('div_MobileComment', false);
            /*
            txt_Mobile.value = place_holder_phone;
            AddCssClass(txt_Mobile, "place-holder");
            */
            
        }
        else if (!CheckPhone(_value)) {
            div_MobileError.style.display = "block";
            AddCssClass(txt_Mobile, "textError");            
            div_MobileSmile.style.display = "none";
            SlideElement('div_MobileComment', true);
        }
        else {
            div_MobileError.style.display = "none";
            div_MobileSmile.style.display = "block";
            SlideElement('div_MobileComment', false);
            AddCssClass(txt_Mobile, "textValid");
        }
    }
    
    function CheckPhone(_phone) {
        if (_phone.length == 0)
            return false;
        var _reg = new RegExp("<%# GetPhoneExpresion %>");
        return _reg.test(_phone);
    }
   
    function GetPhoneText() {
        var txt_Mobile = document.getElementById("<%# txt_Mobile.ClientID %>");
        return txt_Mobile.value;
    }

    function SendMobile() {
        var div_MobileError = document.getElementById("div_MobileError");
        var div_MobileSmile = document.getElementById("div_MobileSmile");
        var txt_Mobile = document.getElementById("<%# txt_Mobile.ClientID %>");
        
        
        var phone = GetPhoneText();
        var Is_Ok = true;

        phone = phone.RemoveSpace();
        if (!CheckPhone(phone)) {
            div_MobileError.style.display = "block";
            SlideElement('div_MobileComment', true);
            div_MobileSmile.style.display = "none";
            Is_Ok = false;
            RemoveCssClass(txt_Mobile, "textValid");
            AddCssClass(txt_Mobile, "textError"); 
            var _message = (phone.length == 0) ? "<%# MissingPhone %>" : "<%# InvalidPhoneNumber  %>";
            SetServerComment(_message);
            
        }
        else {
            RemoveServerComment();
            div_MobileError.style.display = "none";
            div_MobileSmile.style.display = "block";
            SlideElement('div_MobileComment', false);
            AddCssClass(txt_Mobile, "textValid");
            RemoveCssClass(txt_Mobile, "textError"); 
        }

        if (!Is_Ok)
            return;
        toggleDisabledDiv1(true);
        var _params = "Mobile=" + phone;//  + "&email=" + email + "&phone=" + phone;
        var _url = "<%# GetSendMobileService %>";
        CallWebService(_params, _url,
                function (arg) {
                    if (arg == "true") {
                        document.getElementById("<%# div_a.ClientID %>").style.display = "none";
                        document.getElementById("<%# div_b.ClientID %>").style.display = "block";
                        document.getElementById("<%# lbl_PhoneDisplay.ClientID %>").innerHTML = phone;
                        SetServerComment("<%# CodeSent %>");
                        $.history.load("2");
                //        _stage = "2";
                    }
                    else if (arg == "redirect") {
                        window.location = "<%# LoginPage %>";
                        return;
                        //        alert('po');
                        //        toggleDisabledDiv1(false);
                    }
                    else {
                        var __mes = "<%# ServerErrorPhoneNumber %>";
                        SetServerComment(__mes);
                        RemoveCssClass(txt_Mobile, "textValid");
                        AddCssClass(txt_Mobile, "textError");
                        SlideElement('div_MobileComment', false);
                        div_MobileError.style.display = "block";
                        toggleDisabledDiv1(false);
                    }
                },
                function () {
                    var __mes = "<%# ServerErrorPhoneNumber %>";
                    SetServerComment(__mes);
                    RemoveCssClass(txt_Mobile, "textValid");
                    AddCssClass(txt_Mobile, "textError");
                    SlideElement('div_MobileComment', true);
                    div_MobileError.style.display = "block";
                    toggleDisabledDiv1(false);
                });


            }
        function GotoStep2() {
            document.getElementById("<%# div_a.ClientID %>").style.display = "none";
            document.getElementById("<%# div_b.ClientID %>").style.display = "block";
            SetServerComment("<%# CodeSent %>");
        }
       
        function toggleDisabledDiv1(ToDisable) {
            var div_1 = document.getElementById("<%# div_a.ClientID %>");
            
            var btn_send = document.getElementById("btn_SendMobile");
            toggleDisabled(div_1, ToDisable);
            if (ToDisable) {
               
                btn_send.className = "btn_next_active";
                btn_send.innerHTML = "Sending...";
                
            }
            else {

                btn_send.className = "btn_next";
                btn_send.innerHTML = "Next &#62;";
               
            }
        }
        /********** Code / Password  ****************/
        var code_place_holder;
        function CheckPasswordOnPress(_value, e) {
            CheckPassword(_value);
            var keynum;
            if (window.event) // IE            
                keynum = e.keyCode
            else if (e.which) // Netscape/Firefox/Opera            
                keynum = e.which
            if (keynum == 13)
                Log_in();
        }
        function CheckPassword(_value) {
            var div_CodeError = document.getElementById("div_CodeError");
            var div_CodeValid = document.getElementById("div_CodeValid");
            
            
            if (_value.length > 0) {
                div_CodeError.style.display = "none";
                div_CodeValid.style.display = "block";
                return true;
            }
            div_CodeValid.style.display = "none";
            return false;
        }
        function CheckPasswordOnBlur(_txt) {
            var _value = _txt.value;// GetText_PlaceHolder(_txt, code_place_holder);
            if (_value.length > 0 && CheckPassword(_value))
                AddCssClass(_txt, "textValid");
            else if (_value.length == 0) {
                document.getElementById("div_CodeError").style.display = "none";
                document.getElementById("div_CodeValid").style.display = "none";
           //     _txt.style.display = "none";
           //     document.getElementById("<# pass_place.ClientID %>").style.display = "block";
            }
    //        Text_Blur_pass(_txt, code_place_holder);
        }
        

        function Log_in() {
            var txt_pass = document.getElementById("<%# txt_pass.ClientID %>");

            var _password = txt_pass.value;// GetText_PlaceHolder(txt_pass, code_place_holder);
            if (!CheckPassword(_password)) {
                document.getElementById("div_CodeError").style.display = "block";
                RemoveCssClass(txt_pass, "textValid");
                AddCssClass(txt_pass, "textError");
                SetServerComment("<%# FieldMissing %>");
                return;
            }
            RemoveServerComment();
            toggleDisabledDivPassword(true);
            var _params = "password=" + _password + "&invitationId=<%# InvitationId %>";
            var _url = "<%# GetLoginService %>";
            CallWebService(_params, _url,
                function (arg) {
                    var _arg = new Object();
                    _arg = eval('(' + arg + ')');
                    if (_arg.status == "true") {
                        if (_arg.IsTrial == "true")
                            GotoStep3(true);
                        else
                            GotoStep3(false);
                    }
                    else {
                        toggleDisabledDivPassword(false);
                        RemoveCssClass(txt_pass, "textValid");
                        AddCssClass(txt_pass, "textError");
                        document.getElementById("div_CodeError").style.display = "block";
                        if (arg == "false") {
                            SetServerComment("<%# WrongPassword %>");

                        }
                        else
                            SetServerComment("<%# GetServerError %>");

                    }
                },
                function () {
                    toggleDisabledDivPassword(false);
                    var __mes = "<%# GetServerError %>";
                    SetServerComment(__mes);
                }
            );

        }
        function GotoStep3(_exp) {
            //var _params = "?invitationid=" +invitationid;
            var _params = (_exp) ? "&trial=" + _exp : "";
            window.location = "<%# RedirectService %>";  + _params;
        }
        function toggleDisabledDivPassword(ToDisable) {
            var div_b = document.getElementById("<%# div_b.ClientID %>");

            var btn_code = document.getElementById("btn_code");
            toggleDisabled(div_b, ToDisable);
            if (ToDisable) {

                btn_code.className = "btn_next_active";
     //           btn_code.innerHTML = "Sending...";

            }
            else {

                btn_code.className = "btn_next";
                //            btn_code.innerHTML = "Next &#62;";

            }
        }
        
        jQuery(document).ready(function ($) {
            
            $.history.init(function (url) {
                if ($('#<%# hf_IsFirstTime.ClientID %>').val() == "true") {
                    $('#<%# hf_IsFirstTime.ClientID %>').val("false");
                    url = "1";
                }
                setBack(url == "" ? "1" : url);
            });

        });
        function setBack(num) {
            
            if (num == "1") {
                GotoStep1();
            }
            else if (num == "2") {
                GotoStep2();
            }
        }
       
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="hf_IsFirstTime" runat="server" />
    <div class="step1">
 <div class="statusBar statusBarBackground1">
        <div class="detailsService shadow">Step 1</div>
        <div class="location">Step 2</div> 
        <div class="getFreeLeads">Get Started!</div>                 
    </div>
    <div class="container_comment">
        <div class="comment" id="div_ServerComment"  style="display:none;">
        
            <span id="lbl_ServerComment" >
            </span>
        </div>
    </div>
    <!----     MOBILE   --->
    <div runat="server" id="div_a" class="div_TopSpace">
        
        <div class="titleFirst">
            <span>Create Your Account</span>
            <div class="div_AlreadyRegistered titleSecond"><span>Already registered?</span><a href="<%= ResolveUrl("~/PPC/PpcLogin.aspx") %>" class="general">Login</a> </div>
        </div>
            <span>Are you ready? You’re only a few quick steps from getting quality phone leads directly to your mobile phone! Start by entering your mobile phone number below. We’ll send a password via text message to the number you provide to verify that this is actually you.</span>


        
        <div class="titleSecond mobilePhone"><asp:Label ID="lbl_Mobile" runat="server" Text="My mobile phone number is...*"></asp:Label></div>
        <div class="clear"></div>
        <div class="inputParent">           
                
            <div class="inputSun">
                <asp:TextBox ID="txt_Mobile" runat="server" CssClass="textActive"  HolderClass="place-holder"
                onkeyup="ChekValidPhone_KeyPress(this.value, event);" 
                            onblur="ChekValidPhone(this);" ></asp:TextBox>
            </div>

            <div class="inputValid" style="display:none;"  id="div_MobileSmile">
                
            </div>
            <div class="inputError" style="display:none;"  id="div_MobileError">
               
            </div>
            <div class="inputComment" style="display:none;" id="div_MobileComment">
                 <%= PhonePlaceHolder %>
              <div class="chat-bubble-arrow-border"></div>
              <div class="chat-bubble-arrow"></div>
            </div>
            
            
        </div>
            

            
        <!--            We didn't get your phone number. Please note that we currenlty accept advertisers with a US mobile phone only.-->
               
        
        <div>
        <a id="btn_SendMobile" href="javascript:void(0);" onclick="SendMobile();" class="btn_next">Next &#62;</a>
              
                
        </div>
        <div>
            <div class="underNext">We’ll text the password to the phone number above.</div>
           <div class="underNextTextNote">
               *
            </div>
            <div class="underNextText">
                Please note that to use No Problem’s phone lead advertising service, it is essential to have a mobile phone that’s able to receive text messages. We will not share your phone number with anyone, nor use it for anything other than bringing you new customers. We take your <a href="javascript:openWin('http://www2.noproblemppc.com/LegalPrivacy.htm');"  class="general">privacy</a> very seriously.
            </div>
           
        
        </div>
       
    </div>
     <!----     PASSWORD   --->
    <div runat="server" id="div_b"  style="display:none;" class="div_TopSpace">
      <div class="spaceAbove">
        <div class="titleSecond">My password is...</div>
 		<div class="clear"></div></div>
        <div class="inputParent">
            <div class="inputSun">
                <input type="password" id="txt_pass" runat="server" onkeyup="CheckPasswordOnPress(this.value, event);" class="textActive" 
                        onblur="CheckPasswordOnBlur(this);"  style="display:none;" 
                        HolderClass="place-holder" />

                
                
            </div> 
                <div class="inputValid" style="display:none;"  id="div_CodeValid">
                
                </div> 
                 <div class="inputError" style="display:none;"  id="div_CodeError">
               
                </div>
                
           <!--
                 <div class="inputComment" style="display:none;" id="div3">
                 <= PhonePlaceHolder %>
              <div class="chat-bubble-arrow-border"></div>
              <div class="chat-bubble-arrow"></div>
            </div>
                -->
            
	   </div>
       
	    <div>
            <a id="btn_code" href="javascript:void(0);" class="btn_next" onclick="Log_in();">Next &#62;</a>
           
		</div>
        <div class="didnt">
            <div class="titleFirst">Didn’t receive a text message from us?</div>

            <span>Please check that the mobile number you provided is correct: <asp:Label runat="server" ID="lbl_PhoneDisplay"></asp:Label>. If it isn’t, you can <a href="javascript:void(0);" onclick="GotoStep1();" class="general">change your number</a>. If it’s the right number and you still haven’t received a text message, 
                <a href="javascript:void(0);" onclick="SendMobile();" class="general">we’ll resend it.</a></span>
            <br /><br />
            
            <span>Your phone can’t receive text messages? Please contact our customer support at </span>
            <a href="mailto:hi@noproblemppc.com" class="general">hi@noproblemppc.com</a>.
        </div>
    </div>
</div>

</asp:Content>

