﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uploadFileByTelerik.aspx.cs" Inherits="PPC_uploadFileByTelerik" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
    div.RadUpload .ruFakeInput
    {
        visibility: hidden;
        width: 0;
        padding: 0;
    }
    div.RadUpload .ruFileInput
    {
        width: 1;
    }
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        </telerik:RadScriptManager>

        <telerik:RadUpload ID="RadUpload1" runat="server" controlobjectsvisibility="None" >
        </telerik:RadUpload>

        <asp:Button ID="Button1" runat="server" Text="Button" />
    </div>
    </form>
</body>
</html>
