﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FuulUrlExposure.aspx.cs" Inherits="Publisher_FuulUrlExposure" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
       <link type="text/css" rel="Stylesheet" href="../Publisher/style.css" />

    <title></title>
    <script type="text/javascript">

        function CloseWindow() {
            if (window.opener)
                window.close();
            else
                window.location = '<%# GetLogin %>';
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
    <div>
     <div id="primary-content">
    <div class="page-content minisite-content2 MinWin">
        <h3>
            <asp:Label ID="lbl_KeywordTitleVal" runat="server"></asp:Label>
            <br />
            <asp:Label ID="lbl_UrlTitleVal" runat="server" ></asp:Label>
        </h3>
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	    <ContentTemplate>	
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
        <div id="Table_Report" class="table" runat="server" >
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                CssClass="data-table" onrowdatabound="_GridView_RowDataBound">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <Columns>                     
                    <asp:TemplateField>              
                    <HeaderTemplate>
                        <asp:Label ID="Label01" runat="server" Text="<%# lbl_UrlRoot.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        
                            <asp:Label ID="Label1" runat="server" Text="<%# Bind('UrlRoot') %>"></asp:Label>          
                        
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label02" runat="server" Text="<%# lbl_Exposures.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('Exposures') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label03" runat="server" Text="<%# lbl_Requests.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text="<%# Bind('Requests') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label04" runat="server" Text="<%# lbl_CTR.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text="<%# Bind('CTR') %>"></asp:Label>    
                        <asp:Label ID="Label5" runat="server" Text="%"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                </asp:GridView>
                <uc1:TablePaging ID="TablePaging1" runat="server" />
            </div>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </div>
    </div>
    <asp:Label ID="lbl_UrlRoot" runat="server" Text="Root URL" Visible="false"></asp:Label>
     <asp:Label ID="lbl_Exposures" runat="server" Text="Exposures" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Requests" runat="server" Text="Requests" Visible="false"></asp:Label>
        <asp:Label ID="lbl_CTR" runat="server" Text="CTR" Visible="false"></asp:Label>

    <asp:Label ID="lbl_RecordMatch" runat="server" Text="records matched your query" Visible="false"></asp:Label>
       <asp:Label ID="lbl_NoResult" runat="server" Visible="false" Text="There are not results"></asp:Label>    

    </div>
    </form>
</body>
</html>
