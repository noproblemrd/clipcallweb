using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Publisher_EditSecurityLevel : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            
            LoadPageConfirms();
        }
        ScriptManager1.RegisterAsyncPostBackControl(_reapeter);
    }
    

   private void LoadPageConfirms()
    {
        string command = "EXEC dbo.GetPagesSecurityAndConfirmsByParents @ChildPageId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ChildPageId", DBNull.Value);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            DataTable data = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(data);

            _reapeter.DataSource = data;
            _reapeter.DataBind();

            foreach (RepeaterItem item in _reapeter.Items)
            {
                Label lbl = (Label)item.FindControl("lbl_pageId");
                int pageId = int.Parse(lbl.Text);
                Repeater reapeterIn = (Repeater)item.FindControl("_reapeterIn");
                SqlCommand cmd2 = new SqlCommand(command, conn);
                cmd2.Parameters.AddWithValue("@ChildPageId", pageId);
                cmd2.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
                DataTable data2 = new DataTable();
                SqlDataAdapter adapter2 = new SqlDataAdapter(cmd2);
                adapter2.Fill(data2);
                reapeterIn.DataSource = data2;
                reapeterIn.DataBind();
            }
            conn.Close();
        }
    }
    protected void btn_Update_Click(object sender, EventArgs e)
    {
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            foreach (RepeaterItem item in _reapeter.Items)
            {
                SaveSecurityLevel(conn, item);
                Repeater _repIn = (Repeater)item.FindControl("_reapeterIn");
                foreach (RepeaterItem itemIn in _repIn.Items)
                {
                    SaveSecurityLevel(conn, itemIn);
                }
            }
            conn.Close();
        }
    }
    void SaveSecurityLevel(SqlConnection conn, RepeaterItem item)
    {
        string command = "EXEC dbo.SetSecurityPageBySecurityNamePageSecureId " +
            "@SecurityName, @PageSecureId, @IsConfirm, @SiteNameId";
        Label lbl = (Label)item.FindControl("lbl_pageId");
        int pageId = int.Parse(lbl.Text);
        for (int i = 1; i < 6; i++)
        {
            CheckBox cb = (CheckBox)item.FindControl("_cb" + i);
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SecurityName", ("PUBLISHER" + (i)));
            cmd.Parameters.AddWithValue("@PageSecureId", pageId);
            cmd.Parameters.AddWithValue("@IsConfirm", cb.Checked);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            int a = cmd.ExecuteNonQuery();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
    }
    protected void cb_checkedUp(object sender, EventArgs e)
    {
        CheckBox _cb = (CheckBox)sender;
        RepeaterItem item = (RepeaterItem)_cb.NamingContainer;
        Repeater _rep = (Repeater)item.FindControl("_reapeterIn");
        foreach (RepeaterItem item2 in _rep.Items)
        {
            ((CheckBox)item2.FindControl(_cb.ID)).Checked = _cb.Checked;
        }
    }
    protected void cb_checkedBot(object sender, EventArgs e)
    {
        CheckBox _cb = (CheckBox)sender;
        Repeater _rep = (Repeater)_cb.NamingContainer.NamingContainer;
        bool isTrue = false;
        foreach (RepeaterItem item in _rep.Items)
        {
            if (((CheckBox)item.FindControl(_cb.ID)).Checked)
            {
                isTrue = true;
                break;
            }
        }
        RepeaterItem itemMain = (RepeaterItem)_rep.NamingContainer;
        ((CheckBox)itemMain.FindControl(_cb.ID)).Checked = isTrue;
        
    }
}
