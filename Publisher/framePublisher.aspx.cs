using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Publisher_framePublisher : PageSetting
{
    
    protected RegionTab result;   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           // if (string.IsNullOrEmpty(GetGuidSetting()))
            LoadUserSetting();
            whatCityPage(siteSetting.GetSiteID);
            string _stage = Request["EndRegister"];
            if(_stage == "true")
                iframe.Attributes.Add("src", ResolveUrl("~") + @"Publisher/PublisherPayment.aspx");
            else
                iframe.Attributes.Add("src", ResolveUrl("~") + @"Management/professionalDetails.aspx");
            iframe.Attributes.Add("height", "1100px");
            iframe.Attributes.Add("style", "overflow:hidden");
       //     ClientScript.RegisterStartupScript(this.GetType(), "show_div", "showDiv();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "show_div", "showDiv();", true);

            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
        }
        Header.DataBind();
    }

    private void LoadUserSetting()
    {
        string _id = Request.QueryString["UserId"];
        if (string.IsNullOrEmpty(_id) || _id == GetGuidSetting())
            return;

        string name = Request.QueryString["UserName"];
        
        UserMangement um = new UserMangement(_id, name);
        SetGuidSetting(um);
    }
    

    private void whatCityPage(string SiteId)
    { 
        result = DBConnection.GetRegionTab(SiteId);                    
    }


    protected string cityPage
    {
        get
        {
            if (result== RegionTab.Map)
                return "location2014.aspx";//"LocationMap.aspx";//"professionalCities.aspx";            
            else if (result ==  RegionTab.Tree)
                return "professionalCities3.aspx";
            else
                return "professionalCities3.aspx";

        }


    }
    protected string GetPaymentInfo
    {
        get { return Server.HtmlEncode(Hidden_PaymentInfo.Value); }
    }
    protected string GetSegmentsArea
    {
        get { return Server.HtmlEncode(Hidden_SegmentsArea.Value); }
    }
    protected string GetCities
    {
        get { return Server.HtmlEncode(Hidden_Cities.Value); }
    }
    protected string Get_Time
    {
        get { return Server.HtmlEncode(Hidden_Time.Value); }
    }
    protected string Get_MyCredits
    {
        get { return Server.HtmlEncode(Hidden_MyCredits.Value); }
    }
    protected string Get_PublisherNotes
    {
        get { return Server.HtmlEncode(Hidden_PublisherNotes.Value); }
    }
    protected string Get_Audit
    {
        get { return Server.HtmlEncode(Hidden_Audit.Value); }
    }
    protected string Get_VirtualNumbers
    {
        get { return Server.HtmlEncode(Hidden_VirtualNumbers.Value); }
    }
    protected string Get_GeneralInfo
    {
        get { return Server.HtmlEncode(Hidden_GeneralInfo.Value); }
    }
    
}
