﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallAdvertiserReport2 : PageSetting
{
    const string UNKNOWN = "unknown";
    const string Num_Format = "{0:#,0.##}";
    const int ONE_DAY = 1;
    const string V_ICON = "~/Publisher/images/icon-v.png";
    const string X_ICON = "~/Publisher/images/icon-x.png";
    const string REPORT_NAME = "Pro Report";
    private const string BASE_PATH = "~/Publisher/ClipCallControls/";
    protected static readonly string SessionTableName = "dataClipCallProReport";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
         //   dataV = null;
            setDateTime();
            LoadSteps();
            LoadSupplierStatus();
            LoadInactiveReasons();
            LoadSupplierOld();
            LoadDateSelector();
        }
        SetToolboxEvents();
        Header.DataBind();
    }

    private void LoadInactiveReasons()
    {
        ddl_InactiveReason.Items.Clear();
        ListItem li_first = new ListItem("ALL", "ALL");
        li_first.Selected = true;
        ddl_InactiveReason.Items.Add(li_first);
        foreach (ClipCallReport.eInactiveReason sir in Enum.GetValues(typeof(ClipCallReport.eInactiveReason)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(sir), sir.ToString());
            ddl_InactiveReason.Items.Add(li);
        }
    }

    private void LoadDateSelector()
    {
        rbl_date.Items.Clear();
        foreach(ClipCallReport.eDateSelector ds in Enum.GetValues(typeof(ClipCallReport.eDateSelector)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(ds), ds.ToString());
            li.Selected = ds == ClipCallReport.eDateSelector.None;
            rbl_date.Items.Add(li);
        }
    }

    private void LoadSupplierOld()
    {

        ddl_oldPro.Items.Clear();
        foreach (ClipCallReport.eSupplierOldStatus sir in Enum.GetValues(typeof(ClipCallReport.eSupplierOldStatus)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(sir), sir.ToString());
            li.Selected = sir == ClipCallReport.eSupplierOldStatus.ALL;
            ddl_oldPro.Items.Add(li);
        }
    }
    private void LoadSteps()
    {
        ddl_Step.Items.Clear();
        foreach (ClipCallReport.eStepInAppRegistration sir in Enum.GetValues(typeof(ClipCallReport.eStepInAppRegistration)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(sir), sir.ToString());
            li.Selected = sir == ClipCallReport.eStepInAppRegistration.ALL;
            ddl_Step.Items.Add(li);
        }
    }
    private void LoadSupplierStatus()
    {
        ddl_SupplierStatus.Items.Clear();
        foreach (ClipCallReport.eSupplierStatusAppRegistration sir in Enum.GetValues(typeof(ClipCallReport.eSupplierStatusAppRegistration)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(sir), sir.ToString());
            li.Selected = sir == ClipCallReport.eSupplierStatusAppRegistration.ALL;
            ddl_SupplierStatus.Items.Add(li);
        }
    }

    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(REPORT_NAME);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.RegistrationAppReportRequest request = new ClipCallReport.RegistrationAppReportRequest();

        request.From = _FromToDatePicker.GetDateFrom;
        request.To = _FromToDatePicker.GetDateTo;
        ClipCallReport.eStepInAppRegistration _step;
        if (!Enum.TryParse(ddl_Step.SelectedValue, out _step))
            _step = ClipCallReport.eStepInAppRegistration.ALL;
        request.Step = _step;
        ClipCallReport.eSupplierStatusAppRegistration _status;
        if (!Enum.TryParse(ddl_SupplierStatus.SelectedValue, out _status))
            _status = ClipCallReport.eSupplierStatusAppRegistration.ALL;
        request.SupplierStatus = _status;

        ClipCallReport.eSupplierOldStatus old_pro;
        if (!Enum.TryParse(ddl_oldPro.SelectedValue, out old_pro))
            old_pro = ClipCallReport.eSupplierOldStatus.ALL;
        request.SupplierOldStatus = old_pro;

        ClipCallReport.eDateSelector dateSelector;
        if (!Enum.TryParse(rbl_date.SelectedValue, out dateSelector))
            dateSelector = ClipCallReport.eDateSelector.None;
        request.DateSelector = dateSelector;

        ClipCallReport.eInactiveReason inactiveReason;
        if (!Enum.TryParse(ddl_InactiveReason.SelectedValue, out inactiveReason))
            request.InactiveReason = null;
        else
            request.InactiveReason = inactiveReason;

        ClipCallReport.ResultOfListOfRegistrationAppReportResponse response = null;

        try
        {
            response = report.GetRegistrationRepost(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (response.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        _ProReportTable.BindReport(response.Value, SessionTableName);
        lbl_RecordMached.Text = response.Value.Length.ToString();
       
    }
    private string GetUrl(string url, bool isHttps)
    {
        if (string.IsNullOrEmpty(url))
            return null;
        if (url.StartsWith("http"))
            return url;
        return string.Format("http{0}://{1}", (isHttps ? "s" : ""), url);
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow row = (HtmlTableRow)(e.Item.FindControl("tr_show"));
        string css_class = (e.Item.ItemIndex % 2 == 0) ? "odd" : "even";
        string exists_class = row.Attributes["class"];
        row.Attributes.Add("class", string.IsNullOrEmpty(exists_class) ?
            css_class : exists_class + " " + css_class);
    }
    string GetDateTimeStringFormat(DateTime dt)
    {
        return string.Format(siteSetting.DateFormat, dt) + " " +
                    string.Format(siteSetting.TimeFormat, dt);
    }
    /*
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    */
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }

    protected string InactiveSupplierUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallAdvertiserReport2.aspx/InactiveSupplier"); }
    }
    protected string ActiveSupplierUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallAdvertiserReport2.aspx/ActiveSupplier"); }
    }

    [WebMethod(MessageName = "InactiveSupplier")]
    public static string InactiveSupplier(Guid supplierId)
    {

        return Newtonsoft.Json.JsonConvert.SerializeObject(InactiveSupplierManager.InactiveSupplierReason(supplierId, ClipCallReport.eInactiveReason.Blocked));
    }

    
    [WebMethod(MessageName = "ActiveSupplier")]
    public static string ActiveSupplier(Guid supplierId)
    {
        /*
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfGeneralResponse result = null;
        try
        {
            result = ccreport.ActiveSupplier(supplierId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ClipCallReport.GeneralResponse _response = new ClipCallReport.GeneralResponse()
            {
                FailedReason = exc.Message,
                IsSucceeded = false
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(_response);
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            ClipCallReport.GeneralResponse _response = new ClipCallReport.GeneralResponse()
            {
                FailedReason = "Server Failure",
                IsSucceeded = false
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(_response);
        }
        return Newtonsoft.Json.JsonConvert.SerializeObject(result.Value);
         * */
        return Newtonsoft.Json.JsonConvert.SerializeObject(InactiveSupplierManager.ActiveSupplier(supplierId));
    }
    public string GetServicePage
    {
        get
        {
            return ResolveUrl("~/Publisher/ClipCallAdvertiserReport2.aspx/GetReportPage");
        }
    }
    [WebMethod(MessageName = "GetReportPage")]
    public static string GetReportPage()
    {
        PageRenderin page = new PageRenderin();
        page.EnableEventValidation = false;
        UserControl ctl =
             (UserControl)page.LoadControl(BASE_PATH + "ProReportTable.ascx");

        MethodInfo _translate = ctl.GetType().GetMethod("SetDataByPage");
        bool IsBindData = (bool)_translate.Invoke(ctl, new object[] { SessionTableName });
        if (!IsBindData)
            return "done";
        ctl.EnableViewState = false;
        

        //Render control to string
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }

}