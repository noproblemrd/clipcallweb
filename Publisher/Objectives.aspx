﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" 
AutoEventWireup="true" CodeFile="Objectives.aspx.cs" Inherits="Publisher_Objectives" EnableEventValidation="false" 
Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>
<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

<script src="../jquery.min.js" type="text/javascript"></script> 
<script src="../jquery-ui.min.js" type="text/javascript"></script> 


<script type="text/javascript" language="javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {  
    //    alert('show');        
        showDiv();
    }
    function EndHandler()
    {
   //     alert('hide');
        hideDiv();
    }
   function closePopup()
   {
        ClearPopup();
        $find('_modal').hide();
   }
   function ClearPopup()
   {
        document.getElementById("<%# PopUpEditObjective.ClientID %>").className="popModal_del popModal_del_hide";	        
        document.getElementById("<%# ddl_criteria.ClientID %>").selectedIndex = 0;
        document.getElementById("<%# txt_Objecive.ClientID %>").value = "";
        document.getElementById("<%# hf_ObjectiveId.ClientID %>").value = "";
        document.getElementById("<%# GetFromId %>").value = "";
        document.getElementById("<%# GetToId %>").value = "";
        
        var validators = Page_Validators;  
        for (var i = validators.length-1; i > -1; i--)
        {           
            validators[i].style.visibility = "hidden";
        }
   }
   function openPopup()
   {
        document.getElementById("<%# PopUpEditObjective.ClientID %>").className = "popModal_del";
        $find('_modal').show();
   }
   function ConfirmOverride()
   {
        var ToOverride = confirm("<%# GetConfirmOverride %>");
        if(ToOverride)
        { 
                     
            <%# GetConfirmedPostBack %>; 
            $find('_modal').hide();  
   //         showDiv();                    
        }
   }
   function ChkDates()
   {        
        if (!Check_DateValidation(document.getElementById("<%# GetDivDatesId %>")))
        {
            alert("<%# lbl_MissingDates.Text %>");
            return false;
        }
        return (Page_ClientValidate('EditObjecive'))
            
   } 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<cc1:ToolkitScriptManager runat="server" ID="ScriptManager1" ></cc1:ToolkitScriptManager>
<uc1:Toolbox ID="Toolbox1" runat="server" />
<h5><asp:Label ID="lblSubTitle" runat="server">Objective Managment</asp:Label></h5>

<div class="page-content minisite-content5">
	
    <div id="form-analyticsseg">
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                CssClass="data-table" AllowPaging="True" PageSize="20" ShowFooter="true"
                onpageindexchanging="_GridView_PageIndexChanging">
                
                <RowStyle CssClass="__even" />
                <AlternatingRowStyle CssClass="__odd" />
                <PagerStyle HorizontalAlign="Center" CssClass="pager" /> 
                <FooterStyle CssClass="footer"  />
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                            <br />
                            <asp:CheckBox ID="cb_all" CssClass="form-checkbox" runat="server" ></asp:CheckBox>
                        </HeaderTemplate>
                        <ItemTemplate>        
                            <asp:CheckBox ID="cb_choose" CssClass="form-checkbox" runat="server" />
                            <asp:Label ID="lbl_ObjectiveId" runat="server" Text="<%# Bind('ObjectiveId') %>" Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_criteria.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label11" runat="server" Text="<%# Bind('CriteriaTxt') %>"></asp:Label>
                            <asp:Label ID="lbl_CriteriaEnum" runat="server" Text="<%# Bind('Criteria') %>" Visible="false"></asp:Label>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2" runat="server" Text="<%# lblFromDate.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFromDate" runat="server" Text="<%# Bind('FromDate') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lblToDate.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblToDate" runat="server" Text="<%# Bind('ToDate') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label4" runat="server" Text="<%# lbl_objecive.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmountShow" runat="server" Text="<%# Bind('Amount') %>"></asp:Label>
                            <asp:Label ID="lbl_amount" runat="server" Text="<%# Bind('AmountOriginal') %>" Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                           
            </asp:GridView>
        
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<div id="PopUpEditObjective" runat="server" style="max-height:450px;display:none;" class="popModal_del popModal_del_hide"  >
	<div class="top"></div>
	<a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
    	<div class="content">
    	<div class="f2">
            <h2><asp:Label ID="lbl_ManagmentForm" runat="server"  Text="Managment Form"></asp:Label></h2>
    	    <div style="margin-left:40px;">
                <asp:Label ID="lblCriteria" CssClass="label4" runat="server" Text="<%# lbl_criteria.Text %>"></asp:Label>     
                <asp:HiddenField ID="hf_ObjectiveId" runat="server" />
    	    
    	        <asp:DropDownList ID="ddl_criteria"  CssClass="form-select"  runat="server">
                </asp:DropDownList>
    	    
    	    
                <div class="modal1"><uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" /></div>
    	    
    	   
                <asp:Label ID="lblObjecive" runat="server" CssClass="label4" Text="<%# lbl_objecive.Text %>"></asp:Label>
    	    
    	    
    	        <asp:TextBox ID="txt_Objecive" CssClass="form-text" runat="server"></asp:TextBox>
    	        <asp:RequiredFieldValidator ID="RequiredFieldValidator_Objecive" runat="server"
    	         ErrorMessage="Missing" Display="Dynamic" CssClass="error-msg"
                 ValidationGroup="EditObjecive" ControlToValidate="txt_Objecive" ></asp:RequiredFieldValidator>
                 
                <asp:RangeValidator ID="RangeValidator_Objecive" CssClass="form-error" runat="server" 
                ErrorMessage="Invalid number" ControlToValidate="txt_Objecive"
                ValidationGroup="EditObjecive" Display="Dynamic" Type="Double"
                 MinimumValue="0" MaximumValue="1000000"></asp:RangeValidator>
    	   </div>
        </div>  	
	    </div>
	<div class="content">
	<table class="Table_Button">
    <tr>
    <td>
     <div class="popupsegments2">
        <asp:Button ID="btn_Set" runat="server" Text="Set" ValidationGroup="EditObjecive" OnClick="btn_Set_Click" 
        OnClientClick="return ChkDates();"  CssClass="btn" />
        
  
        <input id="btn_cancel" type="button" class="btn" value="Cancel" runat="server" onclick="javascript:closePopup();"/>
        </div>
    </td>
    </tr>
    </table>
	</div>
	<div class="bottom"></div>
        	
</div>
   
<cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btn_virtual"
    PopupControlID="PopUpEditObjective"
    BackgroundCssClass="modalBackground"
    BehaviorID="_modal"               
    DropShadow="false"
    ></cc1:ModalPopupExtender>
    
<div style="display:none;">
   <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />       
   <asp:Button ID="btn_virtual2" runat="server" style="display:none;"  />
</div>
    
<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
<asp:Label ID="lbl_criteria" runat="server" Text="Criteria" Visible="false"></asp:Label>
<asp:Label ID="lblFromDate" runat="server" Text="From date" Visible="false"></asp:Label>
<asp:Label ID="lblToDate" runat="server" Text="To date" Visible="false"></asp:Label>
<asp:Label ID="lbl_objecive" runat="server" Text="Objective" Visible="false"></asp:Label>


<asp:Label ID="lbl_ConfirmOverride" runat="server" Text="The objectives in this record are overlapping with objectives from other records. Save will delete the other objectives. Continue?" Visible="false"></asp:Label>

<asp:Label ID="lbl_MissingDates" runat="server" Text="Missing dates" Visible="false"></asp:Label>

</asp:Content>

