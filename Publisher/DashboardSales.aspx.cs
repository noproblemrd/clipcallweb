﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using System.Reflection;
using System.Text;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Publisher_DashboardSales : PageSetting
{
    const int WEEK_FROM_TODAY = 6;
    const string FILE_NAME = "FinancialDashboard.xml";
    const string Num_Format = "{0:#,0.##}";
    const string Num_Format_4_After_Point = "{0:#,0.####}";
    Dictionary<eFinancialDashboard, string> ListeFinancialDashboard;
    Dictionary<eFinancialDashboardInjection, string> ListeFinancialDashboardInjection;
    Dictionary<eFinancialDashboardGroup, string> ListeFinancialDashboardGroup;
    Dictionary<eFinancialDashboardInjection_Tooltip, string> ListeFinancialDashboardInjection_tooltip;
    Dictionary<eFinancialDashboardAddon_Tooltip, string> ListeFinancialDashboard_tooltip;
    bool IsInjection = false;

    

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadeFinancialDashboardTranslate();
        ScriptManager1.RegisterAsyncPostBackControl(btn_run);
        ScriptManager1.RegisterAsyncPostBackControl(lb_AddOn);
        ScriptManager1.RegisterAsyncPostBackControl(lb_Injedction);
        ScriptManager1.RegisterAsyncPostBackControl(ddl_Origin);
        if (!IsPostBack)
        {
            //        LoadInterval();
            requestV = null;
            dataV = null;
            setDateTime();
            LoadSentences();
            setSentencesDatePicker();
            LoadExperties();
            LoadLastUpdate();
            LoadFilters();
            LoadDefaultReport();
        }
        else
        {
            if (hf_Tab.Value == "injection")
                IsInjection = true;
        }
        LoadValidationCompare();
        BodyDataBind();
        Header.DataBind();
    }

    private void LoadLastUpdate()
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfDateTime result = null;
        try
        {
            result = reports.FinancialDashboardLastUpdate();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        lblLastUpdate.Text = string.Format(siteSetting.DateFormat, result.Value);
    }
    private void LoadFilters()
    {
        WebReferenceReports.Reports report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfFinancialDashboardFiltersContainer result = null;
        try
        {
            result = report.GetFinancialDashboardFilters();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        /**** Sliders Flavors   **/
        ddl_FlavorMain.Items.Add(new ListItem(string.Empty));
        ddl_FlavorCompare.Items.Add(new ListItem(string.Empty));
        foreach (string str in result.Value.Flavors)
        {
            ddl_FlavorMain.Items.Add(new ListItem(str, str));
            ddl_FlavorCompare.Items.Add(new ListItem(str, str));
        }
        ddl_FlavorMain.SelectedIndex = 0;
        ddl_FlavorCompare.SelectedIndex = 0;


        /**** Origin  AddOn  **/
        Dictionary<Guid, string> origin_addon = new Dictionary<Guid, string>();
        origin_addon.Add(Guid.Empty, string.Empty);
        foreach (WebReferenceReports.GuidStringPair gsp in result.Value.AddOnOrigins)
            origin_addon.Add(gsp.Id, gsp.Name);

        /**** Origin  Injection  **/
        Dictionary<Guid, string> origin_Injection = new Dictionary<Guid, string>();
        origin_Injection.Add(Guid.Empty, string.Empty);
        foreach (WebReferenceReports.GuidStringPair gsp in result.Value.InjectionOrigins)
            origin_Injection.Add(gsp.Id, gsp.Name);
        OriginAddOnV = origin_addon;
        OriginInjectionV = origin_Injection;
        SetOrigins();
    }
    void SetOrigins()
    {
        int _main = ddl_Origin.SelectedIndex;
        int _compare = ddl_OriginCompare.SelectedIndex;
        Dictionary<Guid, string> _origin = IsInjection ? OriginInjectionV : OriginAddOnV;
     //   div_LifeTime.Visible = !IsInjection;
        ddl_Origin.Items.Clear();
        ddl_OriginCompare.Items.Clear();
        foreach (KeyValuePair<Guid, string> kvp in _origin)
        {
            ddl_Origin.Items.Add(new ListItem(kvp.Value, kvp.Key.ToString()));
            ddl_OriginCompare.Items.Add(new ListItem(kvp.Value, kvp.Key.ToString()));
        }
        ddl_Origin.SelectedIndex = LastOriginMain;
        ddl_OriginCompare.SelectedIndex = LastOriginCompare;
        LastOriginMain = (_main > -1) ? _main : 0;
        LastOriginCompare = (_compare > -1) ? _compare : 0;
        _up_OriginCompare.Update();
        _up_OriginMain.Update();
    }
    private void LoadeFinancialDashboardTranslate()
    {
        ListeFinancialDashboard = new Dictionary<eFinancialDashboard, string>();
        foreach (eFinancialDashboard efd in Enum.GetValues(typeof(eFinancialDashboard)))
        {
            ListeFinancialDashboard.Add(efd, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eFinancialDashboard", efd.ToString()));
        }
        ListeFinancialDashboardInjection = new Dictionary<eFinancialDashboardInjection, string>();
        foreach (eFinancialDashboardInjection efd in Enum.GetValues(typeof(eFinancialDashboardInjection)))
        {
            ListeFinancialDashboardInjection.Add(efd, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eFinancialDashboardInjection", efd.ToString()));
        }
        ListeFinancialDashboardGroup = new Dictionary<eFinancialDashboardGroup, string>();
        foreach (eFinancialDashboardGroup efg in Enum.GetValues(typeof(eFinancialDashboardGroup)))
        {
            ListeFinancialDashboardGroup.Add(efg, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eFinancialDashboardGroup", efg.ToString()));
        }
        ListeFinancialDashboard_tooltip = new Dictionary<eFinancialDashboardAddon_Tooltip, string>();
        foreach (eFinancialDashboardAddon_Tooltip efg in Enum.GetValues(typeof(eFinancialDashboardAddon_Tooltip)))
        {
            ListeFinancialDashboard_tooltip.Add(efg, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eFinancialDashboardAddon_Tooltip", efg.ToString()));
        }

        ListeFinancialDashboardInjection_tooltip = new Dictionary<eFinancialDashboardInjection_Tooltip, string>();
        foreach (eFinancialDashboardInjection_Tooltip efg in Enum.GetValues(typeof(eFinancialDashboardInjection_Tooltip)))
        {
            ListeFinancialDashboardInjection_tooltip.Add(efg, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eFinancialDashboardInjection_Tooltip", efg.ToString()));
        }

    }

    private void BodyDataBind()
    {
        lbl_HeadingCompare.DataBind();
        lbl_OriginCompare.DataBind();
        lbl_FlavorCompare.DataBind();
        rv_LifeTime.MaximumValue = short.MaxValue.ToString();
        txt_LifeTime.Text = "80";
        
    }
    

    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        ddl_HeadingCompare.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        ddl_HeadingCompare.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;
        //    string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
            ddl_HeadingCompare.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;
        ddl_HeadingCompare.SelectedIndex = 0;

    }
    private void setSentencesDatePicker()
    {
        FromToDatePicker_current.SetFromSentence = lbl_activity.Text;
        FromToDatePicker_compare.SetFromSentence = lbl_activityPast.Text;
        
    }

    private void LoadSentences()
    {
        if (string.IsNullOrEmpty(lbl_sooner.Text))
            lbl_sooner.Text = @"The value should be sooner than the value in the 'activity dates from' field.";

    }

    private void LoadDefaultReport()
    {
        hf_Tab.Value = "addon";
        IsInjection = false;
        WebReferenceReports.FinancialDashboardRequest request = new WebReferenceReports.FinancialDashboardRequest();
        request.FromDate = DateTime.Now.AddDays(-6);
        request.ToDate = DateTime.Now;
        request.GraphToShow = eFinancialDashboard.ActualARDAU.ToString();     
        request.UseCompare = false;
        request.LifeTime = 80;
        request.UseOnlySoldHeadings = false;
        _ShowPast = false;
        RunReport(request);
    }

    private void LoadValidationCompare()
    {
        //compare dates
        TextBox txt_to = FromToDatePicker_compare.GetTextBoxTo();
        string _from = FromToDatePicker_compare.GetTextBoxFrom().ClientID;
        txt_to.Attributes.Add("readonly", "readonly");
        txt_to.CssClass = "_to label_DatePicker form-textcal";
        string _ScriptFunc = "if(OnBlurString(this)) SetCompareDate(this, '" + FromToDatePicker_current.GetParentDivId() +
            @"', """ + lbl_sooner.Text + @""", '" + FromToDatePicker_current.GetTextBoxFrom().ClientID + "');";
        string FromScript = @"$('#" + _from + @"').datepicker({ onClose: function(dateText, inst) { " + _ScriptFunc + " }});";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "FromCompareScript", FromScript, true);
        //current date
        string current_fromId = FromToDatePicker_current.GetTextBoxFrom().ClientID;
        string current_toId = FromToDatePicker_current.GetTextBoxTo().ClientID;

        string InsideScript = "var _result = OnBlurByDays(this); if(_result == 1) Set_Dates_Compars(); else if(_result == -10) " +
            "{ alert('" + lbl_DatesChange.Text + "'); Set_Dates_Compars(); }";
        string from_script =
            @"$('#" + current_fromId + @"').datepicker({ onClose: function(dateText, inst) { " + InsideScript + "}});";
        string to_script =
            @"$('#" + current_toId + @"').datepicker({ onClose: function(dateText, inst) { " + InsideScript + "}});";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "FromCurrentScript", from_script, true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ToCurrentScript", to_script, true);
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (!string.IsNullOrEmpty(PathV))
        {

            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSArea.swf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + _chart + "');", true);
        }

    }
    
    private void setDateTime()
    {
        FromToDatePicker_current.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
            WEEK_FROM_TODAY);
    }
    protected void btn_run_Click(object sender, EventArgs e)
    {
        _RunReport();
    }
    void _RunReport()
    {
        // requestV = null;
        if (IsInjection != WasInjection)
            SetOrigins();
        WebReferenceReports.FinancialDashboardRequest request = new WebReferenceReports.FinancialDashboardRequest();
        request.FromDate = FromToDatePicker_current.GetDateFrom;
        request.ToDate = FromToDatePicker_current.GetDateTo;
        request.HeadingId = new Guid(ddl_Heading.SelectedValue);
        request.OriginId = new Guid(ddl_Origin.SelectedValue);
        request.GraphToShow = (requestV == null)? eFinancialDashboard.Exposures.ToString() : requestV.GraphToShow;
        request.Flavor = ddl_FlavorMain.SelectedValue;
        request.UseOnlySoldHeadings = cb_UseOnlySoldHeadings.Checked;
        int LifeTime;
        if (!int.TryParse(hf_LifeTime.Value, out LifeTime))
        {
            LifeTime = 80;
            txt_LifeTime.Text = "80";
        }
        request.LifeTime = LifeTime;
        requestV = null;
        _ShowPast = cb_Compare.Checked;
        request.IsInjections = IsInjection;

        if (cb_Compare.Checked)
        {
            request.UseCompare = true;
            request.CompareFlavor = ddl_FlavorCompare.SelectedValue;
            request.CompareFromDate = FromToDatePicker_compare.GetDateFrom;
            request.CompareToDate = FromToDatePicker_compare.GetDateTo;
            request.CompareOriginId = new Guid(ddl_OriginCompare.SelectedValue);
            request.CompareHeadingId = new Guid(ddl_HeadingCompare.SelectedValue);
        }
        RunReport(request);
    }
    
    void RunReport(WebReferenceReports.FinancialDashboardRequest request)
    {
       
        
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        
        WebReferenceReports.ResultOfFinancialDashboardResponse result = null;
        try
        {
            result = _report.FinancialDashboard(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        WasInjection = IsInjection;
        requestV = request;
        Dictionary<string, string> dic = dicAarCreteriaV;
        DataTable data = new DataTable();
        data.Columns.Add("Value");
        data.Columns.Add("PastValue");
        data.Columns.Add("Row");
        data.Columns.Add("RowText");
        data.Columns.Add("ShowPast", typeof(bool));
        data.Columns.Add("index", typeof(int));
        data.Columns.Add("Gap");
        data.Columns.Add("tooltip");
        data.Columns.Add("EnumIndex", typeof(int));

       
        if (request.IsInjections)
        {
            SetInstaltionDataRow(data, result.Value);
            eFinancialDashboardInjection efd2;
            if (!Enum.TryParse(request.GraphToShow, out efd2))
                efd2 = eFinancialDashboardInjection.Exposures;
            LoadChart(result.Value, ListeFinancialDashboardInjection[efd2]);
        }
        else
        {
            SetAddOnDataRow(data, result.Value);
            eFinancialDashboard efd2;
            if (!Enum.TryParse(request.GraphToShow, out efd2))
                efd2 = eFinancialDashboard.Exposures;
            LoadChart(result.Value, ListeFinancialDashboard[efd2]);
        }
        
        dataV = data;
        SetTable(data);      

    }
    void SetAddOnDataRow(DataTable data, WebReferenceReports.FinancialDashboardResponse _value)
    {
        foreach (PropertyInfo pi in _value.MainTableData.GetType().GetProperties())
        {
            eFinancialDashboard efd;
            if (!Enum.TryParse(pi.Name, out efd))
                continue;
            DataRow row = data.NewRow();
            row["RowText"] = ListeFinancialDashboard[efd];// "eFinancialDashboard", pi.Name);
            row["EnumIndex"] = (int)efd;
            row["ShowPast"] = _ShowPast;
            row["Row"] = pi.Name;
            row["index"] = (int)((int)efd / 10);
            object o = pi.GetValue(_value.MainTableData, null);
            Type typeObject = o.GetType();
            if (typeObject == typeof(double) || typeObject == typeof(int))
                row["Value"] = GetNumber(o, efd, false);//string.Format("{0:0.####}", o);
            else
                row["Value"] = o.ToString();
            if (_ShowPast)
            {
                object oPast = _value.CompareTableData.GetType().GetProperty(pi.Name).GetValue(_value.CompareTableData, null);
                object oGap = _value.GapTableData.GetType().GetProperty(pi.Name).GetValue(_value.GapTableData, null);
                Type typeObjectPast = oPast.GetType();
                if (typeObjectPast == typeof(double) || typeObjectPast == typeof(int))
                {
                    row["PastValue"] = GetNumber(oPast, efd, true);//string.Format("{0:0.####}", oPast);
                    row["Gap"] = GetNumber(oGap, efd, null);
                }
                else
                {
                    row["PastValue"] = oPast.ToString();
                    row["Gap"] = oGap.ToString();
                }
                //    row["Gap"] = 
            }

            else
            {
                row["PastValue"] = string.Empty;
                row["Gap"] = string.Empty;
            }

            eFinancialDashboardAddon_Tooltip efdit;
            if (Enum.TryParse(pi.Name, out efdit))
                row["tooltip"] = ListeFinancialDashboard_tooltip[efdit];
            else
                row["tooltip"] = string.Empty;
            data.Rows.Add(row);
            
        }
    }
    void SetInstaltionDataRow(DataTable data, WebReferenceReports.FinancialDashboardResponse _value)
    {
        foreach (PropertyInfo pi in _value.MainTableData.GetType().GetProperties())
        {
            eFinancialDashboardInjection efd;
            if (!Enum.TryParse(pi.Name, out efd))
                continue;
            DataRow row = data.NewRow();
            row["RowText"] = ListeFinancialDashboardInjection[efd];// "eFinancialDashboard", pi.Name);
            row["EnumIndex"] = (int)efd;
            row["ShowPast"] = _ShowPast;
            row["Row"] = pi.Name;
            row["index"] = (int)((int)efd / 100);
            object o = pi.GetValue(_value.MainTableData, null);
            Type typeObject = o.GetType();
            if (typeObject == typeof(double) || typeObject == typeof(int))
                row["Value"] = GetNumber(o, efd, false);//string.Format("{0:0.####}", o);
            else
                row["Value"] = o.ToString();
            if (_ShowPast)
            {
                object oPast = _value.CompareTableData.GetType().GetProperty(pi.Name).GetValue(_value.CompareTableData, null);
                object oGap = _value.GapTableData.GetType().GetProperty(pi.Name).GetValue(_value.GapTableData, null);
                Type typeObjectPast = oPast.GetType();
                if (typeObjectPast == typeof(double) || typeObjectPast == typeof(int))
                {
                    row["PastValue"] = GetNumber(oPast, efd, true);//string.Format("{0:0.####}", oPast);
                    row["Gap"] = GetNumber(oGap, efd, null);
                }
                else
                {
                    row["PastValue"] = oPast.ToString();
                    row["Gap"] = oGap.ToString();
                }
                
            }

            else
            {
                row["PastValue"] = string.Empty;
                row["Gap"] = string.Empty;
            }
            eFinancialDashboardInjection_Tooltip efdit;
            if (Enum.TryParse(pi.Name, out efdit))
                row["tooltip"] = ListeFinancialDashboardInjection_tooltip[efdit];
            else
                row["tooltip"] = string.Empty;
            data.Rows.Add(row);
        }
    }
    string GetNumber(object o, eFinancialDashboard efd, bool? IsCompare)
    {
        string _start = "";
        string _end = "";
        if (IsCompare.HasValue)
        {
            switch (efd)
            {

                case (eFinancialDashboard.Exposures):
                case (eFinancialDashboard.Requests):
                case (eFinancialDashboard.RequestsThatGeneratedActualRevenues):
                case (eFinancialDashboard.RequestsThatGeneratedNetActualRevenue):                
                case (eFinancialDashboard.AverageDailyExposures):
                case (eFinancialDashboard.AverageDau):
                    break;
                case (eFinancialDashboard.AverageDailyExposuresDividedByAverageDau):
                case (eFinancialDashboard.Conversion):
                case (eFinancialDashboard.RoiFullFullfilment):
                case (eFinancialDashboard.RoiActual):
                case (eFinancialDashboard.RoiNetActual):
                case (eFinancialDashboard.RoiPotential):
                    _end = "%";
                    break;
                case (eFinancialDashboard.CostOfAddOnInstallation):
                    if ((!IsCompare.Value && new Guid(ddl_Origin.SelectedValue) == Guid.Empty) || (IsCompare.Value && new Guid(ddl_OriginCompare.SelectedValue) == Guid.Empty))
                        _end = " avg";
                    goto case (eFinancialDashboard.PotentialRevenue);
                case (eFinancialDashboard.PotentialRevenue):
                case (eFinancialDashboard.DailyPotentialRevenue):
                case (eFinancialDashboard.LifetimeTimesPotentialARDAU):
                case (eFinancialDashboard.ActualRevenue):
                case (eFinancialDashboard.DailyActualRevenue):
                case (eFinancialDashboard.LifetimeTimesActualARDAU):
                case (eFinancialDashboard.NetActualRevenue):
                case (eFinancialDashboard.LifetimeTimesNetActualRevenueARDAU):
                case (eFinancialDashboard.AveragePotentialPPL):
                case (eFinancialDashboard.AverageActualPPL):
                case (eFinancialDashboard.AverageNetActualPPL):
                case (eFinancialDashboard.AverageFullFullfilmentPPL):
                case (eFinancialDashboard.FullFullfilmentRevenue):
                case (eFinancialDashboard.DailyFullFullfilmentRevenue):
                case (eFinancialDashboard.FullFullfilmentARDAU):                
                case (eFinancialDashboard.DailyNetActualRevenue):               
                    _start = "$";//"$" + string.Format(Num_Format, o);
                    break;
                case (eFinancialDashboard.PotentialARDAU):
                case (eFinancialDashboard.ActualARDAU):
                case (eFinancialDashboard.NetActualARDAU):
                    _end = "¢";
                    break;

            }
        }
        else
            _end = "%";
        Type typeObject = o.GetType();
        if (typeObject == typeof(double))
        {
            double num = (double)o;
            if (double.IsInfinity(num) || double.IsNaN(num))
                return o.ToString();
            if (_end == "¢")
                num = num * 100;
        }
        if (efd == eFinancialDashboard.ActualARDAU
            || efd == eFinancialDashboard.FullFullfilmentARDAU
            || efd == eFinancialDashboard.NetActualARDAU
            || efd == eFinancialDashboard.PotentialARDAU)
        {
            return _start + string.Format(Num_Format_4_After_Point, o) + _end;
        }
        else
        {
            return _start + string.Format(Num_Format, o) + _end;
        }
    }
    string GetNumber(object o, eFinancialDashboardInjection efd, bool? IsCompare)
    {
        string _start = "";
        string _end = "";
        if (IsCompare.HasValue)
        {
            Type typeObject = o.GetType();
            if (typeObject == typeof(double))
            {
                double num = (double)o;
                if (double.IsInfinity(num) || double.IsNaN(num))
                    return o.ToString();

            }
            switch (efd)
            {

                case (eFinancialDashboardInjection.Exposures):
                case (eFinancialDashboardInjection.Requests):
                
                    break;
                case (eFinancialDashboardInjection.Conversion):
                case (eFinancialDashboardInjection.AverageDailyExposuresDividedByAverageDau):
                    _end = "%";
                    break;
                case (eFinancialDashboardInjection.PotentialRevenue):
                case (eFinancialDashboardInjection.CostPerRequestActual):
                case (eFinancialDashboardInjection.CostPerRequestNetActual):
                case (eFinancialDashboardInjection.CostPerRequestPotential):
                case (eFinancialDashboardInjection.ActualRevenue):
                case (eFinancialDashboardInjection.TotalCostOfRequestsActual):
                case (eFinancialDashboardInjection.TotalCostOfRequestsNetActual):
                case (eFinancialDashboardInjection.TotalCostOfRequestsPotential):
                case (eFinancialDashboardInjection.NetActualRevenue):
                case (eFinancialDashboardInjection.CostPerMilliaPotential):
                case (eFinancialDashboardInjection.CostPerMilliaNetActual):
                case (eFinancialDashboardInjection.CostPerMilliaActual):
                case (eFinancialDashboardInjection.AveragePotentialPPL):
                case (eFinancialDashboardInjection.AverageActualPPL):
                case (eFinancialDashboardInjection.AverageNetActualPPL):
                case (eFinancialDashboardInjection.AverageFullFullfilmentPPL):
                case (eFinancialDashboardInjection.RevenuePerMilliaPotential):
                case (eFinancialDashboardInjection.RevenuePerMilliaActual):
                case (eFinancialDashboardInjection.RevenuePerMilliaNetActual):
                case (eFinancialDashboardInjection.FullFullfilmentRevenue):               
                case (eFinancialDashboardInjection.FullFullfilmentARDAU):
                case (eFinancialDashboardInjection.TotalCostOfRequestsFullFullfilment):
                case (eFinancialDashboardInjection.CostPerRequestFullFullfilment):
                case (eFinancialDashboardInjection.CostPerMilliaFullFullfilment):
                case (eFinancialDashboardInjection.RevenuePerMilliaFullFullfilment):
                case (eFinancialDashboardInjection.LifetimeTimesFullFullfilmentRevenueARDAU):
                case (eFinancialDashboardInjection.ActualARDAU):
                case (eFinancialDashboardInjection.LifetimeTimesActualARDAU):
                case (eFinancialDashboardInjection.NetActualARDAU):
                case (eFinancialDashboardInjection.LifetimeTimesNetActualRevenueARDAU):
                case (eFinancialDashboardInjection.PotentialARDAU):
                case (eFinancialDashboardInjection.LifetimeTimesPotentialARDAU):
                case (eFinancialDashboardInjection.CostOfRevenueGeneratingLeadsPotential):
                case (eFinancialDashboardInjection.CostOfRevenueGeneratingLeadsActual):
                case (eFinancialDashboardInjection.CostOfRevenueGeneratingLeadsNetActual):
                case (eFinancialDashboardInjection.CostOfRevenueGeneratingLeadsFullFullfilment):
                case (eFinancialDashboardInjection.NetActualRevenueUpsales):
                case (eFinancialDashboardInjection.TotalCostOfRequestsNetActualUpsales): 
                    _start = "$";//"$" + string.Format(Num_Format, o);
                    break;  
            }
        }
        else
            _end = "%";

        if (efd == eFinancialDashboardInjection.ActualARDAU
            || efd == eFinancialDashboardInjection.FullFullfilmentARDAU
            || efd == eFinancialDashboardInjection.NetActualARDAU
            || efd == eFinancialDashboardInjection.PotentialARDAU)
        {
            return _start + string.Format(Num_Format_4_After_Point, o) + _end;
        }
        else
        {
            return _start + string.Format(Num_Format, o) + _end;
        }
    }

    bool SetVisibleTR(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
            return false;
        return true;
    }
    private void SetTable(DataTable data)
    {
        /*
        if (data.Rows.Count > 0)
            ShowPast = (bool)data.Rows[0]["show_PastPeriod"];
        else
            ShowPast = false;
       */
   //     dataV = data;
        var query = from x in data.AsEnumerable()
                    orderby x.Field<int>("index")
                    group x by x.Field<int>("index") into g
                    select new { _group = GeteFinancialDashboardGroupStr(g.Key), index = g.Key };

        _RepeaterParent.DataSource = query;
        _RepeaterParent.DataBind();

   //     _Repeater.DataSource = data;
    //    _Repeater.DataBind();
        UpdatePanel_table.Update();
    }
    string GeteFinancialDashboardGroupStr(int _index)
    {
        if (!Enum.IsDefined(typeof(eFinancialDashboardGroup), _index))
            return string.Empty;
        return ListeFinancialDashboardGroup[(eFinancialDashboardGroup)_index];
    }
    protected void _Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow tr_data = (HtmlTableRow)e.Item.FindControl("tr_data");
        tr_data.Attributes["class"] = (e.Item.ItemType == ListItemType.Item) ? "odd" : "even";
    }
    protected void _RepeaterParent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.AlternatingItem && e.Item.ItemType != ListItemType.Item)
            return;
        HtmlTableCell _td = (HtmlTableCell)e.Item.FindControl("td_group");
        _td.ColSpan = (_ShowPast) ? 4 : 2;
        int _index = int.Parse(((Label)e.Item.FindControl("lbl_index")).Text);
        if (_index == 0)
            ((HtmlTableRow)e.Item.FindControl("tr_group")).Visible = false;
        Repeater r = (Repeater)e.Item.FindControl("_Repeater");
        IEnumerable<DataRow> query = from x in dataV.AsEnumerable()
                                     where x.Field<int>("index") == _index
                                     orderby x.Field<int>("EnumIndex")
                                     select x;
        r.DataSource = query.CopyToDataTable();
        r.DataBind();

    }
    #region  chart
    bool LoadChart(WebReferenceReports.FinancialDashboardResponse result, string caption)
    {
        /*
        eFinancialDashboard efd;
        if(!Enum.TryParse(requestV.GraphToShow, out efd))
            efd = eFinancialDashboard.ActualARDAU;
        string caption = ListeFinancialDashboard[efd];
         * */
        //    string SymboleValue = CriteriaSetting.GetSymbolCriteria(ceriteria, siteSetting.CurrencySymbol);
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + caption + @"' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='19' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");

        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar='=' ");

        sb.Append(@"bgColor='FFFFFF'>");


        int NumOf_X = result.MainGraph.Length;

        int modulu = result.MainGraph.Length / 15;
        modulu++;

        sb.Append(@"<categories>");
        for (int i = 0; i < result.MainGraph.Length; i++)
        {
            string num = (double.IsInfinity(result.MainGraph[i].Value) || double.IsNaN(result.MainGraph[i].Value)) ? "0" : String.Format("{0:0.####}", result.MainGraph[i].Value);
            string _title = lbl_Current.Text + "= " + string.Format("{0:dd/MM/yyyy}", result.MainGraph[i].Date) + ", (" +
                num + ")";
            if (result.CompareGraph != null && result.CompareGraph.Length > i)
            {
                string numPast = (double.IsNaN(result.CompareGraph[i].Value) || double.IsInfinity(result.CompareGraph[i].Value)) ? "0" : String.Format("{0:0.####}", result.CompareGraph[i].Value);
                _title += "\r\n" + lbl_CompareValue.Text + "= " + string.Format("{0:dd/MM/yyyy}", result.CompareGraph[i].Date) + ", (" +
                    numPast + ")";
            }

            _title += "\r\n" + lbl_CurrentDot.Text;

            //***Dilute labels***//
            //       if ((i + 1) % modulu == 0)
            sb.Append(@"<category name='" + string.Format("{0:dd/MM/yyyy}", result.MainGraph[i].Date) + @"' toolText='" + _title + "'/>");
            //        else
            //             sb.Append(@"<category toolText='" + _title + "'/>");
        }
        sb.Append("</categories>");

        if (result.CompareGraph != null)
        {

            sb.Append(@"<dataset seriesName='" + lbl_CompareValue.Text + @"' color='808080'  anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'>");
            for (int i = 0; i < result.CompareGraph.Length && i < NumOf_X; i++)
            {
                string numPast = (double.IsNaN(result.CompareGraph[i].Value) || double.IsInfinity(result.CompareGraph[i].Value)) ? "0" : String.Format("{0:0.####}", result.CompareGraph[i].Value);
                sb.Append(@"<set value='" + numPast + "'/>");
            }
            sb.Append(@"</dataset>");
        }


        sb.Append(@"<dataset seriesName='" + lbl_Current.Text + @"' color='0080C0'  anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'>");
        for (int i = 0; i < result.MainGraph.Length; i++)
        {
            string num = (double.IsInfinity(result.MainGraph[i].Value) || double.IsNaN(result.MainGraph[i].Value)) ? "0" : String.Format("{0:0.####}", result.MainGraph[i].Value);
            sb.Append(@"<set value='" + num + "'/>");
        }
        sb.Append(@"</dataset>");

        sb.Append(@"</graph>");



        string path = ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) { return false; }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        return true;
    }
    protected void lb_row_click(object sender, EventArgs e)
    {
        WebReferenceReports.FinancialDashboardRequest request = requestV;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        LinkButton lb = (LinkButton)sender;
        string command = lb.CommandArgument;
        request.OnlyGraph = true;
        request.GraphToShow = command;
        WebReferenceReports.ResultOfFinancialDashboardResponse result = null;
        try
        {
            result = _report.FinancialDashboard(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        /*
        foreach (ListItem li in ddl_Showme.Items)
        {
            li.Selected = (li.Value == command);
        }
         
        _UpdatePanel_Showme.Update();
         * * */
        eFinancialDashboard _efd;
        if (!Enum.TryParse(command, out _efd))
            _efd = eFinancialDashboard.ActualARDAU;
        requestV = request;
        LoadChart(result.Value, ListeFinancialDashboard[_efd]);

    }
    protected void ddl_Origin_SelectedIndexChanged(object sender, EventArgs e)
    {
        _RunReport();
    }
    WebReferenceReports.AarCriteria GetCrireria(string name)
    {
        foreach (WebReferenceReports.AarCriteria crit in Enum.GetValues(typeof(WebReferenceReports.AarCriteria)))
        {
            if (crit.ToString() == name)
                return crit;
        }
        return WebReferenceReports.AarCriteria.AvgAttemptsForAcPress1;
    }
    #endregion
    protected void lb_AddOn_Click(object sender, EventArgs e)
    {
  //      SetOrigins();
        _RunReport();
    }
    protected void lb_Injedction_Click(object sender, EventArgs e)
    {
 //       SetOrigins();
        _RunReport();
    }
    protected string GetDivCurrentId
    {
        get { return FromToDatePicker_current.GetParentDivId(); }
    }
    protected string GetDivPastId
    {
        get { return FromToDatePicker_compare.GetParentDivId(); }
    }
    protected string GetDateFormat()
    {
        return ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat);
    }
    protected string GetFromCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxFrom().ClientID; }
    }
    protected string GetToCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxTo().ClientID; }
    }

    protected string GetFromPastId
    {
        get { return FromToDatePicker_compare.GetTextBoxFrom().ClientID; }
    }
    protected string GetToPastId
    {
        get { return FromToDatePicker_compare.GetTextBoxTo().ClientID; }
    }
    protected string GetInvalidDate
    {
        get { return Server.HtmlEncode(lbl_DateError.Text); }
    }
    private DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    private string PathV
    {
        get { return (ViewState["Path"] == null) ? string.Empty : (string)ViewState["Path"]; }
        set { ViewState["Path"] = value; }
    }
    
    private WebReferenceReports.FinancialDashboardRequest requestV
    {
        get { return (ViewState["_request"] == null) ? null : (WebReferenceReports.FinancialDashboardRequest)ViewState["_request"]; }
        set { ViewState["_request"] = value; }
    }
     
    private Dictionary<string, string> dicAarCreteriaV
    {
        get { return (Session["AarCreteria"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)Session["AarCreteria"]; }
        set { Session["AarCreteria"] = value; }
    }
    protected string Lbl_DatesChange
    {
        get { return lbl_DatesChange.Text; }
    }
    bool _ShowPast;
    protected bool ShowPast { get { return _ShowPast; } set { _ShowPast = value; } }
    private Dictionary<Guid, string> OriginAddOnV
    {
        get { return (ViewState["OriginAddOn"] == null) ? null : (Dictionary<Guid, string>)ViewState["OriginAddOn"]; }
        set { ViewState["OriginAddOn"] = value; }
    }
    private Dictionary<Guid, string> OriginInjectionV
    {
        get { return (ViewState["OriginInjection"] == null) ? null : (Dictionary<Guid, string>)ViewState["OriginInjection"]; }
        set { ViewState["OriginInjection"] = value; }
    }
    private int LastOriginMain
    {
        get { return (ViewState["LastOriginMain"] == null) ? 0 : (int)ViewState["LastOriginMain"]; }
        set { ViewState["LastOriginMain"] = value; }
    }
    private int LastOriginCompare
    {
        get { return (ViewState["LastOriginCompare"] == null) ? 0 : (int)ViewState["LastOriginCompare"]; }
        set { ViewState["LastOriginCompare"] = value; }
    }
    private bool WasInjection
    {
        get { return (ViewState["WasInjection"] == null) ? false : (bool)ViewState["WasInjection"]; }
        set { ViewState["WasInjection"] = value; }
    }
    

    
}