using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Publisher_PublisherWelcome : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Master.newAdvEvent += new EventHandler(Master_newAdvEvent);
        this.Header.DataBind();
      
    }
    void Master_newAdvEvent(object sender, EventArgs e)
    {
        //       ScriptManager1.a
        string csnameStep = "setStep";
        string csTextStep = "$(document).ready(function(){$.colorbox({href:'framepublisher.aspx',width:'95%', height:'90%', iframe:true});});";
        //          csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), csnameStep, csTextStep, true);
        //     ScriptManager.RegisterStartupScript(this, this.GetType(), "testi", "testi();", true);   

    }
}
