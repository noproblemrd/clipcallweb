using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Publisher_EditExplanation : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(lbPages);
        ScriptManager1.RegisterAsyncPostBackControl(ddlSite_lang);
        if (!IsPostBack)
        {
            if (!userManagement.IsPublisher())
                Response.Redirect("PublisherLogin.aspx");

            GetSites_lang();
            GetLangDefault();
            GetPages();
            SetDataList(0);
        }
    }
    protected void GetSites_lang()
    {

        string command = "EXEC dbo.GetLanguagesBySiteNameId @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ddlSite_lang.Items.Add(new ListItem((string)reader["LanguageName"], "" + (int)reader["SiteLangId"]));
            }
            conn.Close();
        }
    }
    private void GetLangDefault()
    {
        int langId = -1;
        string command = "SELECT dbo.GetSiteLangIdDefaultBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            langId = (int)cmd.ExecuteScalar();
            conn.Close();
        }
        foreach (ListItem li in ddlSite_lang.Items)
        {
            if (langId.ToString() == li.Value)
                li.Selected = true;
            else
                li.Selected = false;
        }
    }
    private void SetDataList(int index)
    {
        int PageId = int.Parse(lbPages.Items[index].Value);
        int siteLangId = int.Parse(ddlSite_lang.SelectedValue);
        string command = "EXEC dbo.GetSentencesByPageIdSiteLangId @PageId, " +
             "@SiteLangId, @LiteralType";
        DataTable data = new DataTable();
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PageId", PageId);
            cmd.Parameters.AddWithValue("@SiteLangId", siteLangId);
            cmd.Parameters.AddWithValue("@LiteralType", LiteralType.Explanation.ToString());
            
            data.Columns.Add("Id", typeof(int));
            data.Columns.Add("Sentence");
            data.Columns.Add("Translate");
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow row = data.NewRow();
                row["Id"] = (int)reader["Id"];
                row["Sentence"] = (string)reader["Sentence"];
                row["Translate"] = (reader.IsDBNull(2)) ? string.Empty : (string)reader["Translate"];
                data.Rows.Add(row);
            }

            conn.Close();
        }
        DataListSentences.DataSource = data;
        DataListSentences.DataBind();
        UpdatePanelSentences.Update();
    }

    private void GetPages()
    {
        string command = "EXEC dbo.GetPagesOfLiteralType @LiteralType";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@LiteralType", LiteralType.Explanation.ToString());
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string pagedesc = (reader.IsDBNull(2)) ? (string)reader["PageName"] : (string)reader["description"];
                lbPages.Items.Add(new ListItem(pagedesc, "" + (int)reader["Id"]));
            }

            conn.Close();
        }
        lbPages.SelectedIndex = 0;
    }
    
    protected void lbPages_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDataList(lbPages.SelectedIndex);
    }
    protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
    {
        lbPages.SelectedIndex = 0;
        SetDataList(lbPages.SelectedIndex);
        UpdatePanelListPages.Update();
    }
    protected void btn_set_Click(object sender, EventArgs e)
    {
        int SiteLangId = int.Parse(ddlSite_lang.SelectedValue);
        string command = "EXEC dbo.Save_Translate @LiteralId, " +
                    "@SiteLangId, @Translate";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                for (int i = 0; i < DataListSentences.Items.Count; i++)
                {
                    SqlCommand cmd = new SqlCommand(command, conn);
                    cmd.Parameters.AddWithValue("@LiteralId", (int)DataListSentences.DataKeys[i]);
                    cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
                    TextBox tb = (TextBox)(DataListSentences.Items[i].FindControl("tb_translate"));
                    string sentence = tb.Text.Trim();
                    if (sentence == string.Empty)
                        cmd.Parameters.AddWithValue("@Translate", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@Translate", sentence);
                    int a = cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
    }
    /*
    List<string> SiteListV
    {
        get { return (Session["SiteList"] == null) ? new List<string>() : (List<string>)Session["SiteList"]; }
    }   
     * */
}
