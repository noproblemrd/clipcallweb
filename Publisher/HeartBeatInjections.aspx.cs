﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlTypes;

public partial class Publisher_HeartBeatInjections : PageSetting
{
    const int DAYS_INTERVAL = 5;
    string _ChartDataString = "";
    string _ChartColumnsString = "";
    string _Y_Chart = "";
    protected void Page_Load(object sender, EventArgs e)
    {
 //       ScriptManager1.RegisterAsyncPostBackControl(btn_UpdateChart);
        if (!IsPostBack)
        {
            SetToolbox();
            SetDatePicker();
            LoadInjections();
            LoadCampaigns();
            LoadChartType();
            ExecChart();
        }
        else
            SetInjectionsPostBack();
 //       SetToolboxEvents();
        Header.DataBind();
    }

    
    protected void btn_UpdateChart_Click(object sender, EventArgs e)
    {
        ExecChart();
    }
    void ExecChart()
    {
        WebReferenceReports.HeartBeatInjectionRequest _request = new WebReferenceReports.HeartBeatInjectionRequest();
        _request._from = FromToDatePicker_current.GetDateFrom;
        _request._to = FromToDatePicker_current.GetDateTo;
        _request.CampaignId = Guid.Parse(rbl_Origins.SelectedValue);
        Dictionary<Guid, string> InjectionList = InjectionListS;
        Dictionary<SqlGuid, string> dic = new Dictionary<SqlGuid, string>();
        
        foreach (ListItem li in cbl_Injection.Items)
        {
            if (li.Selected)
            {
                 Guid InjectionId = Guid.Parse(li.Value);
                if (InjectionId == Guid.Empty)
                {
                    dic.Clear();
                    break;
                }
                dic.Add(InjectionId, InjectionList[(Guid)InjectionId]);
            }
        }

        _request.HeartBeatChartType = (WebReferenceReports.eHeartBeatChartType)Enum.Parse(typeof(WebReferenceReports.eHeartBeatChartType), rbl_ChartType.SelectedValue);
        /*
        dic.Add(new SqlGuid("B7917883-DC17-E411-87EC-001517D1792A"), "revizer_popup");
        dic.Add(new SqlGuid("EADCD63F-5581-46D3-BD82-6A2760BA88A4"), "AdEnginePopup");
        dic.Add(new SqlGuid("788E2CAE-EF5D-E411-B45D-001517D1792A"), "admedia_intext");
        dic.Add(new SqlGuid("130bc6b6-f837-e411-b45d-001517d1792a"), "jollywallet_shopping");
    //    'B7917883-DC17-E411-87EC-001517D1792A;EADCD63F-5581-46D3-BD82-6A2760BA88A4;788E2CAE-EF5D-E411-B45D-001517D1792A;'
         * */
        var _items =(from pair in dic
                    orderby pair.Key ascending
                    select pair).ToList();


        _request.InjectionId = (from x in dic
                                select (Guid)x.Key).ToArray();

        WebReferenceReports.Reports report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfHeartBeatInjectionResponse result = null;
        try
        {
            result = report.HeartBeatInjectionReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if (result.Value.Length == 0)
            return;
        if (_request.HeartBeatChartType == WebReferenceReports.eHeartBeatChartType.Injection)
            SetChartInjections(result, dic);
        else if (_request.HeartBeatChartType == WebReferenceReports.eHeartBeatChartType.Hits_KDAUs)
            SetChartHitsDAUs(result, dic);
        else //WebReferenceReports.eHeartBeatChartType.Logic
            SetChartLogicHeartBeat(result);

        Header.DataBind();        

    }
    private void SetChartLogicHeartBeat(WebReferenceReports.ResultOfListOfHeartBeatInjectionResponse result)
    {
        
        StringBuilder sb = new StringBuilder();
        DateTime dt_current = result.Value[0].date;
        _ChartColumnsString = "data.addColumn('number', 'Logic');";
        
        foreach (WebReferenceReports.HeartBeatInjectionResponse row in result.Value)
        {
            while (dt_current != row.date)
            {
                sb.Append(AddValuesToStringBuilder(dt_current, 0));
                dt_current = dt_current.AddMinutes(10);
            }
            sb.Append(AddValuesToStringBuilder(dt_current, row.Hits));
            
            dt_current = dt_current.AddMinutes(10);
        }
        if (sb.Length > 2)
            sb.Remove(sb.Length - 1, 1);


        _Y_Chart = "Logic hits";
        _ChartDataString = sb.ToString();
    }
    private void SetChartHitsDAUs(WebReferenceReports.ResultOfListOfHeartBeatInjectionResponse result, Dictionary<SqlGuid, string> dic)
    {
        var _items = (from pair in dic
                      orderby pair.Key ascending
                      select pair).ToList();

        StringBuilder sb = new StringBuilder();
        DateTime dt_current = result.Value[0].date;
        if (dic.Count == 0)
        {
            _ChartColumnsString = "data.addColumn('number', 'Hits/KDAUs');";
            foreach (WebReferenceReports.HeartBeatInjectionResponse row in result.Value)
            {
                while (dt_current != row.date)
                {
                    sb.Append(AddValuesToStringBuilder(dt_current, 0));
                    dt_current = dt_current.AddMinutes(10);
                }
                sb.Append(AddValuesToStringBuilder(dt_current, row.Hits_KDaus));
                /*
                sb.Append("[");
                sb.Append("new Date(" + dt_current.Year + "," + (dt_current.Month - 1) + "," + dt_current.Day + "," + dt_current.Hour + "," + dt_current.Minute + "), "
                    + row.Hits + "],");
                 * */
                dt_current = dt_current.AddMinutes(10);
            }
            if (sb.Length > 2)
                sb.Remove(sb.Length - 1, 1);
        }
        else
        {
            for (int i = 0; i < _items.Count; i++)
                _ChartColumnsString += "data.addColumn('number', '" + _items[i].Value + "');";
            DateTime dt = DateTime.MinValue;
            int indx = _items.Count;
            foreach (WebReferenceReports.HeartBeatInjectionResponse row in result.Value)
            {
                if (row.date != dt)
                {

                    for (; indx < _items.Count; indx++)
                        sb.Append("0,");
                    if (sb.Length > 1)
                    {
                        sb.Remove(sb.Length - 1, 1);
                        sb.Append("],");
                    }
                    if (row.date > dt_current)
                        dt_current = dt_current.AddMinutes(10);
                }
                while (row.date > dt_current)
                {
                    sb.Append("[");
                    sb.Append("new Date(" + dt_current.Year + "," + (dt_current.Month - 1) + "," + dt_current.Day + "," + dt_current.Hour + "," + dt_current.Minute + "), ");
                    for (int i = 0; i < _items.Count; i++)
                        sb.Append("0,");
                    sb.Remove(sb.Length - 1, 1);
                    sb.Append("],");
                    dt_current = dt_current.AddMinutes(10);
                }

                if (row.date != dt)
                {
                    dt = row.date;

                    indx = 0;
                    sb.Append("[");
                    sb.Append("new Date(" + row.date.Year + "," + (row.date.Month - 1) + "," + row.date.Day + "," + row.date.Hour + "," + row.date.Minute + "), ");
                }


                while (_items.Count > indx && _items[indx].Key != row.InjectionId)
                {
                    sb.Append("0,");
                    indx++;
                }
                if (_items.Count > indx && _items[indx].Key == row.InjectionId)
                    sb.Append(row.Hits_KDaus + ",");
                indx++;
            }
            for (; indx < _items.Count; indx++)
                sb.Append("0,");
            if (sb.Length > 1)
            {
                sb.Remove(sb.Length - 1, 1);
                sb.Append("]");
            }
        }

        _Y_Chart = "Hits/KDAUs";
        _ChartDataString = sb.ToString();
    }
    private void SetChartInjections(WebReferenceReports.ResultOfListOfHeartBeatInjectionResponse result, Dictionary<SqlGuid, string> dic)
    {
        var _items = (from pair in dic
                      orderby pair.Key ascending
                      select pair).ToList();

        StringBuilder sb = new StringBuilder();
        DateTime dt_current = result.Value[0].date;
        if (dic.Count == 0)
        {
            _ChartColumnsString = "data.addColumn('number', 'Injections');";
            foreach (WebReferenceReports.HeartBeatInjectionResponse row in result.Value)
            {
                while (dt_current != row.date)
                {
                    sb.Append(AddValuesToStringBuilder(dt_current, 0));
                    dt_current = dt_current.AddMinutes(10);
                }
                sb.Append(AddValuesToStringBuilder(dt_current, row.Hits));
                /*
                sb.Append("[");
                sb.Append("new Date(" + dt_current.Year + "," + (dt_current.Month - 1) + "," + dt_current.Day + "," + dt_current.Hour + "," + dt_current.Minute + "), "
                    + row.Hits + "],");
                 * */
                dt_current = dt_current.AddMinutes(10);
            }
            if (sb.Length > 2)
                sb.Remove(sb.Length - 1, 1);
        }
        else
        {
            for (int i = 0; i < _items.Count; i++)
                _ChartColumnsString += "data.addColumn('number', '" + _items[i].Value + "');";
            DateTime dt = DateTime.MinValue;
            int indx = _items.Count;
            foreach (WebReferenceReports.HeartBeatInjectionResponse row in result.Value)
            {
                if (row.date != dt)
                {

                    for (; indx < _items.Count; indx++)
                        sb.Append("0,");
                    if (sb.Length > 1)
                    {
                        sb.Remove(sb.Length - 1, 1);
                        sb.Append("],");
                    }
                    if (row.date > dt_current)
                        dt_current = dt_current.AddMinutes(10);
                }
                while (row.date > dt_current)
                {
                    sb.Append("[");
                    sb.Append("new Date(" + dt_current.Year + "," + (dt_current.Month - 1) + "," + dt_current.Day + "," + dt_current.Hour + "," + dt_current.Minute + "), ");
                    for (int i = 0; i < _items.Count; i++)
                        sb.Append("0,");
                    sb.Remove(sb.Length - 1, 1);
                    sb.Append("],");
                    dt_current = dt_current.AddMinutes(10);
                }

                if (row.date != dt)
                {
                    dt = row.date;

                    indx = 0;
                    sb.Append("[");
                    sb.Append("new Date(" + row.date.Year + "," + (row.date.Month - 1) + "," + row.date.Day + "," + row.date.Hour + "," + row.date.Minute + "), ");
                }


                while (_items.Count > indx && _items[indx].Key != row.InjectionId)
                {
                    sb.Append("0,");
                    indx++;
                }
                if (_items.Count > indx && _items[indx].Key == row.InjectionId)
                    sb.Append(row.Hits + ",");
                indx++;
            }
            for (; indx < _items.Count; indx++)
                sb.Append("0,");
            if (sb.Length > 1)
            {
                sb.Remove(sb.Length - 1, 1);
                sb.Append("]");
            }
        }

        _Y_Chart = "Hits";
        _ChartDataString = sb.ToString();
    }
    private string AddValuesToStringBuilder(DateTime dt, int hits)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("[");
        sb.Append("new Date(" + dt.Year + "," + (dt.Month - 1) + "," + dt.Day + "," + dt.Hour + "," + dt.Minute + "), "
            + hits + "],");
        return sb.ToString();
    }
    private string AddValuesToStringBuilder(DateTime dt, double num)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("[");
        sb.Append("new Date(" + dt.Year + "," + (dt.Month - 1) + "," + dt.Day + "," + dt.Hour + "," + dt.Minute + "), "
            + num + "],");
        return sb.ToString();
    }
    private void LoadCampaigns()
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfOriginWithDAUs result;
        try
        {
            result = _report.GetDistributionOriginsWithDAUs();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        rbl_Origins.Items.Clear();

        ListItem li1 = new ListItem("ALL", Guid.Empty.ToString());
   //     li1.Attributes.Add("guid", Guid.Empty.ToString());
        rbl_Origins.Items.Add(li1);

        rbl_Origins.SelectedIndex = 0;
        foreach (var gsp in result.Value)
        {
            ListItem li = new ListItem(gsp.OriginName + " (" + gsp.DAUs + ")", gsp.OriginId.ToString());
            //  li.Selected = true;
       //     li.Attributes.Add("guid", gsp.OriginId.ToString());
            rbl_Origins.Items.Add(li);
        }
    }
    private void LoadInjections()
    {
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionWithDAUs resultInjectionData = null;
        //Result<List<DataModel.Origins.InjectionsCockpit.InjectionWithDAUs>> GetInjectionsAndDAUs()
        try
        {
            resultInjectionData = _site.GetInjectionsAndDAUs();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (resultInjectionData.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        Dictionary<Guid, string> dic = new Dictionary<Guid, string>();
        dic.Add(Guid.Empty, string.Empty);
        cbl_Injection.Items.Clear();

        ListItem li1 = new ListItem("ALL", Guid.Empty.ToString());
        li1.Attributes.Add("guid", Guid.Empty.ToString());
        cbl_Injection.Items.Add(li1);

        cbl_Injection.SelectedIndex = 0;
        foreach (WebReferenceSite.InjectionWithDAUs data in resultInjectionData.Value)
        {
            dic.Add(data.InjecitonId, data.InjectionName);
            ListItem li = new ListItem(data.InjectionName + " (" + data.DAUs + ")", data.InjecitonId.ToString());
            li.Attributes.Add("guid", data.InjecitonId.ToString());
            //      li.Attributes.Add("JSvalue", data.InjectionId.ToString());
            cbl_Injection.Items.Add(li);
        }
        InjectionListS = dic;
    }
    private void SetInjectionsPostBack()
    {
        foreach (ListItem li in cbl_Injection.Items)
            li.Attributes.Add("guid", li.Value);
       
    }
    private void LoadChartType()
    {
        rbl_ChartType.Items.Clear();
        foreach (WebReferenceReports.eHeartBeatChartType hbct in Enum.GetValues(typeof(WebReferenceReports.eHeartBeatChartType)))
        {
       //     if (hbct == WebReferenceReports.eHeartBeatChartType.Logic)
       //         continue;
            ListItem li = new ListItem(Get_eHeartBeatChartType_string(hbct), hbct.ToString());
            li.Selected = hbct == WebReferenceReports.eHeartBeatChartType.Injection;
            rbl_ChartType.Items.Add(li);
        }
    }
    private string Get_eHeartBeatChartType_string(WebReferenceReports.eHeartBeatChartType hbct)
    {
        return hbct.ToString().Replace('_', '/');
    }
    
    void SetDatePicker()
    {
        FromToDatePicker_current.SetIntervalDateServer(DAYS_INTERVAL);
    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lbl_TitleReport.Text);
        _Toolbox.RemovePrintButton();
        _Toolbox.RemoveExcelButton();
    }
    protected string ChartDataString()
    {
        return _ChartDataString;
    }
    protected string GetColumns()
    {
        return _ChartColumnsString;
    }
    protected string Y_Chart
    {
        get { return _Y_Chart; }
    }
    Dictionary<Guid, string> InjectionListS
    {
        get { return (Session["InjectionList"] == null) ? new Dictionary<Guid, string>() : (Dictionary<Guid, string>)Session["InjectionList"]; }
        set { Session["InjectionList"] = value; }
    }
    protected string GetInjectionsByOrigin
    {
        get { return ResolveUrl("~/Publisher/InjectionReportService.asmx/GetInjectionsByOrigin"); }
    }
   
}