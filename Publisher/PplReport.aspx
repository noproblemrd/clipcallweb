﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="PplReport.aspx.cs" Inherits="Publisher_PplReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register src="~/Publisher/SalesControls/PplControl.ascx" tagname="PplControl" tagprefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">

    window.onload = appl_init;

    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
    //    pgRegMgr.add_beginRequest(BeginRequestHandler);
        pgRegMgr.add_endRequest(EndHandler);

    }
    /*
    function BeginRequestHandler(sender, args) {
        var elem = args.get_postBackElement();
      //  ActivateAlertDiv('visible', 'AlertDiv', elem.value + ' processing...');
    }
    */
    function BeginHandler() {
        showDiv();
    }
    function EndHandler(sender, args) {
        //     SetTRClass();
   //     if (sender._postBackSettings.sourceElement.id != '<%# ddl_States.ClientID %>')
          hideDiv();
      //  else
        $('#span_cover_Cities').hide();
        

    }
    function ClickButton() {
        if (Page_ClientValidate('ZipCode'))
            BeginHandler();
    }
    $(function () {
        $('#<%# ddl_States.ClientID %>').change(function () {
            document.getElementById('<%# ddl_Cities.ClientID %>').selectedIndex = 0;
            $('#span_cover_Cities').show();
        });
    });
    
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
        <uc1:ToolboxReport ID="_Toolbox" runat="server" />
<div class="page-content minisite-content2">
     
    <asp:Label ID="lbl_ProfitableReport" runat="server" Text="Profitability Report" Visible="false"></asp:Label>
 <div id="form-analytics" > 
   
     <div>
        <div class="form-field">
            <asp:Label ID="lbl_Heading" runat="server" Text="Heading"></asp:Label>
            <asp:DropDownList ID="ddl_Heading" runat="server" CssClass="form-select" >
            </asp:DropDownList>
        </div>
         <div class="form-field">
            <asp:Label ID="lbl_ZipCode" runat="server" Text="Zip code"></asp:Label>
             <asp:TextBox ID="txt_ZipCode" runat="server" CssClass="form-text"></asp:TextBox>
             <asp:RegularExpressionValidator ID="RegularExpressionValidator_ZipCode" runat="server" ErrorMessage="Invalid zipcode"
              ControlToValidate="txt_ZipCode" Display="Static" ValidationGroup="ZipCode"></asp:RegularExpressionValidator>
        </div>
        <div class="form-field">
            <asp:Label ID="lbl_States" runat="server" Text="States"></asp:Label>
            <asp:DropDownList ID="ddl_States" runat="server" CssClass="form-select" 
                onselectedindexchanged="ddl_States_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList>
        </div>
         <div class="form-field" style="position: relative;">
            <asp:Label ID="lbl_Cities" runat="server" Text="Cities"></asp:Label>
            
             <asp:UpdatePanel ID="_UpdatePanel_cities" runat="server" UpdateMode="Conditional">
             <ContentTemplate>
                  <asp:DropDownList ID="ddl_Cities" runat="server" CssClass="form-select">
                </asp:DropDownList>
                
             </ContentTemplate>
             </asp:UpdatePanel>
           <span id="span_cover_Cities" class="span_cover_ddl" style="display:none;"></span>
        </div>
    </div>
    <div class="clear"></div> 
              
    <div class="dashsubmit">
        <asp:Button ID="btn_run" runat="server" Text="Create report"  
            CssClass="CreateReportSubmit2" onclick="btn_run_Click" OnClientClick="ClickButton();" ValidationGroup="ZipCode"/>
    
    </div>
    
 </div>   
    <div class="clear"><!-- --></div>
  
    <div>
         <asp:UpdatePanel ID="UpdatePanel_table" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
            <div class="results" runat="server" visible="false">
                <span class="_Line">
                    <asp:Label ID="lbl_LastUpdate" runat="server" Text="Last update:"></asp:Label>
                    <asp:Label ID="lblLastUpdate" runat="server" Text="Label"></asp:Label>
                </span>
                <span runat="server" id="span_TotalDaus" class="_Line" visible="false">
                    <asp:Label ID="Lbl_TotalDaus" runat="server" Text="Total DAUS:"></asp:Label>
                    <asp:Label ID="LblTotalDaus" runat="server"></asp:Label>
                </span>
                <span runat="server" id="span_MonthlyUser" class="_Line" visible="false">
                    <asp:Label ID="lbl_MonthlyUser" runat="server" Text="Monthly User value today:"></asp:Label>
                    <asp:Label ID="lblMonthlyUser" runat="server" ></asp:Label>
                </span>
            </div>       
           <uc1:PplControl ID="_PplControl" runat="server" />
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
   
</div>
 <asp:Label ID="lbl_PplReport" runat="server" Text="PPL Report" Visible="false"></asp:Label>


</asp:Content>

