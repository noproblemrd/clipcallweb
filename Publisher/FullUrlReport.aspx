﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="FullUrlReport.aspx.cs" Inherits="Publisher_FullUrlReport" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script  type="text/javascript" src="../Calender/Calender.js"></script>

<script type="text/javascript" >
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);

    }
    function BeginHandler() {

        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
</cc1:ToolkitScriptManager> 
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	       
              
                    <div class="callreportaf"><uc1:FromToDate ID="FromToDate1" runat="server" />
                     <div class="form-field UrlReport">
                <asp:Label ID="lbl_Affiliate" runat="server" CssClass="label" Text="Affiliate"></asp:Label>          
                <div class="div_exposureLeft">
	                <asp:DropDownList ID="ddl_Affiliate" CssClass="form-select" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
                    </div>        
    	</div>
    	<div class="clear"></div>
    	
	    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	    <ContentTemplate>	
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
        <div id="Table_Report" class="table" runat="server" >
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                CssClass="data-table">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <Columns>                     
                    <asp:TemplateField>              
                    <HeaderTemplate>
                        <asp:Label ID="Label01" runat="server" Text="<%# lbl_Url.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# Bind('Url') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label02" runat="server" Text="<%# lbl_CreatedOn.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    

                </Columns>
                </asp:GridView>
                <uc1:TablePaging ID="TablePaging1" runat="server" />
            </div>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:Label ID="lbl_Url" runat="server" Text="Full URL" Visible="false"></asp:Label>
     <asp:Label ID="lbl_CreatedOn" runat="server" Text="Created on" Visible="false"></asp:Label>
    
   
   <asp:Label ID="lbl_NoResult" runat="server" Visible="false" Text="There are not results"></asp:Label>    

   <asp:Label ID="lbl_ReportName" runat="server" Visible="false" Text="Full URL Report"></asp:Label>    

</asp:Content>

