﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ticket.aspx.cs" Inherits="Publisher_Ticket" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="~/Controls/Notes.ascx" tagname="Notes" tagprefix="uc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript" src="../general.js"></script>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
    <script src="../jquery.min.js" type="text/javascript"></script> 
    <script src="../jquery-ui.min.js" type="text/javascript"></script> 
    <script type="text/javascript" src="../CallService.js"></script>
    
    <script type="text/javascript">
  //  var HasUpload = false;
    $(function() {
	    $('._datepicker').datepicker({
  //         onClose: function(dateText, inst) { OnBlurString(this); },
  //         dateFormat: '<%# (siteSetting.DateFormatClean).ToLower() %>'
        });
    });
    
    function OpenDetails()
    {
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Notes.ClientID %>").style.display = "none";
        document.getElementById("<%# div_Details.ClientID %>").style.display = "block";
        if(is_explorer)
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("className");
        else
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("class");
        document.getElementById("<%# a_OpenDetails.ClientID %>").className = "selected";
 //       document.getElementById("<%# btn_save.ClientID %>").style.visibility = "visible";
    }
    function OpenNotes()
    {
        var div_Notes = document.getElementById("<%# div_Notes.ClientID %>");
        if(div_Notes.style.display == "block")
            return false;
   //     document.getElementById("<%# btn_save.ClientID %>").style.visibility = "hidden";
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Details.ClientID %>").style.display = "none";
        div_Notes.style.display = "block";
        if(is_explorer)
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("className");
        else
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("class");        
        document.getElementById("<%# lb_OpenNotes.ClientID %>").className = "selected";
        return true;
    }
    
    function On_Call_Blur(sender, e)
    {
    
        var path = "CallService.asmx/GetOneItem";
        var comboCall = $find("<%# RadComboBox_Call.ClientID %>");
       
         if(comboCall.get_value().length > 0)
            return;
        var _value = comboCall.get_text();
        var txt_Advertiser = document.getElementById("<%# txt_Advertiser.ClientID %>");
        txt_Advertiser.value="";
        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
       
        if(_value.length > 0)
        {
            
            
            while(_value.indexOf("&") > -1)
                _value = _value.replace("&", "%26");    
            var param = "_context=" + _value + "&Consumer=" + document.getElementById("<%# hf_ConsumerId.ClientID %>").value;
            CallWebService(param, path, function(arg)
                {
                    
                    
                    if(arg.length == 0)
                    {
                        comboCall.clearSelection();                        
                        return;
                    }
                    var args = arg.split(";;;;");
                    comboItem.set_text(args[0].split(",")[1]);
                    txt_Advertiser.value = args[0].split(",")[0];
                    comboItem.set_value(args[1]);
                    comboCall.trackChanges();
                    var item_remove = comboCall.findItemByValue(args[1]);
              
                    comboCall.clearItems();
                  comboCall.get_items().add(comboItem);
                  
                    comboItem.select();
                    comboCall.commitChanges();
                    comboItem.scrollIntoView();
                  //  alert(comboCall.get_value());
                    
                },
                function(){alert(top.UpdateFailed());}); 
        }
    }
    function On_Client_Blur(sender, e)
    {
        showDiv();
        var combo = $find("<%# RadComboBox_Consumer.ClientID %>");
       
        var path = "ConsumerService.asmx/GetOneItem";
        var _value = combo.get_text();
   
        var combo_call =  $find("<%# RadComboBox_Call.ClientID %>");
        combo_call.clearItems();
        var _hf = document.getElementById("<%# hf_ConsumerId.ClientID %>");
        _hf.value = "";
        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
       
        if(_value.length > 0)
        {
            
            
            while(_value.indexOf("&") > -1)
                _value = _value.replace("&", "%26");    
            var param = "_context=" + _value;
            CallWebService(param, path, function(arg)
                {
                    
                    if(arg.length == 0)
                    {
                        combo.clearSelection();
                        on_consumer_clear();
                        hideDiv();                        
                        return;
                    }
                    var args = arg.split(";;;;");
                    comboItem.set_text(args[0]);
                      comboItem.set_value(args[1]);
                      combo.trackChanges();
                      _hf.value = args[1];
                     combo.clearItems();
                      combo.get_items().add(comboItem);
                      comboItem.select();
                      combo.commitChanges();
                      comboItem.scrollIntoView();
                      hideDiv();
                },
                function(){alert(top.UpdateFailed());}); 
        }
        else
        {
            on_consumer_clear();
            hideDiv();
        }
              
   
    }
    
    function checkMeasageLength(elem, _max, elemCountID)
    {
        var message = document.getElementById("<%# HiddenMaxLength1.ClientID %>").value + " " +
                    _max + " " + document.getElementById("<%# HiddenMaxLength2.ClientID %>").value;
        
                    
        CheckMaxLengthTextbox(elem, _max, message, elemCountID);
        
    }
    
    
    function CheckValues()
    {
        ClearAllValidationAlert();
        var Is_ok = true;
       if(!Page_ClientValidate('_Ticket'))
            Is_ok = false;
        var combo_consumer = $find("<%# RadComboBox_Consumer.ClientID %>");
        if(combo_consumer.get_value().length == 0)
        {
           
            document.getElementById("<%# lbl_missing_consumer.ClientID %>").style.display="inline";
            Is_ok = false;
        }
        var combo_call =  $find("<%# RadComboBox_Call.ClientID %>");
        if(combo_call.get_value().length == 0)
        {
            document.getElementById("<%# lbl_missing_call.ClientID %>").style.display="inline";        
            Is_ok = false;
        }
        
        if(!Is_ok)
            alert(document.getElementById("<%# hf_MissingFields.ClientID %>").value);
        
        return Is_ok;
        
    }
    function ClearAllValidationAlert()
    {
        document.getElementById("<%# lbl_missing_consumer.ClientID %>").style.display="none";
        document.getElementById("<%# lbl_missing_call.ClientID %>").style.display="none";  
         
    }
    function on_call_request(sender, eventArgs)
    {
        var context = eventArgs.get_context();
        context["Consumer"] = document.getElementById("<%# hf_ConsumerId.ClientID %>").value;
    }
    function on_consumer_clear()
    {
        var combo_call =  $find("<%# RadComboBox_Call.ClientID %>");
        combo_call.clearItems();
        combo_call.clearSelection();
        document.getElementById("<%# txt_Advertiser.ClientID %>").value = "";
        document.getElementById("<%# hf_ConsumerId.ClientID %>").value = "";
    }
    function on_consumer_bound(item)
    {

        var combo = $find("<%# RadComboBox_Consumer.ClientID %>");
        var _value = combo.get_value();
        
        if(_value == document.getElementById("<%# hf_ConsumerId.ClientID %>").value)
            return;
        document.getElementById("<%# hf_ConsumerId.ClientID %>").value = _value;
        var combo_call =  $find("<%# RadComboBox_Call.ClientID %>");
        combo_call.clearItems();
        combo_call.clearSelection();
        document.getElementById("<%# txt_Advertiser.ClientID %>").value = "";
        
   
    }
    function on_call_bound(item)
    {
        var combo_call =  $find("<%# RadComboBox_Call.ClientID %>");
        var _txt = combo_call.get_text().split(',');
        if(_txt.length != 3)
            return;
        var _value = combo_call.get_value();
        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
        comboItem.set_text(_txt[1]);
        comboItem.set_value(_value);
        combo_call.trackChanges();
        combo_call.clearItems();
        combo_call.get_items().add(comboItem);
        comboItem.select();
          combo_call.commitChanges();
          comboItem.scrollIntoView();
        document.getElementById("<%# txt_Advertiser.ClientID %>").value = _txt[0];      
    }
    window.onload = appl_init;
    function appl_init() {
        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);       
        on_load_win();
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }   
    function on_load_win()
    {
        parent.ShowIframeDiv();
    }
    function RemoveInactive(sender) {
        for (var i = sender.options.length - 1; i > -1; i--) {
            if (sender.options[i].getAttribute("Inactive") != null) {
                sender.remove(i);
            }
        }
    }
    function on_close_iframe() {
        parent.CloseIframe_after();
    }
</script>

</head>
<body style="position:relative; left:0px; right:0px; top:0px; background-color:transparent">
<form id="form1" runat="server">

<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>

<div class="titlecomplaint">
    <asp:UpdatePanel ID="_UpdatePanel_TabsMenu" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    
        <asp:Label ID="lbl_Title_Title" runat="server"></asp:Label>:
        <asp:Label ID="lbl_Title_Name" runat="server"></asp:Label>
    </ContentTemplate>
    </asp:UpdatePanel>

</div>
<div class="clear"></div>
<div id="form-analyticscomp">

<div class="tabswrap2" style="display:block;">
<div id="Ticket_Tabs" class="tabs2" runat="server"><!-- here the red bar-->
    
    <ul id="ulTabs" runat="server">
        <li><a href="javascript:OpenDetails();" runat="server" id="a_OpenDetails" class="selected">Details</a></li>
        <li id="il_Notes" runat="server"><asp:LinkButton ID="lb_OpenNotes" runat="server" Text="Open Notes" 
            OnClientClick="return OpenNotes();" onclick="lb_OpenNotes_Click"></asp:LinkButton></li>
    </ul>
</div>

 <div class="containertab">
 <%// div details %>
    <div runat="server" id="div_Details">
         
     <div class="form-fieldlong">  
              
                <asp:Label ID="lbl_Title" runat="server" CssClass="label" Text="Title" ></asp:Label>
                <asp:TextBox ID="txt_Title" runat="server" CssClass="form-textlong" ></asp:TextBox>
                <div style="color: Red; display: inline; left: 9px;position: relative; top: -20px; float:right;">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_Title" runat="server" CssClass="error-msg"
                        ErrorMessage="*" ValidationGroup="_Ticket" ControlToValidate="txt_Title"
                         Display="Dynamic"></asp:RequiredFieldValidator>
                         </div> 
                 <div class="counter">
                <asp:Label ID="lbl_TitleCount1" runat="server" Text="200/"></asp:Label>
                <asp:Label ID="lbl_TitleCount2" runat="server" Text="0"></asp:Label>
                </div> 
            
                
      </div>
      <div class="form-fieldshort">
        
                <asp:Label ID="lbl_Consumer" runat="server" class="label" Text="Consumer "></asp:Label>
                <telerik:RadComboBox ID="RadComboBox_Consumer" runat="server" Width="150px" height="150px"
                    DropDownWidth="298px" EmptyMessage="<%# lbl_SearchConsumer.Text %>" HighlightTemplatedItems="true"
                    EnableLoadOnDemand="true" Filter="Contains" OnClientSelectedIndexChanged="on_consumer_bound" 
                    OnClientBlur="On_Client_Blur"  CssClass="ComboBoxInput_Classic">
                    <WebServiceSettings Path="ConsumerService.asmx" Method="GetItems" />
                            
                </telerik:RadComboBox>
                
                <asp:HiddenField ID="hf_ConsumerId" runat="server" />
                <div style="color: Red; display: inline; left: 9px;position: relative; top: -20px; float:right;">
                <asp:Label ID="lbl_missing_consumer" runat="server" Text="*" CssClass="error-msg"
                 style="display:none;"></asp:Label>
                 </div>
       </div>

     
         
        <div class="form-fieldshort">
                <asp:Label ID="lbl_Call" runat="server" class="label" Text="Call"></asp:Label>
                    <telerik:RadComboBox ID="RadComboBox_Call" runat="server" Height="150px" Width="150px"   
                                DropDownWidth="298px" EmptyMessage="<%# lbl_SearchSupplier.Text %>" HighlightTemplatedItems="true"
                                EnableLoadOnDemand="true" Filter="Contains" OnClientItemsRequesting="on_call_request"  
                                OnClientBlur="On_Call_Blur"  OnClientDropDownClosed="on_call_bound"   >
                            <WebServiceSettings Path="CallService.asmx" Method="GetItems" />
                    </telerik:RadComboBox>
                <div style="color: Red; display: inline; left: 9px;position: relative; top: -20px; float:right;">
                    <asp:Label ID="lbl_missing_call" runat="server" Text="*" CssClass="error-msg"
                     style="display:none;"></asp:Label>
                     </div>

                    
         </div>
         <div class="form-fieldshort">
                <asp:Label ID="lbl_Advertiser" runat="server" class="label" Text="Advertiser"></asp:Label>
                <asp:TextBox ID="txt_Advertiser" runat="server"  CssClass="form-textshort" ReadOnly="true"></asp:TextBox>
         </div> 
         <div class="form-fieldshort">
                    <asp:Label ID="lbl_Status" runat="server" class="label" Text="Status"></asp:Label>
                    <asp:DropDownList ID="ddl_Status"  CssClass="form-selectcomp" runat="server" onchange="RemoveInactive(this);">
                    </asp:DropDownList>
          </div>
   
    
          <div class="form-fieldshort">
                    <asp:Label ID="lbl_Owner" runat="server" class="label" Text="Owner"></asp:Label>
                    <asp:DropDownList ID="ddl_Owner"  CssClass="form-selectcomp" runat="server">
                    </asp:DropDownList>
                    
          </div>
          <div class="form-fieldcompfollow">
            <asp:Label ID="lbl_FollowUp" runat="server" class="label" Text="Follow up"></asp:Label>
            <asp:TextBox ID="txt_FollowUp" runat="server" CssClass="_datepicker form-textcompfollow" ></asp:TextBox>
          </div>
          <div class="form-fieldcomptime">
              <asp:DropDownList ID="ddl_Time" CssClass="form-selectcomptime" runat="server">
              </asp:DropDownList>              
          </div>
          
          <div class="form-fieldlongdet">
               
          <div class="counter2">
                        <asp:Label ID="lbl_DescriptionCount1" runat="server" Text="1000/"></asp:Label>
                        <asp:Label ID="lbl_DescriptionCount2" runat="server" Text="0"></asp:Label>
                    </div>
                    <asp:Label ID="lbl_Description" runat="server" class="label" Text="Description"></asp:Label>
                    <asp:TextBox ID="txt_Description" runat="server" CssClass="form-textdesc" TextMode="MultiLine" Rows="4"></asp:TextBox>
                     <div style="color: Red; display: inline; left: 9px;position: relative; top: -50px; float:right;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_Description" runat="server" CssClass="error-msg"
                    ErrorMessage="*" ValidationGroup="_Ticket" ControlToValidate="txt_Description"
                     Display="Dynamic"></asp:RequiredFieldValidator>
                     </div>
            </div>
       
        <div class="form-fieldshort">
                <asp:Label ID="lbl_CreatedBy" runat="server" class="label" Text="Created by"></asp:Label>
                <asp:DropDownList ID="ddl_CreateBy"  CssClass="form-selectcomp" runat="server">
                </asp:DropDownList>
        </div>
        <div class="form-fieldshort">
        <asp:Label ID="lbl_CreatedOn" runat="server" class="label" Text="Created on"></asp:Label>
        <asp:TextBox ID="txt_CreatedOn" runat="server"  CssClass="form-textshort" ReadOnly="true" ></asp:TextBox>
        </div> 
        <div class="form-fieldshort">
                <asp:Label ID="lbl_Severity" runat="server" class="label" Text="Severity"></asp:Label>
                <asp:DropDownList ID="ddl_Severity" CssClass="form-selectcomp" runat="server" onchange="RemoveInactive(this);">
                </asp:DropDownList>
           </div>   
        <div class="form-fieldshort">
            <div class="save">
            <asp:Button ID="btn_save" runat="server" CssClass="form-submit" Text="Save"  OnClick="btn_save_click" 
                OnClientClick="return CheckValues();" ValidationGroup="_Ticket" />
            </div> 
        </div>      
     
     
    </div>
    <%// end div details %>
    <%// div notes %>
        <div runat="server" id="div_Notes" style="display:none; position:relative;">
            <uc1:Notes ID="Notes1" runat="server" />
        </div>
    <%// end div notes %>  

  </div>
         
  

</div>
</div>
     



<div id="divLoader" class="divLoader">   
    <table>
        <tr>
            <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
        </tr>
    </table>
</div>
    
    <%//details sentences %>
    <asp:HiddenField ID="HiddenMaxLength1" runat="server" Value="Max length of message is"  />
    <asp:HiddenField ID="HiddenMaxLength2" runat="server" Value="Characters" />
    <asp:Label ID="lbl_SearchConsumer" runat="server" Text="Search by phone" Visible="false"></asp:Label>
    <asp:Label ID="lbl_SearchSupplier" runat="server" Text="Search by Case / Supplier" Visible="false"></asp:Label>
    <asp:HiddenField ID="hf_MissingFields" runat="server" Value="Few fields are missing. Please complete the missing data." />
    <asp:Label ID="lbl_new" runat="server" Text="New" Visible="false"></asp:Label>
        
    </form>
</body>
</html>
