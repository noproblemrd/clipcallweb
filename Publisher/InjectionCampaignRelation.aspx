﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InjectionCampaignRelation.aspx.cs" Inherits="Publisher_InjectionCampaignRelation" ClientIDMode="AutoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="Stylesheet" href="InjectionCampaign.css" type="text/css" />
    <title></title>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        function CloseWin() {
            window.opener.Reload();
            window.close();
        }
        $(function () {
            var $cb_all = $('.relation-cb-all').find('input[type="checkbox"]');
            $cb_all.change(function () {
                var options = $('#div_relationPartners').find('input[type="checkbox"]');
                for (var i = 0; i < options.length; i++) {
                    if (options[i].id == this.id)
                        continue;
                    options[i].checked = this.checked;
                }
            });
            $('#div_relationPartners').find('input[type="checkbox"]').each(function () {
                if ($cb_all.attr('id') == this.id)
                    return;
                $(this).change(function () {
                    var options = $('#div_relationPartners').find('input[type="checkbox"]');
                    var AllSign = true;
                    for (var i = 0; i < options.length; i++) {
                        if (options[i].id == $cb_all.attr('id'))
                            continue;
                        if (!options[i].checked) {
                            AllSign = false;
                            break;
                        }
                    }
                    $cb_all.get(0).checked = AllSign;
                });
            });
            $('#a_sameValues').click(function () {
                var options = $('#div_relationPartners').find('input[type="checkbox"]');
                var _HitCounter = null, _DayCounter = null;
                for (var i = 0; i < options.length; i++) {
                    if (options[i].id == '<%# CheckBoxAllId %>')
                        continue;
                    var tr_parent = $(options[i]).closest('tr');
                    if (_HitCounter == null) {
                        _HitCounter = tr_parent.find('input.txt_HitCounter').val();
                        if (isNaN(_HitCounter))
                            _HitCounter = 0;
                        _DayCounter = tr_parent.find('input.txt_DayCounter').val();
                        if (isNaN(_DayCounter))
                            _DayCounter = 0;
                        continue;
                    }
                    if (!options[i].checked)
                        continue;
                    tr_parent.find('input.txt_HitCounter').val(_HitCounter);
                    tr_parent.find('input.txt_DayCounter').val(_DayCounter);


                }
            });
        });
        function SaveClick() {
            $('#<%# lb_Save.ClientID %>').hide();
            $('#a_Save_loader').show();
        }
        
    
    </script>
</head>
<body class="Injection_form">
    <form id="form1" runat="server">
    <div>
        
        <div class="Popwin-title">
            <span>Relationship <span><%# MainIdentity(true) %></span> to <span><%# MainIdentity(false) %></span></span>
        </div>
        <div class="relationName">
            <span><span><%# MainIdentity(true) %></span> name</span>
            <asp:TextBox ID="txt_Name" runat="server" ReadOnly="true" CssClass="txt-relation"></asp:TextBox>
            <a class="BtnInjection" id="a_sameValues" href="javascript:void(0);" class="a_sameValues">Use the same values</a>
        </div>

        <div class="relationPartners" id="div_relationPartners">           
            <asp:Repeater ID="_Repeater" runat="server" 
                onitemdatabound="_Repeater_ItemDataBound">
            <HeaderTemplate>
                <table class="RelationTable">
                    <thead>
                        <tr>
                            <th>To Inject</th>
                            <th><asp:Label ID="Label31" runat="server" Text="Hit counter"></asp:Label></th>
			                <th><asp:Label ID="Label50" runat="server" Text="Day counter"></asp:Label></th>		                
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                 <asp:CheckBox ID="cb_all" runat="server" Text="All" CssClass="relation-cb-all" Checked="<%# IsAllChecked %>"/>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:CheckBox ID="_cb" runat="server" Checked="<%# Bind('IsPair') %>" Text="<%# Bind('Name') %>"/>
                        <asp:HiddenField ID="_hf" runat="server" Value="<%# Bind('ID') %>" />
                    </td>
                    <td>
                        <asp:TextBox ID="txt_HitCounter" runat="server" Text="<%# Bind('HitCounter') %>" CssClass="txt_HitCounter"></asp:TextBox>
                        <asp:RangeValidator ID="rv_HitCounter" runat="server" ErrorMessage="Invalid"
                             ControlToValidate="txt_HitCounter" ValidationGroup="pair" Type="Integer" MinimumValue="0" MaximumValue="<%# int.MaxValue %>"></asp:RangeValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txt_DayCounter" runat="server" Text="<%# Bind('DayCounter') %>" CssClass="txt_DayCounter"></asp:TextBox>
                        <asp:RangeValidator ID="rv_DayCounter" runat="server" ErrorMessage="Invalid"
                             ControlToValidate="txt_DayCounter" ValidationGroup="pair" Type="Integer" MinimumValue="0" MaximumValue="<%# int.MaxValue %>"></asp:RangeValidator>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
            </asp:Repeater>
           
        </div>
        <div class="form-field-Relation-Save">
            <asp:LinkButton ID="lb_Save" runat="server" CssClass="BtnInjection" ValidationGroup="pair"
                onclick="lb_Save_Click" OnClientClick="SaveClick();">Save</asp:LinkButton>
             <span id="a_Save_loader" style="display:none;" class="a__loader a_Save_loader"></span>
        </div>
    </div>
    </form>
</body>
</html>
