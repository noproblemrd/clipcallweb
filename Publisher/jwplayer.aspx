﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="jwplayer.aspx.cs" Inherits="Publisher_jwplayer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css" >
        #ClipcallVideo
        {
            margin: 0 auto;
        }
    </style>
    <title></title>
    <script src="../jwplayer8/jwplayer.js" type="text/javascript"></script>
     <script type="text/javascript">jwplayer.key = "y0rIShXb57vnSkTfeDGVEdJRxWxvMmdar4NZxr6O60U=";</script>
<!--    <script type="text/javascript">jwplayer.key = "YiTTzdj7vuYrnKLzmlEHBtKK4FOLklGGehNIf6rsjWg=";</script>-->
   
</head>
<body>
    <form id="form1" runat="server">
    <div >
         <div id="ClipcallVideo">Loading the player...</div>
        <script type="text/javascript">
            var playerInstance = jwplayer("ClipcallVideo");
            
            playerInstance.setup({
                file: "<%= videoUrl %>",
                image: "<%= imgUrl %>",
                autostart: true,
                width: 960,
                height: 540
            });
        </script>
    
    </div>
    </form>
</body>
</html>
