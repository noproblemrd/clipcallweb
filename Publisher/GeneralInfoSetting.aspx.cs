using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Publisher_GeneralInfoSetting : PageSetting, ICallbackEventHandler
{
    const string PAGE_NAME = "professionalDetails.aspx";
    protected Dictionary<string, string> RESPONSE;
    protected void Page_Load(object sender, EventArgs e)
    {
        SetupClient();
        if (!IsPostBack)
        {
           
            LoadPanelsToDisplay();
        }
    }
    
    private void LoadPanelsToDisplay()
    {
        Dictionary<string, string> HasAutoComplete = new Dictionary<string, string>();
        Dictionary<string, string> HasParent_labels = new Dictionary<string, string>();
        Dictionary<int, int> OrigLevelDic = new Dictionary<int, int>();
        DataTable data = new DataTable();
        data.Columns.Add("FieldDesc");
        data.Columns.Add("ControlId");
        data.Columns.Add("PanelShowId");
        data.Columns.Add("PanelShow_Site_Id");
        data.Columns.Add("Show", typeof(bool));
        data.Columns.Add("Valid", typeof(bool));
        data.Columns.Add("Autocomplete", typeof(bool));
        data.Columns.Add("HasAutocomplete");
        data.Columns.Add("HasParent", typeof(bool));
        data.Columns.Add("ParentName", typeof(string));
        data.Columns.Add("ParentId", typeof(int));
        data.Columns.Add("OriginalParentId", typeof(int));
        data.Columns.Add("OriginalParentName", typeof(string));
        data.Columns.Add("HasParentChecked", typeof(bool));


        string command = "EXEC dbo.GetPanelShowBySiteNameIdPageName @SiteNameId, @SiteLangId, @PageName";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.Add("@SiteLangId", siteSetting.siteLangId);
            cmd.Parameters.AddWithValue("@PageName", PAGE_NAME);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow row = data.NewRow();
                row["FieldDesc"] = (string)reader["Translate"];
                row["ControlId"] = (string)reader["ControlId"];
                row["PanelShow_Site_Id"] = (int)reader["PanelShow_Site_Id"];
                row["PanelShowId"] = (int)reader["PanelShowId"];
                row["Show"] = (bool)reader["Show"];
                row["Valid"] = (bool)reader["Valid"];
                row["Autocomplete"] = (bool)reader["AutoComplete"];
                bool _HasAutoComplete = (bool)reader["HasAutoComplete"];
                row["HasAutocomplete"] = _HasAutoComplete;
                bool HasParent = !reader.IsDBNull(11);
                row["HasParent"] = HasParent;
                bool HasParentChecked = false;
                if (HasParent)
                {



                    row["OriginalParentId"] = (int)reader["OriginalParentId"];
                    row["OriginalParentName"] = (string)reader["OriginalParentName"];
                    HasParentChecked = (bool)reader["HasParent"];
                    if (HasParentChecked)
                    {
                        if (!reader.IsDBNull(9))
                        {
                            row["ParentId"] = (int)reader["ParentPanelShowId"];
                            row["ParentName"] = (string)reader["ParentName"];
                        }
                    }
                }
                row["HasParentChecked"] = HasParentChecked;
                if (_HasAutoComplete)
                    OrigLevelDic.Add((int)reader["PanelShowId"], (reader.IsDBNull(11) ? 0 : (int)reader["PanelShowIdChild"]));
                data.Rows.Add(row);
            }
            conn.Close();
        }
        _GridView.DataSource = data;
        _GridView.DataBind();
        int tempLevel = 0;
        List<int> tempList = new List<int>();
        
        while (tempLevel > -1)
        {
           
                tempLevel = GetKeyByValue(OrigLevelDic, tempLevel);
                if (tempLevel != -1)
                    tempList.Add(tempLevel);
            
        }
      
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb_show = (CheckBox)row.FindControl("_CheckBoxDisplay");
            CheckBox cb_valid = (CheckBox)row.FindControl("_CheckBoxMust");
            CheckBox cb__CheckBoxAutocomplete = (CheckBox)row.FindControl("_CheckBoxAutocomplete");
            CheckBox cb_parent = (CheckBox)row.FindControl("_CheckBoxParent");
            string _script = "javascript:ShowCheck('" + cb_show.ClientID + "','" + cb_valid.ClientID + "','" +
                cb__CheckBoxAutocomplete.ClientID + "','" + cb_parent.ClientID + "' );";
            cb_show.Attributes.Add("onclick", _script);
            cb_valid.Attributes.Add("onclick", _script);
            cb__CheckBoxAutocomplete.Attributes.Add("onclick", _script);
            cb_parent.Attributes.Add("onclick", _script);
            CheckBox cb_autocomplete = (CheckBox)row.FindControl("_CheckBoxAutocomplete");
            string HasAuto = ((Label)row.FindControl("lbl_HasAutocomplete")).Text;
            cb_autocomplete.Visible = (HasAuto.ToLower() == "true");
            if (HasAuto.ToLower() == "true")
            {
                string ParentName = ((Label)row.FindControl("lbl_desc")).Text;
                HasAutoComplete.Add(cb__CheckBoxAutocomplete.ClientID, ParentName);
                string lbl_ParentName = ((Label)row.FindControl("lbl_ParentName")).ClientID;
                HasParent_labels.Add(cb_parent.ClientID, lbl_ParentName);
            }

            int PanelShowId = int.Parse(((Label)row.FindControl("lbl_PanelShowId")).Text);
            int PanelShowIdChild = tempList.IndexOf(PanelShowId);
            if (PanelShowIdChild != -1)
                ((Label)row.FindControl("lbl_OriginalLevel")).Text = (PanelShowIdChild + 1).ToString();
        }

        ClientScript.RegisterStartupScript(this.GetType(), "SetAutoCompleteArray",
            "SetAutoCompleteArray('" + GetStringOfDictionary(HasAutoComplete) + "','" +
            GetStringOfDictionary(HasParent_labels) + "');", true);
    }
    private int GetKeyByValue(Dictionary<int, int> dic, int _value)
    {
        foreach (KeyValuePair<int, int> kvp in dic)
        {
            if (kvp.Value == _value)            
                return kvp.Key;
                
        }
        return -1;
    }
    protected void btn_set_Click(object sender, EventArgs e)
    {

        string command = "EXEC dbo.SetPanelShow_Site @PanelShow_SiteId, @Show, "+
            "@Valid, @AutoComplete, @HasParent, @Level";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            foreach (GridViewRow row in _GridView.Rows)
            {
                string PanelShow_SiteId = ((Label)row.FindControl("lbl_PanelShow_Site_Id")).Text;
                bool ToShow = ((CheckBox)row.FindControl("_CheckBoxDisplay")).Checked;
                bool ToValid = false;
                if (ToShow)
                    ToValid = ((CheckBox)row.FindControl("_CheckBoxMust")).Checked;
                CheckBox _cb = (CheckBox)row.FindControl("_CheckBoxAutocomplete");
                CheckBox cb_hasParent = (CheckBox)row.FindControl("_CheckBoxParent");
                bool AutoCom = false;
                if (_cb.Visible)
                    AutoCom = _cb.Checked;
                string ParentName = ((Label)row.FindControl("lbl_ParentName")).Text;
                string _level = ((Label)row.FindControl("lbl_OriginalLevel")).Text;
                int _Level;
                bool hasParent = cb_hasParent.Visible && cb_hasParent.Checked;// && !string.IsNullOrEmpty(ParentName);

                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@PanelShow_SiteId", PanelShow_SiteId);
                cmd.Parameters.AddWithValue("@Show", ToShow);
                cmd.Parameters.AddWithValue("@Valid", ToValid);
                cmd.Parameters.AddWithValue("@AutoComplete", AutoCom);
                cmd.Parameters.AddWithValue("@HasParent", hasParent);
                if (int.TryParse(_level, out _Level))
                    cmd.Parameters.AddWithValue("@Level", _Level);
                else
                    cmd.Parameters.AddWithValue("@Level", DBNull.Value);
                int a = cmd.ExecuteNonQuery();
            }
            conn.Close();
        }
        LoadPanelsToDisplay();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
    }
    private string FindParent(string ParentId)
    {
        foreach (GridViewRow row in _GridView.Rows)
        {
            string PanelShowId = ((Label)row.FindControl("lbl_PanelShowId")).Text;
            if (PanelShowId != ParentId)
                continue;
            if (!((CheckBox)row.FindControl("_CheckBoxAutocomplete")).Checked)
            {
                ParentId = ((Label)row.FindControl("lbl_OriginalParentId")).Text;
                return FindParent(ParentId);
            }
            else
            {
                return ((Label)row.FindControl("lbl_desc")).Text;
            }
        }
        return "0";
    }
    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        StringBuilder result = new StringBuilder();
        foreach (KeyValuePair<string, string> kvp in RESPONSE)
            result.Append(kvp.Key + ";" + kvp.Value + ",");
        if (result.Length == 0)
            return string.Empty;
        return result.ToString().Substring(0, result.Length - 1);
    }
    private string GetStringOfDictionary(Dictionary<string, string> dic)
    {
        StringBuilder result = new StringBuilder();
        foreach (KeyValuePair<string, string> kvp in dic)
            result.Append(kvp.Key + ";" + kvp.Value + ",");
        if (result.Length == 0)
            return string.Empty;
        return result.ToString().Substring(0, result.Length - 1);
    }
    public void RaiseCallbackEvent(string eventArgument)
    {
        if (string.IsNullOrEmpty(eventArgument))
            return;
        RESPONSE = new Dictionary<string, string>();   
        
     //   string show_id = eventArgument.Replace("ShowControl=", "");
        foreach (GridViewRow row in _GridView.Rows) 
        {
      //      GridViewRow row = _GridView.Rows[i];
            CheckBox cb_Parent = (CheckBox)row.FindControl("_CheckBoxParent");
            if (!cb_Parent.Visible)
                continue;
            string label_id = ((Label)row.FindControl("lbl_ParentName")).ClientID;
            if (!((CheckBox)row.FindControl("_CheckBoxAutocomplete")).Checked)
            {
                RESPONSE.Add(label_id, "");
                return;
            }
            
            string OriginalParentId = ((Label)row.FindControl("lbl_OriginalParentId")).Text;
            string _parent=FindParent(OriginalParentId);
            if (_parent != "0")
                _parent = string.Empty;
            
            RESPONSE.Add(label_id, _parent);
           
                
            /*
            string ParentId
            CheckBox cb_Parent = (CheckBox)row.FindControl("_CheckBoxParent");
            
            CheckBox cb_show = (CheckBox)row.FindControl("_CheckBoxDisplay");
            if (cb_show.ClientID != show_id)
                continue;                
            
            bool sss = ((CheckBox)row.FindControl("_CheckBoxMust")).Checked;
            string name = ((Label)row.FindControl("lbl_desc")).Text;
            int t = 0;
             * */
        }
        
    }
   
    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }
    #endregion
}
