<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Publisher/MasterPagePublisher.master" 
CodeFile="EditTranslate.aspx.cs" Inherits="Publisher_EditTranslate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/Toolbox.ascx" TagName="Toolbox" TagPrefix="too" %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link rel="stylesheet" href="popup/PopupStyle.css" type="text/css" media="screen" />
<script src="http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js" type="text/javascript"></script>
<script src="popup/JPopup.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    window.onload = appl_init;
    function appl_init() {
        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);

        pgRegMgr.add_endRequest(EndHandler);

        /****
        var t = document.getElementById("<%# rbl_Type.ClientID %>");
        var inps = t.getElementsByTagName("input");
        for(var i=0;i<inps.length;i++)
        {
            alert(inps[i].value+" "+ inps[i].checked);
        }
        /****/
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }

    function _Search(e, sender) {
        if (e.keyCode == 13) {
            if(!Chk_txt(sender))
            {
                alert("<%# GetAtLeastOneCharacter %>");
                return false;
            }
            <%# SearchTranslate() %>;
            return false;
        }

    }
    function Chk_txt(obj)
    {
        var _txt = obj.value.trim();
        if(_txt.length < 2)
            return false;
        return true;
    }
    function ChkText()
    {
        var _txt = document.getElementById("<%# txt_Search.ClientID %>");
        if(Chk_txt(_txt))
            return true;
        alert("<%# GetAtLeastOneCharacter %>");
        return false;
    }
    function CloseReport()
	{
	    document.getElementById("<%# lbl_report.ClientID %>").innerHTML="";
	    document.getElementById("<%# div_report.ClientID %>").className="popModal_del popModal_del_hide";	 
	    $find("_modal").hide();	    
	}	
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<too:Toolbox ID="_Toolbox" runat="server" />
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>

    <h5><asp:Label CssClass="lblSubTitle" ID="lblSubTitle" runat="server">Welcome to the web translation tool</asp:Label></h5>

<div class="page-content minisite-content5">
<div style="float: left;
    left: 107px;
    margin: 5px;
    position: relative;
    top: -63px;">
<asp:Label ID="lbl_Languages"  runat="server" CssClass="label" Text="Languages"></asp:Label>
        <asp:DropDownList ID="ddlSite_lang"  runat="server" CssClass="form-select" AutoPostBack="true" OnSelectedIndexChanged="ddlSites_SelectedIndexChanged"></asp:DropDownList>            
</div>
<div class="form-analyticsseg">
    <div id="form-analytics">
     
    <div class="SieTable">
    <div class="editradio">   
     <asp:UpdatePanel ID="_UpdatePanelType" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
         <asp:RadioButtonList ID="rbl_Type" runat="server" 
                onselectedindexchanged="rbl_Type_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
            <asp:ListItem Value="Pages" Text="Pages" Selected="True"></asp:ListItem>
            <asp:ListItem Value="Sentence" Text="Sentence"></asp:ListItem>
        </asp:RadioButtonList>
    </ContentTemplate>
    </asp:UpdatePanel>
    
            </div>
            <div style="float:left; width:52%;">
                 <asp:UpdatePanel ID="UpdatePanelListPages" runat="server" UpdateMode="Conditional">
                     <ContentTemplate>
                        
                         <asp:Panel ID="panel_Pages" runat="server">
                            <asp:ListBox ID="lbPages" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lbPages_SelectedIndexChanged" CssClass="list_translate"></asp:ListBox>
                         </asp:Panel>
                         
                         <div class="form-search clearfix" style="position:relative; top:0px;" >
                         <asp:Panel ID="panel_sentence" runat="server" Visible="false">
                             <asp:TextBox ID="txt_Search" CssClass="form-text" MaxLength="40" runat="server" 
                                 onkeypress="return _Search(event, this);"></asp:TextBox>
                             <asp:Button ID="btn_Search" runat="server" Text="Search" 
                                 OnClick="btn_Search_Click" CssClass="form-submit" OnClientClick="return ChkText();" />
                         </asp:Panel>
                        </div>
                     </ContentTemplate>
                     <Triggers>
                         <asp:AsyncPostBackTrigger ControlID="ddlSite_lang" EventName="SelectedIndexChanged" />
                     </Triggers>
                </asp:UpdatePanel>
            </div>  
           
            <div class="clear"></div>
            
                <div class="smallpict">
                <asp:UpdatePanel ID="_UpdatePanelSmallPic" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span id="span_showPic" runat="server"><a id="a_showPic" style="cursor:pointer;"><asp:Image ID="ImgSmallPic" runat="server" Height="90px" Width="70px" /></a></span>
                    </ContentTemplate>
                </asp:UpdatePanel>
                </div>
            </div>

    
     <div class="clear"></div>
    <asp:UpdatePanel ID="UpdatePanelSentences" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
     	    <asp:DataList ID="DataListSentences" runat="server" DataKeyField="Id">
                <ItemTemplate>
                <div runat="server" class="location" id="div_path" visible="<%# Bind('ShowPath') %>">
                    <asp:Label ID="lblPath" runat="server" Text="Location"></asp:Label>:
                    <asp:Label ID="lbl_path" runat="server" Text="<%# Bind('Path') %>" ></asp:Label>
                </div>
                <div class="translation">               
                    <asp:Label ID="lbl_eng"  runat="server" Text="<%# Bind('Sentence') %>"></asp:Label>
                    <asp:TextBox ID="tb_translate" CssClass="translationbox" runat="server" Text="<%# Bind('Translate') %>"></asp:TextBox>
                    <asp:CheckBox ID="cb_UpdateAll" CssClass="checkbox" runat="server" Checked="false" Width="50"/>
                    <asp:Label ID="Label01"  CssClass="translationlabel"  runat="server" Text="<%# lbl_ChangeAll.Text %>"></asp:Label>
                    <asp:Label ID="lbl_PageId" runat="server" Text="<%# Bind('PageId') %>" Visible="false"></asp:Label>
                </div> 
                </ItemTemplate>
            </asp:DataList>
            <asp:Button ID="btn_set" runat="server" Text="SET" OnClick="btn_set_Click" CssClass="form-submit" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lbPages" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    
    <!--
    <td>
        <asp:Button ID="btn_clear" runat="server" Text="Clear Translate" 
        CssClass="form-submit2" OnClick="btn_clear_Click" />
    </td>
    -->

    </div>
</div>
</div>
<div id="popupContact" style="z-index:101;"  >
    <asp:UpdatePanel ID="_UpdatePanelPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h1><asp:Label ID="_TitlePopup" runat="server" Text=""></asp:Label></h1>
    		<p id="contactArea"><asp:Image ID="ImagePopup" runat="server" /></p>
		    <div class="div_close">
    		    <a  id="popupContactClose"><asp:Label ID="lbl_closed" runat="server" Text="Closed"></asp:Label></a>
		    </div>
		</ContentTemplate>
    </asp:UpdatePanel>
</div>
<div id="backgroundPopup"></div>

<div runat="server" id="div_report" class="popModal_del popModal_del_hide">
    <div class="top"></div>
        <a id="a_close" runat="server" class="span_A" href="javascript:CloseReport();"></a>
        <div class="content" >
         <div>
                <h2>                    
                    <asp:Label ID="lbl_titleReport" runat="server" Text="Excel report"></asp:Label>
                </h2>
                </div>
            <asp:UpdatePanel ID="_UpdatePanelReport" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
            <div style="overflow-y:auto; max-height:480px;">
                <asp:Label ID="lbl_report" runat="server" ></asp:Label>
            </div>
            </ContentTemplate>
            </asp:UpdatePanel>
             <div class="popupsegments">
                
                    <input id="btn_closed" type="button" value="Close" onclick="javascript:CloseReport();"
                     class="btn2" />
                    <asp:Button ID="btn_saveLog" runat="server" Text="Open log" CssClass="btn"
                    OnClick="_btn_closed_click" />
                     
               
            </div>
        </div>
        
        <div class="bottom"></div>
    </div>
<cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btnVirtual"
    PopupControlID="div_report"
     BackgroundCssClass="modalBackground" 
      BehaviorID="_modal"               
      DropShadow="false"
    ></cc1:ModalPopupExtender>

<div style="display:none;">
    <asp:Button ID="btn_virtual_search" runat="server" Text="Button" style="display:none;" />
    <asp:Button ID="btnVirtual" runat="server" Text="Button" style="display:none;" />

</div>
    <asp:Label ID="lbl_ChangeAll" runat="server" Text="Change in all places" Visible="false"></asp:Label>
    <asp:Label ID="lbl_NoResult" runat="server" Text="There are no result" Visible="false"></asp:Label>
    <asp:Label ID="lbl_OneCharacter" runat="server" Text="You must at least two character" Visible="false"></asp:Label>
    <asp:Label ID="lbl_incorrectFile" runat="server" Text="Incorrect file type" Visible="false"></asp:Label>
    <asp:Label ID="lbl_incorrectFields" runat="server" Text="Fields names are incorrect" Visible="false"></asp:Label>
    <asp:Label ID="lbl_SavedSuccessfully" runat="server" Text="The heading was saved Successfully" Visible="false"></asp:Label>
     <asp:Label ID="lbl_SavedOk" runat="server" Text="records saved" Visible="false"></asp:Label>
    <asp:Label ID="lbl_dontSaved" runat="server" Text="records does not saved" Visible="false"></asp:Label>
    <asp:Label ID="lbl_problem" runat="server" Text="There are problem with" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DocumentNotSaved" runat="server" Text="records, the document was not saved" Visible="false"></asp:Label>
    <asp:Label ID="lbl_errorLines" runat="server" Text="Error in Line" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Fields" runat="server" Text="Fields" Visible="false"></asp:Label>
    <asp:Label ID="lbl_IdNotExsits" runat="server" Text="Id not exists" Visible="false"></asp:Label>
    <asp:Label ID="lbl_SentenceNotMatch" runat="server" Text="The original sentence of the excel not match. It was changed" Visible="false"></asp:Label>
</asp:Content>

