﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="DashboardSales.aspx.cs" Inherits="Publisher_DashboardSales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>      
<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    var DaysInterval = new Number();
    var ChoosenIntervalOk = new Boolean();
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);

        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
        SetTooltips();
    }
    function CompareToPast(elm) {
        var _div = document.getElementById("_div_compareDate");
        var _btn = document.getElementById("<%# btn_run.ClientID %>");
        if (elm.checked) {
            Set_Dates_Compars();
            _div.style.display = "block";
        }
        else {
            _btn.setAttribute("onclick", "return CheckCurrentDates();");
            _div.style.display = "none";
        }
    }
    function Set_Dates_Compars() {
        //       var _div = document.getElementById("_div_compareDate");
        var _btn = document.getElementById("<%# btn_run.ClientID %>");
        var _format = "<%# GetDateFormat() %>";
        var from_current = document.getElementById("<%# GetFromCurrentId %>");
        var to_current = document.getElementById("<%# GetToCurrentId %>");
        var from_past = document.getElementById("<%# GetFromPastId %>");
        var to_past = document.getElementById("<%# GetToPastId %>");
        var _start = GetDateFormat(from_current.value, _format);
        var _end = GetDateFormat(to_current.value, _format);
        /*
        var diff_day = days_between(_start, _end);

        var end_pass = new Date(_start); //.addDays(-1);
        end_pass.addDays(-1);
        var start_pass = new Date(end_pass);
        start_pass.addDays(diff_day * -1);
        */
        from_past.value = GetDateString(new Date(_start), _format);
        to_past.value = GetDateString(new Date(_end), _format);
        _btn.setAttribute("onclick", "return CheckCurrentAndCompareDates();");
    }
    function Set_Dates_Compars_by(_interval) {
        //       var _div = document.getElementById("_div_compareDate");
        var _btn = document.getElementById("<%# btn_run.ClientID %>");
        var _format = "<%# GetDateFormat() %>";
        var from_current = document.getElementById("<%# GetFromCurrentId %>");
        var to_current = document.getElementById("<%# GetToCurrentId %>");
        var from_past = document.getElementById("<%# GetFromPastId %>");
        var to_past = document.getElementById("<%# GetToPastId %>");
        var _start = GetDateFormat(from_current.value, _format);
        var _end = GetDateFormat(to_current.value, _format);
        /*
        var diff_day = days_between(_start, _end);
        var end_pass = new Date(_start); //.addDays(-1);
        end_pass.addDays(-1);
        var start_pass = new Date(end_pass);
        //    alert(_interval);
        if (_interval == 29)
            start_pass.addMonth(-1);
        else if (_interval == 89)
            start_pass.addMonth(-3);
        else if (_interval == 364)
            start_pass.addFullYear(-1);
        else
            start_pass.addDays(diff_day * -1);
            */
        from_past.value = GetDateString(new Date(_start), _format);
        to_past.value = GetDateString(new Date(_end), _format);
        _btn.setAttribute("onclick", "return CheckCurrentAndCompareDates();");
    }
    function LoadChart(fileXML, _chart) {
        //      alert(fileXML+"----"+_chart);
        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render("div_chart");
    }
    function CheckCurrentDates() {
        $('#<%# hf_LifeTime.ClientID %>').val($('#<%# txt_LifeTime.ClientID %>').val());
        var elem = document.getElementById("<%# GetDivCurrentId %>");
        var result = Check_DateValidation(elem);
        if (!result)
            alert("<%# GetInvalidDate %>");
        return result;
    }
    //CheckCurrentAndPastDates
    function CheckCurrentAndCompareDates() {
        if (!CheckCurrentDates())
            return false;
        var elem = document.getElementById("<%# GetDivPastId %>");
        var result = Check_DateValidation(elem);
        if (!result)
            alert("<%# GetInvalidDate %>");
        return result;
    }
    function SetCompareDate(elem_from, parentIdCurrent, mess, elem_maxId) {
        var _days = GetRangeDays(parentIdCurrent);
        var elm_parent = getParentByClassName(elem_from, "div_DatePicker");
        var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;
        var elem_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
        var elem_max = document.getElementById(elem_maxId);

        var _MaxValue = GetDateFormat(elem_max.value, _format);
        var _start = GetDateFormat(elem_from.value, _format);
        var _end = _start + ((24 * 60 * 60 * 1000) * _days); //GetDateFormat(elem_to.value, _format);
     //   _MaxValue = _MaxValue - (24 * 60 * 60 * 1000);
        //    alert("max= " +elem_max.id+" end= "+elem_from.id);
        //    alert("max= " +_MaxValue+" end= "+_end);
    //    if (_MaxValue < _end) {
        if(_start > _MaxValue){
      //      _end = _MaxValue;
     //       var start_date = new Date(_MaxValue);
            var end_date = new Date(_start);
            end_date.addDays(_days);
         //   start_date.addDays(-1 * _days);
            //       alert(start_date);
         //   elem_from.value = GetDateString(start_date, _format);
       //     elem_to.value = GetDateString(end_date, _format);
            elem_to.value = GetDateString(end_date, _format);
        //    alert("<%# Lbl_DatesChange %>");
        }
        else {
            elem_to.value = GetDateString(new Date(_end), _format);
        }

    }
    function GetIntervalDates() {
        
        return 0;
    }
    function SetIntervalDatesValidation(elem) {
       
        ChoosenIntervalOk = true;
        SetDdlInterval(elem);
        if (!ChoosenIntervalOk)
            alert(document.getElementById('<%# lbl_ChosenInterval.ClientID %>').value);
    }
    function SetDdlInterval(elem) {
        DaysInterval = GetIntervalDates(); //elem.options[elem.selectedIndex].value;
        var _index = elem.selectedIndex;
        var Interval_name = elem.options[_index].value;
        var _days = GetRangeDays("<%# GetDivCurrentId %>");


        if (DaysInterval > _days) {
            _index--;
            ChoosenIntervalOk = false;
            elem.selectedIndex = _index;
            SetDdlInterval(elem);
        }
    }
    function OnBlurByDays(elm) {

        var elm_to;
        var elm_from;
        var elm_parent = getParentByClassName(elm, "div_DatePicker");
        var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;

        if (!validate_Date(elm.value, _format)) {
            try { alert(WrongFormatDate); } catch (ex) { }
            elm.focus();
            elm.select();
            return -1;
        }
        if (elm.className.indexOf("_to") == -1) {
            elm_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
            elm_from = elm;
        }
        else {
            elm_from = getElementsWithMultiClassesByClass("_from", elm_parent, "input")[0];
            elm_to = elm;
        }
        DaysInterval = GetIntervalDates();
        var _end = GetDateFormat(elm_to.value, _format);
        var _start = GetDateFormat(elm_from.value, _format);
        if ((_start + (24 * 60 * 60 * 1000 * DaysInterval)) > _end) {
            if (elm_from.id == elm.id) {
                _end = new Date(_start);
                if (DaysInterval == 29)
                    _end.addMonth(1);
                else if (DaysInterval == 89)
                    _end.addMonth(3);
                else if (DaysInterval == 364)
                    _end.addFullYear(1);
                else {
                    _end = _start + (24 * 60 * 60 * 1000 * DaysInterval);
                    _end = new Date(_end);
                }
                elm_to.value = GetDateString(_end, _format);
            }
            else {

                _start = new Date(_end);
                if (DaysInterval == 29)
                    _start.addMonth(-1);
                else if (DaysInterval == 89)
                    _start.addMonth(-3);
                else if (DaysInterval == 364)
                    _start.addFullYear(-1);
                else {
                    _start = _end - (24 * 60 * 60 * 1000 * DaysInterval);
                    _start = new Date(_start);
                }
                elm_from.value = GetDateString(_start, _format);
            }
           
            return -10;
        }
        return 1;

    }
    function _AddOn() {
        if ($('.selected.-AddOn').length > 0)
            return false;
        $('.-AddOn').addClass('selected');
        $('.-Injection').removeClass('selected');
        $('#<%# hf_Tab.ClientID %>').val('addon');
        return true;
    }
    function _Injection() {
        if ($('.selected.-Injection').length > 0)
            return false;
        $('.-Injection').addClass('selected');
        $('.-AddOn').removeClass('selected');
        $('#<%# hf_Tab.ClientID %>').val('injection');
        return true;
    }
    $(function () {
        SetTooltips();
    });
    function SetTooltips() {
        $('.-tooltip').each(function () {
            var _height = $(this).parent('div.contain-tooltip').find('.tooltip-TextRow').get(0).clientHeight;
            $(this).parent('div.contain-tooltip').find('.tooltip-TextRow').css({ 'visibility': 'visible', 'display': 'none' });
            if (_height > 60)
                $(this).parent('div.contain-tooltip').find('.tooltip-TextRow').css({ 'width': '600px', 'top': '-60px' });
            else if (_height > 45)
                $(this).parent('div.contain-tooltip').find('.tooltip-TextRow').css({ 'width': '600px' });
            else if (_height > 30)
                $(this).parent('div.contain-tooltip').find('.tooltip-TextRow').css({ 'top': '-60px' });
            $(this).mouseover(function () {
                $(this).parent('div.contain-tooltip').find('.tooltip-TextRow').show();
            });
            $(this).mouseleave(function () {
                $(this).parent('div.contain-tooltip').find('.tooltip-TextRow').hide();
            });
        });
    }


</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
 <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>

<div class="page-content minisite-content">
    <h2><asp:Label ID="lbl_Dashboard" runat="server" Text="Financial Dashboard"></asp:Label></h2>
 <div id="form-analytics" > 
    <div class="form-fieldDleft">
    
        <div class="dash"><uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" /></div>
    
        

 
  </div>
    <div class="form-fieldDright" runat="server" visible="false">
       
   
        <div class="form-field">
            <asp:Label ID="lbl_Heading" runat="server" Cssclass="label" Text="Heading"></asp:Label>
            <asp:DropDownList ID="ddl_Heading" CssClass="form-select" runat="server" onchange="SetIntervalDatesValidation(this);">
            </asp:DropDownList>
        </div>
    
    
     </div>
     <div class="clear"></div>
     <div runat="server" visible="false">
         <div class="form-field">
            <asp:Label ID="lbl_Flavor" runat="server"  CssClass="label-secondLine" Text="Flavor"></asp:Label>
            <asp:DropDownList ID="ddl_FlavorMain" CssClass="form-select" runat="server">
            </asp:DropDownList>
        </div>
     </div>
     <div class="clear"></div>    
      <div>
      <div class="form-field-secondLine">
            <asp:CheckBox ID="cb_Compare" runat="server" onclick="CompareToPast(this);" Text="Compare to" />
            
            
        </div>
        <div class="form-field-secondLine" style="width:200px;">        
            <asp:CheckBox ID="cb_UseOnlySoldHeadings" runat="server" Text="Show only paid categories" />
        </div>
        
      
      </div>
      <div class="clear"><!-- --></div>
         <div class="compare2"  id="_div_compareDate" style="display:none;">
               <div>
           
                <div class="dash-compare"><uc1:FromToDatePicker ID="FromToDatePicker_compare" runat="server" /></div>
                <div class="form-field">
                    <asp:UpdatePanel ID="_up_OriginCompare" runat="server" UpdateMode="Conditional">
                    <ContentTemplate> 
                        <asp:Label ID="lbl_OriginCompare" runat="server" Cssclass="label-secondLine" Text="<%# lbl_Origin.Text %>"></asp:Label>        
                        <asp:DropDownList ID="ddl_OriginCompare" CssClass="form-select" runat="server">
                        </asp:DropDownList>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
   
                <div class="form-field" runat="server" visible="false">
                    <asp:Label ID="lbl_HeadingCompare" runat="server" Cssclass="label-secondLine" Text="<%# lbl_Heading.Text %>"></asp:Label>
                    <asp:DropDownList ID="ddl_HeadingCompare" CssClass="form-select" runat="server" onchange="SetIntervalDatesValidation(this);">
                    </asp:DropDownList>
                </div>
             </div>
             <div class="clear"><!-- --></div> 
             <div class="form-field" runat="server" visible="false">
                <asp:Label ID="lbl_FlavorCompare" runat="server"  CssClass="label-secondLine" Text="<%# lbl_Flavor.Text %>"></asp:Label>
                <asp:DropDownList ID="ddl_FlavorCompare" CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
        </div>   
    
      
    
        <div class="clear"></div>
    <div class="dashsubmit">
        <asp:Button ID="btn_run" runat="server" Text="Create report"  CssClass="CreateReportSubmit2"
            onclick="btn_run_Click" OnClientClick="return CheckCurrentDates();" />
    
    </div>
    <div class="clear"><!-- --></div>
    <div class="results">
            <asp:Label ID="lbl_LastUpdate" runat="server" Text="Last update:"></asp:Label>
            <asp:Label ID="lblLastUpdate" runat="server" Text="Label"></asp:Label>
        </div>
     <div id="Ticket_Tabs" class="div-menu-dashboard" runat="server"><!-- here the red bar-->
       
            <ul id="ulTabs" runat="server">
                <li><asp:LinkButton ID="lb_AddOn" runat="server" Text="Add-Ons" 
                    OnClientClick="return _AddOn();" onclick="lb_AddOn_Click" CssClass="-AddOn selected"></asp:LinkButton></li>
                <li><asp:LinkButton ID="lb_Injedction" runat="server" Text="Injections" CssClass="-Injection"
                    OnClientClick="return _Injection();" onclick="lb_Injedction_Click"></asp:LinkButton></li>
               
            </ul>
       <asp:HiddenField ID="hf_Tab" runat="server"/>
            
        </div>
        
       <div class="clear"><!-- --></div> 
        
            <asp:UpdatePanel ID="_up_OriginMain" runat="server" UpdateMode="Conditional">
            <ContentTemplate>  
                <div class="form-field">
                
                    <asp:Label ID="lbl_Origin" runat="server" Cssclass="label" Text="Origin"></asp:Label>        
                    <asp:DropDownList ID="ddl_Origin" CssClass="form-select" runat="server" 
                        AutoPostBack="true" onselectedindexchanged="ddl_Origin_SelectedIndexChanged">
                    </asp:DropDownList>
                       
                </div>
                <div class="form-field" runat="server" id="div_LifeTime">
                   <asp:Label ID="lbl_LifeTime" runat="server" Cssclass="label" Text="Life Time"></asp:Label>
                   <asp:TextBox ID="txt_LifeTime" runat="server" CssClass="form-text"></asp:TextBox>
                    <asp:HiddenField ID="hf_LifeTime" runat="server" />
                    <asp:RangeValidator ID="rv_LifeTime" runat="server" ErrorMessage="Integer number above zero" ControlToValidate="txt_LifeTime"
                            CssClass="error-msg" Type="Integer" MinimumValue="0" MaximumValue="100"></asp:RangeValidator>
                </div>
            </ContentTemplate>
            </asp:UpdatePanel>   
        
 </div>   
    <div class="clear"><!-- --></div>
  
    <div>
        <asp:UpdatePanel ID="UpdatePanel_table" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
           <asp:Repeater ID="_RepeaterParent" runat="server" onitemdatabound="_RepeaterParent_ItemDataBound">
           <HeaderTemplate>
                 <table class="data-tabledash" style="margin:0;">
                    <tr class="_TH" runat="server" id="tr_head">
                        <th>
                            <asp:Label ID="Label101" runat="server" Text="<%# lbl_subject.Text %>"></asp:Label>
                        </th>
                        <th runat="server" id="td_PastValue" visible="<%# ShowPast %>">
                            <asp:Label ID="Label100" runat="server" Text="<%# lbl_CompareValue.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label102" runat="server" Text="<%# lbl_CurrentValue.Text %>"></asp:Label>
                        </th>
                        <th runat="server" id="Th_Gap" visible="<%# ShowPast %>">
                           <asp:Label ID="Label103" runat="server" Text="<%# lbl_Gap.Text %>"></asp:Label> 
                        </th>
                    
                    </tr>
               
           </HeaderTemplate>
           <ItemTemplate>
                    <tr runat="server" id="tr_group">
                        <td class="group group-financial" runat="server" id="td_group" colspan="2">
                            <asp:Label ID="Label200" runat="server" Text="<%# Bind('_group') %>"></asp:Label>
                            <asp:Label ID="lbl_index" runat="server" Text="<%# Bind('index') %>" Visible="false"></asp:Label>
                            
                        </td>
                    </tr>
                   <asp:Repeater ID="_Repeater" runat="server" 
                        onitemdatabound="_Repeater_ItemDataBound" >
            
                    
            
                    <ItemTemplate>                            
                        <tr runat="server" id="tr_data">
                            <td class="td-left">
                                <div class="contain-tooltip">
                                    <asp:LinkButton ID="lb_row" runat="server" Text="<%# Bind('RowText') %>"
                                            CommandArgument="<%# Bind('Row') %>" OnClick="lb_row_click" CssClass="-tooltip"></asp:LinkButton>                   
                                    <asp:Label ID="lbl_row" runat="server" Text="<%# Bind('Row') %>" Visible="false"></asp:Label>  
                                    <div class="tooltip-TextRow" style="visibility:hidden">
                                        <asp:Label ID="lbl_tooltip" runat="server" Text="<%# Bind('tooltip') %>"></asp:Label>
                                    </div>  
                                </div>                            
                            </td>
                            <td runat="server" id="td_PastPeriodIn" visible="<%# Bind('ShowPast') %>">
                                <asp:Label ID="Label10" runat="server" Text="<%# Bind('PastValue') %>"></asp:Label>       
                            </td>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="<%# Bind('Value') %>"></asp:Label>
                            </td>  
                            <td runat="server" id="td_Gap" visible="<%# Bind('ShowPast') %>">
                                <asp:Label ID="Label50" runat="server" Text="<%# Bind('Gap') %>"></asp:Label>       
                            </td>                    
                        </tr>
                    </ItemTemplate>
            
                    
                    </asp:Repeater>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
     <div>
        <asp:UpdatePanel ID="UpdatePanel_chart" runat="server" UpdateMode="Conditional">
        <ContentTemplate>                
            <div id="div_chart"></div>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>


 <asp:Label ID="lbl_Current" runat="server" Text="Main" Visible="false"></asp:Label>
 <asp:Label ID="lbl_activity" Cssclass="label2" runat="server" Text="Activity dates from" Visible="false"></asp:Label>
 <asp:Label ID="lbl_activityPast" Cssclass="label2" runat="server" Text="Compare dates from" Visible="false"></asp:Label>
 <asp:Label ID="lbl_sooner" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lbl_DatesChange" Text="The date you selected have been changed according to interval requirement" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lbl_CurrentDot" runat="server" Text="Current dot" Visible="false"></asp:Label>
<asp:HiddenField ID="lbl_ChosenInterval" runat="server"
            Value="The interval you have chosen is not adequate to the date range you selected. Please choose smaller value" />
 <asp:Label ID="lbl_DateError" runat="server" Text="Invalid date" Visible="false"></asp:Label>
 <asp:Label ID="lbl_CompareValue" runat="server" Text="Compare Value" Visible="false"></asp:Label>
<asp:Label ID="lbl_subject" runat="server" Text="Subject" Visible="false"></asp:Label>
<asp:Label ID="lbl_CurrentValue" runat="server" Text="Main Value" Visible="false"></asp:Label>
<asp:Label ID="lbl_Gap" runat="server" Text="Gap" Visible="false"></asp:Label>
  
</asp:Content>

