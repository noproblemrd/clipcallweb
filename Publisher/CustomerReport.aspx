﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="CustomerReport.aspx.cs" Inherits="Publisher_CustomerReport" %>


<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="~/Publisher/ClipCallControls/CustomerReportControl.ascx" TagPrefix="uc1" TagName="CustomerReportControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
     <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
    <script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.onload = appl_init;

        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);

        }
        function BeginHandler() {
            //     alert('in1');
            showDiv();
        }
        function EndHandler() {
            //      alert('out1');       
            hideDiv();
        }
        function _CreateExcel() {
            create_excel('<%# GetCreateExcel %>', '<%# SessionTableName %>', 'CustomerReport', '<%# DownloadExcel %>');
           
        }
        function chkTextBox(event) {
            //     alert(event.keyCode);
            if (event.keyCode != 13)
                return true;
            run_report();
            return false;

        }
        function run_report() {

            <%# RunReport() %>;

        }
        function TransferOrigin(_a) {
            var _confirm = confirm("Are you sure you want to transfer?");
            if (_confirm == false)
                return;
            var customerId = $(_a).parents('tr').find('input[type="hidden"]').val();
            $(_a).hide();
            var $div_parent = $(_a).parents('div.div_TransferOrigin');
            $div_parent.find('div.div_TransferOrigin_loader').show();
            $.ajax({
                url: "<%# UrlTransferOrigin %>",
                data: "{ 'customerId': '" + customerId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                  //  var _data = eval("(" + data.d + ")");
                    $div_parent.find('span.span_TransferOrigin').html(data.d.IsSuccess ? 'Success' : 'Failed');
                    if (data.d.IsSuccess) {
                        $(_a).parents('tr').find('span.span_origin').html(data.d.CurrentOrigin);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    $div_parent.find('div.div_TransferOrigin_loader').hide();
                }

            });
        }
        
        function PagingStart() {
            var paging = new PagingObject('data-table', '<%# GetPagingMethodPath() %>');
            paging.Start();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="Date-field" runat="server" id="div_Date">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  		
            <div id="div_origin" class="form-field" runat="server">
					<label for="form-subsegment" class="label" runat="server" id="lblExperties" >Origin</label>
                <asp:DropDownList ID="ddl_origin" runat="server" CssClass="form-select">
                </asp:DropDownList>					            
			</div>
            <div style="clear:both;"></div>			 
			<div class="form-field">
                <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Free Text</label>
			    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-text" MaxLength="40" onkeydown="return chkTextBox(event);"></asp:TextBox>								
                
            </div>          
		</div>
      
		<div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>     
        <asp:UpdatePanel id="_UpdatePanel" runat="server"  UpdateMode="Conditional">                           
        <ContentTemplate>   
         <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>     
           
        <div id="Table_Report"  runat="server" >  
				<uc1:CustomerReportControl  runat="server" ID="_CustomerReportControl" />
			</div>
        </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    <div style="display:none;">
    <asp:Button ID="btn_virtual_run" runat="server" Text="Button" style="display:none;" onfocus="this.blur();" />
</div>
    </div>
</asp:Content>

