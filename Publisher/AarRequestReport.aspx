﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AarRequestReport.aspx.cs" Inherits="Publisher_AarRequestReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link href="../FromToDate.css" type="text/css" rel="stylesheet" />
<script  type="text/javascript" src="../Calender/_Calender.js"></script>
<link href="../Calender/CalenderDiv.css" rel="stylesheet" type="text/css" />
<link href="../UpdateProgress/updateProgress.css" type="text/css" rel="stylesheet" />

<script type="text/javascript" >
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }
    function OpenHeadDetails(sender, _id, trId) {
        if($('#'+trId).is(':hidden')){
            $(sender).find("a").attr("class", "SideArrow");
            $('#' + trId).show();
        }
        else{
            $(sender).find("a").attr("class", "arrow");
            $('#' + trId).find("td").children().remove();
            $('#' + trId).hide();
        }
        
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  

<div class="page-content minisite-content2">
    <div id="form-analytics">
        <div class="form-fields clearfix">
            <div class="form-fieldlargeppc">       
                <asp:Label ID="lbl_Heading" runat="server" class="label" Text="Heading"></asp:Label> 
                <asp:DropDownList ID="ddl_Heading" CssClass="form-selectlarge" runat="server">
                </asp:DropDownList>
            </div>
            <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>
          </div>
        
          
    </div>
     <div class="clear"></div>     
     <div class="table2"> 

        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
           <div class="results3"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
            <div class="clear"></div>
            
            <div class="div_ReportTable">
            <asp:Repeater ID="_Repeater" runat="server" 
                    onitemdatabound="_Repeater_ItemDataBound" >
                <HeaderTemplate>
                    <table class="data-table table_AarRequest"">
                    <tr>
                        <th>
                            &nbsp;
                        </th>
                        <th>
                            <asp:Label ID="Label01" runat="server" Text="<%# lbl_HeadingName.Text %>"></asp:Label>     
                        </th>
                        <th>
                            <asp:Label ID="Label02" runat="server" Text="<%# lbl_AvgMinutesForAcPress1.Text %>"></asp:Label>      
                        </th>
                        <th>
                            <asp:Label ID="Label03" runat="server" Text="<%# lbl_AvgMinutesForItcPress1.Text %>"></asp:Label>     
                        </th>
                        <th>
                            <asp:Label ID="Label04" runat="server" Text="<%# lbl_AvgMinutesForItcLastPress1.Text %>"></asp:Label>     
                        </th>
                        <th>
                            <asp:Label ID="Label05" runat="server" Text="<%# lbl_AvgAttemptsForAcPress1.Text %>"></asp:Label>    
                        </th>
                        <th>
                            <asp:Label ID="Label06" runat="server" Text="<%# lbl_AvgAttemptsForItcPress1.Text %>"></asp:Label>  
                        </th>
                        <th>
                             <asp:Label ID="Label07" runat="server" Text="<%# lbl_AvgAttemptsForItcLastPress1.Text %>"></asp:Label>       
                        </th>
                        <th>
                            <asp:Label ID="Label08" runat="server" Text="<%# lbl_AvgMinutesForFullfilment.Text %>"></asp:Label>      
                        </th>
                        <th>
                             <asp:Label ID="Label09" runat="server" Text="<%# lbl_AvgAttemptsForFullfilment.Text %>"></asp:Label>  
                        </th>

                        <th>
                             <asp:Label ID="Label10" runat="server" Text="<%# lbl_NumberOfFullfiledCases.Text %>"></asp:Label>    
                        </th>
                        <th>
                            <asp:Label ID="Label11" runat="server" Text="<%# lbl_NumberOfNotFullfiledCases.Text %>"></asp:Label>          
                        </th>
                        <th>
                              <asp:Label ID="Label12" runat="server" Text="<%# lbl_NumberOfAttemptsToAcs.Text %>"></asp:Label>  
                        </th>

                         <th>
                           <asp:Label ID="Label13" runat="server" Text="<%# lbl_NumberOfAttemptsToItc.Text %>"></asp:Label>           
                        </th>
                        <th>
                            <asp:Label ID="Label14" runat="server" Text="<%# lbl_NumberOfAttemptsToItcLast.Text %>"></asp:Label>     
                        </th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr runat="server" id="tr_main">
                        <td>
                            <a class="arrow" runat="server" id="a_open"></a>
                        </td>
                        <td>
                            <asp:Label ID="Label101" runat="server" Text="<%# Bind('HeadingName') %>"></asp:Label>       
                            <asp:Label ID="lbl_HeadingId" runat="server" Text="<%# Bind('HeadingId') %>" Visible="false"></asp:Label> 
                        </td>
                        <td>
                            <asp:Label ID="Label102" runat="server" Text="<%# Bind('AvgMinutesForAcPress1') %>"></asp:Label>   
                        </td>
                        <td>
                             <asp:Label ID="Label103" runat="server" Text="<%# Bind('AvgMinutesForItcPress1') %>"></asp:Label>    
                        </td>
                        <td>
                             <asp:Label ID="Label104" runat="server" Text="<%# Bind('AvgMinutesForItcLastPress1') %>"></asp:Label> 
                        </td>
                        <td>
                            <asp:Label ID="Label105" runat="server" Text="<%# Bind('AvgAttemptsForAcPress1') %>"></asp:Label>  
                        </td>
                        <td>
                            <asp:Label ID="Label106" runat="server" Text="<%# Bind('AvgAttemptsForItcPress1') %>"></asp:Label>  
                        </td>
                        <td>
                            <asp:Label ID="Label107" runat="server" Text="<%# Bind('AvgAttemptsForItcLastPress1') %>"></asp:Label>  
                        </td>
                        <td>
                            <asp:Label ID="Label108" runat="server" Text="<%# Bind('AvgMinutesForFullfilment') %>"></asp:Label> 
                        </td>
                        <td>
                            <asp:Label ID="Label109" runat="server" Text="<%# Bind('AvgAttemptsForFullfilment') %>"></asp:Label>       
                        </td>
                        <td>
                            <asp:Label ID="Label110" runat="server" Text="<%# Bind('NumberOfFullfiledCases') %>"></asp:Label>       
                        </td>
                        <td>
                            <asp:Label ID="Label111" runat="server" Text="<%# Bind('NumberOfNotFullfiledCases') %>"></asp:Label> 
                        </td>
                        <td>
                             <asp:Label ID="Label112" runat="server" Text="<%# Bind('NumberOfAttemptsToAcs') %>"></asp:Label>       
                        </td>
                        <td>
                            <asp:Label ID="Label113" runat="server" Text="<%# Bind('NumberOfAttemptsToItc') %>"></asp:Label> 
                        </td>
                        <td>
                            <asp:Label ID="Label114" runat="server" Text="<%# Bind('NumberOfAttemptsToItcLast') %>"></asp:Label>  
                        </td>
                    </tr>
                    <tr id="tr__details" runat="server" >
                        <td colspan="15">
                            <asp:GridView ID="_GridViewIn" runat="server" AutoGenerateColumns="false" CssClass="data-table" Width="100%">
                            <Columns>                            
                                <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl100" runat="server" Text="<%# lbl_CaseNumber.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl101" runat="server" Text="<%# bind('CaseNumber') %>" ></asp:Label>
                                </ItemTemplate>
                                 </asp:TemplateField>

                                <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl102" runat="server" Text="<%# lbl_IsFullfiled.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl103" runat="server" Text="<%# bind('IsFullfiled') %>" ></asp:Label>
                                </ItemTemplate>
                                 </asp:TemplateField>

                                <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl110" runat="server" Text="<%# lbl_AvgMinutesForAcPress1.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl111" runat="server" Text="<%# bind('AvgMinutesAcsPress1') %>" ></asp:Label>
                                </ItemTemplate>
                                 </asp:TemplateField>

                                  <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl112" runat="server" Text="<%# lbl_AvgMinutesForItcPress1.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl113" runat="server" Text="<%# bind('AvgMinutesItcsPress1') %>" ></asp:Label>
                                </ItemTemplate>
                                 </asp:TemplateField>

                                <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl114" runat="server" Text="<%# lbl_AvgMinutesForItcLastPress1.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl115" runat="server" Text="<%# bind('AvgMinutesItcsLastPress1') %>" ></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl104" runat="server" Text="<%# lbl_AvgAttemptsForAcPress1.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl105" runat="server" Text="<%# bind('AvgAttemptsAcsPress1') %>" ></asp:Label>
                                </ItemTemplate>
                                 </asp:TemplateField>
                                 
                                <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl106" runat="server" Text="<%# lbl_AvgAttemptsForItcPress1.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl107" runat="server" Text="<%# bind('AvgAttemptsItcsPress1') %>" ></asp:Label>
                                </ItemTemplate>
                                 </asp:TemplateField>
                                 
                                 <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl108" runat="server" Text="<%# lbl_AvgAttemptsForItcLastPress1.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl109" runat="server" Text="<%# bind('AvgAttemptsItcsLastPress1') %>" ></asp:Label>
                                </ItemTemplate>
                                 </asp:TemplateField>

                                  <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl122" runat="server" Text="<%# lbl_MinutesToFillfilment.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl123" runat="server" Text="<%# bind('MinutesToFillfilment') %>" ></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>

                                 <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl124" runat="server" Text="<%# lbl_CallsToFullfilment.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl125" runat="server" Text="<%# bind('CallsToFullfilment') %>" ></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>

                                 <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl116" runat="server" Text="<%# lbl_totalAttemptsAcs.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl117" runat="server" Text="<%# bind('totalAttemptsAcs') %>" ></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>

                                 <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl118" runat="server" Text="<%# lbl_totalAttemptsItcs.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl119" runat="server" Text="<%# bind('totalAttemptsItcs') %>" ></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>

                                 <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lbl120" runat="server" Text="<%# lbl_totalAttemptsItcsLast.Text %>" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl121" runat="server" Text="<%# bind('totalAttemptsItcsLast') %>" ></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>

                                
                            </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </ItemTemplate>
                
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
             <uc1:TablePaging ID="TablePaging1" runat="server" />
             </div>
        </ContentTemplate>
    </asp:UpdatePanel>
  
</div>
</div>
<asp:Label ID="lbl_HeadingName" runat="server" Text="Heading Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_AvgMinutesForAcPress1" runat="server" Text="Avg. (Min) Ac Press1" Visible="false"></asp:Label>
<asp:Label ID="lbl_AvgMinutesForItcPress1" runat="server" Text="Avg. (Min) ITC Press 1" Visible="false"></asp:Label>
<asp:Label ID="lbl_AvgMinutesForItcLastPress1" runat="server" Text="Avg. (Min) Last ITC Press 1" Visible="false"></asp:Label>
<asp:Label ID="lbl_AvgAttemptsForAcPress1" runat="server" Text="Avg. Attempts Ac Press 1" Visible="false"></asp:Label>
<asp:Label ID="lbl_AvgAttemptsForItcPress1" runat="server" Text="Avg. Attempts ITC Press 1" Visible="false"></asp:Label>
<asp:Label ID="lbl_AvgAttemptsForItcLastPress1" runat="server" Text="Avg. Attempts Last ITC Press 1" Visible="false"></asp:Label>
<asp:Label ID="lbl_AvgMinutesForFullfilment" runat="server" Text="Avg. (Min) For Fulfillment" Visible="false"></asp:Label>
<asp:Label ID="lbl_AvgAttemptsForFullfilment" runat="server" Text="Avg. Attempts For Fulfillment" Visible="false"></asp:Label>
<asp:Label ID="lbl_NumberOfFullfiledCases" runat="server" Text="# Fulfilled Cases" Visible="false"></asp:Label>
<asp:Label ID="lbl_NumberOfNotFullfiledCases" runat="server" Text="# Not Fulfilled Cases" Visible="false"></asp:Label>
<asp:Label ID="lbl_NumberOfAttemptsToAcs" runat="server" Text="# Attempts To ACs" Visible="false"></asp:Label>
<asp:Label ID="lbl_NumberOfAttemptsToItc" runat="server" Text="# Attempts To ITC" Visible="false"></asp:Label>
<asp:Label ID="lbl_NumberOfAttemptsToItcLast" runat="server" Text="# Attempts To Last ITC" Visible="false"></asp:Label>


<asp:Label ID="lbl_CaseNumber" runat="server" Text="Case Number" Visible="false"></asp:Label>
<asp:Label ID="lbl_IsFullfiled" runat="server" Text="Is Fullfiled" Visible="false"></asp:Label>
<asp:Label ID="lbl_totalAttemptsAcs" runat="server" Text="Total Attempts ACs" Visible="false"></asp:Label>
<asp:Label ID="lbl_totalAttemptsItcs" runat="server" Text="Total Attempts ITCs" Visible="false"></asp:Label>
<asp:Label ID="lbl_totalAttemptsItcsLast" runat="server" Text="Total Attempts ITCs Last" Visible="false"></asp:Label>
<asp:Label ID="lbl_MinutesToFillfilment" runat="server" Text="Minutes To Fillfilment" Visible="false"></asp:Label>
<asp:Label ID="lbl_CallsToFullfilment" runat="server" Text="Calls To Fullfilment" Visible="false"></asp:Label>

<asp:Label ID="lbl_AARRequest" runat="server" Text="AAR Request" Visible="false"></asp:Label>

<asp:Label ID="lblRecordMached" runat="server" Text="records matched your query" Visible="false"></asp:Label>

</asp:Content>


        
