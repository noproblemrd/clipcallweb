﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class Publisher_AarDashboard : PageSetting
{
    const int WEEK_FROM_TODAY = 6;
    const string FILE_NAME = "AarDashboard.xml";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
    //        LoadInterval();
            dataV = null;
            LoadShowMe();
            setDateTime();
            LoadDefaultReport();
            LoadSentences();
            setSentencesDatePicker();
            LoadExperties();
        }
        LoadValidationCompare();
        ScriptManager1.RegisterAsyncPostBackControl(btn_run);
        Header.DataBind();
    }
    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        XDocument xdd = XDocument.Parse(result);
        
        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;

    }
    private void setSentencesDatePicker()
    {
        FromToDatePicker_current.SetFromSentence = lbl_activity.Text;
        FromToDatePicker_compare.SetFromSentence = lbl_activityPast.Text;
    }

    private void LoadSentences()
    {
        if (string.IsNullOrEmpty(lbl_sooner.Text))
            lbl_sooner.Text = @"The value should be sooner than the value in the 'activity dates from' field.";
    }

    private void LoadDefaultReport()
    {
        WebReferenceReports.AarDashboardReportRequest request = new WebReferenceReports.AarDashboardReportRequest();
        request.FromDate = DateTime.Now.AddDays(-6);
        request.ToDate = DateTime.Now;
        /*
        WebReferenceReports.Interval _interval = WebReferenceReports.Interval.Days;
        foreach (WebReferenceReports.Interval inter in Enum.GetValues(typeof(WebReferenceReports.Interval)))
        {
            if (inter.ToString() == ddl_Interval.SelectedValue)
                _interval = inter;
        }
        
        request.Interval = _interval;
         * */
        request.ShowMe = GetCrireria(ddl_Showme.SelectedValue);
        request.CompareToPast = false;
        
        RunReport(request);
    }

    private void LoadValidationCompare()
    {
        //compare dates
        TextBox txt_to = FromToDatePicker_compare.GetTextBoxTo();
        string _from = FromToDatePicker_compare.GetTextBoxFrom().ClientID;
        txt_to.Attributes.Add("readonly", "readonly");
        txt_to.CssClass = "_to label_DatePicker form-textcal";
        string _ScriptFunc = "if(OnBlurString(this)) SetCompareDate(this, '" + FromToDatePicker_current.GetParentDivId() +
            @"', """ + lbl_sooner.Text + @""", '" + FromToDatePicker_current.GetTextBoxFrom().ClientID + "');";
        string FromScript = @"$('#" + _from + @"').datepicker({ onClose: function(dateText, inst) { " + _ScriptFunc + " }});";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "FromCompareScript", FromScript, true);
        //current date
        string current_fromId = FromToDatePicker_current.GetTextBoxFrom().ClientID;
        string current_toId = FromToDatePicker_current.GetTextBoxTo().ClientID;

        string InsideScript = "var _result = OnBlurByDays(this); if(_result == 1) Set_Dates_Compars(); else if(_result == -10) " +
            "{ alert('" + lbl_DatesChange.Text + "'); Set_Dates_Compars(); }";
        string from_script =
            @"$('#" + current_fromId + @"').datepicker({ onClose: function(dateText, inst) { " + InsideScript + "}});";
        string to_script =
            @"$('#" + current_toId + @"').datepicker({ onClose: function(dateText, inst) { " + InsideScript + "}});";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "FromCurrentScript", from_script, true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ToCurrentScript", to_script, true);
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (!string.IsNullOrEmpty(PathV))
        {

            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSArea.swf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + _chart + "');", true);
        }

    }
    private void LoadShowMe()
    {
        ddl_Showme.Items.Clear();
        Dictionary<string, string> dic = new Dictionary<string, string>();
        foreach (string s in Enum.GetNames(typeof(WebReferenceReports.AarCriteria)))
        {
            string _txt = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "AarCriteria ", s);
            dic.Add(s, _txt);
            //        ddl_Showme.Items.Add(new ListItem(_txt, s));
        }
        IEnumerable<KeyValuePair<string, string>> query = (from x in dic
                                                           select new KeyValuePair<string, string>(x.Key, x.Value)).OrderBy(x => x.Value);
        foreach (KeyValuePair<string, string> kvp in query)
        {
            ddl_Showme.Items.Add(new ListItem(kvp.Value, kvp.Key));
        }
        dicAarCreteriaV = dic;
    }
    /*
    private void LoadInterval()
    {
        ddl_Interval.Items.Clear();
        foreach (string s in Enum.GetNames(typeof(WebReferenceReports.Interval)))
        {
            string _txt = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "Interval", s);
            ddl_Interval.Items.Add(new ListItem(_txt, s));
        }
    }
    */
    private void setDateTime()
    {
        FromToDatePicker_current.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
            WEEK_FROM_TODAY);
    }
    protected void btn_run_Click(object sender, EventArgs e)
    {
        requestV = null;
        WebReferenceReports.AarDashboardReportRequest request = new WebReferenceReports.AarDashboardReportRequest();
        request.FromDate = FromToDatePicker_current.GetDateFrom;
        request.ToDate = FromToDatePicker_current.GetDateTo;
        request.HeadingId = new Guid(ddl_Heading.SelectedValue);
        request.ShowMe = GetCrireria(ddl_Showme.SelectedValue);
        if(cb_Compare.Checked)
        {
           
            request.CompareToPast = true;
            request.PastFromDate = FromToDatePicker_compare.GetDateFrom;
            request.PastToDate = FromToDatePicker_compare.GetDateTo;
        }
        RunReport(request);

    }
    void RunReport(WebReferenceReports.AarDashboardReportRequest request)
    {        
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfAarDashboardReportResponse result = null;
        try
        {
            result = _report.AarDashboard(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        requestV = request;
        Dictionary<string, string> dic = dicAarCreteriaV;
        DataTable data = new DataTable();
        data.Columns.Add("Gap");        
        data.Columns.Add("PastPeriod");
        data.Columns.Add("Row");
        data.Columns.Add("RowText");
        data.Columns.Add("Total");
        data.Columns.Add("ColorGap", typeof(System.Drawing.Color));

        data.Columns.Add("show_PastPeriod", typeof(bool));
        data.Columns.Add("show_Gap", typeof(bool));
        
        foreach (WebReferenceReports.AarTableData td in result.Value.TableRows)
        {
            DataRow row = data.NewRow();
            row["Gap"] = td.Gap;
            row["show_Gap"] = SetVisibleTR(td.Gap);
            row["PastPeriod"] = td.PastPeriod;
            row["show_PastPeriod"] = SetVisibleTR(td.PastPeriod);
            row["Row"] = td.Row.ToString();
            row["Total"] = td.Total;
            row["RowText"] = dic[td.Row.ToString()];
            row["ColorGap"] = System.Drawing.Color.FromName(td.GapColor.ToString());
            data.Rows.Add(row);
        }
        dataV = data;
        LoadChart(result.Value, ddl_Showme.SelectedItem.Text, request.ShowMe);
        SetTable(data);

    }
    bool SetVisibleTR(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
            return false;
        return true;
    }
    private void SetTable(DataTable data)
    {
        if (data.Rows.Count > 0)
            ShowPast = (bool)data.Rows[0]["show_PastPeriod"];
        else
            ShowPast = false;
        /*
        fixed
        var ie = (from x in data.AsEnumerable()
                  //   where x.Field<int>("Group") == _group
                  select new
                  {
                      Group = x.Field<int>("Group"),
                      show_objective = x.Field<bool>("show_objective"),
                      show_Percent = x.Field<bool>("show_Percent"),
                      show_PastPeriod = x.Field<bool>("show_PastPeriod"),
                      show_Gap = x.Field<bool>("show_Gap"),
                      GroupName = x.Field<string>("GroupName")
                  }
                                   ).Distinct();

        int index_visible = 2;
        if (ie.First().show_objective)
            index_visible++;
        if (ie.First().show_Percent)
            index_visible++;
        if (ie.First().show_PastPeriod)
            index_visible++;
        if (ie.First().show_Gap)
            index_visible++;

        _Repeater.DataSource = ie;
        _Repeater.DataBind();
        int i = 0;
        foreach (RepeaterItem item in _Repeater.Items)
        {
            //only in the first group
            HtmlTableRow _tr = (HtmlTableRow)item.FindControl("tr_head");
            _tr.Visible = (i++ == 0);
            //set colspan group
            HtmlTableCell _tc = (HtmlTableCell)item.FindControl("td_group");
            _tc.ColSpan = index_visible;

            int _group = int.Parse(((Label)item.FindControl("lbl_Group")).Text);
            Repeater repeater_in = (Repeater)item.FindControl("_Repeater_in");
            IEnumerable<DataRow> te = (from x in data.AsEnumerable()
                                       where x.Field<int>("Group") == _group
                                       select x);



            DataTable _data = (te.Count() == 0) ? null : te.CopyToDataTable();
            repeater_in.DataSource = _data;
            repeater_in.DataBind();
            int _index = 0;
            foreach (RepeaterItem item_in in repeater_in.Items)
            {
                HtmlTableRow tr = (HtmlTableRow)item_in.FindControl("tr_data");
                string _class = (_index++ % 2 == 0) ? "odd" : "even";
                tr.Attributes.Add("class", _class);
            }
        }
         * */
        _Repeater.DataSource = data;
        _Repeater.DataBind();
        UpdatePanel_table.Update();

    }
    protected void _Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow tr_data = (HtmlTableRow)e.Item.FindControl("tr_data");
        tr_data.Attributes["class"] = (e.Item.ItemType == ListItemType.Item) ? "odd" : "even";
    }
    #region  chart
    bool LoadChart(WebReferenceReports.AarDashboardReportResponse result, string caption, WebReferenceReports.AarCriteria ceriteria)
    {

    //    string SymboleValue = CriteriaSetting.GetSymbolCriteria(ceriteria, siteSetting.CurrencySymbol);
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + caption + @"' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='19' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");

        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar='=' ");

        sb.Append(@"bgColor='FFFFFF'>");


        int NumOf_X = result.CurrentGraph.Count();

        int modulu = result.CurrentGraph.Count() / 15;
        modulu++;

        sb.Append(@"<categories>");
        for (int i = 0; i < result.CurrentGraph.Count(); i++)
        {
            string num = (result.CurrentGraph[i].Y == -1) ? "0" : String.Format("{0:0.##}", result.CurrentGraph[i].Y);
            string _title = lbl_Current.Text + "= " + result.CurrentGraph[i].X + ", (" +
                num + ")";            
            if (result.PastGraph != null && result.PastGraph.Count() > i)
                _title += "\r\n" + lbl_PastPeriod.Text + "= " + result.PastGraph[i].X + ", (" +
                    num + ")";

            _title += "\r\n" + lbl_CurrentDot.Text;

            //***Dilute labels***//
            //       if ((i + 1) % modulu == 0)
            sb.Append(@"<category name='" + result.CurrentGraph[i].X + @"' toolText='" + _title + "'/>");
            //        else
            //             sb.Append(@"<category toolText='" + _title + "'/>");
        }
        sb.Append("</categories>");

        if (result.PastGraph != null)
        {
            
            sb.Append(@"<dataset seriesName='" + lbl_PastPeriod.Text + @"' color='808080'  anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'>");
            for (int i = 0; i < result.PastGraph.Count() && i < NumOf_X; i++)
            {
                string num = (result.PastGraph[i].Y == -1) ? "0" : String.Format("{0:0.##}", result.PastGraph[i].Y);
                sb.Append(@"<set value='" + num + "'/>");
            }
            sb.Append(@"</dataset>");
        }
       

        sb.Append(@"<dataset seriesName='" + lbl_Current.Text + @"' color='0080C0'  anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'>");
        for (int i = 0; i < result.CurrentGraph.Count(); i++)
        {
            string num = (result.CurrentGraph[i].Y == -1) ? "0" : String.Format("{0:0.##}", result.CurrentGraph[i].Y);
            sb.Append(@"<set value='" + num + "'/>");
        }
        sb.Append(@"</dataset>");

        sb.Append(@"</graph>");



        string path = ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) { return false; }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        return true;
    }
    protected void lb_row_click(object sender, EventArgs e)
    {
        WebReferenceReports.AarDashboardReportRequest request = requestV;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        LinkButton lb = (LinkButton)sender;
        string command = lb.CommandArgument;
        request.OnlyGraphs = true;
        request.ShowMe = GetCrireria(command);
        WebReferenceReports.ResultOfAarDashboardReportResponse result = null;
        try
        {
            result = _report.AarDashboard(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (ListItem li in ddl_Showme.Items)
        {
            li.Selected = (li.Value == command);
        }
        _UpdatePanel_Showme.Update();
        LoadChart(result.Value, lb.Text, request.ShowMe);

    }
    WebReferenceReports.AarCriteria GetCrireria(string name)
    {
        foreach (WebReferenceReports.AarCriteria crit in Enum.GetValues(typeof(WebReferenceReports.AarCriteria)))
        {
            if (crit.ToString() == name)
                return crit;
        }
        return WebReferenceReports.AarCriteria.AvgAttemptsForAcPress1;
    }
    #endregion
    protected string GetDivCurrentId
    {
        get { return FromToDatePicker_current.GetParentDivId(); }
    }
    protected string GetDivPastId
    {
        get { return FromToDatePicker_compare.GetParentDivId(); }
    }
    protected string GetDateFormat()
    {
        return ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat);
    }
    protected string GetFromCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxFrom().ClientID; }
    }
    protected string GetToCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxTo().ClientID; }
    }

    protected string GetFromPastId
    {
        get { return FromToDatePicker_compare.GetTextBoxFrom().ClientID; }
    }
    protected string GetToPastId
    {
        get { return FromToDatePicker_compare.GetTextBoxTo().ClientID; }
    }
    protected string GetInvalidDate
    {
        get { return Server.HtmlEncode(lbl_DateError.Text); }
    }
    private DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    private string PathV
    {
        get { return (ViewState["Path"] == null) ? string.Empty : (string)ViewState["Path"]; }
        set { ViewState["Path"] = value; }
    }
    private WebReferenceReports.AarDashboardReportRequest requestV
    {
        get { return (ViewState["_request"] == null) ? null : (WebReferenceReports.AarDashboardReportRequest)ViewState["_request"]; }
        set { ViewState["_request"] = value; }
    }
    private Dictionary<string, string> dicAarCreteriaV
    {
        get { return (Session["AarCreteria"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)Session["AarCreteria"]; }
        set { Session["AarCreteria"] = value; }
    }
    protected string Lbl_DatesChange
    {
        get { return lbl_DatesChange.Text; }
    }
    protected bool ShowPast { get; set; }

    
}