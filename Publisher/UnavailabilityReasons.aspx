﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" 
EnableEventValidation="false"
CodeFile="UnavailabilityReasons.aspx.cs" Inherits="Publisher_UnavailabilityReasons" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

 <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css"/>
 <script type="text/javascript">
     window.onload = appl_init;
     function appl_init() {
         var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
         pgRegMgr.add_beginRequest(BeginHandler);
         pgRegMgr.add_endRequest(EndHandler);         
     }
     function BeginHandler() {
         showDiv();
     }
     function EndHandler() {
         hideDiv();
     }
     function closePopup() {
         document.getElementById("<%# popUpaddUser.ClientID %>").className = "popModal_del popModal_del_hide";
         ClearPopUp();
         $find('_modal').hide();
     }
     function ClearPopUp() {
         var ddl_ShowToAdvertiser = document.getElementById("<%# ddl_ShowToAdvertiser.ClientID %>");
         var ddl_Group = document.getElementById("<%# ddl_Group.ClientID %>");
         document.getElementById("<%# txt_OutOfBalance.ClientID %>").value = "";
         ddl_ShowToAdvertiser.selectedIndex = 0;
         ddl_ShowToAdvertiser.removeAttribute("disabled");
         ddl_Group.selectedIndex = 0;
         ddl_Group.removeAttribute("disabled");
         document.getElementById("<%# txt_ID.ClientID %>").value = "";
         document.getElementById("<%# txt_Name.ClientID %>").value = "";
         document.getElementById("<%# hf_guid.ClientID %>").value = "";
         document.getElementById("<%# hf_OutOfBalance.ClientID %>").value = "";


         var validators = Page_Validators;
         for (var i = validators.length - 1; i > -1; i--) {
             validators[i].style.display = "none";
         }         

     }
     function openPopup() {
         document.getElementById("<%# popUpaddUser.ClientID %>").className = "popModal_del";
         ClearPopUp();
         document.getElementById("<%# txt_OutOfBalance.ClientID %>").value = "<%# YesNoV[eYesNo.No] %>";
         document.getElementById("<%# hf_OutOfBalance.ClientID %>").value = "<%# eYesNo.No.ToString() %>";
         $find('_modal').show();
     }
 
 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<uc1:Toolbox ID="Toolbox1" runat="server" />

<h5>
    <asp:Label ID="lblSubTitle" runat="server" Text="Unavailability reasons"></asp:Label>
</h5>
<div class="page-content minisite-content5">			
    <div id="form-analyticsseg">
	   
	    <asp:UpdatePanel ID="_updatePanelTable" runat="server" UpdateMode="Conditional">
	    <ContentTemplate>
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" ShowFooter="true" 
                CssClass="data-table"
                AllowPaging="True" 
                    onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                 <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                <FooterStyle CssClass="footer"  />
                
                <Columns>                     
                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                        <br />
                        <asp:CheckBox ID="cb_all" runat="server" >
                        </asp:CheckBox>                    
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="cb_choose" runat="server" />                    
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# lbl_ID.Text %>"></asp:Label>                     
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl_CodeId" runat="server" Text="<%# Bind('ID') %>"></asp:Label>
                        <asp:Label ID="lbl_GuidId" runat="server" Text="<%# Bind('GuidId') %>" Visible="false"></asp:Label> 
                    </ItemTemplate>                
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label4" runat="server" Text="<%# lbl_Name.Text %>"></asp:Label>                     
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl_ReasonName" runat="server" Text="<%# Bind('Name') %>"></asp:Label>                        
                    </ItemTemplate>                
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label5" runat="server" Text="<%# lbl_OutOfBalance.Text %>"></asp:Label>                     
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="_lbl_OutOfBalance" runat="server" Text="<%# Bind('OutOfBalance') %>"></asp:Label>                        
                        <asp:Label ID="lbl_OutOfBalance_YN" runat="server" Text="<%# Bind('OutOfBalanceYN') %>" Visible="false"></asp:Label>
                    </ItemTemplate>                
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label7" runat="server" Text="<%# lbl_ShowToAdvertiser.Text %>"></asp:Label>                     
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text="<%# Bind('ShowToAdvertiser') %>"></asp:Label>                        
                        <asp:Label ID="lbl_ShowToAdvertiser_YN" runat="server" Text="<%# Bind('ShowToAdvertiserYN') %>" Visible="false"></asp:Label>
                    </ItemTemplate>                
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label9" runat="server" Text="<%# lbl_Group.Text %>"></asp:Label>                     
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label10" runat="server" Text="<%# Bind('Group') %>"></asp:Label>                        
                        <asp:Label ID="lblGroup" runat="server" Text="<%# Bind('GroupInt') %>" Visible="false"></asp:Label>
                        
                    </ItemTemplate>                
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<div id="popUpaddUser" runat="server" sclass="content" style="max-height:450px;display:none;" class="popModal_del popModal_del_hide"  >
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
        <div class="content" >
            <div class="modaltit">
                <h2><asp:Label ID="lbl_PopupTitle" runat="server" Text="<%# lblSubTitle.Text %>"></asp:Label></h2>
            </div>
            <div>
    	    <center>    	    
                <table>
    	            <tr>
    	            <td>
                        <asp:Label ID="lblid" runat="server" CssClass="label" Text="<%# lbl_ID.Text %>"></asp:Label>   	        
    	           
    	                <asp:TextBox ID="txt_ID" CssClass="form-text" runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:HiddenField ID="hf_guid" runat="server" />
    	            </td>                
    	            </tr> 

                    <tr>
    	            <td>
                        <asp:Label ID="lblName" runat="server"  CssClass="label" Text="<%# lbl_Name.Text %>"></asp:Label>   	        
    	           
    	                <asp:TextBox ID="txt_Name" CssClass="form-text" runat="server"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Missing"
                         Display="Dynamic" ValidationGroup="EditUnavailability" ControlToValidate="txt_Name" 
                         CssClass="error-msg"></asp:RequiredFieldValidator>
    	            </td>                
    	            </tr> 
                    

                    <tr>
    	            <td>
                        <asp:Label ID="lblOutOfBalance" runat="server"  CssClass="label" Text="<%# lbl_OutOfBalance.Text %>"></asp:Label>   	        
    	            
                        <asp:TextBox ID="txt_OutOfBalance"  CssClass="form-text" runat="server"></asp:TextBox>
                        <asp:HiddenField ID="hf_OutOfBalance" runat="server" />
    	            </td>                
    	            </tr> 

                    <tr>
    	            <td>
                        <asp:Label ID="lblShowToAdvertiser" runat="server"  CssClass="label" Text="<%# lbl_ShowToAdvertiser.Text %>"></asp:Label>   	        
    	           
    	                <asp:DropDownList ID="ddl_ShowToAdvertiser"  CssClass="form-select" runat="server">
                        </asp:DropDownList>
    	            </td>                
    	            </tr> 

                    <tr>
    	            <td>
                        <asp:Label ID="_lbl_Group" runat="server"  CssClass="label" Text="<%# lbl_Group.Text %>"></asp:Label>   	        
    	           
    	                <asp:DropDownList ID="ddl_Group"  CssClass="form-select" runat="server">
                        </asp:DropDownList>
    	            </td>                
    	            </tr> 
                </table>
            </center>
            </div>  	    
            <div class="content2">
	            <table class="Table_Button">
                <tr>
                <td>
                    <div class="unavailabilty2">
                    <asp:Button ID="btn_Set" runat="server" Text="Set" ValidationGroup="EditUnavailability"
                     OnClick="btn_Set_Click" CssClass="btn2" />
        
               
                    <input id="btn_cancel" type="button" class="btn" value="Cancel" runat="server" onclick="javascript:closePopup();"/>
                    </div>
                </td>
                </tr>
                </table>
	        </div>
	    </div>
	<div class="bottom2"></div>        	
</div>

<cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btn_virtual"
    PopupControlID="popUpaddUser"
    BackgroundCssClass="modalBackground" 
              
    BehaviorID="_modal"               
    DropShadow="false">
</cc1:ModalPopupExtender>

<div style="display:none;">
    <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />                     
</div>

<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
<asp:Label ID="lbl_ID" runat="server" Text="ID" Visible="false"></asp:Label>
<asp:Label ID="lbl_Name" runat="server" Text="Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_OutOfBalance" runat="server" Text="Out of balance" Visible="false"></asp:Label>
<asp:Label ID="lbl_ShowToAdvertiser" runat="server" Text="Show to advertiser" Visible="false"></asp:Label>

<asp:Label ID="lbl_noreasultSearch" runat="server" Text="There are no results for this search" Visible="false"></asp:Label>
<asp:Label ID="lbl_CantDelete" runat="server" Text="You can not delete the reason 'out of balance'" Visible="false"></asp:Label>
<asp:Label ID="lbl_Delete" runat="server" Text="This values won't be available for new updates. In all the records that used this values it will still" Visible="false"></asp:Label>
<asp:Label ID="lbl_Group" runat="server" Text="Group" Visible="false"></asp:Label>
</asp:Content>

