﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SearchSupplierApp.aspx.cs" 
    Inherits="Publisher_SearchSupplierApp" EnableEventValidation="true" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asps" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    

<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
<link media="screen" rel="stylesheet" href="../jquery/colorbox.css" />
<link type="text/css" rel="Stylesheet" href="../scripts/jquery-ui-1.9.2.custom.dialog.min.css" />
 <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="../jquery/jquery.colorbox1.3.19.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui-1.9.2.custom.dialog.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript" src="../scripts/json2.js"></script>
    <script type="text/javascript">
        function init() {
            message = document.getElementById('<%#Hidden_IfToExite.ClientID%>').value;
            __pageLoad();
        }

        window.onload = init;
        function __pageLoad() {
            appl_init();
            //    txtInit();        
        }

        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);
        }
        function BeginHandler() {
            showDiv();
        }
        function EndHandler() {
            hideDiv();
        }
        function checkSearch() {
            return true;
        }

        function chkTextBox(event) {
            //     alert(event.keyCode);
            if (event.keyCode != 13)
                return true;
            run_report();
            return false;

        }
        function run_report() {

            <%# RunReport() %>;

        }
        function BlurTxtSearch() {
            document.getElementById("<%# txtSearch.ClientID %>").blur();
        }
        function SetCursorToTextEnd(textControlID) {
            var text = document.getElementById(textControlID);
            text.focus();
            if (text != null && text.value.length > 0) {
                if (text.createTextRange) {
                    var range = text.createTextRange();
                    range.moveStart('character', text.value.length);
                    range.collapse();
                    range.select();
                }
                else if (text.setSelectionRange) {
                    //	       text.focus();
                    var textLength = text.value.length;
                    text.setSelectionRange(textLength, textLength);

                }
            }
        }
        
        function active(_a, toActive) {
            var supplierId = $(_a).parents('tr').find('input[type="hidden"]').val();
            $(_a).hide();
            var $div_parent = $(_a).parents('div.div_ActiveInactive');
            $div_parent.find('div.div_active_loader').show();
            var _url = toActive ? "<%# ActiveSupplierUrl %>" : "<%# InactiveSupplierUrl %>";
            $.ajax({
                url: _url,
                data: "{ 'supplierId': '" + supplierId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    $div_parent.find('span.span_activeResponse').html(_data.IsSucceeded ? 'Success' : _data.FailedReason);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    $div_parent.find('div.div_active_loader').hide();
                }

            });
            return false;
        }
          
      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">

    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>

<div class="page-content minisite-content">
    <h2><asp:Label ID="lblTitleSearch" runat="server" Text="Search advertisers"></asp:Label></h2>
    <div class="clearfix">
	    <div class="form-search clearfix">
			             
                    <asp:Panel ID="_PanelNoReasult" CssClass="error-msg" Visible="false" runat="server">
                        <asp:Label ID="lbl_noreasultSearch" runat="server" Text="There is no result for this search"></asp:Label>
                    </asp:Panel> 
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-text" MaxLength="40" onkeydown="return chkTextBox(event);"></asp:TextBox>
					<span id="RegularExpression" class="error-msg" style="display:none;">*</span>
                    
                    <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="form-submit" OnClick="btnSearch_Click" OnClientClick="return checkSearch();" />								
        </div>
         <div class="clear"></div>
        <div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="form-field" runat="server" id="div_SupplierStatus">
				 <asp:Label ID="Label2" runat="server" Text="Supplier Status" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_SupplierStatus" CssClass="form-select" runat="server" ></asp:DropDownList>
			</div>  			
            <div class="form-field" runat="server" id="div_Device">
                <asp:Label ID="lbl_Device" runat="server" Text="Device" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_Device" CssClass="form-select" runat="server" ></asp:DropDownList>
            </div>
              <div class="form-field form-WidthDuration" runat="server" id="div_Category" style="top:0; margin-bottom:0;">
                <asp:Label ID="Label3" runat="server" Text="Category" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_Category" CssClass="form-select" runat="server" ></asp:DropDownList>
            </div>
		</div>
       
       
        <div class="clear"></div>
    <asps:UpdatePanel id="_UpdatePanel" runat="server"  UpdateMode="Conditional">                           
        <ContentTemplate> 
        <div class="status-label" runat="server" id="div_status">
           
                <div class="statusclient">
                    <asp:Image ID="Image3" runat="server" ImageUrl="../images/candidate.png" />
                </div>
                <div class="statusclient">
                    <asp:Label ID="lbl_orangeExplan" runat="server" Text="Registration" CssClass="Label_Icons statusclient"></asp:Label>                
                <div class="statusclient">&nbsp;&nbsp;&nbsp;|</div>
                </div>
                    
                <div class="statusclient">
                    <asp:Image ID="Image2" runat="server" ImageUrl="../images/available.png" />                
                </div>
                <div class="statusclient">
                    <asp:Label ID="lbl_greenExplan" runat="server" Text="Approved" CssClass="Label_Icons statusclient"></asp:Label>
                <div class="statusclient">&nbsp;&nbsp;&nbsp;|</div>
                </div>
                
                <div class="statusclient">
               	    <asp:Image ID="Image1" runat="server" ImageUrl="../images/unavailable.png" />
               	</div>
               	<div class="statusclient">
                    <asp:Label ID="lbl_Unavailable" runat="server" Text="Old pro" CssClass="Label_Icons statusclient"></asp:Label>
                <div class="statusclient">&nbsp;&nbsp;&nbsp;|</div>
                </div>
                
                 <div class="statusclient">
                    <asp:Image ID="Image4" runat="server" ImageUrl="../images/innactive.png" />               	
               	</div>
               	<div class="statusclient">
               	    <asp:Label ID="lbl_redExplan" runat="server" Text="Inactive" CssClass="Label_Icons statusclient"></asp:Label>                
                <div class="statusclient">&nbsp;&nbsp;&nbsp;|</div>
                </div>

                </div>
	    </div>
   <div class="clear"></div>
        <div>
				 <asps:UpdatePanel id="_UpdatePanelTotalResult" runat="server"  UpdateMode="Conditional">                           
                    <ContentTemplate> 	
					<asp:Panel ID="PanelResults" runat="server" CssClass="results4" Visible="false"> 
                        <asp:Label ID="lbl_Count" runat="server"  CssClass="aboutResults"></asp:Label>
                        <asp:Label ID="lbl_Results" runat="server" Text="Results that match your search criteria"></asp:Label>
                    </asp:Panel>   
                </ContentTemplate>
            </asps:UpdatePanel>
                
        </div>
            <div class="clear"></div>
        <div class="table2">
                    <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false" OnRowDataBound="_GridView_RowDataBound">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="Name">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="Name"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:LinkButton ID="LinkButton_name" runat="server" OnClick="btn_Edit_click" Text="<%# Bind('Name') %>" CommandArgument="<%# Bind('Name') %>" 
                               ></asp:LinkButton>
                            <asp:Label ID="lbl_SupplierId" runat="server" Visible="false" Text="<%# Bind('supplierid') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="FullName">
						<HeaderTemplate>
							<asp:Label ID="Label101" runat="server" Text="Full Name"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label112" runat="server" Text="<%# Bind('FullName') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Phone">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="Phone"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label11" runat="server" Text="<%# Bind('Phone') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>					

					<asp:TemplateField SortExpression="Email">
						<HeaderTemplate>
							<asp:Label ID="Label14" runat="server" Text="Email"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lbl_email" runat="server" Text="<%# Bind('Email') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>                   

					<asp:TemplateField SortExpression="Device">
						<HeaderTemplate>
							<asp:Label ID="Label16" runat="server" Text="Device"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label17" runat="server" Text="<%# Bind('Device') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="Categories">
						<HeaderTemplate>
							<asp:Label ID="Label18" runat="server" Text="Categories"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label19" runat="server" Text="<%# Bind('Categories') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>	
                    
                     <asp:TemplateField SortExpression="SupplierStatus">
						<HeaderTemplate>
							<asp:Label ID="Label69" runat="server" Text="Supplier Status"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                             <div runat="server" id="div_ActiveInactive" class="div_ActiveInactive">
                               
                                <a id="a_active" runat="server" href="javascript:void(0);">
                                    <asp:Image ID="img_status" runat="server"  /> 
                                </a>
                                 <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="<%# Bind('s_image') %>" OnClientClick="<%# Bind('SupplierStatusClick') %>"/>
                                 
                                <div style="text-align:center; display:none;" class="div_active_loader">
                                    <asp:Image ID="img_relaunch" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                </div>
                                <span class="span_activeResponse" style="color:red;"></span>
                                 <asp:HiddenField ID="hf_supplierId" runat="server" Value="<%# Bind('SupplierId') %>" />
                            </div>
                            						
						</ItemTemplate>
					</asp:TemplateField>	
                    
                     <asp:TemplateField SortExpression="InactiveReason">
						<HeaderTemplate>
							<asp:Label ID="Label180" runat="server" Text="Inactive Reason"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label190" runat="server" Text="<%# Bind('InactiveReason') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="UserId (Uxcam)">
						<HeaderTemplate>
							<asp:Label ID="Label181" runat="server" Text="UserId"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label191" runat="server" Text="<%# Bind('UserId') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>			

				</Columns>    
				</asp:GridView>
                    
               </div>                      
            <uc1:TablePaging ID="TablePaging1" runat="server" />
        </ContentTemplate>
    </asps:UpdatePanel>		
</div>
</div>
    </div>
<div id="popUpStatusReason" runat="server" style="display:none;" class="popModal_del popModal_del_hide"  >
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
        <div class="content" >
            <div class="modaltit">
            <h2><asp:Label ID="lbl_InactiveReason" runat="server"  Text="Inactive reason"></asp:Label></h2>
            </div>
            <div class="form-field">
                <asp:Label ID="lbl_selectReason" runat="server" CssClass="label4" Text="Select reason"></asp:Label>
                <asp:DropDownList ID="ddl_InactiveReasons" CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            <div class="clear"><!-- --></div>
            <div class="statusbtn">
                <asp:Button ID="btn_InactiveReason" runat="server" Text="Set" CssClass="CreateReportSubmit2"/>
                <asp:HiddenField ID="hf_AccountId" runat="server" />
            </div>
            <div class="clear"><!-- --></div>
        </div>
    <div class="bottom2"></div>
</div>
    
<cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btn_virtual"
    PopupControlID="popUpStatusReason"
    BackgroundCssClass="modalBackground" 
    BehaviorID="_modal"               
    DropShadow="false"
    >
</cc1:ModalPopupExtender>
<div style="display:none;">
   <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />       
</div>

<asp:Label ID="lbl_AddressDetails" runat="server" Text="Address Details" Visible="false"></asp:Label>
<asp:Label ID="lbl_ContactDetails" runat="server" Text="Contact Details" Visible="false"></asp:Label>

<asp:HiddenField ID="HiddenIsSuspend" runat="server" Value="Suspend" />
<asp:HiddenField ID="HiddenSupplierIsGoing" runat="server" Value="The following operation will" />
<asp:HiddenField ID="HiddenSupplierIsGoing_end" runat="server" Value="the supplier" />
<asp:HiddenField ID="HiddenSuspendList" runat="server" Value="Active#1;Suspend#2" />
<asp:HiddenField ID="Hidden_IfToExite" runat="server" Value="Are you sure you want to exit? Unsaved data will be lost!" />
<asp:HiddenField ID="hf_WaterMarkSearch" runat="server" Value="Type Advertiser, company Name, Phone, or Advertiser's Number" />
    
<asp:HiddenField ID="hf_ActivateAccount" runat="server" Value="This action will activate this account - continue?" />


<div id="_buttomSuspend">
    <asp:Label ID="lbl_suspend" runat="server" Visible="false" Text="Suspend"></asp:Label>
    <asp:Label ID="lbl_active" runat="server" Visible="false" Text="Active"></asp:Label>
</div>
<div style="display:none;">
    <asp:Button ID="btn_virtual_run" runat="server" Text="Button" style="display:none;" onfocus="this.blur();" />
</div>
<asp:Label ID="lbl_Missing" runat="server" Visible="false" Text="Missing"></asp:Label>
<asp:Label ID="lbl_InvalidPhoneNumber" runat="server" Visible="false" Text="Invalid phone number"></asp:Label>
<asp:Label ID="lbl_InvalidBizId" runat="server" Visible="false" Text="Invalid Biz ID"></asp:Label>


<asp:Label ID="lbl_GoldenNumberAdvertiser" runat="server" Visible="false" Text="Golden Number Advertiser"></asp:Label>
<asp:Label ID="lbl_SelectVirtualNumber" runat="server" Visible="false" Text="Select Virtual Number"></asp:Label>

<asp:Label ID="lbl_OK" runat="server" Visible="false" Text="OK"></asp:Label>
<asp:Label ID="lbl_Cancel" runat="server" Visible="false" Text="Cancel"></asp:Label>


   <asp:Label ID="lbl_UpdateFaild" runat="server" Visible="false" Text="Update faild"></asp:Label>
</asp:Content>

