﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ClipCallFunnelSummaryReport.aspx.cs" Inherits="Publisher_ClipCallFunnelSummaryReport" %>


<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
      <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function _CreateExcel() {
            var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"AarCallCenterReport"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
       
        function OpenIframe(_path, winName) {

            var _leadWin = window.open(_path, winName, "width=1200, height=650,scrollbars=1");
            _leadWin.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="Date-field" runat="server" id="div_Date">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  			
		</div>
      
		<div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>        
        <div id="Table_Report"  runat="server" >  
				<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="date">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="Date"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblBroker" runat="server" Text='<%# Bind("date") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="caseNumber">
						<HeaderTemplate>
							<asp:Label ID="Label101" runat="server" Text="Case number"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <a id="a_Case" runat="server" href='<%# Bind("CaseScript") %>'>
			                    <asp:Label ID="Label112" runat="server" Text='<%# Bind("CaseNumber") %>'></asp:Label>
						    </a>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="category">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="Category"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label11" runat="server" Text='<%# Bind("category") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="videoLink">
						<HeaderTemplate>
							<asp:Label ID="Label12" runat="server" Text="Video link"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl='<%# Bind("videoLink") %>'>Video link</asp:HyperLink>							
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="videoDuration">
						<HeaderTemplate>
							<asp:Label ID="Label14" runat="server" Text="Video Duration"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label15" runat="server" Text='<%# Bind("videoDuration") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="region">
						<HeaderTemplate>
							<asp:Label ID="Label16" runat="server" Text="Region"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label17" runat="server" Text='<%# Bind("region") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="customerPhone">
						<HeaderTemplate>
							<asp:Label ID="Label196" runat="server" Text="Customer phone"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label197" runat="server" Text='<%# Bind("customerPhone") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="SMS_Sent">
						<HeaderTemplate>
							<asp:Label ID="Label18" runat="server" Text="SMS sent"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label19" runat="server" Text='<%# Bind("SMS_Sent") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="LP_Visits">
						<HeaderTemplate>
							<asp:Label ID="Label20" runat="server" Text="LP Visits"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label21" runat="server" Text='<%# Bind("LP_Visits") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="videoViews">
						<HeaderTemplate>
							<asp:Label ID="Label22" runat="server" Text="Video Views"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label23" runat="server" Text='<%# Bind("videoViews") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="connection">
						<HeaderTemplate>
							<asp:Label ID="Label24" runat="server" Text="Connections"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label25" runat="server" Text='<%# Bind("connection") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="P5">
						<HeaderTemplate>
							<asp:Label ID="Label44" runat="server" Text="P5"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:Label ID="Label45" runat="server" Text='<%# Bind("P5") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="P4">
						<HeaderTemplate>
							<asp:Label ID="Label54" runat="server" Text="P4"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:Label ID="Label55" runat="server" Text='<%# Bind("P4") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="P3">
						<HeaderTemplate>
							<asp:Label ID="Label64" runat="server" Text="P3"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:Label ID="Label65" runat="server" Text='<%# Bind("P3") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="P2">
						<HeaderTemplate>
							<asp:Label ID="Label74" runat="server" Text="P2"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:Label ID="Label75" runat="server" Text='<%# Bind("P2") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="P1">
						<HeaderTemplate>
							<asp:Label ID="Label84" runat="server" Text="P1"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:Label ID="Label85" runat="server" Text='<%# Bind("P1") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>    
				</asp:GridView>
			</div>
        </div>
    </div>
    <asp:Label ID="lbl_ClipCallFunnelSummaryReport" runat="server" Text="ClipCall Funnel Summary Report" Visible="false"></asp:Label>
</asp:Content>
