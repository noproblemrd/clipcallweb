﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_SubscriptionReport : PageSetting
{
    const string RUN_REPORT = "RUN_REPORT";
    const string PAGE_TITLE = "Subscription Report";
    //   const int DAYS_INTERVAL = 30;
    private static readonly DateTime FROM_DATE_DEFAULT = new DateTime(2016, 1, 1);
    public const string STRIPE_SUBSCRIPTION_URL = @"subscriptions/";
    protected readonly string EXCEL_NAME = "ClipCallSubscriptionReport";


    protected readonly string SessionTableName = "data_ClipCallSubscriptionReport";
    protected void Page_Load(object sender, EventArgs e)
    {
         FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        ScriptManager1.RegisterAsyncPostBackControl(btn_virtual_run);
        if (!IsPostBack)
        {

            SetDateInterval();
            ExecRequestReport();
            //      Table_Report.Visible = true;
        }
        else
        {
            if (Request["__EVENTARGUMENT"] == RUN_REPORT)
                ExecRequestReport();
        }
        SetToolbox();
        Header.DataBind();    
    }
    protected override void Render(HtmlTextWriter writer)
    {

        Page.ClientScript.RegisterForEventValidation(btn_virtual_run.UniqueID, RUN_REPORT);
        base.Render(writer);
    }
    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecRequestReport();
    }
    protected void ExecRequestReport()
    {
        Clearreports();

        DateTime _to = FromToDate1.GetDateTo;
        DateTime _from = FromToDate1.GetDateFrom;

        ClipCallReport.SubscriptionReportRequest _request = new ClipCallReport.SubscriptionReportRequest();
        _request.From = _from;
        _request.To = _to;
        _request.Text = txtSearch.Text;
        ClipCallReport.ResultOfListOfSubscriptionReportResponse result = null;

        ClipCallReport.ClipCallReport _report = WebServiceConfig.GetClipCallReportReference(this);
        try
        {
            result = _report.GetPrimeSubscriptions(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        DataTable dt = new DataTable();
        dt.Columns.Add("CreatedOn");
        dt.Columns.Add("CustomerName");
        dt.Columns.Add("CustomerEmail");
        dt.Columns.Add("CustomerPhone");
        dt.Columns.Add("PrimeCollection");
        dt.Columns.Add("Transactions");
        dt.Columns.Add("TotalPlanDiscount");
        dt.Columns.Add("SubsctiptionLink");
        dt.Columns.Add("HasSubsctiptionLink", typeof(bool));
 //       dt.Columns.Add("IsActive", typeof(bool));
        dt.Columns.Add("IsActiveUrl");
        dt.Columns.Add("CanceledAt");
        dt.Columns.Add("OriginSkin");
        dt.Columns.Add("Origin");
        dt.Columns.Add("RequestReport");

        foreach (ClipCallReport.SubscriptionReportResponse data in result.Value)
        {
            DataRow row = dt.NewRow();
            row["CreatedOn"] = string.Format(siteSetting.DateTimeFormat, data.CreatedOn);
            row["CustomerName"] = data.CustomerName;
            row["CustomerEmail"] = data.CustomerEmail;
            row["CustomerPhone"] = data.CustomerPhone;
            row["PrimeCollection"] = "$" + string.Format(GetNumberFormat, data.Amount);
            row["Transactions"] = data.Payments;
            row["TotalPlanDiscount"] = "$" + string.Format(GetNumberFormat, data.TotalPlanDiscount);
            if (!string.IsNullOrEmpty(data.SubscriptionStripeId))
            {
                row["HasSubsctiptionLink"] = true;
                row["SubsctiptionLink"] = PpcSite.GetCurrent().StripeBaseUrl + STRIPE_SUBSCRIPTION_URL + data.SubscriptionStripeId;
            }
            else
                row["HasSubsctiptionLink"] = false;

            row["IsActiveUrl"] = data.IsActive ? V_ICON : X_ICON;

            row["CanceledAt"] = data.CanceledAt == DateTime.MinValue ? string.Empty : string.Format(siteSetting.DateTimeFormat, data.CanceledAt);
            row["OriginSkin"] = data.SkinOrigin.ToString();
            row["Origin"] = data.Origin.ToString();
            row["RequestReport"] = string.Format(Utilities.CUSTOMER_REQUEST_REPORT_URL, data.CustomerId);


            dt.Rows.Add(row);

           
        }
        dataS = dt;

        //       Table_Report.Visible = true;
        lbl_RecordMached.Text = dt.Rows.Count + " " + ToolboxReport1.RecordMached;
        _gv.DataSource = dt;
        _gv.DataBind();
        _UpdatePanel.Update();
    }
    protected string RunReport()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(btn_virtual_run, RUN_REPORT);
        myPostBackOptions.PerformValidation = false;
        //     return ScriptManager.g
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
        //   WebReferenceSite.FundsStatus
    }
    void Clearreports()
    {
        _gv.DataSource = null;
        _gv.DataBind();
        _UpdatePanel.Update();
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(PAGE_TITLE);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(FROM_DATE_DEFAULT);
    }
    protected DataTable dataS
    {
        get { return Session[SessionTableName] == null ? new DataTable() : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
}