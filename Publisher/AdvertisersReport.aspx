﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AdvertisersReport.aspx.cs" Inherits="Publisher_AdvertisersReport" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link href="../FromToDate.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../general.js" ></script>
<script  type="text/javascript" src="../Calender/_Calender.js"></script>
<link href="../Calender/CalenderDiv.css" rel="stylesheet" type="text/css" />
<link href="IncompleteRegistration.css" rel="stylesheet" type="text/css"/>
<link href="../UpdateProgress/updateProgress.css" type="text/css" rel="stylesheet" />

<link media="screen" rel="stylesheet" href="../jquery/colorbox.css" />
<script src="../jquery/jquery.min.js" type="text/javascript" ></script>
<script src="../jquery/jquery.colorbox.js" type="text/javascript" ></script>	


<script type="text/javascript" >
    window.onload = appl_init;
    var message;
   
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
        init();
    }
    function BeginHandler()
    {
        
        showDiv();
    }
    function EndHandler() {
        hideDiv();  
    }
    
    function init()
    {        
        message=document.getElementById('<%#Hidden_IfToExite.ClientID%>').value;
    }
    $(function () {
        $('#<%# ddl_Status.ClientID %>').change(function () {
            if (this.value == '<%# StatusToShow %>')
                $('#<%# div_CompletedStep.ClientID %>').show();
            else
                $('#<%# div_CompletedStep.ClientID %>').hide();

        });
    });


</script> 

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ></cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  
<div class="page-content minisite-content2">
	
					
	<div id="form-analytics">	
		
		    <div class="form-field form-status">
                <asp:Label ID="lbl_ChooseStatus" CssClass="label" runat="server" Text="Status"></asp:Label>                                    
                <asp:DropDownList ID="ddl_Status" CssClass="form-select" runat="server"></asp:DropDownList>			
			</div>
			
			<div class="form-field form-balance">
			    <asp:Label ID="lbl_maxBalance" CssClass="label" runat="server" Text="Max balance"></asp:Label> 
                <asp:TextBox ID="txt_maxBalance" CssClass="form-text" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator_maxBalance" CssClass="error-msg" runat="server" ErrorMessage="Invalid number" Display="Dynamic"
                ValidationGroup="AdReport" ValidationExpression="^\d+\.?\d*?$" ControlToValidate="txt_maxBalance"></asp:RegularExpressionValidator>
			
			</div>
            
           <div class="form-field">
                <asp:Label ID="lbl_IsFromAar" CssClass="label" runat="server" Text="Is from AAR"></asp:Label>                                    
                <asp:DropDownList ID="ddl_IsFromAar" CssClass="form-select" runat="server"></asp:DropDownList>			
            </div>
            <div class="clear"></div>
			<div class="advertiser">
                
                <uc1:FromToDate ID="FromToDate1" runat="server" />
            </div>   
            <div class="form-field form-fieldAdd">
                <asp:Label ID="lbl_WebUserRegistrant" CssClass="label" runat="server" Text="Web user registrant"></asp:Label>                                    
                <asp:DropDownList ID="ddl_WebUserRegistrant" CssClass="form-select" runat="server"></asp:DropDownList>			
            </div>  
            <div class="form-field form-fieldAdd" style="left: 510px;display:none;" runat="server" id="div_CompletedStep">
                <asp:Label ID="lbl_CompletedStep" CssClass="label" runat="server" Text="Completed step and left"></asp:Label>                                    
                <asp:DropDownList ID="ddl_CompletedStep" CssClass="form-select" runat="server"></asp:DropDownList>			
            </div>                                         
             
		
	</div>
	
	<div id="list">                    
        <asp:UpdatePanel runat="server" ID="_uPanel" UpdateMode="Conditional">
            <ContentTemplate>
               <div class="results6"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
               <div class="table">
                <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                CssClass="Grid-table">
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <PagerStyle CssClass="pager" HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateField SortExpression="Number">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="<%# lbl_accountnumber.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lb_Name" runat="server" Text="<% # Bind('Number')%>" OnClick="lb_Name_click" CommandArgument="<%# Bind('SupplierId') %>"></asp:LinkButton>                      
                            </ItemTemplate>
                        </asp:TemplateField>
                    
                        <asp:TemplateField SortExpression="Name">
                            <HeaderTemplate>
                                <asp:Label ID="Label200" runat="server" Text="<%# lbl_name.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text="<%# Bind('Name') %>"></asp:Label>               
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField SortExpression="ContactName">
                            <HeaderTemplate>
                                <asp:Label ID="Label220" runat="server" Text="<%# lbl_contactName.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblContactName" runat="server" Text="<%# Bind('ContactName') %>"></asp:Label>               
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="Phone">
                            <HeaderTemplate>
                                <asp:Label ID="Label7" runat="server" Text="<%# lbl_MainPhone.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text="<%# Bind('Phone') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="Email">
                            <HeaderTemplate>
                                <asp:Label ID="Label9" runat="server" Text="<%# lbl_EmailAddress.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Text="<%# Bind('Email') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                                                
                        <asp:TemplateField SortExpression="StageInRegistration">
                            <HeaderTemplate>
                                <asp:Label ID="Label11" runat="server" Text="<%# lbl_StageInRegistration.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text="<%# Bind('StageInRegistration') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="Balance">
                            <HeaderTemplate>
                                <asp:Label ID="Label14" runat="server" Text="<%# lbl_balance.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label15" runat="server" Text="<%# Bind('Balance') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="CreatonOn">
                            <HeaderTemplate>
                                <asp:Label ID="Label5" runat="server" Text="<%# lbl_CreatOn.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text="<%# Bind('CreatonOn') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField SortExpression="Status">
                            <HeaderTemplate>
                                <asp:Label ID="Labe25" runat="server" Text="<%# lbl_status.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label26" runat="server" Text="<%# Bind('Status') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="StageInTrialRegistration">
                            <HeaderTemplate>
                                <asp:Label ID="Label27" runat="server" Text="<%# lbl_StageInTrialRegistration.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label28" runat="server" Text="<%# Bind('StageInTrialRegistration') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField SortExpression="IsFromAar">
                            <HeaderTemplate>
                                <asp:Label ID="Labe29" runat="server" Text="<%# lblIsFromAar.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label30" runat="server" Text="<%# Bind('IsFromAar') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="TimeZone">
                            <HeaderTemplate>
                                <asp:Label ID="Labe31" runat="server" Text="<%# lbl_TimeZone.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label32" runat="server" Text="<%# Bind('TimeZone') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                                                                
                        
                    </Columns>                            
                </asp:GridView>
                <uc1:TablePaging ID="TablePaging1" runat="server" />
               </div>  
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>   

<asp:Label ID="lbl_accountnumber" runat="server" Text="Account number" Visible="false"></asp:Label>
<asp:Label ID="lbl_name" runat="server" Text="Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_CreatOn" runat="server" Text="Create on" Visible="false"></asp:Label>
<asp:Label ID="lbl_contactName" runat="server" Text="Contact name" Visible="false"></asp:Label>
<asp:Label ID="lbl_status" runat="server" Text="Status" Visible="false"></asp:Label>
<asp:Label ID="lbl_MainPhone" runat="server" Text="Main Phone" Visible="false"></asp:Label>
<asp:Label ID="lbl_EmailAddress" runat="server" Text="Email Address" Visible="false"></asp:Label>
<asp:Label ID="lbl_StageInRegistration" runat="server" Text="Step" Visible="false"></asp:Label>
<asp:Label ID="lbl_balance" runat="server" Text="Balance" Visible="false"></asp:Label>
<asp:Label ID="lbl_StageInTrialRegistration" runat="server" Text="Stage in trial" Visible="false"></asp:Label>
<asp:Label ID="lblIsFromAar" runat="server" Text="Is AAR" Visible="false"></asp:Label>
<asp:Label ID="lbl_TimeZone" runat="server" Text="Time zone" Visible="false"></asp:Label>

	
<asp:Label ID="lblTitleAdvertisersReport" runat="server" Visible="false" Text="Advertisers Report"></asp:Label>
<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>

<asp:HiddenField ID="Hidden_IfToExite" runat="server" Value="Are you sure you want to exit? Unsaved data will be lost!" />
</asp:Content>

