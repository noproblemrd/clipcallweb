﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ProjectView : PublisherPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(lb_notes);
        if (!IsPostBack)
        {
            Guid incidentAccountId;
            if (!Guid.TryParse(Request.QueryString["ia"], out incidentAccountId))
                incidentAccountId = default(Guid);
            LoadDetails(incidentAccountId);
        }
        Header.DataBind();
    }

    private void LoadDetails(Guid incidentAccountId)
    {
        ClipCallReport.PriorityProjectFullData result = incidentAccountId == default(Guid) ? GetData() : GetData(incidentAccountId);
        if (result == null)
            return;
        // Caption
        hf_followUpDate.Value = result.IncidentAccountId.ToString();
        lbl_projectID.Text = result.ProjectId;
        lbl_reasonFollowUp.Text = result.eventType.ToString();
        //Project information
        lbl_DatePublished.Text = GetDateTimeForClient(result.PublishedAt);
        lbl_ProjectsCategory.Text = result.Category;
        lbl_projectTitle.Text = result.CustomerProjectTitle;
        lbl_projectStatus.Text = result.ProjectStatus;
        hl_VideoUrl.Text = "Video_" + result.ProjectId;
        GenerateVideoLink gvl = new GenerateVideoLink(result.Videourl, null);
        hl_VideoUrl.NavigateUrl = gvl.GenerateFullUrl();
        //Customer Information
        lbl_customerName.Text = result.CustomerName;
        lbl_customerId.Text = result.CustomerId.ToString();
        lbl_customerAddress.Text = result.CustomerAddress;
        lbl_customerPhone.Text = result.CustomerPhone;
        lbl_customerPlatform.Text = result.CustomerOS;
        lbl_customerAppVersion.Text = result.CustomerAppVersion;
        lbl_customerMembership.Text = result.IsPrime ? "Prime" : "None";
        //Pro marked for follow-up
        lbl_supplierName.Text = result.SupplierName;
        lbl_supplierId.Text = result.ProUserId.ToString();
        lbl_supplierAddress.Text = result.ProAddress;
        lbl_supplierPhone.Text = result.ProPhone;
        lbl_supplierPlatform.Text = result.ProOS;
        lbl_supplierappVersion.Text = result.ProAppVersion;
        //Pro other project table

        DataTable otherProTable = new DataTable();
        otherProTable.Columns.Add("ProjectId");
        otherProTable.Columns.Add("IncidentAccountId");
        otherProTable.Columns.Add("CustomerName");
        otherProTable.Columns.Add("DatePublisher");
        otherProTable.Columns.Add("ProjectStatus");
        otherProTable.Columns.Add("ProStatus");
        otherProTable.Columns.Add("eventType");
        otherProTable.Columns.Add("ProjectView");
        foreach (ClipCallReport.OtherProProject project in result.otherProProjects)
        {
            DataRow row = otherProTable.NewRow();
            row["ProjectId"] = project.ProjectId;
            row["IncidentAccountId"] = project.IncidentAccountId;
            row["CustomerName"] = project.CustomerName;
            row["DatePublisher"] = GetDateTimeForClient(project.DatePublisher);
            row["ProjectStatus"] = project.ProjectStatus;
            row["ProStatus"] = project.ProStatusOnProject;
            row["eventType"] = project.eventType.HasValue ? project.eventType.ToString() : null;
            row["ProjectView"] = ProjectViewUrl + project.IncidentAccountId;
            otherProTable.Rows.Add(row);
        }
        gv_ProOtherProjects.Caption = string.Format("Other Projects for the Pro");
        gv_ProOtherProjects.DataSource = otherProTable;
        gv_ProOtherProjects.DataBind();

        //Customer projects

        DataTable customersProjectsTable = new DataTable();
        customersProjectsTable.Columns.Add("ProjectId");
     //   customersProjectsTable.Columns.Add("IncidentAccountId");
     //   customersProjectsTable.Columns.Add("CustomerName");
        customersProjectsTable.Columns.Add("DatePublisher");
        customersProjectsTable.Columns.Add("ProjectStatus");
    //    customersProjectsTable.Columns.Add("ProStatus");
        customersProjectsTable.Columns.Add("eventType");
   //     customersProjectsTable.Columns.Add("ProjectView");
        foreach (ClipCallReport.CustomersProjects project in result.customersProjects)
        {
            DataRow row = customersProjectsTable.NewRow();
            row["ProjectId"] = project.ProjectId;
  //          row["IncidentAccountId"] = project.IncidentAccountId;
 //           row["CustomerName"] = project.CustomerName;
            row["DatePublisher"] = GetDateTimeForClient(project.DatePublisher);
            row["ProjectStatus"] = project.ProjectStatus;
            //        row["ProStatus"] = project.ProStatusOnProject;
            row["eventType"] = project.eventType.HasValue ? project.eventType.ToString() : null;
    //        row["ProjectView"] = ProjectViewUrl + project.IncidentAccountId;
            customersProjectsTable.Rows.Add(row);
        }
        gv_CustomersProjects.Caption = string.Format("Customer's projects");
        gv_CustomersProjects.DataSource = customersProjectsTable;
        gv_CustomersProjects.DataBind();

        // other pro

        DataTable proOnTheProject = new DataTable();
        proOnTheProject.Columns.Add("ProjectId");
        proOnTheProject.Columns.Add("IncidentAccountId");
        proOnTheProject.Columns.Add("ProjectView");
        proOnTheProject.Columns.Add("ProName");
        proOnTheProject.Columns.Add("BookedProjects");
        proOnTheProject.Columns.Add("Status");
        proOnTheProject.Columns.Add("eventType");
        
        foreach (ClipCallReport.ProsOnProject project in result.prosOnProjects)
        {
            DataRow row = proOnTheProject.NewRow();
            row["ProjectId"] = project.ProjectId;
            row["IncidentAccountId"] = project.IncidentAccountId;
            row["ProjectView"] = ProjectViewUrl + project.IncidentAccountId;
            row["ProName"] = project.ProName;
            row["BookedProjects"] = project.BookedProjects;
            row["Status"] = project.ProStatusOnProject;
            row["ProStatus"] = project.ProStatusOnProject;
            row["eventType"] = project.eventType.HasValue ? project.eventType.ToString() : null;
            
            otherProTable.Rows.Add(row);
        }
        gv_CustomersProjects.Caption = string.Format("Pros on project");
        gv_proOnTheProject.DataSource = proOnTheProject;
        gv_proOnTheProject.DataBind();

        //ScheduledDate
        if (result.FollowUp != DateTime.MinValue)
            txt_followUpDate.Text = GetDateTimeForClient(result.FollowUp);

        DataTable notesTable = new DataTable();
        notesTable.Columns.Add("CreatedOn");
        notesTable.Columns.Add("Note");
        foreach (ClipCallReport.PriorityProjectNotes note in result.Notes)
        {
            DataRow row = notesTable.NewRow();
            row["CreatedOn"] = GetDateTimeForClient(note.CreatedOn);
            row["Note"] = note.Note;
            notesTable.Rows.Add(note);
        }
        dl_notes.DataSource = notesTable;
        dl_notes.DataBind();
    }
    private ClipCallReport.PriorityProjectFullData GetData(Guid incidentAccountId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfPriorityProjectFullData result = null;
        try
        {
            result = report.GetPriorityProjectData(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return null;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return null;
        }
        return result.Value;
    }
    private ClipCallReport.PriorityProjectFullData GetData()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfPriorityProjectFullData result = null;
        try
        {
            result = report.GetNextPriorityProjectData();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return null;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return null;
        }
        return result.Value;
    }
    void ClosePopupByError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseError", "CloseMeError();", true);
    }
    protected void lb_notes_Click(object sender, EventArgs e)
    {
        string note = txt_note.Text.Trim();
        if (string.IsNullOrEmpty(note))
            return;
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.Result result = null;
        try
        {
            result = report.AddPriorityProjectNote(IncidentAccountIdV, note);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
    }

    protected void lb_next_Click(object sender, EventArgs e)
    {
        LoadDetails(default(Guid));
    }
    protected string GetSetFollowUpDateService
    {
        get
        {
            return ResolveUrl("~/Publisher/ProjectView.aspx/setfollowup"); 
        }
    }
    [WebMethod(true, MessageName ="setfollowup")]
    public static string SetFollowUpDate(Guid incidentAccountId, string date_time)
    {
        SiteSetting site = SiteSetting.GetSiteSetting(HttpContext.Current.Session);
        UserMangement user = UserMangement.GetUserObject(HttpContext.Current.Session);
        DateTime date = user.GetDateTimeFromClient(date_time, site.DateTimeFormatEmpty);
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.Result result = null;
        try
        {
            result = report.SetFollowUpDate(incidentAccountId, date);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { isSucceed = false });
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { isSucceed = false });
        }
        return Newtonsoft.Json.JsonConvert.SerializeObject(new { isSucceed = true });
    }
    protected Guid IncidentAccountIdV
    {
        get { return ViewState["IncidentAccountId"] == null ? default(Guid) : (Guid)ViewState["IncidentAccountId"]; }
        set { ViewState["IncidentAccountId"] = value; }
    }


    
}