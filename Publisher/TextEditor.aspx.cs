using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FreeTextBoxControls;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Collections.Generic;

public partial class Publisher_TextEditor : PageSetting
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            ToolbarStyle.DataSource = Enum.GetNames(typeof(ToolbarStyleConfiguration));
            ToolbarStyle.DataBind();

            FreeTextBoxControls.Support.ResourceManager rm = new FreeTextBoxControls.Support.ResourceManager();
            NameValueCollection languages = rm.GetSupportedLanguages();

            foreach (string key in languages)
            {
                Language.Items.Add(new ListItem(key, languages[key]));
            }
           
            LoadTextLogin();

            ToolbarStyle.SelectedIndex = 1;
            FreeTextBox1.ToolbarStyleConfiguration = (ToolbarStyleConfiguration)Enum.Parse(typeof(ToolbarStyleConfiguration), ToolbarStyle.SelectedValue);
        }
    }
    
    private void LoadTextLogin()
    {
        FreeTextBox1.Width = new Unit(140);
        FreeTextBox1.Text = DBConnection.GetTextLogin(siteSetting.GetSiteID);
    }
    private void LoadTextPaymentTransfer()
    {
        FreeTextBox1.Width = new Unit(650);
        FreeTextBox1.Text = DBConnection.GetTextPaymentTransfer(siteSetting.GetSiteID);
    }
    private void LoadTextCallPurchas()
    {
        FreeTextBox1.Width = new Unit(650);
        FreeTextBox1.Text = DBConnection.GetTextCallPurchas(siteSetting.GetSiteID);
    }
    void LoadTextCreditCard()
    {
        FreeTextBox1.Width = new Unit(650);
        FreeTextBox1.Text = DBConnection.GetTextCreditCard(siteSetting.GetSiteID);
    }
    protected void SaveButton_Click(Object Src, EventArgs E)
    {

 //       Output.Text = FreeTextBox1.Text;
        string textLogin=FreeTextBox1.Text.Trim();
       
        string command = "";
        switch(ddl_Page.SelectedValue)
        {
            case ("Login"): 
                command = "EXEC dbo.InsertTextLoginBySiteNameId @TextLogin, " +
                    "@SiteNameId";
                break;
            case ("PaymentWireTransfer"):
                command = "EXEC dbo.InsertTextPaymentTransferBySiteNameId @TextLogin, " +
                    "@SiteNameId";
                break;
            case ("MyCredit"):
                command = "EXEC dbo.InsertTextCallPurchasBySiteNameId @TextLogin, " +
                    "@SiteNameId";
                break;
            case ("PaymentCreditCard"):
                command = "EXEC dbo.InsertTextPaymentCreditCardBySiteNameId @TextLogin, " +
                    "@SiteNameId";
                break;
            default: return;
        }
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            if (string.IsNullOrEmpty(textLogin))
                cmd.Parameters.AddWithValue("@TextLogin", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@TextLogin", textLogin);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
        ClientScript.RegisterStartupScript(this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
       
    }
    protected void ConfigureButton_Click(Object Src, EventArgs E)
    {
        FreeTextBox1.ToolbarStyleConfiguration = (ToolbarStyleConfiguration)Enum.Parse(typeof(ToolbarStyleConfiguration), ToolbarStyle.SelectedValue);
        FreeTextBox1.Language = Language.SelectedValue;
    }
    protected void _ddl_Page_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (ddl_Page.SelectedValue)
        {
            case ("Login"):                
                LoadTextLogin();
                break;
            case ("PaymentWireTransfer"):                
                LoadTextPaymentTransfer();
                break;
            case ("MyCredit"):                
                LoadTextCallPurchas();
                break;
            case("PaymentCreditCard"):
                LoadTextCreditCard();
                break;
        }
        /*
        if (_RadioButtonList.SelectedValue == "Login")
            LoadTextLogin();
        else
            LoadTextPaymentTransfer();
         * */
    }
    
    /*
    List<string> SiteListV
    {
        get {return( Session["SiteList"] ==null)?new List<string>() : (List<string>)Session["SiteList"];}
    }
     * */
    
}
