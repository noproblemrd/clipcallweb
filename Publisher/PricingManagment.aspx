<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" 
CodeFile="PricingManagment.aspx.cs" Inherits="Publisher_PricingManagment" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link href="PublishStyle.css" rel="stylesheet" type="text/css"/>
    <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="../general.js"></script>
	
	<script type="text/javascript" language="javascript">
	function closePopup()
   {
        document.getElementById("<%# popUpAddPackage.ClientID %>").className="popModal_del popModal_del_hide";
        document.getElementById("<%# txt_NewName.ClientID %>").value="";
        document.getElementById("<%# txt_NewFromAmount.ClientID %>").value="";
        document.getElementById("<%# txt_newBonus.ClientID %>").value="";
        document.getElementById("<%# hf_PackegeId.ClientID %>").value="";
        var _ddl = document.getElementById("<%# ddl_SupplierType.ClientID %>");
        for(var i=0; i<_ddl.options.length; i++)
        {
            if(_ddl.options[i].value == "<%# GetDefaultType %>")
            {
                _ddl.selectedIndex = i;
                break;
            }
        }
        var validators = Page_Validators;  
        for (var i = validators.length-1; i > -1; i--)
        {           
   //         validators[i].style.visibility = "hidden";
            validators[i].style.display = "none";
        }
        try{
            $find('_modal').hide();
        }
        catch(ex){}
   }
   function openPopup()
   {
        document.getElementById("<%# popUpAddPackage.ClientID %>").className="popModal_del";
        $find('_modal').show();	        
   }
	function Check_validation()
   {
        if(!Page_ClientValidate('NewPackage'))
            return false;
        showDiv();
        return true;
   }
   function Check_validation_txt(elm, e)
   {
        var code;
        if (!e) var e = window.event;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;
        if(code != 13)
            return true;
        if(Check_validation())
        {
            <%# GetPostBack %>;
        }
        return false;
   }
   function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }
    function pageLoad(sender, args)
    { 
        appl_init();     
        hideDiv();
//        closePopup();
    }
	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>

   <uc1:Toolbox ID="Toolbox1" runat="server" />
<h5>
			<asp:Label ID="lblSubTitle" runat="server">Pricing packages managment</asp:Label>
			</h5>

<div class="page-content minisite-content5">


	
	   
	
	
			
			
			
        <div id="form-analyticsseg">
			<center>
			<asp:UpdatePanel ID="_updatePanelTable" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
			
                <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                CssClass="data-table"  AllowPaging="true" PageSize="20" 
                    onpageindexchanging="_GridView_PageIndexChanging">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                         
                
                <Columns>                           
                
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                    <br />
                    <asp:CheckBox ID="cb_all" runat="server" >
                    </asp:CheckBox>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cb_choose" runat="server" />
                    
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="<%# lbl_number.Text %>"></asp:Label>  
                           
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCode" runat="server" Text="<%# Bind('Number') %>"></asp:Label>
                    <asp:Label ID="lbl_PackegeId" runat="server" Text="<%# Bind('PricingPackageId') %>" Visible="false"></asp:Label>      
                </ItemTemplate>
                                
                               
                </asp:TemplateField>
                
                <asp:TemplateField>              
                <HeaderTemplate>
                    <asp:Label ID="Label3" runat="server" Text="<%# lbl_name.Text %>"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text="<%# Bind('Name') %>"></asp:Label>
                </ItemTemplate>                             
                </asp:TemplateField>
                
                <asp:TemplateField>              
                <HeaderTemplate>
                    <asp:Label ID="Label5" runat="server" Text="<%# lbl_amount.Text %>"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>                 
                    <asp:Label ID="lbl_FromAmount" runat="server" Text="<%# Bind('FromAmount') %>"
                    ></asp:Label>
                </ItemTemplate>                         
                </asp:TemplateField>
                
                 <asp:TemplateField>              
                <HeaderTemplate>
                    <asp:Label ID="Label7" runat="server" Text="<%# lbl_Bonus.Text %>"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblBonus" runat="server" Text="<%# Bind('bonusPercent') %>"></asp:Label>
                </ItemTemplate>                         
                </asp:TemplateField>

                 <asp:TemplateField>              
                <HeaderTemplate>
                    <asp:Label ID="Label8" runat="server" Text="<%# lbl_Type.Text %>"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblSupplierType" runat="server" Text="<%# Bind('SupplierType') %>"></asp:Label>
                    <asp:Label ID="lblSupplierTypeValue" runat="server" Text="<%# Bind('SupplierTypeValue') %>" Visible="false"></asp:Label>
                </ItemTemplate>                         
                </asp:TemplateField>
                
                
                </Columns>
                
                
                </asp:GridView>
			</ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="_btn_Set" EventName="Click" />
                   
                </Triggers>
			
			</asp:UpdatePanel>
			</center>
			</div>
            <div runat="server" id="div_image" style="text-align:center;">
                <asp:Image ID="img_Payment" runat="server" Height="180" Width="460" />
                <br />
                
                <asp:LinkButton ID="lb_Remove" runat="server" Text="Remove" 
                    onclick="lb_Remove_Click"></asp:LinkButton>
            </div>
      </div>   
    
      <div id="popUpAddPackage" runat="server" class="popModal_del popModal_del_hide"  >
    	<div class="top"></div>
    	<a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
    	<div class="content">
    	    <table style="width:100%;">
    	    <tr>
    	    <td class="add_new_package" colspan="2">
    	        <h2><asp:Label ID="lbl_titleAddPackage" runat="server" Text="Add new package"></asp:Label></h2>
    	    </td>
                
    	    </tr>
    	    <tr>
    	    <td style="width:130px;">
                <asp:Label ID="lbl_NewName" runat="server"  Text="Package name"></asp:Label>
                <asp:HiddenField ID="hf_PackegeId" runat="server" />
    	    
    	    </td>
    	    <td>
    	        <asp:TextBox ID="txt_NewName" CssClass="label2" runat="server"></asp:TextBox>
    	        <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_name" runat="server" ErrorMessage="Missing" Display="Dynamic" ForeColor="red"
                 ValidationGroup="NewPackage" ControlToValidate="txt_NewName"></asp:RequiredFieldValidator>
    	    </td>                
    	    </tr>
    	    
    	    <tr>
    	    <td>
                <asp:Label ID="lbl_NewFromAmount" runat="server" Text="<%# lbl_amount.Text %>"></asp:Label>
    	    
    	    </td>
    	    <td>
    	        <asp:TextBox ID="txt_NewFromAmount" CssClass="label2" runat="server"></asp:TextBox>
    	        <br />
    	        <asp:RequiredFieldValidator ID="RequiredFieldValidator_FromAmount" runat="server" ErrorMessage="Missing" Display="Dynamic" ForeColor="red"
                 ValidationGroup="NewPackage" ControlToValidate="txt_NewFromAmount"></asp:RequiredFieldValidator>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator_NewFromAmount" 
                runat="server" ErrorMessage="Invalid" ValidationGroup="NewPackage" Display="Dynamic"  ForeColor="red"
                  ControlToValidate="txt_NewFromAmount" ValidationExpression="^[1-9]\d*$"></asp:RegularExpressionValidator>
                 
    	    </td>                
    	    </tr>
    	    
    	    <tr>
    	    <td>
                <asp:Label ID="lbl_newBonus" runat="server" Text="<%# lbl_Bonus.Text %>"></asp:Label>
    	    
    	    </td>
    	    <td>
                <input id="txt_newBonus" type="text" class="label2" runat="server" onkeypress="javascript:Check_validation_txt(this, event);" />
                <br />
    	        
    	        <asp:RequiredFieldValidator ID="RequiredFieldValidator_newBonus" runat="server" ErrorMessage="Missing" Display="Dynamic" ForeColor="red"
                 ValidationGroup="NewPackage" ControlToValidate="txt_newBonus"></asp:RequiredFieldValidator>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator_newBonus"  ForeColor="red"
                runat="server" ErrorMessage="Invalid" ValidationGroup="NewPackage" Display="Dynamic"
                  ControlToValidate="txt_newBonus" ValidationExpression="^[1-9]\d*$"></asp:RegularExpressionValidator>
                  
    	    </td>                
    	    </tr>

            <tr>
    	    <td>
                <asp:Label ID="Label2" runat="server" Text="<%# lbl_Type.Text %>"></asp:Label>
    	    
    	    </td>
    	    <td>
                <asp:DropDownList ID="ddl_SupplierType" runat="server">
                </asp:DropDownList>
                  
    	    </td>                
                
    	    </tr>
    	    
    	    
    	    </table>   	
    	</div>
    	<div class="content">
    	<table class="Table_Button">
        <tr>
        <td>
        
            <asp:Button ID="_btn_Set" runat="server" Text="Set" ValidationGroup="NewPackage" 
            OnClick="_btn_Set_Click" OnClientClick="return Check_validation();"
             CssClass="btn2" />
            
        </td>        
        <td>
            <input id="btn_cancel" type="button" class="btn2" value="Cancel" runat="server" onclick="javascript:closePopup();"/>
        </td>
        </tr>
        </table>
            
        
    	</div>
        	<div class="bottom"></div>
    </div>
   
    <cc1:ModalPopupExtender ID="_mpe" runat="server"
            TargetControlID="btn_virtual"
            PopupControlID="popUpAddPackage"
             BackgroundCssClass="modalBackground" 
              BehaviorID="_modal"               
              DropShadow="false"
           ></cc1:ModalPopupExtender>
    
     <div id="divLoader" class="_Background" style="text-align:center;display:none;">   
      <table width="100%" height="100%">
        <tr>
            <td height="100%" style="vertical-align:middle;">
              <img src="../UpdateProgress/ajax-loader.gif" style="text-align:center;vertical-align:middle;" alt="progress"/>
            </td>
        </tr>
     </table>        
    </div>      
    
    <div style="display:none;">
       <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />          
           
   </div>
    
    
    <div>
        <asp:Label ID="lbl_Edit" runat="server" Text="Edit" Visible="false"></asp:Label>
        <asp:Label ID="lbl_Update" runat="server" Text="Update" Visible="false"></asp:Label>
        <asp:Label ID="lbl_Cancel" runat="server" Text="Cancel" Visible="false"></asp:Label>
        <asp:Label ID="lbl_Inactive" runat="server" Text="Inactive" Visible="false"></asp:Label>
        <asp:Label ID="lbl_Active" runat="server" Text="Active" Visible="false"></asp:Label>
        <asp:Label ID="lbl_IsActive" runat="server" Text="Active" Visible="false"></asp:Label>
        

        
        <asp:Label ID="lbl_number" runat="server" Text="Number" Visible="false"></asp:Label>
        <asp:Label ID="lbl_name" runat="server" Text="Name" Visible="false"></asp:Label>
        <asp:Label ID="lbl_amount" runat="server" Text="Larger than" Visible="false"></asp:Label>
        <asp:Label ID="lbl_Bonus" runat="server" Text="Bonus (Percent)" Visible="false"></asp:Label>
        <asp:Label ID="lbl_Type" runat="server" Text="Type" Visible="false"></asp:Label>
        <asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
        
    
    </div>   


</asp:Content>

