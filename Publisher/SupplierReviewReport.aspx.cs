﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_SupplierReviewReport : PublisherPage
{
    const string APPROVE_REVIEW_SCRIPT = "return ApproveReview(this, '{0}')";
    const int DAYS_INTERVAL = 7;
    protected const int ITEM_PAGE = 40;
    protected const int PAGE_PAGES = 10;
    protected readonly string EXCEL_NAME = "SupplierReviewReport";
    protected const string PAGE_TITLE = "Supplier Review Report";

    protected readonly string SessionTableName = "data_SupplierReview";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            SetDateInterval();
            LoadReviewStatuses();
            LoadCustomerApp();
            Guid accountRatingId, customerId, supplierId;
            if (Guid.TryParse(Request.QueryString["cid"], out customerId))
                ExecSupplierReviewReport(customerId: customerId);
            else if (Guid.TryParse(Request.QueryString["sid"], out supplierId))
                ExecSupplierReviewReport(supplierId: supplierId);
            else if (Guid.TryParse(Request.QueryString["arid"], out accountRatingId))
                ExecSupplierReviewReport(accountRatingId: accountRatingId);
            else
                ExecSupplierReviewReport();
        }
        SetToolbox();
        Header.DataBind();
    }

    private void LoadCustomerApp()
    {
        ddl_customerApp.Items.Clear();
        foreach (ClipCallReport.eCustomerOrigin customerOrigin in Enum.GetValues(typeof(ClipCallReport.eCustomerOrigin)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(customerOrigin), customerOrigin.ToString());
            li.Selected = customerOrigin == ClipCallReport.eCustomerOrigin.All;
            ddl_customerApp.Items.Add(li);
        }
    }

    private void ExecSupplierReviewReport(Guid accountRatingId = default(Guid), Guid customerId = default(Guid), Guid supplierId = default(Guid))
    {
        ClipCallReport.SupplierReviewReportRequest reviewsRequest = new ClipCallReport.SupplierReviewReportRequest();
        Guid _customerId;
        if (!Guid.TryParse(txt_CustomerId.Text, out _customerId))
            _customerId = default(Guid);
        if(customerId != default(Guid))
            reviewsRequest.CustomerId = customerId;
        else if(_customerId != default(Guid))
            reviewsRequest.CustomerId = _customerId;
        else if(supplierId != default(Guid))
            reviewsRequest.SupplierId = supplierId;
        else if (accountRatingId != Guid.Empty)
            reviewsRequest.AccountRatingId = accountRatingId;
        else
        {
            reviewsRequest.To = FromToDate1.GetDateTo;
            reviewsRequest.From = FromToDate1.GetDateFrom;
            ClipCallReport.eSupplierReviewStatus reviewStatus;
            if (!Enum.TryParse<ClipCallReport.eSupplierReviewStatus>(ddl_ReviewStatus.SelectedValue, out reviewStatus))
                reviewStatus = ClipCallReport.eSupplierReviewStatus.None;
            reviewsRequest.ReviewStatus = reviewStatus;
            reviewsRequest.IncludeQa = cb_IncludeQaTest.Checked;
            ClipCallReport.eCustomerOrigin customerOrigin;
            if (!Enum.TryParse<ClipCallReport.eCustomerOrigin>(ddl_customerApp.SelectedValue, out customerOrigin))
                customerOrigin = ClipCallReport.eCustomerOrigin.All;
            reviewsRequest.CustomerOrigin = customerOrigin;
        }
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfListOfSupplierReviewReportResponse result = null;
        try
        {
            result = report.SupplierReviewReport(reviewsRequest);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }


        DataTable dt = new DataTable();
        dt.Columns.Add("CreatedOn");
        dt.Columns.Add("CustomerName");
        dt.Columns.Add("CustomerPhone");
        dt.Columns.Add("ProjectId");
        dt.Columns.Add("SupplierName");
        dt.Columns.Add("SupplierPhone");
        dt.Columns.Add("FairPrice");
        dt.Columns.Add("WorkQuality");
        dt.Columns.Add("CustomerService");
        dt.Columns.Add("FlexibleSchedules");
        dt.Columns.Add("Rating");
        dt.Columns.Add("Description");
        dt.Columns.Add("Status");
        dt.Columns.Add("CanApprove", typeof(bool));
        dt.Columns.Add("Approve");
        dt.Columns.Add("ClipCallRate");
        dt.Columns.Add("CustomerReviewUrl");
        dt.Columns.Add("SupplierReviewUrl");

        foreach(ClipCallReport.SupplierReviewReportResponse srrr in result.Value)
        {
            DataRow row = dt.NewRow();
            row["CreatedOn"] = string.Format(siteSetting.DateTimeFormat, srrr.CreatedOn);
            row["CustomerName"] = srrr.CustomerName;
            row["CustomerPhone"] = srrr.CustomerPhone;
            row["ProjectId"] = srrr.ProjectId;
            row["SupplierName"] = srrr.SupplierName;
            row["SupplierPhone"] = srrr.SupplierPhone;
            if(srrr.FairPricing > -1)
                row["FairPrice"] =  srrr.FairPricing;
            if (srrr.WorkQuality > -1)
                row["WorkQuality"] = srrr.WorkQuality;
            if (srrr.CustomerService > -1)
                row["CustomerService"] = srrr.CustomerService;
            if (srrr.FlexibleSchedules > -1)
                row["FlexibleSchedules"] = srrr.FlexibleSchedules;
            if (srrr.ClipCallRate.HasValue)
                row["ClipCallRate"] = srrr.ClipCallRate.Value;
            row["Rating"] = string.Format(NUMBER_FORMAT, srrr.Rating);
            row["Description"] = srrr.Description;
            row["Status"] = srrr.ReviewStatus.ToString();// srrr.ReviewStatus == ClipCallReport.eSupplierReviewStatus.None ? null : srrr.ReviewStatus.ToString();
            row["CanApprove"] = srrr.ReviewStatus == ClipCallReport.eSupplierReviewStatus.Pending;
            row["Approve"] = string.Format(APPROVE_REVIEW_SCRIPT, srrr.AccountRatingId);
            row["CustomerReviewUrl"] = string.Format(Utilities.CUSTOMER_REVIEW_URL, srrr.CustomerId);
            row["SupplierReviewUrl"] = string.Format(Utilities.SUPPLIER_REVIEW_URL, srrr.SupplierId);
            dt.Rows.Add(row);
        }

        dataS = dt;

        lbl_RecordMached.Text = dt.Rows.Count + " " + ToolboxReport1.RecordMached;
        if (dt.Rows.Count > 0)
        {
            double avg = result.Value.Average(x => x.Rating);
            lbl_avg.Text = string.Format(NUMBER_FORMAT, avg) + " avg";
        }
        else
            lbl_avg.Text = null;
        if (result.Value.Length == 0)
        {
            lbl_NPS.Text = "";
        }
        else
        {
            int supporter = (from x in result.Value
                             where x.ReviewStatus == ClipCallReport.eSupplierReviewStatus.Approved && x.ClipCallRate.HasValue && x.ClipCallRate.Value > 8
                             select x).Count();
            int unSupporter = (from x in result.Value
                               where x.ReviewStatus == ClipCallReport.eSupplierReviewStatus.Approved && x.ClipCallRate.HasValue && x.ClipCallRate.Value < 7
                               select x).Count();
            int rowsNum = (from x in result.Value
                           where x.ReviewStatus == ClipCallReport.eSupplierReviewStatus.Approved && x.ClipCallRate.HasValue
                           select x).Count();
            if (rowsNum == 0)
                lbl_NPS.Text = string.Empty;
            else
            {
                double NPS = GetPercent(supporter, rowsNum) - GetPercent(unSupporter, rowsNum);
                lbl_NPS.Text = string.Format(NUMBER_FORMAT, NPS);
            }
        }
        /*  AVG ROW */
        if (dt.Rows.Count > 0)
        {
            DataRow row = dt.NewRow();
            row["CreatedOn"] = "AVG";
            
            row["FairPrice"] = string.Format(NUMBER_FORMAT, result.Value.Average(x => x.FairPricing));
            row["WorkQuality"] = string.Format(NUMBER_FORMAT, result.Value.Average(x => x.WorkQuality));
            row["CustomerService"] = string.Format(NUMBER_FORMAT, result.Value.Average(x => x.CustomerService));
            row["FlexibleSchedules"] = string.Format(NUMBER_FORMAT, result.Value.Average(x => x.FlexibleSchedules));
            row["Rating"] = string.Format(NUMBER_FORMAT, result.Value.Average(x => x.Rating));
            var qClipCallRate = from x in result.Value
                                where x.ClipCallRate.HasValue
                                select x;
            if(qClipCallRate.Count() > 0)
                row["ClipCallRate"] = string.Format(NUMBER_FORMAT, qClipCallRate.Average(x => x.ClipCallRate));            
            row["CanApprove"] = false;
           
            dt.Rows.InsertAt(row, 0);
        }
        /*  AVG ROW */
        _GridView.DataSource = dt;
        _GridView.DataBind();
        _UpdatePanel.Update();
        UpdatePanel_nps.Update();
    }
    private double GetPercent(int num, int all)
    {
        return (double)(100 * num) / (double)all;
    }
    private void LoadReviewStatuses()
    {
        ddl_ReviewStatus.Items.Clear();
        foreach (ClipCallReport.eSupplierReviewStatus reviewStatus in Enum.GetValues(typeof(ClipCallReport.eSupplierReviewStatus)))
        {
            ListItem li = new ListItem(reviewStatus.ToString());
            li.Selected = reviewStatus == ClipCallReport.eSupplierReviewStatus.None;
            ddl_ReviewStatus.Items.Add(li);
        }
    }

    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecSupplierReviewReport();
    }
    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(PAGE_TITLE);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected string ApproveReviewWebMethod
    {
        get { return ResolveUrl("~/Publisher/SupplierReviewReport.aspx/ApproveReview"); }
    }
    [WebMethod(MessageName = "ApproveReview")]
    public static string ApproveReview(Guid accountRatingId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.Result result = null;
        try
        {
            result = ccreport.ApproveReview(accountRatingId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return false.ToString().ToLower();
        }
        return true.ToString().ToLower();
    }

    protected DataTable dataS
    {
        get { return Session[SessionTableName] == null ? new DataTable() : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
}