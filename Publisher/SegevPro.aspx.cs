﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_SegevPro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            LoadReport();
        }
    }

    private void LoadReport()
    {
        string command = "exec [dbo].[GoogleSheetProReport]";
        try
        {
            using (SqlConnection conn = DBConnection.GetClipCallConnString())
            {

                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.CommandTimeout = 180;                   
                    SqlDataReader reader = cmd.ExecuteReader();
                    _GridView.DataSource = reader;
                    _GridView.DataBind();
                    conn.Close();
                }
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        
    }
}