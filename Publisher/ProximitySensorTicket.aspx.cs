﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ProximitySensorTicket : PublisherPage
{
    const string UNKNOWN = "Unknown";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string proximitySensor_id = Request.QueryString["psid"];
            Guid proximitySensorId;
            if (!Guid.TryParse(proximitySensor_id, out proximitySensorId))
            {
                ClosePopupByError();
                return;
            }
            LoadDetails(proximitySensorId);
        }
    }

    private void LoadDetails(Guid proximitySensorId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfProximitySensorData result = null;
        try
        {
            result = report.GetProximitySensorData(proximitySensorId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClosePopupByError();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure || result.Value == null)
        {
            ClosePopupByError();
            return;
        }
        ClipCallReport.ProximitySensorData data = result.Value;
        lbl_Date.Text = string.Format(siteSetting.DateTimeFormat, data.Date);
        lbl_ArrivalDate.Text = data.ArrivalDate == DateTime.MinValue ? UNKNOWN : string.Format(siteSetting.DateTimeFormat, data.ArrivalDate);
        lbl_CheckOutDate.Text = data.CheckOutDate == DateTime.MinValue ? UNKNOWN : string.Format(siteSetting.DateTimeFormat, data.CheckOutDate);
        lbl_Distance.Text = data.Distance < 0 ? UNKNOWN : data.Distance.ToString() + "m";
        lbl_Latitude.Text = data.Latitude.ToString();
        lbl_Longitude.Text = data.Longitude.ToString();        
    }
    void ClosePopupByError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseError", "CloseMeError();", true);
    }
}