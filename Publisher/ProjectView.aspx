﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProjectView.aspx.cs" Inherits="Publisher_ProjectView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" rel="stylesheet" href="Style.css" /> 
    <link type="text/css" rel="stylesheet" href="ProjectViewStyle.css" /> 
    <link rel="Stylesheet" type="text/css" href="../DateTimePicker/jquery.datetimepicker.min.css" />
    <title></title>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="../DateTimePicker/jquery.datetimepicker.full.min.js"></script>
    <script type="text/javascript" src="../general.js"></script>

    <script type="text/javascript">
         function appl_init() {        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
        hideDiv();
        try{
            parent.ShowIframeDiv();
        }
        catch(ex){}
        
        }
        function BeginHandler()
        {     
            showDiv();
        }
        function EndHandler()
        {
            hideDiv();
        }
        $(function () {
             setFollowUpDate();
         });
        function setFollowUpDate() {
            var _dt = '';
             $('.-FollowUpDate').datetimepicker({
                    step: 30,
                    allowBlank: true,
                    format: 'm/d/Y H:i',
                    onClose: function () {
                //      var d = $('#datetimepicker').datetimepicker('getValue');
              //        var dd = $('.-FollowUpDate').val();
              //        setScheduledDateservice(dd);
                   

                 },
                 onShow: function () {
                     _dt = $('.-FollowUpDate').val();
                 },
                 onChangeDateTime: function () {
                     var dd = $('.-FollowUpDate').val();
                     if (_dt != dd) {                         
                         setScheduledDateservice(dd);
                         _dt = dd;
                     }
                 }
            });
        };
        function setScheduledDateservice(date) {
            var incidentAccountId = document.querySelector("#<%# hf_followUpDate.ClientID %>").value;
            showDiv();
             $.ajax({
            url: "<%# GetSetFollowUpDateService %>",
                 data: "{ 'incidentAccountId': '" + incidentAccountId + "', 'date_time': '" + date + "'}",
                 dataType: "json",
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 dataFilter: function (data) { return data; },
                 success: function (data) {
                     var _data = eval("(" + data.d + ")");
                     if (!_data.isSucceed)                      
                         alert('Failed');
                 },
                 
                 error: function (XMLHttpRequest, textStatus, errorThrown) {
                     alert(textStatus);
                 },
                 complete: function (jqXHR, textStatus) {
   //                  $('.-FollowUpDate').blur();
                     hideDiv();
                 }
             });
         }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>
        <div>
             <div runat="server" id="div_next" class="upsalebtn  div_relaunch">

                <asp:LinkButton ID="lb_next" runat="server" CssClass="StopRequest" OnClick="lb_next_Click">NEXT</asp:LinkButton>
            </div>
            <div class="titlerequestTop">          
                <div>
			        <asp:Label ID="lbl_CurrentRequest_Title" runat="server" Text="Project ID:"></asp:Label>
                    <asp:Label ID="lbl_projectID" runat="server"></asp:Label>
			    </div>   
               <div>
			        <asp:Label ID="Label1" runat="server" Text="Reason for follow-up"></asp:Label>:
                    <asp:Label ID="lbl_reasonFollowUp" runat="server"></asp:Label>
			    </div>  
        
            </div>
             <div class="clear"></div>
            <div class="separator"></div>
            <div class="containertab2" style="position:relative;">
                    <div class="left-side">
                        <div class="project-left">
                            <asp:Label ID="Label2" runat="server" CssClass="label" Text="Project Information:"></asp:Label>
                            <div>
                                <asp:Label ID="Label3" runat="server" Text="Date published:"></asp:Label>
                                <asp:Label ID="lbl_DatePublished" runat="server" ></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label4" runat="server" Text="Project's category"></asp:Label>
                                <asp:Label ID="lbl_ProjectsCategory" runat="server"></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label8" runat="server" Text="How the customer name it"></asp:Label>
                                <asp:Label ID="lbl_projectTitle" runat="server"></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label5" runat="server" Text="Video link:"></asp:Label>
                                <asp:HyperLink ID="hl_VideoUrl" runat="server" Target="_blank" style="overflow-wrap: break-word;"></asp:HyperLink>
                            </div>
                            <div>
                                <asp:Label ID="Label7" runat="server" Text="Project status"></asp:Label>
                                <asp:Label ID="lbl_projectStatus" runat="server"></asp:Label>
                            </div>                            
                        </div>
                        <div class="project-left">
                            <asp:Label ID="Label20" runat="server" CssClass="label" Text="Customer Information:"></asp:Label>
                            <div>
                                <asp:Label ID="Label22" runat="server" Text="Name:"></asp:Label>
                                <asp:Label ID="lbl_customerName" runat="server" ></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label21" runat="server" Text="ID:"></asp:Label>
                                <asp:Label ID="lbl_customerId" runat="server"></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label23" runat="server" Text="Address:"></asp:Label>
                                <asp:Label ID="lbl_customerAddress" runat="server"></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label25" runat="server" Text="Phone number:"></asp:Label>
                                <asp:Label ID="lbl_customerPhone" runat="server"></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label26" runat="server" Text="Platform:"></asp:Label>
                                <asp:Label ID="lbl_customerPlatform" runat="server"></asp:Label>
                            </div>                            
                             <div>
                                <asp:Label ID="Label27" runat="server" Text="App version:"></asp:Label>
                                <asp:Label ID="lbl_customerAppVersion" runat="server"></asp:Label>
                            </div>  
                             <div>
                                <asp:Label ID="Label28" runat="server" Text="Membership:"></asp:Label>
                                <asp:Label ID="lbl_customerMembership" runat="server"></asp:Label>
                            </div>  
                             
                        </div>
                         <div class="project-left">
                            <asp:Label ID="Label30" runat="server" CssClass="label" Text="Pro marked for follow-up:"></asp:Label>
                            <div>
                                <asp:Label ID="Label31" runat="server" Text="Name:"></asp:Label>
                                <asp:Label ID="lbl_supplierName" runat="server" ></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label32" runat="server" Text="ID:"></asp:Label>
                                <asp:Label ID="lbl_supplierId" runat="server"></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label33" runat="server" Text="Business address:"></asp:Label>
                                <asp:Label ID="lbl_supplierAddress" runat="server"></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label34" runat="server" Text="Phone number:"></asp:Label>
                                <asp:Label ID="lbl_supplierPhone" runat="server"></asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="Label35" runat="server" Text="Platform:"></asp:Label>
                                <asp:Label ID="lbl_supplierPlatform" runat="server"></asp:Label>
                            </div>                            
                             <div>
                                <asp:Label ID="Label36" runat="server" Text="App version:"></asp:Label>
                                <asp:Label ID="lbl_supplierappVersion" runat="server"></asp:Label>
                            </div>  
                            
                        </div>
                    </div>
                    <div class="center-side">
                        <div class="center-table">
                            <asp:GridView ID="gv_ProOtherProjects" CssClass="data-table" runat="server" AutoGenerateColumns="false" >
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1000" runat="server" Text="Project ID"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Bind("ProjectView") %>' Text='<%# Bind("ProjectId") %>' Target="_blank"></asp:HyperLink>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1002" runat="server" Text="Date published"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1003" runat="server" Text='<%# Bind("DatePublisher") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1004" runat="server" Text="Project status"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1005" runat="server" Text='<%# Bind("ProjectStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1007" runat="server" Text="Customer"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1008" runat="server" Text='<%# Bind("CustomerName") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                     <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1011" runat="server" Text="Pro status on the project"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1012" runat="server" Text='<%# Bind("ProStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1009" runat="server" Text="Reason for follow-up"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1010" runat="server" Text='<%# Bind("eventType") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="center-table">
                            <asp:GridView ID="gv_CustomersProjects" CssClass="data-table" runat="server" AutoGenerateColumns="false">
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label2000" runat="server" Text="Project ID"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2001" runat="server" Text='<%# Bind("ProjectId") %>'></asp:Label>
                                        <% /*   
                                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<# Bind("ProjectView") >' Text='<# Bind("ProjectId") >' Target="_blank"></asp:HyperLink>
                                        */ %>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label2002" runat="server" Text="Date published"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2003" runat="server" Text='<%# Bind("DatePublisher") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label2004" runat="server" Text="Project status"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2005" runat="server" Text='<%# Bind("ProjectStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label2009" runat="server" Text="Reason for follow-up"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2010" runat="server" Text='<%# Bind("eventType") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                         <div class="center-table">
                            <asp:GridView ID="gv_proOnTheProject" CssClass="data-table" runat="server" AutoGenerateColumns="false">
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label3000" runat="server" Text="Project ID"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>                                       
                                        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl='<# Bind("ProjectView") >' Text='<# Bind("ProjectId") >' Target="_blank"></asp:HyperLink>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label3002" runat="server" Text="Pro name"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3003" runat="server" Text='<%# Bind("ProName") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label3004" runat="server" Text="Number of bokked projects"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3005" runat="server" Text='<%# Bind("BookedProjects") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label3006" runat="server" Text="Status"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3007" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label3009" runat="server" Text="Reason for follow-up"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3010" runat="server" Text='<%# Bind("eventType") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="right-side">
                        <div>
                             <asp:Label ID="Label4000" runat="server" CssClass="label" Text="Next time to follow-up:"></asp:Label>
                            <asp:TextBox ID="txt_followUpDate" runat="server" CssClass="-FollowUpDate" Width="110"></asp:TextBox>
                            <asp:HiddenField ID="hf_followUpDate" runat="server" />
                        </div>
                        <div>
                            <asp:Label ID="Label4001" runat="server" CssClass="label" Text="Notes:"></asp:Label>
                            <asp:TextBox ID="txt_note" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox>
                            <asp:LinkButton ID="lb_notes" runat="server" CssClass="StopRequest" OnClick="lb_notes_Click">SUBMIT</asp:LinkButton>
                            <asp:DataList ID="dl_notes" runat="server">
                                 <ItemStyle CssClass="regular" />
                                <ItemTemplate>
                                   <div class="notetit"> 
                                     <asp:Label ID="lblDate" runat="server" Text='<%# Bind("CreatedOn") %>'></asp:Label>
                                       <br />
                                       <asp:Label ID="lblnote" runat="server" Text='<%# Bind("Note") %>'></asp:Label>                                       
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </div>
            </div>
        </div>
<div id="divLoader" class="divLoader">   
    <table>
        <tr>
            <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
        </tr>
    </table>
</div>
    </form>
</body>
</html>
