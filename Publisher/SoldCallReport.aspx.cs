﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

public partial class Publisher_SoldCallReport : PageSetting, ICallbackEventHandler
{
    const int DAYS_INTERVAL = 2;
    const string FILE_NAME = "SoldCallReport.xml";
    bool IsReportExc = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        if (!IsPostBack)
        {
            RecordListV = null;
            SetToolbox();
            SetDateInterval();
            LoadCharts();
            LoadCallsOrigin();
            LoadHeadings();
            Loadorigins();
            LoadGroupBy();
            ExecStartReport();
        }
        else
        {
            if (!ScriptManager1.IsInAsyncPostBack)
            {
                string _str = GetCallbackResult();
                ClientScript.RegisterStartupScript(this.GetType(), "ReciveServerData", "ReciveServerData('" + _str + "');", true);
            }
        }        
        SetupClient();        
        SetToolboxEvents();
        Header.DataBind();
    }

    private void ExecStartReport()
    {
        ExecReport();
    }

    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }

    private void LoadGroupBy()
    {
        ddl_group.Items.Clear();
        foreach (eGroupBySoldCallReport str in Enum.GetValues(typeof(eGroupBySoldCallReport)))
        {
            ListItem li = new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eGroupBySoldCallReport", str.ToString()), str.ToString());
            ddl_group.Items.Add(li);
            if (str == eGroupBySoldCallReport.Without)
                ddl_group.SelectedIndex = ddl_group.Items.Count - 1;
        }
    }
    private void Loadorigins()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetAllOrigins(false);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Affiliate.Items.Clear();
        ddl_Affiliate.Items.Add(new ListItem(lbl_all.Text, Guid.Empty.ToString()));
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            ListItem li = new ListItem(gsp.Name, gsp.Id.ToString());
            ddl_Affiliate.Items.Add(li);
        }
    }
    private void LoadHeadings()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);
        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            Update_Faild();
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {
            
            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;
    }

    private void LoadCallsOrigin()
    {
        foreach (string s in Enum.GetNames(typeof(WebReferenceReports.CallType)))
        {
            ddl_CallsOrigin.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId,
                "CallType", s), s));
        }
    }
   
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_SoldCallReport.Text);
        
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }
    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
       
        SoldCallReportData scrd = scrdV;
        if (scrd == null)
            return;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        scrdV = scrd;
        scrd._Request.PageNumber = -1;
        scrd._Request.PageSize = -1;
        DataTable data = new DataTable();
        PagingData pd;
        data = scrd._func(_report, scrd._Request, out pd);
        if (data == null || data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        Session["data_print"] = data;
        Session["grid_print"] = GridViewWithData();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {        
        SoldCallReportData scrd = scrdV;
        if (scrd == null)
            return;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        scrdV = scrd;
        scrd._Request.PageNumber = -1;
        scrd._Request.PageSize = -1;
        DataTable data = new DataTable();
        PagingData pd;
        data = scrd._func(_report, scrd._Request, out pd);
        if (data == null || data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, "SoldCall");

        if (!te.ExecExcel(data))
            Update_Faild();
    }
    private void LoadCharts()
    {
        foreach (string s in Enum.GetNames(typeof(eChartType)))
        {
            string sen = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eChartType", s);
            ListItem li = new ListItem(sen, s);
            li.Selected = false;
            cb_charts.Items.Add(li);
        }
    }
    
    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
       
        ExecReport();
    }
    private void ExecReport()
    {
        IsReportExc = true;
        DateTime dt_from = FromToDate1.GetDateFrom;
        DateTime dt_to = FromToDate1.GetDateTo;
        if (dt_from == DateTime.MinValue || dt_to == DateTime.MinValue)
        {
            dt_to = DateTime.Now;
            dt_from = dt_to.AddMonths(-1);
        }

        Guid HeadingId = new Guid(ddl_Heading.SelectedValue);
        eGroupBySoldCallReport _groupBy;
        if (!Enum.TryParse(ddl_group.SelectedValue, out _groupBy))
            _groupBy = eGroupBySoldCallReport.Without;

        WebReferenceReports.CallsReportRequest _request = new WebReferenceReports.CallsReportRequest();
        _request.ExpertiseId = HeadingId;
        _request.FromDate = dt_from;
        _request.ToDate = dt_to;
        _request.PageNumber = 1;
        _request.PageSize = TablePaging1.ItemInPage;
        _request.UserId = userManagement.Get_Guid;
        _request.SupplierOriginId = new Guid(ddl_Affiliate.SelectedValue);
        WebReferenceReports.CallType ct;
        if (!Enum.TryParse(ddl_CallsOrigin.SelectedValue, out ct))
            ct = WebReferenceReports.CallType.All;
        _request.CallType = ct;

        SoldCallReportData scrd = new SoldCallReportData() { _Request = _request, GroupBy = _groupBy };

        switch (_groupBy)
        {
            case (eGroupBySoldCallReport.RevenuePerHeading): //data = GetRevenuePerHeading(_report, _request);
                scrd._func = GetRevenuePerHeading;
                break;
            case (eGroupBySoldCallReport.RevenuePerDate): //data = RevenuePerDate(_report, _request);
                scrd._func = RevenuePerDate;
                break;
            case (eGroupBySoldCallReport.CountPerHeading):// data = //CountPerHeading(_report, _request);
                scrd._func = CountPerHeading;
                break;
            case (eGroupBySoldCallReport.CountPerDates): //data = CountPerDates(_report, _request);
                scrd._func = CountPerDates;
                break;
            case (eGroupBySoldCallReport.Without):
                scrd._func = without;
                break;
        }

        ExecuteReport(scrd);
    }

    private void ExecuteReport(SoldCallReportData scrd)
    {
        _GridView.DataSource = null;
        _GridView.DataBind();
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        scrdV = scrd;
        DataTable data = new DataTable();
        PagingData pd;
        data = scrd._func(_report, scrd._Request, out pd);
        if(data!=null && pd!=null)
            LoadGeneralData(pd.TotalPages, pd.TotalRows, pd.CurrentPage);
        SetTotalRow(data, pd.Sum);
        BindDataToGrid(data, scrd.GroupBy);
        
    }

    private void SetTotalRow(DataTable data, decimal _sum)
    {
        if (_sum < 0 || data == null)
            return;
        DataRow row = data.NewRow();
        row[0] = lbl_Total.Text;
        row[1] = string.Format(NUMBER_FORMAT, _sum);
        data.Rows.Add(row);
    }
   
    void BindDataToGrid(DataTable data, eGroupBySoldCallReport _groupBy)
    {

        if (data == null)
        {
            CleanCharts();
            return;
        }        
        if (_groupBy == eGroupBySoldCallReport.Without)
            BindDataToGridView(data);
        else
            BindDataToGridViewGroupBy(data);
    }
    void BindDataToGridView(DataTable data) 
    {
        CleanCharts();
        _GridViewGroupBy.DataSource = null;
        _GridViewGroupBy.DataBind();
        _GridView.DataSource = data;
        _GridView.DataBind();
        
        if (data.Rows.Count == 0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
        
        _UpdatePanel.Update();
        
    }

    private void Load_charts()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "load_charts", "setTimeout('SetCharts();', 200);", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetOnLoadChart", "SetOnLoadChart();", true);
    }
    private string GetOnLoadChartDiv()
    {
        HtmlGenericControl _div = new HtmlGenericControl("div");
        _div.Attributes.Add("class", "divLoaderChart");
        HtmlTable _table = new HtmlTable();
        HtmlTableRow _row = new HtmlTableRow();
        HtmlTableCell _cell = new HtmlTableCell();
        HtmlImage _image = new HtmlImage();
        _image.Src = ResolveUrl("~") + @"UpdateProgress/ajax-loader.gif";
        _image.Attributes.Add("alt", "Loading...");
        _image.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");
        _cell.Controls.Add(_image);
        _row.Controls.Add(_cell);
        _table.Controls.Add(_row);
        _div.Controls.Add(_table);
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                _div.RenderControl(textWriter);
            }
        }
        return sb.ToString();
        
    }
    void BindDataToGridViewGroupBy(DataTable data)
    {
        _GridView.DataSource = null;
        _GridView.DataBind();
        foreach (DataColumn dc in data.Columns)
        {
            dc.ColumnName = GetHeaderColumn(dc.ColumnName);
        }
        _GridViewGroupBy.DataSource = data;
        _GridViewGroupBy.DataBind();
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
            CleanCharts();
        }
        else
        {
            if (IsReportExc && cb_charts.SelectedIndex != -1)
                Load_charts();
            else if(IsReportExc && cb_charts.SelectedIndex == -1)
                CleanCharts();
        }
        _UpdatePanel.Update();
    }
    void CleanCharts()
    {
        XmlGraphFileV = null;
        Charts_Area.Controls.Clear();
        _UpdatePanelCharts.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {        
        lbl_RecordMached.Text = _TotalRows + " " + ToolboxReport1.RecordMached;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
   
    private DataTable CountPerDates(WebReferenceReports.Reports _report, WebReferenceReports.CallsReportRequest _request, out PagingData pd)
    {
        pd = null;
        WebReferenceReports.ResultOfCallsReportCountPerDateResponse result = null;
        try
        {
            result = _report.CallsReportCountPerDate(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (_request.PageNumber > 0)
            pd = new PagingData { Sum = result.Value.Total, CurrentPage = result.Value.CurrentPage, TotalPages = result.Value.TotalPages, TotalRows = result.Value.TotalRows };
        DataTable data = GetDataTable.GetDataTableFromListDateTimeFormat(result.Value.Calls, siteSetting.DateFormat);
        
        
        return data;
    }
    private DataTable CountPerHeading(WebReferenceReports.Reports _report, WebReferenceReports.CallsReportRequest _request, out PagingData pd)
    {
        pd = null;
        WebReferenceReports.ResultOfCallsReportCountPerHeadingResponse result = null;
        try
        {
            result = _report.CallsReportCountPerHeading(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (_request.PageNumber > 0)
            pd = new PagingData { Sum = result.Value.Total, CurrentPage = result.Value.CurrentPage, TotalPages = result.Value.TotalPages, TotalRows = result.Value.TotalRows };
   //     DataTable data = GetDataTable.GetDataTableFromListCorrectNumberDateTimeFormat(result.Value.Calls, siteSetting.DateFormat);
        
        DataTable data = new DataTable();
        data.Columns.Add("Expertise", typeof(string));
        data.Columns.Add("Count", typeof(string));
        foreach(WebReferenceReports.CallDataCountPerHeading cdcp in result.Value.Calls)
        {
            DataRow row = data.NewRow();
            row["Expertise"] = cdcp.Expertise;
            row["Count"] = cdcp.Count + "";
            data.Rows.Add(row);
        }
        return data;

       
    }
    private DataTable RevenuePerDate(WebReferenceReports.Reports _report, WebReferenceReports.CallsReportRequest _request, out PagingData pd)
    {
        pd = null;
        WebReferenceReports.ResultOfCallsReportRevenuePerDateResponse result = null;
        try
        {
            result = _report.CallsReportRevenuePerDate(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (_request.PageNumber > 0)
            pd = new PagingData { Sum = result.Value.Total, CurrentPage = result.Value.CurrentPage, TotalPages = result.Value.TotalPages, TotalRows = result.Value.TotalRows };

        DataTable data = GetDataTable.GetDataTableFromListCorrectNumberDateTimeFormat(result.Value.Calls, siteSetting.DateFormat);
        
        return data; 
    }
    private DataTable GetRevenuePerHeading(WebReferenceReports.Reports _report, WebReferenceReports.CallsReportRequest _request, out PagingData pd)
    {
        WebReferenceReports.ResultOfCallsReportRevenuePerHeadingResponse result = null;
        pd = null;
        try
        {
            result = _report.CallsReportRevenuePerHeading(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (_request.PageNumber > 0)
            pd = new PagingData { Sum = result.Value.Total, CurrentPage = result.Value.CurrentPage, TotalPages = result.Value.TotalPages, TotalRows = result.Value.TotalRows };
     
        return GetDataTable.GetDataTableFromListCorrectNumberDateTimeFormat(result.Value.Calls, siteSetting.DateFormat);        
    }
    string GetHeaderColumn(string _val)
    {
        switch (_val)
        {
            case ("Date"): return lbl_Date.Text;
            case ("Heading"): return lblHeading.Text;
            case ("Revenue"): return lbl_Revenue.Text;
            case ("Sum"): return lbl_Sum.Text;                                     
        }
        return _val;
    }

    private DataTable without(WebReferenceReports.Reports _report, WebReferenceReports.CallsReportRequest _request, out PagingData pd)
    {      
        pd = new PagingData();
        WebReferenceReports.ResultOfCallsReportResponse result = null;
        try
        {
            result = _report.CallsReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        Dictionary<int, string> dic = new Dictionary<int, string>();
        DataTable data = new DataTable();// GetDataTable.GetDataTableFromList(result.Value);
        data.Columns.Add("CaseNumber");
        data.Columns.Add("Date");
        data.Columns.Add("DefaultPrice");
        data.Columns.Add("Expertise");
        data.Columns.Add("Price");
        data.Columns.Add("Recording");
        data.Columns.Add("Supplier");
        data.Columns.Add("HasRecord", typeof(bool));
        data.Columns.Add("ImageUrl");
        data.Columns.Add("SupplierNumber");
        data.Columns.Add("OnClientClick");
        data.Columns.Add("Duration");
        data.Columns.Add("Balance");
        foreach (WebReferenceReports.CallData cd in result.Value.Calls)
        {
            DataRow row = data.NewRow();
            row["CaseNumber"] = cd.CaseNumber;
            row["Date"] = cd.Date;
            row["DefaultPrice"] = String.Format("{0:0.##}", cd.DefaultPrice);
            row["Expertise"] = cd.Expertise;
            row["Price"] = String.Format("{0:0.##}", cd.Price);
            row["Supplier"] = cd.Supplier;
            row["SupplierNumber"] = cd.SupplierNumber;
            row["Duration"] = cd.Duration.ToString();
            row["Balance"] = string.Format(this.GetNumberFormat, cd.Balance);
            string _path = cd.Recording;
                   
            if (!string.IsNullOrEmpty(_path))
            {
                int _index;
                do
                {
                    _index = new Random().Next();
                }
                while (dic.ContainsKey(_index));
                dic.Add(_index, _path);
                row["HasRecord"] = true;
                if (cd.IsAllowedRecording)
                {
                    row["Recording"] = "javascript:OpenRecord('" + _index + "');";
                    row["ImageUrl"] = @"../Management/calls_images/icon_speaker.png";
                }
                else
                    row["ImageUrl"] = @"../Management/calls_images/icon_speakeroff.png";
            }
            else            
                row["HasRecord"] = false;

            row["OnClientClick"] = "javascript:openAdvIframe('" + cd.SupplierId.ToString() + "', '" + HttpUtility.JavaScriptStringEncode(cd.Supplier) + "');";
            data.Rows.Add(row);
        }
        if (_request.PageNumber > 0)
            pd = new PagingData { Sum = -1, CurrentPage = result.Value.CurrentPage, TotalPages = result.Value.TotalPages, TotalRows = result.Value.TotalRows };
        RecordListV = dic;
        return data;
        
    }
    #region  chart
    Dictionary<string, string> LoadChart(DataTable data)
    {
        bool HasChart = false;
        string caption = ddl_group.SelectedItem.Text;
        string xAxisName = data.Columns[0].ColumnName;
        string unit = data.Columns[1].ColumnName;
        Dictionary<string, StringBuilder> list = new Dictionary<string, StringBuilder>();
        
        StringBuilder strchart1 = new StringBuilder();
        
        
        if (cb_charts.Items.FindByValue(eChartType.Line.ToString()).Selected)
        {
            HasChart = true;
            //Line
            strchart1.Append(@"<chart caption='" + caption + "' ");
            strchart1.Append("xAxisName='" + xAxisName + "' ");
            strchart1.Append("yAxisName='" + unit + "' ");
            strchart1.Append(@"showNames='1' showValues='0' ");
            strchart1.Append("showColumnShadow='1'  slantLabels='1' ");
            //  strchart1.Append("showColumnShadow='1' "); //rotateNames=""1""  
            strchart1.Append(@" showAlternateHGridColor='1' AlternateHGridColor='ff5904' ");
            strchart1.Append("numberPrefix=' " + ((unit == lbl_Revenue.Text) ? siteSetting.CurrencySymbol : "") + "' ");
            strchart1.Append("animation='1' ");
            strchart1.Append(@"divLineColor='ff5904' divLineAlpha='20' ");
            strchart1.Append("alternateHGridAlpha='20' labelDisplay='ROTATE' ");
            strchart1.Append(@"canvasBorderColor='666666'  palette='3' ");
            strchart1.Append("lineColor='FF5904' lineAlpha='85' >");//bgColor='#dcdcdc'


            for (int i = 0; i < data.Rows.Count; i++)// DataRow row in data.Rows)
            {
                DataRow row = data.Rows[i];
                if (i == data.Rows.Count - 1 && row[0].ToString() == lbl_Total.Text)
                    continue;
                strchart1.Append("<set label='" + Utilities.ChangeCharacterForXml(row[0].ToString()) + "' value='" + row[1].ToString() + "' />");
            }
            strchart1.Append("</chart>");
            list.Add(ResolveUrl("~") + @"Management/FusionCharts/Charts/Line.swf", strchart1);
            

        }
       
        StringBuilder strchart2 = new StringBuilder();
        if (cb_charts.Items.FindByValue(eChartType.Column.ToString()).Selected)
        {
            HasChart = true;
            //Column
            strchart2.Append("<chart yAxisName='" + unit + "' ");
            strchart2.Append("xAxisName='" + xAxisName + "' ");
            strchart2.Append("caption='" + caption + "' ");
            strchart2.Append("numberPrefix=' " + ((unit == lbl_Revenue.Text) ? siteSetting.CurrencySymbol : "") + "' ");
            strchart2.Append("showBorder='1' borderAlpha='50' ");// imageSave='1'
            strchart2.Append("lineColor='FF5904' lineAlpha='85' ");
            strchart2.Append("animation='1' ");
            strchart2.Append("showAlternateHGridColor='1' AlternateHGridColor='ff5904' ");
            strchart2.Append("slantLabels='1' ");
            strchart2.Append("labelDisplay='ROTATE'>");//


            for (int i = 0; i < data.Rows.Count; i++)// DataRow row in data.Rows)
            {
                DataRow row = data.Rows[i];
                if (i == data.Rows.Count - 1 && row[0].ToString() == lbl_Total.Text)
                    continue;
                strchart2.Append("<set label='" + Utilities.ChangeCharacterForXml(row[0].ToString()) + "' value='" + row[1].ToString() + "' />");
            }
            strchart2.Append("</chart>");
            list.Add(ResolveUrl("~") + @"Management/FusionCharts/Charts/Column3D.swf", strchart2);
        }
        
        StringBuilder strchart3 = new StringBuilder();
        if (cb_charts.Items.FindByValue(eChartType.pie.ToString()).Selected)
        {
            HasChart = true;
            //Pie
            strchart3.Append("<graph caption='" + caption + "' ");
            strchart3.Append("animation='1' ");
            strchart3.Append("YAxisName='" + unit + "' ");
            strchart3.Append("xAxisName='" + xAxisName + "' ");
            strchart3.Append("showValues='0' ");
            strchart3.Append("showNames='1' ");
            strchart3.Append("numberPrefix=' " + ((unit == lbl_Revenue.Text) ? siteSetting.CurrencySymbol : "") + "' ");
            strchart3.Append("formatNumberScale='0' ");
            strchart3.Append("showPercentageValues='0' ");
            strchart3.Append("decimalPrecision='0' ");
            strchart3.Append("showBorder='1' borderAlpha='50' ");
            strchart3.Append("bgColor='#f8ffff'>");

            int ind = 0;
            for (int i = 0; i < data.Rows.Count; i++)// DataRow row in data.Rows)
            {
                DataRow row = data.Rows[i];
                if (i == data.Rows.Count - 1 && row[0].ToString() == lbl_Total.Text)
                    continue;
                string color = ""; ;

                switch (ind % 3)
                {
                    case (0): color = "AFD8F8";
                        break;
                    case (1): color = "F6BD0F";
                        break;
                    case (2): color = "8BBA00";
                        break;
                }
                strchart3.Append("<set name='" + Utilities.ChangeCharacterForXml(row[0].ToString()) +
                    "' value='" + row[1].ToString() + "' color='" + color + "'/>");
                ind++;
            }
            strchart3.Append("</graph>");
            list.Add(ResolveUrl("~") + @"Management/FusionCharts/Charts/Pie3D.swf", strchart3);
        }

        Dictionary<string, string> chart_file_names = new Dictionary<string, string>();
        string path = ConfigurationManager.AppSettings["professionalRecord"];
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"];
        foreach (KeyValuePair<string, StringBuilder> _sb in list)
        {
            int ToFile = 0;
            do
            {
                ToFile = new Random().Next();
            } while (File.Exists(path + ToFile + FILE_NAME));
            string _path = ToFile + FILE_NAME;

            try
            {
                using (FileStream fs = new FileStream(path + _path, FileMode.Create))
                {
                    using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        tw.WriteLine(_sb.Value.ToString());
                    }
                }
            }
            catch (Exception ex) { return null; }
            chart_file_names.Add(pathWeb + _path, _sb.Key);
        }
        return chart_file_names;
    }
    #endregion
    
    GridView GridViewWithData()
    {
        if (_GridView.Rows.Count > 0)
            return _GridView;
        if (_GridViewGroupBy.Rows.Count > 0)
            return _GridViewGroupBy;
        return new GridView();
    }
    
    SoldCallReportData scrdV
    {
        get { return (Session["SoldCallReportData"] == null) ? null : (SoldCallReportData)Session["SoldCallReportData"]; }
        set { Session["SoldCallReportData"] = value; }
    }
    Dictionary<int, string> RecordListV
    {
        get { return (Session["RecordList"] == null) ? new Dictionary<int, string>() : (Dictionary<int, string>)Session["RecordList"]; }
        set { Session["RecordList"] = value; }
    }
    
    Dictionary<string, string> XmlGraphFileV
    {
        get { return (Session["XmlGraph"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)Session["XmlGraph"]; }
        set { Session["XmlGraph"] = value; }
    }
    protected string GetAudioChrome
    {
        get { return ResolveUrl("~") + "Management/AudioChrome.aspx"; }
    }
    protected string IfToClose
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_IfToExite.Text); }
    }

    #region TablePaging Members


    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (scrdV == null)
            return;
        SoldCallReportData _SoldCallReportData = scrdV;
        _SoldCallReportData._Request.PageNumber = _pageNum;
        _SoldCallReportData._Request.PageSize = TablePaging1.ItemInPage;
        ExecuteReport(_SoldCallReportData);
    }

    #endregion

    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        StringBuilder _sb = new StringBuilder();
        foreach (KeyValuePair<string, string> kvp in XmlGraphFileV)
        {
            _sb.Append(kvp.Key + "," + kvp.Value + ";");
        }
        if (_sb.Length > 0)
            return _sb.ToString().Substring(0, _sb.Length - 1);
        return string.Empty;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        SoldCallReportData scrd = scrdV;
        if (scrd == null)
            return;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        scrdV = scrd;
        scrd._Request.PageNumber = -1;
        scrd._Request.PageSize = -1;
        DataTable data = new DataTable();
        PagingData pd;
        data = scrd._func(_report, scrd._Request, out pd);
        Dictionary<string, string> dic = LoadChart(data);
        if (dic != null && dic.Count > 0)
            XmlGraphFileV = dic;
    }
    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }

    #endregion
}


