﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_ExternalSupplierReport : PageSetting
{
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    const string EXCEL_NAME = "External Supplier Report";
    protected void Page_Load(object sender, EventArgs e)
    {
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        if (!IsPostBack)
        {
            SetToolbox();
            ExecReport();
        }
        SetToolboxEvents();
    }

    private void ExecReport()
    {
        WebReferenceReports.PagingRequest _request = new WebReferenceReports.PagingRequest();
        _request.PageNumber = 1;
        _request.PageSize = ITEM_PAGE;
        RequestV = _request;
        DataResult dr = GetDataReport(_request);
        BindData(dr.data);
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
    }

    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (RequestV == null)
            return;
        WebReferenceReports.PagingRequest _request = RequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        DataResult dt = GetDataReport(_request);
        BindData(dt.data);
        LoadGeneralData(dt.TotalPages, dt.TotalRows, dt.CurrentPage);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        WebReferenceReports.PagingRequest _request = RequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataTable data = GetDataReport(_request).data;
        ToExcel te = new ToExcel(this, EXCEL_NAME);
        if (!te.ExecExcel(data))
            Update_Faild();
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        WebReferenceReports.PagingRequest _request = RequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataTable data = GetDataReport(_request).data;
        Session["data_print"] = data;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    private DataResult GetDataReport(WebReferenceReports.PagingRequest _request)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfExternalSupplierReportResponse result = null;
        try
        {
            result = _report.ExternalSupplierReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return null;
        }
        DataTable data = GetDataTable.GetDataTableFromListCorrectNumberInt(result.Value.Rows);
        DataResult dr = new DataResult() { data = data, CurrentPage = result.Value.CurrentPage, TotalRows = result.Value.TotalRows, TotalPages = result.Value.TotalPages };
        return dr;
        /*
        result.Value.Rows[0].Balance;
        result.Value.Rows[0].Name;
        result.Value.Rows[0].Number;
        result.Value.Rows[0].SupplierId;
        result.Value.Rows[0].VoucherBonusDepositSum;
        result.Value.Rows[0].VoucherDepositSum;
        result.Value.Rows[0].VoucherExtraBonusDepositSum;
         */
    }
    void ClearTable()
    {
        _GridView.DataSource = null;
        _GridView.DataBind();
        LoadGeneralData(0, 0, 0);
        _UpdatePanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = _TotalRows + " " + ToolboxReport1.RecordMached;
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    protected void BindData(DataTable data)
    {
        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_ReportName.Text);
    }
    WebReferenceReports.PagingRequest RequestV
    {
        get { return (ViewState["Request"] == null) ? null : (WebReferenceReports.PagingRequest)ViewState["Request"]; }
        set { ViewState["Request"] = value; }
    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
}