﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ZapCreditCard : PageSetting
{
    int PaymentId;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!userManagement.IsPublisher() && !userManagement.IsSupplier())
            Response.Redirect("LogOut.aspx");
        txt_CreditCardDetails.Attributes.Add("readonly", "readonly");
        if (!IsPostBack)
        {
            if (!int.TryParse(Request.QueryString["PaymentId"], out PaymentId) || PaymentId <= 0)
                ClientScript.RegisterClientScriptBlock(this.GetType(), "GoBack", "GoBack();", true);
            LoadYears();
            LoadMonths();
            lbl_SpanPayment.DataBind();
        }
        Header.DataBind();
    }
    private void LoadYears()
    {
        ddl_year.Items.Clear();
      //  ListItem _li = new ListItem(lbl_Choose.Text, string.Empty);
        ddl_year.Items.Add(GetChooseItem());
        int thisyear = DateTime.Now.Year;
        for (int i = thisyear; i < 2021; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
        //    li.Selected = i == thisyear;
            ddl_year.Items.Add(li);
        }
    }
    private ListItem GetChooseItem()
    {
        return new ListItem(lbl_Choose.Text, string.Empty);
    }
    private void LoadMonths()
    {
        int month = DateTime.Now.Month;
        ddl_month.Items.Clear();
        ddl_month.Items.Add(GetChooseItem());
        foreach (eMonths em in Enum.GetValues(typeof(eMonths)))
        {
            int num = (int)em;
            string _month = (num < 10) ? "0" + num : num.ToString();
            ListItem li = new ListItem(_month, num.ToString());
   //         li.Selected = num == month;
            ddl_month.Items.Add(li);
        }
    }
    protected string GetYear
    {
        get { return DateTime.Now.Year.ToString(); }
    }
    protected string GetMonth
    {
        get { return DateTime.Now.Month.ToString(); }
    }
    protected string GetCreditGuardUrl
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetCreditGuardUrl"); }
    }
    protected string GetCreditGuardToken
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetCreditGuardToken"); }
    }
    protected string ExecZapPayment
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/ExecZapPayment"); }
    }
    protected string GetPaymentId
    {
        get { return PaymentId.ToString(); }
    }
    protected string GetPaymentSuccess
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_PaymentSuccess.Text); }
    }
    protected string GetReplaceTokenSuccess
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_TokenReplaceSuccessfuly.Text); }
    }
    protected string IsPublisher
    {
        get { return userManagement.IsPublisher().ToString().ToLower(); }
    }
    protected string SupplierAfterPayment
    {
        get { return ResolveUrl("~/Management/professionalPayment.aspx"); }
    }
}