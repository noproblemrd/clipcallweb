﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class Publisher_Complaints : PageSetting, IPostBackEventHandler
{
    const string RUN_REPORT = "RanReport";

    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            LoadHelpDeskTypes();
            LoadStatus();
            SetToolBox();
            LoadUsers();
        }
        SetToolBoxEvents();
        lblStatus.DataBind();
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    private void SetToolBox()
    {
        _Toolbox.RemoveEdit();
        _Toolbox.RemoveAttach();
        _Toolbox.RemoveSave();
        string script = @"OpenIframe('Ticket.aspx?HelpDesk=" + HelpDeskTypeV + "'); return false;";
        _Toolbox.SetClientScriptToAdd(script);
    }
    private void SetToolBoxEvents()
    {
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
        _Toolbox.DeleteExec += new EventHandler(_Toolbox_DeleteExec);
        ScriptManager1.RegisterAsyncPostBackControl(_Toolbox.GetDeleteControl());
        //  _Toolbox.AddExec += new EventHandler(_Toolbox_AddExec);

    }

    void _Toolbox_DeleteExec(object sender, EventArgs e)
    {
        //       bool IsSelect = false;
        //       int indx = -1;
        List<Guid> list = new List<Guid>();
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
                list.Add(new Guid(((Label)row.FindControl("lbl_guid")).Text));
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.Result result = null;
        // WebReferenceSite.HelpDeskEntriesIds
        try
        {
            result = _site.DeleteHelpDeskEntries(list.ToArray());
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "UpdateSuccess();", true);
        Exec_report();
    }



    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordDisplay + "');", true);
            return;
        }
        //     PrintHelper.PrintWebControl(_GridView, dataV);
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }

        ToExcel to_excel = new ToExcel(this, "Complaints");
        if (!to_excel.ExecExcel(dataV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    private void LoadUsers()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllPublishersResponse result = new WebReferenceSite.ResultOfGetAllPublishersResponse();
        try
        {
            result = _site.GetAllPublishers();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        ddl_Owner.Items.Clear();
        ddl_Owner.Items.Add(new ListItem("", (Guid.Empty.ToString())));
        foreach (WebReferenceSite.PublisherData pd in result.Value.Publishers)
        {
            ddl_Owner.Items.Add(new ListItem(pd.Name, pd.AccountId.ToString()));
        }
        ddl_Owner.SelectedIndex = 0;
    }

    private void LoadStatus()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfSeverityAndStatusContainer result = new WebReferenceSite.ResultOfSeverityAndStatusContainer();
        try
        {
            result = _site.GetAllHelpDeskStatusesAndSeverities(HelpDeskTypeV);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Status.Items.Clear();
        ddl_Status.Items.Add(new ListItem(lbl_All.Text, Guid.Empty.ToString()));
        foreach (WebReferenceSite.HdStatusData hsd in result.Value.StatusList)
        {
            if (hsd.Inactive == false)
                ddl_Status.Items.Add(new ListItem(hsd.Name, hsd.Id.ToString()));
        }
    }

    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        Exec_report();
    }
    void Exec_report()
    {

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.GetHdEntriesRequest _request = new WebReferenceSite.GetHdEntriesRequest();

        _request.FromDate = FromToDate1.GetDateFrom;
        _request.ToDate = FromToDate1.GetDateTo;
        _request.HdTypeId = HelpDeskTypeV;
        Guid status_id = new Guid(ddl_Status.SelectedValue);
        if (status_id != Guid.Empty)
            _request.StatusId = status_id;
        string Complaint_id = txt_ComplaintID.Text.Trim();
        if (!string.IsNullOrEmpty(Complaint_id))
            _request.TicketNumber = Complaint_id;
        Guid user_id = new Guid(ddl_Owner.SelectedValue);
        if (user_id != Guid.Empty)
            _request.OwnerId = user_id;
        string _advertiser = txt_Advertisers.Text;

        if (!string.IsNullOrEmpty(_advertiser))
        {
            ReportWizardService rws = new ReportWizardService();
            _advertiser = rws.GetOneSuggest(_advertiser, "Advertiser");
            _request.SecondaryRegardingObjectName = _advertiser;
        }
        string FollowUpBefore = txt_FollowUpBefore.Text;
        if (!string.IsNullOrEmpty(FollowUpBefore))
        {
            _request.FollowUp = ConvertToDateTime.CalanderToDateTime(FollowUpBefore, siteSetting.DateFormat);
        }


        WebReferenceSite.ResultOfListOfHdEntryData result = new WebReferenceSite.ResultOfListOfHdEntryData();
        try
        {
            result = _site.GetHelpDeskEntries(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        DataTable data = new DataTable();
        data.Columns.Add("ID", typeof(string));
        data.Columns.Add("guid", typeof(Guid));
        data.Columns.Add("CreatedOn", typeof(string));
        data.Columns.Add("Title", typeof(string));
        data.Columns.Add("Status", typeof(string));
        data.Columns.Add("StatusId", typeof(Guid));
        data.Columns.Add("FollowUp", typeof(string));
        data.Columns.Add("Advertiser", typeof(string));
        data.Columns.Add("ScriptComplaint", typeof(string));

        foreach (WebReferenceSite.HdEntryData hed in result.Value)
        {
            DataRow row = data.NewRow();
            row["ID"] = hed.TicketNumber;
            row["guid"] = hed.Id;
            row["CreatedOn"] = string.Format(siteSetting.DateFormat, hed.CreatedOn)
                + " " + string.Format(siteSetting.TimeFormat, hed.CreatedOn);
            row["Title"] = hed.Title;
            row["Advertiser"] = hed.SecondaryRegardingObject;
            row["StatusId"] = hed.StatusId;
            string _status = string.Empty;
            foreach (ListItem li in ddl_Status.Items)
            {
                if (li.Value == hed.StatusId.ToString())
                {
                    _status = li.Text;
                    break;
                }
            }
            row["Status"] = _status;
            if (hed.FollowUp != null)
                row["FollowUp"] = string.Format(siteSetting.DateFormat, hed.FollowUp);

            string _script = "OpenIframe('Ticket.aspx?Ticket=" + hed.TicketNumber /*hed.Id.ToString()*/ +
                "&HelpDesk=" + HelpDeskTypeV.ToString() + "'); return false;";
            row["ScriptComplaint"] = _script;
            data.Rows.Add(row);
        }
        //      _GridView.DataSource = data;
        //       _GridView.DataBind();
        lbl_RecordMached.Text = _Toolbox.GetRecordMaches(data.Rows.Count);
        if (data.Rows.Count == 0)
        {
            _GridView.DataSource = null;
            _GridView.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + _Toolbox.NoRecordDisplay + "');", true);
            _UpdatePanel.Update();
        }
        else
            SetGridView(data);
        dataV = data;

        /*
        result.Value[0].CreatedById;
        result.Value[0].CreatedOn;
        result.Value[0].FollowUp;t.Value[0].Id;
        result.Value[0].
        result.Value[0].Description;InitiatorId;
        result.Value[0].InitiatorName;
        resul
        result.Value[0].OwnerId;
        result.Value[0].RegardingObjectId;
        result.Value[0].RegardingObjectName;
        result.Value[0].SeverityId;
        result.Value[0].StatusId;
        result.Value[0].TicketNumber;
        result.Value[0].Title;
        result.Value[0].TypeId;
         * */
    }
    void SetGridView(DataTable data)
    {
        _GridView.DataSource = data;
        _GridView.DataBind();
        StringBuilder sb = new StringBuilder();
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";
        foreach (GridViewRow _row in _GridView.Rows)
        {

            CheckBox cb = (CheckBox)_row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }
        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;

        _UpdatePanel.Update();
    }
    private void LoadHelpDeskTypes()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = new WebReferenceSite.ResultOfListOfGuidStringPair();
        try
        {
            result = _site.GetAllHelpDeskTypes();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            if (gsp.Name == eHelpDeskType.Complaint.ToString())
            {
                HelpDeskTypeV = gsp.Id;
                break;
            }
        }
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView(dataV);
    }
    protected void ID_complaint_click(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)(((LinkButton)sender).NamingContainer);
        Guid _id = new Guid(((Label)row.FindControl("lbl_ID")).Text);
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    Guid HelpDeskTypeV
    {
        get { return (ViewState["HelpDeskType"] == null) ? Guid.Empty : (Guid)ViewState["HelpDeskType"]; }
        set { ViewState["HelpDeskType"] = value; }
    }
    protected string GetWrongFormatDate
    {
        get { return FromToDate1.GetWrongDateFormat(); }
    }

    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }

    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        if (eventArgument == RUN_REPORT)
            Exec_report();
    }
    protected override void Render(HtmlTextWriter writer)
    {
        Page.ClientScript.RegisterForEventValidation(FromToDate1.GetBtnSubmit().UniqueID, RUN_REPORT);

        base.Render(writer);

    }
    protected string Get_run_report()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(FromToDate1.GetBtnSubmit(), RUN_REPORT);
        myPostBackOptions.PerformValidation = false;

        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
    }

    #endregion

    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }

}
