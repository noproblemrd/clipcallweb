﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class Publisher_SupplierMoneyReport : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(btnSubmit);
       
        if (!IsPostBack)
        {
            SetToolbox();
        }
        SetToolboxEvents();
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lblTitlePage.Text);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
   //     ScriptManager1.RegisterAsyncPostBackControl(ToolboxReport1.GetExcelButton);
        ScriptManager1.RegisterAsyncPostBackControl(ToolboxReport1.GetPrintButton);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        ToExcel te = new ToExcel(this, lblTitlePage.Text);
        DataTable data = GetTableForExport();
        data.Columns.Remove("SupplierId");
        if (!te.ExecExcel(data))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {

        DataTable data = GetTableForExport();
        _GridView.DataSource = data;
        _GridView.DataBind();
        StringWriter stringWriter = new StringWriter();

        // Put HtmlTextWriter in using block because it needs to call Dispose.
        using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
        {
            _GridView.RenderControl(writer);
        }

        //    Session["data_print"] = dataV;
        Session["grid_print"] = stringWriter.ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    DataTable GetTableForExport()
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfSupplierMoneyReportResponse result = null;
        try
        {
            result = _report.SupplierMoneyReportToExcel();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return null;
        }
        DataTable data = GetDataTable.GetDataTableFromListCorrectNumberInt(result.Value);
        
        return data;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int num;
        string _num = txt_SupplierNumber.Text;
        if (!string.IsNullOrEmpty(_num) && !int.TryParse(_num, out num))
            return;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfSupplierMoneyReportResponse result = null;
        try
        {
            result = _report.SupplierMoneyReport(_num);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if (result.Value.SupplierName == "Supplier not found")
        {
            _GridView.DataSource = null;
            _GridView.DataBind();
            _UpdatePanel.Update();
            dataV = null;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SupplierNotFound", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_SupplierNotFound.Text) + "');", true);
            return;
        }
        List<WebReferenceReports.SupplierMoneyReportResponse> list = new List<WebReferenceReports.SupplierMoneyReportResponse>();
        list.Add(result.Value);
        DataTable data = GetDataTable.GetDataTableFromListCorrectNumberInt(list);
        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();
        dataV = data;
    }
    
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
     
}

/*
        public int DepositReal { get; set; }
        public int DepositBonus { get; set; }
        public int UsedReal { get; set; }
        public int UsedBonus { get; set; }
        public int LeftReal { get; set; }
        public int LeftBonus { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string SupplierNumber { get; set; }

*/