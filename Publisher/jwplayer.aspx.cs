﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_jwplayer : System.Web.UI.Page
{
    protected string videoUrl { get; set; }
    protected string imgUrl { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AppendHeader("Access-Control-Allow-Origin", "*.cloudfront.net");
 //        Response.AppendHeader("Access-Control-Allow-Credentials", "true");
       

        GenerateVideoLink gvl = GenerateVideoLink.GetVideoLinkPrams(Request.QueryString);

        if (string.IsNullOrEmpty(gvl.GetVideoUrl))
        {
            return;
        }
        videoUrl = gvl.GetVideoUrl;
        imgUrl = gvl.GetPicPreviewUrl;
    }

}