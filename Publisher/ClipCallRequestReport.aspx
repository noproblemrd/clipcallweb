﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ClipCallRequestReport.aspx.cs" Inherits="Publisher_ClipCallRequestReport" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">


<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
 <link href="../Formal/jquery-ui-1.8.16.custom.css" type="text/css" rel="Stylesheet" />
    <link media="screen" rel="stylesheet" href="../jquery/colorbox.css" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"/>

<script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>
<script  type="text/javascript" src="../Calender/Calender.js"></script>
    <style type="text/css">
        #main
        {
            margin-left: 150px !important;
            margin-top: 0 !important;
        }
     .assdot{
          height: 25px;
        width: 25px;        
        border-radius: 50%;
        display: inline-block;
     }
    ._Empty {
       
        background-color: #ffffff;
       
    }
    ._Green{
        
        background-color: #66ff33;
       
    }
    ._Red{
      
        background-color: #ff3300;
        
    }
    ._Black{
       
        background-color:  #000000;
       
    }
    ._Orange{
       
        background-color:  #ff9900;
       
    }
    .assessmentBorder{
        border-style: solid;
        border-width:2px;
       
    }
    .assessmentBorder label{
         padding-left:5px;
    }
    .BorderEmpty {
       
        background-color: #ffffff;
       
    }
    .BorderGreen{
         border-color: #66ff33;
       
    }
    .BorderRed{
      
        border-color: #ff3300;
        
    }
    .BorderBlack{
       
        border-color:  #000000;
       
    }
    .BorderOrange{
       
        border-color:  #ff9900;
       
    }

    </style>


  <script src="//code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js" type="text/javascript"></script>
<% /* 
          <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>

          <script type="text/javascript" src="../scripts/jquery-ui-1.8.17.custom.autocomplete.min.js"></script>
          */ %>
<script src="../jquery/jquery.colorbox1.3.19.js" type="text/javascript" ></script>	
    <script src="localTimeDisplay.js" type="text/javascript"></script>   

<script type="text/javascript">
    
    var message;
    window.onload = appl_init;
    
    function appl_init() {        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
        message = document.getElementById('<%#Hidden_IfToExite.ClientID%>').value;
        
    }
    function BeginHandler()
    {       
  //     alert('in1');
        showDiv();
    }
    function EndHandler()
    {
        //      alert('out1');       
        hideDiv();
    }
    function OpenAdvertiserIframe(_path)
    {
        $(document).ready(function(){$.colorbox({href:_path,width:'95%', height:'90%', iframe:true});});
    }
    function openAdvIframe(_id)
    {            
         var url = "ConsumerDetails.aspx?Consumer="+_id;
         $(document).ready(function () { $.colorbox({ href: url, width: '1000px', height: '90%', iframe: true }) });
    }
    /***   IFRAME  ****/
    /*
    function CloseIframe(_id)
    {
        
       $find('_modal').hide();
       document.getElementById("<# panel_iframe.ClientID %>").className = "popModal_del2_ie popModal_del2_hide";
       document.getElementById("<# _iframe.ClientID %>").src = "javascript:void(0);";
             
    }
   
    var RequestTicketWins = [];
    function GetWin(_id) {
        for(var i = 0; i < RequestTicketWins.length; i++){
            if (RequestTicketWins[i].id == _id)
                return RequestTicketWins[i].win;
            
        }
        return null;
    }
    */
    function OpenIframe(_path)
    {
        /*
       document.getElementById("<# panel_iframe.ClientID %>").className = "popModal_del2_ie";
       document.getElementById("<# _iframe.ClientID %>").src = _path;
       HideIframeDiv();  
       $find('_modal').show();
        */     
        var _leadWin = window.open(_path, "LeadDetails", "width=1200, height=650,scrollbars=1");
       _leadWin.focus();
    }
    function Reload() {
  //      window.location.href = '<# ThisPath %>';
    }
    /*
    function ShowIframeDiv()
    {
        document.getElementById("divIframeLoader").style.display = "none";
       document.getElementById("div_iframe_main").style.display = "block";
    }
    function HideIframeDiv()
    {
    
        document.getElementById("divIframeLoader").style.display = "block";
       document.getElementById("div_iframe_main").style.display = "none";
   }
   */
   var _noproblem = _noproblem || {};
   _noproblem.IsSelected = false;
    var _dialog;
    var _dialogAssessment;
    $(document).ready(function () {
        _dialog = $("#div_RejectReason").dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            title: "Reject Reason",
            buttons: {
                CANCEL: function () {
                    _dialog.dialog('close');
                },
                REJECT: function () {
                    OnRejectedClick();
                }
            },
            draggable: false,
            beforeClose: function (event, ui) {

            },
        });

        _dialogAssessment = $("#div_assessment").dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            title: "Assessment",
            buttons: {
                CANCEL: function () {
                    _dialogAssessment.dialog('close');
                },
                OK: function () {
                    SetAssessmentService();
             //       OnRejectedClick();
                }
            },
            draggable: false,
            beforeClose: function (event, ui) {

            },
        });
    });
  
   function ConfirmRejectIncident(_a, toConfirm)
   {
       //div_review_loader
       if (toConfirm) {
           var AreYouShure = confirm("Are you sure you want to " + (toConfirm ? "confirm" : "reject") + "?");
           if (!AreYouShure)
               return;
           _ConfirmRejectIncident(_a, toConfirm, "-1");
           return;
       }
       else {
           _aReject = _a;
           _dialog.dialog('open');
       }
       
      
    }
    var _aReject;
    function OnRejectedClick() {
        var list = document.getElementById("<%# rbl_RejectReason.ClientID %>"); 
                var inputs = list.getElementsByTagName("input");
                var rejectReason;
                for (var i = 0; i < inputs.length; i++) {
                    if (inputs[i].checked) {
                    rejectReason = inputs[i].value;
                    break;
                }
            }
        if (rejectReason)
            _ConfirmRejectIncident(_aReject, false, rejectReason);
        else {
            _dialog.dialog('close');
        }
    }
    function _ConfirmRejectIncident(_a, toConfirm, rejectReason) {
        var $div_parent = $(_a).parents('div.div_review');
        var $div_loader = $div_parent.find('div.div_review_loader');
        $div_loader.show();
        var spanResult = $div_parent.find('span.span_review')
        var incidentId = $(_a).parents('tr').find('input[type="hidden"]').val();
        $(_a).parents('div.div_review').find('div.div_review_btn').hide();
        $.ajax({
            url: "<%# GetConfirmRejectIncidentUrl %>",
            data: "{ 'incidentId': '" + incidentId + "', 'toConfirm': " + toConfirm + ", 'rejectReason': " + rejectReason + "}",
           dataType: "json",
           type: "POST",
           contentType: "application/json; charset=utf-8",
           dataFilter: function (data) { return data; },
           success: function (data) {
               var _data = eval("(" + data.d + ")");
               spanResult.html(_data.message);
               if (!toConfirm && _data.isSuccess) {
                   var tr_parent = $div_parent.parents('tr');
                   tr_parent.find('div.div_ManualAar').hide();
               }

           },
           error: function (XMLHttpRequest, textStatus, errorThrown) {
               alert(textStatus);
           },
           complete: function (jqXHR, textStatus) {
               //  HasInHeadingRequest = false;
               $div_loader.hide();
               _dialog.dialog('close');

           }

       });
    }
    function CleanAarSender() {
        $('#h_SendAarIncidentId').val('');
        $('#<%# txt_SupplierName.ClientID %>').val('');
        $('#<%# txt_SupplierPhone.ClientID %>').val('');
        $('#<%# lbl_SendAarResponse.ClientID %>').html('');
        $('#a_SendAar').show();
        $('#<%# img_SendAar_loader.ClientID %>').hide();
    }
    function OpenSendAar(_a) {
        CleanAarSender();
        var incidentId = $(_a).parents('tr').find('input[type="hidden"]').val();
        $('#h_SendAarIncidentId').val(incidentId);
        // $('div.div_send_aar').show();
        $find('send_aar').show();
    }
    function CloseAarSender() {
        $find('send_aar').hide();
    }
    function SendAar() {
        if (!Page_ClientValidate('SendAar'))
            return;
        var incidentId = $('#h_SendAarIncidentId').val();
        if (incidentId.length == 0)
            return;        
        $('#a_SendAar').hide();
        $('#<%# img_SendAar_loader.ClientID %>').show();

       
        
        var supplierName = $('#<%# txt_SupplierName.ClientID %>').val();
        var supplierPhone = $('#<%# txt_SupplierPhone.ClientID %>').val();

        $.ajax({
            url: "<%# GetSendAarSupplierUrl %>",
            data: "{ 'incidentId': '" + incidentId + "', 'supplierName': '" + supplierName + "','phone': '" + supplierPhone  + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var _data = eval("(" + data.d + ")");
                $('#<%# lbl_SendAarResponse.ClientID %>').html(_data.message);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                $('#a_SendAar').show();
                $('#<%# img_SendAar_loader.ClientID %>').hide();               
            }

        });
    }
   
    function Relaunch(_a) {
        var incidentId = $(_a).parents('tr').find('input[type="hidden"]').val();
        $(_a).hide();
        var $div_parent = $(_a).parents('div.div_relaunch');
        $div_parent.find('div.div_relaunch_loader').show();
        $.ajax({
            url: "<%# GetRelaunchUrl %>",
            data: "{ 'incidentId': '" + incidentId + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var _data = eval("(" + data.d + ")");
                $div_parent.find('span.span_relaunchResponse').html(_data.isSuccess ? 'Success' : 'Failed');
                if (_data.status != '<%# AllStatus %>')
                {
                    $(_a).parents('tr').find('span.lbl_callStatus').html(_data.status);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                $div_parent.find('div.div_relaunch_loader').hide();
            }

        });
    }
    function _CreateExcel() {
        var path = '<%# GetCreateExcelUrl %>';
        var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"<%# EXCEL_NAME %>"}';
        $.ajax({
            url: path,
            data: _data,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if (data.d.length == 0) {
                    return;
                }
                var _iframe = document.createElement('iframe');
                _iframe.style.display = 'none';
                _iframe.src = '<%# DownloadExcelUrl %>' + '?fn=' + data.d;
                document.getElementsByTagName('body')[0].appendChild(_iframe);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;

            }

        });
    }
    function ExecuteAar(_a) {
        //div_review_loader
        var AreYouShure = confirm("Are you sure you want to execute AAR?");
        if (!AreYouShure)
            return;
        var $div_parent = $(_a).parents('div.div_ManualAar');
        var $div_loader = $div_parent.find('div.div_ManualAar_loader');
        $div_loader.show();
        var spanResult = $div_parent.find('span.span_ManualAar')
        var incidentId = $(_a).parents('tr').find('input[type="hidden"]').val();
        $div_parent.find('div.div_ManualAar_btn').hide();
        $.ajax({
            url: "<%# GetExecuteManualAarUrl %>",
            data: "{ 'incidentId': '" + incidentId + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
               // var _data = eval("(" + data.d + ")");
                spanResult.html(data.d);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                $div_loader.hide();
            }
        });
    }
    function Unbook(_a) {
        //div_review_loader
        var AreYouShure = confirm("Are you sure? this will stop/refund the payment process and cancel request booking");
        if (!AreYouShure)
            return;
        var $div_parent = $(_a).parents('div.div_unbook');
        var $div_loader = $div_parent.find('div.div_unbook_loader');
        $div_loader.show();
        var spanResult = $div_parent.find('span.span_unbook_response')
        var incidentId = $(_a).parents('tr').find('input[type="hidden"]').val();
        $div_parent.find('div.div_unbook_btn').hide();
        $.ajax({
            url: "<%# GetUnbookProject %>",
            data: "{ 'incidentId': '" + incidentId + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                // var _data = eval("(" + data.d + ")");
                spanResult.html(data.d);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                $div_loader.hide();
            }
        });
    }

    function SetPriority(_elem) {
        //div_review_loader
 //       var AreYouShure = confirm("Are you sure? this will stop/refund the payment process and cancel request booking");
 //       if (!AreYouShure)
        //          return;
        /*
        var $div_parent = $(_elem).parents('div.div_unbook');
        var $div_loader = $div_parent.find('div.div_unbook_loader');
        $div_loader.show();
        var spanResult = $div_parent.find('span.span_unbook_response')
        var incidentId = $(_elem).parents('tr').find('input[type="hidden"]').val();
        $div_parent.find('div.div_unbook_btn').hide();
        */
        var incidentId = $(_elem).parents('tr').find('input[type="hidden"]').val();
        var toImportant = $(_elem).attr("alt") == "<%# LowImportance %>";
        showDiv();
        $.ajax({
            url: "<%# GetChangeCasePriority %>",
            data: "{ 'incidentId': '" + incidentId + "', 'toImportant': " + toImportant + "}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                 var _data = eval("(" + data.d + ")");
                 if (_data.IsSuccess) {
                     $(_elem).attr("src", (_data.CurrentIncidentPriority ? "<%# GetHighImportanceUrl %>" : "<%# GetLowImportanceUrl %>"));
                     $(_elem).attr("alt", (_data.CurrentIncidentPriority ? "<%# HighImportance %>" : "<%# LowImportance %>"));
                 }
                 else
                     alert('Failed');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                hideDiv();
            }
        });
        return false;
    }
    function SetAssessment(_elem) {
        link_assessment = _elem;
        var incidentId = $(_elem).parents('tr').find('input[type="hidden"]').val();
        $elemassessment = $(_elem).parents('td').find('span');;
        document.querySelector('#assessment_incidentid').value = incidentId;
        SetAssessmentValue($elemassessment.html());
        _dialogAssessment.dialog('open');
        return false;
    }
    var $elemassessment;
    var link_assessment;
    function SetAssessmentValue(_value) {
        //for()
        
        var div_assessment = document.querySelector('#div_assessment');
        var inputs = div_assessment.querySelectorAll('input');
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].value == _value) {
                //         inputs[i].parentElement.querySelector('')
                inputs[i].checked = true;
                break;
            }
        }
//        _dialogAssessment.dialog('close');
    }
    function SetAssessmentService() {
         var div_assessment = document.querySelector('#div_assessment');
        var inputs = div_assessment.querySelectorAll('input');
        var _value;
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked) {
                _value = inputs[i].value;
                break;
            }

        }
        var incidentId = document.querySelector('#assessment_incidentid').value;
        showDiv();
        $.ajax({
            url: "<%# GetAssessmentService %>",
            data: "{ 'incidentId': '" + incidentId + "', 'value': '" + _value + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var _data = eval("(" + data.d + ")");
                if (_data.IsSuccess) {
                    if (_data.Assessment!=null) {
                        $elemassessment.html(_data.Assessment);
                        link_assessment.className = "assdot _" + _data.Assessment;
                    }
                    else {
                        $elemassessment.html('');
                        link_assessment.className = "assdot _Empty";
                    }
                }
                else
                    alert('Failed');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                 _dialogAssessment.dialog('close');
                hideDiv();
            }
        });
        return false;
    }
</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
          
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	       
               <div class="main-inputs" runat="server" id="PublisherInput">					
	                <asp:UpdatePanel id="_UpdatePanelSearch" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>   
                        <div id="div_experties" class="form-field" runat="server">
					            <label for="form-subsegment" class="label" runat="server" id="lblExperties" >Heading</label>
                            <asp:DropDownList ID="ddl_Heading" runat="server" CssClass="form-select">
                            </asp:DropDownList>
					            
			            </div>
            			 
			            <div class="form-field">
                          <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Request Id</label>
			              <asp:TextBox ID="txtRequestId" CssClass="form-text" runat="server" ></asp:TextBox>										
                          <asp:CompareValidator runat="server" Type="Integer"  Operator="DataTypeCheck" ControlToValidate="txtRequestId" Display="Dynamic">
                             <asp:Label ID="lbl_RequestIdError" runat="server" Text="Must be number" Visible="true"></asp:Label>
                          </asp:CompareValidator> 
                       </div>            
                    
                    </ContentTemplate>
		            </asp:UpdatePanel>
		            <div class="clear"></div>
		            
                    <div class="form-field" runat="server" id="div_NumberOfAdvertisersProvided" style="margin-bottom:0;">
                        <asp:Label ID="lbl_NumSupplier" runat="server" Text="No. Provided Advertisers" CssClass="label"></asp:Label>
                        <asp:TextBox ID="txt_NumSupplier" runat="server" CssClass="form-text"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator_NumSupplier" runat="server" ErrorMessage="Must be numeric more than zero" CssClass="error-msg"
                         ControlToValidate="txt_NumSupplier" Type="Integer" MaximumValue="100" MinimumValue="0"></asp:RangeValidator>

                    </div>
                     <div class="div_channe" runat="server" id="div_affiliate" visible="false">
                        <asp:Label ID="lbl_channe" CssClass="label" runat="server" Text="Channel"></asp:Label>                                    
                        <asp:DropDownList ID="ddl_channe" CssClass="form-select" runat="server" AutoPostBack="true"
                              Enabled="false"></asp:DropDownList>			
			        </div>
                   <div class="div_channe" runat="server" id="div_CustomerLeadStatus">
                        <asp:Label ID="lbl_CustomerLeadStatus" CssClass="label" runat="server" Text="Customer Lead Status"></asp:Label>                                    
                        <asp:DropDownList ID="ddl_CustomerLeadStatus" CssClass="form-select" runat="server"></asp:DropDownList>			
			        </div>

                   <div class="form-field form-WidthDuration" runat="server" id="div_AppType" style="top:80px; margin-bottom:0;">
                        <asp:Label ID="lbl_AppType" runat="server" Text="App Type" CssClass="label"></asp:Label>
                        <asp:DropDownList ID="ddl_AppType" CssClass="form-select" runat="server" Enabled="false"></asp:DropDownList>

                    </div>
                    <div class="form-field form-WidthDuration" runat="server" id="div_AarInitiated" style="top:80px;left:510px; margin-bottom:0;"> 
                         <asp:Label ID="lbl_AarInitiated" runat="server" Text="AAR Initiated" CssClass="label"></asp:Label>
                        <asp:DropDownList ID="ddl_AarInitiated"  CssClass="form-select" runat="server">
                        </asp:DropDownList>
                    </div>
                   <div class="clear"></div>
                   
		            <div class="form-field form-group" runat="server" id="div_RequestStatus" > 
                         <asp:Label ID="lbl_RequestStatus" runat="server" Text="Request Status" CssClass="label"></asp:Label>
                        <asp:DropDownList ID="ddl_RequestStatus" CssClass="form-select" runat="server"></asp:DropDownList>
                    </div>
                   
                    <div class="form-field" runat="server" id="div_Region" > 
                         <asp:Label ID="lbl_Region2" runat="server" Text="Region" CssClass="label"></asp:Label>
                        <asp:TextBox ID="txt_Region" runat="server" CssClass="form-text" Enabled="false"  style="width:320px;"></asp:TextBox>
                    </div>
                   <div class="form-field" runat="server" id="div_IncludeQa" style="margin-bottom:0; position: absolute; left: 510px;">
                        <asp:Label ID="lbl_IncludeQaTest" runat="server" Text="Include QA" CssClass="label"></asp:Label>
                       <asp:CheckBox ID="cb_IncludeQaTest" runat="server" />

                    </div>
                    <div class="form-field" runat="server" id="div2" style="top:0; margin-bottom:0;">
                            <asp:Label ID="Label44" runat="server" Text="Date selector" CssClass="label"></asp:Label>
                         <asp:RadioButtonList ID="rbl_date" runat="server" RepeatDirection="Vertical" >
                     
                         </asp:RadioButtonList>

                     </div> 
                   <div class="form-field" runat="server" id="div1" > 
                         <asp:Label ID="Label14" runat="server" Text="Origin" CssClass="label"></asp:Label>
                       <asp:DropDownList ID="ddl_Origin" CssClass="form-select" runat="server"></asp:DropDownList>
                    </div>
                   <div class="form-field form-WidthDuration" runat="server" id="div3" style="top:243px;" > 
                         <asp:Label ID="Label39" runat="server" Text="Warning" CssClass="label"></asp:Label>
                       <asp:DropDownList ID="ddl_warning" CssClass="form-select" runat="server"></asp:DropDownList>
                    </div>
                   <div class="form-field form-WidthDuration" runat="server" id="div4" style="top:243px;left: 510px;" > 
                         <asp:Label ID="Label40" runat="server" Text="Pro name / phone / email" CssClass="label"></asp:Label>
                       <asp:TextBox ID="txt_proName" runat="server" CssClass="form-text" ></asp:TextBox>
                    </div>
		           </div> 
        		   
                    <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>        
                 
		  
    		
    	
    	</div>
    	<div style="clear:both;"></div>
    	
	        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	            <ContentTemplate>	
                    <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
	                <div id="Table_Report"  runat="server" >
                              
                                <asp:Repeater runat="server" ID="_reapeter" OnItemDataBound="_reapeter_ItemDataBound" 
                                    >
                                    <HeaderTemplate>
                                        <table class="data-table">
                                        
                                            <thead>
                                            <tr>
                                                <th><asp:Label ID="Label8000" runat="server" Text="Priority"></asp:Label></th>
                                                <th><asp:Label ID="Label33" runat="server" Text="Assessment"></asp:Label></th>
                                                <th><asp:Label ID="lbl_Date" runat="server" Text="Created on"></asp:Label></th>
                                                <th><asp:Label ID="Label24" runat="server" Text="Relaunch date"></asp:Label></th>
                                                <th><asp:Label ID="Label666" runat="server" Text="Last Update"></asp:Label></th>
                                                <th><asp:Label ID="Label3" runat="server" Text="Consumer last see"></asp:Label></th>
                                                <th><asp:Label ID="Label29" runat="server" Text="Last bot"></asp:Label></th>
			                                    <th><asp:Label ID="Label51" runat="server" Text="Case number"></asp:Label></th>
                                                <th><asp:Label ID="Label31" runat="server" Text="Notes"></asp:Label></th>
                                                <th><asp:Label ID="Label37" runat="server" Text="Chat warning"></asp:Label></th>
                                                <th><asp:Label ID="Label38" runat="server" Text="Proximity distance"></asp:Label></th>
                                                <th><asp:Label ID="Label512" runat="server" Text="Flow"></asp:Label></th>
			                                    <th><asp:Label ID="Label001" runat="server" Text="Region"></asp:Label></th>			                                    
			                                    <th><asp:Label ID="Label52" runat="server" Text="Category"></asp:Label></th>			                                    
			                                    <th><asp:Label ID="Label53" runat="server" Text="Consumer"></asp:Label></th>			                                    
                                                <th><asp:Label ID="Label28" runat="server" Text="Consumer Projects"></asp:Label></th>
                                                <th><asp:Label ID="Label27" runat="server" Text="Membership"></asp:Label></th>	
                                                <th><asp:Label ID="Label5001" runat="server" Text="Consumer phone"></asp:Label></th> 
                                                <th><asp:Label ID="Label2800" runat="server" Text="Is Private"></asp:Label></th>
                                                <th><asp:Label ID="Label5003" runat="server" Text="Lead status"></asp:Label></th>
			                                    <th><asp:Label ID="Label2" runat="server" Text="Number of Requested Advertisers"></asp:Label></th>
			                                    <th><asp:Label ID="Label5" runat="server" Text="No. Provided Advertisers" ></asp:Label></th>                          
                                                <th><asp:Label ID="Label1112" runat="server" Text="Call status"></asp:Label></th>
                                                <th><asp:Label ID="Label212" runat="server" Text="Relaunch/Stop"></asp:Label></th>
                                                <th><asp:Label ID="Label142" runat="server" Text="Video"></asp:Label></th>
                                                <th><asp:Label ID="Label992" runat="server" Text="Submit/Reject"></asp:Label></th>
                                                 <th><asp:Label ID="Label4412" runat="server" Text="Send new AAR"></asp:Label></th>
                                                <th><asp:Label ID="Label6512" runat="server" Text="Relaunch"></asp:Label></th>
                                                <th><asp:Label ID="Label7001" runat="server" Text="Interactions"></asp:Label></th>
                                                <th><asp:Label ID="Label7002" runat="server" Text="Execute AAR"></asp:Label></th>
                                                <th><asp:Label ID="Label7003" runat="server" Text="UnBook quote"></asp:Label></th>
                                                <th><asp:Label ID="Label8" runat="server" Text="Origin"></asp:Label></th>
                                                <th><asp:Label ID="Label18" runat="server" Text="OS"></asp:Label></th>
                                                <th><asp:Label ID="Label19" runat="server" Text="OS version"></asp:Label></th>
                                                <th><asp:Label ID="Label20" runat="server" Text="App Version"></asp:Label></th>
                                                <th><asp:Label ID="Label15" runat="server" Text="Carrier"></asp:Label></th>
			                                </tr>
                                            </thead>
			                            </HeaderTemplate>
	                                    <ItemTemplate>
                                            <tr id="tr_show" runat="server">
                                                <td>
                                                    <asp:ImageButton ID="ImageButton1" runat="server"  AlternateText='<%# Bind("ImportantAlt") %>' ImageUrl='<%# Bind("ImportantUrl") %>' OnClientClick="return SetPriority(this);" />   

			                                     </td> 
                                                <td>
                                                    <asp:LinkButton ID="LinkButton1" runat="server"  ToolTip='<%# Bind("AssessmentAlt") %>' CssClass='<%# Bind("AssessmentCss") %>' OnClientClick="return SetAssessment(this);" ></asp:LinkButton>
                                                    <asp:Label ID="Label34" runat="server" Text='<%# Bind("AssessmentValue") %>' style="display:none;"></asp:Label>
                             

			                                     </td> 
			                                     <td>
			                                        <asp:Label ID="lbl_name" runat="server" Text='<%# Bind("CreatedOn") %>' CssClass="date-time-utc"></asp:Label>      
			                                     </td> 
                                                 <td>
			                                        <asp:Label ID="Label25" runat="server" Text='<%# Bind("RelaunchDate") %>' CssClass="date-time-utc"></asp:Label>      
			                                     </td> 
                                                <td>
			                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("LastUpdate") %>' CssClass="date-time-utc"></asp:Label>      
			                                     </td>            	
                                                 <td>
			                                        <asp:Label ID="Label17" runat="server" Text='<%# Bind("ConsumerLastActive") %>'  CssClass="date-time-utc"></asp:Label>      
			                                     </td>
                                                <td>
			                                        <asp:Label ID="Label30" runat="server" Text='<%# Bind("LastBotMessage") %>'  CssClass="date-time-utc"></asp:Label>      
			                                     </td>
			                                     <td>
                                                    
                                                     <a id="a_Case" runat="server" href='<%# Bind("CaseScript") %>'>
			                                            <asp:Label ID="Label9" runat="server" Text='<%# Bind("CaseNumber") %>'></asp:Label>
						                              </a>
			                                        
                                                     <asp:HiddenField ID="hf_incidentId" runat="server" Value='<%# Bind("IncidentId") %>' />    
			                                     </td>
                                                <td>
			                                        <asp:Label ID="Label32" runat="server" Text='<%# Bind("Note") %>' ToolTip='<%# Bind("NoteToolTip") %>'></asp:Label>      
			                                     </td>
                                                 <td>
			                                        <asp:Label ID="Label35" runat="server" Text='<%# Bind("ChatWarning") %>'></asp:Label>      
			                                     </td>
                                                 <td>
			                                        <asp:Label ID="Label36" runat="server" Text='<%# Bind("ProximityDistance") %>'></asp:Label>      
			                                     </td>
                                                <td>
			                                        <asp:Label ID="Label288" runat="server" Text='<%# Bind("Flow") %>'></asp:Label>      
			                                     </td>
			                                     <td>
			                                        <asp:Label ID="Label10" runat="server" Text='<%# Bind("RegionName") %>'></asp:Label>      
			                                     </td>
			                                     <td>
			                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Heading") %>'></asp:Label>      
			                                     </td>			                                     
			                                     <td>
                                                     <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Bind("Consumer") %>' Target="_blank" NavigateUrl='<%# Bind("ConsumerRequestReport") %>'></asp:HyperLink>
			                                     
                                                    <asp:Label ID="Label888" runat="server" Text='<%# Bind("CustomerId") %>' style="display:block;"></asp:Label> 
			                                     </td>
                                                 <td>
			                                        <asp:Label ID="Label279" runat="server" Text='<%# Bind("ConsumerProjects") %>'></asp:Label>      
			                                     </td>
                                                 <td>
			                                        <asp:Label ID="Label26" runat="server" Text='<%# Bind("Membership") %>'></asp:Label>      
			                                     </td>	
                                                <td>
                                                    <asp:HyperLink ID="HyperLink2" runat="server" Text='<%# Bind("ConsumerPhone") %>' Target="_blank" NavigateUrl='<%# Bind("ConsumerRequestReport") %>'></asp:HyperLink>
			                                     </td>
                                                
                                                <td>
			                                      <asp:Label ID="Label2801" runat="server" Text='PRIVATE' ForeColor="Red" Visible='<%# Bind("IsPrivate") %>'></asp:Label>     
			                                     </td>

                                                <td>
			                                      <asp:Label ID="Label5005" runat="server" Text='<%# Bind("CustomerLeadStatus") %>'></asp:Label>     
			                                     </td>
			                                     <td>
                                                   <asp:Label ID="Label4" runat="server"  Text='<%# Bind("NumberRequestedAdvertisers") %>'></asp:Label>
			                                     </td>			                                     
			                                     <td>
                                                   <asp:Label ID="Label7" runat="server" Text='<%# Bind("NumberAdvertisersProvided") %>'></asp:Label>
			                                     </td>	                                  
                                                 <td >
                                                   <asp:Label ID="lbl_callStatus" CssClass="lbl_callStatus" runat="server" Text='<%# Bind("CallStatus") %>'></asp:Label>
			                                     </td>  
                                                <td >
                                                   <asp:Label ID="Label11" CssClass="lbl565" runat="server" Text='<%# Bind("RelaunchStopBy") %>'></asp:Label>
			                                     </td>                                                
                                                 <td>
                                                    <a id="a1" runat="server" href='<%# Bind("VideoLink") %>' target="_blank" visible='<%# Bind("HasVideo") %>'>
                                                        <asp:Label ID="Label192" runat="server" Text="Video"></asp:Label>
                                                    </a>
			                                     </td>
                                                <td>
                                                    <div runat="server" id="div_review" class="div_review" visible='<%# Bind("CanReview") %>'>
                                                        <div class="div_review_btn">
                                                            <a id="a_Submit2" runat="server" href='<%# Bind("ConfirmIncident") %>' visible='<%# Bind("IsNotConfirmIncident") %>' > 
                                                                Confirm                                                      
                                                            </a>
                                                            <a id="a_Submit" runat="server" href="javascript:void(0);" onclick="ConfirmRejectIncident(this, true);" visible='<%# Bind("IsConfirmIncident") %>'> 
                                                                Confirm                                                      
                                                            </a>
                                                             <a id="a_reject" runat="server" href="javascript:void(0);" onclick="ConfirmRejectIncident(this, false);">  
                                                                 Reject                                                     
                                                            </a>
                                                        </div>
                                                        <div style="text-align:center; display:none;" class="div_review_loader">
                                                            <asp:Image ID="img_review_loader" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                                        </div>
                                                        <span class="span_review" style="color:red;"></span>
                                                    </div>
			                                     </td>
                                                <td>
                                                    <div runat="server" id="div_sendAar" class="div_sendAar" visible='<%# Bind("CanSendAar") %>'>
                                                     
                                                        <a id="a_sendAar" runat="server" onclick="OpenSendAar(this);" href="javascript:void(0);">
                                                            <asp:Label ID="Label2212" runat="server" Text="Send"></asp:Label>
                                                        </a>
                                                       
                                                       
                                                    </div>

			                                     </td>
                                                 <td>
                                                    <div runat="server" id="div_relaunch" class="div_relaunch" visible='<%# Bind("CanMakeRelaunch") %>'>
                                                        <a id="a_relaunch" runat="server" onclick="Relaunch(this);" href="javascript:void(0);">
                                                            <asp:Label ID="Label6612" runat="server" Text="Relaunch"></asp:Label>
                                                        </a>
                                                        <div style="text-align:center; display:none;" class="div_relaunch_loader">
                                                            <asp:Image ID="img_relaunch" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                                        </div>
                                                        <span class="span_relaunchResponse" style="color:red;"></span>
                                                    </div>
			                                     </td>

                                                <td>
                                                    
                                                     <a id="a_Interactions" runat="server" href='<%# Bind("Interactions") %>'>
			                                            <asp:Label ID="Label7002" runat="server" Text="Trace"></asp:Label>
						                              </a>
			                                        
			                                     </td>
                                                <td>
                                                    <div runat="server" id="div_ManualAar" class="div_ManualAar"   visible='<%# Bind("CanUseManualAar") %>'>
                                                        <div class="div_ManualAar_btn">
                                                            <a id="a7003" runat="server" href="javascript:void(0);" onclick="ExecuteAar(this);"> 
                                                                Execute                                                      
                                                            </a>                                                            
                                                        </div>
                                                        <div style="text-align:center; display:none;" class="div_ManualAar_loader">
                                                            <asp:Image ID="Image7004" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                                        </div>
                                                        <span class="span_ManualAar" style="color:red;"></span>
                                                    </div>
			                                     </td>
                                                <td>
                                                    <div runat="server" id="div_unbook" class="div_unbook" visible='<%# Bind("CanUnbook") %>'>
                                                        <div class="div_unbook_btn">
                                                            <a id="a_unbook" runat="server" href="javascript:void(0);" onclick="Unbook(this);"> 
                                                                Unbook                                                      
                                                            </a>                                                            
                                                        </div>
                                                        <div style="text-align:center; display:none;" class="div_unbook_loader">
                                                            <asp:Image ID="img_unbook" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                                        </div>
                                                        <span class="span_unbook_response" style="color:red;"></span>
                                                    </div>
			                                     </td>
                                                <td >
                                                   <asp:Label ID="Label12" CssClass="lbl565" runat="server" Text='<%# Bind("Origin") %>'></asp:Label>
			                                     </td>
                                                <td >
                                                   <asp:Label ID="Label16" CssClass="lbl565" runat="server" Text='<%# Bind("OS") %>'></asp:Label>
			                                     </td>
                                                <td >
                                                   <asp:Label ID="Label21" CssClass="lbl565" runat="server" Text='<%# Bind("OsVersion") %>'></asp:Label>
			                                     </td>
                                                <td >
                                                   <asp:Label ID="Label22" CssClass="lbl565" runat="server" Text='<%# Bind("AppVersion") %>'></asp:Label>
			                                     </td>
                                                <td >
                                                   <asp:Label ID="Label23" CssClass="lbl565" runat="server" Text='<%# Bind("Carrier") %>'></asp:Label>
			                                     </td>
			                                </tr>
			                                
			                            </ItemTemplate>
                                        <FooterTemplate>
                                        </table>                
                                        </FooterTemplate>            
                                    </asp:Repeater>    
                              
            		<uc1:TablePaging ID="TablePaging1" runat="server" />
		            
	                </div>	
	            </ContentTemplate>
	        </asp:UpdatePanel>
    	
        			

    </div>
    <%/*  
    <asp:Panel ID="panel_iframe" runat="server" CssClass="popModal_del2_ie popModal_del2_hide" style="display:none;"
     Width="900" Height="90%">
     <div id="divIframeLoader" class="divIframeLoader">   
        <table>
            <tr>
                <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
            </tr>
        </table>
    </div>
    <div id="div_iframe_main" style="height:100%;">
        <div class="top"></div>
            <a id="a_close" runat="server" class="span_A" href="javascript:void(0);" onclick="javascript:CloseIframe();"></a>
        <iframe runat="server" id="_iframe" width="880px" height="100%" frameborder="0" ALLOWTRANSPARENCY="true" scrolling="no" style="overflow:hidden;"></iframe>
        <div class="bottom2"></div>
    </div>
    </asp:Panel>
    
    <cc1:ModalPopupExtender ID="_mpe" runat="server"
                TargetControlID="btn_virtual"
                PopupControlID="panel_iframe"
                 BackgroundCssClass="modalBackground"                  
                  BehaviorID="_modal" 
                  Y="5"              
                  DropShadow="false">
    </cc1:ModalPopupExtender>
     */ %>
    <div style="display:none;">

        <asp:Button ID="btn_virtual" runat="server"  style="display:none;" CausesValidation="false"/>
    </div>

   
    <asp:Label ID="lbl_PageTitle" runat="server" Text="Clipcall Request Reports" Visible="false"></asp:Label>

    <asp:HiddenField ID="Hidden_IfToExite" runat="server" Value="Are you sure you want to exit? Unsaved data will be lost!" />
    <div class="div_send_aar" runat="server" id="div_send_aar">
        <input id="h_SendAarIncidentId" type="hidden" />
         <a id="a_close_SendAar" runat="server" class="span_A" href="javascript:void(0);" onclick="CloseAarSender();"></a>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label13" runat="server" Text="Supplier name:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_SupplierName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_SupplierName" runat="server" ErrorMessage="Required"
                         ControlToValidate="txt_SupplierName" CssClass="error-msg" ValidationGroup="SendAar"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_phone" runat="server" Text="Supplier phone:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_SupplierPhone" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_SupplierPhone" runat="server" ErrorMessage="Required" ValidationGroup="SendAar"
                        CssClass="error-msg" ControlToValidate="txt_SupplierPhone"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rev_SupplierPhone" runat="server" ErrorMessage="Invalid phone number" ValidationGroup=""
                        CssClass="error-msg" ControlToValidate="txt_SupplierPhone"
                         ValidationExpression="<%# siteSetting.GetPhoneRegularExpression %>"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;">
                    <a id="a_SendAar" class="btn2" onclick="SendAar();">Send aar</a>
                    
                    <asp:Image ID="img_SendAar_loader" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                </td>
            </tr>
             <tr>
                <td colspan="2" >
                    <asp:Label ID="lbl_SendAarResponse" runat="server" Text="" CssClass="error-msg"></asp:Label>
                    
                </td>
            </tr>
        </table>
    </div>
    <div id="div_RejectReason" title="Reject Reason">
        <asp:RadioButtonList ID="rbl_RejectReason" runat="server"></asp:RadioButtonList>
        
    </div>

    <div id="div_assessment" title="Assessment">
        <asp:RadioButtonList ID="rbl_assessment" runat="server"></asp:RadioButtonList>
        <input id="assessment_incidentid" type="hidden" />
        
    </div>
    <cc1:ModalPopupExtender ID="_mpeSendAar" runat="server"
	TargetControlID="btn_virtual2"
	PopupControlID="div_send_aar"
	 BackgroundCssClass="modalBackground" 
	  
	  BehaviorID="send_aar"               
	  DropShadow="false"
   ></cc1:ModalPopupExtender> 
     <asp:Button ID="btn_virtual2" runat="server" style="display:none;"  />   
</asp:Content>

