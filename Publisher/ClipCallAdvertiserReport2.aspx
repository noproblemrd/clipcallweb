﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ClipCallAdvertiserReport2.aspx.cs" Inherits="Publisher_ClipCallAdvertiserReport2" %>

<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register Src="~/Publisher/ClipCallControls/ProReportTable.ascx" TagPrefix="uc1" TagName="ProReportTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
     <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
    <style type="text/css" >
        td.data-table-details div._left
        {
            float:left;
        }
        #main{
            margin: 0 200px !important;
        }
    </style>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function _CreateExcel() {
            create_excel('<%# GetCreateExcel %>', '<%# SessionTableName %>', 'AarCallCenterReport', '<%# DownloadExcel %>');
           
        }
        
        $(function () {
            $('.tr_show').each(function () {
                tr_click(this);
            });
        });
        function tr_click(_this) {
            $(_this).click(function () {
                var _next = $(this).next('tr');
                if (_next.is(":visible"))
                    _next.hide();
                else
                    _next.show();
            });
        }
        function inactive(_a) {
            var supplierId = $(_a).parents('tr').find('input[type="hidden"]').val();
            $(_a).hide();
            var $div_parent = $(_a).parents('div.div_ActiveInactive');
            $div_parent.find('div.div_active_loader').show();
            $.ajax({
                url: '<%# InactiveSupplierUrl %>',
                data: "{ 'supplierId': '" + supplierId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    $div_parent.find('span.span_activeResponse').html(_data.IsSucceeded ? 'Success' : _data.FailedReason);
                    
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    $div_parent.find('div.div_active_loader').hide();
                }

            });
        }
        function active(_a) {
            var supplierId = $(_a).parents('tr').find('input[type="hidden"]').val();
            $(_a).hide();
            var $div_parent = $(_a).parents('div.div_ActiveInactive');
            $div_parent.find('div.div_active_loader').show();
            $.ajax({
                url: '<%# ActiveSupplierUrl %>',
                data: "{ 'supplierId': '" + supplierId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    $div_parent.find('span.span_activeResponse').html(_data.IsSucceeded ? 'Success' : _data.FailedReason);
                    
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    $div_parent.find('div.div_active_loader').hide();
                }

            });
        }
        function EndHandler() {
            $(window).scroll(_scroll);
            hideDiv();
        }


        function ReciveServerData(retValue) {

            $(window).scroll(_scroll);
            if (retValue.length == 0)
                return;
            var _trs = $(retValue).get(0).getElementsByTagName('tr');
            var _length = _trs.length;
            for (var i = 1; i < _length; i++) {
       //         _trs[1].className = ((i % 2 == 1) ? "_even" : "_odd");
                tr_click(_trs[1]);
                $('.-GridView').append(_trs[1]);
            }

        }
        $(window).scroll(_scroll);
        function _scroll() {
            if ($(window).scrollTop() > 0.9 * ($(document).height() - $(window).height())) {
                $(window).unbind('scroll');
                _CallServer();
            }
        }
        function _CallServer() {

            $(window).unbind('scroll');
            showDiv();
            $.ajax({
                url: '<%# GetServicePage %>',
            data: null,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if (data.d.length == 0 || data.d == "done") {
                    return;
                }
                ReciveServerData(data.d);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //              alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                hideDiv();

            }

        });
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="Date-field" runat="server" id="div_Date">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div> 
             <div class="form-field" runat="server" id="div2" style="top:0; margin-bottom:0;">
                    <asp:Label ID="Label44" runat="server" Text="Date selector" CssClass="label"></asp:Label>
                 <asp:RadioButtonList ID="rbl_date" runat="server" RepeatDirection="Vertical" >
                     
                 </asp:RadioButtonList>

             </div> 
             <div style="clear:both;"></div>			
              <div class="form-field" runat="server" id="div_Step">
                    <asp:Label ID="lbl_Stpe" runat="server" Text="Step" CssClass="label"></asp:Label>
                    <asp:DropDownList ID="ddl_Step" CssClass="form-select" runat="server" ></asp:DropDownList>

                </div>
              
            <div class="form-field" runat="server" id="div1">
                <asp:Label ID="lbl_status" runat="server" Text="Supplier status" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_SupplierStatus" CssClass="form-select" runat="server" ></asp:DropDownList>

            </div>
             <div class="form-field" runat="server" id="div3">
                <asp:Label ID="Label60" runat="server" Text="Inactive reason" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_InactiveReason" CssClass="form-select" runat="server" ></asp:DropDownList>

            </div>
             <div class="form-field" runat="server" id="div_oldPro">
                    <asp:Label ID="Label43" runat="server" Text="Old Pro" CssClass="label"></asp:Label>
                    <asp:DropDownList ID="ddl_oldPro" CssClass="form-select" runat="server" ></asp:DropDownList>
                </div>
		</div>
      
		<div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>        
        <div id="Table_Report"  runat="server" >  
            <uc1:ProReportTable runat="server" ID="_ProReportTable" />
			</div>
        </div>
    </div>
</asp:Content>

