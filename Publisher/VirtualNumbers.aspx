<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VirtualNumbers.aspx.cs" Inherits="Publisher_VirtualNumbers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript" src="../general.js"></script>
     <link type="text/css" rel="stylesheet" href="style.css" />
    <script type="text/javascript" language="javascript">
    function witchAlreadyStep(level,mode)
    {    
        
        //parent.window.setInitRegistrationStep(level);
        parent.window.clearStepActive();    
        parent.window.setLinkStepActive('linkStep9');               
    }
    function init()
    {
        parent.goUp();
    }
    
    window.onload=init;
    
    
    </script>
</head>
<body id="Body1" class="iframe-inner step9" runat="server" style="background-color:transparent;">

    
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
 
    <h2 style="text-align:center;"><asp:Label ID="lbl_virtualNumbers" runat="server" Text="Virtual numbers"></asp:Label></h2>
  <div class="clearfix"></div>
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
        <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false"
         HeaderStyle-Font-Underline="true"
           GridLines="None" CssClass="data-tablevn" BorderWidth="0" 
          AllowPaging="True" PageSize="20" OnPageIndexChanging="_GridView_PageIndexChanging" >
          <AlternatingRowStyle CssClass="even" />
          <RowStyle CssClass="odd" />
        <Columns>
        
        <asp:TemplateField>        
        <HeaderTemplate>
            <asp:Label ID="Label7" runat="server" Text="<%# lbl_Number.Text %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label8" runat="server" Text="<%# Bind('Number') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField>        
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_ExpertiseName.Text %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label2" runat="server" Text="<%# Bind('ExpertiseName') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
         <asp:TemplateField>        
        <HeaderTemplate>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_ModifiedOn.Text %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label4" runat="server" Text="<%# Bind('ModifiedOn') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
         <asp:TemplateField>        
        <HeaderTemplate>
            <asp:Label ID="Label5" runat="server" Text="<%# lbl_OriginName.Text %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('OriginName') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
         
            
       
        </Columns>
         <HeaderStyle Font-Underline="True" />
            <PagerStyle CssClass="PagerIncompleteReg" ForeColor="Blue" HorizontalAlign="Center" />
        </asp:GridView>
       </ContentTemplate>
        </asp:UpdatePanel>
     
  
    </form>
   
    
        <asp:Label ID="lbl_ExpertiseName" runat="server" Text="Expertise name" Visible="false"></asp:Label>
        <asp:Label ID="lbl_ModifiedOn" runat="server" Text="Modified on" Visible="false"></asp:Label>
        <asp:Label ID="lbl_OriginName" runat="server" Text="Origin name" Visible="false"></asp:Label>
        <asp:Label ID="lbl_Number" runat="server" Text="Number" Visible="false"></asp:Label>
        <asp:Label ID="lbl_noreasult" runat="server" Text="There are no results" Visible="false"></asp:Label>
    
</body>
</html>
