﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" 
AutoEventWireup="true" CodeFile="DepositReport.aspx.cs" Inherits="Publisher_DepositReport" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />

<script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>

<script src="../jquery/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
    
    window.onload = appl_init;
    function appl_init() {        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {        
        hideDiv();
    }
    /*************/
    function LoadChart(fileXML, div_id, parentId) {
        var _chart = "<%# GetChartType %>";
        var chart = new FusionCharts(_chart, div_id, '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render(parentId);
    }
    function CleanChart() {
        document.getElementById("div_chart").innerHTML = "";
    }

    //ICallbackEventHandler
    function SetCharts() {
        CallServer("", "ee");
    }
    function ReciveServerData(retValue) {
        var Charts_Area = document.getElementById("<%# Charts_Area.ClientID %>");
        var div_container = document.getElementById("div_container");
        var div_chart_load = document.getElementById("div_chart_load");
        if (retValue.length == 0) {
            RemoveAllChildNodes(div_container)
            div_container.appendChild(div_chart_load);
            RemoveAllChildNodes(Charts_Area);
            Charts_Area.innerHTML = "";
            return;
        }
        
        RemoveAllChildNodes(div_container);
        div_container.appendChild(div_chart_load);
        RemoveAllChildNodes(Charts_Area);
        Charts_Area.innerHTML = "";
        
        var chart1 = document.createElement('div');
        var _id = "div_chart1";
        chart1.setAttribute("id", _id);
        var _chart = document.createElement('div');
        var _chart_id = "_chart1";
        chart1.appendChild(_chart);
        Charts_Area.appendChild(chart1);

        LoadChart(retValue, _chart_id, _id);
        

    }
    function SetOnLoadChart() {

        var Charts_Area = document.getElementById("<%# Charts_Area.ClientID %>");
        var div_chart_load = document.getElementById("div_chart_load");
        RemoveAllChildNodes(Charts_Area);
        Charts_Area.appendChild(div_chart_load);
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">          
</cc1:ToolkitScriptManager>       
 <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2">

	
<div id="form-analytics">
	<div class="main-inputs">
	<%/*
	        <div id="div_experties" class="form-field" runat="server">
			    <label for="form-subsegment" class="label" runat="server" id="lblHeading">
                    <asp:Label ID="lbl_Heading" runat="server" Text="Heading"></asp:Label>
                </label>
			    <asp:DropDownList ID="ddl_Heading" CssClass="form-select" runat="server">
                </asp:DropDownList>
			</div>
             */ %>
			<div class="form-field">
                <asp:Label ID="lbl_depositType" CssClass="label" runat="server" Text="Deposit Type"></asp:Label>
                <asp:DropDownList ID="ddl_NumOfDeposit"  CssClass="form-select" runat="server">
                </asp:DropDownList>
            
            </div>
			 <div class="clear"><!-- --></div>
             
            
            

            <div class="CreatedBy">
                <asp:Label ID="lbl_CreatedBy" CssClass="label" runat="server" Text="Created by"></asp:Label>
                
                <asp:DropDownList ID="ddl_CreatedBy" CssClass="form-select" runat="server">
                            
                </asp:DropDownList>
                    
            </div>
            
                
            
        </div>    
     <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>
              <div class="clear"><!-- --></div>
                    
	 </div>	

	 <asp:UpdatePanel ID="_UpdatePanelCharts" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div id="Charts_Area" runat="server">
    
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
	<div id="Table_Report" class="table" runat="server">
		<table class="data-table" cellspacing="0">
			
			<tr>
				<td>
                    <div id="div_chart"></div>
                    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                    
		            <asp:GridView ID="_gridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />  
                        <Columns>
                            <asp:TemplateField SortExpression="CreatedOn">
                                <HeaderTemplate >
                                    <asp:Label ID="Label101" runat="server" Text="<%#lbl_Date.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label102" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField SortExpression="Amount">
                                <HeaderTemplate>
                                    <asp:Label ID="Label103" runat="server" Text="<%#lbl_Amount.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label104" runat="server" Text="<%# Bind('Amount') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField SortExpression="VirtualMoney">
                                <HeaderTemplate>
                                    <asp:Label ID="Label105" runat="server" Text="<%#lbl_VirtualMoney.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label106" runat="server" Text="<%# Bind('VirtualMoney') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField SortExpression="Credit">
                                <HeaderTemplate>
                                    <asp:Label ID="Label107" runat="server" Text="<%#lbl_Credit.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label108" runat="server" Text="<%# Bind('Credit') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField SortExpression="Total">
                                <HeaderTemplate>
                                    <asp:Label ID="Label109" runat="server" Text="<%#lbl_total.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label110" runat="server" Text="<%# Bind('Total') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                         <FooterStyle CssClass="footer"  />
                        <PagerStyle CssClass="pager" />
                    </asp:GridView>

                    <uc1:TablePaging ID="TablePaging1" runat="server" />
                    
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
		</table>
	</div>
	
		
</div>
<div id="div_container" style="display:none;">
<div id="div_chart_load"  class="divLoaderChart">
    <table   >
        <tr>
          <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." style="background-color: #FFFFFF;" /></td>
        </tr>
    </table>
</div>
</div>
<asp:Label ID="lbl_NoResult" runat="server" Visible="false" Text="There are not results"></asp:Label>

<asp:Label ID="lbl_Date" runat="server" Visible="false" Text="Date"></asp:Label>
<asp:Label ID="lbl_Amount" runat="server" Visible="false" Text="Amount"></asp:Label>
<asp:Label ID="lbl_VirtualMoney" runat="server" Visible="false" Text="Virtual money"></asp:Label>
<asp:Label ID="lbl_Credit" runat="server" Visible="false" Text="Credit"></asp:Label>

<%/* 

<asp:Label ID="lbl_Sum" runat="server" Visible="false" Text="Sum"></asp:Label>
<asp:Label ID="lbl_Count" runat="server" Visible="false" Text="Count"></asp:Label>
*/ %>

<asp:Label ID="lbl_PageTitle" runat="server" Text="Deposits" Visible="false"></asp:Label>
<asp:Label ID="lbl_total" runat="server" Visible="false" Text="Total"></asp:Label>
</asp:Content>

