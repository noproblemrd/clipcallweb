﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="GeneralSms.aspx.cs" Inherits="Publisher_GeneralSms" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	       
               <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">		
                    <div class="form-field" runat="server" id="div_NumberOfAdvertisersProvided" style="width:100%;">
                        <asp:Label ID="lbl_message" runat="server" Text="Message" CssClass="label"></asp:Label>
                        <asp:TextBox ID="txt_message" runat="server" CssClass="form-text" MaxLength="160" TextMode="MultiLine" Rows="6"
                            style="height: 100px;width: 400px;"></asp:TextBox>
                        
                         <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_message" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_message" ValidationGroup="sms" >Missing</asp:RequiredFieldValidator>
                    </div>
                   <div class="clear"></div>
                    <div class="form-field" runat="server" id="div1" style="width:100%;">
                        <asp:Label ID="lbl_phone" runat="server" Text="Phone" CssClass="label"></asp:Label>
                        <asp:TextBox ID="txt_phone" runat="server" CssClass="form-text"></asp:TextBox>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator_phone" CssClass="error-msg"
        runat="server" ErrorMessage="Invalid" Display="Dynamic"
        ValidationGroup="sms" ControlToValidate="txt_phone"
        ValidationExpression=""></asp:RegularExpressionValidator>
                       <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_phone" Display="Dynamic"
        CssClass="error-msg" ID="RequiredFieldValidator_phone" ValidationGroup="sms" >Missing</asp:RequiredFieldValidator>

                    </div>
                    <div class="clear"></div>
                   <div>
                       <asp:LinkButton ID="lb_send" runat="server" Text="Send" CssClass="anchor_CreateReportSubmit2" ValidationGroup="sms" 
                           OnClick="lb_send_Click" OnClientClick="showDiv();"></asp:LinkButton>
                   </div>
                   <div>
                       
                       <asp:Label ID="lbl_result" runat="server" Text="" CssClass="error-msg"></asp:Label>
                       
                   </div>
                </div>
            </div>
        </div>
</asp:Content>

