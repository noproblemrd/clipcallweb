﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using System.IO;
using System.Text;

public partial class Publisher_DepositReport : PageSetting, ICallbackEventHandler
{
    
    string REPORT_NAME;
    const string FILE_NAME = "DepositReport.xml";
    const string EXCEL_NAME = "DepositReport";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            REPORT_NAME = lbl_PageTitle.Text;
            SetToolbox();
            LoadDepositTypes();
            //         LoadExperties();
            LoadCreatedBy();
        }
        else
        {
            if (!ScriptManager1.IsInAsyncPostBack)
            {
                string _str = GetCallbackResult();
                ClientScript.RegisterStartupScript(this.GetType(), "ReciveServerData", "ReciveServerData('" + _str + "');", true);
            }

        }
        SetupClient();
        SetToolboxEvents();
        FromToDate1.SetProgress();
        Header.DataBind();
    }
    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {        
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (DepositReportRequestV == null)
            return;
        WebReferenceReports.DepositReportRequest _request = DepositReportRequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        DataResult dt = GetRerport(_request, true);
        BindDataToGridview(dt);
    }

    private void LoadCreatedBy()
    {
        ddl_CreatedBy.Items.Clear();
        foreach (string s in Enum.GetNames(typeof(WebReferenceReports.eDepositCreatedBy)))
        {
            ddl_CreatedBy.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId,
                "eDepositCreatedBy", s), s));
        }
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(REPORT_NAME);
    }
    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        DateTime _to = FromToDate1.GetDateTo;
        DateTime _from = FromToDate1.GetDateFrom;

        if (_to == DateTime.MinValue)
        {
            _to = DateTime.Now;

        }

        if (_from == DateTime.MinValue)
        {
            _from = _to.AddMonths(-1);

        }
        else
        {
            if (_from > _to)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + HttpUtility.JavaScriptStringEncode(FromToDate1.GetDateError) + "');", true);
                ClearChart();
                return;
            }

        }
        WebReferenceReports.DepositReportRequest _request = new WebReferenceReports.DepositReportRequest();
        WebReferenceReports.eDepositType _edt;
        if (!Enum.TryParse(ddl_NumOfDeposit.SelectedValue, out _edt))
            _edt = WebReferenceReports.eDepositType.All;
        _request.DepositType = _edt;
        _request.FromDate = _from;
        _request.ToDate = _to;
        WebReferenceReports.eDepositCreatedBy edcb;
        if(!Enum.TryParse(ddl_CreatedBy.SelectedValue, out edcb))
            edcb = WebReferenceReports.eDepositCreatedBy.All;
        _request.CreatedBy = edcb;
        _request.PageNumber = 1;
        _request.PageSize = TablePaging1.ItemInPage;
        DepositReportRequestV = _request;
        DataResult dr = GetRerport(_request, true);
        BindDataToGridview(dr);
        if (dr == null)
        {
            Update_Faild();
            ClearChart();
            
        }        
        else if (dr.data.Rows.Count == 0)
        {
            ClearChart();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
        }
        else
            Load_charts();
    }
    
    DataResult GetRerport(WebReferenceReports.DepositReportRequest _request, bool SetTotal)
    {
        
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);

        WebReferenceReports.ResultOfDepositReportResponse result = null;
        try
        {
            result = _report.GetDepositsReport(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }


        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        DataTable data = new DataTable();
        data.Columns.Add("CreatedOn", typeof(string));
        data.Columns.Add("Amount", typeof(string));
        data.Columns.Add("Credit", typeof(string));
        data.Columns.Add("VirtualMoney", typeof(string));
        data.Columns.Add("Total", typeof(string));
        data.Columns.Add("_Amount", typeof(int));
        data.Columns.Add("_Credit", typeof(int));
        data.Columns.Add("_VirtualMoney", typeof(int));

        foreach (WebReferenceReports.DepositData _data in result.Value.DataList)
        {
            DataRow row = data.NewRow();
            row["CreatedOn"] = string.Format(siteSetting.DateFormat, _data.CreatedOn);
            row["Amount"] = String.Format("{0:#,0}", _data.Amount) + siteSetting.CurrencySymbol;
            row["Credit"] = String.Format("{0:#,0}", _data.Credit) + siteSetting.CurrencySymbol;
            row["VirtualMoney"] = String.Format("{0:#,0}", _data.VirtualMoney) + siteSetting.CurrencySymbol;
            row["Total"] = String.Format("{0:#,0}", _data.Total) + siteSetting.CurrencySymbol;            
            row["_Amount"] = _data.Amount;// +siteSetting.CurrencySymbol;
            row["_Credit"] = _data.Credit;// +siteSetting.CurrencySymbol;
            row["_VirtualMoney"] = _data.VirtualMoney;
            data.Rows.Add(row);
        }
        if (SetTotal && result.Value.TotalRows > 0)
        {
            DataRow row = data.NewRow();
            row["CreatedOn"] = lbl_total.Text;
            row["Amount"] = String.Format("{0:#,0}", result.Value.TotalRow.Amount) + siteSetting.CurrencySymbol;
            row["Credit"] = String.Format("{0:#,0}", result.Value.TotalRow.Credit) + siteSetting.CurrencySymbol;
            row["VirtualMoney"] = String.Format("{0:#,0}", result.Value.TotalRow.VirtualMoney) + siteSetting.CurrencySymbol;
            row["Total"] = String.Format("{0:#,0}", result.Value.TotalRow.Total) + siteSetting.CurrencySymbol;
            data.Rows.Add(row);
        }
        DataResult dr = new DataResult() { data = data, CurrentPage = result.Value.CurrentPage, TotalPages = result.Value.TotalPages, TotalRows = result.Value.TotalRows };
        return dr;
    }
    void BindDataToGridview(DataResult dr)
    {
        if (dr == null)
        {
            ClearGridView();
            return;
        }
        _gridView.DataSource = dr.data;
        _gridView.DataBind();       
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
        _UpdatePanel.Update();
    }
    void ClearGridView()
    {
        _gridView.DataSource = null;
        _gridView.DataBind();
        LoadGeneralData(0, 0, 0);
        _UpdatePanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = _TotalRows + " " + ToolboxReport1.RecordMached;
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    string LoadChart(DataTable data)
    {

        if (data == null || data.Rows.Count == 0)
            return string.Empty;
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + lbl_PageTitle.Text + @"' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='3' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");
     //   sb.Append(@"numberSuffix='%' ");
        sb.Append(@"numberprefix='" + siteSetting.CurrencySymbol + @"' ");
        /*
        if (SymboleValue == "%")
            sb.Append(@"numberSuffix='%' ");
        else if (!string.IsNullOrEmpty(SymboleValue))
            sb.Append(@"numberprefix='" + SymboleValue + @"' ");
        */
        //    sb.Append(@"rotateNames='1' ");
        //     sb.Append(@"slantLabels='1' ");

        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar=' ' ");

        sb.Append(@"bgColor='FFFFFF'>");



        int modulu = data.Rows.Count / 15;
        modulu++;

        StringBuilder sb_Amount = new StringBuilder();
        StringBuilder sb_Credit = new StringBuilder();
        StringBuilder sb_VirtualMoney = new StringBuilder();
        sb.Append(@"<categories>");
        sb_Amount.Append(@"<dataset seriesName='" + lbl_Amount.Text + @"' color='808080' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
        sb_Credit.Append(@"<dataset seriesName='" + lbl_Credit.Text + @"' color='0080C0'  >");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'
        sb_VirtualMoney.Append(@"<dataset seriesName='" + lbl_VirtualMoney.Text + @"' color='8080C0'  >");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'

        foreach (DataRow row in data.Rows)
        {
            string _Amount = row["_Amount"].ToString();
            string _Credit = row["_Credit"].ToString();
            string _VirtualMoney = row["_VirtualMoney"].ToString();
            string _title = lbl_Amount.Text + "=" + _Amount + "\r\n" +
                lbl_Credit.Text + "=" + _Credit + "\r\n" +
                lbl_VirtualMoney.Text + "=" + _VirtualMoney + "\r\n";
            sb.Append(@"<category name='" + row["CreatedOn"].ToString() + @"' toolText='" + _title + "'/>");

            sb_Amount.Append(@"<set value='" + _Amount + "'/>");
            sb_Credit.Append(@"<set value='" + _Credit + "'/>");
            sb_VirtualMoney.Append(@"<set value='" + _VirtualMoney + "'/>");
        }
        sb.Append("</categories>");
        sb_Amount.Append(@"</dataset>");
        sb_Credit.Append(@"</dataset>");
        sb_VirtualMoney.Append(@"</dataset>");

        sb.Append(sb_Amount.ToString());
        sb.Append(sb_Credit.ToString());
        sb.Append(sb_VirtualMoney.ToString());
        sb.Append(@"</graph>");


        string path = //HttpContext.Current.Request.PhysicalApplicationPath + 

            System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) { return string.Empty; }
        string pathWeb = System.Configuration.ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        
        return pathWeb;
    }
    private void LoadDepositTypes()
    {
        ddl_NumOfDeposit.Items.Clear();
        foreach (string s in Enum.GetNames(typeof(WebReferenceReports.eDepositType)))
        {
            ListItem li = new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eDepositType", s), s);
            li.Selected = (s == WebReferenceReports.eDepositType.All.ToString());
            ddl_NumOfDeposit.Items.Add(li);
        }
    }
    /*
    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xd = XDocument.Parse(result);
        if (xd.Element("PrimaryExpertise") == null || xd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement xe in xd.Elements("PrimaryExpertise").Elements())
        {
            string pName = xe.Attribute("Name").Value;
            string _id = xe.Attribute("Code").Value;
            string _guid = xe.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;

    }
     * */
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {       
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        WebReferenceReports.DepositReportRequest _request = DepositReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataTable data = (GetRerport(_request, true)).data;
        ToExcel te = new ToExcel(this, EXCEL_NAME);
        GridView gv = _gridView;
        if (!te.ExecExcel(data, gv))
            Update_Faild();
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
         if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        
         WebReferenceReports.DepositReportRequest _request = DepositReportRequestV;
         _request.PageSize = -1;
         _request.PageNumber = -1;
         DataTable data = (GetRerport(_request, true)).data;
         Session["data_print"] = data;
         Session["grid_print"] = _gridView;
         ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    
    private void Load_charts()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "load_charts", "setTimeout('SetCharts();', 200);", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetOnLoadChart", "SetOnLoadChart();", true);
    }
    private void ClearChart()
    {
        PathV = string.Empty;       
        Charts_Area.Controls.Clear();
        _UpdatePanelCharts.Update();
    }
    
    string PathV 
    {
        get { return (Session["XmlGraph"] == null) ? string.Empty : (string)Session["XmlGraph"]; }
        set { Session["XmlGraph"] = value; }
    }
     
    WebReferenceReports.DepositReportRequest DepositReportRequestV
    {
        get { return (ViewState["DepositReportRequest"] == null) ? null : (WebReferenceReports.DepositReportRequest)ViewState["DepositReportRequest"]; }
        set { ViewState["DepositReportRequest"] = value; }
    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        return PathV;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        WebReferenceReports.DepositReportRequest _request = DepositReportRequestV;
        if (_request == null)
            return;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);

        _request.PageNumber = -1;
        _request.PageSize = -1;
        DataResult dr = GetRerport(_request, false);
        string xml_file = LoadChart(dr.data);
        PathV = xml_file;
    }
    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }

    #endregion
    protected string GetChartType
    {
        get { return ResolveUrl("~") + "Management/FusionCharts/Charts/MSLine.swf"; }
    }
}