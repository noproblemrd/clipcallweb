﻿function appl_init() {

    var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
    pgRegMgr.add_beginRequest(BeginHandler);

    pgRegMgr.add_endRequest(EndHandler);

}
function BeginHandler() {
    document.getElementById("divLoader").style.display = 'block';
}
function EndHandler() {
    document.getElementById("divLoader").style.display = 'none';
}