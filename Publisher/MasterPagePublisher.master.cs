using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Publisher_MasterPagePublisher : System.Web.UI.MasterPage
{
    PageSetting ps;
    public event System.EventHandler newAdvEvent;
    public Publisher_MasterPagePublisher():base()
    {
        this.ID = PagesNames.Publisher.ToString();
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //Security- for the browser not save in the cash memory
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");
        ////
      
        ps = (PageSetting)Page;
        
         /*
       if (Session.IsNewSession)
       {
           if (ps.IsCallback())
           {
               ApplicationInstance.CompleteRequest();
               return;
           }
           ps.UnrecognizedClient(lbl_logOutMes.Text, "logout.aspx");
           return;
       }

       if (!ps.userManagement.IsPublisher())
       {
           if (ps.IsCallback())
           {
               ApplicationInstance.CompleteRequest();
               return;
           }
           Response.Redirect("LogOut.aspx");
       }
        if (string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
        {
            ps.UnrecognizedClient(lbl_logOutMes.Text, "logout.aspx");
        }        
        * */
        
        if (Session.IsNewSession || !ps.userManagement.IsPublisher())
        {
            if (ps.siteSetting == null || string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, ps))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            }
            if (!EncryptString.LogIn(ps, HttpContext.Current))
            {
                Response.Redirect("LogOut.aspx");
                return;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "set_timezone_offset", "set_timezone_offset('" + TimeZoneService + "');", true);
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (ps.siteSetting.GetSiteID == PpcSite.GetCurrent().ZapSiteId)
            RemoveNewAdvertiser();
        else if (ps.siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId)
            RemoveZapLinks();
        liAccountSettings_DeskAppIps.Visible = PpcSite.GetCurrent().SiteId == ((PageSetting)Page).siteSetting.GetSiteID;
        if (!IsPostBack)
        {         
            if (!string.IsNullOrEmpty(ps.siteSetting.LogoPath))
                setLogo(ps.siteSetting.LogoPath);

            if (!string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
                GetTranslate(ps.siteSetting.siteLangId);

            lblUserName.Text = " " + ps.userManagement.User_Name + " ";
            SetMenu();
            SecuritControls sc = new SecuritControls(ps, ps.LinkToSet);            
            sc.SetLinks();
            ps.LoadMasterSiteLinks(this);
            
        }
        else
        {
            
            if (sm == null || !sm.IsInAsyncPostBack)
            {                
                SecuritControls sc = new SecuritControls(ps, ps.LinkToSet);               
                sc.SetLinks();
            }                
        }
        Page.Header.DataBind();
        if (sm == null || !sm.IsInAsyncPostBack)
            ps.SetNoIframe();
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoadDetails", "LoadDetails('" + GetLogOutMessage + "', '" + GetLogOutPath + "');", true);
    }

    
   
    public void setLogo(string name)
    {
        string LogoServerPath = ConfigurationManager.AppSettings["PublisherLogo"] + @"aspjpeg\" + name;
        if(File.Exists(LogoServerPath))
            ImageLogo.ImageUrl = ConfigurationManager.AppSettings["PublisherLogoWeb"] + @"aspjpeg/" + name;
    }
    public void RemoveLogo()
    {
        ImageLogo.ImageUrl = ResolveUrl("~") + "Publisher/images/ClipCallLogo-03.png";
    }
    void GetTranslate(int siteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "MasterPagePublisher.master", siteLangId);

        
    }
    protected void a_AccountSettings_click(object sender, EventArgs e)
    {

    }
    private void SetMenu()
    {
        //li_NewGoldenNumber.Visible = ps.siteSetting.GetSiteID == 
        string pageName =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        if (pageName == a_Search.HRef || pageName == a_NewAdbertiser.CommandArgument || pageName == "PublisherWelcome.aspx" ||
            pageName == a_DpzReport.HRef ||
            pageName == a_AutoRerouterReport.HRef ||
            pageName == a_PPAControlReport.HRef)
        {
            a_Advertisers.Attributes.Add("class", "btn-ads active");
            btn_ads.Attributes.Add("class", "_btn_ads active");

            if (pageName == a_Search.HRef)
                a_Search.Attributes.Add("class", "active");
            else if (pageName == a_DpzReport.HRef)
            {
                a_DpzReport.Attributes.Add("class", "active");
                
            ///    li_NewGoldenNumber.Visible = false;
          //      li_NewAdbertiser.Visible = false;
                
            }
            else if(pageName == a_AutoRerouterReport.HRef)
                a_AutoRerouterReport.Attributes.Add("class", "active");
            else if(pageName == a_PPAControlReport.HRef)
                a_PPAControlReport.Attributes.Add("class", "active");
            else if (pageName == a_NewAdbertiser.CommandArgument)
                a_NewAdbertiser.Attributes.Add("class", "active");
                 
        }
        else if (
            /*
                    pageName == a_ReportWizard.HRef ||                   
                    pageName == a_SavedReport.HRef ||
                    pageName == a_Dashboard.HRef ||
                    pageName == a_AarDashboard.HRef ||        
             * */
            pageName == a_ClipCallRequestReport.HRef ||
                    pageName == a_AarCallCenter.HRef ||
            pageName == a_ClipCallFunnelSummaryReport.HRef ||
            pageName == a_ClipCallAdvertiserReport.HRef ||
            pageName == a_ClipCallInvitationDashboard.HRef ||
            pageName == a_ClipCallInvitationReport.HRef ||
            pageName == a_ClipCallInvitationFunnelReport.HRef ||
            pageName == a_ClipCallAdvertiserFunnelReport.HRef ||
            pageName == a_ClipCallPaymentReport.HRef ||
            pageName == a_ClipCallPaymentReportV2.HRef ||
            pageName == a_ClipCallPaymentReportV3.HRef ||
            pageName == a_ClipCallPaymentReportV4.HRef ||
            pageName == a_ClipCallPaymentReportV4_5.HRef ||
            pageName == a_SubscriptionReport.HRef ||
            pageName == a_QuoteReport.HRef ||
            pageName == a_SurveyReport.HRef ||
            pageName == a_CustomerLeadsReport.HRef ||
            pageName == a_GeneralTextMessage.HRef ||
            pageName == a_SmsCampaignManager.HRef ||
            pageName == a_SmsCampaignExecute.HRef ||
            pageName == a_SmsCampaignReport.HRef ||
            pageName == a_RegistrationReport.HRef ||
            pageName == a_ProPerformanceReport.HRef ||
             pageName == a_SupplierReviewReport.HRef ||
                    pageName == "ReportWizardResult.aspx") 
        {
            a_Reports.Attributes.Add("class", "btn-reports active");
            btn_reports.Attributes.Add("class", "_btn_reports active");
            //ReportCenter
            if (pageName == a_AarCallCenter.HRef ||
                pageName == a_ClipCallFunnelSummaryReport.HRef ||
                pageName == a_ClipCallAdvertiserReport.HRef ||
                 pageName == a_ClipCallInvitationReport.HRef ||
                pageName == a_ClipCallInvitationDashboard.HRef ||
                pageName == a_ClipCallRequestReport.HRef ||
                pageName == a_ClipCallInvitationFunnelReport.HRef ||
                pageName == a_ClipCallAdvertiserFunnelReport.HRef ||               
                pageName == a_QuoteReport.HRef ||
                 pageName == a_SurveyReport.HRef ||
                pageName == a_CustomerLeadsReport.HRef)
                a_ReportCenter.Attributes.Add("class", "active");
                /*
            //Saved reports
            else if (pageName == a_SavedReport.HRef)
                a_SavedReport.Attributes.Add("class", "active");
            //Report Wizard
            else if (pageName == a_ReportWizard.HRef)
                a_ReportWizard.Attributes.Add("class", "active");
            //Dashboard
            else if (pageName == a_Dashboard.HRef)
                a_Dashboard.Attributes.Add("class", "active");
            //AAR
            else if (pageName == a_AarDashboard.HRef)
                a_Aar.Attributes.Add("class", "active");
                 * */
            //Pros           
            else if (pageName == a_RegistrationReport.HRef ||
                pageName == a_ProPerformanceReport.HRef ||
                pageName == a_SupplierReviewReport.HRef)
                a_Pros.Attributes.Add("class", "active");
            //Financial           
            else if(  pageName == a_ClipCallPaymentReport.HRef ||
                        pageName == a_ClipCallPaymentReportV2.HRef ||
                        pageName == a_ClipCallPaymentReportV3.HRef ||
                        pageName == a_ClipCallPaymentReportV4.HRef ||
                        pageName == a_ClipCallPaymentReportV4_5.HRef ||
                        pageName == a_SubscriptionReport.HRef)
                a_Financial.Attributes.Add("class", "active");
            //Campaign
            else if(
                pageName == a_GeneralTextMessage.HRef ||
                pageName == a_SmsCampaignManager.HRef ||
                pageName == a_SmsCampaignExecute.HRef ||
                pageName == a_SmsCampaignReport.HRef)
                a_Campaign.Attributes.Add("class", "active");
        }
        else if (pageName == a_UPSALE.HRef ||
                pageName == a_RefundReport.HRef ||
                pageName == a_Reviews.HRef ||
                pageName == a_Complaints.HRef)
        {
            a_OperationCenter.Attributes.Add("class", "btn-upsale active");
            btn_upsale.Attributes.Add("class", "_btn_upsale active");
            if (pageName == a_UPSALE.HRef)
                a_UPSALE.Attributes.Add("class", "active");
            else if(pageName == a_RefundReport.HRef)
                a_RefundReport.Attributes.Add("class", "active");
            else if (pageName == a_Reviews.HRef)
                a_Reviews.Attributes.Add("class", "active");
            else if(pageName == a_Complaints.HRef)
                a_Complaints.Attributes.Add("class", "active");
        }

        else if (pageName == a_AccountSettings_general.HRef ||
            pageName == a_AccountSettings_Preference.HRef ||
            pageName == a_AccountSettings_BlackList.HRef ||
            pageName == a_AccountSettings_BadWords.HRef ||            
            pageName == a_AccountSettings_GeneralInfo.HRef ||
            pageName == a_AccountSettings_Links.HRef ||
            pageName == a_AccountSettings_WorkingTimes.HRef ||    
            pageName == a_AccountSettings_PostingMessages.HRef ||
            pageName == a_AccountSettings_ClosureDates.HRef ||

            pageName == a_AccountSettings_Text_Editor.HRef ||
            pageName == a_AccountSettings_TextEditor.HRef ||
            pageName == a_AccountSettings_EditTranslate.HRef ||
            pageName == a_AccountSettings_EditExplanation.HRef ||
            pageName == a_AccountSettings_Languages.HRef ||

            pageName == a_AccountSettings_UserManagement.HRef ||
            pageName == a_AccountSettings_Users.HRef ||
            pageName == a_AccountSettings_EditSecurityLevel.HRef ||
            
            pageName == a_AccountSettings_SegmentsManagement.HRef ||            
            pageName == a_AccountSettings_Segment.HRef ||
            pageName == a_AccountSettings_Iframe.HRef ||

            pageName == a_AccountSettings_Ranking.HRef ||
            pageName == a_AccountSettings_PricingManagment.HRef ||
            pageName == a_AccountSettings_Objectives.HRef ||
            pageName == a_AccountSettings_Voucher.HRef ||

            pageName == a_AccountSettings_UnavailabilityReasons.HRef ||
            pageName == a_AccountSettings_RefundReasons.HRef ||
            pageName == a_AccountSettings_UpsaleLoseReasons.HRef ||
            pageName == a_AccountSettings_InactiveReasons.HRef ||
            pageName == a_AccountSettings_ComplaintStatuses.HRef ||
            pageName == a_AccountSettings_ComplaintSeverity.HRef ||
            pageName == a_AccountSettings_Affiliates.HRef
            )
        {
            a_AccountSettings.Attributes.Add("class", "btn-account active");
            btn_account.Attributes.Add("class", "_btn_account active");
            if(pageName == a_AccountSettings_Preference.HRef ||
                pageName == a_AccountSettings_BlackList.HRef ||
                pageName == a_AccountSettings_BadWords.HRef ||                
                pageName == a_AccountSettings_GeneralInfo.HRef ||
                pageName == a_AccountSettings_Links.HRef ||
                pageName == a_AccountSettings_WorkingTimes.HRef ||
                pageName == a_AccountSettings_PostingMessages.HRef ||
                pageName == a_AccountSettings_ClosureDates.HRef)
                a_AccountSettings_general.Attributes.Add("class", "active");
            else if(pageName == a_AccountSettings_TextEditor.HRef ||
                pageName == a_AccountSettings_EditTranslate.HRef ||
                pageName == a_AccountSettings_EditExplanation.HRef ||
                pageName == a_AccountSettings_Languages.HRef)
                a_AccountSettings_Text_Editor.Attributes.Add("class", "active");
            else if(pageName == a_AccountSettings_Users.HRef ||
            pageName == a_AccountSettings_EditSecurityLevel.HRef)
                a_AccountSettings_UserManagement.Attributes.Add("class", "active");
            else if(pageName == a_AccountSettings_Segment.HRef ||
                pageName == a_AccountSettings_Iframe.HRef)
                a_AccountSettings_SegmentsManagement.Attributes.Add("class", "active");
            else if(pageName == a_AccountSettings_Ranking.HRef ||
                pageName == a_AccountSettings_PricingManagment.HRef ||
                pageName == a_AccountSettings_Objectives.HRef ||
                pageName == a_AccountSettings_Voucher.HRef)
                a_AccountSettings_Sales.Attributes.Add("class", "active");
            else if(pageName == a_AccountSettings_UnavailabilityReasons.HRef ||
                pageName == a_AccountSettings_RefundReasons.HRef ||
                pageName == a_AccountSettings_UpsaleLoseReasons.HRef ||
                pageName == a_AccountSettings_InactiveReasons.HRef ||
                pageName == a_AccountSettings_ComplaintStatuses.HRef ||
                pageName == a_AccountSettings_ComplaintSeverity.HRef ||
                pageName == a_AccountSettings_Affiliates.HRef)
                a_AccountSettings_Tables.Attributes.Add("class", "active");
            
        }
        else if (pageName == a_consumers.HRef)
        {
            a_consumers.Attributes.Add("class", "btn-consumers active");
            btn_consumers.Attributes.Add("class", "active");
            if (pageName == a_consumersManagement.HRef)
                a_consumersManagement.Attributes.Add("class", "active");

        }
        
    }
    
    protected void lbLogout_Click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveUrl("~") + "Publisher/LogOut.aspx");
    }
    protected void a_NewAdbertiser_Click(object sender, EventArgs e)
    {
        ps.SetGuidSetting(null);

        if (this.newAdvEvent != null)
            this.newAdvEvent(this, EventArgs.Empty);
            
    }
    public string GetLogOutMessage
    {
        get
        {
            return HttpUtility.JavaScriptStringEncode(lbl_logOutMes.Text);
        }
    }
    public string GetLogOutPath
    {
        get
        {
            return ResolveUrl("~") + "Publisher/LogOut.aspx";
        }
    }
    protected string TimeZoneService
    {
        get
        {
            return ResolveUrl("~") + "Publisher/HandlerTimeZone.ashx?tz=";
        }
    }
    public string GetNoResultsMessage
    {
        get
        {
            return lbl__noResults.Text;
        }
    }
    public string GetFormClientId
    {
        get
        {
            return form1.ClientID;
        }
    }
    public void RemoveNewAdvertiser()
    {
        li_NewAdbertiser.Visible = false;
        li_NewGoldenNumber.Visible = false;

    }
    private void RemoveZapLinks()
    {
        li_NewGoldenNumber.Visible = false;
        li_DpzReport.Visible = false;
        li_AutoRerouterReport.Visible = false;
        li_PPAControlReport.Visible = false;

    }
    
}
