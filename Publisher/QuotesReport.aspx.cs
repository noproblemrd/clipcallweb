﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class Publisher_QuotesReport : PublisherPage
{
    const int DAYS_INTERVAL = 7;
    protected const int ITEM_PAGE = 40;
    protected const int PAGE_PAGES = 10;
    protected readonly string EXCEL_NAME = "ClipCallQuotesReport";
    protected const string PAGE_TITLE = "Quotes Report";

    protected readonly string SessionTableName = "data_quotes";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());


        //      TablePagingConfiguration();
        if (!IsPostBack)
        {
            //  RequestsReportRequestV = null;

            SetDateInterval();
            LoadExperties();
            
            ExecRequestReport();
        }
        SetToolbox();
        //  SetToolboxEvents();
        Header.DataBind();
    }
    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecRequestReport();
    }
    protected void ExecRequestReport()
    {
        ClipCallReport.QuoteReportRequest quoteRequest = new ClipCallReport.QuoteReportRequest();
        quoteRequest.To = FromToDate1.GetDateTo;
        quoteRequest.From = FromToDate1.GetDateFrom;
        Guid categoryId;
        if(!Guid.TryParse(ddl_Heading.SelectedValue, out categoryId))
            categoryId = Guid.Empty;
        quoteRequest.PrimaryExpertiseId = categoryId;
        quoteRequest.IncludeQaTest = cb_IncludeQaTest.Checked;
        quoteRequest.CaseNumber = txtRequestId.Text.Trim();

        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfListOfQuoteReportResponse result = null;
        try
        {
            result = report.GetQuotesReport(quoteRequest);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
         DataTable dt = new DataTable();
        dt.Columns.Add("CaseNumber");
        dt.Columns.Add("OpenCaseTicketScript");
        dt.Columns.Add("QuoteDate");
        dt.Columns.Add("CategoryName");
        dt.Columns.Add("Price");
        dt.Columns.Add("QuoteStatus");
         dt.Columns.Add("SupplierName");
        dt.Columns.Add("SupplierPhone");
        dt.Columns.Add("QuoteScript");

        dt.Columns.Add("Video");
        dt.Columns.Add("CustomerName");
        dt.Columns.Add("CustomerPhone");
        dt.Columns.Add("Address");
        dt.Columns.Add("Trace");
        dt.Columns.Add("CanCreateTransfer", typeof(bool));
        dt.Columns.Add("ClickCreateTransfer");
       foreach(ClipCallReport.QuoteReportResponse quote in result.Value)
       {
           DataRow row = dt.NewRow();
           row["CaseNumber"] = quote.IncidentNumber;
           row["OpenCaseTicketScript"] = "return OpenIframe('MobileAppRequestTicket.aspx?incidentid=" + quote.IncidentId.ToString() + "', 'Case');";
           row["QuoteDate"] = string.Format(siteSetting.DateTimeFormat, quote.QuoteCreatedOn);
           row["CategoryName"] = quote.CategoryName;
           row["Price"] = string.Format("$" + NUMBER_FORMAT, quote.Price);
           row["QuoteStatus"] = quote.QuoteStatus;
           row["SupplierName"] = quote.SupplierName;
           row["SupplierPhone"]=quote.SupplierPhone;
           row["QuoteScript"] = "return Openquote('" + quote.QuoteId + "');";

           if (!string.IsNullOrEmpty(quote.VideoUrl))
           {
               GenerateVideoLink gvl = new GenerateVideoLink(quote.VideoUrl, quote.PicPreview);
               row["Video"] = gvl.GenerateFullUrl();
           }
           else
               row["Video"] = null;
           row["CustomerName"] = quote.CustomerName;
           row["CustomerPhone"] = quote.CustomerPhone;
           row["Address"] = quote.Address;
           row["Trace"] = "return OpenIframe('ClipCallLeadConnections.aspx?incidentid=" + quote.IncidentId.ToString() + "', 'trace');";

           row["CanCreateTransfer"] = false;// quote.CanCreateTransfer;
           if(false)//quote.CanCreateTransfer)
           {
               row["ClickCreateTransfer"] = "return createTransfer(this, '" + quote.QuoteId + "', '" + string.Format("$" + NUMBER_FORMAT, quote.AmountTransfer) + "');";
           }

           dt.Rows.Add(row);
       }

        dataS = dt;
        
        lbl_RecordMached.Text = dt.Rows.Count + " " + ToolboxReport1.RecordMached;
        _GridView.DataSource = dt;
        _GridView.DataBind();
        _UpdatePanel.Update();

    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(PAGE_TITLE);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;
    }
    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
     protected DataTable dataS
    {
        get { return Session[SessionTableName] == null ? new DataTable() : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
     protected string GetCreateExcel
     {
         get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
     }
     protected string GetQuoteUrl
     {
         get { return "QuoteTicket.aspx?quoteid="; }
     }
    protected string CreateTransferUrl
     {
        get
         {
             return ResolveUrl("~/Publisher/QuotesReport.aspx/CreateTransfer");
         }
     }
     [WebMethod(MessageName = "CreateTransfer")]
     public static string CreateTransfer(Guid quoteId)
     {
         ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
         ClipCallReport.ResultOfBoolean result = null;
         try
         {
             result = ccreport.CreateTransfer(quoteId);
         }
         catch (Exception exc)
         {
             dbug_log.ExceptionLog(exc);
             return false.ToString().ToLower();
         }
         if (result.Type == ClipCallReport.eResultType.Failure)
         {
             return false.ToString().ToLower();
         }
         return result.Value.ToString().ToLower();
     }
}