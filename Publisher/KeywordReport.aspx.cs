﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Script.Serialization;
using System.Text;
using System.IO;

public partial class Publisher_KeywordReport : PageSetting, ICallbackEventHandler
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        if (!IsPostBack)
        {
            LoadHeadings();
            SetToolbox();
        }
        SetToolboxEvents();
        SetupClient();

    }

    private void LoadHeadings()
    {
        ddl_Headings.Items.Clear();
        PpcSite _site = PpcSite.GetCurrent();
        ddl_Headings.Items.Add(new ListItem("", Guid.Empty.ToString()));
        IEnumerable<KeyValuePair<Guid, HeadingCode>> query = from x in _site.headings
                                                             orderby x.Value.heading
                                                                 select new KeyValuePair<Guid, HeadingCode>(x.Value.ID, x.Value);
        foreach (KeyValuePair<Guid, HeadingCode> kvp in query)
        {
            ddl_Headings.Items.Add(new ListItem(kvp.Value.heading, kvp.Key.ToString()));
        }
        ddl_Headings.SelectedIndex = 0;
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_KeywordReport.Text);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, "Report");
        if (!te.ExecExcel(dataV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordToPrint + "');", true);
            return;
        }
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        WebReferenceReports.KeywordReportRequest _request = new WebReferenceReports.KeywordReportRequest();
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        _request.Keyword = txt_Keywords.Text;
        _request.HeadingId = new Guid(ddl_Headings.SelectedValue);
        _request.FromDate = FromToDate1.GetDateFrom;
        _request.ToDate = FromToDate1.GetDateTo;
        _request.IncludeUpsales = cb_IncludeUpsales.Checked;
        WebReferenceReports.ResultOfKeywordReportResponse result = null;
        try
        {
            result = _reports.KeywordReport(_request);
        }
        catch (Exception exc)
        {
            Update_Faild();
            dbug_log.ExceptionLog(exc, siteSetting);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        DataTable data = GetDataTable.GetDataTableFromListCorrectNumber(result.Value.DataList);
        IEnumerable<DataRow> data_row = (from x in data.AsEnumerable()
                                         select x).Take(20);
  //      data = data.
        int RowNum = data.Rows.Count;
        div_LastUpdate.Visible = true;
        lblLastUpdate.Text = String.Format("{0:d/M/yyyy HH:mm}", result.Value.Adjusted);
        lbl_RecordMached.Text = RowNum + " " + ToolboxReport1.RecordMached;
        _GridView.DataSource = (RowNum == 0) ? null : data_row.CopyToDataTable();
        _GridView.DataBind();
        PageIndex = 0;
        dataV = data;        
        /*
         * result.Value.DataList[0].Keyword;
         * result.Value.DataList[0].Exposures;
         * result.Value.DataList[0].Requests;
         * result.Value.DataList[0].Heading;
         *  result.Value.DataList[0].OptOutForever;
         *  result.Value.DataList[0].Refunds;
         *  result.Value.DataList[0].PotentialRevenue;
         *  result.Value.DataList[0].ActualRevenue;
         *   result.Value.DataList[0].NetActualRevenue;
         *   
        
        
        result.Value.DataList[0].IsActiveNow;
        
       
       
        result.Value.DataList[0].OptOutRest;
        
        result.Value.DataList[0].RefundCasesIds;
        
        
         * */
    }



    public string GetCallbackResult()
    {
        PageIndex = PageIndex + 1;
        int _index = PageIndex;
        int _min = (_index * 20);
        IEnumerable<DataRow> data_row = (from x in dataV.AsEnumerable()
                                         select x).Skip(_min).Take(20);

        if (data_row.Count() == 0)
            return "done";
        _GridView.DataSource = data_row.CopyToDataTable();
        _GridView.DataBind();
        _GridView.EnableViewState = false;

        //Render control to string
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                _GridView.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        
    }
    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }

    DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    int PageIndex
    {
        get { return (Session["PageIndex"] == null) ? 0 : (int)Session["PageIndex"]; }
        set { Session["PageIndex"] = value; }
    }
}