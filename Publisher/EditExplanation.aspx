<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" 
AutoEventWireup="true" CodeFile="EditExplanation.aspx.cs" 
Inherits="Publisher_EditExplanation"  %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asps" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="PublishStyle.css" rel="stylesheet" type="text/css"/>
<link href="style.css" rel="stylesheet" type="text/css"/>
   <script type="text/javascript" src="../general.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
 <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </cc1:ToolkitScriptManager>
   
		<div class="page-content minisite-content">
							<h2>
							<asp:Label ID="lblSubTitle" runat="server">Welcome to the help tips for the Web tool</asp:Label>
							</h2>
							
							<div id="form-analytics">
								
        <table class="SieTable">
        <tr>
        <td style="width:34%;">
            <asp:Label ID="lbl_Languages" runat="server"  CssClass="label"  Text="Languages"></asp:Label>
        
            <asp:DropDownList ID="ddlSite_lang" runat="server" AutoPostBack="true" CssClass="form-select" OnSelectedIndexChanged="ddlSites_SelectedIndexChanged">
            </asp:DropDownList>            
        </td>
        <td>
             <asps:UpdatePanel ID="UpdatePanelListPages" runat="server" UpdateMode="Conditional">
             <ContentTemplate>
                <asp:ListBox ID="lbPages" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lbPages_SelectedIndexChanged"></asp:ListBox>
             </ContentTemplate>
                 <Triggers>
                     <asp:AsyncPostBackTrigger ControlID="ddlSite_lang" EventName="SelectedIndexChanged" />
                 </Triggers>
            </asps:UpdatePanel>           
            
        </td>
            
        </tr>
        </table>
       
        <asps:UpdatePanel ID="UpdatePanelSentences" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
     	    <div id="trans">
            <asp:DataList ID="DataListSentences" runat="server" DataKeyField="Id" Width="70%">
            
             <ItemTemplate>
             <table style="width:100%">
             <tr >
             <td style="width:50%" class="translation">
             <p>
                 <asp:Label ID="lbl_eng" runat="server" Text="<%# Bind('Sentence') %>" ></asp:Label>
                </p>
             </td>
             <td style="width:50%;" >
             <p>
                 <asp:TextBox ID="tb_translate" runat="server" CssClass="translationbox" Text="<%# Bind('Translate') %>" Width="100%"></asp:TextBox> 
               </p>                
             </td>
             </tr>
             </table>
             </ItemTemplate>
            </asp:DataList>
            </div>
        </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbPages" EventName="SelectedIndexChanged" />
            </Triggers>
        </asps:UpdatePanel>
        <div>
        <center>
        <asp:Button ID="btn_set" runat="server" Text="SET" OnClick="btn_set_Click"
         CssClass="form-submit" />
        </center>
        </div>
    </div>
  </div>
</asp:Content>

