<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="RefundReport.aspx.cs" Inherits="Publisher_RefundReport" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../FromToDate.css" type="text/css" rel="stylesheet" />
<script  type="text/javascript" src="../Calender/_Calender.js"></script>
<link href="../Calender/CalenderDiv.css" rel="stylesheet" type="text/css" />
<link href="../UpdateProgress/updateProgress.css" type="text/css" rel="stylesheet" />
<link href="PublishStyle.css" type="text/css" rel="stylesheet" />
<script type="text/javascript"  src="../CallService.js"></script>

<link media="screen" rel="stylesheet" href="../jquery/colorbox.css" />
<script src="../jquery/jquery.min.js" type="text/javascript" ></script>
<script src="../jquery/jquery.colorbox.js" type="text/javascript" ></script>
<script type="text/javascript" >
    var message;
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);

        pgRegMgr.add_endRequest(EndHandler);
        message = "<%# lbl_IfToExite.Text %>";
    }
    function BeginHandler()
    {       
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }
    
    //send to server
    function CheckFor(contr) {
            
            CallServer(contr, "ee");
        }
        //replay from the server ||not delete||//
    function ReciveServerData(retValue) { }
    //////
    
    function OpenCloseDetail(contr, td_open)
    {
        
        var contrl=document.getElementById(contr);
    
        if(contrl.style.display=='none')  
        { 
            CheckFor(contr+";1");     
            contrl.style.display='table-row';
            document.getElementById(td_open).className="open first";            
        }
        else
        {
            CheckFor(contr+";-1");
            contrl.style.display='none';
            document.getElementById(td_open).className="close first";
        }
    }
    /*
   function CloseRefundAnswer()
   {
        $find("_modalVX").hide();
        CleanRefundAnswer();
   }
   
    function CleanRefundAnswer()
    {
        document.getElementById("<# div_refundVX.ClientID %>").className="popModal_del popModal_del_hide";
 //       $find("_modalVX").hide();
        document.getElementById("<# txt_RefundComment.ClientID %>").value="";
        document.getElementById("<# btn_SendRefund.ClientID %>").removeAttribute("disabled");
        document.getElementById("<# btn_SendBlackList.ClientID %>").removeAttribute("disabled");
        document.getElementById("<# lbl_RefundCommentMissing.ClientID %>").style.visibility="hidden";
    }
    function OpenRefundAnswer(ids, IfRefund, TicketNumber)
    {
        document.getElementById("<# hf_Id.ClientID %>").value=ids;
        document.getElementById("<# hf_TicketNumber.ClientID %>").value = TicketNumber;
        var elm_ifRefund=document.getElementById("<# lbl_refundAuction.ClientID %>");
        var elm_IsApproved=document.getElementById("<# hf_IsApproved.ClientID %>");        
        elm_IsApproved.value=IfRefund;
        var elm_btnBlackList=document.getElementById("<# btn_SendBlackList.ClientID %>");
        var txt_refund=document.getElementById("<# panel_txtRejact.ClientID %>");
        if(IfRefund=="true")
        {
            elm_ifRefund.innerHTML=document.getElementById("<# hf_ApproveRequest.ClientID %>").value;            
            elm_btnBlackList.style.display="inline"; 
            txt_refund.style.display="block";           
        }
        else
        {
            elm_ifRefund.innerHTML=document.getElementById("<# hf_RejectRequest.ClientID %>").value;
            elm_btnBlackList.style.display="none";
            txt_refund.style.display="block";
        }
        document.getElementById("<# div_refundVX.ClientID %>").className="popModal_del";
        $find("_modalVX").show();
    }
    
    function DisableButton()
    {       
         
        var elm_IsApproved=document.getElementById("<# hf_IsApproved.ClientID %>").value;
        var txt_refund=document.getElementById("<# txt_RefundComment.ClientID %>");
        var Missing=document.getElementById("<# lbl_RefundCommentMissing.ClientID %>");
        if(elm_IsApproved.toLowerCase() == "false" && txt_refund.value.length == 0)
        {
            Missing.style.visibility="visible";
            return false;
        }
        var elm1=document.getElementById("<# btn_SendRefund.ClientID %>");
        var elm2=document.getElementById("<# btn_SendBlackList.ClientID %>");
        setTimeout("DisableButtonWait('" + elm1.id + "','"+ elm2.id +"')",50);
//          $find("_modalVX").hide();
         showDiv();
         Missing.style.visibility="hidden";
        return true;
       
    }
    
    function DisableButtonWait(elm1, elm2)
    {
       document.getElementById(elm1).disabled="disabled";
       document.getElementById(elm2).disabled="disabled";  
    }
    
    function pageLoad(sender, args)
    {    
       
        hideDiv();
        appl_init();
    }
    */
    function BlackListVisible(elem, _value, _li, btnId)
    {
        var _values = _value.split(';');
        var select_val = elem.options[elem.selectedIndex].value;
      //  alert("se="+select_val+" val= "+ _value + "==="+(select_val != _value));
        var el = document.getElementById(_li);
        toggleDisabled(el, (select_val != _values[0]));
        var btn = document.getElementById(btnId);
        toggleDisabled(btn, (select_val == _values[1]));
        
    }
    function DisabledServer(elId, ToDisabled)
    {
        toggleDisabled(document.getElementById(elId), 
            ToDisabled)
    }
    /*
    function UpdateRefundSuccess()
    {
        UpdateSuccess();
 //       CloseRefundAnswer();
        
    }
    */
    function OpenRecord(_id) {
   /*
        var url = "../RecordService.asmx/GetPath";
        var params = "_id="+_id;
       CallWebService(params, url, OnCompleteRecord, function(){});
       */

        var _url = '<%# GetRecord %>' + "?record=" + encodeURIComponent(_id);
        window.open(_url, 'record', 'height=300,width=300');
    }
   /*
   function OnCompleteRecord(arg)
   {
        if(arg.length==0)
        {
            alert("<# lbl_RecordError.Text %>");
            return;
        }
 
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        if(is_chrome)
        {
            oWin = window.open('<# GetAudioChrome %>?audio='+ arg, "recordWin", "resizable=0,width=500,height=330");     
            setTimeout("CheckPopupBlocker()", 2000);
            /*
            oWin = window.open(arg, "recordWin");     
            setTimeout("CheckPopupBlocker()", 2000)
            * /
        }
        else
            window.location.href=arg;
   
   }
   */
   
   /***   IFRAME  ****/
    function CloseIframe()
    {
       $find('_modal').hide();
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie popModal_del2_hide";
       document.getElementById("<%# _iframe.ClientID %>").src = "javascript:void(0);";
    }
    function OpenIframe(_path)
    {    
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie";
       document.getElementById("<%# _iframe.ClientID %>").src = _path;
       HideIframeDiv();  
       $find('_modal').show();
    }
    function ShowIframeDiv()
    {
        document.getElementById("divIframeLoader").style.display = "none";
       document.getElementById("div_iframe_main").style.display = "block";
    }
    function HideIframeDiv()
    {
    
        document.getElementById("divIframeLoader").style.display = "block";
       document.getElementById("div_iframe_main").style.display = "none";
    }
    //***************//
   
   
   function checkValidation(elem)
   {
        var div_parent = getParentByClassName(elem, "conttab");
        var _txt = getElementsWithMultiClassesByClass("_PublisherReplay", div_parent, "textarea")[0];
        if(_txt.value.length == 0)
        {
            (getElementsWithMultiClassesByClass("_MissingReplay", div_parent, "span")[0]).style.display = "inline";
            return false;
        }
        return true;
   }
   function RemoveError(elem)
   {
        var div_parent = getParentByClassName(elem, "conttab");
        (getElementsWithMultiClassesByClass("_MissingReplay", div_parent, "span")[0]).style.display = "none";
   }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  

<div class="page-content minisite-content2">
     
					
	<div id="form-analytics">
	<div class="status2">
        <asp:Label ID="lbl_ChooseStatus" CssClass="label" runat="server" Text="Status"></asp:Label>
        <asp:DropDownList ID="ddl_Status" CssClass="form-select" runat="server"></asp:DropDownList>
    </div>
      <div class="refunddate"><uc1:FromToDate ID="FromToDate1" runat="server" /> </div>	
	</div>	
    <div class="clear"></div>
    <div id="list" class="table">          
        <asp:UpdatePanel runat="server" ID="_uPanel" UpdateMode="Conditional">
            <ContentTemplate>
                 <div class="results3"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                <div class="div_gridMyCall">
                    <asp:Repeater runat="server" ID="_reapeter" 
                        onitemdatabound="_reapeter_ItemDataBound">
                        <HeaderTemplate>
                            <table class="data-table">
                                <thead>
                                    <tr>
                                        <th style="width: 30px;">&nbsp;</th>
                                        <th style="width: 40px;"><asp:Label ID="Label31" runat="server" Text="<%# lblTicketNumber.Text %>"></asp:Label></th>
                                        <th style="width: 110px;"><asp:Label ID="Label50" runat="server" Text="<%# lblDate.Text %>"></asp:Label></th>
							            <th style="width: 150px;"><asp:Label ID="Label51" runat="server" Text="<%# lblSupplierName.Text %>"></asp:Label></th>
							            <th style="width: 150px;"><asp:Label ID="Label8" runat="server" Text="<%# lblRefundRequests.Text %>"></asp:Label></th>
							            <th style="width: 150px;"><asp:Label ID="Label9" runat="server" Text="<%# lblApprovedRequest.Text %>"></asp:Label></th>
							            <th style="width:40px;"><asp:Label ID="Label5" runat="server" Text="<%# lblPrice.Text %>"></asp:Label></th>
							            <th style="width:40px;"><asp:Label ID="Label2" runat="server" Text="<%# lblRefundStatus.Text %>"></asp:Label></th>
							            <th style="width:40px;" runat="server" id="th_records" visible="<%# AllowedToRecord %>"><asp:Label ID="Label53" runat="server" Text="<%# lblRecordings.Text %>"></asp:Label></th>
							            
							            
							            
							            
                                        <th style="width:40px;"><asp:Label ID="Label1" runat="server" Text="<%# lblPrimaryExpertise.Text %>"></asp:Label></th>				           							           
							            
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                                    <tr runat="server" id="tr_show">
                                        <td class="close first" id="td_openClose" runat="server">
                                            <a class="arrow" runat="server" id="a_open"></a>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lb_TicketNumber" runat="server" Text="<%# Bind('TicketNumber') %>"
                                             OnClientClick="<%# Bind('CaseScript') %>" CssClass="<%# Bind('a_class') %>"></asp:LinkButton>
                                                  
                                            <asp:Label ID="lbl_IncidentId" runat="server" Text="<%# Bind('IncidentId') %>" Visible="false"></asp:Label>
                                            <asp:Label ID="lbl_SupplierId" runat="server" Text="<%# Bind('SupplierId') %>" Visible="false"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label55" runat="server" Text="<%# Bind('RefundRequestDate') %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lb_SupplierName" runat="server" Text="<%# Bind('SupplierName') %>"
                                             CommandArgument="<%# Bind('SupplierId') %>" OnClick="lb_Name_click"></asp:LinkButton>
                                                 
                                        </td>
                                        <td>
                                            <asp:Label ID="Label10" runat="server" Text="<%# Bind('RefundRequests') %>"></asp:Label>      
                                        </td>
                                        <td>
                                            <asp:Label ID="Label11" runat="server" Text="<%# Bind('ApprovedRequest') %>"></asp:Label>      
                                        </td>
                                        <td>
                                            <asp:Label ID="Label116" runat="server" Text="<%# Bind('Price') %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label155" runat="server" Text="<%# Bind('RefundStatus') %>"></asp:Label>
                                            <asp:Label ID="lbl_RefundStatusValue" runat="server" Text="<%# Bind('RefundStatusValue') %>" Visible="false"></asp:Label>
                                        </td>
                                        <td runat="server" id="td_record" visible="<%# AllowedToRecord %>">
                                            <a id="a_recording" runat="server" href="<%# Bind('Recording') %>" visible="<%# Bind('HasRecord') %>">
                                                <asp:Image ID="img_record" runat="server" ImageUrl="../Management/calls_images/icon_speaker.png" />
                                            </a>							                
							            </td>
							            
							            
                                        
                                        
                                        
                                        <td>
                                            <asp:Label ID="Label45" runat="server" Text="<%# Bind('PrimaryExpertiseName') %>"></asp:Label>
                                        </td>
                                        
                                        
                                        
							            
							        </tr>					     
							        <tr id="tr_openClose"  runat="server" >
							            <td runat="server" id="td_colspan" colspan="<%# GetColSpan() %>">
							           
						              <div class="containertabp" id="tabs">
						              <div class="clear" style="margin-bottom:10px; width:600px;" ><!-- --></div>
						               <div class="conttab">
						                    <div class="form-fieldtabp">
                                                <asp:Label ID="Label001" runat="server" CssClass="label" Text="<%# lblRefundStatus.Text %>"></asp:Label>
                                                <asp:DropDownList ID="ddl_RefundStatus" CssClass="form-selectcompp" runat="server" Enabled="<%# Bind('CanRefund') %>">
                                                </asp:DropDownList> 
						                    </div>
						                   
						                    
						                    <div class="form-fieldtabp">
						                        <asp:Label ID="Label002" runat="server" CssClass="label" Text="<%# lblRefundReason.Text %>"></asp:Label>
                                                <asp:TextBox ID="TextBox002" runat="server" CssClass="form-textcompp Label_ro" Text="<%# Bind('RefundReasonName') %>" ReadOnly="true"></asp:TextBox>						                        
						                    </div>
						                    
						                    <asp:panel class="form-fieldtab2p" runat="server" id="li_ClackList" Enabled="<%# Bind('CanRefund') %>">
                                                <asp:CheckBox ID="cb_BlackList" CssClass="checkbox" runat="server" />
                                                <label id="for_BlackList" runat="server">
                                                    <asp:Label ID="lbl_SendToBlackList" runat="server" CssClass="label" Text="Send to black list"></asp:Label>
                                                </label>
						                    </asp:panel>  
						                    <div class="clear" ><!-- --></div>       
						                    <div class="form-fieldlarge2p">
                                                <asp:Label ID="Label004" runat="server" CssClass="label" Text="<%# lbl_Description.Text %>"></asp:Label>
                                                <asp:TextBox ID="txt_description" runat="server" CssClass="form-textarealarge2p Label_ro" ReadOnly="true" TextMode="MultiLine"
                                                 Rows="3" Text="<%# Bind('Description') %>"></asp:TextBox>
						                    </div>
						                    <div class="form-fieldlarge2p">
						                        <asp:Label ID="Label005" runat="server" CssClass="label" Text="<%# lbl_PublisherReplay.Text %>"></asp:Label>
                                                <asp:TextBox ID="txt_PublisherReplay" runat="server"  CssClass="<%# Bind('PublisherReplayClass') %>" TextMode="MultiLine"
                                                 Rows="3" Text="<%# Bind('PublisherReplay') %>" onkeypress="RemoveError(this);" ReadOnly="<%# Bind('PublisherReplayReadOnly') %>"></asp:TextBox>
                                                <div class="error-msg-wrap">
                                                <asp:Label ID="lbl_MissingReplay" runat="server" Text="Missing" CssClass="error-msg _MissingReplay"
                                                 style="display:none;"></asp:Label>
                                                 </div>
						                    </div>
						                     <div class="clear" ><!-- --></div> 
						                    <div class="form-fieldtabpcheck" runat="server" id="div_btn" visible="<%# Bind('CanRefund') %>">
                                                <asp:Button ID="btnSave" runat="server" CssClass="savep22" Text="<%# lbl_Save.Text %>"
                                                 OnClick="btnSave_click" ValidationGroup="RefundResponse" OnClientClick="return checkValidation(this);" /> 
						                    </div>
						                    </div>						                
						               </div>
						                                            
							            </td>
							        </tr>
							     
                        </ItemTemplate>
                        <FooterTemplate>
                                </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    
                    <uc1:TablePaging ID="TablePaging1" runat="server" />       
               </div>   
               
                
            </ContentTemplate>
        </asp:UpdatePanel>           
    </div>
</div>
            

<asp:Panel ID="panel_iframe" runat="server" CssClass="popModal_del2_ie popModal_del2_hide" style="display:none;"
     Width="750" Height="450">
     <div id="divIframeLoader" class="divIframeLoader">   
        <table>
            <tr>
                <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
            </tr>
        </table>
    </div>
    <div id="div_iframe_main">
        <div class="top"></div>
            <a id="a_close" runat="server" class="span_A" href="javascript:void(0);" onclick="javascript:CloseIframe();"></a>
        <iframe runat="server" id="_iframe" width="750px" height="560px" frameborder="0" ALLOWTRANSPARENCY="true" ></iframe>
        <div class="bottom2"></div>
    </div>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="_mpe" runat="server"
                TargetControlID="btn_virtual"
                PopupControlID="panel_iframe"
                 BackgroundCssClass="modalBackground"                  
                  BehaviorID="_modal"               
                  DropShadow="false">
    </cc1:ModalPopupExtender>
    
    <div style="display:none;">

        <asp:Button ID="btn_virtual" runat="server"  style="display:none;" CausesValidation="false"/>
    </div>
    



<asp:Label ID="lblTicketNumber" runat="server" Text="Ticket num" Visible="false"></asp:Label> 
<asp:Label ID="lblSupplierName" runat="server" Text="Advertiser" Visible="false"></asp:Label>         
<asp:Label ID="lblDate" runat="server" Text="Date" Visible="false"></asp:Label>        
<asp:Label ID="lblRefundRequests" runat="server" Text="Refund requests" Visible="false"></asp:Label>        
<asp:Label ID="lblApprovedRequest" runat="server" Text="Approved request" Visible="false"></asp:Label> 
 
 <asp:Label ID="lbl_SendToBlackList" runat="server" Text="Send to black list" Visible="false"></asp:Label>
 <asp:Label ID="lbl_Description" runat="server" Text="Description" Visible="false"></asp:Label>
 <asp:Label ID="lbl_PublisherReplay" runat="server" Text="Publisher replay" Visible="false"></asp:Label>
 <asp:Label ID="lbl_Save" runat="server" Text="Save" Visible="false"></asp:Label>
<asp:Label ID="lblPrimaryExpertise" runat="server" Text="Heading" Visible="false"></asp:Label>        
<asp:Label ID="lblRefundReason" runat="server" Text="Refund reason" Visible="false"></asp:Label>
<asp:Label ID="lblRecordings" runat="server" Text="Recording" Visible="false"></asp:Label>
<asp:Label ID="lblRefundStatus" runat="server" Text="Refund status" Visible="false"></asp:Label>
<asp:Label ID="lblPrice" runat="server" Text="Price" Visible="false"></asp:Label>
<asp:Label ID="lbl_NoResult" runat="server" Visible="false" Text="There are not results"></asp:Label>    
<asp:Label ID="lbl_all" runat="server" Visible="false" Text="All"></asp:Label> 
<asp:Label ID="lbl_RecordError" runat="server" Text="Server problem. Can not hear the recording" Visible="false"></asp:Label>
<asp:Label ID="lbl_BlackListProblem" runat="server" Text="Server problem. Can not insert to black list" Visible="false"></asp:Label>
<asp:Label ID="lbl_NumAlreadyExists" runat="server" Text="The number already exists" Visible="false"></asp:Label>
<asp:HiddenField ID="hf_ApproveRequest" runat="server" Value="Approve refund request" />
<asp:HiddenField ID="hf_RejectRequest" runat="server" Value="Reject refund request" />

 <asp:Label ID="lblTitleRefund" runat="server" Text="Refund request report" Visible="false"></asp:Label>
 <asp:Label ID="lbl_RefundThisCall" runat="server" Text="Refund this call" Visible="false"></asp:Label>

  <asp:Label ID="lbl_IfToExite" runat="server" Text="Are you sure you want to exit? Unsaved data will be lost!" Visible="false"></asp:Label>

 
</asp:Content>

