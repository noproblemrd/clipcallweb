﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;

public partial class Publisher_InjectionReport : PageSetting
{
    const int DAYS_INTERVAL = 2;
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(btn_Run);
        if (!IsPostBack)
        {
            SetToolbox();
            SetDatePicker();
            LoadInjections();
            LoadCampaigns();
            LoadCountries();
        }
        else
            SetCampainsPostBack();
        SetToolboxEvents();
        Header.DataBind();
    }

    private void SetCampainsPostBack()
    {
        foreach (ListItem li in cbl_Origins.Items)
            li.Attributes.Add("guid", li.Value);
    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lbl_TitleReport.Text);
    }

    private void LoadInjections()
    {
        ListItem li1 = new ListItem("ALL", Guid.Empty.ToString());
        //  li1.Attributes.Add("JSvalue", Guid.Empty.ToString());
        rbl_Injection.Items.Add(li1);

        rbl_Injection.SelectedIndex = 0;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionWithDAUs resultInjectionData = null;
        //Result<List<DataModel.Origins.InjectionsCockpit.InjectionWithDAUs>> GetInjectionsAndDAUs()
        try
        {
            resultInjectionData = _site.GetInjectionsAndDAUs();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (resultInjectionData.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        foreach (WebReferenceSite.InjectionWithDAUs data in resultInjectionData.Value)
        {
            ListItem li = new ListItem(data.InjectionName + " (" + data.DAUs + ")", data.InjecitonId.ToString());
            //      li.Attributes.Add("JSvalue", data.InjectionId.ToString());
            rbl_Injection.Items.Add(li);
        }
    }
    private void LoadCampaigns()
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfOriginWithDAUs result;
        try
        {
            result = _report.GetDistributionOriginsWithDAUs();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        cbl_Origins.Items.Clear();

        ListItem li1 = new ListItem("ALL", Guid.Empty.ToString());
        li1.Attributes.Add("guid", Guid.Empty.ToString());
        cbl_Origins.Items.Add(li1);

        cbl_Origins.SelectedIndex = 0;
        foreach (var gsp in result.Value)
        {
            ListItem li = new ListItem(gsp.OriginName + " (" + gsp.DAUs + ")", gsp.OriginId.ToString());
            //  li.Selected = true;
            li.Attributes.Add("guid", gsp.OriginId.ToString());
            cbl_Origins.Items.Add(li);
        }
    }
    private void LoadCountries()
    {
        string command = "SELECT IP2LocationName FROM dbo.Country ORDER BY Id";
        ddl_Country.Items.Clear();
        ListItem liFirst = new ListItem("ALL", string.Empty) { Selected = true };
        ddl_Country.Items.Add(liFirst);
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string country = (string)reader["IP2LocationName"];
                    ListItem li = new ListItem(country, country) { Selected = false };
                    ddl_Country.Items.Add(li);
                }
                conn.Close();
            }
        }
    }
    protected void btn_Run_Click(object sender, EventArgs e)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(siteSetting.GetUrlWebReference);
        WebReferenceReports.InjectionReportRequest _request = new WebReferenceReports.InjectionReportRequest();
        if (!string.IsNullOrEmpty(ddl_Country.SelectedValue))
            _request.Country = ddl_Country.SelectedItem.Text;
        List<Guid> origins = new List<Guid>();
        foreach (ListItem li in cbl_Origins.Items)
        {
            if (li.Selected)
            {
                if (li.Value == Guid.Empty.ToString())
                {
                    origins.Clear();
                    break;
                }
                origins.Add(Guid.Parse(li.Value));
            }
        }
        _request.Origins = origins.ToArray();
        _request.InjectionId = Guid.Parse(rbl_Injection.SelectedValue);
        _request.FromDate = FromToDatePicker_current.GetDateFrom;
        _request.ToDate = FromToDatePicker_current.GetDateTo;
        WebReferenceReports.ResultOfInjectionReportResponse _result = null;
        try
        {
            _result = _report.InjectionReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (_result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        VRequest = _request;
        DataTable data = new DataTable();
        data.Columns.Add("Date");
        data.Columns.Add("DateLng");
        data.Columns.Add("DAUs");
        data.Columns.Add("InjectionPercent");
        data.Columns.Add("Revenue");
        data.Columns.Add("DUV");
        data.Columns.Add("PPI_DAU");
        foreach (WebReferenceReports.InjectionReportResponseRow row in _result.Value)
        {
            DataRow data_row = data.NewRow();
            data_row["Date"] = (row.Date == DateTime.MaxValue) ? "Total" : string.Format(siteSetting.DateFormat, row.Date);
            data_row["DateLng"] = (row.Date == DateTime.MaxValue) ? "0" : row.Date.Ticks.ToString();
            data_row["DAUs"] = row.DAUS;
            data_row["InjectionPercent"] = (row.InjectionPercent < 0) ? "-" : string.Format(this.GetNumberFormat, row.InjectionPercent) + "%";
            data_row["Revenue"] = string.Format(this.GetNumberFormat, row.Revenue);
            data_row["DUV"] = (row.DUV < 0) ? "-" : string.Format(this.GetNumberFormat, row.DUV);
            data_row["PPI_DAU"] = row.PPI_DAU;
            data.Rows.Add(data_row);
        }
        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetTrTable", "SetTrTable();", true);
    }
    private void SetToolboxEvents()
    {
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
    }
    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {

        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, lbl_TitleReport.Text);

        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);

    }
    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }
        StringWriter stringWrite = new StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
        //    PageRenderin pr = new PageRenderin();
        div_table.RenderControl(htmlWrite);
        //    pr.Controls.Add(div_table);
        //    pr.RenderControl(htmlWrite);

        Session["string_print"] = stringWrite.ToString();
        Session["PrintStyle"] = ResolveUrl("~/Publisher/style.css");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void SetDatePicker()
    {
        FromToDatePicker_current.SetIntervalDateServer(DAYS_INTERVAL);
    }
    protected string GetCampainsOfInjection
    {
        get { return ResolveUrl("~/Publisher/InjectionReportService.asmx/GetCampainsOfInjection"); }
    }
    protected string GetPublishInjection
    {
        get { return ResolveUrl("~/Publisher/InjectionReportService.asmx/PublishInjection"); }
    }
    protected string update_injection_page
    {
        get { return ResolveUrl("~/Publisher/InjectionUpset.aspx"); }
    }
    protected string update_Campaign_page
    {
        get { return ResolveUrl("~/Publisher/CampaignUpset.aspx"); }
    }
    protected string injection_campaign_relation_page
    {
        get { return ResolveUrl("~/Publisher/InjectionCampaignRelation.aspx"); }
    }
    protected string ThisPath
    {
        get { return ResolveUrl("~/Publisher/InjectionReport.aspx"); }
    }
    protected string GetDrillDownPath
    {
        get { return ResolveUrl("~/Publisher/InjectionReportService.asmx/InjectionReportDrillDown"); }
    }
    WebReferenceReports.InjectionReportRequest VRequest
    {
        // get { return (Session["VRequest"] == null) ? new WebReferenceReports.InjectionReportRequest() : (WebReferenceReports.InjectionReportRequest)Session["VRequest"]; }
        set { Session["VInjectionReportRequest"] = value; }
    }
    DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }

}