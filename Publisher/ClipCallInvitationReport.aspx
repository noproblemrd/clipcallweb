﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ClipCallInvitationReport.aspx.cs" Inherits="Publisher_ClipCallInvitationReport" %>

<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
     <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function _CreateExcel() {
            var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"AarCallCenterReport"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
        function OpenCase(_path) {
           
            var _leadWin = window.open(_path, "LeadDetails", "width=1000, height=650");
            _leadWin.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="Date-field" runat="server" id="div_Date">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  			
		</div>
      
		<div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>        
        <div id="Table_Report"  runat="server" >  
				<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="date">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="Date"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblBroker" runat="server" Text="<%# Bind('Date') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="ToPhone">
						<HeaderTemplate>
							<asp:Label ID="Label101" runat="server" Text="To phone"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:Label ID="Label1111" runat="server" Text="<%# Bind('ToPhone') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="InvitationType">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="Invitation type"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label11" runat="server" Text="<%# Bind('InvitationType') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="InvitationFrom">
						<HeaderTemplate>
							<asp:Label ID="Label12" runat="server" Text="Invitation from"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:Label ID="Label13" runat="server" Text="<%# Bind('InvitationFrom') %>"></asp:Label>						
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="SupplierName">
						<HeaderTemplate>
							<asp:Label ID="Label14" runat="server" Text="Supplier name"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label15" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="SupplierPhone">
						<HeaderTemplate>
							<asp:Label ID="Label16" runat="server" Text="Supplier phone"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label17" runat="server" Text="<%# Bind('SupplierPhone') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>
                    
					<asp:TemplateField SortExpression="VideoLink">
						<HeaderTemplate>
							<asp:Label ID="Label18" runat="server" Text="Video"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<a id="a1" runat="server" href="<%# Bind('VideoLink') %>" target="_blank" visible="<%# Bind('HasVideo') %>">
                                <asp:Label ID="Label192" runat="server" Text="Video"></asp:Label>
                            </a>
						</ItemTemplate>
					</asp:TemplateField>

				</Columns>    
				</asp:GridView>
			</div>
        </div>
    </div>
    <asp:Label ID="lbl_ClipCallInvitationReport" runat="server" Text="ClipCall Invitation Report" Visible="false"></asp:Label>
</asp:Content>

