using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;


public partial class Publisher_PublisherLogin : PageSetting
{
 //   const string SITE_ID = "5";
 //   const string SITE_ID = "";
    const string SCRAMBEL = "*5106sp";
    bool IsInLogIn = false;
    protected void Page_Load(object sender, EventArgs e)
    {        
        //Security- for the browser not save in the cash memory
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");
        ////

        if (!IsPostBack)
        {
   //         if (userManagement.IsPublisher())
   //             Response.Redirect("SearchSuppliers.aspx");
       //     Session.Abandon();
//            string siteId = Request.QueryString["SiteId"];
           
            
            if (!string.IsNullOrEmpty(siteSetting.GetSiteID))
            {
                        
                setTextLogin();
            }
            if (Request.Cookies["PublishLogin"] != null)
            {
   //             txt_professionalEmail.Text = Request.Cookies["PublishLogin"].Values["PublishEmail"];
                string _password = Request.Cookies["PublishLogin"].Values["PublishPassword"];
                if (!string.IsNullOrEmpty(_password))
                {
                    try
                    {
                        _password = EncryptString.Decrypt_String(_password, SCRAMBEL);
                    }
                    catch (Exception exc) { }

                    string[] newPassword = _password.Split(';');
                    if (newPassword.Length == 2)
                    {
                        txt_professionalPassword.Attributes.Add("value", newPassword[0]);
                        txt_professionalEmail.Text = newPassword[1];
                    }
                }
            }
            if (Request.Cookies["rememberme"] != null && Request.Cookies["rememberme"].Value == "false")
                ifRememberMe.Checked = false;
        }
        lbl_version.DataBind();
   //     txt_professionalEmail.Attributes.Add("autocomplete", "off");
    }
   
    private void setTextLogin()
    {
        string command = "SELECT dbo.GetTextLoginBySiteNameId(@siteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@siteNameId", siteSetting.GetSiteID);
            lbl_LogInText.Text = (string)cmd.ExecuteScalar();
            conn.Close();
        }
    }
    protected void CheckCode_Enter(object sender, EventArgs e)
    {
        CheckLogIn();
    }
    protected void btnCheckCode_Click(object sender, EventArgs e)
    {
        CheckLogIn();
    }
    protected void CheckLogIn()
    {
        if (IsInLogIn)
            return;
        IsInLogIn = true;
        if (siteSetting == null || string.IsNullOrEmpty(siteSetting.GetSiteID))
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            if (!DBConnection.LoadSiteId(host, this))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
        }
        string email = txt_professionalEmail.Text;
        string password = txt_professionalPassword.Text;

        //Check if try to access multi times

        bool IsApprove = DBConnection.IsLoginApproved(email, siteSetting.GetSiteID);
        if (!IsApprove)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "AccountLocked", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AccountLocked.Text) + "');", true);
            return;
        }
        /**********/

  //      string guid;
        int securityLevel;
 //       string UserName;
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;
        try
        {
            _response = supplier.UserLogin(email, password);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
  //          ClientScript.RegisterStartupScript(this.GetType(), "dbug", "alert('" + ex.Message + "');", true);
            lblComment.Text = "Failed";
            return;
        }
        if (_response.Type != WebReferenceSupplier.eResultType.Success)
        {

    //        ClientScript.RegisterStartupScript(this.GetType(), "dbug", "alert('" + _response.Type.ToString() +";"+ supplier.Url+"');", true);
            lblComment.Text = "Failed";
            return;
        }
        if (_response.Value.UserLoginStatus == WebReferenceSupplier.eUserLoginStatus.NotFound)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Publisher);
            ClientScript.RegisterStartupScript(this.GetType(), "NotFound", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NotFound.Text) + "');", true);
            return;
        }
        if (_response.Value.AffiliateOrigin != Guid.Empty)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Publisher);
            ClientScript.RegisterStartupScript(this.GetType(), "AffiliateUser", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AffiliateUser.Text) + "'); window.location='" + ResolveUrl("~") + "Affiliate/AffiliateLogin.aspx';", true);
            return;
        }
        securityLevel = _response.Value.SecurityLevel;
        if (securityLevel < 1)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Publisher);
            Response.Redirect(ResolveUrl("~") + "Management/ProfessionLogin.aspx");
            return;
        }
        DBConnection.InsertLogin(email, _response.Value.UserId, siteSetting.GetSiteID, eUserType.Publisher);
 //       InsertLogin(email, _response.Value.UserId);
        userManagement = new UserMangement(_response.Value.UserId.ToString(),
            securityLevel.ToString(), _response.Value.Name, email, _response.Value.CanListenToRecords, _response.Value.TimeZoneMinutes);
      //  userManagement.CanListenToRecords = _response.Value.CanListenToRecords;
       
        userManagement.SetUserObject(this);

        int sitelangid = DBConnection.GetSiteLangIdByUserId(new Guid(userManagement.GetGuid), siteSetting.GetSiteID);
        bool IsDashboardHomePage = DBConnection.IsDashboardHomePage(new Guid(userManagement.GetGuid), siteSetting.GetSiteID);


        if (sitelangid > 0)
        {
            siteSetting.siteLangId = sitelangid;           
            Response.Cookies["language"].Value = sitelangid.ToString();
            Response.Cookies["language"].Expires = DateTime.Now.AddMonths(3);
        }
            

        string command = "EXEC dbo.GetPagesSecurityBySecurityLevel @SecurityLevel, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SecurityLevel", this.userManagement.GetSecurityLevel);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string pageName = (!reader.IsDBNull(0)) ? (string)reader["PageName"] :
                    (string)reader["ControlId"];
                if (!this.PageSecurity.ContainsKey(pageName))
                    this.PageSecurity.Add(pageName,
                        (bool)reader["IsConfirm"]);
            }
            this.SetPageSecurity = this.PageSecurity;
            LinkToSet.Clear();
            /*
            string link_advertiser = DBConnection.GetLinkToSet("a_Advertisers", userManagement.GetSecurityLevel, siteSetting.GetSiteID);
            if (string.IsNullOrEmpty(link_advertiser) || link_advertiser == "#")
                link_advertiser = "PublisherWelcome.aspx";
            LinkToSet.Add("a_Advertisers", link_advertiser);
            
            LinkToSet.Add("a_AccountSettings",
                DBConnection.GetLinkToSet("a_AccountSettings", userManagement.GetSecurityLevel, siteSetting.GetSiteID));
            LinkToSet.Add("a_Reports",
                DBConnection.GetLinkToSet("a_Reports", userManagement.GetSecurityLevel, siteSetting.GetSiteID));
            LinkToSet.Add("a_consumers",
                DBConnection.GetLinkToSet("a_consumers", userManagement.GetSecurityLevel, siteSetting.GetSiteID));
            */
            LinkToSet = DBConnection.GetLinksToSet(siteSetting.GetSiteID, userManagement.GetSecurityLevel);

            SaveLinkToSet();
            conn.Close();
        }
        if (ifRememberMe.Checked)
        {
            string newPassword = password + ";" + email;
            newPassword = EncryptString.Encrypt_String(newPassword, SCRAMBEL);
 //           Response.Cookies["PublishLogin"]["PublishEmail"] = email;
            Response.Cookies["PublishLogin"]["PublishPassword"] = newPassword;
            Response.Cookies["PublishLogin"].Expires = DateTime.Now.AddMonths(3);
            Response.Cookies["PublishLogin"].HttpOnly = true;
            Response.Cookies["rememberme"].Values.Clear();
        }
        else
        {
            Response.Cookies["PublishLogin"].Values.Clear();
            Response.Cookies["rememberme"].Value = "false";
            Response.Cookies["rememberme"].Expires = DateTime.Now.AddMonths(3);
        }
        
   //     siteSetting.SetDicLinks(true, this);
    //    dbug_log.UserLog(this);

        Response.Redirect((IsDashboardHomePage) ? "Dashboard.aspx" : "SearchSupplierApp.aspx", false);
    }
    /*
    void InsertLogin(string email, Guid _id)
    {
        string command = "EXEC dbo.InsertLogin @UserName, @SiteNameId, @UserId, @IP, @Success";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@UserName", email);
        cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
        cmd.Parameters.AddWithValue("@IP", Utilities.GetIP(Request));
        if (_id == Guid.Empty)
        {
            cmd.Parameters.AddWithValue("@UserId", DBNull.Value);
            cmd.Parameters.AddWithValue("@Success", false);
        }
        else
        {
            cmd.Parameters.AddWithValue("@UserId", _id);
            cmd.Parameters.AddWithValue("@Success", true);
        }
        int a = cmd.ExecuteNonQuery();
        conn.Close();
    }
     * */
    
}
