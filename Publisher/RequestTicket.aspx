﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RequestTicket.aspx.cs" Inherits="Publisher_RequestTicket" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<%@ Register src="~/Controls/Notes.ascx" tagname="Notes" tagprefix="uc1" %>
<%@ Register src="~/Controls/Upsale/UpsaleControl.ascx" tagname="UpsaleControl" tagprefix="uc2" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link type="text/css" rel="stylesheet" href="Style.css" /> 
    <link href="../AutoCompleteCity/StyleAutoComplete.css" rel="stylesheet" type="text/css"/>
    <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>  
    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript"  src="../CallService.js"></script>
    <script  type="text/javascript" src="../AutoCompleteCity/AutoCompleteCity.js"></script>
    <script type="text/javascript">
    window.onload = appl_init;
    
    function appl_init() {        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
        hideDiv();
        try{
            parent.ShowIframeDiv();
        }
        catch(ex){}
        
    }
    function BeginHandler()
    {     
 //       alert('in');  
        showDiv();
    }
    function EndHandler()
    {
//        alert('out');  
        hideDiv();
    }
    function OpenDetails()
    {
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Notes.ClientID %>").style.display = "none";
        document.getElementById("<%# div_affiliate.ClientID %>").style.display = "none";
        document.getElementById("<%# div_Details.ClientID %>").style.display = "block";
        if(is_explorer)
        {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_AffiliatePayStatus.ClientID %>").removeAttribute("className");
        }
        else
        {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_AffiliatePayStatus.ClientID %>").removeAttribute("class");
        }
        document.getElementById("<%# a_OpenDetails.ClientID %>").className = "selected";
    }
    function OpenNotes()
    {
        var div_Notes = document.getElementById("<%# div_Notes.ClientID %>");
        if(div_Notes.style.display == "block")
            return false;
        var div_affiliate = document.getElementById("<%# div_affiliate.ClientID %>");
        //if(div_affiliate)
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Details.ClientID %>").style.display = "none";
        div_affiliate.style.display = "none";
        div_Notes.style.display = "block";
        if(is_explorer)
        {
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_AffiliatePayStatus.ClientID %>").removeAttribute("className");
        }
        else
        {
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("class");        
            document.getElementById("<%# lb_AffiliatePayStatus.ClientID %>").removeAttribute("class");
        }
        document.getElementById("<%# lb_OpenNotes.ClientID %>").className = "selected";
        showDiv();
        return true;
    }
    function OpenAffiliates()
    {
        var div_affiliate = document.getElementById("<%# div_affiliate.ClientID %>");
        if(div_affiliate.style.display == "block")
            return false;
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Notes.ClientID %>").style.display = "none";
        document.getElementById("<%# div_affiliate.ClientID %>").style.display = "block";
        document.getElementById("<%# div_Details.ClientID %>").style.display = "none";
        if(is_explorer)
        {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("className");
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("className");
        }
        else
        {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("class");
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("class");
        }
        document.getElementById("<%# lb_AffiliatePayStatus.ClientID %>").className = "selected";
        return true;
    }
    function CloseMeError()
    {
        alert("<%# lbl_ErrorLoadData.Text %>");
        parent.CloseIframe()
    }
    function AreUSure() {
        return confirm("<%# AreUSure %>");
    }
    /*
    function OpenRecord(_id) {
        $.ajax({
            url: "<# RecordService %>",
            data: "{'_id': '" + _id + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var arg = data.d;
                if (arg.length == 0) {
                    alert(document.getElementById("<# hf_errorRecord.ClientID %>").value);
                    return;
                }

                var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
                if (is_chrome) {
                    oWin = window.open('<# GetAudioHTML5 %>?audio=' + arg, "recordWin", "resizable=0,width=500,height=330");
                    setTimeout("CheckPopupBlocker()", 2000);

                }
                else
                    window.location.href = arg;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                GeneralServerError();
            }

        });
    }
    */
    function OpenRecord(_id) {
        var oWin;
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('msie') > -1;
            if (is_chrome) {
                oWin = window.open('<%# GetMediaPlayer %>?audio=' + _id, "recordWin", "resizable=0,width=200,height=200");
    //            setTimeout("CheckPopupBlocker()", 2000);

            }
            else
               // window.location.href = arg;
                oWin = window.open('<%# GetAudioHTML5 %>?audio=' + _id, "recordWin", "resizable=0,width=400,height=250");

        }
    /*
    function CheckPopupBlocker() {
        if (_hasPopupBlocker(oWin))
            alert(document.getElementById('<%#Hidden_AllowPopUp.ClientID  %>').value);
    }
    */
    </script>
</head>
<body style="background-color:transparent">
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    <Services> 
        <asp:ServiceReference Path="AutoComplete.asmx" />         
        <asp:ServiceReference Path="~/Publisher/Auction/Auction.asmx" /> 
    </Services> 
     </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="_UpdatePanel_RequestStop" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
   
    <div runat="server" id="div_upsale" class="upsalebtn">
                <asp:ImageButton ID="img_upsaleV" runat="server" ImageUrl="../images/upsale.png"  CssClass="RemoveAddUpsale"/>
            </div>
             <div runat="server" id="div_Stop" class="upsalebtn div_Stop">
                 <asp:LinkButton ID="lb_StopRequest" runat="server" CssClass="StopRequest"
                     onclick="lb_StopRequest_Click" OnClientClick="return AreUSure();">STOP REQUEST</asp:LinkButton>
            </div>
            <div runat="server" id="div_requestStpped" class="upsalebtn">
                <asp:Label ID="lbl_requestStpped" runat="server" Text="Request manually stopped" CssClass="RequestManuallyStopped"></asp:Label>
            </div>
             </ContentTemplate>
    </asp:UpdatePanel>
    
     <div class="titlerequestTop">          
        <asp:Label ID="lbl_CurrentRequest_Title" runat="server" Text="Current Request Ticket"></asp:Label>:
        <asp:Label ID="lbl_RequestNum" runat="server"></asp:Label>
    </div>
     <div class="titlerequestTop" runat="server" id="div_ParentTicket">  
        <asp:HyperLink ID="HL_RequestParent" runat="server">      
            <asp:Label ID="lbl_TitleRequestParent" runat="server" Text="Parent Request Ticket"></asp:Label>:
            <asp:Label ID="lbl_RequestParentNum" runat="server"></asp:Label>
       </asp:HyperLink>
    </div>
    <asp:Repeater ID="_Repeater" runat="server" 
         >        
    <ItemTemplate>
         <div class="titlerequestTop">
            <asp:HyperLink ID="HL_Request" runat="server" NavigateUrl="<%# Bind('url') %>" CssClass="<%# Bind('class') %>">      
                <asp:Label ID="lblRequestTitle" runat="server" Text="<%# lbl_Request_Title.Text %>"></asp:Label>:
                <asp:Label ID="lbl_RequestNum" runat="server" Text="<%# Bind('num') %>"></asp:Label>
           </asp:HyperLink>
            
        </div>                                                                
    </ItemTemplate>
    </asp:Repeater>
    
   
    
    <div class="clear"></div>
    <div class="separator"></div>
    <div id="form-analyticscomp2">

    <div class="tabswrap2" style="display:block;" >
        <div id="Ticket_Tabs" class="tabs2" runat="server"><!-- here the red bar-->
          
            <ul id="ulTabs" runat="server">
                <li><a href="javascript:OpenDetails();" runat="server" id="a_OpenDetails" class="selected">Details</a></li>
                <li><asp:LinkButton ID="lb_OpenNotes" runat="server" Text="Notes" 
                    OnClientClick="return OpenNotes();" onclick="lb_OpenNotes_Click"></asp:LinkButton></li>
                <li><asp:LinkButton ID="lb_AffiliatePayStatus" runat="server" OnClientClick="return OpenAffiliates();"
                 Text="Affiliates" OnClick="lb_AffiliatePayStatus_click"></asp:LinkButton></li>
            </ul>
            
        </div>
        
        <div class="containertab2" style="position:relative;">
         <%// div details %>
            <div runat="server" id="div_Details">
               <div class="form-fieldlong">
                    <asp:Label ID="lbl_Title" runat="server" CssClass="label" Text="Title"></asp:Label>
                    <asp:TextBox ID="txt_Title" runat="server" CssClass="form-textMed label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-fieldshort">
                    <asp:Label ID="lbl_Heading" runat="server" CssClass="label" Text="Heading"></asp:Label>
                    <asp:TextBox ID="txt_Heading" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
               <div class="form-fieldshort">
                    <asp:Label ID="lbl_Region" runat="server" CssClass="label" Text="Region"></asp:Label>
                    <asp:TextBox ID="txt_Region" runat="server" CssClass="form-textshort label_region" ReadOnly="true"></asp:TextBox>
                </div>

                
                <div class="form-fieldlongdet">
                    <asp:Label ID="lbl_Description" runat="server" CssClass="label" Text="Description"></asp:Label>
                    <asp:TextBox ID="txt_Description" runat="server" ReadOnly="true" CssClass="form-textdesc label_ro" TextMode="MultiLine"></asp:TextBox>                    
                </div>
                <div class="form-fieldshort">
                    <asp:Label ID="lbl_Origin" runat="server"  CssClass="label" Text="Origin"></asp:Label>
                    <asp:TextBox ID="txt_Origin" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                 <div class="form-fieldshort">
                    <asp:Label ID="lbl_Consumer" runat="server" CssClass="label" Text="Consumer"></asp:Label>
                    <asp:TextBox ID="txt_Consumer" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                 <div class="form-fieldshort">
                    <asp:Label ID="lbl_Type" runat="server" CssClass="label" Text="Type"></asp:Label>
                    <asp:TextBox ID="txt_Type" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-fieldshort">
                    <asp:Label ID="lbl_CreatedOn" runat="server" CssClass="label" Text="Created on"></asp:Label>
                    <asp:TextBox ID="txt_CreatedOn" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                  <div class="form-fieldshort">
                    <asp:Label ID="lbl_WinningPrice" runat="server" CssClass="label" Text="Winning price"></asp:Label>
                    <asp:TextBox ID="txt_WinningPrice" runat="server"  CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                  <div class="form-fieldshort">
                    <asp:Label ID="lbl_ProvidedAdvertisers" runat="server" CssClass="label" Text="Provided advertisers"></asp:Label>
                    <asp:TextBox ID="txt_ProvidedAdvertisers" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                  <div class="form-fieldshort">
                    <asp:Label ID="lbl_RequestedAdveritsers" runat="server" CssClass="label" Text="Requested advertisers" style="font-size:11px;"></asp:Label>
                    <asp:TextBox ID="txt_RequestedAdveritsers" runat="server"  CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                 <div class="form-fieldshort">
                    <asp:Label ID="lbl_Revenue" runat="server" CssClass="label" Text="Revenue"></asp:Label>
                    <asp:TextBox ID="txt_Revenue" runat="server"  CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-fieldshort">
                    <asp:Label ID="lbl_Url" runat="server" CssClass="label" Text="Url"></asp:Label>
                    <asp:TextBox ID="txt_Url" runat="server"  CssClass="form-textshort label_ro" ReadOnly="true" ></asp:TextBox>
                </div>
                <div class="form-fieldshort">
                    <asp:Label ID="lbl_Control" runat="server" CssClass="label" Text="Control"></asp:Label>
                    <asp:TextBox ID="txt_Control" runat="server"  CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-fieldshort">
                    <asp:Label ID="lbl_Keyword" runat="server" CssClass="label" Text="Keyword"></asp:Label>
                    <asp:TextBox ID="txt_Keyword" runat="server"  CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="clear"></div>
                <br />              
                    <asp:GridView ID="_GridView"  Width="779px" CssClass="data-table2" runat="server" AutoGenerateColumns="false">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                    <Columns>
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Advertiser.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Status.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text="<%# Bind('Status') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_PredefinedPrice.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text="<%# Bind('PredefinedPrice') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_ActualPrice.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text="<%# Bind('ActualPrice') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label9" runat="server" Text="<%# lbl_Charged.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image10" runat="server" ImageUrl="<%# Bind('Charged') %>" />
                        </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label11" runat="server" Text="<%# lblCreatedOn.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label12" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label13" runat="server" Text="<%# lbl_Duration.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label14" runat="server" Text="<%# Bind('Duration') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label15" runat="server" Text="<%# lbl_AarCandidateStatus.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label16" runat="server" Text="<%# Bind('AarCandidateStatus') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label17" runat="server" Text="<%# lbl_IsAnsweringMachine.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label18" runat="server" Text="<%# Bind('IsAnsweringMachine') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label19" runat="server" Text="<%# lbl_DtmfPressed.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label20" runat="server" Text="<%# Bind('DtmfPressed') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label21" runat="server" Text="<%# lbl_IvrFlavor.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label22" runat="server" Text="<%# Bind('IvrFlavor') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label23" runat="server" Text="<%# lbl_IsReject.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label24" runat="server" Text="<%# Bind('IsReject') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label25" runat="server" Text="<%# lbl_Recording.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <a href="<%# Bind('Record') %>" class="recording" id="a_recording" runat="server" visible="<%#  Bind('HasRecord') %>"></a>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label26" runat="server" Text="<%# lbl_RejectReason.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label27" runat="server" Text="<%# Bind('RejectReason') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label126" runat="server" Text="<%# lbl_BillingRate.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label127" runat="server" Text="<%# Bind('BillingRate') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label226" runat="server" Text="<%# lbl_QualityRate.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label227" runat="server" Text="<%# Bind('QualityRate') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label128" runat="server" Text="<%# lbl_LeadBuyerName.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label129" runat="server" Text="<%# Bind('LeadBuyerName') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    </asp:GridView>
                
            </div>
            <%// div notes %>
            <div runat="server" id="div_Notes" style="display:none; width:100%; position:relative;">
                <uc1:Notes ID="Notes1" runat="server" />
            </div>
            <%// div Affiliate %>
            <div runat="server" id="div_affiliate" style="display:none; width:100%; position:relative;" >
                <asp:UpdatePanel ID="_UpdatePanelaffiliate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-fieldshort">
                    <asp:Label ID="lbl_Billing" CssClass="label" runat="server" Text="Payment status"></asp:Label>
                    <asp:DropDownList ID="ddl_Billing" runat="server" CssClass="form-selectcomp" AutoPostBack="true" 
                        onselectedindexchanged="ddl_Billing_SelectedIndexChanged" CausesValidation="false"  ValidationGroup="none" onchange="Page_BlockSubmit = false;">
                    </asp:DropDownList>
                    
                    </div>
                  <div class="form-fieldshort">
                
                
                    <asp:Label ID="lbl_BillingStatus" runat="server" CssClass="label" Text="Payment reasons"></asp:Label>
                    <asp:DropDownList ID="ddl_Status" CssClass="form-selectcomp" runat="server">
                    </asp:DropDownList>
                
                
                  </div>
              </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Button ID="btn_Set" runat="server" Text="Set" CssClass="form-submit" onclick="btn_Set_Click" />
            </div>
        </div>
    </div>
    </div>
    
    <uc2:UpsaleControl ID="_UpsaleControl" runat="server" YPosition="5"/>
    
    <div id="divLoader" class="divLoader">   
        <table>
            <tr>
                <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
            </tr>
        </table>
    </div>
    
    <asp:Label ID="lbl_Advertiser" runat="server" Text="Advertiser" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Status" runat="server" Text="Status" Visible="false"></asp:Label>
    <asp:Label ID="lbl_PredefinedPrice" runat="server" Text="Predefined price" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ActualPrice" runat="server" Text="Actual price" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Charged" runat="server" Text="Charged" Visible="false"></asp:Label>
    
     <asp:Label ID="lblCreatedOn" runat="server" Text="Created On" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Duration" runat="server" Text="Duration" Visible="false"></asp:Label>
    <asp:Label ID="lbl_AarCandidateStatus" runat="server" Text="Aar Candidate Status" Visible="false"></asp:Label>
    <asp:Label ID="lbl_IsAnsweringMachine" runat="server" Text="Is Answering Machine" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DtmfPressed" runat="server" Text="DTMF Pressed" Visible="false"></asp:Label>
    <asp:Label ID="lbl_IvrFlavor" runat="server" Text="Ivr Flavor" Visible="false"></asp:Label>
    <asp:Label ID="lbl_IsReject" runat="server" Text="Reject" Visible="false"></asp:Label>
    <asp:Label ID="lbl_RejectReason" runat="server" Text="Reject Reason " Visible="false"></asp:Label>
    <asp:Label ID="lbl_Recording" runat="server" Text="Recording" Visible="false"></asp:Label>

    <asp:Label ID="lbl_BillingRate" runat="server" Text="Billing Rate" Visible="false"></asp:Label>
    <asp:Label ID="lbl_LeadBuyerName" runat="server" Text="Lead buyer type" Visible="false"></asp:Label>
    <asp:Label ID="lbl_QualityRate" runat="server" Text="Rank" Visible="false"></asp:Label>
    
    <asp:Label ID="lbl_AreUSure" runat="server" Text="Are you sure you want to stop this in-progress request before fulfillment?" Visible="false"></asp:Label>

       <asp:Label ID="lbl_Request_Title" runat="server" Text="Request Ticket" Visible="false"></asp:Label>

    <asp:Label ID="lbl_ErrorLoadData" runat="server" Text="There was an error to load the details." Visible="false"></asp:Label>
    
    <asp:Label ID="lbl_NoResult" runat="server" Text="There are no result" Visible="false" ></asp:Label>

    <asp:HiddenField ID="hf_errorRecord" runat="server" Value="There are errors with the record" />
<asp:HiddenField ID="Hidden_AllowPopUp" runat="server" Value="Allow pop-up windows" />

    <asp:HiddenField ID="hf_IncidentId" runat="server" /> 
    <asp:HiddenField ID="hf_UpsaleId" runat="server" /> 
    </form>
</body>
</html>
