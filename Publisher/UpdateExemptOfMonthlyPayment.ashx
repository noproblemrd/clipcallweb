﻿<%@ WebHandler Language="C#" Class="UpdateExemptOfMonthlyPayment" %>

using System;
using System.Web;
using System.Collections.Generic;

public class UpdateExemptOfMonthlyPayment : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string result = "OK";
        context.Response.ContentType = "application/json";
        try
        {
            WebReferenceSupplier.Supplier service = WebServiceConfig.GetSupplierReference(new SiteSetting(context.Request["siteId"]).GetUrlWebReference);

            List<WebReferenceSupplier.AuditEntry> auditEntries = new List<WebReferenceSupplier.AuditEntry>();
            WebReferenceSupplier.AuditEntry entry = new WebReferenceSupplier.AuditEntry();
            auditEntries.Add(entry);
            Guid supplierId = new Guid(context.Request["supplierGuid"]);
            entry.AdvertiseId = supplierId;
            entry.AdvertiserName = context.Request["supplierName"];
            entry.AuditStatus = WebReferenceSupplier.Status.Update;
            entry.CreatedOn = DateTime.Now;
            entry.FieldId = "ExemptOfMonthlyPayment";
            entry.FieldName = "ExemptOfMonthlyPayment";
            bool isExempt = bool.Parse(context.Request["newExemptValue"]);
            string isExemptOriginal = context.Request["originalState"] == "1" ? "True" : "False";
            entry.NewValue = isExempt.ToString();
            entry.OldValue = isExemptOriginal;
            entry.PageId = "Deposits.aspx";
            entry.PageName = "Deposits";
            entry.UserId = new Guid(context.Request["userId"]);
            entry.UserName = context.Request["userName"];

            var response = service.UpdateExemptionOfMontlyPayment(
                new WebReferenceSupplier.UpdateExemptionOfMonthlyPaymentRequest
                {
                    AuditOn = true,
                    SupplierId = supplierId,
                    IsExemptOfMonthlyPayment = isExempt,
                    AuditEntries = auditEntries.ToArray()
                });
            if (response.Type == WebReferenceSupplier.eResultType.Failure)
                result = "failure";
        }
        catch (Exception exc)
        {
            result = "error";
            dbug_log.ExceptionGlobalLog(exc);
        }
        System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
        context.Response.Write(jss.Serialize(result));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}