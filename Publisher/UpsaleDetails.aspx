﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpsaleDetails.aspx.cs" Inherits="Publisher_UpsaleDetails"  %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/Upsale/UpsaleControl.ascx" tagname="UpsaleControl" tagprefix="uc2" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../general.js"></script>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <link href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css"/>
    
    <script src="../jQuery/Scripts/jquery-1.6.2.min.js" type="text/javascript"></script> 
    <script src="../jQuery/Scripts/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script> 
        <script type="text/javascript"  src="../CallService.js"></script>
        <link href="../AutoCompleteCity/StyleAutoComplete.css" rel="stylesheet" type="text/css"/>
<script  type="text/javascript" src="../AutoCompleteCity/AutoCompleteCity.js"></script>


    <script type="text/javascript" >
        window.onload = appl_init;
        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);
            LoadDatePicker();
        }
        function BeginHandler() {
            showDiv();
        }
        function EndHandler() {
            hideDiv();
            LoadDatePicker();
        }
        function LoadDatePicker() {
            $(function () {
                $('._datepicker').datepicker({
                    onClose: function (dateText, inst) { OnBlurDatePicker(this); },
                    dateFormat: '<%# GetDateFormatToDatePicker %>'
                });
            });
        }
        function OnBlurDatePicker(elm) {
            var _format = "<%# siteSetting.DateFormatClean %>";
            if (!validate_Date(elm.value, _format)) {
                alert("<%# GetWrongFormatDate %>");
                var _date = new Date();
                elm.value = GetDateString(_date, _format);
                elm.focus();
                elm.select();
                return false;
            }
            return true;
        }
       
         function SetDate() {
             var _date = new Date();
             document.getElementById("<%# hf_Date.ClientID %>").value = _date.getFullYear() + ";" + _date.getMonth()
              + ";" + _date.getDate() + ";" + _date.getHours() + ";" + _date.getMinutes();
         }
         function OnUpsaleDone() {
             parent.CloseUpsaleDetails();
         }
    
    </script>

</head>
<body style="background-color:transparent;">
    <form id="form1" runat="server">
    
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">        
    <Services> 
        <asp:ServiceReference Path="AutoComplete.asmx" />         
        <asp:ServiceReference Path="~/Publisher/Auction/Auction.asmx" /> 
    </Services> 
    </cc1:ToolkitScriptManager>

    <div runat="server" id="div_upsale" class="upsalebutons">
        <div class="upsalebtn2">
        <asp:ImageButton ID="img_Save" runat="server" ImageUrl="../images/upsalesave.png" CssClass="RemoveAddUpsale" OnClick="img_Save_click" OnClientClick="SetDate();"/>
        </div>
        <div class="upsalebtn2">
        <asp:ImageButton ID="img_upsaleV" runat="server" ImageUrl="../images/upsale.png"  CssClass="RemoveAddUpsale"/>
        </div>
    </div>
    
    
    <asp:HiddenField ID="hf_Date" runat="server" />
    <div class="titlerequest2">
        
        <asp:Label ID="lbl_Title_Title" runat="server" Text="Upsale"></asp:Label>:
        <asp:Label ID="lbl_Title_Name" runat="server"></asp:Label>
        
    </div>
    <div class="clear"></div>
    <div id="form-analyticscomp2">

        <div class="tabswrap3" style="display:block;" >
            <div id="Ticket_Tabs" class="tabs2" runat="server">
          
                <ul id="ulTabs" runat="server">
                    <li><a runat="server" id="a_OpenDetails" class="selected">Details</a></li>                   
                </ul>
            
            </div>

            <div class="containertab3" style="position:relative;">
                <div runat="server" id="div_Details">
                    <div class="form-fieldshort">
                        <asp:Label ID="lbl_Number" runat="server" CssClass="label" Text="Number"></asp:Label>
                        <asp:TextBox ID="txt_Number" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                   <div class="form-fieldshort">
                        <asp:Label ID="lbl_Consumer" runat="server" CssClass="label" Text="Consumer"></asp:Label>
                        <asp:TextBox ID="txt_Consumer" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-fieldshort">
                        <asp:Label ID="lbl_TotalRequested" runat="server" CssClass="label" Text="Total requested advs."></asp:Label>
                        <asp:TextBox ID="txt_TotalRequested" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>

                     <div class="form-fieldshort" >
                        <asp:Label ID="lbl_TotalConnected" runat="server" CssClass="label" Text="Total connected advs."></asp:Label>
                        <asp:TextBox ID="txt_TotalConnected" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>

                    <br />

                    <div class="form-fieldshort">
                        <asp:Label ID="lbl_PrimaryHeading" runat="server" CssClass="label" Text="Primary heading"></asp:Label>
                        <asp:TextBox ID="txt_PrimaryHeading" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                    <asp:UpdatePanel ID="_UpdateStatuses" runat="server" UpdateMode="Conditional" style="float:left;">
                    <ContentTemplate>                    
                        <div class="form-fieldshort">
                            <asp:Label ID="Label2" runat="server" CssClass="label" Text="<%# lbl_Status.Text %>"></asp:Label>
                            <asp:DropDownList ID="ddl_Status" runat="server" AutoPostBack="true"
                            CausesValidation="false"  ValidationGroup="none" 
                                onchange="Page_BlockSubmit = false;" 
                                onselectedindexchanged="ddl_Status_SelectedIndexChanged" CssClass="form-select2">
                            </asp:DropDownList>
                        </div>
                        <div class="form-fieldshort">
                            <asp:Label ID="lbl_StatusReason" runat="server" CssClass="label" Text="Status reason"></asp:Label>
                            <asp:DropDownList ID="ddl_StatusReason" runat="server" CssClass="form-select2">
                            </asp:DropDownList>
                        </div>
                        <div class="form-fieldcompfollow2" >
                        
                            <asp:Label ID="lbl_FollowUp" runat="server" CssClass="label" Text="Time to call"></asp:Label>

                            
                            <asp:TextBox ID="txt_FollowUp" runat="server" CssClass="_datepicker form-textcompfollow" style="width:72px;font-size:8pt;display:inline;vertical-align:top;"></asp:TextBox>
                            
                            
                        </div>
                        <div class="form-fieldcomptime2" style="margin-top: 19px;">
                        <asp:DropDownList ID="ddl_Time" CssClass="form-selectcomptime" runat="server">
                            </asp:DropDownList> 
                        </div>
                        

                    </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="form-fieldlong3">
                        <asp:Label ID="lbl_Cities" runat="server" Text="Regions" CssClass="label"></asp:Label>
                         <asp:TextBox ID="txt_Cities" runat="server" CssClass="form-textlong label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                    
                </div>
                
                <asp:GridView ID="_GridView"  Width="625px" CssClass="data-table2" runat="server" AutoGenerateColumns="false">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <Columns>
                    <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# lbl_DateCreate.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('CallTime') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label3" runat="server" Text="<%# lbl_RequestNumber.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text="<%# Bind('CaseNumber') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label31" runat="server" Text="<%# lbl_Type.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label42" runat="server" Text="<%# Bind('CaseType') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label51" runat="server" Text="<%# lbl_Region.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label61" runat="server" Text="<%# Bind('RegionName') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label5" runat="server" Text="<%# lbl_AdvertiserName.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label7" runat="server" Text="<%# lbl_Status.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text="<%# Bind('CallStatus') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            </div>

            


        </div>

    </div>
    <asp:PlaceHolder ID="_PlaceHolderUpsale" runat="server"></asp:PlaceHolder>


    <asp:label ID="lbl_DateCreate" runat="server" text="Date create" Visible="false"></asp:label>
    <asp:label ID="lbl_RequestNumber" runat="server" text="Request number" Visible="false"></asp:label>
    <asp:label ID="lbl_AdvertiserName" runat="server" text="Advertiser name" Visible="false"></asp:label>
    <asp:label ID="lbl_Status" runat="server" text="Status" Visible="false"></asp:label>
    <asp:label ID="lbl_Region" runat="server" text="Region" Visible="false"></asp:label>
    <asp:label ID="lbl_Type" runat="server" text="Type" Visible="false"></asp:label>
    <div id="divLoader" class="divLoader">   
        <table>
            <tr>
                <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
            </tr>
        </table>
    </div>
    
                   
<asp:Label ID="lbl_NoResult" runat="server" Text="There are no result" Visible="false" ></asp:Label>
<asp:Label ID="lbl_InvalidDate" runat="server" Text="The 'time to call' should be bigger or equal to current date" Visible="false" ></asp:Label>

<asp:Label ID="lbl_WrongFormatDate" runat="server" Text="Wrong format date" Visible="false" ></asp:Label>

<asp:HiddenField ID="hf_UpsaleId" runat="server" />  
    </form>
</body>
</html>
