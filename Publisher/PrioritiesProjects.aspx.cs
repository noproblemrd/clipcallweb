﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_PrioritiesProjects : PublisherPage
{
    protected readonly string EXCEL_NAME = "ClipCallPrioritiesProjectsReport";
    protected readonly string SessionTableName = "data_PrioritiesProjects";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            ExecPrioritiesReport();
        }
        SetToolbox();
        Header.DataBind();
    }

    private void ExecPrioritiesReport()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfListOfPriorityProjectMinData result = null;
        try
        {
            result = report.GetPriorityProjects();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }

        DataTable table = new DataTable();
        table.Columns.Add("ProjectId");
        table.Columns.Add("IncidentAccountId");
        table.Columns.Add("CustomerName");
        table.Columns.Add("SupplierName");
        table.Columns.Add("ScheduledMeeting");
        table.Columns.Add("eventType");
        table.Columns.Add("FollowUp");
        table.Columns.Add("ProjectView");
        foreach(ClipCallReport.PriorityProjectMinData project in result.Value)
        {
            DataRow row = table.NewRow();
            row["ProjectId"] = project.ProjectId;
            row["IncidentAccountId"] = project.IncidentAccountId;
            row["CustomerName"] = project.CustomerName;
            row["SupplierName"] = project.SupplierName;
            if (project.ScheduledMeeting != DateTime.MinValue)
                row["ScheduledMeeting"] = GetDateTimeForClient(project.ScheduledMeeting);
            row["eventType"] = project.eventType.ToString();
            if (project.FollowUp != DateTime.MinValue)
                row["FollowUp"] = GetDateTimeForClient(project.FollowUp);
            row["ProjectView"] = ProjectViewUrl + project.IncidentAccountId;
            table.Rows.Add(row);
        }
        gv_prioritiesProjects.DataSource = table;
        gv_prioritiesProjects.DataBind();

        dataS = table;
    }

    private void SetToolbox()
    {
        ToolboxReport1.SetTitle("Priorities Report");
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected DataTable dataS
    {
        get { return Session[SessionTableName] == null ? new DataTable() : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
   
}