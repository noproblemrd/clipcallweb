﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Publisher_InvalidRequests : PageSetting
{
    const string REPORT_NAME = "InvalidRequest";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            LoadBadWord();
            LoadBlackList();
  //          RegularExpressionValidator_BlackList.ValidationExpression = siteSetting.GetMobileRegularExpression;
            LoadRequestStatus();
            Loadorigins();
            SetToolbox();

        }
        SetToolboxEvents();
        Header.DataBind();
    }
    private void Loadorigins()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetAllOrigins(false);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Affiliate.Items.Clear();
        ddl_Affiliate.Items.Add(new ListItem(lbl_all.Text, Guid.Empty.ToString()));
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            ListItem li = new ListItem(gsp.Name, gsp.Id.ToString());
            ddl_Affiliate.Items.Add(li);
        }
    }
    private void LoadBlackList()
    {
        BlackListService bls = new BlackListService();
        bls.SetBlackList(siteSetting.GetUrlWebReference);
    }

    private void LoadBadWord()
    {
        BadWordService bws = new BadWordService();
        bws.SetBadWords(siteSetting.GetUrlWebReference);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lblTitleInvalidRequests.Text);
    }
    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, REPORT_NAME);
        if (!te.ExecExcel(dataV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordToPrint + "');", true);
            return;
        }
 //       PrintHelper.PrintWebControl(_GridView, dataV);
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        string badWord = txt_BadWord.Text.Trim();
        string phoneNum = txt_BlackList.Text.Trim();
        DateTime _from_date = FromToDate1.GetDateFrom;
        DateTime _to_date = FromToDate1.GetDateTo;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.InvalidRequestsReportRequest _request = new WebReferenceReports.InvalidRequestsReportRequest();
        if (!string.IsNullOrEmpty(badWord))
            _request.BadWord = badWord;
        if (!string.IsNullOrEmpty(phoneNum))
            _request.PhoneNumber = phoneNum;
        if (_from_date != DateTime.MinValue && _to_date != DateTime.MinValue)
        {
            _request.FromDate = _from_date;
            _request.ToDate = _to_date;
        }
        WebReferenceReports.InvalidRequestStatus irs = WebReferenceReports.InvalidRequestStatus.All;
        string _invalidStatus = ddl_ShowMe.SelectedValue;
        foreach (WebReferenceReports.InvalidRequestStatus _val in Enum.GetValues(typeof(WebReferenceReports.InvalidRequestStatus)))
        {
            if (_val.ToString() == _invalidStatus)
                irs = _val;
        }
        _request.InvalidStatus = irs;
        WebReferenceReports.ResultOfInvalidRequestsReportResponse _result = new WebReferenceReports.ResultOfInvalidRequestsReportResponse();
        try
        {
            _result = _report.InvalidRequestsReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (_result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        DataTable data = GetDataTable.GetDataTableColumsString(_result.Value.InvalidRequests);
        data.Columns.Add("CaseScript", typeof(string));
        foreach (WebReferenceReports.InvalidRequestData ird in _result.Value.InvalidRequests)
        {
            DataRow row = data.NewRow();
            row["CaseNumber"] = ird.CaseNumber;
            row["CreatedOn"] = String.Format("{0:d/M/yyyy HH:mm:ss}", ird.CreatedOn);
            row["ConsumerPhoneNumber"] = ird.ConsumerPhoneNumber;
            row["Title"] = ird.Title;
            row["Description"] = ird.Description;
            row["InvalidStatus"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "InvalidRequestStatus", ird.InvalidStatus.ToString());
            string _id = ird.CaseId.ToString();//resultRequest.Value.Incidents[i].CaseId.ToString();
            string script = "javascript:OpenIframe('RequestTicket.aspx?RequestTicket=" + _id + "');";
            row["CaseScript"] = script;
            data.Rows.Add(row);
        }
        _GridView.PageIndex = 0;
        _GridView.DataSource = data;
        lbl_RecordMached.Text = ToolboxReport1.GetRecordMaches(data.Rows.Count);
        _GridView.DataBind();
        _updatePanel.Update();
        dataV = data;
        if (data.Rows.Count == 0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResults", "alert('" + HttpUtility.JavaScriptStringEncode(Master.GetNoResultsMessage) + "');", true);

    }
    private void LoadRequestStatus()
    {
        List<WebReferenceReports.InvalidRequestStatus> dic = new List<WebReferenceReports.InvalidRequestStatus>();
        foreach (WebReferenceReports.InvalidRequestStatus _val in Enum.GetValues(typeof(WebReferenceReports.InvalidRequestStatus)))
        {
            dic.Add(_val);

        }
        InvalidRequestsStatusComprar stc = new InvalidRequestsStatusComprar();
        dic.Sort(stc);
        foreach (WebReferenceReports.InvalidRequestStatus irs in dic)
        {
            string _value = irs.ToString();
            string _txt = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "InvalidRequestStatus", _value);
            bool IsSelect = (irs == WebReferenceReports.InvalidRequestStatus.All);
            ListItem li = new ListItem();
            li.Value = _value;
            li.Text = _txt;
            li.Selected = IsSelect;
            //hide InvalidPhoneNumber
            if (_value == WebReferenceReports.InvalidRequestStatus.InvalidPhoneNumber.ToString())
                li.Enabled = false;
            ddl_ShowMe.Items.Add(li);
        }
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        _updatePanel.Update();
    }
    DataTable dataV
    {
        get
        {
            return (ViewState["data"] == null) ? new DataTable() :
                (DataTable)ViewState["data"];
        }

        set { ViewState["data"] = value; }
    }
}
