﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallLeadChat : PublisherPage
{
    protected enum eMessageType
    {
        BOT,
        TAD,
        SURVEY_PICKLIST
    }
    const string FOULS_TEXT = "{0} has {1} foul/s in total. {2} are market for this project.";
    const string BASE_PATH = "~/Publisher/ClipCallControls/";
    static List<string> ALL_LIST;
    static List<string> PRO_LIST;
    static List<string> CUST_LIST;

    static List<string> TOD_ALL_LIST;
    static List<string> TOD_PRO_LIST;
    static List<string> TOD_CUST_LIST;
    /*
    static string img_seen;
    static string img_delivered;
    static string img_sent;
    */
    static Publisher_ClipCallLeadChat()
    {
        LoadMessages();
        /*
        img_seen = "~/Publisher/images/icons8-Double Tick_50_seen.png";
        img_delivered = "~/Publisher/images/icons8-Double Tick_50.png";
        img_sent = "~/Publisher/images/icons8-Checkmark_50.png";
         */
        
    }
    static void LoadMessages()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfListOfSideMessagePair result = null;
        try
        {
            result = report.GetClipCallBotMessages();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return;
        }
        ALL_LIST = new List<string>() { string.Empty };
        PRO_LIST = new List<string>() { string.Empty };
        CUST_LIST = new List<string>() { string.Empty };
        TOD_ALL_LIST = new List<string>() { string.Empty };
        TOD_PRO_LIST = new List<string>() { string.Empty };
        TOD_CUST_LIST = new List<string>() { string.Empty };
        foreach (ClipCallReport.SideMessagePair smp in result.Value)
        {
            if (smp.TypeMessage == ClipCallReport.eTypeMessage.BOT)
            {
                switch (smp.Side)
                {
                    case (ClipCallReport.eSideMessage.All):
                        ALL_LIST.Add(smp.Message);
                        break;
                    case (ClipCallReport.eSideMessage.Customer):
                        CUST_LIST.Add(smp.Message);
                        break;
                    case (ClipCallReport.eSideMessage.Pro):
                        PRO_LIST.Add(smp.Message);
                        break;
                    case (ClipCallReport.eSideMessage.NULL):
                        ALL_LIST.Add(smp.Message);
                        CUST_LIST.Add(smp.Message);
                        PRO_LIST.Add(smp.Message);
                        break;
                }
            }
            else
            {
                switch (smp.Side)
                {
                    case (ClipCallReport.eSideMessage.All):
                        TOD_ALL_LIST.Add(smp.Message);
                        break;
                    case (ClipCallReport.eSideMessage.Customer):
                        TOD_CUST_LIST.Add(smp.Message);
                        break;
                    case (ClipCallReport.eSideMessage.Pro):
                        TOD_PRO_LIST.Add(smp.Message);
                        break;
                    case (ClipCallReport.eSideMessage.NULL):
                        TOD_ALL_LIST.Add(smp.Message);
                        TOD_CUST_LIST.Add(smp.Message);
                        TOD_PRO_LIST.Add(smp.Message);
                        break;
                }
            }
        }
        
    }
    Guid incidentAccountId;
    protected void Page_Load(object sender, EventArgs e)
    {
        string ia = Request.QueryString["ia"];
        if(!Guid.TryParse(ia, out incidentAccountId))
        {
            ClosePopupByError("Missing id in query string");
            return;
        }
        if (!IsPostBack)
        {
            LoadChatGeneralDetails();
            LoadChat();
            LoadDestinations();
            LoadMessageTypes();

        }
        else
            LoadSriptMessages();
     //   LoadGenericMessages();
        Page.Controls.Add(Utilities.GetDivLoader());
        Header.DataBind();
    }
   
    private void LoadMessageTypes()
    {
        ddl_TypeOfMessage.Items.Clear();
        foreach(eMessageType messageType in Enum.GetValues(typeof(eMessageType)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(messageType), messageType.ToString());
            li.Selected = messageType == eMessageType.BOT;
            if (messageType != eMessageType.BOT)
                li.Attributes.Add("style", "color:red;");
            ddl_TypeOfMessage.Items.Add(li);
        }
        //Utilities.GetEnumStringFormat
    }

    private void LoadChatGeneralDetails()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfIncidentDetailsForChat result = null;
        try
        {
            result = report.GetGeneralChatDetails(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClosePopupByError("Exception!" + exc.Message);
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            ClosePopupByError("Failure!");
            return;
        }
        hf_customerHassurvey.Value = result.Value.HasActiveCustomerSurvey.ToString().ToLower();
        hf_supplierHassurvey.Value = result.Value.HasActiveSupplieSurvey.ToString().ToLower();

        lbl_fouls.Text = string.Format(FOULS_TEXT, result.Value.SupplierName, result.Value.FoulsDetails.Fouls, result.Value.FoulsDetails.FoulsThisProject);
        hl_fouls.NavigateUrl = string.Format(FOULS_MANAGMENT_URL, result.Value.SupplierId);
        /*
        foreach (ListItem li in cb_SurveyDestination.Items)
        {
            
            if ((li.Value == "Supplier" && result.Value.HasActiveSupplieSurvey) || (li.Value == "Customer" && result.Value.HasActiveCustomerSurvey))
            {
                li.Selected = true;
                li.Enabled = false;
            }            
        }
         * */
        div_cancelSupplierSurvey.Visible = result.Value.HasActiveSupplieSurvey;
        div_cancelCustomerSurvey.Visible = result.Value.HasActiveCustomerSurvey;
    }

   

    private void LoadGenericMessages(string customerName, string proName, string projectId)
    {
        customerName = customerName ?? "ClipCall customer";

        List<string>  AllList = GetListWithNames(ALL_LIST, customerName, proName, projectId);
        List<string>  ProList = GetListWithNames(PRO_LIST, customerName, proName, projectId);
        List<string>  CustList = GetListWithNames(CUST_LIST, customerName, proName, projectId);

        List<string> TodAllList = GetListWithNames(TOD_ALL_LIST, customerName, proName, projectId);
        List<string> TodProList = GetListWithNames(TOD_PRO_LIST, customerName, proName, projectId);
        List<string> TodCustList = GetListWithNames(TOD_CUST_LIST, customerName, proName, projectId);

        lbl_LeadNum.Text = projectId;
        /*
        ddl_messages.Items.Clear();
        foreach (string mes in AllList)
        {
            ddl_messages.Items.Add(mes);
        }
        ddl_messages.SelectedIndex = 0;
         * */
        string _script = "SetMessages(" + JsonConvert.SerializeObject(ProList) + "," + JsonConvert.SerializeObject(CustList) + "," + JsonConvert.SerializeObject(AllList) + "," +
            JsonConvert.SerializeObject(TodProList) + "," + JsonConvert.SerializeObject(TodCustList) + "," + JsonConvert.SerializeObject(TodAllList) + ");";
        SetMessagesScript = _script;
       // ScriptManager.RegisterStartupScript(this, this.GetType(), "SetMessages", _script, true);
        LoadSriptMessages();
    }
    private void LoadSriptMessages()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetMessages", SetMessagesScript, true);
    }
    private List<string> GetListWithNames(List<string> list, string customerName, string proName, string projectId)
    {
        List<string> newList = new List<string>();
        foreach(string s in list)
            newList.Add(s.Replace("{CustName}", customerName).Replace("{ProName}", proName).Replace("{ProjectId}", projectId));
        return newList;
    }
    private void LoadDestinations()
    {
        ddl_destination.Items.Clear();
        foreach(ClipCallReport.eSystemChatMessageDestination dest in Enum.GetValues(typeof(ClipCallReport.eSystemChatMessageDestination)))
        {
            ListItem li = new ListItem(dest.ToString(), dest.ToString());
            li.Selected = dest == ClipCallReport.eSystemChatMessageDestination.All;
        //    li.Attributes.Add("style", "color:red;");
            ddl_destination.Items.Add(li);
        }
    }
   
    
    private void LoadChat()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfLayerChatReportResponse result = null;
        try
        {
            result = report.GetChat(incidentAccountId, null);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClosePopupByError("Exception!" + exc.Message);
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            ClosePopupByError("Failure!");
            return;
        }
        if (result.Value.status != ClipCallReport.HttpStatusCode.OK && result.Value.status != ClipCallReport.HttpStatusCode.PartialContent)
        {
            ClosePopupByError(result.Value.status.ToString());
            return;
        }
        /*
        DataTable dt = new DataTable();
        dt.Columns.Add("SentAt");
        dt.Columns.Add("By");
        dt.Columns.Add("ProMessage");
        dt.Columns.Add("CustMessage");
        dt.Columns.Add("ShowLable", typeof(bool));
        dt.Columns.Add("ShowLink", typeof(bool));
        dt.Columns.Add("ProScript");
        dt.Columns.Add("CustScript");
        dt.Columns.Add("imgPro");
        dt.Columns.Add("imgCust");

        foreach(ClipCallReport.LayerChatMessageResponse message in result.Value.layerChatResponse)
        {
            DataRow row = dt.NewRow();
            row["SentAt"] = message.SentAt.GetDateForClient();// string.Format(siteSetting.DateTimeFormat, message.SentAt);
            row["By"] = message.UserId == Guid.Empty ? "ClipCall-bot" :
                message.UserId == result.Value.CustomerId ? result.Value.CustomerName :
                message.UserId == result.Value.SupplierId ? result.Value.SupplierName : "unknown";
            row["ProMessage"] = message.ProMessage;
            row["CustMessage"] = message.CustMessage;
            bool showLink = message.MessageType == ClipCallReport.eLayerChatMessageType.IMAGE || message.MessageType == ClipCallReport.eLayerChatMessageType.QUOTE || 
                message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO ||  message.MessageType == ClipCallReport.eLayerChatMessageType.AUDIO ||
                message.MessageType == ClipCallReport.eLayerChatMessageType.PHONE_CALL || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO_CHAT;
            row["ShowLink"] = showLink;
            row["ShowLable"] = !showLink;
            string script = null;
            if (message.MessageType == ClipCallReport.eLayerChatMessageType.QUOTE)
                script = "return OpenWin('" + ResolveUrl("~/Publisher/QuoteTicket.aspx?quoteid=") + message.ContentData + "');";
            else if (message.MessageType == ClipCallReport.eLayerChatMessageType.IMAGE)// || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO)
                script = "return OpenWin('" + message.ContentData + "');";
            else if(message.MessageType == ClipCallReport.eLayerChatMessageType.AUDIO || message.MessageType == ClipCallReport.eLayerChatMessageType.PHONE_CALL
                || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO_CHAT || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO)
            {
                string audioUrl;
                if (message.MessageType == ClipCallReport.eLayerChatMessageType.PHONE_CALL || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO_CHAT)
                {
                    MediaChatBody phoneCallBody = JsonConvert.DeserializeObject<MediaChatBody>(message.ContentData);
                    audioUrl = phoneCallBody.url;
                }
                else
                    audioUrl = message.ContentData;
                GenerateVideoLink gvl = new GenerateVideoLink(audioUrl, null);
                script = "return OpenWin('" + gvl.GenerateFullUrl() + "');";                
            }
            row["ProScript"] = script;
            row["CustScript"] = script;

            row["imgPro"] = message.ProRecipientStatus == "read" ? img_seen : message.ProRecipientStatus == "delivered" ? img_delivered : img_sent;
            row["imgCust"] = message.CustomerRecipientStatus == "read" ? img_seen : message.CustomerRecipientStatus == "delivered" ? img_delivered : img_sent;

            dt.Rows.Add(row);
        }
        _GridView.DataSource = dt;
        _GridView.DataBind();
         * */
        hf_incidentAccountId.Value = incidentAccountId.ToString();
        if (result.Value.status == ClipCallReport.HttpStatusCode.PartialContent)
            hf_nextMessageId.Value = result.Value.layerChatResponse[result.Value.layerChatResponse.Length - 1].MessageId;
        _ChatControl.LoadData(result.Value);

        LoadGenericMessages(result.Value.CustomerName, result.Value.SupplierName, result.Value.ProjectId);
    }
     
    void ClosePopupByError(string message)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseError", "CloseMeError('" + message + "');", true);
    }
    protected string GetIncidentAccountId
    {
        get
        {
            return incidentAccountId.ToString();
        }
    }

    protected void btn_send_Click(object sender, EventArgs e)    
    {
        eMessageType messageType;
        if (!Enum.TryParse<eMessageType>(ddl_TypeOfMessage.SelectedValue, out messageType))
            messageType = eMessageType.BOT;
        if(messageType!= eMessageType.BOT)
        {
            surveyExec(messageType);
            return;
        }
        ClipCallReport.LayerSystemChatMessageRequest _request = new ClipCallReport.LayerSystemChatMessageRequest();
        _request.IncidentAccountId = incidentAccountId;
        _request.Message = txt_message.Text;
        ClipCallReport.eSystemChatMessageDestination dest;
        if(!Enum.TryParse(ddl_destination.SelectedValue, out dest))
            dest = ClipCallReport.eSystemChatMessageDestination.All;
        _request.Destination=dest;
        _request.IncludeSms = cb_includeSMS.Checked;
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfLayerSystemChatMessageResponse result = null;
        try
        {
            result = report.SendSystemChatMessage(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            lbl_result.Text = exc.Message;
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            lbl_result.Text = "General Failure";
            return;
        }
        if (result.Value.SentSuccessfully)
        {
            lbl_result.Text = "Success";
            txt_message.Text = string.Empty;
        }
        else
            lbl_result.Text = result.Value.FailedReason;
    }
//    protected void btn_survey_Click(object sender, EventArgs e)
    protected void surveyExec(eMessageType messageType)
    {
        ClipCallReport.CreateSurveyRequest _request = new ClipCallReport.CreateSurveyRequest();
        bool SupplierSelected = false, CustomerSelected = false;
        ClipCallReport.eSystemChatMessageDestination dest;
        if (!Enum.TryParse(ddl_destination.SelectedValue, out dest))
            dest = ClipCallReport.eSystemChatMessageDestination.All;
        switch(dest)
        {
            case(ClipCallReport.eSystemChatMessageDestination.All):
                SupplierSelected = true;
                CustomerSelected = true;
                break;
            case (ClipCallReport.eSystemChatMessageDestination.Customer):
                CustomerSelected = true;
                break;
            case (ClipCallReport.eSystemChatMessageDestination.Pro):
                SupplierSelected = true;
                break;
        }
        /*
        foreach(ListItem li in cb_SurveyDestination.Items)
        {
            if(li.Selected && li.Enabled)
            {
                if (li.Value == "Supplier")
                    SupplierSelected = true;
                else
                    CustomerSelected = true;
            }
        }
         * */
        if(!SupplierSelected && !CustomerSelected)
        {
            lbl_resultSurvey.Text = "You must at least one destination";
            return;
        }
        _request.ToWhom = SupplierSelected && CustomerSelected ? ClipCallReport.eCreateSurveyRequest.BOTH :
            (SupplierSelected ? ClipCallReport.eCreateSurveyRequest.SUPPLIER : ClipCallReport.eCreateSurveyRequest.CUSTOMER);
        _request.IncidentAccountId = incidentAccountId;
        if (messageType == eMessageType.TAD)
        {
            _request.Description = txt_message.Text;
            _request.SurveyType = ClipCallReport.eSurveyType.GeneralDescription;
        }
        else
            _request.SurveyType = ClipCallReport.eSurveyType.Picklist;
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfCreateSurveyResponse result = null;
        try
        {
            result = report.CreateProjectSurvey(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            lbl_resultSurvey.Text = exc.Message;
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            lbl_resultSurvey.Text = "Server Failure";
            return;
        }
        StringBuilder message = new StringBuilder();
        if(result.Value.SupplierResponse!= null)
        {
            message.Append("Supplier: ");
            if (result.Value.SupplierResponse.IsSucceeded)
            {
                message.Append("Succeeded; ");
                hf_supplierHassurvey.Value = true.ToString().ToLower();
            }
            else
                message.Append("Failed: " + result.Value.SupplierResponse.FailureReason + "; ");
        }
        if(result.Value.CustomerResponse!= null)
        {
            message.Append("Customer: ");
            if (result.Value.CustomerResponse.IsSucceeded)
            {
                message.Append("Succeeded;");
                hf_customerHassurvey.Value = true.ToString().ToLower();
            }
            else
                message.Append("Failed: " + result.Value.CustomerResponse.FailureReason + ";");
        }
        lbl_resultSurvey.Text = message.ToString();
        /*
        foreach (ListItem li in cb_SurveyDestination.Items)
        {
            li.Enabled = false;
        }
         * */
    }
    protected void ib_refresh_Click(object sender, ImageClickEventArgs e)
    {
        txt_message.Text = string.Empty;
        ddl_destination.SelectedIndex = 0;
        LoadChat();

    }
    string SetMessagesScript
    {
        get { return Session["SetMessagesScript"] == null ? null : (string)Session["SetMessagesScript"]; }
        set { Session["SetMessagesScript"] = value; }
    }
   
    /*
    List<string> CustList
    {
        get { return Session["CustList"] == null ? null : (List<string>)Session["CustList"]; }
        set { Session["CustList"] = value; }
    }
    List<string> ProList
    {
        get { return Session["ProList"] == null ? null : (List<string>)Session["ProList"]; }
        set { Session["ProList"] = value; }
    }
    List<string> AllList
    {
        get { return Session["AllList"] == null ? null : (List<string>)Session["AllList"]; }
        set { Session["AllList"] = value; }
    }
     * */


    protected void lb_cancelSupplierSurvey_Click(object sender, EventArgs e)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.Result result = null;
        try
        {
            result = report.UncheckSupplierSurveyByIncidentAccountId(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        div_cancelSupplierSurvey.Visible = false;
    }
    protected void lb_cancelCustomerSurvey_Click(object sender, EventArgs e)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.Result result = null;
        try
        {
            result = report.UncheckCustomerSurveyByIncidentAccountId(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        div_cancelCustomerSurvey.Visible = false;
    }
    //~/Publisher/ClipCallControls/ChatControl.ascx
    protected string GetNextMessagesUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallLeadChat.aspx/GetNextMessages"); }
    }
    [WebMethod(MessageName="GetNextMessages")]
    public static NextChatMessagesResponse GetNextMessages(Guid incidentAccountId, string messageId)
    {

        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfLayerChatReportResponse result = null;
        NextChatMessagesResponse _response = new NextChatMessagesResponse();
        try
        {
            result = report.GetChat(incidentAccountId, messageId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            _response.IsSuccess=false;
            _response.FailureResponse = exc.Message;
            return _response;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            _response.IsSuccess=false;
            _response.FailureResponse = "Failure: " + result.Messages[0];
            return _response;
        }
        if (result.Value.status != ClipCallReport.HttpStatusCode.OK && result.Value.status != ClipCallReport.HttpStatusCode.PartialContent)
        {
            _response.IsSuccess=false;
            _response.FailureResponse = "HttpStatusCode: " + result.Value.status;
            return _response;
        }
        
        PageRenderin page = new PageRenderin();
        page.EnableEventValidation = false;
        UserControl ctl = (UserControl)page.LoadControl(BASE_PATH + "ChatControl.ascx");
        MethodInfo LoadData = ctl.GetType().GetMethod("LoadData");
        LoadData.Invoke(ctl, new object[] { result.Value });
        ctl.EnableViewState = false;
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        _response.Table = sb.ToString();
        _response.IsSuccess = true;
        if (result.Value.status == ClipCallReport.HttpStatusCode.PartialContent)
            _response.NextMessageId = result.Value.layerChatResponse[result.Value.layerChatResponse.Length - 1].MessageId;
        return _response;
    }
    public class NextChatMessagesResponse
    {
        public string Table { get; set; }
        public string NextMessageId { get; set; }
        public bool IsSuccess { get; set; }
        public string FailureResponse { get; set; }
    }
}