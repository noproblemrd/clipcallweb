﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AdvertiserRegisterReport.aspx.cs" Inherits="Publisher_AdvertiserRegisterReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    var DaysInterval = new Number();
    var ChoosenIntervalOk = new Boolean();
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);

        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        SetTRClass();
        hideDiv();
    }
    function CompareToPast(elm) {
        var _div = document.getElementById("<%# div_Compare.ClientID %>");
        var _btn = document.getElementById("<%# btn_run.ClientID %>");
        if (elm.checked) {
            Set_Dates_Compars();
            _div.style.display = "block";
        }
        else {
            _btn.setAttribute("onclick", "return CheckCurrentDates();");
            _div.style.display = "none";
        }
    }
    function Set_Dates_Compars() {
        var _btn = document.getElementById("<%# btn_run.ClientID %>");
        var _format = "<%# GetDateFormat() %>";
        var from_current = document.getElementById("<%# GetFromCurrentId %>");
        var to_current = document.getElementById("<%# GetToCurrentId %>");
        var from_past = document.getElementById("<%# GetFromPastId %>");
        var to_past = document.getElementById("<%# GetToPastId %>");
        var _start = GetDateFormat(from_current.value, _format);
        var _end = GetDateFormat(to_current.value, _format);
        /*
        var diff_day = days_between(_start, _end);

        var end_pass = new Date(_start); //.addDays(-1);
        end_pass.addDays(-1);
        var start_pass = new Date(end_pass);
        start_pass.addDays(diff_day * -1);
        */
        from_past.value = GetDateString(new Date(_start), _format);
        to_past.value = GetDateString(new Date(_end), _format);
        _btn.setAttribute("onclick", "return CheckCurrentAndCompareDates();");
    }
    function Set_Dates_Compars_by(_interval) {
        var _btn = document.getElementById("<%# btn_run.ClientID %>");
        var _format = "<%# GetDateFormat() %>";
        var from_current = document.getElementById("<%# GetFromCurrentId %>");
        var to_current = document.getElementById("<%# GetToCurrentId %>");
        var from_past = document.getElementById("<%# GetFromPastId %>");
        var to_past = document.getElementById("<%# GetToPastId %>");
        var _start = GetDateFormat(from_current.value, _format);
        var _end = GetDateFormat(to_current.value, _format);
        /*
        var diff_day = days_between(_start, _end);
        var end_pass = new Date(_start); //.addDays(-1);
        end_pass.addDays(-1);
        var start_pass = new Date(end_pass);
        //    alert(_interval);
        if (_interval == 29)
        start_pass.addMonth(-1);
        else if (_interval == 89)
        start_pass.addMonth(-3);
        else if (_interval == 364)
        start_pass.addFullYear(-1);
        else
        start_pass.addDays(diff_day * -1);
        */
        from_past.value = GetDateString(new Date(_start), _format);
        to_past.value = GetDateString(new Date(_end), _format);
        _btn.setAttribute("onclick", "return CheckCurrentAndCompareDates();");
    }
    function LoadChart(fileXML, _chart) {
        //      alert(fileXML+"----"+_chart);
        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render("div_chart");
    }
    function CheckCurrentDates() {
        var elem = document.getElementById("<%# GetDivCurrentId %>");
        var result = Check_DateValidation(elem);
        if (!result)
            alert("<%# GetInvalidDate %>");
        return result;
    }
    //CheckCurrentAndPastDates
    function CheckCurrentAndCompareDates() {
        if (!CheckCurrentDates())
            return false;
        var elem = document.getElementById("<%# GetDivPastId %>");
        var result = Check_DateValidation(elem);
        if (!result)
            alert("<%# GetInvalidDate %>");
        return result;
    }
    function SetCompareDate(elem_from, parentIdCurrent, mess, elem_maxId) {
        var _days = GetRangeDays(parentIdCurrent);
        var elm_parent = getParentByClassName(elem_from, "div_DatePicker");
        var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;
        var elem_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
        var elem_max = document.getElementById(elem_maxId);

        var _MaxValue = GetDateFormat(elem_max.value, _format);
        var _start = GetDateFormat(elem_from.value, _format);
        var _end = _start + ((24 * 60 * 60 * 1000) * _days); //GetDateFormat(elem_to.value, _format);
        //   _MaxValue = _MaxValue - (24 * 60 * 60 * 1000);
        //    alert("max= " +elem_max.id+" end= "+elem_from.id);
        //    alert("max= " +_MaxValue+" end= "+_end);
        //    if (_MaxValue < _end) {
        if (_start > _MaxValue) {
            _end = _MaxValue;
            var start_date = new Date(_MaxValue);
            var end_date = new Date(_MaxValue);
            end_date.addDays(_days);
            //   start_date.addDays(-1 * _days);
            //       alert(start_date);
            elem_from.value = GetDateString(start_date, _format);
            elem_to.value = GetDateString(end_date, _format);
            alert("<%# Lbl_DatesChange %>");
        }
        else {
            elem_to.value = GetDateString(new Date(_end), _format);
        }

    }
    function GetIntervalDates() {

        return 0;
    }
    function SetIntervalDatesValidation(elem) {

        ChoosenIntervalOk = true;
        SetDdlInterval(elem);
        if (!ChoosenIntervalOk)
            alert(document.getElementById('<%# lbl_ChosenInterval.ClientID %>').value);
    }
    function SetDdlInterval(elem) {
        DaysInterval = GetIntervalDates(); //elem.options[elem.selectedIndex].value;
        var _index = elem.selectedIndex;
        var Interval_name = elem.options[_index].value;
        var _days = GetRangeDays("<%# GetDivCurrentId %>");


        if (DaysInterval > _days) {
            _index--;
            ChoosenIntervalOk = false;
            elem.selectedIndex = _index;
            SetDdlInterval(elem);
        }
    }
    function OnBlurByDays(elm) {

        var elm_to;
        var elm_from;
        var elm_parent = getParentByClassName(elm, "div_DatePicker");
        var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;

        if (!validate_Date(elm.value, _format)) {
            try { alert(WrongFormatDate); } catch (ex) { }
            elm.focus();
            elm.select();
            return -1;
        }
        if (elm.className.indexOf("_to") == -1) {
            elm_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
            elm_from = elm;
        }
        else {
            elm_from = getElementsWithMultiClassesByClass("_from", elm_parent, "input")[0];
            elm_to = elm;
        }
        DaysInterval = GetIntervalDates();
        var _end = GetDateFormat(elm_to.value, _format);
        var _start = GetDateFormat(elm_from.value, _format);
        if ((_start + (24 * 60 * 60 * 1000 * DaysInterval)) > _end) {
            if (elm_from.id == elm.id) {
                _end = new Date(_start);
                if (DaysInterval == 29)
                    _end.addMonth(1);
                else if (DaysInterval == 89)
                    _end.addMonth(3);
                else if (DaysInterval == 364)
                    _end.addFullYear(1);
                else {
                    _end = _start + (24 * 60 * 60 * 1000 * DaysInterval);
                    _end = new Date(_end);
                }
                elm_to.value = GetDateString(_end, _format);
            }
            else {

                _start = new Date(_end);
                if (DaysInterval == 29)
                    _start.addMonth(-1);
                else if (DaysInterval == 89)
                    _start.addMonth(-3);
                else if (DaysInterval == 364)
                    _start.addFullYear(-1);
                else {
                    _start = _end - (24 * 60 * 60 * 1000 * DaysInterval);
                    _start = new Date(_start);
                }
                elm_from.value = GetDateString(_start, _format);
            }

            return -10;
        }
        return 1;

    }
    function SetTRClass() {
        var _index = 0;
        $('.data-tabledash').find('tr').not('._TH').each(function () {
            $(this).attr('class', '');
            if (_index == 0)
                $(this).addClass('_sum');
            if (_index % 2 == 0)
                $(this).addClass('even');
            else
                $(this).addClass('odd');
            _index++;
        });
    }
    $(function () {
        $('#<%# cb_CompareTo.ClientID %>').change(function () {
            if (this.checked)
                $('<%# div_Compare.ClientID %>').show();
            else
                $('<%# div_Compare.ClientID %>').hide();
        });
        SetTRClass();
    });
    function SetTRClass() {
        var _index = 0;
        $('.data-tabledash').find('tr').not('._TH').each(function () {
            $(this).attr('class', '');
            /*
            if (_index == 0)
                $(this).addClass('_sum');
                */
            if (_index % 2 == 0)
                $(this).addClass('even');
            else
                $(this).addClass('odd');
            _index++;
        });
    }
    $(function () {

        var CameFromSlider = '<%# GetCameFromShowSliderOptions %>';
        var CameFromEmail = '<%# GetCameFromShowEmailOptions %>';

        $('#<%# ddl_CameFrom.ClientID %>').change(function () {
            
            $('#<%# div_SliderType.ClientID %>').hide();
            $('#<%# div_EmailType.ClientID %>').hide();
            
            if (this.value == CameFromSlider )
                $('#<%# div_SliderType.ClientID %>').show();
            else if(this.value == CameFromEmail)
                $('#<%# div_EmailType.ClientID %>').show();
           

        });
        $('#<%# ddl_CameFrom_Compare.ClientID %>').change(function () {
            $('#<%# div_SliderType_Compare.ClientID %>').hide();
            $('#<%# div_EmailType_Compare.ClientID %>').hide();

            if (this.value == CameFromSlider)
                $('#<%# div_SliderType_Compare.ClientID %>').show();
            else if (this.value == CameFromEmail)
                $('#<%# div_EmailType_Compare.ClientID %>').show();

        });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
 <uc1:ToolboxReport runat="server" ID="ToolboxReport1" /> 
<div class="page-content minisite-content2">
    <asp:Label ID="lbl_AdvertiserRegisterReport" runat="server" Text="Advertiser Register Report" Visible="false"></asp:Label>
     <div id="form-analytics" > 
        <div class="form-field-singleLine">
    
            <div class="dash-datePicker"><uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" /></div>
            
            <div class="form-field">
                <asp:Label ID="lbl_CameFrom" runat="server" Cssclass="label" Text="Came from"></asp:Label>        
                <asp:DropDownList ID="ddl_CameFrom" CssClass="form-select" runat="server"></asp:DropDownList>
            </div>           

            <div class="form-field" runat="server" id="div_SliderType" style="display: none;">
                <asp:Label ID="lbl_SliderType" runat="server" Cssclass="label" Text="Recruitment slider"></asp:Label>        
                <asp:DropDownList ID="ddl_SliderType" CssClass="form-select" runat="server"></asp:DropDownList>
            </div>
            
            <div class="form-field" runat="server" id="div_EmailType" style="display: none;">
                <asp:Label ID="lbl_EmailType" runat="server" Cssclass="label" Text="Email"></asp:Label>        
                <asp:DropDownList ID="ddl_EmailType" CssClass="form-select" runat="server"></asp:DropDownList>
            </div>

        </div>
        <div class="clear"></div>
        <div class="form-field-singleLine">
            <div class="form-field">
                <asp:Label ID="lbl_Plan" runat="server" Cssclass="label" Text="Plan"></asp:Label>        
                <asp:DropDownList ID="ddl_Plan" CssClass="form-select" runat="server"></asp:DropDownList>
            </div>
                <div class="form-field">
                <asp:Label ID="lbl_AccountManager" runat="server" Cssclass="label" Text="Account manager"></asp:Label>        
                <asp:DropDownList ID="ddl_AccountManager" CssClass="form-select" runat="server"></asp:DropDownList>
            </div>

            <div class="form-field">
                <asp:Label ID="lbl_LandingPage" runat="server" Cssclass="label" Text="Landing Page"></asp:Label>        
                <asp:DropDownList ID="ddl_LandingPage" CssClass="form-select" runat="server"></asp:DropDownList>
            </div>

            <div class="form-field-checkbox">
         <!--       <asp:Label ID="lbl_ShowPage1" runat="server" Cssclass="label" Text="Show step 1 (page hits)"></asp:Label>-->
                <asp:CheckBox ID="cb_ShowPage1" runat="server" Text="Show step 1 (page hits)" Checked="true" />
            </div>
            <!--
        <asp:CheckBoxList ID="cbl_Compare" CssClass="checkboxdash" runat="server" 
            RepeatDirection="Horizontal" RepeatLayout="Table"  >
            <asp:ListItem Value="ShowObjectives"   CellSpacing="90"  Text="Show objectives"></asp:ListItem>
           
            <asp:ListItem Value="CompareToPast" Text="Compare to past" onclick="CompareToPast(this);"></asp:ListItem>
        </asp:CheckBoxList>
        -->
        </div>
        <div class="clear"></div>
        <div class="form-field-singleLine">
            <div class="form-field-checkbox Field-OnlyCheckbox">
                <asp:CheckBox ID="cb_CompareTo" runat="server" onclick="CompareToPast(this);"  Text="Compare to" />
            </div>
        </div>
        <div class="clear"></div>
        
        <div id="div_Compare" runat="server" style="display:none;">
            <div class="form-field-singleLine">                 
                <div class="dash-datePicker"><uc1:FromToDatePicker ID="FromToDatePicker_Compare" runat="server" /></div>            
                <div class="form-field">
                    <asp:Label ID="Label2" runat="server" Cssclass="label" Text="<%# lbl_CameFrom.Text %>"></asp:Label>        
                    <asp:DropDownList ID="ddl_CameFrom_Compare" CssClass="form-select" runat="server"></asp:DropDownList>
                </div>
                <div class="form-field" runat="server" id="div_SliderType_Compare"  style="display: none;">
                    <asp:Label ID="lbl_SliderType_Compare" runat="server" Cssclass="label" Text="<%# lbl_SliderType.Text %>"></asp:Label>        
                    <asp:DropDownList ID="ddl_SliderType_Compare" CssClass="form-select" runat="server"></asp:DropDownList>
                </div>
            
                <div class="form-field" runat="server" id="div_EmailType_Compare"  style="display: none;">
                    <asp:Label ID="lbl_EmailType_Compare" runat="server" Cssclass="label" Text="<%# lbl_EmailType.Text %>"></asp:Label>        
                    <asp:DropDownList ID="ddl_EmailType_Compare" CssClass="form-select" runat="server"></asp:DropDownList>
                </div>

            </div>
            <div class="form-field-singleLine">
                <div class="form-field">
                    <asp:Label ID="Label13" runat="server" Cssclass="label" Text="<%# lbl_Plan.Text %>"></asp:Label>        
                    <asp:DropDownList ID="ddl_Plan_Compare" CssClass="form-select" runat="server"></asp:DropDownList>
                </div>
                    <div class="form-field">
                    <asp:Label ID="Label14" runat="server" Cssclass="label" Text="<%# lbl_AccountManager.Text %>" ></asp:Label>        
                    <asp:DropDownList ID="ddl_AccountManager_Compare" CssClass="form-select" runat="server"></asp:DropDownList>
                </div>

                <div class="form-field">
                    <asp:Label ID="Label1" runat="server" Cssclass="label" Text="<%# lbl_LandingPage.Text %>"></asp:Label>        
                    <asp:DropDownList ID="ddl_LandingPage_Compare" CssClass="form-select" runat="server"></asp:DropDownList>
                </div>

                <div class="form-field-checkbox">
                    
                    <asp:CheckBox ID="cb_ShowPage1_Compare" runat="server" Text="<%# cb_ShowPage1.Text %>" Checked="true"/>
                </div>            
            </div>            
        </div>
      
       <div class="clear"><!-- --></div>   
            
        <div class="dashsubmit">
            <asp:Button ID="btn_run" runat="server" Text="Create report"  CssClass="CreateReportSubmit2"
                onclick="btn_run_Click" OnClientClick="return CheckCurrentDates();" />
    
        </div>
     
        <div class="clear"><!-- --></div>
       
        <asp:UpdatePanel ID="UpdatePanel_chart" runat="server" UpdateMode="Conditional">
        <ContentTemplate>    
            <div runat="server" id="div_chart_container" visible="false" class="div_chart_container">            
                <div id="div_chart" style="float:left;"></div>
                <div id="ChartUsersDetails" class="ChartUsersDetails" style="float:left; padding-left: 15px;">
                    <div class="form-field" style="float:none; margin-top: 18px;">
                        <asp:Label ID="lbl_Chart" runat="server"  CssClass="label" Text="Chart display"></asp:Label>
                        <asp:RadioButtonList ID="rbl_Chart" runat="server" RepeatDirection="Vertical">                    
                        </asp:RadioButtonList>
                    </div>                        
                    <div class="div_btn_UpdateChart">
                        <asp:Button ID="btn_UpdateChart" runat="server" CssClass="CreateReportSubmit2" 
                            Text="Update Chart" onclick="btn_UpdateChart_Click"/>
                    </div>
                </div>
                </div>
        </ContentTemplate>
        </asp:UpdatePanel>
        
    <div class="clear"></div>
        <div class="data-tabledash">
         <asp:UpdatePanel ID="UpdatePanel_table" runat="server" UpdateMode="Conditional">
    <ContentTemplate> 
        <div runat="server" id="div_ProfitTable">
            
               
           <asp:Repeater ID="_Repeater" runat="server" >
           <HeaderTemplate>
                 <table class="data-tabledash" style="margin:0;">
                    <tr class="_TH" runat="server" id="tr_head_title" visible="<%# ShowCompare %>">
                        <th colspan="6">
                            <asp:Label ID="Label112" runat="server" Text="<%# lbl_Value.Text %>"></asp:Label>
                        </th>
                        <th colspan="6" id="tr_head_title_compare" runat="server">
                            <asp:Label ID="Label113" runat="server" Text="<%# lbl_CompareValue.Text %>"></asp:Label>
                        </th>
                        <th id="Th_compare_gap" runat="server">
                            <asp:Label ID="Label220" runat="server" Text="<%# lbl_Gap.Text %>"></asp:Label>
                        </th>
                    </tr>
                    <tr class="_TH" runat="server" id="tr_head">
                        <th>
                            <asp:Label ID="Label101" runat="server" Text="<%# lbl_Step.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label20" runat="server" Text="<%# lbl_Users.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label290" runat="server" Text="<%# lbl_DropOff.Text %>"></asp:Label>
                        </th>
                        
                        <th>
                            <asp:Label ID="Label29" runat="server" Text="<%# lbl_Passed.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label30" runat="server" Text="<%# lbl_DropOffFromStep.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label25" runat="server" Text="<%# lbl_PassedFromStep.Text %>"></asp:Label>
                        </th>
                               
                        <th runat="server" id="th1" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label100" runat="server" Text="<%# lbl_Step.Text %>"></asp:Label>
                        </th>
                        <th runat="server" id="th8" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label22" runat="server" Text="<%# lbl_Users.Text %>"></asp:Label>
                        </th> 
                        <th runat="server" id="th10" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label298" runat="server" Text="<%# lbl_DropOff.Text %>"></asp:Label>
                        </th> 
                                                         
                        <th runat="server" id="th2" visible="<%# ShowCompare %>">
                           <asp:Label ID="Label103" runat="server" Text="<%# lbl_Passed.Text %>"></asp:Label> 
                        </th>
                        <th runat="server" id="th11" visible="<%# ShowCompare %>">
                           <asp:Label ID="Label31" runat="server" Text="<%# lbl_DropOffFromStep.Text %>"></asp:Label> 
                        </th>
                        <th runat="server" id="th9" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label26" runat="server" Text="<%# lbl_PassedFromStep.Text %>"></asp:Label>
                        </th> 
                         
                        <th runat="server" id="th7" visible="<%# ShowCompare %>">
                           
                        </th>
                    </tr>
               
           </HeaderTemplate>
           <ItemTemplate>                                        
                <tr>
                    <td class="td-left">
                        <asp:Label ID="Label6" runat="server" Text="<%# Bind('Step') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label23" runat="server" Text="<%# Bind('Users') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label295" runat="server" Text="<%# Bind('DropOff') %>"></asp:Label>                       
                    </td>
                   
                     <td>
                        <asp:Label ID="Label33" runat="server" Text="<%# Bind('Passed') %>"></asp:Label>                       
                    </td>
                     <td>
                        <asp:Label ID="Label34" runat="server" Text="<%# Bind('DropOffFromStep') %>"></asp:Label>                       
                    </td>
                     <td>
                        <asp:Label ID="Label27" runat="server" Text="<%# Bind('PassedFromStep') %>"></asp:Label>                       
                    </td>
                   
                    <td runat="server" id="td1" visible="<%# Bind('ShowCompare') %>" class="td-left">
                        <asp:Label ID="Label50" runat="server" Text="<%# Bind('Step_Compare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td8" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label24" runat="server" Text="<%# Bind('Users_Compare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td10" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label296" runat="server" Text="<%# Bind('DropOff_Compare') %>"></asp:Label>       
                    </td> 
                    
                    <td runat="server" id="td11" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label35" runat="server" Text="<%# Bind('Passed_Compare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td12" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label36" runat="server" Text="<%# Bind('DropOffFromStep_Compare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td9" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label28" runat="server" Text="<%# Bind('PassedFromStep_Compare') %>"></asp:Label>       
                    </td>                     
                    <td runat="server" id="td7" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label21" runat="server" Text="<%# Bind('Gap') %>" CssClass="<%# Bind('GapPosNeg') %>"></asp:Label>       
                    </td>    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
            </asp:Repeater>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    
   
    </div>
</div>
</div>

<asp:Label ID="lbl_Value" runat="server" Text="Value" Visible="false"></asp:Label>
<asp:Label ID="lbl_CompareValue" runat="server" Text="Compared Value" Visible="false"></asp:Label>
<asp:Label ID="lbl_Gap" runat="server" Text="Gap" Visible="false"></asp:Label>

<asp:Label ID="lbl_Step" runat="server" Text="Step" Visible="false"></asp:Label>
<asp:Label ID="lbl_Users" runat="server" Text="Users" Visible="false"></asp:Label>
<asp:Label ID="lbl_DropOff" runat="server" Text="Drop off" Visible="false"></asp:Label>
<asp:Label ID="lbl_Passed" runat="server" Text="Passed" Visible="false"></asp:Label>
<asp:Label ID="lbl_DropOffFromStep" runat="server" Text="Drop off from step" Visible="false"></asp:Label>
<asp:Label ID="lbl_PassedFromStep" runat="server" Text="Passed from step" Visible="false"></asp:Label>

<asp:Label ID="lbl_DatesChange" Text="The date you selected have been changed according to interval requirement" runat="server" Visible="false"></asp:Label>
 <asp:Label ID="lbl_DateError" runat="server" Text="Invalid date" Visible="false"></asp:Label>
 <asp:HiddenField ID="lbl_ChosenInterval" runat="server"
            Value="The interval you have chosen is not adequate to the date range you selected. Please choose smaller value" />

<asp:Label ID="lbl_CreationDateFrom" runat="server" Text="Creation Date from" Visible="false"></asp:Label>
</asp:Content>

