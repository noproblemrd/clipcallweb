﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SupplierReviewReport.aspx.cs" Inherits="Publisher_SupplierReviewReport" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
     <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
   
     <script type="text/javascript">
         window.onload = appl_init;

         function appl_init() {
             var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
             pgRegMgr.add_beginRequest(BeginHandler);
             pgRegMgr.add_endRequest(EndHandler);

         }
         function BeginHandler() {
             //     alert('in1');
             showDiv();
         }
         function EndHandler() {
             //      alert('out1');
             hideDiv();
         }
         function OpenIframe(_path, key) {
             var _leadWin = window.open(_path, "LeadDetails" + key, "width=1000, height=650");
             _leadWin.focus();
             return false;
         }
         

         function _CreateExcel() {
             var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"<%# EXCEL_NAME %>"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcelUrl %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });

         }
         function ApproveReview(_btn, accountRatingId) {
             //div_review_loader
             var AreYouShure = confirm("You are about to approve the review");
             if (!AreYouShure)
                 return false;
             var $div_parent = $(_btn).parents('div.div_ApproveReview');
             var $div_loader = $div_parent.find('div.div_ApproveReview_loader');
             $div_loader.show();
             var spanResult = $div_parent.find('span.span_ApproveReview_response')
            
             $div_parent.find('div.div_ApproveReview_btn').hide();
             $.ajax({
                 url: "<%# ApproveReviewWebMethod %>",
                 data: "{ 'accountRatingId': '" + accountRatingId + "'}",
                 dataType: "json",
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 dataFilter: function (data) { return data; },
                 success: function (data) {
                     // var _data = eval("(" + data.d + ")");
                     var _response = data.d == "true" ? "success" : "failed";
                     spanResult.html(_response);
                 },
                 error: function (XMLHttpRequest, textStatus, errorThrown) {
                     alert(textStatus);
                 },
                 complete: function (jqXHR, textStatus) {
                     //  HasInHeadingRequest = false;
                     $div_loader.hide();
                 }
             });
             return false;
         }        
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
          
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	       
               <div class="main-inputs" runat="server" id="PublisherInput" style="width:680px;">			
	                
                        <div id="div_experties" class="form-field" runat="server">
					            <label for="form-subsegment" class="label" runat="server" id="lblExperties" >Review Status</label>
                            <asp:DropDownList ID="ddl_ReviewStatus" runat="server" CssClass="form-select">
                            </asp:DropDownList>					            
			            </div>
                     <div class="form-field" runat="server" id="div_IncludeQa">
                        <asp:Label ID="lbl_IncludeQaTest" runat="server" Text="Include QA" CssClass="label"></asp:Label>
                       <asp:CheckBox ID="cb_IncludeQaTest" runat="server" />

                    </div>
                         <div id="div1" class="form-field" runat="server" style="width: 200px;">
                             <asp:UpdatePanel ID="UpdatePanel_nps" runat="server" UpdateMode="Conditional">
	                            <ContentTemplate>
					                            <label for="form-subsegment" class="label" runat="server" id="Label1" >NPS number per the dates below</label>
                                             <asp:Label ID="lbl_NPS" runat="server" Text="" ForeColor="Red" Font-Size="16"></asp:Label>
                                    </ContentTemplate>
                             </asp:UpdatePanel>   
			            </div>                         
            			 <div class="clear"></div>
                   <div id="div2" class="form-field" runat="server">
					            <label for="form-subsegment" class="label" runat="server" id="Label2" >Customer App</label>
                            <asp:DropDownList ID="ddl_customerApp" runat="server" CssClass="form-select">
                            </asp:DropDownList>					            
			            </div>
                     <div class="form-field" runat="server" id="div3">
                        <asp:Label ID="Label3" runat="server" Text="CustomerId" CssClass="label"></asp:Label>
                       <asp:TextBox ID="txt_CustomerId" runat="server" CssClass="form-text"></asp:TextBox>

                    </div>
                   </div>
             <div class="clear"></div>
                    <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div> 
                   
            </div>
         <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	            <ContentTemplate>	
                    <div class="results">
                        <asp:Label ID="lbl_RecordMached" runat="server"></asp:Label>
                        <asp:Label ID="lbl_avg" runat="server" style="display:block;"></asp:Label>
                    </div>
	                <div id="Table_Report"  runat="server" >
                        <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				            <RowStyle CssClass="even" />
				            <AlternatingRowStyle CssClass="odd" />
				            <Columns>
					            <asp:TemplateField SortExpression="CreatedOn">
						            <HeaderTemplate>
							            <asp:Label ID="Label101" runat="server" Text="Created on"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
                                        <asp:Label ID="Label102" runat="server" Text='<%# Bind("CreatedOn") %>'></asp:Label>
						            </ItemTemplate>
					            </asp:TemplateField>

                                <asp:TemplateField SortExpression="CustomerName">
						            <HeaderTemplate>
							            <asp:Label ID="Label203" runat="server" Text="Customer Name"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
                                         <asp:HyperLink ID="HyperLink12" runat="server" Text='<%# Bind("CustomerName") %>' Target="_blank" NavigateUrl='<%# Bind("CustomerReviewUrl") %>'></asp:HyperLink>							            
						            </ItemTemplate>
					            </asp:TemplateField>

                                <asp:TemplateField SortExpression="CustomerPhone">
						            <HeaderTemplate>
							            <asp:Label ID="Label205" runat="server" Text="Customer Phone"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink13" runat="server" Text='<%# Bind("CustomerPhone") %>' Target="_blank" NavigateUrl='<%# Bind("CustomerReviewUrl") %>'></asp:HyperLink>							            
						            </ItemTemplate>
					            </asp:TemplateField>

                                <asp:TemplateField SortExpression="ProjectId">
						            <HeaderTemplate>
							            <asp:Label ID="Label103" runat="server" Text="ProjectId"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label104" runat="server" Text='<%# Bind("ProjectId") %>'></asp:Label>
						            </ItemTemplate>
					            </asp:TemplateField>

                                 <asp:TemplateField SortExpression="SupplierName">
						            <HeaderTemplate>
							            <asp:Label ID="Label105" runat="server" Text="Supplier Name"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink15" runat="server" Text='<%# Bind("SupplierName") %>' Target="_blank" NavigateUrl='<%# Bind("SupplierReviewUrl") %>'></asp:HyperLink>							            
						            </ItemTemplate>
					            </asp:TemplateField>

					            <asp:TemplateField SortExpression="SupplierPhone">
						            <HeaderTemplate>
							            <asp:Label ID="Label107" runat="server" Text="Supplier Phone"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label108" runat="server" Text='<%# Bind("SupplierPhone") %>'></asp:Label>
						            </ItemTemplate>
					            </asp:TemplateField>										           

					            <asp:TemplateField SortExpression="FairPrice">
						            <HeaderTemplate>
							            <asp:Label ID="Label109" runat="server" Text="Fair Price"></asp:Label>
						            </HeaderTemplate>
						            <ItemTemplate>
                                        <asp:Label ID="Label110" runat="server" Text='<%# Bind("FairPrice") %>'></asp:Label>
							            
						            </ItemTemplate>
					            </asp:TemplateField>

					             <asp:TemplateField SortExpression="WorkQuality">
						            <HeaderTemplate>
							            <asp:Label ID="Label111" runat="server" Text="Work Quality"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label112" runat="server" Text='<%# Bind("WorkQuality") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>					

                                 <asp:TemplateField SortExpression="CustomerService">
						            <HeaderTemplate>
							            <asp:Label ID="Label113" runat="server" Text="Customer Service"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label114" runat="server" Text='<%# Bind("CustomerService") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>
                                
                                <asp:TemplateField SortExpression="FlexibleSchedules">
						            <HeaderTemplate>
							            <asp:Label ID="Label115" runat="server" Text="Flexible Schedules"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label116" runat="server" Text='<%# Bind("FlexibleSchedules") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>	

                                <asp:TemplateField SortExpression="Rating">
						            <HeaderTemplate>
							            <asp:Label ID="Label117" runat="server" Text="Rating"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label118" runat="server" Text='<%# Bind("Rating") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>
                                
                                <asp:TemplateField SortExpression="ClipCallRate">
						            <HeaderTemplate>
							            <asp:Label ID="Label147" runat="server" Text="ClipCall Rate"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label148" runat="server" Text='<%# Bind("ClipCallRate") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>	

                                 <asp:TemplateField SortExpression="Description">
						            <HeaderTemplate>
							            <asp:Label ID="Label119" runat="server" Text="Description"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label120" runat="server" Text='<%# Bind("Description") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>	

                                 <asp:TemplateField SortExpression="Status">
						            <HeaderTemplate>
							            <asp:Label ID="Label121" runat="server" Text="Status"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label122" runat="server" Text='<%# Bind("Status") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>	

                                 <asp:TemplateField SortExpression="Approve">
						            <HeaderTemplate>
							            <asp:Label ID="lbl_ApproveReview" runat="server" Text="Approve Review"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
                                         <div runat="server" id="div_ApproveReview" class="div_ApproveReview" visible='<%# Bind("CanApprove") %>'>
                                            <div class="div_ApproveReview_btn">
                                                <asp:Button ID="btn_ApproveReview" runat="server" Text="Approve" OnClientClick='<%# Bind("Approve") %>' />
                                                                                                        
                                            </div>
                                            <div style="text-align:center; display:none;" class="div_ApproveReview_loader">
                                                <asp:Image ID="img_ApproveReview" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                            </div>
                                            <span class="span_ApproveReview_response" style="color:red;"></span>
                                        </div>
                                        
							             
						            </ItemTemplate>
					            </asp:TemplateField>	
                                                                
				            </Columns>    
				            </asp:GridView>
                        </div>
                    </ContentTemplate>
             </asp:UpdatePanel>        
        </div>
</asp:Content>

