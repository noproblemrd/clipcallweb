﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ExternalSupplierReport.aspx.cs" Inherits="Publisher_ExternalSupplierReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<script type="text/javascript" >
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);

    }
    function BeginHandler() {

        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
 <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
</cc1:ToolkitScriptManager> 
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
       
        <div class="clear"></div>
	    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	    <ContentTemplate>	
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
        <div id="Table_Report" class="table" runat="server" >
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                CssClass="data-table">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <Columns>                     
                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label01" runat="server" Text="<%# lbl_Name.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# Bind('Name') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label02" runat="server" Text="<%# lbl_Number.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('Number') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label03" runat="server" Text="<%# lbl_VoucherBonusDepositSum.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text="<%# Bind('VoucherBonusDepositSum') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label04" runat="server" Text="<%# lbl_VoucherDepositSum.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text="<%# Bind('VoucherDepositSum') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label05" runat="server" Text="<%# lbl_VoucherExtraBonusDepositSum.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text="<%# Bind('VoucherExtraBonusDepositSum') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label06" runat="server" Text="<%# lbl_Balance.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text="<%# Bind('Balance') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                </asp:GridView>
                <uc1:TablePaging ID="TablePaging1" runat="server" />
            </div>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:Label ID="lbl_ReportName" runat="server" Text="External Supplier" Visible="false"></asp:Label>

    <asp:Label ID="lbl_Name" runat="server" Text="Name" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Number" runat="server" Text="Number" Visible="false"></asp:Label>
    <asp:Label ID="lbl_VoucherBonusDepositSum" runat="server" Text="Voucher Bonus Deposit" Visible="false"></asp:Label>
    <asp:Label ID="lbl_VoucherDepositSum" runat="server" Text="Voucher Deposit" Visible="false"></asp:Label>
    <asp:Label ID="lbl_VoucherExtraBonusDepositSum" runat="server" Text="Voucher Extra Bonus Deposit" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Balance" runat="server" Text="Balance" Visible="false"></asp:Label>
</asp:Content>

