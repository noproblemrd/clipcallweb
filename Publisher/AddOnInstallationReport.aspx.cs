﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;
using CsvLib;

public partial class Publisher_AddOnInstallationReport : PageSetting
{
    const string TOTAL = "TOTAL";
    const int DAYS_INTERVAL = 2;
    const string FILE_NAME = "InstallationReport_AULT.xml";
    const string CSV_NAME = "InstallationIp.csv";
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(btn_Run);
        if (!IsPostBack)
        {
            SetToolbox();
            SetDatePicker();
            LoadCountries();
            LoadOrigins();
            LoadRblChart();
            lbl_DAUS_Percent.Text = lbl_DAUS.Text + "%";
            RunReport();
        }
        SetToolboxEvents();
        Header.DataBind();
  //      ScriptManager.RegisterStartupScript(this, this.GetType(), "SetTrTable", "SetTrTable();", true);
    }

   
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (!string.IsNullOrEmpty(PathV))
        {
            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSLine.swf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + _chart + "');", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CleanChart",
                "CleanChart();", true);

    }
    private void LoadRblChart()
    {
        //       _GridView.DataBind();
        cbl_chart.Items.Clear();
        foreach (DataControlField dcf in _GridView.Columns)
        {
            if (dcf.SortExpression == "Date")
                continue;
            string name = dcf.SortExpression.Replace('_', ' ');
            ListItem li = new ListItem(name, dcf.SortExpression);
            li.Selected = dcf.SortExpression == "AMUV_Total";
            cbl_chart.Items.Add(li);
        }
    }
    void SetDatePicker()
    {
        FromToDatePicker_current.SetIntervalDateServer(DAYS_INTERVAL);
    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lbl_TitleReport.Text);
    }

    private void SetToolboxEvents()
    {
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, lbl_TitleReport.Text);

        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
         
    }
    private void LoadCountries()
    {
        string command = "SELECT IP2LocationName FROM dbo.Country ORDER BY Id";
        ddl_Country.Items.Clear();
        ListItem liFirst = new ListItem("ALL", string.Empty) { Selected = true };
        ddl_Country.Items.Add(liFirst);
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string country = (string)reader["IP2LocationName"];
                    ListItem li = new ListItem(country, country) { Selected = false };
                    ddl_Country.Items.Add(li);
                }
                conn.Close();
            }
        }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
 
    }
    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }
        StringWriter stringWrite = new StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
    //    PageRenderin pr = new PageRenderin();
        div_table.RenderControl(htmlWrite);
    //    pr.Controls.Add(div_table);
    //    pr.RenderControl(htmlWrite);

        Session["string_print"] = stringWrite.ToString();
        Session["PrintStyle"] = ResolveUrl("~/Publisher/style.css");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    protected void btn_Run_Click(object sender, EventArgs e)
    {
        RunReport();
    }
    void RunReport()
    {
       
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.AddOnInstallationReportRequest _request = new WebReferenceReports.AddOnInstallationReportRequest();
        _request.FromDate = FromToDatePicker_current.GetDateFrom;
        _request.ToDate = FromToDatePicker_current.GetDateTo;
        List<Guid> origins = new List<Guid>();
        foreach (ListItem li in lb_Origin.Items)
        {
            if(!li.Selected)
                continue;
            Guid _id;
            if (!Guid.TryParse(li.Value, out _id))
                continue;
            if (_id == Guid.Empty)
            {
                origins = null;
                break;
            }
            origins.Add(_id);
        }
        if (origins != null && origins.Count > 0)
            _request.Origins = origins.ToArray();
        _request.Country = ddl_Country.SelectedValue;
        RequestSession = _request;
        WebReferenceReports.ResultOfDistributionInstallationReportResponse result;
        try
        {
            result = _report.DistributionInstallationReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            RemoveReportFromPage();
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            RemoveReportFromPage();
            Update_Faild();
            return;
        }
        if (result.Value.Data == null)
        {
            RemoveReportFromPage();
            return;
        }
        div_table.Visible = true;
                
        
        DataTable data = new DataTable();
        data.Columns.Add("Date");
        data.Columns.Add("Install");
        data.Columns.Add("Uniqe_Installs");
        data.Columns.Add("Effective_Installs");
        data.Columns.Add("Total_Installs");
        data.Columns.Add("Uninstalls");
        data.Columns.Add("DAUS");
        data.Columns.Add("DAUs_Percent");
        data.Columns.Add("NP_Revenue");
        data.Columns.Add("Non_NP_Revenue");
        data.Columns.Add("Dealply_Revenue");
        data.Columns.Add("Jollywallet_Revenue");
        data.Columns.Add("Inter_Yield_Revenue");
        data.Columns.Add("Superfish_Revenue");
        data.Columns.Add("Revizer_Popup_Revenue");
        data.Columns.Add("Intext_Revenue");
        data.Columns.Add("First_Offerz_Revenue");
        data.Columns.Add("Admedia_Intext_Revenue");
        data.Columns.Add("Adcash_Revenue");
        data.Columns.Add("Engageya_Revenue");

        data.Columns.Add("Total_Revenue");       

     //   data.Columns.Add("TotalRevenue");
        data.Columns.Add("Cost");
        data.Columns.Add("AULT_Total");
        data.Columns.Add("AMUV_NoProblem");
        data.Columns.Add("DUV_NoProblem");
        data.Columns.Add("AMUV_Total");
        data.Columns.Add("DUV_Total");
        data.Columns.Add("ROI");

        data.Columns.Add("DateLng");
        
        DataRow row = data.NewRow();
        row["Date"] = TOTAL;
        row["DateLng"] = 0;
        row["Install"] = result.Value.DataTotal.Installations;
        row["Uniqe_Installs"] = result.Value.DataTotal.UniqeInstallations;
        row["Effective_Installs"] = result.Value.DataTotal.EffectiveUniqueInstalls;
        row["Total_Installs"] = result.Value.DataTotal.TotalUniqueInstallation;
        row["Uninstalls"] = result.Value.DataTotal.Uninstallation;
        row["DAUS"] = result.Value.DataTotal.DAUS;
        row["DAUs_Percent"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.DAUS_TotalInstallation_percent) + "%";
        row["NP_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.NoProblemRevenue);
        row["Non_NP_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.NonNoProblemRevenue);
        row["Dealply_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.DealplyRevenue);
        row["Jollywallet_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.JollywalletRevenue);
        row["Inter_Yield_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.InterYieldRevenue);
        row["Superfish_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.SuperfishRevenue);
        row["Revizer_Popup_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.RevizerPopupRevenue);
        row["Intext_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.IntextRevenue);
        row["First_Offerz_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.FirstOfferzRevenue);
        row["Admedia_Intext_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.AdmediaIntext);
        row["Adcash_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.AdcashRevenue);
        row["Engageya_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.EngageyaRevenue);
        row["Total_Revenue"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.Revenue);
        row["Cost"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.Cost);
        row["AULT_Total"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.AULT);
        row["AMUV_NoProblem"] =  string.Format(this.GetNumberFormat, result.Value.DataTotal.AMUV_NoProblem);
        row["DUV_NoProblem"] =  string.Format(this.GetNumberFormat, result.Value.DataTotal.DUV_NoProblem);
        row["AMUV_Total"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.AMUV);
        row["DUV_Total"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.DUV);
        row["ROI"] = string.Format(this.GetNumberFormat, result.Value.DataTotal.ROI) + "%";
        data.Rows.Add(row);
        
        foreach (WebReferenceReports.DistributionInstallationReportRow dirr in result.Value.Data)
        {
            DataRow rowDates = data.NewRow();
            rowDates["Date"] = string.Format(siteSetting.DateFormat, dirr.Date);
            rowDates["DateLng"] = dirr.Date.Ticks;
            rowDates["Install"] = dirr.Installations;
            rowDates["Uniqe_Installs"] = dirr.UniqeInstallations;
            rowDates["Effective_Installs"] = dirr.EffectiveUniqueInstalls;
            rowDates["Total_Installs"] = dirr.TotalUniqueInstallation;
            rowDates["Uninstalls"] = dirr.Uninstallation;
            rowDates["DAUS"] = dirr.DAUS;
            rowDates["DAUs_Percent"] = string.Format(this.GetNumberFormat, dirr.DAUS_TotalInstallation_percent) + "%";
            rowDates["NP_Revenue"] = string.Format(this.GetNumberFormat, dirr.NoProblemRevenue);
            rowDates["Non_NP_Revenue"] = string.Format(this.GetNumberFormat, dirr.NonNoProblemRevenue);
            rowDates["Dealply_Revenue"] = string.Format(this.GetNumberFormat, dirr.DealplyRevenue);
            rowDates["Jollywallet_Revenue"] = string.Format(this.GetNumberFormat, dirr.JollywalletRevenue);
            rowDates["Inter_Yield_Revenue"] = string.Format(this.GetNumberFormat, dirr.InterYieldRevenue);
            rowDates["Superfish_Revenue"] = string.Format(this.GetNumberFormat, dirr.SuperfishRevenue);
            rowDates["Revizer_Popup_Revenue"] = string.Format(this.GetNumberFormat, dirr.RevizerPopupRevenue);
            rowDates["Intext_Revenue"] = string.Format(this.GetNumberFormat, dirr.IntextRevenue);
            rowDates["First_Offerz_Revenue"] = string.Format(this.GetNumberFormat, dirr.FirstOfferzRevenue);
            rowDates["Admedia_Intext_Revenue"] = string.Format(this.GetNumberFormat, dirr.AdmediaIntext);
            rowDates["Adcash_Revenue"] = string.Format(this.GetNumberFormat, dirr.AdcashRevenue);
            rowDates["Engageya_Revenue"] = string.Format(this.GetNumberFormat, dirr.EngageyaRevenue);
            rowDates["Total_Revenue"] = string.Format(this.GetNumberFormat, dirr.Revenue);
            rowDates["Cost"] = string.Format(this.GetNumberFormat, dirr.Cost);
            rowDates["AULT_Total"] = string.Format(this.GetNumberFormat, dirr.AULT);
            rowDates["AMUV_NoProblem"] = string.Format(this.GetNumberFormat, dirr.AMUV_NoProblem);
            rowDates["DUV_NoProblem"] = string.Format(this.GetNumberFormat, dirr.DUV_NoProblem);
            rowDates["AMUV_Total"] = string.Format(this.GetNumberFormat, dirr.AMUV);
            rowDates["DUV_Total"] = string.Format(this.GetNumberFormat, dirr.DUV);
            rowDates["ROI"] = string.Format(this.GetNumberFormat, dirr.ROI) + "%";
            data.Rows.Add(rowDates);
        }

        if (data.Rows.Count == 0)
        {
            div_noResults.Visible = true;
            div_container_chart.Visible = false;
            PathV = null;
        }
        else
        {
            div_noResults.Visible = false;
            div_container_chart.Visible = true;
            LoadChart(data);

            /*
            if (_request.Origins == null || _request.Origins.Count() != 1)
                PathV = null;
            else
                LoadChart(data);
             * */
        }
       
        
        dataV = data;
        _GridView.DataSource = data;
        _GridView.DataBind();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetTrTable", "SetTrTable();", true);
        _UpdatePanel.Update();
    }
    void RemoveReportFromPage()
    {
        div_table.Visible = false;
        _GridView.DataSource = null;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    /*
    DataRow AddRowToTable(string detail, int count, DataTable data)
    {
        DataRow row = data.NewRow();
        row["Detail"] = detail;
        row["Count"] = count;
        return row;
    }
     * */
    private void LoadOrigins()
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfOriginWithDAUs result;
        try
        {
            result = _report.GetDistributionOriginsWithDAUs();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        lb_Origin.Items.Clear();
        lb_Origin.Items.Add(new ListItem("ALL", Guid.Empty.ToString()));
        foreach (var gsp in result.Value)
            lb_Origin.Items.Add(new ListItem(gsp.OriginName, gsp.OriginId.ToString()));
       // lb_Origin.Items.Add(new ListItem("stam2", Guid.NewGuid().ToString()));
        lb_Origin.SelectedIndex = 0;
        /*
        ddl_Origin.Items.Clear();
        ddl_Origin.Items.Add(new ListItem("ALL", Guid.Empty.ToString()));
        foreach (WebReferenceReports.GuidStringPair gsp in result.Value)
            ddl_Origin.Items.Add(new ListItem(gsp.Name, gsp.Id.ToString()));
        ddl_Origin.SelectedIndex = 0;
         * */

    }
    
    #region  chart
    bool LoadChart(DataTable data)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + lbl_TitleReport.Text + @"' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='3' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");
        sb.Append(@"numberSuffix='' ");
        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar=' ' ");

        sb.Append(@"bgColor='FFFFFF'>");
        //row["AULT"]
        int modulu = (data.Rows.Count > 15) ? 7 : 1;
       


       // int modulu = data.Rows.Count / 15;
    //    modulu++;

        
        sb.Append(@"<categories>");
        Random rnd = new Random();
        Dictionary<string, StringBuilder> dic = new Dictionary<string,StringBuilder>();
    //    List<string> SelectedItems = new List<string>();
        foreach(ListItem li in cbl_chart.Items)
        {
             if (li.Selected)
             {
    //             SelectedItems.Add(li.Value);
                 StringBuilder _sb = new StringBuilder(@"<dataset seriesName='" + li.Text + @"' color='" + Utilities.GenerateColor(rnd) + "'>");
                 dic.Add(li.Value, _sb);
             }
        }
        for (int i = data.Rows.Count - 1; i > -1; i--)
            //foreach (DataRow row in data.Rows)
        {
            string _date = data.Rows[i].Field<string>("Date");
            if (_date == TOTAL)
                continue;
            string _title = string.Empty;
            foreach (KeyValuePair<string, StringBuilder> kvp in dic)//foreach(string s in SelectedItems)
            {
                string _value = data.Rows[i][kvp.Key].ToString();
                _value = _value.Replace(",", "");
                _value = _value.Replace("%", "");
                kvp.Value.Append(@"<set value='" + _value + @"'/>");
                _title += kvp.Key + "=" + _value + "\r\n";
            }
            sb.Append(@"<category name='" + data.Rows[i].Field<string>("Date") + @"' toolText='" + _title + "'/>");
        }
        sb.Append("</categories>");
        foreach (KeyValuePair<string, StringBuilder> kvp in dic)
        {
            kvp.Value.Append(@"</dataset>");
            sb.Append(kvp.Value.ToString());        
        }
               
        sb.Append(@"</graph>");

        return SetChartXML(sb);
       
    }
    bool SetChartXML(StringBuilder sb)
    {
        string path = //HttpContext.Current.Request.PhysicalApplicationPath + 
           ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            return false;
        }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        return true;
    }

    #endregion

    DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    string PathV
    {
        get { return (ViewState["XmlGraph"] == null) ? "" : (string)ViewState["XmlGraph"]; }
        set { ViewState["XmlGraph"] = value; }
    }

    protected void btn_GroupByIp_Click(object sender, EventArgs e)
    {
        Guid originId = Guid.Empty;
        string originName = String.Empty;
        //get origin selected:
        foreach (ListItem li in lb_Origin.Items)
        {
            if (!li.Selected)
                continue;
            Guid _id;
            if (!Guid.TryParse(li.Value, out _id))
                continue;
            if (_id == Guid.Empty || originId != Guid.Empty)
            {
                //pelase select a single origin.
                HandleNotOriginSelectedByIp();
                return;
            }
            originId = _id;
            originName = li.Text;
        }
        if (originId == Guid.Empty)
        {
            //pelase select a single origin.
            HandleNotOriginSelectedByIp();
            return;
        }
        WebReferenceReports.DistributionsInstallationsByIpRowRequest _request = new WebReferenceReports.DistributionsInstallationsByIpRowRequest();
        _request.From = FromToDatePicker_current.GetDateFrom;
        _request.To = FromToDatePicker_current.GetDateTo;
        _request.OriginId = originId;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfDistributionsInstallationsByIpRow result;
        try
        {
            result = _report.DistributaionInstallationsByIpReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        List<CsvRow> rows = new List<CsvRow>();
        CsvRow titles = new CsvRow();
        titles.Add("IP");
        titles.Add("EasternStandardTime_Utc5");
        titles.Add("UtcTime");

        rows.Add(titles);
        foreach (var item in result.Value)
        {
            CsvRow r = new CsvRow();
            r.Add(item.Ip);
            r.Add(String.Format("{0:dd/MM/yyyy HH:mm:ss}", item.EasternStandardTime));
            r.Add(String.Format("{0:dd/MM/yyyy HH:mm:ss}", item.UtcTime));
            rows.Add(r);
        }
        string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int rnd = new Random().Next();
        path += rnd + CSV_NAME;
        /*
        string filePath = onlyDir + "\\" + fileName;
        int ind = 0;
        
        while (File.Exists(filePath))
        {
            filePath = filePath.Insert(filePath.Length - 4, ind.ToString());
            ind++;
        }
          */
        using (FileStream file = File.Create(path))
        {
            using (CsvFileWriter writer = new CsvFileWriter(file))
            {
                foreach (var row in rows)
                {
                    writer.WriteRow(row);
                }
            }
        }
        Response.ClearContent();
        Response.ContentType = "application/octet-stream";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + CSV_NAME);
        Response.TransmitFile(path);
        Response.Flush();
        Response.End();
        /*
        Response.BinaryWrite(File.ReadAllBytes(path));
        Response.End();
         * */
        /*
        DataTable data = new DataTable();
        data.Columns.Add("IP");
        data.Columns.Add("Count");
        foreach (var item in result.Value)
        {
            DataRow row = data.NewRow();
            row["IP"] = item.Ip;
            row["Count"] = item.Count;
            data.Rows.Add(row);
        }
        ToExcel te = new ToExcel(this, "InstallationsByIp_" + originName);
        if (!te.ExecExcel(data))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
         * */
    }

    private void HandleNotOriginSelectedByIp()
    {
        string _script = "alert('Please select a single origin');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PleaseSelectSingleOrigin", _script, true);
    }
    WebReferenceReports.AddOnInstallationReportRequest RequestSession
    {
        set
        {
            Session["AddOnInstallationReportRequest"] = value;
        }
    }
    protected string GetDrillDownPath
    {
        
        get {
            return ResolveUrl("~/Publisher/PagingService.asmx/GetInstallationDrillDown");
            
        }
    
    }
}