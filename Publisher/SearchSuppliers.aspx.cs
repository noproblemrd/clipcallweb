﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web.Services.Protocols;
using System.Text;

public partial class Publisher_SearchSuppliers : PageSetting, ICallbackEventHandler, IPostBackEventHandler
{


    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    Dictionary<WebReferenceSupplier.DapazStatus, string> ListDapazStatus;
    protected void Page_Load(object sender, EventArgs e)
    {
        

        //Button for Async IPostBackEvent
        ScriptManager1.RegisterAsyncPostBackControl(btn_virtual_run);
        //
        Master.newAdvEvent += new EventHandler(Master_newAdvEvent);
        ScriptManager1.RegisterAsyncPostBackControl(btnSearch);
        ScriptManager1.RegisterAsyncPostBackControl(btn_InactiveReason);
        TablePagingConfiguration();
        SetupClient();
        SetPlaceHolder();
        LoadDapazStatusSentences();
        SetDapazStatusSentences();
        if (IsPostBack)
        {
            LoadTRInline();
            /*
            Dictionary<int, string> dic = PageListV;
            foreach (KeyValuePair<int, string> kvp in dic)
                PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
             * */
            if (Request["__EVENTARGUMENT"] == RUN_REPORT)
                Search_Report();
        }
        else
        {
            LoadInactivityReasons();
            div_status.Visible = false;
            LoadFundsStatuses();
            ZapLoad();
        }
        VirtualNumberDialog.DataBind();
        AccountDialog.DataBind();
        Header.DataBind();


    }

    private void LoadFundsStatuses()
    {
        ddl_FundsStatus.Items.Clear();
        foreach (WebReferenceSite.FundsStatus fs in Enum.GetValues(typeof(WebReferenceSite.FundsStatus)))
        {
            ddl_FundsStatus.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "FundsStatus", fs.ToString()), fs.ToString()));
        }
    }

    private void SetDapazStatusSentences()
    {
        StringBuilder sb = new StringBuilder();
        foreach (KeyValuePair<WebReferenceSupplier.DapazStatus, string> kvp in ListDapazStatus)
        {
            string kv = kvp.Key.ToString() + ";;;" + kvp.Value;
            sb.Append(kv + "|||");
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetStatuses", "SetStatuses('" + sb.ToString() + "');", true);
    }

    private void ZapLoad()
    {
        if (siteSetting.GetSiteID == PpcSite.GetCurrent().ZapSiteId)
       // if (siteSetting.GetSiteID == "zapqa")
        {
            Master.RemoveNewAdvertiser();
            if (userManagement.Get_Guid != new Guid("27F454E4-E63E-4DF1-A730-63E9132E1FE4") && //הילה אלבו קוצר
                userManagement.Get_Guid != new Guid("9E9FB293-267C-4E60-8D62-4DD5C8FF46FC")) //אופיר שפירוביץ
            {
                txt_BizId.ReadOnly = true;
                txt_BizId.CssClass = "read-only";
            }
            txt_Name.ReadOnly = true;
            txt_Name.CssClass = "read-only";
        }
    }

    private void LoadDapazStatusSentences()
    {
        ListDapazStatus = new Dictionary<WebReferenceSupplier.DapazStatus, string>();
        foreach (WebReferenceSupplier.DapazStatus ds in Enum.GetValues(typeof(WebReferenceSupplier.DapazStatus)))
            ListDapazStatus.Add(ds, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "DapazStatus", ds.ToString()));
    }

    private void SetPlaceHolder()
    {
        txtSearch.Attributes["place_holder"] = hf_WaterMarkSearch.Value;
        txtSearch.Attributes["HolderClass"] = "WaterMarkTxt";
    }
    private void TablePagingConfiguration()
    {
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
     //   TablePaging1.PagesInPaging = PAGE_PAGES;
     //   TablePaging1.ItemInPage = ITEM_PAGE;
    }

    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (SearchSuppliersRequestV == null)
            return;
        WebReferenceSupplier.SearchSuppliersRequest _request = SearchSuppliersRequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        LoadTable(_request);
    }
    private void LoadInactivityReasons()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfListOfTableRowData reasons = null;
        try
        {
            reasons = _supplier.GetAllInactivityReasons();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (reasons.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_InactiveReasons.Items.Clear();
        foreach (var gsp in reasons.Value)
        {
            if (gsp.Inactive == false)
                ddl_InactiveReasons.Items.Add(new ListItem(gsp.Name, gsp.Guid.ToString()));
        }
    }

    void Master_newAdvEvent(object sender, EventArgs e)
    {
        //       ScriptManager1.a
        string csnameStep = "setStep";
        string csTextStep = "$(document).ready(function(){$.colorbox({href:'framepublisher.aspx',width:'95%', height:'90%', iframe:true});});";
        //          csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), csnameStep, csTextStep, true);
        //     ScriptManager.RegisterStartupScript(this, this.GetType(), "testi", "testi();", true);   

    }

    private void LoadTRInline()
    {
        List<string> list = listTR;
        if (list.Count > 0)
        {
            foreach (RepeaterItem item in _reapeter.Items)
            {
                //           HtmlGenericControl div = (HtmlGenericControl)item.FindControl("div_openClose");
                HtmlTableRow trDetails = (HtmlTableRow)item.FindControl("tr_openClose");
                if (list.Contains(trDetails.ClientID))
                    trDetails.Attributes.Remove("style");
                else
                    trDetails.Attributes["style"] = "display:none";
            }
        }
    }

    protected void BindData(DataTable data)
    {
        _reapeter.DataSource = data;
        _reapeter.DataBind();
        Update_UpdatePanel();
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow trDetails = (HtmlTableRow)e.Item.FindControl("tr_openClose");
        HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("tr_show");
        HtmlAnchor a_open = (HtmlAnchor)e.Item.FindControl("a_open");
        HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("td_openClose");
        tr.Attributes["class"] = (e.Item.ItemIndex % 2 == 0) ? "odd" : "even";
        a_open.Attributes["href"] = "javascript:OpenCloseDetail('" + trDetails.ClientID + "','" + td.ClientID + "');";
    }
    
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Search_Report();
    }
    protected void txtSearch_Change(object sender, EventArgs e)
    {
        Search_Report();
    }
    
    void Search_Report()
    {
        string searchTxt = txtSearch.Text.Trim();
        if (searchTxt.Length < 2)
            return;
        _PanelNoReasult.Visible = false;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Search_focus", "SetCursorToTextEnd('" + txtSearch.ClientID + "');", true);
        
        WebReferenceSupplier.SearchSuppliersRequest _request = new WebReferenceSupplier.SearchSuppliersRequest();
        _request.PageSize = TablePaging1.ItemInPage;
        _request.PageNumber = 1;
        _request.SearchValue = searchTxt;
        SearchSuppliersRequestV = _request;
        LoadTable(_request);
    }

    private DataResult ExecSearchSupplier(WebReferenceSupplier.SearchSuppliersRequest _request)
    {
        DataTable data = new DataTable();
        data.Columns.Add("accountid", typeof(string));
        //       data.Columns.Add("NavigateUrl", typeof(string));
        data.Columns.Add("name", typeof(string));
        data.Columns.Add("telephone1", typeof(string));
        data.Columns.Add("telephone2", typeof(string));
        data.Columns.Add("address1_line1", typeof(string));
        data.Columns.Add("address1_city", typeof(string));
        data.Columns.Add("script", typeof(string));
        data.Columns.Add("emailaddress1", typeof(string));
        data.Columns.Add("status", typeof(string));
        //  data.Columns.Add("statuscode", typeof(string));
        data.Columns.Add("s_image", typeof(string));
        data.Columns.Add("SupplierNum", typeof(string));
        data.Columns.Add("Balance", typeof(string));
        data.Columns.Add("fax", typeof(string));
        data.Columns.Add("numberofemployees", typeof(string));
        data.Columns.Add("websiteurl", typeof(string));
        data.Columns.Add("address1_line2", typeof(string));
        data.Columns.Add("address1_postalcode", typeof(string));
        data.Columns.Add("ContactName", typeof(string));
        data.Columns.Add("onclick", typeof(string));
        data.Columns.Add("IsSmallWin", typeof(bool));
        data.Columns.Add("SupplierStatus", typeof(string));
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfSearchSuppliersResponse result = null;
        try
        {
            result = supplier.SearchSuppliers(_request);

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            return null;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)        
            return null;

        foreach (WebReferenceSupplier.SupplierSearchRow ssr in result.Value.Rows)
        {
            string _contactName1;
            string _contactName2;
            DataRow row = data.NewRow();
            row["accountid"] = ssr.SupplierId.ToString();
            row["SupplierStatus"] = ListDapazStatus[ssr.DapazStatus];
            row["name"] = string.IsNullOrEmpty(ssr.Name) ? "***" : ssr.Name;
            row["telephone1"] = ssr.Telephone1;
            row["telephone2"] = ssr.Telephone2;
            row["address1_line1"] = ssr.Street;
            row["address1_city"] = ssr.City;
            //  row["address1_country"] = ssr.c (node["address1_country"] == null) ? string.Empty : node["address1_country"].InnerText;
            row["emailaddress1"] = ssr.Email;
            row["Balance"] = string.Format(NUMBER_FORMAT, ssr.CashBalance);
            row["SupplierNum"] = ssr.Number;
            row["fax"] = ssr.Fax;
            row["numberofemployees"] = ssr.NumberOfEmployees.ToString();
            row["websiteurl"] = ssr.WebSite;
            row["address1_line2"] = ssr.HouseNumber;
            row["address1_postalcode"] = ssr.ZipCode;
            bool IsSmallWin = ssr.DapazStatus != WebReferenceSupplier.DapazStatus.RarahQuote && ssr.DapazStatus != WebReferenceSupplier.DapazStatus.PPA && ssr.DapazStatus != WebReferenceSupplier.DapazStatus.PPAHold;
            row["IsSmallWin"] = IsSmallWin;
            _contactName1 = ssr.FirstName;
            _contactName2 = ssr.LastName;
            row["onclick"] = (IsSmallWin) ? "return OpenDpzSupplierDetails('" + ssr.SupplierId.ToString() + "');" : "return true;";

            _contactName1 = (string.IsNullOrEmpty(_contactName1) ? _contactName2 :
                _contactName1 + ((string.IsNullOrEmpty(_contactName2)) ? "" : " " + _contactName2));
            row["ContactName"] = _contactName1;
            //row["status"] = ssr.SupplierStatus.ToString();
            //By Alain 29.11.11 to fix that there is a new status "Trial" and there is no color for it.
            row["status"] = ssr.SupplierStatus == WebReferenceSupplier.SupplierStatus.Trial ? WebReferenceSupplier.SupplierStatus.Inactive.ToString() : ssr.SupplierStatus.ToString();

            row["s_image"] = GetRamzorImage(ssr.SupplierStatus, ssr.IsInactiveWithMoney);
            if (ssr.SupplierStatus == WebReferenceSupplier.SupplierStatus.Inactive)
                row["script"] = "return ConfirmActiveAccount();";
            else
                row["script"] = "InactiveAccount('" + ssr.SupplierId + "'); return false;";


            data.Rows.Add(row);
        }
       return new DataResult() { data = data, CurrentPage = result.Value.CurrentPage, TotalPages = result.Value.TotalPages, TotalRows = result.Value.TotalRows };
    }
    void LoadTable(WebReferenceSupplier.SearchSuppliersRequest _request)
    {
        DataResult dr = ExecSearchSupplier(_request);
        if (dr == null)
        {
            Update_Faild();
            ClearTable();
            return;
        }
        if (dr.data.Rows.Count == 0)
        {
            ClearTable();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(Master.GetNoResultsMessage) + "');", true);
            return;
        }
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
        BindData(dr.data);
        listTR = new List<string>();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "BlurTxtSearch", "BlurTxtSearch();", true);
    }
    void ClearTable()
    {
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        LoadGeneralData(0, 0, 0);
        Update_UpdatePanel();
        PanelResults.Visible = false;
        div_status.Visible = false;
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_Count.Text = _TotalRows.ToString();
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
        PanelResults.Visible = true;
        div_status.Visible = true;
        _UpdatePanelTotalResult.Update();
    }
    string GetRamzorImage(WebReferenceSupplier.SupplierStatus _status, bool IsInactiveWithMoney)
    {
        if(IsInactiveWithMoney)
            return "../images/InactiveWithMoney.png";
        if (_status == WebReferenceSupplier.SupplierStatus.Available)
            return "../images/available.png";
        else if (_status == WebReferenceSupplier.SupplierStatus.Inactive)
            return "../images/innactive.png";
        else if (_status == WebReferenceSupplier.SupplierStatus.Candidate)
            return "../images/candidate.png";
        else if (_status == WebReferenceSupplier.SupplierStatus.Unavailable)
            return "../images/unavailable.png";

        return string.Empty;
    }
   
    protected void btn_Edit_click(object sender, EventArgs e)
    {
        //System.Web.SessionState.HttpSessionState d = new System.Web.SessionState.HttpSessionState();         
        ClearAccountStatus();
        //   IsBack = true;
        LinkButton btn = (LinkButton)sender;
        //btn.Attributes.Add("onClick", "$(document).ready(function(){$.colorbox({href:'framepublisher.aspx',width:'90%', height:'90%', iframe:true});});");

        //Response.Write(btn.ClientID);
        RepeaterItem ri = (RepeaterItem)btn.NamingContainer;
        string name = btn.CommandArgument;
        string _id = ((Label)ri.FindControl("lbl_Guid")).Text;
        string _code = ((LinkButton)ri.FindControl("lb_SupplierNum")).Text;
        string _email = ((Label)ri.FindControl("lbl_emailaddress")).Text;
        UserMangement um = new UserMangement(new Guid(_id), name, _code, _email);
        SetGuidSetting(um);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "openPopup",
            @"$(document).ready(function(){$.colorbox({href:'framepublisher.aspx',width:'95%', height:'90%', iframe:true});});", true);

        
        Update_UpdatePanel();

    }
    void ClearAccountStatus()
    {
        Session["Account_Status"] = null;
    }


    protected void btn_suspend_click(object sender, EventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        Guid accountId = new Guid(btn.CommandArgument);
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfSupplierStatus result = null;
        try
        {
            result = _supplier.ActivateSupplier(accountId, userManagement.Get_Guid);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        string _img = GetRamzorImage(result.Value, false);
        
        RepeaterItem ri = (RepeaterItem)btn.NamingContainer;
        ((Label)ri.FindControl("lbl_status")).Text = result.Value.ToString();
        btn.ImageUrl = _img;
        btn.OnClientClick = "InactiveAccount('" + accountId.ToString() + "'); return false;";
        Update_UpdatePanel();
    }
    void Update_UpdatePanel()
    {
     //   ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadPlaceHolder", "ReloadPlaceHolder();", true);
        _UpdatePanel.Update();
    }
    protected void btn_InactiveReason_click(object sender, EventArgs e)
    {
        Guid accountId = new Guid(hf_AccountId.Value);
        Guid ReasonId = new Guid(ddl_InactiveReasons.SelectedValue);
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfDeactivateSupplierResponse result = null;
        try
        {
            result = _supplier.DeactivateSupplier(accountId, ReasonId, userManagement.Get_Guid);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        string _img = GetRamzorImage(WebReferenceSupplier.SupplierStatus.Inactive, result.Value.IsInactiveWithMoney);
       
        foreach (RepeaterItem item in _reapeter.Items)
        {
            Guid _id = new Guid(((Label)item.FindControl("lbl_Guid")).Text);
            if (_id == accountId)
            {
                ((Label)item.FindControl("lbl_status")).Text = WebReferenceSupplier.SupplierStatus.Inactive.ToString();
                ImageButton ib = (ImageButton)item.FindControl("Image_status");
                ib.OnClientClick = "return ConfirmActiveAccount();";
                ib.ImageUrl = _img;
                break;
            }
        }
        popUpStatusReason.Attributes["class"] = "popModal_del popModal_del_hide";
        hf_AccountId.Value = "";
        ddl_InactiveReasons.SelectedIndex = 0;
        _mpe.Hide();
        Update_UpdatePanel();
    }
    
    
    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        return "";
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        List<string> list = listTR;
        string[] args = eventArgument.Split(';');
        if (args[1] == "1")
            list.Add(args[0]);
        else
            list.Remove(args[0]);
        listTR = list;
    }

    #endregion

    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }
    #region IPostBackEventHandler Members
    const string RUN_REPORT = "RUN_REPORT";
    public void RaisePostBackEvent(string eventArgument)
    {
        /*
        if(eventArgument == RUN_REPORT)
            Search_Report();
         * */
    }
    protected override void Render(HtmlTextWriter writer)
    {

        Page.ClientScript.RegisterForEventValidation(btn_virtual_run.UniqueID, RUN_REPORT);
        base.Render(writer);
    }
    protected string RunReport()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(btn_virtual_run, RUN_REPORT);
        myPostBackOptions.PerformValidation = false;
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
     //   WebReferenceSite.FundsStatus
    }
    #endregion
    List<string> listTR
    {
        get { return (Session["_listTR"] == null) ? new List<string>() : (List<string>)Session["_listTR"]; }
        set { Session["_listTR"] = value; }
    }
    
    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }

    WebReferenceSupplier.SearchSuppliersRequest SearchSuppliersRequestV
    {
        get
        {
            return (Session["SearchSuppliersRequest"] == null) ? null : (WebReferenceSupplier.SearchSuppliersRequest)Session["SearchSuppliersRequest"];
        }
        set { Session["SearchSuppliersRequest"] = value; }
    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
    protected string ScriptPlaceHolder
    {
        get { return ResolveUrl("~/PlaceHolder.js"); }
    }
    protected string GetVirtualNumber
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetVirtualNumber"); }
    }
    protected string GetDpzSupplierDetails
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetSupplirDetails"); }
    }
    protected string SetDpzSupplierDetails
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/SetSupplirDetails"); }
    }
    /*
    protected string MoveToPPCService
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/MoveToPPC"); }
    }
     * */
    protected string GetPhoneRegularExpression
    {
        get { return siteSetting.GetPhoneRegularExpression; }
    }
    protected string GetAccountNumberRegularExpression
    {
        get { return @"^\d{1,8}$"; }
    }
    protected string UpdateFaild
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_UpdateFaild.Text); }
    }
    
    protected string FixStatus
    {
        get { return WebReferenceSupplier.DapazStatus.Fix.ToString(); }
    }
    protected string PPAStatus
    {
        get { return WebReferenceSupplier.DapazStatus.PPA.ToString(); }
    }
    protected string RarahStatus
    {
        get { return WebReferenceSupplier.DapazStatus.Rarah.ToString(); }
    }
    protected string RarahQuoteStatus
    {
        get { return WebReferenceSupplier.DapazStatus.RarahQuote.ToString(); }
    }
    protected string PPAHoldStatus
    {
        get { return WebReferenceSupplier.DapazStatus.PPAHold.ToString(); }
    }
    protected string GetFundsStatusNormal
    {
        get { return WebReferenceSite.FundsStatus.Normal.ToString(); }
    }    
}
