﻿<%@ Page Language="C#"  MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SubscriptionReport.aspx.cs" Inherits="Publisher_SubscriptionReport" %>


<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>  
    <script type="text/javascript">
         window.onload = appl_init;

         function appl_init() {
             var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
             pgRegMgr.add_beginRequest(BeginHandler);
             pgRegMgr.add_endRequest(EndHandler);

         }
         function BeginHandler() {
             //     alert('in1');
             showDiv();
         }
         function EndHandler() {
             //      alert('out1');
             hideDiv();
         }
        
         function _CreateExcel() {
             var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"<%# EXCEL_NAME %>"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcelUrl %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
         }
         function chkTextBox(event) {
             //     alert(event.keyCode);
             if (event.keyCode != 13)
                 return true;
             run_report();
             return false;

         }
         function run_report() {

            <%# RunReport() %>;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
       <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	<%/*  
               <div class="main-inputs" runat="server" id="Div1">		
	                
                        
            			 <div class="form-field">
                            <asp:Label ID="lbl_IncludeQaTest" runat="server" Text="Include QA" CssClass="label"></asp:Label>
                           <asp:CheckBox ID="cb_IncludeQaTest" runat="server" />
                        </div>
                   </div>
        **/ %>        
             <div class="clear"></div>
                            
                <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>  
                		 
			    <div class="form-field" style="position:absolute; top:0; left:350px;">
                    <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Free Text</label>
			        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-text" MaxLength="40" onkeydown="return chkTextBox(event);"></asp:TextBox>								
                
                </div>   
            </div>

            <div style="clear:both;"></div>
    	
	        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	            <ContentTemplate>	
                    <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
	                <div >
                              
                                <asp:GridView ID="_gv" runat="server" CssClass="data-table2" AutoGenerateColumns="false">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />                        
                    <Columns>
                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label101" runat="server" Text="Created On"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label102" runat="server" Text='<%# Bind("CreatedOn") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label103" runat="server" Text="Customer name"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink2" runat="server" Text='<%# Bind("CustomerName") %>' Target="_blank" NavigateUrl='<%# Bind("RequestReport") %>'></asp:HyperLink>
                            
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label105" runat="server" Text="Customer mail"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink3" runat="server" Text='<%# Bind("CustomerEmail") %>' Target="_blank" NavigateUrl='<%# Bind("RequestReport") %>'></asp:HyperLink>                            
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label107" runat="server" Text="Customer phone"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink4" runat="server" Text='<%# Bind("CustomerPhone") %>' Target="_blank" NavigateUrl='<%# Bind("RequestReport") %>'></asp:HyperLink> 
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label177" runat="server" Text="Origin Skin"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label178" runat="server" Text='<%# Bind("OriginSkin") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label197" runat="server" Text="Origin"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label198" runat="server" Text='<%# Bind("Origin") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label109" runat="server" Text="Prime collection # month"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label110" runat="server" Text='<%# Bind("PrimeCollection") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label111" runat="server" Text="# of prime transactions"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label112" runat="server" Text='<%# Bind("Transactions") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label113" runat="server" Text="Total prime discount"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label114" runat="server" Text='<%# Bind("TotalPlanDiscount") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label229" runat="server" Text="Is Active"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image10" runat="server" ImageUrl='<%# Bind("IsActiveUrl") %>' />
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label239" runat="server" Text="Canceled at"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label240" runat="server" Text='<%# Bind("CanceledAt") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label116" runat="server" Text="Stripe link"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl='<%# Bind("SubsctiptionLink") %>' Visible='<%# Bind("HasSubsctiptionLink") %>'>Link</asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
        </asp:GridView>
                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
        <asp:Button ID="btn_virtual_run" runat="server" Text="Button" style="display:none;" onfocus="this.blur();" />
        </div>
</asp:Content>