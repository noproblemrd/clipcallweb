using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Data.SqlClient;

public partial class Publisher_PricingManagment : PageSetting, IPostBackEventHandler
{
    const string MY_CLICK = "SetNewPackage";
    
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    public string GetPostBack
    {
        get { return ClientScript.GetPostBackEventReference(this, MY_CLICK); }
    }
    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        if (eventArgument == MY_CLICK)
        {
            string _id = hf_PackegeId.Value;
            if (string.IsNullOrEmpty(_id))
                SetNewPackage();
            else
                EditPackage(new Guid(_id));
        }
    }

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
  //      ScriptManager1.RegisterAsyncPostBackControl(btn_Set);
  //      ScriptManager1.RegisterAsyncPostBackControl(txt_newBonus);
        if (!IsPostBack)
        {
            SetToolBox();
            LoadSupplierTypes();
            Page.DataBind();
            LoadPackages();
            SetGridView();
            LoadPaymentImage();
            
        }
        SetToolBoxEvents();
        Page.Header.DataBind();
    }

    private void LoadPaymentImage()
    {
        /*
        string command = "SELECT dbo.GetPaymentImageBySiteNameId(@SiteNameId)";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
        string _img = (cmd.ExecuteScalar() == DBNull.Value) ? string.Empty : (string)cmd.ExecuteScalar();
        conn.Close();
         * */
        string _img = DBConnection.GetPaymentImage(siteSetting.GetSiteID);
        if (string.IsNullOrEmpty(_img))
            div_image.Visible = false;
        else
        {
            div_image.Visible = true;
            img_Payment.ImageUrl = _img;
        }
    }

    private void LoadSupplierTypes()
    {
        Dictionary<string, string> dic_Translate = new Dictionary<string, string>();
        foreach (string str in Enum.GetNames(typeof(WebReferenceSite.PricingPackageSupplierType)))
        {
            string _translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "PricingPackageSupplierType", str);
            dic_Translate.Add(str, _translate);
            ListItem li = new ListItem(_translate, str);
            ddl_SupplierType.Items.Add(li);
            if (str == WebReferenceSite.PricingPackageSupplierType.All.ToString())
                ddl_SupplierType.SelectedIndex = ddl_SupplierType.Items.Count - 1;
        }
        DicTranslateV = dic_Translate;
    }
    private void SetToolBox()
    {
        string script = "openPopup(); return false;";
        Toolbox1.SetClientScriptToAdd(script);
    //    Toolbox1.RemoveAttach();
      //  script = "return confirm('" + Toolbox1.GetConfirmDelete + "');";
     //   Toolbox1.SetClientScriptToDelete(script);
    }
    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
    //    Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
        Toolbox1.AttachExec += new EventHandler(Toolbox1_AttachExec);
    }

    void Toolbox1_AttachExec(object sender, EventArgs e)
    {
        string localPath = Toolbox1.GetUrlFile();
        string file_name = Path.GetFileName(localPath);
        SetPaymentImage(file_name);        
    }
    void SetPaymentImage(string file_name)
    {
        string command = "EXEC dbo.SetPaymentImageBySiteNameId @SiteNameId, @PaymentImage";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            if (string.IsNullOrEmpty(file_name))
                cmd.Parameters.AddWithValue("@PaymentImage", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@PaymentImage", file_name);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
        if (string.IsNullOrEmpty(file_name))
        {
            string _url = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
            _url += file_name;
            if (File.Exists(_url))
                File.Delete(_url);
            div_image.Visible = false;

        }
        else
        {
            string _url = System.Configuration.ConfigurationManager.AppSettings["professionalRecordWeb"];
            _url += file_name;
            img_Payment.ImageUrl = _url;
            div_image.Visible = true;
        }
    }

    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        bool IsSelect = false;
        int indx = -1;
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
            {
                if (IsSelect)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ChooseOne", "alert('" + Toolbox1.ChooseOne + "');", true);
                    return;
                }
                IsSelect = true;
                indx = row.RowIndex;
            }
        }
        if (indx > -1)
        {
            GridViewRow row = _GridView.Rows[indx];
            hf_PackegeId.Value = ((Label)row.FindControl("lbl_PackegeId")).Text;
            txt_NewName.Text = ((Label)row.FindControl("lblName")).Text;
            txt_NewFromAmount.Text = ((Label)row.FindControl("lbl_FromAmount")).Text;
            txt_newBonus.Value = ((Label)row.FindControl("lblBonus")).Text;
            string Type_value = ((Label)row.FindControl("lblSupplierTypeValue")).Text;
            for (int i = 0; i < ddl_SupplierType.Items.Count; i++)
            {
                if (ddl_SupplierType.Items[i].Value == Type_value)
                {
                    ddl_SupplierType.SelectedIndex = i;
                    break;
                }
            }
            _mpe.Show();
        }
    }

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            if (cb.Checked)
            {
                Guid _id = new Guid(((Label)row.FindControl("lbl_PackegeId")).Text);
                DeletePackage(false, _id);
            }
        }
        Toolbox1.GetTextBoxSearch.Value = string.Empty;
        LoadPackages();
        SetGridView();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataViewV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            return;
        }
  //      PrintHelper.PrintWebControl(_GridView, dataViewV);
        Session["data_print"] = dataViewV.Table;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        if (dataViewV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordExport + "');", true);
            return;
        }

        ToExcel to_excel = new ToExcel(this, "Segments");
        if (!to_excel.ExecExcel(dataViewV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        string name = Toolbox1.GetTxt;
        HtmlInputText _tb = Toolbox1.GetTextBoxSearch;
        if (name == Toolbox1.GetWaterTextSearch &&
                    _tb.Attributes["class"].Contains("watermark"))
            name = string.Empty;
        if (string.IsNullOrEmpty(name))
            dataViewV = dataV.AsDataView();
        else
        {
            name = name.ToLower();
            DataTable data = dataV;
            data.TableName = "Search";

            EnumerableRowCollection<DataRow> query = from Search in data.AsEnumerable()
                                                     where (Search.Field<string>("Name").ToLower().Contains(name))                                                        
                                                     orderby Search.Field<string>("Name")
                                                     select Search;

            dataViewV = query.AsDataView();
        }
        _GridView.PageIndex = 0;
        SetGridView();
    }
    private void LoadPackages()
    {
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllPricingPackagesResponse result = null;
        try
        {
            result = _site.GetAllPricingPackages();
        }        
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        
        
        DataTable data = GetDataTable.GetDataTableColumsString(result.Value.Packages);
        data.Columns.Add("SupplierTypeValue", typeof(string));

//        WebReferenceSite.PricingPackageSupplierType
        foreach (WebReferenceSite.PricingPackageData ppd in result.Value.Packages)
        {
            DataRow row = data.NewRow();
            row["Number"] = ppd.Number;
            row["PricingPackageId"] = ppd.PricingPackageId.ToString();
            row["Name"] = ppd.Name;
            row["FromAmount"] = ppd.FromAmount.ToString();
            row["bonusPercent"] = ppd.BonusPercent.ToString();
       //     row["ActiveIcon"] = (ppd.IsActive ? "images/ramzor-green.png" : "images/ramzor-red.png");
            row["IsActive"] = ppd.IsActive.ToString();
            row["SupplierType"] = DicTranslateV[ppd.SupplierType.ToString()];
            row["SupplierTypeValue"] = ppd.SupplierType.ToString();
      //      row["IsInActive"] = !ppd.IsActive;
            data.Rows.Add(row);
        }
     //   DataView dada_view = data.AsDataView();
        _GridView.EditIndex = -1;
        dataV = data;
        dataViewV = data.AsDataView();
        
        
    }
    void SetGridView()
    {
        StringBuilder sb = new StringBuilder();
        DataView data = dataViewV;

        _GridView.DataSource = data;
        _GridView.DataBind();
        
        if (data.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noreasultSearch", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            CBsV = string.Empty;
            _updatePanelTable.Update();
            return;
        }
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }

        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        _updatePanelTable.Update();
    }
    void DeletePackage(bool _active, Guid PricingPackageId)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.SetPricingPackageStateRequest _request = new WebReferenceSite.SetPricingPackageStateRequest();
        _request.IsActive = _active;
        _request.PricingPackageId = PricingPackageId;
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.SetPricingPackageState(_request);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        
    }
    
    protected void _btn_Set_Click(object sender, EventArgs e)
    {
        string _id = hf_PackegeId.Value;
        if (string.IsNullOrEmpty(_id))
            SetNewPackage();
        else
            EditPackage(new Guid(_id));
    }
    void EditPackage(Guid _id)
    {
        int FromAmount, BonusPercent;
        if (!int.TryParse(txt_NewFromAmount.Text, out FromAmount))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (!int.TryParse(txt_newBonus.Value, out BonusPercent))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        WebReferenceSite.PricingPackageSupplierType ppst;
        if (!Enum.TryParse(ddl_SupplierType.SelectedValue, out ppst))
            ppst = WebReferenceSite.PricingPackageSupplierType.All;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.UpdatePricingPackageRequest _request = new WebReferenceSite.UpdatePricingPackageRequest();
        _request.BonusPercent = BonusPercent;
        _request.FromAmount = FromAmount;
        _request.Name = txt_NewName.Text;
        _request.PricingPackageId = _id;
        _request.SupplierType = ppst;
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.UpdatePricingPackage(_request);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(_updatePanelTable, _updatePanelTable.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        //closePopup();
        //    ScriptManager.RegisterStartupScript(_updatePanelTable, _updatePanelTable.GetType(), "ClosePopupNewPackage", "alert('ttt');", true);       
        _mpe.Hide();
        ScriptManager.RegisterStartupScript(_updatePanelTable, _updatePanelTable.GetType(), "ClosePopupPackage", "closePopup();", true);
        LoadPackages();
        SetGridView();
    }
    void SetNewPackage()
    {
        int FromAmount, BonusPercent;
        if (!int.TryParse(txt_NewFromAmount.Text, out FromAmount))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (!int.TryParse(txt_newBonus.Value, out BonusPercent))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        WebReferenceSite.PricingPackageSupplierType ppst;
        if (!Enum.TryParse(ddl_SupplierType.SelectedValue, out ppst))
            ppst = WebReferenceSite.PricingPackageSupplierType.All;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.CreateNewPricingPackageRequest _request = new WebReferenceSite.CreateNewPricingPackageRequest();
        _request.Name = txt_NewName.Text;
        _request.FromAmount = FromAmount;
        _request.BonusPercent = BonusPercent;
        _request.SupplierType = ppst;
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.CreateNewPricingPackage(_request);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(_updatePanelTable, _updatePanelTable.GetType(), "ClosePopupNewPackage", "closePopup();", true);
        LoadPackages();
        SetGridView();
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView();
    }
    protected void lb_Remove_Click(object sender, EventArgs e)
    {
        SetPaymentImage(string.Empty);
    }
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    private DataView dataViewV
    {
        get { return (Session["dataView"] == null) ? null : (DataView)Session["dataView"]; }
        set { Session["dataView"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
    Dictionary<string, string> DicTranslateV
    {
        get { return (ViewState["DicTranslate"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["DicTranslate"]; }
        set { ViewState["DicTranslate"] = value; }
    }
    protected string GetDefaultType
    {
        get { return WebReferenceSite.PricingPackageSupplierType.All.ToString(); }
    }

   
}
