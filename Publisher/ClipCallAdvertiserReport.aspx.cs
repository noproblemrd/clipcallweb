﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class Publisher_ClipCallAdvertiserReport : PageSetting
{
    const string Num_Format = "{0:#,0.##}";
    const int ONE_DAY = 1;
    const string V_ICON = "~/Publisher/images/icon-v.png";
    const string X_ICON = "~/Publisher/images/icon-x.png";
    protected readonly string SessionTableName = "dataCCAR";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
            LoadSteps();
        }
        SetToolboxEvents();
        Header.DataBind();
    }

    private void LoadSteps()
    {
        ddl_Step.Items.Clear();
        foreach(WebReferenceReports.eStepInRegistration sir in Enum.GetValues(typeof(WebReferenceReports.eStepInRegistration)))
        {
            ListItem li = new ListItem(GetCleanStepText(sir), sir.ToString());
            li.Selected = sir == WebReferenceReports.eStepInRegistration.ALL;
            ddl_Step.Items.Add(li);
        }
    }
    private string GetCleanStepText(WebReferenceReports.eStepInRegistration sir)
    {
        return sir.ToString().Replace("_", " ");
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_ClipCallAdvertiserReport.Text);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.AdvertiserReportRequest _request = new WebReferenceReports.AdvertiserReportRequest();
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo;
        WebReferenceReports.eStepInRegistration sir;
        if (!Enum.TryParse(ddl_Step.SelectedValue, out sir))
            sir = WebReferenceReports.eStepInRegistration.ALL;
        _request.stepInRegistration = sir;
        WebReferenceReports.ResultOfListOfAdvertiserReportResponse result = null;
        try
        {
            result = reports.ClipCallAdvertiserReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        
        DataTable data = new DataTable();
        data.Columns.Add("date");
        data.Columns.Add("accountId");
        data.Columns.Add("name");
        data.Columns.Add("categories");
        data.Columns.Add("address");
        data.Columns.Add("stepInRegistration");
        data.Columns.Add("phone");
        data.Columns.Add("ReferralCode");
        data.Columns.Add("Device");
        data.Columns.Add("IsConfirmed", typeof(bool));
        data.Columns.Add("ConfirmedIcon");
        data.Columns.Add("LicenseNumber");
        data.Columns.Add("Description");
        data.Columns.Add("Email");
        data.Columns.Add("WebSite");
        data.Columns.Add("Radius");

        foreach (WebReferenceReports.AdvertiserReportResponse row in result.Value)
        {
            DataRow dr = data.NewRow();
            dr["date"] = GetDateTimeStringFormat(row.createdOn);
            dr["accountId"] = row.accountId;
            dr["name"] = row.name;
            dr["address"] = row.address;
            dr["stepInRegistration"] = GetCleanStepText(row.stepInRegistration);
            dr["phone"] = row.phone;
            dr["ReferralCode"] = row.ReferralCode;
            dr["Device"] = row.Device;
            if (row.ConfirmedAgreementPolicy.HasValue)
            {
                dr["IsConfirmed"] = true;
                dr["ConfirmedIcon"] = row.ConfirmedAgreementPolicy.Value ? V_ICON : X_ICON;
            }
            else
                dr["IsConfirmed"] = false;
            dr["LicenseNumber"] = row.LicenseNumber;
            dr["Description"] = row.Description;
            dr["Email"] = row.Email;
            dr["WebSite"] = row.WebSite;
            dr["Radius"] = row.Radius;

            StringBuilder categories = new StringBuilder();
            foreach (string str in row.categories)
                categories.Append(str + ",");
            if (categories.Length > 1)
                categories = categories.Remove(categories.Length - 1, 1);
            dr["categories"] = categories.ToString();
            data.Rows.Add(dr);
        }
        lbl_RecordMached.Text = result.Value.Length.ToString();
        _GridView.DataSource = data;
        _GridView.DataBind();
        dataV = data;

    }
    string GetDateTimeStringFormat(DateTime dt)
    {
        return string.Format(siteSetting.DateFormat, dt) + " " +
                    string.Format(siteSetting.TimeFormat, dt);
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
}