using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Web.Services.Protocols;
using System.Xml.Linq;


public partial class Publisher_PublisherUpsale : PageSetting, IPostBackEventHandler, IOnLoadControl
{
    const string MY_CLICK = "GetReport";
    const string AFTER_AUCTION = "AfterAuction";
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    protected string GetReport;
    ListItem[] HeadingListItems;
    #region IPostBackEventHandler Members
    
    public void RaisePostBackEvent(string eventArgument)
    {
        if (eventArgument == MY_CLICK)
            GetSearch();
        else if (eventArgument == AFTER_AUCTION)
            GetSearch();
    }

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1);
        ScriptManager1.RegisterAsyncPostBackControl(btn_ConfirmDelete);
        btn_CancelDelete.Attributes.Add("onclick", "javascript:ClosePopUpX('" + popUp_X.ClientID + "', '" + _mpeX.BehaviorID + "');");

        a_CloseDelete.Attributes.Add("onclick", "javascript:ClosePopUpX('" + popUp_X.ClientID + "', '" + _mpeX.BehaviorID + "');");
        GetReport = ClientScript.GetPostBackEventReference(this, MY_CLICK);

        TablePaging1._lnkPage_Click += new EventHandler(_Paging_click);
        TablePaging1.PagesInPaging = PAGE_PAGES;
        TablePaging1.ItemInPage = ITEM_PAGE;
        if (!IsPostBack)
        {


            LoadExperties();
            LoadResons();

            //          LoadNumOfRegionLevel();

            this.DataBind();
        }

        else
        {
            string event_argument = Request["__EVENTARGUMENT"];
            if (event_argument == AFTER_AUCTION)
                GetSearch();
        }

        Page.Header.DataBind();
    }


    public string GetPostBackEventAfterAuction
    {
        get
        {
            PostBackOptions pbo = new PostBackOptions(FromToDate1, AFTER_AUCTION, string.Empty, false, true, false, true, true, "");
            return Page.ClientScript.GetPostBackEventReference(pbo);
        }
    }
    private void LoadResons()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfTableRowData result = null;
        try
        {
            result = _site.GetTableData(WebReferenceSite.eTableType.UpsaleLoseReason);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (WebReferenceSite.TableRowData trd in result.Value)
        {
            if (!trd.Inactive)
                ddl_Resons.Items.Add(new ListItem(trd.Name, trd.Guid.ToString()));
        }

    }


    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        List<ListItem> list = new List<ListItem>();
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;// nodePrimary.Attributes["Name"].InnerText;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
            list.Add(new ListItem(pName, _id));
            
        }
        ddl_Heading.SelectedIndex = 0;
        HeadingListItems = list.ToArray();
       
    }


    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        GetSearch();
    }
    protected void _Paging_click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (UpsaleRequestV == null)
            return;
        Exec_Search(_pageNum, UpsaleRequestV);
    }
    void GetSearch()
    {
        _gv_upsale.DataSource = null;
        _gv_upsale.DataBind();
        _UpdatePanel.Update();
        DateTime to_date = FromToDate1.GetDateTo;
        DateTime from_date = FromToDate1.GetDateFrom;

        if (to_date == DateTime.MinValue || from_date == DateTime.MinValue)
        {
            to_date = DateTime.Now;
            from_date = to_date.AddDays(-7);
        }
        else
        {
            if (from_date > to_date)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + HttpUtility.JavaScriptStringEncode(lbl_dateFromBeforTo.Text) + "');", true);

                return;
            }

        }
        TablePaging1.Current_Page = 1;
        Guid ExpertiserId = new Guid(ddl_Heading.SelectedValue);
        WebReferenceReports.GetAllOpenUpsalesRequest _request = new WebReferenceReports.GetAllOpenUpsalesRequest();
        _request.ExpertiseId = ExpertiserId;
        _request.FromDate = from_date;
        _request.ToDate = to_date;
        _request.PageSize = ITEM_PAGE;
        //        UpsaleRequest ur = new UpsaleRequest(ExpertiserId, from_date, to_date);
        UpsaleRequestV = _request;
        Exec_Search(1, _request);

    }
    void Exec_Search(int page_index, WebReferenceReports.GetAllOpenUpsalesRequest _request)
    {
        WebReferenceReports.Reports _site = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfGetAllOpenUpsalesResponse result = null;

        _request.CurrentPage = page_index;
        try
        {
            result = _site.GetAllOpenUpsales(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        TablePaging1.Current_Page = result.Value.CurrentPage;
        TablePaging1.Pages_Count = result.Value.TotalPages;
        lbl_RecordMached.Text = result.Value.TotalRows + " " + lblRecordMached.Text;
        TablePaging1.LoadPages();
        DataTable data = new DataTable();
        data.Columns.Add("UpsaleId");
        data.Columns.Add("UpsaleNumber");

        data.Columns.Add("PrimaryExpertiseName");
        data.Columns.Add("PrimaryExpertiseCode");
        data.Columns.Add("ContactPhone");
        data.Columns.Add("SecondaryExpertise");
        data.Columns.Add("CreatedOn");

        data.Columns.Add("RequiredSuppliers");
        data.Columns.Add("ContactedSuppliers");
        data.Columns.Add("UpsaleDetailsScript");

        foreach (WebReferenceReports.UpsaleData node in result.Value.DataList)
        {
            DataRow row = data.NewRow();
            row["UpsaleId"] = node.UpsaleId.ToString();
            row["UpsaleNumber"] = node.UpsaleNumber;

            row["PrimaryExpertiseName"] = node.ExpertiseName;
            row["PrimaryExpertiseCode"] = node.ExpertiseCode;
            row["ContactPhone"] = node.ConsumerPhone;

            row["CreatedOn"] = string.Format(siteSetting.DateFormat, node.TimeToCall) + " " +
                string.Format(siteSetting.TimeFormat, node.TimeToCall);

            row["RequiredSuppliers"] = node.NumOfRequestedAdvertisers;
            row["ContactedSuppliers"] = node.NumOfConnectedAdvertisers;
            row["UpsaleDetailsScript"] = "return OpenIframeDetails('" + node.UpsaleId.ToString() + "');";
            data.Rows.Add(row);
        }

        if (data.Rows.Count == 0)
        {
            _gv_upsale.DataSource = null;
            _gv_upsale.DataBind();
            string script = "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Value) + "');";
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "No_Result"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "No_Result", script, true);
            _UpdatePanel.Update();
            return;
        }
        _gv_upsale.DataSource = data;
        _gv_upsale.DataBind();
        //        SetGridView();

        _UpdatePanel.Update();
    }
    protected void _gv_upsale_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow row = e.Row;
        if (row.RowType != DataControlRowType.DataRow)
            return;
        ImageButton ib = (ImageButton)row.FindControl("img_upsaleX");
        ib.Attributes.Add("onclick", "javascript:ConfirmDel('" + ib.CommandArgument + "');");
        string _phone = ((Label)row.FindControl("lbl_user_phone")).Text;
        ImageButton ib2 = (ImageButton)row.FindControl("img_upsaleV");
        string UpsaleId = ib.CommandArgument;//((Label)row.FindControl("lbl_IncidentId")).Text;
        string ExpertiseCode = ((Label)row.FindControl("lblExpertiseCode")).Text;

        string _script = _UpsaleControl.GetLoadDetailsScript(_phone, UpsaleId, ExpertiseCode, "") + " return false;";
        ib2.Attributes.Add("onclick", _script);
    }
    /*
    private void SetGridView()
    {
        foreach (GridViewRow row in _gv_upsale.Rows)
        {
            ImageButton ib = (ImageButton)row.FindControl("img_upsaleX");
            ib.Attributes.Add("onclick", "javascript:ConfirmDel('" + ib.CommandArgument + "');");
            string _phone = ((Label)row.FindControl("lbl_user_phone")).Text;
            ImageButton ib2 = (ImageButton)row.FindControl("img_upsaleV");
            string IncidentId = ((Label)row.FindControl("lbl_IncidentId")).Text;
            string ExpertiseCode = ((Label)row.FindControl("lblExpertiseCode")).Text;
            string _region = ((Label)row.FindControl("lblRegionName")).Text;
            string regionLevel = ((Label)row.FindControl("lblRegionLevel")).Text;
            ib2.Attributes.Add("onclick", "javascript:upsale('" + _phone + "', '" + IncidentId + "', '" +
                ExpertiseCode + "', '" + _region + "', '" + regionLevel + "');");

        }
    }
    */
    /*
    protected void img_upsaleV_click(object sender, EventArgs e)
    {
        //   GridViewRow row = (GridViewRow)(((ImageButton)sender).NamingContainer);
        //    string regionLevel = ((Label)row.FindControl("lblRegionLevel")).Text;
        AutoCompleteExtender4.ContextKey = "1";
        _UpdatePanelPopModal.Update();
    }

     * */
    protected void btn_ConfirmDelete_click(object sender, EventArgs e)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.Result result = null;
        WebReferenceSite.UpdateUpsaleRequest _request = new WebReferenceSite.UpdateUpsaleRequest();
        _request.UserId = userManagement.Get_Guid;
        Guid UpsaleId = new Guid(hf_UpsaleId.Value);
        _request.UpsaleId = UpsaleId;
        _request.Status = WebReferenceSite.UpsaleStatus.Lost;
        _request.StatusReasonId = new Guid(ddl_Resons.SelectedValue);
        _request.TimeToCall = DateTime.Now;


        try
        {
            result = _site.UpdateUpsale(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ConfirmDelete_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ConfirmDelete_Faild();
            return;
        }
        FromToDate1_ReportExec(null, EventArgs.Empty);
        string script = "ClosePopUpX('" + popUp_X.ClientID + "', '_modalX')";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePopUpX", script, true);
    }
    void ConfirmDelete_Faild()
    {
        string script = "ClosePopUpX('" + popUp_X.ClientID + "', '_modalX')";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePopUpX", script, true);
        _UpdatePanel.Update();
    }
    /*
    protected void lb_Upsale_click(object sender, EventArgs e)
    {
        string _id = ((LinkButton)sender).CommandArgument;
        _iframe.Attributes.Add("src", "UpsaleDetails.aspx?UpsaleId=" + _id);
        _UpdateIframe.Update();
        _mpeUpsaleDetails.Show();
    }
     * */
    /*
    SortedDictionary<string, string> DicCityV
    {
        get { return (Session["AutoCompleteList"] == null) ? null : (SortedDictionary<string, string>)Session["AutoCompleteList"]; }
        set { Session["AutoCompleteList"] = value; }
    }
    */
    /*
    int RegionLevelV
    {
        get { return (ViewState["RegionLevel"] == null) ? 1 : (int)ViewState["RegionLevel"]; }
        set { ViewState["RegionLevel"] = value; }
    }
    WebReferenceReports.GetAllOpenUpsalesRequest UpsaleRequestV
    {
        get { return (ViewState["UpsaleRequest"] == null) ? null : (WebReferenceReports.GetAllOpenUpsalesRequest)ViewState["UpsaleRequest"]; }
        set { ViewState["UpsaleRequest"] = value; }
    }
     * */
    int RegionLevelV
    {
        get { return (Session["RegionLevel"] == null) ? 1 : (int)Session["RegionLevel"]; }
        set { Session["RegionLevel"] = value; }
    }
    WebReferenceReports.GetAllOpenUpsalesRequest UpsaleRequestV
    {
        get { return (Session["UpsaleRequest"] == null) ? null : (WebReferenceReports.GetAllOpenUpsalesRequest)Session["UpsaleRequest"]; }
        set { Session["UpsaleRequest"] = value; }
    }







    #region IOnLoadControl Members

    public void __OnLoad(UserControl ControlSender)
    {
        if (ControlSender is Controls_Upsale_UpsaleControl)
        {
            _UpsaleControl.LoadDetails(HeadingListItems);
            _UpsaleControl.SetCompleteRequestScript("CloseUpsaleDetails();");
        }
    }

    #endregion
    protected string GetPathUpsaleDetails
    {
        get { return "UpsaleDetails.aspx?UpsaleId="; }
    }
}
