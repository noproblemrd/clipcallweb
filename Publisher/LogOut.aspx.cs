using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Publisher_LogOut : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //Clear cookie login
        if (Request.Cookies["PublishLogin"] != null)
            Response.Cookies["PublishLogin"].Values.Clear();
        if (Request.Cookies["rememberme"] != null)
            Response.Cookies["rememberme"].Values.Clear();
  //      string login = ResolveUrl("~") + "Publisher/PublisherLogin.aspx";
        PageSetting ps = (PageSetting)Page;        
        ps.ClearUserSite();
   //     Session.Abandon();

        
    }
}
