﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Publisher_RequestTicket : PublisherPage, IPostBackEventHandler, IOnLoadControl
{
    const string AFTER_AUCTION = "AfterAuction";
    const string CONFIGURATION_KEY = "MaxRegionLevel";
    const string CHARGED = "images/icon-v.png";
    const string NOT_CHARGED = "images/icon-x.png";
    const string PathTicketRequest = "RequestTicket.aspx?RequestTicket=";
    const string JS_VOID = "javascript:void(0);";
    protected Guid RequestIdForUpsale { get; set; }
    private string full_name;
    private string email;
    protected void Page_Load(object sender, EventArgs e)
    {

        ScriptManager1.RegisterAsyncPostBackControl(ddl_Billing);
        ScriptManager1.RegisterAsyncPostBackControl(btn_Set);
        ScriptManager1.RegisterAsyncPostBackControl(lb_AffiliatePayStatus);
        ScriptManager1.RegisterAsyncPostBackControl(lb_StopRequest);
        Notes1.SetNoteTab += new EventHandler(Notes1_SetNoteTab);
        
        if (!IsPostBack)
        {
 //           if (!userManagement.IsPublisher())
 //               Response.Redirect("LogOut.aspx");
            string RequestTicket = Request.QueryString["RequestTicket"];
            Guid _ticket;
            if (!Guid.TryParse(RequestTicket, out _ticket))
            {
                ClosePopupByError();
                return;
            }
            // = new Guid(RequestTicket);
            TicketIdV = _ticket;
      //      LoadNumOfRegionLevel();
    //        LoadExperties();
            LoadPayStatusAndReasons();
            this.DataBind();
            LoadDetails(_ticket);
        }
        Header.DataBind();
    }

    private void LoadPayStatusAndReasons()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetAllPayStatusAndReasons();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Dictionary<Guid, Dictionary<Guid, string>> dic = new Dictionary<Guid, Dictionary<Guid, string>>();
        foreach (WebReferenceSite.GuidStringPair[] gsps in result.Value)
        {
            Guid _id = Guid.Empty;
            for (int i = 0; i < gsps.Length; i++)
            {
                if (i == 0)
                {
                    dic.Add(gsps[i].Id, new Dictionary<Guid, string>());
                    ddl_Billing.Items.Add(new ListItem(gsps[i].Name, gsps[i].Id.ToString()));
                    _id = gsps[i].Id;
                }
                else
                {
                    dic[_id].Add(gsps[i].Id, gsps[i].Name);
                }
            }
        }
        DicAffiliateV = dic;
    }
   
    void Notes1_SetNoteTab(object sender, EventArgs e)
    {
        SetNoteTab();
    }
    /*
    protected void _Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item)
            return;
        HyperLink hl = (HyperLink)e.Item.FindControl("HL_Request");
        if (hl.NavigateUrl == string.Empty)
            hl.CssClass = "_blunk";
    }
    */
    private void LoadDetails(Guid RequestTicket)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfCaseData _data = null;
        try
        {
            _data = _site.GetCaseData(RequestTicket);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClosePopupByError();
            return;
        }
        if (_data.Type == WebReferenceSite.eResultType.Failure)
        {
            ClosePopupByError();
            return;
        }
       
        WebReferenceSite.CaseData data = _data.Value;
   //     _Repeater.HeaderTemplate.
        div_upsale.Visible = data.ShowUpsaleButton;
        lbl_RequestNum.Text = data.TicketNumber;
        if (data.ParentRequestId != Guid.Empty)
        {
            RequestIdForUpsale = data.ParentRequestId;
            lbl_RequestParentNum.Text = data.ParentRequestTicket;
            HL_RequestParent.NavigateUrl = PathTicketRequest + data.ParentRequestId.ToString();
            div_ParentTicket.Visible = true;
        }
        else
        {
            div_ParentTicket.Visible = false;
            RequestIdForUpsale = RequestTicket;
        }
        DataTable dt = new DataTable();
        dt.Columns.Add("url");
        dt.Columns.Add("num");
        dt.Columns.Add("class");
        foreach (WebReferenceSite.GuidStringPair gsp in data.ConnectedRequests)
        {            
            DataRow row = dt.NewRow();
            row["num"] = gsp.Name;
            row["url"] = (gsp.Id == Guid.Empty) ? "" : PathTicketRequest + gsp.Id.ToString();
            row["class"] = (gsp.Id == Guid.Empty) ? "_blunk" : string.Empty;
            dt.Rows.Add(row);
        }
        _Repeater.DataSource = dt;
        _Repeater.DataBind();
        txt_Title.Text = data.Title;
        txt_Consumer.Text = data.CostumerPhone + @"/ " + data.ConsumerName;
        txt_CreatedOn.Text = data.CreatedOn.ToString();
        txt_Description.Text = data.Description;
        txt_Heading.Text = data.HeadingName;
        txt_Origin.Text = data.OriginName;
        txt_ProvidedAdvertisers.Text = data.ProvidedAdvertisers + "";
        txt_Region.Text = data.RegionName;
        txt_Keyword.Text = data.Keyword;

        full_name = data.ConsumerName;
        email = data.ConsumerEmail;
       
        
        txt_RequestedAdveritsers.Text = data.RequestedAdvertisers + "";
        txt_Revenue.Text = string.Format("{0:0.##}", data.Revenue);
        txt_Url.Text = data.ExposureUrl;
        txt_Control.Text = data.ExposureControlName;

        txt_Type.Text = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "CaseTypePure", data.Type.ToString());
        txt_WinningPrice.Text = string.Format("{0:0.##}", data.WinningPrice);
        if (data.IsStoppedManually)
        {
            div_Stop.Visible = false;
            div_requestStpped.Visible = true;
        }
        else if (data.CanBeStopped)
        {
            div_Stop.Visible = true;
            div_requestStpped.Visible = false;
        }
        else
        {
            div_Stop.Visible = false;
            div_requestStpped.Visible = false;
        }

        DataTable data_table = new DataTable();
        data_table.Columns.Add("SupplierName");
        data_table.Columns.Add("Status");
        data_table.Columns.Add("PredefinedPrice");
        data_table.Columns.Add("Charged");
        data_table.Columns.Add("ActualPrice");
        data_table.Columns.Add("CreatedOn");
        data_table.Columns.Add("Duration");
        data_table.Columns.Add("AarCandidateStatus");
        data_table.Columns.Add("IsAnsweringMachine");
        data_table.Columns.Add("DtmfPressed");
        data_table.Columns.Add("IvrFlavor");
        data_table.Columns.Add("IsReject");
        data_table.Columns.Add("HasRecord", typeof(bool));
        data_table.Columns.Add("Record");
        data_table.Columns.Add("RejectReason");
        data_table.Columns.Add("BillingRate");
        data_table.Columns.Add("QualityRate");
        data_table.Columns.Add("LeadBuyerName");
        Dictionary<string, string> dic = new Dictionary<string, string>();
   //     Dictionary<int, string> records = new Dictionary<int, string>();
        foreach (string str in Enum.GetNames(typeof(eYesNo)))
        {
            dic.Add(str, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", str));
        }
        
        foreach (WebReferenceSite.CallData cd in data.Calls)
        {
            DataRow row = data_table.NewRow();
            row["SupplierName"] = cd.SupplierName;
            row["Status"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "EndStatus", cd.Status.ToString());
            row["PredefinedPrice"] = siteSetting.CurrencySymbol + string.Format("{0:0.##}", cd.PredefinedPrice);
            row["Charged"] = (cd.IsCharged) ? CHARGED : NOT_CHARGED;
            row["ActualPrice"] = siteSetting.CurrencySymbol + string.Format("{0:0.##}", cd.ActualPrice);

            row["CreatedOn"] = string.Format(siteSetting.DateFormat, cd.CreatedOn) + " " + string.Format(siteSetting.TimeFormat, cd.CreatedOn);
            row["Duration"] = (cd.Duration == null) ? "" : cd.Duration;
            row["AarCandidateStatus"] = cd.AarCandidateStatus;
            row["IsAnsweringMachine"] = (cd.IsAnsweringMachine == null) ? "" : ((cd.IsAnsweringMachine == true) ? dic[eYesNo.Yes.ToString()] : dic[eYesNo.No.ToString()]);
            row["DtmfPressed"] = cd.DtmfPressed;
            row["IvrFlavor"] = cd.IvrFlavor;
            row["IsReject"] = (cd.IsRejected) ? dic[eYesNo.Yes.ToString()] : dic[eYesNo.No.ToString()];
            row["RejectReason"] = cd.RejectReason;
            row["BillingRate"] = string.Format("{0:0.####}", cd.BillingRate);
            row["QualityRate"] = string.Format("{0:0.####}", cd.QualityRate);
            row["LeadBuyerName"] = cd.LeadBuyerName;
            string _path = cd.Recording;
            if (!string.IsNullOrEmpty(_path))
            {
                row["HasRecord"] = true;
                /*
                int _index;
                do
                {
                    _index = new Random().Next();
                }
                while (records.ContainsKey(_index));
                records.Add(_index, _path);
                 */
                string RecEnc = EncryptString.EncodeRecordPath(_path);
                row["Record"] = "javascript:OpenRecord('" + RecEnc + "');";
            }
            else
                row["HasRecord"] = false;
           
            

            data_table.Rows.Add(row);
            
        }
        if(data.ShowUpsaleButton)
        {
            
            string _script = _UpsaleControl.GetLoadDetailsScript(data.CostumerPhone, data.UpsaleId.ToString(), data.HeadingCode, data.RegionName, 
                data.RegionLevel, data.Description);
            _script += "return false;";
            img_upsaleV.OnClientClick = _script;
            UpsaleIdV = data.UpsaleId;
        }
    //    RecordListV = records;
        PayReasonIdV = data.PayReasonId;
        PayStatusIdV = data.PayStatusId;
        // Load affiliate tab
        LoadAffiliateTab(data.PayStatusId, data.PayReasonId);
       
        _GridView.DataSource = data_table;
        _GridView.DataBind();

    }
    protected void lb_StopRequest_Click(object sender, EventArgs e)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.Result result = null;
        try
        {
            WebReferenceSite.StopRequestManuallyRequest request = new WebReferenceSite.StopRequestManuallyRequest();
            request.CaseId = TicketIdV;
            request.UserId = userManagement.Get_Guid;
            result = _site.StopRequestManually(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        Update_Success();
        div_Stop.Visible = false;
        div_requestStpped.Visible = true;
        _UpdatePanel_RequestStop.Update();
    }
    void LoadAffiliateTab(Guid PayStatusId, Guid PayReasonId)
    {
        ddl_Billing.SelectedIndex = -1;
        ddl_Status.Items.Clear();
        for (int i = 0; i < ddl_Billing.Items.Count; i++)
        {
            if (ddl_Billing.Items[i].Value == PayStatusId.ToString())
            {
                ddl_Billing.SelectedIndex = i;
                break;
            }
        }
        Dictionary<Guid, Dictionary<Guid, string>> dic = DicAffiliateV;
        foreach (KeyValuePair<Guid, string> kvp in dic[PayStatusId])
        {
            ListItem li = new ListItem(kvp.Value, kvp.Key.ToString());
            li.Selected = (kvp.Key == PayReasonId);
            ddl_Status.Items.Add(li);
        }
    }
    protected void lb_OpenNotes_Click(object sender, EventArgs e)
    {
        SetNoteTab();
        Notes1.LoadData(TicketIdV, WebReferenceSite.NoteType.Case);
    }
    protected void lb_AffiliatePayStatus_click(object sender, EventArgs e)
    {
        LoadAffiliateTab(PayStatusIdV, PayReasonIdV);
        _UpdatePanelaffiliate.Update();
    }
    void SetNoteTab()
    {
        div_Notes.Style[HtmlTextWriterStyle.Display] = "block";
        div_Details.Style[HtmlTextWriterStyle.Display] = "none";
        a_OpenDetails.Attributes.Remove("class");
        lb_OpenNotes.CssClass = "selected";
    }
    void ClosePopupByError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseError", "CloseMeError();", true);
    }
   
    void SetDetailsTab()
    {
        div_Notes.Style[HtmlTextWriterStyle.Display] = "none";
        div_Details.Style[HtmlTextWriterStyle.Display] = "block";
        a_OpenDetails.Attributes["class"] = "selected";
        lb_OpenNotes.CssClass = "";
    }
    protected void ddl_Billing_SelectedIndexChanged(object sender, EventArgs e)
    {
        string _id = ddl_Billing.SelectedValue;
        ddl_Status.Items.Clear();
        foreach (KeyValuePair<Guid, string> kvp in DicAffiliateV[new Guid(ddl_Billing.SelectedValue)])
        {
            ddl_Status.Items.Add(new ListItem(kvp.Value, kvp.Key.ToString()));
        }
        _UpdatePanelaffiliate.Update();
    }
    protected void btn_Set_Click(object sender, EventArgs e)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.UpdateAffiliatePayStatusRequest _request = new WebReferenceSite.UpdateAffiliatePayStatusRequest();
        _request.CaseId = TicketIdV;
        _request.PayStatusId = new Guid(ddl_Billing.SelectedValue);
        _request.PayReasonId = new Guid(ddl_Status.SelectedValue);
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.UpdateAffiliatePayStatus(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        PayStatusIdV = _request.PayStatusId;
        PayReasonIdV = _request.PayReasonId;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
    }
    Guid TicketIdV
    {
        get { return (string.IsNullOrEmpty(hf_IncidentId.Value)) ? Guid.Empty : new Guid(hf_IncidentId.Value); }
        set { hf_IncidentId.Value = value.ToString(); }
    }
    Guid UpsaleIdV
    {
        get { return (string.IsNullOrEmpty(hf_UpsaleId.Value)) ? Guid.Empty : new Guid(hf_UpsaleId.Value); }
        set { hf_UpsaleId.Value = value.ToString(); }
    }
    protected string GetNoResulScript
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text); }
    }


    #region IPostBackEventHandler Members
    public string GetPostBackEventAfterAuction
    {
        get
        {
            PostBackOptions pbo = new PostBackOptions(this, AFTER_AUCTION, string.Empty, false, true, false, true, true, "");
            return Page.ClientScript.GetPostBackEventReference(pbo);
        }
    }
    public void RaisePostBackEvent(string eventArgument)
    {

        if (eventArgument == AFTER_AUCTION)
        {
            div_upsale.Visible = false;
        }
    
    }

    #endregion
    Dictionary<Guid, Dictionary<Guid, string>> DicAffiliateV
    {
        get { return (Session["AffiliatePayment"] == null) ? new Dictionary<Guid, Dictionary<Guid, string>>() : (Dictionary<Guid, Dictionary<Guid, string>>)Session["AffiliatePayment"]; }
        set { Session["AffiliatePayment"] = value; }
    }

  //  data.PayStatusId, data.PayReasonId
    Guid PayStatusIdV
    {
        get { return (ViewState["PayStatusId"] == null) ? Guid.Empty : (Guid)ViewState["PayStatusId"]; }
        set { ViewState["PayStatusId"] = value; }
    }
    Guid PayReasonIdV
    {
        get { return (ViewState["PayReasonId"] == null) ? Guid.Empty : (Guid)ViewState["PayReasonId"]; }
        set { ViewState["PayReasonId"] = value; }
    }
    protected string AreUSure
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_AreUSure.Text); }
    }

    #region IOnLoadControl Members

    public void __OnLoad(UserControl ControlSender)
    {
        if (ControlSender is Controls_Upsale_UpsaleControl)
        {

            _UpsaleControl.LoadDetails(RequestIdForUpsale, full_name, email);
        }
    }

    #endregion
    /*
    Dictionary<int, string> RecordListV
    {
        get { return (Session["RecordList"] == null) ? new Dictionary<int, string>() : (Dictionary<int, string>)Session["RecordList"]; }
        set { Session["RecordList"] = value; }
    }
     * */
    protected string RecordService
    {
        get { return ResolveUrl("~/RecordService.asmx/GetPath"); }
    }
    protected string GetAudioHTML5
    {
        get { return ResolveUrl("~") + "audio/AudioHtml5.aspx"; }
    }
    protected string GetMediaPlayer
    {
        get { return ResolveUrl("~") + "audio/MediaPlayer.aspx"; }
    }
    
    
}
