﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SurveyReport.aspx.cs" Inherits="Publisher_SurveyReport" %>

<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
    <script type="text/javascript">
        function _CreateExcel() {
            var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"AarCallCenterReport"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
        function OpenIframe(_path)
        {             
            var _leadWin = window.open(_path, "trace", "width=1200, height=650,scrollbars=1");
            _leadWin.focus();
        }
        function SetMessages(_custAnswers, _proAnswers) {
            proAnswers = _proAnswers;
            custAnswers = _custAnswers;
           
            $ddl_messages = $('#ddl_answer');
            SetDdlMessagesByDestination(document.querySelector('#<%# ddl_UserMode.ClientID %>'));
        }
        var proAnswers = [];
        var custAnswers = [];
     //   var allAnswers = [];
        var $ddl_messages;
        $(function () {
            var $hf = $('#<%# hf_UserModeSelected.ClientID %>');
            ddl_answer = document.querySelector('#ddl_answer');
            for(var i = 0; i < ddl_answer.options.length; i++){
                if (ddl_answer.options[i].value == $hf.val()) {
                    ddl_answer.options[i].selected = true;
                    break;
                }
            }
          //  $hf.val(document.querySelector('#ddl_answer').value);
            $('#<%# ddl_UserMode.ClientID %>').change(function () {
                SetDdlMessagesByDestination(this);
            });
            $('#ddl_answer').change(function () {
                $hf.val(this.value);
            });
        });
        function SetDdlMessagesByDestination(ddl) {
            var list = [];
            if (ddl.value == '<%# ClipCallReport.eUserMode.All.ToString() %>') {
                list.push(custAnswers);
                list.push(proAnswers)
            }
            else if (ddl.value == '<%# ClipCallReport.eUserMode.Customer.ToString() %>') {
                list.push(custAnswers);
            }
            else if (ddl.value == '<%# ClipCallReport.eUserMode.Supplier.ToString() %>')
                list.push(proAnswers);
            SetDdlMessages(list);
        }
        function SetDdlMessages(list) {

            $ddl_messages.find('option').remove();
            $ddl_messages.append($("<option></option>")
                    .attr("value", null)
                    .text(''));
            list.forEach(function (list_elements) {
                list_elements.forEach(function (element) {
                    $ddl_messages.append($("<option></option>")
                    .attr("value", element.SurveyProjectId)
                    .text(element.Answer));
                });                
            });
            $('#<%# hf_UserModeSelected.ClientID %>').val('');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  
    <div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">
             <div class="form-field" runat="server" id="div_Step">
                    <asp:Label ID="lbl_UserMode" runat="server" Text="User mode" CssClass="label"></asp:Label>
                    <asp:DropDownList ID="ddl_UserMode" CssClass="form-select" runat="server" ></asp:DropDownList>
                 <asp:HiddenField ID="hf_UserModeSelected" runat="server" />
                </div>
              
            <div class="form-field" runat="server" id="div1">
                <asp:Label ID="lbl_status" runat="server" Text="Survey status" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_status" CssClass="form-select" runat="server" ></asp:DropDownList>

            </div>
             <div class="form-field" runat="server" id="div3">
                <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Project Id</label>
			        <asp:TextBox ID="txt_ProjectId" CssClass="form-text" runat="server" ></asp:TextBox>										
                    <asp:CompareValidator runat="server" Type="Integer"  Operator="DataTypeCheck" ControlToValidate="txt_ProjectId" Display="Dynamic" ValidationGroup="survey">
                        <asp:Label ID="lbl_RequestIdError" runat="server" Text="Must be number" Visible="true" ></asp:Label>
                    </asp:CompareValidator> 
            </div>
            <div class="form-field" runat="server" id="div2">
                <asp:Label ID="Label11" runat="server" Text="Answer" CssClass="label"></asp:Label>
                <select id="ddl_answer" class="form-select" style="width:180px;">
                        
                    </select>
            </div>
             <div class="clear"></div>
              <div class="form-field" runat="server" id="div_IncludeQa" style="margin-bottom:0; position: absolute; left: 510px;">
                <asp:Label ID="lbl_IncludeQaTest" runat="server" Text="Include QA" CssClass="label"></asp:Label>
                <asp:CheckBox ID="cb_IncludeQaTest" runat="server" />

            </div>
        </div>
        <div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click" ValidationGroup="survey"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>        
        <div id="Table_Report"  runat="server" >  
            <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
                     <asp:TemplateField SortExpression="CreatedOn">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="CreatedOn"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>                            						
			                <asp:Label ID="Label101" runat="server" Text='<%# Bind("CreatedOn") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="ProjectId">
						<HeaderTemplate>
							<asp:Label ID="Label2" runat="server" Text="ProjectId"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<a id="a_Interactions" runat="server" href='<%# Bind("Trace") %>'>
			                    <asp:Label ID="Label102" runat="server" Text='<%# Bind("ProjectId") %>'></asp:Label>
						    </a>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="UserMode">
						<HeaderTemplate>
							<asp:Label ID="Label3" runat="server" Text="User Mode"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label103" runat="server" Text='<%# Bind("UserMode") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="UserName">
						<HeaderTemplate>
							<asp:Label ID="Label20" runat="server" Text="User Name"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label120" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="UserPhone">
						<HeaderTemplate>
							<asp:Label ID="Label21" runat="server" Text="User Phone"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label121" runat="server" Text='<%# Bind("UserPhone") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="SurveyType">
						<HeaderTemplate>
							<asp:Label ID="Label4" runat="server" Text="Survey Type"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label104" runat="server" Text='<%# Bind("SurveyType") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="StatusReason">
						<HeaderTemplate>
							<asp:Label ID="Label5" runat="server" Text="StatusReason"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label105" runat="server" Text='<%# Bind("StatusReason") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="RespondedDate">
						<HeaderTemplate>
							<asp:Label ID="Label6" runat="server" Text="Responded Date"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label106" runat="server" Text='<%# Bind("RespondedDate") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="RespondedQuestion">
						<HeaderTemplate>
							<asp:Label ID="Label7" runat="server" Text="Responded answer"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label107" runat="server" Text='<%# Bind("RespondedQuestion") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="TAD">
						<HeaderTemplate>
							<asp:Label ID="Label701" runat="server" Text="TAD question"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label702" runat="server" Text='<%# Bind("TAD") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="SupplierName">
						<HeaderTemplate>
							<asp:Label ID="Label8" runat="server" Text="Done With name"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label108" runat="server" Text='<%# Bind("SupplierName") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="SupplierPhone">
						<HeaderTemplate>
							<asp:Label ID="Label9" runat="server" Text="Done With supplier phone"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>							
			                <asp:Label ID="Label109" runat="server" Text='<%# Bind("SupplierPhone") %>'></asp:Label>						    
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="Cancel">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="Cancel"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>	
                            <asp:LinkButton ID="lb_cancel" runat="server" CommandName="Cancel" CommandArgument='<%# Bind("Cancel") %>' onclick="lb_Cancel_Click" Visible='<%# Bind("DisplayCancel") %>'>Cancel</asp:LinkButton>
                            				    
						</ItemTemplate>
					</asp:TemplateField>
                    
                </Columns>
            </asp:GridView>
        </div>
    </div>
    </div>
</asp:Content>

