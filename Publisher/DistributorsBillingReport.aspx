﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="DistributorsBillingReport.aspx.cs" Inherits="Publisher_DistributorsBillingReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        window.onload = appl_init;

        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);

        }
        function BeginHandler() {
            showDiv();
        }
        function EndHandler() {
            hideDiv();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
</cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
    <div id="form-analytics" >
         <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	                
            <div id="div_Origin" class="form-field" runat="server">
                <asp:Label ID="lbl_Origin" CssClass="label" runat="server" Text="Origin"></asp:Label>                                    
                <asp:DropDownList ID="ddl_Origins" CssClass="form-select" runat="server" ></asp:DropDownList>		
            </div>

            <div class="form-field">
                <asp:Label ID="lbl_Years" CssClass="label" runat="server" Text="Year"></asp:Label>                                    
                <asp:DropDownList ID="ddl_Years" CssClass="form-select" runat="server"></asp:DropDownList>	
            </div> 

            <div class="form-field">
                <asp:Label ID="lbl_Month" CssClass="label" runat="server" Text="Month"></asp:Label>                                    
                <asp:DropDownList ID="ddl_Month" CssClass="form-select" runat="server"></asp:DropDownList>	
            </div>    
        </div>
        <div style="clear:both;"></div>
        <div style="text-align: center;">
            <asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
                onclick="lb_RunReport_Click"></asp:LinkButton>
        </div>
        <div style="clear:both;"></div>
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	    <ContentTemplate>	
            <div class="results" runat="server" id="div_NumRequests" visible="false">                 
                <asp:Label ID="lbl_NumRequests" runat="server"></asp:Label>
                <asp:Label ID="lblNumRequest" runat="server" Text="Requests"></asp:Label>
            </div>
            <div class="results" runat="server" id="div_TotalPayment" visible="false">
                <asp:Label ID="lbl_TotalPaymentAmount" runat="server" Text="Total payment amount:"></asp:Label>        
                <asp:Label ID="lblTotalPaymentAmount" runat="server" Text=""></asp:Label>
            </div>
	        <div id="Table_Report"  runat="server" >  
                <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
                <RowStyle CssClass="even" />
                <AlternatingRowStyle CssClass="odd" />
                <Columns>
                    <asp:TemplateField SortExpression="Date">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBroker" runat="server" Text="<%# Bind('Date') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="Region">
                        <HeaderTemplate>
                            <asp:Label ID="Label10" runat="server" Text="<%# lbl_Region.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label11" runat="server" Text="<%# Bind('Region') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="Heading">
                        <HeaderTemplate>
                            <asp:Label ID="Label12" runat="server" Text="<%# lbl_ServiceCategory.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label13" runat="server" Text="<%# Bind('Heading') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="Consumer">
                        <HeaderTemplate>
                            <asp:Label ID="Label14" runat="server" Text="<%# lbl_Consumer.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label15" runat="server" Text="<%# Bind('Consumer') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="Payment">
                        <HeaderTemplate>
                            <asp:Label ID="Label16" runat="server" Text="<%# lbl_Payment.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label17" runat="server" Text="<%# Bind('Payment') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="PaymentReason">
                        <HeaderTemplate>
                            <asp:Label ID="Label18" runat="server" Text="<%# lbl_PaymentReason.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label19" runat="server" Text="<%# Bind('PaymentReason') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                     
                </Columns>    
                </asp:GridView>
                <asp:GridView ID="_GridViewAll" runat="server" CssClass="data-table" AutoGenerateColumns="false">
                <RowStyle CssClass="even" />
                <AlternatingRowStyle CssClass="odd" />
                <Columns>
                     <asp:TemplateField SortExpression="OriginName">
                        <HeaderTemplate>
                            <asp:Label ID="Label112" runat="server" Text="<%# lbl_OriginName.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label113" runat="server" Text="<%# Bind('OriginName') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="PaymentAmount">
                        <HeaderTemplate>
                            <asp:Label ID="Label114" runat="server" Text="<%# lbl_PaymentAmount.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label115" runat="server" Text="<%# Bind('PaymentAmount') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="RequestCount">
                        <HeaderTemplate>
                            <asp:Label ID="Label116" runat="server" Text="<%# lbl_RequestCount.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label117" runat="server" Text="<%# Bind('RequestCount') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="RevenueShare">
                        <HeaderTemplate>
                            <asp:Label ID="Label128" runat="server" Text="<%# lbl_RevenueShare.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label129" runat="server" Text="<%# Bind('RevenueShare') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                </asp:GridView>
                <uc1:TablePaging ID="TablePaging1" runat="server" />
            </div>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<asp:Label ID="lbl_DistributorsBillingReport" runat="server" Text="Distributors Billing Report" Visible="false"></asp:Label>

<asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Region" runat="server" Text="Region" Visible="false"></asp:Label>
<asp:Label ID="lbl_ServiceCategory" runat="server" Text="Service category" Visible="false"></asp:Label>
<asp:Label ID="lbl_Consumer" runat="server" Text="Consumer" Visible="false"></asp:Label>
<asp:Label ID="lbl_Payment" runat="server" Text="Payment" Visible="false"></asp:Label>
<asp:Label ID="lbl_PaymentReason" runat="server" Text="Payment reason" Visible="false"></asp:Label>

<asp:Label ID="lbl_OriginName" runat="server" Text="Origin name" Visible="false"></asp:Label>
<asp:Label ID="lbl_PaymentAmount" runat="server" Text="Payment amount" Visible="false"></asp:Label>
<asp:Label ID="lbl_RequestCount" runat="server" Text="Request count" Visible="false"></asp:Label>
<asp:Label ID="lbl_RevenueShare" runat="server" Text="Revenue share" Visible="false"></asp:Label>
 
</asp:Content>


