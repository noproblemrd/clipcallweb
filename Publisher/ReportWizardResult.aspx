﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ReportWizardResult.aspx.cs" Inherits="Publisher_ReportWizardResult" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="~/Controls/Toolbox/ToolboxWizardReport.ascx" tagname="Toolbox" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
<link media="screen" rel="stylesheet" href="../jquery/colorbox.css" />
<script src="../jquery/jquery.min.js" type="text/javascript" ></script>
<script src="../jquery/jquery.colorbox.js" type="text/javascript" ></script>	
<script type="text/javascript" >
    var message;
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
        message = "<%# lbl_UnsaveData.Text %>";
    }
    function BeginHandler()
    { 
        showDiv();
    }
    function EndHandler()
    {  
        hideDiv();
    }   
    function OpenSaveReport()
    {
        document.getElementById("<%# div_save.ClientID %>").className="popModal_del";
        $find('_modal').show();
    }
    function ClearValidators()
    {
        var validators = Page_Validators;
        
        for (var i = 0;i < Page_Validators.length;  i++)
        {      
			Page_Validators[i].style.visibility = "hidden";	
	    } 	       
    }
    function CloseSave()
    {
        document.getElementById("<%# txt_reportName.ClientID %>").value="";
        document.getElementById("<%# txt_description.ClientID %>").value="";
        ClearValidators();
        $find('_modal').hide();
        document.getElementById("<%# div_save.ClientID %>").className="popModal_del popModal_del_hide";
    }
    function _redirect(reportId)
    {
        window.location = "ReportWizard.aspx?ReportId="+reportId;
    }
    function ReportSaved()
    {
        alert("<%# lbl_CreateSuccessfully.Text %>");
        window.location = "SavedReport.aspx";
    }
    function checkValidations()
    {
        if(!Page_ClientValidate('save_report'))
        {
            alert("<%# lbl_NameAndDescription.Text %>");
            return false;
        }
        return true;
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
 <uc1:Toolbox ID="Toolbox1" runat="server" />
<div class="page-content minisite-content2">
     
	<div id="form-analytics">			
	    <center>
            <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate> 
            
                <div class="results2"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                <div class="table2">         
                <asp:GridView ID="_GridView" runat="server" AllowPaging="True"  
                    CssClass="data-table" onpageindexchanging="_GridView_PageIndexChanging" 
                    PageSize="20" RowStyle-Width="660" onrowdatabound="_GridView_RowDataBound">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                
                <FooterStyle CssClass="footer"  />
                <PagerStyle CssClass="pager" />
                </asp:GridView>
                </div> 
            </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </div>
</div>

<div id="div_save" runat="server" class="popModal_del popModal_del_hide">
    <div class="top"></div>
        <a id="a_CloseDelete" runat="server" class="span_A" href="javascript:CloseSave();"></a>
        <div class="content">
            <h2><asp:Label ID="lbl_titleSave" runat="server" Text="Save"></asp:Label></h2>
            <table>
            <tr>
                <td>                    
                    <asp:Label ID="lbl_reportName"  runat="server" Text="Report name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_reportName" CssClass="form-textsave" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_reportName" runat="server"
                     ErrorMessage="Please provide the report name" ControlToValidate="txt_reportName" CssClass="error-msg"
                      ValidationGroup="save_report"></asp:RequiredFieldValidator>
                </td>               
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_description" runat="server" Text="Description"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_description" runat="server" CssClass="form-textsave" Rows="7" Columns="17" TextMode="MultiLine"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_description" runat="server"
                     ErrorMessage="Please provide the report description" ControlToValidate="txt_description" CssClass="error-msg"
                      ValidationGroup="save_report"></asp:RequiredFieldValidator>
                </td>
            </tr>
            </table>
            <div class="buttons_div">
            <span id="span_initiateCalls" >
                <asp:Button ID="btn_Save" runat="server" Text="Save report" validationgroup="save_report" CssClass="btn"
                 OnClick="btn_Save_click" OnClientClick="javascript:checkValidations();"/>          
                
            </span>
            <input id="btn_Closed" type="button" class="btn" value="Close" runat="server" onclick="javascript:CloseSave();" /> 
            </div>
   
   

 <cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btn_virtual"
    PopupControlID="div_save"
    BackgroundCssClass="modalBackground" 
    BehaviorID="_modal"                
    DropShadow="false"
    ></cc1:ModalPopupExtender>

<div style="display:none;">
    <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />    
</div>
     </div>
     
</div>
    <asp:Label ID="lbl_CreateSuccessfully" runat="server" Text="The report was successfully created" Visible="false"></asp:Label>
    <asp:Label ID="lbl_NameAndDescription" runat="server" Text="please provide the report name and report description" Visible="false"></asp:Label>
    <asp:Label ID="lbl_UnsaveData" runat="server" Text="Are you sure you want to exit? Unsaved data will be lost!" Visible="false" />

    

</asp:Content>

