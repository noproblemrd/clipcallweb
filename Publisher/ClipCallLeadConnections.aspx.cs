﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallLeadConnections : PublisherPage
{
    const string DATE_TIME_FORMAT = "{0:MM/dd/yyyy HH:mm}";
    const string QUOTE_TEXT = "Quote{0} (${1})";
    const string PROXIMITY_WITH_DISTANCE_TEXT = "Proximity Sensor ({0}m)";
    const string PROXIMITY_NONE_DISTANCE_TEXT = "Proximity Sensor";
    const string FOUL_TOTAL_TEXT = "{0} total.";
    const string FOUL_THIS_PROJECT_TEXT = "{0} in this project.";
    const string NONE_FOUL_TEXT = "No fouls";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //           if (!userManagement.IsPublisher())
            //               Response.Redirect("LogOut.aspx");
            string incident_id = Request.QueryString["incidentid"];
            Guid incidentId;
            if (!Guid.TryParse(incident_id, out incidentId))
            {
                ClosePopupByError();
                return;
            }
          
        //    IncidentIdV = incidentId;

            //     this.DataBind();
            LoadDetails(incidentId);
        }
        Header.DataBind();
    }
    private void LoadDetails(Guid incidentId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfIncidentInteractionResponse result = null;
        try
        {
            result = report.GetIncidentInteraction(incidentId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClosePopupByError();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            ClosePopupByError();
            return;
        }

        txt_CustomerName.Text = result.Value.CustomerName;
        txt_CustomerPhone.Text = result.Value.CustomerPhone;
        lbl_LeadNum.Text = result.Value.CaseNumber;
        txt_fullAddress.Text = result.Value.FullAddress;
        txt_hasPush.Text = result.Value.HasNotification ? "Yes" : "No";
        a_projectNumber.HRef = "javascript:OpenIframe('MobileAppRequestTicket.aspx?incidentid=" + incidentId + "');";

        /*   Table Fouls */
        DataTable foulsTable = new DataTable();
        foulsTable.Columns.Add("AdvertiserName");
        foulsTable.Columns.Add("SupplierRequestReport");
        foulsTable.Columns.Add("AdvertiserPhone");
     //   foulsTable.Columns.Add("ChatTrace");
        foulsTable.Columns.Add("ChatTraceScript");
        foulsTable.Columns.Add("FoulsText");
        foulsTable.Columns.Add("FoulsUrl");
        
        foreach(ClipCallReport.AccountFoulFullDetailsResponse foul in result.Value.Fouls)
        {
            DataRow row = foulsTable.NewRow();
            row["AdvertiserName"] = foul.SupplierName;
            row["AdvertiserPhone"] = foul.SupplierPhone;
            row["SupplierRequestReport"] = string.Format(Utilities.SUPPLIER_REQUEST_REPORT_URL, foul.SupplierId);
            row["ChatTraceScript"] = "return OpenChat('" + foul.IncidentAccountId.ToString() + "');";
            row["FoulsText"] = foul.Fouls == 0 ? NONE_FOUL_TEXT : 
                string.Format(FOUL_TOTAL_TEXT, foul.Fouls) + string.Format(FOUL_THIS_PROJECT_TEXT, (foul.FoulsThisProject == 0 ? "Not" : foul.FoulsThisProject.ToString()));
            row["FoulsUrl"] = string.Format(Utilities.FOULS_MANAGMENT_URL, foul.SupplierId);
            foulsTable.Rows.Add(row);
        }
        gv_fouls.DataSource = foulsTable;
        gv_fouls.DataBind();

        /* Main Table */
        DataTable dt = new DataTable();

        dt.Columns.Add("Date");
        dt.Columns.Add("AdvertiserName");
        dt.Columns.Add("AdvertiserPhone");
        dt.Columns.Add("SupplierRequestReport");
        dt.Columns.Add("Type");
        dt.Columns.Add("RecordText");
        dt.Columns.Add("Record");
        dt.Columns.Add("HasDownload", typeof(bool));
        dt.Columns.Add("Download");
        dt.Columns.Add("QualityRating");

        dt.Columns.Add("RecordDescription");
        dt.Columns.Add("RecordDescriptionVisible", typeof(bool));
        foreach (ClipCallReport.IncidentInteractionData iid in result.Value.Values)
        {
            DataRow row = dt.NewRow();
            row["Date"] = iid.Date.GetDateForClient();// string.Format(DATE_TIME_FORMAT, iid.Date);
            row["AdvertiserName"] = iid.SupplierName;
            row["AdvertiserPhone"] = iid.SupplierPhone;
            row["Type"] = iid.IncidentInteractionType.ToString();
            row["SupplierRequestReport"] = string.Format(Utilities.SUPPLIER_REQUEST_REPORT_URL, iid.SupplierId);

            row["RecordDescriptionVisible"] = false;
            
            switch(iid.IncidentInteractionType)
            {
                case(ClipCallReport.eIncidentInteractionType.Video):
                case (ClipCallReport.eIncidentInteractionType.VideoChat):
                case (ClipCallReport.eIncidentInteractionType.PhoneCall):
                    {
                        row["RecordText"] = iid.IncidentInteractionType == ClipCallReport.eIncidentInteractionType.Payment ? iid.Amount.ToString() : "Record link";
                        GenerateVideoLink gvl = new GenerateVideoLink(iid.RecordUrl, iid.PicPreview);
                        row["Record"] = "OpenRecord('" + gvl.GenerateFullUrl() + "');";
                        if (iid.IncidentInteractionType == ClipCallReport.eIncidentInteractionType.Video || iid.IncidentInteractionType == ClipCallReport.eIncidentInteractionType.VideoChat)
                        {
                            string videoUrl = EncryptString.EncodeRecordPath(iid.RecordUrl);
                            row["HasDownload"] = true;
                            row["Download"] = "return DownloadVideo('" + videoUrl + "');";
                        }
                        else
                            row["HasDownload"] = false;
                        break;
                    }
                case(ClipCallReport.eIncidentInteractionType.Payment):
                    row["HasDownload"] = false;
                    break;
                case(ClipCallReport.eIncidentInteractionType.ChatCreatedByCustomer):
                case (ClipCallReport.eIncidentInteractionType.ChatCreatedBySupplier):
                case (ClipCallReport.eIncidentInteractionType.ChatCreatedByServer):
                    row["HasDownload"] = false;
                    row["RecordText"] = "Chat trace";
                    row["Record"] = "return OpenChat('" + iid.IncidentAccountId.ToString() + "');";
                    break;
                case(ClipCallReport.eIncidentInteractionType.Quote):
                    row["HasDownload"] = false;
                    double price = (double)iid.Amount / 100.0;
                    string _price = string.Format(NUMBER_FORMAT, price);
                    row["RecordText"] = string.Format(QUOTE_TEXT, iid.Number, _price);
                    row["Record"] = "return Openquote('" + iid.Id + "')";
                    if(!string.IsNullOrEmpty(iid.Text))
                    {
                        row["RecordDescriptionVisible"] = true;
                        row["RecordDescription"] = iid.Text;
                    }
                    break;
                case(ClipCallReport.eIncidentInteractionType.ProximitySensor):
                     row["HasDownload"] = false;
                     if (iid.Number > 0)
                         row["RecordText"] = string.Format(PROXIMITY_WITH_DISTANCE_TEXT, iid.Number);
                     else
                         row["RecordText"] = PROXIMITY_NONE_DISTANCE_TEXT;
                     row["Record"] = "return OpenProximitySensor('" + iid.Id + "')";
                    break;
                case(ClipCallReport.eIncidentInteractionType.SupplierSurveyResponded):
                case (ClipCallReport.eIncidentInteractionType.CustomerSurveyResponded):
                case (ClipCallReport.eIncidentInteractionType.PaymentFailed):
                case (ClipCallReport.eIncidentInteractionType.SmsUnreadMessagesToCustomer):
                case (ClipCallReport.eIncidentInteractionType.SmsUnreadMessagesToPro):
                case (ClipCallReport.eIncidentInteractionType.ChatWarningCustomer):
                case (ClipCallReport.eIncidentInteractionType.ChatWarningPro):
                case (ClipCallReport.eIncidentInteractionType.QuoteWarning):
                    row["HasDownload"] = false;
                    row["Record"] = "return false;";
                    row["RecordText"] = iid.Text;
                    break;
                    /*
                case (ClipCallReport.eIncidentInteractionType.ChatWarningCustomer):
                case (ClipCallReport.eIncidentInteractionType.ChatWarningPro):
                case (ClipCallReport.eIncidentInteractionType.QuoteWarning):
                    row["HasDownload"] = false;
                    row["Record"] = "return false;";
                    row["RecordText"] = iid.Text;
                    break;
                    */

            }
            row["QualityRating"] = iid.SupplierId == Guid.Empty ? null : iid.QualityRating.HasValue ? iid.QualityRating.Value.ToString() : "not supplied";
               

            dt.Rows.Add(row);
        }
        
        _GridView.DataSource = dt;
        _GridView.DataBind();

    }
    void ClosePopupByError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseError", "CloseMeError();", true);
    }
    protected string GetDownloadVideo
    {
        get { return ResolveUrl("~") + "audio/DownloadVideo.ashx"; }
    }
    protected string GetChatUrl
    {
        get { return "ClipCallLeadChat.aspx?ia="; }
    }
    protected string GetQuoteUrl
    {
        get { return "QuoteTicket.aspx?quoteid="; }
    }
    protected string GetProximitySensorUrl
    {
        get { return "ProximitySensorTicket.aspx?psid="; }
    }
}