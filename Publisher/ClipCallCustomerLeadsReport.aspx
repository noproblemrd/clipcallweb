﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ClipCallCustomerLeadsReport.aspx.cs" Inherits="Publisher_ClipCallCustomerLeadsReport" %>

<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
    <script type="text/javascript">
        function _CreateExcel() {
            var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"AarCallCenterReport"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="form-field">
					<label for="form-subsegment" class="label" runat="server" id="lblExperties" >Lead count (at least):</label>
                <asp:DropDownList ID="ddl_LeadCount" runat="server" CssClass="form-select">
                </asp:DropDownList>
					            
			</div> 			
		</div>
      
		<div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px; padding-right: 25px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
         <div class="results" runat="server" id="div_results" visible="false">
            <span class="result-line">Total rows:<asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></span>           
        </div>       
        <div id="Table_Report"  runat="server" >  
				<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="Name">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="Name"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblBroker" runat="server" Text="<%# Bind('Name') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Phone">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="Phone"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label11" runat="server" Text="<%# Bind('Phone') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Email">
						<HeaderTemplate>
							<asp:Label ID="Label14" runat="server" Text="Email"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label15" runat="server" Text="<%# Bind('Email') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Count">
						<HeaderTemplate>
							<asp:Label ID="Label16" runat="server" Text="Count"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label17" runat="server" Text="<%# Bind('Count') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="FirstLead">
						<HeaderTemplate>
							<asp:Label ID="Label18" runat="server" Text="Duration for First Lead"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label19" runat="server" Text="<%# Bind('FirstLead') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="AvgTimeBetweenLeads">
						<HeaderTemplate>
							<asp:Label ID="Label28" runat="server" Text="Avg Time Between Leads"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label29" runat="server" Text="<%# Bind('AvgTimeBetweenLeads') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

				</Columns>    
				</asp:GridView>
			</div>
        </div>
    </div>
</asp:Content>

