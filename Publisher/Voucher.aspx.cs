﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Publisher_Voucher : PageSetting
{
    const string ExcelName = "Voucher";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetToolbox();
            LoadVouchers();
        }
        SetToolboxEvents();
    }

    private void LoadVouchers()
    {
        
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfVoucherData result = null;
        try
        {
            result = _reports.VoucherReport();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        _GridView.PageIndex = 0;
        DataTable data = GetDataTable.GetDataTableFromListCorrectNumberDateTimeFormat(result.Value, siteSetting.DateFormat);
        _GridView.DataSource = data;
        _GridView.DataBind();
        if(data.Rows.Count==0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + ToolboxReport1.NoRecordExport + "');", true);
        _UpdatePanel.Update();
        dataV = data;
        
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_Vouchers.Text);

    }
    protected void gv_GroupByDates_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, ExcelName);

        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordToPrint + "');", true);
            return;
        }
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
}
