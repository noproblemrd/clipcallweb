﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InjectionUpset.aspx.cs" Inherits="Publisher_InjectionUpset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="Stylesheet" href="InjectionCampaign.css" type="text/css" />
    <title></title>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        function CloseWin() {            
            window.close();
        }
        function CloseReloadWin() {
            window.opener.Reload();
            window.close();
        }

        function SaveClick() {
            $('#<%# lb_Save.ClientID %>').hide();
            $('#a_Save_loader').show();
        }
    
    
    </script>
</head>
<body  class="Injection_form">
    <form id="form1" runat="server">
    <div class="Popwin-title">        
         <span><asp:Label ID="lbl_Title" runat="server" Text="Label"></asp:Label>
        injection</span>
    </div>
    
    <div>
        <table class="Popwin-table">
            <tr>
                <td>
                    <span>Name</span>
                </td>
                <td>
                    <asp:TextBox ID="txt_Name" runat="server"></asp:TextBox>
                </td>                
            </tr>

              <tr>
                <td>
                    <span>Script</span>
                </td>
                <td>                    
                    <asp:TextBox ID="txt_Script" runat="server" TextMode="MultiLine" CssClass="txt_multiline"></asp:TextBox>
                </td>
            </tr>

              <tr>
                <td>
                    <span>URL Script</span>
                </td>
                <td>                    
                    <asp:TextBox ID="txt_UrlScript" runat="server" TextMode="MultiLine" CssClass="txt_multiline"></asp:TextBox>
                </td>
            </tr>

             <tr>
                <td>
                    <span>Script element id</span>
                </td>
                <td>                    
                    <asp:TextBox ID="txt_ScriptId" runat="server"></asp:TextBox>
                </td>
            </tr>

             <tr>
                <td>
                    <span>Block script <br /> with our slider</span>
                </td>
                <td>                    
                    <asp:CheckBox ID="cb_BlockWithUs" runat="server" />
                </td>
            </tr>

              <tr>
                <td>
                    
                </td>
                <td>
                    <asp:LinkButton ID="lb_Save" runat="server" CssClass="BtnInjection" 
                        onclick="lb_Save_Click" OnClientClick="SaveClick();">Save</asp:LinkButton>
                     <span id="a_Save_loader" style="display:none;" class="a__loader a_Save_loader"></span>
                </td>
            </tr>

        </table>
        
    
    </div>
    </form>
</body>
</html>
