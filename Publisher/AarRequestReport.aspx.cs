﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Publisher_AarRequestReport : PageSetting, IPostBackEventHandler
{
    const int DAYS_INTERVAL = 2;
    Dictionary<int, PostBackOptions> dicPostBack;
    protected void Page_Load(object sender, EventArgs e)
    {
        dicPostBack = new Dictionary<int, PostBackOptions>();
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        if (!IsPostBack)
        {
            dataV = null;
            YesNoV = null;
            FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
            SetToolbox();
            LoadHeadings();
            ExecReport();
            //   FromToDate1.GetDateFrom = new DateTime(2010, 3, 13);
        }
        else
        {
            string __EVENTARGUMENT = Request["__EVENTARGUMENT"];
            int num;
            if (int.TryParse(__EVENTARGUMENT, out num))
                _RaisePostBackEvent(num);
        }
        SetToolboxEvents();
    }

    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        BindToTable(dataV, _pageNum, TotalPagesV);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, lbl_AARRequest.Text);

        _Repeater.ItemDataBound -= new RepeaterItemEventHandler(_Repeater_ItemDataBound);
        _Repeater.ItemDataBound += new RepeaterItemEventHandler(_Repeater_ItemDataBoundPrint);

        _Repeater.DataSource = dataV;
        _Repeater.DataBind();
        if (!te.ExecExcel(_Repeater))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
        _Repeater.ItemDataBound += new RepeaterItemEventHandler(_Repeater_ItemDataBound);
        _Repeater.ItemDataBound -= new RepeaterItemEventHandler(_Repeater_ItemDataBoundPrint);
        BindToTable(dataV, TablePaging1.Current_Page, TotalPagesV);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        DataTable data = dataV;
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordToPrint + "');", true);
            return;
        }
        /*
        Repeater NewRepeater = new Repeater();
        NewRepeater.ItemTemplate = _Repeater.ItemTemplate;
        NewRepeater.HeaderTemplate = _Repeater.HeaderTemplate;
        NewRepeater.AlternatingItemTemplate = _Repeater.AlternatingItemTemplate;
        NewRepeater.FooterTemplate = _Repeater.FooterTemplate;
         *  * */
        _Repeater.ItemDataBound -= new RepeaterItemEventHandler(_Repeater_ItemDataBound);
        _Repeater.ItemDataBound += new RepeaterItemEventHandler(_Repeater_ItemDataBoundPrint);
        
        _Repeater.DataSource = data;
        _Repeater.DataBind();
        
        StringWriter stringWriter = new StringWriter();

        // Put HtmlTextWriter in using block because it needs to call Dispose.
        using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
        {
            _Repeater.RenderControl(writer);
        }
        
    //    Session["data_print"] = dataV;
        Session["grid_print"] = stringWriter.ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
        _Repeater.ItemDataBound += new RepeaterItemEventHandler(_Repeater_ItemDataBound);
        _Repeater.ItemDataBound -= new RepeaterItemEventHandler(_Repeater_ItemDataBoundPrint);
        BindToTable(data, TablePaging1.Current_Page, TotalPagesV);
        
    }
    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    private void SetToolbox()
    {
        
        ToolboxReport1.SetTitle(lbl_AARRequest.Text);
    }
    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecReport();
    }
    void ExecReport()
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.AarRequestsReportsRequest _request = new WebReferenceReports.AarRequestsReportsRequest();
        _request.FromDate = FromToDate1.GetDateFrom;
        _request.ToDate = FromToDate1.GetDateTo;
        _request.HeadingId = new Guid(ddl_Heading.SelectedValue);
        WebReferenceReports.ResultOfListOfAarRequestsReportRow result = null;
        try
        {
            result = _report.AarRequestsReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        RequestAarV = _request;
        DataTable data = GetDataTable.GetDataTableFromListCorrectNumber(result.Value);
        lbl_RecordMached.Text = data.Rows.Count + " " + lblRecordMached.Text;
        int CurrentPage = 1;
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + ToolboxReport1.NoRecordToPrint + "');", true);
            _Repeater.DataSource = null;
            _Repeater.DataBind();
            _UpdatePanel.Update();
            CurrentPage = 0;
        }
        int Pages_Count = (data.Rows.Count / TablePaging1.ItemInPage) + (((data.Rows.Count % TablePaging1.ItemInPage) == 0) ? 0 : 1);
        BindToTable(data, CurrentPage, Pages_Count);
        dataV = data;
        TotalPagesV = Pages_Count;        
    }
    void BindToTable(DataTable data, int Current_Page, int Pages_Count)
    {
        TablePaging1.Current_Page = Current_Page;
        TablePaging1.Pages_Count = Pages_Count;        
        TablePaging1.LoadPages();
        _Repeater.DataSource = (Current_Page == 0) ? null : GetDataForGridView(data, Current_Page);
        _Repeater.DataBind();
        _UpdatePanel.Update();
       
    }
    
    protected void _Repeater_ItemDataBoundPrint(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.AlternatingItem && e.Item.ItemType != ListItemType.Item)
            return;        
        HtmlTableRow rowDetails = (HtmlTableRow)e.Item.FindControl("tr__details");
        rowDetails.Visible = false;
        SetDetails(e.Item, true);
    }
    protected void _Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.AlternatingItem && e.Item.ItemType != ListItemType.Item)
            return;
        HtmlTableRow rowDetails = (HtmlTableRow)e.Item.FindControl("tr__details");
        rowDetails.Visible = false;
        HtmlTableRow tr_data = (HtmlTableRow)e.Item.FindControl("tr_main");
        tr_data.Attributes["class"] = (e.Item.ItemType == ListItemType.Item) ? "odd RowClickable" : "even RowClickable";
        tr_data.Attributes["onclick"] = GetPostBackOpen(tr_data, e.Item.ItemIndex);
          
    }
    DataTable GetDataForGridView(DataTable data, int Current_Page)
    {
        DataTable NewData = new DataTable();
        foreach (DataColumn dc in data.Columns)
            NewData.Columns.Add(dc.ColumnName, dc.DataType);
        int i = (TablePaging1.ItemInPage * (Current_Page - 1));
        int maxI = i + TablePaging1.ItemInPage;
        for (; i < maxI && i < data.Rows.Count; i++)
        {
            DataRow row = NewData.NewRow();
            for (int j = 0; j < data.Columns.Count; j++)
            {
                row[j] = data.Rows[i][j];
            }
            NewData.Rows.Add(row);
        }
        return NewData;
    }
    private void LoadHeadings()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = _site.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {
            string pName = nodePrimary.Attribute("Name").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
    }
    #region IPostBackEventHandler Members
    public void RaisePostBackEvent(string eventArgument)
    {
        /*
        int _index = TablePaging1.Current_Page;
        _index = (_index*TablePaging1.ItemInPage)+int.Parse(eventArgument);
        RepeaterItem item = _Repeater.Items[_index];
        SetDetails(item);
        _UpdatePanel.Update();
         * */
    }
    void _RaisePostBackEvent(int num)
    {
      //  int _index = TablePaging1.Current_Page;
    //    _index = (_index * TablePaging1.ItemInPage) + num;
        RepeaterItem item = _Repeater.Items[num];
        SetDetails(item, false);
        _UpdatePanel.Update();
    }
    void SetDetails(RepeaterItem item, bool ForExport)
    {
        HtmlTableRow htr = (HtmlTableRow)item.FindControl("tr__details");

        if (!htr.Visible || ForExport)
        {
            htr.Visible = true;
            GridView gv = (GridView)item.FindControl("_GridViewIn");
            ((HtmlAnchor)item.FindControl("a_open")).Attributes["class"] = "SideArrow RowClickable";
            if (gv.Rows.Count > 0)            
                return;
            WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
            WebReferenceReports.ResultOfListOfAarRequestReportDrillDownRow result = null;
            WebReferenceReports.AarRequestsReportsRequest _request = RequestAarV;
            _request.HeadingId = new Guid(((Label)item.FindControl("lbl_HeadingId")).Text);
            try
            {
                result = _report.AarRequestsReportDrillDown(_request);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, siteSetting);
                Update_Faild();
                return;
            }
            if (result.Type == WebReferenceReports.eResultType.Failure)
            {
                Update_Faild();
                return;
            }
            DataTable data = GetDataTable.GetDataTableFromListCorrectNumberTrueFalse(result.Value, YesNoV);
            if (ForExport)
                gv.EnableViewState = false;
            gv.DataSource = data;
            gv.DataBind();
        }
        else
        {
            ((HtmlAnchor)item.FindControl("a_open")).Attributes["class"] = "arrow RowClickable";
            htr.Visible = false;
        }
    }
    
    /*
    public override void RenderControl(HtmlTextWriter writer)
    {
        base.RenderControl(writer);
        foreach (KeyValuePair<int, PostBackOptions> kvp in dicPostBack)
            Page.ClientScript.RegisterForEventValidation(kvp.Value);
    }
    */
    protected string GetPostBackOpen(Control cntrl, int indx)
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(cntrl, indx.ToString());
        myPostBackOptions.PerformValidation = false;
        if (dicPostBack.ContainsKey(indx))
            dicPostBack[indx] = myPostBackOptions;
        else
            dicPostBack.Add(indx, myPostBackOptions);
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
    }
    
    #endregion
    DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    int TotalPagesV
    {
        get { return (ViewState["TotalPages"] == null) ? 0 : (int)ViewState["TotalPages"]; }
        set { ViewState["TotalPages"] = value; }
    }
    WebReferenceReports.AarRequestsReportsRequest RequestAarV
    {
        get { return (ViewState["RequestAar"] == null) ? new WebReferenceReports.AarRequestsReportsRequest() : (WebReferenceReports.AarRequestsReportsRequest)ViewState["RequestAar"]; }
        set { ViewState["RequestAar"] = value; }
    }
    Dictionary<bool, string> YesNoV
    {
        get 
        { 
            if(Session["YesNo"] == null){
                Dictionary<bool, string> dic = new Dictionary<bool, string>();
                dic.Add(true, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", eYesNo.Yes.ToString()));
                dic.Add(false, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", eYesNo.No.ToString()));                
                Session["YesNo"] = dic;
                return dic;
            }
            else return (Dictionary<bool, string>)Session["YesNo"]; 
        }
        set { Session["YesNo"] = value; }
    }



   
}