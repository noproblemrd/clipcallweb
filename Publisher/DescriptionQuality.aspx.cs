﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class Publisher_DescriptionQuality : PageSetting
{
    const int DAYS_INTERVAL = 2;
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    const string EXCEL_NAME = "Invalid Requests";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            LoadQualityCheckName();
            LoadFilters();
            SetToolbox();
        }
        SetToolboxEvents();
        Header.DataBind();
    }

    private void LoadFilters()
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfGetValidationReportDropDownListsResponse result = null;
        try
        {
            result = _report.GetValidationReportDropDownLists();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        SetDdl(result.Value.Controls, ddl_Controls);
        SetDdl(result.Value.Events, ddl_Events);
        SetDdl(result.Value.Flavors, ddl_Flavors);
        SetDdl(result.Value.Steps, ddl_Steps);
        SetDdl(result.Value.ZipCodeCheckNames, ddl_ZipCodeCheckNames);
    }
    void SetDdl(string[] strs, DropDownList _ddl)
    {
        _ddl.Items.Clear();
        _ddl.Items.Add(new ListItem("", ""));
        foreach (string str in strs)
        {
            _ddl.Items.Add(new ListItem(str, str));
        }
        _ddl.SelectedIndex = 0;
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_PageTitle.Text);

    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        WebReferenceReports.DescriptionValidationReportRequest _request = RequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataTable data = GetDataReport(_request).data;
        SetColumns(data);
        data = SetDataToExcel(data);
        ToExcel te = new ToExcel(this, lbl_PageTitle.Text);
        if (!te.ExecExcel(data))
            Update_Faild();
    }
    DataTable SetDataToExcel(DataTable data)
    {
        DataTable NewTable = new DataTable();
        foreach (DataColumn dc in data.Columns)
        {
            NewTable.Columns.Add(dc.ColumnName, typeof(string));
        }
        foreach (DataRow row in data.Rows)
        {
            DataRow NewRow = NewTable.NewRow();
            foreach (DataColumn dc in data.Columns)
            {
                if (row[dc] == null)
                {
                    NewRow[dc.ColumnName] = null;
                    continue;
                }
                if (row[dc].GetType() == typeof(DataTable))
                {
                    DataTable dataIn = (DataTable)row[dc];
                    string name = "";
                    foreach (DataRow RowIn in dataIn.Rows)
                    {
                        name += RowIn["Passed_name"] + ", ";
                    }
                    NewRow[dc.ColumnName] = ((name.Length > 2) ? name.Substring(0, name.Length - 2) : name);
                }
                else
                    NewRow[dc.ColumnName] = row[dc].ToString();
            }
            NewTable.Rows.Add(NewRow);
        }
        return NewTable;
    }
    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        WebReferenceReports.DescriptionValidationReportRequest _request = RequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataTable data = GetDataReport(_request).data;
        Session["data_print"] = data;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (RequestV == null)
            return;
        WebReferenceReports.DescriptionValidationReportRequest _request = RequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        DataResult dt = GetDataReport(_request);
        BindData(dt.data);
        LoadGeneralData(dt.TotalPages, dt.TotalRows, dt.CurrentPage);
    }

    private void LoadQualityCheckName()
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        ddl_QualityName.Items.Clear();
        ddl_QualityName.Items.Add(new ListItem("", ""));
        foreach (WebReferenceReports.enumQualityCheckName eqcn in Enum.GetValues(typeof(WebReferenceReports.enumQualityCheckName)))
        {
            string trans = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "enumQualityCheckName", eqcn.ToString());
            ddl_QualityName.Items.Add(new ListItem(trans, eqcn.ToString()));
            dic.Add(eqcn.ToString(), trans);
        }
        ddl_QualityName.SelectedIndex = 0;
        dicQualityNameV = dic;
    }

    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        DateTime _to = FromToDate1.GetDateTo;
        DateTime _from = FromToDate1.GetDateFrom;

        if (_to == DateTime.MinValue)
            _to = DateTime.Now;
        if (_from == DateTime.MinValue)
            _from = _to.AddMonths(-1);
        else
        {
            if (_from > _to)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + FromToDate1.GetDateError + "');", true);
                return;
            }
        }


        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.DescriptionValidationReportRequest _request = new WebReferenceReports.DescriptionValidationReportRequest();
        /*
        WebReferenceReports.ResultOfDescriptionValidationGroupBySessionResponse __y;
        __y.Value.Rows[0].Count;
        __y.Value.Rows[0].Session;
         * */
        WebReferenceReports.enumQualityCheckName eqcn;
        if (Enum.TryParse(ddl_QualityName.SelectedValue, out eqcn))
            _request.QualityCheckName = eqcn;
        _request.FromDate = _from;
        _request.ToDate = _to;
        _request.Control = ddl_Controls.SelectedValue;
        _request.Event = ddl_Events.SelectedValue;
        _request.Flavor = ddl_Flavors.SelectedValue;
        _request.Step = ddl_Steps.SelectedValue;
        _request.ZipCodeCheckName = ddl_ZipCodeCheckNames.SelectedValue;
        _request.PageNumber = 1;
        _request.PageSize = ITEM_PAGE;
        RequestV = _request;
        IsGroupBySession = cb_GroupBy.Checked;
        DataResult dr = GetDataReport(_request);

        if (dr == null)
        {
            Update_Faild();
            ClearTable();
            return;
        }
        if (dr.data.Rows.Count == 0)
        {
            ClearTable();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
            return;
        }
        BindData(dr.data);
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);

    }
    private DataResult GetDataReport(WebReferenceReports.DescriptionValidationReportRequest _request)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        if (!IsGroupBySession)
        {
            WebReferenceReports.ResultOfDescriptionValidationBasicReportResponse result = null;
            try
            {
                result = _report.DescriptionValidationBasicReport(_request);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, siteSetting);
                Update_Faild();
                return null;
            }
            if (result.Type == WebReferenceReports.eResultType.Failure)
            {
                Update_Faild();
                return null;
            }
            DataTable data = GetDataTable.GetDataTableColumsString(result.Value.Rows);
            data.Columns.Add("Count", typeof(string));
            data.Columns.Add("LastDescription", typeof(string));
            data.Columns.Add("_data", typeof(DataTable));
          
            foreach (WebReferenceReports.DescriptionValidationReportRow dvrr in result.Value.Rows)
            {
                DataRow row = data.NewRow();
                row["CreatedOn"] = Utilities.GetDateTimeView(dvrr.CreatedOn, siteSetting.DateFormat, siteSetting.TimeFormat);
                row["Description"] = dvrr.Description;
                row["Heading"] = dvrr.Heading;
                row["Keyword"] = dvrr.Keyword;
                row["Origin"] = dvrr.Origin;
                row["Phone"] = dvrr.Phone;
                row["QualityCheckName"] = dvrr.QualityCheckName;
                row["Region"] = dvrr.Region;
                row["Session"] = dvrr.Session;
                row["Url"] = dvrr.Url;
                row["Event"] = dvrr.Event;
                row["ZipCodeCheckName"] = dvrr.ZipCodeCheckName;
                row["Flavor"] = dvrr.Flavor;
                row["Step"] = dvrr.Step;
                row["Control"] = dvrr.Control;
                row["_data"] = new DataTable();
                data.Rows.Add(row);
            }
            /*
       result.Value.Rows[0].CreatedOn;
       result.Value.Rows[0].Description;
       result.Value.Rows[0].Heading;
       result.Value.Rows[0].Keyword;
       result.Value.Rows[0].Origin;
       result.Value.Rows[0].Phone;
       result.Value.Rows[0].QualityCheckName;
       result.Value.Rows[0].Region;
       result.Value.Rows[0].Session;
       result.Value.Rows[0].Url;
        
        */
            DataResult dr = new DataResult() { data = data, CurrentPage = result.Value.CurrentPage, TotalRows = result.Value.TotalRows, TotalPages = result.Value.TotalPages };
            return dr;
        }
        WebReferenceReports.ResultOfDescriptionValidationGroupBySessionResponse _result = null;
        try
        {
            _result = _report.DescriptionValidationGroupBySessionReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return null;
        }
        if (_result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return null;
        }
     //   DataTable _data = GetDataTable.GetDataTableFromList(_result.Value.Rows);
        DataTable _data = new DataTable();
        _data.Columns.Add("Session", typeof(string));
        _data.Columns.Add("LastDescription", typeof(string));
        _data.Columns.Add("Heading", typeof(string));
        _data.Columns.Add("Count", typeof(int));
        _data.Columns.Add("_data", typeof(DataTable));
  //      _data.Columns.Add("Passed", typeof(string));
 //       _data.Columns.Add("Passed_name", typeof(string));
        
   //     Dictionary<Guid, string> dic = new Dictionary<Guid, string>();
        foreach (WebReferenceReports.DescriptionValidationGroupBySessionRow ValueRow in _result.Value.Rows)
        {
            DataRow row = _data.NewRow();
            row["Count"] = ValueRow.Count;
            row["Heading"] = ValueRow.Heading;
            row["LastDescription"] = ValueRow.LastDescription;
            row["Session"] = ValueRow.Session;
    //        string _Passed = "";
    //        string _Passed_name = "";
            DataTable RepeaterData = new DataTable();
            if (ValueRow.Cases != null)
            {
                
                RepeaterData.Columns.Add("Passed_name", typeof(string));
                RepeaterData.Columns.Add("script", typeof(string));
                foreach (WebReferenceReports.GuidStringPair gsp in ValueRow.Cases)
                {
                    string script = "javascript:OpenIframe('RequestTicket.aspx?RequestTicket=" + gsp.Id.ToString() + "');";
                    DataRow dr = RepeaterData.NewRow();
                    dr["Passed_name"] = gsp.Name;
                    dr["script"] = script;
                    RepeaterData.Rows.Add(dr);
                }

            }
            row["_data"] = RepeaterData;
         //   row["Passed"] = (_Passed.Length > 2) ? _Passed.Substring(0, _Passed.Length - 2) : _Passed;
         //   row["Passed_name"] = (_Passed_name.Length > 2) ? _Passed_name.Substring(0, _Passed_name.Length - 2) : _Passed_name;
            _data.Rows.Add(row);
        }
 //       dic_cases = dic;
        _data.Columns.Add("CreatedOn", typeof(string));
        _data.Columns.Add("Description", typeof(string));
        _data.Columns.Add("Keyword", typeof(string));
        _data.Columns.Add("Origin", typeof(string));
        _data.Columns.Add("Phone", typeof(string));
        _data.Columns.Add("QualityCheckName", typeof(string));
        _data.Columns.Add("Region", typeof(string));
        _data.Columns.Add("Url", typeof(string));
        _data.Columns.Add("Event", typeof(string));
        _data.Columns.Add("ZipCodeCheckName", typeof(string));
        _data.Columns.Add("Flavor", typeof(string));
        _data.Columns.Add("Step", typeof(string));
        _data.Columns.Add("Control", typeof(string));
        /*
         row["Event"] = dvrr.Event;
                row["ZipCodeCheckName"] = dvrr.ZipCodeCheckName;
                row["Flavor"] = dvrr.Flavor;
                row["Step"] = dvrr.Step;
                row["Control"] = dvrr.Control;
               
         * */
        DataResult _dr = new DataResult() { data = _data, CurrentPage = _result.Value.CurrentPage, TotalRows = _result.Value.TotalRows, TotalPages = _result.Value.TotalPages };
        return _dr;
        
    }
   
    void SetColumns()
    {
        foreach (DataControlField field in _GridView.Columns)
        {
            switch (field.SortExpression)
            {
                case ("CreatedOn"):
                case ("Description"):                
                case ("Keyword"):
                case ("Origin"):
                case ("Phone"):
                case ("QualityCheckName"):
                case ("Region"):
                case ("Url"):
                case ("Event"):
                case ("ZipCodeCheckName"):
                case ("Flavor"):
                case ("Step"):
                case ("Control"):
                    field.Visible = !IsGroupBySession;
                    break;
                case ("Count"):
                case("LastDescription"):
                case("Passed"):
                    field.Visible = IsGroupBySession;
                    break;

            }
        }
    }
    void SetColumns(DataTable data)
    {
        for (int i = data.Columns.Count - 1; i > -1; i--)
        {
            switch (data.Columns[i].ColumnName)
            {
                case ("CreatedOn"):
                case ("Description"):
                case ("Heading"):
                case ("Keyword"):
                case ("Origin"):
                case ("Phone"):
                case ("QualityCheckName"):
                case ("Region"):
                case ("Url"):
                case ("Event"):
                case ("ZipCodeCheckName"):
                case ("Flavor"):
                case ("Step"):
                case ("Control"):
                    if (IsGroupBySession)
                        data.Columns.Remove(data.Columns[i].ColumnName);
                    break;
                case ("Count"):
                    if (!IsGroupBySession)
                        data.Columns.Remove(data.Columns[i].ColumnName);
                    break;

            }
        }
    }
    void ClearTable()
    {
        _GridView.DataSource = null;
        _GridView.DataBind();
        LoadGeneralData(0, 0, 0);
        _UpdatePanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = _TotalRows + " " + ToolboxReport1.RecordMached;
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    protected void BindData(DataTable data)
    {
        _GridView.DataSource = data;
        _GridView.DataBind();
        SetColumns();
        _UpdatePanel.Update();
    }
    WebReferenceReports.DescriptionValidationReportRequest RequestV
    {
        get { return (ViewState["Request"] == null) ? null : (WebReferenceReports.DescriptionValidationReportRequest)ViewState["Request"]; }
        set { ViewState["Request"] = value; }
    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
    protected bool IsGroupBySession
    {
        get { return (ViewState["IsGroupBySession"] == null) ? false : (bool)ViewState["IsGroupBySession"]; }
        set { ViewState["IsGroupBySession"] = value; }
    }
    Dictionary<string, string> dicQualityNameV
    {
        get { return (Session["dicQualityName"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)Session["dicQualityName"]; }
        set { Session["dicQualityName"] = value; }
    }
    /*
    Dictionary<Guid, string> dic_cases
    {
        get { return (ViewState["dic_cases"] == null) ? new Dictionary<Guid, string>() : (Dictionary<Guid, string>)ViewState["dic_cases"]; }
        set { ViewState["dic_cases"] = value; }
    }
    */
}