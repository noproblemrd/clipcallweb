﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_SearchSupplierApp : PageSetting
{
    const string RUN_REPORT = "RUN_REPORT";
    protected const int ITEM_PAGE = 50;
    protected const int PAGE_PAGES = 10;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Button for Async IPostBackEvent
        ScriptManager1.RegisterAsyncPostBackControl(btn_virtual_run);
        //
        Master.newAdvEvent += new EventHandler(Master_newAdvEvent);
        ScriptManager1.RegisterAsyncPostBackControl(btnSearch);
        TablePagingConfiguration();
        Master.RemoveNewAdvertiser();
        if (!IsPostBack)
        {
      //      LoadTRInline();
            LoadStatusUsers();
            LoadDevices();
            LoadCategories();
        }
        else
        {
            if (Request["__EVENTARGUMENT"] == RUN_REPORT)
                Search_Report();
        }
        Header.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Search_Report();
    }
    void Search_Report()
    {
        string searchTxt = txtSearch.Text.Trim();
        
        _PanelNoReasult.Visible = false;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Search_focus", "SetCursorToTextEnd('" + txtSearch.ClientID + "');", true);

        ClipCallReport.SearchAppSuppliersRequest _request = new ClipCallReport.SearchAppSuppliersRequest();
        _request.PageSize = ITEM_PAGE;//TablePaging1.ItemInPage;
        _request.PageNumber = 1;
        _request.SearchValue = searchTxt;
        _request.CategoryId = new Guid(ddl_Category.SelectedValue);
        ClipCallReport.eMobileDevice _device;
        if (!Enum.TryParse(ddl_Device.SelectedValue, out _device))
            _device = ClipCallReport.eMobileDevice.ALL;
        _request.MobileDevice = _device;
        ClipCallReport.eSupplierStatusAppRegistration _status;
        if (!Enum.TryParse(ddl_SupplierStatus.SelectedValue, out _status))
            _status = ClipCallReport.eSupplierStatusAppRegistration.ALL;
        _request.SupplierStatus = _status;
        /*
        if(string.IsNullOrEmpty(_request.SearchValue) && _request.MobileDevice == ClipCallReport.eMobileDevice.ALL &&
            _request.SupplierStatus == ClipCallReport.eSupplierStatusAppRegistration.ALL && _request.CategoryId == default(Guid))
        {
            BadArgument();
            return;
        }
         * */
        SearchSuppliersRequestV = _request;
        LoadTable(_request);
    }
    void LoadTable(ClipCallReport.SearchAppSuppliersRequest _request)
    {
        DataResult dr = ExecSearchSupplier(_request);
        if (dr == null)
        {
            Update_Faild();
            ClearTable();
            return;
        }
        if (dr.data.Rows.Count == 0)
        {
            ClearTable();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(Master.GetNoResultsMessage) + "');", true);
            return;
        }
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
        BindData(dr.data);
  //      listTR = new List<string>();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "BlurTxtSearch", "BlurTxtSearch();", true);
    }
    private void BadArgument()
    {
        string message = "You must enter at least one value filter";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + message + "');", true);
    }
    private DataResult ExecSearchSupplier(ClipCallReport.SearchAppSuppliersRequest _request)
    {
        DataTable data = new DataTable();
        data.Columns.Add("supplierid");
        //       data.Columns.Add("NavigateUrl", typeof(string));
        data.Columns.Add("Name", typeof(string));
        data.Columns.Add("FullName", typeof(string));
        data.Columns.Add("Phone", typeof(string));
        data.Columns.Add("Email", typeof(string));
        data.Columns.Add("Device", typeof(string));
        data.Columns.Add("Categories", typeof(string));
        data.Columns.Add("s_image", typeof(string));
       data.Columns.Add("SupplierStatusClick", typeof(string));
        data.Columns.Add("InactiveReason");
        data.Columns.Add("UserId");

       
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(this);
       ClipCallReport.ResultOfSearchAppSuppliersResponse result = null;
        try
        {
            result = ccreport.SearchSuppliersApp(_request);

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            return null;
        }

        if (result.Type == ClipCallReport.eResultType.Failure)
            return null;

        foreach (ClipCallReport.SupplierAppRow ssr in result.Value.Rows)
        {            
            DataRow row = data.NewRow();
            row["supplierid"] = ssr.SupplierId.ToString();
            row["Name"] = string.IsNullOrEmpty(ssr.Name) ? "***" : ssr.Name;
            row["FullName"] = ssr.FullName;
            row["Phone"] = ssr.Phone;
            row["Email"] = ssr.Email;
            row["Device"] = ssr.Device;
            row["Categories"] = ssr.Categories;


            row["s_image"] = GetRamzorImage(ssr.SupplierStatus);

            if (ssr.SupplierStatus == ClipCallReport.eSupplierStatusAppRegistration.Inactive)
            {
                if (ssr.InactiveReason.HasValue &&
                    (ssr.InactiveReason.Value == ClipCallReport.eInactiveReason.Blocked || ssr.InactiveReason.Value == ClipCallReport.eInactiveReason.NotConfirmedServiceLocation))
                    row["SupplierStatusClick"] = "return active(this, true);";
                else
                    row["SupplierStatusClick"] = "return false;";
            }
            else
                row["SupplierStatusClick"] = "return active(this, false);";
            row["InactiveReason"] = ssr.InactiveReason.HasValue ? Utilities.GetEnumStringFormat(ssr.InactiveReason.Value) : null;
            row["UserId"] = ssr.UserId;
            
         //   row["SupplierStatusClick"] = "return false;";
            data.Rows.Add(row);
        }
        return new DataResult() { data = data, CurrentPage = result.Value.CurrentPage, TotalPages = result.Value.TotalPages, TotalRows = result.Value.TotalRows };
    }
    protected void _GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    string GetRamzorImage(ClipCallReport.eSupplierStatusAppRegistration _status)
    {

        if (_status == ClipCallReport.eSupplierStatusAppRegistration.Approved)
            return "../images/available.png";
        if (_status == ClipCallReport.eSupplierStatusAppRegistration.Inactive)
            return "../images/innactive.png";
        if (_status == ClipCallReport.eSupplierStatusAppRegistration.InRegistration)
            return "../images/candidate.png";
        /*
        if (_status == ClipCallReport.eSupplierStatusAppRegistration.Old_Pro)
            return "../images/unavailable.png";
         * */

        return string.Empty;
    }
    private void LoadCategories()
    {
        ddl_Category.Items.Clear();
        ddl_Category.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (KeyValuePair<Guid, string> kvp in PpcSite.GetCurrent().Categories)
        {
            ListItem li = new ListItem(kvp.Value, kvp.Key.ToString());
            ddl_Category.Items.Add(li);
        }
        ddl_Category.SelectedIndex = 0;
    }

    private void LoadDevices()
    {
        ddl_Device.Items.Clear();
        foreach (ClipCallReport.eMobileDevice device in Enum.GetValues(typeof(ClipCallReport.eMobileDevice)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(device), device.ToString());
            li.Selected = device == ClipCallReport.eMobileDevice.ALL;
            ddl_Device.Items.Add(li);
        }
    }

    private void LoadStatusUsers()
    {
        ddl_SupplierStatus.Items.Clear();
        foreach(ClipCallReport.eSupplierStatusAppRegistration status in Enum.GetValues(typeof(ClipCallReport.eSupplierStatusAppRegistration)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(status), status.ToString());
            li.Selected = status == ClipCallReport.eSupplierStatusAppRegistration.ALL;
            ddl_SupplierStatus.Items.Add(li);
        }
    }
    private void TablePagingConfiguration()
    {
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        //   TablePaging1.PagesInPaging = PAGE_PAGES;
        //   TablePaging1.ItemInPage = ITEM_PAGE;
    }
    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (SearchSuppliersRequestV == null)
            return;
        ClipCallReport.SearchAppSuppliersRequest _request = SearchSuppliersRequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = ITEM_PAGE;//.ItemInPage;
        LoadTable(_request);
    }
    protected void btn_Edit_click(object sender, EventArgs e)
    {
        //System.Web.SessionState.HttpSessionState d = new System.Web.SessionState.HttpSessionState();         
        ClearAccountStatus();
        //   IsBack = true;
        LinkButton btn = (LinkButton)sender;
        //btn.Attributes.Add("onClick", "$(document).ready(function(){$.colorbox({href:'framepublisher.aspx',width:'90%', height:'90%', iframe:true});});");

        //Response.Write(btn.ClientID);
        GridViewRow row = (GridViewRow)btn.NamingContainer;
        string name = btn.CommandArgument;
        string _id = ((Label)row.FindControl("lbl_SupplierId")).Text;
        string _code = string.Empty;// ((LinkButton)ri.FindControl("lb_SupplierNum")).Text;
        string _email = ((Label)row.FindControl("lbl_email")).Text;
        UserMangement um = new UserMangement(new Guid(_id), name, _code, _email);
        SetGuidSetting(um);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "openPopup",
            @"$(document).ready(function(){$.colorbox({href:'framepublisher.aspx',width:'95%', height:'90%', iframe:true});});", true);


        _UpdatePanel.Update();

    }
    void ClearAccountStatus()
    {
        Session["Account_Status"] = null;
    }
    void Master_newAdvEvent(object sender, EventArgs e)
    {
        return;

    }
    void ClearTable()
    {
        _GridView.DataSource = null;
        _GridView.DataBind();
        LoadGeneralData(0, 0, 0);
        _UpdatePanel.Update(); 
        PanelResults.Visible = false;
        div_status.Visible = false;
    }
    protected void BindData(DataTable data)
    {
        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_Count.Text = _TotalRows.ToString();
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
        PanelResults.Visible = true;
        div_status.Visible = true;
        _UpdatePanelTotalResult.Update();
    }
    protected override void Render(HtmlTextWriter writer)
    {

        Page.ClientScript.RegisterForEventValidation(btn_virtual_run.UniqueID, RUN_REPORT);
        base.Render(writer);
    }
    protected string RunReport()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(btn_virtual_run, RUN_REPORT);
        myPostBackOptions.PerformValidation = false;
   //     return ScriptManager.g
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
        //   WebReferenceSite.FundsStatus
    }
    ClipCallReport.SearchAppSuppliersRequest SearchSuppliersRequestV
    {
        get
        {
            return (Session["SearchSuppliersRequest"] == null) ? null : (ClipCallReport.SearchAppSuppliersRequest)Session["SearchSuppliersRequest"];
        }
        set { Session["SearchSuppliersRequest"] = value; }

    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
    protected string InactiveSupplierUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallAdvertiserReport2.aspx/InactiveSupplier"); }
    }
    protected string ActiveSupplierUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallAdvertiserReport2.aspx/ActiveSupplier"); }
    }
    
}