﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="DefaultWorkingTimes.aspx.cs" Inherits="Publisher_DefaultWorkingTimes" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="style.css" rel="stylesheet" type="text/css"/>
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../general.js"></script>
<script type="text/javascript" language="javascript">
    function cb_click(chkbox, panelId)
    {
        var _panel=document.getElementById(panelId);
        _panel.style.visibility = (chkbox.checked)?"hidden":"visible";       
    }
    function checkTimes(ddlFrom, ddlTo)
    {
        var ddl_from=document.getElementById(ddlFrom);
        var ddl_to=document.getElementById(ddlTo);
        if(ddl_from.selectedIndex>ddl_to.selectedIndex)
        {            
            ddl_to.selectedIndex = ddl_from.selectedIndex;
            
        }        
    }


</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager> 
<div class="page-content minisite-content" >
    <h2><asp:Label ID="lblSubTitle" runat="server">Default working hours</asp:Label></h2>
    <div id="form-analyticsseg">
        <asp:Repeater ID="_Repeater" runat="server">
            <HeaderTemplate>
                <table class="data-table">
                    <thead>
                        <tr>
                            <th>
                                <asp:Label ID="Label1" runat="server" Text="<%# lbl_day.Text %>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label2" runat="server" Text="<%# lbl_hours.Text %>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label3" runat="server" Text="<%# lbl_suspend.Text %>"></asp:Label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
            <ItemTemplate>
                    <tr id="tr_body" runat="server">
                        <td>
                            <asp:Label ID="lblDay" runat="server" Text="<%# Bind('Day') %>"></asp:Label>
                            <asp:Label ID="lblDayId" runat="server" Text="<%# Bind('DayId') %>" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:Panel ID="div_times" runat="server">
                            <p>   
                                <asp:DropDownList runat="server" id="ddl_From"  AutoPostBack="false"  EnableViewState="false" CssClass="form-select">                
                                    <asp:ListItem  Value="00:00">00:00</asp:ListItem>
                                    <asp:ListItem  Value="01:00">01:00</asp:ListItem>
                                    <asp:ListItem  Value="02:00">02:00</asp:ListItem>
                                    <asp:ListItem  Value="03:00">03:00</asp:ListItem>
                                    <asp:ListItem  Value="04:00">04:00</asp:ListItem>
                                    <asp:ListItem  Value="05:00">05:00</asp:ListItem>
                                    <asp:ListItem  Value="06:00">06:00</asp:ListItem>
                                    <asp:ListItem  Value="07:00">07:00</asp:ListItem>
                                    <asp:ListItem  Value="08:00">08:00</asp:ListItem>
                                    <asp:ListItem  Value="09:00">09:00</asp:ListItem>
                                    <asp:ListItem  Value="10:00">10:00</asp:ListItem>
                                    <asp:ListItem  Value="11:00">11:00</asp:ListItem>
                                    <asp:ListItem  Value="12:00">12:00</asp:ListItem>
                                    <asp:ListItem  Value="13:00">13:00</asp:ListItem>
                                    <asp:ListItem  Value="14:00">14:00</asp:ListItem>
                                    <asp:ListItem  Value="15:00">15:00</asp:ListItem>
                                    <asp:ListItem  Value="16:00">16:00</asp:ListItem>
                                    <asp:ListItem  Value="17:00">17:00</asp:ListItem>
                                    <asp:ListItem  Value="18:00">18:00</asp:ListItem>
                                    <asp:ListItem  Value="19:00">19:00</asp:ListItem>
                                    <asp:ListItem  Value="20:00">20:00</asp:ListItem>
                                    <asp:ListItem  Value="21:00">21:00</asp:ListItem>
                                    <asp:ListItem  Value="22:00">22:00</asp:ListItem>
                                    <asp:ListItem  Value="23:00">23:00</asp:ListItem>                            
                                </asp:DropDownList>

                                <asp:Label ID="lbl_To" runat="server" Text="<%# lbl_To.Text %>" ></asp:Label>                        
                        
                                <asp:DropDownList runat="server" id="ddl_To" CssClass="form-select" >
                                    <asp:ListItem  Value="01:00">01:00</asp:ListItem>
                                    <asp:ListItem  Value="02:00">02:00</asp:ListItem>
                                    <asp:ListItem  Value="03:00">03:00</asp:ListItem>
                                    <asp:ListItem  Value="04:00">04:00</asp:ListItem>
                                    <asp:ListItem  Value="05:00">05:00</asp:ListItem>
                                    <asp:ListItem  Value="06:00">06:00</asp:ListItem>
                                    <asp:ListItem  Value="07:00">07:00</asp:ListItem>
                                    <asp:ListItem  Value="08:00">08:00</asp:ListItem>
                                    <asp:ListItem  Value="09:00">09:00</asp:ListItem>
                                    <asp:ListItem  Value="10:00">10:00</asp:ListItem>
                                    <asp:ListItem  Value="11:00">11:00</asp:ListItem>
                                    <asp:ListItem  Value="12:00">12:00</asp:ListItem>
                                    <asp:ListItem  Value="13:00">13:00</asp:ListItem>
                                    <asp:ListItem  Value="14:00">14:00</asp:ListItem>
                                    <asp:ListItem  Value="15:00">15:00</asp:ListItem>
                                    <asp:ListItem  Value="16:00">16:00</asp:ListItem>
                                    <asp:ListItem  Value="17:00">17:00</asp:ListItem>
                                    <asp:ListItem  Value="18:00">18:00</asp:ListItem>
                                    <asp:ListItem  Value="19:00">19:00</asp:ListItem>
                                    <asp:ListItem  Value="20:00">20:00</asp:ListItem>
                                    <asp:ListItem  Value="21:00">21:00</asp:ListItem>
                                    <asp:ListItem  Value="22:00">22:00</asp:ListItem>
                                    <asp:ListItem  Value="23:00">23:00</asp:ListItem>
                                    <asp:ListItem  Value="23:59">23:59</asp:ListItem>
                                </asp:DropDownList> 
                            </p> 
                            </asp:Panel>     
                        </td>
                        <td>
                            <asp:CheckBox ID="cb_suspend" CssClass="form-checkbox" runat="server" Checked="<%# Bind('IsSuspend') %>"/> 
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <asp:Button ID="btn_set" runat="server" Text="Set" CssClass="form-submit" OnClick="btn_set_click"/>
</div>

<asp:Label ID="lbl_day" runat="server" Text="Day" Visible="false"></asp:Label>
<asp:Label ID="lbl_hours" runat="server" Text="Hours" Visible="false"></asp:Label>
<asp:Label ID="lbl_suspend" runat="server" Text="Suspend" Visible="false"></asp:Label>
<asp:Label ID="lbl_To" runat="server" Text="To" Visible="false"></asp:Label>
    
</asp:Content>

