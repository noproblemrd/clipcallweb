﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" 
EnableEventValidation="false"
CodeFile="Holidays.aspx.cs" Inherits="Publisher_Holidays" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>

<%@ Register src="../Controls/FromToDateTime.ascx" tagname="FromToDateTime" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript"  src="../CallService.js"></script>
<script type="text/javascript" >
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }
    /*****  Popup  *******/
    function closePopup() {
        ClearPopup();
        $find('_modal').hide();
    }
    function openPopup() {
        ClearPopup();
        $find('_modal').show();
    }
    function ClearPopup() {
        document.getElementById("<%# hf_HolidayId.ClientID %>").value = "";
        document.getElementById("<%# txt_Name.ClientID %>").value = "";
        <%# FromToDateTime1.GetClientScriptResetValues() %>;
        var validators = Page_Validators;        
        for (var i = 0;i < Page_Validators.length;  i++)
        {
            if(Page_Validators[i].validationGroup == "AddHoliday")
			    Page_Validators[i].style.visibility = "hidden";
	    }         
    }

    function CheckValidation() {
        var _result = <%# GetScriptIfDateNotSooner %>;
        if(!Page_ClientValidate('AddHoliday'))
            return false;
        if(!_result)
            alert("<%# PastDateMessage %>");
        return _result;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
    <uc1:Toolbox ID="Toolbox1" runat="server" />
    
    <h5>
	    <asp:Label ID="lblSubTitle" runat="server">Closure Dates</asp:Label>
	</h5>
<div class="page-content minisite-content5">

			
	    <div id="form-analyticsseg">
	   
			<asp:UpdatePanel ID="_updatePanelTable" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
			
                <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" ShowFooter="false" 
                CssClass="data-table" Width="650">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                 <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                <FooterStyle CssClass="footer"  />
                
                <Columns>                     
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                    <br />
                    <asp:CheckBox ID="cb_all" runat="server" >
                    </asp:CheckBox>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cb_choose" runat="server" />
                    
                </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="<%# lbl_Name.Text %>"></asp:Label> 
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbl___Name" runat="server" Text="<%# Bind('Name') %>"></asp:Label>
                    <asp:Label ID="lbl_HolidayId" runat="server" Text="<%# Bind('ClosureDateId') %>" Visible="false"></asp:Label> 
                </ItemTemplate>                
                </asp:TemplateField>
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label3" runat="server" Text="<%# lbl_FromDate.Text %>"></asp:Label> 
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbl_from_date" runat="server" Text="<%# Bind('FromDate') %>"></asp:Label>
                    <asp:Label ID="lbl_from_time" runat="server" Text="<%# Bind('FromTime') %>"></asp:Label>
                </ItemTemplate>                
                </asp:TemplateField>

                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label4" runat="server" Text="<%# lbl_ToDate.Text %>"></asp:Label> 
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbl_to_date" runat="server" Text="<%# Bind('ToDate') %>"></asp:Label>
                    <asp:Label ID="lbl_to_time" runat="server" Text="<%# Bind('ToTime') %>"></asp:Label>
                </ItemTemplate>                
                </asp:TemplateField>
                
                </Columns>
             </asp:GridView>
             </ContentTemplate>
             </asp:UpdatePanel>

         </div>
 </div>    

 <div id="popUpaddHoliday" runat="server" style="min-height:300px;display:none;" class="popModal_del"  >
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
    <div class="content" >
        <div class="modaltit">
            <h2><asp:Label ID="lbl_AddClosureDate" runat="server" Text="Closure Date Form"></asp:Label></h2>
        </div>
        <uc2:FromToDateTime ID="FromToDateTime1" runat="server" CssFromTextbox="form-textcal" CssToTextbox="form-textcal"/>
        <div>            
            <asp:Label ID="lblName" runat="server" Text="Name" CssClass="label"></asp:Label>
            <asp:TextBox ID="txt_Name" runat="server" CssClass="txtName"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator_Name" runat="server" ErrorMessage="Missing"
             CssClass="error-msg" ControlToValidate="txt_Name" ValidationGroup="AddHoliday"></asp:RequiredFieldValidator>
        </div>
        <asp:Button ID="btn_Set" runat="server" CssClass="form-submit" Text="Set" OnClick="btn_Set_click" OnClientClick="return CheckValidation();"
        ValidationGroup="AddHoliday" />
        <asp:HiddenField ID="hf_HolidayId" runat="server" />

    </div>
    <div class="bottom2"></div>
</div>

<cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btn_virtual"
    PopupControlID="popUpaddHoliday"
    BackgroundCssClass="modalBackground"               
    BehaviorID="_modal"               
    DropShadow="false"
        ></cc1:ModalPopupExtender>

<div style="display:none;">
    <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />                     
</div>

   <asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
 <asp:Label ID="lbl_Name" runat="server" Text="Name" Visible="false"></asp:Label>
 <asp:Label ID="lbl_FromDate" runat="server" Text="From date" Visible="false"></asp:Label>
 <asp:Label ID="lbl_ToDate" runat="server" Text="To date" Visible="false"></asp:Label>
  <asp:Label ID="lbl_DatePast" runat="server" Text="You can not insert closure Date in the past" Visible="false"></asp:Label>

</asp:Content>


