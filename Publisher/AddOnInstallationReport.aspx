﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AddOnInstallationReport.aspx.cs" Inherits="Publisher_AddOnInstallationReport" %>

<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />

<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js"></script>
<script type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>

<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        Set_a_Collapse_position();
        hideDiv();
    }
   
    function SetTrTable() {
      //  var i = 0;
        $('.data-table').find('tr').each(function () {
            /*
            $(this).addClass((i % 2 == 0) ? 'odd' : 'even');
            i++;
            */
            if ($(this).find('th').length > 0)
                return;
            $(this).addClass('tr_pointer');
            var ItOpen = false;
            var _this = this;
            $(this).click(function () {
                if (!ItOpen) {
                    showDiv();

                    var _date = $(this).find('input[type="hidden"]').val();
                    
                    $.ajax({
                        url: '<%# GetDrillDownPath %>',
                        data: "{ 'date': '" + _date + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            if (data.d.length == 0)
                                return;
                            ItOpen = true;
                            var $NewTr = $('<tr>');
                            var $NewTd = $('<td colspan="24">');
                            $NewTd.html(data.d);
                            $NewTr.append($NewTd);
                            $(_this).after($NewTr);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {

                        },
                        complete: function (jqXHR, textStatus) {

                            hideDiv();
                        }

                    });
                }
                else {
                    var $tr = $(_this).next('tr');
                    if ($tr.is(':visible'))
                        $tr.hide();
                    else
                        $tr.show();
                }
            });
        });
    }
    $(function () {
        Set_a_Collapse_position();
        
    });
    function Set_a_Collapse_position() {
        var _left = $('.Toggle_Possition').get(0).offsetLeft;
        _left += 2;
        $('#a_Collapse').css('left', _left + 'px');
        $('#a_Collapse').unbind('click');
        $('#a_Collapse').click(function () {
            if ($(this).html() == '+ Expand') {
                $('.t_Collapse').each(function () {
                    $(this).show();
                });
                $(this).html('-­ Collapse');
            }
            else {
                $('.t_Collapse').each(function () {
                    $(this).hide();
                });
                $(this).html('+ Expand');
            }
        });
    }
    function LoadChart(fileXML, _chart) {

        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render("div_chart");
    }
    function CleanChart() {
        document.getElementById("div_chart").innerHTML = "";
    }
</script>
<style type="text/css">
    .t_Collapse
    {
        display:none;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" >
    
</cc1:ToolkitScriptManager>
<uc1:ToolboxReport ID="_Toolbox" runat="server" />
<div class="page-content minisite-content2"> 
  <div id="form-analytics">
    	       
     
               
            <div class="form-field" style="width:auto;">
                <asp:Label ID="lbl_Origin" runat="server" CssClass="label" Text="Campaign"></asp:Label>   
                <asp:ListBox ID="lb_Origin" runat="server" CssClass="form-select-multi" SelectionMode="Multiple" style="width:320px;"></asp:ListBox>
	           
            </div>
           
            

        
       
        <div class="callreport">
              
             <uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" />
        </div>
        <div class="clear"></div>
         <div class="form-field" style="left: 340px; position: absolute; top: 83px;">
                <asp:Label ID="lbl_Country" runat="server" CssClass="label" Text="Geo"></asp:Label>   
                <asp:DropDownList ID="ddl_Country" runat="server" CssClass="form-select">
                </asp:DropDownList>
            </div>
            <div class="form-field">
                <asp:Button ID="btn_GroupByIp" runat="server" Text="Generate Installs By IP Report" 
                    onclick="btn_GroupByIp_Click" />
            </div>
        <div class="clear"></div>
        
            
            <div class="table2" style="width:100%;">
            
                
                    <div runat="server" id="div_container_chart" class="div_container_chart">
                       <div id="div_chart" style="float:left;"></div>
                       <div class="div_cbl_chart">  
                           <asp:CheckBoxList ID="cbl_chart" runat="server"></asp:CheckBoxList>                   
                        </div>
                    </div>
                <div class="clear"></div>
                <div class="div_BtnInstallationReport">
                <asp:Button ID="btn_Run" runat="server" CssClass="CreateReportSubmit2" 
                    Text="Create report" onclick="btn_Run_Click" 
                    />
            </div>
            
           <div class="clear"></div>
               <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                <div runat="server" id="div_table">                 
                  <div class="results result_noData" runat="server" id="div_noResults" visible="false">
                        <asp:Label ID="lbl_ThereAreNoData" runat="server" Text="There are no data to display"></asp:Label>      
                        
                    </div>
                <div class="clear"></div>
                
                <a href="javascript:void(0);" id="a_Collapse" class="a_Toggle_Collapse">+ Expand</a>
                
                <div class="clear"></div>

                <div class="div_gridMyCall">
                   
                    <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                        CssClass="data-table"  >
                    <RowStyle CssClass="_even" />
                    <AlternatingRowStyle CssClass="_odd" />
                    <Columns>
                        <asp:TemplateField SortExpression="Date">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# lbl_Date.Text %>'></asp:Label> 
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Date") %>'></asp:Label>        
                                 <asp:HiddenField ID="hf_Date" runat="server" Value='<%# Bind("DateLng") %>' />              
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Install">
                            <HeaderTemplate>
                                <asp:Label ID="Label1020" runat="server" Text='<%# lbl_Install.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2020" runat="server" Text='<%# Bind("Install") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField SortExpression="Uniqe_Installs">
                            <HeaderTemplate>
                                <asp:Label ID="Label010" runat="server" Text='<%# lbl_UniqeInstall.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label002" runat="server" Text='<%# Bind("Uniqe_Installs") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField SortExpression="Effective_Installs">
                            <HeaderTemplate>
                                <asp:Label ID="Label910" runat="server" Text='<%# lbl_EffectiveInstalls.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label902" runat="server" Text='<%# Bind("Effective_Installs") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Total_Installs">
                            <HeaderTemplate>
                                <asp:Label ID="Label80" runat="server" Text='<%# lbl_TotalInstalls.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label82" runat="server" Text='<%# Bind("Total_Installs") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Uninstalls">
                            <HeaderTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# lbl_Uninstall.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Uninstalls") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="DAUS">
                            <HeaderTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# lbl_DAUS.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("DAUS") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField SortExpression="DAUs_Percent">
                            <HeaderTemplate>
                                <asp:Label ID="Label85" runat="server" Text='<%# lbl_DAUS_Percent.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label86" runat="server" Text='<%# Bind("DAUs_Percent") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                       

                        <asp:TemplateField SortExpression="NP_Revenue">
                            <HeaderTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# lbl_NPRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%# Bind("NP_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Non_NP_Revenue" HeaderStyle-CssClass="Toggle_Possition">
                            <HeaderTemplate>
                                
                                <asp:Label ID="Label89" runat="server" Text='<%# lbl_NonNPRevenue.Text %>'></asp:Label>                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label90" runat="server" Text='<%# Bind("Non_NP_Revenue") %>'></asp:Label>                                 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Dealply_Revenue" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label11" runat="server" Text='<%# lbl_DealplyRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%# Bind("Dealply_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Jollywallet_Revenue" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%# lbl_JollywalletRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label14" runat="server" Text='<%# Bind("Jollywallet_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Inter_Yield_Revenue" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label15" runat="server" Text='<%# lbl_InterYieldRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label16" runat="server" Text='<%# Bind("Inter_Yield_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Superfish_Revenue" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label17" runat="server" Text='<%# lbl_SuperfishRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label18" runat="server" Text='<%# Bind("Superfish_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField SortExpression="Revizer_Popup_Revenue" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label117" runat="server" Text='<%# lbl_RevizerPopupRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label118" runat="server" Text='<%# Bind("Revizer_Popup_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Intext_Revenue" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label1217" runat="server" Text='<%# lbl_IntextRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1218" runat="server" Text='<%# Bind("Intext_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="First_Offerz_Revenue" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label1267" runat="server" Text='<%# lbl_FirstOfferzRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1268" runat="server" Text='<%# Bind("First_Offerz_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField SortExpression="Admedia_Intext_Revenue" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label1367" runat="server" Text='<%# lbl_AdmediaIntext.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1368" runat="server" Text='<%# Bind("Admedia_Intext_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Adcash_Revenue" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label13671" runat="server" Text='<%# lbl_AdcashRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label13681" runat="server" Text='<%# Bind("Adcash_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField SortExpression="Adcash_Engageya" HeaderStyle-CssClass="t_Collapse" ItemStyle-CssClass="t_Collapse">
                            <HeaderTemplate>
                                <asp:Label ID="Label13672" runat="server" Text='<%# lbl_EngageyaRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label13682" runat="server" Text='<%# Bind("Engageya_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Total_Revenue">
                            <HeaderTemplate>
                                <asp:Label ID="Label19" runat="server" Text='<%# lbl_TotalRevenue.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label20" runat="server" Text='<%# Bind("Total_Revenue") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Cost">
                            <HeaderTemplate>
                                <asp:Label ID="Label21" runat="server" Text='<%# lbl_Cost.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label22" runat="server" Text='<%# Bind("Cost") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="AULT_Total">
                            <HeaderTemplate>
                                <asp:Label ID="Label91" runat="server" Text='<%# lbl_AULT.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label92" runat="server" Text='<%# Bind("AULT_Total") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="AMUV_NoProblem">
                            <HeaderTemplate>
                                <asp:Label ID="Label93" runat="server" Text='<%# lbl_AMUV_NoProblem.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label94" runat="server" Text='<%# Bind("AMUV_NoProblem") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="DUV_NoProblem">
                            <HeaderTemplate>
                                <asp:Label ID="Label95" runat="server" Text='<%# lbl_DUV_NoProblem.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label96" runat="server" Text='<%# Bind("DUV_NoProblem") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="AMUV_Total">
                            <HeaderTemplate>
                                <asp:Label ID="Label97" runat="server" Text='<%# lbl_AMUV.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label98" runat="server" Text='<%# Bind("AMUV_Total") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="DUV_Total">
                            <HeaderTemplate>
                                <asp:Label ID="Label103" runat="server" Text='<%# lbl_DUV.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label104" runat="server" Text='<%# Bind("DUV_Total") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="ROI">
                            <HeaderTemplate>
                                <asp:Label ID="Label23" runat="server" Text='<%# lbl_ROI.Text %>'></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label24" runat="server" Text='<%# Bind("ROI") %>'></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    </asp:GridView>
                </div>
                   
                    </div>
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:Label ID="lbl_TitleReport" runat="server" Text="Installation Report" Visible="false"></asp:Label>

    <asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Install" runat="server" Text="Installs" Visible="false"></asp:Label>
    <asp:Label ID="lbl_UniqeInstall" runat="server" Text="Unique Installs" Visible="false"></asp:Label>
    <asp:Label ID="lbl_EffectiveInstalls" runat="server" Text="Effective Installs" Visible="false"></asp:Label>

    <asp:Label ID="lbl_TotalInstalls" runat="server" Text="Total Installs" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Uninstall" runat="server" Text="Uninstalls" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DAUS" runat="server" Text="DAUs" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DAUS_Percent" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lbl_NPRevenue" runat="server" Text="NP Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_NonNPRevenue" runat="server" Text="NonNP Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DealplyRevenue" runat="server" Text="Dealply Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_JollywalletRevenue" runat="server" Text="Jollywallet Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_InterYieldRevenue" runat="server" Text="newtab rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_SuperfishRevenue" runat="server" Text="Superfish Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_RevizerPopupRevenue" runat="server" Text="RevizerPopup Rev" Visible="false"></asp:Label>
     <asp:Label ID="lbl_IntextRevenue" runat="server" Text="Intext Rev" Visible="false"></asp:Label>
     <asp:Label ID="lbl_FirstOfferzRevenue" runat="server" Text="FirstOfferz rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_AdmediaIntext" runat="server" Text="AdmediaIntext rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_AdcashRevenue" runat="server" Text="Adcash rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_EngageyaRevenue" runat="server" Text="Engageya rev" Visible="false"></asp:Label>
     
    
     
    <asp:Label ID="lbl_TotalRevenue" runat="server" Text="Total Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Cost" runat="server" Text="Cost" Visible="false"></asp:Label>
    <asp:Label ID="lbl_AULT" runat="server" Text="AULT" Visible="false"></asp:Label>
    <asp:Label ID="lbl_AMUV_NoProblem" runat="server" Text="NP AMUV" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DUV_NoProblem" runat="server" Text="NP DUV" Visible="false"></asp:Label>
    <asp:Label ID="lbl_AMUV" runat="server" Text="Total AMUV" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DUV" runat="server" Text="Total DUV" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ROI" runat="server" Text="ROI" Visible="false"></asp:Label>

    <asp:Label ID="lbl_AverageUserLifeTime" runat="server" Text="Average User Life Time" Visible="false"></asp:Label>
  


</asp:Content>

