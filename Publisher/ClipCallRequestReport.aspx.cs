﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class Publisher_ClipCallRequestReport : PageSetting
{
    delegate DataTable AsyncGetDataTable(WebReferenceReports.RequestReportResponse[] resultRequestReport);
    const int DAYS_INTERVAL = 1;
    protected const string LOW_IMPORTANCE = "~/publisher/images/Low-Importance-48.png";
    protected const string HIGH_IMPORTANCE = "~/publisher/images/High-Importance-48.png";
    protected string GetLowImportanceUrl
    {
        get
        {
            return LOW_IMPORTANCE.Substring(1);
        }
    }
    protected string GetHighImportanceUrl
    {
        get
        {
            return HIGH_IMPORTANCE.Substring(1);
        }
    }
    protected const string LowImportance = "LowImportance";
    protected const string HighImportance = "HighImportance";

    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    protected readonly string EXCEL_NAME = "ClipCallRequestReport";


    protected readonly string SessionTableName = "data_CCRR";

    protected string GetCustomerRequestReportUrl(Guid customerId)
    {

        return string.Format(Request.Url.AbsolutePath + "?customerid={0}", customerId);

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());


        //      TablePagingConfiguration();
        if (!IsPostBack)
        {
            //  RequestsReportRequestV = null;

            SetDateInterval();
            LoadExperties();
            LoadPortalRejectReason();
            LoadRequestStatuses();
            LoadAarInitiated();
            LoadTypes();
            LoadAppTypes();
            LoadChannels();
            LoadCustomerLeadStatus();
            LoadDateSelector();
            LoadOrigins();
            LoadAssessment();
            LoadWarnings();
            Clearreports();
            Guid customerId;
            if (Guid.TryParse(Request.QueryString["customerid"], out customerId))
                ExecRequestReport(customerId: customerId);
            else
            {
                Guid supplierId;
                if (Guid.TryParse(Request.QueryString["supplierid"], out supplierId))
                    ExecRequestReport(supplierId: supplierId);
                else
                    ExecRequestReport();
            }
        }
        SetToolbox();
        //  SetToolboxEvents();
        Header.DataBind();
        rev_SupplierPhone.DataBind();
    }

    private void LoadWarnings()
    {
        ddl_warning.Items.Clear();
        foreach(WebReferenceReports.eWarningType val in Enum.GetValues(typeof(WebReferenceReports.eWarningType)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(val), val.ToString());
            li.Selected = val == WebReferenceReports.eWarningType.All;
            ddl_warning.Items.Add(li);
        }
    }

    private void LoadAssessment()
    {
        rbl_assessment.Items.Clear();
        ListItem liall = new ListItem("EMPTY", string.Empty);
        rbl_assessment.Items.Add(liall);
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfLeadAssessment result = null;
        try
        {
            result = _site.GetLeadAssessment();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        foreach(WebReferenceSite.LeadAssessment leadAssessment in result.Value)
        {
            ListItem li = new ListItem(leadAssessment.AssessmentDescription, leadAssessment.Assessment.ToString());
            string _css = "assessmentBorder ";
            li.Attributes.Add("class", _css + "Border" + leadAssessment.Assessment.ToString());
            rbl_assessment.Items.Add(li);
        }
    }
    private void LoadOrigins()
    {

        ddl_Origin.Items.Clear();
        ListItem liall = new ListItem("All", string.Empty);
        liall.Selected = true;
        ddl_Origin.Items.Add(liall);
        foreach (WebReferenceReports.eProjectOrigin _origin in Enum.GetValues(typeof(WebReferenceReports.eProjectOrigin)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(_origin), _origin.ToString());
            ddl_Origin.Items.Add(li);
        }
    }
    private void LoadPortalRejectReason()
    {
        ClipCallReport.ClipCallReport ccr = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfListOfKeyValuePairIntString result = null;
        try
        {
            result = ccr.GetPortalreviewRejectReason();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        rbl_RejectReason.Items.Clear();
        foreach(ClipCallReport.KeyValuePairIntString kvp in result.Value)
        {
            ListItem li = new ListItem(kvp.Value, kvp.Key.ToString());
            rbl_RejectReason.Items.Add(li);
        }
    }
    private void LoadDateSelector()
    {
        rbl_date.Items.Clear();
        foreach (WebReferenceReports.eRequestReportDatesFilter dateFilter in Enum.GetValues(typeof(WebReferenceReports.eRequestReportDatesFilter)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(dateFilter), dateFilter.ToString());
            li.Selected = dateFilter == WebReferenceReports.eRequestReportDatesFilter.Creation;
            rbl_date.Items.Add(li);
        }
    }
    private void LoadCustomerLeadStatus()
    {
        ddl_CustomerLeadStatus.Items.Clear();
        foreach (WebReferenceReports.eCustomerLeadStatus status in Enum.GetValues(typeof(WebReferenceReports.eCustomerLeadStatus)))
        {
            ListItem li = new ListItem(GetRequestsStatusText(status), status.ToString());
            ddl_CustomerLeadStatus.Items.Add(li);
        }
        ddl_CustomerLeadStatus.SelectedIndex = 0;
    }

    private void LoadAarInitiated()
    {
        this.ddl_AarInitiated.Items.Clear();
        ddl_AarInitiated.Items.Add(new ListItem("All", ""));
        ddl_AarInitiated.Items.Add(new ListItem("Yes", "true"));
        ddl_AarInitiated.Items.Add(new ListItem("No", "false"));
    }

    private void LoadRequestStatuses()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        ddl_RequestStatus.Items.Clear();
        WebReferenceSite.ResultOfListOfeRequestsStatus result = null;
        try
        {
            result = _site.GetMobileAppStatuses();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        foreach (WebReferenceSite.eRequestsStatus status in result.Value)
        {
            ListItem li = new ListItem(GetRequestsStatusText(status), status.ToString());
            li.Selected = status == WebReferenceSite.eRequestsStatus.NON_CLOSE__FAILED___;
            ddl_RequestStatus.Items.Add(li);
        }
    }
    string GetRequestsStatusText<T>(T status) where T : struct
    {
        string result = status.ToString().Replace("___", ")");
        result = result.Replace("__", " (");
        result = result.Replace("_", " ");
        return result;
    }
    private void LoadTypes()
    {
        //TODO
    }




    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
    /*
    private void TablePagingConfiguration()
    {
        TablePaging1._lnkPage_Click += new EventHandler(_Paging_click);
        TablePaging1.PagesInPaging = PAGE_PAGES;
        TablePaging1.ItemInPage = ITEM_PAGE;
    }
     * */
    /*
    protected void _Paging_click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (RequestsReportRequestV == null)
            return;
        WebReferenceReports.RequestsReportRequest _request = RequestsReportRequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        LoadExpertiseTable(_request);
    }
    */
    private void LoadChannels()
    {

    }


    private void LoadAppTypes()
    {

    }

    /*
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }
     * */

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        /*
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }

        WebReferenceReports.RequestsReportRequest _request = RequestsReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataResult dr = _resultRequestReport(_request);
        if (dr == null || dr.data.Rows.Count == 0)
        {
            Update_Faild();
            return;
        }
        SetDataToExport(dr.data);
        ToExcel te = new ToExcel(this, EXCEL_NAME);
        if (!te.ExecExcel(dr.data))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
         * */
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        /*
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        WebReferenceReports.RequestsReportRequest _request = RequestsReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataResult dr = _resultRequestReport(_request);
        if (dr == null || dr.data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        //    SetDataToExport(dr.data);

        Session["data_print"] = dr.data;
        Session["grid_print"] = _reapeter;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
         * */

    }
    void SetDataToExport(DataTable data)
    {
        /*
         *  data.Columns.Add("AdvertiserName");
        data.Columns.Add("guid");
        data.Columns.Add("OnClientClick");
        data.Columns.Add("RegionName");
        data.Columns.Add("CaseScript");
         * */
        data.Columns.Remove("guid");
        data.Columns.Remove("OnClientClick");
        data.Columns.Remove("CaseScript");
        data.Columns.Remove("AdvertiserName");
    }

    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_PageTitle.Text);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow row = (HtmlTableRow)(e.Item.FindControl("tr_show"));
        string css_class = (e.Item.ItemIndex % 2 == 0) ? "odd" : "even";
        row.Attributes.Add("class", css_class);
    }

    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;
    }
    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecRequestReport();
    }
    protected void ExecRequestReport(Guid customerId = default(Guid), Guid supplierId = default(Guid))
    {
        Clearreports();

        DateTime _to = FromToDate1.GetDateTo;
        DateTime _from = FromToDate1.GetDateFrom;
        if (_to == DateTime.MinValue)
        {
            _to = DateTime.Now;
        }
        if (_from == DateTime.MinValue)
        {
            _from = _to.AddMonths(-1);
        }
        else
        {
            if (_from > _to)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + FromToDate1.GetDateError + "');", true);
                return;
            }
        }
        WebReferenceReports.RequestReportRequest requestReportRequest = new WebReferenceReports.RequestReportRequest();
        requestReportRequest.CustomerId = customerId;
        requestReportRequest.SupplierId = supplierId;
        // requestReportRequest.PageSize = ITEM_PAGE;
        requestReportRequest.from = _from;
        requestReportRequest.to = _to;
        bool AarInitiated;
        if (bool.TryParse(ddl_AarInitiated.SelectedValue, out AarInitiated))
            requestReportRequest.aarInitiated = AarInitiated;
        requestReportRequest.category = Guid.Parse(ddl_Heading.SelectedValue);
        if (!string.IsNullOrEmpty(txtRequestId.Text.Trim()))
            requestReportRequest.CaseNumber = txtRequestId.Text.Trim();
        int numOfSupplier;
        if (int.TryParse(txt_NumSupplier.Text, out numOfSupplier) && numOfSupplier > -1)
            requestReportRequest.numberProvidedAdvertisers = numOfSupplier;
        /*
        requestReportRequest.requestStatus = (WebReferenceReports.eRequestsStatus)(Enum.Parse(typeof(WebReferenceReports.eRequestsStatus), ddl_RequestStatus.SelectedValue));

         */
        WebReferenceReports.eRequestsStatus requestStatus;
        if (!Enum.TryParse(ddl_RequestStatus.SelectedValue, out requestStatus))
            requestStatus = WebReferenceReports.eRequestsStatus.ALL;
        requestReportRequest.requestStatus = requestStatus;

        WebReferenceReports.eCustomerLeadStatus customerLeadStatus;
        if (!Enum.TryParse(ddl_CustomerLeadStatus.SelectedValue, out customerLeadStatus))
            customerLeadStatus = WebReferenceReports.eCustomerLeadStatus.NONE;
        requestReportRequest.customerLeadStatus = customerLeadStatus;

        WebReferenceReports.eRequestReportDatesFilter datesFilter;
        if (!Enum.TryParse(rbl_date.SelectedValue, out datesFilter))
            datesFilter = WebReferenceReports.eRequestReportDatesFilter.Creation;
        requestReportRequest.datesFilter = datesFilter;

        WebReferenceReports.eProjectOrigin _origin;
        if (Enum.TryParse(ddl_Origin.SelectedValue, out _origin))
            requestReportRequest.origin = _origin;
        else
            requestReportRequest.origin = null;

        WebReferenceReports.eWarningType warning;
        if (Enum.TryParse(ddl_warning.SelectedValue, out warning))
            requestReportRequest.WarningFilter = warning;
        else
            requestReportRequest.WarningFilter = WebReferenceReports.eWarningType.All;

        if (!string.IsNullOrEmpty(txt_proName.Text.Trim()))
            requestReportRequest.ProPrefix = txt_proName.Text.Trim();


        WebReferenceReports.ResultOfListOfRequestReportResponse _response = null;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        try
        {
            _response = _report.ClipCallRequestReport(requestReportRequest);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (_response.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        WebReferenceReports.RequestReportResponse[] resultRequestReport;
        //    if (userManagement.GetEnumSecurityLevel != SecurityLevel.PUBLISHER1 || !cb_IncludeQaTest.Checked)
        if (!cb_IncludeQaTest.Checked && string.IsNullOrEmpty(requestReportRequest.CaseNumber) && requestReportRequest.CustomerId == Guid.Empty)
        {
            var query = from X in _response.Value
                        where !X.isTestQa
                        select X;
            resultRequestReport = query.ToArray();
        }
        else
            resultRequestReport = _response.Value;
        AsyncGetDataTable asyncData = new AsyncGetDataTable(GetDataTable);// (resultRequestReport);
        AsyncGetDataTable asyncDataExcel = new AsyncGetDataTable(GetDataTableForExcel);// (resultRequestReport);

        IAsyncResult result_Data = asyncData.BeginInvoke(resultRequestReport, null, null);
        IAsyncResult result_DataExcel = asyncDataExcel.BeginInvoke(resultRequestReport, null, null);

        DataTable data = asyncData.EndInvoke(result_Data);
        DataTable dataSession = asyncDataExcel.EndInvoke(result_DataExcel);

        int resultCount = data.Rows.Count;
        lbl_RecordMached.Text = resultCount + " " + ToolboxReport1.RecordMached;
        if (resultCount == 0)
            NotResult();
        BindDataToRepeater(data);
        dataS = dataSession;
    }

    void NotResult()
    {
        Clearreports();
        //      ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
    }

    /*
    void LoadExpertiseTable(WebReferenceReports.RequestsReportRequest requestReportRequest)
    {
        DataResult dr = _resultRequestReport(requestReportRequest);
        if (dr == null)
        {
            Update_Faild();
            return;
        }
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
        if (dr.TotalRows > 0)
            BindDataToRepeater(dr.data);
        else
            NotResult();


    }
     */
    void BindDataToRepeater(DataTable data)
    {
        Table_Report.Visible = true;
        _reapeter.DataSource = data;
        _reapeter.DataBind();
        _UpdatePanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = _TotalRows + " " + ToolboxReport1.RecordMached;
        //    TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    DataTable GetDataTable(WebReferenceReports.RequestReportResponse[] resultRequestReport)
    {

        DataTable data = new DataTable();
        data.Columns.Add("CreatedOn");
        data.Columns.Add("RelaunchDate");
        data.Columns.Add("CaseScript");
        data.Columns.Add("Interactions");
        data.Columns.Add("CaseNumber");
        data.Columns.Add("IncidentId");
        data.Columns.Add("RegionName");
        data.Columns.Add("Heading");
        //  data.Columns.Add("OnConsumerClick");
        data.Columns.Add("ConsumerRequestReport");
        data.Columns.Add("Consumer");
        data.Columns.Add("ConsumerPhone");
        data.Columns.Add("ConsumerLastActive");
        data.Columns.Add("CustomerLeadStatus");
        data.Columns.Add("RelaunchStopBy");
        data.Columns.Add("NumberRequestedAdvertisers", typeof(int));
        data.Columns.Add("NumberAdvertisersProvided", typeof(int));
        //     data.Columns.Add("Revenue");
        //     data.Columns["Revenue"].Caption = "IsNumber";
        data.Columns.Add("CallStatus");
        data.Columns.Add("VideoLink");
        data.Columns.Add("HasVideo", typeof(bool));
        data.Columns.Add("CanReview", typeof(bool));
        data.Columns.Add("CanSendAar", typeof(bool));
        data.Columns.Add("CanMakeRelaunch", typeof(bool));
        data.Columns.Add("CanUseManualAar", typeof(bool));
        data.Columns.Add("CanUnbook", typeof(bool));
        data.Columns.Add("LastUpdate");
        data.Columns.Add("ImportantUrl");
        data.Columns.Add("ImportantAlt");

        data.Columns.Add("CustomerId");
        data.Columns.Add("Origin");
        data.Columns.Add("OS");
        data.Columns.Add("OsVersion");
        data.Columns.Add("AppVersion");
        data.Columns.Add("Carrier");
        data.Columns.Add("Membership");
        data.Columns.Add("IsPrivate", typeof(bool));
        data.Columns.Add("ConfirmIncident");
        data.Columns.Add("IsNotConfirmIncident", typeof(bool));
        data.Columns.Add("IsConfirmIncident", typeof(bool));
        data.Columns.Add("Flow");
        data.Columns.Add("ConsumerProjects");
        data.Columns.Add("LastBotMessage");

        data.Columns.Add("Note");
        data.Columns.Add("NoteToolTip");

        data.Columns.Add("AssessmentAlt");
        data.Columns.Add("AssessmentCss");
        data.Columns.Add("AssessmentValue");

        data.Columns.Add("ChatWarning");
        data.Columns.Add("ProximityDistance");

   //     string ASSESSMENT_FUNC = "return SetAssessment(this, '{0}');";

        foreach (WebReferenceReports.RequestReportResponse _data in resultRequestReport)
        {
            DataRow row = data.NewRow();
            /*
            row["Date"] = string.Format(siteSetting.DateFormat, _data.date) + "</br>" +
                    string.Format(siteSetting.TimeFormat, _data.date);
            row["LastUpdate"] = _data.LastUpdate == DateTime.MinValue ? null : string.Format(siteSetting.DateFormat, _data.LastUpdate) + "</br>" +
                    string.Format(siteSetting.TimeFormat, _data.LastUpdate);
             * */
            row["CustomerId"] = _data.contactId;
            row["CreatedOn"] = _data.createdOn.GetDateForClient();
            row["RelaunchDate"] = _data.relaunchDate.GetDateForClient();
            row["LastUpdate"] = _data.LastUpdate.GetDateForClient();
            string _caseScript = "javascript:OpenIframe('MobileAppRequestTicket.aspx?incidentid=" + _data.incidentId.ToString() + "');";
            row["CaseScript"] = _caseScript;
            row["Interactions"] = "javascript:OpenIframe('ClipCallLeadConnections.aspx?incidentid=" + _data.incidentId.ToString() + "');";
            row["CaseNumber"] = _data.caseNumber;
            row["IncidentId"] = _data.incidentId;
            row["RegionName"] = _data.region;
            row["Heading"] = _data.category;

            //   row["OnConsumerClick"] = "javascript:void(0);";
            row["ConsumerRequestReport"] = GetCustomerRequestReportUrl(_data.contactId);
            row["Consumer"] = _data.contactName;
            row["ConsumerPhone"] = _data.contactPhone;
            row["ConsumerLastActive"] = _data.contactLastActive == default(DateTime) ? -1 : _data.contactLastActive.GetDateForClient();
            row["CustomerLeadStatus"] = _data.CustomerLeadStatus;
            row["NumberRequestedAdvertisers"] = _data.requestedAdvertisers;
            row["NumberAdvertisersProvided"] = _data.advertisersProvided;
            row["RelaunchStopBy"] = _data.RelaunchStopBy;
            row["IsPrivate"] = _data.isPrivate;
            //         if (_data.revenue.HasValue)
            //              row["Revenue"] = string.Format("{0:0}", _data.revenue.Value);
            /*
            if (_data.caseStatus == WebReferenceReports.eRequestsStatus.CLOSE || _data.caseStatus == WebReferenceReports.eRequestsStatus.WAITING_FOR_PHONE_VALIDATION)
                row["CallStatus"] = _data.caseStatus + " (" + _data.reviewStatus + ")";
            else
                row["CallStatus"] = _data.caseStatus;
                */
            row["CallStatus"] = _data.caseStatus;
            bool isVideoChat = _data.caseType == WebReferenceReports.CaseTypeCode.VideoChat;
            if (!string.IsNullOrEmpty(_data.videoUrl))
            {
                GenerateVideoLink gvl = new GenerateVideoLink(_data.videoUrl, _data.picPreviewUrl);
                row["VideoLink"] = gvl.GenerateFullUrl();
                row["HasVideo"] = true;
            }
            else
                row["HasVideo"] = false;
            bool _canReview =  _data.leadStatus == WebReferenceReports.LeadStatus.Inactive ? false : _data.reviewStatus == WebReferenceReports.IncidentReviewStatus.WaitnigForReview && !isVideoChat;
            row["CanReview"] = _canReview;// _data.reviewStatus == WebReferenceReports.IncidentReviewStatus.WaitnigForReview && !isVideoChat;
            if (_canReview)
            {
                if (string.IsNullOrEmpty(_data.category))
                {
                    row["IsNotConfirmIncident"] = true;
                    row["IsConfirmIncident"] = false;
                    row["ConfirmIncident"] = _caseScript;
                }
                else
                {
                    row["IsNotConfirmIncident"] = false;
                    row["IsConfirmIncident"] = true;
                }
                //           row["ConfirmIncident"] = (string.IsNullOrEmpty(_data.category)) ? _caseScript : "javascript:ConfirmRejectIncident(this, true);";
            }
            else
            {
                row["IsNotConfirmIncident"] = false;
                row["IsConfirmIncident"] = false;
            }
            row["CanSendAar"] = _data.CanSendAar;// _data.caseStatus == WebReferenceReports.eRequestsStatus.LIVE && !isVideoChat;
            row["CanMakeRelaunch"] = _data.CanMakeRelaunch; //_data.caseStatus == WebReferenceReports.eRequestsStatus.CLOSE && !isVideoChat;
            row["CanUseManualAar"] = _data.CanUseManualAar;
            row["CanUnbook"] = false;// _data.PossibleUnbook;
            row["ImportantUrl"] = _data.IsImportant ? HIGH_IMPORTANCE : LOW_IMPORTANCE;
            row["ImportantAlt"] = _data.IsImportant ? HighImportance : LowImportance;
            row["Origin"] = _data.Origin;
            row["Carrier"] = _data.CarrierName;
            row["OS"] = _data.OS;
            row["OsVersion"] = _data.OsVersion;
            row["AppVersion"] = _data.AppVersion;
            row["Membership"] = _data.Membership;
            row["Flow"] = _data.Flow;
            row["ConsumerProjects"] = _data.contactProjects;

            row["LastBotMessage"] = _data.LastBotMessage == default(DateTime) ? null : _data.LastBotMessage.GetDateForClient().ToString();

            string shortNote = null;
            if (!string.IsNullOrEmpty(_data.NoteDescription))
                shortNote = _data.NoteDescription.Length > 50 ? _data.NoteDescription.Substring(0, 50) + "..." : _data.NoteDescription;

            row["Note"] = shortNote;
            row["NoteToolTip"] = _data.NoteDescription;

            row["AssessmentAlt"] = _data.leadAssessment != null ? _data.leadAssessment.AssessmentDescription : string.Empty;
            string cssDotassessment = "assdot ";
            if (_data.leadAssessment != null)
            {
                cssDotassessment += ("_" + _data.leadAssessment.Assessment.ToString());
                /*
                switch (_data.leadAssessment.Assessment)
                {
                    case (WebReferenceReports.eAssessment.Black):
                        row["AssessmentCss"] = "dotBlack";
                        break;
                    case (WebReferenceReports.eAssessment.Green):
                        row["AssessmentCss"] = "dotGreen";
                        break;
                    case (WebReferenceReports.eAssessment.Orange):
                        row["AssessmentCss"] = "dotOrange";
                        break;
                    case (WebReferenceReports.eAssessment.Red):
                        row["AssessmentCss"] = "dotEmpty";
                        break;
                }
                */
            }
            else
                cssDotassessment += "_Empty";
            row["AssessmentCss"] = cssDotassessment;
            row["AssessmentValue"] = _data.leadAssessment != null ? _data.leadAssessment.Assessment.ToString() : string.Empty;

            row["ProximityDistance"] = _data.ProximityDistanceMeter.HasValue ? string.Format(this.GetNumberFormat, _data.ProximityDistanceMeter.Value) + "m" : string.Empty;
            row["ChatWarning"] = _data.ChatWarningEvent;
            /*
             *  data.Columns.Add("AssessmentAlt");
    data.Columns.Add("AssessmentUrl");
    data.Columns.Add("AssessmentOnClick");
    */

            data.Rows.Add(row);

        }
        return data;

    }
    DataTable GetDataTableForExcel(WebReferenceReports.RequestReportResponse[] resultRequestReport)
    {

        DataTable data = new DataTable();
        data.Columns.Add("CreatedOn", typeof(DateTime));
        data.Columns.Add("RelaunchDate", typeof(DateTime));
        data.Columns.Add("LastUpdate", typeof(DateTime));
        data.Columns.Add("ConsumerLastSeen");
        data.Columns.Add("CaseNumber");
        data.Columns.Add("Flow");
        data.Columns.Add("RegionName");
        data.Columns.Add("Heading");

        data.Columns.Add("Consumer");
        data.Columns.Add("ConsumerId");
        data.Columns.Add("ConsumerProjects");
        data.Columns.Add("Membership");
        data.Columns.Add("ConsumerPhone");
        data.Columns.Add("CustomerLeadStatus");
        data.Columns.Add("NumberRequestedAdvertisers", typeof(int));
        data.Columns.Add("NumberAdvertisersProvided", typeof(int));
        data.Columns.Add("CallStatus");
        data.Columns.Add("RelaunchStopBy");
        data.Columns.Add("VideoLink");
        data.Columns.Add("Origin");
        data.Columns.Add("OS");
        data.Columns.Add("OsVersion");
        data.Columns.Add("AppVersion");
        data.Columns.Add("Carrier");


        foreach (WebReferenceReports.RequestReportResponse _data in resultRequestReport)
        {
            DataRow row = data.NewRow();

            row["CreatedOn"] = _data.createdOn;
            row["RelaunchDate"] = _data.relaunchDate;
            row["LastUpdate"] = _data.LastUpdate;
            row["ConsumerLastSeen"] = _data.contactLastActive == default(DateTime) ? null : _data.contactLastActive.ToString();

            row["CaseNumber"] = _data.caseNumber;
            row["Flow"] = _data.Flow;
            row["RegionName"] = _data.region;
            row["Heading"] = _data.category;

            row["Consumer"] = _data.contactName;
            row["ConsumerId"] = _data.contactId;
            row["ConsumerPhone"] = _data.contactPhone;
            row["Membership"] = _data.Membership;

            row["CustomerLeadStatus"] = _data.CustomerLeadStatus;
            row["NumberRequestedAdvertisers"] = _data.requestedAdvertisers;
            row["NumberAdvertisersProvided"] = _data.advertisersProvided;
            row["RelaunchStopBy"] = _data.RelaunchStopBy;
            //         if (_data.revenue.HasValue)
            //              row["Revenue"] = string.Format("{0:0}", _data.revenue.Value);
            /*
            if (_data.caseStatus == WebReferenceReports.eRequestsStatus.CLOSE || _data.caseStatus == WebReferenceReports.eRequestsStatus.WAITING_FOR_PHONE_VALIDATION)
                row["CallStatus"] = _data.caseStatus + " (" + _data.reviewStatus + ")";
            else
            */
            row["CallStatus"] = _data.caseStatus;
            bool isVideoChat = _data.caseType == WebReferenceReports.CaseTypeCode.VideoChat;
            if (!string.IsNullOrEmpty(_data.videoUrl))
            {
                GenerateVideoLink gvl = new GenerateVideoLink(_data.videoUrl, _data.picPreviewUrl);
                row["VideoLink"] = gvl.GenerateFullUrl();
            }
            row["Origin"] = _data.Origin;
            row["Carrier"] = _data.CarrierName;
            row["OS"] = _data.OS;
            row["OsVersion"] = _data.OsVersion;
            row["AppVersion"] = _data.AppVersion;
            row["ConsumerProjects"] = _data.contactProjects;

            data.Rows.Add(row);

        }
        return data;

    }
    /*
    DataResult _resultRequestReport(WebReferenceReports.RequestsReportRequest requestReportRequest)
    {
        WebReferenceReports.Reports _site = WebServiceConfig.GetReportsReference(this);
        _site.Timeout = 300000;
        WebReferenceReports.ResultOfRequestsReportResponse resultRequestReport;
        try
        {
            resultRequestReport = _site.RequestsReport(requestReportRequest);
            if (resultRequestReport.Type != WebReferenceReports.eResultType.Success)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return null;

            }

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        DataTable data = LoadToDataTable(resultRequestReport);
        DataResult dr = new DataResult() { data = data, CurrentPage = resultRequestReport.Value.CurrentPage, TotalPages = resultRequestReport.Value.TotalPages, TotalRows = resultRequestReport.Value.TotalRows };
        return dr;
    }
     */
    /*
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow row = (HtmlTableRow)(e.Item.FindControl("tr_show"));
        string css_class = (e.Item.ItemIndex % 2 == 0) ? "odd" : "even";
        row.Attributes.Add("class", css_class);

        string _guid = ((Label)(e.Item.FindControl("lbl_guid"))).Text;
        Guid _id;
        if (!Guid.TryParse(_guid, out _id))
            _id = Guid.Empty;
        if (_id == Guid.Empty)
        {
            HtmlControl _a = (HtmlControl)(e.Item.FindControl("a_Case"));
            _a.Attributes.Add("class", "WithoutUnderline");
        }
    }
     * */
    /*
    protected void ddl_Affiliate_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        
        ExecRequestReport();
    }
     * */


    void Clearreports()
    {
        Table_Report.Visible = false;
        //     _dateError.Visible = false;

        //     _PanelExpertiseError.Visible = false;       

        //gv_GroupByExpertise.DataSource = null;
        //gv_GroupByExpertise.DataBind();  
        //      RequestsReportRequestV = null;
        //     TotalRowsV = 0;
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        _UpdatePanel.Update();
    }
    protected DataTable dataS
    {
        get { return Session[SessionTableName] == null ? new DataTable() : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetConfirmRejectIncidentUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallReportService.asmx/ConfirmRejectIncident"); }
    }
    protected string GetExecuteManualAarUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallReportService.asmx/ExecuteManualAar"); }
    }
    protected string GetSendAarSupplierUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallReportService.asmx/SendAarToIncident"); }
    }
    protected string GetRelaunchUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallReportService.asmx/RelaunchIncident"); }
    }
    protected string AllStatus
    {
        get { return WebReferenceCustomer.eRequestsStatus.ALL.ToString(); }
    }
    
    protected string GetUnbookProject
    {
        get { return ResolveUrl("~/Publisher/ClipCallRequestReport.aspx/UnbookProject"); }
    }
    protected string GetChangeCasePriority
    {
        get { return ResolveUrl("~/Publisher/ClipCallRequestReport.aspx/ChangeCasePriority"); }
    }

    [WebMethod(MessageName = "UnbookProject")]
    public static string UnbookProject(Guid incidentId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = ccreport.UnbookQuote(incidentId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return false.ToString().ToLower();
        }
        return result.Value.ToString().ToLower();
    }
    [WebMethod(MessageName = "ChangeCasePriority")]
    public static string ChangeCasePriority(Guid incidentId, bool toImportant)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.SetImportantProjectRequest request = new ClipCallReport.SetImportantProjectRequest()
        {
            IncidentId = incidentId,
            IsImportant = toImportant
        };
        ClipCallReport.ResultOfSetImportantProjectResponse result = null;
        ClipCallReport.SetImportantProjectResponse response = new ClipCallReport.SetImportantProjectResponse();
        try
        {
            result = ccreport.SetImportantProject(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return JsonConvert.SerializeObject(response);
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return JsonConvert.SerializeObject(response);
        }
        return JsonConvert.SerializeObject(result.Value);
    }
    protected string GetAssessmentService
    {
        get { return ResolveUrl("~/Publisher/ClipCallRequestReport.aspx/AssessmentService"); }
    }
    [WebMethod(MessageName = "AssessmentService")]
    public static string AssessmentService(Guid incidentId, string value)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.SetLeadAssessmentRequest request = new ClipCallReport.SetLeadAssessmentRequest();
        request.IncidentId = incidentId;
        if(!string.IsNullOrEmpty(value))
        {
            ClipCallReport.eAssessment assessment;
            if (Enum.TryParse(value, out assessment))
                request.Assessment = assessment;
        }
        var response = new { IsSuccess = false, Assessment = "" };
       
        ClipCallReport.ResultOfSetLeadAssessmentResponse result = null;
        try
        {
            result = ccreport.SetLeadAssessment(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
       //     ClipCallReport.SetLeadAssessmentResponse _res = new ClipCallReport.SetLeadAssessmentResponse() { IsSuccess = false };
            return JsonConvert.SerializeObject(response);
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
       //     ClipCallReport.SetLeadAssessmentResponse _res = new ClipCallReport.SetLeadAssessmentResponse() { IsSuccess = false };
            return JsonConvert.SerializeObject(response);
        }
        var response2 = new { IsSuccess = result.Value.IsSuccess, Assessment = result.Value.LeadAssessment == null ? null : result.Value.LeadAssessment.Assessment.ToString() };
        return JsonConvert.SerializeObject(response2);
    }


}