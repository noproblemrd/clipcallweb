﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="BillingReport.aspx.cs" Inherits="Publisher_BillingReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<%@ Register src="SalesControls/BillingTable.ascx" tagname="BillingTable" tagprefix="uc2" %>
<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
	<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
	window.onload = appl_init;

	function appl_init() {
		var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
		pgRegMgr.add_beginRequest(BeginHandler);
		pgRegMgr.add_endRequest(EndHandler);

	}
	function BeginHandler() {
		showDiv();
	}
	function EndHandler() {
	    $(window).scroll(_scroll);
		hideDiv();
	}


	function ReciveServerData(retValue) {

		$(window).scroll(_scroll);
		if (retValue.length == 0)
			return;
		var _trs = $(retValue).get(0).getElementsByTagName('tr');
		var _length = _trs.length;
		for (var i = 1; i < _length; i++) {
			_trs[1].className = ((i % 2 == 1) ? "_even" : "_odd");
			$('.-GridView').append(_trs[1]);
		}

	}

	$(window).scroll(_scroll);
	function _scroll() {
		if ($(window).scrollTop() > 0.9 * ($(document).height() - $(window).height())) {
			$(window).unbind('scroll');
			_CallServer();
		}
	}

	function _CallServer() {

	    $(window).unbind('scroll');
	    showDiv();
		$.ajax({
			url: '<%# GetServicePage %>',
			data: null,
			dataType: "json",
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataFilter: function (data) { return data; },
			success: function (data) {
				if (data.d.length == 0 || data.d == "done") {
					return;
				}
				ReciveServerData(data.d);

			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				//              alert(textStatus);
			},
			complete: function (jqXHR, textStatus) {
			    //  HasInHeadingRequest = false;
			    hideDiv();

			}

		});
	}
	function Get_Record(record_path) {
		var _url = '<%# record_path %>';
		_url = _url + "?record=" + encodeURIComponent(record_path);
		window.open(_url, 'record', 'height=300,width=300');
	}
	function _CreateExcel() {
		var path = $('#<%# hf_IsAllBrokerReport.ClientID %>').val() == 'false' ?
					'<%# GetCreateExcel %>' : '<%# GetCreateExcelBillingTableAll %>';
		$.ajax({
			url: path,
			data: null,
			dataType: "json",
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataFilter: function (data) { return data; },
			success: function (data) {
				if (data.d.length == 0) {
					return;
				}
				var _iframe = document.createElement('iframe');
				_iframe.style.display = 'none';
				_iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
				document.getElementsByTagName('body')[0].appendChild(_iframe);

			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				//              alert(textStatus);
			},
			complete: function (jqXHR, textStatus) {
				//  HasInHeadingRequest = false;

			}

		});
	}
	function SetIsAllBroker() {
		var ddl_Brokers = document.getElementById("<%# ddl_Brokers.ClientID %>");
		if (ddl_Brokers.options[ddl_Brokers.selectedIndex].value == "<%# GetGuidEmpty %>")
			$('#<%# hf_IsAllBrokerReport.ClientID %>').val('true');
		else
			$('#<%# hf_IsAllBrokerReport.ClientID %>').val('false');
	}
	$(function () {
		$('#<%# cb_MonthDate.ClientID %>').change(function () {
			if (this.checked) {
				$('#<%# div_Month.ClientID %>').hide();
				$('#<%# div_Date.ClientID %>').show();
			}
			else {
				$('#<%# div_Month.ClientID %>').show();
				$('#<%# div_Date.ClientID %>').hide();
			}
		});
		$('#<%# ddl_Brokers.ClientID %>').change(function () {
			if (this.selectedIndex == 0) {
				$('#div_IncludeNonBilledCalls').css('visibility', 'hidden');
				$('#div_BillingRateBydates').css('visibility', 'visible');
			}
			else {
				$('#div_IncludeNonBilledCalls').css('visibility', 'visible');
				$('#div_BillingRateBydates').css('visibility', 'hidden');
			}
		});
	});

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
	<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
	</cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	                
			<div id="div_Brokers" class="form-field" runat="server">
				<asp:Label ID="lbl_Brokers" CssClass="label" runat="server" Text="Brokers"></asp:Label>                                    
				<asp:DropDownList ID="ddl_Brokers" CssClass="form-select" runat="server" ></asp:DropDownList>		
			</div>
			<div class="form-field" runat="server" style="margin-top: 38px;">
				<asp:CheckBox ID="cb_MonthDate" runat="server" Text="By Month/Date" />
			</div>            
			<div class="Date-field" runat="server" id="div_Month">
				<div class="form-field">
					<asp:Label ID="lbl_Years" CssClass="label" runat="server" Text="Year"></asp:Label>                                    
					<asp:DropDownList ID="ddl_Years" CssClass="form-select" runat="server"></asp:DropDownList>	
				</div> 

				<div class="form-field">
					<asp:Label ID="lbl_Month" CssClass="label" runat="server" Text="Month"></asp:Label>                                    
					<asp:DropDownList ID="ddl_Month" CssClass="form-select" runat="server"></asp:DropDownList>	
				</div> 
			</div> 
			<div class="Date-field" runat="server" id="div_Date" style="display: none;">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  			
		</div>
		<div style="clear:both;"></div>
		<div id="div_BillingRateBydates" class="form-field" >
				<asp:CheckBox ID="cb_BillingRateByDates" runat="server" Text="Calculate Billing Rate By Dates" Checked="false" />
		</div>
		<div style="clear:both;"></div>
		 <div id="div_IncludeNonBilledCalls" class="form-field" style="visibility:hidden;">
			 <asp:CheckBox ID="cb_IncludeNonBilledCalls" runat="server" Text="Include non-billed calls" Checked="false" />
		</div>
		<div style="clear:both;"></div>
		<div style="text-align: center;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click" OnClientClick="SetIsAllBroker();"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
		<asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>	
			<div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
			<div class="results" runat="server" id="div_NumRequests" visible="false">                 
				<asp:Label ID="lbl_NumRequests" runat="server"></asp:Label>
				<asp:Label ID="lblNumRequest" runat="server" Text="Requests"></asp:Label>
			</div>
			<div class="results" runat="server" id="div_Net_Actual_Revenue" visible="false">
				<asp:Label ID="lbl_Net_Actual_Revenue" runat="server" Text="Net Actual Revenue (before discount):"></asp:Label>        
				<asp:Label ID="lbl_Net_Actual_Revenue_result" runat="server" Text=""></asp:Label>
			</div>
			<div class="results" runat="server" id="div_last_update" visible="false">
				<asp:Label ID="lbl_last_update" runat="server" Text="Last Update:"></asp:Label>        
				<asp:Label ID="lbl_last_update_result" runat="server" Text=""></asp:Label>
			</div>
			<div id="Table_Report"  runat="server" >  
				<uc2:BillingTable ID="_BillingTable" runat="server" />
				<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="Name">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="<%# lbl_Broker.Text %>"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblBroker" runat="server" Text="<%# Bind('Name') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Count">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="<%# lbl_Count.Text %>"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label11" runat="server" Text="<%# Bind('Count') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Money">
						<HeaderTemplate>
							<asp:Label ID="Label12" runat="server" Text="<%# lbl_Money.Text %>"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label13" runat="server" Text="<%# Bind('Money') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="BillingRate">
						<HeaderTemplate>
							<asp:Label ID="Label49" runat="server" Text="<%# lbl_BillingRate.Text %>"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label40" runat="server" Text="<%# Bind('BillingRate') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="BillingRateByDates">
						<HeaderTemplate>
							<asp:Label ID="Label49" runat="server" Text="<%# lbl_BillingRateByDates.Text %>"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label40" runat="server" Text="<%# Bind('BillingRateByDates') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="Refund">
						<HeaderTemplate>
							<asp:Label ID="Label49" runat="server" Text="<%# lbl_Refund.Text %>"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label40" runat="server" Text="<%# Bind('Refund') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="TotalRefunds">
						<HeaderTemplate>
							<asp:Label ID="Label49" runat="server" Text="<%# lbl_TotalRefunds.Text %>"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label40" runat="server" Text="<%# Bind('TotalRefunds') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="GrossRevenue">
						<HeaderTemplate>
							<asp:Label ID="Label49" runat="server" Text="<%# lbl_GrossRevenue.Text %>"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label40" runat="server" Text="<%# Bind('GrossRevenue') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>    
				</asp:GridView>
			</div>
		</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</div>    	  

  <asp:Label ID="lbl_Broker" runat="server" Text="Broker" Visible="false"></asp:Label>    
 <asp:Label ID="lbl_Count" runat="server" Text="Count" Visible="false"></asp:Label>    
 <asp:Label ID="lbl_Money" runat="server" Text="Money" Visible="false"></asp:Label>    
<asp:Label ID="lbl_BillingRate" runat="server" Text="Current Billing Rate" Visible="false"></asp:Label>
<asp:Label ID="lbl_BillingRateByDates" runat="server" Text="Billing Rate By Dates" Visible="false"></asp:Label>

<asp:Label ID="lbl_Refund" runat="server" Text="Count Refunds" Visible="false"></asp:Label>    
 <asp:Label ID="lbl_TotalRefunds" runat="server" Text="Total Refunds" Visible="false"></asp:Label>    
<asp:Label ID="lbl_GrossRevenue" runat="server" Text="Gross Revenue" Visible="false"></asp:Label>


 <asp:Label ID="lbl_BillingReport" runat="server" Text="Billing Report" Visible="false"></asp:Label>
	<asp:HiddenField ID="hf_IsAllBrokerReport" runat="server" />
</asp:Content>

