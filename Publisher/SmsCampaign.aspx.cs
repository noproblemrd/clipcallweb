﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_SmsCampaign : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetToolboxEvents();
        if(!IsPostBack)
            LoadCampains();
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle("SMS Campaign managment");
        ToolboxReport1.RemovePrintButton();
        ToolboxReport1.RemoveExcelButton();
    }
    void LoadCampains()
    {
        AarService.AarService _service = WebServiceConfig.GetAarService();
        AarService.CampaignData[] _response = null;
        try
        {
            _response = _service.GetSmsCampaigns();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        
        DataTable dt = new DataTable();
        dt.Columns.Add("Id", typeof(int));
        dt.Columns.Add("Name", typeof(string));
        dt.Columns.Add("Text", typeof(string));
        foreach (AarService.CampaignData cd in _response)
        {
            DataRow row = dt.NewRow();
            row["Id"] = cd.Id;
            row["Name"] = cd.name;
            row["Text"] = cd.text;
            dt.Rows.Add(row);
        }
        _GridView.DataSource = dt;
        _GridView.DataBind();
    }
    protected void btn_Click(object sender, EventArgs e)
    {
        AarService.AarService _service = WebServiceConfig.GetAarService();
        AarService.eCampaignCreateResponse _response;
        try
        {
            _response = _service.SetCampaign(txt_CampaignName.Text, txt_message.Text);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            lbl_message.Text = "Faild";
            return;
        }
        lbl_message.Text = _response.ToString().Replace('_', ' ');
        LoadCampains();
    }
}