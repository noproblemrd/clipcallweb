﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_SmsCampaignReport : PageSetting
{
    const string Num_Format = "{0:#,0.##}";
    const int ONE_DAY = 1;
    protected readonly string SessionTableName = "dataSMSC";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_SmsCampaignReport.Text);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.SmsCampaignReportRequest _request = new WebReferenceReports.SmsCampaignReportRequest();
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo;
        WebReferenceReports.ResultOfListOfSmsCampaignReportResponse result = null;
        try
        {
            result = reports.SmsCampaignReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        /*
         public DateTime date { get; set; }
        public string caseNumber { get; set; }
        public string category { get; set; }
        public string videoLink { get; set; }
        public int videoDuration { get; set; }
        public string region { get; set; }
        public int SMS_Sent { get; set; }
        public int LP_Visits { get; set; }
        public int videoViews { get; set; }
        public int connection { get; set; }
        public int P5 { get; set; }
        public int P4 { get; set; }
        public int P3 { get; set; }
        public int P2 { get; set; }
        public int P1 { get; set; }
	[Priority]*/
        DataTable data = new DataTable();
        data.Columns.Add("date");
        data.Columns.Add("bulkId");
        data.Columns.Add("campaign");
        data.Columns.Add("didntTryToSent");
        data.Columns.Add("sent");
        data.Columns.Add("TotalPhonesAttempts");
       
        foreach (WebReferenceReports.SmsCampaignReportResponse row in result.Value)
        {
            DataRow dr = data.NewRow();
            dr["date"] = GetDateTimeStringFormat(row.date);
            dr["bulkId"] = row.bulkId;
            dr["campaign"] = row.campaign;
            dr["didntTryToSent"] = row.didntTryToSent;
            dr["sent"] = row.sent;
            dr["TotalPhonesAttempts"] = row.TotalPhonesAttempts;            
            data.Rows.Add(dr);
        }
        lbl_RecordMached.Text = result.Value.Length.ToString();
        _GridView.DataSource = data;
        _GridView.DataBind();
        dataV = data;

    }
    string GetDateTimeStringFormat(DateTime dt)
    {
        return string.Format(siteSetting.DateFormat, dt) + " " +
                    string.Format(siteSetting.TimeFormat, dt);
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
}