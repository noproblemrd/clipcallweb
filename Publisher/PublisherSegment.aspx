<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" 
CodeFile="PublisherSegment.aspx.cs" Inherits="Publisher_PublisherSegment" Title="Untitled Page"  %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
 
	   
	<script type="text/javascript">
	function appl_init() {
		var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
		pgRegMgr.add_beginRequest(BeginHandler);        
		pgRegMgr.add_endRequest(EndHandler);
	}
	function BeginHandler()
	{        
		showDiv();
	}
	function EndHandler()
	{
		hideDiv();
	}
	window.onload=appl_init;
	function CloseReport()
	{
		document.getElementById("<%# lbl_report.ClientID %>").innerHTML="";
		document.getElementById("<%# div_report.ClientID %>").className="popModal_del popModal_del_hide";	 
		$find("_modal").hide();
		
	}	
	function ShowReport()
	{
		$find("_modal").show();	    
		document.getElementById("<%# div_report.ClientID %>").className="popModal_del";
	}
	/*
	function SearchEnter(e)
	{
		if(e.keyCode == 13)
		{            
			showDiv();
			< SearchEnter %>;
			return false;
		}       
		 return true;
	}
	*/
	
	function openPopup()
	{
		document.getElementById("<%# popUpSegment.ClientID %>").className="popModal_del";
		ChangeChannel(document.getElementById("<%# ddl_channel.ClientID %>"));
		$find('_modalSegment').show();
	}
	function closePopup()
	{
		CleanPopUp();
		$find('_modalSegment').hide();
	}
	function ClearValidators()
	{
		var validators = Page_Validators;
		
		for (var i = 0;i < Page_Validators.length;  i++)
		{      
			Page_Validators[i].style.display = "none";	
		}       
	}
	function CleanPopUp()
	{
		ClearValidators();
		document.getElementById("<%# txt_NewCode.ClientID %>").value = "";
		document.getElementById("<%# hf_SegmentId.ClientID %>").value = "";
		document.getElementById("<%# txt_NewSegment.ClientID %>").value = "";
		document.getElementById("<%# txt_delay.ClientID %>").value = "0";

		document.getElementById("<%# txt_NewMinPrice.ClientID %>").value = "";
		document.getElementById("<%# txt_NewMinBrokerBid.ClientID %>").value = "";
		var cb = document.getElementById("<%# cb_NewAuction.ClientID %>");
		cb.checked = false;
		cb.removeAttribute("disabled");
		document.getElementById("<%# span_auction.ClientID %>").removeAttribute("disabled");
		document.getElementById("<%# cb_NewShowCertificate.ClientID %>").checked = false;
		document.getElementById("<%# ddl_channel.ClientID %>").selectedIndex = 0;
		
		document.getElementById("<%# popUpSegment.ClientID %>").className="popModal_del popModal_del_hide";
	}
	function ChangeChannel(elem)
	{
		disable_auction(elem.selectedIndex != 0);
	//    alert(elem.options[elem.selectedIndex].value);
		disable_delay(elem.options[elem.selectedIndex].value == "Call");
	}
	function disable_delay(ToDisable)
	{
		var _tr = document.getElementById("tr_delay");
		toggleDisabled(_tr, ToDisable);
		if(ToDisable)
			document.getElementById("<%# txt_delay.ClientID %>").value = "0";
	}
	function disable_auction(ToDisable)
	{
		var cb = document.getElementById("<%# cb_NewAuction.ClientID %>");
		var _span = document.getElementById("<%# span_auction.ClientID %>");
		if(ToDisable)
		{
			cb.checked = false;
			cb.setAttribute("disabled", "disabled");
			_span.setAttribute("disabled", "disabled");
		}
		else
		{
			_span.removeAttribute("disabled");
			cb.removeAttribute("disabled");
		}
		
	}
	function HideSegmentPopup()
	{
		if(Page_ClientValidate('segment'))
		{
			$find('_modalSegment').hide();
		}
		
	}
	function appl_init() {
		var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
		pgRegMgr.add_beginRequest(BeginHandler);
		
		pgRegMgr.add_endRequest(EndHandler);
	}
	function BeginHandler()
	{
//        alert('9');
		showDiv();
	}
	function EndHandler()
	{
		hideDiv();
	}
	window.onload = appl_init;
	/*
	function pageLoad(sender, args)
	{
		appl_init();            
 //       hideDiv();
	}
	*/
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" >
	<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
	</cc1:ToolkitScriptManager>
  <uc1:Toolbox ID="Toolbox1" runat="server" />

			<h5><asp:Label ID="lblSubTitle" runat="server">Web headings tools</asp:Label></h5>
<div class="page-content minisite-content5">

	  
	
	
			<div id="form-analyticsseg">
			
			<asp:UpdatePanel ID="_updatePanelTable" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
			
				<asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" ShowFooter="true"
				CssClass="data-table" DataKeyNames="ID" AllowPaging="True" 
					onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
				<RowStyle CssClass="odd" />
				<AlternatingRowStyle CssClass="even" />
				
				<FooterStyle CssClass="footer"  />
				
				<Columns>                           
				
				<asp:TemplateField >              
				<HeaderTemplate>
					<asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
					<br />
					<asp:CheckBox ID="cb_all" runat="server" >
					</asp:CheckBox>                    
				</HeaderTemplate>
				<ItemTemplate>
					<asp:CheckBox ID="cb_choose" runat="server" />
					
				</ItemTemplate>
				</asp:TemplateField>
				
				<asp:TemplateField >              
				<HeaderTemplate>
					<asp:Label ID="Label1" runat="server" Text="<%# lbl_code.Text %>"></asp:Label>                    
				</HeaderTemplate>
				<ItemTemplate>
					<asp:Label ID="lblCode" runat="server" Text="<%# Bind('Code') %>"></asp:Label>
					<asp:Label ID="lbl_guid" runat="server" Text="<%# Bind('ID') %>" Visible="false"></asp:Label>
				</ItemTemplate>
							 
							   
				</asp:TemplateField>
				
				<asp:TemplateField>              
				<HeaderTemplate>
					<asp:Label ID="Label3" runat="server" Text="<%# lbl_segment.Text %>"></asp:Label>                    
				</HeaderTemplate>
				<ItemTemplate>
					<asp:Label ID="lblSegment" runat="server" Text="<%# Bind('Name') %>"></asp:Label>
				</ItemTemplate>
							 
				</asp:TemplateField>
				
				<asp:TemplateField>              
				<HeaderTemplate>
					<asp:Label ID="Label5" runat="server" Text="<%# lbl_Auction.Text %>"></asp:Label>                    
				</HeaderTemplate>
				<ItemTemplate>
					<asp:Image ID="img_IsAuction" runat="server" ImageUrl="<%# Bind('IsAuction') %>" />
					<asp:Label ID="lbl_Is_Auction" runat="server" Text="<%# Bind('Is_Auction') %>"
					 Visible="false"></asp:Label>
				</ItemTemplate>
			   </asp:TemplateField>
				
				 <asp:TemplateField>              
				<HeaderTemplate>
					<asp:Label ID="Label7" runat="server" Text="<%# lbl_MinPrice.Text %>"></asp:Label>                    
				</HeaderTemplate>
				<ItemTemplate>
					<asp:Label ID="lblMinPrice" runat="server" Text="<%# Bind('MinPrice') %>"></asp:Label>
				</ItemTemplate>							
				</asp:TemplateField>

				<asp:TemplateField Visible="false">
					<HeaderTemplate>
						<asp:Label ID="Labelx7" runat="server" Text="<%# lbl_MinBrokerBid.Text %>"></asp:Label> 
					</HeaderTemplate>
					<ItemTemplate>
						<asp:Label ID="lblMinBrokerBid" runat="server" Text="<%# Bind('MinBrokerBid') %>"></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				
				<asp:TemplateField>              
				<HeaderTemplate>
					<asp:Label ID="Label17" runat="server" Text="<%# lbl_ShowCertificate.Text %>"></asp:Label>                    
				</HeaderTemplate>
				<ItemTemplate>
					<asp:Image ID="img_ShowCertificate" runat="server" ImageUrl="<%# Bind('ShowCertificate') %>" /> 
					<asp:Label ID="lbl_ShowCertificate" runat="server" Text="<%# Bind('Show_Certificate') %>"
					 Visible="false"></asp:Label>                   
				</ItemTemplate>
							
				</asp:TemplateField>
				
				<asp:TemplateField>              
				<HeaderTemplate>
					<asp:Label ID="Label77" runat="server" Text="<%# lbl_Channel.Text %>"></asp:Label>                    
				</HeaderTemplate>
				<ItemTemplate>
					<asp:Label ID="Label223" runat="server" Text="<%# Bind('Channel') %>"></asp:Label>
					<asp:Label ID="lblChannel" runat="server" Text="<%# Bind('ValueChannel') %>" Visible="false"></asp:Label>
				</ItemTemplate>                            
				</asp:TemplateField>
				
				<asp:TemplateField>              
				<HeaderTemplate>
					<asp:Label ID="Label57" runat="server" Text="<%# lbl_NotificationDelay.Text %>"></asp:Label>                    
				</HeaderTemplate>
				<ItemTemplate>
					<asp:Label ID="lblNotificationDelay" runat="server" Text="<%# Bind('NotificationDelay') %>"></asp:Label>                    
				</ItemTemplate>                            
				</asp:TemplateField>
				
				</Columns>
				
				<PagerStyle HorizontalAlign="Center" CssClass="pager" />
				</asp:GridView>
			</ContentTemplate>
			</asp:UpdatePanel>
			
			</div>
		</div>   
	<div runat="server" id="div_report" class="popModal_del popModal_del_hide">
	<div class="top"></div>
		<a id="a_close" runat="server" class="span_A" href="javascript:CloseReport();"></a>
		<div class="content" >
		 <div>
				<h2>                    
					<asp:Label ID="lbl_titleReport" runat="server" Text="Delete">Excel report</asp:Label>
				</h2>
				</div>
			<asp:UpdatePanel ID="_UpdatePanelReport" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
			<div style="overflow-y:auto; max-height:480px;">
				<asp:Label ID="lbl_report" runat="server" ></asp:Label>
			</div>
			</ContentTemplate>
			</asp:UpdatePanel>
			 <div class="popupsegments">
				
					<input id="btn_closed" type="button" value="Close" onclick="javascript:CloseReport();"
					 class="btn2" />
					<asp:Button ID="btn_saveLog" runat="server" Text="Open log" CssClass="btn"
					OnClientClick="javascript:hideDiv();" OnClick="_btn_closed_click" />
					 
			   
			</div>
		</div>
		
		<div class="bottom"></div>
	</div>
<cc1:ModalPopupExtender ID="_mpe" runat="server"
	TargetControlID="btnVirtual"
	PopupControlID="div_report"
	 BackgroundCssClass="modalBackground" 
	  BehaviorID="_modal"               
	  DropShadow="false"
	></cc1:ModalPopupExtender>

<div id="popUpSegment" runat="server" class="popModal_del popModal_del_hide" style="display:none;"  >
<asp:UpdatePanel ID="_UpdatePanelEdit" runat="server" UpdateMode="Conditional">
<ContentTemplate>
	
	<div class="top"></div>
	<a id="a1" runat="server" class="span_A" href="javascript:closePopup();"></a>
	<div class="content">
	<div><h2><asp:Label ID="lbl_SegmentForm" runat="server" Text="Heading Form"></asp:Label></h2></div>
	<table class="modaltbl">
	<tr>
	<td >
		<asp:Label ID="lbl_NewCode" runat="server" Text="Id"></asp:Label>
		
	</td>
	<td>
		<asp:TextBox ID="txt_NewCode"  CssClass="form-text1" runat="server"></asp:TextBox>
		<asp:HiddenField ID="hf_SegmentId" runat="server" />
		<br />
		<asp:RequiredFieldValidator ID="RequiredFieldValidator_NewCode" runat="server" CssClass="error-msg"
		ErrorMessage="Missing" Display="Dynamic" ControlToValidate="txt_NewCode" ValidationGroup="segment"></asp:RequiredFieldValidator>
		<asp:RegularExpressionValidator ID="RegularExpressionValidator_NewCode" runat="server" 
		ErrorMessage="You can insert only letters and numbers" ControlToValidate="txt_NewCode"
		CssClass="error-msg" Display="Dynamic" ValidationGroup="segment"
		 ValidationExpression="^[\d\w]+$"></asp:RegularExpressionValidator>
	   
	</td>
	</tr>
	
	<tr>
	<td>
		<asp:Label ID="lbl_NewSegment" runat="server" Text="Heading"></asp:Label>
	</td>
	<td>
		<asp:TextBox ID="txt_NewSegment"  CssClass="form-text1" runat="server"></asp:TextBox>
		 <br />
		<asp:RequiredFieldValidator ID="RequiredFieldValidator_NewSegment" runat="server" CssClass="error-msg"
		ErrorMessage="Missing" Display="Dynamic" ControlToValidate="txt_NewSegment" ValidationGroup="segment"></asp:RequiredFieldValidator>
		<asp:RegularExpressionValidator ID="RegularExpressionValidator_NewSegment" runat="server" 
		ErrorMessage="You can not insert special characters" ControlToValidate="txt_NewSegment"
		CssClass="error-msg" Display="Dynamic" ValidationGroup="segment"
		 ValidationExpression="^[\d\w\s]+$"></asp:RegularExpressionValidator>
	</td>
	</tr>
	
	
	
	<tr>
	<td>
		<asp:Label ID="lbl_NewAuction" runat="server" Text="Auction"></asp:Label>
	</td>
	<td>
		<span id="span_auction" runat="server">
			<asp:CheckBox ID="cb_NewAuction" CssClass="checkbox1"  runat="server" />
		</span>
	</td>        
	</tr>
	
	<tr>
	<td>
		<asp:Label ID="lbl_NewMinPrice" runat="server" Text="Min price"></asp:Label>
	</td>
	<td>
		<asp:TextBox ID="txt_NewMinPrice" CssClass="form-text1" runat="server"></asp:TextBox>
		<br />
		<asp:RequiredFieldValidator ID="RequiredFieldValidator_MinPrice" runat="server" CssClass="error-msg" 
		ErrorMessage="Missing" Display="Dynamic" ControlToValidate="txt_NewMinPrice" ValidationGroup="segment"></asp:RequiredFieldValidator>
		
		<asp:RangeValidator ID="RangeValidator_MinPrice" runat="server" ErrorMessage="Invalid min price" CssClass="error-msg"
		 Display="Dynamic" ControlToValidate="txt_NewMinPrice" ValidationGroup="segment" Type="Integer" MinimumValue="1"
		  MaximumValue="100"></asp:RangeValidator>
		
	</td>        
	</tr>
	<tr>
	<td>
		<asp:Label ID="lbl_NewMinBrokerBid" Text="Minimum Broker Bid" runat="server"></asp:Label>
	</td>
	<td>
		<asp:TextBox ID="txt_NewMinBrokerBid" CssClass="form-text1" runat="server"></asp:TextBox>
		<br />
		<asp:RangeValidator ID="RangeValidator_MinBrokerBid" runat="server" ErrorMessage="Invalid min bid" CssClass="error-msg"
		 Display="Dynamic" ControlToValidate="txt_NewMinBrokerBid" ValidationGroup="segment" Type="Double" MinimumValue="0"
		  MaximumValue="99"></asp:RangeValidator>
	</td>
	</tr>
	
	<tr>
	<td>
		<asp:Label ID="lbl_NewShowCertificate" runat="server" Text="Show Certificate"></asp:Label>
	</td>
	<td>
		<asp:CheckBox ID="cb_NewShowCertificate" CssClass="checkbox1" runat="server" />
		
		
	</td>        
	</tr>
	
	<tr>
	<td>
		<asp:Label ID="lbl_CommunicationChannel" runat="server" Text="Communication Channel"></asp:Label>
	</td>
	<td>
		
		<asp:DropDownList ID="ddl_channel" CssClass="form-text1" runat="server" onchange="javascript:ChangeChannel(this);">
		</asp:DropDownList>
		
	</td>        
	</tr>
	<tr id="tr_delay">
	<td>
		<asp:Label ID="lbl_delay" runat="server" Text="<%# GetNotificationDelay %>" ></asp:Label>
	</td>
	<td>
		<asp:TextBox ID="txt_delay" CssClass="form-text1" runat="server" Text="0"></asp:TextBox>
		 <br />
		<asp:RequiredFieldValidator ID="RequiredFieldValidator_delay" runat="server" CssClass="error-msg"
		ErrorMessage="Missing" Display="Dynamic" ControlToValidate="txt_delay" ValidationGroup="segment"></asp:RequiredFieldValidator>
		
		<asp:RangeValidator ID="RangeValidator_delay" runat="server" ErrorMessage="<%# GetValueRange %>"
		 Type="Integer" MinimumValue="0" MaximumValue="30" Display="Dynamic" ControlToValidate="txt_delay" ValidationGroup="segment"
		 CssClass="error-msg"></asp:RangeValidator>
	</td>
	</tr>
	
	</table>
	</div>
	<div class="content">
	<table class="Table_Button">
	<tr>
	<td>
	<div class="popupsegments">
		<asp:Button ID="btn_Set" runat="server" Text="Set" ValidationGroup="segment" OnClick="btn_Set_Click" 
		 OnClientClick="javascript:HideSegmentPopup();" CssClass="btn2" />
	
		<input id="btn_cancel" type="button" class="btn2" value="Cancel" runat="server" onclick="javascript:closePopup();"/>
		</div>
	</td>
	</tr>
	</table>
	</div>
</ContentTemplate>
</asp:UpdatePanel>        	
</div>
<cc1:ModalPopupExtender ID="_mpeSegment" runat="server"
	TargetControlID="btn_virtual2"
	PopupControlID="popUpSegment"
	 BackgroundCssClass="modalBackground" 
	  
	  BehaviorID="_modalSegment"               
	  DropShadow="false"
   ></cc1:ModalPopupExtender> 
			
<asp:Panel ID="virtualPanel" runat="server" Enabled="false">
   <asp:Button ID="btnVirtual" runat="server" style="display:none;"  />            
   <asp:Button ID="btn_virtual2" runat="server" style="display:none;"  />   
</asp:Panel>
	
	


   
	<asp:Label ID="lbl_ShowCertificate" runat="server" Text="Show certificate" Visible="false"></asp:Label>



	<asp:Label ID="lbl_code" runat="server" Text="Id" Visible="false"></asp:Label>
	<asp:Label ID="lbl_segment" runat="server" Text="Heading" Visible="false"></asp:Label>
	<asp:Label ID="lbl_Auction" runat="server" Text="Auction" Visible="false"></asp:Label>
	<asp:Label ID="lbl_MinPrice" runat="server" Text="Min. Price" Visible="false"></asp:Label>
	<asp:Label ID="lbl_MinBrokerBid" runat="server" Text="Min. Broker Bid" Visible="false"></asp:Label>
	
	<asp:Label ID="lbl_NotificationDelay" runat="server" Text="Notification delay" Visible="false"></asp:Label>
	<asp:Label ID="lbl_range" runat="server" Text="Invalid - value can be" Visible="false"></asp:Label>
   
	<asp:Label ID="lbl_allreadyExists" runat="server" Text="The service or code already exists" Visible="false"></asp:Label>

	   <asp:Label ID="lbl_SavedSuccessfully" runat="server" Text="The heading was saved Successfully" Visible="false"></asp:Label>
	<asp:Label ID="lbl_SavedOk" runat="server" Text="records saved" Visible="false"></asp:Label>
	<asp:Label ID="lbl_dontSaved" runat="server" Text="records does not saved" Visible="false"></asp:Label>
	<asp:Label ID="lbl_problem" runat="server" Text="There are problem with" Visible="false"></asp:Label>
	<asp:Label ID="lbl_DocumentNotSaved" runat="server" Text="records, the document was not saved" Visible="false"></asp:Label>
	<asp:Label ID="lbl_errorLines" runat="server" Text="Error in Line" Visible="false"></asp:Label>
	<asp:Label ID="lbl_Fields" runat="server" Text="Fields" Visible="false"></asp:Label>
	<asp:Label ID="lbl_incorrectFields" runat="server" Text="Fields names are incorrect" Visible="false"></asp:Label>
	<asp:Label ID="lbl_Channel" runat="server" Text="Channel" Visible="false"></asp:Label>


	<asp:HiddenField ID="lbl_NoFile" runat="server" Value="No file was selected" />
	<asp:Label ID="lbl_incorrectFile" runat="server" Text="Incorrect file type" Visible="false"></asp:Label>
	<asp:Label ID="lbl_noResult" runat="server" Text="There are no result" Visible="false"></asp:Label>

	<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
</asp:Content>


