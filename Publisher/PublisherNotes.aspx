<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PublisherNotes.aspx.cs" Inherits="Publisher_PublisherNotes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link type="text/css" rel="stylesheet" href="Style.css" />
   
    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript" >
    var HasUpload = false;
    window.onload=init;
     function init()
    {
        parent.goUp();
    }
    function pageLoad(sender, args)
    {
        parent.hideDiv();
    }
    function witchAlreadyStep(level,mode)
    {    
        
        //parent.window.setInitRegistrationStep(level);
        parent.window.clearStepActive();    
        parent.window.setLinkStepActive('linkStep7');               
    }
    
    function ShowInsert()
    {
        var elm = document.getElementById('div_show');
        if(elm.style.display=='none')
        {
            elm.style.display='inline';
            var elmlink=document.getElementById('<%# lblShowInsert.ClientID %>');
            elmlink.innerHTML=document.getElementById('<%# Hidden_lblHide.ClientID %>').value;
        }
        else
        {
            HideInsert();
        }
    }
    function HideInsert()
    {
        var elm = document.getElementById('div_show');
        elm.style.display='none';
        var elmlink=document.getElementById('<%# lblShowInsert.ClientID %>');
        elmlink.innerHTML=document.getElementById('<%# Hidden_lblShow.ClientID %>').value;
        
        document.getElementById("<%# txtInsertComment.ClientID %>").value = "";
        document.getElementById("<%# lbl_file_name.ClientID %>").innerHTML = "";
        document.getElementById("<%# span_upload.ClientID %>").style.display = "inline";
        document.getElementById("<%# f_name.ClientID %>").value = "";
        if(HasUpload)
            RemoveFileUpload();
    }
    function PassFile()
    {
        var _file = document.getElementById("<%# f_name.ClientID %>");
        if(_file.value.length==0)
        {
            alert(document.getElementById("<%# lbl_NoFile.ClientID %>").value);
            HasUpload = false;
            return false;
        }
        HasUpload = true;
        parent.showDiv();
        return true;      
    }
    function ChkFile()
    {
        var _file = document.getElementById("<%# f_name.ClientID %>");
        if(!HasUpload && _file.value.length > 0)
        {
            alert(document.getElementById("<%# hf_UploadFile.ClientID %>").value);
            return false;
        }
        if (!Page_ClientValidate('note'))
            return false;
        return true;
    }
    function OpenFile(_location)
    {
        document.getElementById("<%# hf_fileName.ClientID %>").value = _location;
        <%# Get_tab_click() %>;
    }
    function UploadedFile(){
        HasUpload = true;
    }

    /*****  ICallbackEventHandler  ****/
    function RemoveFileUpload() {
        HasUpload = false;
        CallServer("", "ee");
    }
    function ReciveServerData(retValue) {
    }
     /*****  ICallbackEventHandler  ****/
    
    </script>
</head>
<body style="margin:0px 0px 0px 0px; background-color:transparent;" class="iframe-inner step7" >
   


   
    <div id="main7" >
    <form id="form7" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   
    <table cellspacing="0" class="main" >
    <tr>
    
    <td>
        <a href="javascript:void(0);" onclick="ShowInsert();" > 
            <H3><asp:Label ID="lblShowInsert" runat="server" Text='<%# Hidden_lblShow.Value %>' Font-Bold="True" ></asp:Label></H3>
        </a>
    </td>
    </tr>
    
    <tr id="InsertComment" >
    <td>
    <div id="div_show">
        <asp:Label ID="lblInsertComment" runat="server" Text="Insert Comment:"></asp:Label>
        <br />
        <asp:TextBox ID="txtInsertComment" runat="server"  TextMode="MultiLine" 
            Rows="3" Width="90%"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
         ErrorMessage="Required field validator" ControlToValidate="txtInsertComment"  CssClass="error-msg"
         ValidationGroup="note">*</asp:RequiredFieldValidator>
           
        <div class="errornotes"><asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="note" CssClass="error-msg" /></div>
       <div class="clear"></div>
        <div class="noteswrap2">
            
            <asp:Label ID="lbl_FileUpload" runat="server" Text="Upload file"></asp:Label>
            <br />
            <span runat="server"  id="span_upload">
                <input type="file" class="notebtn" id="f_name" runat="server" />            
                <asp:Button ID="btn_upload" CssClass="browse_comp" runat="server" Text="Upload" 
                    OnClientClick="return PassFile();" onclick="btn_upload_Click"  />
            </span>
            <asp:Label ID="lbl_file_name" runat="server" ForeColor="Red"></asp:Label>
      

       <div class="notes"><asp:Button ID="btnInsert" runat="server" CssClass="form-submit" OnClientClick="return ChkFile();"
        Text="Insert" OnClick="btnInsert_Click" ValidationGroup="note" />
        </div>
          </div>
           
        </div>
        <div class="clear"><!-- --></div>
    </td>
        
    </tr>
        
    <tr>
        
    <td>
    
    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    
        <asp:DataList ID="dlComments" runat="server" DataKeyField="CommentId" Width="100%">
         <ItemStyle CssClass="regular" />
        <ItemTemplate>
           <div class="notetit"> 
            <asp:Label ID="lblDateTime" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
            <asp:Label ID="lblCreateBy" runat="server" Text="<%# Bind('by') %>"></asp:Label>
            <asp:Label ID="lblBy" runat="server" Text="<%# Bind('CreatedBy') %>"></asp:Label>
            <div class="attachment"><a id="a_attach" runat="server" href="<%# Bind('File') %>" visible="<%# Bind('HasFile') %>">
                <asp:Image ID="img_attach" runat="server" ImageUrl="~/images/attachment1.png" /></a></div>
                </div>
            <br />
            
            <asp:Label ID="lblComment" runat="server" Text="<%# Bind('Text') %>"></asp:Label>           
            
            
        </ItemTemplate>
        </asp:DataList>
            <asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">     
                <ul>
                    <li class="first"><asp:LinkButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click">Previous page</asp:LinkButton></li>
                    <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
                    <li class="last"><asp:LinkButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click">Next page</asp:LinkButton></li>
                </ul>
            </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>
    </td>
    </tr>
    </table>
   
   

   
		
        <asp:HiddenField ID="Hidden_lblTitleComment" runat="server" Value="The comment was created in - " />
        <asp:HiddenField ID="Hidden_lblCreateBy" runat="server" Value="By: " />
        <asp:HiddenField ID="Hidden_lblShow" runat="server" Value="Click here to enter a new comment" />
        <asp:HiddenField ID="Hidden_lblHide" runat="server" Value="Click here to close a new comment" />
        
        <asp:HiddenField ID="lbl_NoFile" runat="server" Value="No file was selected" />
        <asp:HiddenField ID="hf_UploadFile" runat="server" Value="upload the file or remove it" />
        
        <asp:HiddenField ID="hf_fileName" runat="server" />
    </form>
    </div>
    
</body>

</html>
