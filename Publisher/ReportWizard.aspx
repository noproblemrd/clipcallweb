﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" 
AutoEventWireup="true" CodeFile="ReportWizard.aspx.cs" Inherits="Publisher_ReportWizard" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asps" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../general.js" type="text/javascript" />
<!--<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />-->
<script type="text/javascript" src="../CallService.js"></script>


<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<script src="../jquery.min.js" type="text/javascript"></script> 
<script src="../jquery-ui.min.js" type="text/javascript"></script> 


<script type="text/javascript" >

    var firstRow = "";
    function pageLoad(sender, args) 
    {         
        
        appl_init();
    }
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }   
    function HideTable()
    {
        document.getElementById('div_table_filter').style.display='none';
    }
  
    function SetEmptyFirstRow()
    {
        var table = document.getElementById('tbl_filters');  
        var cell_addRemove = getElementsByClass("add_remove", table, "td")[0];           
        (getElementsByClass("remove", cell_addRemove, "a")[0]).style.display = "none";  
        (getElementsByClass("add", cell_addRemove, "a")[0]).style.display = "block";
        var new_row = cell_addRemove.cloneNode(true);
        RemoveAllChildNodes(table);        
        var _tr = document.createElement("tr");
        _tr.appendChild(new_row);
        var _body = document.createElement("tbody");
        _body.appendChild(_tr);
        table.appendChild(_body);
   
    }
    function SetEmptyTable()
    {
        var table = document.getElementById('tbl_filters');  
        var cell_addRemove = getElementsByClass("add_remove", table, "td")[0];   
      //  alert(getElementsByClass("add_remove", table, "td").length);    
        (getElementsByClass("remove", cell_addRemove, "a")[0]).style.display = "none";  
        (getElementsByClass("add", cell_addRemove, "a")[0]).style.display = "block";
        var new_row = cell_addRemove.cloneNode(true);
        RemoveAllChildNodes(table);        
        var _tr = document.createElement("tr");
        _tr.appendChild(new_row);
        var _body = document.createElement("tbody");
        _body.appendChild(_tr);
        table.appendChild(_body);
        document.getElementById('div_table_filter').style.display = "none";
        document.getElementById("<%# selectFieldChoosen.ClientID %>").length=0;
        document.getElementById("<%# selectField.ClientID %>").length=0;
        document.getElementById("<%# ddl_SortBy.ClientID %>").length=1;
        document.getElementById("<%# div_sortBy.ClientID %>").style.display = "block";
        document.getElementById("<%# ddl_SortType.ClientID %>").style.display = "none";
        document.getElementById("<%# ddl_groupBy.ClientID %>").length=1;
        firstRow = "";
    }
    function addRow()
    {
        var table = document.getElementById('tbl_filters');
        var _body = table.getElementsByTagName("tbody")[0];
        var _cells = table.getElementsByTagName("td");
        var rowCount = table.rows.length;
        var selects = table.getElementsByTagName("select");
        
        if(_cells.length==1 && selects.length==0)
        {
            RemoveAllChildNodes(_body);
        }
        
        var ddl_reportIssue = document.getElementById("<%# ddl_reportIssue.ClientID %>")
        var IssueId = ddl_reportIssue.options[ddl_reportIssue.selectedIndex].value;
        var params = "_issueId="+IssueId;
        if(firstRow!="")
        {
           OnAddRow(firstRow); 
           return;
        }
        showDiv();
        CallWebService(params, "ReportWizardService.asmx/GetRow", OnAddRow, On_Error);   
    }
    function OnAddRow(arg)
    {
        var _div = document.createElement('div');
        _div.innerHTML=arg;
        var _row = _div.getElementsByTagName("tr")[0];
        var table = document.getElementById('tbl_filters');
        var _body = table.getElementsByTagName("tbody")[0];
        _body.appendChild(_row);
        hideDiv();
         $('._datepicker').datepicker({
           onClose: function(dateText, inst) { OnBlurString(this); }
        });
        DisableAllAddExeptlast();
  //      alert('2');
        SetListAjax();
 //       setTimeout ( "SetListAjax();", 2000 );
        firstRow = arg;
    }
    function DisableAllAddExeptlast()
    {
        var table = document.getElementById('tbl_filters'); 
        var elems = getElementsByClass("add", table, "a");
        for (var i=0;i<elems.length;i++)
        {
            elems[i].style.display=(i==elems.length-1)?"block":"none";
        }
    }
    function DeleteRow(elem)
    {
       
        var row = getParentByTagName(elem, "tr");
        var table = document.getElementById('tbl_filters');
        var rowCount = table.rows.length;
        var selects = table.getElementsByTagName("select");
        if(rowCount==1)
        {
            if(selects.length==0)
                return;
        //    SetEmptyTable();
            document.getElementById('div_table_filter').style.display = "block";
            SetEmptyFirstRow();
         //   SetFilters(document.getElementById("<%# ddl_reportIssue.ClientID %>"));
            return;
        }
   //         alert(row.tagName);
        var _body = table.getElementsByTagName("tbody")[0];
        _body.removeChild(row);
        var a_add = getElementsByClass("add", _body, "a")[table.rows.length-1];
        
        a_add.style.display="block";
    }
    function SetFilters(elem)
    {
        var _issue = elem.options[elem.selectedIndex].value;
        SetEmptyTable();
        if(_issue==0)
        {         
            return;
        }
        var params = "_issue="+_issue;
        var url = "ReportWizardService.asmx/GetColumns";
        showDiv();
        document.getElementById('div_table_filter').style.display = "block";
        CallWebService(params, url, SetColumns, On_Error);
        
        
    }
    //not in used
    /*
    function SetFiltersFromServer(_issue)
    {
        var params = "_issue="+_issue;
        var url = "ReportWizardService.asmx/GetFilters";
        CallWebService(params, url, OnFilters, On_Error);
    }
    */
    function SetOneFilter(elem, args, ids)
    {
        elem.length=0;
        for(var i=0;i<args.length;i++)
        {
    
            elem.options[elem.options.length]=new Option(args[i], ids[i]);
        }
        elem.selectedIndex=0;
        
    }
   
    function SetColumns(arg)
    {
        var _args = new Array();
        var _ids = new Array();
        GetArray_arg_ids(arg, _args, _ids);
        var _columns = document.getElementById("<%# selectField.ClientID %>");
     //   var _sortBy = document.getElementById("<%# ddl_SortBy.ClientID %>");
        _columns.length=0;
    //    _sortBy.options[_sortBy.length] = new Option("<%# lbl_choose.Text %>", "null");
        for(var i=0; i<_args.length; i++)
        {            
            _columns.options[_columns.length]= new Option(_args[i], _ids[i]);         
//            _sortBy.options[_sortBy.length]= new Option(_args[i], _ids[i]);
        }
        addRow();
        hideDiv();
    }
  
    function GetArray_arg_ids(arg, args, ids)
    {
        var _array = arg.split(";");
        for(var i=0;i<_array.length;i++)
        {
            var _split = _array[i].split(",");
            args.push(_split[0]);
            ids.push(_split[1]);
        }
    }
   
    function On_Error()
    {
        hideDiv();
        alert(top.UpdateFailed());
    }
    function SetOperators(elem)
    {
        var _filter = elem.options[elem.selectedIndex].value
        __element = elem;
        
        var params = "_filter="+_filter;
        var url = "ReportWizardService.asmx/GetOperators_InputValue";
        showDiv();
        CallWebService(params, url, OnOperators, On_Error);
    }
    var __element;
    function OnOperators(arg)
    {
        var _arg = arg.split("$$");
        var _tr = getParentByTagName(__element, "tr");
        var _select = getElementsByClass("operator", _tr, "select")[0];
        var Inside_td = getElementsByClass("td_sel_value", _tr, "td")[0];
        var args = new Array();
        var ids = new Array();
        
        if(_arg[0].length==0)
        {
            _select.length=0;
            hideDiv();
            return;
        }
        
        GetArray_arg_ids(_arg[0], args, ids);
       
        SetOneFilter(_select, args, ids);
        var divTemp = document.createElement("div");
        divTemp.innerHTML=_arg[1];
        var tdnewinput = divTemp.getElementsByTagName("td")[0];
        var _input = tdnewinput.firstChild;
        
        var td_input = getElementsByClass("td_sel_value", _tr, "td")[0];
    //   alert(_tr.hasChildNodes());
        RemoveAllChildNodes(td_input);
        td_input.appendChild(_input);
        (getElementsByClass("esteria", _tr, "td")[0]).style.display="none";
        hideDiv();
        $('._datepicker').datepicker({
           onClose: function(dateText, inst) { OnBlurString(this); }
        });
        SetListAjax();
    }
    function IfTxtExists(elem_txt)
    {
        var _txt = elem_txt.value.trim();
        
        if(_txt.length==0)
            return false;
        return true;
    }
    function IfTxtNumber(elem_txt)
    {
    
//        if(!IfTxtExists(elem_txt))
//            return false;
        var _txt = elem_txt.value.trim();
    //    alert(_txt);
        if(isNaN(_txt))
            return false;
        return true;
    }
    function OnBlurNumber(elem_txt)
    {
        if(!IfTxtExists(elem_txt))
        {
            alert('<%# lbl_missing.Text %>');
            elem_txt.value="";
            SetTdStar(elem_txt, true);
            return;
        }
        if(!IfTxtNumber(elem_txt))
        {
            alert('<%# lbl_InvalidNumber.Text %>');
            elem_txt.value="";
            SetTdStar(elem_txt, true);
            return;
        }
        SetTdStar(elem_txt, false);
    }
    function OnBlurString(elem_txt)
    {
        if(!IfTxtExists(elem_txt))
        {
     //       alert('<%# lbl_missing.Text %>');
            elem_txt.value="";
            SetTdStar(elem_txt, true);
            return;
        }
        SetTdStar(elem_txt, false);
    }
    
    function SetTdStar(elemBroder, TurnOn)
    {
        var parent_tr = getParentByTagName(elemBroder, "tr");
        var _star = getElementsByClass("esteria", parent_tr, "td")[0];
        _star.style.display = (TurnOn)?"inline-block":"none";
    }
    function addField()
    {
        var _Field = document.getElementById("<%# selectField.ClientID %>");
        var _FieldChoose = document.getElementById("<%# selectFieldChoosen.ClientID %>");
        var _sortBy = document.getElementById("<%# ddl_SortBy.ClientID %>");        
        var _groupBy = document.getElementById("<%# ddl_groupBy.ClientID %>");
        if(_groupBy.options[_groupBy.selectedIndex].value != "0")
        {
            alert("<%# lbl_ChooseOnlyOneColumn.Text %>");
            return;
        }
        var arrayIndex = new Array();
        for(var i=0; i<_Field.length; i++)
        {
            if(_Field.options[i].selected)
            {
                _FieldChoose.options[_FieldChoose.length] = new Option(_Field.options[i].text, _Field.options[i].value);
                _sortBy.options[_sortBy.length] = new Option(_Field.options[i].text, _Field.options[i].value);
                _groupBy.options[_groupBy.length] = new Option(_Field.options[i].text, _Field.options[i].value);
                arrayIndex.push(i);
            }
        }
        for(var i=arrayIndex.length-1; i>-1; i--)
        {
            _Field.remove(arrayIndex[i]);
        }
        document.getElementById("missing_select").style.display = "none";
    }
    function removeField()
    {
        var _Field = document.getElementById("<%# selectField.ClientID %>");
        var _FieldChoose = document.getElementById("<%# selectFieldChoosen.ClientID %>");
        var _sortBy = document.getElementById("<%# ddl_SortBy.ClientID %>");
        var _groupBy = document.getElementById("<%# ddl_groupBy.ClientID %>");
        var arrayIndex = new Array();
        for(var i=0; i<_FieldChoose.length; i++)
        {
            if(_FieldChoose.options[i].selected)
            {
                _Field.options[_Field.length] = new Option(_FieldChoose.options[i].text, _FieldChoose.options[i].value);
                arrayIndex.push(i);
            }
        }
        for(var i=arrayIndex.length-1; i>-1; i--)
        {
      //      alert(arrayIndex[i])
            var _txt = _FieldChoose.options[arrayIndex[i]].text;
            var sortByIndex = -1;
            for(var j=0; j<_sortBy.length; j++)
            {
                if(_sortBy.options[j].text == _txt)
                {
                    sortByIndex = j;
                    break;
                }
            }
            if(_sortBy.selectedIndex == sortByIndex)
            {
                _sortBy.selectedIndex = 0;
                document.getElementById("<%# ddl_SortType.ClientID %>").style.display = "none";
            }
            var groupByIndex = -1;
            for(var j=0; j<_groupBy.length; j++)
            {
                if(_groupBy.options[j].text == _txt)
                {
                    groupByIndex = j;
                    break;
                }
            }
            if(_groupBy.selectedIndex == groupByIndex)
            {
                _groupBy.selectedIndex = 0; 
                document.getElementById("<%# div_sortBy.ClientID %>").style.display = "block";               
            }
            _FieldChoose.remove(arrayIndex[i]);
            _sortBy.remove(sortByIndex);
            _groupBy.remove(groupByIndex);
        }
    }
    function groupByClick(elem)
    {
        var _value = elem.options[elem.selectedIndex].value;
        var ddl_SortBy = document.getElementById("<%# ddl_SortBy.ClientID %>");
        var div_sortBy = document.getElementById("<%# div_sortBy.ClientID %>");
        if(_value == "0")
        {
            document.getElementById("<%# ddl_SortType.ClientID %>").style.display = "none";            
            div_sortBy.style.display = "block";
            ddl_SortBy.selectedIndex = 0;
            return;
        }
        
        document.getElementById("<%# ddl_SortType.ClientID %>").style.display = "none";
    //    var ddl_SortBy = document.getElementById("<%# ddl_SortBy.ClientID %>");
        div_sortBy.style.display = "none";
        ddl_SortBy.selectedIndex = 0;
        SetSelectOneValue(_value);
    }
    function SetSelectOneValue(_value)
    {
  //      selectFieldChoosen
        var selectFieldChoosen = document.getElementById("<%# selectFieldChoosen.ClientID %>");
        var selectField = document.getElementById("<%# selectField.ClientID %>");
        var ddl_SortBy = document.getElementById("<%# ddl_SortBy.ClientID %>");
        var ddl_groupBy = document.getElementById("<%# ddl_groupBy.ClientID %>");
        for(var i=selectFieldChoosen.length-1; i > -1; i--)
        {
            var _txt = selectFieldChoosen.options[i].text;
            var _val = selectFieldChoosen.options[i].value;
            if (_val != _value)
            {
                selectFieldChoosen.remove(i);
                selectField.options[selectField.length] = new Option(_txt, _val);
            }
        }
        for(var i=ddl_SortBy.length-1; i > -1; i--)
        {
            var _txt = ddl_SortBy.options[i].text;
            var _val = ddl_SortBy.options[i].value;
            if (_val != _value && _val != "0")
               ddl_SortBy.remove(i); 
        }
        for(var i=ddl_groupBy.length-1; i > -1; i--)
        {
            var _txt = ddl_groupBy.options[i].text;
            var _val = ddl_groupBy.options[i].value;
            if (_val != _value && _val != "0")
               ddl_groupBy.remove(i); 
        }
    }
    function SortClick(elem)
    {
    //ddl_SortType
        var ddl_SortType = document.getElementById("<%# ddl_SortType.ClientID %>");
        ddl_SortType.style.display = (elem.options[elem.selectedIndex].value=="0") ?
                            "none" : "inline";
    }
    function CheckAllData()
    {
        var ddl_reportIssue = document.getElementById("<%# ddl_reportIssue.ClientID %>");
        if(ddl_reportIssue.options[ddl_reportIssue.selectedIndex].value == "0")
        {
            alert('<%# lbl_ChooseReport.Text %>');
            return false;
        }
        var table = document.getElementById('tbl_filters');
        
        var hf_filters = "";
        var hf_operators = "";
        var hf_values = "";
        var hf_selectField  = "";
        var hf_sortBy = ""; 
        var hf_groupBy = "";      
        
        var _selectField = document.getElementById("<%# selectFieldChoosen.ClientID %>");
        var _sortBy = document.getElementById("<%# ddl_SortBy.ClientID %>");
        var _groupBy = document.getElementById("<%# ddl_groupBy.ClientID %>");
        var rows = table.getElementsByTagName("tr");
        var HasValue = true;
        for(var i=0;i<rows.length; i++)
        {
     //       alert(getElementsByClass("filter", rows[i], "select").length);
            var _filter = getElementsByClass("filter", rows[i], "select")[0];
            if(!_filter)
                continue;
            hf_filters += _filter.options[_filter.selectedIndex].value+ ";";
            
            var _operator = getElementsByClass("operator", rows[i], "select")[0];
            hf_operators += _operator.options[_operator.selectedIndex].value+";";
           
            var _value = "";
            var _values = getElementsByClass("operator_value", rows[i], "input");
            if(_values.length==0)
            {
                _values = getElementsByClass("operator_value", rows[i], "select");
                if(_values.length > 0)
                {
                    _value = _values[0].options[_values[0].selectedIndex].value;
                }
            }
            else
                _value = _values[0].value;
                
            if(_value.length==0)
            {
                if(HasValue && _values.length > 0)
                {
                    _values[0].focus();
                }
                HasValue=false;
                SetTdStar(_filter, true);
            }
            _value = _value.replace(";", "");
            hf_values+=_value+";";
        }
        
        if(document.getElementById("<%# selectFieldChoosen.ClientID %>").length==0)
        {
            if(HasValue)
                document.getElementById("<%# selectFieldChoosen.ClientID %>").focus();
            HasValue=false;
            document.getElementById("missing_select").style.display = "inline";
        }
        if(!HasValue)
        {
            alert('<%# lbl_atLeastOneMissing.Text %>');
            return false;
        }
       
        for(var i=0; i<_selectField.length; i++)
        {
            hf_selectField += _selectField.options[i].value+";";
        }
        var _sort = _sortBy.options[_sortBy.selectedIndex].value;
        if(_sort != "0")
        {
            var _typeSort = document.getElementById("<%# ddl_SortType.ClientID %>");
            hf_sortBy = _sort + ";" + _typeSort.options[_typeSort.selectedIndex].value;
        }
        var _group = _groupBy.options[_groupBy.selectedIndex].value;
        if(_group != "0")
        {
            hf_groupBy = _group; 
        }
        document.getElementById("<%# hf_filters.ClientID %>").value = hf_filters;
        document.getElementById("<%# hf_operators.ClientID %>").value = hf_operators;
        document.getElementById("<%# hf_values.ClientID %>").value = hf_values;
        document.getElementById("<%# hf_selectField.ClientID %>").value = hf_selectField;
        document.getElementById("<%# hf_sortBy.ClientID %>").value = hf_sortBy;
        document.getElementById("<%# hf_groupBy.ClientID %>").value = hf_groupBy;
        showDiv();
        return true;
    }
    function OnBlurAutoComplete(elem)
    {
        if(elem.value.length==0)
            return;
        setTimeout(function(){OneSuggest(elem)},200);
        
    }
    function OneSuggest(elem)
    {
        if(document.activeElement==elem)
            return;
        var str = encodeURIComponent(elem.value);
        var _class = elem.className;
        
        
        var params = "str="+str+"&classList="+(_class.split(" ")[0]);
        var url = "ReportWizardService.asmx/GetOneSuggest";
        __element = elem;
        showDiv();
        //    CallWebService(params, url, OnOneSuggest, On_Error);
        CallWebService(params, url, function (arg) {
            elem.value = arg;
            hideDiv();
        }, On_Error);
    }
    var _suggest = ['Region', 'Advertiser', 'Heading', 'AccountManager', 'PageName', 'ControlName', 'PlaceInWebSite', 'Domain', 'Url', 'Keyword'];

    function SetListAjax() {
        for (var i = 0; i < _suggest.length; i++) {
            $("." + _suggest[i]).data('classList', _suggest[i]);
            $("." + _suggest[i]).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ReportWizardService.asmx/SuggestList",
                        data: "{ 'str': '" + request.term + "', " +
                          " 'classList': '" + this.element.data('classList') + "'   }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    value: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                minLength: 1
            });
        }
    /*
    $(".Advertiser").autocomplete({
        source: function(request, response) {
         //   alert(request.term);
            $.ajax({
                url: "ReportWizardService.asmx/SuggestList",
                data: "{ 'str': '" + request.term + "', "+
                      " 'classList': 'Advertiser'   }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function(data) { return data; },
                success: function(data) {
                    response($.map(data.d, function(item) {
                        return {
                            value: item
                        }
                    }))
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        },
        minLength: 1
    });
    $(".Heading").autocomplete({
        source: function(request, response) {
         //   alert(request.term);
            $.ajax({
                url: "ReportWizardService.asmx/SuggestList",
                data: "{ 'str': '" + request.term + "', "+
                      " 'classList': 'Heading'   }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function(data) { return data; },
                success: function(data) {
                    response($.map(data.d, function(item) {
                        return {
                            value: item
                        }
                    }))
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        },
        minLength: 1//,
    //    select: function(event, ui) { $(this).blur(); }
    });
//    alert('3');
    $(".AccountManager").autocomplete({
        source: function(request, response) {
         //   alert(request.term);
            $.ajax({
                url: "ReportWizardService.asmx/SuggestList",
                data: "{ 'str': '" + request.term + "', "+
                      " 'classList': 'AccountManager'   }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function(data) { return data; },
                success: function(data) {
                    response($.map(data.d, function(item) {
                        return {
                            value: item
                        }
                    }))
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        },
        minLength: 1//,
    //    select: function(event, ui) { $(this).blur(); }
    });
    */
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<div class="minisite-content">
    <h2><asp:Label ID="lbl_ReportWizard" runat="server" Text="Report Wizard"></asp:Label></h2>
<div id="form-analytics">
        <div class="form-field">
            <asp:Label ID="lbl_reportIssue" Cssclass="label" runat="server" Text="Report issue"></asp:Label>
            <asp:DropDownList ID="ddl_reportIssue" CssClass="form-select" runat="server" onchange="javascript:SetFilters(this);">
            </asp:DropDownList>
            <div class="iconq2">
            <asp:Image ID="img_ReportIssue" runat="server"  ImageUrl="../images/icon-q.png" CssClass="icon-q" />
            </div>
         
      </div>
    <div id="div_table_filter" class="form-field">
    <asp:Label ID="lbl_filterBy" Cssclass="label_FilterBy" runat="server" Text="Filter by"></asp:Label>
    <table id="tbl_filters">
        
        <asp:PlaceHolder ID="_PlaceHolder" runat="server"></asp:PlaceHolder>
    </table>
    </div>
    <div class="clear"></div>
    <div style="margin-top: 20px;"></div>
    <div class="lists-select2">
      <div class="select3">
      <asp:Label ID="lbl_showMe" runat="server"  Cssclass="label" Text="Show me"></asp:Label>
      </div>
     <div class="form-fieldw"> 
    <div class="list-selectw all">
        <label for="form-all" runat="server" id="lblAllList">
            <asp:Label ID="lbl_AllList" runat="server" CssClass="label3" Text="All List"></asp:Label>
        </label>
        <select id="selectField" multiple="true" class="form-selectw" runat="server" ondblclick="javascript:addField();"></select>			
        <a href="javascript:void(0);" class="btn1" runat="server" id="a_Add" onclick="javascript:addField();">Add</a>
        </div>
        </div>
    <div class="form-fieldw">
    <div class="list-selectw choice">
        <label for="form-choice" runat="server" id="lblYourChoice">
            <asp:Label ID="lbl_yourChoice" runat="server" CssClass="label3" Text="Your choice"></asp:Label>
        </label>
        <select id="selectFieldChoosen"  multiple="true" class="form-selectw" runat="server" ondblclick="javascript:removeField();"></select>
        <span class="span_star" style="display:none;" id="missing_select">*</span>
        <a href="javascript:void(0);" runat="server" id="a_Remove" class="btn2" onclick="javascript:removeField();">Remove</a>
    </div>
    </div>
    
    <div class="form-fieldM" id="div_sortBy" runat="server">
        <asp:Label ID="lbl_sortBy" Class="label3" runat="server" Text="Sort by"></asp:Label>
        <select ID="ddl_SortBy" class="form-select"  runat="server" onchange="SortClick(this);">
        
        </select>
        <asp:DropDownList ID="ddl_SortType" runat="server" >
        </asp:DropDownList>
    
    </div>
    <div class="form-fieldM">
        <asp:Label ID="lbl_groupBy" Class="label3" runat="server" Text="Group by"></asp:Label>
        <select ID="ddl_groupBy" Class="form-select" runat="server" onchange="groupByClick(this);">        
        </select>
        
    
    </div>
   

    <div class="clear"></div>
    <div class="wizard">
        <asp:Button ID="btn_Create" class="CreateReportSubmit2 form-submit" runat="server" Text="Create report" 
            onclick="btn_Create_Click" OnClientClick="return CheckAllData();" />
    
    </div>

</div>
</div>  
</div>
   
    <asp:HiddenField ID="hf_filters" runat="server" />
    <asp:HiddenField ID="hf_operators" runat="server" />
    <asp:HiddenField ID="hf_values" runat="server" />
    <asp:HiddenField ID="hf_selectField" runat="server" />
    <asp:HiddenField ID="hf_sortBy" runat="server" />
    <asp:HiddenField ID="hf_groupBy" runat="server" />
    
    <asp:Label ID="lbl_InvalidNumber" runat="server" Text="Invalid number" Visible="false"></asp:Label>
    <asp:Label ID="lbl_missing" runat="server" Text="Missing" Visible="false"></asp:Label>
    <asp:Label ID="lbl_atLeastOneMissing" runat="server" Text="At least one mandatory field is empty" Visible="false"></asp:Label>
    
    <asp:Label ID="lbl_choose" runat="server" Text="Choose" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ChooseReport" runat="server" Text="Choose any report issue" Visible="false"></asp:Label>
    
    <asp:Label ID="lbl_ChooseOnlyOneColumn" runat="server" Text="You can choose only one column to display when you use group by" Visible="false"></asp:Label>

</asp:Content>


