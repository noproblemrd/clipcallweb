<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="Consumers.aspx.cs" Inherits="Publisher_Consumers" Title="Consumers" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../general.js"></script>
<link media="screen" rel="stylesheet" href="../jquery/colorbox.css" />

<script src="../jquery/jquery.min.js" type="text/javascript" language="javascript"></script>
<script src="../jquery/jquery.colorbox.js" type="text/javascript" language="javascript"></script>	   
<script type="text/javascript" language="javascript">

    var serchCharacter=2;
    var message="<%# GetCloseMessage %>";
    
    //Water text
    function txtInit()
    {
        
        var elm=document.getElementById("<%# txtSearch.ClientID %>");
        if(elm.value.length==0)           
            elm.value=document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value;
        if(elm.value==document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value)
            elm.className="form-text WaterMarkTxt";
        else
            elm.className="form-text";
    }
    function txtOut(elm)
    {
        if(elm.value.length==0)
        {
            elm.value=document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value;
            elm.className="form-text WaterMarkTxt";
        }
    }
    function txtIn(elm)
    {
        
        if(elm.value==document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value && elm.className=="form-text WaterMarkTxt")
        {
            
            elm.className="form-text";
            elm.value="";
        }
    }
    ///
    //check search text
    function chkTextBox(event)
    {  
        if(event.keyCode!=13)
            return true;
        return checkSearch();        
    }
    function checkSearch()
    {
    
        var search=document.getElementById("<%# txtSearch.ClientID %>").value;
        var spans=document.getElementById('RegularExpression');
        var spanSentence=document.getElementById('divComment');
        try{
        document.getElementById('<%# _PanelNoReasult.ClientID %>').style.display="none";
        }catch(ex){}
        while(search.length>0)
        {  
            if(search.substring(search.length-1, search.length)==" ")//search.indexOf(' ',0)==(search.length-1))
            {
                search=search.substring(0, search.length-1);             
            }
            else
                break;
        }
        document.getElementById("<%# txtSearch.ClientID %>").value=search;
        if(search.length<serchCharacter || search==document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value)
        {            
            spans.style.display='inline';
            spanSentence.style.display='block';
            return false;
        }
        else
        {
            showDiv();
            spans.style.display='none';
            spanSentence.style.display='none';
            return true;
         }
    }
    
    function openIframe(_id)
    {
        
 //       _id="000";
         var url = "ConsumerDetails.aspx?Consumer="+_id;
        $(document).ready(function(){$.colorbox({href:url,width:'1000px', height:'90%', iframe:true});});
    }
    //////
    /*
    function pageLoad(sender, args)
    {        
        txtInit();
        hideDiv();
    }
    */
    function OpenAdvertiserIframe(_path)
    {
        
        $(document).ready(function(){$.colorbox({href:_path, width:'95%', height:'90%', iframe:true});});
    }//framepublisher.aspx
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
        txtInit();
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager runat="server" ID="ScriptManager1"></cc1:ToolkitScriptManager>
    
<div class="page-content consumer minisite-content">
    <h2><asp:Label ID="lbl_TitleConsumers" runat="server" Text="Consumer search"></asp:Label></h2>
	    <div id="form-search" class="form-search clearfix">
            <asp:UpdatePanel id="_UpdatePanelSearch" runat="server" UpdateMode="Conditional" >
                <ContentTemplate>                            
                    <div id="divComment" style="display:none; position: absolute; top: -20px;">
                        <asp:Label ID="lblComment" CssClass="error-msg" runat="server" Text="At least two characters"></asp:Label>
                    </div>
                    <asp:Panel ID="_PanelNoReasult" Visible="false" runat="server">
                        <asp:Label ID="lbl_noreasultSearch" CssClass="error-msg" runat="server" Text="There are no results for this search"></asp:Label>
                    </asp:Panel> 
				    <asp:TextBox ID="txtSearch" CssClass="form-text WaterMarkTxt" runat="server" MaxLength="40" onkeydown="return chkTextBox(event);" OnTextChanged="btnSearch_Click" onblur="javascript:txtOut(this);" onfocus="javascript:txtIn(this);"></asp:TextBox>
					<span id="RegularExpression" class="validSearch" style="display:none;">*</span>

					<asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="form-submit" OnClick="btnSearch_Click" OnClientClick="return checkSearch();" />
				<div class="clear"></div>
				<div class="results4">			
                    <asp:Panel ID="PanelResults" runat="server" CssClass="countText" Visible="false"> 
                        <asp:Label ID="lbl_Count" runat="server" CssClass="aboutResults"></asp:Label>
                        <asp:Label ID="lbl_Results" runat="server" Text="Results that match your search criteria"></asp:Label>                                                
                    </asp:Panel>  
                    </div> 
                </ContentTemplate>
			</asp:UpdatePanel>
        </div>	
                <div style="height:30px;">
                <!-- -->
            </div>
       <div class="clear"></div>
        <div class="table2">			
        <asp:UpdatePanel id="_UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" >
            <ContentTemplate>
            <div class="results3"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                <asp:Repeater runat="server" ID="_reapeter">                            
                    <HeaderTemplate>
                    
                        <table class="data-table">
                            <thead>
                                <tr>                                    
                                    <th style="width: 30px; display:none;">&nbsp;</th>
                                    <th style="width: 40px;"><asp:Label ID="Label31" runat="server" Text="<%#lbl_number.Text %>"></asp:Label></th>
						            <th style="width: 170px;"><asp:Label ID="Label50" runat="server" Text="<%#lbl_Fname.Text %>"></asp:Label></th>
						            <th style="width: 170px;"><asp:Label ID="Label2" runat="server" Text="<%#lbl_Lname.Text %>"></asp:Label>					     </th>
						            <th style="width: 170px;"><asp:Label ID="Label1" runat="server" Text="<%#lbl_PhoneNum.Text %>"></asp:Label></th>
						            <th style="width: 110px;"><asp:Label ID="Label51" runat="server" Text="<%#lbl_email.Text %>"></asp:Label></th>
						            <th style="width:40px;"><asp:Label ID="Label32" runat="server" Text="<%#lbl_numRequest.Text %>"></asp:Label></th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr runat="server" id="tr_show">
                                <td class="close first" id="td_openClose" runat="server" style="display:none;">
                                    <a class="arrow" runat="server" id="a_open"></a>
                                </td>
                                <td>
                                    <a id="a_ContactNum" runat="server" href="<%# Bind('OnClientClick') %>">
                                        <asp:Label ID="Label5" runat="server" Text="<%# Bind('Number') %>"></asp:Label>
						            </a>
                                    <asp:Label ID="lbl_guid" runat="server" Text="<%# Bind('ContactId') %>" Visible="false"></asp:Label>                      
						         </td>
						         <td>				     
						             <asp:Label ID="Label230" runat="server" Text="<%# Bind('FirstName') %>"></asp:Label>						        
						         </td>
						         <td>				     
						             <asp:Label ID="Label6" runat="server" Text="<%# Bind('LastName') %>"></asp:Label>						        
						         </td>
						         <td>
						            <asp:Label ID="Label3" runat="server" Text="<%# Bind('MobilePhone') %>"></asp:Label>      
						         </td>
						         <td>
						            <asp:Label ID="Label55" runat="server" Text="<%# Bind('Email') %>"></asp:Label>
						         </td>
						         <td>
						            <asp:Label ID="Label56" runat="server" Text="<%# Bind('NumberOfRequests') %>"></asp:Label>
						         </td>
						     </tr>

                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>                                
                    </FooterTemplate>                           
                </asp:Repeater> 
               
                <uc1:TablePaging ID="TablePaging1" runat="server"  />
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>	
</div>

<div id="divLoader" class="divLoader">   
    <table>
        <tr>
            <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..."/></td>
        </tr>
    </table>
</div>   

<asp:Label ID="lbl_number" runat="server" Text="Number" Visible="false"></asp:Label>
<asp:Label ID="lbl_Fname" runat="server" Text="First name" Visible="false"></asp:Label>
<asp:Label ID="lbl_Lname" runat="server" Text="Last name" Visible="false"></asp:Label>
<asp:Label ID="lbl_PhoneNum" runat="server" Text="Phone number" Visible="false"></asp:Label>
<asp:Label ID="lbl_email" runat="server" Text="email" Visible="false"></asp:Label>    
<asp:Label ID="lbl_numRequest" runat="server" Text="Number of request" Visible="false"></asp:Label>
<asp:HiddenField ID="hf_WaterMarkSearch" runat="server" Value="Type consumer name, phone number or e-mail" />
<asp:Label ID="lbl_IfToExite" runat="server" Text="Are you sure you want to exit? Unsaved data will be lost!" Visible="false"></asp:Label>
<asp:Label ID="lblRecordMached" runat="server" Text="records matched your query" Visible="false"></asp:Label>
</asp:Content>

