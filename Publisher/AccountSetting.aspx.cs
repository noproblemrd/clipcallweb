using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Web.Services;
//using System.Web.Services.Protocols;

public partial class Publisher_AccountSetting : PageSetting
{
    private const string RECORD_KEY = "RecordCalls";
    private const string BalanceAmount_KEY = "SupplierNotifyBalanceAmount";
    const string TIME_ZONE_DEFAULT = "PublisherTimeZone";
    const string MULTIPLE_AUCTION = "AuctionSuppliersMultiplier";
    const string NOTIFICATION_DELAY = "NotificationDelay";
    const string REFUND_ALERT_USER = "RefundAlertUser";
    const string MIN_DEPOSIT_PRICE = "MinDepositPrice";
    const string UNVAILABILITY_LIMIT = "UnavailabilityLimit";
    const string RECHARGE_BONUS_PERCENT = "RechargeBonusPercent";
    const string RECHARGE_FAILED_ALERT_USER = "RechargeFailedAlertUser";
    const string MIN_CALL_DURATION_FOR_EXEMPT_OF_MONTHLY_PAY = "MIN_CALL_DURATION_FOR_EXEMPT_OF_MONTHLY_PAY";
    XDocument CONFIGURATION;
    protected void Page_Load(object sender, EventArgs e)
    {
    //    Session["dbug"] = userManagement.GetSecurityLevel;
        if (!IsPostBack)
        {
            //before the translate!!!//   TO CHANGE!!!!!!
            LoadPaymentMethods();

            RangeValidatorBalanceAlert.MaximumValue = ((short.MaxValue)).ToString();
            //     LoadLanguages();
            LoadABtestingOptions();
            LoadCountries();
            LoadCurrencies();
            LoadTimeZone();
            LoadRegionTabs();
            LoadUsers();
            LoadQuoteDisplayAbTesting();
            ShowUpdate();
            ShowTestMonthlyPayments();
            LoadSites(siteSetting.GetSiteID);
            LoadUserTimeZone_MultipleAuction();
            //        LoadDurationDelay();
            LoadUserPaymentMethods();
            LoadMinimumPriceForDeposit();
            LoadMinCallDurationForExemptOfMonthlyPayments();
            LoadLimitUnavailability();
            Load_Record_MinBalanceAlert_ConfigurationId();
            LoadUserRefountNotification();
            LoadExtraBonus();
            LoadNotificationErrorRecharges();
            BindTextToPage();
           
        }
        
        commentUploadImages.Visible = false;
    }
    void LoadABtestingOptions()
    {
        cb_abtesting.Items.Clear();
        foreach(WebReferenceSite.eQuoteDisplayABTesting abtesting in Enum.GetValues(typeof(WebReferenceSite.eQuoteDisplayABTesting)))
        {
            ListItem li = new ListItem(abtesting.ToString(), abtesting.ToString());
            cb_abtesting.Items.Add(li);
        }
    }
   
    private void ShowTestMonthlyPayments()
    {
        string siteId = siteSetting.GetSiteID.ToLower();
        tr_monthly_payment_test.Visible = (siteId != "np_israel" && siteId != "sales");
    }

    void ShowUpdate()
    {
        tr_update.Visible = siteSetting.GetSiteID == PpcSite.GetCurrent().SiteId;
    }
    void BindTextToPage()
    {
        this.DataBind();
    }
    private void LoadNotificationErrorRecharges()
    {
        string _value = (from p in CONFIGURATION.Element("Configurations").Elements()
                         where p.Element("Key").Value == RECHARGE_FAILED_ALERT_USER
                         select p).FirstOrDefault().Element("Value").Value;

        if (string.IsNullOrEmpty(_value))
        {
            div_ErrorRecharge.Style[HtmlTextWriterStyle.Display] = "none";
            cb_ErrorRecharge.Checked = false;
        }
        else
        {
            div_ErrorRecharge.Style[HtmlTextWriterStyle.Display] = "block";
            cb_ErrorRecharge.Checked = true;
           
           for (int i = 0; i < ddl_ErrorRechargeUsers.Items.Count; i++)
           {
               if (ddl_ErrorRechargeUsers.Items[i].Value == _value)
               {
                   ddl_ErrorRechargeUsers.SelectedIndex = i;
                   break;
               }
           }
           
            
           
        }
        /*
        string result = CONFIGURATION;
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        string _val = "";
        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == RECHARGE_FAILED_ALERT_USER)
            {
                _val = node["Value"].InnerText;
                break;
            }
        }
        if (string.IsNullOrEmpty(_val))
        {
            div_ErrorRecharge.Style[HtmlTextWriterStyle.Display] = "none";
            cb_ErrorRecharge.Checked = false;
        }
        else
        {
            div_ErrorRecharge.Style[HtmlTextWriterStyle.Display] = "block";
            cb_ErrorRecharge.Checked = true;
            foreach (ListItem li in ddl_ErrorRechargeUsers.Items)
            {
                li.Selected = (li.Value == _val);
            }
        }
         * */
    }

    private void LoadExtraBonus()
    {
        string _value = (from p in CONFIGURATION.Element("Configurations").Elements()
                         where p.Element("Key").Value == RECHARGE_BONUS_PERCENT
                         select p).FirstOrDefault().Element("Value").Value;

        txt_ExtraBonus.Text = (_value == "0") ? string.Empty : _value;
        /*
        string result = CONFIGURATION;
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        string _val = "";
        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == RECHARGE_BONUS_PERCENT)
            {
                _val = node["Value"].InnerText;
                break;
            }
        }
        txt_ExtraBonus.Text = (_val == "0") ? string.Empty : _val;
         * */
    }

    private void LoadLimitUnavailability()
    {
        string _value = (from p in CONFIGURATION.Element("Configurations").Elements()
                         where p.Element("Key").Value == UNVAILABILITY_LIMIT
                         select p).FirstOrDefault().Element("Value").Value;

        txt_LimitUnavailability.Text = _value;
        /*
        string result = CONFIGURATION;
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        string _val = "";
        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == UNVAILABILITY_LIMIT)
            {
                _val = node["Value"].InnerText;
                break;
            }
        }
        txt_LimitUnavailability.Text = _val;
         * */
    }

    private void LoadMinCallDurationForExemptOfMonthlyPayments()
    {
        string value = (from p in CONFIGURATION.Element("Configurations").Elements()
                        where p.Element("Key").Value == MIN_CALL_DURATION_FOR_EXEMPT_OF_MONTHLY_PAY
                        select p).FirstOrDefault().Element("Value").Value;
        txt_MinCallDurationSecondsForExemptOfMonthlyPayments.Text = value;
    }

    private void LoadMinimumPriceForDeposit()
    {
        RangeValidator_MinimumDeposit.MaximumValue = int.MaxValue + "";

        string _value = (from p in CONFIGURATION.Element("Configurations").Elements()
                         where p.Element("Key").Value == MIN_DEPOSIT_PRICE
                         select p).FirstOrDefault().Element("Value").Value;
        txt_MinimumDeposit.Text = _value;
        /*
        string result = CONFIGURATION;
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        string _min = "0";
        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == MIN_DEPOSIT_PRICE)
            {
                _min = node["Value"].InnerText;
                break;
            }
        }
        txt_MinimumDeposit.Text = _min;
         * */
    }

    private void LoadUserRefountNotification()
    {
        var query = (from p in CONFIGURATION.Element("Configurations").Elements()
                     where p.Element("Key").Value == REFUND_ALERT_USER
                         select p);//.FirstOrDefault().Element("Value").Value;

        if (query == null || query.Count() == 0 || string.IsNullOrEmpty(query.FirstOrDefault().Element("Value").Value))
        {
            cb_RefundNotification.Checked = false;
            div_RefundNotification.Style[HtmlTextWriterStyle.Display] = "none";
        }
        else
        {
            cb_RefundNotification.Checked = true;
            div_RefundNotification.Style[HtmlTextWriterStyle.Display] = "block";
            for (int i = 0; i < ddl_RefundNotification.Items.Count; i++)
            {
                if (ddl_RefundNotification.Items[i].Value == query.FirstOrDefault().Element("Value").Value)
                {
                    ddl_RefundNotification.SelectedIndex = i;
                    break;
                }
            }
            
            /*
            foreach (ListItem li in ddl_RefundNotification.Items)
            {
                li.Selected = (new Guid(li.Value) == new Guid(query.FirstOrDefault().Element("Value").Value));
            }
             * */
        }

       /* string result = CONFIGURATION;        
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        string _user = string.Empty;
        
        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == REFUND_ALERT_USER)
            {
                _user = node["Value"].InnerText;
                break;
            }            
        }
        if (string.IsNullOrEmpty(_user))
        {
            cb_RefundNotification.Checked = false;
            div_RefundNotification.Style[HtmlTextWriterStyle.Display] = "none";
        }
        else
        {
            cb_RefundNotification.Checked = true;
            div_RefundNotification.Style[HtmlTextWriterStyle.Display] = "block";
            foreach (ListItem li in ddl_RefundNotification.Items)
            {
                li.Selected = (new Guid(li.Value) == new Guid(_user));
            }
        }
        * */
    }
    private void LoadQuoteDisplayAbTesting()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfeQuoteDisplayABTesting result = null;
        try
        {
            result = _site.GetQuoteDisplayAbTesting2_0();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (WebReferenceSite.eQuoteDisplayABTesting ab in result.Value)
        {
            foreach (ListItem li in cb_abtesting.Items)
            {
                if (li.Value == ab.ToString())
                    li.Selected = true;
            }
        }
    }
    private void LoadUsers()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllPublishersResponse result = new WebReferenceSite.ResultOfGetAllPublishersResponse();
        try
        {
            result = _site.GetAllPublishers();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);            
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);            
            return;
        }
        ddl_RefundNotification.Items.Clear();
        ddl_RefundNotification.Items.Add(new ListItem("--- " + lbl_Choose.Text + " ---",
            (Guid.Empty.ToString())));
        ddl_ErrorRechargeUsers.Items.Clear();
        ddl_ErrorRechargeUsers.Items.Add(new ListItem("--- " + lbl_Choose.Text + " ---",
            (Guid.Empty.ToString())));
        foreach (WebReferenceSite.PublisherData pd in result.Value.Publishers)
        {
            ddl_RefundNotification.Items.Add(new ListItem(pd.Name, pd.AccountId.ToString()));
            ddl_ErrorRechargeUsers.Items.Add(new ListItem(pd.Name, pd.AccountId.ToString()));
        }
        ddl_RefundNotification.SelectedIndex = 0;
        ddl_ErrorRechargeUsers.SelectedIndex = 0;
    }

    private void LoadRegionTabs()
    {
        string command = "EXEC dbo.GetRegionTab";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "RegionTab", (string)reader["RegionTabValue"]);
                rbl_cities.Items.Add(new ListItem(translate, ((int)reader["Id"]).ToString()));
            }
            conn.Close();
        }
    }

    

    private void Load_Record_MinBalanceAlert_ConfigurationId()
    {

        string _value = (from p in CONFIGURATION.Element("Configurations").Elements()
                         where p.Element("Key").Value == RECORD_KEY
                         select p).FirstOrDefault().Element("Value").Value;
        cb_AllowRecord.Checked = (_value == "1") ? true : false;

        string _value2 = (from p in CONFIGURATION.Element("Configurations").Elements()
                          where p.Element("Key").Value == BalanceAmount_KEY
                         select p).FirstOrDefault().Element("Value").Value;

        txt_BalanceAlert.Text = _value2;        
    }

    private void LoadUserPaymentMethods()
    {
        string command = "EXEC dbo.GetPaymentMethodsBySiteNameId @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ListItem _li = cbl_Payment.Items.FindByValue((string)reader["PaymentName"]);
                if (_li != null)
                    _li.Selected = true;
            }
            conn.Close();
        }
    }

    private void LoadPaymentMethods()
    {

        string command = "EXEC dbo.GetPaymentMethods";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            //    
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string paymentName = (string)reader["PaymentName"];
                cbl_Payment.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "ePaymentMethodSIte", paymentName), paymentName));
            }
            conn.Close();
        }
    }

    private void LoadUserTimeZone_MultipleAuction()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = _site.GetConfigurationSettings();// GetUserTimeZone(userManagement.GetGuid);
        }
        catch (Exception ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);
    
        if (xdd.Element("Configurations") == null || xdd.Element("Configurations").Element("Error") != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }        
        ddlTimeZone.SelectedIndex = -1;


        string _value = (from p in xdd.Element("Configurations").Elements()
                         where p.Element("Key").Value == TIME_ZONE_DEFAULT
                              select p).FirstOrDefault().Element("Value").Value;

        (ddlTimeZone.Items.FindByValue(_value)).Selected = true;

        _value = (from p in xdd.Element("Configurations").Elements()
                  where p.Element("Key").Value == MULTIPLE_AUCTION
                  select p).FirstOrDefault().Element("Value").Value;

        txt_MultipleAuction.Text = _value;
        
        CONFIGURATION = xdd;
       
        
    }
    
    private void LoadTimeZone()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = _site.GetAllTimeZones();
        }
        catch (Exception ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["TimeZones"] == null || xdd["TimeZones"]["Error"] != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (XmlNode node in xdd["TimeZones"].ChildNodes)
        {
            ddlTimeZone.Items.Add(new ListItem(node.Attributes["TimeZoneName"].InnerText,
                node.Attributes["TimeZoneCode"].InnerText));
        }
    }
    private void LoadCurrencies()
    {
        string command = "EXEC dbo.GetCUrrencies";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string currency = (string)reader["SmallName"] + " - " + (string)reader["Symbol"];
                    ddlMoney.Items.Add(new ListItem(currency, "" + (int)reader["ID"]));
                }
                conn.Close();
            }
            catch (Exception ex)
            {

            }
        }
    }
        
    private void LoadCountries()
    {
        string command = "EXEC dbo.GetCountries";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    DDLCountry.Items.Add(new ListItem((string)reader["CountryName"],
                         "" + (int)reader["Id"]));
                }
                conn.Close();
            }
            catch (Exception ex)
            {

            }
        }
    }

    
   
     private void LoadSites(string SiteNameId)
    {
 //       string SiteNameId = ((ClickChangeSiteEventArgs)e).MyString;
   //     int languageId = -1;
        int CountryId = -1;
        int CurrencyId = -1;
        int regionTab = -1;
        string command = "EXEC dbo.GetSiteDetailBySiteNameId @SiteId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteId", SiteNameId);
            SqlDataReader reader = cmd.ExecuteReader();
            
            if (reader.Read())
            {
                //     languageId = (int)reader["LanguageId"];
                CountryId = (int)reader["CountryId"];
                CurrencyId = (int)reader["CurrencyId"];
                //         cb_AllowRecord.Checked = (bool)reader["CanRecord"];
                txt_URL.Text = (reader.IsDBNull(4)) ? string.Empty : reader.GetString(4);
                if (!reader.IsDBNull(5))
                    LoadPic((string)reader.GetString(5));
                int MaximumValue = (int)reader["MaxAdvertisers"];
                txt_MaximumExpertise.Text = MaximumValue.ToString();
                txt_DefaultExpertise.Text = ((int)reader["DefaultAdvertisers"]).ToString();
                RangeValidator_DefaultExpertise.MaximumValue = MaximumValue.ToString();
                cb_MiniSite.Checked = (bool)reader["DisplayMinisite"];
                regionTab = (int)reader["RegionTabId"];
                txt_icon.Text = (reader.IsDBNull(19)) ? string.Empty : (string)reader["Icon"];
                txt_WebSiteTitle.Text = (reader.IsDBNull(18)) ? string.Empty : (string)reader["PageTitle"];
            }
            conn.Close();
        }
        
        for (int i = 0; i < DDLCountry.Items.Count; i++)
        {
            if (DDLCountry.Items[i].Value == "" + CountryId)
            {
                DDLCountry.SelectedIndex = i;
                break;
            }
        }
        for (int i = 0; i < ddlMoney.Items.Count; i++)
        {
            if (ddlMoney.Items[i].Value == "" + CurrencyId)
            {
                ddlMoney.SelectedIndex = i;
                break;
            }
        }
        for (int i = 0; i < rbl_cities.Items.Count; i++)
        {
            if (rbl_cities.Items[i].Value == regionTab.ToString())
            {
                rbl_cities.SelectedIndex = i;
                break;
            }
        }
        lblSiteName.Text = SiteNameId;
      
       
   
    }

     protected void monthly_payment_test_button_Click(object sender, EventArgs e)
     {
         string siteId = siteSetting.GetSiteID.ToLower();
         if (!(siteId != "np_israel" && siteId != "sales"))
         {
             throw new Exception("Cannot run this test on this environment");
         }
         WebReferenceSite.Site siteReference = WebServiceConfig.GetSiteReference(this);
         siteReference.DoMonthlyDepositsTestFirstOfTheMonth();
     }
    
     protected void btnSet_Click(object sender, EventArgs e)
    {        
         
        if (cbl_Payment.SelectedIndex < 0)
        {           
            return;
        }
    //    int langId = int.Parse(ddlLanguage.SelectedValue);
        int CountryId = int.Parse(DDLCountry.SelectedValue);
         int CurrencyId=int.Parse(ddlMoney.SelectedValue);
         
         int MaximumValue, DefaultValue;
         if ((!int.TryParse(txt_MaximumExpertise.Text, out MaximumValue)) || (!int.TryParse(txt_DefaultExpertise.Text, out DefaultValue)))
         {
             ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
             return;
         }
         if (MaximumValue < DefaultValue)
         {
             ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
             return;
         }
         int MinBalanceAlert;
         if(!int.TryParse(txt_BalanceAlert.Text, out MinBalanceAlert))
         {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
         }
         string command = "EXEC dbo.SetSiteSetting @SiteName, @DefaultCountry, " +
             "@CurrencyId, @RegionTabId, @MaximumValue, @DefaultValue, @MiniSite, " +
             "@PageTitle, @Icon";
         using (SqlConnection conn = DBConnection.GetConnString())
         {
             try
             {
                 conn.Open();
                 SqlCommand cmd = new SqlCommand(command, conn);
                 cmd.Parameters.AddWithValue("@SiteName", lblSiteName.Text);
                 cmd.Parameters.AddWithValue("@DefaultCountry", CountryId);
                 cmd.Parameters.AddWithValue("@CurrencyId", CurrencyId);
                 cmd.Parameters.AddWithValue("@MaximumValue", MaximumValue);
                 cmd.Parameters.AddWithValue("@DefaultValue", DefaultValue);
                 cmd.Parameters.AddWithValue("@MiniSite", cb_MiniSite.Checked);
                 /*
                 string url = txt_URL.Text.Trim();
                 if (string.IsNullOrEmpty(url))
                     cmd.Parameters.AddWithValue("@URL", DBNull.Value);
                 else
                     cmd.Parameters.AddWithValue("@URL", url);
                  * */
                 cmd.Parameters.AddWithValue("@RegionTabId", int.Parse(rbl_cities.SelectedValue));
                 string _title = txt_WebSiteTitle.Text.Trim();
                 if (string.IsNullOrEmpty(_title))
                     cmd.Parameters.AddWithValue("@PageTitle", DBNull.Value);
                 else
                     cmd.Parameters.AddWithValue("@PageTitle", _title);

                 string _icon = txt_icon.Text.Trim();
                 if (string.IsNullOrEmpty(_icon))
                     cmd.Parameters.AddWithValue("@Icon", DBNull.Value);
                 else
                     cmd.Parameters.AddWithValue("@Icon", _icon);
                 int a = cmd.ExecuteNonQuery();
                 conn.Close();
             }
             catch (Exception ex)
             {
                 dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
                 if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                     ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                 return;
             }
         }
        siteSetting.HasMiniSite = cb_MiniSite.Checked;
        setPaymentMethodes();
         string _timeZone = ddlTimeZone.SelectedValue;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = "";
        
        try
        {
            result = _site.SetUserTimeZone(siteSetting.GetSiteID, int.Parse(_timeZone));
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        List<WebReferenceSite.StringPair> list_conf = new List<WebReferenceSite.StringPair>();
        WebReferenceSite.StringPair sp = new WebReferenceSite.StringPair();
         sp.Key=RECORD_KEY;
         sp.Value = (cb_AllowRecord.Checked) ? "1" : "0";
         list_conf.Add(sp);
         sp = new WebReferenceSite.StringPair();
         sp.Key = BalanceAmount_KEY;
         sp.Value = MinBalanceAlert.ToString();
         list_conf.Add(sp);
         sp = new WebReferenceSite.StringPair();
         sp.Key = MULTIPLE_AUCTION;
         sp.Value = txt_MultipleAuction.Text;
         list_conf.Add(sp);
         sp = new WebReferenceSite.StringPair();
         string _user = string.Empty;
        if (cb_RefundNotification.Checked)
        {
            _user = ddl_RefundNotification.SelectedValue;
        }
         sp.Key = REFUND_ALERT_USER;
         sp.Value = _user;
         list_conf.Add(sp);
         sp = new WebReferenceSite.StringPair();
         sp.Key = MIN_DEPOSIT_PRICE;
         sp.Value = txt_MinimumDeposit.Text;
         list_conf.Add(sp);
         sp = new WebReferenceSite.StringPair();
         sp.Key = UNVAILABILITY_LIMIT;
         sp.Value = txt_LimitUnavailability.Text;
         list_conf.Add(sp);
         
         sp = new WebReferenceSite.StringPair();
         sp.Key = RECHARGE_BONUS_PERCENT;
         sp.Value = (string.IsNullOrEmpty(txt_ExtraBonus.Text)) ? "0" : txt_ExtraBonus.Text;
         list_conf.Add(sp);

         sp = new WebReferenceSite.StringPair();
         sp.Key = RECHARGE_FAILED_ALERT_USER;
         if (cb_ErrorRecharge.Checked)
         {
             sp.Value = ddl_ErrorRechargeUsers.SelectedValue;
             div_ErrorRecharge.Style[HtmlTextWriterStyle.Display] = "block";
         }
         else
         {
             sp.Value = string.Empty;
             div_ErrorRecharge.Style[HtmlTextWriterStyle.Display] = "none";
             ddl_ErrorRechargeUsers.SelectedIndex = 0;
         }
         list_conf.Add(sp);
         sp = new WebReferenceSite.StringPair();
         sp.Key = MIN_CALL_DURATION_FOR_EXEMPT_OF_MONTHLY_PAY;
         sp.Value = txt_MinCallDurationSecondsForExemptOfMonthlyPayments.Text;
         list_conf.Add(sp);

         WebReferenceSite.Result _result = null;
        try
        {
            _result = _site.UpdateManyConfigurationSettings(list_conf.ToArray());
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (_result.Type == WebReferenceSite.eResultType.Failure)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
          
        LoadSites(siteSetting.GetSiteID);
        LoadUserPaymentMethods();
        LoadUserTimeZone_MultipleAuction();
        div_RefundNotification.Style.Add(HtmlTextWriterStyle.Display,
              ((cb_RefundNotification.Checked) ? "block" : "none"));
        if (!cb_RefundNotification.Checked)
            ddl_RefundNotification.SelectedIndex = 0;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
          
    }
   
    protected void btn_uploadLogo_Click(object sender, EventArgs e)
    {
        
        commentUploadImages.Visible = false;
        string _path = ConfigurationManager.AppSettings["PublisherLogo"];
        string UploadedFile = _FileImages.PostedFile.FileName;
  //      string UploadedFile = Hidden_FilePath.Value;
         if (string.IsNullOrEmpty(UploadedFile))
         {
             commentUploadImages.Visible = true;
             LoadPic(FileNameV);
             return;
         }
    //     string test = System.IO.Path.GetFileName(UploadedFile);
         string FullName = System.IO.Path.GetFileName(UploadedFile);
         string _name = System.IO.Path.GetFileNameWithoutExtension(UploadedFile);
   //      string[] fileName = FullName.Split('.');
         string _extension = System.IO.Path.GetExtension(UploadedFile);
         if (string.IsNullOrEmpty(_extension))
         {
             InvalidImage();
             return;
         }
         int i = 0;
         while (File.Exists(_path + _name + (i++) + _extension)) ;
         _name = _name + (i - 1);

         string str_path_standard = _path + _name + _extension;

        _FileImages.PostedFile.SaveAs(str_path_standard);

        ASPJPEGLib.IASPJpeg objJpeg;
        objJpeg = new ASPJPEGLib.ASPJpeg();
        try
        {

            objJpeg.Open(str_path_standard);
        }
        catch (Exception exc)
        {
            InvalidImage();
            return;
        }

        // Decrease image size by 50%
        objJpeg.Width = 228;
        objJpeg.Height = 50;
        string new_name = _name + _extension;
        string str_path_thumbNail = _path + @"aspjpeg\" + new_name;
        objJpeg.Save(str_path_thumbNail);

        //insert path to DB        
        string command = "EXEC dbo.InsertLogoBySiteNameId @SiteNameId, @LogoPath ";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@LogoPath", new_name);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
        LoadPic(new_name);
        siteSetting.LogoPath = new_name;
        siteSetting.SetSiteSetting(this);
        Master.setLogo(siteSetting.LogoPath);

    }


    void InvalidImage()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "InvalidImage", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_ErrorImage.Text) + "');", true);
    }
    void LoadPic(string _FileName)
    {
        
        string path2 = ConfigurationManager.AppSettings["PublisherLogo"] + @"aspjpeg\" + _FileName;
        string _path = ConfigurationManager.AppSettings["PublisherLogoWeb"] + @"aspjpeg/" + _FileName;
        if (File.Exists(path2))
        {
            tr_pic.Attributes.Remove("style");
            _ImagePic.ImageUrl = _path;
            FileNameV = _FileName;
        }
        
    }
     
    protected void lb_DeletePic_Click(object sender, EventArgs e)
    {
        
        string path1 = ConfigurationManager.AppSettings["PublisherLogo"] + FileNameV;
        string path2 = ConfigurationManager.AppSettings["PublisherLogo"] + @"aspjpeg\" + FileNameV;
        if (File.Exists(path1))
            File.Delete(path1);
        if (File.Exists(path2))
            File.Delete(path2);
        tr_pic.Attributes["style"] = "display:none";
        _ImagePic.ImageUrl = ResolveUrl("~") + "Publisher/images/ClipCallLogo-03.png";

        string command = "EXEC dbo.DeleteLogoBySiteNameId @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
        siteSetting.LogoPath = string.Empty;
        siteSetting.SetSiteSetting(this);
        Master.RemoveLogo();
         

    }
    
    private void setPaymentMethodes()
    {
        string command = "EXEC dbo.SetPaymentMethodsBySiteNameId @SiteNameId, " +
                    "@PaymentName, @Delimiter";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@PaymentName", GetPaymentMethodsString());
            cmd.Parameters.AddWithValue("@Delimiter", ';');
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
    private string GetPaymentMethodsString()
    {
        string result = "";
        foreach (ListItem li in cbl_Payment.Items)
        {
            if (li.Selected)
                result += li.Value + ";";
        }
        return result;
    }
    string FileNameV
    {
        get { return (ViewState["pic"] == null) ? string.Empty : (string)ViewState["pic"]; }
        set { ViewState["pic"] = value; }
    }
    
    string RecordIdV
    {
        get { return (ViewState["RecordId"] == null) ? string.Empty : (string)ViewState["RecordId"]; }
        set { ViewState["RecordId"] = value; }
    }
    
    string LogoPathV
    {
        get { return (ViewState["LogoPath"] == null) ? string.Empty : (string)ViewState["LogoPath"]; }
        set { ViewState["LogoPath"] = value; }
    }
    protected string UpdateCache
    {
        get { return ResolveUrl("~/ApiAddOns/AddOnServices.asmx/UpdateCache"); }
    }
    protected string updateIntextXml
    {
        get { return ResolveUrl("~/ApiAddOns/AddOnServices.asmx/UpdateIntextXml"); }
    }
    protected string ClearMobileAppPhoneNumber
    {
        get { return ResolveUrl("~/ApiAddOns/AddOnServices.asmx/ClearPhoneMobileApp"); }
    }
    protected string UpdateQuoteAbTestingService
    {
        get { return ResolveUrl("~/Publisher/AccountSetting.aspx/UpdateQuoteAbTesting"); }
    }
    protected string UpdateQuoteAbTestingService2_0
    {
        get { return ResolveUrl("~/Publisher/AccountSetting.aspx/UpdateQuoteAbTesting2_0"); }
    }
    [WebMethod(MessageName = "UpdateQuoteAbTesting")]
    public static string UpdateQuoteAbTesting(int value)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSite.ResultOfBoolean result = null;
        try
        {
            result = _site.SetQuoteDisplayAbTesting(value);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        return result.Value.ToString().ToLower();
    }
    [WebMethod(MessageName = "UpdateQuoteAbTesting2_0")]
    public static string UpdateQuoteAbTesting2_0(string[] abtesting)
    {
        WebReferenceSite.SetABTestingRequest request = new WebReferenceSite.SetABTestingRequest();
        List<WebReferenceSite.eQuoteDisplayABTesting> list = new List<WebReferenceSite.eQuoteDisplayABTesting>();
        foreach (string s in abtesting)
        {
            WebReferenceSite.eQuoteDisplayABTesting ab;
            if (!Enum.TryParse(s, out ab))
                continue;
            list.Add(ab);
        }
        request.ABTesting = list.ToArray();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(PpcSite.GetCurrent().UrlWebReference);
        WebReferenceSite.ResultOfBoolean result = null;
        try
        {
            result = _site.SetQuoteDisplayAbTesting2(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        return result.Value.ToString().ToLower();
    }
     
}
