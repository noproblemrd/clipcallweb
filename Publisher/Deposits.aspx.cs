﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Data;

public partial class Publisher_Deposits : PageSetting
{

    protected PpcSite _cache;
    protected string urlUpdateExemption;
    protected string supplierId;
    protected string supplierName;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        _cache = PpcSite.GetCurrent();
        if (!userManagement.IsPublisher())
            Response.Redirect("LogOut.aspx");
   //     ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(lb_Paymaster);
        if (!IsPostBack)
        {
            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));
            LoadChargingType();
            LoadChargingData();
        }
       

        string root = ResolveUrl("~");
        if (Request.Url.Host == "localhost")
        {
            urlUpdateExemption = "http://" + Request.Url.Host + ":" + Request.Url.Port + root + "Publisher/UpdateExemptOfMonthlyPayment.ashx";
        }
        else
        {
            urlUpdateExemption = "http://" + Request.Url.Host + root + "Publisher/UpdateExemptOfMonthlyPayment.ashx";
        }
        supplierId = GetGuidSetting();
        supplierName = HttpUtility.JavaScriptStringEncode(GetUserNameSetting());
        DataBind();
    }

    private void LoadChargingType()
    {
        ddl_ChargingType.Items.Clear();
        foreach (WebReferenceSupplier.RechargeTypeCode espc in Enum.GetValues(typeof(WebReferenceSupplier.RechargeTypeCode)))
        {
            if (espc == WebReferenceSupplier.RechargeTypeCode.Monthly)
                continue;
            string tran = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "RechargeTypeCode", espc.ToString());
            ddl_ChargingType.Items.Add(new ListItem(tran, espc.ToString()));
        }
    }

    private void LoadChargingData()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfChargingDataForHeadingPaymentPage result = null;
        try
        {
            result = _supplier.GetSupplierChargingDataForHeadingPaymentPage(new Guid(GetGuidSetting()));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if (!result.Value.CanChargeByFundsStatus)
        {
            div_butons.Visible = false;
            lbl_CanMakeSale.Visible = true;
            CanMakeSale = false;
        }
        else
            CanMakeSale = true;
        hf_LowerThanCall.Value = result.Value.MinAmountForAutoRechargeLowBalance.ToString();
        hf_MaximumMonthly.Value = result.Value.MinAmountForAutoRechargeMaxMonthly.ToString();
        SupplierBizId = result.Value.SupplierBizId;
        ZapStatus = result.Value.DapazStatus;
        
        LastRechargeAmount = result.Value.RechargeAmount;
        MonthlyPaymentToPay = result.Value.MonthlyPaymentToPay;
        vatV = result.Value.Vat;
        lb_UpdateCreditCard.Visible = result.Value.HasCreditCardToken;
        lbl_RelativeAmount.Text = result.Value.MonthlyPaymentToPay.ToString();
        lbl_MonthlyAmount.Text = result.Value.MonthlyPaymentBase.ToString();
        hf_AmountOfCharging.Value = result.Value.RechargeAmount.ToString();
        txt_AmountOfCharging.Text = result.Value.RechargeAmount.ToString();
        GetCurrentBalance = (int)result.Value.Balance;
        lbl_TotalAmonut.Text = result.Value.MonthlyPaymentToPay.ToString();
        lblTotalIncludeVat.Text = decimal.Round((decimal)result.Value.MonthlyPaymentToPay * VAT).ToString();
        cb_IsExemptOfMonthlyPayment.Checked = result.Value.IsExemptOfMonthlyPayment;
        hf_ExemptOriginalState.Value = result.Value.IsExemptOfMonthlyPayment ? "1" : "0";
        lb_Paymaster.Text = (result.Value.MonthlyPaymentToPay == 0) ? lbl_Update.Text : lbl_Paymaster.Text;
        if (result.Value.RechargeTypeCode.HasValue)
        {
            foreach (ListItem li in ddl_ChargingType.Items)
                li.Selected = (li.Value == result.Value.RechargeTypeCode.Value.ToString());
        }
        
        /*
        SalesManId = "3281";// result.Value.SupplierBizId;
        SupplierBizId = "12345820";// result.Value.AccountManagerBizId;
        */
        /*
        if (!result.Value.HasCreditCardToken)            
            lb_Update.Visible = false;       
         * */
        DataTable data = new DataTable();
        data.Columns.Add("Segment");
     //   data.Columns.Add("ForMonth");
    //    data.Columns.Add("ForThisMonth");
        data.Columns.Add("HeadingId");
        
        foreach (WebReferenceSupplier.GuidStringPair gsp in result.Value.HeadingsData)
        {
            DataRow row = data.NewRow();
            row["Segment"] = gsp.Name;
         //   row["ForMonth"] = string.Format(NUMBER_FORMAT, cdhd.BaseAmount);
         //   row["ForThisMonth"] = string.Format(NUMBER_FORMAT, cdhd.AmountToPay);
            row["HeadingId"] = gsp.Id.ToString();
            data.Rows.Add(row);
        }
        
        _Repeater.DataSource = data;
        _Repeater.DataBind();
    }

    protected void Update(ePaymentType espct, decimal RechargeAmount)
    {       
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.UpdateAutoRechargeOptionsRequest _request = new WebReferenceSupplier.UpdateAutoRechargeOptionsRequest();
        _request.IsFromDollar = true;
        _request.CreatedByUserId = this.userManagement.Get_Guid;
        _request.SupplierId = new Guid(GetGuidSetting());
        _request.IsAutoRecharge = true;
        _request.RechargeAmount = (int)RechargeAmount;
        _request.CreatedByUserId = userManagement.Get_Guid;
        switch(espct)
        {
            case (ePaymentType.AutoMaxMonthly):
                _request.RechargeTypeCode = WebReferenceSupplier.RechargeTypeCode.MaxMonthly;
                break;
            case (ePaymentType.AutoLow):
                _request.RechargeTypeCode = WebReferenceSupplier.RechargeTypeCode.BalanceLow;
                break;
        }
        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.UpdateAutoRechargeOptions(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
            Update_Faild();
        else
        {
            LoadChargingData();
            Update_Success();
        }
    }

    decimal GetDeposite(out ePaymentType espct)
    {
//        espct = WebReferenceSupplier.eSupplierPricingComponentType.AutoMaxMonthly;
       
        decimal deposite;
         if (!decimal.TryParse(txt_AmountOfCharging.Text, out deposite))
                deposite = -1;
         if (ddl_ChargingType.SelectedValue == WebReferenceSupplier.RechargeTypeCode.MaxMonthly.ToString())
             espct = ePaymentType.AutoMaxMonthly;
         else
             espct = ePaymentType.AutoLow;
         decimal min_val;
         string _minval = (espct == ePaymentType.AutoMaxMonthly) ?
             hf_MaximumMonthly.Value : hf_LowerThanCall.Value;
         
         if (!decimal.TryParse(_minval, out min_val))
             min_val = 0;
         if (min_val > deposite)
        {
            string _script = "MinimumChargeError('" + lbl_AmountError.ClientID + "', " + min_val + ");";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MinimumChargeError", _script, true);
            return -1m;
        }
       
        return deposite;
    }
    protected void lb_Paymaster_Click(object sender, EventArgs e)
    {
        if (!CanMakeSale)
        {
            div_butons.Visible = false;
            lbl_CanMakeSale.Visible = true;
            return;
        }
        ePaymentType espct;
   //     decimal num = 0;
        decimal deposite = GetDeposite(out espct);
        decimal ToChargeGeneral = deposite - LastRechargeAmount;
        if (ToChargeGeneral < 0)
            ToChargeGeneral = 0;
    //    decimal ToChargeCalls = ToChargeGeneral;
        decimal ToChargeTotal = ToChargeGeneral;
        if (deposite <= 0)
            return;
        List<Supplier_Pricing_Component> list_spc = new List<Supplier_Pricing_Component>();
        if (ToChargeGeneral > 0 && ZapStatus != WebReferenceSupplier.DapazStatus.PPAHold)
        {
            Supplier_Pricing_Component spc = new Supplier_Pricing_Component();
            spc.Amount = ToChargeGeneral;
            spc.BaseAmount = 0;
            spc.PaymentType = espct;
            list_spc.Add(spc);
        }
        
        decimal _RelativeAmount;
        if (!decimal.TryParse(lbl_RelativeAmount.Text, out _RelativeAmount))
            _RelativeAmount = 0;
        if (_RelativeAmount > 0)
        {
            ToChargeTotal += _RelativeAmount;
            Supplier_Pricing_Component spc = new Supplier_Pricing_Component();
            spc.Amount = _RelativeAmount;
            spc.BaseAmount = decimal.Parse(lbl_MonthlyAmount.Text);
            spc.PaymentType = ePaymentType.MonthlyFeePayment;
            list_spc.Add(spc);
        }
        bool updateRecharge = (deposite > 0);
        if (updateRecharge)
        {
            Supplier_Pricing_Component spcRecharge = new Supplier_Pricing_Component();
            spcRecharge.Amount = deposite;
            spcRecharge.BaseAmount = LastRechargeAmount;
            spcRecharge.PaymentType  = ePaymentType.Recharge;
            list_spc.Add(spcRecharge);
        }

        /*(ZapStatus == WebReferenceSupplier.DapazStatus.PPAHold) ? 0 :
        Session["returnToSite"] = "http://" + Request.Url.Host + ":"
                + Request.Url.Port + ResolveUrl("~") +
                "Publisher/SearchSuppliers.aspx";
         * */

        if (ToChargeTotal <= 0)
        {
            Update(espct, (int)deposite);
            return;
        }
        if (ZapStatus == WebReferenceSupplier.DapazStatus.PPAHold)
            ToChargeTotal = 0;
        int NewBalance = GetCurrentBalance + (int)ToChargeGeneral;
        int PaymentId = DpzUtilities.SetPayment(list_spc, siteSetting.GetSiteID, ToChargeTotal, ePaymentMethod.CreditCard,
            new Guid(GetGuidSetting()), SupplierBizId, updateRecharge, userManagement.Email, NewBalance, GetCurrentBalance, true);
        if (PaymentId <= 0)
        {
            Update_Faild();
            return;
        }
        //    ClientScript.RegisterClientScriptBlock(this.GetType(), "redirect", "window.location='" + ResolveUrl("~/Publisher/ZapCreditCard.aspx?PaymentId=" + PaymentId.ToString()") + "'", true);
        Response.Redirect(ResolveUrl("~/Publisher/ZapCreditCard.aspx?PaymentId=" + PaymentId.ToString()));
       
    }
    protected void lb_UpdateCreditCard_Click(object sender, EventArgs e)
    {
        if (!CanMakeSale)
        {
            div_butons.Visible = false;
            lbl_CanMakeSale.Visible = true;
            return;
        }
        List<Supplier_Pricing_Component> list_spc = new List<Supplier_Pricing_Component>();
        
        Supplier_Pricing_Component spc = new Supplier_Pricing_Component();
        spc.Amount = 0;
        spc.BaseAmount = 0;
        spc.PaymentType = ePaymentType.AutoLow;
        list_spc.Add(spc);
        int PaymentId = DpzUtilities.SetPayment(list_spc, siteSetting.GetSiteID, 0, ePaymentMethod.CreditCard,
            new Guid(GetGuidSetting()), SupplierBizId, false, userManagement.Email, GetCurrentBalance, GetCurrentBalance, true);
        if (PaymentId <= 0)
        {
            Update_Faild();
            return;
        }
        //    ClientScript.RegisterClientScriptBlock(this.GetType(), "redirect", "window.location='" + ResolveUrl("~/Publisher/ZapCreditCard.aspx?PaymentId=" + PaymentId.ToString()") + "'", true);
        Response.Redirect(ResolveUrl("~/Publisher/ZapCreditCard.aspx?PaymentId=" + PaymentId.ToString()));

        
    }
    /*
    WebReferenceSupplier.GetSupplierChargingDataResponse GetDetails()
    {
        try
        {

            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);

            WebReferenceSupplier.GetSupplierChargingDataRequest request = new WebReferenceSupplier.GetSupplierChargingDataRequest();
            request.SupplierId = new Guid(GetGuidSetting());
         //   request.SupplierId = new Guid("D91C33CB-CFB4-4355-A1DA-506BC8F087DF");
            
            WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result = supplier.GetSupplierChargingData(request);

            if (result.Type == WebReferenceSupplier.eResultType.Success)
                return result.Value;                

            else
                return null;

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            return null;
        }
    }
     * */
    /*
    public override void Update_Faild()
    {
        base.Update_Faild();
        string _script = "parent.parent.closeOpenOverLayIframeNewDeposite();";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Update_Faild", _script, true);
    }
    */
    /*
    WebReferenceSupplier.ChargingDataHeadingData[] ChargingDataHeadingDataV
    {
        get { return (WebReferenceSupplier.ChargingDataHeadingData[])Session["ChargingDataHeadingData"]; }
    }
     * result.Value.MonthlyPaymentToPay.ToString();
        lbl_MonthlyAmount.Text = result.Value.MonthlyPaymentBase.ToString();
        hf_AmountOfCharging.Value = result.Value.RechargeAmount.
     * */
    bool CanMakeSale
    {
        get { return (ViewState["CanMakeSale"] == null) ? false : (bool)ViewState["CanMakeSale"]; }
        set { ViewState["CanMakeSale"] = value; }
    }
    decimal LastBalance
    {
        get { return (ViewState["LastBalance"] == null) ? 0m : (decimal)ViewState["LastBalance"]; }
        set { ViewState["LastBalance"] = value; }
    }
    int LastRechargeAmount
    {
        get { return (ViewState["LastRechargeAmount"] == null) ? 0 : (int)ViewState["LastRechargeAmount"]; }
        set { ViewState["LastRechargeAmount"] = value; }
    }
    int MonthlyPaymentToPay
    {
        get { return (ViewState["MonthlyPaymentToPay"] == null) ? 0 : (int)ViewState["MonthlyPaymentToPay"]; }
        set { ViewState["MonthlyPaymentToPay"] = value; }
    }
    decimal vatV
    {
        get { return (ViewState["vatV"] == null) ? 0m : (decimal)ViewState["vatV"]; }
        set { ViewState["vatV"] = value; }
    }
    protected decimal VAT
    {
        get { return ((vatV / 100m) + 1); }
    }

    protected string GetMinimumChargeError
    {
        get { return HttpUtility.HtmlEncode(lbl_MinimumCharge.Text); }
    }
    protected string MustEnterNumber
    {
        get { return HttpUtility.HtmlEncode(lbl_MustEnterNumber.Text); }
    }
    
    string SupplierBizId
    {
        get { return (ViewState["SupplierBizId"] == null) ? string.Empty : (string)ViewState["SupplierBizId"]; }
        set { ViewState["SupplierBizId"] = value; }
    }
    protected string BlackCircle
    {
        get
        {
            return "" + '\u25CF';
        }
    }
    protected string GetCurrency
    {
        get { return siteSetting.CurrencySymbol; }
    }
    protected string GetBalanceLow
    {
        get { return WebReferenceSupplier.RechargeTypeCode.BalanceLow.ToString(); }
    }
    protected int GetCurrentBalance
    {
        get { return (ViewState["CurrentBalance"] == null) ? 0 : (int)ViewState["CurrentBalance"]; }
        set { ViewState["CurrentBalance"] = value; }
    }
    protected WebReferenceSupplier.DapazStatus ZapStatus
    {
        get { return (ViewState["ZapStatus"] == null) ? WebReferenceSupplier.DapazStatus.PPA : (WebReferenceSupplier.DapazStatus)ViewState["ZapStatus"]; }
        set { ViewState["ZapStatus"] = value; }
    }
    
}