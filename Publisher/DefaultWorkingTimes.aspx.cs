﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Publisher_DefaultWorkingTimes : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            LoadDefaultWorking();
        }
    }

    private void LoadDefaultWorking()
    {
        DataTable data = new DataTable();
        data.Columns.Add("Day");
        data.Columns.Add("DayId", typeof(int));
   //     data.Columns.Add("Suspend", typeof(bool));
        data.Columns.Add("IsSuspend", typeof(bool));
        data.Columns.Add("StartHour", typeof(DateTime));
        data.Columns.Add("EndHour", typeof(DateTime));
        string command = "EXEC dbo.GetDefaultWorkingTimes @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow row = data.NewRow();
                row["Day"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "DayOfWeek", (string)reader["DayName"]);
                row["DayId"] = (int)reader["DayId"];
                bool IsSuspend = (bool)reader["IsSuspend"];
                //    row["Suspend"] = !IsSuspend;
                row["IsSuspend"] = IsSuspend;
                row["StartHour"] = (reader.IsDBNull(2) ? DateTime.MinValue : (DateTime)reader["StartHour"]);
                row["EndHour"] = (reader.IsDBNull(3) ? DateTime.MinValue : (DateTime)reader["EndHour"]);
                data.Rows.Add(row);
            }
            conn.Close();
        }
        _Repeater.DataSource = data;
        _Repeater.DataBind();
        for (int i = 0; i < data.Rows.Count; i++)
        {
            RepeaterItem item = _Repeater.Items[i];
            HtmlTableRow tr = (HtmlTableRow)item.FindControl("tr_body");
            tr.Attributes.Add("class", ((i % 2 == 0) ? "odd" : "even"));
            
            CheckBox cb = (CheckBox)item.FindControl("cb_suspend");
            bool Issuspend = cb.Checked;
            Panel _panel = (Panel)item.FindControl("div_times");

            DropDownList ddl_start = (DropDownList)item.FindControl("ddl_From");
            DropDownList ddl_end = (DropDownList)item.FindControl("ddl_To");
            if (!Issuspend)
            {
                _panel.Attributes.Add("style", "visibility:visible");
                DateTime _start = (DateTime)data.Rows[i]["StartHour"];                
                DateTime _end = (DateTime)data.Rows[i]["EndHour"];                
                SetDDL(ddl_start, _start);
                SetDDL(ddl_end, _end);
            }
            else
                _panel.Attributes.Add("style", "visibility:hidden");
            cb.Attributes.Add("onclick", "javascript:cb_click(this, '" + _panel.ClientID + "');");

            ddl_start.Attributes.Add("onChange", "checkTimes('" + ddl_start.ClientID +
                "', '" + ddl_end.ClientID + "');");
            ddl_end.Attributes.Add("onChange", "checkTimes('" + ddl_start.ClientID +
                "', '" + ddl_end.ClientID + "');");
        }
    }
    void SetDDL(DropDownList ddl, DateTime dt)
    {
        string hour = "" + dt.Hour;
        string minute = "" + dt.Minute;
        hour = (hour.Length == 1) ? "0" + hour : hour;
        minute = (minute.Length == 1) ? "0" + minute : minute;
        string val = hour + ":" + minute;
        foreach (ListItem li in ddl.Items)
        {
            if (li.Value == val)
                li.Selected = true;
            else
                li.Selected = false;
        }
    }
    
    protected void btn_set_click(object sender, EventArgs e)
    {
       
        string command = "EXEC dbo.SetDefaultWorkinHours @SiteNameId, " +
            "@StartHour, @EndHour, @IsSuspend, @DayId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {

            conn.Open();
            foreach (RepeaterItem item in _Repeater.Items)
            {
                string[] _start = ((DropDownList)item.FindControl("ddl_From")).SelectedValue.Split(':');
                string[] _end = ((DropDownList)item.FindControl("ddl_To")).SelectedValue.Split(':');
                SqlCommand cmd = new SqlCommand(command, conn);
                DateTime dt_start = new DateTime(1900, 1, 1,
                    int.Parse(_start[0]), int.Parse(_start[1]), 0);
                DateTime dt_end = new DateTime(1900, 1, 1,
                    int.Parse(_end[0]), int.Parse(_end[1]), 0);
                int dayId = int.Parse(((Label)item.FindControl("lblDayId")).Text);
                bool IsSuspend = ((CheckBox)item.FindControl("cb_suspend")).Checked;
                cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
                cmd.Parameters.AddWithValue("@IsSuspend", IsSuspend);
                cmd.Parameters.AddWithValue("@DayId", dayId);
                if (IsSuspend)
                {
                    cmd.Parameters.AddWithValue("@StartHour", DBNull.Value);
                    cmd.Parameters.AddWithValue("@EndHour", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@StartHour", dt_start);
                    cmd.Parameters.AddWithValue("@EndHour", dt_end);
                }
                int a = cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            conn.Close();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        LoadDefaultWorking();
    }
}
