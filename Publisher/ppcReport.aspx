﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ppcReport.aspx.cs" Inherits="Publisher_ppcReport" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>

<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link href="../FromToDate.css" type="text/css" rel="stylesheet" />
<link href="../Calender/CalenderDiv.css" rel="stylesheet" type="text/css" />
<link href="style.css" type="text/css" rel="stylesheet" />
<link href="../UpdateProgress/updateProgress.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../general.js" ></script>
<script  type="text/javascript" src="../Calender/_Calender.js"></script>

<script  type="text/javascript" src="../ShowHideDiv.js"></script>

<script type="text/javascript">
/*
    function CalenderShow(sender,args)
    { 
         set_div(sender);  
    }
    */
    function ChangeCalendarView(sender,args)
    {
   //     alert('0');
        sender._switchMode("", true);           
    }
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  

<div class="page-content minisite-content2">
    
					
	<div id="form-analytics">
		<div class="form-field">
            <asp:Label ID="lbl_GroupBy" CssClass="label" runat="server" Text="Group by"></asp:Label>
            <asp:DropDownList ID="ddl_GroupBy" CssClass="form-select" runat="server"></asp:DropDownList>
        </div>
        <div class="form-field">
            <asp:Label ID="lbl_AdvertiserId" runat="server" CssClass="label" Text="Supplier ID"></asp:Label>
            <asp:TextBox ID="txt_AdvertiserId" CssClass="form-text"  runat="server"></asp:TextBox>
        </div>
        
         <div class="advertiser"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>
	</div>	
	
    <div class="clear"></div>
    
    <div id="list" class="table">          
        <asp:UpdatePanel runat="server" ID="_uPanel" UpdateMode="Conditional"  >
            <ContentTemplate>
                
            <div class="results3"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                
             <div class="div_gridMyCall">
                    <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" CssClass="data-table"
                        onpageindexchanging="_GridView_PageIndexChanging" PageSize="20" AllowPaging="true">
                    <RowStyle CssClass="_even" />
                    <AlternatingRowStyle CssClass="_odd" />
                    <Columns>
                    <asp:TemplateField SortExpression="SupplierId">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_SupplierId.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text="<%# Bind('SupplierId') %>"></asp:Label> 
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="SupplierName">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_SupplierName.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="CreatedOn">
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_CreatedOn.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="SupplierPhone">
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_SupplierPhone.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text="<%# Bind('SupplierPhone') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="SupplierPhone2">
                        <HeaderTemplate>
                            <asp:Label ID="Label17" runat="server" Text="<%# lbl_SupplierPhone2.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label18" runat="server" Text="<%# Bind('SupplierPhone2') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="NumberOfCalls">
                        <HeaderTemplate>
                            <asp:Label ID="Label9" runat="server" Text="<%# lbl_NumberOfCalls.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label10" runat="server" Text="<%# Bind('NumberOfCalls') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="ConsumerPhone">
                        <HeaderTemplate>
                            <asp:Label ID="Label11" runat="server" Text="<%# lbl_ConsumerPhone.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label12" runat="server" Text="<%# Bind('ConsumerPhone') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<asp:Label ID="lbl_ConsumerPhone" runat="server" Text="Consumer phone" Visible="false"></asp:Label>
<asp:Label ID="lbl_CreatedOn" runat="server" Text="Created on" Visible="false"></asp:Label>
<asp:Label ID="lbl_NumberOfCalls" runat="server" Text="Number of calls" Visible="false"></asp:Label>
<asp:Label ID="lbl_SupplierId" runat="server" Text="Supplier Id" Visible="false"></asp:Label>
<asp:Label ID="lbl_SupplierName" runat="server" Text="Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_SupplierPhone" runat="server" Text="Supplier phone" Visible="false"></asp:Label>
<asp:Label ID="lbl_SupplierPhone2" runat="server" Text="Supplier phone2" Visible="false"></asp:Label>

<asp:Label ID="lblTitleppc" runat="server" Text="None PPC Report" Visible="false"></asp:Label>
</asp:Content>

