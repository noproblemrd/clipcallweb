﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ClipCallInvitationExpendedReport.aspx.cs" Inherits="Publisher_ClipCallInvitationExpendedReport" %>

<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
    <link rel="stylesheet" href="ClipCallInvitation.css" type="text/css" />
	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
    <script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function SetTRClass() {
            var _index = 0;
            $('.data-table').find('tr').each(function () {
                $(this).attr('class', '');
                if (_index != 0) {
                    if (_index % 2 == 0)
                        $(this).addClass('even');
                    else
                        $(this).addClass('odd');
                }
                _index++;
            });
        }
        $(function () {           
            SetTRClass();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >    	
	    <div id="form-analytics" >
		     <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
                <div class="Date-field" runat="server" id="div_Date">
				    <uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			    </div>  			
		    </div>
      
		    <div style="clear:both;"></div>
            <div style="text-align: center; margin-top: 15px;">
			    <asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				    onclick="lb_RunReport_Click"></asp:LinkButton>
		    </div>
		    <div style="clear:both;"></div>
            <div class="results" runat="server" id="div_results" visible="false">
                <span class="result-line">Total invites:<asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></span>
                <span class="result-line">Total link pressed:<asp:Label ID="lbl_TotalLinkPressed" runat="server"></asp:Label></span>
                <span class="result-line">Total app installed:<asp:Label ID="lbl_TotalInstalled" runat="server"></asp:Label></span>
                <span class="result-line">Total comunications:<asp:Label ID="lbl_TotalComunication" runat="server"></asp:Label></span>
            </div>
            <div id="Table_Report"  runat="server" >  
                <asp:Repeater runat="server" ID="_reapeter">                            
                    <HeaderTemplate>
                    
                        <table class="data-table">
                            <thead>
                                <tr>     
                                    <th ><asp:Label ID="Label31" runat="server" Text="Action Invite"></asp:Label></th>
						            <th><asp:Label ID="Label50" runat="server" Text="Link Pressed"></asp:Label></th>
						            <th><asp:Label ID="Label2" runat="server" Text="App Installed"></asp:Label></th>
						            <th><asp:Label ID="Label1" runat="server" Text="Comminication"></asp:Label></th>						            
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
                                <td>
                                   <asp:Label ID="Label400" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label>
                                    <asp:Label ID="Label420" runat="server" Text="<%# Bind('SupplierPhone') %>"></asp:Label>
                                    <asp:Label ID="Label401" runat="server" Text="<%# Bind('Date') %>"></asp:Label>
                                    <asp:Label ID="Label402" runat="server" Text="<%# Bind('VideoType') %>"></asp:Label>
                                    <asp:Label ID="Label403" runat="server" Text="<%# Bind('InviteBy') %>"></asp:Label>                                    
                                </td>
                                <td>
                                    <asp:CheckBox ID="cb_clicked" runat="server" Checked="<%# Bind('Clicked') %>" />     
                                    <asp:Label ID="Label404" runat="server" Text="<%# Bind('ClickedAt') %>"></asp:Label>               
						         </td>
						         <td>				     
						             <asp:CheckBox ID="cb_installed" runat="server" Checked="<%# Bind('Installed') %>" />     
                                    <asp:Label ID="Label405" runat="server" Text="<%# Bind('InstalledAt') %>"></asp:Label>  						        
						         </td>
						         <td>				     
						             <asp:Label ID="Label406" runat="server" Text="Video Chat" CssClass="<%# Bind('HasVideoChat') %>"></asp:Label>
                                    <asp:Label ID="Label407" runat="server" Text="Video Lead" CssClass="<%# Bind('HasVideoLead') %>"></asp:Label>
                                    <asp:Label ID="Label408" runat="server" Text="Chat" CssClass="<%# Bind('HasChat') %>"></asp:Label>
                                    <asp:Label ID="Label409" runat="server" Text="Phone Call" CssClass="<%# Bind('HasPhoneCall') %>"></asp:Label>					        
						         </td>						         
						     </tr>

                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>                                
                    </FooterTemplate>                           
                </asp:Repeater> 
            </div>
        </div>
    </div>        
</asp:Content>

