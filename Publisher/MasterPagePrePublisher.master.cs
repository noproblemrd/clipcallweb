using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Publisher_MasterPagePrePublisher : System.Web.UI.MasterPage
{
    public Publisher_MasterPagePrePublisher()
        : base()
    {
        this.ID = PagesNames.PrePublisher.ToString();
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            PageSetting ps = (PageSetting)Page;
            ps.ClearExceptSiteSetting();
            if (string.IsNullOrEmpty(ps.siteSetting.GetSiteID))
            {
                string host = Request.ServerVariables["SERVER_NAME"];
                if (!DBConnection.LoadSiteId(host, ps))
                    Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");

                if (Request.Cookies["language"] != null)
                {
                    int SiteLangId = int.Parse(Request.Cookies["language"].Value);
                    if (DBConnection.IfSiteLangIdExists(SiteLangId, ps.siteSetting.GetSiteID))
                        ps.siteSetting.siteLangId = SiteLangId;
                }
       //             ps.siteSetting.siteLangId = int.Parse(Request.Cookies["language"].Value);
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        PageSetting _page = (PageSetting)Page;
        if (!IsPostBack)
        {            
            setLogo(_page.siteSetting.LogoPath);
            if (!string.IsNullOrEmpty(_page.siteSetting.GetSiteID))
                getTranslate(_page.siteSetting.siteLangId);
            _page.LoadMasterSiteLinks(this);
        }
        
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm == null || !sm.IsInAsyncPostBack)
            _page.SetNoIframe();
    }
    private void setLogo(string name)
    {
        string LogoServerPath = ConfigurationManager.AppSettings["PublisherLogo"] + @"aspjpeg\" + name;
        if (File.Exists(LogoServerPath))
            ImageLogo.ImageUrl = ConfigurationManager.AppSettings["PublisherLogoWeb"] + @"aspjpeg/" + name;
    }
    private void getTranslate(int siteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "MasterPagePrePublisher.master", siteLangId);
    }
}
