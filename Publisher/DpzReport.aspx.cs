﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Data;


public partial class Publisher_DpzReport : PageSetting
{
    private const string BASE_PATH = "~/Publisher/DpzControls/";
    const int DAY_INTERVAL = 2;
    const string FILE_NAME = "CallReport.xml";
    const string Num_Format = "{0:#,0.##}";
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(btn_run);
        SelectAccount.DataBind();
        SetToolboxEvents();
        if (!IsPostBack)
        {

            ReportExecV = eDpzReportType.NULL;
            LoadDapazStatus();
            SetReportTypes();
            SetStatusCall();
      //      setDateTime();
            div_table_title.Visible = false;
            if(!LoadCustomer())
                setDateTime();
            

        }

    }

    private void LoadDapazStatus()
    {
        ddl_DapazStatus.Items.Clear();
        ddl_DapazStatus.Items.Add(new ListItem("",""));
        Dictionary<WebReferenceReports.DapazStatus, string> list = new Dictionary<WebReferenceReports.DapazStatus, string>();
        foreach (WebReferenceReports.DapazStatus ds in Enum.GetValues(typeof(WebReferenceReports.DapazStatus)))
        {
            string _translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "DapazStatus", ds.ToString());
            list.Add(ds, _translate);
            ddl_DapazStatus.Items.Add(new ListItem(_translate, ds.ToString()));
        }
        ddl_DapazStatus.SelectedIndex = 0;
        DapazStatusList = list;
    }

    private bool LoadCustomer()
    {
        string bizid = Request["bizid"];
        if (string.IsNullOrEmpty(bizid))
            return false;
        WebReferenceReports.DapazReportsSupplierContainer result = DpzUtilities.GetSupplierByBizId((SiteSetting)Session["Site"], bizid);
        if (result == null)
            return false;
        txt_SelectedBusinessName.Text = result.Name;
        txt_BusinessName.Text = result.BizId;
        hf_SelectSupplierId.Value = result.Id.ToString();
        setDateTime(30);
        exec_report();
        return true;
    }
    
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_ZapGroupTitle.Text);
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
        ToolboxReport1.RemovePrintButton();
    }
    /*
    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        DataTable data = null;
        switch(ReportExecV)
        {
            case(eDpzReportType.NULL):
                ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
                return;
            case(eDpzReportType.CallDistributionReportByDay):
            case (eDpzReportType.CallDistributionReportByHour):
            case (eDpzReportType.CallDistributionReportByMonth):
                 PageRenderin page = new PageRenderin();
                UserControl ctl =
                     (UserControl)page.LoadControl(BASE_PATH + "CallDistributionReportControl.ascx");

                MethodInfo GetCompleteTable = ctl.GetType().GetMethod("GetCompleteTable");
                data = (DataTable)GetCompleteTable.Invoke(ctl, null);
                break;
            case (eDpzReportType.CallReport):
            case (eDpzReportType.ConversationSourceReport):
                break;
        }

        
      //  SetDataToExport(dr.data);
        ToExcel te = new ToExcel(this, "stat");
        if (!te.ExecExcel(data))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    
    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        /*
        if (ReportExecV == eDpzReportType.NULL)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        WebReferenceReports.RequestsReportRequest _request = RequestsReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataResult dr = _resultRequestReport(_request);
        if (dr == null || dr.data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        //    SetDataToExport(dr.data);

        Session["data_print"] = dr.data;
        Session["grid_print"] = _reapeter;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
        * /
    }
     */
    private void SetStatusCall()
    {
        ddl_CallStatus.Items.Clear();
        foreach (eDpzStatusCall edst in Enum.GetValues(typeof(eDpzStatusCall)))
            ddl_CallStatus.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eDpzStatusCall", edst.ToString()), edst.ToString()));
        ddl_CallStatus.SelectedIndex = 0;
    }

    private void SetReportTypes()
    {
        ddl_ReportType.Items.Clear();
        foreach (eDpzReportType edrt in Enum.GetValues(typeof(eDpzReportType)))
        {
            if (edrt == eDpzReportType.NULL)
                continue;
            ddl_ReportType.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eDpzReportType", edrt.ToString()), edrt.ToString()));
        }
        ddl_ReportType.SelectedIndex = 0;
    }
    private void setDateTime()
    {
        FromToDatePicker_current.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
            DAY_INTERVAL);
    }
    private void setDateTime(int day)
    {
        FromToDatePicker_current.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat), siteSetting.DateFormat,
            day);
    }
    protected void btn_run_Click(object sender, EventArgs e)
    {
        exec_report();
    }
    void exec_report()
    {
        eDpzReportType edrt;
        if (!Enum.TryParse(ddl_ReportType.SelectedValue, out edrt))
            edrt = (eDpzReportType)1;


        ReportExecV = edrt;
        _PlaceHolder.Controls.Clear();
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.DapazCallsReportRequest request = new WebReferenceReports.DapazCallsReportRequest();
        request.FromDate = FromToDatePicker_current.GetDateFrom;
        request.ToDate = FromToDatePicker_current.GetDateTo;
        WebReferenceReports.DapazStatus _ds;
        if (Enum.TryParse(ddl_DapazStatus.SelectedValue, out _ds))
            request.DapazStatus = _ds;
        Guid SupplierId;
        if (!Guid.TryParse(hf_SelectSupplierId.Value, out SupplierId))
            SupplierId = Guid.Empty;
        request.SupplierId = SupplierId;
        eDpzStatusCall edsc;
        if (!Enum.TryParse(ddl_CallStatus.SelectedValue, out edsc))
            edsc = eDpzStatusCall.AllCalls;
        switch (edsc)
        {
            case (eDpzStatusCall.AllCalls):
                request.CallStatus = null;
                break;
            case (eDpzStatusCall.Failed):
                request.CallStatus = false;
                break;
            case (eDpzStatusCall.Success):
                request.CallStatus = true;
                break;
        }
        switch (edrt)
        {
            case (eDpzReportType.CallReport):
                SetCallReport(request, _report);
                break;
            case (eDpzReportType.CallDistributionReportByDay):
                request.GroupBy = WebReferenceReports.eDapazCallsReportGroupBy.Day;
                goto default;
            case (eDpzReportType.CallDistributionReportByHour):
                request.GroupBy = WebReferenceReports.eDapazCallsReportGroupBy.Hour;
                goto default;
            case (eDpzReportType.CallDistributionReportByMonth):
                request.GroupBy = WebReferenceReports.eDapazCallsReportGroupBy.Month;
                goto default;
            case (eDpzReportType.ConversationSourceReport):
                SetConversionSourceReport(request, _report);
                break;
            default:
                SetRequestCallDistribution(request, _report);
                break;
        }
        div_table_title.Visible = true;
        UpdatePanel_table.Update();
    }
    private void SetConversionSourceReport(WebReferenceReports.DapazCallsReportRequest request, WebReferenceReports.Reports _report)
    {
        WebReferenceReports.ResultOfDapazGroupedByCallerIdReportResponse result = null;
        try
        {
            result = _report.DapazCallsByCallerIdReport(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
            return;
        SetTitelsValues(result.Value.TotalMinutes.ToString(), result.Value.AverageDurationInSeconds.ToString(), result.Value.TotalCallers.ToString());
        string controlPath = BASE_PATH + "SourceCallReport.ascx";
        UserControl uc = (UserControl)LoadControl(controlPath);
        Type type = uc.GetType();
        MethodInfo methodInfo = type.GetMethod("BindReport");
        methodInfo.Invoke(uc, new object[] { result.Value.DataList });
        _PlaceHolder.Controls.Add(uc);
        
    }
    void SetTitelsValues(string TotalMinutesNumber, string AverageTimeCall, string TotalCallersNumber)
    {
        lblTotalCallersNumber.Text = TotalCallersNumber;
        lblTotalMinutesNumber.Text = TotalMinutesNumber;
        lblAverageTimeCall.Text = AverageTimeCall;

    }
    private void SetRequestCallDistribution(WebReferenceReports.DapazCallsReportRequest request, WebReferenceReports.Reports _report)
    {
        WebReferenceReports.ResultOfDapazGroupedCallsResponse result = null;
        try
        {
            result = _report.DapazGroupedCallsReport(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
            return;

        SetTitelsValues(result.Value.TotalMinutes.ToString(), result.Value.AverageDurationInSeconds.ToString(), result.Value.TotalCallers.ToString());
        string controlPath = BASE_PATH + "CallDistributionReportControl.ascx";
        UserControl uc = (UserControl)LoadControl(controlPath);
        Type type = uc.GetType();
        PropertyInfo pi = type.GetProperty("StartDate");
        pi.SetValue(uc, request.FromDate, null);
        pi = type.GetProperty("EndDate");
        pi.SetValue(uc, request.ToDate, null);
        MethodInfo methodInfo = type.GetMethod("SetData");
        methodInfo.Invoke(uc, new object[] { result.Value.DataList, request.GroupBy });
        _PlaceHolder.Controls.Add(uc);
       
    }
    private void SetCallReport(WebReferenceReports.DapazCallsReportRequest request, WebReferenceReports.Reports _report)
    {
        
        WebReferenceReports.ResultOfDapazCallReportResponse result = null;
        try
        {
            result = _report.DapazCallReport(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
            return;
        SetTitelsValues(result.Value.NumberOfMinutes.ToString(), result.Value.AverageCallTimeInSeconds.ToString(), result.Value.NumberOfCallers.ToString());
        string controlPath = BASE_PATH + "CallReportControl.ascx"; 
        UserControl uc = (UserControl)LoadControl(controlPath);
        Type type = uc.GetType();
        MethodInfo methodInfo = type.GetMethod("BindReport");
        methodInfo.Invoke(uc, new object[] { result.Value.CallsList });
        _PlaceHolder.Controls.Add(uc);
    }
    protected string GetDivCurrentId
    {
        get { return FromToDatePicker_current.GetParentDivId(); }
    }
    protected string GetInvalidDate
    {
        get { return Server.HtmlEncode(lbl_DateError.Text); }
    }
    protected string GetSupplierList
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetSupplierList"); }
    }
    protected string GetSupplierListPage
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetSupplierListPage"); }        
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/CreateExcel"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
    
    eDpzReportType ReportExecV
    {
        get { return (Session["ReportExecV"] == null) ? eDpzReportType.NULL : (eDpzReportType)Session["ReportExecV"]; }
        set { Session["ReportExecV"] = value; }
    }
    Dictionary<WebReferenceReports.DapazStatus, string> DapazStatusList
    {
        get { return (Session["DapazStatusList"] == null) ? new Dictionary<WebReferenceReports.DapazStatus, string>() : (Dictionary<WebReferenceReports.DapazStatus, string>)Session["DapazStatusList"]; }
        set { Session["DapazStatusList"] = value; }
    }
}