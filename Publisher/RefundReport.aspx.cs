using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text;

public partial class Publisher_RefundReport : PageSetting, ICallbackEventHandler
{
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;

    private const string RECORD_KEY = "RecordCalls";
    Dictionary<string, string> RefundStatusesV;
    StringBuilder sb_Script;
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
   //     ScriptManager1.RegisterAsyncPostBackControl(btn_SendRefund);
  //      ScriptManager1.RegisterAsyncPostBackControl(btn_SendBlackList);
        SetupClient();
        if (!IsPostBack)
        {
            LoadAllowedToRecord();
            LoadStatus();
            SetToolbox();
        }
        else
        {           
            LoadTRInline();
            /*
            Dictionary<string, int> dic = PageListV;
            foreach (KeyValuePair<string, int> kvp in dic)
                PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
             * */
        }
        
        Header.DataBind();
        SetToolboxEvents();
    }

    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (GetRefundsReportRequestV == null)
            return;
        WebReferenceSite.GetRefundsReportRequest _request = GetRefundsReportRequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        DataResult dt = GetDataReport(_request);
        BindData(dt.data);
        LoadGeneralData(dt.TotalPages, dt.TotalRows, dt.CurrentPage);
    }
    private void LoadAllowedToRecord()
    {        
        string result = string.Empty;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        try
        {
            result = _site.GetConfigurationSettings();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["Configurations"] == null || xdd["Configurations"]["Error"] != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == RECORD_KEY)
            {
                AllowedToRecord = (node["Value"].InnerText == "1");
                break;
            }
        }
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lblTitleRefund.Text);
    }
    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {        
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        WebReferenceSite.GetRefundsReportRequest _request = GetRefundsReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataTable data = GetDataReport(_request).data;
        ToExcel te = new ToExcel(this, "Report");
        if (!te.ExecExcel(data))
            Update_Faild();
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {        
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        WebReferenceSite.GetRefundsReportRequest _request = GetRefundsReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataTable data = GetDataReport(_request).data;
        Session["data_print"] = data;
        Session["grid_print"] = _reapeter;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    private void LoadStatus()
    {
        Dictionary<string, string> RefundStatusesV = new Dictionary<string, string>();
        ddl_Status.Items.Add(new ListItem("--- " + lbl_all.Text + " ---", "-1"));
        foreach (WebReferenceSite.RefundSatus rs in Enum.GetValues(typeof(WebReferenceSite.RefundSatus)))
        {
            string translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "RefundSatus", rs.ToString());
            RefundStatusesV.Add(rs.ToString(), translate);
            ddl_Status.Items.Add(new ListItem(translate, "" + (int)rs));
            if (rs == WebReferenceSite.RefundSatus.Pending)
                ddl_Status.Items[ddl_Status.Items.Count - 1].Selected = true;
        }
        
        
    }
    

    private DataResult GetDataReport(WebReferenceSite.GetRefundsReportRequest _request)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        
        DataTable data = new DataTable();
        data.Columns.Add("IncidentId");
        data.Columns.Add("TicketNumber", typeof(string));
        data.Columns.Add("SupplierId", typeof(string));
        data.Columns.Add("SupplierName", typeof(string));
        data.Columns.Add("RefundReasonName", typeof(string));

        data.Columns.Add("RefundReasonCode");
        data.Columns.Add("RefundRequestDate");
        data.Columns.Add("PrimaryExpertiseName");
        data.Columns.Add("Description");
        data.Columns.Add("RefundStatus");

        data.Columns.Add("Price");
        data.Columns.Add("PublisherReplay");

        data.Columns.Add("Recording");
        data.Columns.Add("HasRecord", typeof(bool));

        data.Columns.Add("RefundRequests");
        data.Columns.Add("ApprovedRequest");
        data.Columns.Add("RefundStatusValue");
        data.Columns.Add("CanRefund", typeof(bool));
        data.Columns.Add("CaseScript", typeof(string));
        data.Columns.Add("PublisherReplayReadOnly", typeof(bool));
        data.Columns.Add("PublisherReplayClass", typeof(string));
        data.Columns.Add("a_class", typeof(string));

        WebReferenceSite.ResultOfGetRefundsReportResponse result = null;
        try
        {
            result = _site.GetRefundsReport(_request);
        }
        catch (Exception ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        
        if(RefundStatusesV==null || RefundStatusesV.Count==0)
            RefundStatusesV = GetRefundStatusTranslate();
        foreach (WebReferenceSite.RefundData _data in result.Value.RefundRequests)
        {
            DataRow row = data.NewRow();
           
            string _path = _data.RecordFileLocation; 
            
            row["Recording"] = "javascript:OpenRecord('" +  EncryptString.EncodeRecordPath(_data.RecordFileLocation) + "');";
            row["HasRecord"] = !string.IsNullOrEmpty(_path);

            string incidentId = _data.CaseId.ToString();
            row["IncidentId"] = incidentId;
            row["TicketNumber"] = _data.TicketNumber;
            row["SupplierId"] = _data.SupplierId.ToString();
            row["SupplierName"] = _data.SupplierName;
            row["RefundReasonName"] = _data.RefundReasonName;
            row["RefundReasonCode"] = _data.RefundReasonCode;

            row["RefundRequestDate"] = string.Format(siteSetting.DateFormat, _data.RefundRequestDate) + " " +
                string.Format(siteSetting.TimeFormat, _data.RefundRequestDate);
            row["PrimaryExpertiseName"] = _data.ExpertiseName;
            row["Description"] = _data.RefundNoteForAdvertiser;// node.Attributes["RefundNoteForAdvertiser"].Value;

            row["PublisherReplay"] = _data.RefundNote;// node.Attributes["RefundNote"].Value;
            row["RefundRequests"] = string.Format(NUMBER_FORMAT, _data.RefundRequestPercentage) + "%";
            row["ApprovedRequest"] = string.Format(NUMBER_FORMAT, _data.RefundsApprovedPercentage) + "%";

            string _RefundStatus = _data.RefundStatus;
            row["RefundStatus"] = RefundStatusesV[_RefundStatus];
            row["RefundStatusValue"] = _RefundStatus;
            row["Price"] = string.Format("{0:0.##}", _data.Price);
            row["CanRefund"] = (_RefundStatus == WebReferenceSite.RefundSatus.Pending.ToString());
            string CaseScript = string.Empty;
            if (_data.CaseId == Guid.Empty)
            {
                CaseScript = "return false;";
                row["a_class"] = "WithoutUnderline";
            }
            else
            {
                CaseScript = "OpenIframe('RequestTicket.aspx?RequestTicket=" + incidentId + "'); return false;";
                row["a_class"] = "";
            }
            row["CaseScript"] = CaseScript;
            bool IsReadOnly = (_RefundStatus != WebReferenceSite.RefundSatus.Pending.ToString());
            row["PublisherReplayReadOnly"] = IsReadOnly;
            row["PublisherReplayClass"] = (IsReadOnly) ? "form-textarealarge2p Label_ro" :
                "form-textarealarge2p _PublisherReplay";
            data.Rows.Add(row);
        }
        DataResult dr = new DataResult() { data = data, CurrentPage = result.Value.CurrentPage, TotalRows = result.Value.TotalRows, TotalPages = result.Value.TotalPages };
        return dr;
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = _TotalRows + " " + ToolboxReport1.RecordMached;
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    Dictionary<string, string> GetRefundStatusTranslate()
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        foreach (string str in Enum.GetNames(typeof(WebReferenceSite.RefundSatus)))
        {
            dic.Add(str,
                EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "RefundSatus", str));
        }
        return dic;
    }
    protected void FromToDate1_ReportExec(Object sender, EventArgs e)
    {
        LoadReport();
    }
    void LoadReport()
    {

        WebReferenceSite.GetRefundsReportRequest _request = new WebReferenceSite.GetRefundsReportRequest();
        DateTime to_date = FromToDate1.GetDateTo;
        DateTime from_date = FromToDate1.GetDateFrom;

        if (to_date == DateTime.MinValue)
        {
            to_date = DateTime.Now;
        }

        if (from_date == DateTime.MinValue)
        {
            from_date = to_date.AddMonths(-1);
        }
        else
        {
            if (from_date > to_date)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + FromToDate1.GetDateError + "');", true);
                return;
            }
        }
        _request.FromDate = from_date;
        _request.ToDate = to_date;
        int statusCode = int.Parse(ddl_Status.SelectedValue);
        if (statusCode != -1)
            _request.RefundStatus = (WebReferenceSite.RefundSatus)statusCode;
        _request.PageNumber = 1;
        _request.PageSize = TablePaging1.ItemInPage;
        GetRefundsReportRequestV = _request;
        DataResult dr = GetDataReport(_request);
        if (dr == null)
        {
            Update_Faild();
            ClearTable();
            return;
        }
        if (dr.data.Rows.Count == 0)
        {
            ClearTable();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
            return;
        }

        BindData(dr.data);
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
    }
    void ClearTable()
    {
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        LoadGeneralData(0, 0, 0);
        _uPanel.Update();
    }
    protected void BindData(DataTable data)
    {
        sb_Script = new StringBuilder();
        _reapeter.DataSource = data;
        _reapeter.DataBind();
        if (sb_Script != null && sb_Script.Length > 0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "DisabledBlackList", sb_Script.ToString(), true);
        _uPanel.Update();
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem _item = e.Item;
        if (!(_item.ItemType == ListItemType.Item || _item.ItemType == ListItemType.AlternatingItem))
            return;
        
        HtmlTableRow trDetails = (HtmlTableRow)_item.FindControl("tr_openClose");
        trDetails.Attributes.Add("style", "display:none");
        HtmlTableRow tr = (HtmlTableRow)_item.FindControl("tr_show");
        HtmlAnchor a_open = (HtmlAnchor)_item.FindControl("a_open");
        HtmlTableCell td = (HtmlTableCell)_item.FindControl("td_openClose");
        tr.Attributes["class"] = (_item.ItemIndex % 2 == 0) ? "odd" : "even";
        a_open.Attributes["href"] = "javascript:OpenCloseDetail('" + trDetails.ClientID + "','" + td.ClientID + "');";

        string RefundStatusValue = ((Label)_item.FindControl("lbl_RefundStatusValue")).Text;
        CheckBox cb = (CheckBox)_item.FindControl("cb_BlackList");
        DropDownList _ddl = (DropDownList)_item.FindControl("ddl_RefundStatus");

        foreach (KeyValuePair<string, string> kvp in RefundStatusesV)
        {
            if (kvp.Key == WebReferenceSite.RefundSatus.OnHold.ToString())
                continue;
            ListItem list_item = new ListItem(kvp.Value, kvp.Key);

            list_item.Selected = (RefundStatusValue == kvp.Key);
            _ddl.Items.Add(list_item);
        }
        Panel _li = (Panel)_item.FindControl("li_ClackList");
        Button _btn = (Button)_item.FindControl("btnSave");

        string script = "BlackListVisible(this, '" + WebReferenceSite.RefundSatus.Approved.ToString() +
            ";" + WebReferenceSite.RefundSatus.Pending.ToString() +
            "', '" + _li.ClientID + "','" + _btn.ClientID + "');";
        _ddl.Attributes.Add("onchange", script);
        HtmlGenericControl _label = (HtmlGenericControl)_item.FindControl("for_BlackList");

        _label.Attributes.Add("for", cb.ClientID);
        if (sb_Script == null)
            sb_Script = new StringBuilder();
        if (RefundStatusValue != WebReferenceSite.RefundSatus.Approved.ToString())
            sb_Script.Append("DisabledServer('" + _li.ClientID + "', true);");
        if (RefundStatusValue == WebReferenceSite.RefundSatus.Pending.ToString())
            sb_Script.Append("DisabledServer('" + _btn.ClientID + "', true);");
    }
    protected void btnSave_click(object sender, EventArgs e)
    {
        RepeaterItem _item = (RepeaterItem)(((Button)sender).NamingContainer);
        CheckBox cb_BlackList = (CheckBox)_item.FindControl("cb_BlackList");
        Guid SupplierId = new Guid(((Label)_item.FindControl("lbl_SupplierId")).Text);
        Guid IncidentId = new Guid(((Label)_item.FindControl("lbl_IncidentId")).Text);
        string PublisherReplay = ((TextBox)_item.FindControl("txt_PublisherReplay")).Text;
        string SupplierName = ((LinkButton)_item.FindControl("lb_SupplierName")).Text;
        string TicketNumber = ((LinkButton)_item.FindControl("lb_TicketNumber")).Text;
        DropDownList ddl = (DropDownList)_item.FindControl("ddl_RefundStatus");
        WebReferenceSite.RefundSatus rs = (WebReferenceSite.RefundSatus)class_enum.GetValue(typeof(WebReferenceSite.RefundSatus), ddl.SelectedValue);
        if (SendRefund(SupplierId, IncidentId, PublisherReplay, rs, SupplierName, TicketNumber))
        {
            if (cb_BlackList.Checked)
            {
                WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
                WebReferenceSite.SetBlackListStatusRequest req = new WebReferenceSite.SetBlackListStatusRequest();
                req.IncidentId = IncidentId;
                req.BlackListStatusToSet = true;
                WebReferenceSite.ResultOfSetBlackListStatusResponse result = null;
                try
                {
                    result = _site.SetBlackListStatus(req);

                }
                catch (Exception ex)
                {
                    dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "FaildBlackList", "alert('" + lbl_BlackListProblem.Text + "');", true);
                    return;
                }
                if (result.Type == WebReferenceSite.eResultType.Failure)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "FaildBlackList", "alert('" + lbl_BlackListProblem.Text + "');", true);
                    return;
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "UpdateSuccess();", true);
                if (result.Value.Status == WebReferenceSite.BlackListStatus.NoChange)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "AlreadyExists", "alert('" + lbl_NumAlreadyExists.Text + "');", true);
            }
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
            LoadReport();
        }
        
    }
    private bool SendRefund(Guid SupplierId, Guid IncidentId, string PublisherReplay,
        WebReferenceSite.RefundSatus RefundStatus, string SupplierName, string TicketNumber)
    {
        
      //  int ReasonCode;

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.UpsertRefundRequest urr = new WebReferenceSite.UpsertRefundRequest();
        urr.SupplierId = SupplierId;
        urr.RefundOwnerId = new Guid(userManagement.GetGuid);
        /*TODO ask Alain?????
        if (int.TryParse(_ids[2], out ReasonCode))
            urr.RefundReasonCode = ReasonCode;
         * */
        urr.RefundNote = PublisherReplay;
        urr.RefundStatus = RefundStatus;
        urr.IncidentId = IncidentId;
        WebReferenceSite.ResultOfUpsertRefundResponse result = null;
        bool IsUpsert = true;
        try
        {
            result = _site.UpsertRefund(urr);
        }
        catch (Exception ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            IsUpsert = false;
            //          return;
        }
        if (IsUpsert && result.Type == WebReferenceSite.eResultType.Failure)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            IsUpsert = false;
            ///       return;
        }
        /*
        if (IsUpsert)        
            SetRefundAudit(SupplierId, SupplierName, TicketNumber);
         * */
        return IsUpsert;
    }
    protected void lb_Name_click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        string _guid = lnk.CommandArgument;
        
        string name = lnk.Text;
        UserMangement um = new UserMangement(_guid, name);
        SetGuidSetting(um);
        string _script = @"$(document).ready(function(){$.colorbox({href:'framepublisher.aspx',width:'95%', height:'90%', iframe:true});});";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "colorbox", _script, true);
    }

   
    /*
    void SetRefundAudit(Guid SupplierId, string SupplierName, string TicketNumber)
    {
        /*
        DataTable data = dataV;
        DataRow row = null;
   
        foreach (DataRow _row in data.Rows)
        {
            if (_row["TicketNumber"].ToString() == hf_TicketNumber.Value)
            {
                row = _row;
                break;
            }
        }
         * /
        OneCallUtilities.OneCallUtilities ocu = WebServiceConfig.GetOneCallUtilitiesReference(this);
        OneCallUtilities.AuditRequest _request = new OneCallUtilities.AuditRequest();
        OneCallUtilities.AuditEntry _audit = new OneCallUtilities.AuditEntry();
        string pagename =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        string page_translate = EditTranslateDB.GetPageNameTranslate(pagename, siteSetting.siteLangId);
        _audit.AdvertiseId = SupplierId;
        _audit.AdvertiserName = SupplierName;
        _audit.AuditStatus = OneCallUtilities.Status.Insert;
        _audit.FieldId = lbl_RefundThisCall.ID;
        _audit.FieldName = lbl_RefundThisCall.Text;
        _audit.OldValue = string.Empty;
        _audit.UserId = new Guid(userManagement.GetGuid);
        _audit.UserName = userManagement.User_Name;
        _audit.NewValue = lblTicketNumber.Text + "-" + TicketNumber;
        _audit.PageName = page_translate;
        _audit.PageId = pagename;
        _request.AuditOn = true;
        _request.AuditEntries = new OneCallUtilities.AuditEntry[] { _audit };
        OneCallUtilities.Result result = null;
        try
        {
            result = ocu.CreateAudit(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
        }
    }
     */
    /*
    HtmlTableCell MakeCell(string _item, int indx)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();
        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }
     * */
    private void LoadTRInline()
    {
        List<string> list = listTR;

        foreach (RepeaterItem item in _reapeter.Items)
        {
            //           HtmlGenericControl div = (HtmlGenericControl)item.FindControl("div_openClose");
            HtmlTableRow trDetails = (HtmlTableRow)item.FindControl("tr_openClose");
            HtmlTableCell td_open = (HtmlTableCell)item.FindControl("td_openClose");
            if (list.Contains(trDetails.ClientID))
            {
                trDetails.Attributes.Remove("style");
                td_open.Attributes["class"] = "open first";
            }
            else
            {
                trDetails.Attributes["style"] = "display:none";
                td_open.Attributes["class"] = "close first";
            }
        }

    }
    /*
   protected void lnkNextPage_Click(object sender, EventArgs e)
   {
       int cp = CurrentPage;
       cp++;
       CurrentPage = cp;
       listTR = new List<string>();
       BindData(dataV);
   }
   
   protected void lnkPreviousPage_Click(object sender, EventArgs e)
   {
       int cp = CurrentPage;
       cp--;
       CurrentPage = cp;
       listTR = new List<string>();
       BindData(dataV);
   }
   protected void lnkPage_Click(object sender, EventArgs e)
   {
       int cp = int.Parse(((LinkButton)sender).CommandArgument);
       CurrentPage = cp - 1;
       listTR = new List<string>();
       BindData(dataV);
   }
   void LoadPages(int countRows)
   {
       PlaceHolderPages.Controls.Clear();
       int countPages = countRows / ITEM_PAGE;
       countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;
       //if there is only one page
       if (countPages == 1)
       {
           div_paging.Visible = false;
           PageListV = null;
           return;
       }

       Dictionary<string, int> dic = new Dictionary<string, int>();
       List<HtmlTableCell> tc = new List<HtmlTableCell>();
       int cp = CurrentPage + 1;
       int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

       if (start > PAGE_PAGES)
       {
           int indx = start - PAGE_PAGES;
           dic.Add("...", indx);
       }
       int i = start;
       for (; i <= countPages &&
           i < (start + PAGE_PAGES); i++)
       {
           dic.Add("" + i, i);
       }
       if (i <= countPages)
       {
           int indx = start + PAGE_PAGES;
           dic.Add("...", indx);
       }
       PageListV = dic;
       foreach (KeyValuePair<string, int> kvp in dic)
           PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
   }
    * */
    /*
    protected void ib_record_click(Object sender, EventArgs e)
    {
        ImageButton ib = (ImageButton)sender;
        RepeaterItem _itam = (RepeaterItem)ib.NamingContainer;

        string _path = ((Label)_itam.FindControl("lbl_record")).Text;
        if (string.IsNullOrEmpty(_path) || !File.Exists(_path))
        {
            string script = "alert('" + lbl_RecordError.Text + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "RecordError", script, true);
            return;
        }

        string TempPath = ConfigurationManager.AppSettings["professionalRecord"];
        string TempPathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"];

        string fileName = System.IO.Path.GetFileName(_path);
        File.Copy(_path, TempPath + fileName, true);
       
        div_record_in.InnerHtml = GetHtmlObject(TempPathWeb + fileName);
        _updateIframe.Update();
     //   div_record.Attributes.Add("class", "div_record");
    //    _mpe.Show();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "openRecord", "openRecord();", true);
    }
     * */
    private string GetHtmlObject(string _path)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<object data=""../play/dewplayer.swf"" width=""200"" height=""20"" name=""dewplayer"" id=""dewplayer"" type=""application/x-shockwave-flash"">");
        sb.Append(@"<param name=""movie"" value=""../play/dewplayer.swf"" />");
        sb.Append(@"<param name=""flashvars"" value=""mp3=" + _path + @"&amp;autostart=1"" />");
        sb.Append(@"<param name=""wmode"" value=""transparent"" />");
        //      sb.Append(@"<embed wmode=""transparent"" src=""flash/home.swf" width="220" height="271" /> 
        sb.Append(@"</object>");
        return sb.ToString();
    }
    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        return "";
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        List<string> list = listTR;
        string[] args = eventArgument.Split(';');
        if (args[1] == "1")
            list.Add(args[0]);
        else
            list.Remove(args[0]);
        listTR = list;
    }

    #endregion
    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }
    /*
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? null : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }*/
    List<string> listTR
    {
        get { return (Session["list_TR"] == null) ? new List<string>() : (List<string>)Session["list_TR"]; }
        set { Session["list_TR"] = value; }
    }
    /*
    int CurrentPage
    {
        get
        {
            //Look for current page in ViewState 
            object o = this.ViewState["CurrentPage"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["CurrentPage"] = value;
        }
    }
    Dictionary<string, int> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<string, int>() :
              (Dictionary<string, int>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }
     * */
    /*
    Dictionary<int, string> RecordListV
    {
        get { return (Session["RecordList"] == null) ? new Dictionary<int, string>() : (Dictionary<int, string>)Session["RecordList"]; }
        set { Session["RecordList"] = value; }
    }
     
    protected string GetAudioChrome
    {
        get { return ResolveUrl("~") + "Management/AudioChrome.aspx"; }
    }
     * */
    /*
    Dictionary<string, string> RefundStatusesV
    {
        get { return (ViewState["RefundStatuses"] == null) ? new Dictionary<string, string>() : (Dictionary<string, string>)ViewState["RefundStatuses"]; }
        set { ViewState["RefundStatuses"] = value; }
    }
     * */
    protected bool AllowedToRecord
    {
        get { return (ViewState["AllowedToRecord"] == null) ? false : (bool)ViewState["AllowedToRecord"]; }
        set { ViewState["AllowedToRecord"] = value; }
    }
    protected int GetColSpan()
    {
        return (AllowedToRecord) ? 10 : 9;
    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
    WebReferenceSite.GetRefundsReportRequest GetRefundsReportRequestV
    {
        get { return (ViewState["GetRefundsReportRequest"] == null) ? null : (WebReferenceSite.GetRefundsReportRequest)ViewState["GetRefundsReportRequest"]; }
        set { ViewState["GetRefundsReportRequest"] = value; }
    }
    protected string GetRecord
    {
        get { return ResolveUrl("~/Publisher/DpzControls/ZapRecord.aspx"); }
    }
    
}
