﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.IO;

public partial class Publisher_PublisherPpcReport : PageSetting
{
    const string FILE_NAME = "PPC_Report.xml";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            SetToolbox();
            LoadHeadings();
        }
        SetToolboxEvents();
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_PPCReport.Text);
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (!string.IsNullOrEmpty(PathV))
        {
            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSArea.swf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + _chart + "');", true);
        }

    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, "Report");
        if (!te.ExecExcel(dataV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordToPrint + "');", true);
            return;
        }
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    private void LoadHeadings()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = _site.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["PrimaryExpertise"] == null || xdd["PrimaryExpertise"]["Error"] != null)
        {           
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XmlNode nodePrimary in xdd["PrimaryExpertise"].ChildNodes)
        {
            string pName = nodePrimary.Attributes["Name"].InnerText;            
            string _guid = nodePrimary.Attributes["ID"].Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
    }

    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.PreDefinedPriceReportRequest _request = new WebReferenceReports.PreDefinedPriceReportRequest();
        WebReferenceReports.ResultOfPreDefinedPriceReportResponse result = null;
        _request.FromDate = FromToDate1.GetDateFrom;
        _request.ToDate = FromToDate1.GetDateTo;
        _request.HeadingId = new Guid(ddl_Heading.SelectedValue);
        try
        {
            result = _report.PreDefinedPriceReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        DataTable data = GetDataTable.GetDataTableFromListCorrectNumberDateTimeFormat(result.Value.DataList, siteSetting.DateFormat);
        _GridView.DataSource = data;
        _GridView.DataBind();
        lbl_RecordMached.Text = ToolboxReport1.GetRecordMaches(data.Rows.Count);
        _UpdatePanel.Update();
        dataV = data;
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + ToolboxReport1.NoRecordToPrint + "');", true);
            PathV = string.Empty;
        }
        else
            LoadChart(result.Value.DataList);
        /*
        result.Value.DataList
        result.Value.DataList[0].ActualPrice;
        result.Value.DataList[0].Date;
        result.Value.DataList[0].PreDefinedPrice;
         * */
    }
    #region  chart
    void LoadChart(WebReferenceReports.PreDefinedPriceData[] result)
    {        
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + lbl_PPCReport.Text + @"' ");
        //    sb.Append(@"subCaption='In Thousands' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='19' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");        
        sb.Append(@"numberprefix='" + siteSetting.CurrencySymbol + @"' ");

        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar='=' ");

        sb.Append(@"bgColor='FFFFFF'>");


     //   int NumOf_X = result.CurrentGraph.Count();

        int modulu = result.Count() / 15;
        modulu++;

        sb.Append(@"<categories>");
        for (int i = 0; i < result.Count(); i++)
        {
            string _title = lbl_PreDefinePPC.Text + "= " +
                siteSetting.CurrencySymbol + String.Format("{0:0.##}", result[i].PreDefinedPrice) + "\r\n" +
                lbl_ActualPPC.Text + "= " + siteSetting.CurrencySymbol + String.Format("{0:0.##}", result[i].ActualPrice) + 
                "\r\n" + String.Format(siteSetting.DateFormat, result[i].Date) + 
                "\r\n" + lbl_CurrentDot.Text;
            
            //***Dilute labels***//
            if ((i) % modulu == 0)
                sb.Append(@"<category name='" + String.Format(siteSetting.DateFormat, result[i].Date) + @"' toolText='" + _title + "'/>");
            else
                sb.Append(@"<category toolText='" + _title + "'/>");
        }
        sb.Append("</categories>");





        
        sb.Append(@"<dataset seriesName='" + lbl_PreDefinePPC.Text + @"' color='808080'  anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'>");
        for (int i = 0; i < result.Count(); i++)
        {
            sb.Append(@"<set value='" + String.Format("{0:0.##}", result[i].PreDefinedPrice) + "'/>");
        }
        sb.Append(@"</dataset>");

        sb.Append(@"<dataset seriesName='" + lbl_ActualPPC.Text + @"' color='008040'  anchorBgColor='008040' plotBorderColor='#008040' plotBorderThickness='3'>");
        for (int i = 0; i < result.Count(); i++)
        {
            sb.Append(@"<set value='" + String.Format("{0:0.##}", result[i].ActualPrice) + "'/>");
        }
        sb.Append(@"</dataset>");
        

       

        sb.Append(@"</graph>");



        string path = //HttpContext.Current.Request.PhysicalApplicationPath + 
            ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) 
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ErrorChart", "alert('" + lbl_ErrorChart.Text + "');", true);
            PathV = string.Empty;
            return;
        }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        
    }
    #endregion
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    private string PathV
    {
        get { return (ViewState["Path"] == null) ? string.Empty : (string)ViewState["Path"]; }
        set { ViewState["Path"] = value; }
    }
}
