﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_BillingReport : PageSetting
{
    const string Num_Format = "{0:#,0.##}";
    const int WEEK_FROM_TODAY = 6;
    delegate void AsyncBrokersLeads(WebReferenceReports.BrokersLeadsReportRequest _request);
    protected void Page_Load(object sender, EventArgs e)
    {
    //    ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(ddl_Brokers);
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(lb_RunReport);
        
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
            LoadMonths();
            LoadYears();
            LoadBrokers();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    private void setDateTime()
    {
   //     _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
  //          WEEK_FROM_TODAY);
        _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_BillingReport.Text);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
      
        
    }
    
    private void LoadYears()
    {
        ddl_Years.Items.Clear();
        int thisyear = DateTime.Now.Year;
        for (int i = 2012; i < 2021; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            li.Selected = i == thisyear;
            ddl_Years.Items.Add(li);
        }
    }

    private void LoadBrokers()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetAllBrokers();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        ddl_Brokers.Items.Clear();
        ddl_Brokers.Items.Add(new ListItem("--GROUP BY BROKER--", Guid.Empty.ToString()));
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            ddl_Brokers.Items.Add(new ListItem(gsp.Name, gsp.Id.ToString()));
        }
        ddl_Brokers.SelectedIndex = 0;
    }

    private void LoadMonths()
    {
        int month = DateTime.Now.Month;
        ddl_Month.Items.Clear();
        foreach (eMonths em in Enum.GetValues(typeof(eMonths)))
        {
            ListItem li = new ListItem(em.ToString(), ((int)em).ToString());
            li.Selected = (int)em == month;
            ddl_Month.Items.Add(li);
        }
    }
    /*
    void Brokers_Leads(WebReferenceReports.BrokersLeadsReportRequest _request)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfBrokersLeadsDataForReportResponse resultBrokersLeads = null;
        try
        {
            resultBrokersLeads = _report.BrokersLeadsReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            return;
        }
        if (resultBrokersLeads.Type == WebReferenceReports.eResultType.Failure)
            return;
        div_Net_Actual_Revenue.Visible = true;
        lbl_Net_Actual_Revenue_result.Text = string.Format(Num_Format, resultBrokersLeads.Value.Sum(x => x.Price));
    }
     * */
    void ExecReport()
    {
        Guid BrokerId = Guid.Parse(ddl_Brokers.SelectedValue);
        if (BrokerId == Guid.Empty)
        {
            BrokerSummary();
            return;
        }
        WebReferenceReports.BrokersLeadsReportRequest _request = new WebReferenceReports.BrokersLeadsReportRequest();
        _request.BrokerId = BrokerId;
        if (cb_MonthDate.Checked)
        {
            _request.fromDate = _FromToDatePicker.GetDateFrom;
            _request.toDate = _FromToDatePicker.GetDateTo.AddDays(1);
        }
        else
        {
            
            int _Month = int.Parse(ddl_Month.SelectedValue);
            int _Year = int.Parse(ddl_Years.SelectedValue);
            _request.fromDate = new DateTime(_Year, _Month, 1);
            _request.toDate = _request.fromDate.AddMonths(1);
        }
        _request.IncludeNonBilledCalls = cb_IncludeNonBilledCalls.Checked;
        /*
        WebReferenceReports.BrokersLeadsReportRequest _BrokersLeadsReportRequest = new WebReferenceReports.BrokersLeadsReportRequest();
        _BrokersLeadsReportRequest.BrokerId = _request.BrokerId;
        _BrokersLeadsReportRequest.Month = _request.Month;
        _BrokersLeadsReportRequest.Year= _request.Year;
         * */
        /*
        AsyncBrokersLeads abl = new AsyncBrokersLeads(Brokers_Leads);
        IAsyncResult AsyResult = null;
        AsyResult = abl.BeginInvoke(_BrokersLeadsReportRequest, null, null);

         */
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        
        WebReferenceReports.ResultOfListOfBrokersLeadsDataForReportResponse _result = null;
        try
        {
            _result = _reports.BrokersLeadsReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (_result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        _BillingTable.Visible = true;
        div_Net_Actual_Revenue.Visible = true;        
        _GridView.Visible = false;
        dataV = null;
        lbl_Net_Actual_Revenue_result.Text = string.Format(Num_Format, _result.Value.Sum(x => x.Price));
        var first = _result.Value.FirstOrDefault();
        if (first != null)
        {
            div_last_update.Visible = true;
            lbl_last_update_result.Text = first.LastUpdate.ToString("dd MMM yy - HH:mm");
        }
        lbl_RecordMached.Text = _result.Value.Length + " " + ToolboxReport1.RecordMached;
        div_NumRequests.Visible = false;
   //     if(
        _BillingTable.BindReport(_result.Value);
   //     abl.EndInvoke(AsyResult);
        _UpdatePanel.Update();
    }

    private void BrokerSummary()
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.BrokersSummaryReportRequest _request = new WebReferenceReports.BrokersSummaryReportRequest();
        if (cb_MonthDate.Checked)
        {
            _request.From = _FromToDatePicker.GetDateFrom;
            _request.To = _FromToDatePicker.GetDateTo.AddDays(1);
            _request.CalculateBillingRateByDates = cb_BillingRateByDates.Checked;
        }
        else
        {
            int _Month = int.Parse(ddl_Month.SelectedValue);
            int _Year = int.Parse(ddl_Years.SelectedValue);
            _request.From = new DateTime(_Year, _Month, 1);
            _request.To = _request.From.AddDays(DateTime.DaysInMonth(_Year, _Month));
            _request.CalculateBillingRateByDates = cb_BillingRateByDates.Checked;
        }
    //    _request.From = dt;
    //    _request.To = dt_end;
        WebReferenceReports.ResultOfListOfBrokerSummaryRow result = null;
        try
        {
            result = _report.BrokersSummaryReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        DataTable data = new DataTable();//GetDataTable.GetDataTableFromListCorrectNumber(result.Value, siteSetting.CurrencySymbol);
        data.Columns.Add("Name");
        data.Columns.Add("Count");
        data.Columns["Count"].Caption = "IsNumber";
        data.Columns.Add("Money");
        data.Columns["Money"].Caption = "IsCurrency;" + siteSetting.CurrencySymbol;
        data.Columns.Add("BillingRate");
        data.Columns.Add("BillingRateByDates");
        data.Columns.Add("Refund");
        data.Columns["Refund"].Caption = "IsNumber";
        data.Columns.Add("TotalRefunds");
        data.Columns["TotalRefunds"].Caption = "IsCurrency;" + siteSetting.CurrencySymbol;
        data.Columns.Add("GrossRevenue");
        data.Columns["GrossRevenue"].Caption = "IsCurrency;" + siteSetting.CurrencySymbol;

      //  data.Columns["Money"].Caption = "IsPercentage";
        
        foreach (WebReferenceReports.BrokerSummaryRow bsr in result.Value)
        {
          //  System.Data.OleDb.OleDbType.
            DataRow row = data.NewRow();
            row["Name"] = bsr.Name;
            row["Count"] = bsr.Count;
            row["Money"] = siteSetting.CurrencySymbol + bsr.Money.ToString("#0.##");
            row["BillingRate"] = String.Format("{0:P2}", bsr.BillingRate);
            row["BillingRateByDates"] = cb_BillingRateByDates.Checked ? String.Format("{0:P2}", bsr.BillingRatePerDates) : "-";
            if (bsr.RefundCount > -1)
            {
                row["Refund"] = bsr.RefundCount;
                row["TotalRefunds"] = siteSetting.CurrencySymbol + bsr.RefundSum.ToString("#0.##");
            }
            row["GrossRevenue"] = siteSetting.CurrencySymbol + bsr.RevenueGross.ToString("#0.##");
            data.Rows.Add(row);
        }

        dataV = data;
        _BillingTable.Visible = false;
        div_Net_Actual_Revenue.Visible = true;
        div_last_update.Visible = true;
        _GridView.Visible = true;
        lbl_Net_Actual_Revenue_result.Text = siteSetting.CurrencySymbol + string.Format(Num_Format, result.Value.Sum(x => x.Money));
        lbl_last_update_result.Text = "Now";
        lbl_RecordMached.Text = data.Rows.Count + " " + ToolboxReport1.RecordMached;
        div_NumRequests.Visible = true;
        lbl_NumRequests.Text = result.Value.Sum(x => x.Count).ToString();

        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();
        
    }
    
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }
    protected string GetServicePage
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/GetBillingReportPage"); }
    }
    protected string GetCreateExcel
    {
        get 
        { 
            return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcel"); 
        }
    }
    protected string GetCreateExcelBillingTableAll
    {
        get
        {
            return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelBillingTableAll");
        }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
    protected string record_path
    {
        get { return ResolveUrl("~/Publisher/DpzControls/ZapRecord.aspx"); }
    }
    protected string GetGuidEmpty
    {
        get { return Guid.Empty.ToString(); }
    }
    DataTable dataV
    {
        get { return (Session["dataBR"] == null) ? null : (DataTable)Session["dataBR"]; }
        set { Session["dataBR"] = value; }
    }
    DataTable dataExcell
    {
        get { return (Session["dataExcellBR"] == null) ? null : (DataTable)Session["dataExcellBR"]; }
        set { Session["dataExcellBR"] = value; }
    }

   
}