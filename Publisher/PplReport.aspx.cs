﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;

public partial class Publisher_PplReport : PageSetting
{
   // const int DAYS_INTERVAL = 2;
    const string Num_Format = "{0:#,0.##}";
    const string Num_Float_Format = "{0:#,0.####}";
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(ddl_States);
  //      ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(btn_run);
        if (!IsPostBack)
        {         
            SetToolbox();
            LoadHeadings();
            LoadStates();
            LoadCities();            
        }
        RegularExpressionValidator_ZipCode.ValidationExpression = siteSetting.GetZipCodeRegularExpression;
        SetToolboxEvents();
        Header.DataBind();
    }
    private void SetToolboxEvents()
    {
        /*
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.SetButtonPrintAsAsync();
         * */
        _Toolbox.RemoveExcelButton();
        _Toolbox.RemovePrintButton();
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
 //       throw new NotImplementedException();
    }

    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
  //      throw new NotImplementedException();
    }
    private void LoadHeadings()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;
    }
    private void LoadCities()
    {
        Guid _id;
        if (!Guid.TryParse(ddl_States.SelectedValue, out _id))
            _id = Guid.Empty;
        ddl_Cities.Items.Clear();
        ddl_Cities.Items.Add(new ListItem("--ALL--", Guid.Empty.ToString()));

        if (_id == Guid.Empty)
            return;
        Dictionary<Guid, string> dic = SalesUtility.GetRegionsByLevel(siteSetting.GetUrlWebReference, 3, _id);       
        foreach (KeyValuePair<Guid, string> kvp in dic)
        {
            ddl_Cities.Items.Add(new ListItem(kvp.Value, kvp.Key.ToString()));
        }
        ddl_Cities.SelectedIndex = 0;
     //   ScriptManager.RegisterStartupScript(this, this.GetType(), "span_cover_Cities", "$('#span_cover_Cities').hide();", true);
    }

    private void LoadStates()
    {
        Dictionary<Guid, string> dic = SalesUtility.GetRegionsByLevel(siteSetting.GetUrlWebReference, 1);
        if (dic == null)
        {
            Update_Faild();
            return;
        }
        ddl_States.Items.Clear();
        ddl_States.Items.Clear();
        ddl_States.Items.Add(new ListItem("--ALL--", Guid.Empty.ToString()));
        foreach (KeyValuePair<Guid, string> kvp in dic)
        {
            ddl_States.Items.Add(new ListItem(kvp.Value, kvp.Key.ToString()));
        }
        ddl_States.SelectedIndex = 0;
    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lbl_PplReport.Text);
    }
    /*
    void SetDatePicker()
    {
      //  _FromToDatePicker.SetFromSentence = lbl_CreationDate.Text;
        _FromToDatePicker.SetIntervalDateServer(DAYS_INTERVAL);
    }
     * */
    protected void btn_run_Click(object sender, EventArgs e)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.PplReportRequest _request = new WebReferenceReports.PplReportRequest();
        Guid HeadingId;
        if (!Guid.TryParse(ddl_Heading.SelectedValue, out HeadingId))
            return;
        _request.HeadingId = HeadingId;
        Guid RegionId;
        if (string.IsNullOrEmpty(txt_ZipCode.Text))
        {
            if (!Guid.TryParse(ddl_Cities.SelectedValue, out RegionId))
                RegionId = Guid.Empty;


            if (RegionId == Guid.Empty)
            {
                if (!Guid.TryParse(ddl_States.SelectedValue, out RegionId))
                    return;
            }
            _request.RegionId = RegionId;
        }
        else
            _request.ZipCode = txt_ZipCode.Text;

        
        
        WebReferenceReports.ResultOfListOfPplReportData result = null;
       // _report.Timeout
        try
        {
            result = _report.PplReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        
        _PplControl.LoadData(result.Value, ddl_Heading.SelectedItem.Text);
        UpdatePanel_table.Update();


    }
    protected void ddl_States_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCities();
        _UpdatePanel_cities.Update();
    }
    
}