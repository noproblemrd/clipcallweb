﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="OptOutAdvancedReport.aspx.cs" Inherits="Publisher_OptOutAdvancedReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js"></script>
<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }

   
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" >
    
</cc1:ToolkitScriptManager>
<uc1:ToolboxReport ID="_Toolbox" runat="server" />
<div class="page-content minisite-content2"> 
  <div id="form-analytics">
    	       
      <div class="main-inputs">	
               
            <div class="form-field">
                <asp:Label ID="lbl_Origin" runat="server" CssClass="label" Text="Origin"></asp:Label>          
	            <asp:DropDownList ID="ddl_Origin" CssClass="form-select" runat="server">
                    
               </asp:DropDownList>
            </div>
            <div class="form-field">
                <asp:Label ID="lbl_TypeReport" runat="server" CssClass="label" Text="Type"></asp:Label>
                <asp:DropDownList ID="ddl_TypeReport" CssClass="form-select" runat="server">
                    <asp:ListItem Value="Regular" Text="Regular" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Advanced" Text="Advanced"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        
       
        <div class="callreport">
              
             <uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" />
        </div>
        <div class="clear"></div>
        <div class="heading">
                <asp:Button ID="btn_Run" runat="server" CssClass="CreateReportSubmit2" 
                    Text="Create report" onclick="btn_Run_Click" 
                    />
            </div>
            
           
            
            <div class="table2">
            
                <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                 <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                   
                    <asp:GridView ID="_GridViewRegular" runat="server" AutoGenerateColumns="false"
                            CssClass="data-table2">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />        
                       
                        <Columns>
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="<%# lbl_Detail.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text="<%# Bind('Str') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label3" runat="server" Text="<%# lbl_Count.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text="<%# Bind('Int') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="_GridViewAdvanced" runat="server" AutoGenerateColumns="false"
                            CssClass="data-table2">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />        
                       
                        <Columns>
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label10" runat="server" Text="<%# lbl_Keyword.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text="<%# Bind('Keyword') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label13" runat="server" Text="<%# lbl_Flavor.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label14" runat="server" Text="<%# Bind('Flavor') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label15" runat="server" Text="<%# lbl_LeadCount.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label16" runat="server" Text="<%# Bind('LeadCount') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label17" runat="server" Text="<%# lbl_OptOutCount.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label18" runat="server" Text="<%# Bind('OptOutCount') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            
    </div>
</div>

    <asp:Label ID="lbl_TitleReport" runat="server" Text="Opt Out Addon Report" Visible="false"></asp:Label>
    <%/* Regular Report */ %>
    <asp:Label ID="lbl_Detail" runat="server" Text="Detail" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Count" runat="server" Text="Count" Visible="false"></asp:Label>
    <%/* Advanced Report */ %>
    <asp:Label ID="lbl_Keyword" runat="server" Text="Keyword" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Flavor" runat="server" Text="Flavor" Visible="false"></asp:Label>
    <asp:Label ID="lbl_LeadCount" runat="server" Text="Lead Count" Visible="false"></asp:Label>
    <asp:Label ID="lbl_OptOutCount" runat="server" Text="Opt Out Count" Visible="false"></asp:Label>
</asp:Content>

