﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Deposits.aspx.cs" Inherits="Publisher_Deposits" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
    /*
        window.onload = appl_init;
        function appl_init() {

            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);

        }
        function BeginHandler() {
            parent.showDiv();
        }
        function EndHandler() {
            parent.hideDiv();

        }  
        */
        function _pay() {
        /*
            $('span.error-msg').each(function () {
                $(this).html('');
            });
            */
            $('#<%# lbl_AmountError.ClientID %>').html('');
            var num = $('#<%# txt_AmountOfCharging.ClientID %>').val();
            if(num.length == 0 || isNaN(num)){
                MustEnterNumber();
                return false;
            }
            var _minval;
            var _ChargingType = $('#<%# ddl_ChargingType.ClientID %>').val();
            if(_ChargingType == "<%# GetBalanceLow %>"){
                _minval = $('#<%# hf_LowerThanCall.ClientID %>').val();
            }
            else{
                _minval = $('#<%# hf_MaximumMonthly.ClientID %>').val();
            }
            var min_val = parseInt(_minval);
            
            if (min_val > num){
                MinimumChargeError(min_val);
                return false;
            }
            return true;
        }

        function updateIsExemptOfMonthlyPayment(){
            var original = ($('#hf_ExemptOriginalState').val() == 1);
            var newValue = $('#cb_IsExemptOfMonthlyPayment').prop('checked');
            if(original == newValue){
                alert('Nothing to update');
                return;
            }
            $("#wait").css("display","block");
            var xmlReq = $.ajax({
            url: "<%#urlUpdateExemption%>",
            data: { siteId: "<%#_cache.ZapSiteId%>", userId: "<%#userManagement.GetGuid %>", userName: "<%#userManagement.User_Name %>", supplierGuid: "<%#supplierId %>", originalState: $('#hf_ExemptOriginalState').val(), newExemptValue: $('#cb_IsExemptOfMonthlyPayment').prop('checked'), supplierName: "<%#supplierName %>" },
            dataType: "json",
            type: "POST"            
            });
            xmlReq.done(function( msg ) {
                alert(msg);
                if(msg == 'OK'){
                    if($('#cb_IsExemptOfMonthlyPayment').prop('checked')){
                        $('#hf_ExemptOriginalState').val('1');
                    }
                    else{
                        $('#hf_ExemptOriginalState').val('0');
                    }
                    location.reload(true);
                }
                else{
                    $("#wait").css("display","none");
                }
            });
        }        

        function MinimumChargeError(min_val) {
            $('#<%# lbl_AmountError.ClientID %>').html('<%# GetMinimumChargeError %>' + ' ' + min_val);
        }
        function MustEnterNumber() {
            $('#<%# lbl_AmountError.ClientID %>').html('<%# MustEnterNumber %>');
        }
        /*
        $(function () {
            $('input[name="ChargingType"]').change(function () {
                $('input[name="ChargingType"]:not(:checked)').each(function () {
                    $(this).parents('tr').find('input[type="text"]').val('');
                });
            });
        });
          */
        

        function calculations() {
            
            $('#<%# txt_AmountOfCharging.ClientID %>').keyup(function (e) {
                var _amount = $(this).val();
                if (_amount.length == 0 || isNaN(_amount))
                    _amount = 0;
                var amount = new Number(_amount)
                var segment = $('#<%# lbl_RelativeAmount.ClientID %>').html();
                amount += parseInt(segment);
                var LastCharging = $('#<%# hf_AmountOfCharging.ClientID %>').val();
                amount = amount - parseInt(LastCharging);
                if (amount < 0)
                    amount = 0;
                $('#<%# lbl_TotalAmonut.ClientID %>').html(amount);
                var AmountVAT = Math.round(amount * <%# VAT %>);
                $('#<%# lblTotalIncludeVat.ClientID %>').html(AmountVAT);
                $('#<%# lb_Paymaster.ClientID %>').html((amount == 0) ? '<%# lbl_Update.Text %>' : '<%# lbl_Paymaster.Text %>');
            });
            $('#<%# txt_AmountOfCharging.ClientID %>').blur(function () {
                _pay();
            });
            $('#<%# ddl_ChargingType.ClientID %>').change(function () {
                _pay();
            });
           
        }

        $(calculations);

    </script>
</head>
<body style="background-color:transparent;">
    <form id="form1" runat="server">
    <div class="NewDeposite">
        <h2 class="title-deposite">
            <asp:Label ID="lbl_CustomerPayment" runat="server" Text="Customer Payment"></asp:Label> 
        </h2>
        <div>
           <h3>
               <asp:Label ID="lbl_HeadingBasedPayment" runat="server" Text="Customers headings"></asp:Label>             
           </h3> 
        </div>    
        
        <asp:Repeater ID="_Repeater" runat="server">
            <ItemTemplate>
                <div class="Heading-table">
                    <div class="Black-Circle">
                       <asp:Label ID="lbl_BlackCircle" runat="server" Text="<%# BlackCircle %>"></asp:Label>
                    </div>
                    <div class="heading-">
                        <asp:Label ID="lbl_Segment" runat="server" Text="<%# Bind('Segment') %>"></asp:Label>
                <asp:HiddenField ID="hf_HeadingId" runat="server" Value="<%# Bind('HeadingId') %>" />
                    </div>
                </div>
                <div class="clear"></div>
            </ItemTemplate>
        </asp:Repeater>       
        
        <div class="PaymentDetails">            
            <asp:Label ID="lbl_BasicMonthly" runat="server" Text="Basic monthly PPA" CssClass="PaymentDetailsTitle"></asp:Label>
            <span class="Relative-Amount">
                <asp:Label ID="lbl_RelativeAmount" runat="server" Text="0"></asp:Label>
                <asp:Label ID="lblCurrency" runat="server" Text="<%# GetCurrency %>"></asp:Label>
            </span>
            
            <asp:Label ID="lbl_MonthlyPayment" runat="server" Text="Monthly payment" CssClass="PaymentDetailsTitle"></asp:Label>
            <span>
                <asp:Label ID="lbl_MonthlyAmount" runat="server" Text="0"></asp:Label>
                <asp:Label ID="Label1" runat="server" Text="<%# GetCurrency %>"></asp:Label>
            </span>
        </div>
        <div class="div_table_deposite"> 
            <table class="PaymentType">
                
                <tr>
                    <td>
                        <asp:Label ID="lbl_IsExemptOfMonthlyPayment" runat="server" Text="Exempt of monthly payment"></asp:Label>:
                    </td>
                    <td>
                        <asp:CheckBox ID="cb_IsExemptOfMonthlyPayment" runat="server" />
                        <a onclick="updateIsExemptOfMonthlyPayment();" ><asp:Label ID="lbl_UpdateIsExemptOfMonthlyPayment" runat="server" Text="Update"></asp:Label></a>
                    </td>   
                    <td></td>                 
                </tr>
                
                <tr>                
                <td>
                     <asp:Label ID="lbl_AmountOfCharging" runat="server" Text="Amount of charging calls"></asp:Label>:
                </td>
                <td>
                     <asp:TextBox ID="txt_AmountOfCharging" runat="server" CssClass="form-text"></asp:TextBox>
                    <asp:HiddenField ID="hf_AmountOfCharging" runat="server" Value="0" />
                </td>
                <td>
                    <asp:Label ID="lbl_AmountError" runat="server" CssClass="error-msg"></asp:Label>
                </td>
                </tr>

                <tr>
                <td>
                    <asp:Label ID="lbl_ChargingType" runat="server" Text="Charging type"></asp:Label>:
                </td>                
                <td>
                    <asp:DropDownList ID="ddl_ChargingType" runat="server" CssClass="form-select">
                        
                    </asp:DropDownList>
                </td>
                </tr>

                <tr>
                <td>
                    <asp:Label ID="lbl_PaymentMethod" runat="server" Text="Payment method"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_CreditCard" runat="server" Text="Credit card"></asp:Label>
                </td>                 
                </tr>

                <tr class="deposit-total">
                <td>
                    <asp:Label ID="lbl_TotalCurrentBilling" runat="server" Text="Total Current Billing"></asp:Label>
                </td>
                <td>
                    <span>
                        <asp:Label ID="lbl_TotalAmonut" runat="server" Text="0"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Text="<%# GetCurrency %>"></asp:Label>
                    </span>
                </td>              
                </tr>

                <tr class="deposit-total">
                <td>
                    <asp:Label ID="lbl_TotalIncludeVat" runat="server" Text="Total include VAT"></asp:Label>
                </td>
                <td>
                    <span>
                        <asp:Label ID="lblTotalIncludeVat" runat="server" Text="0"></asp:Label>
                        <asp:Label ID="Label5" runat="server" Text="<%# GetCurrency %>"></asp:Label>
                    </span>
                </td>              
                </tr>
            </table>
            <div id="wait" style="display:none;border:1px solid black;position:absolute;top:35%;left:45%;padding:2px;"><img src="../UpdateProgress/ajax-loader.gif" /><br />Loading..</div>
            <div class="div-DepositeButoons">
                <div class="div-butons" runat="server" id="div_butons">
                    <asp:LinkButton ID="lb_Paymaster" runat="server" onclick="lb_Paymaster_Click" CssClass="btn-pay" OnClientClick="return _pay();"></asp:LinkButton>
                    <asp:LinkButton ID="lb_UpdateCreditCard" runat="server" onclick="lb_UpdateCreditCard_Click" CssClass="btn-update" Text="Update Credit Card"></asp:LinkButton>

                </div>
                    <asp:Label ID="lbl_CanMakeSale" runat="server" Text="You can not make a sale, the customer is in a status of 'Legal Handling', you should contact the Finance Department." 
                        Visible="false" CssClass="error-msg" style="font-size: 20px;"></asp:Label>
                    
            </div>            
            <asp:HiddenField ID="hf_LowerThanCall" runat="server" />
            <asp:HiddenField ID="hf_MaximumMonthly" runat="server" />
            <asp:HiddenField ID="hf_ExemptOriginalState" runat="server" />
        </div>
    
    </div>
    <asp:Label ID="lbl_ForMonth" runat="server" Text="For Month" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ForThisMonth" runat="server" Text="For This Month"  Visible="false"></asp:Label>

    <asp:Label ID="lbl_MinimumCharge" runat="server" Text="Minimum charge" Visible="false"></asp:Label>
    <asp:Label ID="lbl_MustEnterNumber" runat="server" Text="Must enter amount in numbers" Visible="false"></asp:Label>

    <asp:Label ID="lbl_Paymaster" runat="server" Text="Paymaster" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Update" runat="server" Text="Update" Visible="false"></asp:Label>

    </form>
</body>
</html>
