﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using Telerik.Web.UI;


public partial class Publisher_Ticket : PageSetting
{
    
    const string TIME_FORMAT = "{0:HH:mm}";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Notes1.SetNoteTab += new EventHandler(Notes1_SetNoteTab);
        ScriptManager1.RegisterAsyncPostBackControl(lb_OpenNotes);
        ScriptManager1.RegisterAsyncPostBackControl(btn_save);
        if (!IsPostBack)
        {
            if (userManagement == null || !userManagement.IsPublisher())
                Response.Redirect("LogOut.aspx");
            this.DataBind();
            LoadTimeFollowUp();
            LoadPublisherUsers();            
            LoadStatus_Severity();
            LoadHelpDeskTypes();
            LoadTicketDetails();
            ClientScript.RegisterStartupScript(this.GetType(), "HideInsert", "HideInsert();", true);
            
            
        }
        
        LoadCheckLength();
        
        Header.DataBind();

        
    }

    private void LoadTimeFollowUp()
    {
        for (int i = 8; i < 21; i++)
        {
            string s = string.Empty;
            if (i < 10)
                s = "0" + i;
            else
                s = i + "";
            string val = s + ":00";
            ListItem li0 = new ListItem(val, val);
            val = s + ":30";
            ListItem li1 = new ListItem(val, val);
            ddl_Time.Items.Add(li0);
            ddl_Time.Items.Add(li1);            
        }
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
    }
    protected void lb_OpenNotes_Click(object sender, EventArgs e)
    {
        SetNoteTab();
        Notes1.LoadData(TicketIdV, WebReferenceSite.NoteType.HelpDesk);
    }
    private void LoadHelpDeskTypes()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = new WebReferenceSite.ResultOfListOfGuidStringPair();
        try
        {
            result = _site.GetAllHelpDeskTypes();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            if (gsp.Id == HelpDeskTypeIdV)
                lbl_Title_Title.Text = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId,
                    "eHelpDeskType", gsp.Name);
        }
    }
    private void LoadStatus_Severity()
    {
        string help_desk_type = Request.QueryString["HelpDesk"];
        if (string.IsNullOrEmpty(help_desk_type) || !Utilities.IsGUID(help_desk_type))
            return;
        HelpDeskTypeIdV = new Guid(help_desk_type);
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfSeverityAndStatusContainer result = new WebReferenceSite.ResultOfSeverityAndStatusContainer();
        try
        {
            result = _site.GetAllHelpDeskStatusesAndSeverities(new Guid(help_desk_type));
            
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Status.Items.Clear();
    //    ddl_Status.Items.Add(new ListItem(lbl_All.Text, Guid.Empty.ToString()));
        foreach (WebReferenceSite.HdStatusData hsd in result.Value.StatusList)
        {
            ListItem li = new ListItem(hsd.Name, hsd.Id.ToString());
            li.Enabled = !hsd.Inactive;
            ddl_Status.Items.Add(li);
        }
        ddl_Severity.Items.Clear();
        foreach (WebReferenceSite.HdSeverityData hsd in result.Value.SeverityList)
        {
            ListItem li = new ListItem(hsd.Name, hsd.Id.ToString());
            li.Enabled = !hsd.Inactive;
            ddl_Severity.Items.Add(li);
        }
        
        
    }

    private void LoadTicketDetails()
    {
        string _id = Request.QueryString["Ticket"];
        if (string.IsNullOrEmpty(_id))
        {
            LoadPartialDetails();
            return;
        }
        /*
        string help_desk_type = Request.QueryString["HelpDesk"];
        if (string.IsNullOrEmpty(_id) || string.IsNullOrEmpty(help_desk_type))
            return;
         * */
       
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.GetHdEntriesRequest _request = new WebReferenceSite.GetHdEntriesRequest();
       
        _request.TicketNumber = _id;
        _request.HdTypeId = HelpDeskTypeIdV;

        WebReferenceSite.ResultOfListOfHdEntryData result = new WebReferenceSite.ResultOfListOfHdEntryData();
        try
        {
            result = _site.GetHelpDeskEntries(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Value.Count() != 1)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        WebReferenceSite.HdEntryData hed = result.Value[0];
        lbl_Title_Name.Text = hed.TicketNumber;

   //     RadComboBox_Consumer.Text = hed.InitiatorName;// SecondaryRegardingObject;
        RadComboBoxItem item_Data = new RadComboBoxItem();
        TicketIdV = hed.Id;
        item_Data.Text = hed.InitiatorName;
        item_Data.Value = hed.InitiatorId.ToString();
        item_Data.Selected = true;
        RadComboBox_Consumer.Items.Clear();
        RadComboBox_Consumer.Items.Add(item_Data);
        txt_Title.Text = hed.Title;
        lbl_TitleCount2.Text = hed.Title.Length + "";
        txt_Description.Text = hed.Description;
        lbl_DescriptionCount2.Text = hed.Description.Length + "";
   //     txt_Call.Text = hed.RegardingObjectName;
     //   CallService _cs = new CallService();
   //     string _call = _cs.GetOneItem(hed.RegardingObjectName);
        RadComboBoxItem itemData = new RadComboBoxItem();
        itemData.Text = hed.SecondaryRegardingObject + ", " + hed.RegardingObjectName;
        itemData.Value = hed.RegardingObjectId.ToString();
        itemData.Selected=true;
        RadComboBox_Call.Items.Clear();
        RadComboBox_Call.Items.Add(itemData);
        txt_Advertiser.Text = hed.SecondaryRegardingObject;
        hf_ConsumerId.Value = hed.InitiatorId.ToString();
    //    RadComboBox_Call.
        string status_id = hed.StatusId.ToString();
        foreach (ListItem li in ddl_Status.Items)
        {
            bool IsMatch = (li.Value == status_id);
            if (IsMatch)
            {
                if (!li.Enabled)
                {
                    li.Enabled = true;
                    li.Attributes.Add("Inactive", "true");
                }
            }
            li.Selected = IsMatch;
        }
        string Severity_id = hed.SeverityId.ToString();
        foreach (ListItem li in ddl_Severity.Items)
        {
            bool IsMatch = (li.Value == Severity_id);
            if (IsMatch)
            {
                if (!li.Enabled)
                {
                    li.Enabled = true;
                    li.Attributes.Add("Inactive", "true");
                }
            }
            li.Selected = IsMatch;
        }
        if (hed.FollowUp != null)
        {
            txt_FollowUp.Text = string.Format(siteSetting.DateFormat, hed.FollowUp);
            string time_value = string.Format(TIME_FORMAT, hed.FollowUp);
            foreach (ListItem li in ddl_Time.Items)
                li.Selected = (li.Value == time_value);
        }
        string user_id = hed.OwnerId.ToString();
        foreach (ListItem li in ddl_Owner.Items)
        {
            li.Selected = (li.Value == user_id);
        }
  //      txt_CreatedBy.Text = PublisherUsersV[hed.CreatedById];
        user_id = hed.CreatedById.ToString();
        for(int i = ddl_CreateBy.Items.Count - 1; i > -1; i--)
        {
            if(ddl_CreateBy.Items[i].Value != user_id)
                ddl_CreateBy.Items.RemoveAt(i);
        }

        txt_CreatedOn.Text = string.Format(siteSetting.DateFormat, hed.CreatedOn) + " " +
            string.Format(siteSetting.TimeFormat, hed.CreatedOn);

    }
    private void LoadPartialDetails()
    {
        string consumer = Request.QueryString["Consumer"];
        if (!string.IsNullOrEmpty(consumer))
        {
            ConsumerService _cs = new ConsumerService();
            string _consumer = _cs.GetOneItemByGuid(consumer);
            string[] consumers = _consumer.Split(new string[]{";;;;"}, StringSplitOptions.RemoveEmptyEntries);
            if (consumers.Length > 1)
            {
                RadComboBoxItem item_Data = new RadComboBoxItem();
                item_Data.Text = consumers[0];
                item_Data.Value = consumers[1];
                item_Data.Selected = true;
                RadComboBox_Consumer.Items.Clear();
                RadComboBox_Consumer.Items.Add(item_Data);
                hf_ConsumerId.Value = consumers[1];
            }
        }
        string call = Request.QueryString["Call"];
        if (!string.IsNullOrEmpty(call))
        {
            CallService __cs = new CallService();
            string _call = __cs.GetOneItemByGuid(call);
            string[] calls = _call.Split(new string[] { ";;;;" }, StringSplitOptions.RemoveEmptyEntries);
            if (calls.Length > 1)
            {
                RadComboBoxItem item_Data = new RadComboBoxItem();
                string[] supplier = calls[0].Split(',');
                item_Data.Text = supplier[1];
                item_Data.Value = calls[1];
                item_Data.Selected = true;

                RadComboBox_Call.Items.Clear();
                RadComboBox_Call.Items.Add(item_Data);
                
                txt_Advertiser.Text = supplier[0];
            }
        }
       
        il_Notes.Visible = false;
        a_OpenDetails.HRef = "javascript:void(0);";
        lbl_Title_Name.Text = lbl_new.Text;
    }
    private void LoadCheckLength()
    {
        string value_title = "checkMeasageLength(this, 200, '" + lbl_TitleCount2.ClientID + "');";
        string value_description = "checkMeasageLength(this, 1000, '" + lbl_DescriptionCount2.ClientID + "');";
        txt_Title.Attributes.Add("onkeypress", value_title);
        txt_Title.Attributes.Add("onkeyup", value_title);
        txt_Description.Attributes.Add("onkeypress", value_description);
        txt_Description.Attributes.Add("onkeyup", value_description);
    }
    private void LoadPublisherUsers()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllPublishersResponse result = new WebReferenceSite.ResultOfGetAllPublishersResponse();
        try
        {
            result = _site.GetAllPublishers();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Owner.Items.Clear();
        ddl_CreateBy.Items.Clear();

        foreach (WebReferenceSite.PublisherData pd in result.Value.Publishers)
        {
            ddl_Owner.Items.Add(new ListItem(pd.Name, pd.AccountId.ToString()));
            ListItem _li = new ListItem(pd.Name, pd.AccountId.ToString());
            _li.Selected = (new Guid(userManagement.GetGuid) == pd.AccountId);
            ddl_CreateBy.Items.Add(_li);
        }        
        ddl_Owner.SelectedIndex = 0;
        
    }
    void Notes1_SetNoteTab(object sender, EventArgs e)
    {
        SetNoteTab();
    }
    void SetNoteTab()
    {
        div_Details.Style[HtmlTextWriterStyle.Display] = "none";
        div_Notes.Style[HtmlTextWriterStyle.Display] = "block";
        lb_OpenNotes.CssClass = "selected";
        a_OpenDetails.Attributes.Remove("class");
        _UpdatePanel_TabsMenu.Update();
    }

    
    void SetDetailTab()
    {
        div_Details.Style[HtmlTextWriterStyle.Display] = "block";
        div_Notes.Style[HtmlTextWriterStyle.Display] = "none";
        lb_OpenNotes.CssClass = "";
        a_OpenDetails.Attributes["class"] = "selected";
        _UpdatePanel_TabsMenu.Update();
    }
    protected void btn_save_click(object sender, EventArgs e)
    {
        SetDetailTab();
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.HdEntryData hed = new WebReferenceSite.HdEntryData();
        hed.TypeId = HelpDeskTypeIdV;
        if (TicketIdV != Guid.Empty)
            hed.Id = TicketIdV;
        string _consumer = RadComboBox_Consumer.SelectedValue;
        if (!Utilities.IsGUID(_consumer))
        {
            lbl_missing_consumer.Style[HtmlTextWriterStyle.Display] = "inline";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "missing_value", "alert('" + hf_MissingFields.Value + "');", true);
            return;
        }
        hed.InitiatorId = new Guid(_consumer);        
        hed.Title = txt_Title.Text;
        hed.Description = txt_Description.Text;
        string _call = RadComboBox_Call.SelectedValue;
        if (!Utilities.IsGUID(_call))
        {
            lbl_missing_call.Style[HtmlTextWriterStyle.Display] = "inline";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "missing_value", "alert('" + hf_MissingFields.Value + "');", true);
            return;
        }
        hed.RegardingObjectId = new Guid(_call);
        hed.StatusId = new Guid(ddl_Status.SelectedValue);
        hed.SeverityId = new Guid(ddl_Severity.SelectedValue);
        if (!string.IsNullOrEmpty(txt_FollowUp.Text))
        {
            string[] _times = ddl_Time.SelectedValue.Split(':');
            DateTime date = ConvertToDateTime.CalanderToDateTime(txt_FollowUp.Text, siteSetting.DateFormat);

            hed.FollowUp = new DateTime(date.Year, date.Month, date.Day, int.Parse(_times[0]), int.Parse(_times[1]), 0);
        }
        hed.CreatedById = new Guid(ddl_CreateBy.SelectedValue);
        hed.OwnerId = new Guid(ddl_Owner.SelectedValue);

        WebReferenceSite.ResultOfGuid result = null;
        try
        {
            result = _site.UpsertHelpDeskEntry(hed);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
//        string _script = "try {parent.CloseIframe_after();}catch(ex){}";
        string _script = "on_close_iframe();";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseIframe", _script, true);

    }
   


  

    Guid HelpDeskTypeIdV
    {
        get { return (Session["HelpDeskTypeId"] == null) ? Guid.Empty : (Guid)Session["HelpDeskTypeId"]; }
        set { Session["HelpDeskTypeId"] = value; }
    }
    Dictionary<Guid, string> PublisherUsersV
    {
        get { return (ViewState["PublisherUsers"] == null) ? new Dictionary<Guid, string>() : (Dictionary<Guid, string>)ViewState["PublisherUsers"]; }
        set { ViewState["PublisherUsers"] = value; }
    }
    Guid TicketIdV
    {
        get { return (ViewState["TicketId"] == null) ? Guid.Empty : (Guid)ViewState["TicketId"]; }
        set { ViewState["TicketId"] = value; }
    }
    
}
