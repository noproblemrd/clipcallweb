﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class Publisher_BlackList : PageSetting
{
    const string SEARCH_NUM = "InsertWord";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetToolBox();
        }
        
        SetToolBoxEvents();
        Header.DataBind();       
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (IsPostBack && CBsV != string.Empty)            
                SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }

    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
        Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
   //     Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
    }

    

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        bool IfSuccess = true;
        foreach (GridViewRow row in _GridViewNumbers.Rows)
        {
            string _phone = ((Label)row.FindControl("lblPhoneNumber")).Text;
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
                if (InsertDeleteNumber(_phone, false) == StatusUpdateBlackList.FAILD)
                    IfSuccess = false;
        }
        if (IfSuccess)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "DeleteError", "alert('" + Toolbox1.DeleteError + "');", true);
        Toolbox1.GetTextBoxSearch.Value = string.Empty;
        _search_num();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            return;
        }
 //       PrintHelper.PrintWebControl(_GridViewNumbers, dataV);
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridViewNumbers;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    

    private void SetToolBox()
    {
        Toolbox1.RemoveEdit();
        Toolbox1.RemoveAttach();
        Toolbox1.SearchValidation(@"^\\d+$", lbl_OnlyDigits.Text);
        string _reg1 = @"^\d+$";
        string _reg = (string.IsNullOrEmpty(siteSetting.GetPhoneRegularExpression) || siteSetting.GetPhoneRegularExpression == @"\d+") ?
            siteSetting.GetMobileRegularExpression :
            "(" + siteSetting.GetMobileRegularExpression + ")|(" + siteSetting.GetPhoneRegularExpression + ")";
        string _messages = lbl_OnlyDigits.Text + "|||" + lbl_invalidPhoneNumber.Text;
        Toolbox1.AddValidationExists(Utilities.RegularExpressionForJavascript(_reg1 + "|||" + _reg), 
            _lbl_missing.Text, _messages);
        
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordExport + "');", true);
             return;            
        }
        ToExcel to_excel = new ToExcel(this, "BlackList");
        if(!to_excel.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        _search_num();
    }
    void Toolbox1_AddExec(object sender, EventArgs e)
    {
        string phonNum = Toolbox1.GetTxt;
        StatusUpdateBlackList _status = InsertDeleteNumber(phonNum, true);
        if (_status == StatusUpdateBlackList.FAILD)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        else if (_status == StatusUpdateBlackList.AlreadyExists)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AlreadyExists", "alert('" + lbl_NumAlreadyExists.Text + "');", true);
            return;
        }
        Toolbox1.GetTextBoxSearch.Value = string.Empty;
        _search_num();
    }

    
    protected void _search_num()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.SearchBlackListRequest _search = new WebReferenceSite.SearchBlackListRequest();
        _search.PhoneNumber = Toolbox1.GetTxt; //txt_number.Text;
        HtmlInputText _tb = Toolbox1.GetTextBoxSearch;
        if (_search.PhoneNumber == Toolbox1.GetWaterTextSearch &&
                    _tb.Attributes["class"].Contains("watermark"))
            _search.PhoneNumber = string.Empty;
        WebReferenceSite.ResultOfSearchBlackListResponse _response = null;
        try
        {
            _response = _site.SearchBlackList(_search);
        }
        catch (Exception exc)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (_response.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        DataTable data = new DataTable();
        data.Columns.Add("PhoneNumber");
        foreach (string s in _response.Value.PhoneNumbers)
        {
            DataRow row = data.NewRow();
            row["PhoneNumber"] = s;
            data.Rows.Add(row);
        }
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + lbl_noreasultSearch.Text + "');", true);
            _GridViewNumbers.PageIndex = 0;
            _GridViewNumbers.DataSource = null;
            _GridViewNumbers.DataBind();
            return;
        }
        dataV = data;
        _GridViewNumbers.PageIndex = 0;
        _GridViewNumbers.DataSource = data;
        _GridViewNumbers.DataBind();
        SetGridView();
        
        UpdatePanelNumbers.Update();

    }


    StatusUpdateBlackList InsertDeleteNumber(string phonNum, bool ToAdd)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.SetBlackListStatusRequest _request = new WebReferenceSite.SetBlackListStatusRequest();
        _request.PhoneNumber = phonNum;
        _request.BlackListStatusToSet = ToAdd;
        WebReferenceSite.ResultOfSetBlackListStatusResponse _response = null;
        try
        {
            _response = _site.SetBlackListStatus(_request);
        }
        catch (Exception ex)
        {
      //      ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return StatusUpdateBlackList.FAILD;
        }
        if (_response.Type == WebReferenceSite.eResultType.Failure)
        {
     //       ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return StatusUpdateBlackList.FAILD;
        }
        if (_response.Value.Status == WebReferenceSite.BlackListStatus.NoChange)
        {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "AlreadyExists", "alert('" + lbl_NumAlreadyExists.Text + "');", true);
            return StatusUpdateBlackList.AlreadyExists;
        }
    //    else
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);

        return StatusUpdateBlackList.SUCCESS;
    }
    //
    /*
    protected void lb_Delete_Click(object sender, EventArgs e)
    {
        string phonNum = ((LinkButton)sender).CommandArgument;
        InsertDeleteNumber(phonNum, false);
    }
     * */
    protected void _GridViewBadWords_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridViewNumbers.PageIndex = e.NewPageIndex;
        _GridViewNumbers.DataSource = dataV;
        _GridViewNumbers.DataBind();
        SetGridView();
        UpdatePanelNumbers.Update();
    }
    void SetGridView()
    {
        StringBuilder sb = new StringBuilder();
        HtmlInputCheckBox cb_all = ((HtmlInputCheckBox)_GridViewNumbers.HeaderRow.FindControl("cb_all"));
   //     CheckBox cb_all = ((CheckBox)_GridViewNumbers.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('"+cb_all.ClientID+"');"); 
        string _script = "CheckSetAllCBs('"+cb_all.ClientID+"');";
        foreach (GridViewRow row in _GridViewNumbers.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
            /*
            LinkButton lb = (LinkButton)row.FindControl("lb_Delete");
            string num = lb.CommandArgument;
            string message = hf_confirmDelete.Value + " " + num;
            
            lb.OnClientClick = "return ConfirmDelete('" + message + "');";
             * */
        }
        if (sb.Length > 0)
        {
            string CBs = sb.ToString().Substring(0, sb.Length-1);
         //   SetCBsList(CBs);
            CBsV = CBs;
        }
    }
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
   
}
enum StatusUpdateBlackList { SUCCESS, AlreadyExists, FAILD }
