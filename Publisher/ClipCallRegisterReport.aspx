﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ClipCallRegisterReport.aspx.cs" Inherits="Publisher_ClipCallRegisterReport" %>


<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
     <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
    <style type="text/css" >
        td.data-table-details div._left
        {
            float:left;
        }
    </style>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function _CreateExcel() {
            var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"AarCallCenterReport"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
        $(function () {
            $('.tr_show').each(function () {
                $(this).click(function () {
                    var _next = $(this).next('tr');
                    if (_next.is(":visible"))
                        _next.hide();
                    else
                        _next.show();
                });
            });
        })
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="Date-field" runat="server" id="div_Date">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  			
              <div class="form-field form-WidthDuration" runat="server" id="div_Step" style="top:0; margin-bottom:0;">
                    <asp:Label ID="lbl_Stpe" runat="server" Text="Step" CssClass="label"></asp:Label>
                    <asp:DropDownList ID="ddl_Step" CssClass="form-select" runat="server" ></asp:DropDownList>

                </div>
              
            <div class="form-field form-WidthDuration" runat="server" id="div1" style="top:0; margin-bottom:0; left: 515px;">
                <asp:Label ID="lbl_status" runat="server" Text="Supplier status" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_SupplierStatus" CssClass="form-select" runat="server" ></asp:DropDownList>

            </div>
             <div class="form-field form-WidthDuration" runat="server" id="div_oldPro" style="top:0; margin-bottom:0; left: 685px">
                    <asp:Label ID="Label43" runat="server" Text="Old Pro" CssClass="label"></asp:Label>
                    <asp:DropDownList ID="ddl_oldPro" CssClass="form-select" runat="server" ></asp:DropDownList>
                </div>
		</div>
      
		<div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>        
        <div id="Table_Report"  runat="server" >  
				<asp:Repeater runat="server" ID="_reapeter" 
                onitemdatabound="_reapeter_ItemDataBound">
                <HeaderTemplate>
                    <table class="data-table">
                        <thead>
                            <tr>                                
                                <th><asp:Label ID="Label31" runat="server" Text="Last update"></asp:Label></th>
						        <th><asp:Label ID="Label50" runat="server" Text="Name"></asp:Label></th>
                                <th><asp:Label ID="Label19" runat="server" Text="Status"></asp:Label></th>
                                <th><asp:Label ID="Label9" runat="server" Text="Categories"></asp:Label></th>
						        <th><asp:Label ID="Label1" runat="server" Text="Address"></asp:Label></th>
						        <th><asp:Label ID="Label51" runat="server" Text="Step"></asp:Label></th>
                                 <th><asp:Label ID="Label220" runat="server" Text="Phone"></asp:Label></th>
						        <th><asp:Label ID="Label32" runat="server" Text="Email"></asp:Label></th>
						        <th><asp:Label ID="Label53" runat="server" Text="Device"></asp:Label></th>
                                <th><asp:Label ID="Label2" runat="server" Text="License Number"></asp:Label></th>
                                <th><asp:Label ID="Label10" runat="server" Text="Insured"></asp:Label></th>
                                <th><asp:Label ID="Label11" runat="server" Text="BBB profile"></asp:Label></th>
                                <th><asp:Label ID="Label12" runat="server" Text="Worker's comp"></asp:Label></th>
                                <th><asp:Label ID="Label13" runat="server" Text="Bonded"></asp:Label></th>
                                <th><asp:Label ID="Label7" runat="server" Text="Doesn't has criminal record"></asp:Label></th>
                                <th><asp:Label ID="Label18" runat="server" Text="Is old pro"></asp:Label></th>
						    </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>

                <ItemTemplate>
                    <tr runat="server" id="tr_show" class="tr_show" style="cursor:pointer">                        
						<td><asp:Label ID="Label3" runat="server" Text="<%# Bind('LastUpdate') %>"></asp:Label></td>
						<td><asp:Label ID="Label55" runat="server" Text="<%# Bind('Name') %>"></asp:Label></td>
                        <td><asp:Label ID="Label38" runat="server" Text="<%# Bind('Status') %>"></asp:Label></td>
                        <td><asp:Label ID="Label8" runat="server" Text="<%# Bind('Categories') %>"></asp:Label></td>
                        <td><asp:Label ID="Label221" runat="server" Text="<%# Bind('Address') %>"></asp:Label></td>
						<td><asp:Label ID="Label56" runat="server" Text="<%# Bind('Step') %>"></asp:Label></td>
						<td><asp:Label ID="Label4" runat="server" Text="<%# Bind('Phone') %>"></asp:Label></td>
						<td><asp:Label ID="Label5" runat="server" Text="<%# Bind('Email') %>"></asp:Label></td>
                        <td><asp:Label ID="Label6" runat="server" Text="<%# Bind('Device') %>"></asp:Label></td>
						<td><asp:Image ID="Image2" runat="server" ImageUrl="<%# Bind('LicenseIcon') %>"></asp:Image></td>
                        <td><asp:Label ID="Label15" runat="server" Text="<%# Bind('Insured') %>"></asp:Label></td>                        <td>
                            <asp:HyperLink ID="HyperLink1" runat="server" Text="bbb" NavigateUrl="<%# Bind('bbb') %>" Visible="<%# Bind('bbbV') %>" Target="_blank"></asp:HyperLink></td>
                        <td><asp:Image ID="Image4" runat="server" ImageUrl="<%# Bind('WorkerscompIcon') %>"></asp:Image></td>
                         <td><asp:Image ID="Image5" runat="server" ImageUrl="<%# Bind('BondedIcon') %>"></asp:Image></td>
                        <td><asp:Image ID="Image6" runat="server" ImageUrl="<%# Bind('CriminalRecordIcon') %>"></asp:Image></td>
                        <td><asp:Image ID="Image10" runat="server" ImageUrl="<%# Bind('OldProIcon') %>"></asp:Image></td>
					</tr>
                    <tr id="tr_openClose" runat="server" style="display:none" class="tr_openClose">
                        <td colspan="14" class="data-table-details">
                            <div class="_left">
                                <asp:Label ID="Label007" runat="server" CssClass="label" Text="Description"></asp:Label>
                                <asp:Label ID="Label14" runat="server" Text="<%# Bind('Description') %>"></asp:Label>
                            </div>
                            <div class="clear"></div>
                              <div class="_left">
                                <asp:Label ID="Label37" runat="server" CssClass="label" Text="Website"></asp:Label>
                                  <asp:HyperLink ID="HyperLink2" runat="server" Text="Website" NavigateUrl="<%# Bind('Website') %>" Visible="<%# Bind('WebsiteV') %>" Target="_blank"></asp:HyperLink>
                            </div>
                                                       
                             <div class="_left">
                                <asp:Label ID="Label16" runat="server" CssClass="label" Text="Cover area radius"></asp:Label>
                                <asp:Label ID="Label17" runat="server" Text="<%# Bind('Radius') %>"></asp:Label>
                            </div>                           
                             <div class="_left">
                                <asp:Label ID="Label20" runat="server" CssClass="label" Text="First name"></asp:Label>
                                <asp:Label ID="Label21" runat="server" Text="<%# Bind('FirstName') %>"></asp:Label>
                            </div>
                             <div class="_left">
                                <asp:Label ID="Label22" runat="server" CssClass="label" Text="Last name"></asp:Label>
                                <asp:Label ID="Label23" runat="server" Text="<%# Bind('LastName') %>"></asp:Label>
                            </div>
                            <div class="_left">
                                <asp:Label ID="Label39" runat="server" CssClass="label" Text="Stripe account"></asp:Label>
                                <asp:Label ID="Label40" runat="server" Text="<%# Bind('StripeAccount') %>"></asp:Label>
                            </div>
                             <div class="_left">
                                <asp:Label ID="Label41" runat="server" CssClass="label" Text="Stripe standalone account"></asp:Label>
                                <asp:Label ID="Label42" runat="server" Text="<%# Bind('StripeStandaloneAccount') %>"></asp:Label>
                            </div>
                            <div class="clear"></div>
                              <div class="_left">
                                <asp:Label ID="Label24" runat="server" CssClass="label" Text="Yelp phone"></asp:Label>
                                <asp:Label ID="Label25" runat="server" Text="<%# Bind('YelpPhone') %>"></asp:Label>
                            </div>
                            <div class="_left">
                                <asp:Label ID="Label108" runat="server" CssClass="label" Text="Yelp name"></asp:Label>
                                <asp:Label ID="Label109" runat="server" Text="<%# Bind('YelpName') %>"></asp:Label>
                            </div>
                              <div class="_left">
                                <asp:Label ID="Label26" runat="server" CssClass="label" Text="Yelp rating"></asp:Label>
                                <asp:Label ID="Label27" runat="server" Text="<%# Bind('YelpRating') %>"></asp:Label>
                            </div>
                              <div class="_left">
                                <asp:Label ID="Label28" runat="server" CssClass="label" Text="Yelp reviews"></asp:Label>
                                <asp:Label ID="Label29" runat="server" Text="<%# Bind('YelpReviews') %>"></asp:Label>
                            </div>
                            <div class="_left">
                                <asp:Label ID="Label107" runat="server" CssClass="label" Text="Yelp url"></asp:Label>
                                <asp:HyperLink ID="HyperLink5" runat="server" Text="YelpUrl" NavigateUrl="<%# Bind('YelpUrl') %>" Visible="<%# Bind('YelpUrlV') %>" Target="_blank"></asp:HyperLink>
                            </div>
                            <div class="clear"></div>
                              <div class="_left">
                                <asp:Label ID="Label30" runat="server" CssClass="label" Text="Google rating"></asp:Label>
                                <asp:Label ID="Label33" runat="server" Text="<%# Bind('GoogleRating') %>"></asp:Label>
                            </div>
                              <div class="_left">
                                <asp:Label ID="Label34" runat="server" CssClass="label" Text="Google reviews"></asp:Label>
                                <asp:Label ID="Label35" runat="server" Text="<%# Bind('GoogleReviews') %>"></asp:Label>
                            </div>
                            <div class="_left">
                                <asp:Label ID="Label180" runat="server" CssClass="label" Text="Google url"></asp:Label>
                                <asp:HyperLink ID="HyperLink4" runat="server" Text="GoogleUrl" NavigateUrl="<%# Bind('GoogleUrl') %>" Visible="<%# Bind('GoogleUrlV') %>" Target="_blank"></asp:HyperLink>
                            </div>
                            <div class="clear"></div>
                             <div class="_left">
                                <asp:Label ID="Label36" runat="server" CssClass="label" Text="Portrait image"></asp:Label>
                                <asp:Image ID="Image7" runat="server" ImageUrl="<%# Bind('PortraitImage') %>" ></asp:Image>
                            </div> 

                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
			</div>
        </div>
    </div>
</asp:Content>

