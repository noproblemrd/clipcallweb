﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_SurveyReport : PageSetting
{
    const string TOD = "TOD";
    const string SURVEY = "SURVEY";
    class SurveyProjectAnswer
    {
        public Guid SurveyProjectId{get;set;}
        public string Answer{get;set;}
      //  public ClipCallReport.eUserMode UserMode {get;set;}
    }
    
    const string CUSTOMER = "Customer";
    const string SUPPLIER = "Supplier";
    const string REPORT_NAME = "Survey Report";
    protected static readonly string SessionTableName = "dataClipCallSurveyReport";
    static List<SurveyProjectAnswer> listCustomerSurveyAnswers;
    static List<SurveyProjectAnswer> listSupplierSurveyAnswers;

    static Publisher_SurveyReport()
    {
        GetAllSurveyProjectAnswers();
    }
    static void GetAllSurveyProjectAnswers()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfListOfProjectSurveyData result = null;
        try
        {
            result = report.GetAllSurveyProjectAnswer();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            dbug_log.ExceptionLog(new Exception(result.Messages[0]));
            return;
        }
        listCustomerSurveyAnswers = new List<SurveyProjectAnswer>();
        listSupplierSurveyAnswers = new List<SurveyProjectAnswer>();
        foreach(ClipCallReport.ProjectSurveyData psd in result.Value)
        {
            switch(psd.UserMode)
            {
                case(ClipCallReport.eUserMode.Customer):
                    listCustomerSurveyAnswers.Add(new SurveyProjectAnswer() { Answer = psd.Survey, SurveyProjectId = psd.SurveyProjectId });
                    break;
                case(ClipCallReport.eUserMode.Supplier):
                    listSupplierSurveyAnswers.Add(new SurveyProjectAnswer() { Answer = psd.Survey, SurveyProjectId = psd.SurveyProjectId });
                    break;
            }
            
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            LoadStatus();
            LoadUserMode();
            Guid incidentAccountId = Guid.Empty, supplierId = Guid.Empty;
            if (!Guid.TryParse(Request.QueryString["sid"], out supplierId))
            {
                if (!Guid.TryParse(Request.QueryString["ia"], out incidentAccountId))
                    incidentAccountId = Guid.Empty;
            }
            ExecReport(incidentAccountId: incidentAccountId, supplierId: supplierId);
        }
        LoadAnswers();
        SetToolboxEvents();
        Header.DataBind();
    }

   
    private void LoadAnswers()
    {
        string _script = "SetMessages(" + JsonConvert.SerializeObject(listCustomerSurveyAnswers) + "," + JsonConvert.SerializeObject(listSupplierSurveyAnswers) + ");";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetMessages", _script, true);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(REPORT_NAME);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    private void LoadUserMode()
    {
        ddl_UserMode.Items.Clear();
        foreach (ClipCallReport.eUserMode userMode in Enum.GetValues(typeof(ClipCallReport.eUserMode)))
        {
            string _userMode = userMode.ToString();
            ListItem li = new ListItem(_userMode, _userMode);
            li.Selected = userMode == ClipCallReport.eUserMode.All;
            ddl_UserMode.Items.Add(li);
        }
    }

    private void LoadStatus()
    {
        ddl_status.Items.Clear();
        foreach(ClipCallReport.eSurveyStatus surveyStatus in Enum.GetValues(typeof(ClipCallReport.eSurveyStatus)))
        {
            string _status = surveyStatus.ToString();
            ListItem li = new ListItem(_status, _status);
            li.Selected = surveyStatus == ClipCallReport.eSurveyStatus.Open;
            ddl_status.Items.Add(li);
        }
    }
    private void ExecReport(Guid incidentAccountId = default(Guid), Guid supplierId = default(Guid))
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.GetSurveyReportRequest request = new ClipCallReport.GetSurveyReportRequest();

        request.ProjectId = string.IsNullOrEmpty(txt_ProjectId.Text.Trim()) ? null : txt_ProjectId.Text.Trim();
        ClipCallReport.eUserMode userMode;
        if (!Enum.TryParse<ClipCallReport.eUserMode>(ddl_UserMode.SelectedValue, out userMode))
            userMode = ClipCallReport.eUserMode.All;
        request.UserMode = userMode;

        ClipCallReport.eSurveyStatus surveyStatus;
        if (!Enum.TryParse<ClipCallReport.eSurveyStatus>(ddl_status.SelectedValue, out surveyStatus))
            surveyStatus = ClipCallReport.eSurveyStatus.All;
        request.SurveyStatus = surveyStatus;

        Guid surveyAnswerId;
        if (!Guid.TryParse(hf_UserModeSelected.Value, out surveyAnswerId))
            surveyAnswerId = Guid.Empty;
        request.SurveyProjectId = surveyAnswerId;

        request.IncludeQa = cb_IncludeQaTest.Checked;
        request.IncidentAccountId = incidentAccountId;
        request.SupplierId = supplierId;

        ClipCallReport.ResultOfListOfGetSurveyReportResponse response = null;

        try
        {
            response = report.GetSurveyReport(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (response.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }

        DataTable dt = new DataTable();
        dt.Columns.Add("CreatedOn");
        dt.Columns.Add("ProjectId");
        dt.Columns.Add("UserMode");
      //  dt.Columns.Add("Status");
        dt.Columns.Add("StatusReason");
        dt.Columns.Add("RespondedDate");
        dt.Columns.Add("UserName");
        dt.Columns.Add("UserPhone");

        dt.Columns.Add("RespondedQuestion");
        dt.Columns.Add("SupplierName");
        dt.Columns.Add("SupplierPhone");
        dt.Columns.Add("Trace");
        dt.Columns.Add("DisplayCancel", typeof(bool));
        dt.Columns.Add("Cancel");
        dt.Columns.Add("SurveyType");
        dt.Columns.Add("TAD");

        foreach(ClipCallReport.GetSurveyReportResponse data in response.Value)
        {
            DataRow row = dt.NewRow();
            row["CreatedOn"] = string.Format(siteSetting.DateTimeFormat, data.CreatedOn);
            row["ProjectId"] = data.ProjectId;
            row["UserMode"] = data.IsCustomer ? CUSTOMER : SUPPLIER;
            row["UserName"] = data.UserName;
            row["UserPhone"] = data.UserPhone;
            row["TAD"] = data.TadQuestion;
         //   row["Status"] = data.Status;
            row["StatusReason"] = data.StatusReason;
            row["RespondedDate"] = data.RespondedDate == DateTime.MinValue ? null : string.Format(siteSetting.DateTimeFormat, data.RespondedDate);
            row["RespondedQuestion"] = data.QuestionText;
            row["SupplierName"] = data.SupplierName;
            row["SupplierPhone"] = data.SupplierPhone;
            row["Trace"] = "javascript:OpenIframe('ClipCallLeadConnections.aspx?incidentid=" + data.IncidentId.ToString() + "');";
            row["DisplayCancel"] = data.Status == "Active";
            row["Cancel"] = Newtonsoft.Json.JsonConvert.SerializeObject(new CancelArgument()
            {
                IsCustomer = data.IsCustomer,
                SurveyId = data.SurveyId
            });
            row["SurveyType"] = data.SurveyType == ClipCallReport.eSurveyType.GeneralDescription ? TOD : SURVEY;
            dt.Rows.Add(row);
        }


        lbl_RecordMached.Text = response.Value.Length.ToString();
        _GridView.DataSource = dt;
        _GridView.DataBind();
        dataV = dt;
        
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }
    protected void lb_Cancel_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        CancelArgument arguments = Newtonsoft.Json.JsonConvert.DeserializeObject<CancelArgument>(lb.CommandArgument);
        ClipCallReport.Result result = null;
        try
        {
            result = arguments.IsCustomer ? report.UncheckCustomerSurvey(arguments.SurveyId) : report.UncheckSupplierSurvey(arguments.SurveyId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        //lb.Visible = false;
        ExecReport();
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
    private class CancelArgument
    {
        public Guid SurveyId { get; set; }
        public bool IsCustomer { get; set; }
    }
}