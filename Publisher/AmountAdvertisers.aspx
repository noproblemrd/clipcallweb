<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AmountAdvertisers.aspx.cs" Inherits="Publisher_AmountAdvertisers" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../AutoCompleteCity/StyleAutoComplete.css" rel="stylesheet" type="text/css"/>
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />

<link href="style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">

    window.onload =  appl_init;
    
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }   

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">    
</cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  
 
<div class="page-content minisite-content2">
   
	<div id="form-analytics">	
		<div class="form-field">
            <asp:Label ID="lbl_expertise" CssClass="label" runat="server" Text="Heading"></asp:Label>
            <asp:DropDownList ID="ddl_Heading" CssClass="form-select" runat="server">
               </asp:DropDownList>
        </div>
        <div class="clear"></div>
	    <div class="heading"><asp:Button ID="btnSubmit" CssClass="CreateReportSubmit2" runat="server" Text="Run report" OnClick="btnSubmit_Click" /></div>
    </div>
    <div class="clear"></div>
    
    <div id="list" class="table">
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
            
            <div class="results3"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
            
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" CssClass="data-table" AllowPaging="True" OnPageIndexChanging="_GridView_PageIndexChanging" PageSize="20" >
                <PagerStyle CssClass="pager" HorizontalAlign="Center" />
                    <Columns>                    
                        <asp:TemplateField SortExpression="ExpertiseName">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_ExName.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text="<%# Bind('ExpertiseName') %>"></asp:Label>
                            <asp:Label ID="lbl_IsPrimary" runat="server" Text="<%# Bind('IsPrimary') %>" Visible="false"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="Inactive">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Inactive.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text="<%# Bind('Inactive') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="Candidate">
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_Candidate.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text="<%# Bind('Candidate') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="ApprovedAndAvailable">
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_Available.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text="<%# Bind('ApprovedAndAvailable') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="ApprovedNotAvailable">
                        <HeaderTemplate>
                            <asp:Label ID="Label17" runat="server" Text="<%# lbl_NotAvailable.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label18" runat="server" Text="<%# Bind('ApprovedNotAvailable') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="Total">
                        <HeaderTemplate>
                            <asp:Label ID="Label9" runat="server" Text="<%# lbl_total.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label10" runat="server" Text="<%# Bind('Total') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
            </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
	</div>	
</div>

<asp:Label ID="lbl_ExName" runat="server" Text="Heading" Visible="false"></asp:Label>
<asp:Label ID="lbl_Inactive" runat="server" Text="Inactive" Visible="false"></asp:Label>
<asp:Label ID="lbl_Candidate" runat="server" Text="Candidate" Visible="false"></asp:Label>
<asp:Label ID="lbl_Available" runat="server" Text="Available" Visible="false"></asp:Label>
<asp:Label ID="lbl_total" runat="server" Text="Total" Visible="false"></asp:Label>
<asp:Label ID="lbl_NotAvailable" runat="server" Text="Not available" Visible="false"></asp:Label>


<asp:Label ID="lblTitleAmountAdvertisers" runat="server" Text="Adv. per Status and Heading" Visible="false"></asp:Label>


</asp:Content>


