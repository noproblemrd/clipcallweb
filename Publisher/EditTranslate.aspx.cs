using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Data.OleDb;

public partial class Publisher_EditTranslate : PageSetting, IPostBackEventHandler
{
    const string PAGE_TYPE = "ENUM";
    const string SEARCH_TRANSLTE = "SERACH_TRANSLTE";
    const string EXCEL_NAME = "Translate";
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(lbPages);
        ScriptManager1.RegisterAsyncPostBackControl(ddlSite_lang);
        ScriptManager1.RegisterAsyncPostBackControl(btn_clear);
        ScriptManager1.RegisterAsyncPostBackControl(btn_virtual_search);
  //      ScriptManager1.RegisterAsyncPostBackControl(rbl_Type);
        if (!IsPostBack)
        {
            SetToolbox();
            GetSites_lang();
            GetLangDefault();
            GetPages();
            SetDefaultView();
            SetDataList(0);
            string form_id = Master.GetFormClientId;
            ClientScript.RegisterStartupScript(this.GetType(), "resetPage", "document.getElementById('" + form_id + "').reset();", true);
        }
        else
        {
            if (Request["__EVENTARGUMENT"] == SEARCH_TRANSLTE)
                _Search();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popupLoad", "a_show();", true);
        }
        SetToolBoxEvents();
        
    }

    private void SetDefaultView()
    {
        for (int i = 0; i < rbl_Type.Items.Count; i++)
        {
            if (rbl_Type.Items[i].Value == "Pages")
            {
                rbl_Type.SelectedIndex = i;
                break; ;
            }
        }
        _UpdatePanelType.Update();
    }

    private void SetToolBoxEvents()
    {
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.AttachExec += new EventHandler(_Toolbox_AttachExec);
    }

    void _Toolbox_AttachExec(object sender, EventArgs e)
    {
        string localPath = _Toolbox.GetUrlFile();
        string extension = Path.GetExtension(localPath);
        int siteLangId = int.Parse(ddlSite_lang.SelectedValue);
        SortedDictionary<int, string> dicError = new SortedDictionary<int, string>();
        int index = 0;
        string command = "dbo.Save_Translate_Excel";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            string sConnectionString = string.Empty;

            if (extension == ".xlsx")
                sConnectionString =
                "Provider=Microsoft.ACE.OLEDB.12.0;Password=\"\";User ID=Admin;Data Source=" + localPath + ";Mode=Share Deny Write;Extended Properties=\"HDR=YES;\";Jet OLEDB:Engine Type=37";
            else if (extension == ".xls")
                sConnectionString =
                    "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + localPath + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=YES;""";
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "incorrect_file", "alert('" + lbl_incorrectFile.Text + "');", true);
                return;
            }
            
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            try
            {
                objConn.Open();
                DataTable MySchemaTable = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                string sheetName = MySchemaTable.Rows[0].Field<string>("TABLE_NAME");
                if (!sheetName.Contains("$"))
                    sheetName += "$";
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "]", objConn);

                OleDbDataReader reader = objCmdSelect.ExecuteReader();
                if (!MuchCulomsNames(reader, "Id", "Sentence", "Translate"))
                {
                    lbl_report.Text = lbl_incorrectFields.Text;
                    _UpdatePanelReport.Update();
                    div_report.Attributes["class"] = "popModal_del";
                    _mpe.Show();
                    return;
                }

                conn.Open();

                while (reader.Read())
                {
                    string error = string.Empty;
                    //           WebReferenceSite.PrimaryExpertiseData ped = new WebReferenceSite.PrimaryExpertiseData();

                    bool IfIgnor = true;
                    object o = reader["Id"];
                    int _id = 0;
                    string _sentence, _translate;

                    if (o.ToString().Length != 0)
                    {
                        if (!int.TryParse(o.ToString(), out _id))
                            error += "Id;";
                        IfIgnor = false;
                    }
                    else
                        error += "Id;";

                    o = reader["Sentence"];
                    _sentence = o.ToString();
                    if (o.ToString().Length != 0)
                        IfIgnor = false;

                    o = reader["Translate"];
                    _translate = o.ToString().Trim();

                    if (IfIgnor)
                        continue;
                    index++;
                    if (error != string.Empty)
                    {
                        error = error.Substring(0, error.Length - 1);
                        dicError.Add(index, error);
                        continue;
                    }
                    SqlCommand cmd = new SqlCommand(command, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter RetVal = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    RetVal.Direction = ParameterDirection.ReturnValue;

                    SqlParameter p_id = new SqlParameter("@LiteralId", _id);
                    p_id.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(p_id);

                    SqlParameter p_sentence = new SqlParameter("@Sentence", _sentence);
                    p_sentence.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(p_sentence);

                    SqlParameter p_lang = new SqlParameter("@SiteLangId", siteLangId);
                    p_lang.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(p_lang);

                    SqlParameter p_Translate;
                    if (string.IsNullOrEmpty(_translate))
                        p_Translate = new SqlParameter("@Translate", DBNull.Value);
                    else
                        p_Translate = new SqlParameter("@Translate", _translate);
                    p_Translate.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(p_Translate);

                    int a = cmd.ExecuteNonQuery();
                    cmd.Dispose();

                    int result = (int)RetVal.Value;

                    if (result == -1)
                        dicError.Add(index, lbl_IdNotExsits.Text);
                    else if (result == -2)
                        dicError.Add(index, lbl_SentenceNotMatch.Text);

                }
                conn.Close();
                /*
                OleDbDataAdapter adapter = new OleDbDataAdapter(objCmdSelect);
                DataTable data = new DataTable();
                adapter.Fill(data);
                 * */
                objConn.Close();


            }
            catch (Exception ex)
            {
                //debug
                dbug_log.ExceptionLog(ex, siteSetting);
                //       ScriptManager.RegisterStartupScript(this, this.GetType(), "debug22", "alert('"+ex.Message+"');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
            finally
            {
                if (objConn.State != ConnectionState.Closed)
                    objConn.Close();
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }
        }
        if (dicError.Count != 0)
        {
            ShowErrorMessage(dicError, true, index - dicError.Count);            
            return;
        }
        btn_saveLog.Visible = false;
        string message = lbl_SavedSuccessfully.Text + "!";
        message += @"<br/>" + (index - dicError.Count) + " " + lbl_SavedOk.Text + "!";
        lbl_report.Text = message;
        _UpdatePanelReport.Update();
        div_report.Attributes["class"] = "popModal_del";
        _mpe.Show();
    }
    void ShowErrorMessage(SortedDictionary<int, string> dic, bool IsSaved, int SavedOk)
    {
        StringBuilder sb = new StringBuilder();
        if (IsSaved)
        {
            sb.Append(SavedOk + @" " + lbl_SavedOk.Text + @"!<br/>");
            sb.Append(dic.Count + @" " + lbl_dontSaved.Text + ":");
        }
        else
            sb.Append(lbl_problem.Text + " " + dic.Count + " " + lbl_DocumentNotSaved.Text + "!");
        sb.Append(@"<br/><br/>");
        foreach (KeyValuePair<int, string> kvp in dic)
        {
            string[] errors = kvp.Value.Split(';');
            sb.Append(lbl_errorLines.Text + " " + kvp.Key + ", " + lbl_Fields.Text + ": ");
            for (int i = 0; i < errors.Length; i++)
            {
                string s = errors[i] + ", ";
                if (i == errors.Length - 1)
                    s = s.Substring(0, s.Length - 2);
                sb.Append(s);
            }
            sb.Append(@"<br/>");
        }
        lbl_report.Text = sb.ToString();
        ErrorMessageV = sb.ToString().Replace(@"<br/>", "\r\n");
        _UpdatePanelReport.Update();
        btn_saveLog.Visible = true;
        //string script = "ShowReport();";
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowReport", script, true);
        div_report.Attributes["class"] = "popModal_del";
        _mpe.Show();

    }
    bool MuchCulomsNames(OleDbDataReader reader, params string[] names)
    {
        foreach (string s in names)
        {
            bool _find = false;
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (s.ToLower() == reader.GetName(i).ToLower())
                {
                    _find = true;
                    break;
                }
            }
            if (!_find)
                return false;
        }
        return true;
    }
    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        DataTable data = GetSearchResult(string.Empty);
        data.Columns.Remove("PageId");
        ToExcel to_excel = new ToExcel(this, "Translations");
        if (!to_excel.ExecExcel(data))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    private void SetToolbox()
    {
        _Toolbox.RemoveAdd();
        _Toolbox.RemoveDelete();
        _Toolbox.RemoveEdit();
        _Toolbox.RemovePrint();
        _Toolbox.RemoveSearch();
    }

    private void GetLangDefault()
    {
        int langId = -1;
        string command = "SELECT dbo.GetSiteLangIdDefaultBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            langId = (int)cmd.ExecuteScalar();
            conn.Close();
        }
        foreach (ListItem li in ddlSite_lang.Items)
        {
            if (langId.ToString() == li.Value)
                li.Selected = true;
            else
                li.Selected = false;
        }
    }
   
    private void SetDataList(int index)
    {
        int PageId = int.Parse(lbPages.Items[index].Value);
        int siteLangId = int.Parse(ddlSite_lang.SelectedValue);
  //      Dictionary<int, string> dic = new Dictionary<int, string>();
        DataTable data = new DataTable();
        data.Columns.Add("Id", typeof(int));
        data.Columns.Add("Sentence");
        data.Columns.Add("Translate");
        data.Columns.Add("PageId", typeof(int));
        data.Columns.Add("ShowPath", typeof(bool));
        data.Columns.Add("Path", typeof(string));
      
        string command = "EXEC dbo.GetSentencesByPageIdSiteLangId @PageId, " +
            "@SiteLangId, @LiteralType";

        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PageId", PageId);
            cmd.Parameters.AddWithValue("@SiteLangId", siteLangId);
            cmd.Parameters.AddWithValue("@LiteralType", LiteralType.Regular.ToString());

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow row = data.NewRow();
                string _sentence = (string)reader["Sentence"];
                int _id = (int)reader["Id"];
                row["Id"] = _id;
                row["Sentence"] = _sentence;
                string _translate = (reader.IsDBNull(2)) ? string.Empty : (string)reader["Translate"];
                row["Translate"] = _translate;
                row["PageId"] = PageId;
                row["ShowPath"] = false;
                //        dic.Add(_id, _translate);
                data.Rows.Add(row);
            }
            conn.Close();
        }
        DataListSentences.DataSource = data;
        DataListSentences.DataBind();
     

        string picPath = @"PagePic/" + PicListV[PageId];
        if (picPath != @"PagePic/")
        {
            _TitlePopup.Text = lbPages.SelectedItem.Text;
            ImagePopup.ImageUrl = picPath;
            ImagePopup.Visible = true;
            ImgSmallPic.ImageUrl = picPath;
            span_showPic.Visible = true;
        }
        else
        {
            ImagePopup.Visible = false;
            ImgSmallPic.ImageUrl = null;
            span_showPic.Visible = false;
        }
        UpdatePanelSentences.Update();
        _UpdatePanelPopUp.Update();
        _UpdatePanelSmallPic.Update();
    }
   
    protected void btn_Search_Click(object sender, EventArgs e)
    {
        _Search();
    }
     
    void _Search()
    {
        
        string _sen = txt_Search.Text.Trim();

        if (_sen.Length < 2)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AtLeast", "alert('" + GetAtLeastOneCharacter + "');", true);
            return;
        }


        DataTable data = GetSearchResult(_sen);
        DataListSentences.DataSource = data;
        DataListSentences.DataBind();
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
            btn_set.Enabled = false;
        }
        else
            btn_set.Enabled = true;

        UpdatePanelSentences.Update();
    }
    DataTable GetSearchResult(string _sen)
    {
        int siteLangId = int.Parse(ddlSite_lang.SelectedValue);
        DataTable data = new DataTable();
        data.Columns.Add("Id", typeof(int));
        data.Columns.Add("Sentence");
        data.Columns.Add("Translate");
        data.Columns.Add("PageId", typeof(int));
        data.Columns.Add("ShowPath", typeof(bool));
        data.Columns.Add("Path", typeof(string));

        string command = "EXEC dbo.GetSentensesByString @Sentence, @SiteLangId, @LiteralType";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@Sentence", _sen);
            cmd.Parameters.AddWithValue("@SiteLangId", siteLangId);
            cmd.Parameters.AddWithValue("@LiteralType", LiteralType.Regular.ToString());

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow row = data.NewRow();
                string _sentence = (string)reader["Sentence"];
                int _id = (int)reader["Id"];
                row["Id"] = _id;
                row["Sentence"] = _sentence;
                string _translate = (reader.IsDBNull(2)) ? string.Empty : (string)reader["Translate"];
                row["Translate"] = _translate;
                row["PageId"] = (int)reader["PageId"];
                row["ShowPath"] = true;
                row["Path"] = (string)reader["description"];
                data.Rows.Add(row);
            }
            conn.Close();
        }
        return data;
    }

    private void GetPages()
    {
        Dictionary<int, string> dic=new Dictionary<int,string>();
        Dictionary<int, string> dicType = new Dictionary<int, string>();
        string command = "EXEC dbo.GetPagesOrderByDesc";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                int _id = (int)reader["Id"];
                string pagedesc = (reader.IsDBNull(2)) ? (string)reader["PageName"] : (string)reader["description"];
                lbPages.Items.Add(new ListItem(pagedesc, "" + _id));
                dic.Add(_id, reader.IsDBNull(3) ? string.Empty :
                    (string)reader["PicPageUrl"]);
                dicType.Add(_id, (string)reader["PageType"]);
            }

            conn.Close();
        }
        lbPages.SelectedIndex = 0;
        PicListV = dic;
        PageTypeListV = dicType;
    }
    protected void GetSites_lang()
    {
        
        string command = "EXEC dbo.GetLanguagesBySiteNameId @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ddlSite_lang.Items.Add(new ListItem((string)reader["LanguageName"], "" + (int)reader["SiteLangId"]));
            }
            conn.Close();
        }
    }
   
    protected void lbPages_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDataList(lbPages.SelectedIndex);
    }
    protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
    {
        lbPages.SelectedIndex = 0;
        if (rbl_Type.SelectedValue == "Pages")
            SetDataList(lbPages.SelectedIndex);
        else
            _Search();
        UpdatePanelListPages.Update();
    }
    protected void btn_set_Click(object sender, EventArgs e)
    {

        List<string> _list = new List<string>();
        int SiteLangId = int.Parse(ddlSite_lang.SelectedValue);
        string command = "EXEC dbo.Save_Translate_Similare @LiteralId, " +
                    "@SiteLangId, @Translate, @ChangeAll";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                for (int i = DataListSentences.Items.Count - 1; i >= 0; i--)
                {
                    string original = ((Label)DataListSentences.Items[i].FindControl("lbl_eng")).Text.ToLower();
                    SqlCommand cmd = new SqlCommand(command, conn);
                    cmd.Parameters.AddWithValue("@LiteralId", (int)DataListSentences.DataKeys[i]);
                    cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
                    bool ChangeAll = ((CheckBox)(DataListSentences.Items[i].FindControl("cb_UpdateAll"))).Checked;
                    cmd.Parameters.AddWithValue("@ChangeAll", ChangeAll);
                    TextBox tb = (TextBox)(DataListSentences.Items[i].FindControl("tb_translate"));
                    string sentence = tb.Text.Trim();
                    if (sentence == string.Empty)
                    {
                        if (_list.Contains(original))
                            continue;
                        cmd.Parameters.AddWithValue("@Translate", DBNull.Value);
                    }
                    else
                    {
                        if (!_list.Contains(original))
                            _list.Add(original);
                        cmd.Parameters.AddWithValue("@Translate", sentence);
                    }
                    int a = cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        if (rbl_Type.SelectedValue == "Pages")
            SetDataList(lbPages.SelectedIndex);
        else
            _Search();
    }
    protected void rbl_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbl_Type.SelectedValue == "Pages")
        {
            panel_Pages.Visible = true;
            panel_sentence.Visible = false;
            btn_set.Enabled = true;
            span_showPic.Visible = true;
            //SetDataList(int.Parse(lbPages.SelectedValue));
            SetDataList(lbPages.SelectedIndex);
        }
        else
        {
    //        PageIndex = int.Parse(lbPages.SelectedValue);
            panel_Pages.Visible = false;
            panel_sentence.Visible = true;
            txt_Search.Text = string.Empty;
            btn_set.Enabled = false;
            span_showPic.Visible = false;
            DataListSentences.DataSource = null;
            DataListSentences.DataBind();
        }
        UpdatePanelListPages.Update();
        UpdatePanelSentences.Update();
        _UpdatePanelSmallPic.Update();
    }
    /* Not in used */
    protected void btn_clear_Click(object sender, EventArgs e)
    {
        foreach (DataListItem item in DataListSentences.Items)
        {
            ((TextBox)item.FindControl("tb_translate")).Text = string.Empty;
        }
        UpdatePanelSentences.Update();
    }

    protected void _btn_closed_click(object sender, EventArgs e)
    {
        string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int rnd = new Random().Next();
        path += rnd + EXCEL_NAME + ".txt";
        using (System.IO.StreamWriter _file = new System.IO.StreamWriter(path))
        {
            _file.Write(ErrorMessageV);
        }
        Response.ContentType = "test/plain";
        Response.AppendHeader("Content-disposition", "attachment; filename=Error.txt");
        Response.WriteFile(path);
        Response.End();

    }
    Dictionary<int, string> PicListV
    {
        get
        {
            return (ViewState["PicList"] == null) ? new Dictionary<int, string>() :
            (Dictionary<int, string>)ViewState["PicList"];
        }
        set { ViewState["PicList"] = value; }
    }
    Dictionary<int, string> PageTypeListV
    {
        get
        {
            return (ViewState["PageTypeList"] == null) ? new Dictionary<int, string>() :
            (Dictionary<int, string>)ViewState["PageTypeList"];
        }
        set { ViewState["PageTypeList"] = value; }
    }
    /* If I want to detect if change
    Dictionary<int, string> PageSentenceV
    {
        get
        {
            return (ViewState["PageSentence"] == null) ? new Dictionary<int, string>() :
            (Dictionary<int, string>)ViewState["PageSentence"];
        }
        set { ViewState["PageSentence"] = value; }
    }
     * */
    protected string GetAtLeastOneCharacter
    {
        get { return HttpUtility.HtmlEncode(lbl_OneCharacter.Text); }
    }

    string ErrorMessageV
    {
        get { return (ViewState["ErrorMessage"] == null) ? string.Empty : (string)ViewState["ErrorMessage"]; }
        set { ViewState["ErrorMessage"] = value; }
    }


    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        throw new NotImplementedException();
    }
    protected override void Render(HtmlTextWriter writer)
    {

        Page.ClientScript.RegisterForEventValidation(btn_virtual_search.UniqueID, SEARCH_TRANSLTE);
        base.Render(writer);
    }
    protected string SearchTranslate()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(btn_virtual_search, SEARCH_TRANSLTE);
        myPostBackOptions.PerformValidation = false;
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
    }
    #endregion
    /*
    int PageIndex
    {
        get
        {
            if (ViewState["PageIndex"] == null)
            {
                lbPages.SelectedIndex = 0;
                return int.Parse(lbPages.Items[0].Value);
            }
            else
                return (int)ViewState["PageIndex"];
        }
        set { ViewState["PageIndex"] = value; }
    }
     * */
}
