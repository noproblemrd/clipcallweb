﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="Complaints.aspx.cs" Inherits="Publisher_Complaints" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/Toolbox/ToolboxWizardReport.ascx" tagname="Toolbox" tagprefix="uc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

<!--
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" type="text/css" />
-->
<script src="../jquery.min.js" type="text/javascript"></script> 
<script src="../jquery-ui.min.js" type="text/javascript"></script> 
<style type="text/css">
#ui-datepicker-div{ display: none; }


</style>


<script type="text/javascript" >

$(function() {
     $("._Advertiser").autocomplete({
        source: function(request, response) {
         //   alert(request.term);
            $.ajax({
                url: "ReportWizardService.asmx/SuggestList",
                data: "{ 'str': '" + request.term + "', "+
                      " 'classList': 'Advertiser'   }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function(data) { return data; },
                success: function(data) {
                    response($.map(data.d, function(item) {
                        return {
                            value: item
                        }
                    }))
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        },
        minLength: 1
    });
});
$(function() {

	$('._date_picker').datepicker({
       onClose: function(dateText, inst) { OnBlurDatepicker(this); },
       dateFormat: 'mm/dd/yy'});
});
    function OnBlurDatepicker(elem)
    {       
        if(!CheckDateFormat(elem))
        {
            alert("<%# GetWrongFormatDate %>");            
            elem.focus();
            elem.select();            
        }        
    }
    function CheckDateFormat(elem)
    {
        var _format = "<%# siteSetting.DateFormatClean %>";
        if(!validate_Date(elem.value, _format))                    
            return false;        
        return true;
    }
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
       
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }   
    function CloseIframe_after()
    {
        CloseIframe();
        <%# Get_run_report() %>;
    }
    function CloseIframe()
    {
       $find('_modal').hide();
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie popModal_del2_hide";
       document.getElementById("<%# _iframe.ClientID %>").src = "javascript:void(0);";
    }
    function OpenIframe(_path)
    {    
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie";
       document.getElementById("<%# _iframe.ClientID %>").src = _path;
//  document.getElementById("<%# _iframe.ClientID %>").src = "http://demos.telerik.com/aspnet-ajax/combobox/examples/default/defaultcs.aspx";
       HideIframeDiv();
  
       $find('_modal').show();
    }
    
    function ShowIframeDiv()
    {
        document.getElementById("divIframeLoader").style.display = "none";
       document.getElementById("div_iframe_main").style.display = "block";
    }
    function HideIframeDiv()
    {
        document.getElementById("divIframeLoader").style.display = "block";
       document.getElementById("div_iframe_main").style.display = "none";
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ></cc1:ToolkitScriptManager>
<uc1:Toolbox ID="_Toolbox" runat="server" />
<div class="page-content minisite-content2">
 
  <div id="form-analytics">
  
    
                    <div class="form-field">
        <asp:Label ID="lbl_ComplaintID" runat="server" class="label" Text="Complaint ID"></asp:Label>
        <asp:TextBox ID="txt_ComplaintID"  CssClass="form-text" runat="server"></asp:TextBox>
    </div>
                    <div class="form-field">
        <asp:Label ID="lblStatus" runat="server" class="label" Text="<%# lbl_Status.Text %>"></asp:Label>
        <asp:DropDownList ID="ddl_Status" CssClass="form-select" runat="server">
        </asp:DropDownList>
    </div>
           <div class="complaints"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>


    <div class="form-fieldcomplaintL">
                    <div class="form-field">
            <asp:Label ID="lbl_Advertisers" runat="server" class="label" Text="Advertiser"></asp:Label>
            <asp:TextBox ID="txt_Advertisers" runat="server" CssClass="_Advertiser form-text"></asp:TextBox>
        </div>
                    <div class="form-field">
            <asp:Label ID="lbl_FollowUpBefore" runat="server" class="label" Text="Follow up before"></asp:Label>
            <asp:TextBox ID="txt_FollowUpBefore" runat="server" CssClass="_date_picker form-text"></asp:TextBox>
        </div>
                    <div class="form-field">
                        <asp:Label ID="lbl_Owner" runat="server" class="label" Text="Owner"></asp:Label>
                        <asp:DropDownList ID="ddl_Owner" CssClass="form-select" runat="server">
                        </asp:DropDownList>
                    </div>

    
    </div>             
      

       </div>          

       <div class="table">
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <div class="results5"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
         <div class="div_gridMyCall">
             <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false"
                CssClass="data-table" AllowPaging="True" 
                onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />        
            <FooterStyle CssClass="footer"  />
            <PagerStyle CssClass="pager" />
            <Columns>
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label01" runat="server" Text="<%# lbl_All.Text %>"></asp:Label>
                    <br />
                    <asp:CheckBox ID="cb_all" runat="server" >
                    </asp:CheckBox>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cb_choose" runat="server" />                    
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField SortExpression="ID">              
                <HeaderTemplate>
                    <asp:Label ID="Label03" runat="server" Text="<%# lbl_ID.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    
                   <asp:LinkButton ID="lb_ID" runat="server" Text="<%# Bind('ID') %>" OnClientClick="<%# Bind('ScriptComplaint') %>"></asp:LinkButton>        
                    <asp:Label ID="lbl_guid" runat="server" Text="<%# Bind('guid') %>" Visible="false"></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField SortExpression="CreatedOn">              
                <HeaderTemplate>
                    <asp:Label ID="Label04" runat="server" Text="<%# lbl_CreatedOn.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>            
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField SortExpression="Title">              
                <HeaderTemplate>
                    <asp:Label ID="Label05" runat="server" Text="<%# lbl_Title.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text="<%# Bind('Title') %>"></asp:Label>            
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField SortExpression="Advertiser">              
                <HeaderTemplate>
                    <asp:Label ID="Label06" runat="server" Text="<%# lbl_Advertiser.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text="<%# Bind('Advertiser') %>"></asp:Label>            
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField SortExpression="Status">              
                <HeaderTemplate>
                    <asp:Label ID="Label07" runat="server" Text="<%# lbl_Status.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text="<%# Bind('Status') %>"></asp:Label>            
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField SortExpression="FollowUp">             
                <HeaderTemplate>
                    <asp:Label ID="Label08" runat="server" Text="<%# lbl_FollowUp.Text %>"></asp:Label>                                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text="<%# Bind('FollowUp') %>"></asp:Label>            
                </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
            </asp:GridView>
          </div>  
        </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    
</div>

 

<asp:Panel ID="panel_iframe" runat="server" CssClass="popModal_del2_ie popModal_del2_hide" style="display:none;"
 Width="750" Height="440">
 <div id="divIframeLoader" class="divIframeLoader">   
    <table>
        <tr>
            <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
        </tr>
    </table>
</div>
        

<div id="div_iframe_main" >
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A" href="javascript:void(0);" onclick="javascript:CloseIframe();"></a>
    <iframe runat="server" id="_iframe" width="750px" height="440px" frameborder="0" scrolling="no"  ALLOWTRANSPARENCY="true"></iframe>
    <div class="bottom2"></div>
</div>
</asp:Panel>

<cc1:ModalPopupExtender ID="_mpe" runat="server"
            TargetControlID="btn_virtual"
            PopupControlID="panel_iframe"
             BackgroundCssClass="modalBackground2" 
             
              BehaviorID="_modal"               
              DropShadow="false">
</cc1:ModalPopupExtender>
<div style="display:none;">
   <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />       
</div>
 
<asp:Label ID="lbl_All" runat="server" Text="All" Visible="false"></asp:Label>
<asp:Label ID="lbl_ID" runat="server" Text="ID" Visible="false"></asp:Label>
<asp:Label ID="lbl_CreatedOn" runat="server" Text="Created on" Visible="false"></asp:Label>
<asp:Label ID="lbl_Title" runat="server" Text="Title" Visible="false"></asp:Label>
<asp:Label ID="lbl_Status" runat="server" Text="Status" Visible="false"></asp:Label>
<asp:Label ID="lbl_Advertiser" runat="server" Text="Advertiser" Visible="false"></asp:Label>
<asp:Label ID="lbl_FollowUp" runat="server" Text="Follow up" Visible="false"></asp:Label>
</asp:Content>


