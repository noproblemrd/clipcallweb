﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_AarCallCenter : PageSetting
{
    const string Num_Format = "{0:#,0.##}";
    const int ONE_DAY = 1;
    protected readonly string SessionTableName = "dataACR";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
            LoadSteps();
        }
        SetToolboxEvents();
        Header.DataBind();
    }

    private void LoadSteps()
    {
        ddl_AarStpe.Items.Clear();
        foreach (WebReferenceReports.eAarReportFilter filter in Enum.GetValues(typeof(WebReferenceReports.eAarReportFilter)))
        {
            ListItem li = new ListItem(filter.ToString(), filter.ToString());
            li.Selected = filter == WebReferenceReports.eAarReportFilter.ALL;
            li.Enabled = filter != WebReferenceReports.eAarReportFilter.P3;
            ddl_AarStpe.Items.Add(li);
        }
    }
    private void setDateTime()
    {
             _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
                  ONE_DAY);
      //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_AarCallCenter.Text);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.AarCallCenterRequest _request = new WebReferenceReports.AarCallCenterRequest();
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo.AddDays(1);
        WebReferenceReports.eAarReportFilter filter;
        if (!Enum.TryParse(ddl_AarStpe.SelectedValue, out filter))
            filter = WebReferenceReports.eAarReportFilter.ALL;
        _request.Filter = filter;
        WebReferenceReports.ResultOfListOfAarCallCenterResponse result = null;
        try
        {
            result = reports.AarCallCenterReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        /*
         * CreatedOn datetime,
	Category nvarchar(150),
	VideoLink nvarchar(250),
	VideoDuration int,
	Region nvarchar(200),
	Name nvarchar(250),
	Phone nvarchar(25),
	[Priority]*/
        WebReferenceReports.AarCallCenterResponse[] resultRows;
        if (userManagement.GetEnumSecurityLevel == SecurityLevel.PUBLISHER1)
            resultRows = result.Value;
        else
        {
            var query = from X in result.Value
                        where !X.IsQaTest
                        select X;
            resultRows = query.ToArray();
        }

        DataTable data = new DataTable();
        data.Columns.Add("Date");
        data.Columns.Add("CaseNumber");
        data.Columns.Add("CaseScript");
        data.Columns.Add("Category");
        data.Columns.Add("VideoLink");
        data.Columns.Add("Address");
        data.Columns.Add("Name");
        data.Columns.Add("Phone");
        data.Columns.Add("Priority");
        data.Columns.Add("WatchedVideo");

        data.Columns.Add("YelpUrl");
        data.Columns.Add("HasYelpUrl", typeof(bool));
        data.Columns.Add("YelpRating");
        data.Columns.Add("GoogleUrl");
        data.Columns.Add("HasGoogleUrl", typeof(bool));
        data.Columns.Add("GoogleRating");
        foreach (WebReferenceReports.AarCallCenterResponse row in resultRows)
        {
            DataRow dr = data.NewRow();
            dr["Date"] = GetDateTimeStringFormat(row.CreatedOn);
            dr["CaseNumber"] = row.CaseNumber;
            dr["CaseScript"] = "javascript:OpenCaseTicket('MobileAppRequestTicket.aspx?incidentid=" + row.IncidentId.ToString() + "');";
            dr["Category"] = row.Category;
            GenerateVideoLink videoLinkQuery = new GenerateVideoLink(row.VideoLink, row.PicPreviewUrl);
           // string videoLinkQuery
            dr["VideoLink"] = videoLinkQuery.GenerateFullUrl();//ResolveUrl("~/player/GetVideo.ashx?" + videoLinkQuery.GenerateQueryString());
            dr["Address"] = row.Address;
            dr["Name"] = row.Name;
            dr["Phone"] = row.Phone;// row.Phone.Insert(6, "-").Insert(3, "-");
            dr["Priority"] = row.Priority;
            dr["WatchedVideo"] = row.WatchVideo;

            dr["YelpUrl"] = row.YelpUrl;
            dr["HasYelpUrl"] = !string.IsNullOrEmpty(row.YelpUrl);
            if (row.YelpRating.HasValue)
                dr["YelpRating"] = string.Format(this.GetNumberFormat, row.YelpRating.Value);
            dr["GoogleUrl"] = row.GoogleUrl;
            dr["HasGoogleUrl"] = !string.IsNullOrEmpty(row.GoogleUrl);
            if (row.GoogleRating.HasValue)
                dr["GoogleRating"] = string.Format(this.GetNumberFormat, row.GoogleRating.Value);
            data.Rows.Add(dr);
        }
        lbl_RecordMached.Text = result.Value.Length.ToString();
        _GridView.DataSource = data;
        _GridView.DataBind();
        dataV = data;

    }
    string GetDateTimeStringFormat(DateTime dt)
    {
        return string.Format(siteSetting.DateFormat, dt) + " " +
                    string.Format(siteSetting.TimeFormat, dt);
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
    

}