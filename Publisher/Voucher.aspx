﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="Voucher.aspx.cs" Inherits="Publisher_Voucher" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<script type="text/javascript" language="javascript">
    window.onload = appl_init;
    function appl_init() {
        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);   
        
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }   
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">          
</cc1:ToolkitScriptManager>       
 <uc1:ToolboxReport runat="server" ID="ToolboxReport1" /> 
 
<div class="page-content minisite-content2">
	<div id="form-analytics">
	    <div class="main-inputs">	
	    
	        <div>
                <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                
	                <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false"
                         AllowPaging="true" onpageindexchanging="gv_GroupByDates_PageIndexChanging" 
                            PageSize="20">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />                
                        <FooterStyle CssClass="footer"  />
                        <PagerStyle CssClass="pager" />
                        <Columns>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Number.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text="<%# Bind('Number') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_OriginalBalance.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text="<%# Bind('OriginalBalance') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_CurrentBalance.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text="<%# Bind('CurrentBalance') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label9" runat="server" Text="<%# lbl_AmountOfDeposits.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label10" runat="server" Text="<%# Bind('AmountOfDeposits') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        </Columns>
                    </asp:GridView>	   
                </ContentTemplate>
                </asp:UpdatePanel>     
	        </div>   
            
	    </div>
	</div>
</div>
    

<asp:Label ID="lbl_Date" runat="server" Text="Creation date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Number" runat="server" Text="Number" Visible="false"></asp:Label>
<asp:Label ID="lbl_OriginalBalance" runat="server" Text="Original balance" Visible="false"></asp:Label>
<asp:Label ID="lbl_CurrentBalance" runat="server" Text="Current balance" Visible="false"></asp:Label>
<asp:Label ID="lbl_AmountOfDeposits" runat="server" Text="Amount of deposits" Visible="false"></asp:Label>

<asp:Label ID="lbl_Vouchers" runat="server" Text="Vouchers" Visible="false"></asp:Label>

</asp:Content>




