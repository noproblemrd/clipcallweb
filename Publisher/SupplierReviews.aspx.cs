﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Publisher_SupplierReviews : PageSetting
{
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            LoadStatus();
        }
        else
        {
            Dictionary<int, string> dic = PageListV;
            foreach (KeyValuePair<int, string> kvp in dic)
                PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
        }
    }

    private void LoadStatus()
    {
        ddl_Status.Items.Add(new ListItem("--- " + lbl_All.Text + " ---", "0"));
        foreach (string rs in Enum.GetNames(typeof(WebReferenceReports.ReviewStatus)))
        {
            string translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "ReviewStatus", rs);
            ddl_Status.Items.Add(new ListItem(translate, rs));
        }
        ddl_Status.SelectedIndex = 0;
    }

    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        string _dver = txt_Advertiser.Text;
        AdvertisersService ads = new AdvertisersService();
        Guid _guid = ads.GetGuidAdvertiser(_dver);

        DateTime from_date = FromToDate1.GetDateFrom;
        DateTime to_date = FromToDate1.GetDateTo;
        if (from_date == DateTime.MinValue || to_date == DateTime.MinValue)
        {
            to_date = DateTime.Now;
            from_date = to_date.AddMonths(-1);
        }

        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.GetReviewsRequest _request = new WebReferenceReports.GetReviewsRequest();

        if (_guid == Guid.Empty)
            txt_Advertiser.Text = "";
        else
            _request.SupplierId = _guid;

        string _status = ddl_Status.SelectedValue;
        foreach (WebReferenceReports.ReviewStatus rs in Enum.GetValues(typeof(WebReferenceReports.ReviewStatus)))
        {
            if (rs.ToString() == _status)
            {
                _request.ReviewStatus = rs;
                break;
            }
        }
        _request.FromDate = from_date;
        _request.ToDate = to_date;
      
        WebReferenceReports.ResultOfGetReviewsResponse result = null;
        try
        {
            result = _report.GetReviews(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Faild", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Faild", "top.UpdateFailed();", true);
            return;
        }
        /*
        result.Value.Reviews[0].CreatedOn;
        result.Value.Reviews[0].Description;
        result.Value.Reviews[0].IncidentId;
        result.Value.Reviews[0].IncidentNumber;
        result.Value.Reviews[0].IsLike;
        result.Value.Reviews[0].Name;
        result.Value.Reviews[0].Number;
        result.Value.Reviews[0].Phone;
        result.Value.Reviews[0].ReviewId;
        result.Value.Reviews[0].ReviewStatus;
        result.Value.Reviews[0].SupplierId;
        result.Value.Reviews[0].SupplierName;
        */
   //     Dictionary<Guid, string> dic = new Dictionary<Guid, string>();
        DataTable data = new DataTable();
        data.Columns.Add("Number");
        data.Columns.Add("CreatedOn");
        data.Columns.Add("Phone");
        data.Columns.Add("Name");
        data.Columns.Add("Review");
        data.Columns.Add("IsLike");
        data.Columns.Add("Status");
        data.Columns.Add("StatusPic");
        data.Columns.Add("ReviewId");
        data.Columns.Add("IncidentId");
        data.Columns.Add("OnClientClick");
        data.Columns.Add("SupplierName");
        foreach (WebReferenceReports.ReviewData rd in result.Value.Reviews)
        {
            DataRow row = data.NewRow();
            row["Number"] = rd.Number;
            row["CreatedOn"] = rd.CreatedOn;
            row["Phone"] = rd.Phone;
            row["Name"] = rd.Name;
            row["SupplierName"] = rd.SupplierName;
            row["IncidentId"] = rd.IncidentId.ToString();
            row["OnClientClick"] = "javascript:openIframe('" + rd.CostumerId.ToString() + "');";
            string _review = rd.Description;
        //    string _preview = (_review.Length > 25) ? _review.Substring(0, 22) + "..." : _review;
            row["Review"] = _review;
            row["IsLike"] = (rd.IsLike) ? @"../images/icon-thumbs-up.png" : @"../images/icon-thumbs-down.png";
            Guid reviewId = rd.ReviewId;
            row["ReviewId"] = reviewId.ToString();
            string StatusPic = "";
            switch (rd.ReviewStatus)
            {
                case(WebReferenceReports.ReviewStatus.Approved):
                    StatusPic = "images/ramzor-green.png";
                    break;
                case (WebReferenceReports.ReviewStatus.Candidate):
                    StatusPic = "images/ramzor-orange.png";
                    break;
                case (WebReferenceReports.ReviewStatus.Inactive):
                    StatusPic = "images/ramzor-red.png";
                    break;
            }
            row["StatusPic"] = StatusPic;
            row["Status"] = rd.ReviewStatus.ToString();
            data.Rows.Add(row);
  //          dic.Add(reviewId, _review);
        }

        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + lbl_NoResult.Text + "');", true);
        }
        lbl_RecordMached.Text = data.Rows.Count + " " + lblRecordMached.Text;
      //  _reapeter.DataSource = data;
   ////     _reapeter.DataBind();        
   //     _uPanel.Update();
        BindData(data);
        dataV = data;
//        ReviewsV = dic;        
    }
    protected void BindData(DataTable data)
    {
        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;
        objPDS.PageSize = ITEM_PAGE;
        objPDS.DataSource = data.DefaultView;

        if (data.Rows.Count > 0)
        {

            objPDS.CurrentPageIndex = CurrentPage;
            lnkPreviousPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkNextPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            LoadPages(data.Rows.Count);
            _reapeter.DataSource = objPDS;
            _reapeter.DataBind();
            
            div_paging.Visible = true;
     //       PanelResults.Visible = true;
        }
        else
        {
            CleanPager();
        }
        _uPanel.Update();

    }
    void CleanPager()
    {
        PageListV = null;
        PlaceHolderPages.Controls.Clear();
        div_paging.Visible = false;
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        _uPanel.Update();
    }
    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        //if there is only one page
        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListV = null;
            return;
        }

        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListV = dic;
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
    }
    HtmlTableCell MakeCell(int indx, string _item)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();
        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }
    protected void ib_status_click(object sender, EventArgs e)
    {
        ImageButton ib = (ImageButton)sender;
        Guid ReviewId = new Guid((ib).CommandArgument);
        RepeaterItem row = (RepeaterItem)ib.NamingContainer;
        string _status = ((Label)row.FindControl("lbl_Status")).Text;
        WebReferenceSite.ReviewStatus NewStatus = WebReferenceSite.ReviewStatus.Inactive;
        string StatusPic = "";
        switch (_status)
        {
            case ("Approved"):
                NewStatus = WebReferenceSite.ReviewStatus.Inactive;
                StatusPic = "images/ramzor-red.png";
                break;
            case ("Candidate"):
                NewStatus = WebReferenceSite.ReviewStatus.Approved;
                StatusPic = "images/ramzor-green.png";
                break;
            case ("Inactive"):
                NewStatus = WebReferenceSite.ReviewStatus.Approved;
                StatusPic = "images/ramzor-green.png";
                break;
        }

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.UpdateReviewStatusRequest _request = new WebReferenceSite.UpdateReviewStatusRequest();
        _request.ReviewId = ReviewId;
        _request.ReviewStatus = NewStatus;
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.UpdateReviewStatus(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Faild", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Faild", "top.UpdateFailed();", true);
            return;
        }
        DataTable data = dataV;
        foreach (DataRow _row in data.Rows)
        {
            if (_row["ReviewId"].ToString() == ReviewId.ToString())
            {
                _row["StatusPic"] = StatusPic;
                _row["Status"] = NewStatus.ToString();
                break;
            }
        }

        _reapeter.DataSource = data;
        _reapeter.DataBind();
        _uPanel.Update();
        dataV = data;

    }
    protected void lnkNextPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;
   //     listTR = new List<string>();
        BindData(dataV);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;
    //    listTR = new List<string>();
        BindData(dataV);
    }
    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
  //      listTR = new List<string>();
        BindData(dataV);
    }
    protected void btn_consumer_click(object sender, EventArgs e)
    {
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    int CurrentPage
    {
        get
        {
            //Look for current page in ViewState 
            object o = this.ViewState["_CurrentPageAdvertiser"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["_CurrentPageAdvertiser"] = value;
        }
    }
    protected string GetCloseMessage
    {
        get
        {
            return lbl_IfToExite.Text;
        }
    }
    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }
}
