﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Publisher_InactiveReasons : PageSetting
{
    const string ExcelName = "InactiveReasons";
    const WebReferenceSite.eTableType _eTableType = WebReferenceSite.eTableType.InactivityReason;
    protected void Page_Load(object sender, EventArgs e)
    {
        SetStatusNameId1.SetStatusType(ExcelName, _eTableType, lbl_TitleName.Text);

    }
    protected override void OnLoadComplete(EventArgs e)
    {
        SetStatusNameId1._OnLoadComplete();
        base.OnLoadComplete(e);
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    
}