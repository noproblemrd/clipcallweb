using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class Publisher_Consumers : PageSetting
{
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    protected void Page_Load(object sender, EventArgs e)
    {
        TablePaging1._lnkPage_Click += new EventHandler(_Paging_click);
        TablePaging1.PagesInPaging = PAGE_PAGES;
        TablePaging1.ItemInPage = ITEM_PAGE;
        if (!IsPostBack)
        {            
            this.ClearGuidSetting();
        }        
        Page.Header.DataBind();
    }
    
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        TablePaging1.Current_Page = 1;
        string _txt = txtSearch.Text.Trim();
        txt_searchV = _txt;
        __search(1, _txt);        
    }
    protected void _Paging_click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        __search(_pageNum, txt_searchV);
    }
    void __search(int _CurrentPage, string _txt)
    {
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(this);
        WebReferenceCustomer.SearchConsumersRequestPaged scrp = new WebReferenceCustomer.SearchConsumersRequestPaged();
        scrp.CurrentPage = _CurrentPage;
        scrp.PageSize = ITEM_PAGE;
        scrp.SearchParameter = _txt;
        WebReferenceCustomer.ResultOfPagedListOfConsumerMinimalData result = null;
        try
        {
            result = _customer.SearchConsumersPaged(scrp);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        TablePaging1.Current_Page = result.Value.CurrentPage;
        TablePaging1.Pages_Count = result.Value.NumberOfPages;
        lbl_Count.Text = result.Value.NumberOfRecords + "";
        lbl_RecordMached.Text = result.Value.NumberOfRecords + " " + lblRecordMached.Text;
        _UpdatePanelSearch.Update();
    //    lbl_details.Text = "count= " + result.Value.NumberOfPages + "current page= " + result.Value.CurrentPage;
        DataTable data = new DataTable();
        data.Columns.Add("ContactId");
        data.Columns.Add("Email");
        data.Columns.Add("MobilePhone");
        data.Columns.Add("FirstName");
        data.Columns.Add("LastName");
        data.Columns.Add("NumberOfRequests");
        data.Columns.Add("Number");
        data.Columns.Add("OnClientClick");

        foreach (WebReferenceCustomer.ConsumerMinimalData _counsumer in result.Value.List)
        {

            DataRow row = data.NewRow();
            row["Number"] = string.IsNullOrEmpty(_counsumer.ConsumerNumber) ? @"****" : _counsumer.ConsumerNumber;
            row["ContactId"] = _counsumer.ContactId.ToString();
            row["Email"] = _counsumer.Email;
            row["MobilePhone"] = _counsumer.MobilePhone;
            row["FirstName"] = _counsumer.FirstName;
            row["LastName"] = _counsumer.LastName;
            row["NumberOfRequests"] = _counsumer.NumberOfRequests;

            row["OnClientClick"] = "javascript:openIframe('" + _counsumer.ContactId.ToString() + "');";
            data.Rows.Add(row);
        }
        _reapeter.DataSource = data;
        _reapeter.DataBind();
        int _modulu = 0;
        
        foreach (RepeaterItem _item in _reapeter.Items)
        {
            HtmlTableRow tr = (HtmlTableRow)_item.FindControl("tr_show");
            string _class = (_modulu++ % 2 == 0) ? "even" : "odd";
            tr.Attributes["class"] = _class;
        }
        TablePaging1.LoadPages();
        _UpdatePanel.Update();
    }
    
    
    protected string GetCloseMessage
    {
        get
        {
            return lbl_IfToExite.Text;
        }
    }

    string txt_searchV
    {
        get { return (ViewState["txt_search"] == null) ? string.Empty : (string)ViewState["txt_search"]; }
        set { ViewState["txt_search"] = value; }
    }
    

    
    
}
