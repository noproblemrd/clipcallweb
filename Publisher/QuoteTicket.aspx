﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QuoteTicket.aspx.cs" Inherits="Publisher_QuoteTicket" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" rel="stylesheet" href="Style.css" /> 
    <title></title>
    <script type="text/javascript">
        function CloseMeError() {
            alert("<%# lbl_ErrorLoadData.Text %>");
            //parent.CloseIframe()
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="form-analyticscomp2" style="margin-top:40px; margin-left: 30px;">
        <div class="titlerequestTop" style="margin-top: 20px;">          
            <asp:Label ID="lblQuoteNumber" runat="server" Text="Quote number"></asp:Label>:
            <asp:Label ID="lbl_QuoteNumber" runat="server"></asp:Label>
        </div>
    <%/*    <div class="clear"></div>*/ %> 
        <div>
            <div style="float:left;">
                <div class="form-fieldshort">          
                    <asp:Label ID="Label1" runat="server" Text="Price:" CssClass="label"></asp:Label>
                    <asp:Label ID="lbl_Price" runat="server"></asp:Label>
                </div>
                <div class="clear"></div>
                <div class="form-fieldshort">          
                    <asp:Label ID="Label2" runat="server" Text="Status:" CssClass="label"></asp:Label>
                    <asp:Label ID="lbl_Status" runat="server"></asp:Label>
                </div>
                <div class="clear"></div>

                <div class="form-fieldshort">          
                    <asp:Label ID="Label3" runat="server" Text="Status Reason:" CssClass="label"></asp:Label>
                    <asp:Label ID="lbl_StatusReason" runat="server"></asp:Label>
                </div>
                <div class="clear"></div>

                 <div class="form-fieldshort">          
                    <asp:Label ID="Label4" runat="server" Text="Created On:" CssClass="label"></asp:Label>
                    <asp:Label ID="lbl_CreatedOn" runat="server"></asp:Label>
                </div>
                <div class="clear"></div>

                <div class="form-fieldshort">          
                    <asp:Label ID="Label5" runat="server" Text="Start At:" CssClass="label"></asp:Label>
                    <asp:Label ID="lbl_StartAt" runat="server"></asp:Label>
                </div>
            </div>
            <div style="float:left;">
                <div class="form-fieldshort">          
                    <asp:Label ID="Label7" runat="server" Text="Guarantee Duration:" CssClass="label"></asp:Label>
                    <asp:Label ID="lbl_GuaranteeDuration" runat="server"></asp:Label>
                </div>
                <div class="clear"></div>

                <div class="form-fieldshort">          
                    <asp:Label ID="Label9" runat="server" Text="Include Materials:" CssClass="label"></asp:Label>
                    <asp:CheckBox ID="cb_includematerials" runat="server" />
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="clear"></div>
        <div>          
            <asp:Label ID="Label6" runat="server" Text="Description:" CssClass="label"></asp:Label>
            <asp:TextBox ID="txt_description" runat="server" ReadOnly="true" TextMode="MultiLine" Width="300" Rows="5"></asp:TextBox>
        </div>
        
    </div>
           <asp:Label ID="lbl_ErrorLoadData" runat="server" Text="There was an error to load the details." Visible="false"></asp:Label>
    </form>
</body>
</html>
