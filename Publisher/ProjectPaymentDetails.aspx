﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProjectPaymentDetails.aspx.cs" Inherits="Publisher_ProjectPaymentDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" rel="stylesheet" href="Style.css" /> 

    <title></title>

    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript">


        function createTransfer(_btn, quoteId, amount) {
            //div_review_loader
            var AreYouSure = confirm("You are about to transfer " + amount);
            if (!AreYouSure)
                return false;
            var $div_parent = $(_btn).parents('div.div_CreateTransfer');
            var $div_loader = $div_parent.find('div.div_transfer_loader');
            $div_loader.show();
            var spanResult = $div_parent.find('span.span_CreateTransfer_response')
            var quoteId = $(_btn).parents('tr').find('input[type="hidden"]').val();
            $div_parent.find('div.div_CreateTransfer_btn').hide();
            $.ajax({
                url: "<%# CreateTransferUrl %>",
                data: "{ 'quoteId': '" + quoteId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    // var _data = eval("(" + data.d + ")");
                    var _response = data.d == "true" ? "success" : "failed";
                    spanResult.html(_response);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;
                    $div_loader.hide();
                }
            });
            return false;
        }
        function createPartialTransfer(_btn, quoteId, totalOptional) {
            //div_review_loader
            var $tr_parent = $(_btn).parents('tr.tr_left');
            var _txt = $tr_parent.find('input[type="text"]').val();
            var spanResult = $tr_parent.find('span.span_CreatePartialTransfer_response');
            if (isNaN(_txt) || _txt.length == 0) {
                spanResult.html("Range Validator");
                return false;
            }
            var _amount = parseFloat(_txt);
            if (_amount < 1 || _amount > totalOptional)
            {
                spanResult.html("Range Validator");
                return false;
            }
            var AreYouSure = confirm("You are about to transfer $" + _amount.toFixed(2));
            if (!AreYouSure)
                return false;

            var $div_loader = $tr_parent.find('div.div_partial_transfer_loader');
            $div_loader.show();
            
        //    var quoteId = $(_btn).parents('tr').find('input[type="hidden"]').val();
            $tr_parent.find('div.div_createPartialTransfer_btn').hide();
           
                $.ajax({
                    url: "<%# CreatePartialTransferUrl %>",
                    data: "{ 'quoteId': '" + quoteId + "', 'amount': " + _amount + "}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        // var _data = eval("(" + data.d + ")");
                        var _response = data.d == "true" ? "success" : "failed";
                        spanResult.html(_response);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    },
                    complete: function (jqXHR, textStatus) {
                        //  HasInHeadingRequest = false;
                        $div_loader.hide();
                    }
                });
            
            return false;
        }

        function CloseMeError() {
            alert("<%# lbl_ErrorLoadData.Text %>");
            //parent.CloseIframe()
            window.close();
        }
        
        function createRefundQuote(_btn, quoteId, num) {
            //div_review_loader
            var AreYouSure = confirm("You are about to refund quote " + num);
            if (!AreYouSure)
                return false;
            var $div_parent = $(_btn).parents('div.div_CreateRefund');
            var $div_loader = $div_parent.find('div.div_refund_loader');
            $div_loader.show();
            var spanResult = $div_parent.find('span.span_CreateRefund_response')
            var quoteId = $(_btn).parents('tr').find('input[type="hidden"]').val();
            $div_parent.find('div.div_CreateRefund_btn').hide();
            $.ajax({
                url: "<%# RefundQuoteUrl %>",
                data: "{ 'quoteId': '" + quoteId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    // var _data = eval("(" + data.d + ")");
                    var _response = data.d == "true" ? "success" : "failed";
                    spanResult.html(_response);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;
                    $div_loader.hide();
                }
            });
            return false;
        }
        function createUnbook(incidentAccountId) {
            //div_review_loader
            var AreYouSure = confirm("You are about to unbook");
            if (!AreYouSure)
                return false;
            showDiv();
            $.ajax({
                url: "<%# UnbookIncidentUrl %>",
                data: "{ 'incidentAccountId': '" + incidentAccountId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if(data.d != "true")
                        alert(data.d)
                    else
                    {
                        document.querySelector("#<%# div_unbook.ClientID %>").style.display = 'none';
                        window.location.href = window.location.href;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    hideDiv();
                }
            });
            return false;
        }
        function OpenIframe(_path) {
            var _leadWin = window.open(_path, "ProjectDetails", "width=1200, height=650,scrollbars=1");
            _leadWin.focus();
        }
        function RequestpaymentAll(incidentAccountId) {
            //div_review_loader
            var AreYouShure = confirm("Are you sure to request a payment");
            if (!AreYouShure)
                return false;
            showDiv();
           
            $.ajax({
                url: "<%# RequestpaymentAllWebMethod %>",
                 data: "{ 'incidentAccountId': '" + incidentAccountId + "'}",
                 dataType: "json",
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 dataFilter: function (data) { return data; },
                 success: function (data) {
                     if (data.d != "true")
                         alert(data.d)
                     else {
                         document.querySelector("#<%# div_RequestPayment.ClientID %>").style.display = 'none';
                         window.location.href = window.location.href;
                     }
                 },
                 error: function (XMLHttpRequest, textStatus, errorThrown) {
                     alert(textStatus);
                 },
                 complete: function (jqXHR, textStatus) {
                     //  HasInHeadingRequest = false;
                     hideDiv();
                 }
             });
             return false;
        }        
        function Requestpayment(_btn, incidentAccountId, quoteId) {
            //div_review_loader
            var AreYouShure = confirm("Are you sure to request a payment");
            if (!AreYouShure)
                return false;
            var $div_parent = $(_btn).parents('div.div_requestPayment');
            var $div_loader = $div_parent.find('div.div_requestPayment_loader');
            $div_loader.show();
            var spanResult = $div_parent.find('span.span_requestPayment_response')

            $div_parent.find('div.div_requestPayment_btn').hide();
            $.ajax({
                url: "<%# RequestpaymentWebMethod %>",
                data: "{ 'incidentAccountId': '" + incidentAccountId + "', 'quoteId': '" + quoteId + "'}",
                 dataType: "json",
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 dataFilter: function (data) { return data; },
                 success: function (data) {
                     // var _data = eval("(" + data.d + ")");
                     var _response = data.d == "true" ? "success" : "failed";
                     spanResult.html(_response);
                 },
                 error: function (XMLHttpRequest, textStatus, errorThrown) {
                     alert(textStatus);
                 },
                 complete: function (jqXHR, textStatus) {
                     //  HasInHeadingRequest = false;
                     $div_loader.hide();
                 }
             });
             return false;
        }        
    </script>
</head>
<body style="background-color:#EDF0F4;overflow-x:hidden;overflow-y:auto;">
    <form id="form1" runat="server">
    <div>
      <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">    
     </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="_UpdatePanel_RequestStop" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
   
        <div runat="server" id="div_unbook" class="upsalebtn  div_relaunch" style="margin-left:20px;">
             <asp:LinkButton ID="lb_unbook" runat="server" CssClass="StopRequest">UNBOOK</asp:LinkButton>
        </div>
         <div runat="server" id="div_RequestPayment" class="upsalebtn  div_relaunch">
             <asp:LinkButton ID="lb_RequestPayment" runat="server" CssClass="StopRequest">Request Payment</asp:LinkButton>
        </div>
    
             
    </ContentTemplate>
    </asp:UpdatePanel>     
     
    
        <div class="titlerequestTop" style="margin-bottom:50px;">   
            <a id="a_projectNumber" runat="server">
                <asp:Label ID="lbl_CurrentRequest_Title" runat="server" Text="ProjectId"></asp:Label>:
                <asp:Label ID="lbl_ProjectId" runat="server"></asp:Label>
            </a>
        </div>
        <div class="clear"></div>
        <div class="separator"></div>
        <div id="form-analyticscomp2" style="width:1450px;">

            <div class="tabswrap2" style="display:block;" >
                <div class="containertab2" style="position:relative;">
                    <div runat="server" id="div_Details">
                                             
                            <asp:Repeater runat="server" ID="_reapeter" OnItemDataBound="_reapeter_ItemDataBound">
                                <HeaderTemplate>
                                    <table class="data-table" style="margin:10px !important;">
                                        
                                        <thead>
                                        <tr>                               
                                            <th><asp:Label ID="Label51" runat="server" Text="Quote Date"></asp:Label></th>                 
                                            <th><asp:Label ID="lbl_Date" runat="server" Text="Booked Date"></asp:Label></th>			                                    
			                                <th><asp:Label ID="Label001" runat="server" Text="Payment Plan"></asp:Label></th>	
                                            <th><asp:Label ID="Label53" runat="server" Text="Amount"></asp:Label></th>
                                            <th><asp:Label ID="Label5" runat="server" Text="Customer Bonus" ></asp:Label></th>			                                    
			                                <th><asp:Label ID="Label6" runat="server" Text="Plan Discount" ></asp:Label></th>
                                            <th><asp:Label ID="Label17" runat="server" Text="ClipCall Fee" ></asp:Label></th>
                                            <th><asp:Label ID="Label5001" runat="server" Text="Paid"></asp:Label></th>
                                            <th><asp:Label ID="Label14" runat="server" Text="Past Installments"></asp:Label></th>
                                            <th><asp:Label ID="Label5003" runat="server" Text="Customer Balance"></asp:Label></th>             
                                            <th><asp:Label ID="Label1112" runat="server" Text="Transferred At"></asp:Label></th>  
                                            <th><asp:Label ID="Label11" runat="server" Text="Transferred Amount"></asp:Label></th> 
                                            <th><asp:Label ID="Label15" runat="server" Text="StripeAccountId"></asp:Label></th>
                                            <th><asp:Label ID="Label52" runat="server" Text="Refund"></asp:Label></th>	
                                            <th><asp:Label ID="Label222" runat="server" Text="Transfer"></asp:Label></th>                                                                                         
                                            <th><asp:Label ID="Label244" runat="server" Text="Create request payment"></asp:Label></th>      
                                            <th><asp:Label ID="Label16" runat="server" Text="Request Payment"></asp:Label></th>
                                            <th><asp:Label ID="Label19" runat="server" Text="Request Payment Reason"></asp:Label></th>
                                            <th><asp:Label ID="Label20" runat="server" Text="Request Payment date"></asp:Label></th>
			                            </tr>
                                        </thead>
			                        </HeaderTemplate>
	                                <ItemTemplate>
                                        <tr id="tr_show" runat="server">
                                            <td>                                                    
                                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("QuoteDate") %>'></asp:Label>    
			                                </td>
			                                <td>
			                                    <asp:Label ID="lbl_name" runat="server" Text='<%# Bind("PaidDate") %>'></asp:Label>                                                   
                                                <asp:HiddenField ID="h_QuoteId" runat="server" Value='<%# Bind("QuoteId") %>' />                                                   
			                                </td>       			                         
			                                <td>
			                                    <asp:Label ID="Label501" runat="server" Text='<%# Bind("PaymentPlan") %>'></asp:Label>      
			                                </td>
                                            <td>
			                                    <asp:Label ID="Label503" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
			                                </td>
                                            <td>
                                                <asp:Label ID="Label507" runat="server" Text='<%# Bind("CustomerBonus") %>'></asp:Label>
			                                </td>			                                     
			                                <td>
                                                <asp:Label ID="Label508" runat="server" Text='<%# Bind("PlanDiscount") %>'></asp:Label>
			                                </td>
                                            <td>                                                		                                     
                                                <asp:Label ID="Label18" runat="server" Text='<%# Bind("ClipCallFee") %>'></asp:Label>
			                                </td>
                                            <td>
			                                    <asp:Label ID="Label504" runat="server" Text='<%# Bind("Charged") %>'></asp:Label>     
			                                </td>
                                            <td>
			                                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("PastInstallments") %>'></asp:Label>     
			                                </td>
                                            <td>
			                                    <asp:Label ID="Label505" runat="server" Text='<%# Bind("Balance") %>'></asp:Label>     
			                                </td>
                                             <td>
			                                    <asp:Label ID="Label10" runat="server" Text='<%# Bind("TransferredAt") %>'></asp:Label>     
			                                </td>
                                            <td>
			                                    <asp:Label ID="Label12" runat="server" Text='<%# Bind("TransferredAmount") %>'></asp:Label>     
			                                </td>
                                            <td >
                                                <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" Text='<%# Bind("StripeAccountId") %>'
                                                    NavigateUrl='<%# Bind("StripeAccountUrl") %>' Visible='<%# Bind("HasStripeAccount") %>'>
                                                    </asp:HyperLink>
			                                </td>  
                                            <td >
                                                <div runat="server" id="div_CreateRefund" class="div_CreateRefund" visible='<%# Bind("CanCreateRefund") %>'>
                                                    <div class="div_CreateRefund_btn">
                                                        <asp:Button ID="btn_CreateRefund" runat="server" Text="Create Refund" OnClientClick='<%# Bind("ClickCreateRefund") %>' />
                                                                                                        
                                                    </div>
                                                    <div style="text-align:center; display:none;" class="div_refund_loader">
                                                        <asp:Image ID="img_CreateRefund" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                                    </div>
                                                    <span class="span_CreateRefund_response" style="color:red;"></span>
                                                </div>
                                                <div runat="server" id="div_refunded" visible='<%# Bind("IsRefunded") %>'>
                                                    <span class="span_CreateRefund_response" style="color:red; font-size:18px;">REFUNDED</span>
                                                </div>
			                                </td>   

			                                <td>
                                                <div runat="server" id="div_CreateTransfer" class="div_CreateTransfer" visible='<%# Bind("CanCreateTransfer") %>'>
                                                    <div class="div_CreateTransfer_btn">
                                                        <asp:Button ID="btn_CreateTransfer" runat="server" Text="Create transfer" OnClientClick='<%# Bind("ClickCreateTransfer") %>' />
                                                                                                        
                                                    </div>
                                                    <div style="text-align:center; display:none;" class="div_transfer_loader">
                                                        <asp:Image ID="img_CreateTransfer" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                                    </div>
                                                    <span class="span_CreateTransfer_response" style="color:red;"></span>
                                                </div>
			                                </td>	                                     
			                              <td> 
                                              <div runat="server" id="div_requestPayment" class="div_requestPayment" visible='<%# Bind("CanCreateRequestPayment") %>'>
                                                <div class="div_requestPayment_btn">
                                                    <asp:Button ID="btn_requestPayment" runat="server" Text="Create Request Payment" OnClientClick='<%# Bind("CreateRequestPayment") %>' />
                                                                                                        
                                                </div>
                                                <div style="text-align:center; display:none;" class="div_requestPayment_loader">
                                                    <asp:Image ID="img_requestPayment" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                                </div>
                                                <span class="span_requestPayment_response" style="color:red;"></span>
                                            </div>
                                            </td>
                                            <td>
			                                    <asp:Label ID="Label21" runat="server" Text='<%# Bind("RequestPaymentStatus") %>'></asp:Label>     
			                                </td>
                                            <td>
			                                    <asp:Label ID="Label22" runat="server" Text='<%# Bind("RequestPaymentStatusReason") %>'></asp:Label>     
			                                </td>
                                            <td>
			                                    <asp:Label ID="Label23" runat="server" Text='<%# Bind("RequestPaymentCreatedOn") %>'></asp:Label>     
			                                </td>
                                                 
                                            </tr>
                                            <tr>
                                                <td colspan="13">
                                                    <asp:GridView ID="gv_deal" runat="server" CssClass="data-table2" AutoGenerateColumns="false" Caption="Deal">
                                                        <RowStyle CssClass="odd" />
                                                        <AlternatingRowStyle CssClass="even" />                        
                                                        <Columns>
                                                             <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Label101" runat="server" Text="Amount"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label102" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Label103" runat="server" Text="Operation Type"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label104" runat="server" Text='<%# Bind("OperationType") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Label105" runat="server" Text="Is Done"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label106" runat="server" Text='<%# Bind("IsDone") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Label107" runat="server" Text="Charge link"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl='<%# Bind("ChargeLink") %>' Visible='<%# Bind("HasChargeLink") %>'>Link</asp:HyperLink>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                        
                                                        </Columns>
                                                    </asp:GridView>
   

     <asp:GridView ID="gv_customer" runat="server" CssClass="data-table2" AutoGenerateColumns="false" Caption="Customer">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />                        
                    <Columns>
                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label201" runat="server" Text="Amount"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label202" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label203" runat="server" Text="Operation Type"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label204" runat="server" Text='<%# Bind("OperationType") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label205" runat="server" Text="Is Done"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label206" runat="server" Text='<%# Bind("IsDone") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label207" runat="server" Text="Charge link"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink201" runat="server" Target="_blank" NavigateUrl='<%# Bind("ChargeLink") %>' Visible='<%# Bind("HasChargeLink") %>'>Link</asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label217" runat="server" Text="Invoice"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink211" runat="server" Target="_blank" NavigateUrl='<%# Bind("InvoiceLinkLink") %>' Visible='<%# Bind("HasInvoiceLink") %>'>Invoice</asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
        </asp:GridView>
                                                     <asp:GridView ID="gv_supplier" runat="server" CssClass="data-table2" AutoGenerateColumns="false" Caption="Supplier">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />                        
                    <Columns>
                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="Amount"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="Operation Type"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("OperationType") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="Is Done"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("IsDone") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="Charge link"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink2" runat="server" Target="_blank" NavigateUrl='<%# Bind("ChargeLink") %>' Visible='<%# Bind("HasChargeLink") %>'>Link</asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
        </asp:GridView>
                                                    </td>
                                        </tr>
                                            <tr class="tr_left odd" runat="server" id="tr_partialTransfer" visible='<% # Bind("CanCreatePartialTransfer") %>'>
                                                <td colspan="2">
                                                    <asp:Label ID="Label13" runat="server" Text="Partial Transfer"></asp:Label>
                                                </td>
                                                 <td colspan="5">
                                                     <asp:TextBox ID="txt_transfer" runat="server" Text='<% # Bind("MaxValue") %>'></asp:TextBox>
                                                     
                                                </td>
                                                <td colspan="6">
                                                    <div class="div_createPartialTransfer_btn">
                                                        <asp:Button ID="btn_partialTransfer" runat="server" Text="Create transfer" OnClientClick='<%# Bind("ClickCreatePartialTransfer") %>' />
                                                     </div>
                                                    <div style="text-align:center; display:none;" class="div_partial_transfer_loader">
                                                        <asp:Image ID="img_partialTransfer" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                                    </div>
                                                    <span class="span_CreatePartialTransfer_response" style="color:red;"></span>
                                                </td>
                                            </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>                
                                    </FooterTemplate>      
                                </asp:Repeater>
                            
                        </div>
                </div>
            </div>
        </div>
    </div>
        <asp:Label ID="lbl_ErrorLoadData" runat="server" Text="There was an error to load the details." Visible="false"></asp:Label>
    </form>
</body>
</html>
