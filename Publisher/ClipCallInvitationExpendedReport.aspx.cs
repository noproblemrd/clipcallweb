﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallInvitationExpendedReport : PageSetting
{
    const string Num_Format = "{0:#,0.##}";
    const int ONE_DAY = 1;
    protected readonly string SessionTableName = "dataInvitationExpendedReport";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle("Invitation Dashboard");
        ToolboxReport1.RemovePrintButton();
        /*
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
         * */
        ToolboxReport1.RemoveExcelButton();
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.InvitationReportRequest _request = new WebReferenceReports.InvitationReportRequest();
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo.AddDays(1);
        WebReferenceReports.ResultOfListOfExpendedInvitationReportResponse result = null;
        try
        {
            result = reports.ClipCallExpendedInvitationReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        DataTable table = new DataTable();
        table.Columns.Add("Date");
        //table.Columns.Add("ToPhone");
        table.Columns.Add("VideoType");
        table.Columns.Add("InviteBy");
        table.Columns.Add("SupplierName");
        table.Columns.Add("SupplierPhone");
        table.Columns.Add("Clicked", typeof(bool));
        table.Columns.Add("ClickedAt");
        table.Columns.Add("InstalledAt");
        table.Columns.Add("Installed", typeof(bool));
        table.Columns.Add("HasVideoChat");
        table.Columns.Add("HasVideoLead");
        table.Columns.Add("HasChat");
        table.Columns.Add("HasPhoneCall");
        foreach(WebReferenceReports.ExpendedInvitationReportResponse rec in result.Value)
        {
            DataRow row = table.NewRow();
            row["Date"] = GetDateTimeStringFormat(rec.CreatedOn);
            row["VideoType"] = rec.Invitation_Type.ToString().Replace("_", " ");
            row["InviteBy"] = rec.InvitationFrom == WebReferenceReports.CreateVideoChatBy.AccountInvitation ? "App invitation" : "Web invitation";
            row["SupplierName"] = rec.SupplierName;
            row["SupplierPhone"] = rec.SupplierPhone;
            row["Clicked"] = rec.Clicked != DateTime.MinValue;
            row["ClickedAt"] = GetDateTimeStringFormat(rec.Clicked);
            row["Installed"] = rec.Installed != DateTime.MinValue;
            row["InstalledAt"] = GetDateTimeStringFormat(rec.Installed);
            row["HasVideoChat"] = rec.HasVideoChat ? "HasRecords" : "";
            row["HasVideoLead"] = rec.HasVideoLead ? "HasRecords" : "";
            row["HasChat"] = rec.HasChat ? "HasRecords" : "";
            row["HasPhoneCall"] = rec.HasPhoneCall ? "HasRecords" : "";
            table.Rows.Add(row);
        }
        _reapeter.DataSource = table;
        _reapeter.DataBind();

        lbl_RecordMached.Text = result.Value.Count()+"";
        lbl_TotalLinkPressed.Text = result.Value.Count(x => x.Clicked != DateTime.MinValue) + "";
        lbl_TotalInstalled.Text = result.Value.Count(x => x.Installed != DateTime.MinValue) + "";
        lbl_TotalComunication.Text = result.Value.Count(x => x.HasChat || x.HasPhoneCall || x.HasVideoChat || x.HasVideoLead) + "";
        div_results.Visible = true;
    }
    string GetDateTimeStringFormat(DateTime dateTime)
    {
        if (dateTime == DateTime.MinValue)
            return null;
        return string.Format(siteSetting.DateFormat, dateTime) + " " +
                   string.Format(siteSetting.TimeFormat, dateTime);
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    
}