﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SalesMen.aspx.cs" Inherits="Publisher_SalesMen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script  type="text/javascript" src="../Calender/Calender.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<link href="style.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }

    function SetDetails(_elem) {
        $(_elem).parents(".div_parent").find(".div_details").toggle();
    }
   
    

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
    
</cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  
<div class="page-content minisite-content2">
	<div id="form-analytics">
        <div class="callreport"><uc1:FromToDate runat="server" ID="FromToDate1" /></div>
    </div>
    <div class="clear"></div>
     
    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>         
        <asp:Repeater ID="_Repeater" runat="server" 
            onitemdatabound="_Repeater_ItemDataBound"  >
            <ItemTemplate>
                <div class="div_parent">
                    <h3>                       
                        <a class="row_SalesManTitle" onclick="SetDetails(this);"><asp:Label ID="lbl_Name" runat="server" Text="<%# Container.DataItem %>"></asp:Label></a>
                    </h3> 
                    <div style="display:none;" class="div_details" runat="server" id="div_details">       
                    <asp:Repeater ID="_RepeaterIn" runat="server" onitemdatabound="_RepeaterIn_ItemDataBound">                    
                    <HeaderTemplate>
                        <table class="data-table">
                        <thead >
                        <tr >
                        <th>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label2" runat="server" Text="<%# lbl_Amount.Text %>"></asp:Label>
                        </th>                        
                        <th runat="server" visible="false">
                            <asp:Label ID="Label4" runat="server" Text="<%# lbl_FirstDeposit.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_AccountName.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label6" runat="server" Text="<%# lbl_AccountNumber.Text %>"></asp:Label>
                        </th>                        
                        <th>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_AccountApproved.Text %>"></asp:Label>
                        </th>     
                        <th>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_BizId.Text %>"></asp:Label>
                        </th>   
                        <th>
                            <asp:Label ID="Label8" runat="server" Text="<%# lbl_RechargeType.Text %>"></asp:Label>
                        </th> 
                        <th>
                            <asp:Label ID="Label9" runat="server" Text="<%# lbl_PaymentMethod.Text %>"></asp:Label>
                        </th> 
                        <th>
                            <asp:Label ID="Label10" runat="server" Text="<%# lbl_SalesmanName.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label11" runat="server" Text="<%# lbl_AmountForCalls.Text %>"></asp:Label>
                        </th>  
                        <th>
                            <asp:Label ID="Label12" runat="server" Text="<%# lbl_AmountNotForCalls.Text %>"></asp:Label>
                        </th>                
                        </tr>
                         </thead>
                        <tbody >
                    </HeaderTemplate>
                    <ItemTemplate>
                    
                            <tr id="_tr" runat="server">
                            <td>
                                <asp:Label ID="lbl_DepositCreatedOn" runat="server" Text="<%# Bind('DepositCreatedOn') %>"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_MoneyAmount" runat="server" Text="<%# Bind('MoneyAmount') %>"></asp:Label>
                            </td>                            
                            <td runat="server" visible="false">
                                <asp:Label ID="lbl_IsFirstDeposit" runat="server" Text="<%# Bind('IsFirstDeposit') %>"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_AccountName" runat="server" Text="<%# Bind('AccountName') %>"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_AccountNumber" runat="server" Text="<%# Bind('AccountNumber') %>"></asp:Label>
                            </td>                           
                            <td>
                                <asp:Label ID="lbl_AccountApprovedOn" runat="server" Text="<%# Bind('AccountApprovedOn') %>"></asp:Label>
                            </td>  
                            <td>
                                <asp:Label ID="lbl_BizId" runat="server" Text="<%# Bind('BizId') %>"></asp:Label>
                            </td>    
                             <td>
                                <asp:Label ID="lbl_RechargeType" runat="server" Text="<%# Bind('RechargeType') %>"></asp:Label>
                            </td>  
                             <td>
                                <asp:Label ID="lbl_PaymentMethod" runat="server" Text="<%# Bind('PaymentMethod') %>"></asp:Label>
                            </td> 
                             <td>
                                <asp:Label ID="lbl_SalesmanName" runat="server" Text="<%# Bind('SalesmanName') %>"></asp:Label>
                            </td> 
                            <td>
                                <asp:Label ID="lbl_AmountForCalls" runat="server" Text='<%# Bind("AmountForCalls", "{0:0.00}") %>'></asp:Label>
                            </td>     
                            <td>
                                <asp:Label ID="lbl_AmountNotForCalls" runat="server" Text='<%# Bind("AmountNotForCalls", "{0:0.00}") %>'></asp:Label>
                            </td>                
                            </tr>
                    </ItemTemplate>      
                        <FooterTemplate>
                             </tbody>
                            </table>
                        </FooterTemplate>              
                </asp:Repeater>
                   </div>
                   </div>
            </ItemTemplate>
        </asp:Repeater>
    </ContentTemplate>
    </asp:UpdatePanel>
  
</div>

<asp:Label ID="lbl_Date" runat="server" Text="Deposit date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Amount" runat="server" Text="Amount" Visible="false"></asp:Label>
<asp:Label ID="lbl_FirstDeposit" runat="server" Text="First deposit" Visible="false"></asp:Label>
<asp:Label ID="lbl_AccountName" runat="server" Text="Account name" Visible="false"></asp:Label>
<asp:Label ID="lbl_AccountNumber" runat="server" Text="Account number" Visible="false"></asp:Label>
<asp:Label ID="lbl_AccountApproved" runat="server" Text="Account approved on" Visible="false"></asp:Label>
<asp:Label ID="lbl_BizId" runat="server" Text="BizId" Visible="False"></asp:Label>
<asp:Label ID="lbl_RechargeType" runat="server" Text="Recharge Type" Visible="False"></asp:Label>
<asp:Label ID="lbl_PaymentMethod" runat="server" Text="Payment Method" Visible="False"></asp:Label>
<asp:Label ID="lbl_SalesmanName" runat="server" Text="Salesman Name" Visible="False"></asp:Label>
<asp:Label ID="lbl_AmountForCalls" runat="server" Text="Amount For Calls" Visible="False"></asp:Label>
<asp:Label ID="lbl_AmountNotForCalls" runat="server" Text="Amount Not For Calls" Visible="False"></asp:Label>

<asp:Label ID="lbl_ReportName" runat="server" Text="Salesmen" Visible="false"></asp:Label>


</asp:Content>

