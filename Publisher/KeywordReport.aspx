﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="KeywordReport.aspx.cs" Inherits="Publisher_KeywordReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../FromToDate.css" type="text/css" rel="stylesheet" />
<link href="../Calender/CalenderDiv.css" rel="stylesheet" type="text/css" />
<link href="../UpdateProgress/updateProgress.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script  type="text/javascript" src="../Calender/_Calender.js"></script>
<script  type="text/javascript" src="../ShowHideDiv.js"></script>
<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }

    //send to server
    function CheckFor() {
        CallServer("y", "ee");
    }
    //replay from the server |not delete|//
    function ReciveServerData(retValue) {
        
        if (retValue == 'done') {
            $(window).unbind('scroll');
            return;
        }
        $(window).scroll(_scroll);
        if (retValue.length == 0)
            return;
        var _trs = $(retValue).get(0).getElementsByTagName('tr');
        var _length = _trs.length;
        for (var i = 1; i < _length; i++) {
            _trs[1].className = ((i % 2 == 1) ? "_even" : "_odd");
            $('#<%# _GridView.ClientID %>').append(_trs[1]);
        } 
        var e = 0;

    }

    $(window).scroll(_scroll);
    function _scroll() {
        if ($(window).scrollTop() > 0.9 * ($(document).height() - $(window).height())) {
            $(window).unbind('scroll');
            CallServer("", "");
        }
    }  


</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  
<div class="page-content minisite-content2">
    <div id="form-analytics">
	    <div class="main-inputs">	
          
          <div class="form-field">       
                <asp:Label ID="lbl_Keywords" runat="server" class="label" Text="Keywords"></asp:Label>
                <asp:TextBox ID="txt_Keywords" runat="server" CssClass="form-text"></asp:TextBox>
               
            </div>
             <div class="form-field">
                <asp:Label ID="lbl_Heading" runat="server" class="label" Text="Heading"></asp:Label>
                 <asp:DropDownList ID="ddl_Headings" runat="server" CssClass="form-select">
                 </asp:DropDownList>
             </div>

            
            <div class="clear"></div>
            <div class="form-field">
                 <asp:CheckBox ID="cb_IncludeUpsales" runat="server" Checked="true" Text="Include Upsales:" />
            </div> 
            
            

            
            
           
	    </div>
       
	    <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>   
		 <div class="clear"></div>
		
	</div>
    
    <div id="list" class="table-keyword">          
        <asp:UpdatePanel runat="server" ID="_uPanel" UpdateMode="Conditional">
            <ContentTemplate>
                
            <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
             <div class="results" runat="server" id="div_LastUpdate" visible="false">                 
                <asp:Label ID="lbl_LastUpdate" runat="server" Text="Last update:"></asp:Label>
                <asp:Label ID="lblLastUpdate" runat="server"></asp:Label>
            </div>   
             <div class="div_gridMyCall">
                    <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                        CssClass="data-table"  >
                    <RowStyle CssClass="_even" />
                    <AlternatingRowStyle CssClass="_odd" />
                    <Columns>
                    <asp:TemplateField SortExpression="Keyword">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Keyword.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text="<%# Bind('Keyword') %>"></asp:Label> 
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                     <asp:TemplateField SortExpression="Hits">
                        <HeaderTemplate>
                            <asp:Label ID="Label33" runat="server" Text="<%# lbl_Hits.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label34" runat="server" Text="<%# Bind('Hits') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="Exposures">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Exposures.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text="<%# Bind('Exposures') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="Requests">
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_Requests.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text="<%# Bind('Requests') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="Heading">
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_Category.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text="<%# Bind('Heading') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="OptOutforever">
                        <HeaderTemplate>
                            <asp:Label ID="Label17" runat="server" Text="<%# lbl_OptOutforever.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label18" runat="server" Text="<%# Bind('OptOutforever') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                   

                    <asp:TemplateField SortExpression="PotentialRevenue">
                        <HeaderTemplate>
                            <asp:Label ID="Label13" runat="server" Text="<%# lbl_PotentialRevenue.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label14" runat="server" Text="<%# Bind('PotentialRevenue') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="ActualRevenue">
                        <HeaderTemplate>
                            <asp:Label ID="Label15" runat="server" Text="<%# lbl_ActualRevenue.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label16" runat="server" Text="<%# Bind('ActualRevenue') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="NetActualRevenue">
                        <HeaderTemplate>
                            <asp:Label ID="Label17" runat="server" Text="<%# lbl_NetActualRevenue.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label18" runat="server" Text="<%# Bind('NetActualRevenue') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="OptOutRest">
                        <HeaderTemplate>
                            <asp:Label ID="Label19" runat="server" Text="<%# lbl_OptOutRest.Text %>"></asp:Label> 
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Labe20" runat="server" Text="<%# Bind('OptOutRest') %>"></asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<asp:Label ID="lbl_KeywordReport" runat="server" Text="Keyword Report" Visible="false"></asp:Label>


<asp:Label ID="lbl_Keyword" runat="server" Text="Keyword" Visible="false"></asp:Label>
<asp:Label ID="lbl_Hits" runat="server" Text="Hits" Visible="false"></asp:Label>
 <asp:Label ID="lbl_Exposures" runat="server" Text="Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_Requests" runat="server" Text="Requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_Category" runat="server" Text="Category" Visible="false"></asp:Label>
<asp:Label ID="lbl_OptOutforever" runat="server" Text="Optout forever"  Visible="false"></asp:Label>
<asp:Label ID="lbl_RestOfOptouts" runat="server" Text="Rest of optouts (less)" Visible="false"></asp:Label>
<asp:Label ID="lbl_PotentialRevenue" runat="server" Text="Potential revenue" Visible="false"></asp:Label>
<asp:Label ID="lbl_ActualRevenue" runat="server" Text="Actual Revenue" Visible="false"></asp:Label>
<asp:Label ID="lbl_NetActualRevenue" runat="server" Text="Net Actual Revenue" Visible="false"></asp:Label>
<asp:Label ID="lbl_OptOutRest" runat="server" Text="Opt Out Rest" Visible="false"></asp:Label>
</asp:Content>

