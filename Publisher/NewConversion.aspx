﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="NewConversion.aspx.cs" Inherits="Publisher_NewConversion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register src="~/Publisher/SalesControls/ConversionControl.ascx" tagname="ConversionControl" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<link href="../StyleAutoComplete.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />

<script src="../jquery.min.js" type="text/javascript"></script> 
<script src="../jquery-ui.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>

<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
        RblChargeChange();
    }

    function LoadChart(fileXML, _chart) {

        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render("div_chart");
    }
    function CleanChart() {
        document.getElementById("div_chart").innerHTML = "";
    }
    $(function () {

        $('#<%# ddl_FromHour.ClientID %>').change(function () {
            var _from = this.selectedIndex;
            var _to = document.getElementById('<%# ddl_ToHour.ClientID %>').selectedIndex;
            if (_from > _to) {
                document.getElementById('<%# ddl_ToHour.ClientID %>').selectedIndex = _from;
            }
        });
        $('#<%# ddl_ToHour.ClientID %>').change(function () {
            var _to = this.selectedIndex;
            var _from = document.getElementById('<%# ddl_FromHour.ClientID %>').selectedIndex;
            if (_from > _to) {
                document.getElementById('<%# ddl_FromHour.ClientID %>').selectedIndex = _to;
            }
        });
        RblChargeChange();


    });
    function RblChargeChange() {
        $('#<%# rbl_Chart.ClientID %>').find('input:radio').each(function () {
            $(this).change(function () {
                if (this.checked) {
                    if (this.value == '<%# eConversionChartType.Regular_view.ToString() %>') {
                        $('#<%# btn_UpdateChart.ClientID %>').hide();

                    }
                    else {
                        $('#<%# btn_UpdateChart.ClientID %>').show();

                    }
                }
            });
        });
    }
    
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" >
    
</cc1:ToolkitScriptManager>
<uc1:ToolboxReport ID="_Toolbox" runat="server" />
<div class="page-content minisite-content2"> 
  <div id="form-analytics">
    	       
      <div class="main-inputs">	
               
            <div class="form-field-semi">
                <asp:Label ID="lbl_Heading" runat="server" CssClass="label" Text="Heading"></asp:Label>          
	            <asp:DropDownList ID="ddl_Heading" CssClass="form-select-semi" runat="server" >
               </asp:DropDownList>
            </div>
            <div class="form-field-semi">
                <asp:Label ID="lbl_Country" runat="server" Text="Country" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_Country" CssClass="form-select-semi" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form-field-semi">
                <asp:Label ID="lbl_ControlName" runat="server"  CssClass="label" Text="Flavor"></asp:Label>
                <asp:DropDownList ID="ddl_ControlName" CssClass="form-select-semi" runat="server">
                </asp:DropDownList>
            </div> 
           <div class="form-field-semi">
                <asp:Label ID="lbl_Type" runat="server" Text="Type" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_type" CssClass="form-select-semi" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        
       
        <div class="callreport">
              
             <uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" />
        </div>

        <div class="clear"></div>
         <div class="main-ddl">
            
            <div class="form-field-two-thirds">
                <asp:Label ID="lbl_Origin" runat="server"  CssClass="label" Text="Affiliate"></asp:Label>
                <asp:DropDownList ID="ddl_Origin" CssClass="form-select-two-thirds" runat="server" 
                    AutoPostBack="true" onselectedindexchanged="ddl_Origin_SelectedIndexChanged" >
                </asp:DropDownList>
                         
            </div> 
            <div class="form-field-two-thirds">
                <asp:Label ID="lbl_Interval" runat="server" Text="Interval" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_Interval" CssClass="form-select-two-thirds" runat="server">
                </asp:DropDownList>
            </div> 
            
            <div class="form-field">
                <asp:Label ID="lbl_DayOfWeek" runat="server" Text="Day of week" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_DayOfWeek" CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form-field-semi">
                <asp:Label ID="lbl_FromHour" runat="server" Text="From" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_FromHour" CssClass="form-select-semi" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form-field-semi">
                <asp:Label ID="lbl_ToHour" runat="server" Text="To" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_ToHour" CssClass="form-select-semi" runat="server">
                </asp:DropDownList>
            </div>
                  
        </div>        
       
           <div class="clear"></div> 
            <div class="form-field" runat="server" id="div_affiliate" visible="false">
                <asp:Label ID="lbl_Affiliate" CssClass="label" runat="server" Text="Affiliates"></asp:Label>                                    
                <asp:DropDownList ID="ddl_Affiliate" CssClass="form-select" runat="server"></asp:DropDownList>		
                     
			</div>
              <div class="clear"></div>
            <div class="heading" style="margin:0;">
            <asp:Button ID="btn_Run" runat="server" CssClass="CreateReportSubmit2" Text="Create report" 
                    onclick="btn_Run_Click"  />
            </div>
            
           
            
            <div class="table">
            
                <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                 <div class="resultsMultiple">
                    <asp:Label ID="lbl_RecordMached" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="lblLastUpdate" runat="server"></asp:Label>
                    <asp:Label ID="lblExposureUpdate" runat="server"></asp:Label>
                    <asp:Label ID="lblTotalUsers" runat="server"></asp:Label>
                    <asp:Label ID="lblMonthlyUserValue" runat="server"></asp:Label>
                </div>
                   <div id="div_chart" style="float:left;"></div>
                   <div id="ChartUsersDetails" class="ChartUsersDetails" style="float:left;">
                        <div class="form-field" style="float:none;">
                            <asp:Label ID="lbl_Chart" runat="server"  CssClass="label" Text="Chart display"></asp:Label>
                            <asp:RadioButtonList ID="rbl_Chart" runat="server" RepeatDirection="Vertical">                    
                            </asp:RadioButtonList>
                        </div>
                        <div>
                            <asp:ListBox ID="lb_Origin" runat="server" SelectionMode="Multiple" Rows="15"></asp:ListBox> 
                       </div>
                       <div class="div_btn_UpdateChart">
                            <asp:Button ID="btn_UpdateChart" runat="server" CssClass="CreateReportSubmit2" 
                                Text="Update Chart" onclick="btn_UpdateChart_Click" 
                                     />
                       </div>
                   </div>
                   <div class="clear"></div>
                   
                    <uc1:ConversionControl ID="_conversion" runat="server" />  
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            
            
        </div>
</div>

    
<asp:Label ID="lbl_ConversionReport" runat="server" Text="Conversion Report" Visible="false"></asp:Label>

<asp:Label ID="lbl_RequestsPercent" runat="server" Text="Requests percent" Visible="false"></asp:Label>
 <asp:Label ID="lbl_CallsPercent" runat="server" Text="Calls percent" Visible="false"></asp:Label>


<asp:Label ID="lbl_CreationDate" runat="server" Text="Creation Date from" Visible="false"></asp:Label>
 <asp:Label ID="lbl_RequestUpdate" runat="server" Text="Request update" Visible="false"></asp:Label>
<asp:Label ID="lbl_ExposureUpdate" runat="server" Text="Exposure update" Visible="false"></asp:Label>
<asp:Label ID="lbl_TotalUsers" runat="server" Text="Total DAUS" Visible="false"></asp:Label>    
<asp:Label ID="lbl_MonthlyUserValue" runat="server" Text="Monthly user value today" Visible="false"></asp:Label> 

</asp:Content>




