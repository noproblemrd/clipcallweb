﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="PublisherRefundReport.aspx.cs" Inherits="Publisher_PublisherRefundReport" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script language="javascript" type="text/javascript" src="../Calender/Calender.js"></script>
<link href="style.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>
<link href="../AutoCompleteCity/StyleAutoComplete.css" rel="stylesheet" type="text/css"/>


<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);        
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }   
/*
    function chkExpertise(elm)
    {
        var exper=elm.value;
        if(exper.length==0)
            return true;
         ExpertiserService.CheckExpertise(exper, OnCompleteChkExper, OnErrorEx, OnTimeOutEx);
    }
    function OnCompleteChkExper(arg)
    {
        var elm=document.getElementById("<= txt_MainExpertiser.ClientID %>");
        if(arg=="false")
            elm.value="";            
    }
    function OnTimeOutEx(arg)
    {  
	    alert("timeOut has occured");
    }
    function OnErrorEx(arg)
    {  
//	    alert("Error has occured");
    }
    */
    /*
    function expertisePress(e, elm)
    {
        var code;
        if (!e) var e = window.event;
        if (e.keyCode) code = e.keyCode;
        else if (e.which) code = e.which;        
        if(code==13)
        {
            chkExpertise(elm)
            <# GetReport %>
            return false;
        }
        return true;
    }
    */
    function LoadChart(fileXML, div_id, parentId) {
        var _chart = "<%# GetChartType %>";
        var chart = new FusionCharts(_chart, div_id, '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render(parentId);
    }
    function CleanChart()
    {
        document.getElementById("div_chart").innerHTML = "";
    }


    //ICallbackEventHandler
    function SetCharts() {
        CallServer("", "ee");
    }
    function ReciveServerData(retValue) {
        var Charts_Area = document.getElementById("<%# Charts_Area.ClientID %>");
        var div_container = document.getElementById("div_container");
        var div_chart_load = document.getElementById("div_chart_load");
        if (retValue.length == 0) {
            RemoveAllChildNodes(div_container)
            div_container.appendChild(div_chart_load);
            RemoveAllChildNodes(Charts_Area);
            Charts_Area.innerHTML = "";
            return;
        }

        RemoveAllChildNodes(div_container);
        div_container.appendChild(div_chart_load);
        RemoveAllChildNodes(Charts_Area);
        Charts_Area.innerHTML = "";

        var chart1 = document.createElement('div');
        var _id = "div_chart1";
        chart1.setAttribute("id", _id);
        var _chart = document.createElement('div');
        var _chart_id = "_chart1";
        chart1.appendChild(_chart);
        Charts_Area.appendChild(chart1);

        LoadChart(retValue, _chart_id, _id);


    }
    function SetOnLoadChart() {

        var Charts_Area = document.getElementById("<%# Charts_Area.ClientID %>");
        var div_chart_load = document.getElementById("div_chart_load");
        RemoveAllChildNodes(Charts_Area);
        Charts_Area.appendChild(div_chart_load);
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">
    
</cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  
<div class="page-content minisite-content2">
	<div id="form-analytics">
         
            <div class="form-field">
                    <asp:Label ID="lbl_expertise" CssClass="label" runat="server" Text="Heading"></asp:Label>
                    <asp:DropDownList ID="ddl_Heading" CssClass="form-select" runat="server">
               </asp:DropDownList>
                </div>
                <div class="callreport"><uc1:FromToDate runat="server" ID="FromToDate1" /></div>
            
     </div>
     <div class="clear"></div>
    <div class="table">
        <asp:UpdatePanel ID="_UpdatePanelGraph" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
	                <div id="Charts_Area" runat="server">
    
                    </div>
	            </ContentTemplate>
	            </asp:UpdatePanel>
	    <div id="Table_Report" runat="server">
        	
            <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>    
                    
                    <div class="results1"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
	                <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />                
                        <FooterStyle CssClass="footer"  />
                        <PagerStyle CssClass="pager" />
                        <Columns>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text="<%# Bind('Date') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_SoldCalls.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text="<%# Bind('CallsWon') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_RefundRequests.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text="<%# Bind('AmountOfRefundRequests') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_RefundRequestsPercent.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text="<%# Bind('PercentRefundRequestsOfCalls') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label9" runat="server" Text="<%# lbl_ApprovedRequests.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label10" runat="server" Text="<%# Bind('AmountOfApprovedRequests') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label11" runat="server" Text="<%# lbl_ApprovedRefundsCalls.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label12" runat="server" Text="<%# Bind('PercentApprovedFromCalls') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label13" runat="server" Text="<%# lbl_ApprovedRefundsRequests.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label14" runat="server" Text="<%# Bind('PercentApprovedFromRequests') %>"></asp:Label>
                            
                        </ItemTemplate>
                        </asp:TemplateField>
                            
                        </Columns>
                    </asp:GridView>
                    <uc1:TablePaging ID="TablePaging1" runat="server" />
                </ContentTemplate>
                </asp:UpdatePanel>
        </div>
    </div>
</div> 

<div id="div_container" style="display:none;">
<div id="div_chart_load"  class="divLoaderChart">
    <table   >
        <tr>
          <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." style="background-color: #FFFFFF;" /></td>
        </tr>
    </table>
</div>
</div>

<asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_SoldCalls" runat="server" Text="Sold calls" Visible="false"></asp:Label>
<asp:Label ID="lbl_RefundRequests" runat="server" Text="Refund requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_RefundRequestsPercent" runat="server" Text="% refund requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_ApprovedRequests" runat="server" Text="Approved requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_ApprovedRefundsCalls" runat="server" Text="% Approved refunds from calls" Visible="false"></asp:Label>
<asp:Label ID="lbl_ApprovedRefundsRequests" runat="server" Text="% Approved refunds from requests" Visible="false"></asp:Label>

<asp:Label ID="lbl_refundReport" runat="server" Text="Refund report" Visible="false"></asp:Label>

</asp:Content>

