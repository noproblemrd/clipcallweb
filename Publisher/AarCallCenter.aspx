﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AarCallCenter.aspx.cs" Inherits="Publisher_AarCallCenter" %>

<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function _CreateExcel() {
            var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"AarCallCenterReport"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
        function OpenCaseTicket(_path) {
            /*
           document.getElementById("<# panel_iframe.ClientID %>").className = "popModal_del2_ie";
           document.getElementById("<# _iframe.ClientID %>").src = _path;
           HideIframeDiv();  
           $find('_modal').show();
            */
            var _leadWin = window.open(_path, "LeadDetails", "width=1000, height=650");
            _leadWin.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="Date-field" runat="server" id="div_Date">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  			
		</div>
        <div style="position:absolute; top:0px; left: 332px;">
            <ul>
              <li>P1 – Pro installed app but did not activate Credit Card</li>               
              <li>P2 – Pro clicked 'get the app' but the app was not installed</li>
                <!--
                    <li>P2 – Pro requested lead,connected by phone but the app was not installed</li>
              <li>P3 – Pro requested lead but no call was made app was not installed</li>
                     -->
              <li>P4 – Pro only watched the video, and nothing further app was not installed</li>
              <li>P5 – Pro only visited our landing page, but did not watch video app was not installed</li>
            </ul>            
        </div>
		<div style="clear:both;"></div>
        <div class="form-field" runat="server" id="div_Step">
            <asp:Label ID="lbl_AarStpe" runat="server" Text="Step" CssClass="label"></asp:Label>
            <asp:DropDownList ID="ddl_AarStpe" CssClass="form-select" runat="server" ></asp:DropDownList>
        </div>
        <div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>        
        <div id="Table_Report"  runat="server" >  
				<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="Date">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="Date"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblBroker" runat="server" Text="<%# Bind('Date') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="CaseNumber">
						<HeaderTemplate>
							<asp:Label ID="Label101" runat="server" Text="Case number"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <a id="a_Case" runat="server" href="<%# Bind('CaseScript') %>">
			                    <asp:Label ID="Label112" runat="server" Text="<%# Bind('CaseNumber') %>"></asp:Label>
						    </a>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Category">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="Category"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label11" runat="server" Text="<%# Bind('Category') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="VideoLink">
						<HeaderTemplate>
							<asp:Label ID="Label12" runat="server" Text="Video link"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl="<%# Bind('VideoLink') %>">Video link</asp:HyperLink>							
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Address">
						<HeaderTemplate>
							<asp:Label ID="Label14" runat="server" Text="Address"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label15" runat="server" Text="<%# Bind('Address') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Name">
						<HeaderTemplate>
							<asp:Label ID="Label16" runat="server" Text="Name"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label17" runat="server" Text="<%# Bind('Name') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="Priority">
						<HeaderTemplate>
							<asp:Label ID="Label18" runat="server" Text="Priority"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label19" runat="server" Text="<%# Bind('Priority') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="Phone">
						<HeaderTemplate>
							<asp:Label ID="Label20" runat="server" Text="Phone"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label21" runat="server" Text="<%# Bind('Phone') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="WatchedVideo">
						<HeaderTemplate>
							<asp:Label ID="Label22" runat="server" Text="Watched Video"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label23" runat="server" Text="<%# Bind('WatchedVideo') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>
                     
                    <asp:TemplateField SortExpression="YelpUrl">
						<HeaderTemplate>
							<asp:Label ID="Label72" runat="server" Text="Yelp url"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:HyperLink ID="HyperLink71" runat="server" Target="_blank" NavigateUrl="<%# Bind('YelpUrl') %>" Visible="<%# Bind('HasYelpUrl') %>">Yelp url</asp:HyperLink>							
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="YelpRating">
						<HeaderTemplate>
							<asp:Label ID="Label73" runat="server" Text="Yelp Rating"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label74" runat="server" Text="<%# Bind('YelpRating') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="GoogleUrl">
						<HeaderTemplate>
							<asp:Label ID="Label75" runat="server" Text="Google url"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:HyperLink ID="HyperLink72" runat="server" Target="_blank" NavigateUrl="<%# Bind('GoogleUrl') %>" Visible="<%# Bind('HasGoogleUrl') %>">Google url</asp:HyperLink>							
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="GoogleRating">
						<HeaderTemplate>
							<asp:Label ID="Label76" runat="server" Text="Google Rating"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label77" runat="server" Text="<%# Bind('GoogleRating') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>    
				</asp:GridView>
			</div>
        </div>
    </div>
    <asp:Label ID="lbl_AarCallCenter" runat="server" Text="AAR call center Report" Visible="false"></asp:Label>
</asp:Content>

