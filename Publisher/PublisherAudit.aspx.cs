using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web.Services.Protocols;
using System.Data.SqlClient;

public partial class Publisher_PublisherAudit : PageSetting
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        if (!IsPostBack)
        {
            string csnameStep = "setStep";
            Type cstypeStep = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager csStep = this.Page.ClientScript;
            string csTextStep = "";
            if (!userManagement.IsPublisher() || string.IsNullOrEmpty(GetGuidSetting()))
            {

                
                Response.Redirect("LogOut.aspx");
                   
                return;
            }
            if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
            {
                csTextStep = "witchAlreadyStep(8);";
                csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
            }

            LoadPages();

            MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + siteSetting.GetStyle));

     //       LoadLogs();
        }
    }

    

    private void LoadPages()
    {
        string command = "EXEC [dbo].[GetPagesAudit] @SiteLangId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteLangId", siteSetting.siteLangId);
            SqlDataReader reader = cmd.ExecuteReader();
            ddl_pages.Items.Add(new ListItem("-- " + lbl_all.Text + " --", "0"));
            while (reader.Read())
            {
                string desc = reader.IsDBNull(4) ? "" : (string)reader["Translate"];
                string name = (string)reader["PageName"];
                if (!string.IsNullOrEmpty(desc))
                {
                    ddl_pages.Items.Add(new ListItem(desc, name));
                }
            }
            conn.Close();
        }
    }

    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        DateTime from_date = FromToDate1.GetDateFrom;
        DateTime to_date = FromToDate1.GetDateTo;
        if (from_date == DateTime.MinValue || to_date == DateTime.MinValue)
        {
            from_date = DateTime.Now.AddMonths(-1);
            to_date = DateTime.Now;
        }
        string pageId = ddl_pages.SelectedValue;
        /*
        DataTable data = new DataTable();
        data.Columns.Add("AuditStatus");
        data.Columns.Add("CreatedOn");
        data.Columns.Add("FieldName");
        data.Columns.Add("NewValue");
        data.Columns.Add("OldValue");
        data.Columns.Add("UserName");
        data.Columns.Add("PageId");
         * */
 //       WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this); 
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.AuditReportRequest arr = new WebReferenceReports.AuditReportRequest();
        arr.FromDate = from_date;
        arr.ToDate = to_date;
        arr.PageId = (pageId == "0") ? string.Empty : pageId;
        arr.SupplierId = new Guid(GetGuidSetting());
        WebReferenceReports.ResultOfAuditReportResponse result = new WebReferenceReports.ResultOfAuditReportResponse();
        try
        {
            result = _report.AuditReport(arr); 
        }
        catch (SoapException ex)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }


        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        DataTable data = GetDataTable.GetDataTableColumsString(result.Value.AuditEntries);

        decimal resultNewValue;
        decimal resultOldValue;
       

        foreach (WebReferenceReports.AuditEntry ae in result.Value.AuditEntries)
        {
            DataRow row = data.NewRow();
            row["AuditStatus"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "AuditStatus", ae.AuditStatus.ToString());
            row["CreatedOn"] = string.Format("{0:d/M/yyyy HH:mm}", ae.CreatedOn);
            row["FieldName"] = ae.FieldName;
            row["AdvertiseId"] = ae.AdvertiseId.ToString();
            row["AdvertiserName"] = ae.AdvertiserName;
            row["FieldId"] = ae.FieldId;

            if (ae.NewValue != null && ae.NewValue.Contains(@".") && decimal.TryParse(ae.NewValue, out resultNewValue))
                row["NewValue"] = String.Format("{0:0}", resultNewValue);
            else
                row["NewValue"] = ae.NewValue;

            if (ae.OldValue != null && ae.OldValue.Contains(@".") && decimal.TryParse(ae.OldValue, out resultOldValue))            
                row["OldValue"] = String.Format("{0:0}", resultOldValue); 
            else
                 row["OldValue"]=ae.OldValue;

            row["PageId"] = ae.PageId;
            row["UserId"] = ae.UserId.ToString();
            row["UserName"] = ae.UserName;
            row["PageName"] = ae.PageName;
            data.Rows.Add(row);
        }
        /*
        XmlNodeList xnl = xdd["Logs"].ChildNodes;
        foreach (XmlNode node in xnl)
        {
            DataRow row = data.NewRow();
     //       row["Modified"] = node[""].InnerText;
            row["FieldName"] = node["FieldName"].InnerText;
            row["PreviousValue"] = node["PreviousValue"].InnerText;
            row["CurrentValue"] = node["CurrentValue"].InnerText;
            row["UserName"] = node["UserName"].InnerText;
            data.Rows.Add(row);
        }
         * */
        
        gvLog.DataSource = data;
        gvLog.DataBind();
        dataV = data;
        if (data.Rows.Count == 0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noResults", "alert('" + lbl_noResults.Text + "');", true);
      //  SetRowStyle(gvLog);
    }
    /*
    void SetRowStyle(GridView _gv)
    {
        int i = 0;
        foreach (GridViewRow _row in gvLog.Rows)
        {
            if (i++ % 2 == 0)
                _row.Attributes.Add("class", "odd");
            else
                _row.Attributes.Add("class", "even");
        }
    }
   */
    protected void gvLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataTable data = dataV;
        gvLog.DataSource = data;
        gvLog.PageIndex = e.NewPageIndex;
        gvLog.DataBind();
     //   SetRowStyle(gvLog);
        _UpdatePanel.Update();
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    
    
}
