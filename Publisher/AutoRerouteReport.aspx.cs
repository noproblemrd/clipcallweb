﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_AutoRerouteReport : PageSetting
{
    const int DAY_INTERVAL = 2;
    const string FILE_NAME = "ReroutReport.xml";
    protected void Page_Load(object sender, EventArgs e)
    {
        SetToolboxEvents();
        if (!IsPostBack)
        {
            setDateTime();
            
            ExecReport(DateTime.Now.AddDays(-1*DAY_INTERVAL), DateTime.Now);
        }
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_RerouterReport.Text);
 //       string _script = "_CreateExcel();";
  //      ToolboxReport1.SetExcelJavascript(_script);
        ToolboxReport1.RemovePrintButton();
        ToolboxReport1.RemoveExcelButton();
    }
    protected void btn_run_Click(object sender, EventArgs e)
    {
        ExecReport(FromToDatePicker_current.GetDateFrom, FromToDatePicker_current.GetDateTo);
    }
    void ExecReport(DateTime _from, DateTime _to)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfAutoRerouteReportResponse result = null;
        try
        {
            result = _report.AutoRerouteReport(_from, _to);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        txt_TotalServiceProviders.Text = result.Value.NumberOfSuppliersInThisService.ToString();
        txt_NumberOfRerouteds.Text = result.Value.NumberOfReroutes.ToString();
        txt_ReroutedsNotAnswered.Text = result.Value.NumberOfReroutedCallsThatWereNotAnswered.ToString();
        txt_ReroutedsAnswered.Text = result.Value.NumberOfReroutedCallsThatWereAnswered.ToString();
        txt_PercentageOfAnsweredCallsFromReroutedCalls.Text = string.Format(NUMBER_FORMAT, result.Value.PercentageOfAnsweredCallsFromReroutedCalls) + "%";

        /*
        result.Value.NumberOfReroutedCallsThatWereAnswered;
        result.Value.NumberOfReroutedCallsThatWereNotAnswered;
        result.Value.NumberOfReroutes;
        result.Value.NumberOfSuppliersInThisService;
        result.Value.PercentageOfAnsweredCallsFromReroutedCalls;
         * */
        
    }
    private void setDateTime()
    {
        FromToDatePicker_current.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
            DAY_INTERVAL);
    }
    protected string GetDivCurrentId
    {
        get { return FromToDatePicker_current.GetParentDivId(); }
    }
    protected string GetInvalidDate
    {
        get { return Server.HtmlEncode(lbl_DateError.Text); }
    }
}