﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="PaymentReportV3.aspx.cs" Inherits="Publisher_PaymentReportV3" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
      <style type="text/css">
        tr.future td
        {
            background-color: #d9ffb3;
        }
        tr.unbooked td
        {
            background-color: #ff9999;
        }

    </style>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
   
     <script type="text/javascript">
         window.onload = appl_init;

         function appl_init() {
             var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
             pgRegMgr.add_beginRequest(BeginHandler);
             pgRegMgr.add_endRequest(EndHandler);

         }
         function BeginHandler() {
             //     alert('in1');
             showDiv();
         }
         function EndHandler() {
             //      alert('out1');
             hideDiv();
         }
         function OpenIframe(_path) {
             var _leadWin = window.open(_path, "LeadDetails", "width=1000, height=650");
             _leadWin.focus();
         }
         function _CreateExcel() {
             var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"<%# EXCEL_NAME %>"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcelUrl %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
         }
         function OpenDetails(incidentAccountId, _path) {
             var _leadWin = window.open(_path, incidentAccountId, "width=1250, height=650");
             _leadWin.focus();
        //     return false;
         }
         function OpenChat(id) {
             var _url = '<%# GetChatUrl %>' + id;
             var chatWin = window.open(_url, "chatWin" + id, "resizable=0,width=900,height=900,scrollbars=1");
             return false;
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
          
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	        
               <div class="main-inputs" runat="server" id="Div1">		
	                
                        
            			 <div class="form-field">
                            <asp:Label ID="lbl_IncludeQaTest" runat="server" Text="Include QA" CssClass="label"></asp:Label>
                           <asp:CheckBox ID="cb_IncludeQaTest" runat="server" />
                        </div>
                        <div class="form-field">
                          <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Request Id</label>
			              <asp:TextBox ID="txtRequestId" CssClass="form-text" runat="server" ></asp:TextBox>										
                          <asp:CompareValidator runat="server" Type="Integer"  Operator="DataTypeCheck" ControlToValidate="txtRequestId" Display="Dynamic">
                             <asp:Label ID="lbl_RequestIdError" runat="server" Text="Must be number" Visible="true"></asp:Label>
                          </asp:CompareValidator> 
                       </div>      
                   </div>
             <div class="clear"></div>
                            
                    <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>   
                </div>

            <div style="clear:both;"></div>
    	
	        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	            <ContentTemplate>	
                    <div class="results"><asp:Label ID="lbl_totalAmount" runat="server"></asp:Label></div>
                    <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
	                <div >
                              
                                <asp:Repeater runat="server" ID="_reapeter" OnItemDataBound="_reapeter_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="data-table">
                                        
                                            <thead>
                                            <tr>                               
                                                <th><asp:Label ID="Label51" runat="server" Text="Case number"></asp:Label></th>                 
                                                <th><asp:Label ID="lbl_Date" runat="server" Text="Booked Date"></asp:Label></th>			                                    
			                                    <th><asp:Label ID="Label001" runat="server" Text="Case Date"></asp:Label></th>	
                                                <th><asp:Label ID="Label53" runat="server" Text="Category"></asp:Label></th>
                                                <th><asp:Label ID="Label5" runat="server" Text="Supplier Name" ></asp:Label></th>			                                    
			                                    <th><asp:Label ID="Label6" runat="server" Text="Supplier Phone" ></asp:Label></th>
                                                <th><asp:Label ID="Label17" runat="server" Text="Supplier Status" ></asp:Label></th>
                                                <th><asp:Label ID="Label5001" runat="server" Text="Customer Name"></asp:Label></th>
                                                <th><asp:Label ID="Label14" runat="server" Text="Customer Email"></asp:Label></th>
                                                <th><asp:Label ID="Label5003" runat="server" Text="Customer Phone"></asp:Label></th>             
                                                <th><asp:Label ID="Label1112" runat="server" Text="Is invited"></asp:Label></th>  
                                                <th><asp:Label ID="Label15" runat="server" Text="Payment Plan"></asp:Label></th>

                                                 <th><asp:Label ID="Label52" runat="server" Text="Amount"></asp:Label></th>	
                                                <th><asp:Label ID="Label2" runat="server" Text="Customer Bonus"></asp:Label></th>                                              
                                                <th><asp:Label ID="Label19" runat="server" Text="Plan Discount"></asp:Label></th>
                                                <th><asp:Label ID="Label3" runat="server" Text="ClipCall Fee"></asp:Label></th>
                                                <th><asp:Label ID="Label21" runat="server" Text="Charged"></asp:Label></th>
                                                <th><asp:Label ID="Label23" runat="server" Text="Past Installments"></asp:Label></th>
                                                <th><asp:Label ID="Label22" runat="server" Text="Balance"></asp:Label></th>
                                                <th><asp:Label ID="Label4" runat="server" Text="Transfered At"></asp:Label></th>
                                                <th><asp:Label ID="Label7" runat="server" Text="Transfered"></asp:Label></th>
                                                <th><asp:Label ID="Label8" runat="server" Text="StripeAccountId"></asp:Label></th>
                                                <th><asp:Label ID="Label13" runat="server" Text="Chat trace"></asp:Label></th>
			                                </tr>
                                            </thead>
			                            </HeaderTemplate>
	                                    <ItemTemplate>
                                            <tr id="tr_show" runat="server">
                                                <td>                                                    
                                                     <a id="a_Case" runat="server" href='<%# Bind("CaseScript") %>'>
			                                            <asp:Label ID="Label500" runat="server" Text='<%# Bind("CaseNumber") %>'></asp:Label>
						                              </a>			                                        
                                                     <asp:HiddenField ID="hf_incidentId" runat="server" Value='<%# Bind("IncidentId") %>' />    
                                                    <asp:HiddenField ID="hf_incidentAccountId" runat="server" Value='<%# Bind("IncidentAccountId") %>' />    
			                                     </td>
			                                     <td>
			                                        <asp:Label ID="lbl_name" runat="server" Text='<%# Bind("BookedDate") %>'></asp:Label>                                                   
                                                     <asp:HiddenField ID="h_QuoteId" runat="server" Value='<%# Bind("QuoteId") %>' />
                                                     <input id="h_isOpen" class="h_isOpen" type="hidden" />
			                                     </td>       			                         
			                                     <td>
			                                        <asp:Label ID="Label501" runat="server" Text='<%# Bind("CaseDate") %>'></asp:Label>      
			                                     </td>
                                                 <td>
			                                        <asp:Label ID="Label503" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
			                                     </td>
                                                <td>
                                                    <asp:HyperLink ID="HyperLink5" runat="server" Text='<%# Bind("SupplierName") %>' Target="_blank" NavigateUrl='<%# Bind("SupplierRequestReport") %>'></asp:HyperLink>
			                                     </td>			                                     
			                                     <td>
                                                     <asp:HyperLink ID="HyperLink6" runat="server" Text='<%# Bind("SupplierPhone") %>' Target="_blank" NavigateUrl='<%# Bind("SupplierRequestReport") %>'></asp:HyperLink>
			                                     </td>
                                                 <td>                                                		                                     
                                                   <asp:Label ID="Label18" runat="server" Text='<%# Bind("SupplierStatus") %>'></asp:Label>
			                                     </td>
                                                <td>
                                                    <asp:HyperLink ID="HyperLink3" runat="server" Text='<%# Bind("CustomerName") %>' Target="_blank" NavigateUrl='<%# Bind("ConsumerRequestReport") %>'></asp:HyperLink>
			                                     </td>
                                                <td>
                                                    <asp:HyperLink ID="HyperLink2" runat="server" Text='<%# Bind("CustomerEmail") %>' Target="_blank" NavigateUrl='<%# Bind("ConsumerRequestReport") %>'></asp:HyperLink>
			                                     </td>
                                                <td>
                                                    <asp:HyperLink ID="HyperLink4" runat="server" Text='<%# Bind("CustomerPhone") %>' Target="_blank" NavigateUrl='<%# Bind("ConsumerRequestReport") %>'></asp:HyperLink>
			                                     </td>
                                                 <td >
                                                   <asp:Label ID="Label509" runat="server" Text='<%# Bind("IsInvited") %>'></asp:Label>
			                                     </td>  
                                                <td >
                                                   <asp:Label ID="Label16" runat="server" Text='<%# Bind("PaymentPlan") %>'></asp:Label>
			                                     </td>   

			                                     <td>
			                                        <asp:Label ID="Label502" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>      
			                                     </td>	                                     
			                                     <td>
                                                   <asp:Label ID="Label506" runat="server"  Text='<%# Bind("CustomerBonus") %>'></asp:Label>
                                                </td>
                                                 <td>
                                                   <asp:Label ID="Label20" runat="server"  Text='<%# Bind("PlanDiscount") %>'></asp:Label>
                                                </td>
                                                <td >
                                                   <asp:Label ID="Label10" runat="server" Text='<%# Bind("ClipCallFee") %>'></asp:Label>
			                                     </td>   
                                                <td >
                                                   <asp:Label ID="Label24" runat="server" Text='<%# Bind("Charged") %>'></asp:Label>
			                                     </td> 
                                                <td >
                                                   <asp:Label ID="Label25" runat="server" Text='<%# Bind("PastInstallments") %>'></asp:Label>
			                                     </td> 
                                                <td >
                                                   <asp:Label ID="Label26" runat="server" Text='<%# Bind("Balance") %>'></asp:Label>
			                                     </td> 
                                                <td >
                                                   <asp:Label ID="Label11" runat="server" Text='<%# Bind("TransferedAt") %>'></asp:Label>
			                                     </td>   
                                                <td >
                                                   <asp:Label ID="Label12" runat="server" Text='<%# Bind("AmountTransfered") %>'></asp:Label>
			                                     </td>   
                                                 <td >
                                                    <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" Text='<%# Bind("StripeAccountId") %>'
                                                         NavigateUrl='<%# Bind("StripeAccountUrl") %>'>

                                                    </asp:HyperLink>
			                                     </td>  
                                                <td>
                                                    <asp:LinkButton ID="lb_record" runat="server" OnClientClick='<%# Bind("OpenChat") %>'  Text="Chat trace"></asp:LinkButton>
                                                </td>
                                                 
                                            </tr>
                                            
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>                
                                        </FooterTemplate>      
                                    </asp:Repeater>
                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
        </div>
</asp:Content>

