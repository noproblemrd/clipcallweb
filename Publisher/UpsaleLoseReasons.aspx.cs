﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml.Linq;

public partial class Publisher_UpsaleLoseReasons : PageSetting
{
    const string ExcelName = "UpsaleLoseReasons";
    const WebReferenceSite.eTableType _eTableType = WebReferenceSite.eTableType.UpsaleLoseReason;
    protected void Page_Load(object sender, EventArgs e)
    {
        SetStatusNameId1.SetStatusType(ExcelName, _eTableType, lbl_TitleName.Text);

    }
    protected override void OnLoadComplete(EventArgs e)
    {
        SetStatusNameId1._OnLoadComplete();
        base.OnLoadComplete(e);
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    
}