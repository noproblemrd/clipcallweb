﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SupplierMoneyReport.aspx.cs" Inherits="Publisher_SupplierMoneyReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script type="text/javascript">

    window.onload = appl_init;

    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }   

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">    
</cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  
 
<div class="page-content minisite-content2">
    
	<div id="form-analytics">	
		<div class="form-fieldlargeppc">
            <asp:Label ID="lbl_SupplierNumber" CssClass="label" runat="server" Text="Supplier number"></asp:Label>
            <asp:TextBox ID="txt_SupplierNumber" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator_SupplierNumber" 
                runat="server" ErrorMessage="Only digits available"
             CssClass="error-msg" ControlToValidate="txt_SupplierNumber" 
                ValidationExpression="\d+$"></asp:RegularExpressionValidator>
        </div>
        <div class="clear"></div>
	    <div class="heading"><asp:Button ID="btnSubmit" CssClass="CreateReportSubmit2" runat="server" Text="Run report" OnClick="btnSubmit_Click" /></div>
    </div>
    <div class="clear"></div>
    
    <div id="list" class="table">
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
            
            <div class="results3"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
            
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" CssClass="data-table">
                
                    <Columns>                    
                        <asp:TemplateField SortExpression="SupplierNumber">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_SupplierNumber.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbl_SupplierNum" runat="server" Text="<%# Bind('SupplierNumber') %>"></asp:Label>
                            <asp:Label ID="lbl_SupplierId" runat="server" Text="<%# Bind('SupplierId') %>" Visible="false"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="SupplierName">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_SupplierName.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="DepositReal">
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_DepositReal.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text="<%# Bind('DepositReal') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="DepositBonus">
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_DepositBonus.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text="<%# Bind('DepositBonus') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="UsedReal">
                        <HeaderTemplate>
                            <asp:Label ID="Label17" runat="server" Text="<%# lbl_UsedReal.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label18" runat="server" Text="<%# Bind('UsedReal') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="UsedBonus">
                        <HeaderTemplate>
                            <asp:Label ID="Label9" runat="server" Text="<%# lbl_UsedBonus.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label10" runat="server" Text="<%# Bind('UsedBonus') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="LeftReal">
                        <HeaderTemplate>
                            <asp:Label ID="Label11" runat="server" Text="<%# lbl_LeftReal.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label12" runat="server" Text="<%# Bind('LeftReal') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="LeftBonus">
                        <HeaderTemplate>
                            <asp:Label ID="Label13" runat="server" Text="<%# lbl_LeftBonus.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label14" runat="server" Text="<%# Bind('LeftBonus') %>"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
            </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
	</div>	
</div>

<asp:Label ID="lbl_SupplierName" runat="server" Text="Supplier name" Visible="false"></asp:Label>
<asp:Label ID="lbl_DepositReal" runat="server" Text="Deposit real money" Visible="false"></asp:Label>
<asp:Label ID="lbl_DepositBonus" runat="server" Text="Deposit bonus money" Visible="false"></asp:Label>
<asp:Label ID="lbl_UsedReal" runat="server" Text="Used in real money" Visible="false"></asp:Label>
<asp:Label ID="lbl_UsedBonus" runat="server" Text="Used in bonus money" Visible="false"></asp:Label>
<asp:Label ID="lbl_LeftReal" runat="server" Text="Real money left" Visible="false"></asp:Label>
<asp:Label ID="lbl_LeftBonus" runat="server" Text="Bonus money left" Visible="false"></asp:Label>


<asp:Label ID="lblTitlePage" runat="server" Text="Deposits Vs. Consumption" Visible="false"></asp:Label>
<asp:Label ID="lbl_SupplierNotFound" runat="server" Text="Supplier not found" Visible="false"></asp:Label>


</asp:Content>

