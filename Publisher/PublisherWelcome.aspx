<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" 
AutoEventWireup="true" CodeFile="PublisherWelcome.aspx.cs" Inherits="Publisher_PublisherWelcome" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link type="text/css" rel="stylesheet" href="PublishStyle.css" />
   <script type="text/javascript" src="../general.js"></script>
   
   <link media="screen" rel="stylesheet" href="../jquery/colorbox.css" />
<script src="../jquery/jquery.min.js" type="text/javascript" language="javascript"></script>
<script src="../jquery/jquery.colorbox.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript">
    var message;
    function init()
    {        
        message=document.getElementById('<%#Hidden_IfToExite.ClientID%>').value;        
    }
    
    window.onload=init;



</script>	   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
   <div class="page-content consumer minisite-content">
							<h2>
                                <asp:Label ID="lblWelcome" runat="server" >Welcome!</asp:Label>						
							</h2>
							<div id="form-analytics" >
								<div class="main-inputs clearfix">
									<label for="form-segment"
									 runat="server" id="lblWelDesc">Thank you for choosing our unique advertising</label>
							    </div>
							</div>

</div>
<asp:HiddenField ID="Hidden_IfToExite" runat="server" Value="Are you sure you want to exit? Unsaved data will be lost!" />

</asp:Content>

