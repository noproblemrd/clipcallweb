﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_player : System.Web.UI.Page
{
    protected string audioUrl { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        GenerateVideoLink gvl = GenerateVideoLink.GetVideoLinkPrams(Request.QueryString);

        if (string.IsNullOrEmpty(gvl.GetVideoUrl))
        {
            return;
        }
        audioUrl = gvl.GetVideoUrl;
    }
}