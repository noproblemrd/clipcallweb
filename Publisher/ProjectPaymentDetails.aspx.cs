﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Publisher_ProjectPaymentDetails : PublisherPage
{
    const string INVOICE_LINK = @"~/clipcall/Receipt.aspx?prid={0}";
    ClipCallReport.PaymentReportDetailsResponseV4[] result;
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(lb_unbook);

        if (!IsPostBack)
        {
            string incident_account_id = Request.QueryString["ia"];
            Guid incidentAccountId;
            if (!Guid.TryParse(incident_account_id, out incidentAccountId))
            {
                ClosePopupByError();
                return;
            }
            IncidentAccountIdV = incidentAccountId;

            //     this.DataBind();
            LoadDetails(incidentAccountId);
        }
        LiteralControl lc = Utilities.GetDivLoader();
        Page.Controls.Add(lc);
        Header.DataBind();
    }
    void LoadDetails(Guid incidentAccountId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfPaymentReportMainDetailsResponseV4 _result = null;
        try
        {
            _result = report.GetPaymentReportDetailsV4(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClosePopupByError();
            return;
        }
        if (_result.Type == ClipCallReport.eResultType.Failure)
        {
            ClosePopupByError();
            return;
        }
        lbl_ProjectId.Text = _result.Value.ProjectNumber;
        a_projectNumber.HRef = "javascript:OpenIframe('MobileAppRequestTicket.aspx?incidentid=" + _result.Value.IncidentId + "');";
        div_unbook.Visible = _result.Value.CanCreateRefund;
        if (_result.Value.CanCreateRefund)
            lb_unbook.OnClientClick = "return createUnbook('" + incidentAccountId + "');";

        result = _result.Value.Details;

        var query = from x in result
                    where x.RequestPaymentCreatedOn == DateTime.MinValue && x.AmountTransfered == 0m
                    select x;
        div_RequestPayment.Visible = query.Count() > 0;
        if(div_RequestPayment.Visible)
            lb_RequestPayment.OnClientClick = "return RequestpaymentAll('" + incidentAccountId + "');";


        DataTable dt = new DataTable();
        dt.Columns.Add("QuoteDate");
        dt.Columns.Add("QuoteId");
        dt.Columns.Add("PaidDate");
        dt.Columns.Add("PaymentPlan");
        dt.Columns.Add("Amount");
        dt.Columns.Add("CustomerBonus");
        dt.Columns.Add("PlanDiscount");
        dt.Columns.Add("ClipCallFee");
        dt.Columns.Add("Charged");
        dt.Columns.Add("PastInstallments");
        dt.Columns.Add("Balance");
        dt.Columns.Add("StripeAccountId");
        dt.Columns.Add("StripeAccountUrl");
        dt.Columns.Add("TransferredAt");
        dt.Columns.Add("TransferredAmount");
        dt.Columns.Add("HasStripeAccount", typeof(bool));
        dt.Columns.Add("CanCreateRefund", typeof(bool));
        dt.Columns.Add("ClickCreateRefund");

        dt.Columns.Add("CanCreateTransfer", typeof(bool));
        dt.Columns.Add("ClickCreateTransfer");

        dt.Columns.Add("MaxValue");
 //       dt.Columns.Add("ValidationGroup");
        dt.Columns.Add("ClickCreatePartialTransfer");
        dt.Columns.Add("CanCreatePartialTransfer", typeof(bool));
        dt.Columns.Add("IsRefunded", typeof(bool));

        dt.Columns.Add("RequestPaymentStatus");
        dt.Columns.Add("RequestPaymentStatusReason");
        dt.Columns.Add("RequestPaymentCreatedOn");

        dt.Columns.Add("CanCreateRequestPayment", typeof(bool));
        dt.Columns.Add("CreateRequestPayment");
        //     int _index = 0;
        foreach (ClipCallReport.PaymentReportDetailsResponseV4 data in result)
        {
 //           _index++;
            DataRow row = dt.NewRow();
            row["QuoteDate"] = string.Format(siteSetting.DateTimeFormat, data.QuoteDate);
            row["QuoteId"] = data.QuoteId;
            row["PaidDate"] = string.Format(siteSetting.DateTimeFormat, data.PaidDate);
            row["PaymentPlan"] = data.PaymentPlan;
            row["TransferredAt"] = data.TransferedAt == default(DateTime) ? string.Empty : string.Format(siteSetting.DateTimeFormat, data.TransferedAt);
            row["Amount"] = "$" + string.Format(NUMBER_FORMAT, data.Amount);
            row["CustomerBonus"] = "$" + string.Format(NUMBER_FORMAT, data.CustomerBonus);
            row["PlanDiscount"] = "$" + string.Format(NUMBER_FORMAT, data.PlanDiscount);
            row["ClipCallFee"] = "$" + string.Format(NUMBER_FORMAT, data.ClipCallFee);
            row["Charged"] = "$" + string.Format(NUMBER_FORMAT, data.Charged);
            row["PastInstallments"] = data.PastInstallments;
            row["Balance"] = "$" + string.Format(NUMBER_FORMAT, data.Balance);
            row["TransferredAmount"] = "$" + string.Format(NUMBER_FORMAT, data.AmountTransfered);
            row["StripeAccountId"] = data.StripeAccountId;
            row["StripeAccountUrl"] = PpcSite.GetCurrent().StripeBaseUrl + CONNECT_ACCOUNT_URL + data.StripeAccountId;
            row["CanCreateTransfer"] = data.CanCreateTransfer;
            row["CanCreateRefund"] = data.CanCreateRefund;
            row["IsRefunded"] = data.QuoteStatus == "UnBook";
            row["HasStripeAccount"] = !string.IsNullOrEmpty(data.StripeAccountId);

            row["RequestPaymentStatus"] = data.RequestPaymentStatus;
            row["RequestPaymentStatusReason"] = data.RequestPaymentStatusReason;
            row["RequestPaymentCreatedOn"] = data.RequestPaymentCreatedOn == DateTime.MinValue ? null : string.Format(siteSetting.DateTimeFormat, data.RequestPaymentCreatedOn);
            if (data.CanCreateTransfer)
            {
                row["ClickCreateTransfer"] = "return createTransfer(this, '" + data.QuoteId + "', '" + string.Format("$" + NUMBER_FORMAT, data.FutureTransfered) + "');";
                if(data.IsTransferV2)
                {
                    row["CanCreatePartialTransfer"] = true;
                    row["MaxValue"] = (int)data.FutureTransfered;
 //                   row["ValidationGroup"] = "ValidationGroup" + _index;
                    //(_btn, quoteId, amount, totalOptional, amountDisplay)
                    row["ClickCreatePartialTransfer"] = "return createPartialTransfer(this, '" + data.QuoteId + "', " + data.FutureTransfered + ");";
                }
                else
                {
                    row["CanCreatePartialTransfer"] = false;
                }
            }
            else
            {
                row["CanCreatePartialTransfer"] = false;
            }
            if (data.CanCreateRefund)
                row["ClickCreateRefund"] = "return createRefundQuote(this, '" + data.QuoteId + "', " + data.QuoteNumber + ");";

            if (data.RequestPaymentCreatedOn == DateTime.MinValue && data.AmountTransfered == 0m)
            {
                row["CanCreateRequestPayment"] = true;
                row["CreateRequestPayment"] = "return Requestpayment(this, '" + incidentAccountId + "', '" + data.QuoteId + "');";
            }
            else
                row["CanCreateRequestPayment"] = false;

            dt.Rows.Add(row);
        }
        _reapeter.DataSource = dt;
        _reapeter.DataBind();
       
    }
    private ClipCallReport.PaymentTraceResponse[] GetPaymentTrace(Guid quoteId)
    {
        var query = from x in result
                    where x.QuoteId == quoteId
                    select x.PaymentTraces;
        return query.FirstOrDefault();
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.AlternatingItem && e.Item.ItemType != ListItemType.Item)
            return;
        HtmlTableRow row = (HtmlTableRow)(e.Item.FindControl("tr_show"));

        string css_class = (e.Item.ItemIndex % 2 == 0) ? "odd" : "even";

        row.Attributes.Add("class", css_class);

        Guid quoteId;
        quoteId = Guid.Parse(((HiddenField)e.Item.FindControl("h_QuoteId")).Value);
        ClipCallReport.PaymentTraceResponse[] paymentTrace = GetPaymentTrace(quoteId);
        GridView gv_supplier = (GridView)e.Item.FindControl("gv_supplier");
        GridView gv_customer = (GridView)e.Item.FindControl("gv_customer");
        GridView gv_deal = (GridView)e.Item.FindControl("gv_deal");
        LoadData(paymentTrace, gv_supplier, gv_customer, gv_deal);
    }
    /*** Payment Trace  ****/
    private DataTable GetNewTable()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Amount");
        dt.Columns.Add("OperationType");
        dt.Columns.Add("IsDone");
        dt.Columns.Add("ChargeLink");
        dt.Columns.Add("HasChargeLink", typeof(bool));
        return dt;
    }
    private DataTable GetNewTableCustomer()
    {
        DataTable dt = GetNewTable();
        dt.Columns.Add("InvoiceLinkLink");
        dt.Columns.Add("HasInvoiceLink", typeof(bool));
        return dt;
    }
    private DataRow _setDataRow(DataTable dt, ClipCallReport.PaymentTraceResponse ptr)
    {
        DataRow row = dt.NewRow();
        row["Amount"] = string.Format(NUMBER_FORMAT, ptr.Amount);
        row["OperationType"] = ptr.OperationType;
        row["IsDone"] = ptr.IsDone ? "Done" : string.Empty;
        if (!string.IsNullOrEmpty(ptr.StripeLink))
        {
            row["HasChargeLink"] = true;
            row["ChargeLink"] = PpcSite.GetCurrent().StripeBaseUrl + ptr.StripeLink;
        }
        else
            row["HasChargeLink"] = false;
        return row;
    }
    private void SetDataRow(DataTable dt, ClipCallReport.PaymentTraceResponse ptr)
    {
        DataRow row = _setDataRow(dt, ptr);

        dt.Rows.Add(row);
    }
    private void SetDataRowCustomer(DataTable dt, ClipCallReport.PaymentTraceResponse ptr)
    {
        DataRow row = _setDataRow(dt, ptr);
        row["HasInvoiceLink"] = ptr.PaymentResponseId != Guid.Empty;
        row["InvoiceLinkLink"] = (ptr.PaymentResponseId != Guid.Empty) ? string.Format(INVOICE_LINK, ptr.PaymentResponseId) : null;
        dt.Rows.Add(row);
    }
    public void LoadData(ClipCallReport.PaymentTraceResponse[] rows, GridView gv_supplier, GridView gv_customer, GridView gv_deal)
    {
        DataTable dataSupplier = GetNewTable();

        DataTable dataCustomer = GetNewTableCustomer();

        DataTable dataDeal = GetNewTable();


        foreach (ClipCallReport.PaymentTraceResponse ptr in rows)
        {
            if (ptr.Part == ClipCallReport.ePartResponse.DEAL)
            {
                SetDataRow(dataDeal, ptr);
            }
            else if (ptr.Part == ClipCallReport.ePartResponse.SUPPLIER)
            {
                SetDataRow(dataSupplier, ptr);
            }
            else
            {
                SetDataRowCustomer(dataCustomer, ptr);
            }
        }
        gv_supplier.DataSource = dataSupplier;
        gv_supplier.DataBind();
        gv_customer.DataSource = dataCustomer;
        gv_customer.DataBind();
        gv_deal.DataSource = dataDeal;
        gv_deal.DataBind();

    }
    /*** Payment Trace  ****/
    void ClosePopupByError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseError", "CloseMeError();", true);
    }
    protected string CreateTransferUrl
    {
        get
        {
            return ResolveUrl("~/Publisher/ProjectPaymentDetails.aspx/CreateTransfer");
        }
    }
    protected string CreatePartialTransferUrl
    {
        get
        {
            return ResolveUrl("~/Publisher/ProjectPaymentDetails.aspx/CreatePartialTransfer");
        }
    }
    protected Guid IncidentAccountIdV
    {
        get
        {
            return ViewState["IncidentAccountIdV"] == null ? default(Guid) : (Guid)ViewState["IncidentAccountIdV"];
        }
        set
        {
            ViewState["IncidentAccountIdV"] = value;
        }
    }
    protected string UnbookIncidentUrl
    {
        get
        {
            return ResolveUrl("~/Publisher/ProjectPaymentDetails.aspx/UnbookIncident");
        }
    }
    protected string RefundQuoteUrl
    {
        get
        {
            return ResolveUrl("~/Publisher/ProjectPaymentDetails.aspx/RefundQuote");
        }
    }
    [WebMethod(MessageName = "CreateTransfer")]
    public static string CreateTransfer(Guid quoteId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = ccreport.CreateTransfer(quoteId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return false.ToString().ToLower();
        }
        return result.Value.ToString().ToLower();
    }
    [WebMethod(MessageName = "CreatePartialTransfer")]
    public static string CreatePartialTransfer(Guid quoteId, decimal amount)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = ccreport.CreatePartialTransfer(quoteId, amount);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return false.ToString().ToLower();
        }
        return result.Value.ToString().ToLower();
    }
    [WebMethod(MessageName = "UnbookIncident")]
    public static string UnbookIncident(Guid incidentAccountId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = ccreport.UnbookIncidentByIncidentAccount(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return false.ToString().ToLower();
        }
        return result.Value.ToString().ToLower();
    }
    [WebMethod(MessageName = "RefundQuote")]
    public static string RefundQuote(Guid quoteId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = ccreport.RefundQuote(quoteId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return false.ToString().ToLower();
        }
        return result.Value.ToString().ToLower();
    }
    protected string RequestpaymentAllWebMethod
    {
        get
        {
            return ResolveUrl("~/Publisher/ProjectPaymentDetails.aspx/RequestPaymentAllMethod");
        }
    }
    [WebMethod(MessageName = "RequestPaymentAllMethod")]
    public static string RequestPaymentAllMethod(Guid incidentAccountId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = ccreport.RequestPaymentAll(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return false.ToString().ToLower();
        }
        return result.Value.ToString().ToLower();
    }

    protected string RequestpaymentWebMethod
    {
        get
        {
            return ResolveUrl("~/Publisher/ProjectPaymentDetails.aspx/RequestPaymentMethod");
        }
    }
    [WebMethod(MessageName = "RequestPaymentMethod")]
    public static string RequestPaymentMethod(Guid incidentAccountId, Guid quoteId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = ccreport.RequestPayment(incidentAccountId, quoteId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return false.ToString().ToLower();
        }
        return result.Value.ToString().ToLower();
    }


}