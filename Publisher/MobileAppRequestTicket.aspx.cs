﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_MobileAppRequestTicket : PublisherPage, IPostBackEventHandler, IOnLoadControl
{
    const string NOT_SUPPLIED = "Not supplied";
    const string DATE_TIME_FORMAT = "{0:MM/dd/yyyy HH:mm}";
    const string AFTER_AUCTION = "AfterAuction";
    const string CONFIGURATION_KEY = "MaxRegionLevel";
    const string V_ICON = "images/icon-v.png";
    const string X_ICON = "images/icon-x.png";
    const string PathMobileAppRequestTicket = "MobileAppRequestTicket.aspx?incidentid=";
    const string JS_VOID = "javascript:void(0);";
 //   protected Guid RequestIdForUpsale { get; set; }
    private string full_name;
    private string email;
    protected void Page_Load(object sender, EventArgs e)
    {

    //    ScriptManager1.RegisterAsyncPostBackControl(ddl_Billing);
   //     ScriptManager1.RegisterAsyncPostBackControl(btn_Set);
        ScriptManager1.RegisterAsyncPostBackControl(lb_RelaunchStopLog);
        ScriptManager1.RegisterAsyncPostBackControl(lb_StopRequest);
        Notes1.SetNoteTab += new EventHandler(Notes1_SetNoteTab);

        if (!IsPostBack)
        {
            //           if (!userManagement.IsPublisher())
            //               Response.Redirect("LogOut.aspx");
            string incident_id = Request.QueryString["incidentid"];
            Guid incidentId;
            if (!Guid.TryParse(incident_id, out incidentId))
            {
                ClosePopupByError();
                return;
            }
            IncidentIdV = incidentId;
           
       //     this.DataBind();
            LoadDetails(incidentId);
        }
        Header.DataBind();
    }   

    void Notes1_SetNoteTab(object sender, EventArgs e)
    {
        SetNoteTab();
    }
    /*
    protected void _Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item)
            return;
        HyperLink hl = (HyperLink)e.Item.FindControl("HL_Request");
        if (hl.NavigateUrl == string.Empty)
            hl.CssClass = "_blunk";
    }
    */
    private void LoadDetails(Guid incidentId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfIncidentData _data = null;
        try
        {
            _data = report.GetClipCallIncidentData(incidentId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClosePopupByError();
            return;
        }
        if (_data.Type == ClipCallReport.eResultType.Failure)
        {
            ClosePopupByError();
            return;
        }

        ClipCallReport.IncidentData data = _data.Value;
        //     _Repeater.HeaderTemplate.
        div_relaunch.Visible = data.CanRelaunch;
        
        div_Stop.Visible = data.CanBeStopped;
      
        lbl_RequestNum.Text = data.TicketNumber;
        txt_Title.Text = data.Title;
        txt_Heading.Text = data.CategoryName;
        txt_Region.Text = data.RegionName;
        hl_VideoUrl.Text = data.VideoUrl;
        GenerateVideoLink gvl = new GenerateVideoLink(data.VideoUrl, data.PicPreviewUrl);
        hl_VideoUrl.NavigateUrl = gvl.GenerateFullUrl();
        txt_CreatedOn.Text = data.CreatedOn.GetDateForClient().ToString();// string.Format(DATE_TIME_FORMAT, data.CreatedOn);
        txt_Consumer.Text = string.IsNullOrEmpty(data.CustomerPhone) ? NOT_SUPPLIED : data.CustomerPhone;
        txt_RelaunchDate.Text = data.RelaunchDate.GetDateForClient().ToString();// string.Format(DATE_TIME_FORMAT, data.RelaunchDate);
        txt_ProvidedAdvertisers.Text = data.ProvidedAdvertisers.ToString();
        txt_RequestedAdveritsers.Text = data.RequestedAdvertisers.ToString();
        txt_IncidentStatus.Text = data.IncidentStatus == ClipCallReport.IncidentStatus.NO_AVAILABLE_SUPPLIER && data.Status == ClipCallReport.LeadStatus.Inactive ? "Project was cancelled" : data.IncidentStatus.ToString();
        txt_ReviewStatus.Text = data.ReviewStatus.ToString();
        txt_incidentType.Text = data.caseTypeStatus.ToString();
        txt_fullAddres.Text = data.FullAddress;
        txt_CensoredAddress.Text = data.CensoredAddress;
        full_name = data.CustomerName;
        email = data.CustomerEmail;
        a_projectNumber.HRef = "javascript:OpenIframe('ClipCallLeadConnections.aspx?incidentid=" + incidentId + "');";
        /*
        if (data.IsStoppedManually)
        {
            div_Stop.Visible = false;
            div_requestStpped.Visible = true;
        }
        else if (data.CanBeStopped)
        {
            div_Stop.Visible = true;
            div_requestStpped.Visible = false;
        }
        else
        {
            div_Stop.Visible = false;
            div_requestStpped.Visible = false;
        }
         * */
        DataTable dt_relaunch = new DataTable();

        dt_relaunch.Columns.Add("Date");
        dt_relaunch.Columns.Add("IsRelaunch");
        dt_relaunch.Columns.Add("By");
        dt_relaunch.Columns.Add("OldRegion");
        dt_relaunch.Columns.Add("NewRegion");
        dt_relaunch.Columns.Add("OldCategory");
        dt_relaunch.Columns.Add("NewCategory");

        foreach(ClipCallReport.RelaunchStopData rsd in data.RelaunchStopDataList)
        {
            DataRow row = dt_relaunch.NewRow();
            row["Date"] = rsd.Date.GetDateForClient();// string.Format(DATE_TIME_FORMAT, rsd.Date);
            row["IsRelaunch"] = rsd.IsRelaunch ? "Relaunch" : "Stop";
            row["By"] = rsd.By.ToString();
            row["OldRegion"] = rsd.OldRegion;
            row["NewRegion"] = rsd.NewRegion;
            row["OldCategory"] = rsd.OldCategory;
            row["NewCategory"] = rsd.NewCategory;
            dt_relaunch.Rows.Add(row);
        }
        gv_RelaunchHistory.DataSource = dt_relaunch;
        gv_RelaunchHistory.DataBind();
        /*
         public string AdvertiserName { get; set; }
        public string AdvertiserPhone { get; set; }
        public NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status Status { get; set; }
        public DateTime Date { get; set; }
        public string CallRecord { get; set; }
        public int CallRecordDuration { get; set; }
        public bool HasMobileApp {
         * */
        DataTable data_table = new DataTable();
        data_table.Columns.Add("AdvertiserName");
        data_table.Columns.Add("LastActive");
        data_table.Columns.Add("AdvertiserPhone");
        data_table.Columns.Add("SupplierRequestReport");
        data_table.Columns.Add("IsConfirmed", typeof(bool));
        data_table.Columns.Add("Status");
        data_table.Columns.Add("RejectedReason");
        data_table.Columns.Add("CallRecord");
        data_table.Columns.Add("CallRecordDuration");
        data_table.Columns.Add("Date");
        data_table.Columns.Add("HasMobileApp");
        data_table.Columns.Add("Won");
        data_table.Columns.Add("HasRecord", typeof(bool));
        data_table.Columns.Add("IsWon", typeof(bool));
        data_table.Columns.Add("IsConfirmedThisLead", typeof(bool));
        data_table.Columns.Add("ConfirmedThisLead");
        data_table.Columns.Add("CanSendSms", typeof(bool));
        data_table.Columns.Add("SendSms");
        data_table.Columns.Add("SendSmsText");
        data_table.Columns.Add("Proximity");
        data_table.Columns.Add("WatchedVideo");
        data_table.Columns.Add("ProRating");
        data_table.Columns.Add("AdvertiserStatus");
        data_table.Columns.Add("InactiveReason");

        data_table.Columns.Add("UnblockScript");
        data_table.Columns.Add("CanUnblock", typeof(bool));
        data_table.Columns.Add("IncidentAccountId");

        data_table.Columns.Add("HasSurvey");
        data_table.Columns.Add("SurveyUrl");

        data_table.Columns.Add("CanRelaunchReject", typeof(bool));
        data_table.Columns.Add("RelaunchReject");

        data_table.Columns.Add("ImageRemindMe");
        data_table.Columns.Add("RemindMeFunc");

        foreach (ClipCallReport.IncidentAccountData iad in data.Calls)
        {
            DataRow row = data_table.NewRow();
            row["IncidentAccountId"] = iad.IncidentAccountId;
            row["AdvertiserName"] = iad.AdvertiserName;
            row["AdvertiserPhone"] = iad.AdvertiserPhone;
            row["SupplierRequestReport"] = string.Format(Utilities.SUPPLIER_REQUEST_REPORT_URL, iad.SupplierId);
            row["AdvertiserStatus"]=iad.AdvertiserStatus.ToString();
            row["InactiveReason"] = iad.InactiveReason.HasValue ? Utilities.GetEnumStringFormat(iad.InactiveReason.Value) : null;
            row["IsConfirmed"] = iad.IsConfirmedPolicy;
            row["Status"] = iad.Status.ToString();
            row["RejectedReason"] = iad.RejectedReason.HasValue ? Utilities.GetEnumStringFormat(iad.RejectedReason.Value) : null;
   //         row["CallRecord"] = iad.CallRecord;
            row["CallRecordDuration"] = iad.CallRecordDuration.ToString();
            row["Date"] = iad.Date == DateTime.MinValue ? string.Empty : iad.Date.GetDateForClient().ToString();//string.Format(DATE_TIME_FORMAT, iad.Date);
            row["LastActive"] = iad.LastActive == DateTime.MinValue ? string.Empty : iad.LastActive.GetDateForClient().ToString(); //string.Format(DATE_TIME_FORMAT, iad.LastActive);
            row["HasMobileApp"] = iad.HasMobileApp ? "Yes" : "No";
            if (iad.Status == ClipCallReport.IncidentAccountStatus.CLOSE)
            {
                row["Won"] = V_ICON;
                row["IsWon"] = true;
            }
            else
                row["IsWon"] = false;
            string _path = iad.CallRecord;
            if (!string.IsNullOrEmpty(_path) && userManagement.CanListenToRecords)
            {
                row["HasRecord"] = true;               
                string RecEnc = EncryptString.EncodeRecordPath(_path);
                row["CallRecord"] = "javascript:OpenRecord('" + RecEnc + "');";
            }
            else
                row["HasRecord"] = false;
            if (iad.IsConfirmedPolicyInThisLead.HasValue)
            {
                row["IsConfirmedThisLead"] = true;
                row["ConfirmedThisLead"] = iad.IsConfirmedPolicyInThisLead.Value ? V_ICON : X_ICON;
            }
            else
                row["IsConfirmedThisLead"] = false;
            if (iad.Status == ClipCallReport.IncidentAccountStatus.MATCH && iad.HasMobileApp && iad.AdvertiserStatus != ClipCallReport.SupplierStatus.Inactive)
            {
                row["CanSendSms"] = true;
                if (iad.BlockSendSms)
                {
                    string BlockSendSmsSource = iad.BlockSendSmsSource.HasValue ? Utilities.GetEnumStringFormat(iad.BlockSendSmsSource.Value) : null;
                    row["SendSms"] = "return false;";
                    row["SendSmsText"] = "Block number" + (string.IsNullOrEmpty(BlockSendSmsSource) ? string.Empty : " / " + BlockSendSmsSource);
                }
                else
                {
                    row["SendSms"] = "return send_sms(this, '" + iad.IncidentAccountId.ToString() + "');";
                    row["SendSmsText"] = iad.SentSmsCount > 0 ?
                        string.Format("Sent {0} times", iad.SentSmsCount) : "Send";
                }
            }
            else
                row["CanSendSms"] = false;
            row["Proximity"] = iad.ProximityDate == default(DateTime) ? string.Empty : string.Format(DATE_TIME_FORMAT, iad.ProximityDate);
            row["WatchedVideo"] = iad.WatchedVideo != DateTime.MinValue ? V_ICON : X_ICON;
            row["ProRating"] = iad.AccountRating < 0 ? "not supplied" : iad.AccountRating.ToString();

            if (iad.BlockSendSms && iad.Status == ClipCallReport.IncidentAccountStatus.MATCH)
            {
                row["CanUnblock"] = true;
                row["UnblockScript"] = string.Format("return unblock(this, '{0}');", iad.SupplierId);
            }
            else
            {
                row["CanUnblock"] = false;
                row["UnblockScript"] = "return false;";
            }

            row["HasSurvey"] = iad.HasSurvey ? "yes" : "no";
            if(iad.HasSurvey)
                row["SurveyUrl"] = string.Format(Utilities.SUPPLIER_SURVEY_REPORT_URL, iad.SupplierId);

            if (iad.Status == ClipCallReport.IncidentAccountStatus.REJECT)
            {
                int c = (from x in data.Calls
                            where x.SupplierId == iad.SupplierId && x.IncidentAccounCreatedOn > iad.IncidentAccounCreatedOn
                         select x).Count();
                if (c > 0)
                    row["CanRelaunchReject"] = false;
                else
                {
                    row["CanRelaunchReject"] = true;
                    row["RelaunchReject"] = "return relaunch_reject_lead(this, '" + iad.IncidentAccountId.ToString() + "');";
                }
            }
            else
                row["CanRelaunchReject"] = false;

            row["ImageRemindMe"] = iad.RemindMeLater ? V_ICON : X_ICON;
            row["RemindMeFunc"] = "return remind_me_later(this, '" + iad.IncidentAccountId.ToString() + "');";

            data_table.Rows.Add(row);

        }
        if (data.CanRelaunch)
        {

            string _script = _relaunchControl.GetLoadDetailsScript(data.CustomerPhone, incidentId.ToString(), data.CategoryId.ToString(), data.RegionName,
                data.RegionLevel, string.Empty);
            _script += "return false;";
            lb_relaunch.OnClientClick = _script;
   //         UpsaleIdV = data.UpsaleId;
        }
        SetAarIncidentTable(data.AarCalls, data.IsLiveIncident, incidentId);
        //    RecordListV = records;
  //      PayReasonIdV = data.PayReasonId;
  //      PayStatusIdV = data.PayStatusId;
        // Load affiliate tab
    //    LoadAffiliateTab(data.PayStatusId, data.PayReasonId);

        _GridView.DataSource = data_table;
        _GridView.DataBind();

    }
    protected void SetAarIncidentTable(ClipCallReport.AarIncidentData[] aarIncidentList, bool isAliveIncident, Guid incidentId)
    {
        /*
         *  public string AdvertiserName { get; set; }
        public string AdvertiserPhone { get; set; }
        public string CallStatus { get; set; }
        public DateTime Date { get; set; }        
        public string Device { get; set; }
        public bool WatchedVideo { get; set; }
        public int ViewVideoCount { get; set; }
        public bool ViewAllVideo { get; set; }
        public string CallRecord { get; set; }
        public int CallDuration { get; set; }
        public string TextMessage { get; set; }
        public string CallBackMessage {
         */
        DataTable data_table = new DataTable();
        data_table.Columns.Add("AdvertiserName");
        data_table.Columns.Add("AdvertiserPhone");
        
        data_table.Columns.Add("CallStatus");
        data_table.Columns.Add("Date");
        data_table.Columns.Add("Device");
        data_table.Columns.Add("WatchedVideo");
        data_table.Columns.Add("ViewVideoCount");
        data_table.Columns.Add("ViewAllVideo");
        data_table.Columns.Add("CallRecord");
        data_table.Columns.Add("CallDuration");
        data_table.Columns.Add("CallBackMessage");
        data_table.Columns.Add("HasRecoed", typeof(bool));
        data_table.Columns.Add("SendAar");
        data_table.Columns.Add("CanSendAar", typeof(bool));
        data_table.Columns.Add("SendAarText");

        



        foreach (ClipCallReport.AarIncidentData data in aarIncidentList)
        {
            DataRow row = data_table.NewRow();
            row["AdvertiserName"] = data.AdvertiserName;
            row["AdvertiserPhone"] = data.AdvertiserPhone;
            row["CallStatus"] = data.CallStatus;
            row["Date"] = data.Date.GetDateForClient();//string.Format(DATE_TIME_FORMAT, data.Date);
            row["Device"] = data.Device;
            row["WatchedVideo"] = data.WatchedVideo ? V_ICON : X_ICON;
            row["ViewVideoCount"] = data.ViewVideoCount;
            row["ViewAllVideo"] = data.ViewAllVideo ? V_ICON : X_ICON;
            row["HasRecoed"] = string.IsNullOrEmpty(data.CallRecord);
            row["CallRecord"] = data.CallRecord;
            row["CallDuration"] = data.CallDuration;
            row["CallBackMessage"] = data.CallBackMessage;
            
            if (isAliveIncident)
            {
                row["CanSendAar"] = true;
                if (data.BlockNumber)
                {
                    row["SendAar"] = "return false;";
                    row["SendAarText"] = "Block number";
                }
                else
                {
                    row["SendAar"] = "return send_aar(this, '" + incidentId + "','" + data.AarCallId + "','" + data.AarIncidentId + "');";
                    row["SendAarText"] = data.SentSmsCount == 0 ? "Send" : string.Format("Sent {0} times", data.SentSmsCount);
                }
            }
            else
                row["CanSendAar"] = false;
            data_table.Rows.Add(row);
        }
        gv_aar.DataSource = data_table;
        gv_aar.DataBind();
    }
    protected void lb_StopRequest_Click(object sender, EventArgs e)
    {
        ClipCallReport.ClipCallReport ccr = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfCloseIncidentResponse result = null;
        Guid incidentId = IncidentIdV;
        try
        {
            result = ccr.CloseClipCallIncident(incidentId);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure || !result.Value.IsSuccess)
            Update_Faild();
        else
        {
            Response.Redirect(GetMyPath);
        }
    }
    string GetMyPath
    {
        get { return ResolveUrl("~/Publisher/" + PathMobileAppRequestTicket + IncidentIdV); }
    }
    /*
    void LoadAffiliateTab(Guid PayStatusId, Guid PayReasonId)
    {
        ddl_Billing.SelectedIndex = -1;
        ddl_Status.Items.Clear();
        for (int i = 0; i < ddl_Billing.Items.Count; i++)
        {
            if (ddl_Billing.Items[i].Value == PayStatusId.ToString())
            {
                ddl_Billing.SelectedIndex = i;
                break;
            }
        }
        Dictionary<Guid, Dictionary<Guid, string>> dic = DicAffiliateV;
        foreach (KeyValuePair<Guid, string> kvp in dic[PayStatusId])
        {
            ListItem li = new ListItem(kvp.Value, kvp.Key.ToString());
            li.Selected = (kvp.Key == PayReasonId);
            ddl_Status.Items.Add(li);
        }
    }
     */
    protected void lb_OpenNotes_Click(object sender, EventArgs e)
    {
        SetNoteTab();
        Notes1.LoadData(IncidentIdV, WebReferenceSite.NoteType.Case);
    }
    protected void lb_OpenZillow_Click(object sender, EventArgs e)
    {
        SetZillowTab();
        _zillow.LoadData(IncidentIdV);
        hf_zillowloaded.Value = "1";
        //Notes1.LoadData(IncidentIdV, WebReferenceSite.NoteType.Case);
    }
    /*
    protected void lb_RelaunchStopLog_click(object sender, EventArgs e)
    {
        LoadAffiliateTab(PayStatusIdV, PayReasonIdV);
        _UpdatePanelaffiliate.Update();
    }
     * */
    void SetNoteTab()
    {
        div_Notes.Style[HtmlTextWriterStyle.Display] = "block";
        div_Details.Style[HtmlTextWriterStyle.Display] = "none";
        div_zillow.Style[HtmlTextWriterStyle.Display] = "none";
        a_OpenDetails.Attributes.Remove("class");
        lb_zillow.CssClass = "";
  //      lb_zillow.Attributes.Remove("class");
        lb_OpenNotes.CssClass = "selected";
    }
    void SetZillowTab()
    {
        div_Notes.Style[HtmlTextWriterStyle.Display] = "none";
        div_Details.Style[HtmlTextWriterStyle.Display] = "none";
        div_zillow.Style[HtmlTextWriterStyle.Display] = "block";
        a_OpenDetails.Attributes.Remove("class");
 //       lb_OpenNotes.Attributes.Remove("class");
        lb_OpenNotes.CssClass = "";
        lb_zillow.CssClass = "selected";
    }
    void ClosePopupByError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseError", "CloseMeError();", true);
    }

    void SetDetailsTab()
    {
        div_Notes.Style[HtmlTextWriterStyle.Display] = "none";
        div_Details.Style[HtmlTextWriterStyle.Display] = "block";
        a_OpenDetails.Attributes["class"] = "selected";
        lb_OpenNotes.CssClass = "";
    }
    
    
    Guid IncidentIdV
    {
        get { return (ViewState["IncidentIdV"] == null) ? Guid.Empty : (Guid)ViewState["IncidentIdV"]; }
        set {
            hf_IncidentId.Value = value.ToString();
            ViewState["IncidentIdV"] = value;
        }
        /*
        get { return (string.IsNullOrEmpty(hf_IncidentId.Value)) ? Guid.Empty : new Guid(hf_IncidentId.Value); }
        set { hf_IncidentId.Value = value.ToString(); }
        */
    }
    /*
    Guid UpsaleIdV
    {
        get { return (string.IsNullOrEmpty(hf_UpsaleId.Value)) ? Guid.Empty : new Guid(hf_UpsaleId.Value); }
        set { hf_UpsaleId.Value = value.ToString(); }
    }
     * */
    protected string GetNoResulScript
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text); }
    }


    #region IPostBackEventHandler Members
    public string GetPostBackEventAfterAuction
    {
        get
        {
            PostBackOptions pbo = new PostBackOptions(this, AFTER_AUCTION, string.Empty, false, true, false, true, true, "");
            return Page.ClientScript.GetPostBackEventReference(pbo);
        }
    }
    /*
    public void RaisePostBackEvent(string eventArgument)
    {

        if (eventArgument == AFTER_AUCTION)
        {
            div_upsale.Visible = false;
        }

    }
    */
    #endregion
    /*
    Dictionary<Guid, Dictionary<Guid, string>> DicAffiliateV
    {
        get { return (Session["AffiliatePayment"] == null) ? new Dictionary<Guid, Dictionary<Guid, string>>() : (Dictionary<Guid, Dictionary<Guid, string>>)Session["AffiliatePayment"]; }
        set { Session["AffiliatePayment"] = value; }
    }
    */
    //  data.PayStatusId, data.PayReasonId
    /*
    Guid PayStatusIdV
    {
        get { return (ViewState["PayStatusId"] == null) ? Guid.Empty : (Guid)ViewState["PayStatusId"]; }
        set { ViewState["PayStatusId"] = value; }
    }
    Guid PayReasonIdV
    {
        get { return (ViewState["PayReasonId"] == null) ? Guid.Empty : (Guid)ViewState["PayReasonId"]; }
        set { ViewState["PayReasonId"] = value; }
    }
     * */
    protected string AreUSure
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_AreUSure.Text); }
    }

    #region IOnLoadControl Members

    public void __OnLoad(UserControl ControlSender)
    {
        if (ControlSender is Publisher_ClipCallControls_RelaunchControl)
        {
            _relaunchControl.LoadDetails(IncidentIdV, full_name, email, GetMyPath);
        }
    }

    #endregion
    /*
    Dictionary<int, string> RecordListV
    {
        get { return (Session["RecordList"] == null) ? new Dictionary<int, string>() : (Dictionary<int, string>)Session["RecordList"]; }
        set { Session["RecordList"] = value; }
    }
     * */
   
    protected string RecordService
    {
        get { return ResolveUrl("~/RecordService.asmx/GetPath"); }
    }
    protected string GetAudioHTML5
    {
        get { return ResolveUrl("~") + "audio/AudioHtml5.aspx"; }
    }
    protected string GetMediaPlayer
    {
        get { return ResolveUrl("~") + "audio/MediaPlayer.aspx"; }
    }
    protected string GetDownloadVideo
    {
        get { return ResolveUrl("~") + "audio/DownloadVideo.ashx"; }
    }
    protected string Get_VIcon_Url
    {
        get { return V_ICON; }
    }
    public void RaisePostBackEvent(string eventArgument)
    {
        throw new NotImplementedException();
    }
    protected string SendAarAgainService
    {
        get { return ResolveUrl("~/Publisher/MobileAppRequestTicket.aspx/SendAarAgain"); }
    }
    protected string SendSmsReminderService
    {
        get { return ResolveUrl("~/Publisher/MobileAppRequestTicket.aspx/SendSmsReminder"); }
    }
    protected string SendUnblockSmsService
    {
        get { return ResolveUrl("~/Publisher/MobileAppRequestTicket.aspx/unblocksms"); }
    }
    protected string UpdateRemindMeService
    {
        get { return ResolveUrl("~/Publisher/MobileAppRequestTicket.aspx/updateremindme"); }
    }
    [WebMethod(MessageName = "updateremindme")]
    public static string UpdateRemindMe(Guid incidentAccountId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfUpdateRemindMeResponse result = null;

        try
        {
            result = ccreport.UpdateIncidentAccountRemindMe(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            RemindMeResponse response = new RemindMeResponse
            {
                isSucceeded = false
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(response);
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            RemindMeResponse response = new RemindMeResponse
            {
                isSucceeded = false
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(response);
        }
        RemindMeResponse _response = new RemindMeResponse();
        _response.isSucceeded = result.Value.isSucceeded;
        if (result.Value.isSucceeded)
            _response.image = result.Value.CurrentRemindMe ? V_ICON : X_ICON;
        return Newtonsoft.Json.JsonConvert.SerializeObject(_response);
    }
    class RemindMeResponse
    {
        public bool isSucceeded { get; set; }
        public string image { get; set; }
    }
    [WebMethod(MessageName = "unblocksms")]
    public static string UnblockSms(Guid supplierId)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = ccreport.UnblockSmsSupplier(supplierId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ClipCallReport.GeneralResponseWR _response = new ClipCallReport.GeneralResponseWR()
            {
                FailedReason = exc.Message,
                IsSucceeded = false
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(_response);
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            ClipCallReport.GeneralResponseWR _response = new ClipCallReport.GeneralResponseWR()
            {
                FailedReason = "Server Failure",
                IsSucceeded = false
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(_response);
        }
        ClipCallReport.GeneralResponseWR _resp = new ClipCallReport.GeneralResponseWR()
        {
            FailedReason = result.Value ? "" : "Server Failure",
            IsSucceeded = result.Value
        };
        return Newtonsoft.Json.JsonConvert.SerializeObject(_resp);
    }
    [WebMethod( MessageName="SendSmsReminder")]
    public static string SendSmsReminder(Guid incidentAccountId)
    {
        PpcSite ppcSite = PpcSite.GetCurrent();
        ClipCallReport.ClipCallReport ccr = WebServiceConfig.GetClipCallReportReference(ppcSite.UrlWebReference);
        
        ClipCallReport.ResultOfBoolean result = null;

        try
        {
            result = ccr.SendReminderSmsNewProject(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
            return false.ToString().ToLower();
        return result.Value.ToString().ToLower();
    }
    [WebMethod( MessageName="GetSuplliers")]
    public static string GetSuplliers(Guid incidentId, Guid expertiseId, string regionName, int regionLevel)
    {
        //       Guid _incidentId = new Guid(incidentId);
        //       Guid _expertiseId = new Guid(expertiseId);
        PpcSite ppcSite = PpcSite.GetCurrent();
        ClipCallReport.ClipCallReport ccr = WebServiceConfig.GetClipCallReportReference(ppcSite.UrlWebReference);
        ClipCallReport.ResultOfListOfRelaunchSearchSuppliersResponse result = null;
        ClipCallReport.RelaunchSearchSuppliersRequest _request = new ClipCallReport.RelaunchSearchSuppliersRequest();
        _request.CategoryId = expertiseId;
        _request.IncidentId = incidentId;
        _request.RegionName = regionName;
        _request.RegionLevel = regionLevel;
        try
        {
            result = ccr.SearchClipCallSuppliers(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return string.Empty;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
            return string.Empty;
        DataTable data = new DataTable();
        data.Columns.Add("SupplierName");
        data.Columns.Add("SupplierId");
        data.Columns.Add("SupplierUrl");
        data.Columns.Add("ExtensionNumber");
        data.Columns.Add("DirectNumber");
        data.Columns.Add("HasExtension", typeof(bool));
        foreach (ClipCallReport.RelaunchSearchSuppliersResponse rssr in result.Value)
        {
            DataRow row = data.NewRow();
            string SupplierName = rssr.AccountName;
            if (SupplierName.Length > 50)
                SupplierName = SupplierName.Substring(0, 46) + "...";
            row["SupplierId"] = rssr.AccountId.ToString();
            row["SupplierName"] = SupplierName;
            //row["ExtensionNumber"] = ExtensionNumber;
            row["DirectNumber"] = rssr.Phone;
            row["HasExtension"] = false;
            data.Rows.Add(row);
        }
        Page page = new Page();

        UserControl ctl =
             (UserControl)page.LoadControl(@"~/Publisher/Auction/UpsaleSearch.ascx");

        System.Reflection.MethodInfo m_load = ctl.GetType().GetMethod("LoadDataMin");
        m_load.Invoke(ctl, new object[] { data });
        ctl.EnableViewState = false;

        StringBuilder sb = new StringBuilder();

        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }

        return sb.ToString();
    }
    [WebMethod(MessageName = "SendAarAgain")]
    public static string SendAarAgain(Guid incidentId, Guid aarCallId, Guid aarIncidentId)
    {
        PpcSite ppcSite = PpcSite.GetCurrent();
        ClipCallReport.ClipCallReport ccr = WebServiceConfig.GetClipCallReportReference(ppcSite.UrlWebReference);
        ClipCallReport.SendTextMessageAgainRequest request = new ClipCallReport.SendTextMessageAgainRequest();
        request.AarCallId = aarCallId;
        request.AarIncidentId = aarIncidentId;
        request.IncidentId = incidentId;
        ClipCallReport.ResultOfBoolean result = null;

        try
        {
            result = ccr.SendAarTextMessageAgain(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
            return false.ToString().ToLower();
        return result.Value.ToString().ToLower();
    }
    protected string RelaunchRejectLeadervice
    {
        get { return ResolveUrl("~/Publisher/MobileAppRequestTicket.aspx/RelaunchRejectLead"); }
    }
    [WebMethod(MessageName = "RelaunchRejectLead")]
    public static string RelaunchRejectLead(Guid incidentAccountId)
    {
        PpcSite ppcSite = PpcSite.GetCurrent();
        ClipCallReport.ClipCallReport ccr = WebServiceConfig.GetClipCallReportReference(ppcSite.UrlWebReference);
        
        ClipCallReport.ResultOfGeneralResponse result = null;

        try
        {
            result = ccr.RelaunchRejectIncidentAccount(incidentAccountId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return false.ToString().ToLower();
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
            return false.ToString().ToLower();
        return result.Value.IsSucceeded ? "Success" : string.Format("Failed: {0}", result.Value.FailureReason);
    }
}