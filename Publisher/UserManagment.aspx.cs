using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;

public partial class Publisher_UserManagment : PageSetting
{    
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {  
        ScriptManager1.RegisterAsyncPostBackControl(btn_Set);
        if (!IsPostBack)
        {
            SetToolBox();
            this.DataBind();
            LoadLanguages();
            SetDefaultLanguage();
            LoadYesNo();
            LoadUsers();            
            SetGridView();
            RegularExpressionValidatorPhone.ValidationExpression = siteSetting.GetPhoneRegularExpression;
            
        }
        SetToolBoxEvents();
    }

    private void LoadYesNo()
    {
        Dictionary<eYesNo, string> dic = new Dictionary<eYesNo, string>();
        foreach (eYesNo _yesNo in Enum.GetValues(typeof(eYesNo)))
        {
            dic.Add(_yesNo, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", _yesNo.ToString()));
        }
        YesNoV = dic;
    }

    private void SetToolBox()
    {
        Toolbox1.RemoveAttach();
        string script = "openPopup(); return false;";
        Toolbox1.SetClientScriptToAdd(script);
        
    }
    
    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
        Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
    }

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        List<Guid> list = new List<Guid>();        
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            if (cb.Checked)
            {
                Guid _id = new Guid(((Label)row.FindControl("lbl_AccountId")).Text);
                if (_id == new Guid(userManagement.GetGuid))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "cantdelete", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_cantdelete.Text) + "');", true);
                    return;
                }
                list.Add(_id);
            }
        }
        if (list.Count == 0)
            return;

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.DeletePublishersRequest _request = new WebReferenceSite.DeletePublishersRequest();
        _request.PublisherIds = list.ToArray();
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.DeletePublishers(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Toolbox1.GetTextBoxSearch.Value = string.Empty;
        
        //Delete User From web DB
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            string command = "EXEC dbo.DeleteUser @SiteNameId, @UserId";
            conn.Open();

            foreach (Guid _guid in list)
            {
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
                cmd.Parameters.AddWithValue("@UserId", _guid);
                int a = cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            conn.Close();
        }
        LoadUsers();
        SetGridView();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataViewV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            return;
        }
      //  PrintHelper.PrintWebControl(_GridView, dataViewV);
        Session["data_print"] = dataViewV.Table;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        if (dataViewV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordExport + "');", true);
            return;
        }
        ToExcel to_excel = new ToExcel(this, "Users");
        if (!to_excel.ExecExcel(dataViewV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void Toolbox1_AddExec(object sender, EventArgs e)
    {
        
    }

    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        _btn_Search();
    }

    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        bool IsSelect = false;
        int indx = -1;
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
            {
                if (IsSelect)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ChooseOne", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.ChooseOne) + "');", true);
                    return;
                }
                IsSelect = true;
                indx = row.RowIndex;
            }
        }
        if (indx > -1)
        {
            GridViewRow row = _GridView.Rows[indx];
            hf_UserId.Value = ((Label)row.FindControl("lbl_AccountId")).Text;
            txt_NewFName.Text = ((Label)row.FindControl("lblname")).Text;
            txt_newEmail.Text = ((Label)row.FindControl("lblEmail")).Text;
            txt_NewPhone.Text = ((Label)row.FindControl("lblPhone")).Text;
            txt_BizId.Text = ((Label)row.FindControl("lblBizId")).Text;
            cb_ShowHomePage.Checked = (((Label)row.FindControl("lblShowHomePage")).Text == YesNoV[eYesNo.Yes]);
            cb_CanHearRecord.Checked = (((Label)row.FindControl("lblCanHearAllRecordings")).Text == YesNoV[eYesNo.Yes]);
            string securityLevel = ((Label)row.FindControl("lblSecurityLevel")).Text;
            foreach (ListItem li in ddl_NeySecurityLevel.Items)
                li.Selected = (li.Value == securityLevel);
            string _SiteLangId = ((Label)row.FindControl("lbl_SiteLangId")).Text;
            foreach(ListItem li in ddl_NewLanguges.Items)
                li.Selected = (li.Value == _SiteLangId);
            popUpaddUser.Attributes["class"] = "popModal_del";
            _mpe.Show();
        }
    }
    private void SetDefaultLanguage()
    {
        int langId = 0;
        string command = "SELECT dbo.GetSiteLangIdDefaultBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            langId = (int)cmd.ExecuteScalar();
        }
        foreach (ListItem li in ddl_NewLanguges.Items)
        {
            li.Selected = (int.Parse(li.Value) == langId);
        }
        hf_LangId.Value = langId.ToString();
    }

    private void LoadLanguages()
    {
        Dictionary<int, string> dic = new Dictionary<int, string>();
        string command = "EXEC dbo.GetLanguagesBySiteNameId @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                int SiteLangId = (int)reader["SiteLangId"];
                string LanguageName = (string)reader["LanguageName"];
                dic.Add(SiteLangId, LanguageName);
                ddl_NewLanguges.Items.Add(new ListItem(LanguageName, "" + SiteLangId));
            }
            conn.Close();
        }
        langugesV = dic;
    }
    public string GetPhoneValidation
    {
        get
        {
            return siteSetting.GetPhoneRegularExpression;
        }
    }

    private void LoadUsers()
    {
        DataTable data = new DataTable();       
        data.Columns.Add("AccountNumber");
        data.Columns.Add("Email");
        data.Columns.Add("FirstName");
        data.Columns.Add("Name");
        data.Columns.Add("Password");
        data.Columns.Add("PhoneNumber");
        data.Columns.Add("SecurityLevel");
        data.Columns.Add("AccountId");       
        data.Columns.Add("SiteLangId");
        data.Columns.Add("LangName");
        data.Columns.Add("DashboardAsHomePage", typeof(string));
        data.Columns.Add("CanHearAllRecordings", typeof(string));
        data.Columns.Add("BizId");

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllPublishersResponse result = new WebReferenceSite.ResultOfGetAllPublishersResponse();
        try
        {
            result = _site.GetAllPublishers();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            _updatePanelTable.Update();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            _updatePanelTable.Update();
            return;
        }
        Dictionary<eYesNo, string> dic = YesNoV;
        foreach (WebReferenceSite.PublisherData pd in result.Value.Publishers)
        {
            DataRow row = data.NewRow();
            row["AccountNumber"] = pd.AccountNumber;
            row["Email"] = pd.Email;
            row["Name"] = pd.Name;           
            row["Password"] = pd.Password;
            row["PhoneNumber"] = pd.PhoneNumber;
            row["SecurityLevel"] = pd.SecurityLevel;
            row["AccountId"] = pd.AccountId;
            row["BizId"] = pd.BizId;
            int SiteLangId = GetSiteLangId(pd.AccountId);
            row["LangName"] = langugesV[SiteLangId];
            row["SiteLangId"] = SiteLangId;
            row["DashboardAsHomePage"] = (DBConnection.IsDashboardHomePage(pd.AccountId, siteSetting.GetSiteID)) ? dic[eYesNo.Yes] :
                dic[eYesNo.No];
            row["CanHearAllRecordings"] = (pd.CanHearAllRecordings) ? dic[eYesNo.Yes] :
                dic[eYesNo.No];
            data.Rows.Add(row);          
        }
        _GridView.PageIndex = 0;
        dataV = data;
        dataViewV = data.AsDataView();
        /*
        _GridView.DataSource = data;
        _GridView.DataBind();
        _updatePanelTable.Update();
         * */
    }
    void SetGridView()
    {
        StringBuilder sb = new StringBuilder();
        DataView data = dataViewV;
    //    _GridView.PageIndex = 0;
        _GridView.DataSource = data;
        _GridView.DataBind();
        
        if (data.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noreasultSearch", "alert('" + lbl_noreasultSearch.Text + "');", true);
            CBsV = string.Empty;
            _updatePanelTable.Update();
            return;
        }
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);           
        }

        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        _updatePanelTable.Update();
    }
    /*
    bool GetDashboardAsHomePage(Guid UserId)
    {
        string command = "SELECT dbo.GetDashboardAsHomePage(@SiteNameId, @UserId)";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
        cmd.Parameters.AddWithValue("@UserId", UserId);
        bool result = (bool)cmd.ExecuteScalar();
        conn.Close();
        return result;
    }
     * */
    int GetSiteLangId(Guid UserId)
    {
        int result = -1;
        string command = "EXEC dbo.GetSiteLangIdByUserId @UserId, @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            result = (int)cmd.ExecuteScalar();
            conn.Close();
        }
        return result;
    }

    void SetUser(Guid UserId, int SiteLangId, bool DashboardAsHomePage)
    {
        string command = "EXEC dbo.SetPublisherUser @UserId, @SiteNameId, @SiteLangId, @DashboardAsHomePage";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@SiteLangId", SiteLangId);
            cmd.Parameters.AddWithValue("@DashboardAsHomePage", DashboardAsHomePage);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
    protected void btn_Set_Click(object sender, EventArgs e)
    {
        string fName = txt_NewFName.Text;
        string _id = hf_UserId.Value;
        string email = txt_newEmail.Text;
        string phone = txt_NewPhone.Text;
        string _securityLevel = ddl_NeySecurityLevel.SelectedValue;
        int SiteLangId = int.Parse(ddl_NewLanguges.SelectedValue);
        
        WebReferenceSupplier.UpsertAdvertiserRequest _request = new WebReferenceSupplier.UpsertAdvertiserRequest();
        _request.SupplierId = string.IsNullOrEmpty(_id) ? Guid.Empty : new Guid(_id);
        _request.Email = email;
        _request.Company = fName;
        _request.ContactPhone = phone;
        _request.SecurityLevel = int.Parse(_securityLevel);
        _request.CanHearAllRecordings = cb_CanHearRecord.Checked;
        _request.UserBizId = txt_BizId.Text;
        _request.AuditOn = false;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePopUp", "closePopup();", true);
        
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);

        WebReferenceSupplier.ResultOfUpsertAdvertiserResponse result = null;
        try
        {
            result = supplier.UpsertAdvertiser(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            _updatePanelTable.Update();
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            _updatePanelTable.Update();
            return;
        }
        if (result.Value.Status != WebReferenceSupplier.UpsertAdvertiserStatus.Success)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "UpsertAdvertiserStatusFailed", "alert('" +
                HttpUtility.JavaScriptStringEncode(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "UpsertAdvertiserStatus", result.Value.Status.ToString())) + "');", true);
            _updatePanelTable.Update();
            return;
        }
        Guid userId = result.Value.SupplierId;
        SetUser(userId, SiteLangId, cb_ShowHomePage.Checked);
        
        LoadUsers();
        SetGridView();
    }
    private void _btn_Search()
    {
        string name = Toolbox1.GetTxt;
        HtmlInputText _tb = Toolbox1.GetTextBoxSearch;
        if (name == Toolbox1.GetWaterTextSearch &&
                    _tb.Attributes["class"].Contains("watermark"))
            name = string.Empty;
        if(string.IsNullOrEmpty(name))
            dataViewV = dataV.AsDataView();
        else
        {
            name = name.ToLower();
            DataTable data = dataV;
            data.TableName = "Search";

            EnumerableRowCollection<DataRow> query = from Search in data.AsEnumerable()
                                                     where ((!string.IsNullOrEmpty(Search.Field<string>("Name")) && Search.Field<string>("Name").ToLower().Contains(name))
                                                        || (Search.Field<string>("Email").ToLower().Contains(name)))
                                                     orderby Search.Field<string>("Name")
                                                     select Search;

            dataViewV = query.AsDataView();
        }
        _GridView.PageIndex = 0;  
        SetGridView();
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView();
    }
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    Dictionary<int, string> langugesV
    {
        get { return (ViewState["languges"] == null) ? new Dictionary<int, string>() : (Dictionary<int, string>)ViewState["languges"]; }
        set { ViewState["languges"] = value; }
    }
    private DataView dataViewV
    {
        get { return (Session["dataView"] == null) ? null : (DataView)Session["dataView"]; }
        set { Session["dataView"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
    Dictionary<eYesNo, string> YesNoV
    {
        get { return (Dictionary<eYesNo, string>)ViewState["YesNo"]; }
        set { ViewState["YesNo"] = value; }
    }
    
}
