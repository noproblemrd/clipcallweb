﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="InvalidRequests.aspx.cs" Inherits="Publisher_InvalidRequests" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link href="../FromToDate.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../general.js" language="javascript"></script>
<script language="javascript" type="text/javascript" src="../Calender/_Calender.js"></script>
<link href="../Calender/CalenderDiv.css" rel="stylesheet" type="text/css" />
<link href="style.css" type="text/css" rel="stylesheet" />
<link href="IncompleteRegistration.css" rel="stylesheet" type="text/css"/>
<link href="../UpdateProgress/updateProgress.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../CallService.js" language="javascript"></script>

<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<script src="../jquery.min.js" type="text/javascript"></script> 
<script src="../jquery-ui.min.js" type="text/javascript"></script> 

<script type="text/javascript" language="javascript">
    function ClearValidators()
    {
        var validators = Page_Validators;
        
        for (var i = 0;i < Page_Validators.length;  i++)
        {      
			Page_Validators[i].style.display = "none";	
	    }       
    }
    function ClearTxt()
    {
        ClearValidators();
        document.getElementById("<%# txt_BlackList.ClientID %>").value = "";
        document.getElementById("<%# txt_BadWord.ClientID %>").value = "";
    }
    function pageLoad(sender, args) 
    {        
        hideDiv();    
        appl_init();
    }
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        
  //      pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {
        showDiv();
    }
    function CheckBadwords(elem)
    {
        var _value = elem.value;
        if(_value.length == 0)
            return;
        var params = "str="+_value;
        CallWebService(params, "BadWordService.asmx/CheckOne", OnCheckBadWord, OnError)
    }
    function OnCheckBadWord(arg)
    {
        document.getElementById("<%# txt_BadWord.ClientID %>").value=arg;
    }
    function CheckBlackList(elem)
    {
        var _value = elem.value;
        if(_value.length == 0)
            return;
        var params = "str="+_value;
        CallWebService(params, "BlackListService.asmx/CheckOne", OnCheckBlackList, OnError)
    }
    function OnCheckBlackList(arg)
    {
        document.getElementById("<%# txt_BlackList.ClientID %>").value=arg;
    }
    function OnError()
    {
    }
    
    $(function() {
        $("._BadWord").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "BadWordService.asmx/SuggestList",
                    data: "{ 'str': '" + request.term + "' }",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function(data) { return data; },
                    success: function(data) {
                        response($.map(data.d, function(item) {
                            return {
                                value: item
                            }
                        }))
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            },
            minLength: 1
        });
    });
    $(function() {
        $("._BlackList").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "BlackListService.asmx/SuggestList",
                    data: "{ 'str': '" + request.term + "' }",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function(data) { return data; },
                    success: function(data) {
                        response($.map(data.d, function(item) {
                            return {
                                value: item
                            }
                        }))
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            },
            minLength: 1
        });
    });
    
    /* Popup */
    function CloseIframe()
    {
       $find('_modal').hide();
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie popModal_del2_hide";
       document.getElementById("<%# _iframe.ClientID %>").src = "javascript:void(0);";
    }
    function OpenIframe(_path)
    {    
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie";
       document.getElementById("<%# _iframe.ClientID %>").src = _path;
       HideIframeDiv();  
       $find('_modal').show();
    }
    function ShowIframeDiv()
    {
        document.getElementById("divIframeLoader").style.display = "none";
       document.getElementById("div_iframe_main").style.display = "block";
    }
    function HideIframeDiv()
    {    
        document.getElementById("divIframeLoader").style.display = "block";
       document.getElementById("div_iframe_main").style.display = "none";
    }



</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" >

</cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  
   	
<div class="page-content minisite-content2a">	
					
	<div id="form-analytics">	
		<div class="main-inputs">
		
		   
			<div class="form-field">
			    <asp:Label ID="lbl_BlackList" CssClass="label" runat="server" Text="Blacklisted phone number"></asp:Label> 
			    <asp:TextBox ID="txt_BlackList" CssClass="form-text _BlackList" runat="server" onblur="javascript:CheckBlackList(this);"></asp:TextBox>
                
			</div>
			
			<div class="form-field">
			    <asp:Label ID="lbl_BadWord" CssClass="label" runat="server" Text="Improper Language"></asp:Label>
			    <asp:TextBox ID="txt_BadWord" CssClass="form-text _BadWord" runat="server" onblur="javascript:CheckBadwords(this);"></asp:TextBox>
			</div>
			<div class="form-field">
                <asp:Label ID="lbl_ShowMe" CssClass="label" runat="server" Text="Show me"></asp:Label>                                    
                <asp:DropDownList ID="ddl_ShowMe" CssClass="form-select" runat="server" onchange="javascript:ClearTxt();"></asp:DropDownList>			
			</div>
            <div class="form-field" runat="server" id="div_affiliate" visible="false">
                <asp:Label ID="lbl_Affiliate" CssClass="label" runat="server" Text="Affiliates"></asp:Label>                                    
                <asp:DropDownList ID="ddl_Affiliate" CssClass="form-select" runat="server"></asp:DropDownList>			
			</div>

			
			
		</div>
		 <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>
		
		
	</div>
	
	<div class="clear"></div>
	<div id="list" class="table">                    
        <asp:UpdatePanel runat="server" ID="_updatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                
                <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                
                <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" CssClass="Grid-table" AllowPaging="True" OnPageIndexChanging="_GridView_PageIndexChanging" PageSize="20">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle  CssClass="pager" HorizontalAlign="Center" />
                <Columns>
                    <asp:TemplateField SortExpression="CaseNumber">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_ID.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <a id="a_Case" runat="server" href="<%# Bind('CaseScript') %>">
                                <asp:Label ID="Label9" runat="server" Text="<%# Bind('CaseNumber') %>"></asp:Label>
                            </a>
                                                  
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="CreatedOn">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_date.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text="<% # Bind('CreatedOn')%>"></asp:Label>                      
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="ConsumerPhoneNumber">
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_CustomerNumber.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text="<% # Bind('ConsumerPhoneNumber')%>"></asp:Label>                      
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="Title">
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_heading.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text="<% # Bind('Title')%>"></asp:Label>                      
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="Description">
                        <HeaderTemplate>
                            <asp:Label ID="Label9" runat="server" Text="<%# lbl_description.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label10" runat="server" Text="<% # Bind('Description')%>"></asp:Label>                      
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="InvalidStatus">
                        <HeaderTemplate>
                            <asp:Label ID="Label11" runat="server" Text="<%# lbl_Status.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label12" runat="server" Text="<% # Bind('InvalidStatus')%>"></asp:Label>                      
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                </asp:GridView>
                
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>    

 <asp:Panel ID="panel_iframe" runat="server" CssClass="popModal_del2_ie popModal_del2_hide" style="display:none;"
     Width="760" Height="445">
     <div id="divIframeLoader" class="divIframeLoader">   
        <table>
            <tr>
                <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
            </tr>
        </table>
    </div>
    <div id="div_iframe_main">
        <div class="top"></div>
            <a id="a_close" runat="server" class="span_A" href="javascript:void(0);" onclick="javascript:CloseIframe();"></a>
        <iframe runat="server" id="_iframe" width="760px" height="560px" frameborder="0" ALLOWTRANSPARENCY="true" ></iframe>
        <div class="bottom2"></div>
    </div>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="_mpe" runat="server"
                TargetControlID="btn_virtual"
                PopupControlID="panel_iframe"
                 BackgroundCssClass="modalBackground"                  
                  BehaviorID="_modal"               
                  DropShadow="false">
    </cc1:ModalPopupExtender>
    
    <div style="display:none;">

        <asp:Button ID="btn_virtual" runat="server"  style="display:none;" CausesValidation="false"/>
    </div>
    
<asp:Label ID="lbl_ID" runat="server" Text="ID" Visible="false"></asp:Label>
<asp:Label ID="lbl_date" runat="server" Text="Created on" Visible="false"></asp:Label>
<asp:Label ID="lbl_CustomerNumber" runat="server" Text="Phone number" Visible="false"></asp:Label>
<asp:Label ID="lbl_heading" runat="server" Text="Heading" Visible="false"></asp:Label>
<asp:Label ID="lbl_description" runat="server" Text="Description" Visible="false"></asp:Label>
<asp:Label ID="lbl_Status" runat="server" Text="Status" Visible="false"></asp:Label>

<asp:Label ID="lblTitleInvalidRequests" runat="server" Text="Invalid Requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>

</asp:Content>


