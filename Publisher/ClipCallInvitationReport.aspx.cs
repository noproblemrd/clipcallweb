﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallInvitationReport : PageSetting
{
    const string Num_Format = "{0:#,0.##}";
    const int ONE_DAY = 1;
    protected readonly string SessionTableName = "dataInvitationReport";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_ClipCallInvitationReport.Text);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.InvitationReportRequest _request = new WebReferenceReports.InvitationReportRequest();
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo.AddDays(1);
        WebReferenceReports.ResultOfListOfInvitationReportResponse result = null;
        try
        {
            result = reports.ClipCallInvitationReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        DataTable table = new DataTable();
        table.Columns.Add("Date");
        table.Columns.Add("ToPhone");
        table.Columns.Add("InvitationType");
        table.Columns.Add("InvitationFrom");
        table.Columns.Add("SupplierName");
        table.Columns.Add("SupplierPhone");
        table.Columns.Add("VideoLink");
        table.Columns.Add("HasVideo", typeof(bool));
        foreach(WebReferenceReports.InvitationReportResponse data in result.Value)
        {
            DataRow row = table.NewRow();
            row["Date"] = string.Format(siteSetting.DateFormat, data.CreatedOn) + "</br>" +
                    string.Format(siteSetting.TimeFormat, data.CreatedOn);
            row["ToPhone"] = data.Phone;
            row["InvitationType"] = data.Invitation_Type.ToString();
            row["InvitationFrom"] = data.InvitationFrom == WebReferenceReports.CreateVideoChatBy.AccountWebInvitation ? "Web invitation" : "App invitation";
            row["SupplierName"] = data.SupplierName;
            row["SupplierPhone"] = data.SupplierPhone;
            if (!string.IsNullOrEmpty(data.VideoUrl))
            {
                GenerateVideoLink gvl = new GenerateVideoLink(data.VideoUrl, null);
                row["VideoLink"] = gvl.GenerateFullUrl();
                row["HasVideo"] = true;
            }
            else
                row["HasVideo"] = false;
            table.Rows.Add(row);
        }
        _GridView.DataSource = table;
        _GridView.DataBind();
        lbl_RecordMached.Text = result.Value.Length.ToString();
       
        dataV = table;

    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }

}