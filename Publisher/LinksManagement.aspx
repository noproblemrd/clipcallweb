<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="LinksManagement.aspx.cs" Inherits="Publisher_LinksManagement" Title="Untitled Page" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asps" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="style.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../general.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>
 
<!--
<asp:Label ID="lblSubTitle" runat="server">Links management</asp:Label>
-->
	<!--<div id="form-analytics">-->
	 <div class="page-content minisite-content">
	  <h2>Links management</h2>		
        <asp:Repeater ID="_RepeaterParent" runat="server">
            <HeaderTemplate>
               
            </HeaderTemplate>
            <ItemTemplate>
                <h4>              
                    <asp:Label ID="lbl_title" runat="server" Text="<%# Bind('PageName')%>"></asp:Label>
                    <asp:Label ID="lbl_PageId" runat="server" Text="<%# Bind('PageId')%>" Visible="false"></asp:Label>
                </h4>
                <asp:Repeater ID="_RepeaterChild" runat="server">
                    <HeaderTemplate>
                        <div class="form-fieldset clearfix" id="form-analytics">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="form-field">                
                            <asp:Label ID="lbl_LinkDesc" CssClass="label" runat="server" Text="<%# Bind('Translate') %>"></asp:Label>
                            <asp:Label ID="lbl_LinkId" runat="server" Text="<%# Bind('LinkId') %>" Visible="false"></asp:Label>
                            <div class="form-input">
                                <asp:TextBox ID="txt_Link" CssClass="form-text" runat="server" Text="<%# Bind('url') %>"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator_links" CssClass="error-msg" runat="server" ErrorMessage="<%# lbl_invalid.Text %>" ControlToValidate="txt_Link" ValidationGroup="_links" Display="Static" ValidationExpression="^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$"></asp:RegularExpressionValidator>
                                <!--http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?-->
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
                
            </ItemTemplate>
   
            <FooterTemplate>
                
            </FooterTemplate>
        </asp:Repeater>
   
    <asp:Button ID="btn_set" runat="server" Text="Set" CssClass="form-submit" OnClick="btn_set_Click" ValidationGroup="_links"  />
</div>
<asp:Label ID="lbl_invalid" runat="server" Text="Invalid" Visible="false"></asp:Label>
<asp:Label ID="lbl_logo" runat="server" Text="Logo" Visible="false"></asp:Label>
</asp:Content>

