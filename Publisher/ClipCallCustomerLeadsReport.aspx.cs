﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallCustomerLeadsReport : PageSetting
{
    const int START = 1;
    const int END = 3;
    const string REPORT_NAME = "Customer Leads Report";
    protected readonly string SessionTableName = "CustomerLeadsReport";
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            //  RequestsReportRequestV = null;
            LoadCounts();
            ExecReport();
        }
        SetToolbox();
        //  SetToolboxEvents();
        Header.DataBind();
    }

    private void LoadCounts()
    {
        for (int i = START; i <= END; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            ddl_LeadCount.Items.Add(li);
        }
        ddl_LeadCount.SelectedIndex = 1;
    }
    void ExecReport()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        int num = int.Parse(ddl_LeadCount.SelectedValue);
        ClipCallReport.ResultOfListOfCustomerLeadsReportResponse _response = null;
        try
        {
            _response = report.CustomerLeadsReport(num);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (_response.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        DataTable dt = new DataTable();
        dt.Columns.Add("Name");
        dt.Columns.Add("Phone");
        dt.Columns.Add("Email");
        dt.Columns.Add("Count");
        dt.Columns.Add("FirstLead");
        dt.Columns.Add("AvgTimeBetweenLeads");
        foreach (ClipCallReport.CustomerLeadsReportResponse data in _response.Value)
        {
            DataRow row = dt.NewRow();
            row["Name"] = data.Name;
            row["Phone"] = data.Phone;
            row["Email"] = data.Email;
            row["Count"] = data.Count;
            row["FirstLead"] = data.FirstLeadFromCreated;
            row["AvgTimeBetweenLeads"] = data.AvgTimeBetweenLeads;
            dt.Rows.Add(row);
        }
        div_results.Visible = true;
        lbl_RecordMached.Text = _response.Value.Length.ToString();
        _GridView.DataSource = dt;
        _GridView.DataBind();
        dataV = dt;

    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(REPORT_NAME);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
}