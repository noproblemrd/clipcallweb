using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Publisher_MasterPageFrame : System.Web.UI.MasterPage//, IPostBackEventHandler
{
 //   const string SET_STATUS = "SET_STATUS";
    PageSetting ps;
    protected void Page_Init(object sender, EventArgs e)
    {
        //Security- for the browser not save in the cash memory
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");
        ////
        ps = (PageSetting)Page;
        if (Session.IsNewSession)
            ps.UnrecognizedClient(lbl_logOutMes.Text, "LogOut.aspx");
        if (!IsPostBack)
        {
            if (!ps.userManagement.IsPublisher())
                Response.Redirect("LogOut.aspx");       
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //     SetAccountStatus();
        ScriptManager1.RegisterAsyncPostBackControl(bt_virtual_status);
        ScriptManager1.RegisterAsyncPostBackControl(btn_InactiveReason);
        if (!IsPostBack)
        {
            //      PageSetting ps = (PageSetting)Page;
            if (ps.GetGuidSetting() == string.Empty)
            {
                string direction = "javascript:void(0);";
                linkProfessionalUpdate.HRef = direction;
                linkCalls.HRef = direction;
                linkSetting.HRef = direction;
            }
            else
            {
                a_frameAspx.HRef = ResolveUrl("~") + "Publisher/framePublisher.aspx";
                linkProfessionalUpdate.HRef = ResolveUrl("~") + "Management/ProfessionalUpdate.aspx";
                linkCalls.HRef = ResolveUrl("~") + "Management/Mycalls.aspx";
                linkSetting.HRef = ResolveUrl("~") + "Management/ProfessionalSetting.aspx";
                a_linkCalls.HRef = ResolveUrl("~") + "Management/MyCalls.aspx";
                a_balance.HRef = ResolveUrl("~") + "Management/MyBalance.aspx";
                a_MyReview.HRef = ResolveUrl("~") + "Management/MyReview.aspx";
            }

            
            string pagename =
                System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);


            SetMenu();
            LoadInactivityReasons();
            LoadAccountStatus();
            /*
            if (pagename == GetFileName(a_frameAspx.HRef))
                a_frameAspx.Attributes.Add("class", "active");
            else if (pagename == GetFileName(linkProfessionalUpdate.HRef))
                linkProfessionalUpdate.Attributes["class"] = "active";
            else if (pagename == GetFileName(linkCalls.HRef))
                linkCalls.Attributes["class"] = "active";
            else if (pagename == GetFileName(linkSetting.HRef))
                linkSetting.Attributes["class"] = "active";
            */
            Control cont = this;
            while (cont != null && !cont.GetType().IsSubclassOf(typeof(PageSetting)))
                cont = cont.NamingContainer;
            if (cont != null)
                GetTranslate(ps);
            string _user = ps.GetUserNameSetting();
            lblUserName.Text = (_user == "NEW") ? lbl_NEW.Text : _user;
            lbl_AccountCode.Text = (ps.GetUserSetting() == null) ? "--" : ps.GetUserSetting().User_Code;
            if (!ps.siteSetting.HasMiniSite)
                linkProfessionalUpdate.HRef = "";
            
            if (pagename.ToLower() != "framePublisher.aspx".ToLower())
                MyStyleSheet.Attributes.Add("href", (ResolveUrl("~") + "Management/" + ps.siteSetting.GetStyle));
             
        }
            /*
        else
        {
            if (Request["__EVENTARGUMENT"] == SET_STATUS)
            {
                LoadAccountStatus();
            }
            
        }
             * */
   //     LoadFirstTimeAccountStatus();
        Page.Header.DataBind();
    }


    private void SetMenu()
    {
        string pageName =
            (System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"])).ToLower();


        if (pageName == GetFileName(a_frameAspx.HRef).ToLower())
        {
            a_frameAspx.Attributes.Add("class", "btn-myaccount active");
            btn_myAccount.Attributes.Add("class", "_btn_myaccount active");


        }


        else if (pageName == GetFileName(linkProfessionalUpdate.HRef).ToLower())
        {
            linkProfessionalUpdate.Attributes.Add("class", "btn-myminisite active");
            btn_myminisite.Attributes.Add("class", "_btn_myminisite active");
        }

        else if (pageName == GetFileName(linkSetting.HRef).ToLower())
        {
            linkSetting.Attributes.Add("class", "btn-faq active");
            btn_faq.Attributes.Add("class", "_btn_faq active");
        }

        else if (pageName == GetFileName(linkCalls.HRef).ToLower() ||
                pageName == GetFileName(a_balance.HRef).ToLower() ||
                pageName == GetFileName(a_MyReview.HRef).ToLower())
        {
            linkCalls.Attributes.Add("class", "btn-calls active");
            btn_calls.Attributes.Add("class", "_btn_calls active");

            if (pageName == GetFileName(a_linkCalls.HRef).ToLower())
                a_linkCalls.Attributes.Add("class", "active");
            else if (pageName == GetFileName(a_balance.HRef).ToLower())
                a_balance.Attributes.Add("class", "active");
            else if (pageName == GetFileName(a_MyReview.HRef).ToLower())
                a_MyReview.Attributes.Add("class", "active");

            //else if (pageName == a_balance.HRef)
            // a_balance.Attributes.Add("class", "active");
        }
        
    }
    private string GetFileName(string str)
    {
        string[] strs = str.Split('/');
        return strs[strs.Length - 1];
    }
    public void SetUserName(string name)
    {
        lblUserName.Text = name;
    }
    void GetTranslate(PageSetting ps)
    {
        DBConnection.LoadTranslateToControl(this, "MasterPageFrame.master", ps.siteSetting.siteLangId);

    }
    /*
    private void LoadFirstTimeAccountStatus()
    {
        Account_Status ac = AccountStatusV;
        if (ac == null)
            LoadAccountStatus();
        else
            SetAccountStatus(ac);
    }
     * */
    void LoadAccountStatus()
    {
        if (string.IsNullOrEmpty(ps.GetGuidSetting()))
        {
            string _img_url = Utilities.GetRamzorImage(WebReferenceSupplier.SupplierStatus.Candidate, false);
            string __status = EditTranslateDB.GetControlTranslate(ps.siteSetting.siteLangId, "Supplier_Status", WebReferenceSupplier.SupplierStatus.Candidate.ToString());
            Account_Status _ac = new Account_Status { Img_URL = _img_url, Status = __status, Supplier_status = WebReferenceSupplier.SupplierStatus.Candidate };
            SetAccountStatus(_ac);
            return;
        }
        //   PageSetting ps = (PageSetting)Page;
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfSupplierStatusContainer result = null;
        try
        {
            result = _supplier.GetSupplierStatus(new Guid(ps.GetGuidSetting()));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        /*****
        string pagename =
         System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        dbug_log.ExceptionLog(new Exception(), pagename + "|||" + result.Value.ToString());
        /*****/
        string img_url = Utilities.GetRamzorImage(result.Value.SupplierStatus, result.Value.IsInactiveWithMoney);
        string _status = EditTranslateDB.GetControlTranslate(ps.siteSetting.siteLangId, "Supplier_Status", result.Value.SupplierStatus.ToString());
        Account_Status ac = new Account_Status { Img_URL = img_url, Status = _status, Supplier_status = result.Value.SupplierStatus };
        SetAccountStatus(ac);

    }
    void SetAccountStatus(Account_Status ac)
    {
        Image_status.ImageUrl = ac.Img_URL;
        lbl_TheStatus.Text = ac.Status;
        if (ac.Supplier_status == WebReferenceSupplier.SupplierStatus.Inactive)
            Image_status.OnClientClick = "return ConfirmActiveAccount();";
        else
            Image_status.OnClientClick = "InactiveAccount(); return false;";
        _UpdatePanel_AccountStatus.Update();
   //     AccountStatusV = ac;
    }

    protected void btn_suspend_click(object sender, EventArgs e)
    {

        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfSupplierStatus result = null;
        try
        {
            result = _supplier.ActivateSupplier(new Guid(ps.GetGuidSetting()), ps.userManagement.Get_Guid);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Account_Status ac = new Account_Status();
        ac.Img_URL = Utilities.GetRamzorImage(result.Value, false);
        ac.Supplier_status = result.Value;
        ac.Status = EditTranslateDB.GetControlTranslate(ps.siteSetting.siteLangId, "SupplierStatus", result.Value.ToString());
        SetAccountStatus(ac);
        ScriptManager.RegisterClientScriptBlock(Image_status, typeof(Page), "run_report", "try{top.run_report();}catch(ex){}", true);
        //       _UpdatePanel_AccountStatus.Update(); 
    }

    protected void btn_InactiveReason_click(object sender, EventArgs e)
    {

        Guid ReasonId = new Guid(ddl_InactiveReasons.SelectedValue);
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfDeactivateSupplierResponse result = null;
        try
        {
            result = _supplier.DeactivateSupplier(new Guid(ps.GetGuidSetting()), ReasonId, ps.userManagement.Get_Guid);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Account_Status ac = new Account_Status();
        ac.Img_URL = Utilities.GetRamzorImage(WebReferenceSupplier.SupplierStatus.Inactive, result.Value.IsInactiveWithMoney);
        ac.Supplier_status = WebReferenceSupplier.SupplierStatus.Inactive;
        ac.Status = EditTranslateDB.GetControlTranslate(ps.siteSetting.siteLangId, "Supplier_Status", WebReferenceSupplier.SupplierStatus.Inactive.ToString());
        SetAccountStatus(ac);
        popUpStatusReason.Attributes["class"] = "popModal_del popModal_del_hide";
        _mpe_Master.Hide();
        ScriptManager.RegisterClientScriptBlock(Image_status, typeof(Page), "run_report", "try{top.run_report();}catch(ex){}", true);
    }
    private void LoadInactivityReasons()
    {
        //    PageSetting ps = (PageSetting)Page;
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(ps);
        WebReferenceSupplier.ResultOfListOfTableRowData reasons = null;
        try
        {
            reasons = _supplier.GetAllInactivityReasons();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, ps.siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (reasons.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_InactiveReasons.Items.Clear();
        foreach (var gsp in reasons.Value)
        {
            if (gsp.Inactive == false)
                ddl_InactiveReasons.Items.Add(new ListItem(gsp.Name, gsp.Guid.ToString()));
        }
    }
    /*
    Account_Status AccountStatusV
    {
        set { Session["Account_Status"] = value; }
        get { return (Session["Account_Status"] == null) ? null : (Account_Status)Session["Account_Status"]; }
    }
    */

   
    public string bt_virtual_status_UniqueId
    {
        get { return bt_virtual_status.UniqueID; }
    }
    /*
    public string GetSetStatus
    {
        get { return SET_STATUS; }
    }
     * */
}
