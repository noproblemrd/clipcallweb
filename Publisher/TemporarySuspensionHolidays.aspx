﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="TemporarySuspensionHolidays.aspx.cs" Inherits="Publisher_TemporarySuspensionHolidays" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<%@ Register src="../Controls/Toolbox/ToolboxWizardReport.ascx" tagname="ToolboxWizardReport" tagprefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link href="style.css" rel="stylesheet" type="text/css"/>
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="../general.js"></script>
<script type="text/javascript" src="../Calender/_Calender.js"></script>
<script type="text/javascript" language="javascript">
    var _format;
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
        _format = "<%# GetCalenderDateFormat %>";
        SetDateDefault();
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }
    /************/
    function OpenPopUp() {
        $find("_modal").show();
    }
    function ClosePopUp() {
        $find("_modal").hide();
        document.getElementById("<%# txt_Name.ClientID %>").value = "";
        document.getElementById("<%# txt_from.ClientID %>").value = "";
        document.getElementById("<%# txt_to.ClientID %>").value = "";
    }

    /*********/
    function ChkValidation() {
        if (!Page_ClientValidate('AddHoliday'))
            return false;

    }
    /****dates*****/
    function SetDateDefault() {        
        
        SetNowDateFirstTime(document.getElementById("<%# txt_from.ClientID %>"), document.getElementById("<%# ddl_FromDate.ClientID %>"));
        SetNowDateFirstTime(document.getElementById("<%# txt_to.ClientID %>"), document.getElementById("<%# ddl_ToDate.ClientID %>"));

    }
    function SetNowDateFirstTime(_txt, _dll) {
        var _date = new Date();
        var hour = _date.getHours();

        if (hour > 23) {
            _date = _date.addDays(1);
            hour = 0;
        }
        else
            hour++;

        var _hour = ((hour < 10) ? "0" + hour : hour) + ":00";

        var date_str = GetDateString(_date, "<%# GetCalenderDateFormat %>");
        document.getElementById("<%# hf_CurrentTime.ClientID %>").value = date_str + ";" + _hour;
        _txt.value = date_str;
        Set_Dll(_dll, _hour);
    }
    function SetNowDate(_calender, _dll) {
        var _date = new Date();
        Set_Date(_calender, _dll, _date);
        
    }
    function Set_Date(_calender, _dll, _date) {
        var _txt = _calender._textbox._element;
        //      _calender.set_selectedDate(_date)
     //   _calender._selectedDate = _date;
        var hour = _date.getHours();

        if (hour > 23) {
            _date = _date.addDays(1);
            hour = 0;
        }
        else
            hour++;

        var _hour = ((hour < 10) ? "0" + hour : hour) + ":00";

        var date_str = GetDateString(_date, "<%# GetCalenderDateFormat %>");
        _calender.set_selectedDate(date_str)
 //       document.getElementById("<%# hf_CurrentTime.ClientID %>").value = date_str + ";" + _hour;
        _txt.value = date_str;
        Set_Dll(_dll, _hour);
    }
    function Set_Dll(_dll, _value) {
        for (var i = 0; i < _dll.length; i++) {
            if (_dll.options[i].value == _value) {
                _dll.selectedIndex = i;
                return;
            }
        }
    }
    function showCalender(_calender) {
        var calender = $find(_calender);
        calender.show();
    }
    function HideCalender(_calender, control) {
        var calender = $find(_calender);
        Hide_Calender("000", calender, _format, control);
    //    calender.hide();
    }
    function FromDateHide(sender, args) {
        var ddl_FromDate = document.getElementById("<%# ddl_FromDate.ClientID %>");
        var txt_from = document.getElementById("<%# txt_from.ClientID %>");
        var date_from = txt_from.value;
        if (!validate_Date(date_from, _format)) {
            alert('0000');
            SetNowDate(sender, ddl_FromDate);
            return;
        }
        var date_from_mil = new Date(GetDateFormat(date_from, _format));
        var hour_from = ddl_FromDate.value.split(':')[0];
        var _hour = parseInt(hour_from);
        date_from_mil.setHours(_hour);
        if (date_from_mil.getTime() < new Date().getTime()) {
            alert('0000');
            SetNowDate(sender, ddl_FromDate);
            return;
        }
        /*
        var txt_to = document.getElementById("<%# txt_to.ClientID %>");
        var date_to = txt_to.value;
        if(!validate_Date(date_to, _format))
        {
            WrongDateTo();
            return;
        }
        
        var date_to_mil = new Date(GetDateFormat(date, _format));
        
        */
  //       || GetDateFormat(date_to, _format)

    }
    function WrongDateTo(){
        
    }
    function OnCalenderShow(sender, args) {
    /*
        var _obj = sender;
    
        for (att in _obj) {
            if (att.indexOf("set") > -1) {
                alert("attribute name= " + att);
                alert("attribute data= " + _obj[att]);
            }
}

*/
        set_div(sender);
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager> 
<uc1:ToolboxWizardReport ID="_Toolbox" runat="server" />
<div class="page-content minisite-content" >
    <h2><asp:Label ID="lblSubTitle" runat="server" Text="Period suspension"></asp:Label></h2>
    
    <div id="form-analyticsseg">
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" ShowFooter="true" 
                CssClass="data-table">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                 <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                <FooterStyle CssClass="footer"  />
                
                <Columns>                     
                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                        <br />
                        <asp:CheckBox ID="cb_all" runat="server" >
                        </asp:CheckBox>                    
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="cb_choose" runat="server" />                    
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# lbl_Name.Text %>"></asp:Label>      
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text="<%# Bind('Name') %>"></asp:Label> 
                        <asp:Label ID="lbl_ID" runat="server" Text="<%# Bind('ClosureDateId') %>" Visible="false"></asp:Label>       
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label4" runat="server" Text="<%# lbl_FromDate.Text %>"></asp:Label>      
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text="<%# Bind('FromDate') %>"></asp:Label>       
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label6" runat="server" Text="<%# lbl_ToDate.Text %>"></asp:Label>      
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text="<%# Bind('ToDate') %>"></asp:Label>       
                    </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<div id="div_PopUpAdd" runat="server" sclass="content" style="max-height:450px;display:none;" class="popModal_del popModal_del_hide"  >
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A" href="javascript:ClosePopUp();"></a>
        <div class="content" >
            <div class="modaltit">
                <h2><asp:Label ID="lbl_PopupTitle" runat="server" Text="Add suspension"></asp:Label></h2>
            </div>
            <div>
    	    <center>    	    
                <table>
                    <tr>
                    <td colspan="2">
                        <asp:Label ID="lblName" runat="server" Text="<%# lbl_Name.Text %>"></asp:Label>
                        <asp:TextBox ID="txt_Name" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator_name" runat="server" 
                        ErrorMessage="Missing" CssClass="error-msg" ControlToValidate="txt_Name"
                          ValidationGroup="AddHoliday"></asp:RequiredFieldValidator>
                    </td>
                    </tr>
    	            <tr>
    	            <td>
                        <asp:Label ID="Label30" runat="server" Text="<%# lbl_FromDate.Text %>"></asp:Label>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender_FromDate" runat="server" TargetControlID="txt_from" 
                        EnableViewState="true" WatermarkText="<%# GetCalenderDateFormat %>" BehaviorID="WaterFrom"></cc1:TextBoxWatermarkExtender>
                        <asp:TextBox ID="txt_from" runat="server" CssClass="form-textcal" autocomplete="off" 
                            onfocus="javascript:showCalender('CalendarPopupFrom');" onblur="javascript:HideCalender('CalendarPopupFrom', this);"></asp:TextBox>
                             
                        <cc1:CalendarExtender ID="CalendarExtender_FromDate" PopupPosition="BottomRight" runat="server" 
                            TargetControlID="txt_from" PopupButtonID="a_fromDate" Format="<%# GetCalenderDateFormat %>" 
                            OnClientShown="OnCalenderShow" OnClientHidden="FromDateHide"
                            BehaviorID="CalendarPopupFrom"></cc1:CalendarExtender>
                        <asp:DropDownList ID="ddl_FromDate" runat="server">
                        </asp:DropDownList>
    	            </td> 
                    <td>
                        <asp:Label ID="Label31" runat="server" Text="<%# lbl_ToDate.Text %>"></asp:Label>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender_ToDate" runat="server" TargetControlID="txt_to" 
                        EnableViewState="true" WatermarkText="<%# GetCalenderDateFormat %>" BehaviorID="WaterTo"></cc1:TextBoxWatermarkExtender>
                        <asp:TextBox ID="txt_to" runat="server" CssClass="form-textcal" autocomplete="off" 
                            onfocus="javascript:showCalender('CalendarPopupTo');" onblur="javascript:HideCalender('CalendarPopupTo', this);"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender_ToDate" PopupPosition="BottomRight" runat="server" 
                            TargetControlID="txt_to" PopupButtonID="a_fromDate" Format="<%# GetCalenderDateFormat %>" 
                            BehaviorID="CalendarPopupTo"></cc1:CalendarExtender>
                        <asp:DropDownList ID="ddl_ToDate" runat="server">
                        </asp:DropDownList>
                    </td>           
                            
    	            </tr> 

                    <tr>
                    <td colspan="2">
                        <asp:Button ID="btn_Set" runat="server" Text="Set" ValidationGroup="AddHoliday" OnClientClick="return ChkValidation();"/>
                        <asp:HiddenField ID="hf_CurrentTime" runat="server" />
                    </td>
                    </tr>
                </table>
            </center>
            </div>  	    
            
	    </div>
	<div class="bottom2"></div>        	
</div>

<cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btn_virtual"
    PopupControlID="div_PopUpAdd"
    BackgroundCssClass="modalBackground" 
              
    BehaviorID="_modal"               
    DropShadow="false">
</cc1:ModalPopupExtender>

<div style="display:none;">
    <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />                     
</div>

<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
<asp:Label ID="lbl_Name" runat="server" Text="Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_FromDate" runat="server" Text="From date" Visible="false"></asp:Label>
<asp:Label ID="lbl_ToDate" runat="server" Text="To date" Visible="false"></asp:Label>
</asp:Content>


