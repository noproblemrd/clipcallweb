﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_TemporarySuspensionHolidays : PageSetting
{
    protected override void OnPreInit(EventArgs e)
    {

        siteSetting = new SiteSetting("1");
        Session["Site"] = siteSetting;
        userManagement = new UserMangement(Guid.Empty.ToString(), "1", "yoav");
        Session["UserGuid"] = userManagement;
        LinkToSet = new System.Collections.Generic.Dictionary<string, string>();
        LinkToSet.Clear();
        string link_advertiser = DBConnection.GetLinkToSet("a_Advertisers", userManagement.GetSecurityLevel, siteSetting.GetSiteID);
        if (string.IsNullOrEmpty(link_advertiser) || link_advertiser == "#")
            link_advertiser = "PublisherWelcome.aspx";
        LinkToSet.Add("a_Advertisers", link_advertiser);

        LinkToSet.Add("a_AccountSettings",
            DBConnection.GetLinkToSet("a_AccountSettings", userManagement.GetSecurityLevel, siteSetting.GetSiteID));
        LinkToSet.Add("a_Reports",
            DBConnection.GetLinkToSet("a_Reports", userManagement.GetSecurityLevel, siteSetting.GetSiteID));
        LinkToSet.Add("a_consumers",
            DBConnection.GetLinkToSet("a_consumers", userManagement.GetSecurityLevel, siteSetting.GetSiteID));
        SaveLinkToSet();
        base.OnPreInit(e);
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            SetToolbox();
            LoadTimes();
            _dataBind();
            LoadSuspendPeriods();
        }
        SetToolboxEvents();
        Header.DataBind();
    }

    private void LoadTimes()
    {
        for (int i = 0; i < 24; i++)
        {
            string str = ((i < 10) ? "0" + i : i + "") + ":00";
            ddl_FromDate.Items.Add(new ListItem(str, str));
            ddl_ToDate.Items.Add(new ListItem(str, str));                
        }
    }

    private void _dataBind()
    {
        Page.DataBind();
    }

    private void SetToolboxEvents()
    {
        _Toolbox.DeleteExec += new EventHandler(_Toolbox_DeleteExec);
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
    }

    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    void _Toolbox_DeleteExec(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    private void SetToolbox()
    {
        _Toolbox.RemoveAttach();
        _Toolbox.RemoveEdit();
        _Toolbox.RemoveSave();
        _Toolbox.SetClientScriptToAdd("OpenPopUp(); return false;");
    }

    private void LoadSuspendPeriods()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfClosureDateData result = null;
        try
        {
            result = _site.GetClosureDates();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        DataTable data =  GetDataTable.GetDataTableFromListDateTimeFormat(result.Value, siteSetting.DateFormat, siteSetting.TimeFormat);
        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();        
    }
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    protected string GetCalenderDateFormat
    {
        get
        {
            return siteSetting.DateFormatClean;
        }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }   
}