﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_InjectionUpset : PageSetting
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (this.siteSetting == null || string.IsNullOrEmpty(this.siteSetting.GetSiteID))
             Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Guid InjectionId;
            if (Guid.TryParse(Request.QueryString["InjectionId"], out InjectionId))
            {
                VInjectionId = InjectionId;
                //        public Result<DataModel.Injection.InjectionData> GetInjectionDetails(Guid injectionId)
                WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
                WebReferenceSite.ResultOfInjectionData _result = null;
                try
                {
                    _result = _site.GetInjectionDetails(InjectionId);
                }
                catch (Exception exc)
                {
                    dbug_log.ExceptionLog(exc);
                    CloseWindow();
                    return;
                }
                if (_result.Type == WebReferenceSite.eResultType.Failure)
                {
                    CloseWindow();
                    return;
                }
                cb_BlockWithUs.Checked = _result.Value.BlockWithUs;
                lbl_Title.Text = "Update";
                txt_Name.Text = _result.Value.Name;
                txt_Script.Text = _result.Value.ScriptBefore;
                txt_UrlScript.Text = _result.Value.ScriptUrl;
                txt_ScriptId.Text = _result.Value.ScriptElementId;

                VName = _result.Value.Name;
                VScript = _result.Value.ScriptBefore;
                VUrlScript = _result.Value.ScriptUrl;
                VBlockWithUs = _result.Value.BlockWithUs;
                VScriptElementId = _result.Value.ScriptElementId;
            }
            else
                lbl_Title.Text = "Add";
           
        }
        Header.DataBind();
    }
    protected void lb_Save_Click(object sender, EventArgs e)
    {
        string name = txt_Name.Text;
        string _Script = txt_Script.Text;
        string _UrlScript = txt_UrlScript.Text;
        string _ScriptElementId = txt_ScriptId.Text.Trim();
        if (VName == name && VScript == _Script && VUrlScript == _UrlScript && 
            VBlockWithUs == cb_BlockWithUs.Checked && _ScriptElementId == VScriptElementId)
        {

            return;
        }
        //        public Result<Guid> UpsertInjectionDetails(DataModel.Injection.InjectionData data)
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.InjectionData data = new WebReferenceSite.InjectionData();
        data.InjectionId = VInjectionId;
        data.Name = name;
        data.ScriptBefore = _Script;
        data.ScriptUrl = _UrlScript;
        data.ScriptElementId = _ScriptElementId;
        data.BlockWithUs = cb_BlockWithUs.Checked;
        WebReferenceSite.ResultOfGuid result = null;
        try
        {
            result = _site.UpsertInjectionDetails(data);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        CloseReloadWindow();
       
    }
    void CloseWindow()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "CloseWin", "CloseWin();", true);
    }
    void CloseReloadWindow()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "CloseReloadWin", "CloseReloadWin();", true);
    }
    Guid VInjectionId
    {
        get { return ViewState["VInjectionId"] == null ? Guid.Empty : (Guid)ViewState["VInjectionId"]; }
        set { ViewState["VInjectionId"] = value; }
    }
    string VName
    {
        get { return ViewState["VName"] == null ? string.Empty : (string)ViewState["VName"]; }
        set { ViewState["VName"] = value; }
    }
    string VScript
    {
        get { return ViewState["VScript"] == null ? string.Empty : (string)ViewState["VScript"]; }
        set { ViewState["VScript"] = value; }
    }
    string VUrlScript
    {
        get { return ViewState["VUrlScript"] == null ? string.Empty : (string)ViewState["VUrlScript"]; }
        set { ViewState["VUrlScript"] = value; }
    }
    string VScriptElementId
    {
        get { return ViewState["ScriptElementId"] == null ? string.Empty : (string)ViewState["ScriptElementId"]; }
        set { ViewState["ScriptElementId"] = value; }
    }
    bool VBlockWithUs
    {
        get { return ViewState["VBlockWithUs"] == null ? false : (bool)ViewState["VBlockWithUs"]; }
        set { ViewState["VBlockWithUs"] = value; }
    }
    
}