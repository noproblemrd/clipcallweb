﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class Publisher_PaymentReportV4_5 : PublisherPage
{
    const string SUPPLIER_REVIEW_URL = @"~/Publisher/SupplierReviewReport.aspx?arid={0}";
    const string SUPPLIER_RATING_TEXT = "Supplier Rate: {0}";
    const string UNBOOKED = "Unbooked";
    const string PAGE_TITLE = "Payment Report";
    const int DAYS_INTERVAL = 7;

    protected const int ITEM_PAGE = 40;
    protected const int PAGE_PAGES = 10;
    public const string CONNECT_ACCOUNT_URL = @"applications/users/";
    //  static readonly string StripeBaseUrl;

    protected readonly string EXCEL_NAME = "ClipCallPaymentReport";

    protected readonly string SessionTableName = "data_CCPR";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            LoadOrigins();
            SetDateInterval();
            ExecRequestReport();
            //      Table_Report.Visible = true;
        }
        SetToolbox();
        Header.DataBind();
    }
    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecRequestReport();
    }
    private void LoadOrigins()
    {

        ddl_Origin.Items.Clear();
        ListItem liall = new ListItem("All", string.Empty);
        liall.Selected = true;
        ddl_Origin.Items.Add(liall);
        foreach (WebReferenceReports.eProjectOrigin _origin in Enum.GetValues(typeof(WebReferenceReports.eProjectOrigin)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(_origin), _origin.ToString());
            ddl_Origin.Items.Add(li);
        }
    }
    protected void ExecRequestReport()
    {
        Clearreports();

        DateTime _to = FromToDate1.GetDateTo;
        DateTime _from = FromToDate1.GetDateFrom;

        ClipCallReport.PaymentReportV3Request _request = new ClipCallReport.PaymentReportV3Request();
        _request.From = _from;
        _request.To = _to;
        _request.ProjectNumber = txtRequestId.Text.Trim();

        ClipCallReport.eProjectOrigin _origin;
        if (Enum.TryParse(ddl_Origin.SelectedValue, out _origin))
            _request.Origin = _origin;
        else
            _request.Origin = null;

        ClipCallReport.ResultOfListOfPaymentReportResponseV3_1 result = null;

        ClipCallReport.ClipCallReport _report = WebServiceConfig.GetClipCallReportReference(this);
        try
        {
            result = _report.GetPaymentReportV3_5(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        ClipCallReport.PaymentReportResponseV3_1[] values;
        if (!cb_IncludeQaTest.Checked)
        {
            var query = from x in result.Value
                        where !x.IsQaTest
                        select x;
            values = query.ToArray();
        }
        else
            values = result.Value;
        DataTable dt = new DataTable();
        //   dt.Columns.Add("PaymentDate");
        dt.Columns.Add("BookedDate");//
        dt.Columns.Add("CaseScript");//
        dt.Columns.Add("CaseNumber");//
        dt.Columns.Add("Origin");
        dt.Columns.Add("IncidentId");//
        dt.Columns.Add("IncidentAccountId");//
        dt.Columns.Add("CaseDate");//
        dt.Columns.Add("Amount");//
        dt.Columns.Add("Category");//
        dt.Columns.Add("CustomerName");//
        dt.Columns.Add("CustomerPhone");//
        dt.Columns.Add("CustomerEmail");//
        dt.Columns.Add("CustomerBonus");//
        dt.Columns.Add("SupplierName");//
        dt.Columns.Add("SupplierPhone");//
        dt.Columns.Add("IsInvited");//
        dt.Columns.Add("Refund");//

        dt.Columns.Add("PlanDiscount");//
        dt.Columns.Add("ClipCallFee");//
        dt.Columns.Add("Charged");//
        dt.Columns.Add("PastInstallments");//
        dt.Columns.Add("Balance");//
                                  //  dt.Columns.Add("TransferedAt");
        dt.Columns.Add("AmountTransfered");//
        dt.Columns.Add("StripeAccountId");//
        dt.Columns.Add("StripeAccountUrl");//
        //  dt.Columns.Add("FutureTransfer");
        //  dt.Columns.Add("IsFuture");
        dt.Columns.Add("PaymentPlan");//
        dt.Columns.Add("SupplierStatus");//
        dt.Columns.Add("OpenChat");//
        dt.Columns.Add("SupplierRequestReport");//
        dt.Columns.Add("ConsumerRequestReport");//
        dt.Columns.Add("ReversalTransferred");//
        dt.Columns.Add("SupplierRate");//
        dt.Columns.Add("SupplierRateUrl");//
        dt.Columns.Add("HasSupplierRate", typeof(bool));//
        dt.Columns.Add("ClipCallRate");//
        dt.Columns.Add("RequestPayment");//
        dt.Columns.Add("ScheduledDate");

        foreach (ClipCallReport.PaymentReportResponseV3_1 datarow in values)
        {
            DataRow row = dt.NewRow();
            row["BookedDate"] = string.Format(siteSetting.DateTimeFormat, datarow.BookedDate);//
            string _path = ResolveUrl("~/Publisher/ProjectPaymentDetails.aspx?ia=" + datarow.IncidentAccountId.ToString());//
            row["CaseScript"] = "javascript:OpenDetails('" + datarow.IncidentAccountId.ToString() + "', '" + _path + "');";//
            row["CaseNumber"] = datarow.CaseNumber;//
            row["Origin"] = datarow.SkinOrigin;
            row["IncidentId"] = datarow.IncidentId;//
            row["IncidentAccountId"] = datarow.IncidentAccountId;//
            row["CaseDate"] = string.Format(siteSetting.DateTimeFormat, datarow.CaseDate);//
            row["Amount"] = datarow.IsUnbooked ? UNBOOKED : string.Format("$" + NUMBER_FORMAT, datarow.Amount);//
            row["Refund"] = datarow.Refund.HasValue ? string.Format("$" + NUMBER_FORMAT, datarow.Refund.Value) : null;//
            row["Category"] = datarow.Category;//
            row["CustomerName"] = datarow.CustomerName;//
            row["CustomerEmail"] = datarow.CustomerEmail;//
            row["CustomerPhone"] = datarow.CustomerPhone;//
            row["ConsumerRequestReport"] = string.Format(Utilities.CUSTOMER_REQUEST_REPORT_URL, datarow.CustomerId);//
            row["CustomerBonus"] = datarow.IsUnbooked ? UNBOOKED : datarow.CustomerBonus == 0 ? null : string.Format("$" + NUMBER_FORMAT, datarow.CustomerBonus);//
            row["SupplierName"] = datarow.SupplierName;//
            row["SupplierPhone"] = datarow.SupplierPhone;//
            row["SupplierRequestReport"] = string.Format(Utilities.SUPPLIER_REQUEST_REPORT_URL, datarow.SupplierId);//
            row["SupplierStatus"] = datarow.SupplierStatus;//
            row["IsInvited"] = datarow.IsInvited ? "Is invited" : null;//
            row["ScheduledDate"] = datarow.ScheduledDate == default(DateTime) ? string.Empty : string.Format("{0:MM/d/yyyy HH:mm}", datarow.ScheduledDate);
            if (!datarow.IsUnbooked)
            {
                if (datarow.ClipCallFee.HasValue)
                    row["ClipCallFee"] = string.Format("$" + NUMBER_FORMAT, datarow.ClipCallFee.Value);//
                                                                                                       //            if (datarow.TransferedAt != DateTime.MinValue)
                                                                                                       //                 row["TransferedAt"] = string.Format(siteSetting.DateTimeFormat, datarow.TransferedAt);
                if (datarow.AmountTransfered.HasValue)
                    row["AmountTransfered"] = string.Format("$" + NUMBER_FORMAT, datarow.AmountTransfered.Value);//
            }
            row["ReversalTransferred"] = datarow.ReversalTransferred.HasValue ? string.Format("$" + NUMBER_FORMAT, datarow.ReversalTransferred.Value) : null;//
            row["StripeAccountId"] = datarow.StripeAccountId;//
            row["StripeAccountUrl"] = PpcSite.GetCurrent().StripeBaseUrl + CONNECT_ACCOUNT_URL + datarow.StripeAccountId;//
            if (datarow.AccountRatingId != Guid.Empty)
            {
                row["HasSupplierRate"] = true;//
                row["SupplierRate"] = string.Format(SUPPLIER_RATING_TEXT, string.Format(NUMBER_FORMAT, datarow.SupplierRating));
                row["SupplierRateUrl"] = string.Format(SUPPLIER_REVIEW_URL, datarow.AccountRatingId);//
            }
            else
                row["HasSupplierRate"] = false;//
            if (datarow.ClipCallRate.HasValue)
                row["ClipCallRate"] = datarow.ClipCallRate.Value;//
            /*
            if (datarow.IsUnbooked)
                row["IsFuture"] = "2";
            else if (datarow.FutureTransfer != DateTime.MinValue)
            {
                row["FutureTransfer"] = string.Format(siteSetting.DateTimeFormat, datarow.FutureTransfer);
                row["IsFuture"] = "1";
            }
            else
                row["IsFuture"] = "0";
             * */
            row["PaymentPlan"] = datarow.PaymentPlan.ToString();//
            row["Charged"] = string.Format("$" + NUMBER_FORMAT, datarow.Charged);//
            row["Balance"] = string.Format("$" + NUMBER_FORMAT, datarow.Balance);//
            row["PastInstallments"] = datarow.Installments;//
            row["PlanDiscount"] = string.Format("$" + NUMBER_FORMAT, datarow.PlanDiscount);//

            row["OpenChat"] = "return OpenChat('" + datarow.IncidentAccountId + "');";//
            row["RequestPayment"] = datarow.RequestPaymentStatuses;//
            dt.Rows.Add(row);
        }
        dataS = dt;

        //       Table_Report.Visible = true;
        lbl_RecordMached.Text = dt.Rows.Count + " " + ToolboxReport1.RecordMached;
        string totalAmount;
        if (dt.Rows.Count > 0)
            totalAmount = string.Format("$" + NUMBER_FORMAT, values.Sum(x => x.Amount));
        else
            totalAmount = string.Format("$" + NUMBER_FORMAT, 0);
        lbl_totalAmount.Text = totalAmount + " Total Amount";
        _reapeter.DataSource = dt;
        _reapeter.DataBind();
        _UpdatePanel.Update();

    }
    void Clearreports()
    {
        //        Table_Report.Visible = false;

        _reapeter.DataSource = null;
        _reapeter.DataBind();
        _UpdatePanel.Update();
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(PAGE_TITLE);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow row = (HtmlTableRow)(e.Item.FindControl("tr_show"));

        string css_class = (e.Item.ItemIndex % 2 == 0) ? "odd" : "even";

        row.Attributes.Add("class", css_class);
        /*
        string h_QuoteId = ((HiddenField)e.Item.FindControl("h_QuoteId")).Value;
        row.Attributes.Add("onclick", "OpenTrace(this, '" + h_QuoteId + "');");
        */
        /*
        Guid incidentAccountId = Guid.Parse(((HiddenField)e.Item.FindControl("hf_incidentAccountId")).Value);
        string _path = ResolveUrl("~/Publisher/ProjectPaymentDetails.aspx?ia=" + incidentAccountId);
        row.Attributes.Add("onclick", "OpenDetails('" + incidentAccountId.ToString() + "', '" + _path + "');");
        row.Style.Add("cursor", "pointer");
         * */
    }
   

    protected DataTable dataS
    {
        get { return Session[SessionTableName] == null ? new DataTable() : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
   
    protected string GetChatUrl
    {
        get { return "ClipCallLeadChat.aspx?ia="; }
    }
    protected string GetSetCheduledDateService
    {
        get { return ResolveUrl("~/Publisher/PaymentReportV4_5.aspx/SetCheduledDate"); }
    }
    [WebMethod(MessageName = "SetCheduledDate")]
    public static string SetCheduledDate(Guid incidentAccountId, string date)
    {
        ClipCallReport.ClipCallReport ccreport = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.SetIncidentAccountScheduledDateRequest request = new ClipCallReport.SetIncidentAccountScheduledDateRequest();
        request.IncidentAccountId = incidentAccountId;
        if (string.IsNullOrEmpty(date))
            request.ScheduledDate = default(DateTime);
        else
        {
            DateTime d;
            if (DateTime.TryParse(date, out d))
                request.ScheduledDate = d;
            else
                return JsonConvert.SerializeObject(new { IsSuccess = false, Message = "Datetime format error" });
        }
       


        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = ccreport.SetIncidentAccountScheduledDate(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            //     ClipCallReport.SetLeadAssessmentResponse _res = new ClipCallReport.SetLeadAssessmentResponse() { IsSuccess = false };
            return JsonConvert.SerializeObject(new { IsSuccess = false, Message = exc.Message });
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            //     ClipCallReport.SetLeadAssessmentResponse _res = new ClipCallReport.SetLeadAssessmentResponse() { IsSuccess = false };
            return JsonConvert.SerializeObject(new { IsSuccess = false, Message = result.Messages[0] });
        }
        var response2 = new { IsSuccess = result.Value, Message = "General error" };
        return JsonConvert.SerializeObject(response2);
    }
    /*
    private const string PaymentTrace_PATH = "~/Publisher/ClipCallControls/PaymentTrace.ascx";
    [WebMethod(MessageName = "GetPaymentTrace")]
    public static string GetPaymentTrace(Guid quoteId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);

        ClipCallReport.ResultOfListOfPaymentTraceResponse result = null;
        try
        {
            result = report.GetPaymentTrace(quoteId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return null;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return null;
        }
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(PaymentTrace_PATH);
        Type CtlType = ctl.GetType();
        MethodInfo LoadData = CtlType.GetMethod("LoadData");
        LoadData.Invoke(ctl, new object[] { result.Value });

        ctl.EnableViewState = false;
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();

    }
    */
}