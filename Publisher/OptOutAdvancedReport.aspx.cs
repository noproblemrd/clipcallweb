﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Publisher_OptOutAdvancedReport : PageSetting
{
    const int DAYS_INTERVAL = 2;
    protected void Page_Load(object sender, EventArgs e)
    {
     //   TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(btn_Run);
        if (!IsPostBack)
        {
            dataV = null;
    //        ConversionReportRequestV = null;
            SetToolbox();
            SetDatePicker();
            LoadOrigins();
            RunReport();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    /*
    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }
    */
    private void LoadOrigins()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result;
        try
        {
            result = _site.GetAllOrigins(false);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        ddl_Origin.Items.Clear();
        ddl_Origin.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
            ddl_Origin.Items.Add(new ListItem(gsp.Name, gsp.Id.ToString()));
        ddl_Origin.SelectedIndex = 0;

    }
    void SetDatePicker()
    {
   //     FromToDatePicker_current.SetFromSentence = lbl_CreationDate.Text;
        FromToDatePicker_current.SetIntervalDateServer(DAYS_INTERVAL);
    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lbl_TitleReport.Text);
    }
    
    private void SetToolboxEvents()
    {

        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, lbl_TitleReport.Text);

        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordToPrint + "');", true);
            return;
        }
        Session["data_print"] = dataV;
        Session["grid_print"] = (_GridViewRegular.Visible) ? _GridViewRegular : _GridViewAdvanced;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    
    protected void btn_Run_Click(object sender, EventArgs e)
    {
        RunReport();
    }
    void RunReport()
    {
        WebReferenceReports.OptOutReportsRequest _request = new WebReferenceReports.OptOutReportsRequest();
        DateTime _from = FromToDatePicker_current.GetDateFrom;
        DateTime _to = FromToDatePicker_current.GetDateTo.AddDays(1);
        Guid OriginId = new Guid(ddl_Origin.SelectedValue);
        if (ddl_TypeReport.SelectedValue == "Regular")
            RunRegularReport(_from, _to, OriginId);
        else
            RunAdvacedReport(_from, _to, OriginId);
    }
    void RunRegularReport(DateTime _from, DateTime _to, Guid OriginId)
    {
        _GridViewAdvanced.Visible = false;
        _GridViewRegular.Visible = true;
        /*
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfStringIntPair result;
        try
        {
            result = _report.OptOutRegularReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if(result.Value.Length==0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordToPrint) + "');", true);
         * */
        DataTable data = new DataTable();
        data.Columns.Add("Str");
        data.Columns.Add("Int");
        string command = "EXEC [dbo].[GetWidgetRemoveRegular] @from, @to, @OriginId";
        try
        {
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@from", _from);
                    cmd.Parameters.AddWithValue("@to", _to);
                    if (OriginId == Guid.Empty)
                        cmd.Parameters.AddWithValue("@OriginId", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@OriginId", OriginId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DataRow row = data.NewRow();
                        row["Str"] = ((string)reader["RemovePeriod"]).Replace("_", " ").Trim();
                        row["Int"] = (int)reader["optOutCount"];
                        data.Rows.Add(row);
                    }
                    conn.Close();
                }
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            data = null;
        }
        dataV = data;
        _GridViewRegular.DataSource = data;
        _GridViewRegular.DataBind();
        _UpdatePanel.Update();
    }

    void RunAdvacedReport(DateTime _from, DateTime _to, Guid OriginId)
    {
        _GridViewAdvanced.Visible = true;
        _GridViewRegular.Visible = false;

        DataTable data = new DataTable();
        data.Columns.Add("Keyword");
        data.Columns.Add("Flavor");
        data.Columns.Add("LeadCount");
        data.Columns.Add("OptOutCount");
        string command = (OriginId == Guid.Empty) ?
            "EXEC [dbo].[GetWidgetRemoveAdvanced] @from, @to" :
            "EXEC [dbo].[GetWidgetRemoveAdvanced_Origin] @from, @to, @OriginId";

        try
        {
            using (SqlConnection conn = DBConnection.GetConnString())
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@from", _from);
                    cmd.Parameters.AddWithValue("@to", _to);
                    if (OriginId != Guid.Empty)
                        cmd.Parameters.AddWithValue("@OriginId", OriginId);
                                           
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DataRow row = data.NewRow();
                        row["Keyword"] = (string)reader["Keyword"];
                        row["Flavor"] = (string)reader["ControlName"];
                        row["LeadCount"] = (int)reader["CountIncident"];
                        row["OptOutCount"] = (int)reader["RemoveCount"];
                        data.Rows.Add(row);
                    }
                    conn.Close();
                }
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            data = null;
        }
        dataV = data;
        _GridViewAdvanced.DataSource = data;
        _GridViewAdvanced.DataBind();
        _UpdatePanel.Update();

    }
    DataTable dataV
    {
        get { return (Session["data"] == null) ? null : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    
   
}