﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="HeartBeatInjections.aspx.cs" Inherits="Publisher_HeartBeatInjections" %>

<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="Stylesheet" href="InjectionCampaign.css" type="text/css" />
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
    <style type="text/css">
        #chart_div
        {
            top: 680px; 
            position:absolute;
           
        }
    </style>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js"></script>
     <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript">
       
        
        $(function () {
            var _div = document.getElementById('chart_div');
            var _left = (screen.width - 1200) / 2;
            if (_left < 1)
                _left = 0;
            _div.style.left = _left + 'px';

            $('#<%# rbl_Origins.ClientID %>').find('input[type="radio"]').each(function () {
                $(this).change(function () {
                    SetBelongInjections(this);
                    /*
                    if (this.value == '<# Guid.Empty.ToString() %>') {
                        ClearOrigins();
                        RemoveAllBelongInjections();
                        return;
                    }
                    GetInjectionsByOrigin(this.value);
                */
                });
                if(this.checked)
                    SetBelongInjections(this);
            });
            $('#<%# cbl_Injection.ClientID %>').find('input[type="checkbox"]').each(function () {
                $(this).change(function () {
                    if (this.checked) {
                        var _id = GetInjectionValue(this);
                        if (_id == '<%# Guid.Empty.ToString() %>')
                            ClearInjections();
                        else
                            ClearInjectionAll();
                    }
                    else {
                        var options = document.getElementById('<%# cbl_Injection.ClientID %>').getElementsByTagName('input');
                        var IsSign = false;
                        var CheckBoxAll;
                        for (var i = 0; i < options.length; i++) {
                            var _id = GetInjectionValue(options[i]);
                            if (_id == '<%# Guid.Empty.ToString() %>')
                                CheckBoxAll = i;
                            if (options[i].checked) {
                                IsSign = true;
                                break;
                            }
                        }
                        if (!IsSign)
                            options[CheckBoxAll].checked = true;
                    }

                });
            });

        });
        function SetBelongInjections(_this) {
            if (_this.value == '<%# Guid.Empty.ToString() %>') {
                ClearOrigins();
                RemoveAllBelongInjections();
                return;
            }
            GetInjectionsByOrigin(_this.value);

        }
        function GetInjectionValue(InputInjection) {
            return $(InputInjection).parent('span').attr('guid');
        }
        function ClearInjectionAll() {
            var options = document.getElementById('<%# cbl_Injection.ClientID %>').getElementsByTagName('input');
            for (var i = 0; i < options.length; i++) {
                var _val = GetInjectionValue(options[i]);
                if (_val == '<%# Guid.Empty.ToString() %>') {
                    options[i].checked = false;
                    return;
                }
            }
        }
       
        function ClearOrigins() {
            var options = document.getElementById('<%# rbl_Origins.ClientID %>').getElementsByTagName('input');
            for (var i = 0; i < options.length; i++) {
                var _val = options[i].value;//GetInjectionValue(options[i]);
                if (_val == '<%# Guid.Empty.ToString() %>')
                    continue;            
                options[i].checked = false;            
            }
        }
        function ClearInjections() {
            var options = document.getElementById('<%# cbl_Injection.ClientID %>').getElementsByTagName('input');
            for (var i = 0; i < options.length; i++) {
    //            $(options[i]).parent('span').removeClass('BelongInjection');
                
                var _val = GetInjectionValue(options[i]);
                if (_val != '<%# Guid.Empty.ToString() %>')
                    options[i].checked = false;
                    
            }
        }
        function RemoveAllBelongInjections() {
            var options = document.getElementById('<%# cbl_Injection.ClientID %>').getElementsByTagName('input');
            for (var i = 0; i < options.length; i++) 
                $(options[i]).parent('span').removeClass('BelongInjection');                
        }
        function GetInjectionsByOrigin(OriginID) {
            //  showDiv();
            $.ajax({
                url: '<%# GetInjectionsByOrigin %>',
                data: "{ OriginId: '" + OriginID + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {

                    var options = document.getElementById('<%# cbl_Injection.ClientID %>').getElementsByTagName('input');
                    var campaigns = (data.d.length == 0) ? [''] : data.d.split(';');
                    for (var i = 0; i < options.length; i++) {
                        var _id = $(options[i]).parent('span').attr('guid');
                        var IsFindId = false;
                        for (var j = 0; j < campaigns.length; j++) {
                            if (campaigns[j] == _id) {
                                IsFindId = true;
                                break;
                            }
                        }
                        //   options[i].checked = IsFindId;
                        if (IsFindId)
                            $(options[i]).parent('span').addClass('BelongInjection');
                        else
                            $(options[i]).parent('span').removeClass('BelongInjection');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    /*
                    var _log = '';
                    for (str in XMLHttpRequest) {
                    _log += str + '=' + XMLHttpRequest[str] + ";";
                    }
                    console.log(_log);
                    */
                },
                complete: function (jqXHR, textStatus) {

                 //   hideDiv();
                }

            });
        }
    </script>
    <script type="text/javascript">

        // Load the Visualization API and the piechart package.
        google.load('visualization', '1.0', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = new google.visualization.DataTable();
            
            data.addColumn('date', 'X');
            <%# GetColumns() %>
            
           
            data.addRows([
              <%# ChartDataString() %>
            ]);


            var options = {
                width: 1200,
                height: 500,
                hAxis: {
                    title: 'Date/Time'//,
                  //  format: 'MMM dd, y, HH:mm',
                },
                vAxis: {
                    title: '<%# Y_Chart %>'
                },
                series: {
                    1: { curveType: 'function' }
                }
                 
            };
            var date_formatter = new google.visualization.DateFormat({
                pattern: "MMM dd, yyyy HH:mm"
            });
            date_formatter.format(data, 0);
            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" >    
    </cc1:ToolkitScriptManager>
    <uc1:ToolboxReport ID="_Toolbox" runat="server" />
     <div class="page-content minisite-content2">  	       
           <div class="form-MultiSelect">   
             <div class="form-field form-field-select">
                <div class="form-select-titles">
                    <div>
                        <asp:Label ID="lbl_Injection" runat="server" CssClass="label" Text="Injection"></asp:Label>   
                    </div>
                   
                </div>
                <div class="clear"></div>
                <div class="div_ItemList div_ItemList_left">                   
                    <asp:CheckBoxList ID="cbl_Injection" runat="server">
                    </asp:CheckBoxList>
                </div>   
            </div>                     
            <div class="form-field form-field-select">
                 <div class="form-select-titles">
                    <div>
                        <asp:Label ID="lbl_Origin" runat="server" CssClass="label div_ItemList_right" Text="Campaign"></asp:Label>    
                    </div>
                    
                </div>                
                <div class="clear"></div>
                <div class="div_ItemList div_ItemList_right">   
                    <asp:RadioButtonList ID="rbl_Origins" runat="server" >
                    </asp:RadioButtonList>
                   
                </div>                 
            </div>            
        </div>
        <div class="clear"></div>
        <div class="callreport">              
                <uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" />
        </div>
         <div class="form-field"> 
             <asp:Label ID="lbl_ChartType" runat="server" Text="Type" CssClass="label"></asp:Label>  
            <asp:RadioButtonList ID="rbl_ChartType" runat="server" RepeatDirection="Horizontal">
            </asp:RadioButtonList>
                   
        </div>      
        <div class="clear"></div>
            <div class="div_btn_UpdateChart" style="text-align: center;">
            <asp:Button ID="btn_UpdateChart" runat="server" CssClass="CreateReportSubmit2" 
                Text="Update Chart" onclick="btn_UpdateChart_Click" />
        </div>
        <div class="clear"></div>
            
    </div>
        
                
            
    
    <div id="chart_div" style="width:1200px; height:500px;"></div>
    <asp:Label ID="lbl_TitleReport" runat="server" Text="Heart Beat injections" Visible="false"></asp:Label>
</asp:Content>

