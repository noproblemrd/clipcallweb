﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Publisher_DeskAppIps : PageSetting
{

    private const string loadQuery = @"
SELECT *
  FROM DesktopAppForceInstallConfig
";

    private const string deleteQuery = @"
delete from DesktopAppForceInstallConfig
where IP = @ip
";

    private const string updateQuery = @"
update DesktopAppForceInstallConfig
set FromDateTime = @from
, ToDateTime = @to
, Inject = @inject
, Comment = @comment
where IP = @ip
";

    private const string insertQuery = @"
insert into DesktopAppForceInstallConfig (IP, FromDateTime, ToDateTime, Inject, Comment) values (@ip, @from, @to, @inject, @comment)
";

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(btn_Set);
        if (!IsPostBack)
        {
            SetToolBox();
            DataBindTopage();
            LoadCurrentIps();
            __Search();
        }
        SetToolBoxEvents();
        Header.DataBind();
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }

    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }

    private void __Search()
    {
        string name = Toolbox1.GetTxt;
        HtmlInputText _tb = Toolbox1.GetTextBoxSearch;
        if (name == Toolbox1.GetWaterTextSearch &&
                    _tb.Attributes["class"].Contains("watermark"))
            name = string.Empty;
        if (string.IsNullOrEmpty(name))
            dataViewV = dataV;
        else
        {
            name = name.ToLower();
            DataTable data = dataV;
            data.TableName = "Search";

            EnumerableRowCollection<DataRow> query = from Search in data.AsEnumerable()
                                                     where Search.Field<string>("Ip").ToLower().Contains(name)
                                                     orderby Search.Field<string>("Ip")
                                                     select Search;

            dataViewV = (query.Count() == 0) ? null : query.CopyToDataTable();
        }
        _GridView.PageIndex = 0;
        SetGridView();
    }

    private void LoadCurrentIps()
    {
        List<IpContainer> ips = new List<IpContainer>();
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(loadQuery, conn))
            {
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ips.Add(new IpContainer()
                    {
                        Ip = (string)reader["IP"],
                        From = (DateTime)reader["FromDateTime"],
                        To = (DateTime)reader["ToDateTime"],
                        Inject = (bool)reader["Inject"],
                        Comment = Convert.ToString(reader["Comment"])
                    });
                }
            }
        }
        dataV = GetDataTable.GetDataTableFromList<IpContainer>(ips);
    }

    public class IpContainer
    {
        public string Ip { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public bool Inject { get; set; }
        public string Comment { get; set; }
    }

    private void DataBindTopage()
    {
        popUpadd.DataBind();
    }

    private void SetToolBox()
    {
        Toolbox1.RemoveEdit();
        Toolbox1.RemoveAttach();
        Toolbox1.RemoveExcel();
        Toolbox1.RemovePrint();
        string script = "openPopup(); return false;";
        Toolbox1.SetClientScriptToAdd(script);
        //      Toolbox1.SetConfirmDelete = lbl_Delete.Text;
    }

    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
        ////     Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        //Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        //Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        //Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
    }

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        List<string> list = new List<string>();
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            if (cb.Checked)
            {
                string ip = ((Label)row.FindControl("lbl__ip")).Text;
                list.Add(ip);
            }
        }
        if (list.Count == 0)
            return;

        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            foreach (var ip in list)
            {
                using (SqlCommand cmd = new SqlCommand(deleteQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@ip", ip);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        Toolbox1.GetTextBoxSearch.Value = string.Empty;
        LoadCurrentIps();
        __Search();
    }

    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        __Search();
    }

    protected void btn_Set_Click(object sender, EventArgs e)
    {
        string ip = txt_IP.Text;
        DateTime from = DateTime.Parse(txt_From.Text);
        DateTime to = DateTime.Parse(txt_To.Text);
        bool inject = cb_Inject.Checked;
        string comment = txt_Comment.Text;

        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            int changes;
            using (SqlCommand cmd = new SqlCommand(updateQuery, conn))
            {
                cmd.Parameters.AddWithValue("@ip", ip);
                cmd.Parameters.AddWithValue("@from", from);
                cmd.Parameters.AddWithValue("@to", to);
                cmd.Parameters.AddWithValue("@inject", inject);
                cmd.Parameters.AddWithValue("@comment", comment);
                changes = cmd.ExecuteNonQuery();
            }
            if (changes == 0)
            {
                using (SqlCommand cmd = new SqlCommand(insertQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@ip", ip);
                    cmd.Parameters.AddWithValue("@from", from);
                    cmd.Parameters.AddWithValue("@to", to);
                    cmd.Parameters.AddWithValue("@inject", inject);
                    cmd.Parameters.AddWithValue("@comment", comment);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        popUpadd.Attributes.Add("class", "popModal_del popModal_del_hide");
        LoadCurrentIps();
        __Search();
        _mpe.Hide();
    }

    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView();
    }

    void SetGridView()
    {
        StringBuilder sb = new StringBuilder();
        DataView data = dataViewV.AsDataView();
        _GridView.DataSource = data;
        _GridView.DataBind();

        if (data.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noreasultSearch", "alert('" + HttpUtility.JavaScriptStringEncode(Master.GetNoResultsMessage) + "');", true);
            CBsV = string.Empty;
            _updatePanelTable.Update();
            return;
        }
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }

        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        _updatePanelTable.Update();
    }

    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }

    private DataTable dataViewV
    {
        get { return (Session["dataView"] == null) ? new DataTable() : (DataTable)Session["dataView"]; }
        set { Session["dataView"] = value; }
    }

    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
}