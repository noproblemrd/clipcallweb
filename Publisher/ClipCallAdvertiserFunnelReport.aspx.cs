﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallAdvertiserFunnelReport : PageSetting
{
    const int ONE_DAY = 7;
    protected readonly string SessionTableName = "AdvertiseFunnelReportData";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        WebReferenceReports.FromToDateTimeRequest _request = new WebReferenceReports.FromToDateTimeRequest();
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(this);
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo;
       
        WebReferenceReports.ResultOfFunnelAdvertiserReportResponse result = null;
        try
        {
            result = reports.ClipCallFunnelAdvertiserReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if (result.Value == null)
        {
            _GridView.DataSource = null;
            _GridView.DataBind();
            dataV = null;
            return;
        }
        DataTable data = new DataTable();
        data.Columns.Add("Sum");
        data.Columns.Add("IOS");
        data.Columns.Add("Android");
        data.Columns.Add("IosFromAar");
        data.Columns.Add("AndroidFromAar");
       
        DataRow row = data.NewRow();
        row["Sum"] = result.Value.Sum;
        row["IOS"] = result.Value.IOS;
        row["Android"] = result.Value.ANDROID;
        row["IosFromAar"] = result.Value.IOS_AAR;
        row["AndroidFromAar"] = result.Value.ANDROID_AAR;       
        data.Rows.Add(row);

        _GridView.DataSource = data;
        _GridView.DataBind();
        dataV = data;
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle("Advertiser Complete Registration Report");
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
}