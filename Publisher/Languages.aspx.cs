﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Publisher_Languages : PageSetting
{
    List<string> _list = new List<string>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadLanguages();
            LoadSiteLanguages();                        
            LoadDefaultLang();
        }
        string stam = ClientScript.GetPostBackEventReference(btn_set, this.ToString());
    }
    
    protected override void Render(HtmlTextWriter writer)
    {
        
        foreach (string str in _list)
        {
                
            ClientScript.RegisterForEventValidation(new PostBackOptions(selectLanguages, str));
            ClientScript.RegisterForEventValidation(new PostBackOptions(selectLanguages2, str));
            ClientScript.RegisterForEventValidation(new PostBackOptions(ddl_chooseLang, str));
    //        ClientScript.RegisterForEventValidation(btn_set.UniqueID);
        }
    //    ClientScript.RegisterForEventValidation(btn_set.UniqueID);
        base.Render(writer);
    }
    
   

    private void LoadDefaultLang()
    {
        int langId = -1;
        string command = "SELECT dbo.GetLangIdDefaultBySiteNameId(@SiteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            langId = (int)cmd.ExecuteScalar();
            conn.Close();
        }
        for (int i = 0; i < ddl_chooseLang.Items.Count; i++)
        {
            if (langId.ToString() == ddl_chooseLang.Items[i].Value)
            {
                ddl_chooseLang.SelectedIndex = i;
                hf_selectedIndex.Value = i.ToString();
                break;
            }
        }           
    }

    private void LoadSiteLanguages()
    {
        string hf = string.Empty;
        selectLanguages2.Items.Clear();
        ddl_chooseLang.Items.Clear();
        string command = "EXEC dbo.GetLanguagesBySiteNameId @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string _id = "" + (int)reader["LanguageId"];
                string _name = (string)reader["LanguageName"];
                selectLanguages2.Items.Add(new ListItem(_name, _id));
                ddl_chooseLang.Items.Add(new ListItem(_name, _id));
                hf += _id + "," + _name + ";";
                for (int i = 0; i < selectLanguages.Items.Count; i++)
                {
                    if (_id == selectLanguages.Items[i].Value)
                    {
                        selectLanguages.Items.RemoveAt(i);
                        break;
                    }
                }
            }
            conn.Close();
        }
        if (hf.Length > 0)
            hf = hf.Substring(0, hf.Length - 1);
        hf_source.Value = hf;
    }

    private void LoadLanguages()
    {
        
        selectLanguages.Items.Clear();
        string command = "EXEC dbo.GetLanguages";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string _lang = (string)reader["LanguageName"];
                string _id = "" + (int)reader["Id"];
                selectLanguages.Items.Add(new ListItem(_lang, _id));
                _list.Add(_id);

            }
            conn.Close();
        }
        
    }
    protected void btn_set_Click(object sender, EventArgs e)
    {
        int defaultlang = int.Parse(hf_selected.Value);
        string command = "EXEC dbo.SetLanguagesBySiteNameId @SiteNameId, " +
                "@Languages, @DefaultLang, @Delimiter";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@Languages", GetLangsValues());
            cmd.Parameters.AddWithValue("@DefaultLang", defaultlang);
            cmd.Parameters.AddWithValue("@Delimiter", ";");
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
        LoadLanguages();
        LoadSiteLanguages();
        LoadDefaultLang();
        ClientScript.RegisterStartupScript(this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
    }
    string GetLangsValues()
    {
        return hf_array.Value;
        /*
        string result = string.Empty;
        foreach (ListItem li in selectLanguages2.Items)
        {
            result = li.Value + ";";
        }
        return result;
         * */
    }

   
}
