﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" 
AutoEventWireup="true" CodeFile="RequestReport.aspx.cs" Inherits="Publisher_RequestReport" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
 

<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
 
<script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>
<script  type="text/javascript" src="../Calender/Calender.js"></script>

<link media="screen" rel="stylesheet" href="../jquery/colorbox.css" type="text/css" />
<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../scripts/jquery-ui-1.8.17.custom.autocomplete.min.js"></script>

<script src="../jquery/jquery.colorbox1.3.19.js" type="text/javascript" ></script>	   
<link href="../Formal/jquery-ui-1.8.16.custom.css" type="text/css" rel="Stylesheet" />
<script type="text/javascript">
    
    var message;
    window.onload = appl_init;
    
    function appl_init() {        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
        message = document.getElementById('<%#Hidden_IfToExite.ClientID%>').value;
        
    }
    function BeginHandler()
    {       
  //     alert('in1');
        showDiv();
    }
    function EndHandler()
    {
  //      alert('out1');
        hideDiv();
    }
    function OpenAdvertiserIframe(_path)
    {
        $(document).ready(function(){$.colorbox({href:_path,width:'95%', height:'90%', iframe:true});});
    }
    function openAdvIframe(_id)
    {            
         var url = "ConsumerDetails.aspx?Consumer="+_id;
         $(document).ready(function () { $.colorbox({ href: url, width: '1000px', height: '90%', iframe: true }) });
    }
    /***   IFRAME  ****/
    function CloseIframe()
    {
       $find('_modal').hide();
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie popModal_del2_hide";
       document.getElementById("<%# _iframe.ClientID %>").src = "javascript:void(0);";
    }
    function OpenIframe(_path)
    {    
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie";
       document.getElementById("<%# _iframe.ClientID %>").src = _path;
       HideIframeDiv();  
       $find('_modal').show();
    }
    function ShowIframeDiv()
    {
        document.getElementById("divIframeLoader").style.display = "none";
       document.getElementById("div_iframe_main").style.display = "block";
    }
    function HideIframeDiv()
    {
    
        document.getElementById("divIframeLoader").style.display = "block";
       document.getElementById("div_iframe_main").style.display = "none";
   }
   var _noproblem = _noproblem || {};
   _noproblem.IsSelected = false;
   $(function () {
       $("._Url").autocomplete({

           source: function (request, response) {

               $.ajax({
                   url: "<%# SuggestNewList %>",
                   data: "{ 'str': '" + request.term + "', 'classList': 'Url' }",
                   dataType: "json",
                   type: "POST",
                   contentType: "application/json; charset=utf-8",
                   dataFilter: function (data) { return data; },
                   success: function (data) {

                       response($.map(data.d, function (item) {
                           return {
                               value: item
                           }
                       }))
                   },
                   error: function (XMLHttpRequest, textStatus, errorThrown) {
                       //              alert(textStatus);
                   },
                   complete: function (jqXHR, textStatus) {
                       //  HasInHeadingRequest = false;
                   }

               });
           },
           minLength: 1,
           close: function (event, ui) {
               //      if (!_noproblem.IsSelected && event.target.value.length != 0) {
               //             SlideElement("div_ServiceComment", true); // document.getElementById("div_ServiceComment").style.display = "block";
               //               $('#div_ServiceError').show();
               //       }
               GetOneUrl();
               _noproblem.IsSelected = false;
           },
           /*
           open: function (event, ui) {
           SlideElement("div_ServiceComment", false); // document.getElementById("div_ServiceComment").style.display = "none";
           $('#div_ServiceError').hide();
           },
           */
           select: function (event, ui) {
               //                SlideElement("div_ServiceComment", false); // document.getElementById("div_ServiceComment").style.display = "none";
               //                 $("#div_ServiceSmile").show();
               //                 $('#div_ServiceError').hide();
               _noproblem.IsSelected = true;
           }
       });
   });
 //  $(function () {
 //      $('#<%# txt_Url.ClientID %>').blur(function () {
   function GetOneUrl() {
       var _this = $('#<%# txt_Url.ClientID %>');
       var _txt = $.trim(_this.val());
       _this.val(_txt);
       if (_txt.length == 0)
           return;
       $.ajax({
           type: "POST", //GET or POST or PUT or DELETE verb
           url: "<%# GetNewOneSuggest %>", // Location of the service   "PhoneService.svc/GetNumbers"
           data: "{ 'str': '" + _txt + "', 'classList': 'Url' }", //Data sent to server
           //   data: "{ 'str': 'http://10.0.0.71/NoProblemWebSite/slider/supplierIframe.aspx?SiteId=2&ExpertiseCode=1&ExpertiseLevel=1&ExpertiseName=&RegionCode=19724&RegionLevel=3&RegionName=&origionId=%7BCF879946-455C-DF11-80CD-0003FF727321%7D&waterMarkComment=&PageName=&PlaceInWebSite=&ControlName=%20flavour%20a&Domain=&Url=&Keyword=', 'classList': 'Url' }",

           contentType: "application/json; charset=utf-8", // content type sent to server
           dataType: "json", //Expected data format from server
           processdata: false, //True or False          
           success: function (result) {//On Successfull service call                            
               if (result.d)
                   _this.val(result.d);
               else
                   _this.val('');
           },
           error: function (jqXHR, textStatus, errorThrown) {
               alert(textStatus);
               //            EndLoader(_this);
           }
       });
   }
 //      });
//   });
</script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" >
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
          
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	       
               <div class="main-inputs" runat="server" id="PublisherInput">					
	                <asp:UpdatePanel id="_UpdatePanelSearch" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>   
                        <div id="div_experties" class="form-field" runat="server">
					            <label for="form-subsegment" class="label" runat="server" id="lblExperties" >Heading</label>
                            <asp:DropDownList ID="ddl_Heading" runat="server" CssClass="form-select">
                            </asp:DropDownList>
					            
			            </div>
            			 
			            <div class="form-field">
                          <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Request Id</label>
			              <asp:TextBox ID="txtRequestId" CssClass="form-text" runat="server" ></asp:TextBox>										
                          <asp:CompareValidator runat="server" Type="Integer"  Operator="DataTypeCheck" ControlToValidate="txtRequestId" Display="Dynamic">
                             <asp:Label ID="lbl_RequestIdError" runat="server" Text="Must be number" Visible="true"></asp:Label>
                          </asp:CompareValidator> 
                       </div>            
                    
                    </ContentTemplate>
		            </asp:UpdatePanel>
		            <div class="clear"></div>
		            <div class="form-field form-group">
		             <asp:Label ID="lbl_type" CssClass="label" runat="server" Text="Type"></asp:Label> 
                        <asp:DropDownList ID="ddl_Type"  CssClass="form-select" runat="server">
                        </asp:DropDownList>
        		    
        		    </div>
                    <div class="form-field" runat="server" id="div_NumberOfAdvertisersProvided">
                        <asp:Label ID="lbl_NumSupplier" runat="server" Text="<%# lbl_NumberOfAdvertisersProvided.Text %>" CssClass="label"></asp:Label>
                        <asp:TextBox ID="txt_NumSupplier" runat="server" CssClass="form-text"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator_NumSupplier" runat="server" ErrorMessage="Must be numeric more than zero" CssClass="error-msg"
                         ControlToValidate="txt_NumSupplier" Type="Integer" MaximumValue="<%# short.MaxValue %>" MinimumValue="0"></asp:RangeValidator>

                    </div>
                     <div class="div_affiliate" runat="server" id="div_affiliate">
                        <asp:Label ID="lbl_Origin" CssClass="label" runat="server" Text="Origin"></asp:Label>                                    
                        <asp:DropDownList ID="ddl_Affiliate" CssClass="form-select" runat="server" AutoPostBack="true"
                             onselectedindexchanged="ddl_Affiliate_SelectedIndexChanged"></asp:DropDownList>			
			        </div>
                   <div class="form-url" runat="server" id="div_url">
                        <asp:Label ID="lbl_Url" runat="server" Text="URL" CssClass="label"></asp:Label>
                        <asp:TextBox ID="txt_Url" runat="server" CssClass="form-text _Url"></asp:TextBox>
                       

                    </div>
                    <div class="clear"></div>
                     <div class="form-field" runat="server" id="div_NumberOfUpsales">
                        <asp:Label ID="lbl_NumberOfUpsales" runat="server" Text="Number of upsales" CssClass="label"></asp:Label>
                        <asp:DropDownList ID="ddl_NumberOfUpsales" CssClass="form-select" runat="server"></asp:DropDownList>

                    </div>
                   <div class="form-field" runat="server" id="div_SliderType">
                        <asp:Label ID="lbl_SliderType" runat="server" Text="Slider Type" CssClass="label"></asp:Label>
                        <asp:DropDownList ID="ddl_SliderType" CssClass="form-select" runat="server"></asp:DropDownList>

                    </div>
		            <div class="form-field form-group" runat="server" id="div_OnlyUpsale" visible="false"> 
                         <asp:Label ID="lbl_OnlyUpsale" runat="server" Text="Only upsale available" CssClass="label"></asp:Label>
                        <asp:CheckBox ID="cb_OnlyUpsale" runat="server" />
                    </div>
                    <div class="form-field form-WidthDuration" runat="server" id="div_IncludeMaxDuration"> 
                         <asp:Label ID="lbl_IncludeMaxDuration" runat="server" Text="Include max call duration" CssClass="label"></asp:Label>
                        <asp:DropDownList ID="ddl_IncludeMaxDuration"  CssClass="form-select" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-field form-WidthRejectByEmailOrName" runat="server" id="div_OnlyRejectByEmailOrName"> 
                         <asp:Label ID="lbl_OnlyRejectByEmailOrName" runat="server" Text="Reject By Email/Name" CssClass="label"></asp:Label>
                        <asp:CheckBox ID="cb_OnlyRejectByEmailOrName" runat="server" />
                    </div>
		           </div> 
        		   
                    <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>        
                 
		  
    		
    	
    	</div>
    	<div style="clear:both;"></div>
    	
	        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	            <ContentTemplate>	
                    <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
	                <div id="Table_Report"  runat="server" >
                              
                                <asp:Repeater runat="server" ID="_reapeter" 
                                    onitemdatabound="_reapeter_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="data-table">
                                        
                                            <thead>
                                            <tr>
                                                
                                                <th><asp:Label ID="lbl_Date" runat="server" Text="<%#lbl_Date.Text%>"></asp:Label></th>
			                                    <th><asp:Label ID="Label51" runat="server" Text="<%#lbl_Id.Text%>"></asp:Label></th>
			                                    <th><asp:Label ID="Label001" runat="server" Text="<%#lbl_Region.Text%>"></asp:Label></th>			                                    
			                                    <th><asp:Label ID="Label52" runat="server" Text="<%#lbl_Heading.Text%>"></asp:Label></th>			                                    
			                                    <th><asp:Label ID="Label53" runat="server" Text="<%#lbl_Consumer.Text%>"></asp:Label></th>			                                    
			                                    <th runat="server" id="th_NumberOfRequestedAdvertisers" visible="<%#  ToShow %>"><asp:Label ID="Label1" runat="server" Text="<%#lbl_NumberOfRequestedAdvertisers.Text%>"></asp:Label></th>
			                                    <th runat="server" id="th_NumberOfAdvertisersProvided" visible="<%#  ToShow %>"><asp:Label ID="Label5" runat="server" Text="<%#lbl_NumberOfAdvertisersProvided.Text%>" ></asp:Label></th>			                                    
			                                    <th runat="server" id="th_Revenue" visible="<%#  ToShow %>"><asp:Label ID="Label6" runat="server" Text="<%#lbl_Revenue.Text%>" ></asp:Label></th>
                                                <th><asp:Label ID="Label1112" runat="server" Text="<%#lbl_Status.Text%>" ></asp:Label></th>
                                                <th runat="server" visible="<%# with_upsale %>"><asp:Label ID="Label142" runat="server" Text="<%#lbl_Upsales.Text%>" ></asp:Label></th>
                                                <th runat="server" visible="<%# with_duration %>"><asp:Label ID="Label992" runat="server" Text="<%#lbl_MaxCallDuration.Text%>" ></asp:Label></th>

			                                </tr>
                                            </thead>
			                            </HeaderTemplate>
	                                    <ItemTemplate>
                                            <tr id="tr_show" runat="server">
                                            
                                                
                                                 
			                                     <td>
			                                        <asp:Label ID="lbl_name" runat="server" Text="<%# Bind('Date') %>"></asp:Label>      
			                                     </td>            			                         
			                                     <td>
                                                    
                                                     <a id="a_Case" runat="server" href="<%# Bind('CaseScript') %>">
			                                            <asp:Label ID="Label9" runat="server" Text="<%# Bind('Id') %>"></asp:Label>
						                              </a>
			                                        
                                                     <asp:Label ID="lbl_guid" runat="server" Text="<%# Bind('guid') %>" Visible="false"></asp:Label>
			                                             
			                                     </td>
			                                     <td>
			                                        <asp:Label ID="Label10" runat="server" Text="<%# Bind('RegionName') %>"></asp:Label>      
			                                     </td>
			                                     <td>
			                                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('Heading') %>"></asp:Label>      
			                                     </td>			                                     
			                                     <td>
			                                      <a id="a_ContactNum" runat="server" href="<%# Bind('OnClientClick') %>">
			                                        <asp:Label ID="Label3" runat="server" Text="<%# Bind('Consumer') %>"></asp:Label>
						                          </a>
			                                     </td>
			                                     <td style="width:70px;" runat="server" id="td1" visible="<%#  ToShow %>">
                                                   <asp:Label ID="Label4" runat="server"  Text="<%# Bind('NumberRequestedAdvertisers') %>"></asp:Label>
			                                     </td>			                                     
			                                     <td style="width:70px;" runat="server" id="td2" visible="<%#  ToShow %>">
                                                   <asp:Label ID="Label7" runat="server" Text="<%# Bind('NumberAdvertisersProvided') %>"></asp:Label>
			                                     </td>			                                     
			                                     <td runat="server" id="td3" visible="<%#  ToShow %>">
                                                   <asp:Label ID="Label8" runat="server" Text="<%# Bind('Revenue') %>"></asp:Label>
			                                     </td>
                                                 <td >
                                                   <asp:Label ID="Label11" runat="server" Text="<%# Bind('Status') %>"></asp:Label>
			                                     </td>
                                                 <td runat="server" visible="<%# with_upsale %>">
                                                   <asp:Label ID="Label192" runat="server" Text="<%# Bind('Upsales') %>"></asp:Label>
			                                     </td>
                                                 <td runat="server" visible="<%# with_duration %>">
                                                   <asp:Label ID="Label1992" runat="server" Text="<%# Bind('MaxCallDuration') %>"></asp:Label>
			                                     </td>
			                                </tr>
			                                
			                            </ItemTemplate>
                                        <FooterTemplate>
                                        </table>                
                                        </FooterTemplate>            
                                    </asp:Repeater>    
                              
            		<uc1:TablePaging ID="TablePaging1" runat="server" />
		            
	                </div>	
	            </ContentTemplate>
	        </asp:UpdatePanel>
    	
        			

    </div>
    
    <asp:Panel ID="panel_iframe" runat="server" CssClass="popModal_del2_ie popModal_del2_hide" style="display:none;"
     Width="900" Height="90%">
     <div id="divIframeLoader" class="divIframeLoader">   
        <table>
            <tr>
                <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
            </tr>
        </table>
    </div>
    <div id="div_iframe_main" style="height:100%;">
        <div class="top"></div>
            <a id="a_close" runat="server" class="span_A" href="javascript:void(0);" onclick="javascript:CloseIframe();"></a>
        <iframe runat="server" id="_iframe" width="950px" height="100%" frameborder="0" ALLOWTRANSPARENCY="true" ></iframe>
        <div class="bottom2"></div>
    </div>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="_mpe" runat="server"
                TargetControlID="btn_virtual"
                PopupControlID="panel_iframe"
                 BackgroundCssClass="modalBackground"                  
                  BehaviorID="_modal" 
                  Y="5"              
                  DropShadow="false">
    </cc1:ModalPopupExtender>
    
    <div style="display:none;">

        <asp:Button ID="btn_virtual" runat="server"  style="display:none;" CausesValidation="false"/>
    </div>

    <asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Id" runat="server" Text="Id" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Heading" runat="server" Text="Heading" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Consumer" runat="server" Text="Consumer" Visible="false"></asp:Label>
    <asp:Label ID="lbl_NumberOfRequestedAdvertisers" runat="server" Text="Number of Requested Advertisers" Visible="false"></asp:Label>
    <asp:Label ID="lbl_NumberOfAdvertisersProvided" runat="server" Text="No. Provided Advertisers" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Revenue" runat="server" Text="Revenue" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Region" runat="server" Text="Region" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Status" runat="server" Text="Status" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Upsales" runat="server" Text="Upsales" Visible="false"></asp:Label>
     <asp:Label ID="lbl_MaxCallDuration" runat="server" Text="Max call duration" Visible="false"></asp:Label>

    
    <asp:Label ID="lbl_NoResult" runat="server" Visible="false" Text="There are no results"></asp:Label>
    <asp:Label ID="lbl_PageTitle" runat="server" Text="Request reports" Visible="false"></asp:Label>

    <asp:HiddenField ID="Hidden_IfToExite" runat="server" Value="Are you sure you want to exit? Unsaved data will be lost!" />
    <asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
     <asp:Label ID="lbl_NoUpsale" runat="server" Text="No upsale" Visible="false"></asp:Label>
 <asp:Label ID="lbl_WithoutUpsale" runat="server" Text="Hide upsale column" Visible="false"></asp:Label>

</asp:Content>