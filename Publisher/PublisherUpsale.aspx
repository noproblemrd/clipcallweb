<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true"
 CodeFile="PublisherUpsale.aspx.cs" Inherits="Publisher_PublisherUpsale"   %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<%@ Register src="~/Controls/Upsale/UpsaleControl.ascx" tagname="UpsaleControl" tagprefix="uc2" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
     
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
<link href="../AutoCompleteCity/StyleAutoComplete.css" rel="stylesheet" type="text/css"/>
<link href="../general.js" type="text/javascript" />
<link href="../FromToDate.css" type="text/css" rel="stylesheet" />
<link href="Auction/style.css" rel="stylesheet" type="text/css"/>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script  type="text/javascript" src="../AutoCompleteCity/AutoCompleteCity.js"></script>
<script  type="text/javascript" src="../Calender/_Calender.js"></script>

	
<script type="text/javascript">
    var intervalRequest;
    var timerWait;

    function ConfirmDel(_val) {
        document.getElementById("<%# popUp_X.ClientID %>").className = "popModal_del3";
        $get("<%# hf_UpsaleId.ClientID  %>").value = _val;
        $find('_modalX').show();
    }

    function ClosePopUpX(_elementId, modalId) {
        document.getElementById(_elementId).className = "popModal_del3 popModal_del3_hide";
        document.getElementById("<%# ddl_Resons.ClientID %>").selectedIndex = 0;

        $find(modalId).hide();
    }





    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }
    function CloseUpsaleDetails() {
        $find('_modalUpsaleDetails').hide();
        document.getElementById("<%# _iframe.ClientID %>").src = "";
        <%# GetPostBackEventAfterAuction %>;
    }
    window.onload = appl_init;

    function OpenIframeDetails(_id)
    {
        var _iframe = document.getElementById("<%# _iframe.ClientID %>");
        var _url = "<%# GetPathUpsaleDetails %>" + _id;
        _iframe.setAttribute("src", _url);
        $find("_modalUpsaleDetails").show();
        return false;
    }
    
</script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    <Services> 
        <asp:ServiceReference Path="AutoComplete.asmx" />         
        <asp:ServiceReference Path="~/Publisher/Auction/Auction.asmx" /> 
    </Services> 
</cc1:ToolkitScriptManager>
    
<div class="page-content minisite-content">
    <h2><asp:Label ID="lbl_welcome" runat="server" Text="Welcome to your upsale report"></asp:Label></h2>
	
	<div id="form-analytics" class="clearfix" >
	    <div class="form-field">
            <asp:Label ID="lbl_expertise" runat="server" Text="Heading" CssClass="label"></asp:Label>
            <asp:DropDownList ID="ddl_Heading" CssClass="form-select" runat="server">
               </asp:DropDownList>                                           
        </div>
       		<div class="refunddate"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>		 			
	</div>
	<div class="table">
	<asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
       
        <div class="results5"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
            <asp:GridView ID="_gv_upsale" runat="server" AutoGenerateColumns="false" 
                CssClass="data-table" onrowdatabound="_gv_upsale_RowDataBound">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <PagerStyle CssClass="pager" />
                <Columns>
                    <asp:TemplateField >
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_IncidentNumber.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lb_Upsale" runat="server" Text="<%# Bind('UpsaleNumber') %>" 
                             CommandArgument="<%# Bind('UpsaleId') %>" OnClientClick="<%# Bind('UpsaleDetailsScript') %>"></asp:LinkButton>

                           
                        </ItemTemplate>
                    </asp:TemplateField>
                                 
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label31" runat="server" Text="<%# lbl_TimeToCall.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label32" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_UserPhone.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbl_user_phone" runat="server" Text="<%# Bind('ContactPhone') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="<%# lbl_Segment.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text="<%# Bind('PrimaryExpertiseName') %>"></asp:Label>
                            <asp:Label ID="lblExpertiseCode" runat="server" Text="<%# Bind('PrimaryExpertiseCode') %>" Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                   

                    <asp:TemplateField ItemStyle-Width="30">
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="<%# lbl_NoAdvertisers.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text="<%# Bind('RequiredSuppliers') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField ItemStyle-Width="30">
                        <HeaderTemplate>
                            <asp:Label ID="Label77" runat="server" Text="<%# lbl_ContactedSuppliers.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label78" runat="server" Text="<%# Bind('ContactedSuppliers') %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label9" runat="server" Text="<%# lbl_Upsale.Text %>"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="img_upsaleV" runat="server"  ImageUrl="images/icon-v.png"  CssClass="RemoveAddUpsale"/>
                            <asp:ImageButton ID="img_upsaleX" runat="server" ImageUrl="images/icon-x.png" CommandArgument="<%# Bind('UpsaleId') %>" CssClass="RemoveAddUpsale" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            </asp:GridView>
            
            <uc1:TablePaging ID="TablePaging1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</div>


                    

<div id="popUp_X" runat="server" class="popModal_del3 popModal_del3_hide"  style="display:none;">
   
    <a id="a_CloseDelete" runat="server" class="span_A"></a>
    <div class="content">
            <h2><asp:Label ID="lbl_titleDelete" runat="server" Text="Delete"></asp:Label></h2>
            <div class="form-field">
                <asp:Label ID="lbl_confirmDel" runat="server" Text="Please choose a reason for not upsaling this call"></asp:Label>
                <asp:DropDownList ID="ddl_Resons" CssClass="form-select" runat="server"></asp:DropDownList>
		    </div>
		    <div class="form-cta">
		        <asp:Button ID="btn_ConfirmDelete" runat="server" CssClass="btn" Text="Confirm" OnClick="btn_ConfirmDelete_click" OnClientClick="javascript:showDiv();" />
		        <input id="btn_CancelDelete" type="button" class="btn" value="Cancel" runat="server"/>  
		    </div>
		    
        </div>
    </div>

    <cc1:ModalPopupExtender ID="_mpeX" runat="server"
    TargetControlID="btn_virtualX"
    PopupControlID="popUp_X"
    BackgroundCssClass="modalBackground" 
    BehaviorID="_modalX"                
    DropShadow="false"
    ></cc1:ModalPopupExtender>
    

<div id="div_UpsaleDetails" runat="server" class="popModal_Upsale" style="display:none;">
    <div class="top"></div>
    <a id="a_CloseUpsaleDetails" runat="server" class="span_A" href="javascript:void(0);" onclick="CloseUpsaleDetails();"></a>
    <div class="content">
    <asp:UpdatePanel ID="_UpdateIframe" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
   
        <iframe runat="server" id="_iframe" width="670px" height="650px" frameborder="0" ALLOWTRANSPARENCY="true" scrolling="auto"  ></iframe>
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</div>
<cc1:ModalPopupExtender ID="_mpeUpsaleDetails" runat="server"
    TargetControlID="btn_virtualUpsaleDetails"
    PopupControlID="div_UpsaleDetails"
    BackgroundCssClass="modalBackground" 
    BehaviorID="_modalUpsaleDetails"                  
    DropShadow="false"
></cc1:ModalPopupExtender>
    <uc2:UpsaleControl ID="_UpsaleControl" runat="server" />
    <div style="display:none;">
        <asp:Button ID="btn_virtualX" runat="server" style="display:none;"  />    
        <asp:Button ID="btn_virtualUpsaleDetails" runat="server" style="display:none;"  /> 
    </div>
	<div>
	    <asp:Label ID="lbl_IncidentNumber" runat="server" Text="Number" Visible="false"></asp:Label>
	    <asp:Label ID="lbl_UserPhone" runat="server" Text="User Phone" Visible="false"></asp:Label>
	    <asp:Label ID="lbl_Segment" runat="server" Text="Heading" Visible="false"></asp:Label>			    
	    <asp:Label ID="lbl_NoAdvertisers" runat="server" Text="No. of required suppliers" Visible="false"></asp:Label>
	    <asp:Label ID="lbl_ContactedSuppliers" runat="server" Text="No. of contacted suppliers" Visible="false"></asp:Label>
	    <asp:Label ID="lbl_Upsale" runat="server" Text="Upsale" Visible="false"></asp:Label>
	    <asp:Label ID="lbl_TimeToCall" runat="server" Text="Time to call" Visible="false"></asp:Label>
        
	</div>
	<div>
		
        <asp:HiddenField ID="lbl_NoResult" runat="server" Value="There are no result" />
        <asp:Label ID="lbl_dateFromBeforTo" runat="server" Text="From date must be befor to date!" Visible="false"></asp:Label>
        <asp:Label ID="lbl_ChooseExpertise" runat="server" Text="Choose service" Visible="false"></asp:Label>
        <asp:HiddenField ID="lbl_ALL" runat="server" Value="ALL" />
    </div>
	
	<asp:Label ID="lbl_errorExpertise" runat="server" Text="Error service" Visible="false"></asp:Label>
    <div class="bottom"></div>
  

 <div id="divLoader" class="divLoader">   
    <table>
        <tr>
            <td>
                <img src="../UpdateProgress/ajax-loader.gif" alt="Loading..."/>
            </td>
        </tr>
    </table>
</div>  

<asp:HiddenField ID="hf_UpsaleId" runat="server" />
 
<asp:Label ID="lblRecordMached" runat="server" Text="records matched your query" Visible="false"></asp:Label>
</asp:Content>

