﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="PublisherPayment.aspx.cs" 
Inherits="Publisher_PublisherPayment" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="style.css" rel="Stylesheet" type="text/css" />
        <script type="text/javascript" src="../general.js"></script>

<script type="text/javascript">
    var recharge_val = new Number();
    function witchAlreadyStep(level) {
        parent.window.witchAlreadyStep(level);
        parent.window.setLinkStepActive('linkStep6');
    }
    /***OnLoad**/
    window.onload = appl_init;
    function appl_init() {

        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        parent.showDiv();
    }
    function EndHandler() {
        parent.hideDiv();
    }
    /***Payment details window */
    function OpenPaymentDetails(_url) {
        window.open(_url, 'my_PaymentDetails', 'fullscreen=no,width=800,height=500,resizable=no');
    }
    function OnClosePaymentDetails() {
    }

    function openAddDeposit(deposite)
    {
        parent.closeOpenOverLayIframe(deposite);
    }
    function RechargeTypeChanged(_value) {
        var div_DayInMonth = document.getElementById("<%# div_DayInMonth.ClientID %>");
        if (_value == "<%# WebReferenceSupplier.RechargeTypeCode.Monthly.ToString() %>") {
            toggleDisabled(div_DayInMonth, false);
            div_DayInMonth.style.display = "block";
        }
        else {
            toggleDisabled(div_DayInMonth, true);
            div_DayInMonth.style.display = "none";
        }
    }
    function Check_validation() {
        var cb_Recharge = document.getElementById("<%# cb_Recharge.ClientID %>");
        if (!cb_Recharge.checked)
            return true;
        if (!Page_ClientValidate('RechargeAmount'))
            return false;
        return true;

    }
    function cb_Recharge_click(IsChecked) {
        var txt_RechargeAmount = document.getElementById("<%# txt_RechargeAmount.ClientID %>");
        if (!IsChecked) {
            var _num = txt_RechargeAmount.value;
            txt_RechargeAmount.readOnly = true;
            recharge_val = (Is_Integer(_num)) ? parseInt(_num) : 0;
            txt_RechargeAmount.value = "";
        }
        else {
            txt_RechargeAmount.readOnly = false;
            txt_RechargeAmount.value = GetRecharge_val();
        }
    }
    function GetRecharge_val() {
        if (!recharge_val)
            return "";
        if(recharge_val < 1)
            return "";
        return recharge_val;
    }

</script>
</head>
<body style="background-color:transparent;" class="iframe-inner step8" >
<div id="_mainaudit">
<form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <div class="PublisherPayment_toolBox">
        <uc1:Toolbox ID="Toolbox1" runat="server" />
    </div>
    <div>
        <asp:Label ID="lbl_CurrentBalance" runat="server" Text="Balance"></asp:Label>
        <asp:Label ID="lblCurrentBalance" runat="server" ></asp:Label>
        
        <asp:Label ID="lbl_Currency" runat="server"></asp:Label>
    </div>
    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        
        <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false"
            CssClass="data-table"  Width="480px" style="display:inline-table;">
        <RowStyle CssClass="odd" />
        <AlternatingRowStyle CssClass="even" />        
        <FooterStyle CssClass="footer"  />
        <PagerStyle CssClass="pager" />
        <Columns>                
            <asp:TemplateField SortExpression="CreatedOn">              
            <HeaderTemplate>
                <asp:Label ID="Label1" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>                                       
            </HeaderTemplate>
            <ItemTemplate>                    
                <asp:Label ID="Label01" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField SortExpression="Number">              
            <HeaderTemplate>
                <asp:Label ID="Label2" runat="server" Text="<%# lbl_Deposition.Text %>"></asp:Label>                                       
            </HeaderTemplate>
            <ItemTemplate>          
                <asp:LinkButton ID="lb_Deposition" runat="server" Text="<%# Bind('Number') %>" OnClick="lb_Deposition_click"></asp:LinkButton>
                <asp:Label ID="lbl_DepositionId" runat="server" Text="<%# Bind('SupplierPricingId') %>" Visible="false"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField SortExpression="Amount">              
            <HeaderTemplate>
                <asp:Label ID="Label3" runat="server" Text="<%# lbl_Amount.Text %>"></asp:Label>                                       
            </HeaderTemplate>
            <ItemTemplate>                    
                <asp:Label ID="Label03" runat="server" Text="<%# Bind('Amount') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

             <asp:TemplateField SortExpression="Total">              
            <HeaderTemplate>
                <asp:Label ID="Label4" runat="server" Text="<%# lbl_Total.Text %>"></asp:Label>                                       
            </HeaderTemplate>
            <ItemTemplate>                    
                <asp:Label ID="Label04" runat="server" Text="<%# Bind('Total') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField SortExpression="PaymentMethod">              
            <HeaderTemplate>
                <asp:Label ID="Label5" runat="server" Text="<%# lbl_PaymentMethod.Text %>"></asp:Label>                                       
            </HeaderTemplate>
            <ItemTemplate>                    
                <asp:Label ID="Label05" runat="server" Text="<%# Bind('PaymentMethod') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>
        <uc1:TablePaging ID="TablePaging1" runat="server" On_lnkPage_Click="_tp__lnkPage_Click"  />
    </ContentTemplate>        
    </asp:UpdatePanel>

    <div id="div_Recharge" runat="server" class="Recharge">
        <asp:CheckBox ID="cb_Recharge" runat="server" CssClass="cb_Recharge" onclick="cb_Recharge_click(this.checked);" Text="Recharge" />
        <div class="group_recharge">            
            <asp:Label ID="lbl_RechargeAmount" runat="server" Text="Recharge amount" CssClass="lbl_RechargeAmount"></asp:Label>
            <asp:TextBox ID="txt_RechargeAmount" runat="server" CssClass=""></asp:TextBox>
          
         

        </div>
        
        <div class="group_recharge">            
            <asp:Label ID="lbl_RechargeType" runat="server" Text="Recharge type"></asp:Label>
            <asp:DropDownList ID="ddl_RechargeType" runat="server" onchange="RechargeTypeChanged(this.value);">
            </asp:DropDownList>
        </div>

        <div class="group_recharge">            
            <asp:Panel runat="server" ID="div_DayInMonth">
            <asp:Label ID="lbl_DayInMonth" runat="server" Text="Day in month"></asp:Label>
            <asp:DropDownList ID="ddl_DayInMonth" runat="server">
            </asp:DropDownList>
        </asp:Panel>
        </div>
        <br />
           <asp:RequiredFieldValidator ID="RequiredFieldValidator_RechargeAmount" runat="server" 
            ErrorMessage="Missing" ValidationGroup="RechargeAmount" CssClass="error-msg"
             ControlToValidate="txt_RechargeAmount" Display="Dynamic"></asp:RequiredFieldValidator>
            
            <asp:RangeValidator ID="RangeValidator_RechargeAmount" runat="server" ErrorMessage="<%# GetInvalidRechargeDeposite %>"
            ValidationGroup="RechargeAmount" CssClass="error-msg" ControlToValidate="txt_RechargeAmount"
             Type="Integer" MaximumValue="<%# _MaxValue %>" MinimumValue="<%# _MinValue %>"
              Display="Dynamic"></asp:RangeValidator>
            
        <div style="float:none;" class="clear">         
            <asp:Button ID="btn_SaveRecharge" runat="server" CssClass="btn_SaveRecharge" Text="Recharge" 
                onclick="btn_SaveRecharge_Click" ValidationGroup="RechargeAmount" OnClientClick="return Check_validation();" />          
        </div>
    
    </div>
<asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Deposition" runat="server" Text="Deposit No." Visible="false"></asp:Label>
<asp:Label ID="lbl_Amount" runat="server" Text="Amount" Visible="false"></asp:Label>
<asp:Label ID="lbl_PaymentMethod" runat="server" Text="Payment method" Visible="false"></asp:Label>
<asp:Label ID="lbl_Total" runat="server" Text="Total" Visible="false"></asp:Label>

<asp:Label ID="lbl_InvalidRechargeDeposite" runat="server" Text="Invalid, recharge deposit must be integer and more than" Visible="false"></asp:Label>
<asp:Label ID="lbl_ValidationSearch" runat="server" Text="You can search only on deposit Number" Visible="false"></asp:Label>

<asp:HiddenField ID="hf_MinDeposite" runat="server" />
</form>
</div>
</body>

</html>



