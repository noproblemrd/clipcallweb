﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;

public partial class Publisher_UpsaleDetails : PageSetting, IOnLoadControl
{
    const string CONFIGURATION_KEY = "MaxRegionLevel";
    const string TIME_FORMAT = "{0:HH:mm}";
    bool HasUpsale;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!userManagement.IsPublisher())
                Response.Redirect("LogOut.aspx");
            LoadTimeFollowUp();
            LoadUpsaleStatus();
            DataBind();
            LoadUpsaleDetails();
            if (HasUpsale)
            {
                Controls_Upsale_UpsaleControl _upsaleControl = (Controls_Upsale_UpsaleControl)Page.LoadControl(ResolveUrl("~") + "Controls/Upsale/UpsaleControl.ascx");
                _upsaleControl.SetCompleteRequestScript("OnUpsaleDone();");
                _PlaceHolderUpsale.Controls.Add(_upsaleControl);
            }
        }
        Header.DataBind();
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (IsPostBack)
        {
            if (!ScriptManager1.IsInAsyncPostBack && HasUpsale)
            {
                Controls_Upsale_UpsaleControl _upsaleControl = (Controls_Upsale_UpsaleControl)Page.LoadControl(ResolveUrl("~") + "Controls/Upsale/UpsaleControl.ascx");
                _upsaleControl.SetCompleteRequestScript("OnUpsaleDone();");
                _PlaceHolderUpsale.Controls.Add(_upsaleControl);
            }
        }

    }
    private void LoadUpsaleStatus()
    {
        foreach (WebReferenceReports.UpsaleStatus us in Enum.GetValues(typeof(WebReferenceReports.UpsaleStatus)))
        {
            ddl_Status.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "UpsaleStatus", us.ToString()), us.ToString()));
        }
    }

    private void LoadUpsaleDetails()
    {
        HasUpsale = false;
        string _upsale_id = Request.QueryString["UpsaleId"];
        ddl_StatusReason.Items.Clear();
        if (!Utilities.IsGUID(_upsale_id))
        {
            PageErrorLoad();
            return;
        }
        Guid UpsaleId = new Guid(_upsale_id);
        
        UpsaleIdV = UpsaleId;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfUpsaleDataExpanded result = null;
        try
        {
            result = _report.GetUpsaleData(UpsaleId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        WebReferenceReports.UpsaleDataExpanded data = result.Value;
        
        txt_Consumer.Text = data.ConsumerPhone;
        txt_FollowUp.Text = string.Format(siteSetting.DateFormat, data.TimeToCall);
        txt_Number.Text = data.UpsaleNumber;
        lbl_Title_Name.Text = data.UpsaleNumber;
        txt_PrimaryHeading.Text = data.ExpertiseName;
        txt_TotalRequested.Text = data.NumOfRequestedAdvertisers + "";
        txt_TotalConnected.Text = data.NumOfConnectedAdvertisers + "";
        SetTimeFollowUp(data.TimeToCall);
        string _Regions = string.Empty;
        foreach (string _s in data.Regions)
        {
            _Regions += _s + ", ";
        }
        if (_Regions.Length > 2)
        {
            _Regions = _Regions.Substring(0, _Regions.Length - 2);
            txt_Cities.Text = _Regions;
        }
        for (int i = 0; i < ddl_Status.Items.Count; i++)
        {
            if (ddl_Status.Items[i].Value == data.UpsaleStatus.ToString())//WebReferenceReports.UpsaleStatus.Success)
            {
                ddl_Status.SelectedIndex = i;
                break;
            }
        }
        if (data.UpsaleStatus == WebReferenceReports.UpsaleStatus.Success)
        {
            ddl_Status.Enabled = false;
            ddl_StatusReason.Items.Add(new ListItem(ddl_Status.SelectedValue, Guid.Empty.ToString()));
            img_Save.Visible = false;
        }
        else if (data.UpsaleStatus == WebReferenceReports.UpsaleStatus.Open)
        {
            HasUpsale = true;
            img_Save.Visible = true;
            img_upsaleV.Visible = true;
            foreach (ListItem li in ddl_Status.Items)
            {
                if (li.Value == WebReferenceReports.UpsaleStatus.Success.ToString())
                {
                    li.Enabled = false;
                    break;
                }
            }
            if (data.StatusReasonId == Guid.Empty)
            {
                ListItem _li = new ListItem("", Guid.Empty.ToString());
                _li.Selected = true;
                ddl_StatusReason.Items.Add(_li);
            }
            Dictionary<Guid, KeyValuePair<string, bool>> dic = GetUpsaleStatusReason(WebReferenceSite.eTableType.UpsalePendingReason);
            foreach (KeyValuePair<Guid, KeyValuePair<string, bool>> kvp in dic)
            {
                if (!kvp.Value.Value || kvp.Key == data.StatusReasonId)
                {
                    ListItem li = new ListItem(kvp.Value.Key, kvp.Key.ToString());
                    li.Selected = (kvp.Key == data.StatusReasonId);
                    ddl_StatusReason.Items.Add(li);
                }
            }
            string _script = "upsale('" + data.ConsumerPhone + "', '" + data.UpsaleId.ToString() + "', '" + data.ExpertiseCode + "', '');";//, '" + data.RegionName + "', '" + data.RegionLevel + "'); ";
            _script += "return false;";
            img_upsaleV.OnClientClick = _script;
            
        }
        else
        {
            img_Save.Visible = false;
            img_upsaleV.Visible = false;
            ddl_Status.Enabled = false;
            ddl_StatusReason.Items.Add(new ListItem(GetChoosenUpsaleStatusReason(WebReferenceSite.eTableType.UpsaleLoseReason, data.StatusReasonId), data.StatusReasonId.ToString()));
        }
         
        DataTable dt = new DataTable();
        dt.Columns.Add("CallStatus", typeof(string));
        dt.Columns.Add("CallTime", typeof(string));
        dt.Columns.Add("CaseNumber", typeof(string));
        dt.Columns.Add("SupplierName", typeof(string));
        dt.Columns.Add("CaseId", typeof(string));
        dt.Columns.Add("SupplierId", typeof(string));
        dt.Columns.Add("RegionName", typeof(string));
        dt.Columns.Add("CaseType", typeof(string));
        foreach (WebReferenceReports.ConnectedSupplierData csd in data.ConnectedSuppliers)
        {
            DataRow row = dt.NewRow();
            row["CallStatus"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "EndStatus", csd.CallStatus.ToString());
            row["CallTime"] = string.Format(siteSetting.DateFormat, csd.CallTime) + " " +
                string.Format(siteSetting.TimeFormat, csd.CallTime);
            row["CaseNumber"] = csd.CaseNumber;
            row["SupplierName"] = csd.SupplierName;
            row["RegionName"] = csd.RegionName;
            /* for future */
            row["CaseId"] = csd.CaseId.ToString();
            row["SupplierId"] = csd.SupplierId.ToString();
            row["CaseType"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "CaseTypeCode", csd.CaseType.ToString());
       //     WebReferenceReports.CaseTypeCode
            dt.Rows.Add(row);
        }
        _GridView.DataSource = dt;
        _GridView.DataBind();
            
    }
    private void LoadTimeFollowUp()
    {
        for (int i = 8; i < 21; i++)
        {
            string s = string.Empty;
            if (i < 10)
                s = "0" + i;
            else
                s = i + "";
            string val = s + ":00";
            ListItem li0 = new ListItem(val, val);
            val = s + ":30";
            ListItem li1 = new ListItem(val, val);
            ddl_Time.Items.Add(li0);
            ddl_Time.Items.Add(li1);
        }
    }
    private void SetTimeFollowUp(DateTime date)
    {
        string _time = string.Format(TIME_FORMAT, date);        
        for (int i = 0; i < ddl_Time.Items.Count; i++)
        {
            if (_time == ddl_Time.Items[i].Value)
            {
                ddl_Time.SelectedIndex = i;
                return;
            }
        }
        string[] hour_min = _time.Split(':');
        string NewTime = "";
        int _min = int.Parse(hour_min[1]);
        int _promote = 0;
        if(_min > 30)
        {
            NewTime = "00";
            _promote = 1;
        }
        else
        {
            if(_min > 0)
                NewTime="30";
            else
                NewTime="00";
        }
        int _hour = int.Parse(hour_min[0]) + _promote;
        if (_hour > 20)
            NewTime = "20:30";
        else
            NewTime = ((_hour > 9) ? "" : "0") + _hour + ":" + NewTime;

        for (int i = 0; i < ddl_Time.Items.Count; i++)
        {
            if (NewTime == ddl_Time.Items[i].Value)
            {
                ddl_Time.SelectedIndex = i;
                return;
            }
        }
    }
    Dictionary<Guid, KeyValuePair<string, bool>> GetUpsaleStatusReason(WebReferenceSite.eTableType ett)
    {
        Dictionary<Guid, KeyValuePair<string, bool>> dic = new Dictionary<Guid, KeyValuePair<string, bool>>();
                
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfTableRowData result = null;
        try
        {
            result = _site.GetTableData(ett);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            return dic;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return dic;
        foreach (WebReferenceSite.TableRowData trd in result.Value)
        {
            dic.Add(trd.Guid, new KeyValuePair<string, bool>(trd.Name, trd.Inactive));
        }
        return dic;
    }
    string GetChoosenUpsaleStatusReason(WebReferenceSite.eTableType ett, Guid choosenId)
    {
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfTableRowData result = null;
        try
        {
            result = _site.GetTableData(ett);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            return string.Empty;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
            return string.Empty;
        foreach (WebReferenceSite.TableRowData trd in result.Value)
        {
            if (trd.Guid == choosenId)
                return trd.Name;
        }
        return string.Empty;
    }
    void PageErrorLoad()
    {
    }
    protected void ddl_Status_SelectedIndexChanged(object sender, EventArgs e)
    {
        WebReferenceSite.eTableType ett = (ddl_Status.SelectedValue==WebReferenceReports.UpsaleStatus.Open.ToString())?
            WebReferenceSite.eTableType.UpsalePendingReason:
            WebReferenceSite.eTableType.UpsaleLoseReason;
        ddl_StatusReason.Items.Clear();
        Dictionary<Guid, KeyValuePair<string, bool>> dic = GetUpsaleStatusReason(ett);
        foreach (KeyValuePair<Guid, KeyValuePair<string, bool>> kvp in dic)
        {
            if (!kvp.Value.Value)
            {
                ListItem li = new ListItem(kvp.Value.Key, kvp.Key.ToString());                
                ddl_StatusReason.Items.Add(li);
            }
        }
        _UpdateStatuses.Update();
    }
    protected void img_Save_click(object sender, EventArgs e)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.Result result = null;
        WebReferenceSite.UpdateUpsaleRequest _request = new WebReferenceSite.UpdateUpsaleRequest();
        _request.UserId = userManagement.Get_Guid;        
        _request.UpsaleId = UpsaleIdV;
        WebReferenceSite.UpsaleStatus us;
        if(!Enum.TryParse(ddl_Status.SelectedValue, out us))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        switch (us)
        {
            case(WebReferenceSite.UpsaleStatus.Lost):
                _request.Status = WebReferenceSite.UpsaleStatus.Lost;
                _request.TimeToCall = DateTime.Now;
                break;
            case(WebReferenceSite.UpsaleStatus.Success):
                return;
            case(WebReferenceSite.UpsaleStatus.Open):
                _request.Status = WebReferenceSite.UpsaleStatus.Open;
                DateTime _date = SaveOpen();
                if (_date == DateTime.MinValue)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "InvalidDate",
                        "alert('" + HttpUtility.JavaScriptStringEncode(lbl_InvalidDate.Text) + "');", true);
                    return;
                }
                _request.TimeToCall = _date;
                break;

        }

        _request.StatusReasonId = (Utilities.IsGUID(ddl_StatusReason.SelectedValue)) ?
            new Guid(ddl_StatusReason.SelectedValue) : Guid.Empty;
        

        try
        {
            result = _site.UpdateUpsale(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Success", "top.UpdateSuccess();", true);
        LoadUpsaleDetails();
    }
    DateTime SaveOpen()
    {        
        string[] _times = ddl_Time.SelectedValue.Split(':');
        DateTime _date = ConvertToDateTime.CalanderToDateTime(txt_FollowUp.Text, siteSetting.DateFormat);
        string[] C_date = hf_Date.Value.Split(';');
        DateTime ClientDate = new DateTime(int.Parse(C_date[0]), (int.Parse(C_date[1]) + 1),
            int.Parse(C_date[2]), int.Parse(C_date[3]), int.Parse(C_date[4]), 0);
        DateTime _TimeToCall = new DateTime(_date.Year, _date.Month, _date.Day, int.Parse(_times[0]), int.Parse(_times[1]), 0);
        if (_TimeToCall < ClientDate)
            return DateTime.MinValue;
        return _TimeToCall;
        
    }
    protected string GetDateFormatToDatePicker
    {
        get
        {
            string result = (siteSetting.DateFormatClean).ToLower();
            result = result.Replace("yyyy", "yy");
            return result;
        }
    }
    protected string GetNoResulScript
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text); }
    }
    Guid UpsaleIdV
    {
        get { return (string.IsNullOrEmpty(hf_UpsaleId.Value)) ? Guid.Empty : new Guid(hf_UpsaleId.Value); }
        set { hf_UpsaleId.Value = value.ToString(); }
    }
    protected string GetWrongFormatDate
    {
        get
        {
            string mess = lbl_WrongFormatDate + " " + siteSetting.DateFormatClean;
            return HttpUtility.JavaScriptStringEncode(mess);
        }
    }
    #region IOnLoadControl Members

    public void __OnLoad(UserControl ControlSender)
    {
        if (ControlSender is Controls_Upsale_UpsaleControl)
            ((Controls_Upsale_UpsaleControl)ControlSender).LoadDetails(Guid.Empty);
    }

    #endregion
}