﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Publisher_PaymentReport : PageSetting
{
    const string UNBOOKED = "Unbooked";
    const string PAGE_TITLE = "Payment Report";
    const int DAYS_INTERVAL = 7;
    protected const int ITEM_PAGE = 40;
    protected const int PAGE_PAGES = 10;
    protected readonly string EXCEL_NAME = "ClipCallPaymentReport";

    protected readonly string SessionTableName = "data_CCPR";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {

            SetDateInterval();            
          ExecRequestReport();
      //      Table_Report.Visible = true;
        }
        SetToolbox();
        Header.DataBind();
    }
    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecRequestReport();
    }
    protected void ExecRequestReport()
    {
        Clearreports();

        DateTime _to = FromToDate1.GetDateTo;
        DateTime _from = FromToDate1.GetDateFrom;

        ClipCallReport.FromToDateTimeRequest _request = new ClipCallReport.FromToDateTimeRequest();
        _request.From = _from;
        _request.To = _to;
        ClipCallReport.ResultOfListOfPaymentReportResponse result = null;

        ClipCallReport.ClipCallReport _report = WebServiceConfig.GetClipCallReportReference(this);
        try
        {
            result = _report.GetPaymentReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        ClipCallReport.PaymentReportResponse[] values;
        if (!cb_IncludeQaTest.Checked)
        {
            var query = from x in result.Value
                        where !x.IsQaTest
                        select x;
            values = query.ToArray();
        }
        else
            values = result.Value;
        DataTable dt = new DataTable();
        dt.Columns.Add("PaymentDate");
        dt.Columns.Add("QuoteId");
        dt.Columns.Add("CaseScript");
        dt.Columns.Add("CaseNumber");
        dt.Columns.Add("IncidentId");
        dt.Columns.Add("CaseDate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Category");
        dt.Columns.Add("CustomerName");
        dt.Columns.Add("CustomerPhone");
        dt.Columns.Add("CustomerBonus");
        dt.Columns.Add("SupplierName");
        dt.Columns.Add("SupplierPhone");
        dt.Columns.Add("IsInvited");

        dt.Columns.Add("ClipCallFee");
        dt.Columns.Add("TransferedAt");
        dt.Columns.Add("AmountTransfered");
        dt.Columns.Add("StripeAccountId");
        dt.Columns.Add("FutureTransfer");
        dt.Columns.Add("IsFuture");
        dt.Columns.Add("PaymentPlan");
        dt.Columns.Add("SupplierStatus");

        foreach (ClipCallReport.PaymentReportResponse datarow in values)
        {
            DataRow row = dt.NewRow();
            row["PaymentDate"] = string.Format(siteSetting.DateTimeFormat, datarow.PaymentDate);
            row["QuoteId"] = datarow.QuoteId;
            row["CaseScript"] = "javascript:OpenIframe('MobileAppRequestTicket.aspx?incidentid=" + datarow.IncidentId.ToString() + "');";
            row["CaseNumber"] = datarow.CaseNumber;
            row["IncidentId"] = datarow.IncidentId;
            row["CaseDate"] = string.Format(siteSetting.DateTimeFormat, datarow.CaseDate);
            row["Amount"] = datarow.IsUnbooked ? UNBOOKED : string.Format("$" + NUMBER_FORMAT, datarow.Amount);
            row["Category"] = datarow.Category;
            row["CustomerName"] = datarow.CustomerName;
            row["CustomerPhone"] = datarow.CustomerPhone;
            row["CustomerBonus"] = datarow.IsUnbooked ? UNBOOKED : datarow.CustomerBonus == 0 ? null : string.Format("$" + NUMBER_FORMAT, datarow.CustomerBonus);
            row["SupplierName"] = datarow.SupplierName;
            row["SupplierPhone"] = datarow.SupplierPhone;
            row["SupplierStatus"] = datarow.SupplierStatus;
            row["IsInvited"] = datarow.IsInvited ? "Is invited" : null;
            if (!datarow.IsUnbooked)
            {
                if (datarow.ClipCallFee.HasValue)
                    row["ClipCallFee"] = string.Format("$" + NUMBER_FORMAT, datarow.ClipCallFee.Value);
                if (datarow.TransferedAt != DateTime.MinValue)
                    row["TransferedAt"] = string.Format(siteSetting.DateTimeFormat, datarow.TransferedAt);
                if (datarow.AmountTransfered.HasValue)
                    row["AmountTransfered"] = string.Format("$" + NUMBER_FORMAT, datarow.AmountTransfered.Value);
            }
            row["StripeAccountId"] = datarow.StripeAccountId;
            if(datarow.IsUnbooked)
                row["IsFuture"] = "2";
            else if (datarow.FutureTransfer != DateTime.MinValue)
            {
                row["FutureTransfer"] = string.Format(siteSetting.DateTimeFormat, datarow.FutureTransfer);
                row["IsFuture"] = "1";
            }
            else
                row["IsFuture"] = "0";
            row["PaymentPlan"] = datarow.PaymentPlan.ToString();

            dt.Rows.Add(row);
        }
        dataS = dt;
        
 //       Table_Report.Visible = true;
        lbl_RecordMached.Text = dt.Rows.Count + " " + ToolboxReport1.RecordMached;
        _reapeter.DataSource = dt;
        _reapeter.DataBind();
        _UpdatePanel.Update();
         
    }
    void Clearreports()
    {
//        Table_Report.Visible = false;
      
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        _UpdatePanel.Update();
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(PAGE_TITLE);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        string _isFuture = ((HiddenField)(e.Item.FindControl("hf_isFuture"))).Value;
        HtmlTableRow row = (HtmlTableRow)(e.Item.FindControl("tr_show"));
        string css_class = _isFuture == "1" ? "future" : (_isFuture == "2" ? "unbooked" : ((e.Item.ItemIndex % 2 == 0) ? "odd" : "even"));
        row.Attributes.Add("class", css_class);
        string h_QuoteId = ((HiddenField)e.Item.FindControl("h_QuoteId")).Value;
        row.Attributes.Add("onclick", "OpenTrace(this, '" + h_QuoteId + "');");

        row.Style.Add("cursor", "pointer");
    }
    protected DataTable dataS
    {
        get { return Session[SessionTableName] == null ? new DataTable() : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    private const string PaymentTrace_PATH = "~/Publisher/ClipCallControls/PaymentTrace.ascx";
     [WebMethod(MessageName = "GetPaymentTrace")]
    public static string GetPaymentTrace(Guid quoteId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
         
        ClipCallReport.ResultOfListOfPaymentTraceResponse result = null;
        try
        {
            result = report.GetPaymentTrace(quoteId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return null;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return null;
        }
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(PaymentTrace_PATH);
        Type CtlType = ctl.GetType();
        MethodInfo LoadData = CtlType.GetMethod("LoadData");
        LoadData.Invoke(ctl, new object[] { result.Value });
        
        ctl.EnableViewState = false;
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();

    }
}