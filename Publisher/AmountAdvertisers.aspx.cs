using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Linq;

public partial class Publisher_AmountAdvertisers : PageSetting
{
    const string MY_CLICK = "GetReport";
    const string REPORT_NAME = "AmountAdvertiserReport";
    protected string GetReport;
    protected void Page_Load(object sender, EventArgs e)
    {
        GetReport = ClientScript.GetPostBackEventReference(this, MY_CLICK);
        if (!IsPostBack)
        {
            LoadExperties();
            SetToolbox();
        }
        else
        {
            string event_argument = Request["__EVENTARGUMENT"];
            if (event_argument == MY_CLICK)
                btnSubmit_Click(null, EventArgs.Empty);
        }
        SetToolboxEvents();
        Page.Header.DataBind();
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lblTitleAmountAdvertisers.Text);
    }
    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, REPORT_NAME);
        if (!te.ExecExcel(dataV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
   //     PrintHelper.PrintWebControl(_GridView, dataV);
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        _GridView.DataSource = null;
        _GridView.DataBind();
        DataTable data = new DataTable();
        data.Columns.Add("Total");
        data.Columns.Add("ApprovedAndAvailable");
        data.Columns.Add("ApprovedNotAvailable");
        data.Columns.Add("Candidate");
        data.Columns.Add("Inactive");
        data.Columns.Add("ExpertiseName");
        data.Columns.Add("IsPrimary");
        
        
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.GetExpertiseReportRequest _request = new WebReferenceReports.GetExpertiseReportRequest();
        
        /*
        if (!string.IsNullOrEmpty(expertiser))
        {
            if (list.ContainsKey(expertiser))
            {
                _request.ExpertiseId = new Guid(list[expertiser]);
                _request.ExpertiseLevel = 1;
            }
            else
            {
                lbl_ErrorExpertiser.Attributes.Add("style", "display:inline");
                return;
            }
        }
        lbl_ErrorExpertiser.Attributes.Add("style", "display:none");
         * */
        _request.ExpertiseId = new Guid(ddl_Heading.SelectedValue);
        _request.ExpertiseLevel = 1;
        WebReferenceReports.ResultOfGetExpertiseReportResponse result = null;
        try
        {
            result = _report.GetExpertiseReport(_request);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        foreach (WebReferenceReports.ExpertiseRow er in result.Value.Report)
        {
            DataRow row = data.NewRow();
            row["ExpertiseName"] = er.Name;
            row["Total"] = er.Total;
            row["Candidate"] = er.Candidate;
            row["ApprovedAndAvailable"] = er.Available;
            row["ApprovedNotAvailable"] = er.NotAvailable;
            row["Inactive"] = er.Inactive;
            row["IsPrimary"] = er.IsPrimary.ToString();

            data.Rows.Add(row);
        }
        /*
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result.Value.XmlReport);
        if (xdd["AllExpertises"] != null)
        {
            foreach (XmlNode node in xdd["AllExpertises"].ChildNodes)
            {
                data.Rows.Add(GetGridRow(data, node));
                foreach (XmlNode node2 in node.ChildNodes)
                {
                    if (node2.Name == "SecondaryExpertise")
                    {
                        data.Rows.Add(GetGridRow(data, node2));
                    }
                }
            }
        }
        else
        {
            foreach (XmlNode node in xdd.ChildNodes)
            {
                data.Rows.Add(GetGridRow(data, node));
                foreach (XmlNode node2 in node.ChildNodes)
                {
                    if (node2.Name == "SecondaryExpertise")
                    {
                        data.Rows.Add(GetGridRow(data, node2));
                    }
                }
            }
        }
         * */
        _GridView.DataSource = data;
        _GridView.DataBind();
        lbl_RecordMached.Text = ToolboxReport1.GetRecordMaches(data.Rows.Count);
        SetGridView();
        dataV = data;
    }
    private void SetGridView()
    {
        foreach (GridViewRow _row in _GridView.Rows)
        {
            string IsPrimary = ((Label)_row.FindControl("lbl_IsPrimary")).Text.ToLower();
            _row.CssClass = (IsPrimary == "true") ? "odd" : "even";
        }
        _UpdatePanel.Update();
    }
 //   private DataRow GetGridRow(DataTable data, XmlNode node)
//    {
        /*
        data.Columns.Add("ApprovedAndAvailable");
        data.Columns.Add("ApprovedNotAvailable");
         * */
        /*
        DataRow row = data.NewRow();
        row["ExpertiseName"] = node.Attributes["ExpertiseName"].Value;
        row["Total"] = node["Total"].InnerText;
        row["Candidate"] = node["Candidate"].InnerText;
        row["ApprovedAndAvailable"] = node["ApprovedAndAvailable"].InnerText;
        row["ApprovedNotAvailable"] = node["ApprovedNotAvailable"].InnerText;
        row["Inactive"] = node["Inactive"].InnerText;
        row["ExpertiseLevel"] = node.Name;
        return row;
    }
         * */
    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;     

    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataTable data = dataV;
        _GridView.DataSource = data;
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataBind();
        SetGridView();
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    /*
    SortedDictionary<string, string> DicExpertiseV
    {
        get { return (Session["AutoCompleteListExp"] == null) ? null : (SortedDictionary<string, string>)Session["AutoCompleteListExp"]; }
        set { Session["AutoCompleteListExp"] = value; }
    }
   */
}
