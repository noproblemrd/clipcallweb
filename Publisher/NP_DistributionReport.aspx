﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="NP_DistributionReport.aspx.cs" Inherits="Publisher_NP_DistributionReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>


<%@ Register src="SalesControls/NP_DistributionReport.ascx" tagname="NP_DistributionReport" tagprefix="uc2" %>
<%@ Register Src="~/Publisher/SalesControls/NP_DistributionReportOrigin.ascx" TagPrefix="uc1" TagName="NP_DistributionReportOrigin" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
    <script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

    <script type="text/javascript">
	    window.onload = appl_init;

	    function appl_init() {
		    var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
		    pgRegMgr.add_beginRequest(BeginHandler);
		    pgRegMgr.add_endRequest(EndHandler);

	    }
	    function BeginHandler() {
		    showDiv();
	    }
	    function EndHandler() {
	     //   $(window).scroll(_scroll);
		    hideDiv();
	    }
        /*

	    function ReciveServerData(retValue) {

		    $(window).scroll(_scroll);
		    if (retValue.length == 0)
			    return;
		    var _trs = $(retValue).get(0).getElementsByTagName('tr');
		    var _length = _trs.length;
		    for (var i = 1; i < _length; i++) {
			    _trs[1].className = ((i % 2 == 1) ? "_even" : "_odd");
			    $('.-GridView').append(_trs[1]);
		    }

	    }
       
	    $(window).scroll(_scroll);
	    function _scroll() {
		    if ($(window).scrollTop() > 0.9 * ($(document).height() - $(window).height())) {
			    $(window).unbind('scroll');
			    _CallServer();
		    }
	    }

	    function _CallServer() {

	        $(window).unbind('scroll');
	        showDiv();
		    $.ajax({
			    url: '<# GetServicePage %>',
			    data: null,
			    dataType: "json",
			    type: "POST",
			    contentType: "application/json; charset=utf-8",
			    dataFilter: function (data) { return data; },
			    success: function (data) {
				    if (data.d.length == 0 || data.d == "done") {
					    return;
				    }
				    ReciveServerData(data.d);

			    },
			    error: function (XMLHttpRequest, textStatus, errorThrown) {
				    //              alert(textStatus);
			    },
			    complete: function (jqXHR, textStatus) {
			        //  HasInHeadingRequest = false;
			        hideDiv();

			    }

		    });
	    }
	    */
	    function _CreateExcel() {
	        var IsGeneral = $('#<%# hf_IsGeneral.ClientID %>').val();
	        if (IsGeneral.length == 0)
	            return;
	        var path = IsGeneral == 'false' ?
					    '<%# GetCreateExcelOrigin %>' : '<%# GetCreateExcelGeneral %>';
	        showDiv();
		    $.ajax({
			    url: path,
			    data: null,
			    dataType: "json",
			    type: "POST",
			    contentType: "application/json; charset=utf-8",
			    dataFilter: function (data) { return data; },
			    success: function (data) {
				    if (data.d.length == 0) {
					    return;
				    }
				    var _iframe = document.createElement('iframe');
				    _iframe.style.display = 'none';
				    _iframe.src = '<%# GetDownloadExcel %>' + '?fn=' + data.d;
				    document.getElementsByTagName('body')[0].appendChild(_iframe);

			    },
			    error: function (XMLHttpRequest, textStatus, errorThrown) {
				    //              alert(textStatus);
			    },
			    complete: function (jqXHR, textStatus) {
				    //  HasInHeadingRequest = false;
			        hideDiv();
			    }

		    });
	    }
	   
	    $(function () {
		    $('#<%# cb_MonthDate.ClientID %>').change(function () {
			    if (this.checked) {
				    $('#<%# div_Month.ClientID %>').hide();
				    $('#<%# div_Date.ClientID %>').show();
			    }
			    else {
				    $('#<%# div_Month.ClientID %>').show();
				    $('#<%# div_Date.ClientID %>').hide();
			    }
		    });		    
	    });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
	</cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="_ToolboxReport" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	                
			<div id="div_Origin" class="form-field" runat="server">
                <asp:Label ID="lbl_Origin" CssClass="label" runat="server" Text="Origin"></asp:Label>                                    
                <asp:DropDownList ID="ddl_Origins" CssClass="form-select" runat="server" ></asp:DropDownList>		
            </div>
			<div class="form-field" runat="server" style="margin-top: 38px;">
				<asp:CheckBox ID="cb_MonthDate" runat="server" Text="By Month/Date" />
			</div>            
			<div class="Date-field" runat="server" id="div_Month">
				<div class="form-field">
					<asp:Label ID="lbl_Years" CssClass="label" runat="server" Text="Year"></asp:Label>                                    
					<asp:DropDownList ID="ddl_Years" CssClass="form-select" runat="server"></asp:DropDownList>	
				</div> 

				<div class="form-field">
					<asp:Label ID="lbl_Month" CssClass="label" runat="server" Text="Month"></asp:Label>                                    
					<asp:DropDownList ID="ddl_Month" CssClass="form-select" runat="server"></asp:DropDownList>	
				</div> 
			</div> 
			<div class="Date-field" runat="server" id="div_Date" style="display: none;">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  			
		</div>
		
		<div style="clear:both;"></div>
		<div style="text-align: center;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
		<asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>	
            <div class="results" runat="server" id="div_NonResults" visible="false">
                 <asp:Label ID="lbl_NonResults" runat="server" Text="There are not results"></asp:Label>
            </div>
			<div class="results" runat="server" id="div_TotalEffectiveInstalls" visible="false">                 
				<asp:Label ID="lbl_TotalEffectiveInstalls" runat="server"></asp:Label>
				<asp:Label ID="lblTotalEffectiveInstalls" runat="server" Text="Total effective installs"></asp:Label>
			</div>
			<div class="results" runat="server" id="div_TotalPaymentAmount" visible="false">
				<asp:Label ID="lblTotalPaymentAmount" runat="server" Text="Total payment amount:"></asp:Label>        
				<asp:Label ID="lbl_TotalPaymentAmount" runat="server" Text=""></asp:Label>
			</div>
			
			<div id="Table_Report"  runat="server" >  
				<uc2:NP_DistributionReport ID="_NP_DistributionReport" runat="server" />
                <uc1:NP_DistributionReportOrigin runat="server" ID="_NP_DistributionReportOrigin" />
			</div>
            <asp:HiddenField ID="hf_IsGeneral" runat="server" />
		</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</div>    
    <asp:Label ID="lbl_NP_DistributionReport" runat="server" Text="NP Distribution report" Visible="false"></asp:Label>
</asp:Content>

