﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;

public partial class Publisher_PPAControlReport : PageSetting
{
    const int DAY_INTERVAL = 2;
    const string FILE_NAME = "PPAControlReportReport.xml";
    protected void Page_Load(object sender, EventArgs e)
    {
        SetToolboxEvents();
        if (!IsPostBack)
        {
            ResetThisSession();
            setDateTime();
            ExecReport(DateTime.Now.AddDays(-1 * DAY_INTERVAL), DateTime.Now);
        }
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_PPAControlReport.Text);
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
        ToolboxReport1.RemovePrintButton();
       // ToolboxReport1.RemoveExcelButton();
    }
    protected void btn_run_Click(object sender, EventArgs e)
    {
       
        ExecReport(FromToDatePicker_current.GetDateFrom, FromToDatePicker_current.GetDateTo);
        
    }
    void ExecReport(DateTime _from, DateTime _to)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfPpaControlReportRow result = null;
        try
        {
            result = _report.PpaControlReport(_from, _to);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        string controlPath = "~/Publisher/DpzControls/PPAControlReportControl.ascx";
        UserControl uc = (UserControl)LoadControl(controlPath);
        Type type = uc.GetType();
        MethodInfo methodInfo = type.GetMethod("BindReport");
        methodInfo.Invoke(uc, new object[] { result.Value, NUMBER_FORMAT, siteSetting.siteLangId });
        _PlaceHolder.Controls.Add(uc);
        
    }
    private void setDateTime()
    {
        FromToDatePicker_current.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
            DAY_INTERVAL);
    }
    private void ResetThisSession()
    {
        Session["PPAControlReport"] = null;
        Session["PageIndexPPAControlReport"] = null;
    }
    protected string GetDivCurrentId
    {
        get { return FromToDatePicker_current.GetParentDivId(); }
    }
    protected string GetInvalidDate
    {
        get { return Server.HtmlEncode(lbl_DateError.Text); }
    }
    protected string GetPPCControlReportPage
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetPPCControlReport"); }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/CreateExcelPPAControlReport"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
}