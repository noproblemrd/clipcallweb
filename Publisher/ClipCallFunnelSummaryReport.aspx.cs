﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallFunnelSummaryReport : PageSetting
{
    const string Num_Format = "{0:#,0.##}";
    const int ONE_DAY = 1;
    protected readonly string SessionTableName = "dataCFCR";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_ClipCallFunnelSummaryReport.Text);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.FunnelSummaryRequest _request = new WebReferenceReports.FunnelSummaryRequest();
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo;
        WebReferenceReports.ResultOfListOfFunnelSummaryResponse result = null;
        try
        {
            result = reports.FunnelSummaryReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        WebReferenceReports.FunnelSummaryResponse[] resultRows;
        if (userManagement.GetEnumSecurityLevel == SecurityLevel.PUBLISHER1)
            resultRows = result.Value;
        else
        {
            var query = from X in result.Value
                        where !X.isQaTest
                        select X;
            resultRows = query.ToArray();
        }
            
        /*
         public DateTime date { get; set; }
        public string caseNumber { get; set; }
        public string category { get; set; }
        public string videoLink { get; set; }
        public int videoDuration { get; set; }
        public string region { get; set; }
        public int SMS_Sent { get; set; }
        public int LP_Visits { get; set; }
        public int videoViews { get; set; }
        public int connection { get; set; }
        public int P5 { get; set; }
        public int P4 { get; set; }
        public int P3 { get; set; }
        public int P2 { get; set; }
        public int P1 { get; set; }
	[Priority]*/
        DataTable data = new DataTable();
        data.Columns.Add("date");
        data.Columns.Add("caseNumber");
        data.Columns.Add("CaseScript");
        data.Columns.Add("category");
        data.Columns.Add("videoLink");
        data.Columns.Add("videoDuration", typeof(int));
        data.Columns.Add("region");
        data.Columns.Add("customerPhone");
        data.Columns.Add("SMS_Sent", typeof(int));
        data.Columns.Add("LP_Visits", typeof(int));
        data.Columns.Add("videoViews", typeof(int));
        data.Columns.Add("connection", typeof(int));
        data.Columns.Add("P5", typeof(int));
        data.Columns.Add("P4", typeof(int));
        data.Columns.Add("P3", typeof(int));
        data.Columns.Add("P2", typeof(int));
        data.Columns.Add("P1", typeof(int));
        foreach (WebReferenceReports.FunnelSummaryResponse row in resultRows)
        {
            DataRow dr = data.NewRow();
            dr["date"] = GetDateTimeStringFormat(row.date);
            dr["caseNumber"] = row.caseNumber;
            dr["CaseScript"] = "javascript:OpenIframe('MobileAppRequestTicket.aspx?incidentid=" + row.incidentId.ToString() + "', 'LeadDetails" + row.incidentId.ToString() + "'); ";
            dr["category"] = row.category;
            dr["videoLink"] = new GenerateVideoLink(row.videoLink, row.PicPreviewUrl).GenerateFullUrl();
            dr["videoDuration"] = row.videoDuration;
            dr["region"] = row.region;
            dr["customerPhone"] = row.CustomerPhone;
            dr["SMS_Sent"] = row.SMS_Sent;
            dr["LP_Visits"] = row.LP_Visits;
            dr["videoViews"] = row.videoViews;
            dr["connection"] = row.connection;
            dr["P5"] = row.P5;
            dr["P4"] = row.P4;
            dr["P3"] = row.P3;
            dr["P2"] = row.P2;
            dr["P1"] = row.P1;
            data.Rows.Add(dr);
        }
        lbl_RecordMached.Text = result.Value.Length.ToString();
        _GridView.DataSource = data;
        _GridView.DataBind();
        dataV = data;

    }
    string GetDateTimeStringFormat(DateTime dt)
    {
        return string.Format(siteSetting.DateFormat, dt) + " " +
                    string.Format(siteSetting.TimeFormat, dt);
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
}