﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ProPerformanceReport : PageSetting
{
    const string RUN_REPORT = "RUN_REPORT";
    const string LOCK_ICON = "~/Publisher/images/switch_lock.png";
    const string UNLOCK_ICON = "~/Publisher/images/switch_unlock.png";
    const string UNLOCK_DISABLE_ICON = "~/Publisher/images/switch_unlock_disable.png";
    const int MONTHS = 1;
    protected readonly string SessionTableName = "ProPerformanceReport";
    const string LOCK = "Lock";
    const string UNLOCK = "Unlock";

    

    protected void Page_Load(object sender, EventArgs e)
    {
       ScriptManager1.RegisterAsyncPostBackControl(lb_RunReport);
   //     ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(_GridView);
        //     ScriptManager1.RegisterAsyncPostBackControl(_GridView);
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
            sortV = new List<string>();
        }
        else
        {
            if (Request["__EVENTARGUMENT"] == RUN_REPORT)
                ExecReport();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    protected override void Render(HtmlTextWriter writer)
    {

        Page.ClientScript.RegisterForEventValidation(btn_virtual_run.UniqueID, RUN_REPORT);
        base.Render(writer);
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }
    private void ExecReport()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.FromToDateTimeRequest _request = new ClipCallReport.FromToDateTimeRequest();
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo;
   //     _request.TextSearch = txtSearch.Text.Trim();


        ClipCallReport.ResultOfListOfSupplierPerformanceResponse result = null;
        try
        {
            result = report.GetSupplierPerformanceReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if (result.Value == null)
        {
            _GridView.DataSource = null;
            _GridView.DataBind();
            dataV = null;
            lbl_RecordMached.Text = null;
            return;
        }
        DataTable dt = GetDataTable(result.Value);
        lbl_RecordMached.Text = result.Value.Length.ToString();
        _GridView.DataSource = dt;
        _GridView.DataBind();
        _UpdatePanel.Update();
        dataV = dt;
        rowsV = result.Value;
    }
    private DataTable GetDataTable(ClipCallReport.SupplierPerformanceResponse[] rowsData)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("AccountId");
        dt.Columns.Add("ProName");
        dt.Columns.Add("AmountOfLeads", typeof(int));
        dt.Columns.Add("LeadsResponsed", typeof(int));
        dt.Columns.Add("BookingRate");
        dt.Columns.Add("TotalRevenues");
        dt.Columns.Add("RevenuePerBookedLead");
        dt.Columns.Add("InactiveReason");
        dt.Columns.Add("LastActive");
        dt.Columns.Add("Switch");
        dt.Columns.Add("Status");
        dt.Columns.Add("SwitchArgument");
        dt.Columns.Add("SwitchScript");
        dt.Columns.Add("LeadsResponsedRate");
        dt.Columns.Add("LeadsBooked", typeof(int));
        dt.Columns.Add("UnbookLeads", typeof(int));
        dt.Columns.Add("RevenuePerRespondedLead");

        dt.Columns.Add("AvgRank");
        dt.Columns.Add("NPS");
        dt.Columns.Add("Reviews");
        dt.Columns.Add("TotalQualityScore");

        foreach (var data in rowsData)
        {
            DataRow row = dt.NewRow();
            row["AccountId"] = data.AccountId;
            row["ProName"] = data.ProName;
            row["AmountOfLeads"] = data.AmountOfLeads;
            row["LeadsResponsed"] = data.LeadsResponded;
            row["BookingRate"] = string.Format(GetNumberFormat, data.BookingRate);
            row["TotalRevenues"] = string.Format("{0:#,0}", data.TotalRevenues);
            row["RevenuePerBookedLead"] = string.Format(GetNumberFormat, data.RevenuePerBookedLead);
            row["InactiveReason"] = data.InactiveReason.HasValue ? data.InactiveReason.Value.ToString() : null;
            row["LastActive"] = data.LastActive == DateTime.MinValue ? "---" : string.Format(siteSetting.DateTimeFormat, data.LastActive);
            row["LeadsResponsedRate"] = string.Format(GetNumberFormat, data.LeadsRespondedRate);
            row["LeadsBooked"] = data.LeadsBooked;
            row["UnbookLeads"] = data.UnbookLeads;
            row["RevenuePerRespondedLead"] = string.Format(GetNumberFormat, data.RevenuePerRespondedLead);

            if(data.ProRating.HasValue)
                row["AvgRank"] = string.Format(GetNumberFormat, data.ProRating.Value);
            if (data.NPS.HasValue)
                row["NPS"] = string.Format(GetNumberFormat, data.NPS.Value);

            bool EnableScript = true;
            SwitchArgumentObj sao = new SwitchArgumentObj();

            switch (data.SupplierStatus)
            {
                case (ClipCallReport.SupplierStatus.Approved):
                case (ClipCallReport.SupplierStatus.Trial):
                    row["Switch"] = LOCK_ICON;
                    sao.AccountId = data.AccountId;
                    sao.IsActive = true;
                    row["Status"] = LOCK;
                    break;
                case (ClipCallReport.SupplierStatus.Inactive):
                    {
                        switch (data.InactiveReason)
                        {
                            case (ClipCallReport.eInactiveReason.Blocked):
                            case (ClipCallReport.eInactiveReason.NotConfirmedServiceLocation):
                            case (ClipCallReport.eInactiveReason.BlockedOnLowPerformance):
                                row["Switch"] = UNLOCK_ICON;
                                sao.AccountId = data.AccountId;
                                sao.IsActive = false;
                                row["Status"] = UNLOCK;
                                break;
                            default:
                                row["Switch"] = UNLOCK_DISABLE_ICON;
                                EnableScript = false;
                                break;
                        }
                    }
                    break;
                default:
                    row["Switch"] = UNLOCK_DISABLE_ICON;
                    EnableScript = false;
                    break;
            }
            row["SwitchArgument"] = Newtonsoft.Json.JsonConvert.SerializeObject(sao);
            row["SwitchScript"] = "return " + EnableScript.ToString().ToLower() + ";";
            row["Reviews"] = data.Reviews;
            if (data.TotalQualityScore.HasValue)
                row["TotalQualityScore"] = data.TotalQualityScore;
            dt.Rows.Add(row);

            
        }
        return dt;
    }
    protected void _GridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        System.Reflection.PropertyInfo prop = typeof(ClipCallReport.SupplierPerformanceResponse).GetProperty(e.SortExpression);
        ClipCallReport.SupplierPerformanceResponse[] rowsData = rowsV;
        IEnumerable<ClipCallReport.SupplierPerformanceResponse> query;
        if (IsAscDirection(e.SortExpression))
        {
            query = rowsData.OrderBy(x => prop.GetValue(x, null));
        }
        else
        {
            query = rowsData.OrderByDescending(x => prop.GetValue(x, null));
        }
        _GridView.DataSource = GetDataTable(query.ToArray());
        _GridView.DataBind();
        _UpdatePanel.Update();
                    
    }
    private bool IsAscDirection(string sortExpression)
    {
        List<string> sort = sortV;
        bool result;
        if (!sort.Contains(sortExpression))
        {
            sort.Add(sortExpression);
            result = false;
        }
        else
        {
            sort.Remove(sortExpression);
            result = true;
        }
 //       sortV = sort;
        return result;
    }
    protected void ib_switch_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ib = (ImageButton)sender;
        Label lbl_inactiveReason = (Label)(ib.Parent.FindControl("lbl_inactiveReason"));
        Label lbl_switch = (Label)(ib.Parent.FindControl("lbl_switch"));
        SwitchArgumentObj sao = Newtonsoft.Json.JsonConvert.DeserializeObject<SwitchArgumentObj>(ib.CommandArgument);
        if (sao.AccountId == Guid.Empty)
            return;
        if(sao.IsActive)
        {
            ClipCallReport.GeneralResponseWR gr = InactiveSupplierManager.InactiveSupplierReason(sao.AccountId, ClipCallReport.eInactiveReason.BlockedOnLowPerformance);
            if(gr.IsSucceeded)
            {
                ib.ImageUrl = UNLOCK_ICON;
                sao.IsActive = false;
                lbl_inactiveReason.Text = ClipCallReport.eInactiveReason.BlockedOnLowPerformance.ToString();
                lbl_switch.Text = UNLOCK;
                ib.CommandArgument = Newtonsoft.Json.JsonConvert.SerializeObject(sao);
            }
        }
        else
        {
            ClipCallReport.GeneralResponseWR gr = InactiveSupplierManager.ActiveSupplier(sao.AccountId);
            if (gr.IsSucceeded)
            {
                ib.ImageUrl = LOCK_ICON;
                sao.IsActive = true;
                lbl_inactiveReason.Text = "";
                lbl_switch.Text = LOCK;
                ib.CommandArgument = Newtonsoft.Json.JsonConvert.SerializeObject(sao);
            }
        }
        _UpdatePanel.Update();
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle("Pro Performance Report");
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected string RunReport()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(btn_virtual_run, RUN_REPORT);
        myPostBackOptions.PerformValidation = false;
        //     return ScriptManager.g
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
        //   WebReferenceSite.FundsStatus
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDatesByMonth(MONTHS);
            //(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
            
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    ClipCallReport.SupplierPerformanceResponse[] rowsV
    {
        get { return (Session["rows"] == null) ? null : (ClipCallReport.SupplierPerformanceResponse[])Session["rows"]; }
        set { Session["rows"] = value; }
    }
    
    List<string> sortV
    {
        get { return (ViewState["sortV"] == null) ? new List<string>() : (List<string>)ViewState["sortV"]; }
        set { ViewState["sortV"] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
    protected string ActiveSupplierUrl
    {
        get { return ResolveUrl("~/Publisher/ClipCallAdvertiserReport2.aspx/ActiveSupplier"); }
    }
    protected string InactiveSupplierUrl
    {
        get { return ResolveUrl("~/Publisher/ProPerformanceReport.aspx/InactiveSupplier"); }
    }
    public class SwitchArgumentObj
    {
        public bool IsActive { get; set; }
        public Guid AccountId { get; set; }
      //  public string StatusControlId { get; set; }
    }


    
}