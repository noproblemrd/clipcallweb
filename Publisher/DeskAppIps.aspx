﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master"
    AutoEventWireup="true" CodeFile="DeskAppIps.aspx.cs" Inherits="Publisher_DeskAppIps"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/Toolbox.ascx" TagName="Toolbox" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
    <script type="text/javascript">
        window.onload = appl_init;
        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);
        }
        function BeginHandler() {
            showDiv();
        }
        function EndHandler() {
            hideDiv();
        }

        function closePopup() {
            document.getElementById("<%# popUpadd.ClientID %>").className = "popModal_del popModal_del_hide";
            ClearPopUp();
            $find('_modal').hide();
        }
        function ClearPopUp() {

            document.getElementById("<%# txt_IP.ClientID %>").value = "";
            document.getElementById("<%# txt_From.ClientID %>").value = "";
            document.getElementById("<%# txt_To.ClientID %>").value = "";
            document.getElementById("<%# cb_Inject.ClientID %>").checked = false;
            document.getElementById("<%# txt_Comment.ClientID %>").value = "";

            var validators = Page_Validators;
            for (var i = validators.length - 1; i > -1; i--) {
                validators[i].style.display = "none";
            }

        }
        function openPopup() {
            document.getElementById("<%# popUpadd.ClientID %>").className = "popModal_del";
            ClearPopUp();
            $find('_modal').show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <uc1:Toolbox ID="Toolbox1" runat="server" />
    <h5>
        <asp:Label ID="lblSubTitle" runat="server" Text="Desktop App IPs"></asp:Label>
    </h5>
    <div class="page-content minisite-content5">
        <div id="form-analyticsseg">
            <asp:UpdatePanel ID="_updatePanelTable" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" ShowFooter="true"
                        CssClass="data-table" AllowPaging="True" OnPageIndexChanging="_GridView_PageIndexChanging"
                        PageSize="20">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                        <FooterStyle CssClass="footer" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                                    <br />
                                    <asp:CheckBox ID="cb_all" runat="server"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="cb_choose" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label03" runat="server" Text="<%# lbl_ip.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl__ip" runat="server" Text="<%# Bind('Ip') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label04" runat="server" Text="<%# lbl_From.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl__From" runat="server" Text="<%# Bind('From') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label05" runat="server" Text="<%# lbl_To.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl__To" runat="server" Text="<%# Bind('To') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label06" runat="server" Text="<%# lbl_Inject.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl__Inject" runat="server" Text="<%# Bind('Inject') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label063" runat="server" Text="<%# lbl_Comment.Text %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl__Comment" runat="server" Text="<%# Bind('Comment') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="popUpadd" runat="server" sclass="content" style="max-height: 450px; display: none;"
        class="popModal_del popModal_del_hide">
        <div class="top">
        </div>
        <a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
        <div class="content">
            <div class="modaltit">
                <h2>
                    New IP</h2>
            </div>
            <div>
                <center>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblip" runat="server" CssClass="label" Text="<%# lbl_ip.Text %>"></asp:Label>
                                <asp:TextBox ID="txt_IP" CssClass="form-text" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator_ip" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
                                    Display="Dynamic" ValidationGroup="IpEdit" ControlToValidate="txt_IP" CssClass="error-msg"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblFrom" runat="server" CssClass="label" Text="<%# lbl_From.Text %>"></asp:Label>
                                <asp:TextBox ID="txt_From" CssClass="form-text" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator_From" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
                                    Display="Dynamic" ValidationGroup="IpEdit" ControlToValidate="txt_From" CssClass="error-msg"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTo" runat="server" CssClass="label" Text="<%# lbl_To.Text %>"></asp:Label>
                                <asp:TextBox ID="txt_To" CssClass="form-text" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator_To" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
                                    Display="Dynamic" ValidationGroup="IpEdit" ControlToValidate="txt_To" CssClass="error-msg"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date format is: MM/dd/yyyy [hh:mm:ss]
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInject" runat="server" CssClass="label" Text="<%# lbl_Inject.Text %>"></asp:Label>
                                <asp:CheckBox ID="cb_Inject" runat="server"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblComment" runat="server" CssClass="label" Text="<%# lbl_Comment.Text %>"></asp:Label>
                                <asp:TextBox ID="txt_Comment" runat="server" CssClass="form-text" ></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
            <div class="content2">
                <table class="Table_Button">
                    <tr>
                        <td>
                            <div class="unavailabilty2">
                                <asp:Button ID="btn_Set" runat="server" Text="Set" ValidationGroup="IpEdit" OnClick="btn_Set_Click"
                                    CssClass="btn2" />
                                <input id="btn_cancel" type="button" class="btn" value="Cancel" runat="server" onclick="javascript:closePopup();" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="bottom2">
        </div>
    </div>
    <cc1:ModalPopupExtender ID="_mpe" runat="server" TargetControlID="btn_virtual" PopupControlID="popUpadd"
        BackgroundCssClass="modalBackground" BehaviorID="_modal" DropShadow="false">
    </cc1:ModalPopupExtender>
    <div style="display: none;">
        <asp:Button ID="btn_virtual" runat="server" Style="display: none;" />
    </div>
    <asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ip" runat="server" Text="IP" Visible="false"></asp:Label>
    <asp:Label ID="lbl_From" runat="server" Text="From" Visible="false"></asp:Label>
    <asp:Label ID="lbl_To" runat="server" Text="To" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Inject" runat="server" Text="Inject Third Party?" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Comment" runat="server" Text="Comment" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Delete" runat="server" Text="This values won't be available for new updates. In all the records that used this values it will still"
        Visible="false"></asp:Label>
    <asp:Label ID="lbl_Missing" runat="server" Text="Missing" Visible="false"></asp:Label>
    <asp:Label ID="lbl_TitelEdit" runat="server" Text="Edit affiliate" Visible="false"></asp:Label>
    <asp:Label ID="lbl_TitelNew" runat="server" Text="New affiliate" Visible="false"></asp:Label>
</asp:Content>
