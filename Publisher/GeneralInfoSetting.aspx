<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="GeneralInfoSetting.aspx.cs" Inherits="Publisher_GeneralInfoSetting" Title="Untitled Page" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asps" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="style.css" rel="stylesheet" type="text/css"/>
   <script type="text/javascript" src="../general.js"></script>
   <script type="text/javascript">
   var HasAutoComplete=new Array();
   var Parents = new Array();
   function ShowCheck(ShowControl, ValidControl, CheckBoxAutocomplete, cbParent)
   {
     //  var message="ShowControl="+ShowControl;
        
        var _show= $get(ShowControl);
        var _valid =$get(ValidControl);
        var _complete = $get(CheckBoxAutocomplete); 
        var _parent = $get(cbParent);               
        if(!_show.checked)
        {
            _valid.checked=false;
            if(_complete) 
                _complete.checked=false;
        }
        if(_complete && !_complete.checked && _parent)
            _parent.checked=false;
       for(var i=0; i<Parents.length;i++)
       {
        
            var AutoComplete = HasAutoComplete[i].split(";");
            if(AutoComplete[1].length==0)
                continue;
            
            var _parents=Parents[i].split(";");
            var LabelParent = $get(_parents[1]);
       //     alert("i="+i);
            var checkBox_parent=$get(_parents[0]);
            if(($get(AutoComplete[0]).checked && !checkBox_parent) ||
                ($get(AutoComplete[0]).checked && checkBox_parent.checked))
            {
                
                var parnt="";
                for(var j=i-1; j>-1;j--)
                {
          //          alert("j="+j);
                    var _HasAutoComplete=HasAutoComplete[j].split(";");
                    var _HasParent=Parents[j].split(";");
          //          alert((_HasParent[0]));
       //             alert(_HasAutoComplete[1])
                    var checkBoxParent=$get(_HasParent[0]);
                    if($get(_HasAutoComplete[0]).checked)
                    {
                        if(!checkBoxParent || checkBoxParent.checked)
                        {
           //                 if(checkBoxParent)
           //                     alert(checkBoxParent.checked + checkBoxParent.id+";;;"+i);
                            parnt=_HasAutoComplete[1];                                      
                         }
                            
                        
                    }                     
                    
                    if(parnt.length==0)
                        continue;
                    else
                        break;
                }
      //          alert("par= "+parnt+" label= "+LabelParent.id);
          //     if(parnt.length==0)  
           //         if(checkBox_parent)
          //              checkBox_parent.checked=true;
                LabelParent.innerHTML=parnt;
            }
            else
            {
                LabelParent.innerHTML="";
                
     //               checkBox_parent.checked=checkBox_parent;
            }
       }
   }
   
   
    function CheckFor(contr) {
            var message="ParentControl="+contr;
            CallServer(contr.id, "ee");
        }
        //replay from the server |not delete|//
    function ReciveServerData(retValue) {
        if(retValue.length==0)
            return;
        alert(retValue);
        var elements=retValue.split(",");
        for(var i=0;i<elements.length;i++)
        {
            var elemantVals=elements[i].split(";");
            document.getElementById(elemantVals[0]).innerHTML=elemantVals[1];
        }
    }
    function SetAutoCompleteArray(argAuto, argParent)
    {
        if(argAuto.length==0 || argParent.length==0)
            return;
         var elementsAuto=argAuto.split(",");
         var elementsParent=argParent.split(",");
        for(var i=0;i<elementsAuto.length;i++)
        {
     //       var elemantVals=elements[i].split(";");
            HasAutoComplete[i]=elementsAuto[i];
            Parents[i]=elementsParent[i];
        }
    }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager runat="server" ID="ScriptManager1"></cc1:ToolkitScriptManager>


<div class="page-content minisite-content">
    <h2><asp:Label ID="lblSubTitle" runat="server">General info setting</asp:Label></h2>
    <div id="form-analyticsseg">
        <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" CssClass="data-table">
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />
            <Columns>
                <asp:TemplateField ItemStyle-CssClass="dt_generalSetting">
                    <HeaderTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# lbl_Field.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl_desc" runat="server" Text="<%# Bind('FieldDesc') %>"></asp:Label>
                        <asp:Label ID="lbl_ControlId" runat="server" Text="<%# Bind('ControlId') %>" Visible="false"></asp:Label>
                        <asp:Label ID="lbl_PanelShowId" runat="server" Text="<%# Bind('PanelShowId') %>" Visible="false"></asp:Label>
                        <asp:Label ID="lbl_PanelShow_Site_Id" runat="server" Text="<%# Bind('PanelShow_Site_Id') %>" Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                    
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# lbl_Display.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="_CheckBoxDisplay" CssClass="form-checkbox" runat="server" Checked="<%# Bind('Show') %>"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                    
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label3" runat="server" Text="<%# lbl_Must.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="_CheckBoxMust" CssClass="form-checkbox" runat="server" Checked="<%# Bind('Valid') %>"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                     
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label4" runat="server" Text="<%# lbl_Autocomplete.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="_CheckBoxAutocomplete" CssClass="form-checkbox" runat="server" Checked="<%# Bind('Autocomplete') %>"></asp:CheckBox>
                        <asp:Label ID="lbl_HasAutocomplete" runat="server" Text="<%# Bind('HasAutocomplete') %>" Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>   
                    
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label13" runat="server" Text="<%# lbl_Parent.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl_ParentName" runat="server" Text="<%# Bind('ParentName') %>" ></asp:Label>
                        <asp:Label ID="lbl_ParentId" runat="server" Text="<%# Bind('ParentId') %>" Visible="false"></asp:Label>
                        <asp:Label ID="lbl_OriginalParentId" runat="server" Text="<%# Bind('OriginalParentId') %>" Visible="false"></asp:Label>
                        <asp:Label ID="lbl_OriginalParentName" runat="server" Text="<%# Bind('OriginalParentName') %>" Visible="false"></asp:Label>
                        <asp:CheckBox ID="_CheckBoxParent" runat="server" Checked="<%# Bind('HasParentChecked') %>" Visible="<%# Bind('HasParent') %>"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField> 
                     
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label33" runat="server" Text="<%# lbl_Level.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl_OriginalLevel" runat="server" Text="" ></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <asp:Button ID="btn_set" runat="server" Text="Set" CssClass="form-submit" OnClick="btn_set_Click" />
</div>

<asp:Label ID="lbl_Field" runat="server" Text="Field" Visible="false"></asp:Label>
<asp:Label ID="lbl_Display" runat="server" Text="Display" Visible="false"></asp:Label>
<asp:Label ID="lbl_Must" runat="server" Text="Required information" Visible="false"></asp:Label>
<asp:Label ID="lbl_Autocomplete" runat="server" Text="Validate AutoComplete" Visible="false"></asp:Label>
<asp:Label ID="lbl_Parent" runat="server" Text="Level of Dependency" Visible="false"></asp:Label>
<asp:Label ID="lbl_Level" runat="server" Text="Original level" Visible="false"></asp:Label>
</asp:Content>

