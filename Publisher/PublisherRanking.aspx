<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="PublisherRanking.aspx.cs" Inherits="Publisher_PublisherRanking" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asps" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="PublishStyle.css" rel="stylesheet" type="text/css"/>
   <script type="text/javascript" src="../general.js"></script>
   <script type="text/javascript" language="javascript">
   
    var values=new Array();
    var labels=new Array();
   
   function LoadValuesLabels(values_, labels_)
   {
        var _values=values_.split(";");
        var _labels=labels_.split(";");
        for(var i=0;i<_values.length;i++)
        {
            values[i]=document.getElementById(_values[i]);
            labels[i]=document.getElementById(_labels[i]);
        }
   }
   function ValidateIfNum(elm)
   {
        if(isNaN(elm.value) || elm.value.indexOf('.')!=-1)
            elm.value="0";
        var sum=0;
        for(var i=0;i<values.length;i++)
        {
            var _add=parseInt(values[i].value);
            sum=sum+_add;
        }
        document.getElementById("<%= lblSum.ClientID %>").innerHTML=sum.toString();
   }
   function ValidateNums()
   {
        var _num=0;
        for(var i=0;i<values.length;i++)
        {
            var _add=parseInt(values[i].value);
            _num=_num+_add;
        }
        if(_num==100)
            return true;
        if(_num>100)
            alert(document.getElementById("<%= Hidden_Above.ClientID %>").value);
        else
            alert(document.getElementById("<%= Hidden_Under.ClientID %>").value);
        return false;
   }
   //not in used
   function ValidateNum(element, _lbl)    
   {
        var result=true;
       for(var _i=0; _i<labels.length; _i++)
            labels[_i].style.display="none";
        var txt=element.value;
        
        if(isNaN(txt) ||  txt.indexOf('.') != -1)
        {
            element.value="0";
            _lbl.style.display="inline";
            result=false;
  //          return;
        }
        var sum=new Number();
        sum=100;
        var IsFin=false;
        var index=GetIndex(element);
        var i=index;
        while(!IsFin)
        {
            var _num=parseInt(values[i].value);
            sum=sum-_num;
            if(sum<0)
            {
                _num=_num+sum;
          //      alert(_num);
                values[i].value=_num;
                sum=0;
            }
            
            i--;
            if(i<0)
                i=values.length-1;
            if(i==index)
            {
                IsFin=true;
                if(sum>0)
                {
                    i++;
                    if(i>=values.length)
                        i=0;
                    _num=parseInt(values[i].value);
                    _num=_num+sum;
                    values[i].value=_num;
                }
            }
        }
        return result;       
            
   }
   function GetIndex(element)
   {
        for(var i=0; i<values.length; i++)
            if(element==values[i])
                return i;
                
        return 5;
   }
  
   
   function GetLblValid(_id)
   {        
        return document.getElementById(_id);
   }
   
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager runat="server" ID="ScriptManager1">
    </cc1:ToolkitScriptManager>

   <div class="page-content minisite-content">
		    
			    <h2>
			    <asp:Label ID="lblSubTitle" runat="server">Advertiser ranking</asp:Label>
			    </h2>
			    <div id="form-analytics">
			    
                    <asp:Repeater ID="_Repeater" runat="server">
                    <HeaderTemplate>
                    <table>
                    </HeaderTemplate>
                    <ItemTemplate>
                     <div>
                         <asp:Label ID="lbl_ParameterName" runat="server" Text="<%# Bind('Name') %>" ></asp:Label>:
                  </div>
                   
                       <div> <asp:TextBox ID="txt_ParameterValue" runat="server" Text="<%# Bind('Value') %>"></asp:TextBox>
                        <asp:Label ID="lbl_ValidValue" runat="server" Text="<%# lbl_NotValid.Text %>" ForeColor="red"
                         style="display:none;"></asp:Label>
                         
                        <asp:Label ID="lbl_Code" runat="server" Text="<%# Bind('Code') %>" Visible="false"></asp:Label>
                   </div>
                    </ItemTemplate>
                    <FooterTemplate>
                    </table>
                    </FooterTemplate>
                    </asp:Repeater>
                   
			 
			        <div>
			        <table>
			        <tr>
			        <td style="padding-right:15px;">
			            <asp:Label ID="lbl_Sum" runat="server" Text="Sum" Font-Bold="True"></asp:Label>
			        </td>
			        <td>
                        <asp:Label ID="lblSum" runat="server" Text=""></asp:Label>
			        </td>
			        </tr>
			        </table>
                        
			        
			        <center>
                        <asp:Button ID="btnSet" runat="server" Text="Set"  
                        CssClass="form-submit" OnClick="btnSet_Click" OnClientClick="return ValidateNums();"   />
                    </center>
			        </div>
			    </div>
		   
 </div>   
<div>
    <asp:Label ID="lbl_NotValid" runat="server" Text="Not valid" Visible="false"></asp:Label>
    
    <asp:HiddenField ID="Hidden_Above" runat="server" Value="you are above 100%">
    </asp:HiddenField>
    <asp:HiddenField ID="Hidden_Under" runat="server" Value="you are under 100%">
    </asp:HiddenField>
</div>
</asp:Content>

