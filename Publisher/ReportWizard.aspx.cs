﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Publisher_ReportWizard : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetIssueRepor();
            SetSortType();
            SetSort_GroupBy();
            string ReportId = Request.QueryString["ReportId"];
            if (!string.IsNullOrEmpty(ReportId))
                LoadReport(int.Parse(ReportId));
            else
                LoadEmptyReport(); 
        }
        Header.DataBind();
    }

    private void SetSort_GroupBy()
    {
        ddl_SortBy.Items.Add(new ListItem("-- " + lbl_choose.Text + " --", "0"));
        ddl_groupBy.Items.Add(new ListItem("-- " + lbl_choose.Text + " --", "0"));
    }

    private void LoadEmptyReport()
    {
        _PlaceHolder.Controls.Add(GetEmptyRowFilter());
        ScriptManager.RegisterStartupScript(this, this.GetType(), "DisableTable",
            "HideTable();", true);
    }
    HtmlTableRow GetEmptyRowFilter()
    {
        HtmlTableRow row = new HtmlTableRow();
        HtmlTableCell cell = new HtmlTableCell();
        cell.Attributes["class"] = "add_remove";
        HtmlAnchor anchor_add = new HtmlAnchor();
        anchor_add.Attributes["class"] = "add";
        anchor_add.HRef = "javascript:void(0);";
        anchor_add.Attributes["onclick"] = "addRow();";

        HtmlAnchor anchor_Remove = new HtmlAnchor();
        anchor_Remove.Attributes["class"] = "remove";
        anchor_Remove.HRef = "javascript:void(0);";
        anchor_Remove.Attributes["onclick"] = "DeleteRow(this);";
        anchor_Remove.Attributes["style"] = "display:none;";

        cell.Controls.Add(anchor_add);
        cell.Controls.Add(anchor_Remove);
        row.Controls.Add(cell);
        return row;
    }
   
    private void LoadReport(int ReportId)
    {
        int WizardIssueId;
        string name;
        string description;
        
        //     Dictionary<int, string> dicFilters = new Dictionary<int, string>();
        List<FilterSelect> listFilterSelect = new List<FilterSelect>();
        string command = "EXEC dbo.GetWizardReportDetails @ReportId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ReportId", ReportId);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                WizardIssueId = (int)reader["WizardIssueId"];
                name = (string)reader["Name"];
                description = (string)reader["Description"];
            }
            else
                return;
            reader.Dispose();
            cmd.Dispose();

            foreach (ListItem li in ddl_reportIssue.Items)
            {
                li.Selected = (li.Value == WizardIssueId.ToString());
            }
            SetColumnsSelect(WizardIssueId);
            Dictionary<int, string> dicFilters = SetFiltersColumns(WizardIssueId);

            command = "EXEC dbo.GetReportFiltersByReportId @ReportId";
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ReportId", ReportId);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                FilterSelect fs = new FilterSelect();
                fs._value = (string)reader["Value"];
                fs.WizardFilterId = (int)reader["Wizard_FilterId"];
                fs.WizardOperatorId = (int)reader["OperatorId"];
                listFilterSelect.Add(fs);
            }
            ReportWizardService rws = new ReportWizardService();
            foreach (FilterSelect fs in listFilterSelect)
            {
                _PlaceHolder.Controls.Add(rws.GetNewHTMLRow(WizardIssueId, fs));
            }
            if (listFilterSelect.Count == 0)
            {
                _PlaceHolder.Controls.Add(GetEmptyRowFilter());
            }
            reader.Dispose();
            cmd.Dispose();

            command = "EXEC dbo.GetReportColumnsByReportId @ReportId";
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ReportId", ReportId);
            reader = cmd.ExecuteReader();
            int id_sort = -1;
            int select_sort = -1;
            while (reader.Read())
            {
                int _id = (int)reader["Id"];
                string PageName = (string)reader["PageName"];
                string _name = (string)reader["Name"];
                string _name_translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, PageName, _name);
                ListItem li = new ListItem(_name_translate, _id + "");
                ListItem li_group = new ListItem(_name_translate, _id + "");
                selectFieldChoosen.Items.Add(li);
                ddl_SortBy.Items.Add(li);
                ddl_groupBy.Items.Add(li_group);
                selectField.Items.RemoveAt(GetIndexSelectField(_id));
                if (!reader.IsDBNull(2))
                {
                    select_sort = _id;
                    id_sort = (int)reader["Report_SortId"];
                }
                if ((bool)reader["IsGroupBy"])
                {
                    //          ddl_groupBy.Items.Add(li);
                    ddl_groupBy.SelectedIndex = 1;
                    div_sortBy.Attributes["style"] = "display:none;";
                    ddl_SortType.Attributes["style"] = "display:none;";
                }
            }
            if (select_sort != -1)
            {
                foreach (ListItem li in ddl_SortBy.Items)
                    li.Selected = (select_sort.ToString() == li.Value);
                foreach (ListItem li in ddl_SortType.Items)
                    li.Selected = (id_sort.ToString() == li.Value);
                ddl_SortType.Attributes.Add("style", "display:inline");
            }
            string script = @"$('._datepicker').datepicker({ onClose: function(dateText, inst) { OnBlurString(this); }  });";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "DatePicker", script, true);
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFirstTimeFilters", "SetFiltersFromServer('" + WizardIssueId + "');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SetListAjax", "SetListAjax();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "DisableAllAddExeptlast", "DisableAllAddExeptlast();", true);
            conn.Close();
        }

    }
    int GetIndexSelectField(int _id)
    {
        for (int i = 0; i < selectField.Items.Count; i++)
        {
            if (selectField.Items[i].Value == _id.ToString())
                return i;
        }
        return -1;               
        
    }
    private void SetColumnsSelect(int _issue)
    {
        string command = "EXEC dbo.GetColumnsByWizardIssueId @WizardFilterId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@WizardFilterId", _issue);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string name = (string)reader["Name"];
                int _id = (int)reader["Id"];
                string Page_name = (string)reader["PageName"];
                string translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, Page_name, name);
                ListItem li = new ListItem(translate, _id + "");
                selectField.Items.Add(li);
                //           ddl_SortBy.Items.Add(li);
            }
            conn.Close();
        }
    }

    private Dictionary<int, string> SetFiltersColumns(int WizardIssueId)
    {
        Dictionary<int, string> dic = new Dictionary<int, string>();
        string command = "EXEC dbo.GetWizardFiltersByWizardIssueId @WizardIssueId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@WizardIssueId", WizardIssueId);
            SqlDataReader reader = cmd.ExecuteReader();
            //     int firstOne = -1;
            while (reader.Read())
            {
                string name = (string)reader["Name"];
                int _id = (int)reader["Id"];
                string translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eWizardFilters", name);
                dic.Add(_id, translate);
            }
            conn.Close();
        }
        return dic;
    }

    private void SetSortType()
    {
        string command = "EXEC dbo.GetReportSort";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string name = (string)reader["Name"];
                int _id = (int)reader["Id"];
                string translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eSortType", name);
                ddl_SortType.Items.Add(new ListItem(translate, _id + ""));
            }
            conn.Close();
        }
        ddl_SortType.Attributes.Add("style", "display:none");
    }
    void SetIssueRepor()
    {
        ddl_reportIssue.Items.Clear();
        ddl_reportIssue.Items.Add(new ListItem(lbl_choose.Text, "0"));
        string command = "EXEC dbo.GetWizardIssues";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string name = (string)reader["Name"];
                int _id = (int)reader["Id"];
                string translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eReportIssue", name);
                ddl_reportIssue.Items.Add(new ListItem(translate, _id + ""));
            }
            conn.Close();
        }
        ddl_reportIssue.SelectedIndex = 0;
       
    }

    protected void btn_Create_Click(object sender, EventArgs e)
    {
        int ReportId = -1;
        if (ReportIdV > 0)
            DeleteReport(ReportIdV);
        int WizardIssueId = int.Parse(ddl_reportIssue.SelectedValue);
    //    string _values = hf_filters.Value.Split(new string[] { ";;" }, StringSplitOptions.None); 
        string command = "EXEC dbo.InsertReportWizard @Wizaed_IssueId, @UserId, " +
            "@SiteNameId, @ReportColumns, @ReportFilters, @ReportFiltersValues, " +
            "@WizardOperators, @SortTypeId, @ColumnSort, @IsGroupBy";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@Wizaed_IssueId", WizardIssueId);
            cmd.Parameters.AddWithValue("@UserId", new Guid(userManagement.GetGuid));
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@ReportColumns", hf_selectField.Value);
            cmd.Parameters.AddWithValue("@ReportFilters", hf_filters.Value);
            cmd.Parameters.AddWithValue("@ReportFiltersValues", hf_values.Value);
            cmd.Parameters.AddWithValue("@WizardOperators", hf_operators.Value);
            if (string.IsNullOrEmpty(hf_sortBy.Value))
            {
                cmd.Parameters.AddWithValue("@SortTypeId", DBNull.Value);
                cmd.Parameters.AddWithValue("@ColumnSort", DBNull.Value);
            }
            else
            {
                string[] _sort = hf_sortBy.Value.Split(';');
                int ColumnId = int.Parse(_sort[0]);
                cmd.Parameters.AddWithValue("@SortTypeId", int.Parse(ddl_SortType.SelectedValue));
                cmd.Parameters.AddWithValue("@ColumnSort", ColumnId);
            }
            cmd.Parameters.AddWithValue("@IsGroupBy", !(string.IsNullOrEmpty(hf_groupBy.Value)));
            ReportId = (int)cmd.ExecuteScalar();
            conn.Close();
        }
        Response.Redirect("ReportWizardResult.aspx?ReportId=" + ReportId);
    }

    private void DeleteReport(int ReportId)
    {
        string command = "EXEC dbo.DeleteWizardReportIfNotConfirmed @ReportId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ReportId", ReportId);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
    
    int ReportIdV
    {
        get { return (ViewState["ReportId"] == null) ? -1 : (int)ViewState["ReportId"]; }
        set { ViewState["ReportId"] = value; }
    }    
}

