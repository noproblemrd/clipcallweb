﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AutoRerouteReport.aspx.cs" Inherits="Publisher_AutoRerouteReport" %>

<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<link type="text/css" rel="Stylesheet" href="../scripts/jquery-ui-1.9.2.custom.dialog.min.css" />

<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="../scripts/jquery-ui-1.9.2.custom.DatePicker_Dialog.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var DaysInterval = new Number();
    var ChoosenIntervalOk = new Boolean();
    function CheckCurrentDates() {
        var elem = document.getElementById("<%# GetDivCurrentId %>");
        var result = Check_DateValidation(elem);
        if (!result)
            alert("<%# GetInvalidDate %>");
        return result;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
        <div class="main-inputs" style="width: 100%;">	
            <div class="form-fieldDleft">
    
        <div class="dash"><uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" /></div>
 
        </div>
        </div>
        <div class="div-btnRunReport">
            <asp:Button ID="btn_run" runat="server" Text="Create report"  CssClass="CreateReportSubmit2"
                onclick="btn_run_Click" OnClientClick="return CheckCurrentDates();" />    
        </div>
    </div>
    <div class="clear"><!-- --></div> 
    <div>
         <div runat="server" id="div_table_title">
            <table class="data-table table-borde table-reroute">
            <tr>
            <td>
                <asp:Label ID="lbl_TotalServiceProviders" runat="server" Text="Total service providers"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_TotalServiceProviders" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
            </tr>

            <tr>
            <td>
                <asp:Label ID="lbl_NumberOfRerouteds" runat="server" Text="Total number of rerouteds made"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_NumberOfRerouteds" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
            </tr>

            <tr>
            <td>
                <asp:Label ID="lbl_ReroutedsNotAnswered" runat="server" Text="Total number of rerouteds made and were not answered"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_ReroutedsNotAnswered" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
            </tr>

            <tr>
            <td>
                <asp:Label ID="lbl_ReroutedsAnswered" runat="server" Text="Total number of rerouteds made and were answered"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_ReroutedsAnswered" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
            </tr>

            <tr>
            <td>
                <asp:Label ID="lbl_PercentageOfAnsweredCallsFromReroutedCalls" runat="server" Text="Percentage of answered calls from rerouted calls"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_PercentageOfAnsweredCallsFromReroutedCalls" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
            </tr>
            </table>
            
        
    </div>
</div>
</div>
    <asp:Label ID="lbl_RerouterReport" runat="server" Text="Reroute Report" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DateError" runat="server" Text="Invalid date" Visible="false"></asp:Label>
</asp:Content>

