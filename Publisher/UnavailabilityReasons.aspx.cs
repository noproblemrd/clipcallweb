﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml.Linq;

public partial class Publisher_UnavailabilityReasons : PageSetting
{
    const string ExcelName = "UnavailabilityReasons";
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        txt_OutOfBalance.Attributes.Add("readonly", "readonly");
        ScriptManager1.RegisterAsyncPostBackControl(btn_Set);
        if (!IsPostBack)
        {
            SetToolBox();
            popUpaddUser.DataBind();
            LoadeYesNo();
            LoadGroupsUnavailabilityReasons();
            LoadUnavailabilityReasons();
        }
        SetToolBoxEvents();
        Header.DataBind();
    }

    private void LoadGroupsUnavailabilityReasons()
    {
        ddl_Group.Items.Clear();
        foreach (eUnavailabilityReasonGroup urg in Enum.GetValues(typeof(eUnavailabilityReasonGroup)))
        {
            ListItem li = new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eUnavailabilityReasonGroup", urg.ToString()), ((int)urg).ToString());
            ddl_Group.Items.Add(li);
        }
    }

    private void LoadUnavailabilityReasons()
    {
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        string result = string.Empty;
        try
        {
            result = supplier.GetUnavailabilityReasons();

        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);  

        if (xdd.Element("UnavailabilityResons") == null || xdd.Element("UnavailabilityResons").Element("Error") != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
       
        Dictionary<eYesNo, string> dic = YesNoV;
        DataTable data = new DataTable();
        data.Columns.Add("ID", typeof(string));
        data.Columns.Add("GuidId", typeof(string));
        data.Columns.Add("Name", typeof(string));
        data.Columns.Add("OutOfBalance", typeof(string));
        data.Columns.Add("ShowToAdvertiser", typeof(string));
        data.Columns.Add("OutOfBalanceYN", typeof(string));
        data.Columns.Add("ShowToAdvertiserYN", typeof(string));
        data.Columns.Add("Group", typeof(string));
        data.Columns.Add("GroupInt", typeof(string));
        foreach (XElement node in xdd.Element("UnavailabilityResons").Elements())
        {
            string Inactive = node.Attribute("Inactive").Value;
            if (Inactive != "0")
                continue;
            DataRow row = data.NewRow();
            string reason = node.Value;
            string reasonGuidId = node.Attribute("ID").Value;
            string reasonId = node.Attribute("IDsmall").Value;
            
            string DisplayLevel = node.Attribute("DisplayLevel").Value;
            row["GuidId"] = reasonGuidId;
            row["ID"] = reasonId;
            row["Name"] = reason;
            string _GroupInt = node.Attribute("GroupCode").Value;
            row["GroupInt"] = _GroupInt;
            string GroupName = ((eUnavailabilityReasonGroup)(int.Parse(_GroupInt))).ToString();
            row["Group"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eUnavailabilityReasonGroup", GroupName);
            if (DisplayLevel == "2")
            {
                row["OutOfBalance"] = dic[eYesNo.Yes];
                row["OutOfBalanceYN"] = eYesNo.Yes.ToString();
                row["ShowToAdvertiser"] = dic[eYesNo.No];
                row["ShowToAdvertiserYN"] = eYesNo.No.ToString();
            }
            else
            {
                row["OutOfBalance"] = dic[eYesNo.No];
                row["OutOfBalanceYN"] = eYesNo.No.ToString();
                if (DisplayLevel == "1")
                {
                    row["ShowToAdvertiser"] = dic[eYesNo.No];
                    row["ShowToAdvertiserYN"] = eYesNo.No.ToString();
                }
                else
                {
                    row["ShowToAdvertiser"] = dic[eYesNo.Yes];
                    row["ShowToAdvertiserYN"] = eYesNo.Yes.ToString();
                }
            }
            data.Rows.Add(row);                
        }
        dataV = data;
    }
    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
        Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
    }

    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        bool IsSelect = false;
        int indx = -1;
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
            {
                if (IsSelect)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ChooseOne", "alert('" + Toolbox1.ChooseOne + "');", true);
                    return;
                }
                IsSelect = true;
                indx = row.RowIndex;
            }
        }
        if (indx > -1)
        {
            GridViewRow row = _GridView.Rows[indx];
            hf_guid.Value = ((Label)row.FindControl("lbl_GuidId")).Text;
            txt_ID.Text = ((Label)row.FindControl("lbl_CodeId")).Text;
            txt_Name.Text = ((Label)row.FindControl("lbl_ReasonName")).Text;
            string _OutOfBalance = ((Label)row.FindControl("lbl_OutOfBalance_YN")).Text;
            hf_OutOfBalance.Value = _OutOfBalance;
            eYesNo _yesNo = (eYesNo)(class_enum.GetValue(typeof(eYesNo), _OutOfBalance));
            bool IsDisable = _yesNo == eYesNo.Yes;
            txt_OutOfBalance.Text = YesNoV[_yesNo];
            string _ShowToAdvertiser = ((Label)row.FindControl("lbl_ShowToAdvertiser_YN")).Text;
            _yesNo = (eYesNo)(class_enum.GetValue(typeof(eYesNo), _ShowToAdvertiser));
            foreach (ListItem li in ddl_ShowToAdvertiser.Items)
            {
                li.Selected = (li.Value == _yesNo.ToString());
            }
            ddl_ShowToAdvertiser.Enabled = !IsDisable;
            string GroupInt = ((Label)row.FindControl("lblGroup")).Text;
            for (int i = 0; i < ddl_Group.Items.Count; i++)
            {
                if (ddl_Group.Items[i].Value == GroupInt)
                {
                    ddl_Group.SelectedIndex = i;
                    break;
                }
            }
            ddl_Group.Enabled = !IsDisable;
        }
        popUpaddUser.Attributes["class"] = "popModal_del";
        _mpe.Show();
    }

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        List<Guid> list = new List<Guid>();
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            if (cb.Checked)
            {
                string _YesNo = ((Label)row.FindControl("lbl_OutOfBalance_YN")).Text;
                if (_YesNo == eYesNo.Yes.ToString())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CantDelete", @"alert(""" + lbl_CantDelete.Text + @""");", true);
                    return;
                }
                Guid _id = new Guid(((Label)row.FindControl("lbl_GuidId")).Text);                
                list.Add(_id);              
            }
        }
        if (list.Count == 0)
            return;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.DeleteUnavailabilityReasons(list.ToArray());
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);           
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        LoadUnavailabilityReasons();
        _btn_Search();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataViewV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            return;
        }
        //  PrintHelper.PrintWebControl(_GridView, dataViewV);
        Session["data_print"] = dataViewV.ToTable();
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        if (dataViewV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordExport + "');", true);
            return;
        }
        ToExcel to_excel = new ToExcel(this, ExcelName);
        if (!to_excel.ExecExcel(dataViewV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void Toolbox1_AddExec(object sender, EventArgs e)
    {
        
    }

    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        _btn_Search();
    }
    private void _btn_Search()
    {
        string name = Toolbox1.GetTxt;
        HtmlInputText _tb = Toolbox1.GetTextBoxSearch;
        if (name == Toolbox1.GetWaterTextSearch &&
                    _tb.Attributes["class"].Contains("watermark"))
            name = string.Empty;
        if (string.IsNullOrEmpty(name))
            dataViewV = dataV.AsDataView();
        else
        {
            name = name.ToLower();
            DataTable data = dataV;
            data.TableName = "Search";

            EnumerableRowCollection<DataRow> query = from Search in data.AsEnumerable()
                                                     where ((Search.Field<string>("Name").ToLower().Contains(name))
                                                        || (Search.Field<string>("ID").ToLower().Contains(name)))
                                                     orderby Search.Field<string>("Name")
                                                     select Search;

            dataViewV = query.AsDataView();
        }
        _GridView.PageIndex = 0;
        SetGridView();
    }
    void SetGridView()
    {
        StringBuilder sb = new StringBuilder();
        DataView data = dataViewV;
        //    _GridView.PageIndex = 0;
        _GridView.DataSource = data;
        _GridView.DataBind();

        if (data.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noreasultSearch", "alert('" + lbl_noreasultSearch.Text + "');", true);
            CBsV = string.Empty;
            _updatePanelTable.Update();
            return;
        }
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }

        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        _updatePanelTable.Update();
    }
    protected void btn_Set_Click(object sender, EventArgs e)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.UpsertUnavailabilityReasonRequest _request = new WebReferenceSite.UpsertUnavailabilityReasonRequest();
        Guid _guid = (string.IsNullOrEmpty(hf_guid.Value)) ? Guid.Empty : new Guid(hf_guid.Value);
        _request.Guid = _guid;
        eYesNo _YesNo;//(eYesNo)(class_enum.GetValue(typeof(eYesNo), ddl_ShowToAdvertiser.SelectedValue));
        if (!Enum.TryParse<eYesNo>(ddl_ShowToAdvertiser.SelectedValue, out _YesNo))
            return;
        _request.IsSystem = _YesNo == eYesNo.No;
        _request.Name = txt_Name.Text.Trim();
        _request.GroupId = int.Parse(ddl_Group.SelectedValue);
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.UpsertUnavailabilityReason(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClearPopUp", "ClearPopUp();", true);
        _mpe.Hide();
        LoadUnavailabilityReasons();
        _btn_Search();
        
    }
    private void LoadeYesNo()
    {
        Dictionary<eYesNo, string> dic = new Dictionary<eYesNo, string>();
        foreach (eYesNo s in Enum.GetValues(typeof(eYesNo)))
        {
            string translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", s.ToString());            
            ListItem li2 = new ListItem(translate, s.ToString());
            ddl_ShowToAdvertiser.Items.Add(li2);
            dic.Add(s, translate);
        }        
        ddl_ShowToAdvertiser.SelectedIndex = 0;
        YesNoV = dic;
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView();
    }
    private void SetToolBox()
    {
        Toolbox1.RemoveAttach();
        string script = "openPopup(); return false;";
        Toolbox1.SetClientScriptToAdd(script);
        Toolbox1.SetConfirmDelete = lbl_Delete.Text;
    }


    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    private DataView dataViewV
    {
        get { return (Session["dataView"] == null) ? null : (DataView)Session["dataView"]; }
        set { Session["dataView"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
    protected Dictionary<eYesNo, string> YesNoV
    {
        get { return (ViewState["YesNo"] == null) ? null : (Dictionary<eYesNo, string>)ViewState["YesNo"]; }
        set { ViewState["YesNo"] = value; }
    }
}