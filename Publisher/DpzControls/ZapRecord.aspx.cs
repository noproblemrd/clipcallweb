﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Publisher_DpzControls_ZapRecord : PageSetting
{
    //const string SCRAMBEL = "simsim";
    string _ZapRecord;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!userManagement.IsAuthenticated())
            Response.Redirect(ResolveUrl("~") + "Publisher/LogOut.aspx");
        _ZapRecord = Request.QueryString["record"];
        if (string.IsNullOrEmpty(_ZapRecord))
        {
            Header.DataBind();
            ClientScript.RegisterStartupScript(this.GetType(), "ErrorRecord", "RecordError();", true);
            return;
        }
        string RecordPath = EncryptString.DecodeRecordPath(_ZapRecord);        
        if (!RecordPath.StartsWith("http:") && !File.Exists(RecordPath))
        {
            Header.DataBind();
            ClientScript.RegisterStartupScript(this.GetType(), "ErrorRecord", "RecordError();", true);
        }
        
        this.DataBind();
    }
    protected string ZapRecord
    {
        get { return (string.IsNullOrEmpty(_ZapRecord)) ? string.Empty : 
            ResolveUrl("~/Publisher/DpzControls/_GetRecord.ashx?record=" + HttpUtility.UrlEncode(_ZapRecord)); }
    }
    protected string ErrorRecord
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_RecordError.Text); }
    }
}