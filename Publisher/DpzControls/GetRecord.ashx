﻿<%@ WebHandler Language="C#" Class="GetRecord" %>

using System;
using System.Web;
using System.IO;

public class GetRecord : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        context.Response.Expires = 0;
        context.Response.CacheControl = "no-cache";
        context.Response.Cache.SetNoStore();
        context.Response.AppendHeader("Pragma", "no-cache");

        string app = context.Request.Form["app"];
        string AppPassword = context.Request.Form["AppPassword"];
        string user = context.Request.Form["user"];
        string password = context.Request.Form["password"];
        string audioId = context.Request.Form["audio"];
        
        if (app != "zap" || AppPassword != "1q2w3e4r!")
        {
            ErrorParams(context, "Application login faild!");
            return;
        }
        SiteSetting ss;
        if (!Utilities.IsPublisher(context.Request, user, password, out ss))
        {
            ErrorParams(context, "User login faild!");
            return;
        }
         
        string RecordPath = DpzUtilities.GetPathRecord(audioId, ss);
    //    string RecordPath = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"] + audioId + ".mp3";
        if (string.IsNullOrEmpty(audioId))
        {
            ErrorParams(context, "RecordId not found!");
            return;
        }
          /*
        string fileName;
        string TempPath = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
  //      string TempPathWeb = System.Configuration.ConfigurationManager.AppSettings["professionalRecordWeb"];
       // string _path = dic[id];
        string p_record;
        if (!audioId.Contains("http"))
        {
            if (string.IsNullOrEmpty(audioId) || !File.Exists(audioId))
            {
                if (!File.Exists(audioId))
                    dbug_log.ExceptionLog(new Exception(audioId));
                ErrorParams(context, "RecordId not found!");
                return;
            }

            fileName = System.IO.Path.GetFileName(audioId);
            File.Copy(audioId, TempPath + fileName, true);
            p_record = TempPathWeb + fileName;
        }
        else
        {
            fileName = Guid.NewGuid().ToString() + ".mp3";
            if (!DpzUtilities.GetPathTwillo(audioId, TempPath + fileName))
            {
                ErrorParams(context, "RecordId not found!");
                return;
            }
            p_record = TempPathWeb + fileName;
        }
        return p_record;   
           * */
   //     FileInfo fi = new FileInfo(RecordPath);
      //  context.Response.Buffer = true;
        byte[] byteArray = File.ReadAllBytes(RecordPath);
        context.Response.ContentType = @"audio/mpeg";
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + audioId + ".mp3");
        context.Response.OutputStream.Write(byteArray, 0, byteArray.Length);
        
     //   FileStream stream = new FileStream(RecordPath, FileMode.Open);
      //  byte[] mp3 = null;
      //  mp3 = new byte[System.Convert.ToInt32(stream.Length)];

      //  stream.Read(mp3, 0, System.Convert.ToInt32(stream.Length));
      //  stream.Close();
  //      context.Response.AppendHeader("Content-Length", fi.Length.ToString());
        
    //    context.Response.AppendHeader("Content-Disposition", audioId + ".mp3");
       // string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
       // path += "test.mp3";
       // context.Response.OutputStream.Write(mp3, 0, mp3.Length);
    //    context.Response.BinaryWrite(System.IO.File.ReadAllBytes(RecordPath));
     //   context.ApplicationInstance.CompleteRequest();
    //    context.Response.WriteFile(RecordPath);
   //     context.Response.TransmitFile(RecordPath);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
    void ErrorParams(HttpContext context, string message)
    {
        context.Response.ContentType = "application/json; charset=UTF-8;";
        context.Response.StatusCode = 200; // error
     //  context.Response.SuppressContent = true;
       context.Response.Write(message);
       context.Response.End();
    }

}
/*
 *         <input  name="app" type="hidden" value="zap" />
         <input  name="AppPassword" type="hidden" value="1q2w3e4r!" />
          <input  name="user" type="hidden" value="" />
           <input  name="password" type="hidden" value="" />

 * */