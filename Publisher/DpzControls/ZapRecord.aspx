﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ZapRecord.aspx.cs" Inherits="Publisher_DpzControls_ZapRecord" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function RecordError() {
            alert('<%# ErrorRecord %>');
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <OBJECT ID="MediaPlayer" WIDTH="192" HEIGHT="190" CLASSID="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
            STANDBY="Loading Windows Media Player components..." TYPE="application/x-oleobject">
            <PARAM NAME="FileName" VALUE="<%# ZapRecord %>">
            <PARAM name="autostart" VALUE="true">
            <PARAM name="ShowControls" VALUE="true">
            <param name="ShowStatusBar" value="true">
            <PARAM name="ShowDisplay" VALUE="false">
            <EMBED TYPE="application/x-mplayer2" SRC="<%# ZapRecord %>" NAME="MediaPlayer"
            WIDTH="192" HEIGHT="190" ShowControls="1" ShowStatusBar="1" ShowDisplay="0" autostart="1"> </EMBED>
        </OBJECT>
    </div>
     
    <asp:Label ID="lbl_RecordError" runat="server" Text="Record not found!" Visible="false"></asp:Label>
    </form>
</body>
</html>
