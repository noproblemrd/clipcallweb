﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SourceCallReport.ascx.cs" Inherits="Publisher_DpzControls_SourceCallReport" %>

<asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" CssClass="data-table -GridView">
<AlternatingRowStyle CssClass="odd" />
<Columns>
    <asp:TemplateField SortExpression="num">
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_num.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label2" runat="server" Text="<%# Bind('num') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="CallerId">
        <HeaderTemplate>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_CallerId.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label4" runat="server" Text="<%# Bind('CallerId') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Count">
        <HeaderTemplate>
            <asp:Label ID="Label5" runat="server" Text="<%# lbl_Count.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('Count') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

</Columns>
</asp:GridView>

<asp:Label ID="lbl_num" runat="server" Text="Num" Visible="false"></asp:Label>
<asp:Label ID="lbl_CallerId" runat="server" Text="Source Call" Visible="false"></asp:Label>
<asp:Label ID="lbl_Count" runat="server" Text="Number of Calls" Visible="false"></asp:Label>

<input id="hf_GetReport" type="hidden" value="<%# GetSourceCallReport %>" />
