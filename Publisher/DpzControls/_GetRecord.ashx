﻿<%@ WebHandler Language="C#" Class="_GetRecord" %>

using System;
using System.Web;
using System.IO;

public class _GetRecord : IHttpHandler {
 //   const string SCRAMBEL = "simsim";
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        context.Response.Expires = 0;
        context.Response.CacheControl = "no-cache";
        context.Response.Cache.SetNoStore();
        context.Response.AppendHeader("Pragma", "no-cache");
        context.Response.ContentType = @"audio/mpeg";

        string RecordPath = context.Request.QueryString["record"];
        if (string.IsNullOrEmpty(RecordPath))
        {
            context.Response.SuppressContent = true;
            context.Response.End();
            return;
        }
        byte[] byteArray;
        RecordPath = EncryptString.DecodeRecordPath(RecordPath);
        if (RecordPath.StartsWith("http:"))
        {
            byteArray = Utilities.GetTwillo(RecordPath);
        }
        else
        {
            if (!File.Exists(RecordPath))
            {
                //      RecordPath = @"D:\upload\123456.mp3";
                context.Response.SuppressContent = true;
                context.Response.End();
                return;
            }
            byteArray = File.ReadAllBytes(RecordPath);
        }
        
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=Record.mp3");
        context.Response.OutputStream.Write(byteArray, 0, byteArray.Length);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}