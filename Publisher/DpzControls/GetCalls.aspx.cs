﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

public partial class Publisher_DpzControls_GetCalls : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        NameValueCollection nvc = Request.Form;
        string app = nvc["app"];
        string AppPassword = nvc["AppPassword"];
        if (app != "zap" && AppPassword != "1q2w3e4r")
        {
            AccessDenied();
            return;
        }
        string _user = nvc["user"];
        string _password = nvc["password"];
        string BizId = nvc["BizId"];
        
        if (!Utilities.PublisherLogin(this, _user, _password))
        {
            AccessDenied();
            return;
        }
        Response.Redirect(ResolveUrl("~/Publisher/DpzReport.aspx?bizid=" + BizId));

    }
    protected void AccessDenied()
    {
        Response.Redirect(ResolveUrl("~/AccessDenied.aspx"));
    }
}