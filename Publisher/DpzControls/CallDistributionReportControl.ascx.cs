﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Configuration;
using System.IO;
using System.Web.UI.HtmlControls;
public partial class Publisher_DpzControls_CallDistributionReportControl : UserControlParent
{
    PageSetting _page;
    const string FILE_NAME = "DistributionReport.xml";
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
       
        
    }
    protected void LoadChart()
    {
        if (!string.IsNullOrEmpty(PathV))
        {
            //string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSLine.swf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + GetChartFlash() + "');", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CleanChart",
                "CleanChart();", true);

    }
    public void SetData(WebReferenceReports.DapazGroupedCallsContainer[] result, WebReferenceReports.eDapazCallsReportGroupBy edcrg)
    {
        this.EnableViewState = false;
        dataV = null;
        PageIndex = 0;
        if (result == null)
            return;
        _page = (PageSetting)Page;
        DataTable data = new DataTable();
        data.Columns.Add("num", typeof(int));
        data.Columns.Add("AverageDurationInSeconds");
        data.Columns.Add("CallsTimeInSeconds");
        data.Columns.Add("NumberOfCallers");
        data.Columns.Add("Time");
        int i = 1;
        eDpzReportType edrt = eDpzReportType.CallDistributionReportByHour;
        foreach (WebReferenceReports.DapazGroupedCallsContainer dgcc in result)
        {
            DataRow row = data.NewRow();
            row["num"] = i++;
            row["AverageDurationInSeconds"] = new TimeSpan(0, 0, dgcc.AverageDurationInSeconds).ToString();
            row["CallsTimeInSeconds"] = new TimeSpan(0, 0, dgcc.CallsTimeInSeconds).ToString();
            row["NumberOfCallers"] = dgcc.NumberOfCallers;
            string _time = string.Empty;
            
            switch (edcrg)
            {
                case(WebReferenceReports.eDapazCallsReportGroupBy.Day):
                    _time = string.Format(_page.siteSetting.DateFormat, dgcc.Time);
                    edrt = eDpzReportType.CallDistributionReportByDay;
                    break;
                case(WebReferenceReports.eDapazCallsReportGroupBy.Hour):
                    _time = string.Format(_page.siteSetting.TimeFormat, dgcc.Time);
                    edrt = eDpzReportType.CallDistributionReportByHour;
                    break;
                case(WebReferenceReports.eDapazCallsReportGroupBy.Month):
                    _time = string.Format(@"{0:MMMM, yyyy}", dgcc.Time);
                    edrt = eDpzReportType.CallDistributionReportByMonth;
                    break;
            }
            row["Time"] = _time;
            data.Rows.Add(row);            
        }
        dataV = data;
        IEnumerable<DataRow> query = (from x in data.AsEnumerable()
                                      select x).Take(20);
        int _count = query.Count();
        _GridView.DataSource = (_count == 0) ? null : query.CopyToDataTable();
        _GridView.DataBind();
        if (_count > 0)
        {
            ChartV = edcrg;
            if (edcrg == WebReferenceReports.eDapazCallsReportGroupBy.Month)
                LoadChart(result);
            else
                LoadChart(result, edrt);
        }
        
    }
    public string CreateExcel()
    {
        ToExcel toexcel = new ToExcel(null, "Report");
        DataTable data = GetDataTable.GetDataTableFromDataTableAndGridView(dataV, _GridView);
        return toexcel.CreateExcel(data);
    }
    #region  chart
    void LoadChart(WebReferenceReports.DapazGroupedCallsContainer[] result, eDpzReportType edrt)
    {
        
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + EditTranslateDB.GetControlTranslate(_page.siteSetting.siteLangId, "eDpzReportType", edrt.ToString()) + @"' ");
        //    sb.Append(@"subCaption='In Thousands' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='3' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");
    //    sb.Append(@"numberSuffix='%' ");
        
        //    sb.Append(@"rotateNames='1' ");
        //     sb.Append(@"slantLabels='1' ");

        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar=' ' ");

        sb.Append(@"bgColor='FFFFFF'>");

        IEnumerable<WebReferenceReports.DapazGroupedCallsContainer> query = from x in result
                                                                            orderby x.Time
                                                                            select x;
        /*
        int modulu = data.Rows.Count / 15;
        modulu++;
        */
        StringBuilder sbTime = new StringBuilder();
        StringBuilder sbCalls = new StringBuilder();
        sb.Append(@"<categories>");
        sbTime.Append(@"<dataset seriesName='" + lbl_Minutes.Text + @"' color='808080' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
        sbCalls.Append(@"<dataset seriesName='" + lbl_Calls.Text + @"' color='0080C0'  >");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'
        string _DateFormat = string.Empty;
        int HourInterval = 1;
        switch (edrt)
        {
            case (eDpzReportType.CallDistributionReportByDay):
                _DateFormat = _page.siteSetting.DateFormat;
                HourInterval = 24;
                break;
            case (eDpzReportType.CallDistributionReportByHour):
                _DateFormat = _page.siteSetting.TimeFormat;
                HourInterval = 1;
                break;
            case (eDpzReportType.CallDistributionReportByMonth):
                HourInterval = 1;
                _DateFormat = @"{0:MMMM, yyyy}";
                break;
        }
        DateTime startdt = StartDate;// query.First().Time;
        DateTime enddt = (EndDate > query.Last().Time) ? EndDate : query.Last().Time;
        int i = 0;
        int maxcount = query.Count();
        while (startdt <= enddt)//(int i = query.Count() - 1; i > -1; i--)
        {
            if (i < maxcount && startdt == query.ElementAt(i).Time)
            {
                string CallsDuration = (query.ElementAt(i).CallsTimeInSeconds / 60).ToString();
                string CallTime = string.Format(_DateFormat, query.ElementAt(i).Time);
                string NumOfCalls = query.ElementAt(i).NumberOfCallers.ToString();
                
        //        string _title = lbl_Calls.Text + "=" + NumOfCalls + "\r\n" +
         //           lbl_Minutes.Text + "=" + CallsDuration + "\r\n";
                string _title = string.Format(_page.siteSetting.DateFormat, query.ElementAt(i).Time) + " " + string.Format(_page.siteSetting.TimeFormat, query.ElementAt(i).Time);
                sb.Append(@"<category name='" + CallTime + @"' toolText='" + _title + "'/>");

                sbTime.Append(@"<set value='" + CallsDuration + "'/>");
                sbCalls.Append(@"<set value='" + NumOfCalls + "'/>");
                i++;
            }
            else
            {
                string CallTime = string.Format(_DateFormat, startdt);
     //           string _title = lbl_Calls.Text + "=" + 0 + "\r\n" +
     //               lbl_Minutes.Text + "=" + 0 + "\r\n";
                string _title = string.Format(_page.siteSetting.DateFormat, startdt) + " " + string.Format(_page.siteSetting.TimeFormat, startdt);
                sb.Append(@"<category name='" + CallTime + @"' toolText='" + _title + "'/>");

                sbTime.Append(@"<set value='" + 0 + "'/>");
                sbCalls.Append(@"<set value='" + 0 + "'/>");
            }
            startdt = startdt.AddHours(HourInterval);
        }
        
        sb.Append("</categories>");
        sbTime.Append(@"</dataset>");
        sbCalls.Append(@"</dataset>");

        sb.Append(sbTime.ToString());
        sb.Append(sbCalls.ToString());
        sb.Append(@"</graph>");


        string path = //HttpContext.Current.Request.PhysicalApplicationPath + 
            ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) 
        {
            dbug_log.ExceptionLog(ex, _page.siteSetting.GetSiteID);
            return; 
        }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        LoadChart();
    }

    void LoadChart(WebReferenceReports.DapazGroupedCallsContainer[] result)
    {
      
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<chart  caption='" + EditTranslateDB.GetControlTranslate(_page.siteSetting.siteLangId, "eDpzReportType", eDpzReportType.CallDistributionReportByMonth.ToString()) + @"' ");
        sb.Append(@"showLabels='1' showvalues='1' decimals='0'>");

        IEnumerable<WebReferenceReports.DapazGroupedCallsContainer> query = from x in result
                                                                            orderby x.Time
                                                                            select x;
        /*
        int modulu = data.Rows.Count / 15;
        modulu++;
        */
        StringBuilder sbTime = new StringBuilder();
        StringBuilder sbCalls = new StringBuilder();
        sb.Append(@"<categories>");

        sbTime.Append(@"<dataset seriesName='" + lbl_Minutes.Text + @"' color='808080' showValues='1'>");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
        sbCalls.Append(@"<dataset seriesName='" + lbl_Calls.Text + @"' color='0080C0'  showValues='1'>");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'
        string _DateFormat = @"{0:MMMM, yyyy}";

        DateTime startdt = StartDate;// query.First().Time;
        DateTime enddt = (EndDate > query.Last().Time) ? EndDate : query.Last().Time;
        int i = 0;
        int maxcount = query.Count();
        while (startdt <= enddt)//(int i = query.Count() - 1; i > -1; i--)
        {
            if (i < maxcount && startdt == query.ElementAt(i).Time)
            {
                string CallsDuration = (query.ElementAt(i).CallsTimeInSeconds / 60).ToString();
                string CallTime = string.Format(_DateFormat, query.ElementAt(i).Time);
                string NumOfCalls = query.ElementAt(i).NumberOfCallers.ToString();

                //        string _title = lbl_Calls.Text + "=" + NumOfCalls + "\r\n" +
                //           lbl_Minutes.Text + "=" + CallsDuration + "\r\n";
                string _title = string.Format(_page.siteSetting.DateFormat, query.ElementAt(i).Time) + " " + string.Format(_page.siteSetting.TimeFormat, query.ElementAt(i).Time);
                sb.Append(@"<category name='" + CallTime + @"' toolText='" + _title + "'/>");

                sbTime.Append(@"<set value='" + CallsDuration + "'/>");
                sbCalls.Append(@"<set value='" + NumOfCalls + "'/>");
                i++;
            }
            else
            {
                string CallTime = string.Format(_DateFormat, startdt);
                //           string _title = lbl_Calls.Text + "=" + 0 + "\r\n" +
                //               lbl_Minutes.Text + "=" + 0 + "\r\n";
                string _title = string.Format(_page.siteSetting.DateFormat, startdt) + " " + string.Format(_page.siteSetting.TimeFormat, startdt);
                sb.Append(@"<category name='" + CallTime + @"' toolText='" + _title + "'/>");

                sbTime.Append(@"<set value='" + 0 + "'/>");
                sbCalls.Append(@"<set value='" + 0 + "'/>");
            }
            startdt = startdt.AddMonths(1);
        }

        sb.Append("</categories>");
        sbTime.Append(@"</dataset>");
        sbCalls.Append(@"</dataset>");

        sb.Append(sbTime.ToString());
        sb.Append(sbCalls.ToString());
        sb.Append(@"</chart>");


        string path = //HttpContext.Current.Request.PhysicalApplicationPath + 
            ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, _page.siteSetting.GetSiteID);
            return;
        }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        LoadChart();
    }
    public bool SetDataByPage()
    {
        DataTable data = dataV;
        if (data == null)
            return false;
        PageIndex = PageIndex + 1;
        int _index = PageIndex;
        int _min = (_index * 20);
        IEnumerable<DataRow> data_row = (from x in data.AsEnumerable()
                                         select x).Skip(_min).Take(20);

        if (data_row.Count() == 0)
            return false;
        _GridView.DataSource = data_row.CopyToDataTable();
        _GridView.DataBind();
        return true;
    }
    #endregion
    string PathV
    {
        get { return (ViewState["XmlGraph"] == null) ? "" : (string)ViewState["XmlGraph"]; }
        set { ViewState["XmlGraph"] = value; }
    }
    string GetChartFlash()
    {
        return (ChartV == WebReferenceReports.eDapazCallsReportGroupBy.Month) ? ResolveUrl("~") + "Management/FusionCharts/Charts/MSColumn3D.swf" :
            ResolveUrl("~") + "Management/FusionCharts/Charts/MSLine.swf";
    }
    WebReferenceReports.eDapazCallsReportGroupBy ChartV
    {
        get { return (ViewState["Chart"] == null) ? WebReferenceReports.eDapazCallsReportGroupBy.Hour : (WebReferenceReports.eDapazCallsReportGroupBy)ViewState["Chart"]; }
        set { ViewState["Chart"] = value; }
    }
     DataTable dataV
    {
        get { return (Session["DistributionReport"] == null) ? null : (DataTable)Session["DistributionReport"]; }
        set { Session["DistributionReport"] = value; }
    }
    int PageIndex
    {
        get { return (Session["PageIndexDistributionReport"] == null) ? 0 : (int)Session["PageIndexDistributionReport"]; }
        set { Session["PageIndexDistributionReport"] = value; }
    }
    protected string GetDistributionReport
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetDistributionReport"); }
    }
     
}