﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallReportControl.ascx.cs" Inherits="Publisher_DpzControls_CallReportControl" %>
 
<asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" CssClass="data-table -GridView">
<AlternatingRowStyle CssClass="odd" />
<Columns>
    <asp:TemplateField SortExpression="num">
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_num.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="lbl_LineNumber" runat="server" Text="<%# Bind('num') %>"></asp:Label>
            <asp:Label ID="lblSupplierId" runat="server" Text="<%# Bind('SupplierId') %>" Visible="false"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField SortExpression="SupplierName">
        <HeaderTemplate>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_UserName.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label4" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField SortExpression="BizId">
        <HeaderTemplate>
            <asp:Label ID="Label115" runat="server" Text="<%# lbl_BizId.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label116" runat="server" Text="<%# Bind('BizId') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField SortExpression="CallDateTime">
        <HeaderTemplate>
            <asp:Label ID="Label5" runat="server" Text="<%# lbl_DateTimeCall.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('CallDateTime') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField SortExpression="CallerId">
        <HeaderTemplate>
            <asp:Label ID="Label7" runat="server" Text="<%# lbl_SourceCall.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label8" runat="server" Text="<%# Bind('CallerId') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField SortExpression="VirtualNumber">
        <HeaderTemplate>
            <asp:Label ID="Label9" runat="server" Text="<%# lbl_VirtualNumber.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label10" runat="server" Text="<%# Bind('VirtualNumber') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField SortExpression="TargerNumber">
        <HeaderTemplate>
            <asp:Label ID="Label11" runat="server" Text="<%# lbl_DestinationNumber.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label12" runat="server" Text="<%# Bind('TargerNumber') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField SortExpression="CallDurationInSeconds">
        <HeaderTemplate>
            <asp:Label ID="Label13" runat="server" Text="<%# lbl_DurationCall.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label14" runat="server" Text="<%# Bind('CallDurationInSeconds') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField SortExpression="CallStatus">
        <HeaderTemplate>
            <asp:Label ID="Label15" runat="server" Text="<%# lbl_Status.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label16" runat="server" Text="<%# Bind('CallStatus') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField SortExpression="Rating">
        <HeaderTemplate>
            <asp:Label ID="Label17" runat="server" Text="<%# lbl_Rating.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label18" runat="server" Text="<%# Bind('Rating') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
     <asp:TemplateField SortExpression="RecordFileLocation" Visible="false">
        <HeaderTemplate>
            <asp:Label ID="Label17" runat="server" Text="<%# lbl_Record.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <a id="a_recording" runat="server" href="javascript:void(0);" visible="<%# Bind('HasRecord') %>" onclick="Get_Record(this);">
                <asp:Image ID="img_record" runat="server" ImageUrl="../../Management/calls_images/icon_speaker.png" />
                <asp:HiddenField ID="hf_RecordPath" runat="server" Value="<%# Bind('RecordFileLocation') %>" />
            </a>
        </ItemTemplate>
    </asp:TemplateField>
</Columns>   
</asp:GridView>
<asp:Label ID="lbl_num" runat="server" Text="Num" Visible="false"></asp:Label>
<asp:Label ID="lbl_UserName" runat="server" Text="User name" Visible="false"></asp:Label>
<asp:Label ID="lbl_DateTimeCall" runat="server" Text="Date time call" Visible="false"></asp:Label>
<asp:Label ID="lbl_SourceCall" runat="server" Text="Source call" Visible="false"></asp:Label>
<asp:Label ID="lbl_VirtualNumber" runat="server" Text="Virtual number" Visible="false"></asp:Label>
<asp:Label ID="lbl_DestinationNumber" runat="server" Text="Destination number" Visible="false"></asp:Label>
<asp:Label ID="lbl_DurationCall" runat="server" Text="Duration call" Visible="false"></asp:Label>
<asp:Label ID="lbl_Status" runat="server" Text="Status" Visible="false"></asp:Label>
<asp:Label ID="lbl_Rating" runat="server" Text="Rating" Visible="false"></asp:Label>
<asp:Label ID="lbl_Record" runat="server" Text="Record" Visible="false"></asp:Label>
<asp:Label ID="lbl_BizId" runat="server" Text="Biz Id" Visible="false"></asp:Label>
<input id="hf_GetReport" type="hidden" value="<%# GetCallReport %>" />
<input id="hf_GetRecord" type="hidden" value="<%# GetRecord %>" />