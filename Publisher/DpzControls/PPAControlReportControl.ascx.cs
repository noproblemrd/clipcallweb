﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;

public partial class Publisher_DpzControls_PPAControlReportControl : System.Web.UI.UserControl
{
   // string number_format;
   // int SiteLangId;
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    public void BindReport(WebReferenceReports.PpaControlReportRow[] datas, string number_format, int SiteLangId)
    {
        Dictionary<WebReferenceReports.RechargeTypeCode, string> listRechargeCode = new Dictionary<WebReferenceReports.RechargeTypeCode, string>();
        DataTable data = new DataTable();
        data.Columns.Add("BizId");
        data.Columns.Add("Name");
        data.Columns.Add("Balance");
        data.Columns.Add("IsTotalRow", typeof(string));
        data.Columns.Add("Charged");
        data.Columns.Add("Heading");
        data.Columns.Add("Calls");
        data.Columns.Add("SupplierType");
        data.Columns.Add("RechargeAmount");
        data.Columns.Add("RechargeType");
        data.Columns.Add("Revenue");
        data.Columns.Add("AccountType");
        data.Columns.Add("AccountStatus");
        data.Columns.Add("PricePerCall");
        data.Columns.Add("Deposits");
        data.Columns.Add("South");
        data.Columns.Add("Shfela");
        data.Columns.Add("Center");
        data.Columns.Add("Sharon");
        data.Columns.Add("Jerusalem");
        data.Columns.Add("North");
        int _index = 0;
        foreach (WebReferenceReports.PpaControlReportRow pcrr in datas)
        {
            DataRow row = data.NewRow();
            row["BizId"] = pcrr.BizId;
            row["Name"] = pcrr.Name;
            row["Balance"] = string.Format(number_format, pcrr.Balance);
            row["Calls"] = pcrr.Calls;
            row["Charged"] = pcrr.Charged;
            row["Heading"] = pcrr.Heading;
            row["IsTotalRow"] = pcrr.IsTotalRow;
            row["SupplierType"] = (pcrr.IsOldFromSaar) ? lbl_OldPPA.Text : lbl_NewPPA.Text;
            row["RechargeAmount"] = string.Format(number_format, pcrr.RechargeAmount);
            row["Revenue"] = string.Format(number_format, pcrr.Revenue);
            if (pcrr.RechargeTypeCode.HasValue)
            {
                if (!listRechargeCode.ContainsKey(pcrr.RechargeTypeCode.Value))
                    listRechargeCode.Add(pcrr.RechargeTypeCode.Value, GetTranslate(pcrr.RechargeTypeCode.Value, SiteLangId));
                row["RechargeType"] = listRechargeCode[pcrr.RechargeTypeCode.Value];
            }
         //   row["class"] = pcrr.IsTotalRow ? "TH_TD" : (_index % 2 == 0) ? "even" : "odd";
            row["IsTotalRow"] = pcrr.IsTotalRow.ToString();// +";" + _index++;
            row["AccountType"] = pcrr.AccountType;
            row["AccountStatus"] = pcrr.Status;
            row["PricePerCall"] = string.Format(number_format, pcrr.PricePerCall);
            row["Deposits"] = string.Format(number_format, pcrr.Deposits);
            row["South"] = pcrr.South;
            row["Shfela"] = pcrr.Shfela;
            row["Center"] = pcrr.Center;
            row["Sharon"] = pcrr.Sharon;
            row["Jerusalem"] = pcrr.Jerusalem;
            row["North"] = pcrr.North;
            data.Rows.Add(row);
        }
        dataV = data;
        IEnumerable<DataRow> query = (from x in data.AsEnumerable()
                                      select x).Take(20);

        _Repeater.DataSource = (query.Count() == 0) ? null : query.CopyToDataTable();
        _Repeater.DataBind();
    }
    protected void _Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.AlternatingItem && e.Item.ItemType != ListItemType.Item)
            return;
        string details = ((Label)e.Item.FindControl("lbl_IsTotalRow")).Text;
        HtmlTableRow _control = (HtmlTableRow)e.Item.FindControl("_tr");
     //   string[] _details = details.Split(';');
        if (true.ToString() == details)
            _control.Attributes.Add("class", "TH_TD");
        else
        {
         //   int _index = int.Parse(_details[1]);
            _control.Attributes.Add("class", "even");
        }
    }
    public bool SetDataByPage()
    {
        DataTable data = dataV;
        if (data == null)
            return false;
        PageIndex = PageIndex + 1;
        int _index = PageIndex;
        int _min = (_index * 20);
        IEnumerable<DataRow> data_row = (from x in data.AsEnumerable()
                                         select x).Skip(_min).Take(20);

        if (data_row.Count() == 0)
            return false;
        _Repeater.DataSource = data_row.CopyToDataTable();
        _Repeater.DataBind();
        return true;
    }
    public string CreateExcel()
    {
        ToExcel toexcel = new ToExcel(null, "Report");
       // DataTable data = GetDataTable.GetDataTableFromDataTableAndGridView(dataV, _GridView);
        return toexcel.CreateExcel(dataV);
    }
    string GetTranslate(WebReferenceReports.RechargeTypeCode rtc, int SiteLangId)
    {
        return EditTranslateDB.GetControlTranslate(SiteLangId, "RechargeTypeCode", rtc.ToString());
    }
    DataTable dataV
    {
        get { return (Session["PPAControlReport"] == null) ? null : (DataTable)Session["PPAControlReport"]; }
        set { Session["PPAControlReport"] = value; }
    }
    int PageIndex
    {
        get { return (Session["PageIndexPPAControlReport"] == null) ? 0 : (int)Session["PageIndexPPAControlReport"]; }
        set { Session["PageIndexPPAControlReport"] = value; }
    }
   
}