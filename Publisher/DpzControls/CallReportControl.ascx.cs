﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_DpzControls_CallReportControl : UserControlParent
{
    PageSetting _page;
    const string SCRAMBEL = "simsim";
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    public void BindReport(WebReferenceReports.DapazReportsCallContainer[] datas)
    {
        PageIndex = 0;
        dataV = null;
        if (datas == null)
            return;
        this.EnableViewState = false;
        _page = (PageSetting)Page;
        DataTable _data = new DataTable();
        _data.Columns.Add("num", typeof(int));
        _data.Columns.Add("CallDateTime");
        _data.Columns.Add("CallDurationInSeconds");
        _data.Columns.Add("CallerId");

        _data.Columns.Add("CallStatus");
        _data.Columns.Add("SupplierId");
        _data.Columns.Add("SupplierName");
        _data.Columns.Add("TargerNumber");
        _data.Columns.Add("VirtualNumber");
        _data.Columns.Add("Rating");
        _data.Columns.Add("BizId");
        _data.Columns.Add("HasRecord", typeof(bool));
        _data.Columns.Add("RecordFileLocation");
        int i = 1;
        foreach (WebReferenceReports.DapazReportsCallContainer data in datas)
        {
            
            DataRow row = _data.NewRow();
            row["num"] = i++;
            row["CallDateTime"] = string.Format(_page.siteSetting.DateFormat, data.CallDateTime) + " " + string.Format(_page.siteSetting.TimeFormat, data.CallDateTime);
            TimeSpan ts = new TimeSpan(0, 0, data.CallDurationInSeconds);
            row["CallDurationInSeconds"] = ts.ToString();
            row["CallerId"] = data.CallerId;
            row["CallStatus"] = data.CallStatus;
            row["SupplierId"] = data.SupplierId.ToString();
            row["SupplierName"] = data.SupplierName;
            row["TargerNumber"] = data.TargerNumber;
            row["VirtualNumber"] = data.VirtualNumber;
            row["BizId"] = data.BizId;
            row["HasRecord"] = !string.IsNullOrEmpty(data.RecordFileLocation);
            string EncodePath = EncryptString.Encrypt_String(data.RecordFileLocation, SCRAMBEL);
            row["RecordFileLocation"] = EncodePath;
            _data.Rows.Add(row);
        }
        dataV = _data;
        IEnumerable<DataRow> query = (from x in _data.AsEnumerable()
                                      select x).Take(20);

        _GridView.DataSource = (query.Count() == 0) ? null : query.CopyToDataTable();
        _GridView.DataBind();
       
    }
    public bool SetDataByPage()
    {
        DataTable data = dataV;
        if (data == null)
            return false;
        PageIndex = PageIndex + 1;
        int _index = PageIndex;
        int _min = (_index * 20);
        IEnumerable<DataRow> data_row = (from x in data.AsEnumerable()
                                         select x).Skip(_min).Take(20);

        if (data_row.Count() == 0)
            return false;
        _GridView.DataSource = data_row.CopyToDataTable();
        _GridView.DataBind();
        return true;
    }
    public string CreateExcel()
    {
        ToExcel toexcel = new ToExcel(null, "Report");
        DataTable data = GetDataTable.GetDataTableFromDataTableAndGridView(dataV, _GridView);
        return toexcel.CreateExcel(data);
    }
     DataTable dataV
    {
        get { return (Session["CallReport"] == null) ? null : (DataTable)Session["CallReport"]; }
        set { Session["CallReport"] = value; }
    }
     int PageIndex
    {
        get { return (Session["PageIndexCallReport"] == null) ? 0 : (int)Session["PageIndexCallReport"]; }
        set { Session["PageIndexCallReport"] = value; }
    }
    protected string GetCallReport
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetCallReport"); }
    }
    protected string GetRecord
    {
        get { return ResolveUrl("~/Publisher/DpzControls/ZapRecord.aspx"); }
    }
}