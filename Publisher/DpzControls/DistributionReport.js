﻿function ReciveServerData(retValue) {

    $(window).scroll(_scroll);
    if (retValue.length == 0)
        return;
    var _trs = $(retValue).get(0).getElementsByTagName('tr');
    var _length = _trs.length;
    for (var i = 1; i < _length; i++) {
        if(_trs[1].className.length == 0)
            _trs[1].className = ((i % 2 == 1) ? "_even" : "_odd");
        $('.-GridView').append(_trs[1]);
    }

}

$(window).scroll(_scroll);
function _scroll() {
    if ($(window).scrollTop() > 0.9 * ($(document).height() - $(window).height())) {
        $(window).unbind('scroll');        
        _CallServer();
    }
}

function _CallServer() {
    var _url = document.getElementById('hf_GetReport').value;
    $(window).unbind('scroll');
    $.ajax({
        url: _url,
        data: null,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            if (data.d.length == 0 || data.d == "done") {
                return;
            }
            ReciveServerData(data.d);

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //              alert(textStatus);
        },
        complete: function (jqXHR, textStatus) {
            //  HasInHeadingRequest = false;

        }

    });
}
function Get_Record(a_elem) {
    var record_path = $(a_elem).find("input[type='hidden']").val();
    var _url = document.getElementById('hf_GetRecord').value;
    _url = _url + "?record=" + encodeURIComponent(record_path);
    window.open(_url, 'record', 'height=300,width=300');
}
