﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_DpzControls_SourceCallReport : UserControlParent
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void BindReport(WebReferenceReports.DapazGroupByCallerIdContainer[] result)
    {
        PageIndex = 0;
        dataV = null;
        if (result == null)
            return;
        this.EnableViewState = false;
        DataTable data = new DataTable();
        data.Columns.Add("num", typeof(int));
        data.Columns.Add("CallerId");
        data.Columns.Add("Count");
        int i = 1;
        foreach (WebReferenceReports.DapazGroupByCallerIdContainer container in result)
        {
            DataRow row = data.NewRow();
            row["num"] = i++;
            row["CallerId"] = container.CallerId;
            row["Count"] = container.Count;
            data.Rows.Add(row);
        }
        dataV = data;
        IEnumerable<DataRow> query = (from x in data.AsEnumerable()
                                      select x).Take(20);
        _GridView.DataSource = (query.Count() == 0) ? null : query.CopyToDataTable();
        _GridView.DataBind();

    }
    public bool SetDataByPage()
    {
        DataTable data = dataV;
        if (data == null)
            return false;
        PageIndex = PageIndex + 1;
        int _index = PageIndex;
        int _min = (_index * 20);
        IEnumerable<DataRow> data_row = (from x in data.AsEnumerable()
                                         select x).Skip(_min).Take(20);

        if (data_row.Count() == 0)
            return false;
        _GridView.DataSource = data_row.CopyToDataTable();
        _GridView.DataBind();
        return true;
    }
    public string CreateExcel()
    {
        ToExcel toexcel = new ToExcel(null, "Report");
        DataTable data = GetDataTable.GetDataTableFromDataTableAndGridView(dataV, _GridView);
        return toexcel.CreateExcel(data);
    }
    
    DataTable dataV
    {
        get { return (Session["SourceCallReport"] == null) ? null : (DataTable)Session["SourceCallReport"]; }
        set { Session["SourceCallReport"] = value; }
    }
    int PageIndex
    {
        get { return (Session["PageIndexSourceCallReport"] == null) ? 0 : (int)Session["PageIndexSourceCallReport"]; }
        set { Session["PageIndexSourceCallReport"] = value; }
    }
    protected string GetSourceCallReport
    {
        get { return ResolveUrl("~/Management/DapazService.asmx/GetSourceCallReport"); }
    }
}