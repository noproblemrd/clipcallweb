﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PPAControlReportControl.ascx.cs" Inherits="Publisher_DpzControls_PPAControlReportControl" EnableViewState="false" %>

<asp:Repeater ID="_Repeater" runat="server" 
    onitemdatabound="_Repeater_ItemDataBound" >
<HeaderTemplate>
    <table class="data-table -GridView">
    
    <tr class="TH_TD">
        <td>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_BizId.Text %>"></asp:Label> 
        </td>
        <td>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Name.Text %>"></asp:Label> 
        </td>
        <td>
            <asp:Label ID="Label5" runat="server" Text="<%# lbl_SupplierType.Text %>"></asp:Label> 
        </td>
        <td>
            <asp:Label ID="Label9" runat="server" Text="<%# lbl_RechargeType.Text %>"></asp:Label> 
        </td>
        <td>
            <asp:Label ID="Label11" runat="server" Text="<%# lbl_RechargeAmount.Text %>"></asp:Label> 
        </td>
        <td>
            <asp:Label ID="Label13" runat="server" Text="<%# lbl_Balance.Text %>"></asp:Label> 
        </td>
        <td>
            <asp:Label ID="Label15" runat="server" Text="<%# lbl_Heading.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label17" runat="server" Text="<%# lbl_Calls.Text %>"></asp:Label> 
        </td>
         <td>
              <asp:Label ID="Label19" runat="server" Text="<%# lbl_Charged.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label21" runat="server" Text="<%# lbl_Revenue.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label7" runat="server" Text="<%# lbl_AccountType.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label8" runat="server" Text="<%# lbl_AccountStatus.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label23" runat="server" Text="<%# lbl_PricePerCall.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label24" runat="server" Text="<%# lbl_Deposits.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label25" runat="server" Text="<%# lbl_South.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label26" runat="server" Text="<%# lbl_Shfela.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label27" runat="server" Text="<%# lbl_Center.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label28" runat="server" Text="<%# lbl_Sharon.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label29" runat="server" Text="<%# lbl_Jerusalem.Text %>"></asp:Label> 
        </td>
        <td>
             <asp:Label ID="Label30" runat="server" Text="<%# lbl_North.Text %>"></asp:Label> 
        </td>
    </tr>
   
</HeaderTemplate>
<ItemTemplate>
    <tr runat="server" id="_tr">
        <td>
            <asp:Label ID="Label2" runat="server" Text="<%# Bind('BizId') %>"></asp:Label>
            <asp:Label ID="lbl_IsTotalRow" runat="server" Text="<%# Bind('IsTotalRow') %>" Visible="false"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label4" runat="server" Text="<%# Bind('Name') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('SupplierType') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label10" runat="server" Text="<%# Bind('RechargeType') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label12" runat="server" Text="<%# Bind('RechargeAmount') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label14" runat="server" Text="<%# Bind('Balance') %>"></asp:Label>
        </td>
        <td>
             <asp:Label ID="Label16" runat="server" Text="<%# Bind('Heading') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label18" runat="server" Text="<%# Bind('Calls') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label20" runat="server" Text="<%# Bind('Charged') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label22" runat="server" Text="<%# Bind('Revenue') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label31" runat="server" Text="<%# Bind('AccountType') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label32" runat="server" Text="<%# Bind('AccountStatus') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label33" runat="server" Text="<%# Bind('PricePerCall') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label34" runat="server" Text="<%# Bind('Deposits') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label35" runat="server" Text="<%# Bind('South') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label36" runat="server" Text="<%# Bind('Shfela') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label37" runat="server" Text="<%# Bind('Center') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label38" runat="server" Text="<%# Bind('Sharon') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label39" runat="server" Text="<%# Bind('Jerusalem') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label40" runat="server" Text="<%# Bind('North') %>"></asp:Label>
        </td>
    
    </tr>
</ItemTemplate>
<FooterTemplate>
    
    </table>
</FooterTemplate>
</asp:Repeater>


<asp:Label ID="lbl_BizId" runat="server" Text="BizId" Visible="false"></asp:Label>
<asp:Label ID="lbl_Name" runat="server" Text="Supplier name" Visible="false"></asp:Label>
<asp:Label ID="lbl_SupplierType" runat="server" Text="Supplier type" Visible="false"></asp:Label>
<asp:Label ID="lbl_RechargeType" runat="server" Text="Recharge type" Visible="false"></asp:Label>
<asp:Label ID="lbl_RechargeAmount" runat="server" Text="Recharge amount" Visible="false"></asp:Label>
<asp:Label ID="lbl_Balance" runat="server" Text="Balance" Visible="false"></asp:Label>
<asp:Label ID="lbl_Heading" runat="server" Text="Heading" Visible="false"></asp:Label>
<asp:Label ID="lbl_Calls" runat="server" Text="Calls" Visible="false"></asp:Label>
<asp:Label ID="lbl_Charged" runat="server" Text="Charged" Visible="false"></asp:Label>
<asp:Label ID="lbl_Revenue" runat="server" Text="Revenue" Visible="false"></asp:Label>

<asp:Label ID="lbl_NewPPA" runat="server" Text="New PPA" Visible="false"></asp:Label>
<asp:Label ID="lbl_OldPPA" runat="server" Text="Old PPA" Visible="false"></asp:Label>

<asp:Label ID="lbl_AccountType" runat="server" Text="Account Type" Visible="false"></asp:Label>
<asp:Label ID="lbl_AccountStatus" runat="server" Text="Account Status" Visible="false"></asp:Label>
<asp:Label ID="lbl_PricePerCall" runat="server" Text="Price Per Call" Visible="false"></asp:Label>
<asp:Label ID="lbl_Deposits" runat="server" Text="Deposits" Visible="false"></asp:Label>
<asp:Label ID="lbl_South" runat="server" Text="South" Visible="false"></asp:Label>
<asp:Label ID="lbl_Shfela" runat="server" Text="Shfela" Visible="false"></asp:Label>
<asp:Label ID="lbl_Center" runat="server" Text="Center" Visible="false"></asp:Label>
<asp:Label ID="lbl_Sharon" runat="server" Text="Sharon" Visible="false"></asp:Label>
<asp:Label ID="lbl_Jerusalem" runat="server" Text="Jerusalem" Visible="false"></asp:Label>
<asp:Label ID="lbl_North" runat="server" Text="North" Visible="false"></asp:Label>