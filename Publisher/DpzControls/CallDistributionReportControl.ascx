﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallDistributionReportControl.ascx.cs" Inherits="Publisher_DpzControls_CallDistributionReportControl" %>
<div runat="server" id="div_script">

</div>
<asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" CssClass="data-table -GridView">
<AlternatingRowStyle CssClass="odd" />
<Columns>
    <asp:TemplateField SortExpression="num">
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_num.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="lbl_LineNumber" runat="server" Text="<%# Bind('num') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Time">
        <HeaderTemplate>
            <asp:Label ID="Label2" runat="server" Text="<%# lbl_Time.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('Time') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="NumberOfCallers">
        <HeaderTemplate>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_NumberOfCallers.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label7" runat="server" Text="<%# Bind('NumberOfCallers') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="CallsTimeInSeconds">
        <HeaderTemplate>
            <asp:Label ID="Label4" runat="server" Text="<%# lbl_CallsTimeInSeconds.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label8" runat="server" Text="<%# Bind('CallsTimeInSeconds') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="AverageDurationInSeconds">
        <HeaderTemplate>
            <asp:Label ID="Label5" runat="server" Text="<%# lbl_AverageDurationInSeconds.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label9" runat="server" Text="<%# Bind('AverageDurationInSeconds') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
</Columns>
</asp:GridView>

<asp:Label ID="lbl_num" runat="server" Text="Num" Visible="false"></asp:Label>
<asp:Label ID="lbl_NumberOfCallers" runat="server" Text="Number Of Callers" Visible="false"></asp:Label>
<asp:Label ID="lbl_Time" runat="server" Text="Time" Visible="false"></asp:Label>
<asp:Label ID="lbl_CallsTimeInSeconds" runat="server" Text="Call Duration" Visible="false"></asp:Label>
<asp:Label ID="lbl_AverageDurationInSeconds" runat="server" Text="Average Call Duration" Visible="false"></asp:Label>

<asp:Label ID="lbl_Minutes" runat="server" Text="Minutes" Visible="false"></asp:Label>
<asp:Label ID="lbl_Calls" runat="server" Text="Calls" Visible="false"></asp:Label>

<input id="hf_GetReport" type="hidden" value="<%# GetDistributionReport %>" />