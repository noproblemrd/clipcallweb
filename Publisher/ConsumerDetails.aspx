<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConsumerDetails.aspx.cs" 
Inherits="Publisher_ConsumerDetails" EnableEventValidation="false"%>


    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title>Consumer details</title>
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
<link href="../Management/styleheader.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../general.js"></script>
<link href="style.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    var oWin;
   function CloseIframe(_mes)
   {        
        alert(_mes);
        top.$.colorbox._close();
   }
   //call to web service for get the record
   function OpenRecord(_id)
   {
        //alert(_id);
        RecordService.GetPath(_id, OnComplete, OnErrorEx, OnTimeOutEx);
   }
   function OnComplete(arg)
   {
        if(arg.length==0)
        {
            alert(document.getElementById("<%# hf_errorRecord.ClientID %>").value);
            return;
        }
 
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        if(is_chrome)
        {
            oWin = window.open('<%# GetAudioChrome %>?audio='+ arg, "recordWin", "resizable=0,width=500,height=330");     
            setTimeout("CheckPopupBlocker()", 2000);
            /*
            oWin = window.open(arg, "recordWin");     
            setTimeout("CheckPopupBlocker()", 2000)
            */
        }
        else
            window.location.href=arg;
   
   }
   function CheckPopupBlocker()
   {
         if(_hasPopupBlocker(oWin))
                alert(document.getElementById('<%#Hidden_AllowPopUp.ClientID  %>').value);
   }
   function _hasPopupBlocker(poppedWindow) {   
        if (poppedWindow==null || typeof poppedWindow =="undefined" || !poppedWindow || poppedWindow.closed || poppedWindow.innerHeight==0) 
            return true;
        return false;

    }
   function OnErrorEx(arg)
   {
        alert(arg);
   }
   function OnTimeOutEx(arg)
   {
        alert(arg);
   }
   //////
   
   function ShowInsert()
    {
        var elm = document.getElementById('div_show');
        if(elm.style.display=='none')
        {
            elm.style.display='inline';
            var elmlink=document.getElementById('<%# lblShowInsert.ClientID %>');
            
            elmlink.innerHTML=document.getElementById('<%# Hidden_lblHide.ClientID %>').value;
            
        }
        else
        {
            HideInsert();
        }
    }
    function HideInsert()
    {
        var elm = document.getElementById('div_show');
        elm.style.display='none';
        var elmlink=document.getElementById('<%# lblShowInsert.ClientID %>');
        elmlink.innerHTML=document.getElementById('<%# Hidden_lblShow.ClientID %>').value;
        document.getElementById("<%# txtInsertComment.ClientID %>").value = "";
    }
    function CheckValidComment()
    {
        if(!Page_ClientValidate('note'))
            return false;
        showDiv();
        return true;
    }
    function chk_valid_update()
    {
        if(!Page_ClientValidate('detail'))
            return false;
        showDiv();
        return true;
    }
    function tab_click(num)
    {
  //      alert(num);
        showDiv();
        
         /*
        if(typeof Page_Validators  != "undefined")
        { 
            var validators = Page_Validators;
            for (var i = 0;i < validators.length; i++)
            {  
                alert(validators[i]);          
                ValidatorEnable(validators[i], false);            
            }
        }
        */
        try{ValidatorOnSubmit();}
        catch(ex){}
  
        if(num=='1')
            <%# Get_tab_click(1)%>;
        else if(num=='2')
            <%# Get_tab_click(2)%>;       
        else if(num=='3')
            <%# Get_tab_click(3)%>;
        else if(num=='4')
            <%# Get_tab_click(4)%>;
        else
            hideDiv();
   
    }
    function chkOneBox(control)
  {
  /*
        if(GetValidatorOfControl(control))        
            control.className="txt_focus";        
        else    
            control.className="form-text";
 */       
  }
  function GetValidatorOfControl(control)
  {
    //    var IsInvalid=false;
        var validators = Page_Validators;  
        for (var i = 0;i < validators.length; i++)
        {
            if(control==document.getElementById(validators[i].controltovalidate))
            {
                if(!validators[i].isvalid)
                    return true;
               
            }
        }
        return false;
  }
    function pageLoad(sender, args)
    {        
        
        hideDiv();
    }
    
    function CloseIframe_after()
    {
        CloseIframe();
 //       <# Get_run_report() >;
    }
    function CloseIframe()
    {
       $find('_modal').hide();
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie popModal_del2_hide";
       document.getElementById("<%# _iframe.ClientID %>").src = "javascript:void(0);";
    }
    function OpenIframe(_path)
    {    
       document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie";
       document.getElementById("<%# _iframe.ClientID %>").src = _path;
       HideIframeDiv();  
       $find('_modal').show();
    }
    
    function ShowIframeDiv()
    {
        document.getElementById("divIframeLoader").style.display = "none";
       document.getElementById("div_iframe_main").style.display = "block";
    }
    function HideIframeDiv()
    {
        document.getElementById("divIframeLoader").style.display = "block";
       document.getElementById("div_iframe_main").style.display = "none";
    }
    function OpenAdvertiserIframeParent(_path)
    {
        
        parent.OpenAdvertiserIframe(_path);
    }
   </script>
</head>
<body class="consumers" style="background:transparent;">
<form id="form1" runat="server" clientidmode="AutoID">
<div>
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
        <Services >
        <asp:ServiceReference Path="../RecordService.asmx" />
            
        </Services>
    </cc1:ToolkitScriptManager>
    <div class="content" id="publisher_content" >
        <div class="iframe-contentconsumer" >
            <table cellspacing="0">
                <tr>
                    <td colspan="100%"></td>
				</tr>
				<tr>
					
					<td class="consumer-content">
					    <h2><asp:Label ID="lbl_Consumer" runat="server" Text="consumer"></asp:Label>: <asp:Label ID="lbl_consume_name" runat="server" ></asp:Label></h2>
                       <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">    
                        <ContentTemplate>
                        <asp:MultiView id="_MultiView" ActiveViewIndex="0" Runat="server">
                        
                        <asp:View ID="View1" runat="server">
                        <%//  General info %>
                        <div id="_main" class="tofes1_main">    	
                            <p><b><asp:Label ID="lbl_consumerDetails" runat="server" Text="Click this tab to manage your Consumer General Info"></asp:Label></b></p>
	                        <asp:Panel ID="_PanelFName" runat="server">   
                                <p>	        
                                    <asp:Label ID="lbl_Fname" runat="server" CssClass="form-label" Text="first name"></asp:Label>
		                            <asp:TextBox runat="server" ID="txt_Fname" CssClass="form-text"  TabIndex="0" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Fname" Display="Dynamic"
                                     ForeColor="red" CssClass="error" ID="RequiredFieldValidatorFName" ValidationGroup="detail" 
                                     ErrorMessage="*" >Missing</asp:RequiredFieldValidator>
	                            </p>
                            </asp:Panel>
        
                            <asp:Panel ID="_PanelLName" runat="server">   
                                <p>	        
                                    <asp:Label ID="lbl_Lname" runat="server" CssClass="form-label" Text="Last name"></asp:Label>
		                            <asp:TextBox runat="server" ID="txt_Lname"  CssClass="form-text"  TabIndex="0" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorLname"  ControlToValidate="txt_Lname" 
        		                    runat="server" ErrorMessage="*" Display="Dynamic" ForeColor="red" ValidationGroup="detail" ValidationExpression="^\D+$">Invalid</asp:RegularExpressionValidator>
	                            </p>
                            </asp:Panel>

                            <asp:Panel ID="_PanelPhone1" runat="server">   
                                <p>	        
                                    <asp:Label ID="lbl_phone1" runat="server" CssClass="form-label" Text="Main phone"></asp:Label>
		                            <asp:TextBox runat="server" ID="txt_MobilePhone" CssClass="form-text"  TabIndex="10" autocomplete="off"></asp:TextBox>            
		                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_MobilePhone" Display="Dynamic" 
		                            ForeColor="red" CssClass="error" ID="RequiredFieldValidatorMobilePhone" ValidationGroup="detail" ErrorMessage="*" >Missing</asp:RequiredFieldValidator>
        		                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobilePhone"  ControlToValidate="txt_MobilePhone" 
        		                    runat="server" ErrorMessage="*" Display="Dynamic" ForeColor="red" ValidationGroup="detail" ValidationExpression="\d+">Invalid</asp:RegularExpressionValidator>
                                </p>
                            </asp:Panel>
                            
                            <asp:Panel ID="_PanelPhone2" runat="server">
                                <p>	        
                                    <asp:Label ID="lbl_phone2" runat="server" CssClass="form-label" Text="Phone number 2"></asp:Label>
		                            <asp:TextBox runat="server" ID="txt_phone2"  CssClass="form-text"  TabIndex="1" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>                                  
        		                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone2"  ControlToValidate="txt_phone2" runat="server" ErrorMessage="*" Display="Dynamic" ForeColor="red" ValidationGroup="detail" ValidationExpression="\d+">Invalid</asp:RegularExpressionValidator>
	                            </p>
                            </asp:Panel>

                            <asp:Panel ID="_PanelEmail" runat="server">   
                                <p>	        
                                    <asp:Label ID="lbl_email" runat="server" CssClass="form-label" Text="Email"></asp:Label>
		                            <asp:TextBox runat="server" ID="txt_email" CssClass="form-text" TabIndex="2" onblur="javascript:chkOneBox(this);" autocomplete="off"></asp:TextBox>
                                   
        		                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" ControlToValidate="txt_email" runat="server" ErrorMessage="*" Display="Dynamic" ForeColor="red" ValidationGroup="detail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" >Invalid</asp:RegularExpressionValidator>
	                            </p>
                            </asp:Panel>

                            <asp:Panel ID="_PanelNumRequest" runat="server">   
                                <p>	        
                                    <asp:Label ID="lbl_numRequest" runat="server" CssClass="form-label" Text="Number of request"></asp:Label>
		                            <asp:TextBox runat="server" ID="txt_numRequest"  CssClass="form-text"  TabIndex="4" autocomplete="off" 
		                            ReadOnly="true" onfocus="javascript:this.blur();"></asp:TextBox>          
                                </p>
                            </asp:Panel>
                            
                            <asp:Panel ID="_PanelBlackList" runat="server">   
                                <p>	        
                                    <asp:Label ID="lbl_blackList" runat="server" CssClass="form-label" Text="Mark the consumer as blacklist"></asp:Label>
                                    <asp:CheckBox ID="cb_BlackList" runat="server" />      
                                </p>
                            </asp:Panel>

                            <div>
                                <asp:Button runat="server"  CssClass="form-submit" ID="btn_update" OnClientClick="return chk_valid_update();"
                                OnClick="btn_update_OnClick"  Text="Update" ValidationGroup="detail"  TabIndex="3" />        
                            </div>
                        </div>        
                        <%// End General info %>
                    </asp:View>        
                    
                    <asp:View ID="View2" runat="server">
                    <%//  Request %>
            	    <asp:Repeater runat="server" ID="_reapeter">
                        <HeaderTemplate>
                            <table class="data-table" style="width:460px !important;">
                                <tr>
                                    <th style="width: 30px;">&nbsp;</th>
                                    <th><asp:Label ID="Label50" runat="server" Text="<%#lbl_RequestNumber.Text %>"></asp:Label></th>
			                        <th><asp:Label ID="Label51" runat="server" Text="<%#lbl_Date.Text %>"></asp:Label></th>
			                        <th><asp:Label ID="Label52" runat="server" Text="<%#lbl_Expertise.Text %>"></asp:Label></th>
			                        <th><asp:Label ID="Label53" runat="server" Text="<%#lbl_region.Text %>"></asp:Label></th>
			                        <th><asp:Label ID="Label1" runat="server" Text="<%#lbl_Advertisers.Text %>"></asp:Label></th>
			                    </tr>
			                </HeaderTemplate>
	                        <ItemTemplate>
                                <tr class="odd">
                                    <td class="close first" id="td_openClose" runat="server">
                                        <asp:LinkButton ID="ib_open" runat="server" CommandArgument="<%# Bind('IncidentId') %>" 
                                         CssClass="arrow" OnClick="ib_open_Click" OnClientClick="showDiv();"></asp:LinkButton>
                                        <asp:HiddenField ID="hf_IfClose" runat="server" Value="false" />
                                        
                                    </td>
			                         <td>
			                            <asp:Label ID="lbl_name" runat="server" Text="<%# Bind('IncidentNumber') %>"></asp:Label>      
                                        <asp:Label ID="lbl_IncidentId" runat="server" Text="<%# Bind('IncidentId') %>" Visible="false"></asp:Label>
			                         </td>
			                         <td>
			                            <asp:Label ID="Label2" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>      
			                         </td>
			                         <td>
			                            <asp:Label ID="Label3" runat="server" Text="<%# Bind('ExpertiseName') %>"></asp:Label>
			                         </td>
			                         <td>
                                       <asp:Label ID="Label4" runat="server" Text="<%# Bind('RegionName') %>"></asp:Label>
			                         </td>
			                         <td>
                                       <asp:Label ID="Label7" runat="server" Text="<%# Bind('NumberOfSuppliersContacted') %>"></asp:Label>
			                         </td>
			                    </tr>
			                    <tr>
			                        <td colspan="6" style="display:none;" id="tr_OpenDetails" runat="server">
			                        <div >
							       	    <div >
							       	        
							       	        <div class="description_request">
						                     <asp:Label ID="Label11" runat="server" CssClass="label" Text="<%# lbl_description.Text %>"></asp:Label>
						                     <div class="wrap">
						             	        <asp:Label ID="Label81" runat="server" Text="<%# Bind('Description') %>"></asp:Label>
						                     </div>
						                    </div>			                       
                                        
			                            
			                           </div>								             
							           </div> 
			                            
                                        <asp:GridView ID="_GridViewIn" runat="server" AutoGenerateColumns="false" 
                                        AllowPaging="true" CssClass="table-incindent" Width="460px">
                                        
                                            <Columns>
                                                <asp:TemplateField SortExpression="SupplierName">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label101" runat="server" Text="<%# lbl_AdvertiserName.Text %>"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <div class="details_request">
                                                            <asp:Label ID="lblBadWord" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                       

                                                <asp:TemplateField SortExpression="Records">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label105" runat="server" Text="<%# lbl_Recording.Text %>"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate> 
                                                       
                                                            <a id="a_recording"  runat="server" href="<%# Bind('RecordFileLocation') %>" visible="<%# Bind('HasRecord') %>" >
                                                            <asp:Image ID="img_recording" runat="server" ImageUrl="<%# Bind('ImageUrl') %>" /></a>  
                                                      
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField SortExpression="SupplierName">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label131" runat="server" Text="<%# lbl_Complaint.Text %>"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                       
                                                            <a runat="server" id="a_Complaint" href="<%# Bind('Complaint') %>">
                                                                <asp:Image ID="Image1" runat="server" ImageUrl="images/icon-toolbox-add.png" />
                                                            </a>
                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>    
                                            </Columns>
                                        </asp:GridView>
                                       
			                        </td>
			                    </tr>
			                </ItemTemplate>
                            <FooterTemplate>
                            </table>                
                            </FooterTemplate>            
                        </asp:Repeater>   
                        <div id="div_pagingRecord"  CssClass="pager" runat="server" visible="false" >
                        <center>
                            <table>
                                <tr>
                                    <td class="td_Pages"><asp:LinkButton ID="lnkPreviousPageRecord" runat="server" 
                                     OnClick="lnkPreviousPageRecord_Click"
                                     >Previous</asp:LinkButton></td>
                                    <asp:PlaceHolder ID="PlaceHolderPagesRecord" runat="server"></asp:PlaceHolder>
                                    <td class="td_Pages"><asp:LinkButton ID="lnkNextPageRecord" runat="server"
                                     OnClick="lnkNextPageRecord_Click" >Next</asp:LinkButton></td>
                                </tr>
                            </table>
                        </center>
                        </div>
          
                        <%//  End Request %>
                    </asp:View>        
                    <asp:View ID="View3" runat="server">
                    <%// Complaints %>
                        <asp:GridView ID="_GridView_complaints" runat="server" AutoGenerateColumns="false"
                            CssClass="data-table" AllowPaging="True" 
                            onpageindexchanging="_GridView_complaints_PageIndexChanging" PageSize="20">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />        
                        <FooterStyle CssClass="footer"  />
                        <PagerStyle CssClass="pager" />
                        <Columns>
                            
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label903" runat="server" Text="<%# lbl_ID.Text %>"></asp:Label>                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                
                               <asp:LinkButton ID="lb_ID" runat="server" Text="<%# Bind('ID') %>" OnClientClick="<%# Bind('ScriptComplaint') %>"></asp:LinkButton>        
                                <asp:Label ID="lbl_guid" runat="server" Text="<%# Bind('guid') %>" Visible="false"></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label904" runat="server" Text="<%# lbl_CreatedOn.Text %>"></asp:Label>                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label94" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>            
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label905" runat="server" Width="200" Text="<%# lbl_Title.Text %>"></asp:Label>                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label95" style="display:block;" runat="server" Width="100" Text="<%# Bind('Title') %>"></asp:Label>            
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label906" runat="server" Width="100" Text="<%# lbl_Advertiser.Text %>"></asp:Label>                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton901" Width="100" runat="server" Text="<%# Bind('Advertiser') %>"
                                 OnClientClick="<%# Bind('Advertiser_script') %>"></asp:LinkButton>
                                
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label907" runat="server" Text="<%# lbl_Status.Text %>"></asp:Label>                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label97" runat="server" Text="<%# Bind('Status') %>"></asp:Label>            
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label908" runat="server" Text="<%# lbl_FollowUp.Text %>"></asp:Label>                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label98" runat="server" Text="<%# Bind('FollowUp') %>"></asp:Label>            
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        </asp:GridView>
                    <%//  End Complaints %>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                    <%//  Notes %>
                        <table cellspacing="0" class="main">
                            <tr>
                                <td class="regular">
                                    <a href="#" onclick="ShowInsert();" ><asp:Label ID="lblShowInsert" runat="server" Text="<%= Hidden_lblShow.Value %>" Font-Bold="True" ></asp:Label></a>
                                </td>
                            </tr>
                            <tr id="InsertComment">
                                <td class="regular">
                                    <div id="div_show">
                                        <asp:Label ID="lblInsertComment" runat="server" Text="Insert Comment"></asp:Label>:
                                        <br />
                                        <asp:TextBox ID="txtInsertComment" runat="server"  TextMode="MultiLine" Rows="3" Width="90%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorComment" runat="server" ErrorMessage="Missing" ControlToValidate="txtInsertComment" ValidationGroup="note">*</asp:RequiredFieldValidator>
                                        <br />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="note" />
                                        <br />
                                        <asp:Button ID="btnInsert" runat="server" CssClass="form-submit" Text="Insert" OnClick="btnInsert_Click" ValidationGroup="note" OnClientClick="return CheckValidComment();" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:DataList ID="dlComments" runat="server" Width="100%" GridLines="Horizontal">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDateTime" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
                                        <asp:Label ID="lblCreateBy" runat="server" Text="<%# lbl_CreateBy.Text %>"></asp:Label>
                                        <asp:Label ID="lblBy" runat="server" Text="<%# Bind('UserName') %>"></asp:Label>
                                        <br />                
                                        <asp:Label ID="lblComment" runat="server" Text="<%# Bind('Description') %>"></asp:Label>           
                                    </ItemTemplate>
                                </asp:DataList>
                                <div id="div_paging" runat="server" visible="false">
                                    <table>
                                        <tr>
                                            <td>
                                            <asp:LinkButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click"
                                            OnClientClick="showDiv();">Previous</asp:LinkButton>
                                            </td>
                                            <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
                                            <td>
                                            <asp:LinkButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click"
                                            OnClientClick="showDiv();">Next</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                               </div>
                            </td>
                        </tr>
                    </table>        
                    <%// End Notes %>
                </asp:View>                
                </asp:MultiView>
	            </ContentTemplate>
                </asp:UpdatePanel>			
				</td>
				
			</tr>
			
		</table>
	</div>
        <asp:UpdatePanel ID="_UpdatePanelNavigat" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
       
	
	<div class="tab-navigationconsumer">					    
	    <ul id="ulSteps">
			<li id="step1" runat="server" >
			    <a id="lb_ConsumerGeneralInfo" runat="server" href="javascript:tab_click('1');">
			        <span>1</span>
                    <asp:Literal ID="lbl_ConsumerGeneralInfo" runat="server" Text="General Info"></asp:Literal>
			    </a>
			   
			</li>				
			<li id="step2" runat="server">
			    <a id="lb_Requests" runat="server" href="javascript:tab_click('2');">
			        <span>2</span>
                    <asp:Literal ID="lbl_Request" runat="server" Text="Requests"></asp:Literal>
			    </a>
			   
			</li> 
			<li id="step3" runat="server" >
                <a id="lb_Complaints" runat="server" href="javascript:tab_click('3');" >
			        <span>3</span>
                    <asp:Literal ID="lbl_Complaints" runat="server" Text="Complaints"></asp:Literal>
			    </a>
			    
            </li>	   
            <li id="step4" runat="server" class="last">
                <a id="lb_Notes" runat="server" href="javascript:tab_click('4');" >
			        <span>4</span>
                    <asp:Literal ID="lbl_notes" runat="server" Text="Notes"></asp:Literal>
			    </a>
			    
            </li>
            						
		</ul>					    
	</div>
	 </ContentTemplate>
        </asp:UpdatePanel>
</div>
   
</div>
<div id="divLoader" class="_Background" style="text-align:center;display:none;">   
    <table width="100%" height="100%">
        <tr>
            <td height="100%" style="vertical-align:middle;">
                <img src="../UpdateProgress/ajax-loader.gif" style="text-align:center;vertical-align:middle;" alt=""/>
            </td>
        </tr>
    </table>
</div>   

<asp:Panel ID="panel_iframe" runat="server" CssClass="popModal_del2_ie popModal_del2_hide" style="display:none;"
 Width="750" Height="440">
 <div id="divIframeLoader" class="divIframeLoader">   
    <table>
        <tr>
            <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
        </tr>
    </table>
</div>
<div id="div_iframe_main">
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A" href="javascript:void(0);" onclick="javascript:CloseIframe();"></a>
    <iframe runat="server" id="_iframe" width="750px" height="440px" frameborder="0"  ALLOWTRANSPARENCY="true" ></iframe>
    <div class="bottom2"></div>
</div>
</asp:Panel>
<cc1:ModalPopupExtender ID="_mpe" runat="server"
            TargetControlID="btn_virtual"
            PopupControlID="panel_iframe"
             BackgroundCssClass="modalBackground" 
             
              BehaviorID="_modal"               
              DropShadow="false">
</cc1:ModalPopupExtender>

                
<div id="div_LabelRequest">
    <asp:Label ID="lbl_RequestNumber" runat="server" Text="Request ID" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Expertise" runat="server" Text="Heading" Visible="false"></asp:Label>
    <asp:Label ID="lbl_region" runat="server" Text="Region" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Advertisers" runat="server" Text="Advertisers" Visible="false"></asp:Label>
    <asp:Label ID="lbl_description" runat="server" Text="Description" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Complaint" runat="server" Text="Complaint" Visible="false"></asp:Label>
    
</div>

<div id="div_incidentDetails">
    <asp:Label ID="lbl_AdvertiserName" runat="server" Text="Advertiser" Visible="false"></asp:Label>       
    <asp:Label ID="lbl_Recording" runat="server" Text="Recording" Visible="false"></asp:Label>   
</div>
<%// Complaints labels %>
<asp:Label ID="lbl_ID" runat="server" Text="ID" Visible="false"></asp:Label>
<asp:Label ID="lbl_CreatedOn" runat="server" Text="Created on" Visible="false"></asp:Label>
<asp:Label ID="lbl_Title" runat="server" Text="Title" Visible="false"></asp:Label>
<asp:Label ID="lbl_Status" runat="server" Text="Status" Visible="false"></asp:Label>
<asp:Label ID="lbl_Advertiser" runat="server" Text="Advertiser" Visible="false"></asp:Label>
<asp:Label ID="lbl_FollowUp" runat="server" Text="Follow up" Visible="false"></asp:Label>
<%// End Complaints labels %>

<asp:HiddenField ID="hf_errorId" runat="server" Value="Sorry, An error occurred , the window will be closed." />
<asp:HiddenField ID="hf_errorRecord" runat="server" Value="There are errors with the record" />
<asp:Label ID="lbl_CreateBy" runat="server" Text="By:" Visible="false"></asp:Label>

<asp:HiddenField ID="Hidden_lblShow" runat="server" Value="Click here to enter a new comment" />
<asp:HiddenField ID="Hidden_lblHide" runat="server" Value="Click here to close a new comment" />
<div style="display:none;">

    <asp:Button ID="btn_virtual" runat="server"  style="display:none;" CausesValidation="false"/>
</div>

<asp:HiddenField ID="Hidden_AllowPopUp" runat="server" Value="Allow pop-up windows">
</asp:HiddenField>
    
</form>
</body>
</html>
