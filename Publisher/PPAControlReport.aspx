﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="PPAControlReport.aspx.cs" Inherits="Publisher_PPAControlReport" %>

<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<link type="text/css" rel="Stylesheet" href="../scripts/jquery-ui-1.9.2.custom.dialog.min.css" />

<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="../scripts/jquery-ui-1.9.2.custom.DatePicker_Dialog.min.js" type="text/javascript"></script>
<script src="DpzControls/DistributionReport.js" type="text/javascript"></script>
<script type="text/javascript">
    var DaysInterval = new Number();
    var ChoosenIntervalOk = new Boolean();
    function CheckCurrentDates() {
        var elem = document.getElementById("<%# GetDivCurrentId %>");
        var result = Check_DateValidation(elem);
        if (!result)
            alert("<%# GetInvalidDate %>");
        return result;
    }
    function _CreateExcel() {
        $.ajax({
            url: '<%# GetCreateExcel %>',
            data: null,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if (data.d.length == 0) {
                    return;
                }
                var _iframe = document.createElement('iframe');
                _iframe.style.display = 'none';
                _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                document.getElementsByTagName('body')[0].appendChild(_iframe);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //              alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;

            }

        });
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
        <div class="main-inputs" style="width: 100%;">	
            <div class="form-fieldDleft">
    
        <div class="dash"><uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" /></div>
 
        </div>
        </div>
        <div class="div-btnRunReport">
            <asp:Button ID="btn_run" runat="server" Text="Create report"  CssClass="CreateReportSubmit2"
                onclick="btn_run_Click" OnClientClick="return CheckCurrentDates();" />    
        </div>
    </div>
    <div class="clear"><!-- --></div> 
    <div>
        <asp:PlaceHolder ID="_PlaceHolder" runat="server"></asp:PlaceHolder>   
    </div>
</div>


<asp:Label ID="lbl_PPAControlReport" runat="server" Text="PPA Control Report" Visible="false"></asp:Label>
 <asp:Label ID="lbl_DateError" runat="server" Text="Invalid date" Visible="false"></asp:Label>
 <input type="hidden" id="hf_GetReport" value="<%: GetPPCControlReportPage %>" />
</asp:Content>

