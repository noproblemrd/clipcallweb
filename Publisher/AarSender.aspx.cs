﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_AarSender : PageSetting
{
    const string DATE_TIME_FORMAT = "{0:dd/MM/yyyy HH:mm:ss}";
    static readonly string FolderPath;// = "professionalRecord";
    static Publisher_AarSender()
    {
        FolderPath = ConfigurationManager.AppSettings["professionalRecord"];
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SetToolboxEvents();
        if (!IsPostBack)
        {
            LoadCampaigns();
            LoadTasks();
        }
    }

    private void LoadTasks()
    {
        AarService.AarService _service = WebServiceConfig.GetAarService();
        AarService.CampaignTaskData[] _response = null;
        try
        {
            _response = _service.GetSmsCampaignTasks();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        ddl_ExecTask.Items.Clear();
        foreach (AarService.CampaignTaskData cd in _response)
        {
            ddl_ExecTask.Items.Add(new ListItem(cd.campaign + " " + string.Format(DATE_TIME_FORMAT, cd.date), cd.Id.ToString()));
        }
    }

    private void LoadCampaigns()
    {
        AarService.AarService _service = WebServiceConfig.GetAarService();
        AarService.CampaignData[] _response = null;
        try
        {
            _response = _service.GetSmsCampaigns();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        ddl_Campaign.Items.Clear();
        foreach (AarService.CampaignData cd in _response)
        {
            ddl_Campaign.Items.Add(new ListItem(cd.name, cd.Id.ToString()));
        }
    }

    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle("SMS Campaign execute");
        ToolboxReport1.RemovePrintButton();
        ToolboxReport1.RemoveExcelButton();
    }
    protected void btn_Click(object sender, EventArgs e)
    {       
        AarService.AarService _service = WebServiceConfig.GetAarService();
        AarService.SmsManagerResponse _response = null;
        _service.Timeout = 600000;
        if(string.IsNullOrEmpty(hf_filepath.Value))
        {
            lbl_message.Text = "Missing a CSV file";
            return;
        }
        try
        {
            _response = _service.CreateBulkSms(hf_filepath.Value, int.Parse(ddl_Campaign.SelectedValue));
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            lbl_message.Text = "Failed";
            return;
        }
        lbl_message.Text = _response.status.ToString().Replace('_', ' ');          

    }
    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        if (f_csv.PostedFile != null) {
          try {
              string FullName = FolderPath + f_csv.PostedFile.FileName;
              f_csv.PostedFile.SaveAs(FullName);
              hf_filepath.Value = FullName;
              lbl_FileUpload.InnerText = "Upload Successful!";
          }
          catch (Exception ex) {
              dbug_log.ExceptionLog(ex);
              hf_filepath.Value = string.Empty;
              lbl_FileUpload.InnerText = "Error saving file";
          }
       }
    }
    protected void btn_Task_Click(object sender, EventArgs e)
    {       
        AarService.AarService _service = WebServiceConfig.GetAarService();
        AarService.SmsManagerResponse _response = null;
        
        try
        {
            _response = _service.ReExecuteTaskSms(int.Parse(ddl_ExecTask.SelectedValue));
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            lbl_message.Text = "Failed";
            return;
        }
        lbl_messageTask.Text = _response.status.ToString().Replace('_', ' ');

    }

        
    
}