﻿<%@ WebHandler Language="C#" Class="DownloadExcel" %>

using System;
using System.Web;

public class DownloadExcel : IHttpHandler {
    private const string GZIP = "gzip";
    const string EXCEL_NAME = "Report";
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();
        string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        string filename = context.Request.QueryString["fn"];
        if (string.IsNullOrEmpty(filename))
        {
            context.Response.SuppressContent = true;
            context.Response.End();
            return;
        }
        path += filename;
        //context.Response.ContentType = "application/vnd.ms-excel";// "application/ms-excel";// "Application/x-msexcel";      
        context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.shee";
        context.Response.AppendHeader("Content-disposition", "attachment; filename=" + EXCEL_NAME + ".xlsx");
        context.Response.BinaryWrite(System.IO.File.ReadAllBytes(path));

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}