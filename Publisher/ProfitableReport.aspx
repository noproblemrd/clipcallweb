﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ProfitableReport.aspx.cs" Inherits="Publisher_ProfitableReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">

    window.onload = appl_init;

    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);

    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        SetTRClass();
        hideDiv();   
    }


    function CompareToPast(elm) {
        var _div = document.getElementById("<%# _div_compareDate.ClientID %>");
        var _btn = document.getElementById("<%# btn_run.ClientID %>");
        if (elm.checked) {
            Set_Dates_Compars();
            _div.style.display = "block";
        }
        else {
            _btn.setAttribute("onclick", "return CheckCurrentDates();");
            _div.style.display = "none";
        }
    }
    function Set_Dates_Compars() {
        var _btn = document.getElementById("<%# btn_run.ClientID %>");
        var _format = "<%# GetDateFormat() %>";
        var from_current = document.getElementById("<%# GetFromCurrentId %>");
        var to_current = document.getElementById("<%# GetToCurrentId %>");
        var from_past = document.getElementById("<%# GetFromPastId %>");
        var to_past = document.getElementById("<%# GetToPastId %>");
        var _start = GetDateFormat(from_current.value, _format);
        var _end = GetDateFormat(to_current.value, _format);
        /*
        var diff_day = days_between(_start, _end);

        var end_pass = new Date(_start); //.addDays(-1);
        end_pass.addDays(-1);
        var start_pass = new Date(end_pass);
        start_pass.addDays(diff_day * -1);
        */
        from_past.value = GetDateString(new Date(_start), _format);
        to_past.value = GetDateString(new Date(_end), _format);
        _btn.setAttribute("onclick", "return CheckCurrentAndCompareDates();");
    }
    function Set_Dates_Compars_by(_interval) {
        var _btn = document.getElementById("<%# btn_run.ClientID %>");
        var _format = "<%# GetDateFormat() %>";
        var from_current = document.getElementById("<%# GetFromCurrentId %>");
        var to_current = document.getElementById("<%# GetToCurrentId %>");
        var from_past = document.getElementById("<%# GetFromPastId %>");
        var to_past = document.getElementById("<%# GetToPastId %>");
        var _start = GetDateFormat(from_current.value, _format);
        var _end = GetDateFormat(to_current.value, _format);
        /*
        var diff_day = days_between(_start, _end);
        var end_pass = new Date(_start); //.addDays(-1);
        end_pass.addDays(-1);
        var start_pass = new Date(end_pass);
        //    alert(_interval);
        if (_interval == 29)
        start_pass.addMonth(-1);
        else if (_interval == 89)
        start_pass.addMonth(-3);
        else if (_interval == 364)
        start_pass.addFullYear(-1);
        else
        start_pass.addDays(diff_day * -1);
        */
        from_past.value = GetDateString(new Date(_start), _format);
        to_past.value = GetDateString(new Date(_end), _format);
        _btn.setAttribute("onclick", "return CheckCurrentAndCompareDates();");
    }
    function LoadChart(fileXML, _chart) {
        //      alert(fileXML+"----"+_chart);
        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render("div_chart");
    }
    function CheckCurrentDates() {
        var elem = document.getElementById("<%# GetDivCurrentId %>");
        var result = Check_DateValidation(elem);
        if (!result)
            alert("<%# GetInvalidDate %>");
        return result;
    }
    //CheckCurrentAndPastDates
    function CheckCurrentAndCompareDates() {
        if (!CheckCurrentDates())
            return false;
        var elem = document.getElementById("<%# GetDivPastId %>");
        var result = Check_DateValidation(elem);
        if (!result)
            alert("<%# GetInvalidDate %>");
        return result;
    }
    function SetCompareDate(elem_from, parentIdCurrent, mess, elem_maxId) {
        var _days = GetRangeDays(parentIdCurrent);
        var elm_parent = getParentByClassName(elem_from, "div_DatePicker");
        var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;
        var elem_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
        var elem_max = document.getElementById(elem_maxId);

        var _MaxValue = GetDateFormat(elem_max.value, _format);
        var _start = GetDateFormat(elem_from.value, _format);
        var _end = _start + ((24 * 60 * 60 * 1000) * _days); //GetDateFormat(elem_to.value, _format);
        //   _MaxValue = _MaxValue - (24 * 60 * 60 * 1000);
        //    alert("max= " +elem_max.id+" end= "+elem_from.id);
        //    alert("max= " +_MaxValue+" end= "+_end);
        //    if (_MaxValue < _end) {
        if (_start > _MaxValue) {
            _end = _MaxValue;
            var start_date = new Date(_MaxValue);
            var end_date = new Date(_MaxValue);
            end_date.addDays(_days);
            //   start_date.addDays(-1 * _days);
            //       alert(start_date);
            elem_from.value = GetDateString(start_date, _format);
            elem_to.value = GetDateString(end_date, _format);
            alert("<%# Lbl_DatesChange %>");
        }
        else {
            elem_to.value = GetDateString(new Date(_end), _format);
        }

    }
    function GetIntervalDates() {

        return 0;
    }
    function SetIntervalDatesValidation(elem) {

        ChoosenIntervalOk = true;
        SetDdlInterval(elem);
        if (!ChoosenIntervalOk)
            alert(document.getElementById('<%# lbl_ChosenInterval.ClientID %>').value);
    }
    function SetDdlInterval(elem) {
        DaysInterval = GetIntervalDates(); //elem.options[elem.selectedIndex].value;
        var _index = elem.selectedIndex;
        var Interval_name = elem.options[_index].value;
        var _days = GetRangeDays("<%# GetDivCurrentId %>");


        if (DaysInterval > _days) {
            _index--;
            ChoosenIntervalOk = false;
            elem.selectedIndex = _index;
            SetDdlInterval(elem);
        }
    }
    function OnBlurByDays(elm) {

        var elm_to;
        var elm_from;
        var elm_parent = getParentByClassName(elm, "div_DatePicker");
        var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;

        if (!validate_Date(elm.value, _format)) {
            try { alert(WrongFormatDate); } catch (ex) { }
            elm.focus();
            elm.select();
            return -1;
        }
        if (elm.className.indexOf("_to") == -1) {
            elm_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
            elm_from = elm;
        }
        else {
            elm_from = getElementsWithMultiClassesByClass("_from", elm_parent, "input")[0];
            elm_to = elm;
        }
        DaysInterval = GetIntervalDates();
        var _end = GetDateFormat(elm_to.value, _format);
        var _start = GetDateFormat(elm_from.value, _format);
        if ((_start + (24 * 60 * 60 * 1000 * DaysInterval)) > _end) {
            if (elm_from.id == elm.id) {
                _end = new Date(_start);
                if (DaysInterval == 29)
                    _end.addMonth(1);
                else if (DaysInterval == 89)
                    _end.addMonth(3);
                else if (DaysInterval == 364)
                    _end.addFullYear(1);
                else {
                    _end = _start + (24 * 60 * 60 * 1000 * DaysInterval);
                    _end = new Date(_end);
                }
                elm_to.value = GetDateString(_end, _format);
            }
            else {

                _start = new Date(_end);
                if (DaysInterval == 29)
                    _start.addMonth(-1);
                else if (DaysInterval == 89)
                    _start.addMonth(-3);
                else if (DaysInterval == 364)
                    _start.addFullYear(-1);
                else {
                    _start = _end - (24 * 60 * 60 * 1000 * DaysInterval);
                    _start = new Date(_start);
                }
                elm_from.value = GetDateString(_start, _format);
            }

            return -10;
        }
        return 1;

    }
    function SetTRClass(){
        var _index = 0;
        $('.data-tabledash').find('tr').not('._TH').each(function () {
            $(this).attr('class', '');
            if (_index == 0)
                $(this).addClass('_sum');
            if (_index % 2 == 0)
                $(this).addClass('even');
            else
                $(this).addClass('odd');
            _index++;
        });
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
         <uc1:ToolboxReport runat="server" ID="ToolboxReport1" /> 
<div class="page-content minisite-content2">
     
    <asp:Label ID="lbl_ProfitableReport" runat="server" Text="Profitability Report" Visible="false"></asp:Label>
 <div id="form-analytics" > 
    <div class="form-fieldDleft">
    
        <div class="dash"><uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" /></div>
  </div>
    <div class="form-fieldDright">
       
   
        <div class="form-field-Quarter">
            <asp:Label ID="lbl_Heading" runat="server" Cssclass="label" Text="Heading"></asp:Label>
            <asp:DropDownList ID="ddl_Heading" CssClass="form-select-semi" runat="server" onchange="SetIntervalDatesValidation(this);">
            </asp:DropDownList>
        </div>
        <div class="form-field-semi">
            <asp:Label ID="lbl_Partner" runat="server"  CssClass="label" Text="Partner"></asp:Label>
            <asp:DropDownList ID="ddl_Partner" CssClass="form-select-semi" runat="server">
            </asp:DropDownList>
        </div>    
        <div class="form-field-semi">
            <asp:Label ID="lbl_Country" runat="server"  CssClass="label" Text="Country"></asp:Label>
            <asp:DropDownList ID="ddl_Country" CssClass="form-select-semi" runat="server">
            </asp:DropDownList>
        </div>
     </div>
    
     <div class="clear"></div>    
     <div>
        <div class="form-field-secondLine">
            <asp:CheckBox ID="cb_OnlyNonUpsales" runat="server"  Text="Only non upsales" />
        </div>
         <div class="form-field-profit">
            <asp:Label ID="lbl_SliderType" runat="server"  CssClass="label" Text="Type"></asp:Label>
            <asp:DropDownList ID="ddl_SliderType" CssClass="form-select" runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <div class="clear"></div> 
      <div>
        <div class="form-field-secondLine">
            <asp:CheckBox ID="cb_Compare" runat="server" onclick="CompareToPast(this);" Text="Compare to" />
        </div>
            
         <div class="form-field-profit">
             <asp:Label ID="lbl_ProfitableLosing" runat="server"  CssClass="label" Text="Profitable / Losing"></asp:Label>
            <asp:DropDownList ID="ddl_ProfitableLosing" CssClass="form-select" runat="server">
            </asp:DropDownList>
         </div>

      </div>
      <div class="clear"><!-- --></div>
         <div class="compare2"  id="_div_compareDate" style="display:none;" runat="server">
               <div>
           
                <div class="dash-compare"><uc1:FromToDatePicker ID="FromToDatePicker_compare" runat="server" /></div>               
                <div class="form-fieldDright" style="left:0;">
                    <div class="form-field-Quarter">
                        <asp:Label ID="lbl_HeadingCompare" runat="server" Cssclass="label-secondLine" Text="<%# lbl_Heading.Text %>"></asp:Label>
                        <asp:DropDownList ID="ddl_HeadingCompare" CssClass="form-select-semi" runat="server" onchange="SetIntervalDatesValidation(this);">
                        </asp:DropDownList>
                    </div>
                    <div class="form-field-semi">
                         <asp:Label ID="lbl_PartnerCompare" runat="server" Cssclass="label-secondLine" Text="<%# lbl_Partner.Text %>"></asp:Label>        
                        <asp:DropDownList ID="ddl_PartnerCompare" CssClass="form-select-semi" runat="server">
                        </asp:DropDownList> 
                    </div>    
                    <div class="form-field-semi">
                        <asp:Label ID="lbl_CountryCompare" runat="server"  CssClass="label-secondLine" Text="<%# lbl_Country.Text %>"></asp:Label>
                        <asp:DropDownList ID="ddl_CountryCompare" CssClass="form-select-semi" runat="server">
                        </asp:DropDownList>
                    </div>
                 </div>               
             </div>
             <div class="clear"></div>    
             <div>
                <div class="form-field-secondLine">
                    <asp:CheckBox ID="cb_OnlyNonUpsalesCompare" runat="server"  Text="<%# cb_OnlyNonUpsales.Text %>" />
                </div>
                 <div class="form-field-profit">
                    <asp:Label ID="lbl_SliderType_Compare" runat="server"  CssClass="label" Text="Type"></asp:Label>
                    <asp:DropDownList ID="ddl_SliderType_Compare" CssClass="form-select" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>   
    
        <div class="clear"></div>
    <div class="dashsubmit">
        <asp:Button ID="btn_run" runat="server" Text="Create report"  CssClass="CreateReportSubmit2"
            OnClientClick="return CheckCurrentDates();" onclick="btn_run_Click" />
    
    </div>
    
 </div>   
    <div class="clear"><!-- --></div>
  
    
    <asp:UpdatePanel ID="UpdatePanel_table" runat="server" UpdateMode="Conditional">
    <ContentTemplate> 
        <div runat="server" id="div_ProfitTable">
            <div class="results">
                <span class="_Line">
                    <asp:Label ID="lbl_LastUpdate" runat="server" Text="Last update:"></asp:Label>
                    <asp:Label ID="lblLastUpdate" runat="server" Text="Label"></asp:Label>
                </span>
                <span runat="server" id="span_TotalDaus" class="_Line" visible="false">
                    <asp:Label ID="Lbl_TotalDaus" runat="server" Text="Total DAUS:"></asp:Label>
                    <asp:Label ID="LblTotalDaus" runat="server"></asp:Label>
                </span>
                <span runat="server" id="span_MonthlyUser" class="_Line" visible="false">
                    <asp:Label ID="lbl_MonthlyUser" runat="server" Text="Monthly User value today:"></asp:Label>
                    <asp:Label ID="lblMonthlyUser" runat="server" ></asp:Label>
                </span>
                <div class="results_Compare">                
                    <span runat="server" id="span_TotalDausCompare" class="_Line" visible="false">
                        <asp:Label ID="Lbl_TotalDausCompare" runat="server" Text="Total DAUS:"></asp:Label>
                        <asp:Label ID="LblTotalDausCompare" runat="server"></asp:Label>
                    </span>
                    <span runat="server" id="span_MonthlyUserCompare" class="_Line" visible="false">
                        <asp:Label ID="lbl_MonthlyUserComapre" runat="server" Text="Monthly User value today:"></asp:Label>
                        <asp:Label ID="lblMonthlyUserCompare" runat="server" ></asp:Label>
                    </span>
                </div>    
            </div>     
               
           <asp:Repeater ID="_Repeater" runat="server" EnableViewState="false">
           <HeaderTemplate>
                 <table class="data-tabledash" style="margin:0;">
                    <tr class="_TH" runat="server" id="tr_head_title">
                        <th colspan="15">
                            <asp:Label ID="Label112" runat="server" Text="<%# lbl_Value.Text %>"></asp:Label>
                        </th>
                        <th colspan="15" id="tr_head_title_compare" runat="server"  visible="<%# ShowCompare %>">
                            <asp:Label ID="Label113" runat="server" Text="<%# lbl_CompareValue.Text %>"></asp:Label>
                        </th>
                        <th id="Th_compare_gap" runat="server"  visible="<%# ShowCompare %>">
                            <asp:Label ID="Label220" runat="server" Text="<%# lbl_Gap.Text %>"></asp:Label>
                        </th>
                    </tr>
                    <tr class="_TH" runat="server" id="tr_head">
                        <th>
                            <asp:Label ID="Label101" runat="server" Text="<%# lbl_Heading.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label20" runat="server" Text="<%# lbl_TotalRequest.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label290" runat="server" Text="<%# lbl_Exposures.Text %>"></asp:Label>
                        </th>
                        
                        <th>
                            <asp:Label ID="Label29" runat="server" Text="<%# lbl_Exposure_KDAUs.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label30" runat="server" Text="<%# lbl_Request_KDAUs.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label25" runat="server" Text="<%# lbl_Request_Exposure.Text %>"></asp:Label>
                        </th>
                         <th>
                            <asp:Label ID="Label377" runat="server" Text="<%# lbl_Revenue_TotalRequest.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label102" runat="server" Text="<%# lbl_Requests.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label367" runat="server" Text="<%# lbl_EPPL.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Revenue.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label2" runat="server" Text="<%# lbl_Cost.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label37" runat="server" Text="<%# lbl_CPM.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label38" runat="server" Text="<%# lbl_RPM.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label12" runat="server" Text="<%# lbl_Profit.Text %>"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="Label13" runat="server" Text="<%# lbl_ProfitPercent.Text %>"></asp:Label>
                        </th>                       
                        <th runat="server" id="th1" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label100" runat="server" Text="<%# lbl_Heading.Text %>"></asp:Label>
                        </th>
                        <th runat="server" id="th8" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label22" runat="server" Text="<%# lbl_TotalRequest.Text %>"></asp:Label>
                        </th> 
                        <th runat="server" id="th10" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label298" runat="server" Text="<%# lbl_Exposures.Text %>"></asp:Label>
                        </th> 
                                                         
                        <th runat="server" id="th2" visible="<%# ShowCompare %>">
                           <asp:Label ID="Label103" runat="server" Text="<%# lbl_Exposure_KDAUs.Text %>"></asp:Label> 
                        </th>
                        <th runat="server" id="th11" visible="<%# ShowCompare %>">
                           <asp:Label ID="Label31" runat="server" Text="<%# lbl_Request_KDAUs.Text %>"></asp:Label> 
                        </th>
                        <th runat="server" id="th9" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label26" runat="server" Text="<%# lbl_Request_Exposure.Text %>"></asp:Label>
                        </th> 
                         <th runat="server" id="th130" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label308" runat="server" Text="<%# lbl_Revenue_TotalRequest.Text %>"></asp:Label>
                        </th>
                        <th runat="server" id="th12" visible="<%# ShowCompare %>">
                           <asp:Label ID="Label32" runat="server" Text="<%# lbl_Requests.Text %>"></asp:Label> 
                        </th>
                        <th runat="server" id="th13" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label309" runat="server" Text="<%# lbl_EPPL.Text %>"></asp:Label>
                        </th>
                        <th runat="server" id="th3" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Revenue.Text %>"></asp:Label>
                        </th>                                  
                        <th runat="server" id="th4" visible="<%# ShowCompare %>">
                           <asp:Label ID="Label5" runat="server" Text="<%# lbl_Cost.Text %>"></asp:Label> 
                        </th>
                        <th runat="server" id="th14" visible="<%# ShowCompare %>">
                           <asp:Label ID="Label39" runat="server" Text="<%# lbl_CPM.Text %>"></asp:Label> 
                        </th>
                        <th runat="server" id="th15" visible="<%# ShowCompare %>">
                           <asp:Label ID="Label40" runat="server" Text="<%# lbl_RPM.Text %>"></asp:Label> 
                        </th>
                        <th runat="server" id="th5" visible="<%# ShowCompare %>">
                            <asp:Label ID="Label14" runat="server" Text="<%# lbl_Profit.Text %>"></asp:Label>
                        </th>                                  
                        <th runat="server" id="th6" visible="<%# ShowCompare %>">
                           <asp:Label ID="Label15" runat="server" Text="<%# lbl_ProfitPercent.Text %>"></asp:Label> 
                        </th>
                        <th runat="server" id="th7" visible="<%# ShowCompare %>">
                           
                        </th>
                    </tr>
               
           </HeaderTemplate>
           <ItemTemplate>                                        
                <tr>
                    <td class="td-left">
                        <asp:Label ID="Label6" runat="server" Text="<%# Bind('Heading') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label23" runat="server" Text="<%# Bind('TotalRequest') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label295" runat="server" Text="<%# Bind('Exposures') %>"></asp:Label>                       
                    </td>
                   
                     <td>
                        <asp:Label ID="Label33" runat="server" Text="<%# Bind('Exposure_KDAUs') %>"></asp:Label>                       
                    </td>
                     <td>
                        <asp:Label ID="Label34" runat="server" Text="<%# Bind('Request_KDAUs') %>"></asp:Label>                       
                    </td>
                     <td>
                        <asp:Label ID="Label27" runat="server" Text="<%# Bind('Request_Exposure') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label309" runat="server" Text="<%# Bind('Revenue_TotalRequest') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="<%# Bind('Requests') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label337" runat="server" Text="<%# Bind('EPPL') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="<%# Bind('Revenue') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="<%# Bind('Cost') %>"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label41" runat="server" Text="<%# Bind('CPM') %>"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label42" runat="server" Text="<%# Bind('RPM') %>"></asp:Label>
                    </td>
                     <td>
                        <asp:Label ID="Label16" runat="server" Text="<%# Bind('Profit') %>" CssClass="<%# Bind('ValuePosNeg') %>"></asp:Label>                       
                    </td>
                    <td>
                        <asp:Label ID="Label17" runat="server" Text="<%# Bind('ProfitPercent') %>" CssClass="<%# Bind('ValuePosNeg') %>"></asp:Label>
                    </td>  
                    <td runat="server" id="td1" visible="<%# Bind('ShowCompare') %>" class="td-left">
                        <asp:Label ID="Label50" runat="server" Text="<%# Bind('HeadingCompare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td8" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label24" runat="server" Text="<%# Bind('TotalRequestCompare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td10" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label296" runat="server" Text="<%# Bind('ExposuresCompare') %>"></asp:Label>       
                    </td> 
                    
                    <td runat="server" id="td11" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label35" runat="server" Text="<%# Bind('Exposure_KDAUs_Compare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td12" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label36" runat="server" Text="<%# Bind('Request_KDAUs_Compare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td9" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label28" runat="server" Text="<%# Bind('Request_Exposure_Compare') %>"></asp:Label>       
                    </td> 
                     <td runat="server" id="td131" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label310" runat="server" Text="<%# Bind('Revenue_TotalRequest_Compare') %>"></asp:Label>       
                    </td> 

                    <td runat="server" id="td2" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label10" runat="server" Text="<%# Bind('RequestsCompare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td13" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label347" runat="server" Text="<%# Bind('EPPLCompare') %>"></asp:Label>       
                    </td>                   
                    <td runat="server" id="td3" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label9" runat="server" Text="<%# Bind('RevenueCompare') %>"></asp:Label>       
                    </td>   
                    <td runat="server" id="td4" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label11" runat="server" Text="<%# Bind('CostCompare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td14" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label43" runat="server" Text="<%# Bind('CPMCompare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td15" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label44" runat="server" Text="<%# Bind('RPMCompare') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td5" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label18" runat="server" Text="<%# Bind('ProfitCompare') %>" CssClass="<%# Bind('ComparePosNeg') %>"></asp:Label>       
                    </td>   
                    <td runat="server" id="td6" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label19" runat="server" Text="<%# Bind('ProfitPercentCompare') %>" CssClass="<%# Bind('ComparePosNeg') %>"></asp:Label>       
                    </td> 
                    <td runat="server" id="td7" visible="<%# Bind('ShowCompare') %>">
                        <asp:Label ID="Label21" runat="server" Text="<%# Bind('Gap') %>" CssClass="<%# Bind('GapPosNeg') %>"></asp:Label>       
                    </td>    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
            </asp:Repeater>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    
   
</div>

<asp:Label ID="lbl_DatesChange" Text="The date you selected have been changed according to interval requirement" runat="server" Visible="false"></asp:Label>
<asp:HiddenField ID="lbl_ChosenInterval" runat="server"
            Value="The interval you have chosen is not adequate to the date range you selected. Please choose smaller value" />
 <asp:Label ID="lbl_DateError" runat="server" Text="Invalid date" Visible="false"></asp:Label>

 <asp:Label ID="lbl_Requests" runat="server" Text="#Sold" Visible="false"></asp:Label>
 <asp:Label ID="lbl_Exposures" runat="server" Text="Exposures" Visible="false"></asp:Label>
 
  <asp:Label ID="lbl_TotalRequest" runat="server" Text="Total Requests" Visible="false"></asp:Label>
   
  
   <asp:Label ID="lbl_Revenue" runat="server" Text="Revenue" Visible="false"></asp:Label>
  <asp:Label ID="lbl_Cost" runat="server" Text="Cost" Visible="false"></asp:Label>
  <asp:Label ID="lbl_CPM" runat="server" Text="CPM" Visible="false"></asp:Label>
  <asp:Label ID="lbl_RPM" runat="server" Text="RPM" Visible="false"></asp:Label>
   <asp:Label ID="lbl_Profit" runat="server" Text="Profit" Visible="false"></asp:Label>
  <asp:Label ID="lbl_ProfitPercent" runat="server" Text="Profit %" Visible="false"></asp:Label>

   <asp:Label ID="lbl_Value" runat="server" Text="Value" Visible="false"></asp:Label>
    <asp:Label ID="lbl_CompareValue" runat="server" Text="Compared Value" Visible="false"></asp:Label>

     <asp:Label ID="lbl_Gap" runat="server" Text="Gap" Visible="false"></asp:Label>
     <asp:Label ID="lbl_Exposure_KDAUs" runat="server" Text="Exposure/KDAUs" Visible="false"></asp:Label>
<asp:Label ID="lbl_Request_KDAUs" runat="server" Text="Request/KDAUs" Visible="false"></asp:Label>
<asp:Label ID="lbl_Request_Exposure" runat="server" Text="Requests/Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_Revenue_TotalRequest" runat="server" Text="Revenue/Total request" Visible="false"></asp:Label>
<asp:Label ID="lbl_EPPL" runat="server" Text="EPPL" Visible="false"></asp:Label>


</asp:Content>

