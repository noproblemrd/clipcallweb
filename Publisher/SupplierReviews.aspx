﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SupplierReviews.aspx.cs" Inherits="Publisher_SupplierReviews" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../FromToDate.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../general.js" language="javascript"></script>
<script language="javascript" type="text/javascript" src="../Calender/_Calender.js"></script>
<link href="../Calender/CalenderDiv.css" rel="stylesheet" type="text/css" />
<link href="style.css" type="text/css" rel="stylesheet" />
<link href="../UpdateProgress/updateProgress.css" type="text/css" rel="stylesheet" />

<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<script src="../jquery.min.js" type="text/javascript"></script> 
<script src="../jquery-ui.min.js" type="text/javascript"></script> 

<link media="screen" rel="stylesheet" href="../jquery/colorbox.css" />
<!--<script src="../jquery/jquery2.min.js" type="text/javascript" language="javascript"></script>-->
<script src="../colorbox/jquery.colorbox.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" language="javascript">
    var message="<%# GetCloseMessage %>";
    function pageLoad(sender, args) 
    {         
        appl_init();
    }
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();  
    }
    
    function openIframe(_id)
    {            
         var url = "ConsumerDetails.aspx?Consumer="+_id;
        $(document).ready(function(){$.colorbox({href:url,width:'1000px', height:'90%', iframe:true});});
   //     $(function(){$.colorbox({href:url,innerWidth:'840px', innerHeight:'90%', iframe:true});});
        /*
         $(function ()
    {
        $(".example6").colorbox({iframe:true, innerWidth:425, innerHeight:344});    
    })
    */
    }
    
    
    $(function() {
        $("._Advertiser").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "../AdvertisersService.asmx/GetSuggestionsAdvertisers",
                    data: "{ 'prefixText': '" + request.term + "' }",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function(data) { return data; },
                    success: function(data) {
                        response($.map(data.d, function(item) {
                            return {
                                value: item
                            }
                        }))
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //    alert(textStatus);
                    }
                });
            },
            minLength: 1
        });
    });

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ></cc1:ToolkitScriptManager>
<div class="page-content minisite-content">
   
	 <h2>Supplier Reviews</h2>
					
	<div id="form-analytics" >	
		
		    <div class="form-field form-status">
                <asp:Label ID="lbl_ChooseStatus" CssClass="label" runat="server" Text="Status"></asp:Label>                                    
                <asp:DropDownList ID="ddl_Status" CssClass="form-select" runat="server"></asp:DropDownList>			
			</div>
			
			<div class="form-field form-balance">
			    <asp:Label ID="lbl_Advertiser" CssClass="label" runat="server" Text="Advertiser"></asp:Label> 
                <asp:TextBox ID="txt_Advertiser" CssClass="form-text _Advertiser" runat="server"></asp:TextBox>
                
			
			</div>
			 <div class="reviews"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>                                        

		
	</div>
	
	<div id="list" class="table">                    
        <asp:UpdatePanel runat="server" ID="_uPanel" UpdateMode="Conditional">
            <ContentTemplate> 
             <div class="results1"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
               <asp:Repeater runat="server" ID="_reapeter"   >
                <HeaderTemplate>
                    <table class="data-table">
                        <thead>
                            <tr>
                                <th><asp:Label ID="Label50" runat="server" Text="<%#lbl_CreatOn.Text %>"></asp:Label></th>                                
                                <th><asp:Label ID="Label55" runat="server" Text="<%#lbl_supplierName.Text %>"></asp:Label></th>  
                                <th><asp:Label ID="Label31" runat="server" Text="<%#lbl_consumer.Text %>"></asp:Label></th>						        
						        <th><asp:Label ID="Label1" runat="server" Text="<%#lbl_name.Text %>"></asp:Label></th>
						        <th><asp:Label ID="Label51" runat="server" Text="<%#lbl_Like.Text %>"></asp:Label></th>
						        <th><asp:Label ID="Label32" runat="server" Text="<%#lbl_status.Text %>"></asp:Label></th>
						        
						    </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                        
                <ItemTemplate>
                        <tr runat="server" id="tr_show" class="odd">
                            
                            <td><asp:Label ID="Label200" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label></td>
                            <td><asp:Label ID="Label209" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label></td>
						    <td>
						        <a id="a_ContactNum" runat="server" href="<%# Bind('OnClientClick') %>">
                                    <asp:Label ID="Label202" runat="server" Text="<%# Bind('Phone') %>"></asp:Label> 
						        </a>
						    </td>
						    <td><asp:Label ID="Label3" runat="server" Text="<%# Bind('Name') %>"></asp:Label></td>
						    <td>
                                <asp:Image ID="img_like" runat="server" ImageUrl="<%# Bind('IsLike') %>" />
						    </td>
						    <td>
						        <asp:ImageButton ID="ib_status" runat="server" ImageUrl="<%# Bind('StatusPic') %>"
                                 OnClick="ib_status_click" CommandArgument="<%# Bind('ReviewId') %>"/>
                                <asp:Label ID="lbl_Status" runat="server" Text="<%# Bind('Status') %>" Visible="false"></asp:Label> 
						    </td>						    
						</tr>
						<tr>
						     <td colspan="5" class="even">
						        <div style="text-align:center;">
                                    <asp:Label ID="lbl_review" runat="server" Text="<%# Bind('Review') %>"></asp:Label>
						        </div>
						        
                            </td>
						</tr>
					</ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>          
            
             <asp:Panel id="div_paging" CssClass="pagination" runat="server" visible="false">     
                <ul>
                    <li class="first"><asp:LinkButton ID="lnkPreviousPage" runat="server" OnClick="lnkPreviousPage_Click">Previous page</asp:LinkButton></li>
                    <asp:PlaceHolder ID="PlaceHolderPages" runat="server"></asp:PlaceHolder>
                    <li class="last"><asp:LinkButton ID="lnkNextPage" runat="server" OnClick="lnkNextPage_Click">Next page</asp:LinkButton></li>
                </ul>
            </asp:Panel>  
                    
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>   



<asp:Label ID="lbl_CreatOn" runat="server" Text="Create on" Visible="false"></asp:Label>
<asp:Label ID="lbl_consumer" runat="server" Text="Consumer" Visible="false"></asp:Label>
<asp:Label ID="lbl_name" runat="server" Text="Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_Like" runat="server" Text="Rating" Visible="false"></asp:Label>
<asp:Label ID="lbl_status" runat="server" Text="Status" Visible="false"></asp:Label>

<asp:Label ID="lbl_supplierName" runat="server" Text="Supplier name" Visible="false"></asp:Label>


<asp:Label ID="lbl_number" runat="server" Text="Number" Visible="false"></asp:Label>

<asp:Label ID="lbl_UserPhone" runat="server" Text="User phone" Visible="false"></asp:Label>
<asp:Label ID="lbl_Review" runat="server" Text="Review" Visible="false"></asp:Label>


<asp:Label ID="lbl_All" runat="server" Text="All" Visible="false"></asp:Label>
<asp:Label ID="lbl_NoResult" runat="server" Visible="false"
     Text="There are no results. Please change your report criterion and try again."></asp:Label>

<asp:Label ID="lbl_IfToExite" runat="server" Text="Are you sure you want to exit? Unsaved data will be lost!" Visible="false"></asp:Label>
<asp:Label ID="lblRecordMached" runat="server" Text="records matched your query" Visible="false"></asp:Label>
</asp:Content>



