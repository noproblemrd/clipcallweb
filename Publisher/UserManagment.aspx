<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" 
EnableEventValidation="false" 
CodeFile="UserManagment.aspx.cs" Inherits="Publisher_UserManagment" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">


  <%--  <link href="PublishStyle.css" rel="stylesheet" type="text/css"/>--%>
    <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css"/>

	   <script type="text/javascript" language="javascript">
	   function closePopup()
	   {
	        document.getElementById("<%# popUpaddUser.ClientID %>").className="popModal_del popModal_del_hide";	        
	        document.getElementById("<%# txt_NewFName.ClientID %>").value="";
	        document.getElementById("<%# txt_newEmail.ClientID %>").value="";
	        document.getElementById("<%# txt_NewPhone.ClientID %>").value="";
	        document.getElementById("<%# hf_UserId.ClientID %>").value = "";
	        document.getElementById("<%# txt_BizId.ClientID %>").value = "";
	        document.getElementById("<%# cb_ShowHomePage.ClientID %>").checked = false;
	        document.getElementById("<%# cb_CanHearRecord.ClientID %>").checked = false;
	        var validators = Page_Validators;  
            for (var i = validators.length-1; i > -1; i--)
            {
            //    validators[i].style.visibility = "hidden";
                validators[i].style.display = "none";
            }
	        var ddl = document.getElementById("<%# ddl_NeySecurityLevel.ClientID %>");	 
	        ddl.selectedIndex=0;
	        ddlLang = document.getElementById("<%# ddl_NewLanguges.ClientID %>");
	        var langId=document.getElementById("<%# hf_LangId.ClientID %>").value;
	        
	        for(var j=0;j<ddlLang.length;j++)
	        {
	           
	            if(langId==ddlLang.options[j].value)
	            {
	               
	                ddlLang.selectedIndex=j;
	                break;
	            }    
	        }
	        
	        $find('_modal').hide();
	   }
	   function openPopup()
	   {
	        document.getElementById("<%# popUpaddUser.ClientID %>").className="popModal_del";
	        $find('_modal').show();	        
	   }
	   function Check_validation()
	   {
	        if(!Page_ClientValidate('NewPublisher'))
	            return false;
	        showDiv();
	        return true;
	   }
	   function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        
  //      pgRegMgr.add_endRequest(EndHandler);
        }
        function BeginHandler()
        {
            showDiv();
        }
	    function pageLoad(sender, args)
        {
            appl_init();            
            hideDiv();
            
        }
	   
	   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <uc1:Toolbox ID="Toolbox1" runat="server" />
    <h5>
			<asp:Label ID="lblSubTitle" runat="server">User managment</asp:Label>
			</h5>
<div class="page-content minisite-content5">

			
	    <div id="form-analyticsseg">
	   
			<asp:UpdatePanel ID="_updatePanelTable" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
			
                <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" ShowFooter="false" 
                CssClass="data-table" Width="650" DataKeyNames="AccountId"
                AllowPaging="True" 
                    onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                 <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                <FooterStyle CssClass="footer"  />
                
                <Columns>                     
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                    <br />
                    <asp:CheckBox ID="cb_all" runat="server" >
                    </asp:CheckBox>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cb_choose" runat="server" />
                    
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">              
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="<%# lbl_Number.Text %>"></asp:Label> 
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("AccountNumber") %>'></asp:Label>
                    <asp:Label ID="lbl_AccountId" runat="server" Text='<%# Bind("AccountId") %>' Visible="false"></asp:Label> 
                </ItemTemplate>
                
                </asp:TemplateField>
                
                <asp:TemplateField>              
                <HeaderTemplate>
                    <asp:Label ID="Label101" runat="server" Text="<%# lbl_BizId.Text %>"></asp:Label> 
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblBizId" runat="server" Text='<%# Bind("BizId") %>'></asp:Label>
                </ItemTemplate>
                
                </asp:TemplateField>
                

                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label3" runat="server" Text="<%# lbl_name.Text %>"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblname" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
               
                </asp:TemplateField>
                
               
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label8" runat="server" Text="<%# lbl_email.Text %>"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                </ItemTemplate>
                
                </asp:TemplateField>
                
                                 
                 <asp:TemplateField Visible="false">              
                <HeaderTemplate>
                    <asp:Label ID="Label10" runat="server" Text="<%# lbl_phone.Text %>"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("PhoneNumber") %>'></asp:Label>
                </ItemTemplate>
                
                </asp:TemplateField>
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label12" runat="server" Text="<%# lbl_password.Text %>"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblPassword" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
                </ItemTemplate>                
                </asp:TemplateField>
                
                 <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label14" runat="server" Text="<%# lbl_SecurityLevel.Text %>"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblSecurityLevel" runat="server" Text='<%# Bind("SecurityLevel") %>'></asp:Label>
                </ItemTemplate>
                
                </asp:TemplateField>
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label18" runat="server" Text="<%# lbl_Language.Text %>"></asp:Label>                    
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbl_LangName" runat="server" Text='<%# Bind("LangName") %>'></asp:Label>
                    <asp:Label ID="lbl_SiteLangId" runat="server" Text='<%# Bind("SiteLangId") %>' Visible="false"></asp:Label> 
                </ItemTemplate>                
                </asp:TemplateField>
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label19" runat="server" Text="<%# lbl_HomePage.Text %>"></asp:Label>                   
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblShowHomePage" runat="server" Text='<%# Bind("DashboardAsHomePage") %>'></asp:Label>                 
                </ItemTemplate>                
                </asp:TemplateField>

                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label20" runat="server" Text="<%# lbl_CanHearRecord.Text %>"></asp:Label>                   
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCanHearAllRecordings" runat="server" Text='<%# Bind("CanHearAllRecordings") %>'></asp:Label>                 
                </ItemTemplate>                
                </asp:TemplateField>
                
                </Columns>
             </asp:GridView>
             </ContentTemplate>
             </asp:UpdatePanel>

         </div>
 </div>    
  

    <div id="popUpaddUser" runat="server" sclass="content" style="max-height:450px;display:none;" class="popModal_del popModal_del_hide"  >
 <div class="top"></div>
    	<a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
          <div class="content" >
      
    	    <div class="modaltit">
            <h2><asp:Label ID="lbl_UserForm" runat="server" Text="User Form"></asp:Label></h2>
            </div>
            
    	    <div>
    	    <center>
    	    
                <table>
    	    <tr>
    	    <td>
                <asp:Label ID="lbl_NewFName" runat="server" Text="Account name"></asp:Label>
                <asp:HiddenField ID="hf_UserId" runat="server" />
    	        
    	    </td>
    	    <td>
    	        <asp:TextBox ID="txt_NewFName" CssClass="form-text" runat="server"></asp:TextBox><br />
                
    	    </td>                
    	    </tr>   	    
    	    <tr>
    	    <td>
                <asp:Label ID="lbl_BizId" runat="server" Text="BizId"></asp:Label>
    	    
    	    </td>
    	    <td>
    	        <asp:TextBox ID="txt_BizId" CssClass="form-text" runat="server"></asp:TextBox><br />
    	       
                 
    	    </td>                
    	    </tr>
    	    
    	    
    	    <tr>
    	    <td>
                <asp:Label ID="lbl_newEmail" runat="server" Text="E-mail"></asp:Label>
    	    
    	    </td>
    	    <td>
    	        <asp:TextBox ID="txt_newEmail" CssClass="form-text" runat="server"></asp:TextBox><br />
    	        <asp:RequiredFieldValidator ID="RequiredFieldValidator_Email" runat="server" ErrorMessage="Missing" Display="Dynamic"
                 ValidationGroup="NewPublisher" ControlToValidate="txt_newEmail" CssClass="error-msg"></asp:RequiredFieldValidator>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator_newEmail" 
                runat="server" ErrorMessage="Invalid email"  Display="Dynamic" CssClass="error-msg"
                 ValidationGroup="NewPublisher" ControlToValidate="txt_newEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    	    </td>                
    	    </tr>
    	    
    	    <tr>
    	    <td>
                <asp:Label ID="lbl_NewPhone" runat="server" Text="Phone number"></asp:Label>
    	    
    	    </td>
    	    <td>
    	        <asp:TextBox ID="txt_NewPhone" CssClass="form-text"  runat="server"></asp:TextBox><br />
    	        <asp:RequiredFieldValidator ID="RequiredFieldValidator_Phone" runat="server" ErrorMessage="Missing" 
                Display="Dynamic" CssClass="error-msg"
                 ValidationGroup="NewPublisher" ControlToValidate="txt_NewPhone" ></asp:RequiredFieldValidator>
                 
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" Display="Dynamic" CssClass="error-msg2"
                runat="server" ErrorMessage="Invalid phone number" ValidationExpression="<%# GetPhoneValidation %>"
                 ValidationGroup="NewPublisher" ControlToValidate="txt_NewPhone"></asp:RegularExpressionValidator>
    	    </td>                
    	    </tr>
    	    
    	    <tr>
    	    <td>
                <asp:Label ID="lbl_NewSecurityLevel" runat="server" Text="Security level"></asp:Label>    	    
    	    </td>
    	    <td>
    	        <asp:DropDownList ID="ddl_NeySecurityLevel" CssClass="form-text"  runat="server">
                    <asp:ListItem Value="1" Text="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                </asp:DropDownList>
    	    </td>                
             </tr>
             <tr>
    	    <td>
                <asp:Label ID="lbl_NewLanguage" runat="server" Text="Language"></asp:Label>    	    
    	    </td>
    	    <td>
    	        <asp:DropDownList ID="ddl_NewLanguges" CssClass="form-text"  runat="server">
                    
                </asp:DropDownList>
    	    </td>                
             </tr> 
             <tr>
             <td>
                <asp:Label ID="lbl_ShowHomePage" runat="server" Text="<%# lbl_HomePage.Text %>"></asp:Label>
             </td>
             <td>
                 <asp:CheckBox ID="cb_ShowHomePage" runat="server" />                
             </td>
             </tr>    
             
             <tr>
             <td>
                <asp:Label ID="lbl_CanHearRecordings" runat="server" Text="<%# lbl_CanHearRecord.Text %>"></asp:Label>
             </td>
             <td>
                 <asp:CheckBox ID="cb_CanHearRecord" runat="server" />                
             </td>
             </tr>      
    	    </table>             
             </center>
    	    </div> 
           
	<div class="content2">
	<table class="Table_Button">
    <tr>
    <td>
    
        <asp:Button ID="btn_Set" runat="server" Text="Set" ValidationGroup="NewPublisher" OnClick="btn_Set_Click" OnClientClick="return Check_validation();"
         CssClass="btn4" />
        
    </td>        
    <td>
        <input id="btn_cancel" type="button" class="btn4" value="Cancel" runat="server" onclick="javascript:closePopup();"/>
    </td>
    </tr>
    </table>
	</div>
	</div>
	<div class="bottom2"></div>
        	
</div>
    <cc1:ModalPopupExtender ID="_mpe" runat="server"
            TargetControlID="btn_virtual"
            PopupControlID="popUpaddUser"
             BackgroundCssClass="modalBackground" 
              
              BehaviorID="_modal"               
              DropShadow="false"
           ></cc1:ModalPopupExtender>
           
           <div id="divLoader" class="_Background" style="text-align:center;display:none;">   
      <table width="100%" height="100%">
        <tr>
            <td height="100%" style="vertical-align:middle;">
              <img src="../UpdateProgress/ajax-loader.gif" style="text-align:center;vertical-align:middle;" alt="progress"/>
            </td>
        </tr>
     </table>        
    </div>   
    <div style="display:none;">
       <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />          
           
   </div>


    <asp:Label ID="lbl_Number" runat="server" Text="ID" Visible="false"></asp:Label>
    <asp:Label ID="lbl_name" runat="server" Text="Name" Visible="false"></asp:Label>
    
    <asp:Label ID="lbl_email" runat="server" Text="E-mail" Visible="false"></asp:Label>
    <asp:Label ID="lbl_phone" runat="server" Text="Phone number" Visible="false"></asp:Label>
    <asp:Label ID="lbl_password" runat="server" Text="Password" Visible="false"></asp:Label>
    <asp:Label ID="lbl_SecurityLevel" runat="server" Text="Security level" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Language" runat="server" Text="Language" Visible="false"></asp:Label>
    <asp:Label ID="lbl_HomePage" runat="server" Text="Show dashboard as home page" Visible="false"></asp:Label> 
    <asp:Label ID="lbl_CanHearRecord" runat="server" Text="Can listen to recordings" Visible="false"></asp:Label> 
    
   
    <asp:HiddenField ID="hf_LangId" runat="server" />
    <asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
    <asp:Label ID="lbl_noreasultSearch" runat="server" Text="There are no results for this search" Visible="false"></asp:Label>
    <asp:Label ID="lbl_cantdelete" runat="server" Text="You can not delete yourself" Visible="false"></asp:Label>

</asp:Content>

