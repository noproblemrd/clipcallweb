<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master"
 AutoEventWireup="true" 
CodeFile="EditSecurityLevel.aspx.cs" 
Inherits="Publisher_EditSecurityLevel" Title="Security Setting"  %>



  <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asps" %>
    
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="PublishStyle.css" rel="stylesheet" type="text/css"/>
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />

   <script type="text/javascript" src="../general.js"></script>
   <script type="text/javascript" language="javascript">
   function pageLoad(sender, args)
   {
        hideDiv();
   }
   
   
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">
   <%// ClientIDMode property for async postback for repeater control in update panel %>
   <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
   </cc1:ToolkitScriptManager>
    

 <div class="page-content minisite-content">
							<h2>
                                <asp:Label ID="lblTitleSecurity" runat="server" Text="Security level setting"></asp:Label>
                                
							</h2>
                            
                            <asps:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true" >
                            <ContentTemplate>
                            
                            	
		<asp:Repeater runat="server" ID="_reapeter"  >
            
            <HeaderTemplate>
                <table class="data-table" border="0">
                    <tr>
                        <th>
			        <asp:Label ID="Label50" runat="server" Text="<%#lbl_PageName.Text %>"></asp:Label>
                            
			     </th>
			     <th>
			         <asp:Label ID="Label51" runat="server" Text="<%#lbl_publisher1.Text %>"></asp:Label>
			     </th>
			     <th>
			         <asp:Label ID="Label52" runat="server" Text="<%#lbl_publisher2.Text %>"></asp:Label>
			     </th>
			     <th>
			         <asp:Label ID="Label53" runat="server" Text="<%#lbl_publisher3.Text %>"></asp:Label>
			     </th>
			     <th>
			         <asp:Label ID="Label1" runat="server" Text="<%#lbl_publisher4.Text %>"></asp:Label>
			     </th>
			     <th>
			         <asp:Label ID="Label2" runat="server" Text="<%#lbl_publisher5.Text %>"></asp:Label>
			     </th>
                    </tr>
            </HeaderTemplate>
	        <ItemTemplate>
                <tr class="odd" >
			     <td>
			        <asp:Label ID="lbl_name" runat="server" Text="<%# Bind('PageDesc') %>"></asp:Label>      
                    <asp:Label ID="lbl_pageId" runat="server" Text="<%# Bind('ID') %>" Visible="false"></asp:Label>
			     </td>
			     <td>
			        <asp:CheckBox ID="_cb1" runat="server" Checked="<%#Bind('PUBLISHER1') %>"  Enabled="false" />
			     </td>
			     <td>
			        <asp:CheckBox ID="_cb2" runat="server" Checked="<%#Bind('PUBLISHER2') %>"
			         AutoPostBack="true" OnCheckedChanged="cb_checkedUp" onclick="javascript:showDiv();"  />
			     </td>
			     <td>
			        
                    <asp:CheckBox ID="_cb3" runat="server" Checked="<%#Bind('PUBLISHER3') %>"
                     AutoPostBack="true" OnCheckedChanged="cb_checkedUp" onclick="javascript:showDiv();" />
			     </td>
			     <td>
			        
                    <asp:CheckBox ID="_cb4" runat="server" Checked="<%#Bind('PUBLISHER4') %>"
                    AutoPostBack="true" OnCheckedChanged="cb_checkedUp" onclick="javascript:showDiv();" />
			     </td>
			     <td>
			        
                    <asp:CheckBox ID="_cb5" runat="server" Checked="<%#Bind('PUBLISHER5') %>"
                     AutoPostBack="true" OnCheckedChanged="cb_checkedUp" onclick="javascript:showDiv();" />
			     </td>
			     </tr>
			     <asp:Repeater runat="server" ID="_reapeterIn"  >
			      <ItemTemplate>
			     <tr class="even">
			     
			    
			      <td>
			        <asp:Label ID="lbl_name" runat="server" Text="<%# Bind('PageDesc') %>"></asp:Label>      
                    <asp:Label ID="lbl_pageId" runat="server" Text="<%# Bind('ID') %>" Visible="false"></asp:Label>
			     </td>
			     <td>
			        <asp:CheckBox ID="_cb1" runat="server" Checked="<%#Bind('PUBLISHER1') %>"  Enabled="false" />
			     </td>
			     <td>
			        <asp:CheckBox ID="_cb2" runat="server" Checked="<%#Bind('PUBLISHER2') %>" 
			        AutoPostBack="true" OnCheckedChanged="cb_checkedBot" onclick="javascript:showDiv();" />
			     </td>
			     <td>
			        
                    <asp:CheckBox ID="_cb3" runat="server" Checked="<%#Bind('PUBLISHER3') %>"
                     AutoPostBack="true" OnCheckedChanged="cb_checkedBot" onclick="javascript:showDiv();" />
			     </td>
			     <td>
			        
                    <asp:CheckBox ID="_cb4" runat="server" Checked="<%#Bind('PUBLISHER4') %>"
                     AutoPostBack="true" OnCheckedChanged="cb_checkedBot" onclick="javascript:showDiv();" />
			     </td>
			     <td>
			        
                    <asp:CheckBox ID="_cb5" runat="server" Checked="<%#Bind('PUBLISHER5') %>"
                     AutoPostBack="true" OnCheckedChanged="cb_checkedBot" onclick="javascript:showDiv();" />
			     </td>
			    
                </tr>
                </ItemTemplate>            
            </asp:Repeater>  			     
                     </ItemTemplate>
            <FooterTemplate>
                </table>    
                            
            </FooterTemplate>                      
            </asp:Repeater>      					
						</ContentTemplate>
                            </asps:UpdatePanel>			
							
	
    <div>
    <center>
        <asp:Button ID="btn_Update" runat="server" Text="Update" CssClass="form-submit" OnClick="btn_Update_Click" />
    </center>
    </div>
  
	</div>
	
	<div id="divLoader" class="_Background" style="text-align:center;display:none;">   
    <table width="100%" height="100%">
        <tr>
            <td height="100%" style="vertical-align:middle;">
                <img src="../UpdateProgress/ajax-loader.gif" style="text-align:center;vertical-align:middle;" alt=""/>
            </td>
        </tr>
    </table>
</div>  

    <div id="header_table">
    <asp:Label ID="lbl_PageName" runat="server" Text="Page Name" Visible="false"></asp:Label>
        <asp:Label ID="lbl_publisher1" runat="server" Text="group 1" Visible="false"></asp:Label>
        <asp:Label ID="lbl_publisher2" runat="server" Text="group 2" Visible="false"></asp:Label>
        <asp:Label ID="lbl_publisher3" runat="server" Text="group 3" Visible="false"></asp:Label>
        <asp:Label ID="lbl_publisher4" runat="server" Text="group 4" Visible="false"></asp:Label>
        <asp:Label ID="lbl_publisher5" runat="server" Text="group 5" Visible="false"></asp:Label>
    
    </div>


</asp:Content>

