﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Globalization;

public partial class Publisher_ReportWizardResult : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string _report = Request.QueryString["ReportId"];
            int reportId;
            if (!int.TryParse(_report, out reportId))
                Response.Redirect("ReportWizard.aspx");
            ReportIdV = reportId;
            SetToolBox();
            LoadReport(reportId);
        }
        else
        {
            SetGridView();
        }
        SetToolBoxEvents();
    }

    private void SetGridView()
    {
        foreach (GridViewRow _row in _GridView.Rows)
            SetRowGridView(_row);
    }

    private void SetToolBox()
    {
        Toolbox1.RemoveAdd();
        Toolbox1.RemoveDelete();
        Toolbox1.RemoveAttach();
        string _script = "OpenSaveReport(); return false;";
        Toolbox1.SetClientScriptToSave(_script);
        Toolbox1.RemoveEditConditions();
    }    
    private void SetToolBoxEvents()
    {
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);      
    }

    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        string _post = (ReportIdV == -1) ? "" : "?ReportId=" + ReportIdV;
        Response.Redirect("ReportWizard.aspx" + _post);
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.NoRecordDisplay) + "');", true);
            return;
        }
   //     PrintHelper.PrintWebControl(_GridView, dataV);
        DataTable data = GetDataTableForExport(dataV);
        Session["data_print"] = data;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.NoRecordExport) + "');", true);
            return;
        }

        ToExcel to_excel = new ToExcel(this, "WizardReport");
        DataTable data = GetDataTableForExport(dataV);
        if (!to_excel.ExecExcel(data))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    private void LoadReport(int reportId)
    {        
        List<string> listColumns = new List<string>();
        List<KeyValuePair<string, eWizardSelectType>> TranslateColumns = new List<KeyValuePair<string, eWizardSelectType>>();
        string _sort = string.Empty;
        bool select_sort = true;
        bool IsGroupBy = false;
        string name = string.Empty;
        CultureInfo ci = CultureInfo.CreateSpecificCulture("en-US");        
        List<WebReferenceReports.Filter> listFilters = new List<WebReferenceReports.Filter>();
        string command = "EXEC dbo.GetReportFiltersByReportId @ReportId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            SqlCommand cmd = new SqlCommand(command, conn);
            conn.Open();
            cmd.Parameters.AddWithValue("@ReportId", reportId);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                WebReferenceReports.Filter _filter = new WebReferenceReports.Filter();
                _filter.FilterField = (string)reader["FilterName"];
                _filter.Operator = (string)reader["OperatorName"];
                string filter_type = (string)reader["FilterType"];
                string _value = (string)reader["Value"];
                if (filter_type == "Date")
                    _filter.Value = DateTime.Parse(_value, ci);
                else if (filter_type == "Bool")
                    _filter.Value = (_value == eYesNo.Yes.ToString());
                else
                    _filter.Value = _value;
                listFilters.Add(_filter);
            }
            reader.Dispose();
            cmd.Dispose();

            command = "EXEC dbo.GetReportColumnsByReportId @ReportId";
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ReportId", reportId);
            reader = cmd.ExecuteReader();
            
            
            while (reader.Read())
            {
                string _name = (string)reader["Name"];
                string page_name = (string)reader["PageName"];
                listColumns.Add(_name);
                IsGroupBy = (bool)reader["IsGroupBy"];

                string _key = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, page_name, _name);
                eWizardSelectType _val;
                if (reader.IsDBNull(6))
                    _val = eWizardSelectType.None;
                else
                {
                    string _type = (string)reader["WizardSelectType"];
                    Enum.TryParse(_type, out _val);
                }
                KeyValuePair<string, eWizardSelectType> kvp = new KeyValuePair<string, eWizardSelectType>(_key, _val);
                TranslateColumns.Add(kvp);
                if (!reader.IsDBNull(2))
                {
                    select_sort = ((string)reader["SortSymbol"] == "DESC");
                    _sort = _name;
                }
            }
            reader.Dispose();
            cmd.Dispose();


            command = "EXEC dbo.GetWizardReportDetails @ReportId";
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ReportId", reportId);
            reader = cmd.ExecuteReader();
            
            if (reader.Read())
            {
                name = (string)reader["IssueName"];
            }
            conn.Close();
        }
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.WizardReportRequest _request = new WebReferenceReports.WizardReportRequest();
        
        _request.Columns = listColumns.ToArray();
        _request.Filters = listFilters.ToArray();
        if (IsGroupBy)
        {
            _request.GroupBy = listColumns[0];
            TranslateColumns.Add(new KeyValuePair<string, eWizardSelectType>("Count", eWizardSelectType.None));
        }
        WebReferenceReports.Issues _issue;
        if (!Enum.TryParse(name, out _issue))
            return;

        _request.Issue = _issue;
        if (!string.IsNullOrEmpty(_sort))
        {
            _request.OrderBy = _sort;
            _request.Descending = select_sort;
        }

        WebReferenceReports.ResultOfListOfListOfString result;
        try
        {
            result = _report.WizardReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClientScript.RegisterStartupScript(this.GetType(), "Failed", "top.UpdateFailed();_redirect('" + reportId + "');", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Failed", "top.UpdateFailed();_redirect('" + reportId + "');", true);
            return;
        }
        DataTable data = GetDataTable.GetdatatTableFromArrayTwoDimensional(result.Value, TranslateColumns);
        dataV = data;
        _GridView.DataSource = data;
        lbl_RecordMached.Text = Toolbox1.GetRecordMaches(data.Rows.Count);
        _GridView.DataBind();
        if (_GridView.Rows.Count == 0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.NoRecordDisplay) + "');_redirect('" + reportId + "');", true);
        _UpdatePanel.Update();
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataTable data = dataV;
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    protected void btn_Save_click(object sender, EventArgs e)
    {
        string name = txt_reportName.Text.Trim();
        string description = txt_description.Text.Trim();
        string command = "EXEC dbo.SetWizardReportsFixed @ReportId, " +
                    "@ReportName, @ReportDescription";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@ReportId", ReportIdV);
            cmd.Parameters.AddWithValue("@ReportName", name);
            cmd.Parameters.AddWithValue("@ReportDescription", description);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
//        Response.Redirect("SavedReport.aspx");
        ClientScript.RegisterStartupScript(this.GetType(), "CreateSuccessfully", "ReportSaved();", true);
    }
    protected void _GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow _row = e.Row;
        SetRowGridView(_row);
    }
    void SetRowGridView(GridViewRow _row)
    {
        if (_row.RowType != DataControlRowType.DataRow)
            return;

        foreach (TableCell tc in _row.Cells)
        {
            if (!tc.Text.Contains(";;;"))
                continue;
            string[] strs = tc.Text.Split(new string[] { ";;;" }, StringSplitOptions.None);
            if (strs.Count() != 3)
                continue;
            LinkButton lb = new LinkButton();
            lb.Text = strs[0];
            lb.CommandArgument = strs[1];
            lb.CommandName = strs[2];
            lb.Click += new EventHandler(lb_Click);
    //        lb.ID = strs[0];
            tc.Controls.Add(lb);
        }
    }
    DataTable GetDataTableForExport(DataTable data)
    {

        DataTable NewData = data.Clone();        
        foreach (DataRow row in data.Rows)
        {
            DataRow NewRow = NewData.NewRow();
            for (int i = 0; i < data.Columns.Count; i++)
            {
                string str = row[i].ToString();
                if (str.Contains(";;;"))
                {
                    string[] strs = str.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    if (strs.Length == 3)
                        str = strs[0];
                }
                NewRow[i] = str;
            }
            NewData.Rows.Add(NewRow);
        }
        return NewData;
    }
    void lb_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        if (lb.CommandName == eWizardSelectType.Advertiser.ToString())
        {
            string name = lb.Text;
            string _id = lb.CommandArgument;
            UserMangement um = new UserMangement(_id, name);
            SetGuidSetting(um);
            string csnameStep = "Openadvertiser";
            string csTextStep = "$(document).ready(function(){$.colorbox({href:'framepublisher.aspx',width:'95%', height:'90%', iframe:true});});";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), csnameStep, csTextStep, true);
        }
        else if (lb.CommandName == eWizardSelectType.Consumer.ToString())
        {
            string url = "ConsumerDetails.aspx?Consumer=" + lb.CommandArgument;
            string script = @"$(document).ready(function(){$.colorbox({href:'" + url + @"',width:'1000px', height:'90%', iframe:true});});";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "OpenConsumer", script, true);
        }
    }
    DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    int ReportIdV
    {
        get { return (ViewState["ReportId"] == null) ? -1 : (int)ViewState["ReportId"]; }
        set { ViewState["ReportId"] = value; }
    }

   
}
