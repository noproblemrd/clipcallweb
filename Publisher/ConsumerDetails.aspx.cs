using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.Text;

public partial class Publisher_ConsumerDetails : PageSetting, IPostBackEventHandler
{
    protected const int ITEM_PAGE = 10;
    protected const int PAGE_PAGES = 10;
    protected const int ITEM_PAGE_RECORD = 20;

    private const string RECORD_KEY = "RecordCalls";
    const string TAB_CLICK = "TabClick";
  //  protected string _tab_click;
    public string Get_tab_click(int num)
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(this, num.ToString());
        myPostBackOptions.PerformValidation = false;

        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);      
        
    }
    
    #region IPostBackEventHandler Members

    void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
    {
        tab_click(eventArgument);
    }

    #endregion
    
    protected override void Render(HtmlTextWriter writer)
   {
       Page.ClientScript.RegisterForEventValidation(this.UniqueID, "1");
       Page.ClientScript.RegisterForEventValidation(this.UniqueID, "2");
       Page.ClientScript.RegisterForEventValidation(this.UniqueID, "3");
       Page.ClientScript.RegisterForEventValidation(this.UniqueID, "4");         
       base.Render(writer);     
    }
 
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(lb_ConsumerGeneralInfo);
        ScriptManager1.RegisterAsyncPostBackControl(lb_Requests);
        ScriptManager1.RegisterAsyncPostBackControl(lb_Notes);
        
        
        if (!IsPostBack)
        {            
            if (!userManagement.IsPublisher())
            {
                Response.Redirect(ResolveUrl("~") + @"Publisher/LogOut.aspx");                
                return;
            }

            dicV = null;
            
            _MultiView.ActiveViewIndex = 0;
            string _id = Request["Consumer"];
            if (!Utilities.IsGUID(_id))
            {                
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "errorGuid", "CloseIframe('" + hf_errorId.Value + "');", true);
                return;
            }
            Guid _guid = new Guid(_id);           
            ConsumerIdV = _guid;
            LoadGeneralInfo(_guid);
            LoadAllowedToRecord();
            lb_ConsumerGeneralInfo.Attributes.Add("class", "active");
            RegularExpressionValidatorMobilePhone.ValidationExpression = siteSetting.GetPhoneRegularExpression;
            RegularExpressionValidatorPhone2.ValidationExpression = siteSetting.GetPhoneRegularExpression;
            LoadHelpDeskTypes();
        }
        else
        {

            Dictionary<int, string> dic = PageListCommentV;
            foreach (KeyValuePair<int, string> kvp in dic)
                PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
            
            dic = PageListRecordV;
            foreach (KeyValuePair<int, string> kvp in dic)
                PlaceHolderPagesRecord.Controls.Add(MakeCellRecord(kvp.Key, kvp.Value));
            
            
        }
        Page.Header.DataBind();
    }

    private void LoadHelpDeskTypes()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = new WebReferenceSite.ResultOfListOfGuidStringPair();
        try
        {
            result = _site.GetAllHelpDeskTypes();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            if (gsp.Name == "Complaint")
            {
                HelpDeskTypeV = gsp.Id;
                break;
            }
        }
    }
    private void LoadAllowedToRecord()
    {
        string result = string.Empty;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        try
        {
            result = _site.GetConfigurationSettings();
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["Configurations"] == null || xdd["Configurations"]["Error"] != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        foreach (XmlNode node in xdd["Configurations"].ChildNodes)
        {
            if (node["Key"].InnerText == RECORD_KEY)
            {
                AllowedToRecord = (node["Value"].InnerText == "1");
                break;
            }
        }
    }
    
    protected void tab_click(string _tab)
    {
        
        lb_ConsumerGeneralInfo.Attributes.Remove("class");
        lb_Requests.Attributes.Remove("class");
        lb_Complaints.Attributes.Remove("class");
        lb_Notes.Attributes.Add("class", "last");

        int tabIndex = int.Parse(_tab) - 1;
        switch (tabIndex)
        {
            case (0): LoadGeneralInfo(InfoResponseV);
                lb_ConsumerGeneralInfo.Attributes.Add("class", "active"); 
                goto default;
            case (1): LoadRequests();
                lb_Requests.Attributes.Add("class", "active"); 
                goto default;
            case (2): LoadComplaints();
                lb_Complaints.Attributes.Add("class", "active");
                goto default;
            case (3): LoadDetails();
                lb_Notes.Attributes.Add("class", "active"); 
                goto default;
            default: _MultiView.ActiveViewIndex = tabIndex;
                break;
        }
        _UpdatePanel.Update();
        _UpdatePanelNavigat.Update();
    }
    
    #region Tab1
    void LoadGeneralInfo(Guid _id)
    {
        WebReferenceCustomer.CustomersService _cust = WebServiceConfig.GetCustomerReference(this);
        WebReferenceCustomer.GetConsumerInfoRequest _request = new WebReferenceCustomer.GetConsumerInfoRequest();
        _request.ContactId = _id;
        WebReferenceCustomer.ResultOfGetConsumerInfoResponse result = null;
        try
        {
            result = _cust.GetConsumerInfo(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        LoadGeneralInfo(result.Value);
        lbl_consume_name.Text = result.Value.FirstName;
        InfoResponseV = result.Value;


    }
    void LoadGeneralInfo(WebReferenceCustomer.GetConsumerInfoResponse _response)
    {

        txt_Fname.Text = _response.FirstName;
        txt_Lname.Text = _response.LastName;
        txt_MobilePhone.Text = _response.MobilePhone;
        txt_phone2.Text = _response.PhoneNumber1;
        txt_email.Text = _response.Email;
        txt_numRequest.Text = _response.NumberOfRequests.ToString();
        cb_BlackList.Checked = _response.IsBlackList;
    }
    protected void btn_update_OnClick(object sender, EventArgs e)
    {
        WebReferenceCustomer.CustomersService _cust = WebServiceConfig.GetCustomerReference(this);
        WebReferenceCustomer.UpdateConsumerInfoRequest _request = new WebReferenceCustomer.UpdateConsumerInfoRequest();
        _request.ContactId = ConsumerIdV;
        _request.Email = txt_email.Text;
        _request.FirstName = txt_Fname.Text;
        _request.LastName = txt_Lname.Text;
        _request.MobilePhone = txt_MobilePhone.Text;
        _request.PhoneNumber1 = txt_phone2.Text;
        _request.IsBlackList = cb_BlackList.Checked;
        WebReferenceCustomer.Result result = null;
        try
        {
            result = _cust.UpdateConsumerInfo(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        WebReferenceCustomer.GetConsumerInfoResponse temp = InfoResponseV;
        temp.Email = _request.Email;
        temp.FirstName = _request.FirstName;
        temp.LastName = _request.LastName;
        temp.MobilePhone = _request.MobilePhone;
        temp.MobilePhone = _request.MobilePhone;
        temp.PhoneNumber1 = _request.PhoneNumber1;
        _UpdatePanel.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        InfoResponseV = temp;

    }
    #endregion
    #region Tab2
    void LoadRequests()
    {
        DataTable data = dataV;
        if (data == null)
        {
            WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(this);
            WebReferenceCustomer.GetCustomerLeadsRequest request = new WebReferenceCustomer.GetCustomerLeadsRequest();
            request.CustomerId = ConsumerIdV;
            WebReferenceCustomer.ResultOfGetConsumerIncidentsResponse _response = null;
            try
            {
                _response = _customer.GetCustomerIncidents(request);
            }
            catch (Exception ex)
            {
                dbug_log.ExceptionLog(ex, siteSetting);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
            if (_response.Type == WebReferenceCustomer.eResultType.Failure)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
   //         data = GetDataTable.GetDataTableFromList(_response.Value.ConsumerIncidents);
            data = GetDataTable.GetDataTableFromListCorrectNumber(_response.Value.ConsumerIncidents);
        }
        BindDataRecord(data);
        dataV = data;

    }
    protected void ib_open_Click(object sender, EventArgs e)
    {
        LinkButton ib = (LinkButton)sender;
        RepeaterItem item = (RepeaterItem)ib.NamingContainer;
        HtmlTableCell htr = (HtmlTableCell)item.FindControl("tr_OpenDetails");
        HtmlTableCell htc = (HtmlTableCell)item.FindControl("td_openClose");
        HiddenField hf = (HiddenField)item.FindControl("hf_IfClose");
        if (hf.Value == "true")
        {
            htr.Attributes.Add("style", "display:none");
            htc.Attributes.Add("class", "close first");
            _UpdatePanel.Update();
            hf.Value = "false";
            return;
        }
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(this);
        WebReferenceCustomer.GetConsumerIncidentDataRequest _request = new WebReferenceCustomer.GetConsumerIncidentDataRequest();
        _request.LeadId = new Guid(ib.CommandArgument);
        _request.CustomerId = userManagement.Get_Guid;
        WebReferenceCustomer.ResultOfGetConsumerIncidentDataResponse result = null;
        try
        {
            result = _customer.GetConsumerIncidentData(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Random rnd = new Random();
        Dictionary<int, string> dic = dicV;
        DataTable data = new DataTable();
        data.Columns.Add("RecordFileLocation");
        data.Columns.Add("HasRecord", typeof(bool));
        data.Columns.Add("SupplierName");
        data.Columns.Add("SupplierId");
        data.Columns.Add("ImageUrl");
        data.Columns.Add("Complaint", typeof(string));
        foreach (WebReferenceCustomer.ConsumerIncidentAccountData _incident in result.Value.IncidentAccounts)
        {
            DataRow row = data.NewRow();
            if (string.IsNullOrEmpty(_incident.RecordFileLocation))
                row["HasRecord"] = false;
            else
            {
                int _index;
                do
                {
                    _index = rnd.Next();
                } while (dic.ContainsKey(_index));
                row["HasRecord"] = true;
                if (_incident.AllowedToHear)
                {
                    string command = "javascript:OpenRecord('" + _index + "');";
                    row["RecordFileLocation"] = command;
                    dic.Add(_index, _incident.RecordFileLocation);
                    row["ImageUrl"] = @"../Management/calls_images/icon_speaker.png";
                }
                else
                {
                    row["ImageUrl"] = @"../Management/calls_images/icon_speakeroff.png";
                    row["RecordFileLocation"] = string.Empty;
                }
            }
            row["SupplierName"] = _incident.SupplierName;
            row["SupplierId"] = _incident.SupplierId;
            row["Complaint"] = "javascript:OpenIframe('Ticket.aspx?HelpDesk=" + HelpDeskTypeV.ToString() +
                "&Consumer=" + ConsumerIdV.ToString() + "&Call=" + _incident.LeadAccountId.ToString() + "');";//+"&Ticket=" +
            //          row["Complaint"] = "Ticket.aspx?HelpDesk=" + HelpDeskTypeV.ToString();
            data.Rows.Add(row);
        }
        GridView gv = (GridView)item.FindControl("_GridViewIn");
        gv.DataSource = data;
        gv.DataBind();
        foreach (DataControlField dcf in gv.Columns)
        {
            if (dcf.SortExpression == "Records")
            {
                dcf.Visible = AllowedToRecord;
                break;
            }
        }
        htr.Attributes.Add("style", "display:table-cell");
        htc.Attributes.Add("class", "open first");
        hf.Value = "true";
        _UpdatePanel.Update();
        dicV = dic;
    }
    #region Record pagging
    protected void BindDataRecord(DataTable data)
    {
        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;
        objPDS.PageSize = ITEM_PAGE_RECORD;
        objPDS.DataSource = data.DefaultView;
        
        if (data.Rows.Count > 0)
        {

            objPDS.CurrentPageIndex = CurrentPageRecord;
            lnkPreviousPageRecord.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkNextPageRecord.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkPreviousPageRecord.Enabled = !objPDS.IsFirstPage;
            lnkNextPageRecord.Enabled = !objPDS.IsLastPage;
            LoadPagesRecord(data.Rows.Count);
            _reapeter.DataSource = objPDS;
            _reapeter.DataBind();

            div_pagingRecord.Visible = true;
            //       PanelResults.Visible = true;
        }
        else
        {
            div_pagingRecord.Visible = false;
            _reapeter.DataSource = null;
            _reapeter.DataBind();
            //         _PanelNoReasult.Visible = true;
            //         PanelResults.Visible = false;
        }
        _UpdatePanel.Update();
    }
    void LoadPagesRecord(int countRows)
    {
        PlaceHolderPagesRecord.Controls.Clear();
        int countPages = countRows / ITEM_PAGE_RECORD;
        countPages += (countRows % ITEM_PAGE_RECORD == 0) ? 0 : 1;

        //      Dictionary<string, int> dic = new Dictionary<string, int>();
        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListRecordV = null;
            return;
        }
        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPageRecord + 1;
        int start = (((int)(CurrentPageRecord / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }

        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListRecordV = dic;
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPagesRecord.Controls.Add(MakeCellRecord(kvp.Key, kvp.Value));
    }
    protected void lnkPageRecord_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPageRecord = cp - 1;
        BindDataRecord(dataV);
    }
    HtmlTableCell MakeCellRecord(int indx, string _item)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_Record_" + indx;
        lb.Text = _item;
        if (indx != CurrentPageRecord + 1)
        {
            lb.Click += new EventHandler(lnkPageRecord_Click);
            lb.OnClientClick = "showDiv();";
        }
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();
        htc.Controls.Add(lb);
        htc.Attributes.Add("class", "td_num_pages");
        return htc;
    }
    protected void lnkNextPageRecord_Click(object sender, EventArgs e)
    {
        int cp = CurrentPageRecord;
        cp++;
        CurrentPageRecord = cp;

        BindDataRecord(dataV);
    }

    protected void lnkPreviousPageRecord_Click(object sender, EventArgs e)
    {
        int cp = CurrentPageRecord;
        cp--;
        CurrentPageRecord = cp;

        BindDataRecord(dataV);
    }
    Dictionary<int, string> PageListRecordV
    {
        get
        {
            return (ViewState["PageListRecord"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageListRecord"];
        }
        set { ViewState["PageListRecord"] = value; }
    }
    int CurrentPageRecord
    {
        get
        {
            //Look for current page in ViewState 
            object o = this.ViewState["CurrentPageRecord"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["CurrentPageRecord"] = value;
        }
    }
    #endregion
    #endregion
    #region Tab3
    void LoadComplaints()
    {
        LoadComplaintsStatus();
        _GridView_complaints.PageIndex = 0;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.GetHdEntriesRequest _request = new WebReferenceSite.GetHdEntriesRequest();
        _request.HdTypeId = HelpDeskTypeV;
        _request.InitiatorId = ConsumerIdV;
       
        WebReferenceSite.ResultOfListOfHdEntryData result = new WebReferenceSite.ResultOfListOfHdEntryData();
        try
        {
            result = _site.GetHelpDeskEntries(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        DataTable data = new DataTable();
        data.Columns.Add("ID", typeof(string));
        data.Columns.Add("guid", typeof(Guid));
        data.Columns.Add("CreatedOn", typeof(string));
        data.Columns.Add("Title", typeof(string));
        data.Columns.Add("Status", typeof(string));
        data.Columns.Add("StatusId", typeof(Guid));
        data.Columns.Add("FollowUp", typeof(string));
        data.Columns.Add("Advertiser", typeof(string));
        data.Columns.Add("ScriptComplaint", typeof(string));
        data.Columns.Add("Advertiser_script", typeof(string));

        foreach (WebReferenceSite.HdEntryData hed in result.Value)
        {
            DataRow row = data.NewRow();
            row["ID"] = hed.TicketNumber;
            row["guid"] = hed.Id;
            row["CreatedOn"] = string.Format(siteSetting.DateFormat, hed.CreatedOn);
            row["Title"] = hed.Title;
            row["Advertiser"] = hed.SecondaryRegardingObject;
            row["StatusId"] = hed.StatusId;
            string adv_script = "OpenAdvertiserIframeParent('framepublisher.aspx?UserId=" +
                hed.SecondaryRegardingObjectId.ToString() + "&UserName=" + 
                Server.HtmlEncode(hed.SecondaryRegardingObject) + "'); return false;";
      //      row["AdvertiserId"] = hed.SecondaryRegardingObjectId.ToString();
            row["Advertiser_script"] = adv_script;
            string _status = string.Empty;
            foreach (KeyValuePair<Guid, string> kvp in complaints_statusV)
            {
                if (kvp.Key == hed.StatusId)
                {
                    _status = kvp.Value;
                    break;
                }
            }
            row["Status"] = _status;
            if (hed.FollowUp != null)
                row["FollowUp"] = string.Format(siteSetting.DateFormat, hed.FollowUp);

            string _script = "OpenIframe('Ticket.aspx?Ticket=" + hed.TicketNumber /*hed.Id.ToString()*/ +
                "&HelpDesk=" + HelpDeskTypeV.ToString() + "'); return false;";
            row["ScriptComplaint"] = _script;
            data.Rows.Add(row);
        }
        //      _GridView.DataSource = data;
        //       _GridView.DataBind();
        _GridView_complaints.DataSource = data;
        _GridView_complaints.DataBind();
        _UpdatePanel.Update();
        data_complaintsV = data;
    }
    protected void _GridView_complaints_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView_complaints.PageIndex = e.NewPageIndex;
        _GridView_complaints.DataSource = data_complaintsV;
        _GridView_complaints.DataBind();
        _UpdatePanel.Update();
    }
    private void LoadComplaintsStatus()
    {
        if (complaints_statusV != null)
            return;
        Dictionary<Guid, string> dic = new Dictionary<Guid, string>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfSeverityAndStatusContainer result = new WebReferenceSite.ResultOfSeverityAndStatusContainer();
        try
        {
            result = _site.GetAllHelpDeskStatusesAndSeverities(HelpDeskTypeV);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }        
        foreach (WebReferenceSite.HdStatusData hsd in result.Value.StatusList)
        {
            dic.Add(hsd.Id, hsd.Name);
        }
        complaints_statusV = dic;
    }
    Dictionary<Guid, string> complaints_statusV
    {
        get { return (ViewState["complaints_status"] == null) ? null : (Dictionary<Guid, string>)ViewState["complaints_status"]; }
        set { ViewState["complaints_status"] = value; }
    }
    DataTable data_complaintsV
    {
        get { return (ViewState["data_complaints"] == null) ? new DataTable() : (DataTable)ViewState["data_complaints"]; }
        set { ViewState["data_complaints"] = value; }
    }
    #endregion
    #region Tab4

    void LoadDetails()
    {
        DataTable data = dataCommentV;
        if (data == null)
        {
            WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(this);
            WebReferenceCustomer.GetConsumerNotesRequest _request = new WebReferenceCustomer.GetConsumerNotesRequest();
            _request.ContactId = ConsumerIdV;
            WebReferenceCustomer.ResultOfGetConsumerNotesResponse result = null;
            try
            {
                result = _customer.GetConsumerNotes(_request);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
            if (result.Type == WebReferenceCustomer.eResultType.Failure)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return;
            }
            data = GetDataTable.GetDataTableFromList(result.Value.Notes);
        }
        //      dlComments.DataSource = data;
        //     dlComments.DataBind();
        BindData(data);
        dataCommentV = data;

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(this);
        WebReferenceCustomer.CreateConsumerNoteRequest _request = new WebReferenceCustomer.CreateConsumerNoteRequest();
        _request.Description = txtInsertComment.Text;
        _request.UserId = new Guid(userManagement.GetGuid);
        _request.ContactId = ConsumerIdV;
        WebReferenceCustomer.Result result = null;
        try
        {
            result = _customer.CreateConsumerNote(_request);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        dataCommentV = null;
        LoadDetails();
    }
    #region Note pagging
    protected void BindData(DataTable data)
    {
        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;
        objPDS.PageSize = ITEM_PAGE;
        objPDS.DataSource = data.DefaultView;

        if (data.Rows.Count > 0)
        {

            objPDS.CurrentPageIndex = CurrentPage;
            lnkPreviousPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkNextPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            LoadPages(data.Rows.Count);
            dlComments.DataSource = objPDS;
            dlComments.DataBind();

            div_paging.Visible = true;
        }
        else
        {
            div_paging.Visible = false;
            dlComments.DataSource = null;
            dlComments.DataBind();
        }
        _UpdatePanel.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "HideInsert", "HideInsert();", true);
    }
    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListCommentV = null;
            return;
        }

        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp = CurrentPage + 1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES) + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListCommentV = dic;
        foreach (KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
    }
    HtmlTableCell MakeCell(int indx, string _item)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
        {
            lb.Click += new EventHandler(lnkPage_Click);
            lb.OnClientClick = "showDiv();";
        }
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
        HtmlTableCell htc = new HtmlTableCell();
        htc.Controls.Add(lb);
        return htc;
    }
    protected void lnkPage_Click(object sender, EventArgs e)
    {
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
        BindData(dataCommentV);
    }
    protected void lnkNextPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;

        BindData(dataCommentV);
    }
    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;

        BindData(dataCommentV);
    }

    DataTable dataCommentV
    {
        get { return (ViewState["dataComment"] == null) ? null : (DataTable)ViewState["dataComment"]; }
        set { ViewState["dataComment"] = value; }
    }
    int CurrentPage
    {
        get
        {
            //Look for current page in ViewState 
            object o = this.ViewState["CurrentPage"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["CurrentPage"] = value;
        }
    }
    Dictionary<int, string> PageListCommentV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }
    #endregion
    #endregion

    Guid ConsumerIdV
    {
        get { return (ViewState["consumerId"] == null) ? Guid.Empty : (Guid)ViewState["consumerId"]; }
        set { ViewState["consumerId"] = value; }
    }
    WebReferenceCustomer.GetConsumerInfoResponse  InfoResponseV
    {
        get { return (ViewState["InfoRespons"] == null) ? null :
      (WebReferenceCustomer.GetConsumerInfoResponse)ViewState["InfoRespons"];
    }
        set { ViewState["InfoRespons"] = value; }
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? null : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    
    Dictionary<int, string> dicV
    {
        get { return (Session["RecordList"] == null) ? new Dictionary<int, string>() : (Dictionary<int, string>)Session["RecordList"]; }
        set { Session["RecordList"] = value; }
    }
    bool AllowedToRecord
    {
        get { return (ViewState["AllowedToRecord"] == null) ? false : (bool)ViewState["AllowedToRecord"]; }
        set { ViewState["AllowedToRecord"] = value; }
    }
    protected string GetAudioChrome
    {
        get { return ResolveUrl("~") + "Management/AudioChrome.aspx"; }
    }
    Guid HelpDeskTypeV
    {
        get { return (ViewState["HelpDeskType"] == null) ? new Guid() : (Guid)ViewState["HelpDeskType"]; }
        set { ViewState["HelpDeskType"] = value; }
    }
}
