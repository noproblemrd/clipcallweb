﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_AdEngineAppUpsert : PageSetting
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (this.siteSetting == null || string.IsNullOrEmpty(this.siteSetting.GetSiteID))
            Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(lb_Add);
        if(!IsPostBack)
        {
            LoadCampaigns();
        }
    }

    private void LoadCampaigns()
    {
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("ID");
        data.Columns.Add("URL");
        data.Columns.Add("Width");
        data.Columns.Add("Height");

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfAdEngineCampaignData result = null;
        try
        {
            result = _site.GetAllAdEngineCampaigns();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        foreach (WebReferenceSite.AdEngineCampaignData cd in result.Value)
        {
            DataRow row = data.NewRow();
            row["Name"] = cd.Name;
            row["ID"] = cd.Id;
            row["URL"] = cd.URL;
            row["Width"] = cd.Width;
            row["Height"] = cd.Height;
            data.Rows.Add(row);
        }
        _GridView.DataSource = data;
        _GridView.DataBind();
    }
    protected void lb_Add_Click(object sender, EventArgs e)
    {

    }
    protected void _GridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}