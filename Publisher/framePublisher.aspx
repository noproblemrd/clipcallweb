<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Publisher/MasterPageFrame.master"
CodeFile="framePublisher.aspx.cs" Inherits="Publisher_framePublisher" %>

<%@ MasterType VirtualPath="~/Publisher/MasterPageFrame.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <!--<link href="style.css" rel="stylesheet" type="text/css"/>-->
<!--     <link href="../Management/style.css" rel="stylesheet" type="text/css"/>-->
<link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
        <script type="text/javascript" src="../general.js"></script>
   <!--     <script type="text/javascript" src="scripts/FramePublisher.js"></script>-->
<!--[if IE 6]><link href="style-ie6.css" rel="stylesheet" type="text/css"/><![endif]-->
<style type="text/css" xml:space>

</style>
<script type="text/javascript">
   // window.onload=init;
//    var _liNumber = 7;
    var _liNumber = 0;
    var arrStepsNames=new Array();
/*
 function ifhere()
 {
    alert("jjjjjjj");
 }
 */
    function init()
    {        
        arrStepsNames[0]=""; 
        arrStepsNames[1]= "<%# Get_GeneralInfo %>";
        arrStepsNames[2]= "<%# GetSegmentsArea %>";
        arrStepsNames[3]= "<%# GetCities %>";
        arrStepsNames[4]= "<%# Get_Time %>"; 
        arrStepsNames[5]= "<%# Get_MyCredits %>";
        arrStepsNames[6]= "<%# GetPaymentInfo %>";//"<%#Hidden_PaymentInfo.Value%>"; 
        arrStepsNames[7]="<%# Get_PublisherNotes %>"; 
        arrStepsNames[8]="<%# Get_Audit %>"; 
        arrStepsNames[9]="<%# Get_VirtualNumbers %>"; 
    }
    function setUserNameSetting(name)
    {
        var labelUserName = '<%# (Master.FindControl("lblUserName")).ClientID %>';
        document.getElementById(labelUserName).innerHTML=name;
    }     
      
    
    /*
    arrStepsNames[0]=""; 
     arrStepsNames[1]= '<=Hidden_GeneralInfo.Value%>';
    arrStepsNames[2]= '<=Hidden_SegmentsArea.Value%>';
    arrStepsNames[3]= '<=Hidden_Cities.Value%>';
    arrStepsNames[4]= '<Hidden_Time.Value%>'; 
    arrStepsNames[5]= '<=Hidden_MyCredits.Value%>';
    arrStepsNames[6]= '<=Hidden_PaymentInfo.Value%>'; 
    arrStepsNames[7]='<=Hidden_PublisherNotes.Value%>'; 
    arrStepsNames[8]='<=Hidden_Audit.Value%>'; 
    arrStepsNames[9]='<=Hidden_VirtualNumbers.Value%>'; 
    
    */
    function witchAlreadyStep(level)
    {      
        if(arrStepsNames.length==0)
            init();
   //     alert('po');
        var steps=document.getElementById("ulSteps").getElementsByTagName("li");
        
        var alreadyStep=level+1;
        for(var i=0; i<steps.length;i++)
        {
    
                var liNumber=i+1;
     
                if(liNumber < _liNumber)
                {
                   
                    if(liNumber<=alreadyStep)
                    {      
                               
                       steps[i].innerHTML="<a id='linkStep" +
                        liNumber + "' href='javascript:void(0);' style='cursor:hand;' class='mouseover' onclick='javascript:step" +
                         liNumber + "();'><span class='mouseover' >" + liNumber + 
                         "</span><span class='titleNavigation' style='display:inline-block;width:80px;'>" + arrStepsNames[liNumber]+ "</span></a>";
                    }    
                    else
                    {
                       steps[i].innerHTML="<a id='linkStep" +
                        liNumber + "' href='javascript:void(0);' style='cursor:text;'  onclick='javascript:return false;'><span class='mouseover' >" + liNumber + 
                         "</span><span class='titleNavigation'>" + arrStepsNames[liNumber]+ "</span></a>";
                    } 
                    
                    /*
                                       listStepObj.innerHTML="<a id='linkStep" +
                    liNumber + "' href='javascript:void(0);' style='cursor:hand;' class='mouseover' onclick='javascript:step" +
                     liNumber + "();'><span class='mouseover' style='vertical-align:top;'>" + liNumber + 
                     "</span><span style='display:inline-block;width:80px;'>" + arrStepsNames[liNumber]+ "</span></a>";
                    */
                    
                }
                
                else
                {
                       if(level != 0)
                       {
                           steps[i].innerHTML="<a id='linkStep" +
                            liNumber + "' href='javascript:void(0);' style='cursor:hand;' class='mouseover' onclick='javascript:step" +
                             liNumber + "();'><span class='mouseover' >" + liNumber + 
                             "</span><span class='titleNavigation'>" + arrStepsNames[liNumber]+ "</span></a>";
                       }
                       else
                       {
                           steps[i].innerHTML="<a id='linkStep" +
                            liNumber + "' href='javascript:void(0);' style='cursor:text;'  onclick='javascript:return false;'><span class='mouseover' >" + liNumber + 
                             "</span><span class='titleNavigation'>" + arrStepsNames[liNumber]+ "</span></a>";
                       }
                }
                
                               
             
                      
        }          
       
    }
    
    
    function clearStepActive()
    {      
        var steps=document.getElementById("ulSteps").getElementsByTagName("li");
//        alert(steps.length);
        for(i=1;i<steps.length+1;i++)
        {
            
            var linkObj=document.getElementById('linkStep' + i);
  //          alert(linkObj.id+";   "+(linkObj.className=="active mouseover"));
            if(linkObj.className=="active mouseover")
                linkObj.className="mouseover";            
        }     
      
    }
    
    function setInitRegistrationStep(level)
    {       

        switch(level)
        {    
             
           case 1:
             step2();
             break;
             
           case 2:
             step3();
             break;
             
           case 3:
             step4();
             break;
             
           case 4:
             step5();
             break;
             
           case 5:
             step6();
             break;
             
           case 6:
             step1();
             break;    
           
           default:
            break;  
        }
    }
    
    function setLinkStepActive(linkId)
    {    
       
      document.getElementById(linkId).className="active mouseover";      
    }    
   
    function goUp()
    {
       window.scrollTo(0,0); 
    }
    
    function step1()
    {     
        //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src = '<%#ResolveUrl("~")%>Management/professionalDetails.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '1100px';
        // document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow:hidden"); 
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no"); 
    }    
   
    
    function step2()
    {
        //window.scrollTo(0,0);
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/professionChooseProfessions.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '900px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no"); 
    }

   
    function step3() {
        clearStepActive();
        //window.scrollTo(0,0);  
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/<%#cityPage%>';  
        document.getElementById("<%# iframe.ClientID %>").height='900px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow-x:hidden; overflow-y:auto;");
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "auto"); 
    }

    function step4() {
        
        clearStepActive();
        //window.scrollTo(0,0);  
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/professionalDates.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '900px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no"); 
    }
    
    function step5()
    { 
        //window.scrollTo(0,0);             
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Management/professionalCallPurchase.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '900px';
        //     document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow-x:hidden; overflow-y:auto;"); 
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no"); 
        
    }
    
    function step6()
    {
       //window.scrollTo(0,0);               
        document.getElementById("<%# iframe.ClientID %>").src = '<%#ResolveUrl("~")%>Publisher/PublisherPayment.aspx';
       document.getElementById("<%# iframe.ClientID %>").height = '900px';
       //      document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow-x:hidden; overflow-y:auto;");  
       document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no");   
    }  
    
    function step7()
    { 
       
        var __iframe = document.getElementById("<%# iframe.ClientID %>");
        __iframe.src='<%#ResolveUrl("~")%>Publisher/PublisherNotes.aspx';  
        __iframe.height='900px';
    //    document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow-x:hidden; overflow-y:auto;");
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "no"); 
    }
    
    function step8()
    {
        //window.scrollTo(0,0);           
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Publisher/PublisherAudit.aspx';  
        document.getElementById("<%# iframe.ClientID %>").height='1000px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow-x:scroll;");
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "auto"); 
    }
    function step9()
    {
        //window.scrollTo(0,0);           
        document.getElementById("<%# iframe.ClientID %>").src='<%#ResolveUrl("~")%>Publisher/VirtualNumbers.aspx';
        document.getElementById("<%# iframe.ClientID %>").height = '900px';
        document.getElementById("<%# iframe.ClientID %>").setAttribute("style", "overflow-x:scroll;");
        document.getElementById("<%# iframe.ClientID %>").setAttribute("scrolling", "auto"); 
    }


    function closeOpenOverLayIframe(deposite)
    {
        if (document.getElementById("overlayIframePublisher").style.display == 'none' || document.getElementById("overlayIframePublisher").style.display == '') {
            document.getElementById("overlayIframePublisher").style.display = 'block';
            var url = 'addDeposit.aspx';
            if (deposite != null && deposite.length > 0)
                url = url + "?DepositId=" + deposite;
            document.getElementById("iframeOverlay").src = url;
        }
        else {
            document.getElementById("overlayIframePublisher").style.display = 'none';
            document.getElementById("iframeOverlay").src = '';
        }
    }
    function closeOpenOverLayIframeNewDeposite() {
        if (document.getElementById("overlayIframePublisher").style.display == 'none' || document.getElementById("overlayIframePublisher").style.display == '') {
            document.getElementById("overlayIframePublisher").style.display = 'block';
            var url = 'Deposits.aspx';            
            document.getElementById("iframeOverlay").src = url;
        }
        else {
            document.getElementById("overlayIframePublisher").style.display = 'none';
            document.getElementById("iframeOverlay").src = '';
        }
    }
    function closeOverLayIframe() {
        document.getElementById("overlayIframePublisher").style.display = 'none';
        document.getElementById("iframeOverlay").src = '';
        document.getElementById('<%# iframe.ClientID %>').src = document.getElementById('<%# iframe.ClientID %>').src;
        SetAccountStatus();
    }
    function addSourceIframeOverlay(strSource) {
        
    }
    
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<!--------------------------------- Here starts the relevant section -------------------------------->
	
        
        <div id="containerIframes" style="position:relative;">
            
            <div id="overlayIframePublisher" class="overlayIframePublisher popModal_Upsale" >         
                
               <a id="a_CloseUpsaleDetails" runat="server" class="span_A" href="javascript:void(0);" onclick="closeOpenOverLayIframe();"></a>
               <iframe id="iframeOverlay" name="iframeOverlay" frameBorder="0"  class="iframeOverlay"  ALLOWTRANSPARENCY="true"></iframe>
           
            </div>
            

		    <div id="publisher_content" class="contentIframeTabs"  >
				 <div class="iframe-content" >
						
									<!--<iframe src="<%=ResolveUrl("~")%>Management/professionalDetails.aspx" height="600px" width="550px" #width="500px" id="iframe" name="iframe" frameborder="0" scrolling="auto">-->
				<iframe  
					width="550px" id="iframe" name="iframe"  frameborder="0" scrolling="no"
					runat="server" ALLOWTRANSPARENCY="true" class="iframe2">
										
				</iframe>
								
							
						
					</div>
                 <div class="tab-navigation">					    
					    <ul id="ulSteps">
                            
							<li id="liStep1" runat="server" ></li>							
							
							<li id="liStep2"></li>
                            
                            <li id="liStep3"></li>
							 
							<li id="liStep4"></li>
							
							<li id="liStep5"></li>
							
							<li id="liStep6"></li>
							
							<li id="liStep7"></li>
							
							<li id="liStep8"></li>
							
							<li id="liStep9" class="last"></li>
							
						</ul>
					    
					    
					</div>
		    </div> 			
		</div>
        

       

       
        
       
         		
		<div>
            <asp:HiddenField ID="Hidden_GeneralInfo" runat="server" Value="General Info" />
            <asp:HiddenField ID="Hidden_SegmentsArea" runat="server" Value="Heading" />
            <asp:HiddenField ID="Hidden_Cities" runat="server" Value="Working Area" />
            <asp:HiddenField ID="Hidden_Time" runat="server"  Value="Availability Schedule"/>
            <asp:HiddenField ID="Hidden_MyCredits" runat="server" Value="My credits" />
            <asp:HiddenField ID="Hidden_PaymentInfo" runat="server" Value="Payment Info" />
            <asp:HiddenField ID="Hidden_PublisherNotes" runat="server" Value="Publisher notes" />
            <asp:HiddenField ID="Hidden_Audit" runat="server" Value="Audit" />
            <asp:HiddenField ID="Hidden_VirtualNumbers" runat="server" Value="Virtual numbers" />
		</div>
		
                
  </asp:Content>