﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallInvitationFunnelReport : PageSetting
{
    const int ONE_DAY = 1;
    protected readonly string SessionTableName = "InvitationFunnelReportData";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
            LoadInvitationSource();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        WebReferenceReports.InvitationFunnelReportRequest _request = new WebReferenceReports.InvitationFunnelReportRequest();
        WebReferenceReports.Reports reports = WebServiceConfig.GetReportsReference(this);
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo.AddDays(1);
        WebReferenceReports.eCreateInvitationBy cib;
        if (!Enum.TryParse(ddl_InvitationSource.SelectedValue, out cib))
            cib = WebReferenceReports.eCreateInvitationBy.ALL;
        _request.createInvitationBy = cib;
        WebReferenceReports.ResultOfInvitationFunnelReportResponse result = null;
        try
        {
            result = reports.InvitationFunnelReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if(result.Value == null)
        {
            _GridView.DataSource = null;
            _GridView.DataBind();
            dataV = null;
            return;
        }
        DataTable data = new DataTable();
        data.Columns.Add("InvitesClicks");
        data.Columns.Add("IosSentSms");
        data.Columns.Add("AndroidSentSms");
        data.Columns.Add("WebSentSms");
        data.Columns.Add("LinkClicks");
        data.Columns.Add("Installs");
        data.Columns.Add("Video");
        data.Columns.Add("Payments");

        DataRow row = data.NewRow();
        row["AndroidSentSms"] = result.Value.AndroidSentSms;
        row["Installs"] = result.Value.Installs;
        row["InvitesClicks"] = result.Value.Invites;
        row["IosSentSms"] = result.Value.IosSentSms;
        row["LinkClicks"] = result.Value.LinkClicks;
        row["Payments"] = result.Value.Payments;
    //    row["SentSms"] = result.Value.SentSms;
        row["Video"] = result.Value.Video;
        row["WebSentSms"] = result.Value.WebSentSms;
        data.Rows.Add(row);

        _GridView.DataSource = data;
        _GridView.DataBind();
        dataV = data;
    }
    private void LoadInvitationSource()
    {
      //  WebReferenceReports.InvitationFunnelReportRequest req = new WebReferenceReports.InvitationFunnelReportRequest();
     //   req.createInvitationBy == WebReferenceReports.eCreateInvitationBy
        ddl_InvitationSource.Items.Clear();
        foreach(WebReferenceReports.eCreateInvitationBy item in  Enum.GetValues(typeof(WebReferenceReports.eCreateInvitationBy)))
        {
            ListItem li = new ListItem(item.ToString(), item.ToString());
            li.Selected = item == WebReferenceReports.eCreateInvitationBy.ALL;
            ddl_InvitationSource.Items.Add(li);
        }
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle("Invitation Funnel Report");
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
}