﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;

public partial class Publisher_PopupEngineReport : PageSetting
{
    const int DAYS_INTERVAL = 2;
    const string ALL_VALUE = "ALL_VALUE";
    const string FILE_NAME = "PopupEngineReport.xml";
    const string DATE_FORMAT_DAY = "{0:MM/dd/yyyy ddd}";
    protected const string PERCENT = "%";
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(btn_Run);
        if (!IsPostBack)
        {
            dataV = null;
          //  ConversionReportRequestV = null;
            SetToolbox();
            SetDatePicker();
            LoadCountries();
            LoadOrigins();
            LoadAdEngineCampaign();
            ExecReport();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    protected void btn_Run_Click(object sender, EventArgs e)
    {
        ExecReport();
    }
    private void ExecReport()
    {
        WebReferenceReports.AdEngineReportRequest _request = new WebReferenceReports.AdEngineReportRequest();
        _request._from = FromToDatePicker_current.GetDateFrom;
        _request._to = FromToDatePicker_current.GetDateTo;
        _request.OriginId = Guid.Parse(ddl_Origin.SelectedValue);
        _request.AdEngineCampaign = Guid.Parse(ddl_Campaign.SelectedValue);
        ListItem li_country = ddl_Country.SelectedItem;
        _request.Country = (li_country.Value == "-1") ? string.Empty : li_country.Text;

        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfAdEngineReportResponse result = null;
        try
        {
            result = _report.AdEngineReport(_request);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if(result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        DataTable data = new DataTable();
        data.Columns.Add("Date");
        data.Columns.Add("Hits");
        foreach(WebReferenceReports.AdEngineReportResponse aerr in result.Value)
        {
            DataRow row = data.NewRow();
            row["Date"] = string.Format(siteSetting.DateFormat, aerr.Date);
            row["Hits"] = aerr.Hits;
            data.Rows.Add(row);
        }
        if (data.Rows.Count == 0)
        {
            _GridView.DataSource = null;
            div_results.Visible = false;
            dataV = null;
        }
        else
        {
            _GridView.DataSource = data;
            div_results.Visible = true;
            lbl_RecordMached.Text = data.Rows.Count + " " + _Toolbox.RecordMached;
            dataV = data;
        }
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    private void SetToolboxEvents()
    {

        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.SetButtonPrintAsAsync();
    }
    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, lbl_AdEngineReport.Text);

        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordToPrint + "');", true);
            return;
        }
     //   Session["data_print"] = dataV;
        
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        _GridView.EnableViewState = false;
        PageRenderin _page = new PageRenderin();
        _page.Controls.Add(_GridView);
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                _GridView.RenderControl(textWriter);
            }
        }
        Session["string_print"] = sb.ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    private void LoadAdEngineCampaign()
    {
        PpcSite _cache = PpcSite.GetCurrent();

        ddl_Campaign.Items.Clear();
        ddl_Campaign.Items.Add(new ListItem("--ALL--", Guid.Empty.ToString()));
        foreach(AdEngineCampaignData aecd in _cache.AdEnginePopupCampaign)
            ddl_Campaign.Items.Add(new ListItem(aecd.Name, aecd.ID.ToString()));
        ddl_Campaign.SelectedIndex = 0;
    }

    private void LoadOrigins()
    {
        ddl_Origin.Items.Clear();
        ddl_Origin.Items.Add(new ListItem("--ALL--", Guid.Empty.ToString()));
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfOriginWithDAUs result;
        try
        {
            result = _report.GetDistributionOrigins();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }

        foreach (WebReferenceReports.OriginWithDAUs owd in result.Value)
            ddl_Origin.Items.Add(new ListItem(owd.OriginName, owd.OriginId.ToString()));
        ddl_Origin.SelectedIndex = 0;
    }
    private void LoadCountries()
    {
        ddl_Country.Items.Clear();
        ddl_Country.Items.Add(new ListItem("ALL", "-1"));
        string command = "EXEC [dbo].[GetCountries]";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem li = new ListItem((string)reader["CountryName"], ((int)reader["Id"]).ToString());
                    ddl_Country.Items.Add(li);
                }
            }
            conn.Close();
        }
        ddl_Country.SelectedIndex = 0;
    }
    void SetDatePicker()
    {
  //      FromToDatePicker_current.SetFromSentence = lbl_CreationDate.Text;
        FromToDatePicker_current.SetIntervalDateServer(DAYS_INTERVAL);
    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lbl_AdEngineReport.Text);
    }
    DataTable dataV
    {
        get { return (Session["AdEngineTable"] == null) ? null : (DataTable)Session["AdEngineTable"]; }
        set { Session["AdEngineTable"] = value; }
    }
    
}