﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_QuoteTicket : PublisherPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            string quote_id = Request.QueryString["quoteid"];
            Guid quoteId;
            if (!Guid.TryParse(quote_id, out quoteId))
            {
                ClosePopupByError();
                return;
            }
            LoadQuoteDetails(quoteId);
        }

    }

    private void LoadQuoteDetails(Guid quoteId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfQuoteDetailsResponse result = null;
        try
        {
            result = report.GetQuoteDetails(quoteId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClosePopupByError();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            ClosePopupByError();
            return;
        }
        ClipCallReport.Quote quote = result.Value.Quote;
        lbl_CreatedOn.Text = string.Format(siteSetting.DateTimeFormat, quote.CreatedOn);
        lbl_Price.Text = "$" + string.Format(NUMBER_FORMAT, quote.Price);
        lbl_QuoteNumber.Text = quote.Number + "";
        lbl_StartAt.Text = string.Format(siteSetting.DateFormat, quote.StartJobAt);
        lbl_Status.Text = quote.Status;
        lbl_StatusReason.Text = quote.StatusReason;
        txt_description.Text = quote.Description;
        cb_includematerials.Checked = quote.IncludedMaterials.HasValue ? quote.IncludedMaterials.Value : false;
        lbl_GuaranteeDuration.Text = quote.GuaranteeMonths.HasValue ? quote.GuaranteeMonths.Value.ToString() : null;


    }
    void ClosePopupByError()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseError", "CloseMeError();", true);
    }
}