﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClipCallLeadChat.aspx.cs" Inherits="Publisher_ClipCallLeadChat" %>
<%@ Register src="ClipCallControls/ChatControl.ascx" tagname="ChatControl" tagprefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link type="text/css" rel="stylesheet" href="Style.css" /> 
    <style type="text/css">
        html {
          overflow-x: hidden;
          overflow-y: auto;
        }
        .data-table2 TD
        {
            text-align:left !important;
        }
        .CreateReportSubmit2
        {
            margin: 0 auto;
            display: inline-block;
        }
        .refresh_button
        {
            float:right;
        }
        .enable_btn
        {
            width: 104px;
            height: 42px;
            margin: 0 auto;
            display: inline-block;
            cursor:not-allowed;
        }
        .error-msg
        {
            padding-right: 30px;
        }
        .pnl_fouls
        {
            font-size: 14px;
            font-weight: bold;
        }
    </style>
    <title></title>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="localTimeDisplay.js" type="text/javascript"></script>   
    <script type="text/javascript">
        var incidentAccountId = '<%# GetIncidentAccountId %>';
        function CloseMeError(message) {
            alert(message);
            window.close();
        }
        function OpenWin(url) {
            var _url = url;
            var chatWin = window.open(_url, "chatWin_", "resizable=0,width=900,height=900");
            return false;
        }
        function sendMessage() {
            if (Page_ClientValidate('chat')) {
                $(".CreateReportSubmit2").hide();
                $(".enable_btn").show();
            }


        }
        function createSurvey() {
            
            $(".btn_survey").hide();
            $(".enable_btn_survey").show();
            


        }
        function SetMessages(_proMessages, _custMessages, _allMessages,
                                _todProMessages, _todCustMessages, _todAllMessages)
        {
            proMessages = _proMessages;
            custMessages = _custMessages;
            allMessages = _allMessages;
            todProMessages = _todProMessages;
            todCustMessages = _todCustMessages;
            todAllMessages = _todAllMessages;
            $ddl_messages = $('#ddl_messages');
            SetDdlMessagesByDestination();
        }
        var proMessages = [];
        var custMessages = [];
        var allMessages = [];

        var todProMessages = [];
        var todCustMessages = [];
        var todAllMessages = [];

        var $ddl_messages;
        $(function () {
            var $txt = $('#<%# txt_message.ClientID %>');
            $('#<%# ddl_destination.ClientID %>').change(function () {
                SetDdlMessagesByDestination();
            });
            $('#ddl_messages').change(function () {
                $txt.val(this.value);
            });
            $('#<%# ddl_TypeOfMessage.ClientID %>').change(function () {
                SetDdlMessagesByDestination();
            });
        });
        function SetDdlMessagesByDestination()
        {
            // var list;
            var ddl_destination = document.querySelector('#<%# ddl_destination.ClientID %>');
            var ddl_TypeOfMessage = document.querySelector('#<%# ddl_TypeOfMessage.ClientID %>');
            if (ddl_TypeOfMessage.value == '<%# eMessageType.BOT.ToString() %>') {
                ddl_TypeOfMessage.classList.remove('red-color');
                for(var i = 0; i < ddl_destination.length; i++)
                    ddl_destination.options[i].removeAttribute('disabled');  
                if (ddl_destination.value == '<%# ClipCallReport.eSystemChatMessageDestination.All.ToString() %>')
                    SetDdlMessages(allMessages);
                else if (ddl_destination.value == '<%# ClipCallReport.eSystemChatMessageDestination.Customer.ToString() %>')
                    SetDdlMessages(custMessages);
                else if (ddl_destination.value == '<%# ClipCallReport.eSystemChatMessageDestination.Pro.ToString() %>')
                    SetDdlMessages(proMessages);
            }
            else {
                ddl_TypeOfMessage.classList.add('red-color');
                var customerHassurvey = document.querySelector('#<%# hf_customerHassurvey.ClientID %>').value;
                var supplierHassurvey = document.querySelector('#<%# hf_supplierHassurvey.ClientID %>').value;
                for(var i = 0; i < ddl_destination.length; i++){                    
                    if(ddl_destination.options[i].value  == '<%# ClipCallReport.eSystemChatMessageDestination.All.ToString() %>'){
                        if(customerHassurvey == 'false' && supplierHassurvey == 'false')
                            ddl_destination.options[i].removeAttribute('disabled');                            
                        else
                            ddl_destination.options[i].setAttribute('disabled', 'disabled');
                    }
                    else if(ddl_destination.options[i].value  == '<%# ClipCallReport.eSystemChatMessageDestination.Customer.ToString() %>'){
                        if(customerHassurvey == 'true')
                            ddl_destination.options[i].setAttribute('disabled', 'disabled');
                        else
                            ddl_destination.options[i].removeAttribute('disabled');
                    }
                    else if(ddl_destination.options[i].value  == '<%# ClipCallReport.eSystemChatMessageDestination.Pro.ToString() %>'){
                        if(supplierHassurvey == 'true')
                            ddl_destination.options[i].setAttribute('disabled', 'disabled');
                        else
                            ddl_destination.options[i].removeAttribute('disabled');
                    }
                }
                if (ddl_TypeOfMessage.value == '<%# eMessageType.TAD.ToString() %>') {
                    if (ddl_destination.value == '<%# ClipCallReport.eSystemChatMessageDestination.All.ToString() %>')
                        SetDdlMessages(todAllMessages);
                    else if (ddl_destination.value == '<%# ClipCallReport.eSystemChatMessageDestination.Customer.ToString() %>')
                        SetDdlMessages(todCustMessages);
                    else if (ddl_destination.value == '<%# ClipCallReport.eSystemChatMessageDestination.Pro.ToString() %>')
                        SetDdlMessages(todProMessages);
                }
                else {
                    SetDdlMessages([]);
                }
            }
             
        }
        function SetDdlMessages(list)
        {
           
            $ddl_messages.find('option').remove();
            list.forEach(function (element) {
                $ddl_messages.append($("<option></option>")
                .attr("value", element)
                .text(element));                   
            });
            

        }
        function ReciveServerData(retValue, HasMore) {
            if (HasMore)
                $(window).scroll(_scroll);
            if (retValue.length == 0)
                return;
            var _trs = $(retValue).find('table').get(0).getElementsByTagName('tr');
            var _length = _trs.length;
            for (var i = 1; i < _length; i++) {
                _trs[1].className = ((i % 2 == 1) ? "_even" : "_odd");
                $('.-GridView').append(_trs[1]);
            }
            window.localTime.start();

        }

        $(window).scroll(_scroll);
        function _scroll() {
            if ($(window).scrollTop() > 0.9 * ($(document).height() - $(window).height())) {
                $(window).unbind('scroll');
                _CallServer();
            }
        }
        function _CallServer() {

            $(window).unbind('scroll');
            showDiv();
            var iaId = $('#<%# hf_incidentAccountId.ClientID %>').val();
            var messageId = $('#<%# hf_nextMessageId.ClientID %>').val();
            if (messageId == null || messageId.length == 0) {
                hideDiv();
                return;
            }
            $.ajax({
                url: '<%# GetNextMessagesUrl %>',
                data: "{ 'incidentAccountId': '" + iaId + "','messageId': '" + messageId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    var result = data.d;//eval("(" + data.d + ")");
                    if (!result.IsSuccess) {
                        alert(result.FailureResponse);
                        return;
                    }
                    $('#<%# hf_nextMessageId.ClientID %>').val(result.NextMessageId);
                    
                    ReciveServerData(result.Table, (result.NextMessageId != null && result.NextMessageId.length > 0));

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //              alert(textStatus);
                    hideDiv();
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;
                    hideDiv();

                }

            });
        }
        function ClientValidate(source, arguments) {
           
            if (document.querySelector('#<%# ddl_TypeOfMessage.ClientID %>').value == '<%# eMessageType.SURVEY_PICKLIST.ToString() %>')
                arguments.IsValid = true;
            else if(document.querySelector('#<%# txt_message.ClientID %>').value.length == 0)
                arguments.IsValid = false;
            else
                arguments.IsValid = true;
        }
        function showDiv() {
            document.getElementById("divLoader").style.display = 'block';
        }
        function hideDiv() {
            document.getElementById("divLoader").style.display = 'none';
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <div>
       <asp:HiddenField ID="hf_incidentAccountId" runat="server" />
       <asp:HiddenField ID="hf_nextMessageId" runat="server" />
        <div class="titlerequestTop" style="margin-top: 20px;">          
            <asp:Label ID="lbl_Lead" runat="server" Text="Lead"></asp:Label>:
            <asp:Label ID="lbl_LeadNum" runat="server"></asp:Label>
        </div>
        <div class="clear"></div>
        <div class="separator"></div>
        <div id="form-analyticscomp2" style="width:600px; position:static;">
            <div class="containertab2" >   
                 <div class="form-field">
                    <asp:Label ID="Label1000" runat="server" Text="Type of message" CssClass="label"></asp:Label>
                    <asp:DropDownList ID="ddl_TypeOfMessage" CssClass="form-select" runat="server" ></asp:DropDownList>
                </div>             
                <div class="form-field">
                    <asp:Label ID="Label43" runat="server" Text="Destination" CssClass="label"></asp:Label>
                    <asp:DropDownList ID="ddl_destination" CssClass="form-select" runat="server" ></asp:DropDownList>
                </div>
                <div class="form-field" style="width: 400px;">
                    <asp:Label ID="Label7" runat="server" Text="Message" CssClass="label"></asp:Label>

               <%/*    <asp:DropDownList ID="_ddl_messages" CssClass="form-select" runat="server" Width="400"  ></asp:DropDownList>*/%>  
                    <select id="ddl_messages" class="form-select" style="width:400px;">
                        
                    </select>
                </div>
                
                <div class="clear"></div>
                <div class="form-field" style="width: 400px;">
                    <asp:Label ID="Label8" runat="server" Text="Message" CssClass="label"></asp:Label>
                    <asp:TextBox ID="txt_message" runat="server" TextMode="MultiLine" Rows="5" Width="350"></asp:TextBox>
                    <asp:CustomValidator ID="cv_FieldValidator" runat="server" ErrorMessage="RequiredFieldValidator"  ValidationGroup="chat"
                         Display="Static" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                    
                </div>
                <div class="form-field">
                    <asp:Label ID="Label9" runat="server" Text="Include SMS" CssClass="label"></asp:Label>
                    <asp:CheckBox ID="cb_includeSMS" runat="server" />
                </div>
                <div class="clear"></div>
                <div style="text-align:center;">
                    <asp:Button ID="btn_send" runat="server" Text="Send message" ValidationGroup="chat" OnClick="btn_send_Click" CssClass="CreateReportSubmit2" OnClientClick="sendMessage();" />
                    <div class="enable_btn" style="display:none;"></div>
                    <asp:ImageButton ID="ib_refresh" runat="server" ImageUrl="~/Publisher/images/refresh.png" CssClass="refresh_button" Width="30" Height="30" OnClick="ib_refresh_Click" />
                    <div>
                        <asp:Label ID="lbl_result" runat="server" Text="" CssClass="error-msg"></asp:Label>
                    </div>
                    
                </div>
            </div>
            <div class="containertab2" > 
                <asp:Panel ID="pnl_fouls" runat="server" CssClass="form-field pnl_fouls" Width="590">
                     <asp:Label ID="lbl_fouls" runat="server" Text="" ></asp:Label>
                    <asp:HyperLink ID="hl_fouls" runat="server" Target="_blank">Pro fouls page</asp:HyperLink>
                </asp:Panel>              
                <div class="clear"></div>
                 <div class="form-field" style="width:500px;">
                    <asp:Label ID="lbl_resultSurvey" runat="server" Text="" CssClass="error-msg"></asp:Label>
                </div>
                <div class="clear"></div>
                <div class="form-field" runat="server" id="div_cancelSupplierSurvey">
                 <!--   <asp:Label ID="Label11" runat="server" Text="Cancel supplier survey" CssClass="label"></asp:Label>-->
                    <asp:LinkButton ID="lb_cancelSupplierSurvey" runat="server" OnClick="lb_cancelSupplierSurvey_Click" OnClientClick="showDiv();">Cancel supplier survey</asp:LinkButton>
                </div>
                <div class="form-field" runat="server" id="div_cancelCustomerSurvey">
           <!--         <asp:Label ID="Label12" runat="server" Text="Cancel customer survey" CssClass="label"></asp:Label>-->
                    <asp:LinkButton ID="lb_cancelCustomerSurvey" runat="server" OnClick="lb_cancelCustomerSurvey_Click" OnClientClick="showDiv();">Cancel customer survey</asp:LinkButton>
                </div>

            </div>
            <div class="containertab2" >
         <%// div details %>
            <div runat="server" id="div_Details" style="overflow-y: auto;">
               
                <div class="clear"></div>
                <uc1:ChatControl ID="_ChatControl" runat="server" />
               
                
            </div>
        </div>
        </div>
       <asp:HiddenField ID="hf_customerHassurvey" runat="server" />
       <asp:HiddenField ID="hf_supplierHassurvey" runat="server" />
    </div>
    </form>
</body>
</html>
