using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Publisher_PublisherRanking : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        txt_Availability.Text = "0";
        txt_Availability.Attributes.Add("onblur", "javascript:ValidateNum(this, GetLblValid('" + lbl_ValidAvailability.ClientID + "'));");

        txt_Price.Text = "100";
        txt_Price.Attributes.Add("onblur", "javascript:ValidateNum(this, GetLblValid('" + lbl_ValidPrice.ClientID + "'));");

        txt_PublisherRanking.Text = "0";
        txt_PublisherRanking.Attributes.Add("onblur", "javascript:ValidateNum(this, GetLblValid('" + lbl_ValidPublisherRanking.ClientID + "'));");

        txt_surveys.Text = "0";
        txt_surveys.Attributes.Add("onblur", "javascript:ValidateNum(this, GetLblValid('" + lbl_ValidSurveys.ClientID + "'));");
        Page.Header.DataBind();
         * */
        if (!IsPostBack)
        {
            
            LoadRanking();
        }
        if(valuesV.Length>0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadValuesLabels",
            "LoadValuesLabels('" + valuesV + "','" + labelsV + "');", true);
        
    }

    private void LoadRanking()
    {
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("Code");
        data.Columns.Add("Value");

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
 //       SiteInterfaceLocal.SetAdvertiserRankingsRequest req = new SiteInterfaceLocal.SetAdvertiserRankingsRequest();
        WebReferenceSite.ResultOfGetRankingParametersResponse _response = null;
        try
        {
            _response = _site.GetRankingParameters();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if(_response.Type == WebReferenceSite.eResultType.Failure)
        {
      //      string _test = "alert('" + _response.Type.ToString() + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        string str = "";
        foreach(WebReferenceSite.RankingParametersEntry rpe in _response.Value.Parameters)
        {
            DataRow row = data.NewRow();
            row["Name"] = rpe.Name;
            row["Code"] = rpe.Code;
            row["Value"] = rpe.Weight;
            data.Rows.Add(row);
        }
        
        _Repeater.DataSource = data;
        _Repeater.DataBind();
        if (data.Rows.Count == 0)
            return;
        string _values = "";
        string _labels = "";
        int sum = 0;
        foreach (RepeaterItem _item in _Repeater.Items)
        {
            Label _lbl = (Label)_item.FindControl("lbl_ValidValue");
            TextBox _txt = (TextBox)_item.FindControl("txt_ParameterValue");
    //        _txt.Attributes.Add("onblur", "javascript:ValidateNum(this, GetLblValid('" + _lbl.ClientID + "'));");
            _txt.Attributes.Add("onblur", "javascript:ValidateIfNum(this);");
            _values += _txt.ClientID + ";";
            _labels += _lbl.ClientID + ";";
            sum += int.Parse(_txt.Text);
        }
       
        valuesV = _values.Substring(0, _values.Length - 1);
        labelsV = _labels.Substring(0, _labels.Length - 1);
        lblSum.Text = sum.ToString();
 //       ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadValuesLabels",
 //           "LoadValuesLabels('" + valuesV + "','" + labelsV + "');", true);
    }
    
    protected void btnSet_Click(object sender, EventArgs e)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.SetAdvertiserRankingsRequest _request = new WebReferenceSite.SetAdvertiserRankingsRequest();
        _request.Parameters = new WebReferenceSite.RankParamCodeValuePair[_Repeater.Items.Count];
        for (int i = 0; i < _Repeater.Items.Count; i++)
        {
            string code = ((Label)_Repeater.Items[i].FindControl("lbl_Code")).Text;
            string val = ((TextBox)_Repeater.Items[i].FindControl("txt_ParameterValue")).Text;
            _request.Parameters[i] = new WebReferenceSite.RankParamCodeValuePair();
            _request.Parameters[i].Code = int.Parse(code);
            _request.Parameters[i].Weight = int.Parse(val);
        }
        WebReferenceSite.Result _result = _site.SetRankingParametersValues(_request);
        if (_result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Success", "top.UpdateSuccess();", true);
        
    }
    string valuesV
    {
        set { ViewState["values"] = value; }
        get { return (ViewState["values"] == null) ? "" : (string)ViewState["values"]; }
    }
    string labelsV
    {
        set { ViewState["labels"] = value; }
        get { return (ViewState["labels"] == null) ? "" : (string)ViewState["labels"]; }
    }
}
