<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AccountSetting.aspx.cs" Inherits="Publisher_AccountSetting"  %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asps" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

    <link href="style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../general.js"></script>
<script type="text/javascript">
    function chkPaymentMethod()
    {
        var lbl_notification = document.getElementById("<%# lbl_SelectUser.ClientID %>");
        lbl_notification.style.display = "none";
        var lbl_ErrorRechargeSelectUser = document.getElementById("<%# lbl_ErrorRechargeSelectUser.ClientID %>");
        lbl_ErrorRechargeSelectUser.style.display = "none";
        var ValidationOK = true;
         if (!Page_ClientValidate('GeneralInfo'))
            ValidationOK = false;
        var lbl=document.getElementById('<%# lbl_CommentPayment.ClientID %>');
        lbl.style.display = "none";
        var CHK = document.getElementById("<%#cbl_Payment.ClientID%>");
        var checkbox = CHK.getElementsByTagName("input");
        var checkboxSelect = false;
        for(var i=0;i<checkbox.length;i++)
        {
            if(checkbox[i].checked)
            {
                checkboxSelect = true;
                break;
            }
        }
        if(!checkboxSelect)
        {
            lbl.style.display="inline";
            ValidationOK = false;
        }
        if(document.getElementById('<%# cb_RefundNotification.ClientID %>').checked)
        {
            if(document.getElementById("<%# ddl_RefundNotification.ClientID %>").selectedIndex == 0)
            {
                lbl_notification.style.display = "inline";
                ValidationOK = false;
            }
        }
        if (document.getElementById('<%# cb_ErrorRecharge.ClientID %>').checked) {
            if (document.getElementById("<%# ddl_ErrorRechargeUsers.ClientID %>").selectedIndex == 0) {
                lbl_ErrorRechargeSelectUser.style.display = "inline";
                ValidationOK = false;
            }
        }
        return ValidationOK;
    }
    function IsValidatorOfControl(control)
  {
    //    var IsInvalid=false;
        var validators = Page_Validators;  
        for (var i = 0;i < validators.length; i++)
        {
            if(control==document.getElementById(validators[i].controltovalidate))
            {
                if(!validators[i].isvalid)
                    return false;               
            }
        }
        return true;
  }
    function chkTxtBox(elm)
    {
        var _txt =  document.getElementById("<%# txt_DefaultExpertise.ClientID %>");
        if(!IsValidatorOfControl(elm))
            _txt.setAttribute("readonly", "readonly");
        else
        {
            document.getElementById("<%# RangeValidator_DefaultExpertise.ClientID %>").maximumvalue =(elm.value);
 //           alert(document.getElementById("<%# RangeValidator_DefaultExpertise.ClientID %>").maximumvalue );
            if(_txt.value>elm.value)
                _txt.value=elm.value;
            _txt.removeAttribute("readonly");
            ClearValidatorsOfControl(_txt);
        }
    }
    function ClearValidatorsOfControl(cont)
    {
        var validators = Page_Validators;
        
        for (var i = 0;i < Page_Validators.length;  i++)
        {
            if(Page_Validators[i].controltovalidate==cont.id)  
			    Page_Validators[i].style.display = "none";	
	    }       
    }
    function SetTdClass()
    {
        var _table=document.getElementById("_table_body");
        var _TR = _table.getElementsByTagName("tr");
        var index=0;       
        
        for(var i=0;i<_TR.length;i++)
        { 
           if(_TR[i].parentNode.id != _table.id)
                continue;      
            
            var _TD = _TR[i].getElementsByTagName("td");
            var _class=(index++%2==0)?"even_TD":"odd_TD";
            if(_TD[1])
                _TD[1].className=_class;          
        }       
    }
    window.onload=On_Load_Page;
    function On_Load_Page()
    {
        SetTdClass();
        document.getElementById("<%# cb_RefundNotification.ClientID %>").onclick = function ()
                    {
                        var div_RefundNotification = document.getElementById("<%# div_RefundNotification.ClientID %>");
                        div_RefundNotification.style.display = (this.checked) ? 
                                                    "block" : "none";
                        if(!this.checked)
                             document.getElementById("<%# lbl_SelectUser.ClientID %>").style.display = "none";
                  //      if(this.checked)
                 //           document.getElementById("<%# ddl_RefundNotification.ClientID %>").selectedIndex = 0;
                    }
        document.getElementById("<%# cb_ErrorRecharge.ClientID %>").onclick = function () {
                        var div_ErrorRecharge = document.getElementById("<%# div_ErrorRecharge.ClientID %>");
                        div_ErrorRecharge.style.display = (this.checked) ?
                                                    "block" : "none";
                        if (!this.checked)
                            document.getElementById("<%# lbl_ErrorRechargeSelectUser.ClientID %>").style.display = "none";
                    }
                }
                function UpdateCache() {
                    $('#btn_UpdateCache').hide();
                    $('#span_Updating').show();
                    var _data = "{UserId: '<%#  userManagement.GetGuid %>'}";
                    $.ajax({
                        url: '<%# UpdateCache %>',
                        data: _data,
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            var _response = eval('(' + data.d + ')');
                            if (_response.status == "Faild")
                                alert('Faild');
                            else if (_response.status == "PartialSuccess") {
                                var message = 'Partial success:';
                                if (_response.CrmFailds != 0)
                                    message += "\r\n" + _response.CrmFailds + " CRM servers failed to update";
                                if (_response.WebServerFaild != 0)
                                    message += "\r\n" + _response.WebServerFaild + " web servers failed to update";
                                alert(message);
                            }
                            else if (_response.status == "DataFaildProblem") {
                                var message = 'Data Faild Problem:';
                                alert(message);
                            }
                            else if (_response.status == "UserNotFound") {
                                var message = 'User Not Found:';
                                alert(message);
                            }
                            else if (_response.status == 'UpdateInProcessing') {
                                $('#span_InProcess').show();
                                $('#span_Updating').hide();
                                $('#btn_UpdateCache').hide();
                                return;
                            }
                            $('#btn_UpdateCache').show();
                            $('#span_Updating').hide();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        },
                        complete: function (jqXHR, textStatus) {
                            //  HasInHeadingRequest = false;
                        }

                    });
                }
    function updateIntextXml() {
        $('#btn_updateIntextXml').hide();
        $('#span_updateIntextXml').show();
        var _data = "{UserId: '<%#  userManagement.GetGuid %>'}";
        $.ajax({
            url: '<%# updateIntextXml %>',
            data: _data,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var _response = eval('(' + data.d + ')');
                if (_response.status == "Faild") {
                    var _message = 'Faild';
                    for (var i = 0; i < _response.OriginFailds.length; i++) {
                        _message += "\r\n" + _response.OriginFailds[i];
                    }
                    alert(_message);
                }
                
                $('#btn_updateIntextXml').show();
                $('#span_updateIntextXml').hide();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
            }

        });
    }
    function ClearPhoneNumber() {
        var _phone  = document.getElementById('<%# txt_ClearPhoneNumber.ClientID %>').value;
        if (confirm("Are you sure you want to clear all suppliers/customers/AAR suppliers with phone number: " + _phone))
            _ClearPhoneNumber(_phone);
    }
    function _ClearPhoneNumber(_phone) {
        $('#span_ClearPhoneNumber').show();
        if (!_phone || _phone.length == 0) {
            $('#span_ClearPhoneNumber').html('Failed - empty number');
            return;
        }
        $('#btn_ClearPhoneNumber').hide();
        $('#span_ClearPhoneNumber').html('Updating...');
       
        
        var _data = "{phone: '" + _phone + "'}";
        $.ajax({
            url: '<%# ClearMobileAppPhoneNumber %>',
            data: _data,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var _response = eval('(' + data.d + ')');
                if (_response) 
                    $('#span_ClearPhoneNumber').html('Success');
                else
                    $('#span_ClearPhoneNumber').html('Failed');

                $('#btn_ClearPhoneNumber').show();
          //      $('#span_ClearPhoneNumber').hide();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
            }

        });
    }
    /*
    function UpdateQuoteAbTesting() {
        if (!Page_ClientValidate('QuoteAbTesting'))
            return false;
        var _value = $('#<# txt_QuoteAbTesting.ClientID %>').val();
        $('#span_QuoteAbTesting').show();
       
        $('#btn_QuoteAbTesting').hide();
        $('#span_QuoteAbTesting').html('Updating...');


        var _data = "{value: " + _value + "}";
        $.ajax({
            url: '<# UpdateQuoteAbTestingService %>',
            data: _data,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var _response = eval('(' + data.d + ')');
                if (_response)
                    $('#span_QuoteAbTesting').html('Success');
                else
                    $('#span_QuoteAbTesting').html('Failed');

                $('#btn_QuoteAbTesting').show();
                //      $('#span_ClearPhoneNumber').hide();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
            }

        });
    }
    */
    function UpdateQuoteAbTesting2_0() {
        var _request = [];
        var tds = document.querySelector('#<%# cb_abtesting.ClientID %>').querySelectorAll('td');

        for (var i = 0; i < tds.length; i++) {
            if (tds[i].querySelector('input').checked == true)
                _request.push(tds[i].querySelector('label').innerHTML);
        }
 
        $('#span_QuoteAbTesting').show();

        $('#btn_QuoteAbTesting').hide();
        $('#span_QuoteAbTesting').html('Updating...');


        var _data = "{abtesting: " + JSON.stringify(_request) + "}";
        $.ajax({
            url: '<%# UpdateQuoteAbTestingService2_0 %>',
            data: _data,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var _response = eval('(' + data.d + ')');
                if (_response)
                    $('#span_QuoteAbTesting').html('Success');
                else
                    $('#span_QuoteAbTesting').html('Failed');

                $('#btn_QuoteAbTesting').show();
                //      $('#span_ClearPhoneNumber').hide();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
            }

        });
    }
   

function btn_UpdateCache_onclick() {

}

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>

<div class="page-content minisite-content account-settings">
	<h2><asp:Label ID="lblWelAccountSetting" runat="server" Text="Welcome to your Account Settings"></asp:Label></h2>		
    <table class="data-table" cellspacing="0" id="_table">
        <tbody id="_table_body">
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lblSite" CssClass="label" runat="server" Text="Site:"></asp:Label>
                </td>
                <td class="even_TD">
                    <asp:Label ID="lblSiteName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr runat="server" id="tr_update">
                <td class="TH_TD">
                    <asp:Label ID="lbl_UpdateCache" CssClass="label" runat="server" Text="Update cache"></asp:Label>
                </td>
                <td class="even_TD">
                    <input id="btn_UpdateCache" type="button" value="Update" onclick="UpdateCache();" />
                    <span id="span_Updating" style="display:none;">Updating...</span>
                    <span id="span_InProcess" style="display:none;">The update is in process,It will take a few minutes.</span>
                </td>
            </tr>
            <tr runat="server" id="tr_updateIntextXml">
                <td class="TH_TD">
                    <asp:Label ID="lbl_updateIntextXml" CssClass="label" runat="server" Text="Update Intext XML"></asp:Label>
                </td>
                <td class="even_TD">
                    <input id="btn_updateIntextXml" type="button" value="Update Intext XML" onclick="updateIntextXml();" />
                    <span id="span_updateIntextXml" style="display:none;">Updating...</span>
                </td>
            </tr>
            <tr runat="server" id="tr_ClearPhoneNumber">
                <td class="TH_TD">
                    <asp:Label ID="lbl_clearPhoneNumber" CssClass="label" runat="server" Text="Clear mobile app phone number"></asp:Label>
                </td>
                <td class="even_TD">
                    <asp:TextBox ID="txt_ClearPhoneNumber" CssClass="form-text" runat="server"></asp:TextBox>                   
                    <input id="btn_ClearPhoneNumber" type="button" value="Clear phone number" onclick="ClearPhoneNumber();" />
                    <span id="span_ClearPhoneNumber" style="display:none;">Updating...</span>
                </td>
            </tr>
             <tr runat="server" id="tr_QuoteAbTesting">
                <td class="TH_TD">
                 <%--    <asp:Label ID="Label3" CssClass="label" runat="server" Text="Quote Ab-Testing // 0 only 'B', 100 only 'A'"></asp:Label> --%>
                    <asp:Label ID="Label4" CssClass="label" runat="server" Text="Quote Ab-Testing"></asp:Label>
                </td>
                 <%--
                <td class="even_TD">
                    <asp:TextBox ID="txt_QuoteAbTesting" CssClass="form-text" runat="server"></asp:TextBox>                    
                    <input id="btn_QuoteAbTesting" type="button" value="Insert" onclick="UpdateQuoteAbTesting();" />
                     <asp:RequiredFieldValidator ID="rfv_QuoteAbTesting" runat="server" ErrorMessage="Required" ValidationGroup="QuoteAbTesting"
                          ControlToValidate="txt_QuoteAbTesting"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rv_QuoteAbTesting" runat="server" ErrorMessage="Range between 0-100" ValidationGroup="QuoteAbTesting"
                         MinimumValue="0" MaximumValue="100" Type="Integer" ControlToValidate="txt_QuoteAbTesting"></asp:RangeValidator>                    
                    <span id="span_QuoteAbTesting" style="display:none;">Updating...</span>
                </td>
                     --%>
                <td class="even_TD">
                    <asp:CheckBoxList ID="cb_abtesting" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                     <input id="btn_QuoteAbTesting" type="button" value="Set AB testing" onclick="UpdateQuoteAbTesting2_0();" />
                    <span id="span_QuoteAbTesting" style="display:none;">Updating...</span>
                </td>
            </tr>
            <tr runat="server" id="_tr_unvisible" visible="false">
                <td class="TH_TD">
                    <asp:Label ID="lblCountry" CssClass="label" runat="server" Text="Select default country"></asp:Label>
                </td>
                <td class="even_TD">
                    <asp:DropDownList ID="DDLCountry" CssClass="form-select" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lblTimeZone" CssClass="label" runat="server" Text="Select Time Zone"></asp:Label>
               </td>
                <td class="odd_TD">
                    <asp:DropDownList ID="ddlTimeZone" CssClass="form-select" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="TH_TD">       
                    <asp:Label ID="lbl_MoneySymbol" CssClass="label" runat="server" Text="Currency symbol"></asp:Label>
                </td>
                <td class="even_TD">        
                    <asp:DropDownList ID="ddlMoney" CssClass="form-select" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="TH_TD">       
                    <asp:Label ID="lbl_canRecord" CssClass="label" runat="server" Text="Allow recording"></asp:Label>
                </td>
                <td class="odd_TD">       
                    <asp:CheckBox ID="cb_AllowRecord" CssClass="form-checkbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="TH_TD">       
                    <asp:Label ID="lbl_URL" CssClass="label" runat="server" Text="URL site"></asp:Label>
                </td>
                <td class="even_TD">       
                    <asp:TextBox ID="txt_URL" CssClass="form-text" runat="server" ReadOnly="true"></asp:TextBox>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="TH_TD">   
                    <asp:Label ID="lbl_Payment" CssClass="label" runat="server" Text="Payment method"></asp:Label>
                </td>
                <td class="odd_TD"> 
                    <asp:CheckBoxList ID="cbl_Payment" runat="server" CssClass="tt" RepeatDirection="Horizontal"></asp:CheckBoxList>
                    <asp:Label ID="lbl_CommentPayment" runat="server" Text="No payment method selected" style="display:none;" CssClass="tl" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="TH_TD">   
                    <asp:Label ID="lbl_Cities" CssClass="label" runat="server" Text="Location display"></asp:Label>
                </td>
                <td class="even_TD" >
                    <asp:RadioButtonList ID="rbl_cities" runat="server" BorderStyle="None" CssClass="Table_radio tt" RepeatDirection="Horizontal"></asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="TH_TD">   
                    <asp:Label ID="lbl_MiniSite" CssClass="label" runat="server" Text="Mini Site Display"></asp:Label>
                </td>
                <td class="even_TD">
                    <asp:CheckBox ID="cb_MiniSite" CssClass="form-checkbox" runat="server" />
                </td>
            </tr>   
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_BalanceAlert" CssClass="label" runat="server" Text="Minimum balance alert"></asp:Label>
                </td>
                <td class="odd_TD">
                    <asp:TextBox ID="txt_BalanceAlert" CssClass="form-text" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorBalanceAlert" CssClass="error-msg" runat="server" ErrorMessage="Invalid - can insert only round nubmer" ControlToValidate="txt_BalanceAlert" ValidationGroup="GeneralInfo" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidatorBalanceAlert" CssClass="error-msg" runat="server" ErrorMessage="Invalid" ControlToValidate="txt_BalanceAlert" ValidationGroup="GeneralInfo" Display="Dynamic" MaximumValue="100" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>  
            <tr>
                <td class="TH_TD">   
                    <asp:Label ID="lbl_MaximumExpertise" CssClass="label" runat="server" Text="Maximum value in pick list"></asp:Label>
                </td>
                <td class="even_TD">
                    <asp:TextBox ID="txt_MaximumExpertise" CssClass="form-text" runat="server" onblur="javascript:chkTxtBox(this);"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_MaximumExpertise" CssClass="error-msg" runat="server" ErrorMessage="Missing" Display="Dynamic" ControlToValidate="txt_MaximumExpertise" ValidationGroup="GeneralInfo"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator_MaximumExpertise" CssClass="error-msg" runat="server" ErrorMessage="Invalid - value can't be under 3" ControlToValidate="txt_MaximumExpertise" ValidationGroup="GeneralInfo" Display="Dynamic" MaximumValue="100" MinimumValue="3" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>   
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_DefaultExpertise" CssClass="label" runat="server" Text="Default value in Pick List"></asp:Label>
                </td>
                <td class="odd_TD">
                    <asp:TextBox ID="txt_DefaultExpertise" CssClass="form-text" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_DefaultExpertise" CssClass="error-msg" runat="server" ErrorMessage="Missing" Display="Dynamic" ControlToValidate="txt_DefaultExpertise" ValidationGroup="GeneralInfo"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator_DefaultExpertise" CssClass="error-msg" runat="server" ErrorMessage="Invalid range validation" ControlToValidate="txt_DefaultExpertise" ValidationGroup="GeneralInfo" Display="Dynamic" MaximumValue="100" MinimumValue="1" Type="Integer" EnableClientScript="true"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_MultipleAuction" CssClass="label" runat="server" Text="Multiple auction"></asp:Label>
                </td>
                <td class="odd_TD">
                    <asp:TextBox ID="txt_MultipleAuction" CssClass="form-text" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorMultipleAuction" CssClass="error-msg" runat="server" ErrorMessage="Missing" Display="Dynamic" ControlToValidate="txt_MultipleAuction" ValidationGroup="GeneralInfo"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidatorMultipleAuction" runat="server" CssClass="error-msg" ErrorMessage="Invalid - value can be 1-10" ControlToValidate="txt_MultipleAuction" ValidationGroup="GeneralInfo" Display="Dynamic" MaximumValue="10" MinimumValue="1" Type="Integer" EnableClientScript="true"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_WebSiteTitle" CssClass="label" runat="server" Text="Web site title"></asp:Label>
                </td>
                <td class="odd_TD">
                    <asp:TextBox ID="txt_WebSiteTitle" CssClass="form-text" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_icon" CssClass="label" runat="server" Text="Icon link"></asp:Label>
                </td>
                <td class="odd_TD">
                    <asp:TextBox ID="txt_icon" CssClass="form-text" runat="server"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_RefundNotification" CssClass="label" runat="server" Text="Notification for new refund request"></asp:Label>
                </td>
                <td class="odd_TD">
                    <div style="float:left; line-height: 28px; #line-height:32px;">
                        <input id="cb_RefundNotification" class="checkbox" runat="server" type="checkbox"/>
                        <label runat="server" id="label_RefundNotification" for="<%# cb_RefundNotification.ClientID %>" >
                            <asp:Label ID="lblRefundNotificationYesNo" runat="server" Text="Yes / No"></asp:Label>
                        </label>
                    </div>
                    <div runat="server" id="div_RefundNotification" style="display:none; margin-left: 6px; padding-top:5px; float:left;">
                        <asp:Label ID="lbl_RefundNotificationUser"  runat="server" Text="User"></asp:Label>
                        <asp:DropDownList ID="ddl_RefundNotification" CssClass="form-text" runat="server">
                        </asp:DropDownList>
                        
                    </div>
                    <asp:Label ID="lbl_SelectUser" runat="server" Text="You need to choose a user" CssClass="error-msg"
                     style="display:none;"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_MinimumDeposit" CssClass="label" runat="server" Text="Minimum price for deposit"></asp:Label>
                </td>
                <td class="odd_TD">                    
                    <asp:TextBox ID="txt_MinimumDeposit" runat="server"></asp:TextBox>
                    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_MinimumDeposit" 
                    CssClass="error-msg" runat="server" ErrorMessage="Missing" Display="Dynamic" 
                    ControlToValidate="txt_MinimumDeposit" ValidationGroup="GeneralInfo"></asp:RequiredFieldValidator>
                    
                    <asp:RangeValidator ID="RangeValidator_MinimumDeposit" runat="server" Display="Dynamic"
                    ErrorMessage="RangeValidator" CssClass="error-msg" ValidationGroup="GeneralInfo"
                     Type="Integer" MinimumValue="0" MaximumValue="1000" ControlToValidate="txt_MinimumDeposit"></asp:RangeValidator>
                </td>
            </tr>
            
            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_LimitUnavailability" CssClass="label" runat="server" Text="Limit for unavailability (days)"></asp:Label>
                </td>
                <td class="odd_TD">                    
                    <asp:TextBox ID="txt_LimitUnavailability" runat="server"></asp:TextBox>
                                        
                    <asp:RangeValidator ID="RangeValidator_LimitUnavailability" runat="server" Display="Dynamic"
                    ErrorMessage="RangeValidator" CssClass="error-msg" ValidationGroup="GeneralInfo"
                     Type="Integer" MinimumValue="1" MaximumValue="1000" ControlToValidate="txt_LimitUnavailability"></asp:RangeValidator>
                </td>
            </tr>

            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_MinCallDurationSecondsForExemptOfMonthlyPayments" CssClass="label" runat="server" Text="Min Call Duration Seconds For Exempt Of Monthly Payments" ></asp:Label>
                </td>
                <td class="odd_TD">
                    <asp:TextBox ID="txt_MinCallDurationSecondsForExemptOfMonthlyPayments" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="RangeRangeValidator_MinCallDurationSecondsForExemptOfMonthlyPayments" CssClass="error-msg" ValidationGroup="GeneralInfo" runat="server" Display="Dynamic"
                        type="Integer" MinimumValue="-1" MaximumValue="999" ControlToValidate="txt_MinCallDurationSecondsForExemptOfMonthlyPayments" ErrorMessage="RangeValidator"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_MinCallDurationSecondsForExemptOfMonthlyPayments" CssClass="error-msg" ValidationGroup="GeneralInfo" runat="server" Display="Dynamic"
                        ControlToValidate="txt_MinCallDurationSecondsForExemptOfMonthlyPayments" ErrorMessage="Required" ></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_ExtraBonus" CssClass="label" runat="server" Text="% extra bonus for auto recharge"></asp:Label>
                </td>
                <td class="odd_TD">                    
                    <asp:TextBox ID="txt_ExtraBonus" runat="server"></asp:TextBox>
                                        
                    <asp:RangeValidator ID="RangeValidator_ExtraBonus" runat="server" Display="Dynamic"
                    ErrorMessage="The values should be 1 to 10" CssClass="error-msg" ValidationGroup="GeneralInfo"
                     Type="Integer" MinimumValue="1" MaximumValue="10" ControlToValidate="txt_ExtraBonus"></asp:RangeValidator>
                </td>
            </tr>

            <tr>
                <td class="TH_TD">
                    <asp:Label ID="lbl_ErrorRecharge" CssClass="label" runat="server" Text="Notification for error recharge"></asp:Label>
                </td>
                <td class="odd_TD">                 
                    <div style="float:left; line-height: 28px; #line-height:32px;">   
                        <asp:CheckBox ID="cb_ErrorRecharge" runat="server" CssClass="checkbox" />
                        <label runat="server" id="label1" for="<%# cb_ErrorRecharge.ClientID %>" >
                            <asp:Label ID="Label2" runat="server" Text="<%# lblRefundNotificationYesNo.Text %>"></asp:Label>
                        </label>
                    </div>
                    <div runat="server" id="div_ErrorRecharge" style="display:none; margin-left: 6px; padding-top:5px; float:left;">
                        <asp:Label ID="lbl_ErrorRechargeUsers"  runat="server" Text="User"></asp:Label>
                        <asp:DropDownList ID="ddl_ErrorRechargeUsers" CssClass="form-text" runat="server">
                        </asp:DropDownList>                        
                    </div>
                    <asp:Label ID="lbl_ErrorRechargeSelectUser" runat="server" Text="<%# lbl_SelectUser.Text %>" CssClass="error-msg"
                     style="display:none;"></asp:Label>
                </td>
            </tr>
            
            
            <tr>
                <td class="TH_TD">       
                    <asp:Label ID="lbl_logo" CssClass="label" runat="server" Text="Upload logo"></asp:Label>
                </td>
                <td class="even_TD" valign="middle">
                     <div style="float:left;  position: relative; top: 15px;"><input id="_FileImages" type="file" class="form-file" runat="server" /></div>
                    <div style="float:left; position: relative; left: 10px; top:-8px;"><asp:Button ID="btn_uploadLogo" runat="server" Text="Upload" CssClass="form-submit" OnClick="btn_uploadLogo_Click"  />  
                     <asp:Label ID="commentUploadImages" CssClass="error-msg" runat="server" Text="No picture selected" Visible="false"></asp:Label></div>
                </td>
            </tr>
            
            
            
            <tr runat="server" id="tr_pic" style="display:none;">
                <td class="TH_TD">
                </td>
                <td class="even_TD" >
                   <asp:Image runat="server" ID="_ImagePic" ImageUrl="~/Publisher/images/ClipCallLogo-03.png" />
                   <asp:LinkButton ID="lb_DeletePic" runat="server" OnClick="lb_DeletePic_Click">Delete</asp:LinkButton>  
                </td>
            </tr>

            <tr runat="server" id="tr_monthly_payment_test" >
                <td class="TH_TD">
                Test Monthly Payments (as if first of the month)
                </td>
                <td class="even_TD" >                                       
                   <asp:LinkButton ID="monthly_payment_test_button" runat="server" OnClick="monthly_payment_test_button_Click" CssClass="form-submit">Run Test</asp:LinkButton>  
                </td>
            </tr>
        </tbody>
    </table>
    <asp:Button ID="btnSet" runat="server" Text="Set" OnClick="btnSet_Click" ValidationGroup="GeneralInfo" CssClass="form-submit"  OnClientClick="return chkPaymentMethod();"  />
</div>

    <asp:Label ID="lbl_Choose" runat="server" Text="Choose" Visible="false"></asp:Label>
     <asp:Label ID="lbl_ErrorImage" runat="server" Text="Invalid image file" Visible="false"></asp:Label>
</asp:Content>

