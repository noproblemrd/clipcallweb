    <%@ Page Language="C#" AutoEventWireup="true" CodeFile="PublisherAudit.aspx.cs" Inherits="Publisher_PublisherAudit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="~/Controls/FromToDate.ascx" tagname="FromToDate" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
   <link href="PublishStyle.css" rel="stylesheet" type="text/css"/>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
    <script type="text/javascript" src="../general.js"></script>
    
    <script type="text/javascript" language="javascript">
    function witchAlreadyStep(level,mode)
    {    
        
        //parent.window.setInitRegistrationStep(level);
        parent.window.clearStepActive();    
        parent.window.setLinkStepActive('linkStep8');               
    }
    
    function init()
    {
        parent.goUp();
    }
    
    window.onload=init;
    
    
    </script>
</head>
<body style="background-color:transparent;" class="iframe-inner step8" >
<div id="mainaudit">
    <form id="form1" runat="server">
        <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <div class="ddl_pages">
    
        <asp:Label ID="lbl_pages" runat="server" Text="Page" CssClass="label"></asp:Label><br />
    <asp:DropDownList ID="ddl_pages" runat="server" CssClass="form-select">
    </asp:DropDownList>
    </div>
       <div class="audit"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>
    
    
    <br />
    
   
  
    
    
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <div class="audit_table">
        <asp:GridView ID="gvLog" runat="server" AutoGenerateColumns="false" 
         HeaderStyle-Font-Underline="true"
           GridLines="None" CssClass="data-table" BorderWidth="0" 
          AllowPaging="True" PageSize="20" OnPageIndexChanging="gvLog_PageIndexChanging">
        <HeaderStyle CssClass="th_max" />
         <RowStyle CssClass="odd_max" />
         <AlternatingRowStyle CssClass="even_max" />
        <Columns>
        
         <asp:TemplateField >      
        <HeaderTemplate>
            <asp:Label ID="Label5" runat="server" Text="<%#hf_CreatedOn.Value %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField>
        <HeaderTemplate>
            <asp:Label ID="Label11" runat="server" Text="<%#hf_UserName.Value %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label12" runat="server" Text="<%# Bind('UserName') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField> 
        
        <asp:TemplateField >        
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="<%#hf_pageId.Value %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label2" runat="server" Text="<%# Bind('PageName') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField >
        <HeaderTemplate>
            <asp:Label ID="Label11" runat="server" Text="<%#hf_AuditStatus.Value %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label12" runat="server" Text="<%# Bind('AuditStatus') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>    
            
        <asp:TemplateField>       
        <HeaderTemplate >
            <asp:Label ID="Label3" runat="server" Text="<%#hf_FieldName.Value %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label4" runat="server" Text="<%# Bind('FieldName') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
            
                
        <asp:TemplateField >       
        <HeaderTemplate>
            <asp:Label ID="Label7" runat="server" Text="<%#hf_OldValue.Value %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label8" runat="server" Text="<%# Bind('OldValue') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>  
            
        <asp:TemplateField >
        <HeaderTemplate>
            <asp:Label ID="Label9" runat="server" Text="<%#hf_NewValue.Value %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label10" runat="server" Text="<%# Bind('NewValue') %>"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField> 
                       
          
            
        </Columns>         
            
            
            <HeaderStyle Font-Underline="True" />
            <PagerStyle  CssClass="pager" ForeColor="Blue" HorizontalAlign="Center" />
            
        </asp:GridView>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
  
    
  
        <asp:HiddenField ID="hf_AuditStatus" runat="server" Value="Audit status" />
        <asp:HiddenField ID="hf_CreatedOn" runat="server" Value="Date" />
        <asp:HiddenField ID="hf_FieldName" runat="server" Value="Field name" />
        <asp:HiddenField ID="hf_NewValue" runat="server" Value="New value" />
        <asp:HiddenField ID="hf_OldValue" runat="server" Value="Old Value" />
        <asp:HiddenField ID="hf_UserName" runat="server" Value="Created by" />
        <asp:HiddenField ID="hf_pageId" runat="server" Value="Page" />
        
    <asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
    <asp:Label ID="lbl_noResults" runat="server" Text="There are no results" Visible="false"></asp:Label>
    
   
    </form>
    </div>
</body>
</html>
