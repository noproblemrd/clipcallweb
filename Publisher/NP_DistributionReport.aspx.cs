﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_NP_DistributionReport : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(lb_RunReport);
        if(!IsPostBack)
        {            
            setDateTime();
            LoadMonths();
            LoadYears();
            LoadDistributtionOrigins();
        }
        SetToolboxEvents();

    }

    
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        
        Guid _id;
        if (!Guid.TryParse(ddl_Origins.SelectedValue, out _id))
            _id = Guid.Empty;
        if (_id == Guid.Empty)
            GeneralDistributionReport();
        else
            OriginDistributionReport(_id);
        _UpdatePanel.Update();
        
    }

    private void OriginDistributionReport(Guid _id)
    {
        _NP_DistributionReport.Visible = false;
        _NP_DistributionReportOrigin.Visible = true;
        WebReferenceReports.NP_DistributionReportOriginRequest _request = new WebReferenceReports.NP_DistributionReportOriginRequest();
        
        DateTime _end;
        _request._from = GetStartEndDate(out _end);
        _request._to = _end;
       
        _request.OriginId = _id;
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(siteSetting.GetUrlWebReference);
        WebReferenceReports.ResultOfListOfNP_DistributionReportOriginResponse result = null;
        try
        {
            result = _reports.NP_DistributioReportByOrigin(_request);
        }
        catch (Exception exc)
        {
            Update_Faild();
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if (result.Value.Length == 0)
        {
            SetNoResults();
            return;
        }
        int _total = (from x in result.Value
                      select x.EffectiveInstalls).Sum();
        decimal _TotalEffectiveInstalls = (from x in result.Value
                                           select x.FixCost).Sum();
        SetGeneralResults(_total, _TotalEffectiveInstalls);
        hf_IsGeneral.Value = "false";
        _NP_DistributionReportOrigin.LoadData(result.Value);
    }
    private void GeneralDistributionReport()
    {
        _NP_DistributionReport.Visible = true;
        _NP_DistributionReportOrigin.Visible = false;
        WebReferenceReports.NP_DistributionReportRequest _request = new WebReferenceReports.NP_DistributionReportRequest();
        DateTime _end;
        _request._from = GetStartEndDate(out _end);
        _request._to = _end;

        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(siteSetting.GetUrlWebReference);
        WebReferenceReports.ResultOfListOfNP_DistributionReportResponse result = null;
        try
        {
            result = _reports.NP_DistributionReport(_request);
        }
        catch(Exception exc)
        {
            Update_Faild();
            dbug_log.ExceptionLog(exc);
            return;
        }
        if(result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if (result.Value.Length == 0)
        {
            SetNoResults();
            return;
        }
        int _total = (from x in result.Value
                      select x.EffectiveInstalls).Sum();
        decimal _TotalEffectiveInstalls = (from x in result.Value
                                           select x.Cost).Sum();
        SetGeneralResults(_total, _TotalEffectiveInstalls);
        hf_IsGeneral.Value = "true";
        _NP_DistributionReport.LoadData(result.Value);
    }

    private void SetNoResults()
    {
        hf_IsGeneral.Value = "";
        div_NonResults.Visible = true;
        div_TotalEffectiveInstalls.Visible = false;
        div_TotalPaymentAmount.Visible = false;
    }
    private void SetGeneralResults(int _totalInstalls, decimal _TotalCost)
    {
        div_NonResults.Visible = false;
        div_TotalEffectiveInstalls.Visible = true;
        div_TotalPaymentAmount.Visible = true;
        lbl_TotalEffectiveInstalls.Text = string.Format(NUMBER_FORMAT, _totalInstalls);
        lbl_TotalPaymentAmount.Text = string.Format(NUMBER_FORMAT, _TotalCost) + " " + siteSetting.CurrencySymbol;
    }
    private void SetToolboxEvents()
    {
        _ToolboxReport.SetTitle(lbl_NP_DistributionReport.Text);
        _ToolboxReport.RemovePrintButton();
        string _script = "_CreateExcel();";
        _ToolboxReport.SetExcelJavascript(_script);
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void LoadDistributtionOrigins()
    {
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(siteSetting.GetUrlWebReference);        
        WebReferenceReports.ResultOfListOfOriginWithDAUs result = null;
        try
        {
            result = _reports.GetDistributionOrigins();
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if(result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        ddl_Origins.Items.Clear();
        ListItem li = new ListItem("--ALL--", Guid.Empty.ToString());
        li.Selected = true;
        ddl_Origins.Items.Add(li);
        foreach(WebReferenceReports.OriginWithDAUs owd in result.Value)
            ddl_Origins.Items.Add(new ListItem(owd.OriginName, owd.OriginId.ToString()));

    }
    private void LoadMonths()
    {
        int month = DateTime.Now.Month;
        ddl_Month.Items.Clear();
        foreach (eMonths em in Enum.GetValues(typeof(eMonths)))
        {
            ListItem li = new ListItem(em.ToString(), ((int)em).ToString());
            li.Selected = (int)em == month;
            ddl_Month.Items.Add(li);
        }
    }
    private void LoadYears()
    {
        ddl_Years.Items.Clear();
        int thisyear = DateTime.Now.Year;
        for (int i = 2012; i < 2021; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            li.Selected = i == thisyear;
            ddl_Years.Items.Add(li);
        }
    }
    DateTime GetStartEndDate(out DateTime _end)
    {
        if (cb_MonthDate.Checked)
        {
            _end = _FromToDatePicker.GetDateTo;
            return _FromToDatePicker.GetDateFrom;            
        }
        else
        {
            return _GetStartEndDate(out _end);
        }
    }
    DateTime _GetStartEndDate(out DateTime _end)
    {
        int month;
        if (!int.TryParse(ddl_Month.SelectedValue, out month))
            month = DateTime.Now.Month;
        int year;
        if (!int.TryParse(ddl_Years.SelectedValue, out year))
            year = DateTime.Now.Year;
        DateTime _start = new DateTime(year, month, 1);
        _end = _start.AddMonths(1).AddDays(-1);
        return _start;
    }
    protected string GetCreateExcelGeneral
    {
        get
        {
            return ResolveUrl("~/Publisher/PagingService.asmx/NP_DistributionReport_CreateExcel");
        }
    }
    protected string GetCreateExcelOrigin
    {
        get
        {
            return ResolveUrl("~/Publisher/PagingService.asmx/NP_DistributionReportOrigin_CreateExcel");
        }
    }
    protected string GetDownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
   
}