﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using System.Configuration;

public partial class Publisher_AdvertiserRegisterReport : PageSetting
{
    const int WEEK_FROM_TODAY = 6;
    const string FILE_NAME = "AdvertiserRegisterReport.xml";

    bool _ShowCompare;
    Dictionary<WebReferenceReports.eCompletedRegistrationStep, string> RegistrationStep_translate;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        SetToolboxEvents();
        Load_eCompletedRegistrationStep_translate();
        RegistrationStep_translate = RegistrationStepV;
        if (!IsPostBack)
        {
            SetTextDatePicker();
            LoadIntervalChartTypes();
            SetToolbox();
            setDateTime();
            LoadCameFrom();
            LoadLandingPage();
            LoadPlans();
            LoadSalesMen();
            LoadSliders();
            
        }
        ScriptManager1.RegisterAsyncPostBackControl(btn_run);
        this.DataBind();  
    }

    private void LoadSliders()
    {
        string _style = "color: green;";
        ddl_SliderType.Items.Clear();
        ddl_SliderType_Compare.Items.Clear();
        ddl_SliderType.Items.Add(new ListItem("All flavors"));
        ddl_SliderType_Compare.Items.Add(new ListItem("All flavors"));

        ddl_EmailType.Items.Clear();
        ddl_EmailType_Compare.Items.Clear();
        ddl_EmailType.Items.Add(new ListItem("All flavors"));
        ddl_EmailType_Compare.Items.Add(new ListItem("All flavors"));

        foreach (eFlavour _flavor in Enum.GetValues(typeof(eFlavour)))
        {
            int _flavor_int = (int)_flavor;
            if (_flavor_int >= 5000 && _flavor_int <= 5999)                
            {
                ListItem li = new ListItem(_flavor.ToString(), _flavor.ToString());
                ListItem li_compare = new ListItem(_flavor.ToString(), _flavor.ToString());
                switch (_flavor_int)
                {
                    case (5011):
                    case (5012):
                    case (5013):
                    case (5014):
                    case (5015):
                    case (5016):
                        li.Text += " (active)";
                        li.Attributes["style"] = _style;
                        li_compare.Text += " (active)";
                        li_compare.Attributes["style"] = _style;
                        break;
                }               
                ddl_SliderType.Items.Add(li);
                ddl_SliderType_Compare.Items.Add(li_compare);
            }

            if (_flavor_int >= 6000 && _flavor_int <= 6999)
            {
                ListItem li = new ListItem(_flavor.ToString(), _flavor.ToString());
                ListItem li_compare = new ListItem(_flavor.ToString(), _flavor.ToString());

                ddl_EmailType.Items.Add(li);
                ddl_EmailType_Compare.Items.Add(li_compare);
            }


        }

        ddl_SliderType.SelectedIndex = 0;
        ddl_SliderType_Compare.SelectedIndex = 0;

        ddl_SliderType.SelectedIndex = 0;

    }

    private void SetTextDatePicker()
    {
        FromToDatePicker_current.SetFromSentence = lbl_CreationDateFrom.Text;
        FromToDatePicker_Compare.SetFromSentence = lbl_CreationDateFrom.Text;
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (!string.IsNullOrEmpty(PathV))
        {
            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSLine.swf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + _chart + "');", true);
        }
        else
        {
            div_chart_container.Visible = false;
            UpdatePanel_chart.Update();
        }
        

    }
    private void LoadIntervalChartTypes()
    {
        rbl_Chart.Items.Clear();
        foreach (eInterval _type in Enum.GetValues(typeof(eInterval)))
        {
            if (_type == eInterval.HOUR || _type == eInterval.YEAR)
                continue;
            string s = Utilities.GetEnumStringFormat(_type);// _type.ToString().Replace("_", " ");
            ListItem li = new ListItem(s, _type.ToString());
            li.Selected = _type == eInterval.DAY;
            rbl_Chart.Items.Add(li);
        }
    }
    private void Load_eCompletedRegistrationStep_translate()
    {
        if (RegistrationStepV != null)
            return;
        Dictionary<WebReferenceReports.eCompletedRegistrationStep, string> dic = new Dictionary<WebReferenceReports.eCompletedRegistrationStep, string>();
        foreach (WebReferenceReports.eCompletedRegistrationStep _item in Enum.GetValues(typeof(WebReferenceReports.eCompletedRegistrationStep)))
        {
            dic.Add(_item, EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eCompletedRegistrationStep", _item.ToString()));
        }
        RegistrationStepV = dic;
    }

    private void LoadSalesMen()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllPublishersResponse result = null;
        try
        {
            result = _site.GetAllPublishers();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        ddl_AccountManager.Items.Clear();
        ddl_AccountManager_Compare.Items.Clear();
        ListItem li_compare = new ListItem("ALL", string.Empty);
        li_compare.Selected = true;
        ListItem li = new ListItem("ALL", string.Empty);
        li.Selected = true;
        ListItem li2 = new ListItem("Auto registration", Guid.Empty.ToString());
        ListItem li2_Compare = new ListItem("Auto registration", Guid.Empty.ToString());

        IEnumerable<WebReferenceSite.PublisherData> publishers = result.Value.Publishers.OrderBy(z => z.Name);
        ddl_AccountManager.Items.Add(li);
        ddl_AccountManager.Items.Add(li2);
        ddl_AccountManager_Compare.Items.Add(li_compare);
        ddl_AccountManager_Compare.Items.Add(li2_Compare);
        foreach (WebReferenceSite.PublisherData _publisher in publishers)
        {
            ddl_AccountManager.Items.Add(new ListItem(_publisher.Name, _publisher.AccountId.ToString()));
            ddl_AccountManager_Compare.Items.Add(new ListItem(_publisher.Name, _publisher.AccountId.ToString()));
        }
    }

    private void LoadPlans()
    {
        ddl_Plan.Items.Clear();
        ddl_Plan_Compare.Items.Clear();
        foreach (WebReferenceReports.eRegisterPlan erp in Enum.GetValues(typeof(WebReferenceReports.eRegisterPlan)))
        {
            string trans = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eRegisterPlan", erp.ToString());
            ddl_Plan.Items.Add(new ListItem(trans, erp.ToString()));
            ddl_Plan_Compare.Items.Add(new ListItem(trans, erp.ToString()));
        }
    }



    private void LoadCameFrom()
    {
        ddl_CameFrom.Items.Clear();
        ddl_CameFrom_Compare.Items.Clear();
        foreach(WebReferenceReports.eRegisterCameFrom ercf in Enum.GetValues(typeof(WebReferenceReports.eRegisterCameFrom)))
        {
            string trans = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eRegisterCameFrom", ercf.ToString());
            ddl_CameFrom.Items.Add(new ListItem(trans, ercf.ToString()));
            ddl_CameFrom_Compare.Items.Add(new ListItem(trans, ercf.ToString()));
        }
    }

    private void LoadLandingPage()
    {
        ddl_LandingPage.Items.Clear();
        ddl_LandingPage_Compare.Items.Clear();

        foreach (WebReferenceReports.eRegisterLandingPageType erlp in Enum.GetValues(typeof(WebReferenceReports.eRegisterLandingPageType)))
        {
            //string trans = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eRegisterCameFrom", erlp.ToString());
            ddl_LandingPage.Items.Add(new ListItem(erlp.ToString(), erlp.ToString()));
            ddl_LandingPage_Compare.Items.Add(new ListItem(erlp.ToString(), erlp.ToString()));            
        }
    }

    private void setDateTime()
    {
        FromToDatePicker_current.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
            WEEK_FROM_TODAY);
    }
    protected void btn_run_Click(object sender, EventArgs e)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.AdvertiserRegistrationRequest _request = new WebReferenceReports.AdvertiserRegistrationRequest();
        _request.ShowStep1 = cb_ShowPage1.Checked;
        _request.From = FromToDatePicker_current.GetDateFrom;
        _request.To = FromToDatePicker_current.GetDateTo;
        WebReferenceReports.eRegisterCameFrom came_from;
        if(!Enum.TryParse(ddl_CameFrom.SelectedValue, out came_from))
            came_from = WebReferenceReports.eRegisterCameFrom.All;
        _request.CameFrom = came_from;

        WebReferenceReports.eRegisterLandingPageType landinpage;
        if (!Enum.TryParse(ddl_LandingPage.SelectedValue, out landinpage))
            landinpage = WebReferenceReports.eRegisterLandingPageType.All;
        _request.landingPage = landinpage;
       
        WebReferenceReports.eRegisterPlan register_plan;
        if (!Enum.TryParse(ddl_Plan.SelectedValue, out register_plan))
            register_plan = WebReferenceReports.eRegisterPlan.All;
        _request.Plan = register_plan;
        Guid _Sales_man;
        if (Guid.TryParse(ddl_AccountManager.SelectedValue, out _Sales_man))
            _request.SalseManId = _Sales_man;
        else
            _request.SalseManId = null;
        if (came_from == WebReferenceReports.eRegisterCameFrom.RecruitmentSlider)
            _request.SliderFlavor = ddl_SliderType.SelectedValue;
        else if (came_from == WebReferenceReports.eRegisterCameFrom.Email)
            _request.SliderFlavor = ddl_EmailType.SelectedValue;

        if (cb_CompareTo.Checked)
        {
            _request.CompareRequest = new WebReferenceReports.AdvertiserRegistrationRequest();
            _request.CompareRequest.From = FromToDatePicker_Compare.GetDateFrom;
            _request.CompareRequest.To = FromToDatePicker_Compare.GetDateTo;
            if(!Enum.TryParse(ddl_CameFrom_Compare.SelectedValue, out came_from))
                came_from = WebReferenceReports.eRegisterCameFrom.All;
            _request.CompareRequest.CameFrom = came_from;

            if (!Enum.TryParse(ddl_LandingPage_Compare.SelectedValue, out landinpage))
                landinpage = WebReferenceReports.eRegisterLandingPageType.All;
            _request.CompareRequest.landingPage = landinpage;
            
            if (!Enum.TryParse(ddl_Plan_Compare.SelectedValue, out register_plan))
                register_plan = WebReferenceReports.eRegisterPlan.All;
            _request.CompareRequest.Plan = register_plan;
            if (Guid.TryParse(ddl_AccountManager_Compare.SelectedValue, out _Sales_man))
                _request.CompareRequest.SalseManId = _Sales_man;
            else
                _request.CompareRequest.SalseManId = null;
            _request.CompareRequest.ShowStep1 = cb_ShowPage1_Compare.Checked;
            if (came_from == WebReferenceReports.eRegisterCameFrom.RecruitmentSlider)
                _request.CompareRequest.SliderFlavor = ddl_SliderType_Compare.SelectedValue;
            else if(came_from == WebReferenceReports.eRegisterCameFrom.Email)
                _request.CompareRequest.SliderFlavor = ddl_EmailType_Compare.SelectedValue;

        }
        WebReferenceReports.ResultOfAdvertiserRegistrationResponse result = null;
        try
        {
            result = _report.AdvertiserRegistrationReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        
        _ShowCompare = cb_CompareTo.Checked;
        DataTable data = new DataTable();
        data.Columns.Add("Step");
        data.Columns.Add("Users");
        data.Columns.Add("DropOff");
        data.Columns.Add("Passed");
        data.Columns.Add("DropOffFromStep");
        data.Columns.Add("PassedFromStep");
        data.Columns.Add("Step_Compare");
        data.Columns.Add("Users_Compare");
        data.Columns.Add("DropOff_Compare");
        data.Columns.Add("Passed_Compare");
        data.Columns.Add("DropOffFromStep_Compare");
        data.Columns.Add("PassedFromStep_Compare");
        data.Columns.Add("Gap");
        data.Columns.Add("GapPosNeg");
        data.Columns.Add("ShowCompare", typeof(bool));
       // Dictionary<WebReferenceReports.eCompletedRegistrationStep, string> dic = RegistrationStepV;
        foreach (WebReferenceReports.eCompletedRegistrationStep CompletedRegistrationStep in Enum.GetValues(typeof(WebReferenceReports.eCompletedRegistrationStep)))
        {
            DataRow row = data.NewRow();
            IEnumerable<WebReferenceReports.AdvertiserRegistrationResponseData> queryCompare = null;
            int _step = (int)CompletedRegistrationStep;
            IEnumerable<WebReferenceReports.AdvertiserRegistrationResponseData> queryCurrent = from x in result.Value.CurrentData
                                                                                        where x.Step == _step
                                                                                        select x;
            bool ShowRow = true;
            row["ShowCompare"] = _ShowCompare;
            if (_ShowCompare)
            {
                queryCompare = from x in result.Value.CompareData
                                where x.Step == _step
                                select x;
                
            }
            if (queryCurrent.Count() > 0)
            {
                WebReferenceReports.AdvertiserRegistrationResponseData _value = queryCurrent.FirstOrDefault();
                row["Step"] = GetCompletedRegistrationStepWithNumber(CompletedRegistrationStep);
                row["Users"] = GetStringFormatNumberPercent(_value.Users);
                row["DropOff"] = GetStringFormatNumberPercent(_value.DropOff);
                row["Passed"] = GetStringFormatNumberPercent(_value.Passed);
                row["DropOffFromStep"] = GetStringFormatNumberPercent(_value.DropOffFromStep);
                row["PassedFromStep"] = GetStringFormatNumberPercent(_value.PassedFromStep);
            }
            else if (queryCompare != null && queryCompare.Count() > 0)
            {
                row["Step"] = GetCompletedRegistrationStepWithNumber(CompletedRegistrationStep);
                row["Users"] = "-";
                row["DropOff"] = "-";
                row["Passed"] = "-";
                row["DropOffFromStep"] = "-";
                row["PassedFromStep"] = "-";
            }
            else
                ShowRow = false;
            if (queryCompare != null && queryCompare.Count() > 0)
            {
                WebReferenceReports.AdvertiserRegistrationResponseData _value = queryCompare.FirstOrDefault();
                row["Step_Compare"] = GetCompletedRegistrationStepWithNumber(CompletedRegistrationStep);
                row["Users_Compare"] = GetStringFormatNumberPercent(_value.Users);
                row["DropOff_Compare"] = GetStringFormatNumberPercent(_value.DropOff);
                row["Passed_Compare"] = GetStringFormatNumberPercent(_value.Passed);
                row["DropOffFromStep_Compare"] = GetStringFormatNumberPercent(_value.DropOffFromStep);
                row["PassedFromStep_Compare"] = GetStringFormatNumberPercent(_value.PassedFromStep);
                double? _gap = (from x in result.Value.GAP
                               where x.key == CompletedRegistrationStep
                               select x.value).FirstOrDefault();
                row["Gap"] = GetStringFormatNumberPercent_NegativePositive(_gap);
                row["GapPosNeg"] = (_gap < 0) ? "negative" : "positive";
                
            }
            else if (queryCompare != null && queryCurrent.Count() > 0)
            {
                row["Step_Compare"] = GetCompletedRegistrationStepWithNumber(CompletedRegistrationStep);
                row["Users_Compare"] = "-";
                row["DropOff_Compare"] = "-";
                row["Passed_Compare"] = "-";
                row["DropOffFromStep_Compare"] = "-";
                row["PassedFromStep_Compare"] = "-";
                row["Gap"] = "-";
                row["GapPosNeg"] = "positive";
            }
            if(ShowRow)
                data.Rows.Add(row);
        }
        Dictionary<DateTime, int> dic_users = new Dictionary<DateTime, int>();
        foreach (WebReferenceReports.DatetimeIntPair dip in result.Value.RegiterUsers)
            dic_users.Add(dip.key, dip.value);
        LoadChart(dic_users);
        RegistrationUsersV = dic_users;
        dataV = data;
        _Repeater.DataSource = data;
        _Repeater.DataBind();
        UpdatePanel_table.Update();
        UpdatePanel_chart.Update();


    }
    string GetCompletedRegistrationStepWithNumber(WebReferenceReports.eCompletedRegistrationStep _step)
    {
        return ((int)_step + 1) + "-" + RegistrationStep_translate[_step];
    }
    string GetStringFormatNumberPercent_NegativePositive(double? num)
    {
        if (num == null)
            return "-";
        return string.Format(this.GetNumberFormat, num) + "%";
    }
    string GetStringFormatNumberPercent(double num)
    {
        if (num < 0)
            return "-";
        return string.Format(this.GetNumberFormat, num) + "%";
    }
    string GetStringFormatNumberPercent(int num)
    {
        if (num < 0)
            return "-";
        return num.ToString();
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetButtonPrintAsAsync();
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, lbl_AdvertiserRegisterReport.Text);
        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        _Repeater.DataSource = dataV;
        _Repeater.DataBind();
        PageRenderin page = new PageRenderin();
        page.Controls.Add(_Repeater);
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                _Repeater.RenderControl(textWriter);
            }
        }
        Session["grid_print"] = sb.ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_AdvertiserRegisterReport.Text);
    }
    protected void btn_UpdateChart_Click(object sender, EventArgs e)
    {
        LoadChart(RegistrationUsersV);
    }
    #region  chart

    bool LoadChart(Dictionary<DateTime, int> dic_users)
    {
        eInterval _interval;
        if (!Enum.TryParse(rbl_Chart.SelectedValue, out _interval))
            _interval = eInterval.DAY;
        Dictionary<DateTime, int> dic = GetUsersByInterval(dic_users, _interval);
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + lbl_AdvertiserRegisterReport.Text + @"' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='3' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");
        sb.Append(@"numberSuffix='' ");
        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar=' ' ");

        sb.Append(@"bgColor='FFFFFF'>");



        int modulu = dic_users.Count / 15;
        modulu++;


        sb.Append(@"<categories>");
        StringBuilder sb_users = new StringBuilder();

        sb_users.Append(@"<dataset seriesName='" + lbl_Users.Text + @"' color='FF0000' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
        string string_format_date = (_interval == eInterval.YEAR) ? "m" : "dd/MM/yyyy";
        foreach (KeyValuePair<DateTime, int> kvp in dic)
        {
           
            string _title = lbl_Users.Text + "=" + kvp.Value + "\r\n";
            sb.Append(@"<category name='" + kvp.Key.ToString(string_format_date) + @"' toolText='" + _title + "'/>");

            sb_users.Append(@"<set value='" + kvp.Value + "'/>");
            
        }
        sb_users.Append(@"</dataset>");
        
        sb.Append("</categories>");
        sb.Append(sb_users.ToString());
        
        sb.Append(@"</graph>");
        div_chart_container.Visible = true;
        return SetChartXML(sb);

    }
    Dictionary<DateTime, int> GetUsersByInterval(Dictionary<DateTime, int> dic_users, eInterval _interval)
    {
        if (_interval == eInterval.DAY)
            return dic_users;
        Dictionary<DateTime, int> new_dic_users = new Dictionary<DateTime, int>();
        if (_interval == eInterval.WEEK)
        {
            int _sum = 0;
            DateTime start_date = dic_users.Min(x => x.Key);
            DateTime end_date = dic_users.Max(x => x.Key);
            start_date = start_date.AddDays(-1 * (int)start_date.DayOfWeek);
            foreach (KeyValuePair<DateTime, int> kvp in dic_users)
            {
                _sum += kvp.Value;
                if (kvp.Key.DayOfWeek == System.DayOfWeek.Saturday)
                {
                    new_dic_users.Add(start_date, _sum);
                    _sum = 0;
                    start_date = kvp.Key.AddDays(1);
                }
            }
            if (start_date <= end_date)
                new_dic_users.Add(start_date, _sum);
        }
        else
        {
            int _sum = 0;
            DateTime start_date = dic_users.Min(x => x.Key);
            DateTime end_date = dic_users.Max(x => x.Key);
            start_date = start_date.AddDays(-1 * ((int)start_date.Day-1));
            foreach (KeyValuePair<DateTime, int> kvp in dic_users)
            {
                if (kvp.Key.Day == 1)
                {
                    if(start_date < kvp.Key)
                        new_dic_users.Add(start_date, _sum);
                    _sum = 0;
                    start_date = kvp.Key;
                }
                _sum += kvp.Value;
                
            }
            if (start_date <= end_date)
                new_dic_users.Add(start_date, _sum);
        }
        return new_dic_users;
    }
    bool SetChartXML(StringBuilder sb)
    {
        string path = //HttpContext.Current.Request.PhysicalApplicationPath + 
           ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            return false;
        }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        return true;
    }

    #endregion

    // DateTime properties
    protected string GetDateFormat()
    {
        return ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat);
    }
    protected string GetFromCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxFrom().ClientID; }
    }
    protected string GetToCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxTo().ClientID; }
    }

    protected string GetFromPastId
    {
        get { return FromToDatePicker_Compare.GetTextBoxFrom().ClientID; }
    }
    protected string GetToPastId
    {
        get { return FromToDatePicker_Compare.GetTextBoxTo().ClientID; }
    }
    protected string GetInvalidDate
    {
        get { return Server.HtmlEncode(lbl_DateError.Text); }
    }
    protected string Lbl_DatesChange
    {
        get { return lbl_DatesChange.Text; }
    }
    //******//

    protected string GetDivCurrentId
    {
        get { return FromToDatePicker_current.GetParentDivId(); }
    }
    protected string GetDivPastId
    {
        get { return FromToDatePicker_Compare.GetParentDivId(); }
    }
    protected string GetCameFromShowSliderOptions
    {
        get { return WebReferenceReports.eRegisterCameFrom.RecruitmentSlider.ToString(); }
    }

    protected string GetCameFromShowEmailOptions
    {
        get { return WebReferenceReports.eRegisterCameFrom.Email.ToString(); }
    }


    protected bool ShowCompare { get { return _ShowCompare; } set { _ShowCompare = value; } }
    DataTable dataV
    {
        get { return (Session["data"] == null) ? null : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    Dictionary<WebReferenceReports.eCompletedRegistrationStep, string> RegistrationStepV
    {
        get { return (Session["eCompletedRegistrationStep"] == null) ? null : (Dictionary<WebReferenceReports.eCompletedRegistrationStep, string>)Session["eCompletedRegistrationStep"]; }
        set { Session["eCompletedRegistrationStep"] = value; }
    }
    Dictionary<DateTime, int> RegistrationUsersV
    {
        get { return (Session["RegistrationUsers"] == null) ? null : (Dictionary<DateTime, int>)Session["RegistrationUsers"]; }
        set { Session["RegistrationUsers"] = value; }
    }
    string PathV
    {
        get { return (ViewState["XmlGraph"] == null) ? "" : (string)ViewState["XmlGraph"]; }
        set { ViewState["XmlGraph"] = value; }
    }

}