using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Web.Services.Protocols;

public partial class Publisher_PublisherNotes : PageSetting, IPostBackEventHandler, ICallbackEventHandler
{
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    private const string OPEN_FILE_CLICK = "OpenFile";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!userManagement.IsPublisher() || string.IsNullOrEmpty(GetGuidSetting()))
            {
                Response.Redirect("LogOut.aspx");
                return;
            }

            ClientScript.RegisterStartupScript(this.GetType(), "bdika", "HideInsert();", true);
            
            LoadSupplierComments(GetGuidSetting());

            string csnameStep = "setStep";
            Type cstypeStep = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager csStep = this.Page.ClientScript;
            string csTextStep = "";

            if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
            {
                csTextStep = "witchAlreadyStep(7);";
                csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
            }

        }
        else
        {
            Dictionary<int, string> dic = PageListV;
            foreach (KeyValuePair<int, string> kvp in dic)
                PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
        }
        SetCallBackEvent();
        Page.Header.DataBind();
    }
    
    void LoadSupplierComments(string SupplierId)
    {
        DataTable data = new DataTable();
        data.Columns.Add("CommentId", typeof(string));
        data.Columns.Add("CreatedBy", typeof(string));
        data.Columns.Add("Text", typeof(string));
        data.Columns.Add("CreatedOn", typeof(string));
   //     data.Columns.Add("Title", typeof(string));
        data.Columns.Add("by", typeof(string));
        data.Columns.Add("HasFile", typeof(bool));
        data.Columns.Add("File", typeof(string));

        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        string result = string.Empty;
        try
        {
            result = supplier.GetSupplierComments(SupplierId);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["SupplierComments"] == null || xdd["SupplierComments"]["Error"] != null)
        {
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "Failed"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlNodeList xnl = xdd["SupplierComments"].ChildNodes;
        foreach (XmlNode node in xnl)
        {
            string CommentId = node["CommentId"].InnerText;
            string CreatedBy = node["CreatedBy"].InnerText;
            string Text = node["Text"].InnerText;
            string CreatedOn = node["CreatedOn"].InnerText;
            string FilePath = (node["File"] == null) ? string.Empty : node["File"].InnerText;
            DataRow row = data.NewRow();
            row["CommentId"] = CommentId;
            row["CreatedBy"] = CreatedBy;
            row["Text"] = Text;
            row["CreatedOn"] = CreatedOn;
   //         row["Title"] = Hidden_lblTitleComment.Value;
            row["by"] = Hidden_lblCreateBy.Value;
            if (!string.IsNullOrEmpty(FilePath))
            {
                row["HasFile"] = true;
            //    string url = System.Configuration.ConfigurationManager.AppSettings["NotesFilesWeb"] +
           //         FilePath;
                row["File"] = @"javascript:OpenFile('" + FilePath + "')";
                    
            }
            else
                row["HasFile"] = false;
            data.Rows.Add(row);
        }
        dataV = data;
        BindData(data);
 //       dlComments.DataSource = data;
 //       dlComments.DataBind();
    }
    protected void OpenFile()
    {
        string file_name = hf_fileName.Value;
        string[] _names = file_name.Split('.');
        string extention = string.Empty;
        if (_names.Length > 1)        
            extention = "." + _names[1];
        //file:///e:/stxavier/docs/7.jpg
        string path = System.Configuration.ConfigurationManager.AppSettings["NotesFiles"] +
            file_name;
        FileInfo fi = new FileInfo(path);
        string _ContentType = Utilities.GetMimeType(fi);
        Page.Response.ContentType = _ContentType;// "image/jpeg";// "application/vnd.ms-excel";// "application/ms-excel";// "Application/x-msexcel";       
        Page.Response.AppendHeader("Content-disposition", "attachment; filename=notes"+extention);

        //    _page.Response.WriteFile(path);
        Page.Response.BinaryWrite(File.ReadAllBytes(path));
        Page.Response.End();  
        
    }
    
    protected void btnInsert_Click(object sender, EventArgs e)
    {
  //      string SupplierId = "870C4060-455C-DF11-80CD-0003FF727321";
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        string xmlUpsert = @"<SupplierComment ID="""">" +
            "<SupplierId>" + GetGuidSetting() + "</SupplierId>" +
            "<Text>" + Server.HtmlEncode(txtInsertComment.Text) + "</Text>" +
            "<UserId>" + userManagement.GetGuid + "</UserId>" +
            "<File>" + FileNameV + "</File>" + 
            "</SupplierComment>";
        string result = supplier.UpsertSupplierComment(xmlUpsert);
        ClientScript.RegisterStartupScript(this.GetType(), "bdika", "HideInsert();", true);
        txtInsertComment.Text = string.Empty;
        FileNameV = null;
        lbl_file_name.Text = string.Empty;
        span_upload.Attributes.Add("style", "display:inline");
        LoadSupplierComments(GetGuidSetting());
    }
    protected void btn_upload_Click(object sender, EventArgs e)
    {
        string name = f_name.PostedFile.FileName;
        if (string.IsNullOrEmpty(name))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "noFile", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoFile.Value) + "');", true);
            return;
        }
        string extension = System.IO.Path.GetExtension(name);
        string localPath = System.Configuration.ConfigurationManager.AppSettings["NotesFiles"];
        string FileName = string.Empty;
        do
        {
            FileName = Utilities.RandomString(10);
        } while (File.Exists(localPath + FileName + extension));
        string _path = localPath + FileName + extension;       
        
        f_name.PostedFile.SaveAs(_path);
        FileNameV = FileName + extension;
        lbl_file_name.Text = FileName + extension;
        span_upload.Attributes.Add("style", "display:none");
        lblShowInsert.Text = Hidden_lblHide.Value;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UploadedFile", "UploadedFile();", true);
    }

    #region table
    protected void BindData(DataTable data)
    {
        PagedDataSource objPDS = new PagedDataSource();
        objPDS.AllowPaging = true;
        objPDS.PageSize = ITEM_PAGE;
        objPDS.DataSource = data.DefaultView;

        if (data.Rows.Count > 0)
        {
            
            objPDS.CurrentPageIndex = CurrentPage;
            lnkPreviousPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkNextPage.Visible = !(objPDS.IsFirstPage && objPDS.IsLastPage);
            lnkPreviousPage.Enabled = !objPDS.IsFirstPage;
            lnkNextPage.Enabled = !objPDS.IsLastPage;
            LoadPages(data.Rows.Count);
            dlComments.DataSource = objPDS;
            dlComments.DataBind();
    
            div_paging.Visible = true;
        }
        else
        {
            div_paging.Visible = false;
            dlComments.DataSource = null;
            dlComments.DataBind();
        }
        _UpdatePanel.Update();
    }
    void LoadPages(int countRows)
    {
        PlaceHolderPages.Controls.Clear();
        int countPages = countRows / ITEM_PAGE;
        countPages += (countRows % ITEM_PAGE == 0) ? 0 : 1;

        //if there is only one page
        if (countPages == 1)
        {
            div_paging.Visible = false;
            PageListV = null;
            return;
        }
        Dictionary<int, string> dic = new Dictionary<int, string>();
        List<HtmlTableCell> tc = new List<HtmlTableCell>();
        int cp= CurrentPage+1;
        int start = (((int)(CurrentPage / PAGE_PAGES)) * PAGE_PAGES)  + 1;

        if (start > PAGE_PAGES)
        {
            int indx = start - PAGE_PAGES;
            dic.Add(indx, "...");            
        }
        int i = start;
        for (; i <= countPages &&
            i < (start + PAGE_PAGES); i++)
        {
            dic.Add(i, "" + i);
        }
        if (i <= countPages)
        {
            int indx = start + PAGE_PAGES;
            dic.Add(indx, "...");
        }
        PageListV = dic;
        foreach(KeyValuePair<int, string> kvp in dic)
            PlaceHolderPages.Controls.Add(MakeCell(kvp.Key, kvp.Value));
    }
    LinkButton MakeCell(int indx, string _item)
    {
        LinkButton lb = new LinkButton();
        lb.CommandArgument = "" + indx;
        lb.ID = "lb_" + indx;
        lb.Text = _item;
        if (indx != CurrentPage + 1)
            lb.Click += new EventHandler(lnkPage_Click);
        else
        {
            lb.Enabled = false;
            lb.CssClass = "active";
        }
     //   HtmlTableCell htc = new HtmlTableCell();
    //    htc.Controls.Add(lb);
    //    htc.Attributes.Add("class", "td_num_pages");
        return lb;
    }
   
    protected void lnkPage_Click(object sender, EventArgs e)
    {       
        int cp = int.Parse(((LinkButton)sender).CommandArgument);
        CurrentPage = cp - 1;
        BindData(dataV);
    }
    protected void lnkNextPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp++;
        CurrentPage = cp;
       
        BindData(dataV);
    }

    protected void lnkPreviousPage_Click(object sender, EventArgs e)
    {
        int cp = CurrentPage;
        cp--;
        CurrentPage = cp;
       
        BindData(dataV);
    }
    int CurrentPage
    {
        get
        {
            //Look for current page in ViewState 
            object o = this.ViewState["CurrentPage"];
            if (o == null)
                return 0; // default page index of 0
            else
                return (int)o;
        }
        set
        {
            this.ViewState["CurrentPage"] = value;
        }
    }
    DataTable dataV
    {
        get
        {
            return (ViewState["data"] == null) ? new DataTable() :
              (DataTable)ViewState["data"];
        }
        set { ViewState["data"] = value; }
    }
    Dictionary<int, string> PageListV
    {
        get
        {
            return (ViewState["PageList"] == null) ? new Dictionary<int, string>() :
              (Dictionary<int, string>)ViewState["PageList"];
        }
        set { ViewState["PageList"] = value; }
    }
    #endregion
    string FileNameV
    {
        get { return (Session["FileName"] == null) ? string.Empty : (string)Session["FileName"]; }
        set { Session["FileName"] = value; }
    }

    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        if (eventArgument == OPEN_FILE_CLICK)
            OpenFile();
    }
    protected override void Render(HtmlTextWriter writer)
    {
        Page.ClientScript.RegisterForEventValidation(this.UniqueID, OPEN_FILE_CLICK);
        
        base.Render(writer);

    }
    public string Get_tab_click()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(this, OPEN_FILE_CLICK);
        myPostBackOptions.PerformValidation = false;
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);

    }

    #endregion

    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
       return string.Empty;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        string file_name = FileNameV;
        string localPath = System.Configuration.ConfigurationManager.AppSettings["NotesFiles"];

        string _path = localPath + file_name;
        if (File.Exists(_path))
        {
            try
            {
                File.Delete(_path);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, siteSetting);
            }
        }
        FileNameV = string.Empty;
    }
    protected void SetCallBackEvent()
    {
        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);
    }

    #endregion
}
