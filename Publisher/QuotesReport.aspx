﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="QuotesReport.aspx.cs" Inherits="Publisher_QuotesReport" %>


<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
     <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
   
     <script type="text/javascript">
         window.onload = appl_init;

         function appl_init() {
             var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
             pgRegMgr.add_beginRequest(BeginHandler);
             pgRegMgr.add_endRequest(EndHandler);

         }
         function BeginHandler() {
             //     alert('in1');
             showDiv();
         }
         function EndHandler() {
             //      alert('out1');
             hideDiv();
         }
         function OpenIframe(_path, key) {
             var _leadWin = window.open(_path, "LeadDetails" + key, "width=1000, height=650");
             _leadWin.focus();
             return false;
         }
         function Openquote(id) {
             var _url = '<%# GetQuoteUrl %>' + id;
             var chatWin = window.open(_url, "quoteWin" + id, "resizable=0,width=600,height=600");
             return false;
         }

         function _CreateExcel() {
             var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"<%# EXCEL_NAME %>"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcelUrl %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
         }
         function createTransfer(_btn, quoteId, amount) {
             //div_review_loader
             var AreYouShure = confirm("You are about to transfer " + amount);
             if (!AreYouShure)
                 return false;
             var $div_parent = $(_btn).parents('div.div_CreateTransfer');
             var $div_loader = $div_parent.find('div.div_transfer_loader');
             $div_loader.show();
             var spanResult = $div_parent.find('span.span_CreateTransfer_response')
             var incidentId = $(_btn).parents('tr').find('input[type="hidden"]').val();
             $div_parent.find('div.div_CreateTransfer_btn').hide();
             $.ajax({
                 url: "<%# CreateTransferUrl %>",
                 data: "{ 'quoteId': '" + quoteId + "'}",
                 dataType: "json",
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 dataFilter: function (data) { return data; },
                 success: function (data) {
                     // var _data = eval("(" + data.d + ")");
                     var _response = data.d == "true" ? "success" : "failed";
                     spanResult.html(_response);
                 },
                 error: function (XMLHttpRequest, textStatus, errorThrown) {
                     alert(textStatus);
                 },
                 complete: function (jqXHR, textStatus) {
                     //  HasInHeadingRequest = false;
                     $div_loader.hide();
                 }
             });
             return false;
         }
         </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
          
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	       
               <div class="main-inputs" runat="server" id="PublisherInput" style="width:680px;">			
	                
                        <div id="div_experties" class="form-field" runat="server">
					            <label for="form-subsegment" class="label" runat="server" id="lblExperties" >Heading</label>
                            <asp:DropDownList ID="ddl_Heading" runat="server" CssClass="form-select">
                            </asp:DropDownList>					            
			            </div>
                        <div class="form-field">
                          <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Project Id</label>
			              <asp:TextBox ID="txtRequestId" CssClass="form-text" runat="server" ></asp:TextBox>										
                          <asp:CompareValidator runat="server" Type="Integer"  Operator="DataTypeCheck" ControlToValidate="txtRequestId" Display="Dynamic">
                             <asp:Label ID="lbl_RequestIdError" runat="server" Text="Must be number" Visible="true"></asp:Label>
                          </asp:CompareValidator> 
                       </div>         
            			 <div class="form-field">
                            <asp:Label ID="lbl_IncludeQaTest" runat="server" Text="Include QA" CssClass="label"></asp:Label>
                           <asp:CheckBox ID="cb_IncludeQaTest" runat="server" />
                        </div>
                   </div>
             <div class="clear"></div>
                    <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div> 
            </div>
         <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	            <ContentTemplate>	
                    <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
	                <div id="Table_Report"  runat="server" >
                        <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				            <RowStyle CssClass="even" />
				            <AlternatingRowStyle CssClass="odd" />
				            <Columns>
					            <asp:TemplateField SortExpression="CaseNumber">
						            <HeaderTemplate>
							            <asp:Label ID="Label1" runat="server" Text="Case Number"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Bind("CaseNumber") %>' OnClientClick='<%# Bind("OpenCaseTicketScript") %>'>LinkButton</asp:LinkButton>
						            </ItemTemplate>
					            </asp:TemplateField>

                                <asp:TemplateField SortExpression="Date">
						            <HeaderTemplate>
							            <asp:Label ID="Label101" runat="server" Text="Quote Date"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label112" runat="server" Text='<%# Bind("QuoteDate") %>'></asp:Label>
						            </ItemTemplate>
					            </asp:TemplateField>

                                 <asp:TemplateField SortExpression="CategoryName">
						            <HeaderTemplate>
							            <asp:Label ID="Label14" runat="server" Text="Category"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label15" runat="server" Text='<%# Bind("CategoryName") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>

					            <asp:TemplateField SortExpression="Price">
						            <HeaderTemplate>
							            <asp:Label ID="Label10" runat="server" Text="Price"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label11" runat="server" Text='<%# Bind("Price") %>'></asp:Label>
						            </ItemTemplate>
					            </asp:TemplateField>										           

					            <asp:TemplateField SortExpression="QuoteStatus">
						            <HeaderTemplate>
							            <asp:Label ID="Label16" runat="server" Text="Quote Status"></asp:Label>
						            </HeaderTemplate>
						            <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Bind("QuoteStatus") %>' OnClientClick='<%# Bind("QuoteScript") %>'>LinkButton</asp:LinkButton>
							            
						            </ItemTemplate>
					            </asp:TemplateField>

					             <asp:TemplateField SortExpression="SupplierName">
						            <HeaderTemplate>
							            <asp:Label ID="Label18" runat="server" Text="Supplier Name"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label19" runat="server" Text='<%# Bind("SupplierName") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>					

                                 <asp:TemplateField SortExpression="SupplierPhone">
						            <HeaderTemplate>
							            <asp:Label ID="Label41" runat="server" Text="Supplier Phone"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label42" runat="server" Text='<%# Bind("SupplierPhone") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>	

                                 <asp:TemplateField SortExpression="Video">
						            <HeaderTemplate>
							            <asp:Label ID="Label43" runat="server" Text="Video"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <a id="a1" runat="server" href='<%# Bind("Video") %>' target="_blank">
                                            <asp:Label ID="Label192" runat="server" Text="Video"></asp:Label>
                                        </a> 
						            </ItemTemplate>
					            </asp:TemplateField>	

                                 <asp:TemplateField SortExpression="CustomerName">
						            <HeaderTemplate>
							            <asp:Label ID="Label45" runat="server" Text="Customer Name"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label46" runat="server" Text='<%# Bind("CustomerName") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>	

                                 <asp:TemplateField SortExpression="CustomerPhone">
						            <HeaderTemplate>
							            <asp:Label ID="Label47" runat="server" Text="Customer Phone"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label48" runat="server" Text='<%# Bind("CustomerPhone") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>	

                                 <asp:TemplateField SortExpression="Address">
						            <HeaderTemplate>
							            <asp:Label ID="Label49" runat="server" Text="Address"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
							            <asp:Label ID="Label50" runat="server" Text='<%# Bind("Address") %>'></asp:Label> 
						            </ItemTemplate>
					            </asp:TemplateField>	

                                 <asp:TemplateField SortExpression="Trace">
						            <HeaderTemplate>
							            <asp:Label ID="Label51" runat="server" Text="Trace"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
                                        <asp:LinkButton ID="lb_trace" runat="server" Text="Trace" OnClientClick='<%# Bind("Trace") %>'></asp:LinkButton>
							             
						            </ItemTemplate>
					            </asp:TemplateField>	

                                 <asp:TemplateField SortExpression="Trace">
						            <HeaderTemplate>
							            <asp:Label ID="lbl_CreateTransfer" runat="server" Text="Create transfer"></asp:Label> 
						            </HeaderTemplate>
						            <ItemTemplate>
                                         <div runat="server" id="div_CreateTransfer" class="div_CreateTransfer" visible='<%# Bind("CanCreateTransfer") %>'>
                                            <div class="div_CreateTransfer_btn">
                                                <asp:Button ID="btn_CreateTransfer" runat="server" Text="Create transfer" OnClientClick='<%# Bind("ClickCreateTransfer") %>' />
                                                                                                        
                                            </div>
                                            <div style="text-align:center; display:none;" class="div_transfer_loader">
                                                <asp:Image ID="img_CreateTransfer" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                                            </div>
                                            <span class="span_CreateTransfer_response" style="color:red;"></span>
                                        </div>
                                        
							             
						            </ItemTemplate>
					            </asp:TemplateField>	
                                                                
				            </Columns>    
				            </asp:GridView>
                        </div>
                    </ContentTemplate>
             </asp:UpdatePanel>        
        </div>
</asp:Content>

