﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using ClosedXML.Excel;


public partial class Publisher_SalesMen : PageSetting
{
    Dictionary<string, List<WebReferenceReports.SalesmanDepositData>> dic;

    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            SetToolbox();
        }
        SetToolboxEvents();
    }

    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        dic = dataV;
        BindToRepeater();
        //ToExcel te = new ToExcel(this, "SalesMen");
        //if (!te.ExecExcel(_Repeater))
        //Update_Faild();        
        ToNiceExcel();
    }

    private void ToNiceExcel()
    {
        List<List<object>> data = new List<List<object>>();
        List<object> titles = new List<object>();
        data.Add(titles);
        titles.Add("Deposit Date");
        titles.Add("Amount");
        titles.Add("Account Name");
        titles.Add("Account NP Number");
        titles.Add("Account Approved On");
        titles.Add("Account Zap Number");
        titles.Add("Recharge Type");
        titles.Add("Payment Method");
        titles.Add("Sales Person");
        titles.Add("Amount For Calls");
        titles.Add("Amount Not For Calls");

        foreach (var item in dic)
        {
            foreach (var deposit in item.Value)
            {
                List<object> row = new List<object>();
                row.Add(deposit.DepositCreatedOn);
                row.Add(deposit.MoneyAmount);
                row.Add(deposit.AccountName);
                row.Add(deposit.AccountNumber);
                row.Add(deposit.AccountApprovedOn);
                row.Add(deposit.BizId);
                row.Add(deposit.RechargeType);
                row.Add(deposit.PaymentMethod);
                row.Add(deposit.SalesmanName);
                row.Add(deposit.AmountForCalls);
                row.Add(deposit.AmountNotForCalls);
                data.Add(row);
            }
        }

        ToExcel te = new ToExcel(this, "SalesMen");
        var excel = te.CreateExcelXLWorkBook(data);
        te.DownloadExcelXLWorkBook(excel);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        dic = dataV;
        BindToRepeater();
        foreach (RepeaterItem _item in _Repeater.Items)
        {
            if (_item.ItemType != ListItemType.Item && _item.ItemType != ListItemType.AlternatingItem)
                continue;
            HtmlGenericControl _div = ((HtmlGenericControl)_item.FindControl("div_details"));
            _div.Style[HtmlTextWriterStyle.Display] = "block";
        }
        Session["data_print"] = null;
        Session["grid_print"] = _Repeater;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_ReportName.Text);
    }

    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.SalesmenReportRequest _request = new WebReferenceReports.SalesmenReportRequest();
        _request.FromDate = FromToDate1.GetDateFrom;
        _request.ToDate = FromToDate1.GetDateTo;
        if (_request.FromDate == DateTime.MinValue || _request.ToDate == DateTime.MinValue)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "WrongDateFormat",
                "alert('" + FromToDate1.GetWrongDateFormat() + "');", true);
            return;
        }
        WebReferenceReports.ResultOfListOfSalesmanDeposits result = null;
        try
        {
            result = _reports.SalesmenReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        dic = new Dictionary<string, List<WebReferenceReports.SalesmanDepositData>>();

        foreach (WebReferenceReports.SalesmanDeposits smd in result.Value)
        {
            dic.Add(smd.SalesmanName, smd.Deposits.ToList());
        }
        dataV = dic;
        BindToRepeater();
        _UpdatePanel.Update();
    }

    void BindToRepeater()
    {
        _Repeater.DataSource = dic.Keys;
        _Repeater.DataBind();

    }

    protected void _Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        string _name = ((Label)e.Item.FindControl("lbl_Name")).Text;
        Repeater _rep = (Repeater)e.Item.FindControl("_RepeaterIn");
        foreach (KeyValuePair<string, List<WebReferenceReports.SalesmanDepositData>> kvp in dic)
        {
            if (kvp.Key == _name)
            {
                _rep.DataSource = kvp.Value;
                _rep.DataBind();
                break;
            }
        }
    }

    protected void _RepeaterIn_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow _tr = (HtmlTableRow)e.Item.FindControl("_tr");
        _tr.Attributes["class"] = (e.Item.ItemIndex % 2 == 0) ? "even" : "odd";
    }

    Dictionary<string, List<WebReferenceReports.SalesmanDepositData>> dataV
    {
        get { return (Session["data"] == null) ? new Dictionary<string, List<WebReferenceReports.SalesmanDepositData>>() : (Dictionary<string, List<WebReferenceReports.SalesmanDepositData>>)Session["data"]; }
        set { Session["data"] = value; }
    }

    public override Control CrossPageControl
    {
        get
        {
            return _Repeater;
        }
    }
}
