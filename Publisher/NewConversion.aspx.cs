﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Reflection;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Publisher_NewConversion : PageSetting
{
    const int DAYS_INTERVAL = 2;
    const string ALL_VALUE = "ALL_VALUE";
    const string FILE_NAME = "CoversionReport.xml";
    const string DATE_FORMAT_DAY = "{0:MM/dd/yyyy ddd}";
    protected const string PERCENT = "%";
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(ddl_Origin);
        if (!IsPostBack)
        {
            dataV = null;
            PageIndex = 1;
            ConversionReportRequestV = null;
            SetToolbox();
            SetDatePicker();
            LoadExpertises();
            LoadConversionChartTypes();
            LoadConversionDetails();
            LoadCountries();
            LoadFlavors();
            LoadTypes();
            LoadInterval();
      //      LoadCountries();
            ExecReport();
        }
        SetToolboxEvents();
        Header.DataBind();
    }

    private void LoadTypes()
    {
        ddl_type.Items.Clear();
        ddl_type.Items.Add(new ListItem("--ALL--", "-1"));
        foreach (eSliderType est in Enum.GetValues(typeof(eSliderType)))
            ddl_type.Items.Add(new ListItem(est.ToString(), ((int)est).ToString()));
        ddl_type.SelectedIndex = 0;
    }

    private void LoadConversionChartTypes()
    {
 	    rbl_Chart.Items.Clear();
        foreach(eConversionChartType _type in Enum.GetValues(typeof(eConversionChartType)))
        {
            string s = Utilities.GetEnumStringFormat(_type);// _type.ToString().Replace("_", " ");
            ListItem li = new ListItem(s, _type.ToString());
            li.Selected = _type == eConversionChartType.Regular_view;
            rbl_Chart.Items.Add(li);
        }
    }

    private void LoadFlavors()
    {
        ddl_ControlName.Items.Clear();
        ddl_ControlName.Items.Add(new ListItem(string.Empty));
        List<string> list = new List<string>();
        foreach (string s in Enum.GetNames(typeof(eFlavour)))
        {
            list.Add(s);           
        }
        list.Sort();
        foreach (string s in list)
        {
            ddl_ControlName.Items.Add(new ListItem(s, s));
        }
        ddl_ControlName.SelectedIndex = 0;
    }

    private void LoadCountries()
    {
        ddl_Country.Items.Clear();
        ddl_Country.Items.Add(new ListItem("ALL", "-1"));
        string command = "EXEC [dbo].[GetCountries]";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem li = new ListItem((string)reader["CountryName"], ((int)reader["Id"]).ToString());
                    ddl_Country.Items.Add(li);
                }
            }
            conn.Close();
        }
        ddl_Country.SelectedIndex = 0;
    }

    private void LoadInterval()
    {
        ddl_Interval.Items.Clear();
        foreach (string str in Enum.GetNames(typeof(WebReferenceReports.eConversionInterval)))
        {
            var _txt = str.Replace('_', ' ');
            ListItem li = new ListItem(_txt, str);
            li.Selected = str == WebReferenceReports.eConversionInterval.DAY.ToString();
            ddl_Interval.Items.Add(li);
        }

        ddl_DayOfWeek.Items.Clear();
        string Everyday = "Everyday";
        ddl_DayOfWeek.Items.Add(new ListItem(Everyday, Everyday));
        foreach (string str in Enum.GetNames(typeof(DayOfWeek)))
        {
            ListItem li = new ListItem(str, str);
            ddl_DayOfWeek.Items.Add(li);
        }
        ddl_DayOfWeek.SelectedIndex = 0;

        ddl_FromHour.Items.Clear();
        ddl_ToHour.Items.Clear();
        for (int i = 0; i < 25; i++)
        {
            string str = (i < 10) ? "0" + i : i.ToString();
            ListItem li1 = new ListItem(str, str);
            ListItem li2 = new ListItem(str, str);
            if (i != 24)
                ddl_FromHour.Items.Add(li1);
            if (i != 0)
                ddl_ToHour.Items.Add(li2);
        }
        ddl_FromHour.SelectedIndex = 0;
        ddl_ToHour.SelectedIndex = 23;
        DefaultIntervalReportV = new DefaultIntervalReport(Everyday, int.Parse(ddl_FromHour.SelectedValue), int.Parse(ddl_ToHour.SelectedValue));
    }
    void SetDatePicker()
    {
        FromToDatePicker_current.SetFromSentence = lbl_CreationDate.Text;
        FromToDatePicker_current.SetIntervalDateServer(DAYS_INTERVAL);
    }
    private void LoadConversionDetails()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfExposuresPicklistsContainer result = null;
        try
        {
            result = _site.GetExposuresPicklists();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        WebReferenceSite.ExposuresPicklistsContainer Exposures = result.Value;
        ddl_Origin.Items.Clear();
        lb_Origin.Items.Clear();
        ddl_Origin.Items.Add(new ListItem(string.Empty, Guid.Empty.ToString()));
    //    lb_Origin.Items.Add(new ListItem(string.Empty, Guid.Empty.ToString()));
        foreach (WebReferenceSite.GuidStringPair owp in Exposures.Origins)
        {
            ddl_Origin.Items.Add(new ListItem(owp.Name, owp.Id.ToString()));
            lb_Origin.Items.Add(new ListItem(owp.Name, owp.Id.ToString()));
        }
        lb_Origin.SelectedIndex = 0;
     //   lb_Origin.Style.Add(HtmlTextWriterStyle.Display, "none");
    }
    /*
    private void LoadStringsToDdl(DropDownList _ddl, string[] strs)
    {
        _ddl.Items.Add(new ListItem(string.Empty));
        foreach (string str in strs)
        {
            _ddl.Items.Add(new ListItem(str));
        }
        _ddl.SelectedIndex = 0;
    }
     * */
    protected void ddl_Origin_SelectedIndexChanged(object sender, EventArgs e)
    {
        ExecReport();
        /*
   //     ddl_Website.Items.Clear();
        Guid _id = new Guid(ddl_Origin.SelectedValue);  
      
        if (ConversionReportRequestV == null)
            return;
        WebReferenceReports.ConversionReportRequest _request = ConversionReportRequestV;
        _request.OriginId = _id;
        _request.WebsiteId = Guid.Empty;
        ConversionReportRequestV = _request;
        ExecReport(_request);
         * */
    }


    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (!string.IsNullOrEmpty(PathV))
        {
            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSLine.swf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + _chart + "');", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CleanChart",
                "CleanChart();", true);

    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lbl_ConversionReport.Text);
    }
    private void LoadExpertises()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);
        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;
    }

    protected void btn_Run_Click(object sender, EventArgs e)
    {
       
          ExecReport();
        
    }
    protected void btn_UpdateChart_Click(object sender, EventArgs e)
    {
        eConversionChartType _type;
        if (!Enum.TryParse(rbl_Chart.SelectedValue, out _type))
            _type = eConversionChartType.Regular_view;
        if (_type == eConversionChartType.Regular_view)
            return;
        btn_UpdateChart.Style.Add(HtmlTextWriterStyle.Display, "block");
        WebReferenceReports.ConversionReportUsersRequest _request = new WebReferenceReports.ConversionReportUsersRequest();
        Guid ExpertiseId;
        if (!Guid.TryParse(ddl_Heading.SelectedValue, out ExpertiseId))
            ExpertiseId = Guid.Empty;
        int CountryId;
        if (!int.TryParse(ddl_Country.SelectedValue, out CountryId))
            CountryId = -1;
        _request.ExpertiseId = ExpertiseId;
        _request.CountryId = CountryId;
        _request.FromDate = FromToDatePicker_current.GetDateFrom;
        _request.ToDate = FromToDatePicker_current.GetDateTo.AddDays(1);
        WebReferenceReports.eConversionInterval eci;
        if (!Enum.TryParse(ddl_Interval.SelectedValue, out eci))
            eci = WebReferenceReports.eConversionInterval.DAY;
        _request.Interval = eci;
        List<Guid> list = new List<Guid>();
        foreach (ListItem li in lb_Origin.Items)
        {
            if (li.Selected)
            {
                Guid id;
                if (!Guid.TryParse(li.Value, out id))
                    continue;
                list.Add(id);
            }
        }
        _request.Origins = list.ToArray();
        _request.SliderType = int.Parse(ddl_type.SelectedValue);
        _request.interval_time_report = GetIntervalTimeReport();
        if (_type == eConversionChartType.EXP__KDAUS || _type == eConversionChartType.Requests__KDAUS)
        {
            UserChart_ExpKDAUS_RequestKDAUS(_request, _type);
            return;
        }
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfConversionUsersReportResponse result = null;
        _reports.Timeout = 180000;
        try
        {
            result = _reports.ConversionReportUsers(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        SetUsersChart(result.Value, _request, _type);
    }

    private void UserChart_ExpKDAUS_RequestKDAUS(WebReferenceReports.ConversionReportUsersRequest _request, eConversionChartType _type)
    {
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfConversionUsersReportExposureResponse result = null;
        try
        {
            result = _reports.ConversionReportUsersExposures(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        SetUsersChart(result.Value, _request, _type);
    }
    
    
    WebReferenceReports.IntervalTimeReport GetIntervalTimeReport()
    {
        WebReferenceReports.IntervalTimeReport _interval_time_report = null;
        int _from = int.Parse(ddl_FromHour.SelectedValue);
        int _to = int.Parse(ddl_ToHour.SelectedValue);
        if (!DefaultIntervalReportV.IsDefault(ddl_DayOfWeek.SelectedValue, _from, _to))
        {
            _interval_time_report = new WebReferenceReports.IntervalTimeReport();
            WebReferenceReports.DayOfWeek dof;
            if (Enum.TryParse(ddl_DayOfWeek.SelectedValue, out dof))
                _interval_time_report.day = dof;
            else
                _interval_time_report.day = null;
            _interval_time_report.from = _from;
            _interval_time_report.to = (_to - 1);
            //Check if valid in server
            if (_interval_time_report.from > _interval_time_report.to)
                _interval_time_report.to = _interval_time_report.from;

        }
        return _interval_time_report;
    }
    void ExecReport()
    {
    //    lb_Origin.Style.Add(HtmlTextWriterStyle.Display, "none");
   //     ddl_Origin.Style.Add(HtmlTextWriterStyle.Display, "block");
        btn_UpdateChart.Style.Add(HtmlTextWriterStyle.Display, "none");
        lb_Origin.SelectedIndex = 0;
        rbl_Chart.SelectedIndex = 0;
        WebReferenceReports.ConversionReportRequest _request = new WebReferenceReports.ConversionReportRequest();
        
        Guid ExpertiseId;
        if (!Guid.TryParse(ddl_Heading.SelectedValue, out ExpertiseId))
            ExpertiseId = Guid.Empty;
        int CountryId;
        if (!int.TryParse(ddl_Country.SelectedValue, out CountryId))
            CountryId = -1;
        _request.ExpertiseId = ExpertiseId;
        _request.FromDate = FromToDatePicker_current.GetDateFrom;
        _request.ToDate = FromToDatePicker_current.GetDateTo.AddDays(1);
        _request.ControlName = ddl_ControlName.SelectedValue;
        _request.CountryId = CountryId;
        WebReferenceReports.eConversionInterval eci;
        if (!Enum.TryParse(ddl_Interval.SelectedValue, out eci))
            eci = WebReferenceReports.eConversionInterval.DAY;
        _request.Interval = eci;
        Guid OriginId;
        if (!Guid.TryParse(ddl_Origin.SelectedValue, out OriginId))
            OriginId = Guid.Empty;
        _request.OriginId = OriginId;
        _request.SliderType = int.Parse(ddl_type.SelectedValue);
        /*
        int _from = int.Parse(ddl_FromHour.SelectedValue);
        int _to = int.Parse(ddl_ToHour.SelectedValue);
        if (!DefaultIntervalReportV.IsDefault(ddl_DayOfWeek.SelectedValue, _from, _to))
        {
            _request.interval_time_report = new WebReferenceReports.IntervalTimeReport();
            WebReferenceReports.DayOfWeek dof;
            if (Enum.TryParse(ddl_DayOfWeek.SelectedValue, out dof))
                _request.interval_time_report.day = dof;
            else
                _request.interval_time_report.day = null;
            _request.interval_time_report.from = _from;
            _request.interval_time_report.to = (_to - 1);
            //Check if valid in server
            if (_request.interval_time_report.from > _request.interval_time_report.to)
                _request.interval_time_report.to = _request.interval_time_report.from;
                
        }
         * */
        _request.interval_time_report = GetIntervalTimeReport();
        ConversionReportRequestV = _request;
        ExecReport(_request);

        /*
        result.Value[0].CallCount;
        result.Value[0].CallsPercent;
        result.Value[0].Date;
        result.Value[0].ExposureCount;
        result.Value[0].RequestCount;
        result.Value[0].RequestPercent;
        */

    }
    string GetNumString(decimal num, string EndWith)
    {
        return SalesUtility.GetNumberForReportNonNegative(num, NUMBER_FORMAT, EndWith);
    }
    string GetNumString(double num, string EndWith)
    {
        return SalesUtility.GetNumberForReportNonNegative(num, NUMBER_FORMAT, EndWith);
    }
    string GetNumString(decimal num)
    {
        return SalesUtility.GetNumberForReportNonNegative(num, NUMBER_FORMAT, null);
    }
    string GetNumString(double num)
    {
        return SalesUtility.GetNumberForReportNonNegative(num, NUMBER_FORMAT, null);
    }
    void ExecReport(WebReferenceReports.ConversionReportRequest _request)
    {
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfConversionReportResponse result = null;
        _reports.Timeout = 180000;
        try
        {
            result = _reports.ConversionReport2(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        DateTime requestsLastUpdate = result.Value.RequestsLastUpdate;
        DateTime expousresLastUpdate = result.Value.ExposuresLastUpdate;

        //     DataTable data = GetDataTable.GetDataTableFromListCorrectNumberDateTimeFormat(result.Value, siteSetting.DateFormat);
        DataTable data = new DataTable();
        data.Columns.Add("CallCount", typeof(string));
        data.Columns.Add("CallsPercent", typeof(string));
        data.Columns.Add("Date", typeof(string));
        data.Columns.Add("ExposureCount", typeof(string));
        data.Columns.Add("LogicHits", typeof(string));
        data.Columns.Add("Upsale", typeof(string));
        data.Columns.Add("RequestCount", typeof(string));
        data.Columns.Add("UniqueInstalls");

        data.Columns.Add("TotalRequests_Exposures", typeof(string));
        data.Columns.Add("StoppedRequests", typeof(string));
        data.Columns.Add("SaleRate", typeof(string));
        data.Columns.Add("NonUpsalesCount", typeof(string));
        data.Columns.Add("OriginalRequests_Exposures", typeof(string));
        data.Columns.Add("DateString", typeof(string));
        

        data.Columns.Add("DAUS", typeof(string));
        data.Columns.Add("Exp_KDAUs", typeof(string));
        data.Columns.Add("Req_KDAUs", typeof(string));
        data.Columns.Add("LogicHits_DAUs", typeof(string));
      //  data.Columns.Add("Req_KDAUs__KDAUs", typeof(string));
        data.Columns.Add("Revenue_KDAUs", typeof(string));
    //    data.Columns.Add("Conversion_KDAUs", typeof(string));

        data.Columns.Add("Hits_KDAUs", typeof(string));
        data.Columns.Add("Hits_Exposures", typeof(string));
        data.Columns.Add("CPM", typeof(string));
        data.Columns.Add("RPM", typeof(string));
        data.Columns.Add("LogicHits_Exposure", typeof(string));
      
        if (_request.Interval != WebReferenceReports.eConversionInterval.DAILY_AVERAGE)
        {
            foreach (WebReferenceReports.ConversionReportRow crr in result.Value.DataList)
            {
                DataRow row = data.NewRow();
                row["CallCount"] = crr.CallCount + "";
                row["CallsPercent"] = GetNumString(crr.CallsPercent, PERCENT);
                row["Date"] = _request.Interval == WebReferenceReports.eConversionInterval.HOUR ? crr.Date.ToString() : string.Format(DATE_FORMAT_DAY, crr.Date);
                row["DateString"] = crr.Date.Ticks.ToString();
                row["ExposureCount"] = crr.ExposureCount + "";
                row["RequestCount"] = crr.RequestCount + "";
                row["TotalRequests_Exposures"] = GetNumString(crr.TotalRequests_Exposures, PERCENT);
                row["Upsale"] = crr.UpsalesCount.ToString();
                row["StoppedRequests"] = crr.StoppedRequests + "";
                row["NonUpsalesCount"] = crr.NonUpsalesCount + "";
                row["OriginalRequests_Exposures"] = GetNumString(crr.OriginalRequests_Exposures, PERCENT);
                row["SaleRate"] = GetNumString(crr.SaleRate, PERCENT);
                row["LogicHits"] = crr.LogicHits + "";
                row["LogicHits_DAUs"] = GetNumString(crr.LogicHits_DAUS);

                row["DAUS"] = crr.DAUS + "";
                row["Exp_KDAUs"] = GetNumString(crr.Exposure_KDAUs);
                row["Req_KDAUs"] = GetNumString(crr.Request_KDAUs);
                row["Revenue_KDAUs"] = GetNumString(crr.Revenue_KDAUs);

                row["Hits_Exposures"] = GetNumString(crr.ExpHits_Exposures);
                row["Hits_KDAUs"] = GetNumString(crr.ExpHits_KDAUs);

                row["UniqueInstalls"] = crr.UniqueInstalls;
                row["CPM"] = GetNumString(crr.CPM);
                row["RPM"] = GetNumString(crr.RPM);
                row["LogicHits_Exposure"] = GetNumString(crr.LogicHits_Exposure);
               
                data.Rows.Add(row);
            }
        }
        else
        {            
            DataRow row = data.NewRow();
            row["CallCount"] = GetNumString(result.Value.DataList.Average(x => x.CallCount));
            double _CallsPercent = result.Value.DataList.Average(x => x.CallsPercent);
            row["CallsPercent"] = GetNumString(_CallsPercent, PERCENT);
            row["Date"] = "Average";
            row["DateString"] = _request.FromDate.Ticks.ToString() + ";" + _request.ToDate.Ticks.ToString();
            row["ExposureCount"] = GetNumString(result.Value.DataList.Average(x => x.ExposureCount));
            row["RequestCount"] = GetNumString(result.Value.DataList.Average(x => x.RequestCount));
            double _TotalRequests_Exposures = result.Value.DataList.Average(x => x.TotalRequests_Exposures);
            row["TotalRequests_Exposures"] = GetNumString(_TotalRequests_Exposures, PERCENT);
            row["Upsale"] = GetNumString(result.Value.DataList.Average(x => x.UpsalesCount));
            row["StoppedRequests"] = GetNumString(result.Value.DataList.Average(x => x.StoppedRequests)); 
            row["NonUpsalesCount"] = GetNumString(result.Value.DataList.Average(x => x.NonUpsalesCount));
            double _OriginalRequests_Exposures = result.Value.DataList.Average(x => x.OriginalRequests_Exposures);
            row["OriginalRequests_Exposures"] = GetNumString(_OriginalRequests_Exposures, PERCENT);
            double _SaleRate = result.Value.DataList.Average(x => x.SaleRate);
            row["SaleRate"] = GetNumString(_SaleRate, PERCENT);

            row["LogicHits"] = result.Value.DataList.Average(x => x.LogicHits) + "";
            row["LogicHits_DAUs"] = GetNumString(result.Value.DataList.Average(x => x.LogicHits_DAUS));

            row["DAUS"] = GetNumString(result.Value.DataList.Average(x => x.DAUS));
            row["Exp_KDAUs"] = GetNumString(result.Value.DataList.Average(x => x.Exposure_KDAUs)); 
            row["Req_KDAUs"] = GetNumString(result.Value.DataList.Average(x => x.Request_KDAUs));
            row["Revenue_KDAUs"] = GetNumString(result.Value.DataList.Average(x => x.Revenue_KDAUs));
        //    row["Conversion_KDAUs"] = string.Format(NUMBER_FORMAT, result.Value.DataList.Average(x => x.Conversion_KDAUs));

            row["Hits_Exposures"] = GetNumString(result.Value.DataList.Average(x => x.ExpHits_Exposures));
            row["Hits_KDAUs"] = GetNumString(result.Value.DataList.Average(x => x.ExpHits_KDAUs));
            row["UniqueInstalls"] = GetNumString(result.Value.DataList.Average(x => x.UniqueInstalls));

            row["CPM"] = GetNumString(result.Value.DataList.Average(x => x.CPM));
            row["RPM"] = GetNumString(result.Value.DataList.Average(x => x.RPM));
            row["LogicHits_Exposure"] = GetNumString(result.Value.DataList.Average(x => x.LogicHits_Exposure));
            
            data.Rows.Add(row);
        }
        _conversion.LoadData(data, true, _request.CountryId);
        long _users = GetTotalUsers(result.Value.DataList);
        lbl_RecordMached.Text = _Toolbox.GetRecordMaches(data.Rows.Count);
        lblLastUpdate.Text = GetLastUpdate(requestsLastUpdate);
        lblExposureUpdate.Text = GetExposureUpdate(expousresLastUpdate);
        lblTotalUsers.Text = GetTotalUsers(_users);
        lblMonthlyUserValue.Text = GetMonthlyUserValue(_users, result.Value.DataList);
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecordDisplay", "alert('" + _Toolbox.NoRecordToPrint + "');", true);
            PathV = string.Empty;
        }
        else
            LoadChart(data, _request.Interval== WebReferenceReports.eConversionInterval.HOUR);
        _UpdatePanel.Update();
        dataV = data;
    }
    string GetMonthlyUserValue(long _users, WebReferenceReports.ConversionReportRow[] rows)
    {
        decimal revenueSum;
        if (rows == null || rows.Length == 0)
            revenueSum = 0m;
        else
            revenueSum = (from x in rows
                     select x.Revenue).Sum();
        decimal _MonthlyUser = (_users == 0) ? 0m : (revenueSum / (decimal)_users) * 30m * 100m;
        return lbl_MonthlyUserValue.Text + ": " + string.Format(NUMBER_FORMAT, _MonthlyUser);
    }
    string GetLastUpdate(DateTime dt)
    {
        return HttpUtility.HtmlEncode(lbl_RequestUpdate.Text + ": " + dt);
    }
    string GetExposureUpdate(DateTime dt)
    {
        return HttpUtility.HtmlEncode(lbl_ExposureUpdate.Text + ": " + dt);
    }
    long GetTotalUsers(WebReferenceReports.ConversionReportRow[] rows)
    {
        long users;
        if(rows == null || rows.Length == 0)
            users = 0;
        else
            users = (from x in rows
                     select ((long)x.DAUS)).Sum();
   //     return lbl_TotalUsers + ": " + users;
        return users;
    }
    string GetTotalUsers(long _users)
    {

        return lbl_TotalUsers.Text + ": " + string.Format(NUMBER_FORMAT, _users);
    }
    private void SetToolboxEvents()
    {

        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
        _Toolbox.SetButtonPrintAsAsync();
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, lbl_ConversionReport.Text);

        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordToPrint + "');", true);
            return;
        }
        Session["data_print"] = dataV;
        PageRenderin page = new PageRenderin();
        UserControl ctl = (UserControl)page.LoadControl(ResolveUrl("~/Publisher/SalesControls/ConversionControl.ascx"));
        MethodInfo LoadData = ctl.GetType().GetMethod("LoadData");
        LoadData.Invoke(ctl, new object[] { dataV });
        ctl.EnableViewState = false;
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        Session["string_print"] = sb.ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }


    #region  chart
    void SetUsersChart(WebReferenceReports.ConversionUsersReportResponse[] users, WebReferenceReports.ConversionReportUsersRequest _request, eConversionChartType _type)
    {
        bool IsValueUser = _type == eConversionChartType.User_Value;
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + Utilities.GetEnumStringFormat(_type) + @"' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='3' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");
        sb.Append(@"numberSuffix='' ");
        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar=' ' ");

        sb.Append(@"bgColor='FFFFFF'>");


        /*
        int modulu = data.Rows.Count / 15;
        modulu++;
        */

        sb.Append(@"<categories>");
        
        var query = from x in users
                    group x by x.OriginId into qOrigin
                    select new
                    {
                        Origin = new KeyValuePair<Guid, string>(qOrigin.Key, qOrigin.FirstOrDefault().OriginName),
                        item = from y in qOrigin
                                group y by y.date into qDate
                                select new
                                {
                                    date = qDate.Key,
                                    _daus = qDate.FirstOrDefault().DAUS,
                                    revenue = qDate.FirstOrDefault().Revenue
                             //       value = new KeyValuePair<DateTime, int>(qDate.Key, qDate.FirstOrDefault().DAUS),
                                }
                    };
        Dictionary<Guid, StringBuilder> dic = new Dictionary<Guid, StringBuilder>();
        Random rnd = new Random();
        if (_request.Interval != WebReferenceReports.eConversionInterval.DAILY_AVERAGE)
        {
            DateTime MaxDate = _request.ToDate.AddDays(-1);//users.Max(x => x.date);
            DateTime MinDate;// = _request.FromDate;//users.Min(x => x.date);
            if (_request.interval_time_report == null)
            {
                //  MaxDate = _request.ToDate.AddDays(-1);
                MinDate = _request.FromDate;
            }
            else
            {
                System.DayOfWeek _dayofweek;
                if (!Enum.TryParse(_request.interval_time_report.day.ToString(), out _dayofweek))
                    _dayofweek = System.DayOfWeek.Monday;
                int daysUntilDay = ((int)_dayofweek - (int)_request.FromDate.DayOfWeek + 7) % 7;
                MinDate = _request.FromDate.AddDays(daysUntilDay);
                daysUntilDay = ((int)_dayofweek - (int)(MaxDate.AddDays(-7)).DayOfWeek + 7) % 7;
                MaxDate = MaxDate.AddDays(-7).AddDays(daysUntilDay);
            }
            while (MinDate <= MaxDate)
            {
                string _title = "";
                foreach (var Origin in query)
                {
                    if (!dic.ContainsKey(Origin.Origin.Key))
                        dic.Add(Origin.Origin.Key, new StringBuilder(@"<dataset seriesName='" + Origin.Origin.Value + @"' color='" +
                            Utilities.GenerateColor(rnd) + @"' >"));
                    var SubQuery = from x in Origin.item
                                   where x.date == MinDate
                                   select x;
                    int daus;
                    if (SubQuery.Count() == 0)
                        daus = 0;
                    else
                        daus = SubQuery.FirstOrDefault()._daus;
                    if (IsValueUser)
                    {
                        decimal user_value = (daus == 0) ? 0m : (SubQuery.FirstOrDefault().revenue / (decimal)daus) * 30m * 100m;
                        dic[Origin.Origin.Key].Append(@"<set value='" + user_value + "'/>");
                        _title += Origin.Origin.Value + "=" + string.Format(this.GetNumberFormat, user_value) + "\r\n";
                    }
                    else
                    {
                        dic[Origin.Origin.Key].Append(@"<set value='" + daus + "'/>");
                        _title += Origin.Origin.Value + "=" + string.Format(this.GetNumberFormat, daus) + "\r\n";
                    }
                }
                sb.Append(@"<category name='" + string.Format(siteSetting.DateFormat, MinDate) + @"' toolText='" + _title + "'/>");
                if (_request.interval_time_report != null || _request.Interval == WebReferenceReports.eConversionInterval.WEEK)
                    MinDate = MinDate.AddDays(7);
                else if (_request.Interval == WebReferenceReports.eConversionInterval.DAY || _request.Interval == WebReferenceReports.eConversionInterval.HOUR)
                    MinDate = MinDate.AddDays(1);
                else
                    MinDate = MinDate.AddMonths(1);
            }
        }
        else
        {
            DateTime MaxDate = _request.ToDate.AddDays(-1);//users.Max(x => x.date);
            DateTime MinDate;// = _request.FromDate;//users.Min(x => x.date);
            if (_request.interval_time_report == null)
            {
                //  MaxDate = _request.ToDate.AddDays(-1);
                MinDate = _request.FromDate;
            }
            else
            {
                System.DayOfWeek _dayofweek;
                if (!Enum.TryParse(_request.interval_time_report.day.ToString(), out _dayofweek))
                    _dayofweek = System.DayOfWeek.Monday;
                int daysUntilDay = ((int)_dayofweek - (int)_request.FromDate.DayOfWeek + 7) % 7;
                MinDate = _request.FromDate.AddDays(daysUntilDay);
                daysUntilDay = ((int)_dayofweek - (int)(MaxDate.AddDays(-7)).DayOfWeek + 7) % 7;
                MaxDate = MaxDate.AddDays(-7).AddDays(daysUntilDay);
            }
            Dictionary<Guid, Dictionary<double, decimal>> dicAvg = new Dictionary<Guid, Dictionary<double, decimal>>();
            while (MinDate <= MaxDate)
            {
                foreach (var Origin in query)
                {
                    if (!dicAvg.ContainsKey(Origin.Origin.Key))
                        dicAvg.Add(Origin.Origin.Key, new Dictionary<double, decimal>());
                    var SubQuery = from x in Origin.item
                                   where x.date == MinDate
                                   select x;
                //    double _daus;// = Origin.item.Average(x => x._daus);
                //    decimal _revenue;// = Origin.item.Average(x => x.revenue);
                    if (SubQuery.Count() == 0)
                    {
                        dicAvg[Origin.Origin.Key].Add(0,0);
                    }
                    else
                    {
                        dicAvg[Origin.Origin.Key].Add(SubQuery.FirstOrDefault()._daus, SubQuery.FirstOrDefault().revenue);
                    }
                

                }
                if (_request.interval_time_report == null)
                    MinDate = MinDate.AddDays(1);
                else
                    MinDate = MinDate.AddDays(7);
            }

            string _title = "";
            foreach (KeyValuePair<Guid, Dictionary<double, decimal>> kvp in dicAvg)
            {
                string OriginName = (from x in query
                                     where x.Origin.Key == kvp.Key
                                     select x.Origin.Value).FirstOrDefault();
                if (!dic.ContainsKey(kvp.Key))
                    dic.Add(kvp.Key, new StringBuilder(@"<dataset seriesName='" + OriginName + @"' color='" +
                        Utilities.GenerateColor(rnd) + @"' >"));

                var SubQuery = from x in kvp.Value
                               select x;        


                if (IsValueUser)
                {
                    double avgDaus;
                    decimal avdRevenue;
                    if(SubQuery.Count() == 0)
                    {
                        avgDaus = 0;
                        avdRevenue = 0;
                    }
                    else
                    {
                        avgDaus = SubQuery.Average(x => x.Key);
                        avdRevenue = SubQuery.Average(x => x.Value);
                    }
                    decimal user_value = (avgDaus == 0) ? 0m : (avdRevenue / (decimal)avgDaus) * 30m * 100m;
                    dic[kvp.Key].Append(@"<set value='" + user_value + "'/>");
                    _title += OriginName + "=" + string.Format(this.GetNumberFormat, user_value) + "\r\n";
                }
                else
                {
                    double avgDaus;
                    if(SubQuery.Count() == 0)
                        avgDaus = 0;
                    else
                        avgDaus = SubQuery.Average(x => x.Key);
                    dic[kvp.Key].Append(@"<set value='" + avgDaus + "'/>");
                    _title += OriginName + "=" + string.Format(this.GetNumberFormat, avgDaus) + "\r\n";
                }
            }
            sb.Append(@"<category name='Average' toolText='" + _title + "'/>");
            /*
            string _title = "";
            foreach (var Origin in query)
            {
                if (!dic.ContainsKey(Origin.Origin.Key))
                    dic.Add(Origin.Origin.Key, new StringBuilder(@"<dataset seriesName='" + Origin.Origin.Value + @"' color='" +
                        Utilities.GenerateColor(rnd) + @"' >"));
                double _daus = Origin.item.Average(x => x._daus);
                decimal _revenue = Origin.item.Average(x => x.revenue);
                if (IsValueUser)
                {
                    decimal user_value = (_daus == 0) ? 0m : (_revenue / (decimal)_daus) * 30m * 100m;
                    dic[Origin.Origin.Key].Append(@"<set value='" + user_value + "'/>");
                    _title += Origin.Origin.Value + "=" + string.Format(this.GetNumberFormat, user_value) + "\r\n";
                }
                else
                {
                    dic[Origin.Origin.Key].Append(@"<set value='" + _daus + "'/>");
                    _title += Origin.Origin.Value + "=" + string.Format(this.GetNumberFormat, _daus) + "\r\n";
                }
            }
            sb.Append(@"<category name='Average' toolText='" + _title + "'/>");           
             * */
        }
      
        sb.Append("</categories>");
        foreach (KeyValuePair<Guid, StringBuilder> kvp in dic)
        {
            kvp.Value.Append(@"</dataset>");
            sb.Append(kvp.Value.ToString());
        }
        sb.Append(@"</graph>");
        SetChartXML(sb);
        
    }
    void SetUsersChart(WebReferenceReports.ConversionUsersReportExposureResponse[] users, WebReferenceReports.ConversionReportUsersRequest _request, eConversionChartType _type)
    {
        
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + Utilities.GetEnumStringFormat(_type) + @"' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='3' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");
        sb.Append(@"numberSuffix='' ");
        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar=' ' ");

        sb.Append(@"bgColor='FFFFFF'>");


        /*
        int modulu = data.Rows.Count / 15;
        modulu++;
        */
       
        sb.Append(@"<categories>");

        var query = from x in users
                    group x by x.OriginId into qOrigin
                    select new
                    {
                        Origin = new KeyValuePair<Guid, string>(qOrigin.Key, qOrigin.FirstOrDefault().OriginName),
                        item = from y in qOrigin
                               group y by y.date into qDate
                               select new
                               {
                                   date = qDate.Key,
                               //    _daus = qDate.FirstOrDefault().DAUS,
                              //     _Exp = qDate.FirstOrDefault().Exposure,
                                   EXP_KDAUS = qDate.FirstOrDefault().EXP_KDAUS,
                                   Request_KDAUS = qDate.FirstOrDefault().Request_KDAUS
                                   //       value = new KeyValuePair<DateTime, int>(qDate.Key, qDate.FirstOrDefault().DAUS),
                               }
                    };
        Dictionary<Guid, StringBuilder> dic = new Dictionary<Guid, StringBuilder>();
        Random rnd = new Random();
        if (_request.Interval != WebReferenceReports.eConversionInterval.DAILY_AVERAGE)
        {
            
            DateTime MaxDate =  _request.ToDate.AddDays(-1);//users.Max(x => x.date);
            DateTime MinDate;// = _request.FromDate;//users.Min(x => x.date);
            if(_request.interval_time_report == null)
            {
               //  MaxDate = _request.ToDate.AddDays(-1);
                 MinDate = _request.FromDate;
            }
            else
            {
                System.DayOfWeek _dayofweek;
                if (!Enum.TryParse(_request.interval_time_report.day.ToString(), out _dayofweek))
                    _dayofweek = System.DayOfWeek.Monday;
                int daysUntilDay = ((int)_dayofweek - (int)_request.FromDate.DayOfWeek + 7) % 7;
                MinDate = _request.FromDate.AddDays(daysUntilDay);
                daysUntilDay = ((int)_dayofweek - (int)(MaxDate.AddDays(-7)).DayOfWeek + 7) % 7;
                MaxDate = MaxDate.AddDays(-7).AddDays(daysUntilDay);
            }
            while (MinDate <= MaxDate)
            {
                string _title = "";
                foreach (var Origin in query)
                {
                    if (!dic.ContainsKey(Origin.Origin.Key))
                        dic.Add(Origin.Origin.Key, new StringBuilder(@"<dataset seriesName='" + Origin.Origin.Value + @"' color='" +
                            Utilities.GenerateColor(rnd) + @"' >"));
                    var SubQuery = from x in Origin.item
                                   where x.date == MinDate
                                   select x;
                    double _value;
                    if (SubQuery.Count() == 0)
                        _value = 0;
                    else
                    {
                        if (_type == eConversionChartType.EXP__KDAUS)
                            _value = SubQuery.FirstOrDefault().EXP_KDAUS;
                        else
                            _value = SubQuery.FirstOrDefault().Request_KDAUS;
                    }

                    dic[Origin.Origin.Key].Append(@"<set value='" + _value + "'/>");
                    _title += Origin.Origin.Value + "=" + string.Format(GetNumberFormat, _value) + "\r\n";
                    
                }
                sb.Append(@"<category name='" + string.Format(siteSetting.DateFormat, MinDate) + @"' toolText='" + _title + "'/>");
                if (_request.interval_time_report != null || _request.Interval == WebReferenceReports.eConversionInterval.WEEK)
                    MinDate = MinDate.AddDays(7);
                else if (_request.Interval == WebReferenceReports.eConversionInterval.DAY || _request.Interval == WebReferenceReports.eConversionInterval.HOUR)                    
                    MinDate = MinDate.AddDays(1);
                else
                    MinDate = MinDate.AddMonths(1);
            }
        }
        else
        {
            DateTime MaxDate = _request.ToDate.AddDays(-1);//users.Max(x => x.date);
            DateTime MinDate;// = _request.FromDate;//users.Min(x => x.date);
            if (_request.interval_time_report == null)
            {
                //  MaxDate = _request.ToDate.AddDays(-1);
                MinDate = _request.FromDate;
            }
            else
            {
                System.DayOfWeek _dayofweek;
                if (!Enum.TryParse(_request.interval_time_report.day.ToString(), out _dayofweek))
                    _dayofweek = System.DayOfWeek.Monday;
                int daysUntilDay = ((int)_dayofweek - (int)_request.FromDate.DayOfWeek + 7) % 7;
                MinDate = _request.FromDate.AddDays(daysUntilDay);
                daysUntilDay = ((int)_dayofweek - (int)(MaxDate.AddDays(-7)).DayOfWeek + 7) % 7;
                MaxDate = MaxDate.AddDays(-7).AddDays(daysUntilDay);
            }
            Dictionary<Guid, List<double>> dicAvg = new Dictionary<Guid, List<double>>();
            while (MinDate <= MaxDate)
            {
                foreach (var Origin in query)
                {
                    if (!dicAvg.ContainsKey(Origin.Origin.Key))
                        dicAvg.Add(Origin.Origin.Key, new List<double>());
                    var SubQuery = from x in Origin.item
                                   where x.date == MinDate
                                   select x;

                    if (SubQuery.Count() == 0)
                        dicAvg[Origin.Origin.Key].Add(0);
                    else
                    {
                        if(_type == eConversionChartType.EXP__KDAUS)
                            dicAvg[Origin.Origin.Key].Add(SubQuery.FirstOrDefault().EXP_KDAUS);
                        else
                            dicAvg[Origin.Origin.Key].Add(SubQuery.FirstOrDefault().Request_KDAUS);
                    }
                    
                }
                if (_request.interval_time_report != null)
                    MinDate = MinDate.AddDays(7);
                else
                    MinDate = MinDate.AddDays(1);
            }

            string _title = "";
            foreach (KeyValuePair<Guid, List<double>> kvp in dicAvg)
            {
                string OriginName = (from x in query
                                     where x.Origin.Key == kvp.Key
                                     select x.Origin.Value).FirstOrDefault();
                if (!dic.ContainsKey(kvp.Key))
                    dic.Add(kvp.Key, new StringBuilder(@"<dataset seriesName='" + OriginName + @"' color='" +
                        Utilities.GenerateColor(rnd) + @"' >"));

                var SubQuery = from x in kvp.Value
                               select x;

                double _avg = (SubQuery.Count() == 0) ? 0 : SubQuery.Average();
                dic[kvp.Key].Append(@"<set value='" + _avg + "'/>");
                _title += OriginName + "=" + string.Format(this.GetNumberFormat, _avg) + "\r\n";
                
            }
            sb.Append(@"<category name='Average' toolText='" + _title + "'/>");
        }

        sb.Append("</categories>");
        foreach (KeyValuePair<Guid, StringBuilder> kvp in dic)
        {
            kvp.Value.Append(@"</dataset>");
            sb.Append(kvp.Value.ToString());
        }
        sb.Append(@"</graph>");
        SetChartXML(sb);

    }
    bool LoadChart(DataTable data, bool IsHours)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + lbl_ConversionReport.Text + @"' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='3' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");
        sb.Append(@"numberSuffix='' ");
        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar=' ' ");

        sb.Append(@"bgColor='FFFFFF'>");



        int modulu = data.Rows.Count / 15;
        modulu++;

        
        sb.Append(@"<categories>");
        StringBuilder sbRequestsPercent = new StringBuilder();
        StringBuilder sbCallsPercent = new StringBuilder();
        StringBuilder sbExposures = new StringBuilder();
        StringBuilder sbCalls = new StringBuilder();
        StringBuilder sbTotalRequests = new StringBuilder();
        StringBuilder sbRequests = new StringBuilder();
        StringBuilder sbExposure_KDAUs = new StringBuilder();
        sbRequestsPercent.Append(@"<dataset seriesName='" + lbl_RequestsPercent.Text + @"' color='FFFF00' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
        sbCallsPercent.Append(@"<dataset seriesName='" + lbl_CallsPercent.Text + @"' color='0000A0'  >");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'
        sbExposures.Append(@"<dataset seriesName='" + "Exposures" + @"' color='808080' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
        sbCalls.Append(@"<dataset seriesName='" + "Calls" + @"' color='FF0000'  >");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'
        sbTotalRequests.Append(@"<dataset seriesName='" + "Total Requests" + @"' color='00CC00' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
        sbRequests.Append(@"<dataset seriesName='" + "Requests" + @"' color='0080C0'  >");
        sbExposure_KDAUs.Append(@"<dataset seriesName='" + "Exposure/KDAUs" + @"' color='000000'  >");
        for (int i = data.Rows.Count - 1; i > -1; i--)
        {
            string RequestsPercent = data.Rows[i]["TotalRequests_Exposures"].ToString();
            string CallsPercent = data.Rows[i]["CallsPercent"].ToString();


            double __exposures = double.Parse(data.Rows[i]["ExposureCount"].ToString());
            string _exposures = ((int)(__exposures / 100.0)).ToString();
            string _calls = data.Rows[i]["CallCount"].ToString();
            string _total_request = data.Rows[i]["RequestCount"].ToString();
            string _requests = data.Rows[i]["NonUpsalesCount"].ToString();
            string _Exposure_KDAUs = data.Rows[i]["Exp_KDAUs"].ToString();
            string _title = lbl_RequestsPercent.Text + "=" + RequestsPercent + "\r\n" +
                lbl_CallsPercent.Text + "=" + CallsPercent + "\r\n" +
                "Exposures=" + __exposures + "\r\n" +
                "Calls=" + _calls + "\r\n" +
            "TotalRequest=" + _total_request + "\r\n" +
                "Requests=" + _requests + "\r\n" +
                "Exposure/KDAUs=" + _Exposure_KDAUs + "\r\n";
            sb.Append(@"<category name='" + data.Rows[i]["Date"].ToString() + @"' toolText='" + _title + "'/>");

            sbRequestsPercent.Append(@"<set value='" + RequestsPercent.Replace("%", "") + "'/>");
            sbCallsPercent.Append(@"<set value='" + CallsPercent.Replace("%", "") + "'/>");
            sbExposures.Append(@"<set value='" + _exposures + "'/>");
            sbCalls.Append(@"<set value='" + _calls + "'/>");
            sbTotalRequests.Append(@"<set value='" + _total_request + "'/>");
            sbRequests.Append(@"<set value='" + _requests + "'/>");
            sbExposure_KDAUs.Append(@"<set value='" + _Exposure_KDAUs + "'/>");
        }
        sbRequestsPercent.Append(@"</dataset>");
        sbCallsPercent.Append(@"</dataset>");
        sbExposures.Append(@"</dataset>");
        sbCalls.Append(@"</dataset>");
        sbTotalRequests.Append(@"</dataset>");
        sbRequests.Append(@"</dataset>");
        sbExposure_KDAUs.Append(@"</dataset>");
        sb.Append("</categories>");
        sb.Append(sbRequestsPercent.ToString());
        sb.Append(sbCallsPercent.ToString());       
        sb.Append(sbExposures.ToString());
        sb.Append(sbCalls.ToString());
        sb.Append(sbTotalRequests.ToString());
        sb.Append(sbRequests.ToString());
        sb.Append(sbExposure_KDAUs.ToString());
        /*
        if (!IsHours)
        {
            StringBuilder sbRequests = new StringBuilder();
            StringBuilder sbCalls = new StringBuilder();
            sbRequests.Append(@"<dataset seriesName='" + lbl_RequestsPercent.Text + @"' color='808080' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
            sbCalls.Append(@"<dataset seriesName='" + lbl_CallsPercent.Text + @"' color='0080C0'  >");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'

            for (int i = data.Rows.Count - 1; i > -1; i--)
            {
                string RequestsPercent = data.Rows[i]["RequestPercent"].ToString();
                string CallsPercent = data.Rows[i]["CallsPercent"].ToString();
                string _title = lbl_RequestsPercent.Text + "=" + RequestsPercent + "\r\n" +
                    lbl_CallsPercent.Text + "=" + CallsPercent + "\r\n";
                sb.Append(@"<category name='" + data.Rows[i]["Date"].ToString() + @"' toolText='" + _title + "'/>");

                sbRequests.Append(@"<set value='" + RequestsPercent.Replace("%", "") + "'/>");
                sbCalls.Append(@"<set value='" + CallsPercent.Replace("%", "") + "'/>");
            }
            sbRequests.Append(@"</dataset>");
            sbCalls.Append(@"</dataset>");
            sb.Append("</categories>");
            sb.Append(sbRequests.ToString());
            sb.Append(sbCalls.ToString());
        }
        else
        {
            StringBuilder sbExposures = new StringBuilder();
            StringBuilder sbCalls = new StringBuilder();
            StringBuilder sbTotalRequests = new StringBuilder();
            StringBuilder sbRequests = new StringBuilder();
            sbExposures.Append(@"<dataset seriesName='" + "Exposures" + @"' color='808080' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
            sbCalls.Append(@"<dataset seriesName='" + "Calls" + @"' color='FF0000'  >");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'
            sbTotalRequests.Append(@"<dataset seriesName='" + "Total Requests" + @"' color='00CC00' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
            sbRequests.Append(@"<dataset seriesName='" + "Requests" + @"' color='0080C0'  >");

            for (int i = data.Rows.Count - 1; i > -1; i--)
            {
                int __exposures = int.Parse(data.Rows[i]["ExposureCount"].ToString());
                string _exposures = (__exposures / 100).ToString();
                string _calls = data.Rows[i]["CallCount"].ToString();
                string _total_request = data.Rows[i]["RequestCount"].ToString();
                string _requests = data.Rows[i]["NonUpsalesCount"].ToString();
                string _title = "Exposures=" + __exposures + "\r\n" +
                    "Calls=" + _calls + "\r\n"+
                "TotalRequest=" + _total_request + "\r\n" +
                    "Requests=" + _requests + "\r\n";
                sb.Append(@"<category name='" + data.Rows[i]["Date"].ToString() + @"' toolText='" + _title + "'/>");

                sbExposures.Append(@"<set value='" + _exposures + "'/>");
                sbCalls.Append(@"<set value='" + _calls + "'/>");
                sbTotalRequests.Append(@"<set value='" + _total_request + "'/>");
                sbRequests.Append(@"<set value='" + _requests + "'/>");
            }
            sbExposures.Append(@"</dataset>");
            sbCalls.Append(@"</dataset>");
            sbTotalRequests.Append(@"</dataset>");
            sbRequests.Append(@"</dataset>");
            sb.Append("</categories>");
            sb.Append(sbExposures.ToString());
            sb.Append(sbCalls.ToString());
            sb.Append(sbTotalRequests.ToString());
            sb.Append(sbRequests.ToString());
        }
        */
        sb.Append(@"</graph>");

        return SetChartXML(sb);
       
    }
    bool SetChartXML(StringBuilder sb)
    {
        string path = //HttpContext.Current.Request.PhysicalApplicationPath + 
           ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            return false;
        }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        return true;
    }

    #endregion


    string PathV
    {
        get { return (ViewState["XmlGraph"] == null) ? "" : (string)ViewState["XmlGraph"]; }
        set { ViewState["XmlGraph"] = value; }
    }

    WebReferenceReports.ConversionReportRequest ConversionReportRequestV
    {
        get { return (Session["ConversionReportRequestV"] == null) ? null : (WebReferenceReports.ConversionReportRequest)Session["ConversionReportRequestV"]; }
        set { Session["ConversionReportRequestV"] = value; }
    }



    DataTable dataV
    {
        get { return (Session["ConversionTable"] == null) ? null : (DataTable)Session["ConversionTable"]; }
        set { Session["ConversionTable"] = value; }
    }
    int PageIndex
    {
        get { return (Session["PageIndexConversionTable"] == null) ? 0 : (int)Session["PageIndexConversionTable"]; }
        set { Session["PageIndexConversionTable"] = value; }
    }
    DefaultIntervalReport DefaultIntervalReportV
    {
        get { return (ViewState["DefaultIntervalReport"] == null) ? null : (DefaultIntervalReport)ViewState["DefaultIntervalReport"]; }
        set { ViewState["DefaultIntervalReport"] = value; }
    }

   
}
