﻿(function () {
    window.localTime = {
        getDateString: function (dt) {
            return (dt.getMonth() + 1) + "/" + (dt.getDate() > 9 ? dt.getDate() : "0" + dt.getDate()) + "/" + dt.getFullYear() + " " +
                (dt.getHours() > 9 ? dt.getHours() : "0" + dt.getHours()) + ":" + (dt.getMinutes() > 9 ? dt.getMinutes() : "0" + dt.getMinutes());
        },
        getMilisecond: function (num) {
            if (isNaN(num))
                return 0;
            return parseInt(num) || 0;
        },
        start: function () {
            $('span.date-time-utc').each(function () {
                var milli = localTime.getMilisecond($(this).html());
                if (milli == 0) {
                 //   $(this).html("");
                    return;
                }
                var dt = new Date(milli);
                $(this).html(localTime.getDateString(dt));
            });
            $('input.date-time-utc').each(function () {
                var milli = localTime.getMilisecond($(this).val());
                if (milli == 0) {
                    // $(this).val("");
                    return;
                }
                var dt = new Date(milli);
                $(this).val(localTime.getDateString(dt));
            });
        }

    };
})();
$(function () {
    window.localTime.start();
});