<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Publisher/MasterPagePublisher.master" CodeFile="SearchSuppliers.aspx.cs" Inherits="Publisher_SearchSuppliers" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asps" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
     

<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
<link media="screen" rel="stylesheet" href="../jquery/colorbox.css" />
<link type="text/css" rel="Stylesheet" href="../scripts/jquery-ui-1.9.2.custom.dialog.min.css" />
 <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="../jquery/jquery.colorbox1.3.19.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui-1.9.2.custom.dialog.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../PlaceHolder.js"></script>
<script type="text/javascript" src="../scripts/json2.js"></script>

<script type="text/javascript" >
    var serchCharacter=2;
    var message;
    var DapazStatusTranslate = [];
    function openWin(src) // 509296
    {

        window.open(src,'_blank');
    }
    //not in used
    function openNewWin(src)
    {
        var newWindow = window.open(this.getAttribute('href'), '_blank');
        newWindow.focus();
        return false;
    }
    //send to server
    function CheckFor(contr) {
            CallServer(contr, "ee");
        }
        //replay from the server |not delete|//
    function ReciveServerData(retValue) {
       
        
    }
    function OpenCloseDetail(contr, td_open)    {
        
        var contrl=document.getElementById(contr);
  
        if(contrl.style.display=='none')  
        { 
            CheckFor(contr+";1");            
      //      contrl.style.display='table-row';
    //        contrl.removeAttribute("style");
            contrl.style.display='';
            document.getElementById(td_open).className="open first";
            
           }
        else
        {
            CheckFor(contr+";-1");
            contrl.style.display='none';
            document.getElementById(td_open).className="close first";
            }
    }
    
    function chkTextBox(event)
    {
   //     alert(event.keyCode);
        if(event.keyCode!=13)
            return true;
        if(checkSearch())
            run_report();
        return false;
        
    }
    function checkSearch()
    {
    
        var search=document.getElementById("<%# txtSearch.ClientID %>").value;
        var spans=document.getElementById('RegularExpression');
        var spanSentence=document.getElementById('divComment');
        try{
        document.getElementById('<%# _PanelNoReasult.ClientID %>').style.display="none";
        }catch(ex){}
        while(search.length>0)
        {  
            if(search.substring(search.length-1, search.length)==" ")//search.indexOf(' ',0)==(search.length-1))
            {
                search=search.substring(0, search.length-1);             
            }
            else
                break;
        }
        document.getElementById("<%# txtSearch.ClientID %>").value=search;
        if(search.length<serchCharacter || search==document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value)
        {
            
            spans.style.display='inline';
            spanSentence.style.display='block';
            return false;
        }
        else
        {
            spans.style.display='none';
            spanSentence.style.display='none';
            return true;
         }
    }
    
    function setAdvertiser()
    {
        document.getElementById("_iframe").src='<%#ResolveUrl("~")%>Publisher/framePublisher.aspx'; 
    }
    
   function SetCursorToTextEnd(textControlID) 
    {
	    var text = document.getElementById(textControlID);
	    text.focus();
	    if (text != null && text.value.length > 0)
            {
		    if (text.createTextRange) 
		    {
			    var range = text.createTextRange();
			    range.moveStart('character', text.value.length);
    		    range.collapse();
    		    range.select();
		    }
		    else if (text.setSelectionRange) 
		    {
	//	       text.focus();
			    var textLength = text.value.length;
			    text.setSelectionRange(textLength, textLength);		    
		
		    }
	    }
    }
/*
 onblur="javascript:txtOut(this);"  onfocus="javascript:txtIn(this);"
    function txtOut(elm)
    {
        if(elm.value.length==0)
        {
            elm.value=document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value;
            elm.className="form-text WaterMarkTxt";
        }
    }
    
    function txtInit()
    {
        
        var elm=document.getElementById("<%# txtSearch.ClientID %>");

        if(elm.value.length==0)           
            elm.value=document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value;
        if(elm.value==document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value)
            elm.className="form-text WaterMarkTxt";
        else
            elm.className="form-text";
    }
    function txtIn(elm)
    {
        if(elm.value==document.getElementById("<%# hf_WaterMarkSearch.ClientID %>").value)
        {
            
            elm.className="form-text";
            elm.value="";
        }
    }
    */
    function init()
    {        
        message=document.getElementById('<%#Hidden_IfToExite.ClientID%>').value;
        __pageLoad();        
    }
    
    window.onload=init;
    function __pageLoad()
    {       
        appl_init(); 
    //    txtInit();        
    }
    
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }
    ////

    
    function closePopup()
	{
	    document.getElementById("<%# popUpStatusReason.ClientID %>").className = "popModal_del popModal_del_hide";
	    document.getElementById("<%# hf_AccountId.ClientID %>").value = "";
	    var ddl_InactiveReasons = document.getElementById("<%# ddl_InactiveReasons.ClientID %>");
	    ddl_InactiveReasons.selectedIndex = 0;
	    $find('_modal').hide();
	}
	function InactiveAccount(_id)
	{
	    document.getElementById("<%# popUpStatusReason.ClientID %>").className = "popModal_del";
	    document.getElementById("<%# hf_AccountId.ClientID %>").value = _id;
	    var ddl_InactiveReasons = document.getElementById("<%# ddl_InactiveReasons.ClientID %>");
	    ddl_InactiveReasons.selectedIndex = 0;
	    $find('_modal').show();
	}
	function ConfirmActiveAccount()
	{
	    var _confirmed = confirm(document.getElementById("<%# hf_ActivateAccount.ClientID %>").value);
	    return _confirmed;
	}
	function run_report()
	{
	    
	    <%# RunReport() %>;
	    
	}
    function BlurTxtSearch()
    {
        document.getElementById("<%# txtSearch.ClientID %>").blur();
    }
	/*
	function OpenSupplierWin(_id)
	{
	    window.open("framepublisher.aspx?User="+_id , "_blank" , 'fullscreen=no,width=1000,height=500,resizable=no', false);
	//window.location="framepublisher.aspx?User="+_id;
	}
	*/
    function ReloadPlaceHolder(){
        var head = document.getElementsByTagName("head")[0];
        var _script=document.createElement('script'); 
        _script.src='<%# ScriptPlaceHolder %>';
        _script.type = 'text/javascript';
        _script.async = true;
        head.appendChild(_script);
    }
   
    $(function() {
        var _indexTr = 0
		$('.AccountDialog').find('tr').each(function(){
            if(_indexTr % 2 == 0)
                $(this).addClass("tr-odd");
            else
                $(this).removeClass("tr-odd");
            _indexTr++;
        });
		$( ".AccountDialog" ).dialog({
			autoOpen: false,
            modal: true,
			width: 470,
            resizable: false,
			buttons: [
				{
					text: "<%# lbl_OK.Text %>",                   
					click: function() {
                        var _this = this;
                        if(!Page_ClientValidate('GoldenAccount'))
                        {
                        /*
                            for(var i = 0; i < Page_Validators.length; i++){
                                if(Page_Validators[i].validationGroup != 'GoldenAccount')
                                    continue;

                            }
                            */
                            return;
                        }
                        var obj = {};
                        obj.IsActive  = $('#<%# cb_IsActive.ClientID %>').prop('checked');
                        obj.BizId = $('#<%# txt_BizId.ClientID %>').val();
                        obj.name = $('#<%# txt_Name.ClientID %>').val();
                        obj.PhoneNumber = $('#<%# txt_PhoneNumber.ClientID %>').val();
                        obj.SmsNumber = $('#<%# txt_SmsNumber.ClientID %>').val();
                        obj.VirtualNumber = $('#<%# txt_VirtualNumber.ClientID %>').val();
                        obj.supplierid = $('#<%# hf_SupplierId.ClientID %>').val();
                        obj.SendSMS = $('#<%# cb_SendSms.ClientID %>').prop('checked');
                        obj.MoveTo = $('#<%# ddl_MoveTo.ClientID %>').val();
                        obj.FundsStatus = $('#<%# ddl_FundsStatus.ClientID %>').val();
                  //      var _data = "{'supplierid': '" + SupplierId + "', 'BizId': '" + BizId + "', 'IsActive': '" + IsActive.toString() + "', 'name': '"+ name +
                  //              "', 'PhoneNumber': '" + PhoneNumber + "', 'SmsNumber': '" + SmsNumber + "', 'VirtualNumber': '" + VirtualNumber +"', 'SendSMS': '" + SendSMS + "'}";
                         var _data = JSON.stringify(obj);
                        showDiv();
                        $.ajax({
                            url: "<%# SetDpzSupplierDetails %>",
                            data: _data,
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            dataFilter: function (data) { return data; },
                            success: function (data) {
                                if(data.d.length == 0)
                                    alert('error');
                                else {
                                    var _result = eval('(' + data.d + ')');
                                    if(_result.ResponseStatus == 'SupplierDoesNotExist')
                                        alert(_result.ResponseStatus);
                                    else if(_result.ResponseStatus == 'SupplierIsNotDapazGoldenNumberAdvertiser')
                                        alert(_result.ResponseStatus);
                                    else if(_result.ResponseStatus == 'VirtualNumberNotAvailable')
                                        alert(_result.ResponseStatus);
                                    else if(_result.ResponseStatus == 'OK')
                                        $( _this ).dialog( "close" );
                                }
                            
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                            },
                            complete: function (jqXHR, textStatus) {
                                hideDiv();
                                run_report();
                            }
                        });
						
					}
				},
				{
					text: "<%# lbl_Cancel.Text %>",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
        $( ".VirtualNumberDialog" ).dialog({
			autoOpen: false,
            modal: true
        });
        $('#<%# btn_Choose.ClientID %>').click(function(){
             $('#hf_PageIndex').val('');
           GetVirtualNumber(GetPageIndex()); 
        });
          /*
        $('#btn_MoveToPPC').click(function(){
            var _id = $('#<%# hf_SupplierId.ClientID %>').val();
            if(_id.length > 0){
                 $.ajax({
                url: "<# MoveToPPCService %>",
                data: "{'supplierid':'" + _id + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if(data.d.length == 0){
                        return;
                    }
                    hideDiv();
                    if(data.d == "faild"){
                       
                        alert("<%# UpdateFaild %>");

                        return;
                    }
                    run_report();
                    
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //              alert(textStatus);
                    alert("<%# UpdateFaild %>");
                    hideDiv();
                },
                complete: function (jqXHR, textStatus) {
                }
                });
                
            }
            
            $( ".AccountDialog" ).dialog("close");
        });
        */
    });
    
    function ClearAccountDialogValidation()
    {
        for(var i = 0; i < Page_Validators.length; i++){
            if(Page_Validators[i].validationGroup == 'GoldenAccount'){
                Page_Validators[i].style.display = "none";    
            }
        }
    }
    
    function GetPageIndex(){
         var str = $('#hf_PageIndex').val();
        var pageindex;
        if(str.length > 0 && !isNaN(str))
            pageindex = new Number(str);
        else
            pageindex = 0; 
        return pageindex;
    }
    function SetPageIndex(pageindex){
        $('#hf_PageIndex').val(pageindex);
    }
    function  GetVirtualNumber(pageindex){
        
        $.ajax({
            url: "<%# GetVirtualNumber %>",
            data: "{ 'PageIndex': '" + pageindex + "' }",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if(data.d.length==0)
                    return;
                var _data = eval('(' + data.d + ')');
                var $table = $('.VirtualNumberDialog > table');
                $table.empty();
                var $tr;
                for(var i = 0; i < _data.numbers.length; i++){
                     if((i % 3) == 0){
                        if($tr)
                            $table.append($tr);
                        $tr = $('<tr>');
                    }
                    $tr.append($('<td><a href="javascript:void(0);">'+ _data.numbers[i] + '</a></td>'));
                }
                if($tr)
                    $table.append($tr);
                $table.find('a').each(function(){
                    $(this).click(function(){
                          SetOneVirtualNumber($(this).html());
                          $( ".VirtualNumberDialog" ).dialog( "close" );
                    });
                });    
                if(_data.HasPrevious)
                    EnableAnchor($('.VirtualNumberDialog').find('#<%# a_previous.ClientID %>'), PreviusVirtualNumber); 
                else
                   DisableAnchor($('.VirtualNumberDialog').find('#<%# a_previous.ClientID %>'));
                if(_data.HasNext) 
                   EnableAnchor($('.VirtualNumberDialog').find('#<%# a_next.ClientID %>'), NextVirtualNumber);
                else
                   DisableAnchor($('.VirtualNumberDialog').find('#<%# a_next.ClientID %>'))
                 $( ".VirtualNumberDialog" ).dialog( "open" );
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //              alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                
            }

        });
    }
    function DisableAnchor($_a){
         $_a.addClass('_disable');
         $_a.unbind('click');
    }
    function EnableAnchor($_a, _onclick){
         $_a.removeClass('_disable');
         $_a.unbind('click');
         $_a.click(function(){
             _onclick();
         });
    }
    function NextVirtualNumber(){
          var pageindex = GetPageIndex();
          if(pageindex == 0)
            pageindex = 1;
          pageindex++;
          SetPageIndex(pageindex);
          GetVirtualNumber(pageindex);
    }
    function PreviusVirtualNumber(){
          var pageindex = GetPageIndex();
          pageindex--;
          SetPageIndex(pageindex);
          GetVirtualNumber(pageindex);
    }
    function SetOneVirtualNumber(num){
          $('#<%# txt_VirtualNumber.ClientID %>').val(num);
    }
    function OpenDpzSupplierDetails(_id){
        showDiv();       
         $.ajax({
            url: "<%# GetDpzSupplierDetails %>",
            data: "{'supplierid':'" + _id + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if(data.d.length == 0){
                    return;
                }
                var _data = eval('(' + data.d + ')');
                
                if(_data.ResponseStatus != 'OK'){
                    alert(_data.ResponseStatus);
                }
                else{
                    ClearAccountDialogValidation();
                    $('#hf_PageIndex').val('');
                    $('#<%# txt_Name.ClientID %>').val(_data.Name);
                    $('#<%# txt_Heading.ClientID %>').val(_data.HeadingName);
                    $('#<%# txt_BizId.ClientID %>').val(_data.BizId);
                    $('#<%# txt_VirtualNumber.ClientID %>').val(_data.VirtualNumber);
                    $('#<%# cb_IsActive.ClientID %>').prop('checked', _data.IsActive);
                     $('#<%# cb_SendSms.ClientID %>').prop('checked', _data.SendSMS);
                    $('#<%# hf_SupplierId.ClientID %>').val(_data.SupplierId);
                     $('#<%# txt_PhoneNumber.ClientID %>').val(_data.PhoneNumber);
                     $('#<%# txt_SmsNumber.ClientID %>').val(_data.SmsNumber);
                      $('#<%# txt_GoldenStatus.ClientID %>').val(GetStatusFromList(_data.Status));
                      $('#<%# hf_Status.ClientID %>').val(_data.Status);
                    SetFundsStatus(_data.FundsStatus);
                     $('#<%# ddl_MoveTo.ClientID %>').empty();
                     var ddl_MoveTo = $('#<%# ddl_MoveTo.ClientID %>').get(0);
                     var option1 = document.createElement("option");
                     option1.text = '';
                     option1.value = '';
                     AddToSelect(ddl_MoveTo, option1);
                     for(var i = 0; i < _data.MoveToStatuses.length; i++) {
                        var option = document.createElement("option");
                        option.value = _data.MoveToStatuses[i];
                        option.text = GetStatusFromList(_data.MoveToStatuses[i]);
                        AddToSelect(ddl_MoveTo, option);
                     }
                     $( ".AccountDialog" ).dialog('open');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //              alert(textStatus);
                hideDiv();
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                hideDiv();
            }
        });
        return false;

    }
        function AddToSelect(_select, option)
        {
            try{
               _select.add(option, null); 
            }
            catch(ex)
            {
                _select.add(option);
            }
        }
		 function NewGoldenNumber() {
            $('#<%# txt_Name.ClientID %>').val('');
            $('#<%# txt_BizId.ClientID %>').val('');
            $('#<%# txt_VirtualNumber.ClientID %>').val('');
            $('#<%# cb_IsActive.ClientID %>').prop('checked', true);
            $('#<%# cb_SendSms.ClientID %>').prop('checked', true);
            $('#<%# hf_SupplierId.ClientID %>').val('');
            $('#hf_PageIndex').val('');
            $('#<%# txt_PhoneNumber.ClientID %>').val('');
            $('#<%# txt_SmsNumber.ClientID %>').val('');
            $('#<%# txt_GoldenStatus.ClientID %>').val('');
            $('#<%# hf_Status.ClientID %>').val('');
            $('#<%# ddl_MoveTo.ClientID %>').empty();
            var ddl_MoveTo = $('#<%# ddl_MoveTo.ClientID %>').get(0);
            var option1 = document.createElement("option");
            option1.text = "<%# FixStatus %>";
            option1.value = GetStatusFromList(option1.text);
            AddToSelect(ddl_MoveTo, option1);

            var option2 = document.createElement("option");
            option2.text = "<%# RarahStatus %>";
            option2.value = GetStatusFromList(option2.text);
            AddToSelect(ddl_MoveTo, option2);

            SetFundsStatus("<%# GetFundsStatusNormal %>");
              ClearAccountDialogValidation();
			$( ".AccountDialog" ).dialog( "open" );

			
		};
    function SetStatuses(str)
    {
        var strs = str.split('|||');
        for(var i = 0; i < strs.length; i++){
            if(strs[i].length == 0)
                continue;
            var key_value = strs[i].split(';;;');
            var kv = {};
            kv.key = key_value[0];
            kv.value = key_value[1];
            DapazStatusTranslate.push(kv);
        }
    }
    function GetStatusFromList(status){
        for(var i = 0; i < DapazStatusTranslate.length; i++){
            if(DapazStatusTranslate[i].key == status)
                return DapazStatusTranslate[i].value;
        }
        return '';
    }
    function SetFundsStatus(status){
        var ddl_FundsStatus = $('#<%# ddl_FundsStatus.ClientID %>').get(0);
        for(var i = 0; i < ddl_FundsStatus.length; i++){
             if(ddl_FundsStatus.options[i].value == status) {
                  ddl_FundsStatus.selectedIndex = i;
                  return;
              }
        }
    }
    function VirtualNumberValid(sender, args) {
        var _str = args.Value;
        //arguments.IsValid = IsValidDateByFormat(_str, "mm/dd/yyyy");
        if(_str.length > 0) {
            args.IsValid = true;
            return;
        }
        var _MoveTo = $('#<%# ddl_MoveTo.ClientID %>').val();
        if(_MoveTo.length == 0)
            _MoveTo = $('#<%# hf_Status.ClientID %>').val();         
        if(_MoveTo == "<%# PPAStatus %>" || _MoveTo == "<%# RarahQuoteStatus %>" || _MoveTo == "<%# PPAHoldStatus %>"){
             args.IsValid = true;
             return;
        }
        args.IsValid = false;
    }
    
      
</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server" ClientIDMode="AutoID">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>

<div class="page-content minisite-content">
    <h2><asp:Label ID="lblTitleSearch" runat="server" Text="Search advertisers"></asp:Label></h2>
    <div class="clearfix">
	    <div class="form-search clearfix">
			
                    <div id="divComment" style="position: absolute; top: -20px; display:none;">
                        <asp:Label ID="lblComment" CssClass="error-msg" runat="server" Text="At least two characters"></asp:Label>
                    </div>
                    <asp:Panel ID="_PanelNoReasult" CssClass="error-msg" Visible="false" runat="server">
                        <asp:Label ID="lbl_noreasultSearch" runat="server" Text="There is no result for this search"></asp:Label>
                    </asp:Panel> 
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-text" MaxLength="40" onkeydown="return chkTextBox(event);"></asp:TextBox>
					<span id="RegularExpression" class="error-msg" style="display:none;">*</span>
                    
                    <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="form-submit" OnClick="btnSearch_Click" OnClientClick="return checkSearch();" />								
				 <asps:UpdatePanel id="_UpdatePanelTotalResult" runat="server"  UpdateMode="Conditional">                           
        <ContentTemplate> 	
					<asp:Panel ID="PanelResults" runat="server" CssClass="results4" Visible="false"> 
                        <asp:Label ID="lbl_Count" runat="server"  CssClass="aboutResults"></asp:Label>
                        <asp:Label ID="lbl_Results" runat="server" Text="Results that match your search criteria"></asp:Label>
                    </asp:Panel>   
                </ContentTemplate>
            </asps:UpdatePanel>
                
        </div>
       
        <div class="clear"></div>
    <asps:UpdatePanel id="_UpdatePanel" runat="server"  UpdateMode="Conditional">                           
        <ContentTemplate> 
        <div class="status-label" runat="server" id="div_status">
           
                <div class="statusclient">
                    <asp:Image ID="Image3" runat="server" ImageUrl="../images/candidate.png" />
                </div>
                <div class="statusclient">
                    <asp:Label ID="lbl_orangeExplan" runat="server" Text="Candidate" CssClass="Label_Icons statusclient"></asp:Label>                
                <div class="statusclient">&nbsp;&nbsp;&nbsp;|</div>
                </div>
                    
                <div class="statusclient">
                    <asp:Image ID="Image2" runat="server" ImageUrl="../images/available.png" />                
                </div>
                <div class="statusclient">
                    <asp:Label ID="lbl_greenExplan" runat="server" Text="Available" CssClass="Label_Icons statusclient"></asp:Label>
                <div class="statusclient">&nbsp;&nbsp;&nbsp;|</div>
                </div>
                
                <div class="statusclient">
               	    <asp:Image ID="Image1" runat="server" ImageUrl="../images/unavailable.png" />
               	</div>
               	<div class="statusclient">
                    <asp:Label ID="lbl_Unavailable" runat="server" Text="Unavailable" CssClass="Label_Icons statusclient"></asp:Label>
                <div class="statusclient">&nbsp;&nbsp;&nbsp;|</div>
                </div>
                
                 <div class="statusclient">
                    <asp:Image ID="Image4" runat="server" ImageUrl="../images/innactive.png" />               	
               	</div>
               	<div class="statusclient">
               	    <asp:Label ID="lbl_redExplan" runat="server" Text="Inactive" CssClass="Label_Icons statusclient"></asp:Label>                
                <div class="statusclient">&nbsp;&nbsp;&nbsp;|</div>
                </div>

                <div class="statusclient">
                    <asp:Image ID="Image5" runat="server" ImageUrl="../images/InactiveWithMoney.png" />               	
               	</div>
               	<div class="statusclient">
               	    <asp:Label ID="lbl_InactiveWithMoney" runat="server" Text="Inactive With Money" CssClass="Label_Icons statusclient"></asp:Label>                
                <div class="statusclient"></div>
                </div>
	    </div>
   <div class="clear"></div>
        <div class="table2">
            <asp:Repeater runat="server" ID="_reapeter" 
                onitemdatabound="_reapeter_ItemDataBound">
                <HeaderTemplate>
                    <table class="data-table">
                        <thead>
                            <tr>
                                <th style="width: 30px;">&nbsp;</th>
                                <th style="width: 40px;"><asp:Label ID="Label31" runat="server" Text="<%#HiddenSupplierNum.Value %>"></asp:Label></th>
						        <th style="width: 170px;"><asp:Label ID="Label50" runat="server" Text="<%#HiddenName.Value %>"></asp:Label></th>
						        <th style="width: 170px;"><asp:Label ID="Label1" runat="server" Text="<%#HiddenContactName.Value %>"></asp:Label></th>
						        <th style="width: 110px;"><asp:Label ID="Label51" runat="server" Text="<%#HiddenPhone1.Value %>"></asp:Label></th>
                                 <th style="width: 110px;"><asp:Label ID="Label220" runat="server" Text="<%#HiddenTrafficLights.Value %>"></asp:Label></th>
						        <th style="width:40px;"><asp:Label ID="Label32" runat="server" Text="<%#HiddenBalance.Value %>"></asp:Label></th>
						        <th style="width:40px;"><asp:Label ID="Label53" runat="server" Text="<%#HiddenTrafficLights.Value %>"></asp:Label></th>
						    </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                        
                <ItemTemplate>
                        <tr runat="server" id="tr_show">
                            <td class="close first" id="td_openClose" runat="server"><a class="arrow" runat="server" id="a_open"></a></td>
                            <td><asp:LinkButton ID="lb_SupplierNum" runat="server" OnClick="btn_Edit_click" Text="<%# Bind('SupplierNum') %>" CommandArgument="<%# Bind('name') %>" OnClientClick="<%# Bind('onclick') %>"></asp:LinkButton></td>
						    <td>
                                <asp:LinkButton ID="LinkButton_name" runat="server" OnClick="btn_Edit_click" Text="<%# Bind('name') %>" CommandArgument="<%# Bind('name') %>" OnClientClick="<%# Bind('onclick') %>"></asp:LinkButton>
                                <asp:Image ID="img_GoldenAccount" runat="server" ImageUrl="~/Publisher/images/GoldenAccount.png" Visible="<%# Bind('IsSmallWin') %>" />
                            </td>
						    <td><asp:Label ID="Label3" runat="server" Text="<%# Bind('ContactName') %>"></asp:Label></td>
						    <td><asp:Label ID="Label55" runat="server" Text="<%# Bind('telephone1') %>"></asp:Label></td>
                            <td><asp:Label ID="Label221" runat="server" Text="<%# Bind('SupplierStatus') %>"></asp:Label></td>
						    <td><asp:Label ID="Label56" runat="server" Text="<%# Bind('Balance') %>"></asp:Label></td>
						    <td><asp:ImageButton ID="Image_status" runat="server"   OnClick="btn_suspend_click" ImageUrl="<%#Bind('s_image') %>"
						         OnClientClick="<%# Bind('script') %>" CommandArgument="<%#Bind('accountid') %>"/></td>
						</tr>
						<tr id="tr_openClose" runat="server" style="display:none">
						     <td colspan="7" class="data-table-details">
						        <div class="details-address">
                                    <h3>
                                        <asp:Label ID="Label002" runat="server" Text="<%# lbl_AddressDetails.Text %>"></asp:Label>
                                    </h3>
                                    <ul>
                                        <li>
                                            <asp:Label ID="Label007" runat="server" CssClass="label" Text="<%# Hiddenaddress1_city.Value %>"></asp:Label>
                                            <asp:Label ID="Label8" runat="server" Text="<%# Bind('address1_city') %>"></asp:Label>
                                        </li>
                                        <li>
                                            <asp:Label ID="Label9" runat="server" CssClass="label" Text="<%# Hiddenaddress1_line1.Value %>"></asp:Label>
                                            <asp:Label ID="Label10" runat="server" Text="<%# Bind('address1_line1') %>"></asp:Label>
                                        </li>
                                        <li>
                                            <asp:Label ID="Label11" runat="server" CssClass="label" Text="<%# Hidden_HouseNumber.Value %>"></asp:Label>
                                            <asp:Label ID="Label12" runat="server" Text="<%# Bind('address1_line2') %>"></asp:Label>
                                        </li>
                                        <li>
                                            <asp:Label ID="Label13" runat="server" CssClass="label" Text="<%# Hidden_ZipCode.Value %>"></asp:Label>
                                            <asp:Label ID="Label14" runat="server" Text="<%# Bind('address1_postalcode') %>"></asp:Label>
                                        </li>
                                    </ul>    
                                </div>
                                <div class="details-contact">
                                    <h3>
                                        <asp:Label ID="Label21" runat="server" Text="<%# lbl_ContactDetails.Text %>"></asp:Label> 
                                    </h3>
                                    <ul>
                                        <li>
                                            <asp:Label ID="Label17" runat="server" CssClass="label" Text="<%# HiddenPhone2.Value %>"></asp:Label>
                                            <asp:Label ID="Label18" runat="server" Text="<%# Bind('telephone2') %>"></asp:Label>
                                        </li>
                                        <li>
                                            <asp:Label ID="Label4" runat="server" CssClass="label" Text="<%# Hidden_Fax.Value %>"></asp:Label>
                                            <asp:Label ID="Label81" runat="server" Text="<%# Bind('fax') %>"></asp:Label>
                                        </li>
                                        <li>
                                            <asp:Label ID="Label15" runat="server" CssClass="label" Text="<%# Hidden_WebSite.Value %>"></asp:Label>
                                            <asp:Label ID="Label16" runat="server" Text="<%# Bind('websiteurl') %>"></asp:Label>
                                        </li>
                                        <li>
                                            <asp:Label ID="Label5" runat="server" CssClass="label" Text="<%# Hiddenemailaddress1.Value %>"></asp:Label>
                                            <asp:Label ID="lbl_emailaddress" runat="server" Text="<%# Bind('emailaddress1') %>"></asp:Label>
                                        </li>
                                        <li>
                                            <asp:Label ID="Label19" runat="server" CssClass="label" Text="<%# Hidden_Employees.Value %>"></asp:Label>
                                            <asp:Label ID="Label20" runat="server" Text="<%# Bind('numberofemployees') %>"></asp:Label>
                                        </li>
                                    </ul>
                                </div>
                                <asp:Label ID="lbl_Guid" runat="server" Visible="false" Text="<%#Bind('accountid') %>"></asp:Label>
                                <asp:Label ID="lbl_status" runat="server" Visible="false" Text="<%#Bind('status') %>"></asp:Label>
                            </td>
						</tr>
					</ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
               </div>                      
            <uc1:TablePaging ID="TablePaging1" runat="server" />
        </ContentTemplate>
    </asps:UpdatePanel>		
</div>
</div>
<div id="popUpStatusReason" runat="server" style="display:none;" class="popModal_del popModal_del_hide"  >
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
        <div class="content" >
            <div class="modaltit">
            <h2><asp:Label ID="lbl_InactiveReason" runat="server"  Text="Inactive reason"></asp:Label></h2>
            </div>
            <div class="form-field">
                <asp:Label ID="lbl_selectReason" runat="server" CssClass="label4" Text="Select reason"></asp:Label>
                <asp:DropDownList ID="ddl_InactiveReasons" CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            <div class="clear"><!-- --></div>
            <div class="statusbtn">
                <asp:Button ID="btn_InactiveReason" runat="server" Text="Set" CssClass="CreateReportSubmit2" OnClick="btn_InactiveReason_click" />
                <asp:HiddenField ID="hf_AccountId" runat="server" />
            </div>
            <div class="clear"><!-- --></div>
        </div>
    <div class="bottom2"></div>
</div>
    
<cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btn_virtual"
    PopupControlID="popUpStatusReason"
    BackgroundCssClass="modalBackground" 
    BehaviorID="_modal"               
    DropShadow="false"
    >
</cc1:ModalPopupExtender>
<div style="display:none;">
   <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />       
</div>
<div id="AccountDialog" class="AccountDialog" style="display:none;" runat="server" title="<%# lbl_GoldenNumberAdvertiser.Text %>">   
    <table>
    <colgroup>
        <col style="width: 110px; padding-left: 20px;"/>
        <col style="width: 205px;"/>
        <col style="padding-left: 3px; width: 80px;"/>
        <col style="width: auto;"/>
    </colgroup>
    <tr class="tr-odd">
    <td class="td-first">
        <asp:Label ID="lbl_Name" runat="server" Text="Name"></asp:Label>
        
        <asp:HiddenField ID="hf_SupplierId" runat="server" />
    </td>
    <td>
        <asp:TextBox ID="txt_Name" runat="server"></asp:TextBox>
        
    </td>
     
    <td colspan="2">
       <asp:RequiredFieldValidator ID="rfv_Name" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
            ControlToValidate="txt_Name" ValidationGroup="GoldenAccount" Display="Dynamic"></asp:RequiredFieldValidator>
    </td>
    </tr>
    <tr>
    <td>
        <asp:Label ID="lbl_BizId" runat="server" Text="BizId"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txt_BizId" runat="server"></asp:TextBox>
        
    </td>
     
    <td colspan="2">
       <asp:RequiredFieldValidator ID="rfv_BizId" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
            ControlToValidate="txt_BizId" ValidationGroup="GoldenAccount" Display="Dynamic"></asp:RequiredFieldValidator>
         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<%# lbl_InvalidBizId.Text %>"
            ControlToValidate="txt_BizId" ValidationExpression="<%# GetAccountNumberRegularExpression %>" ValidationGroup="GoldenAccount"
            Display="Dynamic"></asp:RegularExpressionValidator>
    </td>
    </tr>
    <tr class="tr-odd">
    <td>
        <asp:Label ID="lbl_VirtualNumber" runat="server" Text="Virtual number"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txt_VirtualNumber" runat="server" ReadOnly="true"></asp:TextBox>         
        
    </td>
    <td>
        <a id="btn_Choose" class="formSubmit" runat="server">Choose</a>
        
    </td>
    <td>
        <asp:CustomValidator ID="cv_VirtualNumber" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
            ControlToValidate="txt_VirtualNumber" ValidateEmptyText="true" ValidationGroup="GoldenAccount" Display="Dynamic" CssClass="error-msg"
             ClientValidationFunction="VirtualNumberValid"></asp:CustomValidator>
      
    </td>
    </tr>
    <tr>
    <td>
        <asp:Label ID="lbl_PhoneNumber" runat="server" Text="Phone number"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txt_PhoneNumber" runat="server"></asp:TextBox>
        
    </td>
    
    <td colspan="2">
       <asp:RequiredFieldValidator ID="rfv_PhoneNumber" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
            ControlToValidate="txt_PhoneNumber" ValidationGroup="GoldenAccount" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="rev_PhoneNumber" runat="server" ErrorMessage="<%# lbl_InvalidPhoneNumber.Text %>"
            ControlToValidate="txt_PhoneNumber" ValidationExpression="<%# GetPhoneRegularExpression %>" ValidationGroup="GoldenAccount"
            Display="Dynamic"></asp:RegularExpressionValidator>
    </td>
    </tr>
    <tr class="tr-odd">
    <td>
        <asp:Label ID="lbl_SmsNumber" runat="server" Text="Sms number"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txt_SmsNumber" runat="server"></asp:TextBox>
        
    </td>
    
    <td colspan="2">
       <asp:RegularExpressionValidator ID="rev_SmsNumber" runat="server" ErrorMessage="<%# lbl_InvalidPhoneNumber.Text %>"
            ControlToValidate="txt_SmsNumber" ValidationExpression="<%# GetPhoneRegularExpression %>" ValidationGroup="GoldenAccount"
            Display="Dynamic"></asp:RegularExpressionValidator>
    </td>
    </tr>

    <tr class="tr-odd">
    <td>
        <asp:Label ID="lbl_Heading" runat="server" Text="Heading"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txt_Heading" runat="server" ReadOnly="true"></asp:TextBox>
        
    </td>
    
    <td colspan="2">
      
    </td>
    </tr>

    <tr class="tr-odd">
    <td>
        <asp:Label ID="lbl_Status" runat="server" Text="<%# HiddenTrafficLights.Value %>"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txt_GoldenStatus" runat="server" ReadOnly="true"></asp:TextBox>        
        <asp:HiddenField ID="hf_Status" runat="server" />        
    </td>
    
    <td colspan="2">
       
    </td>
    </tr>
   
   <tr>
    <td>
        <asp:Label ID="lbl_MoneyStatus" runat="server" Text="Funds status"></asp:Label>
    </td>
    <td>
        <asp:DropDownList ID="ddl_FundsStatus" runat="server">
        </asp:DropDownList>  
    </td>
    
    <td colspan="2">
      
    </td>
    </tr>

    <tr>
    <td>
        <asp:Label ID="lbl_MoveTo" runat="server" Text="Move to"></asp:Label>
    </td>
    <td>         
        <asp:DropDownList ID="ddl_MoveTo" runat="server">
        </asp:DropDownList>
    </td>
    
    <td colspan="2">
       
    </td>
    </tr>
    <tr class="tr-odd">
    <td>
        <asp:Label ID="lbl_IsActive" runat="server" Text="Is active"></asp:Label>
    </td>
    <td class="td-IsActive">
         <asp:CheckBox ID="cb_IsActive" runat="server" Checked="true" />
    </td>
    
    <td colspan="2">
    
    </td>
    </tr>

    <tr>
    <td>
        <asp:Label ID="lbl_SendSms" runat="server" Text="To Send SMS"></asp:Label>
    </td>
    <td class="td-IsActive">
         <asp:CheckBox ID="cb_SendSms" runat="server" Checked="true" />
    </td>
    
    <td colspan="2">
    
    </td>
    </tr>
    </table>
</div>
<div id="VirtualNumberDialog" class="VirtualNumberDialog" style="display:none;" runat="server" title="<%# lbl_SelectVirtualNumber.Text %>">
    <table>
    
    </table>
    <div class="NextPrevious">
        <a id="a_previous" runat="server" onselectstart="return false;">Previous</a>
        <a id="a_next"  runat="server" onselectstart="return false;">Next</a>
    </div>
    <input id="hf_PageIndex" type="hidden" />
</div>           
           
<asp:Label ID="lbl_AddressDetails" runat="server" Text="Address Details" Visible="false"></asp:Label>
<asp:Label ID="lbl_ContactDetails" runat="server" Text="Contact Details" Visible="false"></asp:Label>
<div id="_head">
<asp:HiddenField ID="HiddenSupplierNum" runat="server" Value="ID" />
<asp:HiddenField ID="HiddenName" runat="server" Value="Name" />
<asp:HiddenField ID="HiddenContactName" runat="server" Value="Contact name" />
<asp:HiddenField ID="HiddenPhone1" runat="server" Value="Phone" />
<asp:HiddenField ID="HiddenBalance" runat="server" Value="Balance" />
<asp:HiddenField ID="HiddenTrafficLights" runat="server" Value="Status" />    
</div>

<div id="_inside">
<asp:HiddenField ID="Hidden_Fax" runat="server" Value="Fax" />
<asp:HiddenField ID="Hiddenemailaddress1" runat="server" Value="Email" />
<asp:HiddenField ID="Hiddenaddress1_city" runat="server" Value="City" />
<asp:HiddenField ID="Hiddenaddress1_line1" runat="server" Value="Street" />
<asp:HiddenField ID="Hidden_HouseNumber" runat="server" Value="House number" />
<asp:HiddenField ID="Hidden_ZipCode" runat="server" Value="Zip code" />
<asp:HiddenField ID="Hidden_WebSite" runat="server" Value="Web site" />
<asp:HiddenField ID="HiddenPhone2" runat="server" Value="Contact phone" />
<asp:HiddenField ID="Hidden_Employees" runat="server" Value="Number of employees" />
</div>
    
<asp:HiddenField ID="HiddenIsSuspend" runat="server" Value="Suspend" />
<asp:HiddenField ID="HiddenSupplierIsGoing" runat="server" Value="The following operation will" />
<asp:HiddenField ID="HiddenSupplierIsGoing_end" runat="server" Value="the supplier" />
<asp:HiddenField ID="HiddenSuspendList" runat="server" Value="Active#1;Suspend#2" />
<asp:HiddenField ID="Hidden_IfToExite" runat="server" Value="Are you sure you want to exit? Unsaved data will be lost!" />
<asp:HiddenField ID="hf_WaterMarkSearch" runat="server" Value="Type Advertiser, company Name, Phone, or Advertiser's Number" />
    
<asp:HiddenField ID="hf_ActivateAccount" runat="server" Value="This action will activate this account - continue?" />


<div id="_buttomSuspend">
    <asp:Label ID="lbl_suspend" runat="server" Visible="false" Text="Suspend"></asp:Label>
    <asp:Label ID="lbl_active" runat="server" Visible="false" Text="Active"></asp:Label>
</div>
<div style="display:none;">
    <asp:Button ID="btn_virtual_run" runat="server" Text="Button" style="display:none;" onfocus="this.blur();" />
</div>
<asp:Label ID="lbl_Missing" runat="server" Visible="false" Text="Missing"></asp:Label>
<asp:Label ID="lbl_InvalidPhoneNumber" runat="server" Visible="false" Text="Invalid phone number"></asp:Label>
<asp:Label ID="lbl_InvalidBizId" runat="server" Visible="false" Text="Invalid Biz ID"></asp:Label>


<asp:Label ID="lbl_GoldenNumberAdvertiser" runat="server" Visible="false" Text="Golden Number Advertiser"></asp:Label>
<asp:Label ID="lbl_SelectVirtualNumber" runat="server" Visible="false" Text="Select Virtual Number"></asp:Label>

<asp:Label ID="lbl_OK" runat="server" Visible="false" Text="OK"></asp:Label>
<asp:Label ID="lbl_Cancel" runat="server" Visible="false" Text="Cancel"></asp:Label>


   <asp:Label ID="lbl_UpdateFaild" runat="server" Visible="false" Text="Update faild"></asp:Label>
</asp:Content>



