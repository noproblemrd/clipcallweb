﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClipCallLeadConnections.aspx.cs" Inherits="Publisher_ClipCallLeadConnections" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" rel="stylesheet" href="Style.css" /> 
    <style type="text/css">
        .data-main-table
        {
            margin-top: 30px !important;
        }
    </style>
     <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script> 
     <script src="localTimeDisplay.js" type="text/javascript"></script>  
    <title></title>
    <script type="text/javascript">
        function CloseMeError() {
            alert("<%# lbl_ErrorLoadData.Text %>");
            //parent.CloseIframe()
            window.close();
        }
        function OpenRecord(url) {
            
                oWin = window.open(url, "recordWin", "resizable=0,width=1000,height=600");
                return false;

        }
        function DownloadVideo(str) {
            // var oWin = window.open('<%# GetDownloadVideo %>?str=' + str, "downloadVideo", "resizable=0,width=200,height=200");
            window.location.href = '<%# GetDownloadVideo %>?str=' + str;
            return false;
        }
        function VideoError() {
            alert('Something went wrong with this video!');
        }
        function OpenChat(id) {
            var _url = '<%# GetChatUrl %>' + id;
            var chatWin = window.open(_url, "chatWin" + id, "resizable=0,width=900,height=900,scrollbars=1");
            return false;
        }
        function Openquote(id) {
            var _url = '<%# GetQuoteUrl %>' + id;
            var chatWin = window.open(_url, "quoteWin" + id, "resizable=0,width=600,height=600");
            return false;
        }

        function OpenProximitySensor(id) {
            var _url = '<%# GetProximitySensorUrl %>' + id;
            var chatWin = window.open(_url, "ProximitySensorWin" + id, "resizable=0,width=600,height=600");
            return false;
        }
        function OpenIframe(_path) {           
            var _leadWin = window.open(_path, "ProjectDetails", "width=1200, height=650,scrollbars=1");
            _leadWin.focus();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
    <div>
        <div class="titlerequestTop" style="margin-top: 20px;">
            <a id="a_projectNumber" runat="server">
			    <asp:Label ID="lbl_Lead" runat="server" Text="Lead"></asp:Label>:
                <asp:Label ID="lbl_LeadNum" runat="server"></asp:Label>
			</a>          
            
        </div>
        <div class="clear"></div>
        <div class="separator"></div>
        <div id="form-analyticscomp2" style="width:950px; position:static;">

            <div class="containertab2" >
         <%// div details %>
            <div runat="server" id="div_Details">
               <div class="form-fieldshort">
                    <asp:Label ID="lbl_CustomerName" runat="server" CssClass="label" Text="Customer name"></asp:Label>
                    <asp:TextBox ID="txt_CustomerName" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-fieldshort">
                    <asp:Label ID="lbl_CustomerPhone" runat="server" CssClass="label" Text="Customer phone"></asp:Label>
                    <asp:TextBox ID="txt_CustomerPhone" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="form-fieldlongdet">
                    <asp:Label ID="Label9" runat="server" CssClass="label" Text="Full Address"></asp:Label>
                    <asp:TextBox ID="txt_fullAddress" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" style="width:310px;"></asp:TextBox>
                </div>
                <div class="form-fieldshort">
                    <asp:Label ID="Label10" runat="server" CssClass="label" Text="Has push notifications"></asp:Label>
                    <asp:TextBox ID="txt_hasPush" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                </div>
              
                <div class="clear"></div>
                <br />  
                <div class="div_supplier_fouls">
                    <asp:GridView ID="gv_fouls"  Width="450px" CssClass="data-table2" runat="server" AutoGenerateColumns="false" Caption="Pros in this project">
                        <RowStyle CssClass="odd"  />
                    <AlternatingRowStyle CssClass="even" />
                        
                        <Columns>
                              <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label1000" runat="server" Text="Advertiser name"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1000" runat="server" Text='<%# Bind("AdvertiserName") %>' Target="_blank" NavigateUrl='<%# Bind("SupplierRequestReport") %>'></asp:HyperLink>   
                            </ItemTemplate>
                            </asp:TemplateField>

                              <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label1001" runat="server" Text="Advertiser Phone"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1002" runat="server" Text='<%# Bind("AdvertiserPhone") %>' Target="_blank" NavigateUrl='<%# Bind("SupplierRequestReport") %>'></asp:HyperLink>
                                </ItemTemplate>
                                </asp:TemplateField>    

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label1002" runat="server" Text="Chat Trace"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>                                
                                    <asp:LinkButton ID="link1003" runat="server" OnClientClick='<%# Bind("ChatTraceScript") %>'  Text="Chat Trace"></asp:LinkButton>
                                </ItemTemplate>
                                </asp:TemplateField>    

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label1004" runat="server" Text="Fouls"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1004" runat="server" Text='<%# Bind("FoulsText") %>' Target="_blank" NavigateUrl='<%# Bind("FoulsUrl") %>'></asp:HyperLink>
                                </ItemTemplate>
                                </asp:TemplateField>    
                        </Columns>
                        
                    </asp:GridView>
                </div>
                <div class="clear"></div>
                
                    <asp:GridView ID="_GridView"  Width="779px" CssClass="data-table2 data-main-table" runat="server" AutoGenerateColumns="false">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                        
                    <Columns>
                          <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="Date"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("Date") %>' CssClass="date-time-utc"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="Advertiser name"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Bind("AdvertiserName") %>' Target="_blank" NavigateUrl='<%# Bind("SupplierRequestReport") %>'></asp:HyperLink>   
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="Advertiser Phone"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink2" runat="server" Text='<%# Bind("AdvertiserPhone") %>' Target="_blank" NavigateUrl='<%# Bind("SupplierRequestReport") %>'></asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>            
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="Type"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label13" runat="server" Text="Record"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lb_record" runat="server" OnClientClick='<%# Bind("Record") %>'  Text='<%# Bind("RecordText") %>'></asp:LinkButton>
                            <br />
                            <asp:Label ID="lbl_record" runat="server" Text='<%# Bind("RecordDescription") %>' Visible='<%# Bind("RecordDescriptionVisible") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label14" runat="server" Text="Download"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lb_download" runat="server" OnClientClick='<%# Bind("Download") %>'  Text="Download" Visible='<%# Bind("HasDownload") %>'></asp:LinkButton>
                            
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label15" runat="server" Text="Quality Rating"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label16" runat="server" Text='<%# Bind("QualityRating") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                       
                    </Columns>
                        
                    </asp:GridView>
                
            </div>
        </div>
        </div>
    </div>
        <asp:Label ID="lbl_ErrorLoadData" runat="server" Text="There was an error to load the details." Visible="false"></asp:Label>
    </form>
</body>
</html>
