﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AarSender.aspx.cs" Inherits="Publisher_AarSender" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <style type="text/css">
        .main-inputs, .form-field
        {
            width: 100% !important;
        }
        input[type="text"], textarea 
        {
            width: 400px !important;
        }
        .txt_message
        {
            height: 100px !important;
        }
    </style>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>  
    <script type="text/javascript">
        function EnableThisButton(btn)
        {
            $(btn).hide();
            $(btn).parent().find('img').show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />    
 <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	       
               <div class="main-inputs" runat="server" id="PublisherInput">					
                   <asp:Label ID="Label2" runat="server" Text="Execute new tsk"></asp:Label>
            			 
			            <div class="form-field">
                            <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">CSV File:</label>
                            <input id="f_csv" type="file" runat="server" class="form-file"/>
		                    <input id="btn_upload" type="submit" value="Upload" runat="server" onserverclick="UploadBtn_Click" class="btn" />
                            <label  runat="server" id="lbl_FileUpload"></label>
                            <asp:HiddenField ID="hf_filepath" runat="server" />
                       </div>  
                   <div class="clear"></div>
                   <div class="form-field">
                            <label for="form-subsegment" class="label" runat="server" id="Label1">Campaign:</label>
                       <asp:DropDownList ID="ddl_Campaign" runat="server"></asp:DropDownList>
			              
                       </div>  
                   <div class="clear"></div>
                   <div class="form-field">
                       <asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				                    onclick="btn_Click" OnClientClick="EnableThisButton(this);"></asp:LinkButton>
                       <asp:Image ID="img_RunReport_loader" style="display:none;" CssClass="img_RunReport_loader" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" />
                   </div>
                   <div class="clear"></div>
                   <div class="form-field">
                       <asp:Label ID="lbl_message" runat="server"  ForeColor="Red"></asp:Label>
                   </div>    
        </div>
            <div class="main-inputs" runat="server" id="Div1">					
                   <asp:Label ID="Label3" runat="server" Text="Re-execute task"></asp:Label>
            			
                   <div class="form-field">
                            <label for="form-subsegment" class="label" runat="server" id="Label6">Task:</label>
                       <asp:DropDownList ID="ddl_ExecTask" runat="server"></asp:DropDownList>
			              
                       </div>  
                   <div class="clear"></div>
                   <div class="form-field">
                       <asp:LinkButton ID="btn_ExecTask" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				                    onclick="btn_Task_Click" OnClientClick="EnableThisButton(this);"></asp:LinkButton>
                        <asp:Image ID="Image1" style="display:none;" CssClass="img_RunReport_loader" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" />

                   </div>
                   <div class="clear"></div>
                   <div class="form-field">
                       <asp:Label ID="lbl_messageTask" runat="server"  ForeColor="Red"></asp:Label>
                   </div>    
        </div>
    </div>
    </div>
</asp:Content>

