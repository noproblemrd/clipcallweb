﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.IO;

public partial class Publisher_PublisherRefundReport : PageSetting, ICallbackEventHandler
{
    const int DAYS_INTERVAL = 2;
    const string FILE_NAME = "RefundReport.xml";
    const string EXCEL_NAME = "RefundReport";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            SetToolbox();
            SetDateInterval();
            LoadExperties();
            ExecReport();
        }
        else
        {
            if (!ScriptManager1.IsInAsyncPostBack)
            {
                string _str = GetCallbackResult();
                if(!string.IsNullOrEmpty(_str))
                    ClientScript.RegisterStartupScript(this.GetType(), "ReciveServerData", "ReciveServerData('" + _str + "');", true);
            }

        }
        SetupClient();
        SetToolboxEvents();
        Header.DataBind();
    }
    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (RefundReportRequestV == null)
            return;
        WebReferenceReports.RefundReportRequest _request = RefundReportRequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        DataResult dr = GetDataReport(_request);
        BindDataToGridView(dr);
    }    
    /*
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (!string.IsNullOrEmpty(PathV))
        {

            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSLine.swf";
            //         dbug_log.ExceptionLog(new Exception(), "path= " + PathV + "_chart= " + _chart);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + _chart + "');", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CleanChart",
                "CleanChart();", true);

    }
     * */
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_refundReport.Text);

    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        /*
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, EXCEL_NAME);

        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
         * */
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        WebReferenceReports.RefundReportRequest _request = RefundReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataResult dr = GetDataReport(_request);
        ToExcel te = new ToExcel(this, EXCEL_NAME);

        if (!te.ExecExcel(dr.data))
            Update_Faild();
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        /*
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordToPrint + "');", true);
            return;
        }
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
         * */
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        WebReferenceReports.RefundReportRequest _request = RefundReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataResult dr = GetDataReport(_request);
        Session["data_print"] = dr.data;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecReport();
    }
    void ExecReport()
    {
        PathV = string.Empty;
        WebReferenceReports.RefundReportRequest _request = new WebReferenceReports.RefundReportRequest();
        _request.FromDate = FromToDate1.GetDateFrom;
        _request.ToDate = FromToDate1.GetDateTo;
        if (_request.FromDate == DateTime.MinValue || _request.ToDate == DateTime.MinValue)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "WrongDateFormat",
                "alert('" + FromToDate1.GetWrongDateFormat() + "');", true);
            return;
        }
        /*
        string heading = txt_MainExpertiser.Text;
        if (!string.IsNullOrEmpty(heading))
        {
            ExpertiserService ExpS = new ExpertiserService();
            _request.HeadingId = ExpS.GetGuidExpertise(heading);
        }
         * */
        _request.HeadingId = new Guid(ddl_Heading.SelectedValue);
        _request.PageNumber = 1;
        _request.PageSize = TablePaging1.ItemInPage;
        RefundReportRequestV = _request;
        DataResult dr = GetDataReport(_request);
        if (dr == null)
        {
            ClearGridView();
            Update_Faild();
            return;
        }
        BindDataToGridView(dr);
        if (dr.data.Rows.Count == 0)
        {
            ClearChart();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
        }
        else
            Load_charts();
    }
    private void ClearChart()
    {
        PathV = string.Empty;
        Charts_Area.Controls.Clear();
        _UpdatePanelGraph.Update();
    }
    void ClearGridView()
    {
        _GridView.DataSource = null;
        _GridView.DataBind();
        LoadGeneralData(0, 0, 0);
        _UpdatePanel.Update();
    }
    void BindDataToGridView(DataResult dr)
    {
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
        _GridView.DataSource = dr.data;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = _TotalRows + " " + ToolboxReport1.RecordMached;
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    DataResult GetDataReport(WebReferenceReports.RefundReportRequest _request)
    {
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfRefundReportResponse result = null;
        try
        {
            result = _reports.RefundsReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        
        DataTable data = new DataTable();
        data.Columns.Add("AmountOfApprovedRequests", typeof(string));
        data.Columns.Add("AmountOfRefundRequests", typeof(string));
        data.Columns.Add("CallsWon", typeof(string));
        data.Columns.Add("Date", typeof(string));
        data.Columns.Add("PercentApprovedFromCalls", typeof(string));
        data.Columns.Add("PercentApprovedFromRequests", typeof(string));
        data.Columns.Add("PercentRefundRequestsOfCalls", typeof(string));

        foreach (WebReferenceReports.RefundReportRow rrr in result.Value.DataList)
        {
            DataRow row = data.NewRow();
            row["AmountOfApprovedRequests"] = rrr.AmountOfApprovedRequests + "";
            row["AmountOfRefundRequests"] = rrr.AmountOfRefundRequests + "";
            row["CallsWon"] = rrr.CallsWon + "";
            row["Date"] = string.Format(siteSetting.DateFormat, rrr.Date);
            row["PercentApprovedFromCalls"] = string.Format(NUMBER_FORMAT, rrr.PercentApprovedFromCalls) + "%";
            row["PercentApprovedFromRequests"] = string.Format(NUMBER_FORMAT, rrr.PercentApprovedFromRequests) + "%";
            row["PercentRefundRequestsOfCalls"] = string.Format(NUMBER_FORMAT, rrr.PercentRefundRequestsOfCalls) + "%";
            data.Rows.Add(row);
        }
        DataResult dr = new DataResult() { CurrentPage = result.Value.CurrentPage, TotalPages = result.Value.TotalPages, TotalRows = result.Value.TotalRows, data = data };
        return dr;
    }
    private void Load_charts()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "load_charts", "setTimeout('SetCharts();', 200);", true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetOnLoadChart", "SetOnLoadChart();", true);
    }
    /*
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
     * */
    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["PrimaryExpertise"] == null || xdd["PrimaryExpertise"]["Error"] != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XmlNode nodePrimary in xdd["PrimaryExpertise"].ChildNodes)
        {

            string pName = nodePrimary.Attributes["Name"].InnerText;
            string _id = nodePrimary.Attributes["Code"].Value;
            string _guid = nodePrimary.Attributes["ID"].Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;

    }
    #region  chart
    string LoadChart(DataTable data)
    {
        
    //    string SymboleValue = CriteriaSetting.GetSymbolCriteria(ceriteria, siteSetting.CurrencySymbol);
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + lbl_refundReport.Text + @"' ");
        //    sb.Append(@"subCaption='In Thousands' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='3' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");
        sb.Append(@"numberSuffix='%' ");
        /*
        if (SymboleValue == "%")
            sb.Append(@"numberSuffix='%' ");
        else if (!string.IsNullOrEmpty(SymboleValue))
            sb.Append(@"numberprefix='" + SymboleValue + @"' ");
        */
        //    sb.Append(@"rotateNames='1' ");
        //     sb.Append(@"slantLabels='1' ");
    
        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar=' ' ");

        sb.Append(@"bgColor='FFFFFF'>");

        
        
        int modulu = data.Rows.Count / 15;
        modulu++;

        StringBuilder sbApproved = new StringBuilder();
        StringBuilder sbRefund = new StringBuilder();
        sb.Append(@"<categories>");
        sbApproved.Append(@"<dataset seriesName='" + lbl_ApprovedRefundsCalls.Text + @"' color='808080' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
        sbRefund.Append(@"<dataset seriesName='" + lbl_RefundRequestsPercent.Text + @"' color='0080C0'  >");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'
        /*
         * result.Value.DataList[0].PercentApprovedFromCalls;       
        result.Value.DataList[0].PercentRefundRequestsOfCalls;
         */
        foreach (DataRow row in data.Rows)
        {
            string PercentApprovedFromCalls = row["PercentApprovedFromCalls"].ToString().Replace("%", "");
            string PercentRefundRequestsOfCalls = row["PercentRefundRequestsOfCalls"].ToString().Replace("%", "");
            string _title = lbl_ApprovedRefundsCalls.Text + "=" + PercentApprovedFromCalls + "\r\n" +
                lbl_RefundRequestsPercent.Text + "=" + PercentRefundRequestsOfCalls + "\r\n";
            sb.Append(@"<category name='" + row["Date"].ToString() + @"' toolText='" + _title + "'/>");

            sbApproved.Append(@"<set value='" + PercentApprovedFromCalls + "'/>");
            sbRefund.Append(@"<set value='" + PercentRefundRequestsOfCalls + "'/>");
        }
        sb.Append("</categories>");
        sbApproved.Append(@"</dataset>");
        sbRefund.Append(@"</dataset>");

        sb.Append(sbApproved.ToString());
        sb.Append(sbRefund.ToString());
        sb.Append(@"</graph>");


        string path = //HttpContext.Current.Request.PhysicalApplicationPath + 
            ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) { return string.Empty; }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        return pathWeb;
        
    }
    
    #endregion

    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        return PathV;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        WebReferenceReports.RefundReportRequest _request = RefundReportRequestV;
        if (_request == null)
            return;
    //    WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);

        _request.PageNumber = -1;
        _request.PageSize = -1;
        DataResult dr = GetDataReport(_request);
        string xml_file = LoadChart(dr.data);
        PathV = xml_file;
    }
    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }

    #endregion
    /*
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
     * */
    /*
    SortedDictionary<string, string> DicExpertiseV
    {
        get { return (Session["AutoCompleteListExp"] == null) ? null : (SortedDictionary<string, string>)Session["AutoCompleteListExp"]; }
        set { Session["AutoCompleteListExp"] = value; }
    }
     * */
    string PathV
    {
        get { return (Session["XmlGraph"] == null) ? string.Empty : (string)Session["XmlGraph"]; }
        set { Session["XmlGraph"] = value; }
    }
    WebReferenceReports.RefundReportRequest RefundReportRequestV
    {
        get { return (ViewState["RefundReportRequest"] == null) ? null : (WebReferenceReports.RefundReportRequest)ViewState["RefundReportRequest"]; }
        set { ViewState["RefundReportRequest"] = value; }
    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
    protected string GetChartType
    {
        get { return ResolveUrl("~") + "Management/FusionCharts/Charts/MSLine.swf"; }
    }
}
