﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master"  EnableEventValidation="true" 
AutoEventWireup="true" CodeFile="Languages.aspx.cs" Inherits="Publisher_Languages" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asps" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

    <link href="PublishStyle.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript" language="javascript">
    function addLang()
    {
        var ListLang = document.getElementById("<%#selectLanguages.ClientID%>");   
        
        if(ListLang.selectedIndex >= 0)
        {                  
            var ListChoose = document.getElementById("<%#selectLanguages2.ClientID%>");          
            
            ListChoose.options[ListChoose.options.length] = 
                new Option(ListLang.options[ListLang.selectedIndex].text, ListLang.options[ListLang.selectedIndex].value);
            ListLang.remove(ListLang.selectedIndex);
            SetDDLLang();
         }                
    }
    function removeLang()
    {
        
        var ListChoose = document.getElementById("<%#selectLanguages2.ClientID%>");  
        var ListLang = document.getElementById("<%#selectLanguages.ClientID%>");
        var ddl_lang = document.getElementById("<%#ddl_chooseLang.ClientID%>");
            
        if(ListChoose.selectedIndex>=0)
        {                  
            if(ddl_lang.options.length>0)
            {
                var _default = ddl_lang.options[ddl_lang.selectedIndex].value;
                if(_default ==  ListChoose.options[ListChoose.selectedIndex].value)
                {
                    alert(document.getElementById("<%# hf_DeleteDefaultLang.ClientID %>").value);
                    return;
                }
            }       
            ListLang.options[ListLang.options.length] = 
                new Option(ListChoose.options[ListChoose.selectedIndex].text, ListChoose.options[ListChoose.selectedIndex].value);
            ListChoose.remove(ListChoose.selectedIndex);
            SetDDLLang();
            
         }   
    }
    function SetDDLLang()
    {
        var ListChoose = document.getElementById("<%#selectLanguages2.ClientID%>");
        var ddl_lang = document.getElementById("<%#ddl_chooseLang.ClientID%>");
        
        var _select=(ddl_lang.selectedIndex>-1)?ddl_lang.options[ddl_lang.selectedIndex].value:"0";
        var _arr=new Array();
        
        for(var i=0;i<ListChoose.length;i++)
        {
            _arr.push(ListChoose.options[i].text+";"+ListChoose.options[i].value);
        }
        _arr.sort();       
        ListChoose.length=0;
        ddl_lang.length=0;
        var _i=0;
        for(var i=0;i<_arr.length;i++)
        {
            var _val=_arr[i].split(";");
         //   var _option = new Option(_val[0], _val[1]);
            ListChoose.options[ListChoose.options.length] = new Option(_val[0], _val[1]);          
            ddl_lang.options[ddl_lang.options.length] = new Option(_val[0], _val[1]);
            if(_val[1]==_select)
                _i=i;
        }
        ddl_lang.selectedIndex=_i;      
    }
    function ChkIfExists()
    {
        var ddl = document.getElementById("<%#ddl_chooseLang.ClientID%>");
        var hflist = document.getElementById("<%#hf_array.ClientID%>");
        var _arrstr = new String();
        _arrstr="";
        var _count= ddl.options.length;
        if(_count>0)
        {
            var select_arr=new Array();
            for(var i=0;i<ddl.length;i++)
            {
                _arrstr=_arrstr+ddl.options[i].value+";";
                select_arr.push(ddl.options[i].value);
            }
            var mess="";
            var Source_arr=document.getElementById("<%# hf_source.ClientID %>").value.split(";");
            for(var i=0;i<Source_arr.length;i++)
            {
               
                var _elems = Source_arr[i].split(',');
                var IsExists = false;
                for (var j=0;j<select_arr.length;j++)
                {
                    if(select_arr[j]==_elems[0])
                    {
                        IsExists=true;
                        break;
                    }
                }
                if(!IsExists)
                {                   
                    mess=mess+((mess.length!=0)?", ":" ");
                    mess=mess+_elems[1];
                }
            }
            if(mess.length>0)
            {
                var delete1 = document.getElementById("<%# hf_ToDelete1.ClientID %>").value;
                var delete2 = document.getElementById("<%# hf_ToDelete2.ClientID %>").value;
                var delete3 = document.getElementById("<%# hf_ToDelete3.ClientID %>").value;
                var NewLine = '\r\n';
                var IfConfirmed = confirm(delete1+NewLine+delete2+NewLine+delete3+NewLine+mess);
                if(!IfConfirmed)
                    return false;
            }
            hflist.value=_arrstr;
            document.getElementById("<%# hf_selected.ClientID %>").value=ddl.options[ddl.selectedIndex].value;
            return true;
        }
        else
            return false;
    }
    function confirmdChange()
    {
        var _selectedIndex = document.getElementById("<%# hf_selectedIndex.ClientID %>");
        var ddl=document.getElementById("<%# ddl_chooseLang.ClientID %>");
        var _lang = ddl.options[ddl.selectedIndex].text;
        var _con = confirm(document.getElementById("<%# hf_changeDefault.ClientID %>").value+" "+ _lang+"?");
        if(!_con)        
            ddl.selectedIndex = parseInt(_selectedIndex.value);        
        else
            _selectedIndex.value = ddl.selectedIndex;
    }
   
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
 </cc1:ToolkitScriptManager>

<div class="page-content minisite-content" >
		    <h2><asp:Label ID="lblSubTitle" runat="server">Languages</asp:Label></h2>		
	    <div id="form-analytics">
	        <div class="lists-select clearfix">
                <div class="list-select all">
	                <label for="form-all" runat="server" id="lblAllList">
                        <asp:Label ID="lbl_AllList" runat="server" Text="All List"></asp:Label>
	                </label>
	                <select id="selectLanguages" multiple="true" class="select" runat="server" ondblclick="javascript:addProfession();"></select>			
	                <a href="#" class="btn" runat="server" id="btnAdd" onclick="javascript:addLang();">Add</a>
                </div>
                <div class="list-select choice">
	                <label for="form-choice" runat="server" id="lblYourChoice">
                        <asp:Label ID="lbl_yourChoice" runat="server" Text="Your choice"></asp:Label>
	                </label>
	                <select id="selectLanguages2"  multiple="true" class="select" runat="server" ondblclick="javascript:removeProfession();"></select>
                    <input type="hidden" id="hidden_professions" class="form-select" runat="server" />			
	                <a href="#" runat="server" id="a_Remove" class="btn" onclick="javascript:removeLang();">Remove</a>
                </div>
            </div>
            <div class="language">
                <asp:Label ID="lbl_DefaultLang" CssClass="label" runat="server" Text="Default language"></asp:Label>
                <asp:DropDownList ID="ddl_chooseLang" CssClass="form-select" runat="server"
                onchange="confirmdChange();"></asp:DropDownList>
            </div>
            <asp:Button ID="btn_set" runat="server" Text="Set" CssClass="form-submit" 
            OnClientClick="return ChkIfExists();" onclick="btn_set_Click" />
  
        </div>

    
    </div>
            
    <asp:HiddenField ID="hf_array" runat="server" />
    <asp:HiddenField ID="hf_selected" runat="server" />
    <asp:HiddenField ID="hf_source" runat="server" />
    
    <asp:HiddenField ID="hf_ToDelete1" runat="server" Value="Attention! Deleting a language will delete all  translations as well. If you want to use this language again you will have to translate all the labels again" />
    <asp:HiddenField ID="hf_ToDelete2" runat="server" Value="After delete, language setting will be reset to default for all users and advertisers using this language" />
    <asp:HiddenField ID="hf_ToDelete3" runat="server" Value="Are you sure you want to delete this languages?" />
    <asp:HiddenField ID="hf_DeleteDefaultLang" runat="server" Value="You can not remove the default language" />
    
    <asp:HiddenField ID="hf_changeDefault" runat="server" Value="Do you want to change your default language to" />
    
    <asp:HiddenField ID="hf_selectedIndex" runat="server" />
	     
 
 

</asp:Content>

