﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Reflection;

public partial class Publisher_Dashboard : PageSetting
{
    const int WEEK_FROM_TODAY = 6;
    const string FILE_NAME = "Dashboard.xml";
    
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {
            LoadInterval();
            LoadShowMe();
            setDateTime();
            LoadDefaultReport();
            LoadSentences();
            setSentencesDatePicker();
        }
        LoadValidationCompare();
        ScriptManager1.RegisterAsyncPostBackControl(btn_run);
        Header.DataBind();  
        
    }

    private void setSentencesDatePicker()
    {
        FromToDatePicker_current.SetFromSentence = lbl_activity.Text;
        FromToDatePicker_compare.SetFromSentence = lbl_activityPast.Text;
    }

    private void LoadSentences()
    {
        if (string.IsNullOrEmpty(lbl_sooner.Text))
            lbl_sooner.Text = @"The value should be sooner than the value in the 'activity dates from' field.";
    }

    private void LoadDefaultReport()
    {        
        WebReferenceReports.DashboardReportRequest request = new WebReferenceReports.DashboardReportRequest();
        request.FromDate = DateTime.Now.AddDays(-6);
        request.ToDate = DateTime.Now;

        WebReferenceReports.Interval _interval = WebReferenceReports.Interval.Days;
        foreach (WebReferenceReports.Interval inter in Enum.GetValues(typeof(WebReferenceReports.Interval)))
        {
            if (inter.ToString() == ddl_Interval.SelectedValue)
                _interval = inter;
        }

        request.Interval = _interval;
        request.ShowMe = GetCrireria(ddl_Showme.SelectedValue);
        request.CompareToPast = false;
        request.ShowObjectives = false;
        
        RunReport(request);    
    }

    private void LoadValidationCompare()
    {
        //compare dates
        TextBox txt_to = FromToDatePicker_compare.GetTextBoxTo();
        string _from = FromToDatePicker_compare.GetTextBoxFrom().ClientID;
        txt_to.Attributes.Add("readonly", "readonly");
        txt_to.CssClass = "_to label_DatePicker form-textcal";
        string _ScriptFunc = "if(OnBlurString(this)) SetCompareDate(this, '" + FromToDatePicker_current.GetParentDivId() +
            @"', """ + lbl_sooner.Text + @""", '" + FromToDatePicker_current.GetTextBoxFrom().ClientID + "');";
        string FromScript = @"$('#" + _from + @"').datepicker({ onClose: function(dateText, inst) { " + _ScriptFunc + " }});";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "FromCompareScript", FromScript, true);
        //current date
        string current_fromId = FromToDatePicker_current.GetTextBoxFrom().ClientID;
        string current_toId = FromToDatePicker_current.GetTextBoxTo().ClientID;
        
        string InsideScript = "var _result = OnBlurByDays(this); if(_result == 1) Set_Dates_Compars(); else if(_result == -10) " +
            "{ alert('" + lbl_DatesChange.Text + "'); Set_Dates_Compars(); }";
        string from_script =
            @"$('#" + current_fromId + @"').datepicker({ onClose: function(dateText, inst) { " + InsideScript + "}});";
        string to_script =
            @"$('#" + current_toId + @"').datepicker({ onClose: function(dateText, inst) { " + InsideScript + "}});";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "FromCurrentScript", from_script, true);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ToCurrentScript", to_script, true);
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);        
        if (!string.IsNullOrEmpty(PathV))
        {
            
            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSArea.swf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + _chart + "');", true);
        }
        
    }
    private void LoadShowMe()
    {
        ddl_Showme.Items.Clear();
        Dictionary<string, string> dic = new Dictionary<string, string>();
        foreach(string s in Enum.GetNames(typeof(WebReferenceReports.Criteria)))
        {
            string _txt = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "Criteria", s);
            dic.Add(s, _txt);
    //        ddl_Showme.Items.Add(new ListItem(_txt, s));
        }
        IEnumerable<KeyValuePair<string, string>> query = (from x in dic
                                                           select new KeyValuePair<string, string>(x.Key, x.Value)).OrderBy(x => x.Value);
        foreach (KeyValuePair<string, string> kvp in query)
        {
            ddl_Showme.Items.Add(new ListItem(kvp.Value, kvp.Key));
        }
    }

    private void LoadInterval()
    {
        ddl_Interval.Items.Clear();
        foreach (string s in Enum.GetNames(typeof(WebReferenceReports.Interval)))
        {
            string _txt = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "Interval", s);
            ddl_Interval.Items.Add(new ListItem(_txt, s));
        }
    }   

    private void setDateTime()
    {
        FromToDatePicker_current.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
            WEEK_FROM_TODAY);
    }
    protected void btn_run_Click(object sender, EventArgs e)
    {
        requestV = null;
        WebReferenceReports.DashboardReportRequest request = new WebReferenceReports.DashboardReportRequest();
        request.FromDate = FromToDatePicker_current.GetDateFrom;
        request.ToDate = FromToDatePicker_current.GetDateTo;

        WebReferenceReports.Interval _interval = WebReferenceReports.Interval.Days;
        foreach (WebReferenceReports.Interval inter in Enum.GetValues(typeof(WebReferenceReports.Interval)))
        {
            if (inter.ToString() == ddl_Interval.SelectedValue)
                _interval = inter;
        }
       
        request.Interval = _interval;
        request.ShowMe = GetCrireria(ddl_Showme.SelectedValue);
        foreach (ListItem _li in cbl_Compare.Items)
        {
            if (_li.Value == "CompareToPast" && _li.Selected)
            {
                request.CompareToPast = true;
                request.PastFromDate = FromToDatePicker_compare.GetDateFrom;
                request.PastToDate = FromToDatePicker_compare.GetDateTo;
            }
            else if (_li.Value == "ShowObjectives" && _li.Selected)
                request.ShowObjectives = true;
            
        }
        RunReport(request);    
        
        
        /*
        result.Value.TableRows[0].Gap;
        result.Value.TableRows[0].Objective;
        result.Value.TableRows[0].PastPeriod;
        result.Value.TableRows[0].Percentage;
        result.Value.TableRows[0].Row;
        result.Value.TableRows[0].Total;
        */
    }
    void RunReport(WebReferenceReports.DashboardReportRequest request)
    {
        request.CurrencySymbol = siteSetting.CurrencySymbol;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfDashboardReportResponse result = null;
        try
        {
            result = _report.DashboardReport(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Value == null)
            return;
        requestV = request;
    //    Dictionary<string, bool> ToShow = new Dictionary<string, bool>();
        DataTable data = new DataTable();
        data.Columns.Add("Gap");
        data.Columns.Add("Objective");
        data.Columns.Add("PastPeriod");
        data.Columns.Add("Percentage");
        data.Columns.Add("Row");
        data.Columns.Add("RowText");
        data.Columns.Add("Total");
        data.Columns.Add("ColorPer", typeof(System.Drawing.Color));
        data.Columns.Add("ColorGap", typeof(System.Drawing.Color));
        data.Columns.Add("Group", typeof(int));
        data.Columns.Add("GroupName", typeof(string));

        data.Columns.Add("show_objective", typeof(bool));
        data.Columns.Add("show_Percent", typeof(bool));
        data.Columns.Add("show_PastPeriod", typeof(bool));
        data.Columns.Add("show_Gap", typeof(bool));
        /*
        foreach (DataColumn dc in data.Columns)
        {            
            ToShow.Add(dc.ColumnName, false);
        }
         * */
       
        foreach (WebReferenceReports.TableData td in result.Value.TableRows)
        {
            DataRow row = data.NewRow();
            row["Gap"] = td.Gap;
            row["show_Gap"] = SetVisibleTR(td.Gap);
                    
            row["Objective"] = td.Objective;
            row["show_objective"] = SetVisibleTR(td.Objective);
            row["PastPeriod"] = td.PastPeriod;
            row["show_PastPeriod"] = SetVisibleTR(td.PastPeriod);
            row["Percentage"] = td.Percentage;
            row["show_Percent"] = SetVisibleTR(td.Percentage);
            
            row["Row"] = td.Row.ToString();
            
            row["Total"] = td.Total;
            
            row["RowText"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "Criteria", td.Row.ToString());
            
            row["Group"] = td.Group;
            row["GroupName"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eGroupDashboard", ((eGroupDashboard)td.Group).ToString());//lbl_Group.Text + " " + td.Group;
            row["ColorGap"] = System.Drawing.Color.FromName(td.GapColor.ToString());
            row["ColorPer"] = System.Drawing.Color.FromName(td.PercentageColor.ToString());
            data.Rows.Add(row);
        }
        dataV = data;
        LoadChart(result.Value, ddl_Showme.SelectedItem.Text, request.ShowMe);
        SetTable(data);        
        
    }
    bool SetVisibleTR(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
            return false;
        return true;
    }
    private void SetTable(DataTable data)
    {

        var ie = (from x in data.AsEnumerable()
                  //   where x.Field<int>("Group") == _group
                  select new
                  {
                      Group = x.Field<int>("Group"),
                      show_objective = x.Field<bool>("show_objective"),
                      show_Percent = x.Field<bool>("show_Percent"),
                      show_PastPeriod = x.Field<bool>("show_PastPeriod"),
                      show_Gap = x.Field<bool>("show_Gap"),
                      GroupName = x.Field<string>("GroupName")
                  }
                                   ).Distinct();

        int index_visible = 2;
        if (ie.First().show_objective)
            index_visible++;
        if (ie.First().show_Percent)
            index_visible++;
        if (ie.First().show_PastPeriod)
            index_visible++;
        if (ie.First().show_Gap)
            index_visible++;

        _Repeater.DataSource = ie;
        _Repeater.DataBind();
        int i = 0;
        foreach (RepeaterItem item in _Repeater.Items)
        {
            //only in the first group
            HtmlTableRow _tr = (HtmlTableRow)item.FindControl("tr_head");            
            _tr.Visible = (i++ == 0);
            //set colspan group
            HtmlTableCell _tc = (HtmlTableCell)item.FindControl("td_group");
            _tc.ColSpan = index_visible;

            int _group = int.Parse(((Label)item.FindControl("lbl_Group")).Text);
            Repeater repeater_in = (Repeater)item.FindControl("_Repeater_in");
            IEnumerable<DataRow> te = (from x in data.AsEnumerable()
                                       where x.Field<int>("Group") == _group
                                       select x);



            DataTable _data = (te.Count() == 0) ? null : te.CopyToDataTable();
            repeater_in.DataSource = _data;
            repeater_in.DataBind();
            int _index = 0;
            foreach (RepeaterItem item_in in repeater_in.Items)
            {
                HtmlTableRow tr = (HtmlTableRow)item_in.FindControl("tr_data");
                string _class = (_index++ % 2 == 0) ? "odd" : "even";
                tr.Attributes.Add("class", _class);
            }
        }       
        UpdatePanel_table.Update();
         
    }
    #region  chart
    bool LoadChart(WebReferenceReports.DashboardReportResponse result, string caption, WebReferenceReports.Criteria ceriteria)
    {

        string SymboleValue = CriteriaSetting.GetSymbolCriteria(ceriteria, siteSetting.CurrencySymbol);
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + caption + @"' ");
    //    sb.Append(@"subCaption='In Thousands' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='19' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");

        if(SymboleValue == "%")
            sb.Append(@"numberSuffix='%' ");
        else if(!string.IsNullOrEmpty(SymboleValue))
            sb.Append(@"numberprefix='"+SymboleValue+@"' ");
            
    //    sb.Append(@"rotateNames='1' ");
   //     sb.Append(@"slantLabels='1' ");
        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar='=' ");
         
        sb.Append(@"bgColor='FFFFFF'>");

        
        int NumOf_X = result.CurrentGraph.Count();

        int modulu = result.CurrentGraph.Count() / 15;
        modulu++;

        sb.Append(@"<categories>");
        for (int i = 0; i < result.CurrentGraph.Count(); i++)
        {
            string _title = lbl_Current.Text + "= " + result.CurrentGraph[i].X + ", (" +
                String.Format("{0:0.##}", result.CurrentGraph[i].Y) + ")";
            if (result.ObjectivesGraph != null && result.ObjectivesGraph.Count() > i)
                _title += "\r\n" + lbl_objective.Text + "= " + result.ObjectivesGraph[i].X + ", (" +
                    String.Format("{0:0.##}", result.ObjectivesGraph[i].Y) + ")";
            if (result.PastGraph != null && result.PastGraph.Count() > i)
                _title += "\r\n" + lbl_PastPeriod.Text + "= " + result.PastGraph[i].X + ", (" +
                    String.Format("{0:0.##}", result.PastGraph[i].Y) + ")";

            _title += "\r\n" + lbl_CurrentDot.Text;
            
           //***Dilute labels***//
     //       if ((i + 1) % modulu == 0)
                sb.Append(@"<category name='" + result.CurrentGraph[i].X + @"' toolText='" + _title + "'/>");
    //        else
   //             sb.Append(@"<category toolText='" + _title + "'/>");
        }
        sb.Append("</categories>");


        

        
        if (result.PastGraph != null)
        {
            sb.Append(@"<dataset seriesName='" + lbl_PastPeriod.Text + @"' color='808080'  anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'>");
            for (int i = 0; i < result.PastGraph.Count() && i < NumOf_X; i++)
            {
                sb.Append(@"<set value='" + String.Format("{0:0.##}", result.PastGraph[i].Y) + "'/>");
            }
            sb.Append(@"</dataset>");
        }
        if (result.ObjectivesGraph != null)
        {
            sb.Append(@"<dataset seriesName='" + lbl_objective.Text + @"' color='008040'  anchorBgColor='008040' plotBorderColor='#008040' plotBorderThickness='3'>");
            for (int i = 0; i < result.ObjectivesGraph.Count(); i++)
            {
                sb.Append(@"<set value='" + String.Format("{0:0.##}", result.ObjectivesGraph[i].Y) + "'/>");
            }
            sb.Append(@"</dataset>");
        }

        sb.Append(@"<dataset seriesName='" + lbl_Current.Text + @"' color='0080C0'  anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'>");
        for (int i = 0; i < result.CurrentGraph.Count(); i++)
        {
            sb.Append(@"<set value='" + String.Format("{0:0.##}", result.CurrentGraph[i].Y) + "'/>");
        }
        sb.Append(@"</dataset>");

        sb.Append(@"</graph>");



        string path = ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;
        
        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) { return false; }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        return true;
    }
    protected void lb_row_click(object sender, EventArgs e)
    {
        WebReferenceReports.DashboardReportRequest request = requestV;
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        LinkButton lb = (LinkButton)sender;
        string command = lb.CommandArgument;
        request.OnlyGraphs = true;
        request.ShowMe = GetCrireria(command);
        WebReferenceReports.ResultOfDashboardReportResponse result = null;
        try
        {
            result = _report.DashboardReport(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (ListItem li in ddl_Showme.Items)
        {
            li.Selected = (li.Value == command);                
        }
        _UpdatePanel_Showme.Update();
        LoadChart(result.Value, lb.Text, request.ShowMe);
       
    }
    WebReferenceReports.Criteria GetCrireria(string name)
    {        
        foreach (WebReferenceReports.Criteria crit in Enum.GetValues(typeof(WebReferenceReports.Criteria)))
        {
            if (crit.ToString() == name)
                return crit;
        }
        return WebReferenceReports.Criteria.AmountOfCalls;
    }
    #endregion
    protected string GetDivCurrentId
    {
        get { return FromToDatePicker_current.GetParentDivId(); }
    }
    protected string GetDivPastId
    {
        get { return FromToDatePicker_compare.GetParentDivId(); }
    }
    protected string GetDateFormat()
    {
        return ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat);
    }
    protected string GetFromCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxFrom().ClientID; }
    }
    protected string GetToCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxTo().ClientID; }
    }

    protected string GetFromPastId
    {
        get { return FromToDatePicker_compare.GetTextBoxFrom().ClientID; }
    }
    protected string GetToPastId
    {
        get { return FromToDatePicker_compare.GetTextBoxTo().ClientID; }
    }
    protected string GetInvalidDate
    {
        get { return Server.HtmlEncode(lbl_DateError.Text); }
    }
    private DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    private string PathV
    {
        get { return (ViewState["Path"] == null) ? string.Empty : (string)ViewState["Path"]; }
        set { ViewState["Path"] = value; }
    }
    private WebReferenceReports.DashboardReportRequest requestV
    {
        get { return (ViewState["_request"] == null) ? null : (WebReferenceReports.DashboardReportRequest)ViewState["_request"]; }
        set { ViewState["_request"] = value; }
    }
    protected string Lbl_DatesChange
    {
        get { return lbl_DatesChange.Text; }
    }
}
