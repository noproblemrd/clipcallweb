﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_InvalidRequestSession : PublisherPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string _session = Request.QueryString["session"];
            if (string.IsNullOrEmpty(_session))
            {
                closeTheWindow(GetLogin);
                return;
            }
            LoadDetails(_session);
        }
    }

    private void LoadDetails(string _session)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfDescriptionValidationReportRow result = null;
        try
        {
            result = _report.DescriptionValidationSessionDrillDown(_session);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        DataTable data = GetDataTable.GetDataTableFromList(result.Value);
        _GridView.DataSource = data;
        _GridView.DataBind();
    }
    
    protected string GetLogin
    {
        get { return "LogOut.aspx"; }
    }
}