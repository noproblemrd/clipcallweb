﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Publisher_AdvertisersReport : PageSetting
{
    const int DAYS_INTERVAL = 182;
    const string REPORT_NAME = "AdvertiserReport";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        LoadStepRegistration_Sentence();
        if (!IsPostBack)
        {
            SetToolbox();
            SetDateInterval();
            LoadWebUserRegistrant();
            LoadAdvertisersStatus();
            LoadIsFrom();
            LoadStepRegistration();
            setValidationGroup();
            ExecReport();
        }
        SetToolboxEvents();
        Header.DataBind();
      //  setValidationGroup();
    }

    private void LoadStepRegistration()
    {
        Dictionary<string, string> dic = dic_translate_RegistrationV;
        foreach (WebReferenceReports.eCompletedRegistrationStep completed_step in Enum.GetValues(typeof(WebReferenceReports.eCompletedRegistrationStep)))
        {
            //string tran = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eCompletedRegistrationStep", completed_step.ToString());
           // dic.Add(completed_step.ToString(), tran);
            if ((int)completed_step == 0)
                continue;
            int _num = (completed_step == WebReferenceReports.eCompletedRegistrationStep.All) ? 1 : (int)completed_step + 1;
            string tran = _num + "- " + dic[completed_step.ToString()];
            ListItem li = new ListItem(tran, completed_step.ToString());
            li.Selected = (completed_step == WebReferenceReports.eCompletedRegistrationStep.PaymentRegistered);
            if (completed_step == WebReferenceReports.eCompletedRegistrationStep.All)
                ddl_CompletedStep.Items.Insert(0, li);
            else
                ddl_CompletedStep.Items.Add(li);
        }
    }
    private void LoadStepRegistration_Sentence()
    {
        if (dic_translate_RegistrationV != null)
            return;
        Dictionary<string, string> dic = new Dictionary<string, string>();
        foreach (WebReferenceReports.eCompletedRegistrationStep completed_step in Enum.GetValues(typeof(WebReferenceReports.eCompletedRegistrationStep)))
        {
            string tran = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eCompletedRegistrationStep", completed_step.ToString());
            dic.Add(completed_step.ToString(), tran);           
        }
        dic_translate_RegistrationV = dic;
    }

    private void LoadWebUserRegistrant()
    {
        foreach (eWebUserRegStatus wurs in Enum.GetValues(typeof(eWebUserRegStatus)))
        {
            string tran = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eWebUserRegStatus", wurs.ToString());
            ListItem li = new ListItem(tran, wurs.ToString());
            li.Selected = (wurs == eWebUserRegStatus.All);
            ddl_WebUserRegistrant.Items.Add(li);
        }
    }
    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
    private void LoadIsFrom()
    {
        ddl_IsFromAar.Items.Clear();
        ddl_IsFromAar.Items.Add(new ListItem(lbl_all.Text, string.Empty));
        Dictionary<eYesNo, string> dic = new Dictionary<eYesNo, string>();
        foreach (eYesNo _eYesNo in Enum.GetValues(typeof(eYesNo)))
        {
            string str = _eYesNo.ToString();
            string _translate = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eYesNo", str);
            ddl_IsFromAar.Items.Add(new ListItem(_translate, str));
            dic.Add(_eYesNo, _translate);
        }
        dicYesNo = dic;
    }

    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (AdvertisersReportRequestV == null)
            return;
        /*
        WebReferenceReports.AdvertisersReportRequest _request = AdvertisersReportRequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
         * */
        DataResult dr = GetDataReport(_pageNum);
        DataBindToGridview(dr);
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lblTitleAdvertisersReport.Text);
    }
    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        
        if(TotalRowsV==0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        
    //    WebReferenceReports.AdvertisersReportRequest _request = AdvertisersReportRequestV;
   //     _request.PageSize = -1;
   //     _request.PageNumber = -1;

        DataTable data = SData; //GetDataReport(_request).data;
        ToExcel te = new ToExcel(this, REPORT_NAME);
        if (!te.ExecExcel(data, _GridView))
            Update_Faild();
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {       
        if(TotalRowsV==0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
   //     WebReferenceReports.AdvertisersReportRequest _request = AdvertisersReportRequestV;
  //      _request.PageSize = -1;
  //      _request.PageNumber = -1;
        DataTable data = SData; //GetDataReport(_request).data;
        Session["data_print"] = data;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    private void setValidationGroup()
    {
    //    string _script = "var _reg = new RegExp('"+Utilities.RegularExpressionForJavascript(@"^\d+\.?\d*?$")"+');"+
   //         "if (
        FromToDate1.SetClientScript(Utilities.RegularExpressionForJavascript(@"^\d+\.?\d*?$"), txt_maxBalance.ClientID);
        FromToDate1.GetBtnSubmit().ValidationGroup = "AdReport";
    }

    private void LoadAdvertisersStatus()
    {
        List<WebReferenceReports.SupplierStatus> dic = new List<WebReferenceReports.SupplierStatus>();
        foreach (WebReferenceReports.SupplierStatus _val in Enum.GetValues(typeof(WebReferenceReports.SupplierStatus)))
        {
            switch (_val)
            {
                case(WebReferenceReports.SupplierStatus.All):
                case (WebReferenceReports.SupplierStatus.Available):
                case (WebReferenceReports.SupplierStatus.Candidate):
                case (WebReferenceReports.SupplierStatus.Inactive):
                case (WebReferenceReports.SupplierStatus.Unavailable):
                    dic.Add(_val);
                    break;
            }
            
            
            
        }
        SupplierStatusComprar stc = new SupplierStatusComprar();
        dic.Sort(stc);
        foreach (WebReferenceReports.SupplierStatus ss in dic)
        {
            string _value = ss.ToString();
            string _txt = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "SupplierStatus", _value);
            bool IsSelect = (ss == WebReferenceReports.SupplierStatus.Available);
            ListItem li = new ListItem();
            li.Value = _value;
            li.Text = _txt;
            li.Selected = IsSelect;
            ddl_Status.Items.Add(li);
        }
    }

    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecReport();
        
    }
    void ExecReport()
    {
        DateTime _fromDate = FromToDate1.GetDateFrom;
        DateTime _toDate = FromToDate1.GetDateTo;
        string _status = ddl_Status.SelectedValue;
        WebReferenceReports.SupplierStatus ss = new WebReferenceReports.SupplierStatus();
        foreach (WebReferenceReports.SupplierStatus _val in Enum.GetValues(typeof(WebReferenceReports.SupplierStatus)))
        {
            if (_val.ToString() == _status)
            {
                ss = _val;
                break;
            }
        }
        decimal _maxValue;
        if (!decimal.TryParse(txt_maxBalance.Text, out _maxValue))
            _maxValue = -100;


        WebReferenceReports.AdvertisersReportRequest _request = new WebReferenceReports.AdvertisersReportRequest();
        if (_fromDate != DateTime.MinValue)
            _request.FromDate = _fromDate;
        if (_toDate != DateTime.MinValue)
            _request.ToDate = _toDate;
        if (_maxValue >= 0)
            _request.MaxBalance = _maxValue;
        _request.SupplierStatus = ss;
       // _request.RegistrationStep
        eYesNo _yes_no;
        if (Enum.TryParse(ddl_IsFromAar.SelectedValue, out _yes_no))
            _request.IsFromAAR = eYesNo.Yes == _yes_no;

        eWebUserRegStatus ewurs;
        if (!Enum.TryParse(ddl_WebUserRegistrant.SelectedValue, out ewurs))
            ewurs = eWebUserRegStatus.All;

        if (ewurs != eWebUserRegStatus.All)
            _request.IsWebRegistrant = (ewurs == eWebUserRegStatus.WebRegistrant);

        WebReferenceReports.eCompletedRegistrationStep crs;
        if (!Enum.TryParse(ddl_CompletedStep.SelectedValue, out crs))
            crs = WebReferenceReports.eCompletedRegistrationStep.PaymentRegistered;
        _request.RegistrationStep = crs;

      //  _request.PageNumber = 1;
     //   _request.PageSize = TablePaging1.ItemInPage;
        AdvertisersReportRequestV = _request;
        DataResult dr = GetDataReport(_request);
        if (dr == null)
        {
            Update_Faild();
            ClearTable();
            return;
        }
        if (dr.TotalRows == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResults", "alert('" + Master.GetNoResultsMessage + "');", true);
            ClearTable();
        }
        else
        {
            DataBindToGridview(dr);
            LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
        }
        
    }
    void ClearTable()
    {
        SData = null;
        _GridView.DataSource = null;
        _GridView.DataBind();
        LoadGeneralData(0, 0, 0);        
        _uPanel.Update();
    }
    void DataBindToGridview(DataResult dr)
    {
        _GridView.DataSource = GetBindData(dr);
        _GridView.DataBind();        
        _uPanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = GetTotalRecords(_TotalRows);// ToolboxReport1.GetRecordMaches(_TotalRows);
    //    TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    string GetTotalRecords(int _total_rows)
    {
        if(_total_rows > 999)
            return ToolboxReport1.GetRecordMaches();
        return ToolboxReport1.GetRecordMaches(_total_rows);
    }
    DataResult GetDataReport(WebReferenceReports.AdvertisersReportRequest _request)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfAdvertisersReportResponse result = null;
        try
        {
            result = _report.AdvertisersReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        _GridView.PageIndex = 0;
        //     result.Value.Suppliers[0].TimeZone
        DataTable data = GetDataTable.GetDataTableColumsString(result.Value.Suppliers);
        Dictionary<string, string> dic_translate_register = dic_translate_RegistrationV;
        foreach (WebReferenceReports.SupplierData sd in result.Value.Suppliers)
        {
            DataRow row = data.NewRow();
            row["Number"] = string.IsNullOrEmpty(sd.Number) ? "****" : sd.Number;
            row["Balance"] = (sd.Balance == 0) ? "0" : String.Format("{0:#,0.##}", sd.Balance);
            row["ContactName"] = sd.ContactName;
            row["CreatonOn"] = String.Format(siteSetting.DateFormat, sd.CreatonOn);
            row["Email"] = sd.Email;
            row["Name"] = sd.Name;
            row["Phone"] = sd.Phone;
            row["StageInRegistration"] = (sd.StageInRegistration == null) ? "---" : dic_translate_register[sd.StageInRegistration.Value.ToString()];
            row["SupplierId"] = sd.SupplierId.ToString();
            row["Status"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "SupplierStatus", sd.Status.ToString());
            row["StageInTrialRegistration"] = sd.StageInTrialRegistration.ToString();
            row["IsFromAar"] = dicYesNo[(sd.IsFromAar) ? eYesNo.Yes : eYesNo.No];
            row["TimeZone"] = sd.TimeZone;
            data.Rows.Add(row);
        }
        SData = data;
        int _sum = result.Value.Suppliers.Length;
        int _total_page = ((_sum - 1) / TablePaging1.ItemInPage) + 1;
        TotalRowsV = _sum;
        DataResult dr = new DataResult() { data = data, CurrentPage = 1, TotalRows = _sum, TotalPages = _total_page };
        return dr;
        
    }
    DataResult GetDataReport(int current_page)
    {
        DataResult dr = new DataResult();
        dr.data = SData;
        dr.TotalRows = TotalRowsV;
        dr.TotalPages = ((dr.data.Rows.Count - 1) / TablePaging1.ItemInPage) + 1;
        dr.CurrentPage = current_page;
        return dr;
    }
    DataTable GetBindData(DataResult dr)
    {        
        int _from = (dr.CurrentPage - 1) * TablePaging1.ItemInPage;
        int _to = ((dr.TotalRows - _from) > TablePaging1.ItemInPage) ? TablePaging1.ItemInPage : (dr.TotalRows - _from);
        IEnumerable<DataRow> query = dr.data.AsEnumerable().Skip(_from).Take(_to);

         DataTable SpecificData = query.CopyToDataTable();
        return SpecificData;
    }
    protected void lb_Name_click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        string _guid = lnk.CommandArgument;

        GridViewRow row = (GridViewRow)lnk.NamingContainer;
        string name = ((Label)row.FindControl("lblName")).Text;
        string _email = ((Label)row.FindControl("lblEmail")).Text;
        
        UserMangement um = new UserMangement(new Guid(_guid), name, lnk.Text, _email);
        SetGuidSetting(um);
        string _script = @"$(document).ready(function(){$.colorbox({href:'framepublisher.aspx',width:'95%', height:'90%', iframe:true});});";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "colorbox", _script, true);
    }
    /*
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        _uPanel.Update();
    }
     * */
    /*
    DataTable dataV
    {
        get
        {
            return (ViewState["data"] == null) ? new DataTable() :
                (DataTable)ViewState["data"];
        }

        set { ViewState["data"] = value; }
    }
     * */

    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
    WebReferenceReports.AdvertisersReportRequest AdvertisersReportRequestV
    {
        get { return (ViewState["AdvertisersReportRequest"] == null) ? null : (WebReferenceReports.AdvertisersReportRequest)ViewState["AdvertisersReportRequest"]; }
        set { ViewState["AdvertisersReportRequest"] = value; }
    }
    Dictionary<eYesNo, string> dicYesNo
    {
        get { return (ViewState["dicYesNo"] == null) ? null : (Dictionary<eYesNo, string>)ViewState["dicYesNo"]; }
        set { ViewState["dicYesNo"] = value; }
    }
    protected string StatusToShow
    {
        get
        {
            return WebReferenceReports.SupplierStatus.Candidate.ToString();
        }
    }
    protected DataTable SData
    {
        get { return (Session["AdvertiseReportDate"] == null) ? null : (DataTable)Session["AdvertiseReportDate"]; }
        set { Session["AdvertiseReportDate"] = value; }
    }
    Dictionary<string, string> dic_translate_RegistrationV
    {
        get { return (Session["dic_translate_Registration"] == null) ? null : (Dictionary<string, string>)Session["dic_translate_Registration"]; }
        set { Session["dic_translate_Registration"] = value; }
    }
}
