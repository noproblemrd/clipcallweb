﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ProPerformanceReport.aspx.cs" Inherits="Publisher_ProPerformanceReport" %>

<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
     <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
    <script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>
     <script type="text/javascript">
        window.onload = appl_init;

        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);

        }
        function BeginHandler() {
            //     alert('in1');
            showDiv();
        }
        function EndHandler() {
            //      alert('out1');       
            hideDiv();
        }
        function _CreateExcel() {
            var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"CustomerReport"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
        function chkTextBox(event) {
            //     alert(event.keyCode);
            if (event.keyCode != 13)
                return true;
            run_report();
            return false;

        }
        function run_report() {

            <%# RunReport() %>;

        }
       
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="Date-field" runat="server" id="div_Date">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  		
           
            <div style="clear:both;"></div>			 
			<div class="form-field" style="display:none;">
                <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Free Text</label>
			    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-text" MaxLength="40" onkeydown="return chkTextBox(event);"></asp:TextBox>								
                
            </div>          
		</div>
      
		<div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>   
       
        <asp:UpdatePanel id="_UpdatePanel" runat="server"  UpdateMode="Conditional" ChildrenAsTriggers="true">                           
        <ContentTemplate>   
        
         <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>     
           
        <div id="Table_Report"  runat="server" >  
				<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false" AllowSorting="true" 
                    OnSorting="_GridView_Sorting" EnableSortingAndPagingCallbacks="true" >
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="ProName" >
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_ProName" runat="server" Text="Pro name" CommandName="Sort" CommandArgument="ProName" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lbl2" runat="server" Text='<%# Bind("ProName") %>'></asp:Label>
                            <asp:HiddenField ID="hf_AccountId" runat="server" Value='<%# Bind("AccountId") %>' />
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="LastActive">
						<HeaderTemplate>
                             <asp:LinkButton ID="lnk_LastActive" runat="server" Text="Last active" CommandName="Sort" CommandArgument="LastActive" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lbl802" runat="server" Text='<%# Bind("LastActive") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="AmountOfLeads">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_AmountOfLeads" runat="server" Text="Amount of leads" CommandName="Sort" CommandArgument="AmountOfLeads" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label112" runat="server" Text='<%# Bind("AmountOfLeads") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="LeadsRespondedRate">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_LeadsResponsedRate" runat="server" Text="Lead response rate" CommandName="Sort" CommandArgument="LeadsRespondedRate" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label1201" runat="server" Text='<%# Bind("LeadsResponsedRate") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>	

					<asp:TemplateField SortExpression="LeadsResponded">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_LeadsResponsed" runat="server" Text="Response leads" CommandName="Sort" CommandArgument="LeadsResponded" /> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label11" runat="server" Text='<%# Bind("LeadsResponsed") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>					

                    <asp:TemplateField SortExpression="LeadsBooked">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_LeadsBooked" runat="server" Text="Leads booked" CommandName="Sort" CommandArgument="LeadsBooked" /> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label203" runat="server" Text='<%# Bind("LeadsBooked") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="BookingRate">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_BookingRate" runat="server" Text="Booking rate" CommandName="Sort" CommandArgument="BookingRate" /> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label101" runat="server" Text='<%# Bind("BookingRate") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="TotalRevenues">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_TotalRevenues" runat="server" Text="Total revenue" CommandName="Sort" CommandArgument="TotalRevenues" /> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label15" runat="server" Text='<%# Bind("TotalRevenues") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="RevenuePerBookedLead">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_RevenuePerBookedLead" runat="server" Text="Revenue per booked lead" CommandName="Sort" CommandArgument="RevenuePerBookedLead" /> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label815" runat="server" Text='<%# Bind("RevenuePerBookedLead") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="RevenuePerRespondedLead">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_RevenuePerRespondedLead" runat="server" Text="Revenue per responded lead" CommandName="Sort" CommandArgument="RevenuePerRespondedLead" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label225" runat="server" Text='<%# Bind("RevenuePerRespondedLead") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="AvgRank">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_AvgRank" runat="server" Text="Avg Rank" CommandName="Sort" CommandArgument="ProRating" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label327" runat="server" Text='<%# Bind("AvgRank") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="Reviews">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_Reviews" runat="server" Text="Number of Reviews" CommandName="Sort" CommandArgument="Reviews" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label_Reviews" runat="server" Text='<%# Bind("Reviews") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="NPS">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_NPS" runat="server" Text="NPS" CommandName="Sort" CommandArgument="NPS" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label328" runat="server" Text='<%# Bind("NPS") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="UnbookLeads">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_UnbookLeads" runat="server" Text="Unbook leads" CommandName="Sort" CommandArgument="UnbookLeads" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label325" runat="server" Text='<%# Bind("UnbookLeads") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>


                    <asp:TemplateField SortExpression="Lock_Unlock">
						<HeaderTemplate>
							<asp:Label ID="Label168" runat="server" Text="Lock/Unlock account"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:ImageButton ID="ib_switch" runat="server" ImageUrl='<%# Bind("Switch") %>' CommandArgument='<%# Bind("SwitchArgument") %>' 
                                OnClick="ib_switch_Click" OnClientClick='<%# Bind("SwitchScript") %>' />
                            <asp:Label ID="lbl_switch" runat="server" Text='<%# Bind("Status") %>' CssClass="error-msg2" style="display:block; color:red;"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="TotalQualityScore">
						<HeaderTemplate>
                            <asp:LinkButton ID="lnk_TotalQualityScore" runat="server" Text="Total Quality Score" CommandName="Sort" CommandArgument="TotalQualityScore" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label_TotalQualityScore" runat="server" Text='<%# Bind("TotalQualityScore") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="InactiveReason">
						<HeaderTemplate>
							<asp:Label ID="Label186" runat="server" Text="Inactive reason"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lbl_inactiveReason" runat="server" Text='<%# Bind("InactiveReason") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    	
				</Columns>    
				</asp:GridView>
			</div>
         
        </ContentTemplate>
          
            </asp:UpdatePanel>
             
        </div>
    <div style="display:none;">
    <asp:Button ID="btn_virtual_run" runat="server" Text="Button" style="display:none;" onfocus="this.blur();" />
        
</div>
    </div>

</asp:Content>

