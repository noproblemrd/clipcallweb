﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_FoulsManagment : PageSetting
{
    protected readonly string SessionTableName = "data_fould";
    protected readonly string EXCEL_NAME = "AccountFoulsManagmentExcel";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetToolBox();
            Guid supplierId;
            if (!Guid.TryParse(Request.QueryString["accountid"], out supplierId))
                supplierId = Guid.Empty;
            if (supplierId == Guid.Empty)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseMeError", "CloseMeError();", true);
                return;
            }
            LoadFoulTypes();
            LoadData(supplierId);
        }
        SetToolBoxEvents();
        Header.DataBind();
    }
    void LoadFoulTypes()
    {
        ddl_FoulType.Items.Clear();
        foreach (ClipCallReport.eFoulType foulType in Enum.GetValues(typeof(ClipCallReport.eFoulType)))
        {
            ListItem item = new ListItem(foulType.ToString(), ((int)foulType).ToString());
            ddl_FoulType.Items.Add(item);
        }
    }
    private void LoadData(Guid supplierId)
    {
        ClipCallReport.ClipCallReport clipCall = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.ResultOfAccountFoulResponse result;
        try
        {
            result = clipCall.GetSupplierFouls(supplierId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        lbl_supplierName.Text = result.Value.SupplierName;
        lbl_supplierPhoneNumber.Text = result.Value.Phone;

        ddl_projects.Items.Clear();
        foreach(ClipCallReport.ProjectIncidentAccountPair project in result.Value.ConnectedProjects)
        {
            ListItem li = new ListItem(project.ProjectId, project.IncidentAccountId.ToString());
            ddl_projects.Items.Add(li);
        }


        DataTable table = new DataTable();
        table.Columns.Add("Date");
        table.Columns.Add("ProjectId");
        table.Columns.Add("CustomerName");
        table.Columns.Add("Category");
        table.Columns.Add("AccountFoulId");
        table.Columns.Add("FoulType");
        table.Columns.Add("From");
        table.Columns.Add("Description");
        table.Columns.Add("InternalNote");
        foreach(ClipCallReport.AccountFoulDataResponse data in result.Value.Fouls)
        {
            DataRow row = table.NewRow();
            row["Date"] = string.Format(siteSetting.DateFormat, data.ProjectCreatedOn);
            row["ProjectId"] = data.ProjectId;
            row["CustomerName"] = data.CustomerName;
            row["Category"] = data.Category;
            row["AccountFoulId"] = data.AccountFoulId;
            row["FoulType"] = Utilities.GetEnumStringFormat(data.FoulType);
            row["From"] = Utilities.GetEnumStringFormat(data.Related);
            row["Description"] = data.Description;
            row["InternalNote"] = data.InternalNote;
            table.Rows.Add(row);
        }
        dataS = table;
        _GridView.DataSource = table;
        _GridView.DataBind();
    }

    private void SetToolBox()
    {
        Toolbox1.RemoveAttach();
        Toolbox1.RemovePrint();
        Toolbox1.RemoveDelete();
        Toolbox1.RemoveSearch();
        string script = "openFoulPopupAdd(); return false;";
        Toolbox1.SetClientScriptToAdd(script);
        string _script = "_CreateExcel();";
        Toolbox1.SetExcelJavascript(_script);
        string scriptUpdate = "openFoulPopupUpdate(); return false;";
        Toolbox1.SetClientScriptToEdit(scriptUpdate);
    }

    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
        Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
    //    Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
    }
    protected DataTable dataS
    {
        get { return Session[SessionTableName] == null ? new DataTable() : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    private void Toolbox1_EditExec(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

   

    private void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        
    }

    private void Toolbox1_AddExec(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    private void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }
    /*
    protected string AddFoulService
    {
        get { return ResolveUrl("~/Publisher/FoulsManagment.aspx/AddFoul"); }
    }
    */
    protected string SetFoulService
    {
        get { return ResolveUrl("~/Publisher/FoulsManagment.aspx/SetFoul"); }
    }
 //   [WebMethod(MessageName = "AddFoul")]
    public static string AddFoul(Guid incidentAccountId, int type, string description, string internalNote)
    {
       
        ClipCallReport.ClipCallReport clipCall = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.AccountFoulRequest request = new ClipCallReport.AccountFoulRequest()
        {
            IncidentAccountId = incidentAccountId,
            Description = description,
            FoulType = (ClipCallReport.eFoulType)type,
            InternalNote = internalNote
        };
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = clipCall.AddFoul(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { IsSucceed = false });
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { IsSucceed = false });
        }
        return Newtonsoft.Json.JsonConvert.SerializeObject(new { IsSucceed = result.Value });
    }
    [WebMethod(MessageName = "SetFoul")]
    public static string SetFoul(Guid? accountFoulId, Guid incidentAccountId, int type, string description, string internalNote)
    {
        if (!accountFoulId.HasValue)
            return AddFoul(incidentAccountId, type, description, internalNote);
        ClipCallReport.ClipCallReport clipCall = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.AccountFoulRequest request = new ClipCallReport.AccountFoulRequest()
        {
            IncidentAccountId = incidentAccountId,
            AccountFoulId = accountFoulId.Value,
            Description = description,
            FoulType = (ClipCallReport.eFoulType)type,
            InternalNote = internalNote
        };
        ClipCallReport.ResultOfBoolean result = null;
        try
        {
            result = clipCall.UpdateFoul(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { IsSucceed = false });
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { IsSucceed = false });
        }
        return Newtonsoft.Json.JsonConvert.SerializeObject(new { IsSucceed = result.Value });
    }
}