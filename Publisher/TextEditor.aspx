<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="TextEditor.aspx.cs" Inherits="Publisher_TextEditor" Title="Untitled Page" ValidateRequest="false" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="style.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<div class="page-content minisite-content">
    <h2><asp:Label ID="lblTitleTextEditor" runat="server" Text="Text editor"></asp:Label></h2>
    <div class="form-fieldset">
        <asp:Label ID="lbl_PageLocation" CssClass="label" runat="server" Text="Location"></asp:Label>:
        <asp:DropDownList ID="ddl_Page" CssClass="form-select" runat="server" AutoPostBack="True" OnSelectedIndexChanged="_ddl_Page_SelectedIndexChanged">
            <asp:ListItem Value="Login" Selected="True">Login</asp:ListItem>
            <asp:ListItem Value="PaymentWireTransfer">Payment wire transfer</asp:ListItem>
            <asp:ListItem Value="MyCredit">Pricing per call</asp:ListItem>
            <asp:ListItem Value="PaymentCreditCard">Payment credit card</asp:ListItem>
        </asp:DropDownList>
    </div>
    
	<FTB:FreeTextBox id="FreeTextBox1" runat="Server" ButtonHeight="20" BackColor="LightYellow" GutterBackColor="Lavender" ToolbarBackColor="White" Width="150px" AllowHtmlMode="false" EnableHtmlMode="false" />
    <asp:Button id="SaveButton" Text="Save" onclick="SaveButton_Click" runat="server" CssClass="form-submit" />
    <div class="form-fieldset">
	    <asp:dropdownlist CssClass="form-select" ID="ToolbarStyle" Runat="Server" />
	    <asp:dropdownlist CssClass="form-select" ID="Language" Runat="Server" />
	    <asp:Button ID="ConfigureButton" Text="Configure" CssClass="form-submit44" OnClick="ConfigureButton_Click" runat="Server" />
	</div>
    
    
</div>

</asp:Content>

