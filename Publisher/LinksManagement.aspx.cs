using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

public partial class Publisher_LinksManagement : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {       
            LoadLinks();
        }
    }
   
    private void LoadLinks()
    {
        List<int> list = new List<int>();
        DataTable data = new DataTable();
        data.Columns.Add("PageName", typeof(string));
        data.Columns.Add("Translate", typeof(string));
        data.Columns.Add("LinkId", typeof(int));
        data.Columns.Add("url", typeof(string));
        data.Columns.Add("PageId", typeof(int));

        DataTable data_out = new DataTable();
        data_out.Columns.Add("PageId", typeof(int));
        data_out.Columns.Add("PageName", typeof(string));
       
        string command = "EXEC dbo.GetLinksBySiteNameId @SiteNameId, @SiteLangId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            cmd.Parameters.AddWithValue("@SiteLangId", siteSetting.siteLangId);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow row = data.NewRow();
                int PageId = (int)reader["PageId"];
                row["PageId"] = PageId;
                row["Translate"] = (string)reader["Translate"];
                row["LinkId"] = (int)reader["LinkId"];
                row["url"] = (reader.IsDBNull(2) ? string.Empty : (string)reader["url"]);
                data.Rows.Add(row);


                if (!list.Contains(PageId))
                {
                    DataRow _row = data_out.NewRow();
                    _row["PageName"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "PagesNames", (string)reader["PageName"]);
                    _row["PageId"] = PageId;
                    data_out.Rows.Add(_row);
                    list.Add(PageId);
                }
            }
            conn.Close();
        }
        _RepeaterParent.DataSource = data_out;
        _RepeaterParent.DataBind();
        
        foreach(RepeaterItem _item in _RepeaterParent.Items )
        {
            int _pageId = int.Parse(((Label)_item.FindControl("lbl_PageId")).Text);
            var _links = data.AsEnumerable()
                .Where(x => x.Field<int>("PageId") == _pageId)
                .Select(x => new 
                {
                    Translate = x.Field<string>("Translate"),
         //           MasterName = x.Field<string>("MasterName"),
                    LinkId = x.Field<int>("LinkId"),
                    url = x.Field<string>("url")
                });
            Repeater _repeater = (Repeater)_item.FindControl("_RepeaterChild");
            _repeater.DataSource = _links;
            _repeater.DataBind();
        }        
    }
   
    protected void btn_set_Click(object sender, EventArgs e)
    {
        string command = "EXEC dbo.SetLinkBySiteNameId_LinkId @SiteNameId, @LinkId, @url";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            try
            {
                conn.Open();
                foreach (RepeaterItem _item in _RepeaterParent.Items)
                {
                    Repeater _repeater = (Repeater)_item.FindControl("_RepeaterChild");
                    foreach (RepeaterItem _item_child in _repeater.Items)
                    {
                        int LinkId = int.Parse(((Label)_item_child.FindControl("lbl_LinkId")).Text);
                        string _url = ((TextBox)_item_child.FindControl("txt_Link")).Text.Trim();
                        SqlCommand cmd = new SqlCommand(command, conn);
                        cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
                        cmd.Parameters.AddWithValue("@LinkId", LinkId);
                        if (string.IsNullOrEmpty(_url))
                            cmd.Parameters.AddWithValue("@url", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@url", _url);
                        int a = cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Failed", "top.UpdateFailed();", true);
                //      ClientScript.RegisterStartupScript(this.GetType(), "Failed", "alert('"+ex.Message+"');", true);
                return;
            }
        }
        ClientScript.RegisterStartupScript(this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
    }
}
