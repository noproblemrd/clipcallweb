﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProximitySensorTicket.aspx.cs" Inherits="Publisher_ProximitySensorTicket" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link type="text/css" rel="stylesheet" href="Style.css" /> 
    <title></title>
    <script type="text/javascript">
        function CloseMeError() {
            alert("<%# lbl_ErrorLoadData.Text %>");
            //parent.CloseIframe()
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <div id="form-analyticscomp2" style="margin-top:40px; margin-left: 30px;">
        <div class="titlerequestTop" style="margin-top: 20px;">          
            <asp:Label ID="lblQuoteNumber" runat="server" Text="Proximity Sensor"></asp:Label>:
        </div>
        <div class="clear"></div>
        <div class="form-fieldshort">          
            <asp:Label ID="Label1" runat="server" Text="Date:" CssClass="label"></asp:Label>
            <asp:Label ID="lbl_Date" runat="server"></asp:Label>
        </div>
        <div class="clear"></div>
        <div class="form-fieldshort">          
            <asp:Label ID="Label2" runat="server" Text="Arrival Date:" CssClass="label"></asp:Label>
            <asp:Label ID="lbl_ArrivalDate" runat="server"></asp:Label>
        </div>
        <div class="clear"></div>

        <div class="form-fieldshort">          
            <asp:Label ID="Label3" runat="server" Text="Checkout Date:" CssClass="label"></asp:Label>
            <asp:Label ID="lbl_CheckOutDate" runat="server"></asp:Label>
        </div>
        <div class="clear"></div>

         <div class="form-fieldshort">          
            <asp:Label ID="Label4" runat="server" Text="Latitude:" CssClass="label"></asp:Label>
            <asp:Label ID="lbl_Latitude" runat="server"></asp:Label>
        </div>
        <div class="clear"></div>

        <div class="form-fieldshort">          
            <asp:Label ID="Label5" runat="server" Text="Longitude:" CssClass="label"></asp:Label>
            <asp:Label ID="lbl_Longitude" runat="server"></asp:Label>
        </div>

        <div class="clear"></div>
        <div class="form-fieldshort">          
            <asp:Label ID="Label6" runat="server" Text="Distance:" CssClass="label"></asp:Label>
            <asp:Label ID="lbl_Distance" runat="server"></asp:Label>
        </div>
        
    </div>
           <asp:Label ID="lbl_ErrorLoadData" runat="server" Text="There was an error to load the details." Visible="false"></asp:Label>
    </form>
</body>
</html>
