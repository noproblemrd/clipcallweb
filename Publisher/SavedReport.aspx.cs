﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class Publisher_SavedReport : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetToolBox();
            LoadReports();
        }
        SetToolBoxEvents();
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    
    private void SetToolBox()
    {
        Toolbox1.RemoveAttach();    
        Toolbox1.RemovePrint();
        Toolbox1.RemoveExcel();
        Toolbox1.RemoveSave();
    }
    private void SetToolBoxEvents()
    {
        Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
    }
    
    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        bool IsSelect = false;
        int indx = -1;
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
            {
                if (IsSelect)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ChooseOne", "alert('" + Toolbox1.ChooseOne + "');", true);
                    return;
                }
                IsSelect = true;
                indx = row.RowIndex;
            }
        }
        if (indx > -1)
        {
            GridViewRow row = _GridView.Rows[indx];
            int reportId = int.Parse(((Label)row.FindControl("lbl_reportId")).Text);
            Response.Redirect("ReportWizard.aspx?ReportId=" + reportId);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ChooseOne", "alert('" + Toolbox1.ChooseOne + "');", true);
    }

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        string command = "EXEC dbo.DeleteReportWizardByReportId @ReportId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();

            foreach (GridViewRow row in _GridView.Rows)
            {
                CheckBox cb = (CheckBox)row.FindControl("cb_choose");
                if (cb.Checked)
                {
                    SqlCommand cmd = new SqlCommand(command, conn);
                    int _id = int.Parse(((Label)row.FindControl("lbl_reportId")).Text);
                    cmd.Parameters.AddWithValue("@ReportId", _id);
                    int a = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
            conn.Close();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "UpdateSuccess();", true);
        LoadReports();
    }

    void Toolbox1_AddExec(object sender, EventArgs e)
    {
        Response.Redirect("ReportWizard.aspx");
    }

    private void LoadReports()
    {
        Dictionary<Guid, string> dicUsers = new Dictionary<Guid, string>();
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("Description");
        data.Columns.Add("ReportId");
        data.Columns.Add("IssueName");
        data.Columns.Add("CreatedOn");
        data.Columns.Add("UserId");
        data.Columns.Add("ToReport");

        string command = "EXEC dbo.GetWizardReports @SiteNameId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow row = data.NewRow();
                row["Name"] = (string)reader["Name"];
                row["Description"] = (string)reader["Description"];
                row["IssueName"] = (string)reader["IssueName"];
                row["CreatedOn"] = String.Format("{0:MM/dd/yyyy}", (DateTime)reader["DateCreate"]);
                int _reportId = (int)reader["Id"];
                row["ReportId"] = _reportId;
                Guid _id = (Guid)reader["UserId"];
                row["UserId"] = _id;
                row["ToReport"] = "ReportWizardResult.aspx?ReportId=" + _reportId;
                data.Rows.Add(row);
                if (!dicUsers.ContainsKey(_id))
                    dicUsers.Add(_id, string.Empty);
            }
            conn.Close();
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.GetUserNamesRequest _request = new WebReferenceSite.GetUserNamesRequest();
        _request.Guids = dicUsers.Keys.ToArray();
        WebReferenceSite.ResultOfListOfGuidUserNamePair result;
        try
        {
            result = _site.GetUserNames(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ClientScript.RegisterStartupScript(this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        _GridView.PageIndex = 0;
        foreach (WebReferenceSite.GuidUserNamePair id_name in result.Value)
        {
            dicUsers[id_name.UserId] = id_name.UserName;
        }
        UserId_UserNameV = dicUsers;
        dataV = data;
        lbl_RecordMached.Text = Toolbox1.GetRecordMaches(data.Rows.Count);
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            _GridView.DataSource = null;
            _GridView.DataBind();
            _UpdatePanel.Update();
            return;
        }
        
        SetGridView();
    }
    void SetGridView()
    {
       
        Dictionary<Guid, string> dicUsers = UserId_UserNameV;
        StringBuilder sb = new StringBuilder();
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";
        foreach (GridViewRow _row in _GridView.Rows)
        {
            Label userName = (Label)_row.FindControl("lblUser");
            Guid _id = new Guid(((Label)_row.FindControl("lblUserId")).Text);
            userName.Text = dicUsers[_id];

            CheckBox cb = (CheckBox)_row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }
        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs; 
        
        _UpdatePanel.Update();
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView();
    }
    
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
    Dictionary<Guid, string> UserId_UserNameV
    {
        get { return (ViewState["UserId_UserName"] == null) ? new Dictionary<Guid, string>() : 
            (Dictionary<Guid, string>)ViewState["UserId_UserName"]; }
        set { ViewState["UserId_UserName"] = value; }
    }
    
}
