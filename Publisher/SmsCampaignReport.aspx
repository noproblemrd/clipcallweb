﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SmsCampaignReport.aspx.cs" Inherits="Publisher_SmsCampaignReport" %>


<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
       <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function _CreateExcel() {
            var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"AarCallCenterReport"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="Date-field" runat="server" id="div_Date">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  			
		</div>
      
		<div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>        
        <div id="Table_Report"  runat="server" >  
				<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="date">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="Date"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblBroker" runat="server" Text="<%# Bind('date') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="bulkId">
						<HeaderTemplate>
							<asp:Label ID="Label101" runat="server" Text="BulkId"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label112" runat="server" Text="<%# Bind('bulkId') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="campaign">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="Campaign"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label11" runat="server" Text="<%# Bind('campaign') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
                    
                    <asp:TemplateField SortExpression="TotalPhonesAttempts">
						<HeaderTemplate>
							<asp:Label ID="Label14" runat="server" Text="Total phones attempts"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label15" runat="server" Text="<%# Bind('TotalPhonesAttempts') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="sent">
						<HeaderTemplate>
							<asp:Label ID="Label16" runat="server" Text="Sent"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label17" runat="server" Text="<%# Bind('sent') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="didntTryToSent">
						<HeaderTemplate>
							<asp:Label ID="Label18" runat="server" Text="Didnt try to sent"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label19" runat="server" Text="<%# Bind('didntTryToSent') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

				</Columns>    
				</asp:GridView>
			</div>
        </div>
    </div>
    <asp:Label ID="lbl_SmsCampaignReport" runat="server" Text="SMS Campaign Report" Visible="false"></asp:Label>
</asp:Content>
