<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="BadWords.aspx.cs" Inherits="Publisher_BadWords" Title="Untitled Page"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="style.css" rel="stylesheet" type="text/css"/>
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../general.js"></script>
<script type="text/javascript" >
   
    
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        
  //      pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {
        showDiv();
    }
    function Show_Div()
    {
       // if (Page_ClientValidate('BadWords'))
            showDiv();
    }
   
   function pageLoad(sender, args)
    {        
      //  $get("= lbl_missing.ClientID %>").style.display="none";
        hideDiv();
        appl_init();
    }
   
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager runat="server" ID="ScriptManager1" ></cc1:ToolkitScriptManager>
<uc1:Toolbox ID="Toolbox1" runat="server" />

    <h5><asp:Label ID="lblSubTitle" runat="server">Imporper Language Filter</asp:Label></h5>

<div class="page-content minisite-content5">
	
    <div id="form-analyticsseg">
	    <asp:UpdatePanel ID="UpdatePanelSentences" runat="server" UpdateMode="Conditional" >
            <ContentTemplate>
                <asp:GridView ID="_GridViewBadWords" runat="server" AutoGenerateColumns="false" CssClass="data-table data-table-bad-words" AllowPaging="True" onpageindexchanging="_GridViewBadWords_PageIndexChanging" PageSize="20">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                                <br />
                                <asp:CheckBox ID="cb_all" CssClass="form-checkbox" runat="server" ></asp:CheckBox>
                            </HeaderTemplate>
                            <ItemTemplate>        
                                <asp:CheckBox ID="cb_choose" CssClass="form-checkbox" runat="server" />
                                <asp:Label ID="lbl_BadWordId" runat="server" Text="<%# Bind('BadWordId') %>" Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="<%# lbl_BadWord.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBadWord" runat="server" Text="<%# Bind('Word') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="__even" />
                    <AlternatingRowStyle CssClass="__odd" />
                    <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<div id="divLoader" class="divLoader">   
    <table>
        <tr>
            <td>
                <img src="../UpdateProgress/ajax-loader.gif" alt=""/>
            </td>
        </tr>
    </table>
</div>   

<asp:Label ID="lbl_BadWord" runat="server" Text="Improper Language" Visible="false"></asp:Label>
<asp:Label ID="lbl_Delete" runat="server" Text="Delete" Visible="false"></asp:Label>
<asp:HiddenField ID="hf_confirmDelete" runat="server" Value="Do you want to delete" />
<asp:Label ID="lbl_WordAlreadyExists" runat="server" Text="The word already exists" Visible="false"></asp:Label>
<asp:Label ID="lbl_WordIsNullOrEmpty" runat="server" Text="The field is empty" Visible="false"></asp:Label>
<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
</asp:Content>

