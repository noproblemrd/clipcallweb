﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Data;

public partial class Publisher_ScheduleMessagesPosting : PageSetting
{
   
    DataTable data;
    protected void Page_Load(object sender, EventArgs e)
    {
        ToolkitScriptManager1.RegisterAsyncPostBackControl(btn_set);
        if (!IsPostBack)
        {

            LoadData();
        }
    }

    private void LoadData()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfMessagingTimeData result = null;
        try
        {
            result = _site.GetMessagingTimes();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        data = new DataTable();
        data.Columns.Add("Day");
        data.Columns.Add("DayId", typeof(string));
        data.Columns.Add("IsSuspend", typeof(bool));
        data.Columns.Add("FromHour", typeof(string));
        data.Columns.Add("ToHour", typeof(string));
        for (int i = 1; i <= (int)DayOfWeek.Saturday; i++)
        {
            DataRow row = data.NewRow();
            row["Day"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "DayOfWeek", ((DayOfWeek)i).ToString());
            row["DayId"] = ((DayOfWeek)i).ToString();
            row["IsSuspend"] = true;
            data.Rows.Add(row);
        }
        foreach (WebReferenceSite.MessagingTimeData mtd in result.Value)
        {
            DataRow row = GetRow(mtd.DayOfWeek.ToString(), data);
            if (row == null)
                continue;
            row["IsSuspend"] = false;
            row["FromHour"] = mtd.FromHour;
            row["ToHour"] = mtd.ToHour;
        }
        _Repeater.DataSource = data;
        _Repeater.DataBind();
        _UpdatePanel.Update();
        
    }
    DataRow GetRow(string _day, DataTable _data)
    {
        foreach (DataRow row in data.Rows)
        {
            if ((string)row["DayId"] == _day)
                return row;
        }
        return null;
    }
    protected void _Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow tr = (HtmlTableRow)item.FindControl("tr_body");
        tr.Attributes.Add("class", ((item.ItemIndex % 2 == 0) ? "odd" : "even"));

        CheckBox cb = (CheckBox)item.FindControl("cb_suspend");
        bool Issuspend = (bool)data.Rows[item.ItemIndex]["IsSuspend"];
        Panel _panel = (Panel)item.FindControl("div_times");

        DropDownList ddl_start = (DropDownList)item.FindControl("ddl_From");
        DropDownList ddl_end = (DropDownList)item.FindControl("ddl_To");
        if (!Issuspend)
        {
            _panel.Attributes.Add("style", "visibility:visible");
            string[] _from = (((Label)item.FindControl("lblStart")).Text).Split(':');
            string[] _to = (((Label)item.FindControl("lblEnd")).Text).Split(':');


            SetDDL(ddl_start, _from);
            SetDDL(ddl_end, _to);
        }
        else
            _panel.Attributes.Add("style", "visibility:hidden");
        cb.Attributes.Add("onclick", "javascript:cb_click(this, '" + _panel.ClientID + "');");

        ddl_start.Attributes.Add("onChange", "checkTimes('" + ddl_start.ClientID +
            "', '" + ddl_end.ClientID + "');");
        ddl_end.Attributes.Add("onChange", "checkTimes('" + ddl_start.ClientID +
            "', '" + ddl_end.ClientID + "');");
    }
   
    void SetDDL(DropDownList ddl, string[] dt)
    {
        string hour = dt[0];
        string minute = dt[1];
        hour = (hour.Length == 1) ? "0" + hour : hour;
        minute = (minute.Length == 1) ? "0" + minute : minute;
        string val = hour + ":" + minute;
        foreach (ListItem li in ddl.Items)
        {
            if (li.Value == val)
                li.Selected = true;
            else
                li.Selected = false;
        }
    }

    protected void btn_set_click(object sender, EventArgs e)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);//UpdateMessagingTimes

        List<WebReferenceSite.MessagingTimeData> list = new List<WebReferenceSite.MessagingTimeData>();

        foreach (RepeaterItem item in _Repeater.Items)
        {
            bool IsSuspend = ((CheckBox)item.FindControl("cb_suspend")).Checked;

            if (IsSuspend)
                continue;
            WebReferenceSite.MessagingTimeData mtd = new WebReferenceSite.MessagingTimeData();
   

            string _day = ((Label)item.FindControl("lblDayId")).Text;
            WebReferenceSite.DayOfWeek dow;
            if(!Enum.TryParse(_day, out dow))
                continue;
            mtd.DayOfWeek = dow;
            mtd.FromHour = ((DropDownList)item.FindControl("ddl_From")).SelectedValue;
            mtd.ToHour = ((DropDownList)item.FindControl("ddl_To")).SelectedValue;
            list.Add(mtd);
        }
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.UpdateMessagingTimes(list.ToArray());
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        LoadData();
    }
    
}