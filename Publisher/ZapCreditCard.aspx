﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ZapCreditCard.aspx.cs" Inherits="Publisher_ZapCreditCard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link  rel="stylesheet" type="text/css" href="../Management/style-full-rtl.css" />
    <title></title>     
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../scripts/json.js"></script>
    <script type="text/javascript">
        function GoBack(){
            history.go(-1);
        }
        function IDValidator(id) {
            id += ""; //cast as string
            if (id.length == 8)
                id = '0' + id;
            if (id.length != 9 || isNaN(id)) {
                return false;
            }
            var counter = 0;
            for (var i = 0; i < id.length; i++) {
                var incNum = id.charAt(i) * ((i % 2) + 1); //multiply digit by 1 or 2
                counter += (incNum > 9) ? incNum - 9 : incNum; //sum the digits up and add to counter
            }
            return (counter % 10 == 0);
        }
        function ExpierValidator() {
            var year = <%# GetYear %>;
            var month = <%# GetMonth %>;
            var ddl_year = $('#<%# ddl_year.ClientID %>').val();
            var ddl_month = $('#<%# ddl_month.ClientID %>').val();
            if(ddl_year.length == 0 || ddl_month.length == 0)
                return false;
            if(parseInt(ddl_year) != year)
                return true;
            return (parseInt(ddl_month) >= month);
        }
        function ClearValidation(){
            $('#<%# lbl_DateInvalid.ClientID %>').hide();
            $('#<%# lbl_CardHolderNameMissing.ClientID %>').hide();
            $('#<%# lbl_IdError.ClientID %>').hide();
        }
        $(function () {
            top.hideDiv();
            $('#<%# txt_IdNumber.ClientID %>').blur(function () {
                 $('#<%# lbl_IdError.ClientID %>').hide();
                var id = $(this).val();
                if (id.length == 0)
                    return;
                if (!IDValidator(id)) {
                    $('#<%# lbl_IdError.ClientID %>').show();
                    return;
                }
            });
            $('#<%# txt_CardHolderName.ClientID %>').focus(function (){
                $('#<%# lbl_CardHolderNameMissing.ClientID %>').hide();
            });
            $('#a_CCDetails').click(function(){
                //ExecZapPayment("testtoken");
                ClearValidation();
                var IsValid = true;
                if(!ExpierValidator()){
                    $('#<%# lbl_DateInvalid.ClientID %>').show();
                    IsValid = false;
                }                    
                var name = $('#<%# txt_CardHolderName.ClientID %>').val();
                name = GetStringForJson(name);
                if(($.trim(name)).length == 0){
                    $('#<%# lbl_CardHolderNameMissing.ClientID %>').show();
                    IsValid = false;
                }
                var id = $('#<%# txt_IdNumber.ClientID %>').val();
                if (!IDValidator(id)) {
                    $('#<%# lbl_IdError.ClientID %>').show();
                    IsValid = false;
                }
                var expier = $('#<%# ddl_month.ClientID %>').val() + '-' + $('#<%# ddl_year.ClientID %>').val();
                if(!IsValid)
                    return;
                top.showDiv();
               
                 $.ajax({
                    url: '<%# GetCreditGuardUrl %>',
                    data: '{ "PaymentId": <%# GetPaymentId %>, "cc_name": "' + name + '", "cc_expier": "' + 
                                expier + '", "cc_id": "' + id + '"}',
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        if (data.d.length == 0) {
                            top.UpdateFailed();
                            return;
                        }
                        var result = eval('(' + data.d + ')');
                        if(result.Status == "success") {
                             LockText();
                            ToCreditGuard(result.Url);
                        }
                        else if(result.Msg.length > 0)
                            alert(result.Msg);
                        else
                            top.UpdateFailed();
                        
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                       top.UpdateFailed();
                    },
                    complete: function (jqXHR, textStatus) {
                        top.hideDiv();
                    }

                });
            });
            $('select').change(function(){
                $('#<%# lbl_DateInvalid.ClientID %>').hide();
            });
        });
        var _interval;
        function ToCreditGuard(url){
            var _opener = window.open(url, "_opener", 'menubar=0,resizable=0,scrollbars0,toolbar=0,width=700,height=400');
            clearInterval(_interval);
            _interval = setInterval(function () {
                if (_opener.closed) {                    
                    clearInterval(_interval);
                    window.focus();
                                      
                    GetToken();
                }
            }, 1000);
            

        }
        function LockText(){
            $('#<%# txt_IdNumber.ClientID %>').attr('readonly', 'readonly');
            $('#<%# txt_IdNumber.ClientID %>').addClass('read-only');
            $('#<%# txt_CardHolderName.ClientID %>').attr('readonly', 'readonly');
            $('#<%# txt_CardHolderName.ClientID %>').addClass('read-only');
            document.getElementById("<%# ddl_year.ClientID %>").disabled = true;
            document.getElementById("<%# ddl_month.ClientID %>").disabled = true; 
        }
        function UnLockText(){
            $('#<%# txt_IdNumber.ClientID %>').removeAttr('readonly', 'readonly');
            $('#<%# txt_IdNumber.ClientID %>').removeClass('read-only');
            $('#<%# txt_CardHolderName.ClientID %>').removeAttr('readonly', 'readonly');
            $('#<%# txt_CardHolderName.ClientID %>').removeClass('read-only');
            document.getElementById("<%# ddl_year.ClientID %>").disabled = false;
            document.getElementById("<%# ddl_month.ClientID %>").disabled = false; 
        }
        function GetToken(){
            top.showDiv();
            $.ajax({
                url: '<%# GetCreditGuardToken %>',
                data: '{ "PaymentId": <%# GetPaymentId %>}',
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length > 0) {
                        var ta = eval('(' + data.d + ')');
                        if(ta.Amount == 0)
                            ExecZapPayment(ta.Token);
                        else if (ta.Amount == -1){
                           top.hideDiv();
                           UnLockText();
                           alert(ta.Msg);
                           return; 
                        }
                        else{
                            top.hideDiv();
                            TurnOnPayment(ta.Token);                             
                        }                        
                        return;
                    }
                    top.hideDiv();
                    top.UpdateFailed();
                        
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    top.hideDiv();
                    top.UpdateFailed();
                    
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;
                    
                }

            });
           
        }
        function TurnOnPayment(token){
            $('#<%# txt_CreditCardDetails.ClientID %>').val(token);
            var $a_CCDetails = $('#a_CCDetails');
            $a_CCDetails.removeAttr('href');
            $a_CCDetails.unbind('click');
            $('#span_payment').hide();
            var $a_payment = $('#a_payment');
            $a_payment.show();
            $a_payment.click(function(){
                ExecZapPayment();
            });
        }
        function ExecZapPayment(token){
            if(token)
                $('#<%# txt_CreditCardDetails.ClientID %>').val(token);
            top.showDiv();
            $.ajax({
                url: '<%# ExecZapPayment %>',
                data: '{ "PaymentId": <%# GetPaymentId %>}',
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length > 0) {
                        var result = eval('(' + data.d + ')');
                        if(result._result == "success"){
                            if(result.Amount == 0)
                                alert('<%# GetReplaceTokenSuccess %>');
                            else
                                alert('<%# GetPaymentSuccess %>');

                        }
                        else if(result.Msg.length > 0){
                            alert(result.Msg);
                        }
                        else
                           top.UpdateFailed(); 
                    }
                    else
                        top.UpdateFailed();
                        
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    top.UpdateFailed();
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;
                    top.hideDiv();
                    if(<%# IsPublisher %>)
                        parent.closeOverLayIframe();
                    else
                        window.location = '<%# SupplierAfterPayment %>';

                }

            });
        }
         
    </script>
</head>
<body style="background-color:transparent;">
    <form id="form1" runat="server">
    <div class="NewDeposite">        
        <h2 class="title-deposite">
            <asp:Label ID="lbl_CustomerPayment" runat="server" Text="Customer Payment"></asp:Label> 
        </h2>
        <div class="div_table_deposite">
            <table class="CreditPaymentType">
            <tr>
            <td>             
                <asp:Label ID="lbl_ExpirationDate" runat="server" Text="Expiration Date"></asp:Label>
            </td>
            <td> 
            <span class="div-table-payment">
                <asp:Label ID="lbl_Year" runat="server" Text="Year"></asp:Label>          
                <asp:DropDownList ID="ddl_year" runat="server">
                </asp:DropDownList>
            </span>
            <span class="div-table-payment" style="float:left;">
                <asp:Label ID="lbl_Month" runat="server" Text="Month"></asp:Label> 
                <asp:DropDownList ID="ddl_month" runat="server">
                </asp:DropDownList>
            </span>
            </td>
            <td>
               <asp:Label ID="lbl_DateInvalid" runat="server" Text="Invalid date expire" CssClass="error-msg" style="display: none;"></asp:Label> 
            </td>
            </tr>

            <tr>
            <td>             
                <asp:Label ID="lbl_IdNumber" runat="server" Text="ID number"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_IdNumber" runat="server" CssClass="form-text"></asp:TextBox>                 
            </td>
            <td>
                <asp:Label ID="lbl_IdError" runat="server" Text="ID number not valid" CssClass="error-msg" style="display: none;"></asp:Label>
            </td>
            </tr>

            <tr>
            <td>             
                <asp:Label ID="lbl_CardHolderName" runat="server" Text="Card holder name"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_CardHolderName" runat="server" CssClass="form-text"></asp:TextBox>
                
            </td>
            <td>
                <asp:Label ID="lbl_CardHolderNameMissing" runat="server" Text="Missing" CssClass="error-msg" style="display: none;"></asp:Label>
            </td>
            </tr>

            <tr>
            <td>             
                <a href="javascript:void(0);" id="a_CCDetails" class="btn-SetCreditCard"><asp:Label ID="lbl_CreditCardDetails" runat="server" Text="Credit card details"></asp:Label></a>
            </td>
            <td>
                <asp:TextBox ID="txt_CreditCardDetails" runat="server" CssClass="form-text read-only form-token"></asp:TextBox>
            </td>
            </tr>
            </table>
        </div>
        <div class="div-btn">
           <a id="a_payment" class="btn-payment" href="javascript:void(0);" style="display:none;"><asp:Label ID="lbl_Payment" runat="server" Text="Payment"></asp:Label></a> 
           <span id="span_payment" class="span_payment"><asp:Label ID="lbl_SpanPayment" runat="server" Text="<%# lbl_Payment.Text %>"></asp:Label></span>
        </div>
    
    </div>
    <asp:Label ID="lbl_PaymentSuccess" runat="server" Text="Payment was successful" Visible="false"></asp:Label>
    <asp:Label ID="lbl_TokenReplaceSuccessfuly" runat="server" Text="Credit card token replace successfuly" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Choose" runat="server" Text="Choose" Visible="false"></asp:Label>
    </form>
</body>
</html>
