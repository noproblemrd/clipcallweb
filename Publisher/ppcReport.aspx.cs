﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Publisher_ppcReport : PageSetting
{
    const string REPORT_NAME = "PPCReport";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            SetToolbox();
            LoadGroupBy();
        }
        SetToolboxEvents();
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lblTitleppc.Text);
    }
    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, REPORT_NAME);
        if (!te.ExecExcel(dataV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + ToolboxReport1.NoRecordToPrint + "');", true);
            return;
        }
     //   PrintHelper.PrintWebControl(_GridView, dataV);
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);    
    }
    private void LoadGroupBy()
    {
        SortedDictionary<string, string> dic = new SortedDictionary<string, string>();
        foreach (WebReferenceReports.eGroupBy gb in Enum.GetValues(typeof(WebReferenceReports.eGroupBy)))
        {
            string _val = gb.ToString();
            string _txt = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eGroupBy", _val);
            dic.Add(_txt, _val);
        }
        foreach (KeyValuePair<string, string> kvp in dic)
        {
            ListItem li = new ListItem(kvp.Key, kvp.Value);
            li.Selected = (kvp.Value == WebReferenceReports.eGroupBy.None.ToString());
            ddl_GroupBy.Items.Add(li);
        }
    }

    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        DateTime from_date = FromToDate1.GetDateFrom;
        DateTime to_date = FromToDate1.GetDateTo;
        string advertiserId = txt_AdvertiserId.Text.Trim();
        if (from_date == DateTime.MinValue || to_date == DateTime.MinValue)
        {
            DateTime dt = DateTime.Now;
            from_date = dt.AddMonths(-1);
            to_date = dt;
        }
        // public Result<NonPpcCallsReportResponse> NonPpcCallsReport(NonPpcCallsReportRequest request)
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.NonPpcCallsReportRequest _request = new WebReferenceReports.NonPpcCallsReportRequest();
        
        _request.FromDate = from_date;
        _request.ToDate = to_date;
        _request.GroupBy = GetGroupByString(ddl_GroupBy.SelectedValue);
        if (ddl_GroupBy.SelectedValue == "Dates")
            _GridView.CssClass = "data-tableppc";
        else
            _GridView.CssClass = "data-table";

        _request.AdvertiserId = advertiserId;
        WebReferenceReports.ResultOfNonPpcCallsReportResponse result = null;
        try
        {
            result = _report.NonPpcCallsReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Dictionary<string, bool> ToShow;
        DataTable data = GetDataTable.GetDataTableFromListVisibleColumsDateFormat(result.Value.Cases, out ToShow, siteSetting.DateFormat);
        _GridView.PageIndex = 0;
        dataV = data;
        lbl_RecordMached.Text = ToolboxReport1.GetRecordMaches(data.Rows.Count);
        ToShowV = ToShow;
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + Master.GetNoResultsMessage + "');", true);
            dataV = null;
        }
     //   else
            SetTable();
    }
    private void SetTable()
    {
        
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        Dictionary<string, bool> ToShow = ToShowV;
        foreach (KeyValuePair<string, bool> kvp in ToShow)
        {
            int _index = GetColumIndex(kvp.Key);
            if (_index < 0)
                continue;
            _GridView.Columns[_index].Visible = kvp.Value;
        }
        _uPanel.Update();
    }
    WebReferenceReports.eGroupBy GetGroupByString(string _val)
    {
        foreach (WebReferenceReports.eGroupBy gb in Enum.GetValues(typeof(WebReferenceReports.eGroupBy)))
        {
            if (gb.ToString() == _val)
                return gb;
        }
        return WebReferenceReports.eGroupBy.None;
    }
    int GetColumIndex(string name)
    {
        foreach (DataControlField field in _GridView.Columns)
        {
            if (field.SortExpression == name)
            {
                return _GridView.Columns.IndexOf(field);
            }
        }

        return -1;
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetTable();
    }

    private DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    Dictionary<string, bool> ToShowV
    {
        get { return (ViewState["ToShow"] == null) ? new Dictionary<string, bool>() : (Dictionary<string, bool>)ViewState["ToShow"]; }
        set { ViewState["ToShow"] = value; }
    }
}
