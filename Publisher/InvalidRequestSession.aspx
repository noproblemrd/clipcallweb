﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvalidRequestSession.aspx.cs" Inherits="Publisher_InvalidRequestSession" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <link type="text/css" rel="Stylesheet" href="style.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="primary-content">
    <div class="page-content minisite-content2 MinWin">
        <h3>
            <asp:Label ID="lbl_PageTitle" runat="server" Text="Description Quality By Session"></asp:Label>

            
        </h3>
        <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />  
            <Columns>
                <asp:TemplateField SortExpression="CreatedOn"  >
                    <HeaderTemplate >
                        <asp:Label ID="Label101" runat="server" Text="<%#lbl_Date.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label102" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="Session">
                    <HeaderTemplate>
                        <asp:Label ID="Label117" runat="server" Text="<%#lbl_Session.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                       
                            <asp:Label ID="Label118" runat="server" Text="<%# Bind('Session') %>"></asp:Label>
                       
                    </ItemTemplate>
                </asp:TemplateField>
                                
                    <asp:TemplateField SortExpression="Description">
                    <HeaderTemplate>
                        <asp:Label ID="Label103" runat="server" Text="<%#lbl_Description.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label104" runat="server" Text="<%# Bind('Description') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Heading" >
                    <HeaderTemplate>
                        <asp:Label ID="Label105" runat="server" Text="<%#lbl_Heading.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label106" runat="server" Text="<%# Bind('Heading') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Keyword">
                    <HeaderTemplate>
                        <asp:Label ID="Label107" runat="server" Text="<%#lbl_Keyword.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label108" runat="server" Text="<%# Bind('Keyword') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Origin" >
                    <HeaderTemplate>
                        <asp:Label ID="Label109" runat="server" Text="<%#lbl_Origin.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label110" runat="server" Text="<%# Bind('Origin') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Phone">
                    <HeaderTemplate>
                        <asp:Label ID="Label111" runat="server" Text="<%#lbl_Phone.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label112" runat="server" Text="<%# Bind('Phone') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="QualityCheckName" >
                    <HeaderTemplate>
                        <asp:Label ID="Label113" runat="server" Text="<%#lbl_QualityCheckName.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label114" runat="server" Text="<%# Bind('QualityCheckName') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Region" >
                    <HeaderTemplate>
                        <asp:Label ID="Label115" runat="server" Text="<%#lbl_Region.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label116" runat="server" Text="<%# Bind('Region') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                                
                <asp:TemplateField SortExpression="Url">
                    <HeaderTemplate>
                        <asp:Label ID="Label119" runat="server" Text="<%#lbl_Url.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label120" runat="server" Text="<%# Bind('Url') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField SortExpression="Event" >
                    <HeaderTemplate>
                        <asp:Label ID="Label500" runat="server" Text="<%#lbl_Event.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label501" runat="server" Text="<%# Bind('Event') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="ZipCodeCheckName" >
                    <HeaderTemplate>
                        <asp:Label ID="Label502" runat="server" Text="<%#lbl_ZipCodeCheckNames.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label503" runat="server" Text="<%# Bind('ZipCodeCheckName') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Flavor" >
                    <HeaderTemplate>
                        <asp:Label ID="Label504" runat="server" Text="<%#lbl_Flavor.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label505" runat="server" Text="<%# Bind('Flavor') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Step" >
                    <HeaderTemplate>
                        <asp:Label ID="Label506" runat="server" Text="<%#lbl_Step.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label507" runat="server" Text="<%# Bind('Step') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Control" >
                    <HeaderTemplate>
                        <asp:Label ID="Label508" runat="server" Text="<%#lbl_Control.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label509" runat="server" Text="<%# Bind('Control') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 
            </Columns>
                <FooterStyle CssClass="footer"  />
            <PagerStyle CssClass="pager" />
        </asp:GridView>
    </div>
    </div>
    </div>
    <asp:Label ID="lbl_Date" runat="server" Visible="false" Text="Date"></asp:Label>
<asp:Label ID="lbl_Description" runat="server" Visible="false" Text="Description"></asp:Label>
<asp:Label ID="lbl_Heading" runat="server" Visible="false" Text="Heading"></asp:Label>
<asp:Label ID="lbl_Keyword" runat="server" Visible="false" Text="Keyword"></asp:Label>
<asp:Label ID="lbl_Origin" runat="server" Visible="false" Text="Origin"></asp:Label>
<asp:Label ID="lbl_Phone" runat="server" Visible="false" Text="Phone"></asp:Label>
<asp:Label ID="lbl_QualityCheckName" runat="server" Visible="false" Text="Quality check name"></asp:Label>
<asp:Label ID="lbl_Region" runat="server" Visible="false" Text="Region"></asp:Label>
<asp:Label ID="lbl_Session" runat="server" Visible="false" Text="Session"></asp:Label>
<asp:Label ID="lbl_Url" runat="server" Visible="false" Text="Url"></asp:Label>
<asp:Label ID="lbl_Event" runat="server" Visible="false" Text="Event"></asp:Label>

<asp:Label ID="lbl_Flavor" runat="server" Visible="false" Text="Flavor"></asp:Label>
<asp:Label ID="lbl_Step" runat="server" Visible="false" Text="Step"></asp:Label>
<asp:Label ID="lbl_Control" runat="server" Visible="false" Text="Control"></asp:Label>
<asp:Label ID="lbl_ZipCodeCheckNames" runat="server" Text="Zip Code" Visible="false"></asp:Label>
    </form>
</body>
</html>
