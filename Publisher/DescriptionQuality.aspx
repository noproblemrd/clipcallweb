﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="DescriptionQuality.aspx.cs" Inherits="Publisher_DescriptionQuality" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        SetDrillSession();
        hideDiv();
    }

    $(function () {
        SetDrillSession();
    });
    function SetDrillSession() {
        $('a._drillSession').each(function () {
            $(this).click(function () {
                var _session = $(this).find('span').html();
                var _opener = window.open('InvalidRequestSession.aspx?session=' + _session, 'InvalidRequestSession', 'width=1250,height=800,resizable=yes,scrollbars=yes');
                _opener.focus();
            });
        });
    }

    /***  Iframe Request Ticket   ****/
    function CloseIframe() {
        $find('_modal').hide();
        document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie popModal_del2_hide";
        document.getElementById("<%# _iframe.ClientID %>").src = "javascript:void(0);";
    }
    function OpenIframe(_path) {
        document.getElementById("<%# panel_iframe.ClientID %>").className = "popModal_del2_ie";
        document.getElementById("<%# _iframe.ClientID %>").src = _path;
        HideIframeDiv();
        $find('_modal').show();
    }
    function ShowIframeDiv() {
        document.getElementById("divIframeLoader").style.display = "none";
        document.getElementById("div_iframe_main").style.display = "block";
    }
    function HideIframeDiv() {

        document.getElementById("divIframeLoader").style.display = "block";
        document.getElementById("div_iframe_main").style.display = "none";
    }
    /***  Iframe Request Ticket   ****/
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">          
</cc1:ToolkitScriptManager>       
 <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2">

	
<div id="form-analytics">
	<div class="main-inputs with-total">
	
			<div class="form-field">
                <asp:Label ID="lbl_QualityName" CssClass="label" runat="server" Text="Quality Name"></asp:Label>
                <asp:DropDownList ID="ddl_QualityName"  CssClass="form-select" runat="server">
                </asp:DropDownList>
            
            </div>
            
			 
             <div class="form-field">
                <asp:Label ID="lbl_Flavors" CssClass="label" runat="server" Text="Flavors"></asp:Label>
                <asp:DropDownList ID="ddl_Flavors"  CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form-field">
                <asp:Label ID="lbl_Events" CssClass="label" runat="server" Text="Events"></asp:Label>
                <asp:DropDownList ID="ddl_Events"  CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form-field">
                <asp:Label ID="lbl_GroupBy" CssClass="label" runat="server" Text="Group by session"></asp:Label>
                 <asp:CheckBox ID="cb_GroupBy" runat="server" />
            </div>
            <div class="clear"><!-- --></div>
            <div class="form-field">
                <asp:Label ID="lbl_ZipCodeCheckNames" CssClass="label" runat="server" Text="Zip Code"></asp:Label>
                <asp:DropDownList ID="ddl_ZipCodeCheckNames"  CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form-field">
                <asp:Label ID="lbl_Steps" CssClass="label" runat="server" Text="Steps"></asp:Label>
                <asp:DropDownList ID="ddl_Steps"  CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form-field">
                <asp:Label ID="lbl_Controls" CssClass="label" runat="server" Text="Controls"></asp:Label>
                <asp:DropDownList ID="ddl_Controls"  CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            
        </div>    
        <div class="clear"><!-- --></div>
     <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>
              <div class="clear"><!-- --></div>
                    
	 </div>	
     
	<div id="Table_Report" class="table" runat="server">
		
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                    
		<asp:GridView ID="_GridView" runat="server" CssClass="data-table" 
                AutoGenerateColumns="false">
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />  
            <Columns>
                <asp:TemplateField SortExpression="CreatedOn"  >
                    <HeaderTemplate >
                        <asp:Label ID="Label101" runat="server" Text="<%#lbl_Date.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label102" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField SortExpression="Session">
                    <HeaderTemplate>
                        <asp:Label ID="Label117" runat="server" Text="<%#lbl_Session.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a id="a_session" runat="server" class="_drillSession" href="javascript:void(0);">
                            <asp:Label ID="Label118" runat="server" Text="<%# Bind('Session') %>"></asp:Label>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                                
                    <asp:TemplateField SortExpression="Description">
                    <HeaderTemplate>
                        <asp:Label ID="Label103" runat="server" Text="<%#lbl_Description.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label104" runat="server" Text="<%# Bind('Description') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 

                <asp:TemplateField SortExpression="Heading" >
                    <HeaderTemplate>
                        <asp:Label ID="Label105" runat="server" Text="<%#lbl_Heading.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label106" runat="server" Text="<%# Bind('Heading') %>" ></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Keyword">
                    <HeaderTemplate>
                        <asp:Label ID="Label107" runat="server" Text="<%#lbl_Keyword.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label108" runat="server" Text="<%# Bind('Keyword') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Origin" >
                    <HeaderTemplate>
                        <asp:Label ID="Label109" runat="server" Text="<%#lbl_Origin.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label110" runat="server" Text="<%# Bind('Origin') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Phone">
                    <HeaderTemplate>
                        <asp:Label ID="Label111" runat="server" Text="<%#lbl_Phone.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label112" runat="server" Text="<%# Bind('Phone') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="QualityCheckName" >
                    <HeaderTemplate>
                        <asp:Label ID="Label113" runat="server" Text="<%#lbl_QualityCheckName.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label114" runat="server" Text="<%# Bind('QualityCheckName') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Region" >
                    <HeaderTemplate>
                        <asp:Label ID="Label115" runat="server" Text="<%#lbl_Region.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label116" runat="server" Text="<%# Bind('Region') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                                
                <asp:TemplateField SortExpression="Url">
                    <HeaderTemplate>
                        <asp:Label ID="Label119" runat="server" Text="<%#lbl_Url.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label120" runat="server" Text="<%# Bind('Url') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField SortExpression="Count">
                    <HeaderTemplate>
                        <asp:Label ID="Label121" runat="server" Text="<%#lbl_Count.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label122" runat="server" Text="<%# Bind('Count') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="LastDescription" >
                    <HeaderTemplate>
                        <asp:Label ID="Label205" runat="server" Text="<%#lbl_Description.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label206" runat="server" Text="<%# Bind('LastDescription') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Event" >
                    <HeaderTemplate>
                        <asp:Label ID="Label500" runat="server" Text="<%#lbl_Event.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label501" runat="server" Text="<%# Bind('Event') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="ZipCodeCheckName" >
                    <HeaderTemplate>
                        <asp:Label ID="Label502" runat="server" Text="<%#lbl_ZipCodeCheckNames.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label503" runat="server" Text="<%# Bind('ZipCodeCheckName') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Flavor" >
                    <HeaderTemplate>
                        <asp:Label ID="Label504" runat="server" Text="<%#lbl_Flavor.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label505" runat="server" Text="<%# Bind('Flavor') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Step" >
                    <HeaderTemplate>
                        <asp:Label ID="Label506" runat="server" Text="<%#lbl_Step.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label507" runat="server" Text="<%# Bind('Step') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Control" >
                    <HeaderTemplate>
                        <asp:Label ID="Label508" runat="server" Text="<%#lbl_Control.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label509" runat="server" Text="<%# Bind('Control') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField SortExpression="Passed" >
                    <HeaderTemplate>
                        <asp:Label ID="Label305" runat="server" Text="<%#lbl_Passed.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Repeater ID="_Repeater" runat="server" DataSource="<%# Bind('_data') %>">
                            <ItemTemplate>
                                <asp:HyperLink ID="a_request" runat="server" NavigateUrl="<%# Bind('script') %>" Text="<%# Bind('Passed_name') %>"></asp:HyperLink>
                                
                            </ItemTemplate>
                            <SeparatorTemplate>
                                <span>, </span>
                            </SeparatorTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
                <FooterStyle CssClass="footer"  />
            <PagerStyle CssClass="pager" />
        </asp:GridView>

        <uc1:TablePaging ID="TablePaging1" runat="server" />
                    
        </ContentTemplate>
        </asp:UpdatePanel>
                
	</div>
	
		
</div>

 <asp:Panel ID="panel_iframe" runat="server" CssClass="popModal_del2_ie popModal_del2_hide" style="display:none;"
     Width="900" Height="90%">
     <div id="divIframeLoader" class="divIframeLoader">   
        <table>
            <tr>
                <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
            </tr>
        </table>
    </div>
    <div id="div_iframe_main" style="height:100%;">
        <div class="top"></div>
            <a id="a_close" runat="server" class="span_A" href="javascript:void(0);" onclick="javascript:CloseIframe();"></a>
        <iframe runat="server" id="_iframe" width="880px" height="100%" frameborder="0" ALLOWTRANSPARENCY="true" ></iframe>
        <div class="bottom2"></div>
    </div>
</asp:Panel>
<cc1:ModalPopupExtender ID="_mpe" runat="server"
            TargetControlID="btn_virtual"
            PopupControlID="panel_iframe"
                BackgroundCssClass="modalBackground"                  
                BehaviorID="_modal" 
                Y="5"              
                DropShadow="false">
</cc1:ModalPopupExtender>
    
<div style="display:none;">

    <asp:Button ID="btn_virtual" runat="server"  style="display:none;" CausesValidation="false"/>
</div>


<asp:Label ID="lbl_NoResult" runat="server" Visible="false" Text="There are no results"></asp:Label>

<asp:Label ID="lbl_Date" runat="server" Visible="false" Text="Date"></asp:Label>
<asp:Label ID="lbl_Description" runat="server" Visible="false" Text="Description"></asp:Label>
<asp:Label ID="lbl_Heading" runat="server" Visible="false" Text="Heading"></asp:Label>
<asp:Label ID="lbl_Keyword" runat="server" Visible="false" Text="Keyword"></asp:Label>
<asp:Label ID="lbl_Origin" runat="server" Visible="false" Text="Origin"></asp:Label>
<asp:Label ID="lbl_Phone" runat="server" Visible="false" Text="Phone"></asp:Label>
<asp:Label ID="lbl_QualityCheckName" runat="server" Visible="false" Text="Quality check name"></asp:Label>
<asp:Label ID="lbl_Region" runat="server" Visible="false" Text="Region"></asp:Label>
<asp:Label ID="lbl_Session" runat="server" Visible="false" Text="Session"></asp:Label>
<asp:Label ID="lbl_Url" runat="server" Visible="false" Text="Url"></asp:Label>
<asp:Label ID="lbl_Event" runat="server" Visible="false" Text="Event"></asp:Label>

<asp:Label ID="lbl_Flavor" runat="server" Visible="false" Text="Flavor"></asp:Label>
<asp:Label ID="lbl_Step" runat="server" Visible="false" Text="Step"></asp:Label>
<asp:Label ID="lbl_Control" runat="server" Visible="false" Text="Control"></asp:Label>


<asp:Label ID="lbl_Count" runat="server" Visible="false" Text="Count"></asp:Label>
<asp:Label ID="lbl_Passed" runat="server" Visible="false" Text="Form"></asp:Label>



<asp:Label ID="lbl_PageTitle" runat="server" Text="Description Quality" Visible="false"></asp:Label>
<asp:Label ID="lbl_total" runat="server" Visible="false" Text="Total"></asp:Label>
</asp:Content>



