﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Publisher_Auction_UpsaleSearch : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void LoadData(DataTable data, int SiteLangId)
    {
        lbl_Results.Text = EditTranslateDB.GetControlTranslate(SiteLangId, "eGeneralAuction", eGeneralAuction.Results.ToString());
        _Repeater.DataSource = data;
        _Repeater.DataBind();
    }
    public void LoadDataMin(DataTable data)
    {
        lbl_Results.Text = eGeneralAuction.Results.ToString();
        _Repeater.DataSource = data;
        _Repeater.DataBind();
    }
    
}
