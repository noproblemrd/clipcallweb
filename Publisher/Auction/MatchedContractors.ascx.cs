using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Publisher_Auction_MatchedContractors : System.Web.UI.UserControl
{
    public string _expertise;
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    public void LoadData(string Expertise)
    {
        this._expertise = Expertise;
        this.DataBind();
    }
    public string GetIcon()
    {
        return ResolveUrl("~") + "images/icon-calling.gif";
    }
    public void SetTranslate(int SiteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "MatchedContractors.ascx", SiteLangId);
    }
}
