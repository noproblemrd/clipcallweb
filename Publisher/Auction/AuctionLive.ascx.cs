using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Publisher_Auction_AuctionLive : System.Web.UI.UserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    public void LoadData(DataTable data, string _mode)
    {
        switch (_mode)
        {
            case ("Initiated"): div_initiateCall.Attributes["style"] = "display:block;";
                break;
            case ("Open"): div_openCall.Attributes["style"] = "display:block;";                
                break;
            case ("Close"):
                break;
        }
        _Repeater.DataSource = data;
        _Repeater.DataBind();
    }
    public void SetTranslate(int SiteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "MatchedContractors.ascx", SiteLangId);
    }
}
