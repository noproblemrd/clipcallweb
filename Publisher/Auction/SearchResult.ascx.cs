using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Publisher_Auction_SearchResult : System.Web.UI.UserControl
{
    /*
    protected void Page_Init(object sender, EventArgs e)
    {
        
        HtmlLink link = new HtmlLink();
        link.Href = "style.css";
        link.Attributes.Add("rel", "stylesheet");
        link.Attributes.Add("type", "text/css");
        Page.Header.Controls.Add(link);
         
    }
     * */
    public string GetIcon()
    {
        return ResolveUrl("~") + "images/icons/icon-call-now_clear.gif";
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void LoadData(DataTable data)
    {
        _Repeater.DataSource = data;
        _Repeater.DataBind();
    }
    public void SetTranslate(int SiteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "MatchedContractors.ascx", SiteLangId);
    }
}
