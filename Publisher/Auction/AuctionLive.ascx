<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AuctionLive.ascx.cs" Inherits="Publisher_Auction_AuctionLive" %>

<div id="div_initiateCall" class="explanation clearfix" runat="server" style="display:none;">
<center>
    <asp:Label ID="lbl_speakingWith" runat="server" Text="You are speaking with"></asp:Label>
    </center>
   </div>
 <div id="div_openCall" class="explanation clearfix" runat="server" style="display:none;">
 <center>
     <asp:Label ID="lbl_ringInSecond" runat="server" Text="Your Phone will ring in a second. The following Suppliers are willing to talk to you NOW."></asp:Label>
     </center>
      </div>                      

<asp:Repeater ID="_Repeater" runat="server">
<HeaderTemplate>
    <div style="overflow-x: hidden; overflow-y: auto; height: 132px;">
    <table id="call_results" cellspacing="0" border="0"  >    
</HeaderTemplate>
<ItemTemplate>
    <tr>
        <td class="details">
            <h3 class="name">
            
                <asp:Label ID="Label1" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label>
                          
            </h3>
            <asp:Image ID="_Image" runat="server" Width="82" Height="85" ImageUrl="<%# Bind('Logo') %>" CssClass="image" />          
        </td>
        <td class="status">
            <div class="statusContent">
            <div class="shortDescription">
                <asp:Label ID="lbl_Desc" runat="server" Text="<%# Bind('ShortDescription') %>"></asp:Label>
            </div>
            <div class="icons">
            <div class="edit">
                <asp:Label ID="lblCalls" runat="server" Text="<%# lbl_Calls.Text %>"></asp:Label>:
                <asp:Label ID="lbl_CallsNum" runat="server" Text="<%# Bind('SumAssistanceRequests') %>"></asp:Label>
            </div>
            <div class="users">
                <asp:Label ID="lblEmployees" runat="server" Text="<%# lbl_Employees.Text %>"></asp:Label>:
                <asp:Label ID="lbl_EmployeesNum" runat="server" Text="<% # Bind('NumberOfEmployees')%>"></asp:Label>            
           </div>
            </div></div>
        </td>
        
        <td class="call">
            <div class="wrap">
            <asp:Label ID="lblCallNow" runat="server" Text="<%# lbl_CallNow.Text %>" CssClass="label"></asp:Label>
            <asp:Image ID="_Image_CallNow" runat="server" ImageUrl="<%# Bind('CallIcon') %>" AlternateText="Call Now" CssClass="<%# Bind('CallIconClass') %>" />            
            
           
             <asp:Label ID="lbl_phone" runat="server" Text="" CssClass="phone"></asp:Label>
            </div>
        </td>
    </tr>
</ItemTemplate>
<FooterTemplate>
    </table>
    </div>
</FooterTemplate>
</asp:Repeater>
<asp:Label ID="lbl_Calls" runat="server" Text="Calls" Visible="false"></asp:Label>
<asp:Label ID="lbl_Employees" runat="server" Text="Employees" Visible="false"></asp:Label>
<asp:Label ID="lbl_CallNow" runat="server" Text="CALL NOW" Visible="false"></asp:Label>
