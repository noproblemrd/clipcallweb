<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchResult.ascx.cs" Inherits="Publisher_Auction_SearchResult" %>
<asp:Repeater ID="_Repeater" runat="server">
<HeaderTemplate>
<div style="overflow-x: hidden; overflow-y: auto; height: 132px;">
    <table id="call_results" cellspacing="0" border="0">
    
</HeaderTemplate>
<ItemTemplate>
    <tr>
        <td class="details">
            <h3 class="name">
                <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank"
                 NavigateUrl="<%# Bind('SupplierUrl') %>" Text="<%# Bind('SupplierName') %>"></asp:HyperLink>            
            </h3>
            <asp:Image ID="_Image" runat="server" Width="82" Height="85" ImageUrl="<%# Bind('Logo') %>" CssClass="image" />          
        </td>
        <td class="status">
            <div class="statusContent">
            <div class="shortDescription">
                <asp:Label ID="lbl_Desc" runat="server" Text="<%# Bind('ShortDescription') %>"></asp:Label>
            </div>
            <div class="icons">
            <div class="edit">
                <asp:Label ID="lblCalls" runat="server" Text="<%# lbl_Calls.Text %>"></asp:Label>:
                <asp:Label ID="lbl_CallsNum" runat="server" Text="<%# Bind('SumAssistanceRequests') %>"></asp:Label>
            </div>
            <div class="users">
                <asp:Label ID="lblEmployees" runat="server" Text="<%# lbl_Employees.Text %>"></asp:Label>:
                <asp:Label ID="lbl_EmployeesNum" runat="server" Text="<% # Bind('NumberOfEmployees')%>"></asp:Label>            
           </div>
            </div></div>
        </td>
        
        <td class="call">
            <div class="wrap">
            <asp:Label ID="lblCallNow" runat="server" Text="<%# lbl_CallNow.Text %>" CssClass="label"></asp:Label>
            <asp:Image ID="_Image_CallNow" runat="server" ImageUrl="<%# GetIcon() %>" AlternateText="Call Now" />            
            
            <asp:Label ID="lbl_Extension" runat="server" Text="<%# Bind('ExtensionNumber') %>" CssClass="extension"></asp:Label>
             <asp:Label ID="lbl_phone" runat="server" Text="<%# Bind('DirectNumber') %>" CssClass="phone"></asp:Label>
            </div>
        </td>
    </tr>
</ItemTemplate>
<FooterTemplate>
    </table>
</div>
</FooterTemplate>
</asp:Repeater>

<asp:Label ID="lbl_Calls" runat="server" Text="Calls" Visible="false"></asp:Label>
<asp:Label ID="lbl_Employees" runat="server" Text="Employees" Visible="false"></asp:Label>
<asp:Label ID="lbl_CallNow" runat="server" Text="CALL NOW" Visible="false"></asp:Label>
