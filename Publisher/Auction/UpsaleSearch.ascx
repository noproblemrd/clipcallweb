﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpsaleSearch.ascx.cs" Inherits="Publisher_Auction_UpsaleSearch" %>
<center>
<h3>
    <asp:Label ID="lbl_Results" runat="server" Text="Results"></asp:Label>
</h3>
</center>
<asp:Repeater ID="_Repeater" runat="server">
<HeaderTemplate>
    <table class="result_table">
</HeaderTemplate>
<ItemTemplate>
    <tr >
        <td>
             <asp:Label ID="Label1" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label>
        </td>
        <td class="span_num">
            <span id="span_num" runat="server" visible="<%# Bind('HasExtension') %>">
        <asp:Label ID="Label2" runat="server" Text="<%# Bind('ExtensionNumber') %>"></asp:Label>
        <span>-</span>
    </span>
    <asp:Label ID="Label3" runat="server" Text="<%# Bind('DirectNumber') %>"></asp:Label>
    
        </td>
    </tr>
</ItemTemplate>
<FooterTemplate>
    </table>
</FooterTemplate>
</asp:Repeater>




