﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class Publisher_Holidays : PageSetting
{    
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(btn_Set);
        if (!IsPostBack)
        {
            SetToolBox();
            LoadHOlidays();
            FromToDateTime1.SetDate_To_Today(true);
        }
        SetToolBoxEvents();

    }

    private void LoadHOlidays()
    {

        DataTable data = GetTableHolidays();
        if (data == null)
            return;
        SetGridView(data);
        _updatePanelTable.Update();

        
    }
    DataTable GetTableHolidays()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfClosureDateData result = null;
        try
        {
            result = _site.GetClosureDates();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        DataTable data = new DataTable();//GetDataTable.GetDataTableFromListDateTimeFormat(result.Value, siteSetting.DateFormat, siteSetting.TimeFormat);
        data.Columns.Add("Name", typeof(string));
        data.Columns.Add("ClosureDateId", typeof(string));
        data.Columns.Add("FromDate", typeof(string));
        data.Columns.Add("FromTime", typeof(string));
        data.Columns.Add("ToDate", typeof(string));
        data.Columns.Add("ToTime", typeof(string));
        foreach (WebReferenceSite.ClosureDateData cdd in result.Value)
        {
            DataRow row = data.NewRow();
            row["Name"] = cdd.Name;
            row["ClosureDateId"] = cdd.ClosureDateId.ToString();
            row["FromDate"] = string.Format(siteSetting.DateFormat, cdd.FromDate);
            row["FromTime"] = string.Format(siteSetting.TimeFormat, cdd.FromDate);
            row["ToDate"] = string.Format(siteSetting.DateFormat, cdd.ToDate);
            row["ToTime"] = string.Format(siteSetting.TimeFormat, cdd.ToDate);
            data.Rows.Add(row);
        }
        return data;
    }
    void SetGridView(DataTable data)
    {
        StringBuilder sb = new StringBuilder();
   //     DataView data = dataViewV;
        //    _GridView.PageIndex = 0;
        _GridView.DataSource = data;
        _GridView.DataBind();

        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noreasultSearch", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.NoRecordDisplay) + "');", true);
            CBsV = string.Empty;
            _updatePanelTable.Update();
            return;
        }
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }

        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        _updatePanelTable.Update();
    }
    private void SetToolBox()
    {
        Toolbox1.RemoveAttach();
        Toolbox1.RemoveSearch();
        string script = "openPopup(); return false;";
        Toolbox1.SetClientScriptToAdd(script);

    }
    protected void btn_Set_click(object sender, EventArgs e)
    {
        DateTime date_from = FromToDateTime1.GetDateFrom;
        DateTime date_to = FromToDateTime1.GetDateTo;
        if (date_from == DateTime.MinValue || date_to == DateTime.MinValue)
        {
            FromToDateTime1._ErrorDate();
            return;
        }
        if (date_from > date_to)
        {
            FromToDateTime1._ErrorDateSooner();
            return;
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.UpsertClosureDateRequest _request = new WebReferenceSite.UpsertClosureDateRequest();
        _request.FromDate = date_from;
        _request.ToDate = date_to;
        _request.Name = txt_Name.Text;
        string _id = hf_HolidayId.Value;
        _request.ClosureDateId = (Utilities.IsGUID(_id)) ? new Guid(_id) : Guid.Empty;
        WebReferenceSite.ResultOfGuid result = null;
        try
        {
            result = _site.UpsertClosureDate(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            this.Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        Update_Success();
        _mpe.Hide();
        LoadHOlidays();        
    }
    private void SetToolBoxEvents()
    {
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
    }

    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        bool IsSelect = false;
        int indx = -1;
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
            {
                if (IsSelect)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ChooseOne", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.ChooseOne) + "');", true);
                    return;
                }
                IsSelect = true;
                indx = row.RowIndex;
            }
        }
        if (indx > -1)
        {
            GridViewRow row = _GridView.Rows[indx];
            hf_HolidayId.Value = ((Label)row.FindControl("lbl_HolidayId")).Text;

            string _from_date = ((Label)row.FindControl("lbl_from_date")).Text;
            DateTime date_from = ConvertToDateTime.CalanderToDateTime(_from_date, ((PageSetting)Page).siteSetting.DateFormat);
            string _from_time = ((Label)row.FindControl("lbl_from_time")).Text;
            TimeSpan time_from = DateTime.Parse(_from_time).TimeOfDay;
            date_from = date_from.Add(time_from);

            string _to_date = ((Label)row.FindControl("lbl_to_date")).Text;
            DateTime date_to = ConvertToDateTime.CalanderToDateTime(_to_date, ((PageSetting)Page).siteSetting.DateFormat);
            string _to_time = ((Label)row.FindControl("lbl_to_time")).Text;
            TimeSpan to_from = DateTime.Parse(_to_time).TimeOfDay;
            date_to = date_to.Add(to_from);

            FromToDateTime1.SetDateFrom(date_from);
            FromToDateTime1.SetDateTo(date_to);

            txt_Name.Text = ((Label)row.FindControl("lbl___Name")).Text;
            
            _mpe.Show();
        }
    }

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        List<Guid> list = new List<Guid>();
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            if (cb.Checked)
            {
                Guid _id = new Guid(((Label)row.FindControl("lbl_HolidayId")).Text);
                list.Add(_id);
               
            }
        }
        if (list.Count == 0)
            return;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.DeleteClosureDates(list.ToArray());
        }        
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            this.Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        Update_Success();
        LoadHOlidays();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        DataTable data = GetTableHolidays();

        if (data == null || data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            return;
        }
        //  PrintHelper.PrintWebControl(_GridView, dataViewV);
        Session["data_print"] = data;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        DataTable data = GetTableHolidays();
        if (data == null || data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordExport + "');", true);
            return;
        }
        ToExcel to_excel = new ToExcel(this, "Holidays");
        if (!to_excel.ExecExcel(data.AsDataView(), _GridView))
            Update_Faild();
    }
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    protected string GetScriptIfDateNotSooner
    {
        get
        {
            return FromToDateTime1.GetScriptIfDateNotSooner();
        }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
    protected string PastDateMessage
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl_DatePast.Text); }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    
}