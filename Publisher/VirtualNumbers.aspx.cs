using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class Publisher_VirtualNumbers : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!userManagement.IsPublisher() && string.IsNullOrEmpty(GetGuidSetting()))
            {
                string csnameLogin = "setLogin";                

                if (!ClientScript.IsClientScriptBlockRegistered(this.GetType(), csnameLogin))
                {
                    string csTextLogin = "top.location='" + ResolveUrl("~") + @"Publisher/PublisherLogin.aspx';";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), csnameLogin, csTextLogin, true);
                }
                return;
            }
            
            string csnameStep = "setStep";
            if (! ClientScript.IsStartupScriptRegistered(this.GetType(), csnameStep))
            {
                string csTextStep = "witchAlreadyStep(9);";
                ClientScript.RegisterStartupScript(this.GetType(), csnameStep, csTextStep, true);
            }  
            LoadReport();
        }
        
    }
    
    void LoadReport()
    {
        /*
        DataTable data = new DataTable();
        data.Columns.Add("ModifiedOn");
        data.Columns.Add("ExpertiseId");
        data.Columns.Add("ExpertiseName");
        data.Columns.Add("OriginName");
        data.Columns.Add("OriginId");
        data.Columns.Add("Number");
        */
        WebReferenceSupplier.Supplier _supplier=WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.GetSuppliersDirectNumbersRequest _request=new WebReferenceSupplier.GetSuppliersDirectNumbersRequest();
        _request.SupplierId = new Guid(GetGuidSetting());
        WebReferenceSupplier.ResultOfGetSuppliersDirectNumbersResponse _response = null;
        try
        {
            _response = _supplier.GetSuppliersDirectNumbers(_request);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (_response.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        DataTable data = GetDataTable.GetDataTableFromListDateTimeFormat(_response.Value.DirectNumberCollection, siteSetting.DateFormat, siteSetting.TimeFormat);//GetDataTable.GetDataTableFromList(_response.Value.DirectNumberCollection);
        if (data.Rows.Count == 0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + lbl_noreasult.Text + "');", true);
        _GridView.DataSource = data;
        _GridView.DataBind();
        dataV = data;
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataTable data = dataV;
        _GridView.DataSource = data;
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    
}
