﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="PopupEngineReport.aspx.cs" Inherits="Publisher_PopupEngineReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="../jquery-ui.min.js" type="text/javascript"></script> 
    <script type="text/javascript">
        window.onload = appl_init;
        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);
        }
        function BeginHandler() {
            showDiv();
        }
        function EndHandler() {
            hideDiv();
       //     RblChargeChange();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" >    
    </cc1:ToolkitScriptManager>
    <uc1:ToolboxReport ID="_Toolbox" runat="server" />
    <div class="page-content minisite-content2"> 
  <div id="form-analytics">
    	       
      <div class="main-inputs">	
               
             <div class="form-field">
                <asp:Label ID="lbl_Origin" runat="server" Text="Origin" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_Origin" CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
             <div class="form-field">
                <asp:Label ID="lbl_Campaign" runat="server" Text="Campaign" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_Campaign" CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            
        </div>        
       
        <div class="callreport">
              
             <uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" />
        </div>

        <div class="clear"></div>
         <div class="main-ddl">
            
           
            
            <div class="form-field">
                <asp:Label ID="lbl_Country" runat="server" Text="Country" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_Country" CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>
            
                  
        </div>        
       
        <div class="clear"></div> 
           
              <div class="clear"></div>
            <div class="heading" style="margin:0;">
            <asp:Button ID="btn_Run" runat="server" CssClass="CreateReportSubmit2" Text="Create report" 
                    onclick="btn_Run_Click"  />
            </div>
            
           
            
            <div class="table">
            
                <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="resultsMultiple" id="div_results" runat="server" visible="false">
                        <asp:Label ID="lbl_RecordMached" runat="server"></asp:Label>                    
                    </div>
                    <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                        CssClass="data-table"  >
                    <RowStyle CssClass="_even" />
                    <AlternatingRowStyle CssClass="_odd" />
                    <Columns>
                        <asp:TemplateField SortExpression="Date">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="Date"></asp:Label> 
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text="<%# Bind('Date') %>"></asp:Label>        
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Hits">
                            <HeaderTemplate>
                                <asp:Label ID="Label1020" runat="server" Text="Hits"></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2020" runat="server" Text="<%# Bind('Hits') %>"></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                      
                            
                    </Columns>
                    </asp:GridView>
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            
            
        </div>
    </div>
<asp:Label ID="lbl_AdEngineReport" runat="server" Text="Ad Engine Report" Visible="false"></asp:Label>
</asp:Content>


