﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" 
AutoEventWireup="true" CodeFile="SavedReport.aspx.cs" Inherits="Publisher_SavedReport" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/Toolbox/ToolboxWizardReport.ascx" tagname="Toolbox" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript">
    function pageLoad(sender, args) 
    {         
        
        appl_init();
    }
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }   




</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ></cc1:ToolkitScriptManager>

<uc1:Toolbox runat="server" ID="Toolbox1" /> 
<div class="page-content minisite-content2">
 
    <div id="form-analytics">
    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="results2"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div> 
        <div class="table2">   
        <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false"
            CssClass="data-table" AllowPaging="True" 
            onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
        <RowStyle CssClass="odd" />
        <AlternatingRowStyle CssClass="even" />        
        <FooterStyle CssClass="footer"  />
        <PagerStyle CssClass="pager" />
        <Columns>
        
        <asp:TemplateField >              
        <HeaderTemplate>
            <asp:Label ID="Label01" runat="server" Text="<%# lbl_All.Text %>"></asp:Label>
            <br />
            <asp:CheckBox ID="cb_all" runat="server" >
            </asp:CheckBox>                    
        </HeaderTemplate>
        <ItemTemplate>
            <asp:CheckBox ID="cb_choose" runat="server" />
            
        </ItemTemplate>
        </asp:TemplateField>
                
        <asp:TemplateField>
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_name.Text %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
        
            <asp:HyperLink ID="hl_report" runat="server" Text="<%# Bind('Name') %>" NavigateUrl="<%# Bind('ToReport') %>"></asp:HyperLink>
            
            <asp:Label ID="lbl_reportId" runat="server" Text="<%# Bind('ReportId') %>" Visible="false"></asp:Label>
        </ItemTemplate>    
        </asp:TemplateField>
        
        <asp:TemplateField>
        <HeaderTemplate>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_ReportIssue.Text %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label4" runat="server" Text="<%# Bind('IssueName') %>"></asp:Label>
        </ItemTemplate>    
        </asp:TemplateField>
        
        <asp:TemplateField>
        <HeaderTemplate>
            <asp:Label ID="Label5" runat="server" Text="<%# lbl_description.Text %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('Description') %>"></asp:Label>
        </ItemTemplate>    
        </asp:TemplateField>
        
        <asp:TemplateField>
        <HeaderTemplate>
            <asp:Label ID="Label7" runat="server" Text="<%# lbl_createdOn.Text %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label8" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label>
        </ItemTemplate>    
        </asp:TemplateField>
        
         <asp:TemplateField>
        <HeaderTemplate>
            <asp:Label ID="Label9" runat="server" Text="<%# lbl_CreatedBy.Text %>"></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="lblUser" runat="server" ></asp:Label>
            <asp:Label ID="lblUserId" runat="server" Text="<%# Bind('UserId') %>" Visible="false"></asp:Label>
        </ItemTemplate>    
        </asp:TemplateField>
        
        </Columns>
        </asp:GridView>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</div>

<asp:Label ID="lbl_All" runat="server" Text="All" Visible="false"></asp:Label>
<asp:Label ID="lbl_name" runat="server" Text="Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_ReportIssue" runat="server" Text="Report issue" Visible="false"></asp:Label>
<asp:Label ID="lbl_description" runat="server" Text="Description" Visible="false"></asp:Label>
<asp:Label ID="lbl_createdOn" runat="server" Text="Created on" Visible="false"></asp:Label>
<asp:Label ID="lbl_CreatedBy" runat="server" Text="Created by" Visible="false"></asp:Label>


</asp:Content>

