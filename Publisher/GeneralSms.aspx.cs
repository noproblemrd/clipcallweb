﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_GeneralSms : PageSetting
{
    const string SUCCESS = "The message sent successfully!";
    const string FAILED = "The message failed to sent!";
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            SetToolbox();
            RegularExpressionValidator_phone.ValidationExpression = siteSetting.GetMobileRegularExpression;
        }
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle("General Text Message");
        ToolboxReport1.RemoveExcelButton();
        ToolboxReport1.RemovePrintButton();
    }
    protected void lb_send_Click(object sender, EventArgs e)
    {
        AarService.AarService aar = WebServiceConfig.GetAarService();
        AarService.ResultOfBoolean result = null;
        bool isSuccess = false;
        try
        {
            result = aar.SendGeneralSms(txt_message.Text, txt_phone.Text);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            SetResultLabel(isSuccess);
            return;
        }
        if (result.Type != AarService.eResultType.Failure)
            isSuccess = result.Value;
        SetResultLabel(isSuccess);
        if(isSuccess)
        {
            txt_message.Text = "";
            txt_phone.Text = "";
        }
    }
    void SetResultLabel(bool isSuccess)
    {
        lbl_result.Text = isSuccess ? SUCCESS : FAILED;
    }
}