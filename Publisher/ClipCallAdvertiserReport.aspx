﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="ClipCallAdvertiserReport.aspx.cs" Inherits="Publisher_ClipCallAdvertiserReport" %>


<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

	<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>    
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function _CreateExcel() {
            var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"AarCallCenterReport"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                 alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
<div class="page-content minisite-content2" >    	
	<div id="form-analytics" >
		 <div class="main-inputs" runat="server" id="PublisherInput" style="width:100%;">	        
            <div class="Date-field" runat="server" id="div_Date">
				<uc1:FromToDatePicker ID="_FromToDatePicker" runat="server" />
			</div>  			
            <div class="form-field form-WidthDuration" runat="server" id="div_Step" style="top:0; margin-bottom:0;">
                <asp:Label ID="lbl_Stpe" runat="server" Text="Step" CssClass="label"></asp:Label>
                <asp:DropDownList ID="ddl_Step" CssClass="form-select" runat="server" ></asp:DropDownList>
            </div>
		</div>
      
		<div style="clear:both;"></div>
        <div style="text-align: center; margin-top: 15px;">
			<asp:LinkButton ID="lb_RunReport" runat="server" Text="Run" CssClass="anchor_CreateReportSubmit2" 
				onclick="lb_RunReport_Click"></asp:LinkButton>
		</div>
		<div style="clear:both;"></div>
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>        
        <div id="Table_Report"  runat="server" >  
				<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="date">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="Date"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblBroker" runat="server" Text="<%# Bind('date') %>"></asp:Label>
                            <asp:HiddenField ID="hf_accountId" runat="server" Value="<%# Bind('accountId') %>" />
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="name">
						<HeaderTemplate>
							<asp:Label ID="Label101" runat="server" Text="Name"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label112" runat="server" Text="<%# Bind('name') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="categories">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="Categories"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label11" runat="server" Text="<%# Bind('categories') %>"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>					

					<asp:TemplateField SortExpression="address">
						<HeaderTemplate>
							<asp:Label ID="Label14" runat="server" Text="Address"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label15" runat="server" Text="<%# Bind('address') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="Radius">
						<HeaderTemplate>
							<asp:Label ID="Label69" runat="server" Text="Radius"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label70" runat="server" Text="<%# Bind('Radius') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="stepInRegistration">
						<HeaderTemplate>
							<asp:Label ID="Label16" runat="server" Text="Registration Step"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label17" runat="server" Text="<%# Bind('stepInRegistration') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

					 <asp:TemplateField SortExpression="phone">
						<HeaderTemplate>
							<asp:Label ID="Label18" runat="server" Text="Phone"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label19" runat="server" Text="<%# Bind('phone') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>					

                     <asp:TemplateField SortExpression="ReferralCode">
						<HeaderTemplate>
							<asp:Label ID="Label41" runat="server" Text="Referral Code"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label42" runat="server" Text="<%# Bind('ReferralCode') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>	

                     <asp:TemplateField SortExpression="LicenseNumber">
						<HeaderTemplate>
							<asp:Label ID="Label61" runat="server" Text="License Number"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label62" runat="server" Text="<%# Bind('LicenseNumber') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>	

                     <asp:TemplateField SortExpression="Description">
						<HeaderTemplate>
							<asp:Label ID="Label63" runat="server" Text="Description"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label64" runat="server" Text="<%# Bind('Description') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>	

                     <asp:TemplateField SortExpression="Email">
						<HeaderTemplate>
							<asp:Label ID="Label65" runat="server" Text="Email"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label66" runat="server" Text="<%# Bind('Email') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>	

                     <asp:TemplateField SortExpression="WebSite">
						<HeaderTemplate>
							<asp:Label ID="Label67" runat="server" Text="WebSite"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label68" runat="server" Text="<%# Bind('WebSite') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>	                     	

                     <asp:TemplateField SortExpression="Device">
						<HeaderTemplate>
							<asp:Label ID="Label43" runat="server" Text="Device"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label44" runat="server" Text="<%# Bind('Device') %>"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>
                    
                     <asp:TemplateField SortExpression="IsConfirmed">
						<HeaderTemplate>
							<asp:Label ID="Label43" runat="server" Text="Is Confirmed Policy"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:Image ID="Image1" runat="server" ImageUrl="<%# Bind('ConfirmedIcon') %>" Visible="<%# Bind('IsConfirmed') %>" />
						</ItemTemplate>
					</asp:TemplateField>	
				</Columns>    
				</asp:GridView>
			</div>
        </div>
    </div>
    <asp:Label ID="lbl_ClipCallAdvertiserReport" runat="server" Text="ClipCall Advertiser Report" Visible="false"></asp:Label>
</asp:Content>

