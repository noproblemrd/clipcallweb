﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="DpzReport.aspx.cs" Inherits="Publisher_DpzReport" %>
 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>      
<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register src="~/Publisher/DpzControls/CallDistributionReportControl.ascx" tagname="CallDistribution" tagprefix="uc2" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<link type="text/css" rel="Stylesheet" href="../scripts/jquery-ui-1.9.2.custom.dialog.min.css" />

 <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
 
 <script src="../scripts/jquery-ui-1.9.2.custom.DatePicker_Dialog.min.js" type="text/javascript"></script>
<script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>
          <!--
<script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>
-->

 

<script type="text/javascript">
    var DaysInterval = new Number();
    var ChoosenIntervalOk = new Boolean();
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);

        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }
    function CheckSupplierDate() {
       
        if ($('#<%# hf_SelectSupplierId.ClientID %>').val().length > 0 && $('#<%# hf_SelectSupplierId.ClientID %>').val().length == 0) {
            $.ajax({
                url: "<%# GetSupplierList %>",
                data: "{ 'PageIndex': '" + pageindex + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0)
                        return;
                    var _data = eval('(' + data.d + ')');
                    var $table = $('.VirtualNumberDialog > table');
                    $table.empty();
                    var $tr;
                    for (var i = 0; i < _data.numbers.length; i++) {
                        if ((i % 3) == 0) {
                            if ($tr)
                                $table.append($tr);
                            $tr = $('<tr>');
                        }
                        $tr.append($('<td><a href="javascript:void(0);">' + _data.numbers[i] + '</a></td>'));
                    }
                    if ($tr)
                        $table.append($tr);
                    $table.find('a').each(function () {
                        $(this).click(function () {
                            SetOneVirtualNumber($(this).html());
                            $(".VirtualNumberDialog").dialog("close");
                        });
                    });
                    if (_data.HasPrevious)
                        EnableAnchor($('.VirtualNumberDialog').find('#<%# a_previous.ClientID %>'), PreviusVirtualNumber);
                    else
                        DisableAnchor($('.VirtualNumberDialog').find('#<%# a_previous.ClientID %>'));
                    if (_data.HasNext)
                        EnableAnchor($('.VirtualNumberDialog').find('#<%# a_next.ClientID %>'), NextVirtualNumber);
                    else
                        DisableAnchor($('.VirtualNumberDialog').find('#<%# a_next.ClientID %>'))
                    $(".VirtualNumberDialog").dialog("open");

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //              alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
            return false;
        }
        return true;
    }
    function CheckCurrentDates() {
        var elem = document.getElementById("<%# GetDivCurrentId %>");
        var result = Check_DateValidation(elem);
        if (!result)
            alert("<%# GetInvalidDate %>");
        return result;
    }
    /*
    function SetIntervalDatesValidation(elem) {

        ChoosenIntervalOk = true;
        SetDdlInterval(elem);
        if (!ChoosenIntervalOk)
            alert(document.getElementById('<# lbl_ChosenInterval.ClientID %>').value);
    }
    */
    function SetDdlInterval(elem) {
        DaysInterval = GetIntervalDates(); //elem.options[elem.selectedIndex].value;
        var _index = elem.selectedIndex;
        var Interval_name = elem.options[_index].value;
        var _days = GetRangeDays("<%# GetDivCurrentId %>");


        if (DaysInterval > _days) {
            _index--;
            ChoosenIntervalOk = false;
            elem.selectedIndex = _index;
            SetDdlInterval(elem);
        }
    }
    function OnBlurByDays(elm) {

        var elm_to;
        var elm_from;
        var elm_parent = getParentByClassName(elm, "div_DatePicker");
        var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;

        if (!validate_Date(elm.value, _format)) {
            try { alert(WrongFormatDate); } catch (ex) { }
            elm.focus();
            elm.select();
            return -1;
        }
        if (elm.className.indexOf("_to") == -1) {
            elm_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
            elm_from = elm;
        }
        else {
            elm_from = getElementsWithMultiClassesByClass("_from", elm_parent, "input")[0];
            elm_to = elm;
        }
        DaysInterval = GetIntervalDates();
        var _end = GetDateFormat(elm_to.value, _format);
        var _start = GetDateFormat(elm_from.value, _format);
        if ((_start + (24 * 60 * 60 * 1000 * DaysInterval)) > _end) {
            if (elm_from.id == elm.id) {
                _end = new Date(_start);
                if (DaysInterval == 29)
                    _end.addMonth(1);
                else if (DaysInterval == 89)
                    _end.addMonth(3);
                else if (DaysInterval == 364)
                    _end.addFullYear(1);
                else {
                    _end = _start + (24 * 60 * 60 * 1000 * DaysInterval);
                    _end = new Date(_end);
                }
                elm_to.value = GetDateString(_end, _format);
            }
            else {

                _start = new Date(_end);
                if (DaysInterval == 29)
                    _start.addMonth(-1);
                else if (DaysInterval == 89)
                    _start.addMonth(-3);
                else if (DaysInterval == 364)
                    _start.addFullYear(-1);
                else {
                    _start = _end - (24 * 60 * 60 * 1000 * DaysInterval);
                    _start = new Date(_start);
                }
                elm_from.value = GetDateString(_start, _format);
            }

            return -10;
        }
        return 1;

    }
    /* CHART  */
    function LoadChart(fileXML, _chart) {
        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render("div_chart");
    }

    $(function () {

        $(".SelectAccount").dialog({
            autoOpen: false,
            resizable: false,
            width: 500,
            modal: true,
            beforeClose: function (event, ui) {
                //         var eeeee = '0';
            }
        });
        $('#<%# btn_Choose.ClientID %>').click(function () {
            $('#hf_SupplierPageIndex').val('');
            GetSupplierList(null);
        });
        $('#<%# txt_BusinessName.ClientID %>').blur(function () {
            if ($(this).val().length == 0) {
                $('#<%# txt_SelectedBusinessName.ClientID %>').val('');                
                $('#<%# hf_SelectSupplierId.ClientID %>').val('');
            }
        });

    });
    function GetSupplierPageIndex() {
        var str = $('#hf_SupplierPageIndex').val();
        var pageindex;
        if (str.length > 0 && !isNaN(str))
            pageindex = new Number(str);
        else
            pageindex = 1;
        return pageindex;
    }
    function SetSupplierPageIndex(PageIndex) {
        $('#hf_SupplierPageIndex').val(PageIndex);
    }
    function GetSupplierList(pageindex) {
        var WebService;
        var TheData;
         if (pageindex) {
             WebService = "<%# GetSupplierListPage %>";
             TheData = "{ 'IndexPage': '" + pageindex + "' }";
         }
         else {
             WebService = "<%# GetSupplierList %>";
             var _str = $('#<%# txt_BusinessName.ClientID %>').val();
             TheData = "{ 'str': '" + _str + "' }";
         }
        $.ajax({
            url: WebService,
            data: TheData,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if (data.d.length == 0)
                    return;
                var _data = eval('(' + data.d + ')');
                var $table = $('.SelectAccount > table');
                $table.empty();
                var $tr;
                for (var i = 0; i < _data.Datas.length; i++) {
                    if ((i % 3) == 0) {
                        if ($tr)
                            $table.append($tr);
                        $tr = $('<tr>');
                    }
                    $tr.append($('<td><a href="javascript:void(0);">' + ((_data.Datas[i].Name.length == 0) ? _data.Datas[i].BizId : _data.Datas[i].Name) + '</a><input type="hidden" value="' + _data.Datas[i].SupplierId + '"/></td>'));
                }
                if ($tr)
                    $table.append($tr);
                $table.find('a').each(function () {
                    $(this).click(function () {
                        SetOneSupplier(this);
                        $(".SelectAccount").dialog("close");
                    });
                });
                if (_data.HasPrevious)
                    EnableAnchor($('.SelectAccount').find('#<%# a_previous.ClientID %>'), PreviusSupplierList);
                else
                    DisableAnchor($('.SelectAccount').find('#<%# a_previous.ClientID %>'));
                if (_data.HasNext)
                    EnableAnchor($('.SelectAccount').find('#<%# a_next.ClientID %>'), NextSupplierList);
                else
                    DisableAnchor($('.SelectAccount').find('#<%# a_next.ClientID %>'))
                $(".SelectAccount").dialog("open");

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                            //alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;

            }

        });
    }
    function SetOneSupplier(elem) {
        $('#<%# txt_SelectedBusinessName.ClientID %>').val($(elem).html());
        var supplier_id = $(elem).parent().find('input').val();
        $('#<%# hf_SelectSupplierId.ClientID %>').val(supplier_id);
    }
    function DisableAnchor($_a){
         $_a.addClass('a_disable');
         $_a.unbind('click');
    }
    function EnableAnchor($_a, _onclick){
         $_a.removeClass('a_disable');
         $_a.unbind('click');
         $_a.click(function(){
             _onclick();
         });
     }
     function NextSupplierList() {
         var pageindex = GetSupplierPageIndex();
         pageindex++;
         SetSupplierPageIndex(pageindex);
         GetSupplierList(pageindex);
     }
     function PreviusSupplierList() {
         var pageindex = GetSupplierPageIndex();
         pageindex--;
         SetSupplierPageIndex(pageindex);
         GetSupplierList(pageindex);
     }
     function IfJSLoaded(jsscriptpath) {
         var _scripts = document.getElementsByTagName('head')[0].getElementsByTagName('script');
         for (var i = 0; i < _scripts.length; i++) {
             if (_scripts[i].src.indexOf(jsscriptpath) > -1)
                 return true;
         }
         return false;
     }
     function _CreateExcel() {
         $.ajax({
             url: '<%# GetCreateExcel %>',
             data: null,
             dataType: "json",
             type: "POST",
             contentType: "application/json; charset=utf-8",
             dataFilter: function (data) { return data; },
             success: function (data) {
                 if (data.d.length == 0) {
                     return;
                 }
                 var _iframe = document.createElement('iframe');
                 _iframe.style.display = 'none';
                 _iframe.src = '<%# DownloadExcel %>' + '?fn=' + data.d;
                 document.getElementsByTagName('body')[0].appendChild(_iframe);

             },
             error: function (XMLHttpRequest, textStatus, errorThrown) {
                 //              alert(textStatus);
             },
             complete: function (jqXHR, textStatus) {
                 //  HasInHeadingRequest = false;

             }

         });
     }
    
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
 <uc1:ToolboxReport runat="server" ID="ToolboxReport1" /> 
<div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    <div class="main-inputs" style="width: 100%;">	
               
        <div class="form-field">
             <asp:Label ID="lbl_BusinessName" runat="server" Cssclass="label" Text="Business Name"></asp:Label>
                   <asp:TextBox ID="txt_BusinessName" runat="server" CssClass="form-text"></asp:TextBox>
        </div>
        <div class="form-field" style="margin-top:33px; text-align: center;">
            <a id="btn_Choose" class="a_choose" runat="server">Choose</a>
        </div>
        <div class="form-field" style="margin-top:37px;">
             <asp:TextBox ID="txt_SelectedBusinessName" runat="server" CssClass="form-text" ReadOnly="true"></asp:TextBox>
            <asp:HiddenField ID="hf_SelectSupplierId" runat="server" />    
        </div>  
    </div> 
    
    
     <div class="clear"></div>
     <div>
         <div class="form-field">
            <asp:Label ID="lbl_ReportType" runat="server"  CssClass="label-secondLine" Text="Report Type"></asp:Label>
            <asp:DropDownList ID="ddl_ReportType" CssClass="form-select" runat="server">
                
            </asp:DropDownList>
        </div>
     </div>
     <div>
         <div class="form-field">
            <asp:Label ID="lbl_CallStatus" runat="server"  CssClass="label-secondLine" Text="Call Status"></asp:Label>
            <asp:DropDownList ID="ddl_CallStatus" CssClass="form-select" runat="server">
                
            </asp:DropDownList>
        </div>
     </div>
     <div class="form-fieldDleft" style="margin-top: -19px;">
    
        <div class="dash"><uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" /></div>
 
  </div>
     <div class="clear"></div>    
     <div>
         <div class="form-field">
            <asp:Label ID="lbl_Status" runat="server"  CssClass="label-secondLine" Text="Status"></asp:Label>
            <asp:DropDownList ID="ddl_DapazStatus" CssClass="form-select" runat="server">
                
            </asp:DropDownList>
        </div>
     </div>
     <div class="clear"></div>
    <div class="dashsubmit">
        <asp:Button ID="btn_run" runat="server" Text="Create report"  CssClass="CreateReportSubmit2"
            onclick="btn_run_Click" OnClientClick="return CheckCurrentDates();" />
    
    </div>
    </div>  
     <div class="clear"><!-- --></div> 
    <div>
        <asp:UpdatePanel ID="UpdatePanel_table" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
        <div runat="server" id="div_table_title">
            <table class="data-table table-borde">
            <tr>
                <th>
                   <asp:Label ID="lbl_TotalMinutesNumber" runat="server" Text="Total minutes number"></asp:Label>  
                </th>
                <th>
                    <asp:Label ID="lbl_TotalCallersNumber" runat="server" Text="Total callers number"></asp:Label>
            
                </th>
                <th>
                    <asp:Label ID="lbl_AverageTimeCall" runat="server" Text="Average time call"></asp:Label>
                </th>
            </tr>
            <tr>
                <td>
                   <asp:Label ID="lblTotalMinutesNumber" runat="server"></asp:Label>  
                </td>
                <td>
                    <asp:Label ID="lblTotalCallersNumber" runat="server"></asp:Label>
            
                </td>
                <td>
                    <asp:Label ID="lblAverageTimeCall" runat="server"></asp:Label>
                </td>
            </tr>
            </table>
        </div>
         <div>
            <div id="div_chart"></div> 
        </div>
        <asp:PlaceHolder ID="_PlaceHolder" runat="server"></asp:PlaceHolder>  
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
     
</div>
<div id="SelectAccount" class="SelectAccount" style="display:none;" runat="server" title="<%# lbl_SelectAccount.Text %>">
    <table>
    
    </table>
    <div class="NextPrevious">
        <a id="a_previous" runat="server" onselectstart="return false;">Previous</a>
        <a id="a_next"  runat="server" onselectstart="return false;">Next</a>
    </div>
    <input id="hf_SupplierPageIndex" type="hidden" />
</div>   
    <asp:Label ID="lbl_SelectAccount" runat="server" Text="Select Account" Visible="false"></asp:Label>
   

       <asp:Label ID="lbl_DateError" runat="server" Text="Invalid date" Visible="false"></asp:Label>
        <asp:Label ID="lbl_ZapGroupTitle" runat="server" Text="Zap Group Report" Visible="false"></asp:Label>
</asp:Content>

