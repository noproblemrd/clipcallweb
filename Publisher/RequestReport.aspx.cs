﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;
using System.IO;
using System.Web.Services.Protocols;
using System.Xml.Linq;

public partial class Publisher_RequestReport : PageSetting
{
    const int DAYS_INTERVAL = 1;
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    const string EXCEL_NAME = "RequestReport";
    
    protected void Page_Load(object sender, EventArgs e)
    {

        ScriptManager1.RegisterAsyncPostBackControl(ddl_Affiliate);
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());

        TablePagingConfiguration();
        if (!IsPostBack)
        {
          //  RequestsReportRequestV = null;
            SetToolbox();
            SetDateInterval();
            LoadExperties();
            LoadTypes();
            LoadRequestTypes();
            Loadorigins();
            LoadNumberOfUpsales();
            LoadDurationCallOptions();
            Clearreports();
            PageBound();
            ExecRequestReport();
        }            
        SetToolboxEvents();       
        
    }
    private void LoadTypes()
    {
        ddl_SliderType.Items.Clear();
        ddl_SliderType.Items.Add(new ListItem("--ALL--", "-1"));
        foreach (eSliderType est in Enum.GetValues(typeof(eSliderType)))
            ddl_SliderType.Items.Add(new ListItem(est.ToString(), ((int)est).ToString()));
        ddl_SliderType.SelectedIndex = 0;
    }
    private void LoadDurationCallOptions()
    {
        ddl_IncludeMaxDuration.Items.Clear();
        foreach(string str in Enum.GetNames(typeof(WebReferenceReports.eCallDurationType)))
        {
            ddl_IncludeMaxDuration.Items.Add(new ListItem(str, str));
        }
        ddl_IncludeMaxDuration.SelectedIndex = 0;
    }

    private void LoadNumberOfUpsales()
    {
        ddl_NumberOfUpsales.Items.Clear();
        ddl_NumberOfUpsales.Items.Add(new ListItem(lbl_WithoutUpsale.Text, ""));
        ddl_NumberOfUpsales.Items.Add(new ListItem(lbl_all.Text, "-1"));
        ddl_NumberOfUpsales.Items.Add(new ListItem(lbl_NoUpsale.Text, "0"));
        for (int i = 1; i < 8; i++)
            ddl_NumberOfUpsales.Items.Add(new ListItem(i.ToString(), i.ToString()));
        ddl_NumberOfUpsales.SelectedIndex = 0;
    }

    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
    private void TablePagingConfiguration()
    {
        TablePaging1._lnkPage_Click += new EventHandler(_Paging_click);
        TablePaging1.PagesInPaging = PAGE_PAGES;
        TablePaging1.ItemInPage = ITEM_PAGE;
    }
    protected void _Paging_click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (RequestsReportRequestV == null)
            return;
        WebReferenceReports.RequestsReportRequest _request = RequestsReportRequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        LoadExpertiseTable(_request);
    }
    private void PageBound()
    {
        div_NumberOfAdvertisersProvided.DataBind();
        if (siteSetting.GetSiteID == PpcSite.GetCurrent().ZapSiteId)
        {
            div_NumberOfUpsales.Visible = false;
            div_IncludeMaxDuration.Visible = false;
            div_OnlyUpsale.Visible = false;
            div_OnlyRejectByEmailOrName.Visible = false;
        }
    }
    private void Loadorigins()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetAllOrigins(true);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Affiliate.Items.Clear();
        ddl_Affiliate.Items.Add(new ListItem(lbl_all.Text, Guid.Empty.ToString()));
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            ListItem li = new ListItem(gsp.Name, gsp.Id.ToString());
            ddl_Affiliate.Items.Add(li);
        }
        ddl_Affiliate.SelectedIndex = 0;
    }
   

    private void LoadRequestTypes()
    {
        ddl_Type.Items.Clear();
        foreach (WebReferenceReports.CaseType ct in Enum.GetValues(typeof(WebReferenceReports.CaseType)))
        {
            ListItem li = new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "CaseType", ct.ToString()), ct.ToString());
            li.Selected = (ct == WebReferenceReports.CaseType.All);
            ddl_Type.Items.Add(li);
        }
    }
    

    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        
        WebReferenceReports.RequestsReportRequest _request = RequestsReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataResult dr = _resultRequestReport(_request);
        if (dr == null || dr.data.Rows.Count == 0)
        {
            Update_Faild();
            return;
        }
        SetDataToExport(dr.data);
        ToExcel te = new ToExcel(this, EXCEL_NAME);
        if (!te.ExecExcel(dr.data))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        WebReferenceReports.RequestsReportRequest _request = RequestsReportRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataResult dr = _resultRequestReport(_request);
        if (dr == null || dr.data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
    //    SetDataToExport(dr.data);

        Session["data_print"] = dr.data;
        Session["grid_print"] = _reapeter;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
         
    }
    void SetDataToExport(DataTable data)
    {
        /*
         *  data.Columns.Add("AdvertiserName");
        data.Columns.Add("guid");
        data.Columns.Add("OnClientClick");
        data.Columns.Add("RegionName");
        data.Columns.Add("CaseScript");
         * */
        data.Columns.Remove("guid");
        data.Columns.Remove("OnClientClick");
        data.Columns.Remove("CaseScript");
        data.Columns.Remove("AdvertiserName");
    }

    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_PageTitle.Text);
    }
   
  
    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);
        
        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;
    }
    protected void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecRequestReport();
    }
    protected void ExecRequestReport()
    {
        Clearreports();

        DateTime _to = FromToDate1.GetDateTo;
        DateTime _from = FromToDate1.GetDateFrom;

        if (_to == DateTime.MinValue)
        {
            _to = DateTime.Now;

        }

        if (_from == DateTime.MinValue)
        {
            _from = _to.AddMonths(-1);

        }
        else
        {
            if (_from > _to)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + FromToDate1.GetDateError + "');", true);

                return;
            }

        }
        WebReferenceReports.RequestsReportRequest requestReportRequest = new WebReferenceReports.RequestsReportRequest();
        requestReportRequest.PageSize = ITEM_PAGE;
        requestReportRequest.FromDate = _from;
        requestReportRequest.ToDate = _to;
        requestReportRequest.OnlyForUpsale = cb_OnlyUpsale.Checked;
        requestReportRequest.OnlyRejectByEmailOrName = cb_OnlyRejectByEmailOrName.Checked;
        requestReportRequest.SliderType = int.Parse(ddl_SliderType.SelectedValue);
        WebReferenceReports.eCallDurationType ecdt;
        if (!Enum.TryParse(ddl_IncludeMaxDuration.SelectedValue, out ecdt))
            ecdt = WebReferenceReports.eCallDurationType.Without;
        requestReportRequest.WithMaxCallDuration = ecdt;
        with_duration = ecdt == WebReferenceReports.eCallDurationType.With;
        int NumOfUpsales;
        if (int.TryParse(ddl_NumberOfUpsales.SelectedValue, out NumOfUpsales))
        {
            requestReportRequest.NumberOfUpsales = NumOfUpsales;
            with_upsale = true;
        }
        else
        {
            requestReportRequest.NumberOfUpsales = null;
            with_upsale = false;
        }
        
        string RequestId = txtRequestId.Text.Trim();
        WebReferenceReports.CaseType _caseType;
        if (!Enum.TryParse(ddl_Type.SelectedValue, out _caseType))
            _caseType = WebReferenceReports.CaseType.All;
        requestReportRequest.Type = _caseType;
        requestReportRequest.ExpertiseId = new Guid(ddl_Heading.SelectedValue);
        requestReportRequest.CaseNumber = RequestId;
        int NumSupplier;
        if (int.TryParse(txt_NumSupplier.Text, out NumSupplier))
            requestReportRequest.SuppliersProvided = NumSupplier;
        requestReportRequest.CaseOrigin = new Guid(ddl_Affiliate.SelectedValue);
        string _url = txt_Url.Text;
        _url = new ReportWizardService().GetNewOneSuggest(_url, "Url");
        if(!string.IsNullOrEmpty(_url))
            requestReportRequest.Url = _url;

        requestReportRequest.PageNumber = 1;
        RequestsReportRequestV = requestReportRequest;
        LoadExpertiseTable(requestReportRequest);
        
        _UpdatePanelSearch.Update();

    }    
    
    void NotResult()
    {
        Clearreports();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
    }


    void LoadExpertiseTable(WebReferenceReports.RequestsReportRequest requestReportRequest)
    {        
        DataResult dr = _resultRequestReport(requestReportRequest);
        if (dr == null)
        {
            Update_Faild();
            return;
        }
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
        if (dr.TotalRows > 0)
            BindDataToRepeater(dr.data);
        else
            NotResult();      
        

    }
    void BindDataToRepeater(DataTable data)
    {
        Table_Report.Visible = true;
        _reapeter.DataSource = data;
        _reapeter.DataBind();
        _UpdatePanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = _TotalRows + " " + ToolboxReport1.RecordMached;
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    DataTable LoadToDataTable(WebReferenceReports.ResultOfRequestsReportResponse resultRequestReport)
    {
        DataTable data = new DataTable();
        data.Columns.Add("Date");
        data.Columns.Add("Id");
        data.Columns.Add("Heading");
        data.Columns.Add("Consumer");
        data.Columns.Add("NumberRequestedAdvertisers");
        data.Columns.Add("NumberAdvertisersProvided");
        data.Columns.Add("Revenue");
        data.Columns.Add("AdvertiserName");
        data.Columns.Add("guid");
        data.Columns.Add("OnClientClick");
        data.Columns.Add("RegionName");
        data.Columns.Add("CaseScript");
        data.Columns.Add("Status");
        data.Columns.Add("a_class");
        data.Columns.Add("Upsales");
        data.Columns.Add("MaxCallDuration");
        

        foreach (WebReferenceReports.IncidentData _data in resultRequestReport.Value.Incidents)
        {
            DataRow row = data.NewRow();

            row["Date"] = string.Format(siteSetting.DateFormat, _data.CreatedOn) + "</br>" +
                    string.Format(siteSetting.TimeFormat, _data.CreatedOn);
            row["Id"] = _data.CaseNumber;
            row["Heading"] = _data.ExpertiseName;
            row["Consumer"] = _data.ConsumerName;

            row["RegionName"] = _data.RegionName;
            row["NumberRequestedAdvertisers"] = _data.NumberOfRequestedAdvertisers.ToString();
            row["NumberAdvertisersProvided"] = _data.NumberOfAdvertisersProvided.ToString();
            row["Revenue"] = String.Format("{0:0}", _data.Revenue);
            string _id = _data.CaseId.ToString();
            row["guid"] = _id;


            row["OnClientClick"] = "javascript:openAdvIframe('" + _data.ConsumerId + "');";
            row["CaseScript"] = (_data.CaseId == Guid.Empty) ? "" : "javascript:OpenIframe('RequestTicket.aspx?RequestTicket=" + _id + "');";
           
            row["Status"] = _data.RequestStatus;
            row["Upsales"] = _data.Upsales;
            row["MaxCallDuration"] = _data.MaxCallDuration;
            data.Rows.Add(row);

        }
        return data;
    }
    DataResult _resultRequestReport(WebReferenceReports.RequestsReportRequest requestReportRequest)
    {
        WebReferenceReports.Reports _site = WebServiceConfig.GetReportsReference(this);
        _site.Timeout = 300000;
        WebReferenceReports.ResultOfRequestsReportResponse resultRequestReport;
        try
        {
            resultRequestReport = _site.RequestsReport(requestReportRequest);
            if (resultRequestReport.Type != WebReferenceReports.eResultType.Success)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return null;

            }

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        DataTable data = LoadToDataTable(resultRequestReport);
        DataResult dr = new DataResult() { data = data, CurrentPage = resultRequestReport.Value.CurrentPage, TotalPages = resultRequestReport.Value.TotalPages, TotalRows = resultRequestReport.Value.TotalRows };
        return dr;
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow row = (HtmlTableRow)(e.Item.FindControl("tr_show"));
        string css_class = (e.Item.ItemIndex % 2 == 0) ? "odd" : "even";
        row.Attributes.Add("class", css_class);
        
        string _guid = ((Label)(e.Item.FindControl("lbl_guid"))).Text;
        Guid _id;
        if (!Guid.TryParse(_guid, out _id))
            _id = Guid.Empty;
        if (_id == Guid.Empty)
        {
            HtmlControl _a = (HtmlControl)(e.Item.FindControl("a_Case"));
            _a.Attributes.Add("class", "WithoutUnderline");
        }        
    }
    protected void ddl_Affiliate_SelectedIndexChanged(object sender, EventArgs e)
    {
        /*
        if(RequestsReportRequestV == null)
            return;
         
        WebReferenceReports.RequestsReportRequest _request = RequestsReportRequestV;
        _request.CaseOrigin = new Guid(ddl_Affiliate.SelectedValue);
        _request.PageNumber = 1;
        RequestsReportRequestV = _request;
        LoadExpertiseTable(_request);
        _UpdatePanelSearch.Update();
         * * */
        ExecRequestReport();
    }
   

    void Clearreports()
    {
        Table_Report.Visible = false;
        //     _dateError.Visible = false;

   //     _PanelExpertiseError.Visible = false;       

        //gv_GroupByExpertise.DataSource = null;
        //gv_GroupByExpertise.DataBind();  
        RequestsReportRequestV = null;
        TotalRowsV = 0;
        _reapeter.DataSource = null;
        _reapeter.DataBind();
        _UpdatePanel.Update();
    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
    
    protected bool ToShow
    {
        get { return userManagement.IsPublisher(); }
    }

    WebReferenceReports.RequestsReportRequest RequestsReportRequestV
    {
        get
        {
            return (Session["RequestsReportRequest"] == null) ? null : (WebReferenceReports.RequestsReportRequest)Session["RequestsReportRequest"];
        }
        set { Session["RequestsReportRequest"] = value; }
    }
    protected string SuggestNewList
    {
        get { return ResolveUrl("~/Publisher/ReportWizardService.asmx/SuggestNewList"); }
    }
    protected string GetNewOneSuggest
    {
        get { return ResolveUrl("~/Publisher/ReportWizardService.asmx/GetNewOneSuggest"); }
    }
    protected bool with_upsale
    {
        get { return (ViewState["with_upsale"] == null) ? false : (bool)ViewState["with_upsale"]; }
        set { ViewState["with_upsale"] = value; }
    }
    protected bool with_duration
    {
        get { return (ViewState["with_duration"] == null) ? false : (bool)ViewState["with_duration"]; }
        set { ViewState["with_duration"] = value; }
    }
    
}
