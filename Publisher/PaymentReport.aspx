﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="PaymentReport.aspx.cs" Inherits="Publisher_PaymentReport" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
   
    <style type="text/css">
        tr.future td
        {
            background-color: #d9ffb3;
        }
        tr.unbooked td
        {
            background-color: #ff9999;
        }

    </style>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
   
     <script type="text/javascript">
         window.onload = appl_init;

         function appl_init() {
             var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
             pgRegMgr.add_beginRequest(BeginHandler);
             pgRegMgr.add_endRequest(EndHandler);

         }
         function BeginHandler() {
             //     alert('in1');
             showDiv();
         }
         function EndHandler() {
             //      alert('out1');
             hideDiv();
         }
         function OpenIframe(_path) {
             var _leadWin = window.open(_path, "LeadDetails", "width=1000, height=650");
             _leadWin.focus();
         }
         function _CreateExcel() {
             var path = '<%# GetCreateExcel %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"<%# EXCEL_NAME %>"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcelUrl %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
         }
         function OpenTrace(tr, quoteId) {
             var _tr = $(tr).next('tr');
             if (_tr.is(":visible")) {
                 _tr.hide();
                 return;
             }
             var isOpen = $(tr).find('.h_isOpen').val();
             if (isOpen == "1") {
                 _tr.show();
                 return;
             }
             var path = '<%# HttpContext.Current.Request.Url.AbsolutePath %>' + "/GetPaymentTrace";
             var _data = '{"quoteId":"' + quoteId + '"}';
             $.ajax({
                 url: path,
                 data: _data,
                 dataType: "json",
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 dataFilter: function (data) { return data; },
                 success: function (data) {
                     if (data.d.length == 0) {
                         return;
                     }
                     var _tr = $(tr).next('tr');
                     _tr.find('td').html(data.d);
                     _tr.show();
                     $(tr).find('.h_isOpen').val("1");

                 },
                 error: function (XMLHttpRequest, textStatus, errorThrown) {
                     alert(textStatus);
                 },
                 complete: function (jqXHR, textStatus) {
                     //  HasInHeadingRequest = false;

                 }

             });
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
   <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
          
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	        
               <div class="main-inputs" runat="server" id="Div1">		
	                
                        
            			 <div class="form-field">
                            <asp:Label ID="lbl_IncludeQaTest" runat="server" Text="Include QA" CssClass="label"></asp:Label>
                           <asp:CheckBox ID="cb_IncludeQaTest" runat="server" />
                        </div>
                   </div>
             <div class="clear"></div>
                            
                    <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>   
                </div>

            <div style="clear:both;"></div>
    	
	        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	            <ContentTemplate>	
                    <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
	                <div >
                              
                                <asp:Repeater runat="server" ID="_reapeter" OnItemDataBound="_reapeter_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="data-table">
                                        
                                            <thead>
                                            <tr>                                                
                                                <th><asp:Label ID="lbl_Date" runat="server" Text="Payment Date"></asp:Label></th>
			                                    <th><asp:Label ID="Label51" runat="server" Text="Case number"></asp:Label></th>
			                                    <th><asp:Label ID="Label001" runat="server" Text="Case Date"></asp:Label></th>			                                    
			                                    <th><asp:Label ID="Label52" runat="server" Text="Amount"></asp:Label></th>			                                    
			                                    <th><asp:Label ID="Label53" runat="server" Text="Category"></asp:Label></th>			                                    
                                                <th><asp:Label ID="Label5001" runat="server" Text="Customer Name"></asp:Label></th>
                                                <th><asp:Label ID="Label5003" runat="server" Text="Customer Phone"></asp:Label></th>
			                                    <th><asp:Label ID="Label2" runat="server" Text="Customer Bonus"></asp:Label></th>
			                                    <th><asp:Label ID="Label5" runat="server" Text="Supplier Name" ></asp:Label></th>			                                    
			                                    <th><asp:Label ID="Label6" runat="server" Text="Supplier Phone" ></asp:Label></th>
                                                <th><asp:Label ID="Label17" runat="server" Text="Supplier Status" ></asp:Label></th>
                                                <th><asp:Label ID="Label1112" runat="server" Text="Is invited"></asp:Label></th>  
                                                                                              
                                                <th><asp:Label ID="Label3" runat="server" Text="ClipCall Fee"></asp:Label></th>
                                                <th><asp:Label ID="Label4" runat="server" Text="Transfered At"></asp:Label></th>
                                                <th><asp:Label ID="Label7" runat="server" Text="Transfered"></asp:Label></th>
                                                <th><asp:Label ID="Label8" runat="server" Text="StripeAccountId"></asp:Label></th>
                                                 <th><asp:Label ID="Label15" runat="server" Text="Payment Plan"></asp:Label></th>
                                                <th><asp:Label ID="Label9" runat="server" Text="Future Payment"></asp:Label></th>                                               
			                                </tr>
                                            </thead>
			                            </HeaderTemplate>
	                                    <ItemTemplate>
                                            <tr id="tr_show" runat="server">
			                                     <td>
			                                        <asp:Label ID="lbl_name" runat="server" Text="<%# Bind('PaymentDate') %>"></asp:Label>                                                   
                                                     <asp:HiddenField ID="h_QuoteId" runat="server" Value="<%# Bind('QuoteId') %>" />
                                                     <input id="h_isOpen" class="h_isOpen" type="hidden" />
			                                     </td>            			                         
			                                     <td>                                                    
                                                     <a id="a_Case" runat="server" href="<%# Bind('CaseScript') %>">
			                                            <asp:Label ID="Label500" runat="server" Text="<%# Bind('CaseNumber') %>"></asp:Label>
						                              </a>			                                        
                                                     <asp:HiddenField ID="hf_incidentId" runat="server" Value="<%# Bind('IncidentId') %>" />    
			                                     </td>
			                                     <td>
			                                        <asp:Label ID="Label501" runat="server" Text="<%# Bind('CaseDate') %>"></asp:Label>      
			                                     </td>
			                                     <td>
			                                        <asp:Label ID="Label502" runat="server" Text="<%# Bind('Amount') %>"></asp:Label>      
			                                     </td>			                                     
			                                     <td>
			                                        <asp:Label ID="Label503" runat="server" Text="<%# Bind('Category') %>"></asp:Label>
			                                     </td>
                                                <td>
			                                      <asp:Label ID="Label504" runat="server" Text="<%# Bind('CustomerName') %>"></asp:Label>     
			                                     </td>
                                                <td>
			                                      <asp:Label ID="Label505" runat="server" Text="<%# Bind('CustomerPhone') %>"></asp:Label>     
			                                     </td>
			                                     <td>
                                                   <asp:Label ID="Label506" runat="server"  Text="<%# Bind('CustomerBonus') %>"></asp:Label>
			                                     </td>			                                     
			                                     <td>
                                                   <asp:Label ID="Label507" runat="server" Text="<%# Bind('SupplierName') %>"></asp:Label>
			                                     </td>			                                     
			                                     <td>
                                                   <asp:Label ID="Label508" runat="server" Text="<%# Bind('SupplierPhone') %>"></asp:Label>
			                                     </td>
                                                 <td>
                                                   <asp:Label ID="Label18" runat="server" Text="<%# Bind('SupplierStatus') %>"></asp:Label>
			                                     </td>
                                                 <td >
                                                   <asp:Label ID="Label509" runat="server" Text="<%# Bind('IsInvited') %>"></asp:Label>
			                                     </td>                                                

                                                <td >
                                                   <asp:Label ID="Label10" runat="server" Text="<%# Bind('ClipCallFee') %>"></asp:Label>
			                                     </td>   
                                                <td >
                                                   <asp:Label ID="Label11" runat="server" Text="<%# Bind('TransferedAt') %>"></asp:Label>
			                                     </td>   
                                                <td >
                                                   <asp:Label ID="Label12" runat="server" Text="<%# Bind('AmountTransfered') %>"></asp:Label>
			                                     </td>   
                                                <td >
                                                   <asp:Label ID="Label13" runat="server" Text="<%# Bind('StripeAccountId') %>"></asp:Label>
			                                     </td>  
                                                <td >
                                                   <asp:Label ID="Label16" runat="server" Text="<%# Bind('PaymentPlan') %>"></asp:Label>
			                                     </td>   
                                                <td >
                                                   <asp:Label ID="Label14" runat="server" Text="<%# Bind('FutureTransfer') %>"></asp:Label>
                                                    <asp:HiddenField ID="hf_isFuture" runat="server" Value="<%# Bind('IsFuture') %>" />
			                                     </td>   
                                            </tr>
                                            <tr style="display:none;" class="tr_trace">
                                                <td colspan="11" >

                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>                
                                        </FooterTemplate>      
                                    </asp:Repeater>
                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
        </div>
   
</asp:Content>

