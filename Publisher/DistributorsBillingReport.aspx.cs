﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_DistributorsBillingReport : PageSetting
{
    const string DATE_FORMAT = "{0:dd/MM/yyyy HH:mm}";
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(lb_RunReport);
        TablePagingConfiguration();
        if (!IsPostBack)
        {
            LoadMonths();
            LoadYears();
            Loadorigins();
        }
        SetToolboxEvents();
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(lbl_DistributorsBillingReport.Text);
        ToolboxReport1.RemovePrintButton();
      //  string _script = "_CreateExcel();";
      //  ToolboxReport1.SetExcelJavascript(_script);

        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);

    }
    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
                
        ToExcel te = new ToExcel(this, lbl_DistributorsBillingReport.Text);
        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
    private void TablePagingConfiguration()
    {
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
      //  TablePaging1.PagesInPaging = PAGE_PAGES;
       // TablePaging1.ItemInPage = ITEM_PAGE;
    }

    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        LoadPageData(dataV, _pageNum, TablePaging1.ItemInPage, (_GridView.Visible) ? _GridView : _GridViewAll);
    }

    protected void _Paging_click(object sender, EventArgs e)
    {
        
    }
    private void LoadMonths()
    {
        int month = DateTime.Now.Month;
        ddl_Month.Items.Clear();
        foreach (eMonths em in Enum.GetValues(typeof(eMonths)))
        {
            ListItem li = new ListItem(em.ToString(), ((int)em).ToString());
            li.Selected = (int)em == month;
            ddl_Month.Items.Add(li);
        }
    }
    private void LoadYears()
    {
        ddl_Years.Items.Clear();
        int thisyear = DateTime.Now.Year;
        for (int i = 2012; i < 2021; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            li.Selected = i == thisyear;
            ddl_Years.Items.Add(li);
        }
    }
    private void Loadorigins()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetAllOrigins(false);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Origins.Items.Clear();
        ddl_Origins.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            ListItem li = new ListItem(gsp.Name, gsp.Id.ToString());
            ddl_Origins.Items.Add(li);
        }
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }
    void ExecReport()
    {
        WebReferenceReports.Reports report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.DistributorBillingReportRequest _request = new WebReferenceReports.DistributorBillingReportRequest();
        Guid OriginId;
        if (!Guid.TryParse(ddl_Origins.SelectedValue, out OriginId))
            OriginId = Guid.Empty;
        int month;
        if (!int.TryParse(ddl_Month.SelectedValue, out month))
            month = DateTime.Now.Month;
        int year;
        if (!int.TryParse(ddl_Years.SelectedValue, out year))
            year = DateTime.Now.Year;
        _request.OriginId = OriginId;
        _request.Year = year;
        _request.Month = month;
        DataTable data;
        GridView gv;
        if (OriginId != Guid.Empty)
        {
            WebReferenceReports.ResultOfListOfDistributorBillingReportIndividualRow result = null;
            try
            {
                result = report.DistributorBillingReportIndividual(_request);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, siteSetting);
                Update_Faild();
                return;
            }
            if (result.Type == WebReferenceReports.eResultType.Failure)
            {
                Update_Faild();
                return;
            }
            data = GetDataTable.GetDataTableFromListDateTimeFormat(result.Value, DATE_FORMAT);
            gv = _GridView;
            _GridViewAll.Visible = false;
            _GridView.Visible = true;
            div_NumRequests.Visible = false;
            div_TotalPayment.Visible = false;
           
        }
        else
        {
            WebReferenceReports.ResultOfListOfDistributorsReportSummaryRow result = null;
            try
            {
                result = report.DistributorBillingReportSummary(_request);
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc, siteSetting);
                Update_Faild();
                return;
            }
            if (result.Type == WebReferenceReports.eResultType.Failure)
            {
                Update_Faild();
                return;
            }
            data = new DataTable();
            data.Columns.Add("OriginName");
            data.Columns.Add("PaymentAmount");
            data.Columns.Add("RequestCount");
            data.Columns.Add("RevenueShare");

            foreach (WebReferenceReports.DistributorsReportSummaryRow drsr in result.Value)
            {
                DataRow row = data.NewRow();
                row["OriginName"] = drsr.OriginName;
                row["PaymentAmount"] = String.Format("{0:0.##}", drsr.PaymentAmount);
                row["RequestCount"] = drsr.RequestCount;
                row["RevenueShare"] = String.Format("{0:0.##}", drsr.RevenueShare) + "%";
                data.Rows.Add(row);
            }

         //   data = GetDataTable.GetDataTableFromListCorrectNumber(result.Value);
            gv = _GridViewAll;
            _GridViewAll.Visible = true;
            _GridView.Visible = false;
            lbl_NumRequests.Text = result.Value.Sum(x => x.RequestCount).ToString();
            lblTotalPaymentAmount.Text = siteSetting.CurrencySymbol + result.Value.Sum(c => c.PaymentAmount).ToString("#,0.##");
            div_NumRequests.Visible = true;
            div_TotalPayment.Visible = true;
        }
         
        
        dataV = data;
        TablePaging1.InitialPages(data.Rows.Count);
        LoadPageData(data, 1, TablePaging1.ItemInPage, gv);
    }
    void LoadPageData(DataTable data, int PageIndex, int ItemsInPage, GridView gv)
    {
        PageIndex = PageIndex - 1;
        int start = PageIndex * ItemsInPage;
        int end  = start + ItemsInPage;
        IEnumerable<DataRow> query = from x in data.AsEnumerable()
                                     where data.Rows.IndexOf(x) >= start && data.Rows.IndexOf(x) < end
                                     select x;

        gv.DataSource = (query.Count() == 0) ? null : query.CopyToDataTable();
        gv.DataBind();
        TablePaging1.LoadPages();
        _UpdatePanel.Update();            

    }
    DataTable dataV
    {
        get { return (Session["data"] == null) ? null : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
}