﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" 
EnableEventValidation="false"
AutoEventWireup="true" CodeFile="Affiliates.aspx.cs" Inherits="Publisher_Affiliates" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>
<%@ MasterType VirtualPath="~/Publisher/MasterPagePublisher.master"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<script type="text/javascript">
     window.onload = appl_init;
     function appl_init() {
         var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
         pgRegMgr.add_beginRequest(BeginHandler);
         pgRegMgr.add_endRequest(EndHandler);
         /*
         var rx = document.getElementById("<%# RequiredFieldValidator_Password.ClientID %>");
         for (att in rx) {
            if(att.indexOf("dis") != -1)
            {
             alert("attribute name= " + att);
             alert("attribute data= " + rx[att]);
             }
         }
         */
     //    var rx = new RegExp('^[0-9]+$');
     }
     function BeginHandler() {
         showDiv();
     }
     function EndHandler() {
         hideDiv();
     }

     function closePopup() {
         document.getElementById("<%# popUpadd.ClientID %>").className = "popModal_del popModal_del_hide";
         ClearPopUp();
         $find('_modal').hide();
     }
     function ClearPopUp() {

         document.getElementById("<%# txt_ID.ClientID %>").value = "";
         document.getElementById("<%# txt_Name.ClientID %>").value = "";
         document.getElementById("<%# txt_ContactPerson.ClientID %>").value = "";
         document.getElementById("<%# hf_guid.ClientID %>").value = "";
         document.getElementById("<%# txt_PhoneNumber.ClientID %>").value = "";
         document.getElementById("<%# txt_Email.ClientID %>").value = "";
         document.getElementById("<%# txt_Password.ClientID %>").value = "";
         document.getElementById("<%# ddl_Origin.ClientID %>").selectIndex = 0;

         document.getElementById("<%# txt_Password.ClientID %>").removeAttribute("readonly");
  //       var rx = document.getElementById("<%# RequiredFieldValidator_Password.ClientID %>");
 //        rx.disabled = false;

         var validators = Page_Validators;
         for (var i = validators.length - 1; i > -1; i--) {
             validators[i].style.display = "none";
         }

     }
     function openPopup() {
         document.getElementById("<%# popUpadd.ClientID %>").className = "popModal_del";
         ClearPopUp();
         document.getElementById("<%# txt_Password.ClientID %>").setAttribute("readonly", "readonly");
         var _title = document.getElementById("<%# lbl_PopupTitle.ClientID %>");
         _title.innerHTML = "<%# GetTitleNew %>";
         var rx = document.getElementById("<%# RequiredFieldValidator_Password.ClientID %>");
         rx.dispose();
         $find('_modal').show();
     }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>
<uc1:Toolbox ID="Toolbox1" runat="server" />

<h5>
    <asp:Label ID="lblSubTitle" runat="server" Text="Affiliates"></asp:Label>
</h5>
<div class="page-content minisite-content5">			
    <div id="form-analyticsseg">
        <asp:UpdatePanel ID="_updatePanelTable" runat="server" UpdateMode="Conditional">
	    <ContentTemplate>
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" ShowFooter="true" 
                CssClass="data-table"
                AllowPaging="True" 
                    onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                 <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                <FooterStyle CssClass="footer"  />                
                <Columns>                     
                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label01" runat="server" Text="<%# lbl_all.Text %>"></asp:Label>
                        <br />
                        <asp:CheckBox ID="cb_all" runat="server" >
                        </asp:CheckBox>                    
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="cb_choose" runat="server" />                    
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField Visible="false">              
                    <HeaderTemplate>
                        <asp:Label ID="_lbl_ID" runat="server" Text="<%# lbl_id.Text %>"></asp:Label>                                        
                        
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl__AccountNumber" runat="server" Text="<%# Bind('AccountNumber') %>"></asp:Label>         
                        <asp:Label ID="lbl_UserId" runat="server" Text="<%# Bind('AccountId') %>" Visible="false"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label03" runat="server" Text="<%# lbl_Name.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl__Name" runat="server" Text="<%# Bind('Name') %>"></asp:Label>         
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label04" runat="server" Text="<%# lbl_ContactPerson.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl__ContactName" runat="server" Text="<%# Bind('ContactName') %>"></asp:Label>         
                    </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label05" runat="server" Text="<%# lbl_PhoneNumber.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl__PhoneNumber" runat="server" Text="<%# Bind('PhoneNumber') %>"></asp:Label>         
                    </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label06" runat="server" Text="<%# lbl_Email.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl__Email" runat="server" Text="<%# Bind('Email') %>"></asp:Label>         
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label07" runat="server" Text="<%# lbl_Password.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbl__Password" runat="server" Text="<%# Bind('Password') %>"></asp:Label>         
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label08" runat="server" Text="<%# lbl_OriginName.Text %>"></asp:Label> 
                                                       
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text="<%# Bind('OriginName') %>"></asp:Label> 
                        <asp:Label ID="lbl_OriginId" runat="server"  Text="<%# Bind('OriginId') %>" Visible="false"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<div id="popUpadd" runat="server" sclass="content" style="max-height:450px;display:none;" class="popModal_del popModal_del_hide"  >
    <div class="top"></div>
    <a id="a_close" runat="server" class="span_A" href="javascript:closePopup();"></a>
        <div class="content" >
            <div class="modaltit">
                <h2><asp:Label ID="lbl_PopupTitle" runat="server"></asp:Label></h2>
            </div>
            <div>
    	    <center>    	    
                <table>
    	            <tr>
    	            <td>
                        <asp:Label ID="lblid" runat="server" CssClass="label" Text="<%# lbl_id.Text %>"></asp:Label>   	        
    	           
    	                <asp:TextBox ID="txt_ID" CssClass="form-text" runat="server" ReadOnly="true"></asp:TextBox>
                        <asp:HiddenField ID="hf_guid" runat="server" />
    	            </td>                
    	            </tr> 

                    <tr>
    	            <td>
                        <asp:Label ID="lblName" runat="server"  CssClass="label" Text="<%# lbl_Name.Text %>"></asp:Label>   	        
    	           
    	                <asp:TextBox ID="txt_Name" CssClass="form-text" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator_Name" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
                         Display="Dynamic" ValidationGroup="AffiliateEdit" ControlToValidate="txt_Name" 
                         CssClass="error-msg"></asp:RequiredFieldValidator>
    	            </td>                
    	            </tr> 
                    

                    <tr>
    	            <td>
                        <asp:Label ID="lblContactPerson" runat="server"  CssClass="label" Text="<%# lbl_ContactPerson.Text %>"></asp:Label>   	        
    	            
                        <asp:TextBox ID="txt_ContactPerson"  CssClass="form-text" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator_ContactPerson" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
                         Display="Dynamic" ValidationGroup="AffiliateEdit" ControlToValidate="txt_ContactPerson" 
                         CssClass="error-msg"></asp:RequiredFieldValidator>
    	            </td>                
    	            </tr> 

                    <tr>
    	            <td>
                        <asp:Label ID="lblPhoneNumber" runat="server"  CssClass="label" Text="<%# lbl_PhoneNumber.Text %>"></asp:Label>   	        
                        <asp:TextBox ID="txt_PhoneNumber"  CssClass="form-text" runat="server"></asp:TextBox>
    	                <asp:RequiredFieldValidator ID="RequiredFieldValidator_PhoneNumber" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
                         Display="Dynamic" ValidationGroup="AffiliateEdit" ControlToValidate="txt_PhoneNumber" 
                         CssClass="error-msg"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator_PhoneNumber" runat="server" ErrorMessage="Invalid phone number"
                         ControlToValidate="txt_PhoneNumber" Display="Dynamic" ValidationGroup="AffiliateEdit" CssClass="error-msg"
                         ValidationExpression="^[0-9]+$">
                        </asp:RegularExpressionValidator>
    	                
    	            </td>                
    	            </tr> 

                    <tr>
    	            <td>
                        <asp:Label ID="lblEmail" runat="server"  CssClass="label" Text="<%# lbl_Email.Text %>"></asp:Label>   	        
                        <asp:TextBox ID="txt_Email"  CssClass="form-text" runat="server"></asp:TextBox>
    	                <asp:RequiredFieldValidator ID="RequiredFieldValidator_Email" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
                         Display="Dynamic" ValidationGroup="AffiliateEdit" ControlToValidate="txt_Email" 
                         CssClass="error-msg"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator_Email" 
                            runat="server" ErrorMessage="Invalid email"
                         ControlToValidate="txt_Email" Display="Dynamic" 
                            ValidationGroup="AffiliateEdit" CssClass="error-msg"
                         ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    	                
    	            </td>                
    	            </tr> 

                     <tr>
    	            <td>
                        <asp:Label ID="lblPassword" runat="server"  CssClass="label" Text="<%# lbl_Password.Text %>"></asp:Label>   	        
                        <asp:TextBox ID="txt_Password"  CssClass="form-text" runat="server"></asp:TextBox>
    	                <asp:RequiredFieldValidator ID="RequiredFieldValidator_Password" runat="server" ErrorMessage="<%# lbl_Missing.Text %>"
                         Display="Dynamic" ValidationGroup="AffiliateEdit" ControlToValidate="txt_Password" 
                         CssClass="error-msg"></asp:RequiredFieldValidator>                        
    	                
    	            </td>                
    	            </tr> 

                    <tr>
    	            <td>
                        <asp:Label ID="lblOrigin" runat="server"  CssClass="label" Text="<%# lbl_OriginName.Text %>"></asp:Label>   	        
                        <asp:DropDownList ID="ddl_Origin" CssClass="form-select" runat="server">
                        </asp:DropDownList>            
    	                
    	            </td>                
    	            </tr> 
                </table>
            </center>
            </div>  	    
            <div class="content2">
	            <table class="Table_Button">
                <tr>
                <td>
                    <div class="unavailabilty2">
                    <asp:Button ID="btn_Set" runat="server" Text="Set" ValidationGroup="AffiliateEdit"
                     OnClick="btn_Set_Click" CssClass="btn2" />
        
               
                    <input id="btn_cancel" type="button" class="btn" value="Cancel" runat="server" onclick="javascript:closePopup();"/>
                    </div>
                </td>
                </tr>
                </table>
	        </div>
	    </div>
	<div class="bottom2"></div>        	
</div>

<cc1:ModalPopupExtender ID="_mpe" runat="server"
    TargetControlID="btn_virtual"
    PopupControlID="popUpadd"
    BackgroundCssClass="modalBackground" 
              
    BehaviorID="_modal"               
    DropShadow="false">
</cc1:ModalPopupExtender>

<div style="display:none;">
    <asp:Button ID="btn_virtual" runat="server" style="display:none;"  />                     
</div>

<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>
<asp:Label ID="lbl_id" runat="server" Text="ID" Visible="false"></asp:Label>
<asp:Label ID="lbl_Name" runat="server" Text="Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_ContactPerson" runat="server" Text="Contact person" Visible="false"></asp:Label>
<asp:Label ID="lbl_PhoneNumber" runat="server" Text="Phone number" Visible="false"></asp:Label>
<asp:Label ID="lbl_Email" runat="server" Text="Email" Visible="false"></asp:Label>
<asp:Label ID="lbl_Password" runat="server" Text="Password" Visible="false"></asp:Label>
<asp:Label ID="lbl_OriginName" runat="server" Text="Origin name" Visible="false"></asp:Label>

<asp:Label ID="lbl_Delete" runat="server" Text="This values won't be available for new updates. In all the records that used this values it will still" Visible="false"></asp:Label>
<asp:Label ID="lbl_Missing" runat="server" Text="Missing" Visible="false"></asp:Label>

<asp:Label ID="lbl_TitelEdit" runat="server" Text="Edit affiliate" Visible="false"></asp:Label>
<asp:Label ID="lbl_TitelNew" runat="server" Text="New affiliate" Visible="false"></asp:Label>


</asp:Content>

