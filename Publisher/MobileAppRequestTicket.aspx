﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MobileAppRequestTicket.aspx.cs" Inherits="Publisher_MobileAppRequestTicket" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/Notes.ascx" tagname="Notes" tagprefix="uc1" %>
<%@ Register src="~/Publisher/ClipCallControls/RelaunchControl.ascx" tagname="RelaunchControl" tagprefix="uc2" %>
<%@ Register src="~/Publisher/ClipCallControls/ZillowControl.ascx" tagname="Zillow" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link type="text/css" rel="stylesheet" href="Style.css" /> 
    <link href="../AutoCompleteCity/StyleAutoComplete.css" rel="stylesheet" type="text/css"/>
    <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>  
    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript"  src="../CallService.js"></script>
    <script  type="text/javascript" src="../AutoCompleteCity/AutoCompleteCity.js"></script>
     <script src="localTimeDisplay.js" type="text/javascript"></script>   
    <script type="text/javascript">
    window.onload = appl_init;
    
    function appl_init() {        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
        hideDiv();
        try{
            parent.ShowIframeDiv();
        }
        catch(ex){}
        
    }
    function BeginHandler()
    {     
 //       alert('in');  
        showDiv();
    }
    function EndHandler()
    {
//        alert('out');  
        hideDiv();
    }
    function OpenDetails()
    {
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Notes.ClientID %>").style.display = "none";
        document.getElementById("<%# div_RelaunchStopLog.ClientID %>").style.display = "none";        
        document.getElementById("<%# div_Details.ClientID %>").style.display = "block";
        document.getElementById("<%# div_aar.ClientID %>").style.display = "none";   
        document.getElementById("<%# div_zillow.ClientID %>").style.display = "none";

        if(is_explorer)
        {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_RelaunchStopLog.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_aar.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_zillow.ClientID %>").removeAttribute("className");
        }
        else
        {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_RelaunchStopLog.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_aar.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_zillow.ClientID %>").removeAttribute("class");
        }
        document.getElementById("<%# a_OpenDetails.ClientID %>").className = "selected";
    }
    function OpenNotes()
    {
        var div_Notes = document.getElementById("<%# div_Notes.ClientID %>");
        if(div_Notes.style.display == "block")
            return false;
        var div_RelaunchStopLog = document.getElementById("<%# div_RelaunchStopLog.ClientID %>");
        //if(div_affiliate)
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Details.ClientID %>").style.display = "none";
        document.getElementById("<%# div_aar.ClientID %>").style.display = "none";   
        document.getElementById("<%# div_zillow.ClientID %>").style.display = "none";
        div_RelaunchStopLog.style.display = "none";
        div_Notes.style.display = "block";
        if(is_explorer)
        {
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_RelaunchStopLog.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_aar.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_zillow.ClientID %>").removeAttribute("className");
        }
        else
        {
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("class");        
            document.getElementById("<%# lb_RelaunchStopLog.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_aar.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_zillow.ClientID %>").removeAttribute("class");
        }
        document.getElementById("<%# lb_OpenNotes.ClientID %>").className = "selected";
        showDiv();
        return true;
    }
    function OpenRelaunchLog()
    {
        var div_RelaunchStopLog = document.getElementById("<%# div_RelaunchStopLog.ClientID %>");
        if (div_RelaunchStopLog.style.display == "block")
            return false;
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Notes.ClientID %>").style.display = "none";
        document.getElementById("<%# div_RelaunchStopLog.ClientID %>").style.display = "block";
        document.getElementById("<%# div_Details.ClientID %>").style.display = "none";
        document.getElementById("<%# div_aar.ClientID %>").style.display = "none";   
        document.getElementById("<%# div_zillow.ClientID %>").style.display = "none";
        if(is_explorer)
        {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("className");
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_aar.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_zillow.ClientID %>").removeAttribute("className");
        }
        else
        {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("class");
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_aar.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_zillow.ClientID %>").removeAttribute("class");
        }
        document.getElementById("<%# lb_RelaunchStopLog.ClientID %>").className = "selected";
        return false;
    }
    function OpenAAR()
    {
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Notes.ClientID %>").style.display = "none";
        document.getElementById("<%# div_RelaunchStopLog.ClientID %>").style.display = "none";
        document.getElementById("<%# div_Details.ClientID %>").style.display = "none";
        document.getElementById("<%# div_zillow.ClientID %>").style.display = "none";
        document.getElementById("<%# div_aar.ClientID %>").style.display = "block";

        if (is_explorer) {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_RelaunchStopLog.ClientID %>").removeAttribute("className");
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_zillow.ClientID %>").removeAttribute("className");
        }
        else {
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_RelaunchStopLog.ClientID %>").removeAttribute("class");
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_zillow.ClientID %>").removeAttribute("class");
        }
        document.getElementById("<%# lb_aar.ClientID %>").className = "selected";
        return false;
    }
    function OpenZillow() {
        var div_zillow = document.getElementById("<%# div_zillow.ClientID %>");
        if (div_zillow.style.display == "block")
            return false;
        var is_explorer = navigator.userAgent.toLowerCase().indexOf('msie 7') > -1;
        document.getElementById("<%# div_Details.ClientID %>").style.display = "none";
        document.getElementById("<%# div_aar.ClientID %>").style.display = "none";
        document.getElementById("<%# div_RelaunchStopLog.ClientID %>").style.display = "none";
        document.getElementById("<%# div_Notes.ClientID %>").style.display = "none";
        div_zillow.style.display = "block";
        if (is_explorer) {
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_RelaunchStopLog.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_aar.ClientID %>").removeAttribute("className");
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("className");
        }
        else {
            document.getElementById("<%# a_OpenDetails.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_RelaunchStopLog.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_aar.ClientID %>").removeAttribute("class");
            document.getElementById("<%# lb_OpenNotes.ClientID %>").removeAttribute("class");
        }
        document.getElementById("<%# lb_zillow.ClientID %>").className = "selected";
        if (document.getElementById("<%# hf_zillowloaded.ClientID %>").value == "1")
            return false;

        showDiv();
        return true;
    }
    function CloseMeError()
    {
        alert("<%# lbl_ErrorLoadData.Text %>");
        //parent.CloseIframe()
        window.close();
    }
    function AreUSure() {
        return confirm("<%# AreUSure %>");
    }
    /*
    function OpenRecord(_id) {
        $.ajax({
            url: "<# RecordService %>",
            data: "{'_id': '" + _id + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var arg = data.d;
                if (arg.length == 0) {
                    alert(document.getElementById("<# hf_errorRecord.ClientID %>").value);
                    return;
                }

                var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
                if (is_chrome) {
                    oWin = window.open('<# GetAudioHTML5 %>?audio=' + arg, "recordWin", "resizable=0,width=500,height=330");
                    setTimeout("CheckPopupBlocker()", 2000);

                }
                else
                    window.location.href = arg;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                GeneralServerError();
            }

        });
    }
    */
    function OpenRecord(_id) {
        var oWin;
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('msie') > -1;
            if (is_chrome) {
                oWin = window.open('<%# GetMediaPlayer %>?audio=' + _id, "recordWin", "resizable=0,width=200,height=200");
    //            setTimeout("CheckPopupBlocker()", 2000);

            }
            else
               // window.location.href = arg;
                oWin = window.open('<%# GetAudioHTML5 %>?audio=' + _id, "recordWin", "resizable=0,width=400,height=250");

        }
        function CloseReloadWin() {
            window.opener.Reload();
            window.close();
        }
        function DownloadVideo(str) {
            var oWin = window.open('<%# GetDownloadVideo %>?str=' + str, "downloadVideo", "resizable=0,width=200,height=200");
        }
        function send_aar(_this, incidentId, aarCallId, aarIncidentId) {
            var $this = $(_this);
            $this.hide();
            var $Sendaar_loader = $this.parent().find(".div_Sendaar_loader");
            $Sendaar_loader.show();
            var sendaar_result = $this.parent().find(".sendaar_result");
            var _data = '{ "incidentId": "' + incidentId + '", "aarCallId": "' + aarCallId + '", "aarIncidentId": "' + aarIncidentId + '"}';
            $.ajax({
                url: '<%# SendAarAgainService %>',
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    $Sendaar_loader.hide();
                    sendaar_result.show();
                    sendaar_result.html((data.d == "true") ? "Success" : "Failed");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest);
                }
            });
            return false;
        }
        function send_sms(_this, incidentAccountId) {
            var $this = $(_this);
            $this.hide();
            var $Sendaar_loader = $this.parent().find(".div_Sendsms_loader");
            $Sendaar_loader.show();
            var sendaar_result = $this.parent().find(".sendsms_result");
            var _data = '{ "incidentAccountId": "' + incidentAccountId + '"}';
            $.ajax({
                url: '<%# SendSmsReminderService %>',
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    $Sendaar_loader.hide();
                    sendaar_result.show();
                    sendaar_result.html((data.d == "true") ? "Success" : "Failed");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest);
                }
            });
            return false;
        }
        function unblock(_a, supplierId) {
            $(_a).hide();
            var $div_parent = $(_a).parent('td');
            $div_parent.find('div.div_unblock_loader').show();
            $.ajax({
                url: "<%# SendUnblockSmsService %>",
                data: "{ 'supplierId': '" + supplierId + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    $div_parent.find('span.unblock_result').show();
                    $div_parent.find('span.unblock_result').html(_data.IsSucceeded ? 'Success' : _data.FailedReason);
                    if (_data.IsSucceeded)
                        TurnedOnSms($div_parent);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    $div_parent.find('div.div_unblock_loader').hide();
                }

            });
            return false;
        }
        function TurnedOnSms($div_unblock) {
            var $tr = $div_unblock.parent('tr');
            var incidentAccountId = $tr.find("input[type='hidden']").val();
            var $lb_sendSms = $tr.find('.lb_sendSms');
      //      $lb_sendSms.prop('onclick', null).off('click');
         //   $lb_sendSms.prop('onclick', "return send_sms(this, '" + incidentAccountId + "');");
            $lb_sendSms.get(0).setAttribute('onclick', "return send_sms(this, '" + incidentAccountId + "');");
            $lb_sendSms.html('Send');
        }
        function OpenIframe(_path) {
            var _leadWin = window.open(_path, "TraceDetails", "width=1200, height=650,scrollbars=1");
            _leadWin.focus();
        }


        function relaunch_reject_lead(_this, incidentAccountId) {
            var $this = $(_this);
            $this.hide();
            var $Sendaar_loader = $this.parent().find(".div_RelaunchRejectLead_loader");
            $Sendaar_loader.show();
            var sendaar_result = $this.parent().find(".RelaunchRejectLead_result");
            var _data = '{ "incidentAccountId": "' + incidentAccountId + '"}';
            $.ajax({
                url: '<%# RelaunchRejectLeadervice %>',
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    $Sendaar_loader.hide();
                    sendaar_result.show();
                    sendaar_result.html(data.d);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest);
                }
            });
            return false;
        }
         function remind_me_later(_this, incidentAccountId) {
     //       var $this = $(_this);
    //        $this.hide();
   //         var $Sendaar_loader = $this.parent().find(".div_RelaunchRejectLead_loader");
    //        $Sendaar_loader.show();
             //        var sendaar_result = $this.parent().find(".RelaunchRejectLead_result");
             showDiv();
            var _data = '{ "incidentAccountId": "' + incidentAccountId + '"}';
            $.ajax({
                url: '<%# UpdateRemindMeService %>',
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    var res = eval('(' + data.d + ')');
                    if (!res.isSucceeded)
                        alert('Update remind me failed');
                    else {
                        _this.src = res.image;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest);
                },
                complete: function (jqXHR, textStatus) {
                    hideDiv();
                }
            });
            return false;
        }
       
    </script>
</head>
<body style="background-color:#EDF0F4;overflow-x:hidden;overflow-y:hidden;">
     <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    <Services> 
        <asp:ServiceReference Path="AutoComplete.asmx" />         
        <asp:ServiceReference Path="~/Publisher/Auction/Auction.asmx" /> 
    </Services> 
     </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="_UpdatePanel_RequestStop" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
   
    <div runat="server" id="div_relaunch" class="upsalebtn  div_relaunch">
                <asp:LinkButton ID="lb_relaunch" runat="server" CssClass="StopRequest">RELAUNCH</asp:LinkButton>
            </div>
             <div runat="server" id="div_Stop" class="upsalebtn div_Stop">
                 <asp:LinkButton ID="lb_StopRequest" runat="server" CssClass="StopRequest"
                     onclick="lb_StopRequest_Click" OnClientClick="return AreUSure();">STOP REQUEST</asp:LinkButton>
            </div>
        <%/* 
            <div runat="server" id="div_requestStpped" class="upsalebtn">
                <asp:Label ID="lbl_requestStpped" runat="server" Text="Request manually stopped" CssClass="RequestManuallyStopped"></asp:Label>
            </div>
            */%>
             </ContentTemplate>
    </asp:UpdatePanel>
    
     <div class="titlerequestTop">          
          <a id="a_projectNumber" runat="server">
			    <asp:Label ID="lbl_CurrentRequest_Title" runat="server" Text="Current Request Ticket"></asp:Label>:
                <asp:Label ID="lbl_RequestNum" runat="server"></asp:Label>
			</a>   
        
    </div>
     
    
    
    <div class="clear"></div>
    <div class="separator"></div>
    <div id="form-analyticscomp2" style="width:1400px;">

    <div class="tabswrap2" style="display:block;" >
        <div id="Ticket_Tabs" class="tabs2" runat="server"><!-- here the red bar-->
          
            <ul id="ulTabs" runat="server">
                <li><a href="javascript:OpenDetails();" runat="server" id="a_OpenDetails" class="selected">Details</a></li>
                <li><asp:LinkButton ID="lb_OpenNotes" runat="server" Text="Notes" 
                    OnClientClick="return OpenNotes();" onclick="lb_OpenNotes_Click"></asp:LinkButton></li>
                <li><asp:LinkButton ID="lb_RelaunchStopLog" runat="server" OnClientClick="return OpenRelaunchLog();"
                 Text="Relaunch\Stop log"></asp:LinkButton></li>
                <li><asp:LinkButton ID="lb_aar" runat="server" OnClientClick="return OpenAAR();"
                 Text="AAR"></asp:LinkButton></li>

                <li><asp:LinkButton ID="lb_zillow" runat="server" Text="Zillow Real Estate" 
                    OnClientClick="return OpenZillow();" onclick="lb_OpenZillow_Click"></asp:LinkButton></li>
            </ul>
            
        </div>
        <asp:HiddenField ID="hf_zillowloaded" runat="server" />
        <div class="containertab2" style="position:relative;">
         <%// div details %>
            <div runat="server" id="div_Details">
                <div style="width:800px; float:left;">
                   <div class="form-fieldlong">
                        <asp:Label ID="lbl_Title" runat="server" CssClass="label" Text="Title"></asp:Label>
                        <asp:TextBox ID="txt_Title" runat="server" CssClass="form-textMed label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-fieldshort">
                        <asp:Label ID="lbl_Heading" runat="server" CssClass="label" Text="Heading"></asp:Label>
                        <asp:TextBox ID="txt_Heading" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                   <div class="form-fieldshort">
                        <asp:Label ID="lbl_Region" runat="server" CssClass="label" Text="Region"></asp:Label>
                        <asp:TextBox ID="txt_Region" runat="server" CssClass="form-textshort label_region" ReadOnly="true"></asp:TextBox>
                    </div>

                
                    <div class="form-fieldlongdet">
                        <asp:Label ID="lbl_VideoUrl" runat="server" CssClass="label" Text="Video link"></asp:Label>
                        <asp:HyperLink ID="hl_VideoUrl" runat="server" Target="_blank" style="overflow-wrap: break-word;"></asp:HyperLink>
                    </div>
                    <div class="form-fieldshort">
                        <asp:Label ID="lbl_CreatedOn" runat="server" CssClass="label" Text="Created on"></asp:Label>
                        <asp:TextBox ID="txt_CreatedOn" runat="server" CssClass="form-textshort label_ro date-time-utc" ReadOnly="true"></asp:TextBox>
                    </div>
                
                     <div class="form-fieldshort">
                        <asp:Label ID="lbl_Consumer" runat="server" CssClass="label" Text="Consumer"></asp:Label>
                        <asp:TextBox ID="txt_Consumer" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                 
                     <div class="form-fieldshort">
                        <asp:Label ID="lbl_RelaunchDate" runat="server" CssClass="label" Text="Relaunch date"></asp:Label>
                        <asp:TextBox ID="txt_RelaunchDate" runat="server" CssClass="form-textshort label_ro date-time-utc" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-fieldshort">
                         <asp:Label ID="lbl_ProvidedAdvertisers" runat="server" CssClass="label" Text="Provided advertisers"></asp:Label>
                        <asp:TextBox ID="txt_ProvidedAdvertisers" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                      <div class="form-fieldshort">
                        <asp:Label ID="lbl_RequestedAdveritsers" runat="server" CssClass="label" Text="Requested advertisers" style="font-size:11px;"></asp:Label>
                        <asp:TextBox ID="txt_RequestedAdveritsers" runat="server"  CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
 
                      <div class="form-fieldshort">
                        <asp:Label ID="lbl_IncidentStatus" runat="server" CssClass="label" Text="Request status"></asp:Label>
                        <asp:TextBox ID="txt_IncidentStatus" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                
                  
               
                     <div class="form-fieldshort">
                   
                    </div>
                    <div class="form-fieldshort">
                    
                    </div>
                  
                    <div class="form-fieldshort">
                        <asp:Label ID="lbl_ReviewStatus" runat="server" CssClass="label" Text="Review status"></asp:Label>
                        <asp:TextBox ID="txt_ReviewStatus" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                     <div class="form-fieldshort">
                        <asp:Label ID="lbl_fullAddress" runat="server" CssClass="label" Text="Full address"></asp:Label>
                        <asp:TextBox ID="txt_fullAddres" runat="server" CssClass="form-textshort label_region" ReadOnly="true"></asp:TextBox>
                    </div> 
                    <div class="form-fieldshort">
                   
                    </div>
                     <div class="form-fieldshort">
                    
                    </div>
                    <div class="form-fieldshort">
                    
                    </div>
                    <div class="form-fieldshort">
                         <asp:Label ID="lbl_incidentType" runat="server" CssClass="label" Text="Type"></asp:Label>
                        <asp:TextBox ID="txt_incidentType" runat="server" CssClass="form-textshort label_ro" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-fieldshort">
                        <asp:Label ID="lbl_CensoredAddress" runat="server" CssClass="label" Text="Censored address"></asp:Label>
                        <asp:TextBox ID="txt_CensoredAddress" runat="server" CssClass="form-textshort label_region" ReadOnly="true"></asp:TextBox>
                    </div>
                    <%/* 
                    <div class="form-fieldshort">
                    
                    </div>
     */ %>
                </div>
                <% /*!-- 
                <div style="float:left; border:solid;">
                    <div class="form-fieldshort">
                        <asp:Label ID="Label14" runat="server" CssClass="label" Text="Review status"></asp:Label>
                        <asp:TextBox ID="TextBox1" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text="blabla"></asp:TextBox>
                    </div>
                    </div>
                    --% */%>
                <div class="clear"></div>
                <div style="overflow-y:auto; max-height:280px;">
                    <asp:GridView ID="_GridView"  Width="779px" CssClass="data-table2" runat="server" AutoGenerateColumns="false">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                    <Columns>
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="Advertiser name"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                             <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Bind("AdvertiserName") %>' Target="_blank" NavigateUrl='<%# Bind("SupplierRequestReport") %>'></asp:HyperLink>                            
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label201" runat="server" Text="Last Active"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label202" runat="server" Text='<%# Bind("LastActive") %>' CssClass="date-time-utc"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="AdvertiserPhone"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink2" runat="server" Text='<%# Bind("AdvertiserPhone") %>' Target="_blank" NavigateUrl='<%# Bind("SupplierRequestReport") %>'></asp:HyperLink>                       
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label80" runat="server" Text="Advertiser Status"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label81" runat="server" Text='<%# Bind("AdvertiserStatus") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label080" runat="server" Text="Inactive Reason"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label081" runat="server" Text='<%# Bind("InactiveReason") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label50" runat="server" Text="Confirmed policy"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image51" runat="server" ImageUrl="<%# Get_VIcon_Url %>" Visible='<%# Bind("IsConfirmed") %>' />
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="Status"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label205" runat="server" Text="Rejected Reason"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label206" runat="server" Text='<%# Bind("RejectedReason") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label29" runat="server" Text="Won"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image10" runat="server" ImageUrl='<%# Bind("Won") %>' Visible='<%# Bind("IsWon") %>' />
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="Call duration"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text='<%# Bind("CallRecordDuration") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label9" runat="server" Text="Call date"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label10" runat="server" Text='<%# Bind("Date") %>' CssClass="date-time-utc"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label11" runat="server" Text="Has mobile app"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label12" runat="server" Text='<%# Bind("HasMobileApp") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label13" runat="server" Text="Call record"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="hl_callrecord" runat="server" Text="Call record" Visible='<%# Bind("HasRecord") %>'
                                 NavigateUrl='<%# Bind("CallRecord") %>' ></asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label53" runat="server" Text="Policy confirmed/reject"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image54" runat="server" ImageUrl='<%# Bind("ConfirmedThisLead") %>' Visible='<%# Bind("IsConfirmedThisLead") %>' />
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label55" runat="server" Text="Send sms"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lb_56" runat="server" Text='<%# Bind("SendSmsText") %>' Visible='<%# Bind("CanSendSms") %>' 
                                OnClientClick='<%# Bind("SendSms") %>' CssClass="lb_sendSms"></asp:LinkButton>
                            <div style="text-align:center; display:none;" class="div_Sendsms_loader">
                                <asp:Image ID="img_SendSms_loader" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                            </div>
                            <asp:Label ID="Label1544" runat="server" CssClass="error-msg sendsms_result" style="display:none;"></asp:Label>
                            <asp:HiddenField ID="hf_incidentAccountId" runat="server" Value='<%# Bind("IncidentAccountId") %>' />
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label253" runat="server" Text="Proximity"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label212" runat="server" Text='<%# Bind("Proximity") %>' CssClass="date-time-utc"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2911" runat="server" Text="Watched video"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image2990" runat="server" ImageUrl='<%# Bind("WatchedVideo") %>' />
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2961" runat="server" Text="Remind me later"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="imb_remind" runat="server" ImageUrl='<%# Bind("ImageRemindMe") %>' OnClientClick='<%# Bind("RemindMeFunc") %>' />
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label258" runat="server" Text="Rating"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label259" runat="server" Text='<%# Bind("ProRating") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label278" runat="server" Text="Unblock phone"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                             <asp:LinkButton ID="lb_unblock" runat="server" Text="Unblock" Visible='<%# Bind("CanUnblock") %>' OnClientClick='<%# Bind("UnblockScript") %>'></asp:LinkButton>
                            <div style="text-align:center; display:none;" class="div_unblock_loader">
                                <asp:Image ID="img_unblock_loader" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                            </div>
                            <asp:Label ID="Label1574" runat="server" CssClass="error-msg unblock_result" style="display:none;"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label758" runat="server" Text="TAD"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink12" runat="server" Text='<%# Bind("HasSurvey") %>' Target="_blank" NavigateUrl='<%# Bind("SurveyUrl") %>'></asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>
                       
                          <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2626" runat="server" Text="Relaunch reject lead"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lb_RelaunchRejectLead" runat="server" Text="Relaunch" Visible='<%# Bind("CanRelaunchReject") %>' OnClientClick='<%# Bind("RelaunchReject") %>'></asp:LinkButton>
                            <div style="text-align:center; display:none;" class="div_RelaunchRejectLead_loader">
                                <asp:Image ID="img_RelaunchRejectLead_loader" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                            </div>
                            <asp:Label ID="lbl_error_RelaunchRejectLead" runat="server" CssClass="error-msg RelaunchRejectLead_result" style="display:none;"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    </asp:GridView>
                </div>
            </div>
            <%// div notes %>
            <div runat="server" id="div_Notes" style="display:none; width:100%; position:relative;">
                <uc1:Notes ID="Notes1" runat="server" />
            </div>
           <%// div Relaunch log %>
            <div runat="server" id="div_RelaunchStopLog" style="display:none; width:100%; position:relative;" >
                <asp:UpdatePanel ID="_UpdatePanel_RelaunchHistory" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                   
                         <asp:GridView ID="gv_RelaunchHistory"  Width="779px" CssClass="data-table2" runat="server" AutoGenerateColumns="false">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                    <Columns>
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1000" runat="server" Text="Date"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1001" runat="server" Text='<%# Bind("Date") %>' CssClass="date-time-utc"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1002" runat="server" Text="Relaunch/Stop"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1003" runat="server" Text='<%# Bind("IsRelaunch") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1004" runat="server" Text="By"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1005" runat="server" Text='<%# Bind("By") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1007" runat="server" Text="Old Region"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1008" runat="server" Text='<%# Bind("OldRegion") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1009" runat="server" Text="New Region"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1010" runat="server" Text='<%# Bind("NewRegion") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1011" runat="server" Text="Old Category"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1012" runat="server" Text='<%# Bind("OldCategory") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1013" runat="server" Text="New Category"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1014" runat="server" Text='<%# Bind("NewCategory") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    </asp:GridView>
                  
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>

             <%// div AAR %>
            <div runat="server" id="div_aar" style="display:none; max-height:500px; overflow-y:auto; width: 840px;" >
              
                   
                         <asp:GridView ID="gv_aar"  Width="779px" CssClass="data-table2" runat="server" AutoGenerateColumns="false">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                    <Columns>
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2000" runat="server" Text="Name"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2001" runat="server" Text='<%# Bind("AdvertiserName") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2002" runat="server" Text="Phone"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2003" runat="server" Text='<%# Bind("AdvertiserPhone") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2004" runat="server" Text="Status"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2005" runat="server" Text='<%# Bind("CallStatus") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2007" runat="server" Text="Date"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2008" runat="server" Text='<%# Bind("Date") %>' CssClass="date-time-utc"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2009" runat="server" Text="Device"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2010" runat="server" Text='<%# Bind("Device") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2011" runat="server" Text="Watched video"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image2000" runat="server" ImageUrl='<%# Bind("WatchedVideo") %>' />
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2013" runat="server" Text="ViewVideoCount"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2014" runat="server" Text='<%# Bind("ViewVideoCount") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2021" runat="server" Text="View all video"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image2001" runat="server" ImageUrl='<%# Bind("ViewAllVideo") %>' />
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label20223" runat="server" Text="Call record"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="hl_20001" runat="server" NavigateUrl='<%# Bind("CallRecord") %>' Target="_blank" Visible='<%# Bind("HasRecoed") %>'>CallRecord</asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2011" runat="server" Text="Call duration"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                             <asp:Label ID="Label2024" runat="server" Text='<%# Bind("CallDuration") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2023" runat="server" Text="CallBackMessage"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2025" runat="server" Text='<%# Bind("CallBackMessage") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label2026" runat="server" Text="Send AAR"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Bind("SendAarText") %>' Visible='<%# Bind("CanSendAar") %>' OnClientClick='<%# Bind("SendAar") %>'></asp:LinkButton>
                            <div style="text-align:center; display:none;" class="div_Sendaar_loader">
                                <asp:Image ID="img_review_loader" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                            </div>
                            <asp:Label ID="Label1444" runat="server" CssClass="error-msg sendaar_result" style="display:none;"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                       
                    </Columns>
                    </asp:GridView>
                  
              
            </div>
            <%// div Zillow %>
            <div runat="server" id="div_zillow" style="display:none; width:100%; position:relative;">
                <uc1:Zillow ID="_zillow" runat="server" />
            </div>
        </div>
    </div>
    </div>
    
    <uc2:RelaunchControl ID="_relaunchControl" runat="server" YPosition="5"/>
    
    <div id="divLoader" class="divLoader">   
        <table>
            <tr>
                <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." /></td>
            </tr>
        </table>
    </div>
        <asp:Label ID="lbl_AreUSure" runat="server" Text="Are you sure you want to stop this in-progress request before fulfillment?" Visible="false"></asp:Label>
         <asp:Label ID="lbl_NoResult" runat="server" Text="There are no result" Visible="false" ></asp:Label>
     <asp:Label ID="lbl_ErrorLoadData" runat="server" Text="There was an error to load the details." Visible="false"></asp:Label>
    <asp:HiddenField ID="hf_errorRecord" runat="server" Value="There are errors with the record" />
<asp:HiddenField ID="Hidden_AllowPopUp" runat="server" Value="Allow pop-up windows" />

    <asp:HiddenField ID="hf_IncidentId" runat="server" /> 
    <asp:HiddenField ID="hf_UpsaleId" runat="server" /> 
    </form>
</body>
</html>
