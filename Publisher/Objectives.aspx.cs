﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;

public partial class Publisher_Objectives : PageSetting//, ICallbackEventHandler
{
    const string CONFIRMED_OVERRIDE = "ConfirmedOverride";
    protected string GetConfirmedPostBack
    {
        get { return ClientScript.GetPostBackEventReference(btn_virtual2, CONFIRMED_OVERRIDE); }
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
  //      SetupClient();
        ScriptManager1.RegisterAsyncPostBackControl(btn_Set);
        ScriptManager1.RegisterAsyncPostBackControl(btn_virtual2);
        
        if (!IsPostBack)
        {
            // double flo = double.MaxValue;
            RangeValidator_Objecive.MaximumValue = (int.MaxValue).ToString();
            LoadCriteria();
            LoadObjectives();
            SetToolBox();
            PopUpEditObjective.DataBind();
        }
        else
        {
            string event_argument = Request["__EVENTARGUMENT"];
            if (event_argument == CONFIRMED_OVERRIDE)
                ConfirmedUpdateObjective();
        }
        SetToolBoxEvents();
        Header.DataBind();
    }
    private void SetToolBox()
    {
        Toolbox1.RemoveAttach();
        
        string script = "openPopup(); return false;";
        Toolbox1.SetClientScriptToAdd(script);

    }

    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
  //      Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
        ScriptManager1.RegisterAsyncPostBackControl(Toolbox1.GetSearchButton);
    }

    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        bool IsSelect = false;
        int indx = -1;
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
            {
                if (IsSelect)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ChooseOne", "alert('" + Toolbox1.ChooseOne + "');", true);
                    return;
                }
                IsSelect = true;
                indx = row.RowIndex;
            }
        }
        
        if (indx > -1)
        {
            GridViewRow row = _GridView.Rows[indx];
            hf_ObjectiveId.Value = ((Label)row.FindControl("lbl_ObjectiveId")).Text;
            txt_Objecive.Text = ((Label)row.FindControl("lbl_amount")).Text;
            _FromToDatePicker.SetFromDate(((Label)row.FindControl("lblFromDate")).Text);
            _FromToDatePicker.SetToDate(((Label)row.FindControl("lblToDate")).Text);


            string criteria = ((Label)row.FindControl("lbl_CriteriaEnum")).Text;
            foreach (ListItem li in ddl_criteria.Items)
                li.Selected = (li.Value == criteria);

            PopUpEditObjective.Attributes["class"] = "popModal_del";
            _mpe.Show();
        }
         
    }
    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        List<Guid> list = new List<Guid>();
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            if (cb.Checked)
            {
                Guid _id = new Guid(((Label)row.FindControl("lbl_ObjectiveId")).Text);               
                list.Add(_id);               
            }
        }
        if (list.Count == 0)
            return;

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
     
        
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.DeleteObjectives(list.ToArray());
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        LoadObjectives();
        _Search();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataViewV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            return;
        }
        //  PrintHelper.PrintWebControl(_GridView, dataViewV);
        Session["data_print"] = dataViewV.Table;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        if (dataViewV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordExport + "');", true);
            return;
        }
        ToExcel to_excel = new ToExcel(this, "Objectives");
        if (!to_excel.ExecExcel(dataViewV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }
     
    void LoadObjectives()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfObjectiveData result = null;
        try
        {
            result = _site.GetAllObjectives();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            _UpdatePanel.Update();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            _UpdatePanel.Update();
            return;
        }
        DataTable data = new DataTable();
        data.Columns.Add("Amount");
        data.Columns.Add("Criteria");
        data.Columns.Add("CriteriaTxt");
        data.Columns.Add("FromDate");
        data.Columns.Add("FromDateOrg", typeof(DateTime));
        data.Columns.Add("ObjectiveId");
        data.Columns.Add("ToDate");
        data.Columns.Add("AmountOriginal");
        foreach (WebReferenceSite.ObjectiveData od in result.Value)
        {
            string CurrencySymbol = "";
            string percent = "";
            string _symbol = CriteriaSetting.GetSymbolCriteria(od.Criteria.ToString(), siteSetting.CurrencySymbol);
            if (_symbol == "%")
                percent = _symbol;
            else if (!string.IsNullOrEmpty(_symbol))
                CurrencySymbol = _symbol;
            DataRow row = data.NewRow();
            row["Amount"] = CurrencySymbol + string.Format("{0:0.##}", od.Amount) + percent;
            row["AmountOriginal"] = string.Format("{0:0.##}", od.Amount);
            row["CriteriaTxt"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "Criteria", od.Criteria.ToString());
            row["FromDate"] = string.Format(siteSetting.DateFormat, od.FromDate);
            row["FromDateOrg"] = od.FromDate;
            row["ObjectiveId"] = od.ObjectiveId.ToString();
            row["ToDate"] = string.Format(siteSetting.DateFormat, od.ToDate);
            row["Criteria"] = od.Criteria.ToString();
            data.Rows.Add(row);
        }
        dataV = data;
        /*
        result.Value[0].Amount;
        result.Value[0].Criteria;
        result.Value[0].FromDate;
        result.Value[0].ObjectiveId;
        result.Value[0].ToDate;
       */

    }
    private void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        _Search();
    }
    private void _Search()
    {
        string sentence = Toolbox1.GetTxt.Trim().ToLower();
        DataTable data = dataV;
        data.TableName = "Search";
        if (!string.IsNullOrEmpty(sentence))
        {
            EnumerableRowCollection<DataRow> query = from Search in data.AsEnumerable()
                                                     where ((!string.IsNullOrEmpty(Search.Field<string>("CriteriaTxt")) &&
                                                     Search.Field<string>("CriteriaTxt").ToLower().Contains(sentence)))
                                                     orderby Search.Field<string>("Criteria"), Search.Field<DateTime>("FromDateOrg")
                                                     select Search;

            dataViewV = query.AsDataView();
        }
        else
        {
            EnumerableRowCollection<DataRow> query = from Search in data.AsEnumerable()
    //                                                 orderby Search.Field<string>("Criteria")
                                                     orderby Search.Field<string>("Criteria"), Search.Field<DateTime>("FromDateOrg")
                                                     select Search;

            dataViewV = query.AsDataView();
        }
        _GridView.PageIndex = 0;
        /*
        _GridView.DataSource = dataViewV;
        _GridView.DataBind();
        _UpdatePanel.Update();
         * */
        SetGridView();
    }
    void SetGridView()
    {
        StringBuilder sb = new StringBuilder();
        DataView data = dataViewV;
        //    _GridView.PageIndex = 0;
        _GridView.DataSource = data;
        _GridView.DataBind();

        if (data.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noreasultSearch", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            CBsV = string.Empty;
            _UpdatePanel.Update();
            return;
        }
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }

        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        _UpdatePanel.Update();
    }
    private void LoadCriteria()
    {
        ddl_criteria.Items.Clear();
        foreach (string s in Enum.GetNames(typeof(WebReferenceReports.Criteria)))
        {
            string _txt = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "Criteria", s);
            ddl_criteria.Items.Add(new ListItem(_txt, s));
        }
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView();
    }
    protected void btn_Set_Click(object sender, EventArgs e)
    {
        WebReferenceSite.CreateObjectiveRequest request = new WebReferenceSite.CreateObjectiveRequest();
        
        string _criteria = ddl_criteria.SelectedValue;
        WebReferenceSite.Criteria CriteriaEnum = WebReferenceSite.Criteria.AmountOfAdvertisers;
        bool IsFind = false;
        foreach (WebReferenceSite.Criteria crit in Enum.GetValues(typeof(WebReferenceSite.Criteria)))
        {
            if (crit.ToString() == _criteria)
            {
                IsFind = true;
                CriteriaEnum = crit;
                break;
            }
        }
        if (!IsFind)
            return;
        decimal amount;
        if(!decimal.TryParse(txt_Objecive.Text, out amount))
            return;
        request.IsApproved = false;
        request.Amount = amount;
        request.Criteria = CriteriaEnum;
        request.FromDate = _FromToDatePicker.GetDateFrom;
        request.ToDate = _FromToDatePicker.GetDateTo;
        if (!string.IsNullOrEmpty(hf_ObjectiveId.Value))
            request.ObjectiveId = new Guid(hf_ObjectiveId.Value);
        UpdateObjective(request);
        
    }
    private void ConfirmedUpdateObjective()
    {
        WebReferenceSite.CreateObjectiveRequest request = requestV;
        request.IsApproved = true;
        UpdateObjective(request);
    }
    private void UpdateObjective(WebReferenceSite.CreateObjectiveRequest request)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfCreateObjectiveResponse result = null;
        try
        {
            result = _site.CreateObjective(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Value.NeedsApproval)
        {
            requestV = request;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "confirmOverride",
                @"setTimeout(""ConfirmOverride()"", 100);", true);
        }
        else
        {
            requestV = null;
            _mpe.Hide();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ClearPopup",
                "ClearPopup();", true);
            LoadObjectives();
            _Search();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        }
    }
    
    protected string GetConfirmOverride
    {
        get { return Server.HtmlEncode(lbl_ConfirmOverride.Text); }
    }
    protected string GetFromId
    {
        get { return _FromToDatePicker.GetTextBoxFrom().ClientID; }
    }
    protected string GetToId
    {
        get { return _FromToDatePicker.GetTextBoxTo().ClientID; }
    }
    protected string GetDivDatesId
    {
        get { return _FromToDatePicker.GetParentDivId(); }
    }
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    //For export to excel
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    private DataView dataViewV
    {
        get { return (Session["dataView"] == null) ? new DataView() : (DataView)Session["dataView"]; }
        set { Session["dataView"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
    WebReferenceSite.CreateObjectiveRequest requestV
    {
        get { return (ViewState["request"] == null) ? new WebReferenceSite.CreateObjectiveRequest() : 
            (WebReferenceSite.CreateObjectiveRequest)ViewState["request"]; }
        set { ViewState["request"] = value; }
    }
    
    /*
    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        string script = (IsConfirmOverride)?

    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        IsConfirmOverride = (eventArgument == "true");
    }
    protected void SetupClient()
    {

        string ScriptRef = Page.ClientScript.GetCallbackEventReference(this, "arg"
            , "ReciveServerData", "context");

        string callBackScript =
            "function CallServer(arg, context)" +
            "{ " + ScriptRef + "; }";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
            "CallServer", callBackScript, true);

    }
    #endregion
     * */
}
