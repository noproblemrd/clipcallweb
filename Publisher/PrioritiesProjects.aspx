﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="PrioritiesProjects.aspx.cs" Inherits="Publisher_PrioritiesProjects" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <script type="text/javascript">
function _CreateExcel() {
        var path = '<%# GetCreateExcelUrl %>';
        var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"<%# EXCEL_NAME %>"}';
        $.ajax({
            url: path,
            data: _data,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if (data.d.length == 0) {
                    return;
                }
                var _iframe = document.createElement('iframe');
                _iframe.style.display = 'none';
                _iframe.src = '<%# DownloadExcelUrl %>' + '?fn=' + data.d;
                document.getElementsByTagName('body')[0].appendChild(_iframe);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;

            }

        });
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
      <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
    </cc1:ToolkitScriptManager> 
          
     <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
            <div style="clear:both;"></div>
    	
	        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	            <ContentTemplate>	
                    <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
	                <div id="Table_Report"  runat="server" >
                         <asp:GridView ID="gv_prioritiesProjects" CssClass="data-table" runat="server" AutoGenerateColumns="false">
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1000" runat="server" Text="Project ID"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Bind("ProjectView") %>' Text='<%# Bind("ProjectId") %>' Target="_blank">HyperLink</asp:HyperLink>
                                        <asp:HiddenField ID="hf_incidentaccountId" runat="server" Value='<%# Bind("IncidentAccountId") %>' />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1002" runat="server" Text="Customer"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1003" runat="server" Text='<%# Bind("CustomerName") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1004" runat="server" Text="Pro to follow-up"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1005" runat="server" Text='<%# Bind("SupplierName") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1007" runat="server" Text="Schedules meeting"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1008" runat="server" Text='<%# Bind("ScheduledMeeting") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1009" runat="server" Text="Reason for follow-up"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1010" runat="server" Text='<%# Bind("eventType") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label1011" runat="server" Text="Follow-up time"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1012" runat="server" Text='<%# Bind("FollowUp") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                        
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>
            </div>
        </div>
                              
</asp:Content>

