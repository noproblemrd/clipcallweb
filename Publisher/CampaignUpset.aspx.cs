﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_CampaignUpset : PageSetting
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (this.siteSetting == null || string.IsNullOrEmpty(this.siteSetting.GetSiteID))
            Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Guid CampaignId;
            if (Guid.TryParse(Request.QueryString["CampaignId"], out CampaignId))
            {
                LoadOriginData(CampaignId);

            }
            else
            {
                cb_IsDistribution.Checked = true;
                lbl_Title.Text = "Add";
            }
        }
        else
        {
            rv_CostPerInstall.Enabled = false;
            rv_CostPerRequest.Enabled = false;
            rv_RevenueShare.Enabled = false;
            rv_Factor.Enabled = false;
        }
        Header.DataBind();
    }
    void LoadOriginData(Guid CampaignId)
    {
         lable_originid.Text = CampaignId.ToString();
        VCampaignId = CampaignId;
        //        public Result<DataModel.Injection.InjectionData> GetInjectionDetails(Guid injectionId)
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.ResultOfOriginData _result = null;
        try
        {
            _result = _site.GetOriginDetails(CampaignId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            CloseWindow();
            return;
        }
        if (_result.Type == WebReferenceSite.eResultType.Failure)
        {
            CloseWindow();
            return;
        }
        lbl_Title.Text = "Update";

        txt_Name.Text = _result.Value.Name;
        cb_UseSameInUpsale.Checked = _result.Value.UseSameInUpsale.HasValue ? _result.Value.UseSameInUpsale.Value : false;
        cb_IncludeCampaign.Checked = _result.Value.IncludeCampaign.HasValue ? _result.Value.IncludeCampaign.Value : false;
        cb_IsApiSeller.Checked = _result.Value.IsApiSeller.HasValue ? _result.Value.IsApiSeller.Value : false;
        cb_IsInFinancialDashboard.Checked = _result.Value.IsInFinancialDashboard.HasValue ? _result.Value.IsInFinancialDashboard.Value : false;
        cb_ShowNoProblemInSlider.Checked = _result.Value.ShowNoProblemInSlider.HasValue ? _result.Value.ShowNoProblemInSlider.Value : false;
        cb_IsDistribution.Checked = _result.Value.IsDistribution.HasValue ? _result.Value.IsDistribution.Value : false;
        //  cb_AdEngineEnabled.Checked = _result.Value.AdEngineEnabled.HasValue ? _result.Value.AdEngineEnabled.Value : false;
        txt_CostPerRequest.Text = _result.Value.CostPerRequest.HasValue ? string.Format(this.GetNumberFormat, _result.Value.CostPerRequest.Value) :
            "0";
        txt_CostPerInstall.Text = _result.Value.CostPerInstall.HasValue ? string.Format(this.GetNumberFormat, _result.Value.CostPerInstall.Value) :
            "0";
        txt_RevenueShare.Text = _result.Value.RevenueShare.HasValue ? string.Format(this.GetNumberFormat, _result.Value.RevenueShare.Value) :
            "0";
        txt_SliderBroughtToYouByName.Text = _result.Value.SliderBroughtToYouByName;

        if (_result.Value.Factor.HasValue)
            txt_Factor.Text = string.Format(this.GetNumberFormat, _result.Value.Factor.Value);
        txt_EmailToSend.Text = _result.Value.EmailToSendInstallationReport;
        cb_SendDailyEmail.Checked = _result.Value.SendInstallationReport.HasValue ? _result.Value.SendInstallationReport.Value : false;

        txt_MaxFactor.Text = string.Format(this.GetNumberFormat, _result.Value.MaxFactor.Value);
        txt_MaxFactorEmail.Text = _result.Value.EmailToSendMaxFactorReport;
        cb_MaxFactor.Checked = _result.Value.SendMaxFactorReport.HasValue ? _result.Value.SendMaxFactorReport.Value : false;

    }
    protected void lb_Save_Click(object sender, EventArgs e)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        //UpsertOriginDetails
        WebReferenceSite.OriginData od = new WebReferenceSite.OriginData();
        od.Id = VCampaignId;
        string _name = txt_Name.Text;
        if (string.IsNullOrEmpty(_name))
            return;
        od.Name = _name;
        od.UseSameInUpsale = cb_UseSameInUpsale.Checked;
        od.IncludeCampaign = cb_IncludeCampaign.Checked;
        od.IsApiSeller = cb_IsApiSeller.Checked;
        od.IsInFinancialDashboard = cb_IsInFinancialDashboard.Checked;
        od.ShowNoProblemInSlider = cb_ShowNoProblemInSlider.Checked;
        od.IsDistribution = cb_IsDistribution.Checked;
   //     od.AdEngineEnabled = cb_AdEngineEnabled.Checked;
        decimal _num;
        if (decimal.TryParse(txt_CostPerRequest.Text, out _num))
            od.CostPerRequest = _num;
        if (decimal.TryParse(txt_CostPerInstall.Text, out _num))
            od.CostPerInstall = _num;
        if (decimal.TryParse(txt_RevenueShare.Text, out _num))
            od.RevenueShare = _num;

        if (decimal.TryParse(txt_Factor.Text, out _num))
            od.Factor = _num;
        od.SendInstallationReport = cb_SendDailyEmail.Checked;
     //   if (cb_SendDailyEmail.Checked)
            od.EmailToSendInstallationReport = txt_EmailToSend.Text;

        if (decimal.TryParse(txt_MaxFactor.Text, out _num))
            od.MaxFactor = _num;
        od.SendMaxFactorReport = cb_MaxFactor.Checked;
//        if (cb_MaxFactor.Checked)
            od.EmailToSendMaxFactorReport = txt_MaxFactorEmail.Text;

        od.SliderBroughtToYouByName = txt_SliderBroughtToYouByName.Text;

        WebReferenceSite.ResultOfGuid rog = null;
        try
        {
            rog = _site.UpsertOriginDetails(od);
        }
        catch (Exception exc)
        {
            Update_Faild();
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (rog.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        if (VCampaignId == Guid.Empty)
        {
            VCampaignId = rog.Value;
            lable_originid.Text = rog.Value.ToString();
        }
        else
        {
            CloseReloadWindow();
        }
    }

    void CloseWindow()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "CloseWin", "CloseWin();", true);
    }
    void CloseReloadWindow()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "CloseReloadWin", "CloseReloadWin();", true);
    }
    Guid VCampaignId
    {
        get { return ViewState["VCampaignId"] == null ? Guid.Empty : (Guid)ViewState["VCampaignId"]; }
        set { ViewState["VCampaignId"] = value; }
    }
    
}