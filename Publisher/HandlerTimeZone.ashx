﻿<%@ WebHandler Language="C#" Class="HandlerTimeZone" %>

using System;
using System.Web;

public class HandlerTimeZone : IHttpHandler, System.Web.SessionState.IRequiresSessionState 
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();
        HttpRequest Request = context.Request;
        HttpResponse Response = context.Response;

        context.Response.ContentType = "application/x-javascript; charset=UTF-8;"; //"application/json; charset=UTF-8;";
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

        int tz;
        if (!int.TryParse(context.Request.QueryString["tz"], out tz))
            tz = 0;
        UserMangement.SetTimeZone(context, tz);


    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}