﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class Publisher_InjectionCampaignRelation :PageSetting
{
    bool IsInjection;
    
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (this.siteSetting == null || string.IsNullOrEmpty(this.siteSetting.GetSiteID))
        {
            //            Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
           // ClientScript.RegisterStartupScript(this.GetType(), "window.close", "window.close();", true);
            Response.Write(@"<script type=""text/javascript"">window.close();</script>");
            Response.Flush();
            Response.End();
            return;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string _param = Request.QueryString["InjectionId"];
            Guid _id;
            if (Guid.TryParse(_param, out _id))
            {
                IsInjection = true;
                LoadInjectionName(_id);
                LoadCampaignsRelationship(_id);
            }
            else
            {
                _param = Request.QueryString["CampaignId"];
                if (!Guid.TryParse(_param, out _id))
                {
                    CloseWindow();
                    return;
                }
                IsInjection = false;
                LoadCampaignnName(_id);
                LoadInjectionsRelationship(_id);
            }

            VID = _id;
            SetIsInjection();
            this.DataBind();
        }
        else
        {
            IsInjection = VIsInjection;
            Header.DataBind();
        }
        
    }

    private void LoadInjectionName(Guid InjectionId)
    {
         WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.ResultOfInjectionData resultInjectionData = null;
        //Result<List<DataModel.Origins.InjectionsCockpit.InjectionWithDAUs>> GetInjectionsAndDAUs()
        try
        {
            resultInjectionData = _site.GetInjectionDetails(InjectionId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (resultInjectionData.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        txt_Name.Text = resultInjectionData.Value.Name;
        
    }
    private void LoadCampaignnName(Guid CampaignId)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.ResultOfOriginData resultOriginData = null;
        //Result<List<DataModel.Origins.InjectionsCockpit.InjectionWithDAUs>> GetInjectionsAndDAUs()
        try
        {
            resultOriginData = _site.GetOriginDetails(CampaignId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (resultOriginData.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        txt_Name.Text = resultOriginData.Value.Name;

    }

   
    
    void LoadCampaignsRelationship(Guid InjectionId)
    {
        List<WebReferenceSite.InjectionOriginData> list = new List<WebReferenceSite.InjectionOriginData>();
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfListOfOriginWithDAUs resultCampaigns;
        try
        {
            resultCampaigns = _report.GetDistributionOriginsWithDAUs();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (resultCampaigns.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }       

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionOriginData result = null;
        try
        {
            result = _site.GetOriginsByInjection(InjectionId);
        }
        catch (Exception exc)
        {
            Update_Faild();
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }

        foreach (WebReferenceSite.InjectionOriginData iod in result.Value)
            list.Add(iod);
        LastData = list;
        DataTable dt = GetTableScheme();
       
        int _mutch = 0;
        foreach (var gsp in resultCampaigns.Value)
        {
            /*
            ListItem li = new ListItem(gsp.Name, gsp.Id.ToString());
            li.Attributes.Add("guid", gsp.Id.ToString());
            cbl_relation.Items.Add(li);
             */
            DataRow row = dt.NewRow();
            row["Name"] = gsp.OriginName + " (" + gsp.DAUs + ")";
            row["ID"] = gsp.OriginId.ToString();
            bool IsExists = false;
            foreach (WebReferenceSite.InjectionOriginData iod in result.Value)
            {
                if (gsp.OriginId == iod.OriginId)
                {
                    IsExists = true;
                    _mutch++;
                    row["IsPair"] = true;
                    row["HitCounter"] = iod.HitCounter;
                    row["DayCounter"] = iod.DayCounter;
                    break;
                }
            }
            if(!IsExists)
                row["IsPair"] = false;
           
            dt.Rows.Add(row);
        }
        IsAllChecked = _mutch == resultCampaigns.Value.Length;
        _Repeater.DataSource = dt;
        _Repeater.DataBind();
 //       SetCheckBoxList(result.Value);
  //      SetData(result.Value, dt);

    }
    DataTable GetTableScheme()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Name");
        dt.Columns.Add("ID");
        dt.Columns.Add("HitCounter");
        dt.Columns.Add("DayCounter");
        dt.Columns.Add("IsPair", typeof(bool));
        return dt;
    }
    protected bool IsAllChecked
    {
        get;
        set;
    }
    
    void LoadInjectionsRelationship(Guid CampaignId)
    {
        List<WebReferenceSite.InjectionOriginData> list = new List<WebReferenceSite.InjectionOriginData>();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionData resultInjectionData = null;
        //Result<List<DataModel.Origins.InjectionsCockpit.InjectionWithDAUs>> GetInjectionsAndDAUs()
        try
        {
            resultInjectionData = _site.GetInjectionData();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            Update_Faild();
            return;
        }
        if (resultInjectionData.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }


        WebReferenceSite.ResultOfListOfInjectionOriginData result = null;
        try
        {
            result = _site.GetInjectionsByOrigin(CampaignId);
        }
        catch (Exception exc)
        {
            Update_Faild();
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        foreach (WebReferenceSite.InjectionOriginData iod in result.Value)
            list.Add(iod);
        LastData = list;
        DataTable dt = GetTableScheme();
       
        int _mutch = 0;
        foreach (WebReferenceSite.InjectionData data in resultInjectionData.Value)
        {
            /*
            ListItem li = new ListItem(data.Name, data.InjectionId.ToString());
            li.Attributes.Add("guid", data.InjectionId.ToString());
            cbl_relation.Items.Add(li);
             * */
            
             DataRow row = dt.NewRow();
            row["Name"] = data.Name;
            row["ID"] = data.InjectionId.ToString();
            bool IsExists = false;
            foreach (WebReferenceSite.InjectionOriginData iod in result.Value)
            {
                if (data.InjectionId == iod.InjectionId)
                {
                    IsExists = true;
                    _mutch++;
                    row["IsPair"] = true;
                    row["HitCounter"] = iod.HitCounter;
                    row["DayCounter"] = iod.DayCounter;
                    break;
                }
            }
            if (!IsExists)
                row["IsPair"] = false;
            dt.Rows.Add(row);

        }
        IsAllChecked = _mutch == resultInjectionData.Value.Length;
        _Repeater.DataSource = dt;
        _Repeater.DataBind();
        //SetCheckBoxList(result.Value);   


    }
    protected void _Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Header)
            return;
        CheckBoxAllId = e.Item.FindControl("cb_all").ClientID;
    }
    
    protected void lb_Save_Click(object sender, EventArgs e)
    {
        List<WebReferenceSite.InjectionOriginData> list = new List<WebReferenceSite.InjectionOriginData>();
      //  List<Guid> list = new List<Guid>();
        foreach(RepeaterItem item in _Repeater.Items)        
        {
            Control ctr = item.FindControl("_cb");
            if (ctr == null)
                continue;
            CheckBox _cb = (CheckBox)ctr;
            if (!_cb.Checked)
                continue;
            WebReferenceSite.InjectionOriginData ior = new WebReferenceSite.InjectionOriginData();

            Guid _id = Guid.Parse(((HiddenField)(item.FindControl("_hf"))).Value);
            TextBox _txt = (TextBox)(item.FindControl("txt_HitCounter"));
            int _num;
            if (!int.TryParse(_txt.Text, out _num))
                _num = 0;
            ior.HitCounter = _num;
            _txt = (TextBox)(item.FindControl("txt_DayCounter"));
            if (!int.TryParse(_txt.Text, out _num))
                _num = 0;
            ior.DayCounter = _num;
            ior.InjectionId = IsInjection ? VID : _id;
            ior.OriginId = IsInjection ? _id : VID;
            if (IsInjection)
                ior.OriginName = _cb.Text;
            else
                ior.InjectionName = _cb.Text;
            list.Add(ior);
         //   list.Add(Guid.Parse(li.Value));
        }
        
        /*
        if (list.Count == 0)
            return;
         * */
        if (IsInjection)
            SaveCampaignsByInjection(VID, list);
        else
            SaveInjectionsByCampaign(VID, list);
        WriteLog(list);
    }
    void SaveCampaignsByInjection(Guid InjectionId, List<WebReferenceSite.InjectionOriginData> Campaigns)
    {

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.UpdateConnectionsByInjection(InjectionId, Campaigns.ToArray());
        }
        catch (Exception exc)
        {
            Update_Faild();
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        CloseWindow();
    }
    void SaveInjectionsByCampaign(Guid CampaignId, List<WebReferenceSite.InjectionOriginData> Injections)
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(siteSetting.GetUrlWebReference);
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.UpdateConnectionsByOrigin(CampaignId, Injections.ToArray());
        }
        catch (Exception exc)
        {
            Update_Faild();
            dbug_log.ExceptionLog(exc);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        CloseWindow();
    }
    void WriteLog(List<WebReferenceSite.InjectionOriginData> Newlist)
    {
        StringBuilder sb = new StringBuilder();
        List<WebReferenceSite.InjectionOriginData> OldList = LastData;
        for (int i = OldList.Count - 1; i > -1; i--)
        {
            for (int j = 0; j < Newlist.Count; j++)
            {
                if(OldList[i].InjectionId == Newlist[j].InjectionId && OldList[i].OriginId == Newlist[j].OriginId)
                {
                    if (OldList[i].HitCounter != Newlist[j].HitCounter)
                        sb.AppendLine((IsInjection ? ("Origin: " + Newlist[j].OriginName) : ("Injection: " + Newlist[j].InjectionName)) + " HitCounter changed to " + (IsInjection ? ("Injection: ") : ("Origin: ")) + txt_Name.Text + " OldValue: " + OldList[i].HitCounter + " NewValue: " + Newlist[j].HitCounter);
                    if (OldList[i].DayCounter != Newlist[j].DayCounter)
                        sb.AppendLine((IsInjection ? ("Origin: " + Newlist[j].OriginName) : ("Injection: " + Newlist[j].InjectionName)) + " DayCounter changed to " + (IsInjection ? ("Injection: ") : ("Origin: ")) + txt_Name.Text + " OldValue: " + OldList[i].DayCounter + " NewValue: " + Newlist[j].DayCounter);
                    OldList.RemoveAt(i);
                    Newlist.RemoveAt(j);
                    break;                    
                }
            }
        }
        foreach(WebReferenceSite.InjectionOriginData iod in OldList)
            sb.AppendLine((IsInjection ? ("Origin: " + iod.OriginName) : ("Injection: " + iod.InjectionName)) + " remove from " + (IsInjection ? ("Injection: ") : ("Origin: ")) + txt_Name.Text + " DayCounter: " + iod.DayCounter + " HitCounter: " + iod.HitCounter);
        foreach (WebReferenceSite.InjectionOriginData iod in Newlist)
            sb.AppendLine((IsInjection ? ("Origin: " + iod.OriginName) : ("Injection: " + iod.InjectionName)) + " add to " + (IsInjection ? ("Injection: ") : ("Origin: ")) + txt_Name.Text + " DayCounter: " + iod.DayCounter + " HitCounter: " + iod.HitCounter);
        CacheLog cl = new CacheLog();
        cl.WriteInjectionLog(userManagement.Get_Guid, userManagement.User_Name, sb);

    }
    void CloseWindow()
    {
        lb_Save.Visible = false;
        ClientScript.RegisterStartupScript(this.GetType(), "CloseWin", "CloseWin();", true);
    }
    bool VIsInjection
    {
        get { return ViewState["VIsInjection"] == null ? true : (bool)ViewState["VIsInjection"]; }
        //set { ViewState["VIsInjection"] = value; }
    }
    void SetIsInjection()
    {
        ViewState["VIsInjection"] = IsInjection;
    }
    protected string MainIdentity(bool GetMain)
    {
        return (GetMain ^ IsInjection) ? "Campaign" : "Injection";
    }
    Guid VID
    {
        get { return ViewState["VID"] == null ? Guid.Empty : (Guid)ViewState["VID"]; }
        set { ViewState["VID"] = value; }
    }
    protected string CheckBoxAllId
    {
        get;
        set;
    }
    List<WebReferenceSite.InjectionOriginData> LastData
    {
        get { return (Session["LastData"] == null) ? null : (List<WebReferenceSite.InjectionOriginData>)Session["LastData"]; }
        set { Session["LastData"] = value; }
    }



    
}