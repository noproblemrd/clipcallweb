﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SmsCampaign.aspx.cs" Inherits="Publisher_SmsCampaign" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <style type="text/css">
        .main-inputs, .form-field
        {
            width: 100% !important;
        }
        input[type="text"], textarea 
        {
            width: 400px !important;
        }
        .txt_message
        {
            height: 100px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" /> 
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	       
               <div class="main-inputs" runat="server" id="PublisherInput">					
	                
            			 
			            <div class="form-field">
                            <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Campaign name</label>
                            <asp:TextBox ID="txt_CampaignName" CssClass="form-text" runat="server"></asp:TextBox>
			              
                       </div>  
                   <div class="clear"></div>
                   <div class="form-field">
                            <label for="form-subsegment" class="label" runat="server" id="Label1">Text message:</label>
                            <asp:TextBox ID="txt_message" CssClass="form-text txt_message" runat="server"  TextMode="MultiLine"></asp:TextBox>
			              
                       </div>  
                   <div class="clear"></div>
                   <div class="form-field" style="text-align:center;">
                       <asp:LinkButton ID="lb_AddReport" runat="server" Text="Add" CssClass="anchor_CreateReportSubmit2" 
				                    onclick="btn_Click"></asp:LinkButton>
                   </div>
                   <div class="clear"></div>
                   <div class="form-field">
                       <asp:Label ID="lbl_message" runat="server"  ForeColor="Red"></asp:Label>
                   </div>    
        </div>
            <div>
            <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="true">

            </asp:GridView>
        </div>
    </div>
        
    </div>
</asp:Content>

