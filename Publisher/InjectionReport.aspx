﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="InjectionReport.aspx.cs" Inherits="Publisher_InjectionReport" %>


<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
<link rel="Stylesheet" href="InjectionCampaign.css" type="text/css" />

<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js"></script>
<script type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>
<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
      //  Set_a_Collapse_position();
        hideDiv();
    }

    function SetTrTable() {
        //  var i = 0;
        $('.data-table').find('tr').each(function () {
            /*
            $(this).addClass((i % 2 == 0) ? 'odd' : 'even');
            i++;
            */
            if ($(this).find('th').length > 0)
                return;
            $(this).addClass('tr_pointer');
            var ItOpen = false;
            var _this = this;
            $(this).click(function () {
                if (!ItOpen) {
                    showDiv();

                    var _date = $(this).find('input[type="hidden"]').val();

                    $.ajax({
                        url: '<%# GetDrillDownPath %>',
                        data: "{ 'date': '" + _date + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            if (data.d.length == 0)
                                return;
                            ItOpen = true;
                            var $NewTr = $('<tr>');
                            var $NewTd = $('<td colspan="24">');
                            $NewTd.html(data.d);
                            $NewTr.append($NewTd);
                            $(_this).after($NewTr);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {

                        },
                        complete: function (jqXHR, textStatus) {

                            hideDiv();
                        }

                    });
                }
                else {
                    var $tr = $(_this).next('tr');
                    if ($tr.is(':visible'))
                        $tr.hide();
                    else
                        $tr.show();
                }
            });
        });
    }

    function LoadChart(fileXML, _chart) {

        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render("div_chart");
    }
    function CleanChart() {
        document.getElementById("div_chart").innerHTML = "";
    }
    
    function GetCheckBoxListValues(chkBoxID) {
        var chkBox = document.getElementById(chkBoxID);
        var options = chkBox.getElementsByTagName('input');
    //    var listOfSpans = chkBox.getElementsByTagName('span');
        for (var i = 0; i < options.length; i++) {
            if (options[i].checked) {
                //      alert(listOfSpans[i].attributes["JSvalue"].value);
           //     alert(options[i].parentNode.attributes["JSvalue"]);
            }
        }
    }
    $(function () {
        $('#<%# rbl_Injection.ClientID %>').find('input[type="radio"]').each(function () {
            $(this).change(function () {
                SetBelongCampaign(this);
            });
            if(this.checked)
                SetBelongCampaign(this);
        });
        $('#<%# cbl_Origins.ClientID %>').find('input[type="checkbox"]').each(function () {
            $(this).change(function () {
                if (this.checked) {
                    var _id = GetOriginValue(this);
                    if (_id == '<%# Guid.Empty.ToString() %>')
                        ClearOrigins();
                    else
                        ClearOriginAll();
                }
                else {
                    var options = document.getElementById('<%# cbl_Origins.ClientID %>').getElementsByTagName('input');
                    var IsSign = false;
                    var CheckBoxAll;
                    for (var i = 0; i < options.length; i++) {
                        var _id = GetOriginValue(options[i]);
                        if (_id == '<%# Guid.Empty.ToString() %>')
                            CheckBoxAll = i;
                        if (options[i].checked) {
                            IsSign = true;
                            break;
                        }
                    }
                    if (!IsSign)
                        options[CheckBoxAll].checked = true;
                }

            });
        });
        function SetBelongCampaign(_this) {
            if (_this.value == '<%# Guid.Empty.ToString() %>') {
                ClearCampagins();
                return;
            }
            GetCampaignOfInjection(_this.value);
        }
        function checkMultiSelection()
        {
            var indexSelected=0;

            $('#<%# cbl_Origins.ClientID %>').find('input[type="checkbox"]').each(function () {
                if ($(this).prop('id').indexOf("cbl_Origins_0") == -1) // instead of the first checkbox : all
                {                    
                    if ($(this).prop('checked') == true)
                        indexSelected++;
                }
                
            }
            )

            if (indexSelected > 1)
            {
                alert("Multiple selection not allowed!");
                return false;
            }
               
            return true;
        }

        $('#a_update_injection').click(function () {
            
            var _selected = GetSelectInjection();
            var _query = (_selected.length == 0) ? '' : '?InjectionId=' + _selected;
            var update_injection = window.open("<%# update_injection_page %>" + _query, "update_injection", "width=600, height=600");
            update_injection.focus();
        });
        $('#a_update_campaign').click(function () {            
            if (checkMultiSelection() == false)
                return;
            var _selected = GetSelectCampaign();
            var _query = (_selected.length == 0) ? '' : '?CampaignId=' + _selected;
            var update_injection = window.open("<%# update_Campaign_page %>" + _query, "update_campaign", "width=600, height=" + (screen.height - 150));
            update_injection.focus();
        });
        $('#a_injection_relation').click(function () {
            var _selected = GetSelectInjection();
            if (_selected.length == 0)
                return;
            var _query = '?InjectionId=' + _selected;
            var update_injection = window.open("<%# injection_campaign_relation_page %>" + _query, "injection_relation", "scrollbars=1,width=600, height=" + (screen.height - 150));
            update_injection.focus();
        });
        $('#a_campaign_relation').click(function () {
            if (checkMultiSelection() == false)
                return;
            var _selected = GetSelectCampaign();
            if (_selected.length == 0)
                return;
            var _query = '?CampaignId=' + _selected;
            var update_injection = window.open("<%# injection_campaign_relation_page %>" + _query, "Campaign_relation", "scrollbars=1,width=600, height=" + (screen.height - 150));
            update_injection.focus();
        });
        $('#a_publish').click(function () {
            PublishInjection();
        });
    });
    function Reload() {
        window.location.href = '<%# ThisPath %>';
    }
    
    function GetSelectInjection() {
        var options = document.getElementById('<%# rbl_Injection.ClientID %>').getElementsByTagName('input');
        for (var i = 0; i < options.length; i++) {
            if (options[i].checked) {
                var _val = options[i].value;
                if (_val == '<%# Guid.Empty.ToString() %>')
                    return '';
                return _val;
            }
        }
        return '';
    }
    function GetSelectCampaign() {
        var options = document.getElementById('<%# cbl_Origins.ClientID %>').getElementsByTagName('input');
        for (var i = 0; i < options.length; i++) {
            if (options[i].checked) {
                var _val = GetOriginValue(options[i]);
                if (_val == '<%# Guid.Empty.ToString() %>')
                    return '';
                return _val;
            }
        }
        return '';
    }
    function GetOriginValue(InputOrigin) {
        return $(InputOrigin).parent('span').attr('guid');
    }
    function ClearOriginAll() {
        var options = document.getElementById('<%# cbl_Origins.ClientID %>').getElementsByTagName('input');
        for (var i = 0; i < options.length; i++) {
            var _val = GetOriginValue(options[i]);
            if (_val == '<%# Guid.Empty.ToString() %>') {
                options[i].checked = false;
                return;
            }
        }
    }
    function ClearOrigins() {
        var options = document.getElementById('<%# cbl_Origins.ClientID %>').getElementsByTagName('input');
        for (var i = 0; i < options.length; i++) {
            var _val = GetOriginValue(options[i]);
            if (_val == '<%# Guid.Empty.ToString() %>')
                continue;            
            options[i].checked = false;            
        }
    }
    function ClearCampagins() {
        var options = document.getElementById('<%# cbl_Origins.ClientID %>').getElementsByTagName('input');
        for (var i = 0; i < options.length; i++) {
            $(options[i]).parent('span').removeClass('BelongInjection');
            /*
            var _val = options[i].value;
            if (_val != '<# Guid.Empty.ToString() %>')
                options[i].checked = false;
                */
        }
    }
    function GetCampaignOfInjection(InjectionID) {
      //  showDiv();
        $.ajax({
            url: '<%# GetCampainsOfInjection %>',
            data: "{ InjectionId: '" + InjectionID + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {

                var options = document.getElementById('<%# cbl_Origins.ClientID %>').getElementsByTagName('input');
                var campaigns = (data.d.length == 0) ? [''] : data.d.split(';');
                for (var i = 0; i < options.length; i++) {
                    var _id = $(options[i]).parent('span').attr('guid');
                    var IsFindId = false;
                    for (var j = 0; j < campaigns.length; j++) {
                        if (campaigns[j] == _id) {
                            IsFindId = true;
                            break;
                        }
                    }
                    //   options[i].checked = IsFindId;
                    if (IsFindId)
                        $(options[i]).parent('span').addClass('BelongInjection');
                    else
                        $(options[i]).parent('span').removeClass('BelongInjection');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                /*
                var _log = '';
                for (str in XMLHttpRequest) {
                _log += str + '=' + XMLHttpRequest[str] + ";";
                }
                console.log(_log);
                */
            },
            complete: function (jqXHR, textStatus) {

                hideDiv();
            }

        });
    }
    function PublishInjection() {
        //  showDiv();
        if (!confirm('You are about to update cache!!!'))
            return;
        $('#a_publish').hide();
        $('#a_publish_loader').show();
        $.ajax({
            url: '<%# GetPublishInjection %>',
            data: "{ UserId: '<%# userManagement.GetGuid %>' }",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if(data.d != "true")
                    alert('Publish faild');


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert('Publish faild');
            },
            complete: function (jqXHR, textStatus) {
                $('#a_publish').show();
                $('#a_publish_loader').hide();

            }

        });
    }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" >    
    </cc1:ToolkitScriptManager>
    <uc1:ToolboxReport ID="_Toolbox" runat="server" />
    
    <div class="page-content minisite-content2"> 
        <div class="form-MultiSelect">   
             <div class="form-field form-field-select">
                <div class="form-select-titles">
                    <div>
                        <asp:Label ID="lbl_Injection" runat="server" CssClass="label" Text="Injection"></asp:Label>   
                    </div>
                    <div class="form-select-title-right">
                        <a href="javascript:void(0);" class="BtnInjection" id="a_update_injection">Update / Add</a>
                        <a href="javascript:void(0);" class="BtnInjection" id="a_injection_relation">Pair</a>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="div_ItemList div_ItemList_left">                   
                    <asp:RadioButtonList ID="rbl_Injection" runat="server" >
                    </asp:RadioButtonList>
                </div>   
            </div>                     
            <div class="form-field form-field-select">
                 <div class="form-select-titles">
                    <div>
                        <asp:Label ID="lbl_Origin" runat="server" CssClass="label div_ItemList_right" Text="Campaign"></asp:Label>    
                    </div>
                    <div class="form-select-title-right">
                        <a href="javascript:void(0);" class="BtnInjection" id="a_update_campaign">Update / Add</a>
                        <a href="javascript:void(0);" class="BtnInjection" id="a_campaign_relation">Pair</a>
                    </div>
                </div>                
                <div class="clear"></div>
                <div class="div_ItemList div_ItemList_right">   
                   
                    <asp:CheckBoxList ID="cbl_Origins" runat="server">
                    </asp:CheckBoxList>
                </div>     
                
            </div>
            
        </div>
        <div class="clear"></div>
        <div class="form-field-publish">
            <a href="javascript:void(0);" class="BtnInjection btn-publish" id="a_publish">Publish now</a>
            <span id="a_publish_loader" style="display:none;" class="a__loader a_publish_loader"></span>
        </div>
        <div class="clear"></div>   
        <div class="callreport new-stage">
              
            <uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" />
        </div>
        <div class="form-field form-field-country">
            <asp:Label ID="lbl_Country" runat="server" CssClass="label" Text="Geo"></asp:Label>   
            <asp:DropDownList ID="ddl_Country" runat="server" CssClass="form-select">
            </asp:DropDownList>
        </div>
        <div class="clear"></div> 
        <div class="div-btnInjection">
             <asp:Button ID="btn_Run" runat="server" CssClass="CreateReportSubmit2" 
                    Text="Create report" onclick="btn_Run_Click"/>
            
        </div>
        <div class="clear"></div>  
         <div class="table2" style="width:100%;" runat="server" id="div_table">
            
            <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="div_gridMyCall">
                   
                    <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                        CssClass="data-table"  >
                    <RowStyle CssClass="_even" />
                    <AlternatingRowStyle CssClass="_odd" />
                    <Columns>
                        <asp:TemplateField SortExpression="Date">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="Date"></asp:Label> 
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text="<%# Bind('Date') %>"></asp:Label>        
                                    <asp:HiddenField ID="hf_Date" runat="server" Value="<%# Bind('DateLng') %>" />              
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="DAUs">
                            <HeaderTemplate>
                                <asp:Label ID="Label1020" runat="server" Text="DAUs"></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2020" runat="server" Text="<%# Bind('DAUs') %>"></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                            <asp:TemplateField SortExpression="InjectionPercent">
                            <HeaderTemplate>
                                <asp:Label ID="Label010" runat="server" Text="Injection percent"></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label002" runat="server" Text="<%# Bind('InjectionPercent') %>"></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="Revenue">
                            <HeaderTemplate>
                                <asp:Label ID="Label80" runat="server" Text="Revenue"></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label82" runat="server" Text="<%# Bind('Revenue') %>"></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="DUV">
                            <HeaderTemplate>
                                <asp:Label ID="Label3" runat="server" Text="DUV"></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text="<%# Bind('DUV') %>"></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="PPI_DAU">
                            <HeaderTemplate>
                                <asp:Label ID="Label5" runat="server" Text="PPI DAU"></asp:Label> 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text="<%# Bind('PPI_DAU') %>"></asp:Label>                             
                            </ItemTemplate>
                        </asp:TemplateField>
                            
                    </Columns>
                    </asp:GridView>
                </div>
                   
            </div>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </div>
     <asp:Label ID="lbl_TitleReport" runat="server" Text="Injection Management" Visible="false"></asp:Label>
</asp:Content>

