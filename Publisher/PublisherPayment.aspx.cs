﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_PublisherPayment : PageSetting, IToolBoxControl
{
    const string MONEY_FORMAT = "{0:#,0.##}";//"{0:0,0.##}";
    const string EXCEL_NAME = "PaymentReport";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!userManagement.IsPublisher() || string.IsNullOrEmpty(GetGuidSetting()))
                Response.Redirect("LogOut.aspx");

            string csnameStep = "setStep";
            if (!ClientScript.IsStartupScriptRegistered(this.GetType(), csnameStep))
            {
                string csTextStep = "witchAlreadyStep(6);";
                ClientScript.RegisterStartupScript(this.GetType(), csnameStep, csTextStep, true);
            }
            LoadRechargeTypes();
            LoadDayInMonth();
            LoadPaymentHistory();
            LoadSupplierCharging();
            RangeValidator_RechargeAmount.DataBind();
            LoadValidationSearch();
            LoadDollarIcon();
        }
        Header.DataBind();
        
                
    }

    private void LoadDollarIcon()
    {
        string _script = "parent.closeOpenOverLayIframeNewDeposite();";
        Toolbox1.VisibleDollarIcon(_script);
    }

    private void LoadValidationSearch()
    {
        string _reg = @"^\\d+$";
        Toolbox1.SearchValidation(_reg, HttpUtility.HtmlEncode(lbl_ValidationSearch.Text));
    }

    private void LoadSupplierCharging()
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.GetSupplierChargingDataRequest _request = new WebReferenceSupplier.GetSupplierChargingDataRequest();
        _request.SupplierId = new Guid(GetGuidSetting());
        WebReferenceSupplier.ResultOfGetSupplierChargingDataResponse result = null;
        try
        {
            result = _supplier.GetSupplierChargingData(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        WebReferenceSupplier.GetSupplierChargingDataResponse data = result.Value;
        lblCurrentBalance.Text = string.Format(MONEY_FORMAT, data.CurrentBalance);
        lbl_Currency.Text = siteSetting.CurrencySymbol;

        UserMangement um = GetUserSetting();
        um.Email = data.Email;
        this.SetGuidSetting(um);

        if (PpcSite.GetCurrent().IsZapSite(siteSetting.GetSiteID))// && PpcSite.GetCurrent().UserZap != "NoProblemInvoicesProd")
            div_Recharge.Visible = false;
        else
        {
        
        
            if (data.RechargeEnabled && !string.IsNullOrEmpty(data.CreditCardToken))
            {
                cb_Recharge.Checked = data.IsAutoRenew;
                string RechargeType = data.RechargeTypeCode.ToString();
                Utilities.SetDropDownList(ddl_RechargeType, RechargeType);
                if (data.RechargeTypeCode == WebReferenceSupplier.RechargeTypeCode.Monthly)
                {
                    div_DayInMonth.Style[HtmlTextWriterStyle.Display] = "block";
                    div_DayInMonth.Enabled = true;
                    Utilities.SetDropDownList(ddl_DayInMonth, data.RechargeDayOfMonth + "");
                }
                else
                {
                    div_DayInMonth.Style[HtmlTextWriterStyle.Display] = "none";
                    div_DayInMonth.Enabled = false;
                }
                txt_RechargeAmount.Text = (data.IsAutoRenew) ? data.RechargeAmount + "" : "";
                if (data.IsAutoRenew)
                    txt_RechargeAmount.Attributes.Remove("readOnly");
                else
                    txt_RechargeAmount.Attributes.Add("readOnly", "readOnly");
            }
            else
                div_Recharge.Visible = false;
        }
        
        int _MinDeposit = (int)data.MinDepositPrice;
        SortedDictionary<int, int> pricePercentList = new SortedDictionary<int, int>();
        foreach (WebReferenceSupplier.PricingPackageData ppd in data.Packages)
        {
            if (!ppd.IsActive)
                continue;
            if (pricePercentList.ContainsKey(ppd.FromAmount))
                continue;
            pricePercentList.Add(ppd.FromAmount, ppd.BonusPercent);
        }
        BonusServiceDataV = new BonusServiceData(pricePercentList, _MinDeposit, data.RechargeBonusPercent);
        MinDepositV = _MinDeposit;
    }
    private void LoadPaymentHistory()
    {
        
        WebReferenceSupplier.GetSupplierPricingHistoryRequest _request = new WebReferenceSupplier.GetSupplierPricingHistoryRequest();
        _request.SupplierId = new Guid(GetGuidSetting());
        _request.PageSize = TablePaging1.ItemInPage;
       
        PricingHistoryRequestV = _request;
        LoadPaymentHistory(_request, 1);
    }
    void LoadPaymentHistory(WebReferenceSupplier.GetSupplierPricingHistoryRequest _request, int PageIndex)
    {
        _request.PageNumber = PageIndex;
        WebReferenceSupplier.ResultOfGetSupplierPricingHistoryResponse result = ResultOfGetSupplierPricingHistory(_request);
        if (result == null)
            return;
        DataTable data = LoadResultToDataTable(result);
        TablePaging1.Current_Page = result.Value.CurrentPage;
        TablePaging1.Pages_Count = result.Value.TotalPages;
        TotalRowsV = result.Value.TotalPages;
        TablePaging1.LoadPages();
        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    
    WebReferenceSupplier.ResultOfGetSupplierPricingHistoryResponse ResultOfGetSupplierPricingHistory(WebReferenceSupplier.GetSupplierPricingHistoryRequest _request)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfGetSupplierPricingHistoryResponse result = null;
        try
        {
            result = _supplier.GetSupplierPricingHistory(_request);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
        return result;
        
    }
    protected void lb_Deposition_click(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)(((LinkButton)sender).NamingContainer);
        Guid _id = new Guid(((Label)row.FindControl("lbl_DepositionId")).Text);
        string _param = _id.ToString();
        string _script = "openAddDeposit('" + _param + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPaymentTicket", _script, true);
    }
    DataTable LoadResultToDataTable(WebReferenceSupplier.ResultOfGetSupplierPricingHistoryResponse result)
    {
        Dictionary<WebReferenceSupplier.ePaymentMethod, string> dic = DicPaymentMethod();
       
        DataTable data = new DataTable();
        data.Columns.Add("Amount", typeof(string));
        data.Columns.Add("Total", typeof(string));
        data.Columns.Add("CreatedOn", typeof(string));
        data.Columns.Add("Number", typeof(string));
        data.Columns.Add("PaymentMethod", typeof(string));
        data.Columns.Add("SupplierPricingId", typeof(string));
        
        foreach (WebReferenceSupplier.PricingRowData prd in result.Value.Pricings)
        {
            DataRow row = data.NewRow();
            row["Amount"] = siteSetting.CurrencySymbol + string.Format(MONEY_FORMAT, prd.Amount);
            row["Total"] = siteSetting.CurrencySymbol + string.Format(MONEY_FORMAT, prd.Total);
            row["CreatedOn"] = string.Format(siteSetting.DateFormat, prd.CreatedOn) + " " +
                string.Format(siteSetting.TimeFormat, prd.CreatedOn);
            row["Number"] = prd.Number;
            row["PaymentMethod"] = dic[prd.PaymentMethod];
            
            row["SupplierPricingId"] = prd.SupplierPricingId.ToString();
            data.Rows.Add(row);
        }
        return data;

    }
    Dictionary<WebReferenceSupplier.ePaymentMethod, string> DicPaymentMethod()
    {
        Dictionary<WebReferenceSupplier.ePaymentMethod, string> dic = new Dictionary<WebReferenceSupplier.ePaymentMethod, string>();
        foreach (WebReferenceSupplier.ePaymentMethod epm in Enum.GetValues(typeof(WebReferenceSupplier.ePaymentMethod)))
        {
            dic.Add(epm, 
                EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "ePaymentMethod", epm.ToString()));
        }
        return dic;
    }
    private void LoadRechargeTypes()
    {
        ddl_RechargeType.Items.Clear();
        foreach (string s in Enum.GetNames(typeof(WebReferenceSupplier.RechargeTypeCode)))
        {
            ddl_RechargeType.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "RechargeTypeCode", s), s));
        }
    }

    private void LoadDayInMonth()
    {
        ddl_DayInMonth.Items.Clear();
        
        for (int i = 1; i < 32; i++)
        {
            ddl_DayInMonth.Items.Add(new ListItem(i + "", i + ""));
        }
    }





    #region TablePaggingControl Members



    protected void _tp__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        WebReferenceSupplier.GetSupplierPricingHistoryRequest _request = PricingHistoryRequestV;
     //   _request.PageNumber = _pageNum;
        LoadPaymentHistory(_request, _pageNum);
    }

    #endregion



    #region IToolBoxControl Members

    public void SetToolBox(UserControl ControlSender)
    {
        Toolbox1.RemoveAttach();
        Toolbox1.RemoveDelete();
        Toolbox1.RemoveEdit();
    }

    public void SetToolBoxEvents(UserControl ControlSender)
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
        Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.NoRecordDisplay) + "');", true);
            return;
        }
        WebReferenceSupplier.GetSupplierPricingHistoryRequest _request = PricingHistoryRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        WebReferenceSupplier.ResultOfGetSupplierPricingHistoryResponse result = ResultOfGetSupplierPricingHistory(_request);
        if (result == null)
        {
            Update_Faild();
            return;
        }
        DataTable data = LoadResultToDataTable(result);
        PricingHistoryRequestV = _request;
        
        Session["data_print"] = data;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.NoRecordExport) + "');", true);
            return;
        }
        WebReferenceSupplier.GetSupplierPricingHistoryRequest _request = PricingHistoryRequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        WebReferenceSupplier.ResultOfGetSupplierPricingHistoryResponse result = ResultOfGetSupplierPricingHistory(_request);
        if (result == null)
        {
            Update_Faild();
            return;
        }
        DataTable data = LoadResultToDataTable(result);
        PricingHistoryRequestV = _request;
        
        ToExcel te = new ToExcel(this, EXCEL_NAME);
        if (!te.ExecExcel(data, _GridView))
            Update_Faild();
    }

    void Toolbox1_AddExec(object sender, EventArgs e)
    {
        if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "openAddDeposit"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "openAddDeposit", "openAddDeposit(null);", true);
            return;
        }
    }

    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        string _num = Toolbox1.GetTxt;
        if (string.IsNullOrEmpty(_num))
        {
            LoadPaymentHistory();
            return;
        }
        WebReferenceSupplier.GetSupplierPricingHistoryRequest _request = new WebReferenceSupplier.GetSupplierPricingHistoryRequest();
        _request.SupplierId = new Guid(GetGuidSetting());
        _request.PageSize = TablePaging1.ItemInPage;
        _request.PricingNumber = _num;
        PricingHistoryRequestV = _request;
        LoadPaymentHistory(_request, 1);
    }
    protected void btn_SaveRecharge_Click(object sender, EventArgs e)
    {
        WebReferenceSupplier.Supplier _supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.UpdateAutoRechargeOptionsRequest _request = new WebReferenceSupplier.UpdateAutoRechargeOptionsRequest();
        _request.CreatedByUserId = userManagement.Get_Guid;
        _request.SupplierId = new Guid(GetGuidSetting());
        if (cb_Recharge.Checked)
        {
            _request.IsAutoRecharge = true;
            _request.RechargeAmount = int.Parse(txt_RechargeAmount.Text);
            WebReferenceSupplier.RechargeTypeCode rtc;
            if (Enum.TryParse(ddl_RechargeType.SelectedValue, out rtc))
                _request.RechargeTypeCode = rtc;
            if (rtc == WebReferenceSupplier.RechargeTypeCode.Monthly)            
                _request.DayOfMonth = int.Parse(ddl_DayInMonth.SelectedValue);            
        }
        else
            _request.IsAutoRecharge = false;

        WebReferenceSupplier.Result result = null;
        try
        {
            result = _supplier.UpdateAutoRechargeOptions(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        RequiredFieldValidator_RechargeAmount.IsValid = true;

        LoadSupplierCharging();
    }
    #endregion
    WebReferenceSupplier.GetSupplierPricingHistoryRequest PricingHistoryRequestV
    {
        get
        {
            return (Session["PricingHistoryRequest"] == null) ? new WebReferenceSupplier.GetSupplierPricingHistoryRequest() : (WebReferenceSupplier.GetSupplierPricingHistoryRequest)Session["PricingHistoryRequest"];
        }
        set { Session["PricingHistoryRequest"] = value; }
    }
    
    int MinDepositV
    {
        get { return (string.IsNullOrEmpty(hf_MinDeposite.Value)) ? 0 : int.Parse(hf_MinDeposite.Value); }
        set { hf_MinDeposite.Value = value + ""; }
    }
    protected string GetInvalidRechargeDeposite
    {
        get { return lbl_InvalidRechargeDeposite.Text + " " + MinDepositV; }
    }
    protected string _MinValue
    {
        get { return MinDepositV + ""; }
    }
    protected string _MaxValue
    {
        get { return short.MaxValue.ToString(); }
    }
    BonusServiceData BonusServiceDataV
    {
        set { Session["BonusServiceData"] = value; }
    }
    /*
    WebReferenceSupplier.ChargingDataHeadingData[] ChargingDataHeadingDataV
    {
        set { Session["ChargingDataHeadingData"] = value; }
    }
     */
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
    
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
     
   
}