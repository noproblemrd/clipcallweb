﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_CustomerReport : PageSetting
{
    const string RUN_REPORT = "RUN_REPORT";
    const string V_ICON = "~/Publisher/images/icon-v.png";
    const string X_ICON = "~/Publisher/images/icon-x.png";
    protected const string BASE_PATH = "~/Publisher/ClipCallControls/";

    const int ONE_DAY = 7;
    protected string SessionTableName
    {
        get
        {
            return _CustomerReportControl.SessionTableName;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(lb_RunReport);
        if (!IsPostBack)
        {
   //         dataV = null;
            setDateTime();
            LoadOrigins();
        }
        else
        {
            if (Request["__EVENTARGUMENT"] == RUN_REPORT)
                ExecReport();
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    protected override void Render(HtmlTextWriter writer)
    {

        Page.ClientScript.RegisterForEventValidation(btn_virtual_run.UniqueID, RUN_REPORT);
        base.Render(writer);
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }
    private void LoadOrigins()
    {

        ddl_origin.Items.Clear();
        ListItem liall = new ListItem("All", string.Empty);
        liall.Selected = true;
        ddl_origin.Items.Add(liall);
        foreach (ClipCallReport.eOrigin _origin in Enum.GetValues(typeof(ClipCallReport.eOrigin)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(_origin), _origin.ToString());
            ddl_origin.Items.Add(li);
        }
    }

    private void ExecReport()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.CustomerReportRequest _request = new ClipCallReport.CustomerReportRequest();
        _request.From = _FromToDatePicker.GetDateFrom;
        _request.To = _FromToDatePicker.GetDateTo;
        _request.TextSearch = txtSearch.Text.Trim();

        ClipCallReport.eOrigin _origin;
        if (Enum.TryParse(ddl_origin.SelectedValue, out _origin))
            _request.Origin = _origin;
        else
            _request.Origin = null;

        ClipCallReport.ResultOfListOfCustomerRowReport result = null;
        try
        {
            result = report.CustomerReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        
        if (result.Value == null)
        {
            /*
            _GridView.DataSource = null;
            _GridView.DataBind();
            */
    //        dataV = null;
            lbl_RecordMached.Text = null;
            return;
        }
        lbl_RecordMached.Text = result.Value.Length.ToString();
        _CustomerReportControl.BindReport(result.Value);
        _UpdatePanel.Update();
        StartPaging();
        /*
        DataTable dt = new DataTable();
        dt.Columns.Add("CustomerId");
        dt.Columns.Add("CreatedOn");
        dt.Columns.Add("Phone");
        dt.Columns.Add("Name");
        dt.Columns.Add("Email");
        dt.Columns.Add("Region");
        dt.Columns.Add("HasFacebook");
        dt.Columns.Add("Credit");
        dt.Columns.Add("Device");
        dt.Columns.Add("ClipCallVer");
        dt.Columns.Add("Status");
        dt.Columns.Add("DisplayFacebbokIcon", typeof(bool));
        dt.Columns.Add("Membership");

        dt.Columns.Add("Origin");
        dt.Columns.Add("OriginalOrigin");
        dt.Columns.Add("TransferOrigin");
        dt.Columns.Add("Carrier");
        dt.Columns.Add("RequestReport");
        dt.Columns.Add("VerizonSkinPopupAppeared");

        foreach(var data in result.Value)
        {
            DataRow row = dt.NewRow();
            row["CustomerId"] = data.CustomerId;
            row["CreatedOn"] = string.Format(siteSetting.DateTimeFormat, data.CreatedOn);
            row["Phone"] = data.Phone;
            row["Name"] = data.Name;
            row["Region"] = data.Region;
            row["Email"] = data.Email;
            if (string.IsNullOrEmpty(data.FacebookId))
            {
                row["DisplayFacebbokIcon"] = false;
            }
            else
            {
                row["HasFacebook"] = V_ICON;
                row["DisplayFacebbokIcon"] = true;
            }
            row["Credit"] = string.Format(this.GetNumberFormat, data.Credit);
            row["Device"] = data.Device;
            row["ClipCallVer"] = data.ClipCallVersion;
            row["Status"] = data.Status;
            row["Membership"] = data.Membership;

            row["Origin"] = data.ReportOrigin;
            row["OriginalOrigin"] = data.Origin;
            row["Carrier"] = data.CarrierName;
            row["RequestReport"] = string.Format(Utilities.CUSTOMER_REQUEST_REPORT_URL, data.CustomerId);
            row["VerizonSkinPopupAppeared"] = data.VerizonSkinPopupAppeared ? "Yes" : "No";
            dt.Rows.Add(row);
        }
        lbl_RecordMached.Text = result.Value.Length.ToString();
        _GridView.DataSource = dt;
        _GridView.DataBind();
        _UpdatePanel.Update();
        dataV = dt;
        */
    }
    protected void StartPaging()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "PagingStart", "PagingStart();", true);
    }
    protected string GetPagingMethodPath()
    {
        return (this.AppRelativeVirtualPath + "/GetReportPage").Remove(0, 1);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle("Customer Report");
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected string RunReport()
    {
        PostBackOptions myPostBackOptions = new PostBackOptions(btn_virtual_run, RUN_REPORT);
        myPostBackOptions.PerformValidation = false;
        //     return ScriptManager.g
        return Page.ClientScript.GetPostBackEventReference(myPostBackOptions);
        //   WebReferenceSite.FundsStatus
    }
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    /*
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    */
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
    protected string UrlTransferOrigin
    {
        get
        {
            return ResolveUrl("~/Publisher/CustomerReport.aspx/TransferOrigin");
        }
    }
    [WebMethod(MessageName = "GetReportPage")]
    public static string GetReportPage()
    {
        PageRenderin page = new PageRenderin();
        page.EnableEventValidation = false;
        UserControl ctl =
             (UserControl)page.LoadControl(BASE_PATH + "CustomerReportControl.ascx");

        MethodInfo _translate = ctl.GetType().GetMethod("SetDataByPage");
        bool IsBindData = (bool)_translate.Invoke(ctl, null);
        if (!IsBindData)
            return "done";
        ctl.EnableViewState = false;


        //Render control to string
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
            {
                ctl.RenderControl(textWriter);
            }
        }
        return sb.ToString();
    }
    [WebMethod(MessageName="TransferOrigin")]
    public static TransferOriginResponse TransferOrigin(Guid customerId)
    {
        TransferOriginResponse response = new TransferOriginResponse();
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);

        
        

        ClipCallReport.ResultOfString result = null;
        try
        {
            result = report.TransferOrigin(customerId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);

            return response;
        }
        if (result.Type == ClipCallReport.eResultType.Failure)
        {
            dbug_log.MessageLog(result.Messages);
            return response;
        }
        if(string.IsNullOrEmpty(result.Value))
            return response;
        response.IsSuccess = true;
        response.CurrentOrigin = result.Value;
        return response;
    }
    public class TransferOriginResponse
    {
        public TransferOriginResponse()
        {
            IsSuccess = false;
        }
        public bool IsSuccess { get; set; }
        public string CurrentOrigin { get; set; }
    }
}