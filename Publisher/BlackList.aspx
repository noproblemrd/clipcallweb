﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="BlackList.aspx.cs" Inherits="Publisher_BlackList" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="style.css" rel="stylesheet" type="text/css"/>
<link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../general.js"></script>
<script type="text/javascript" language="javascript">
    window.onload = appl_init;    
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager runat="server" ID="ScriptManager1" ></cc1:ToolkitScriptManager>
<uc1:Toolbox ID="Toolbox1" runat="server" />

	<h5><asp:Label ID="lblSubTitle" runat="server">Black list numbers</asp:Label></h5>

<div class="page-content minisite-content5">
    
    <div id="form-analyticsseg">
        <asp:UpdatePanel ID="UpdatePanelNumbers" runat="server" UpdateMode="Conditional" >
            <ContentTemplate>
                <asp:GridView ID="_GridViewNumbers" runat="server" AutoGenerateColumns="false" CssClass="data-table" AllowPaging="True" PageSize="15" onpageindexchanging="_GridViewBadWords_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <input type="checkbox" class="form-checkbox" runat="server" id="cb_all" /><!--  <asp:CheckBox ID="_cb_all" runat="server" Checked="false" />-->
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="cb_choose" CssClass="form-checkbox" runat="server" Checked="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="<%# lbl_BlackNum.Text %>"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPhoneNumber" runat="server" Text="<%# Bind('PhoneNumber') %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="__even" />
                    <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                    <AlternatingRowStyle CssClass="__odd" />
                 </asp:GridView>  
            </ContentTemplate>
        </asp:UpdatePanel> 
    </div>
</div>
  
<div id="divLoader" class="divLoader">  
    <table>
        <tr>
            <td>
                <img src="../UpdateProgress/ajax-loader.gif" alt=""/>
            </td>
        </tr>
    </table>
</div>

<asp:Label ID="lbl_BlackNum" runat="server" Text="Black number" Visible="false"></asp:Label>
<asp:Label ID="lbl_Delete" runat="server" Text="Delete" Visible="false"></asp:Label>
<asp:HiddenField ID="hf_confirmDelete" runat="server" Value="Do you want to delete" />
<asp:Label ID="lbl_NumAlreadyExists" runat="server" Text="The number already exists" Visible="false"></asp:Label>
<asp:Label ID="lbl_noreasultSearch" runat="server" Text="There are no results for this search" Visible="false"></asp:Label>
<asp:Label ID="lbl_invalidPhoneNumber" runat="server" Text="Invalid phone number" Visible="false"></asp:Label>
<asp:Label ID="lbl_OnlyDigits" runat="server" Text="please use digits" Visible="false"></asp:Label>
<asp:Label ID="_lbl_missing" runat="server" Text="Missing" Visible="false"></asp:Label>
</asp:Content>

