﻿
 function setUserNameSetting(name)
    {
        var labelUserName = '<%= (Master.FindControl("lblUserName")).ClientID %>';
        document.getElementById(labelUserName).innerHTML=name;
    }     
      
    var arrStepsNames=new Array();
    arrStepsNames[0]=""; 
     arrStepsNames[1]= '<%=Hidden_GeneralInfo.Value%>';
    arrStepsNames[2]= '<%=Hidden_SegmentsArea.Value%>';
    arrStepsNames[3]= '<%=Hidden_Cities.Value%>';
    arrStepsNames[4]= '<%=Hidden_Time.Value%>'; 
    arrStepsNames[5]= '<%=Hidden_MyCredits.Value%>';
    arrStepsNames[6]= '<%=Hidden_PaymentInfo.Value%>'; 
    arrStepsNames[7]='<%=Hidden_PublisherNotes.Value%>'; 
    arrStepsNames[8]='<%=Hidden_Audit.Value%>'; 
    
    
    function witchAlreadyStep(level)
    {      
     

        var steps=document.getElementById("ulSteps");
        var alreadyStep=level*1+1;
        
        for(i=0;i<steps.childNodes.length;i++)
        {
            if(steps.childNodes[i].id!=undefined)
            {
                liNumber=steps.childNodes[i].id.replace('liStep','');
                listStepObj=document.getElementById('liStep' + liNumber);
                
                if(liNumber<7)
                {
                   
                    if(liNumber<=alreadyStep)
                    {      
                                   
                       listStepObj.innerHTML="<a id='linkStep" +
                        liNumber + "' href='#' style='cursor:hand;' class='mouseover' onclick='javascript:step" +
                         liNumber + "();'><span class='mouseover'>" + liNumber + 
                         "</span><literal>" + arrStepsNames[liNumber]+ "</literal></a>";
                    }    
                    else
                    {
                       listStepObj.innerHTML="<a id='linkStep" +
                        liNumber + "' href='#' style='cursor:text;'  onclick='javascript:return false;'><span>" + liNumber + 
                         "</span><literal>" + arrStepsNames[liNumber]+ "</literal></a>";
                    } 
                }
                
                else
                {                    
                       listStepObj.innerHTML="<a id='linkStep" +
                        liNumber + "' href='#' style='cursor:hand;' class='mouseover' onclick='javascript:step" +
                         liNumber + "();'><span class='mouseover'>" + liNumber + 
                         "</span><literal>" + arrStepsNames[liNumber]+ "</literal></a>";
                }
                
                               
            }            
        }          
      
    }
    
    function clearStepActive()
    {      
        for(i=1;i<9;i++)
        {
            
            linkObj=document.getElementById('linkStep' + i);
            
            if(linkObj.className=="active mouseover")
                linkObj.className="mouseover";            
        }     
      
    }
    
    function setInitRegistrationStep(level)
    {       
       
        
        switch(level)
        {    
             
           case 1:
             step2();
             break;
             
           case 2:
             step3();
             break;
             
           case 3:
             step4();
             break;
             
           case 4:
             step5();
             break;
             
           case 5:
             step6();
             break;
             
           case 6:
             step1();
             break;    
           
           default:
            break;  
        }
    }
    
    function init()
    {
        //alert(document.getElementById("<%=Hidden_GeneralInfo.ClientID%>").value);
      
        var ifFinished=false;
        if(ifFinished)
            witchAlreadyStep(1);
       
    }
    
    function setLinkStepActive(linkId)
    {    
       
      document.getElementById(linkId).className="active mouseover";      
    }    
   
    function step1()
    {     
        document.getElementById("iframe").src='<%=ResolveUrl("~")%>Management/professionalDetails.aspx';  
        document.getElementById("iframe").height='670px';      
    }    
   
    
    function step2()
    { 
        document.getElementById("iframe").src='<%=ResolveUrl("~")%>Management/professionChooseProfessions.aspx';  
        document.getElementById("iframe").height='670px';  
    }
    
    function step3()
    {  
        document.getElementById("iframe").src='<%=ResolveUrl("~")%>Management/professionalCities2.aspx';  
        document.getElementById("iframe").height='670px';    
    }
    
    function step4()
    {  
        document.getElementById("iframe").src='<%=ResolveUrl("~")%>Management/professionalDates.aspx';  
        document.getElementById("iframe").height='850px';    
    }
    
    function step5()
    {              
        document.getElementById("iframe").src='<%=ResolveUrl("~")%>Management/professionalCallPurchase.aspx';          
        document.getElementById("iframe").height='670px';        
    }
    
    function step6()
    {               
       document.getElementById("iframe").src='<%=ResolveUrl("~")%>Management/professionalPayment.aspx';  
       document.getElementById("iframe").height='860px';       
    }  
    
    function step7()
    {           
        document.getElementById("iframe").src='<%=ResolveUrl("~")%>Publisher/PublisherNotes.aspx';  
        document.getElementById("iframe").height='850px';
    }
    
    function step8()
    {           
        document.getElementById("iframe").src='<%=ResolveUrl("~")%>Publisher/PublisherAudit.aspx';  
        document.getElementById("iframe").height='900px';
    }
    
    window.onload=init;
    
    