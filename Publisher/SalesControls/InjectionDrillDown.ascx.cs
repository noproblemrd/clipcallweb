﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_SalesControls_InjectionDrillDown : System.Web.UI.UserControl
{
    protected const string NUMBER_FORMAT = "{0:#,0.##}";
    public int row_num { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void LoadData(WebReferenceReports.InjectionReportDrillDownResponse[] rows)
    {        
        DataTable data = new DataTable();
        data.Columns.Add("Campaign");
        data.Columns.Add("DAUS");
        data.Columns.Add("InjectionPercent");
        data.Columns.Add("Revenue");
        data.Columns.Add("DUV");
        data.Columns.Add("PPI_DAU");


        foreach (WebReferenceReports.InjectionReportDrillDownResponse dirr in rows)
        {
            DataRow rowDates = data.NewRow();
            rowDates["Campaign"] = dirr.OriginName;
            rowDates["DAUS"] = dirr.DAUS;
            rowDates["InjectionPercent"] = (dirr.InjectionPercent < 0) ? "-" : string.Format(NUMBER_FORMAT, dirr.InjectionPercent) + "%";
            rowDates["Revenue"] = string.Format(NUMBER_FORMAT, dirr.Revenue);
            rowDates["DUV"] = (dirr.DUV < 0) ? "-" : string.Format(NUMBER_FORMAT, dirr.DUV);
            rowDates["PPI_DAU"] = dirr.PPI_DAU;            
            data.Rows.Add(rowDates);
        }
        row_num = rows.Length;
        _GridView.DataSource = data;
        _GridView.DataBind();

    }
}