﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_SalesControls_ConversionControl : UserControlTool
{
    private bool _WithStop = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetClickOnTable", "SetClickOnTable();", true);
    }
    public void LoadData(DataTable data, bool WithStop, int CountryId)
    {
        _WithStop = WithStop;
        _Repeater.DataSource = data;
        _Repeater.DataBind();
        hf_CountryId.Value = CountryId.ToString();
        this.DataBind();
    }
    
    protected string GetConversionDetails
    {
        get {
            return (_WithStop) ? ResolveUrl("~/Publisher/PagingService.asmx/GetConversionDetails2") :
            ResolveUrl("~/Publisher/PagingService.asmx/GetConversionDetails"); 
        }
    }
    DataTable dataV
    {
        get { return (Session["ConversionTable"] == null) ? null : (DataTable)Session["ConversionTable"]; }
        set { Session["ConversionTable"] = value; }
    }
    int PageIndex
    {
        get { return (Session["PageIndexConversionTable"] == null) ? 0 : (int)Session["PageIndexConversionTable"]; }
        set { Session["PageIndexConversionTable"] = value; }
    }
}