﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConversionDrillDown.ascx.cs" Inherits="Publisher_SalesControls_ConversionDrillDown" %>
<asp:GridView ID="_GridView" runat="server" CssClass="data-table2 table-width" AutoGenerateColumns="false">
<RowStyle CssClass="even" />
<AlternatingRowStyle CssClass="odd" />
<Columns>
    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1" runat="server" Text="<%# lbl_Origin.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label2" runat="server" Text="<%# Bind('Origin') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1051" runat="server" Text="<%# lbl_UniqueInstalls.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1052" runat="server" Text="<%# Bind('UniqueInstalls') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1000" runat="server" Text="<%# lbl_DAUS.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1002" runat="server" Text="<%# Bind('DAUS') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

     <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1300" runat="server" Text="<%# lbl_LogicHits.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1302" runat="server" Text="<%# Bind('LogicHits') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label3" runat="server" Text="<%# lbl_ExposureCount.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label4" runat="server" Text="<%# Bind('ExposureCount') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

     <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1005" runat="server" Text="<%# lbl_TotalRequests.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1006" runat="server" Text="<%# Bind('TotalRequests') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label5" runat="server" Text="<%# lbl_RequestCount.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label6" runat="server" Text="<%# Bind('RequestCount') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label7" runat="server" Text="<%# lbl_UpsalesCount.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label8" runat="server" Text="<%# Bind('UpsalesCount') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    
    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label77" runat="server" Text="<%# lbl_Stoped.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label78" runat="server" Text="<%# Bind('Stoped') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label701" runat="server" Text="<%# lbl_Hits_KDAUs.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label702" runat="server" Text="<%# Bind('ExpHits_KDAUs') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1009" runat="server" Text="<%# lbl_Exp_KDAUs.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1010" runat="server" Text="<%# Bind('Exp_KDAUs') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

     <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1701" runat="server" Text="<%# lbl_LogicHits_DAUs.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1702" runat="server" Text="<%# Bind('LogicHits_DAUs') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

     <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1771" runat="server" Text="<%# lbl_LogicHits_Exposures.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1772" runat="server" Text="<%# Bind('LogicHits_Exposure') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label703" runat="server" Text="<%# lbl_Hits_Exposures.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label704" runat="server" Text="<%# Bind('ExpHits_Exposures') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1029" runat="server" Text="<%# lbl_Req_KDAUs.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1030" runat="server" Text="<%# Bind('Req_KDAUs') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label9" runat="server" Text="<%# lbl_TotalRequests_Exposures.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label10" runat="server" Text="<%# Bind('TotalRequests_Exposures') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1011" runat="server" Text="<%# lbl_OriginalRequests_Exposures.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1012" runat="server" Text="<%# Bind('OriginalRequests_Exposures') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>    

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label11" runat="server" Text="<%# lbl_CallCount.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label12" runat="server" Text="<%# Bind('CallCount') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label13" runat="server" Text="<%# lbl_CallsPercent.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label14" runat="server" Text="<%# Bind('CallsPercent') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

     <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1061" runat="server" Text="<%# lbl_SaleRate.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1062" runat="server" Text="<%# Bind('SaleRate') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1063" runat="server" Text="<%# lbl_Money_KDAUs.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1064" runat="server" Text="<%# Bind('Money_KDAUs') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1863" runat="server" Text="<%# lbl_CPM.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1864" runat="server" Text="<%# Bind('CPM') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField>
    <HeaderTemplate>
        <asp:Label ID="Label1463" runat="server" Text="<%# lbl_RPM.Text %>"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <asp:Label ID="Label1464" runat="server" Text="<%# Bind('RPM') %>"></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

     
</Columns>
</asp:GridView>
<asp:Label ID="lbl_Origin" runat="server" Text="Origin" Visible="false"></asp:Label>  
<asp:Label ID="lbl_ExposureCount" runat="server" Text="Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_RequestCount" runat="server" Text="Requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_TotalRequests" runat="server" Text="Total requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_UpsalesCount" runat="server" Text="Upsold" Visible="false"></asp:Label>
<asp:Label ID="lbl_CallCount" runat="server" Text="Calls" Visible="false"></asp:Label>
<asp:Label ID="lbl_TotalRequests_Exposures" runat="server" Text="Total requests > Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_CallsPercent" runat="server" Text="Calls percent" Visible="false"></asp:Label> 
<asp:Label ID="lbl_Stoped" runat="server" Text="Stoped" Visible="false"></asp:Label> 
<asp:Label ID="lbl_DAUS" runat="server" Text="DAUS" Visible="false"></asp:Label> 
<asp:Label ID="lbl_Exp_KDAUs" runat="server" Text="Exp/KDAUs" Visible="false"></asp:Label> 
<asp:Label ID="lbl_Req_KDAUs" runat="server" Text="Req/KDAUs" Visible="false"></asp:Label> 
<asp:Label ID="lbl_OriginalRequests_Exposures" runat="server" Text="Original requests > Exposures" Visible="false"></asp:Label> 
<asp:Label ID="lbl_UniqueInstalls" runat="server" Text="Live installs" Visible="false"></asp:Label>

<asp:Label ID="lbl_SaleRate" runat="server" Text="Sale rate" Visible="false"></asp:Label> 
<asp:Label ID="lbl_Money_KDAUs" runat="server" Text="$/KDAUs" Visible="false"></asp:Label> 

<asp:Label ID="lbl_Hits_KDAUs" runat="server" Text="Hits/KDAUs" Visible="false"></asp:Label>
<asp:Label ID="lbl_Hits_Exposures" runat="server" Text="Hits/Exposures" Visible="false"></asp:Label>

<asp:Label ID="lbl_LogicHits" runat="server" Text="Logic hits" Visible="false"></asp:Label>
<asp:Label ID="lbl_LogicHits_DAUs" runat="server" Text="LogicHits/DAUs" Visible="false"></asp:Label>

<asp:Label ID="lbl_LogicHits_Exposures" runat="server" Text="LogicHits/Exposures" Visible="false"></asp:Label>

<asp:Label ID="lbl_CPM" runat="server" Text="CPM" Visible="false"></asp:Label>
<asp:Label ID="lbl_RPM" runat="server" Text="RPM" Visible="false"></asp:Label>
