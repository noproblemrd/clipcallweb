﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NP_DistributionReportOrigin.ascx.cs" Inherits="Publisher_SalesControls_NP_DistributionReportOrigin" %>

<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
    <RowStyle CssClass="even" />
    <AlternatingRowStyle CssClass="odd" />
    <Columns>
        <asp:TemplateField SortExpression="Date">
            <HeaderTemplate>
                <asp:Label ID="Label1" runat="server" Text="Origin"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDate" runat="server" Text="<%# Bind('Date') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField SortExpression="Installs">
            <HeaderTemplate>
                <asp:Label ID="Label2" runat="server" Text="Installs"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblInstalls" runat="server" Text="<%# Bind('Installs') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

         <asp:TemplateField SortExpression="UniqueInstalls">
            <HeaderTemplate>
                <asp:Label ID="Label3" runat="server" Text="Unique Installs"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblUniqueInstalls" runat="server" Text="<%# Bind('UniqueInstalls') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

         <asp:TemplateField SortExpression="EffectiveInstalls">
            <HeaderTemplate>
                <asp:Label ID="Label4" runat="server" Text="Effective Installs"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblEffectiveInstalls" runat="server" Text="<%# Bind('EffectiveInstalls') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField SortExpression="CalcCost">
            <HeaderTemplate>
                <asp:Label ID="Label5" runat="server" Text="Calc Cost"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblCalcCost" runat="server" Text="<%# Bind('CalcCost') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField SortExpression="FixCost">
            <HeaderTemplate>
                <asp:Label ID="Label6" runat="server" Text="Fix Cost"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblFixCost" runat="server" Text="<%# Bind('FixCost') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

    </Columns>
</asp:GridView>
