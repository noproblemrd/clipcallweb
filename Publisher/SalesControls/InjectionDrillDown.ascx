﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InjectionDrillDown.ascx.cs" Inherits="Publisher_SalesControls_InjectionDrillDown" %>

 <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
    CssClass="data-table"  >
<RowStyle CssClass="_even" />
<AlternatingRowStyle CssClass="_odd" />
<Columns>
    <asp:TemplateField SortExpression="Campaign">
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="Campaign"></asp:Label> 
                                
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label2" runat="server" Text="<%# Bind('Campaign') %>" CssClass="-date-"></asp:Label>        
                                               
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="DAUS">
        <HeaderTemplate>
            <asp:Label ID="Label31" runat="server" Text="DAUs"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label32" runat="server" Text="<%# Bind('DAUS') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

        <asp:TemplateField SortExpression="InjectionPercent">
        <HeaderTemplate>
            <asp:Label ID="Label010" runat="server" Text="Injection Percent"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label002" runat="server" Text="<%# Bind('InjectionPercent') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Revenue">
        <HeaderTemplate>
            <asp:Label ID="Label80" runat="server" Text="Revenue"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label82" runat="server" Text="<%# Bind('Revenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="DUV">
        <HeaderTemplate>
            <asp:Label ID="Label3" runat="server" Text="DUV"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label4" runat="server" Text="<%# Bind('DUV') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="PPI_DAU">
        <HeaderTemplate>
            <asp:Label ID="Label5" runat="server" Text="PPI_DAU"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('PPI_DAU') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

       
</Columns>
</asp:GridView>
