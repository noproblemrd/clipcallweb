﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_SalesControls_InstallationDrillDown : System.Web.UI.UserControl
{
    protected const string NUMBER_FORMAT = "{0:#,0.##}";
    public int row_num { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    public void LoadData(WebReferenceReports.DistributionInstallationReportResponseDrillDown rows)
    {
        lbl_DAUS_Percent.Text = lbl_DAUS.Text + "%";
        DataTable data = new DataTable();
        data.Columns.Add("Origin");
        data.Columns.Add("Install");
        data.Columns.Add("UniqeInstall");
        data.Columns.Add("EffectiveUniqeInstalls");
        data.Columns.Add("TotalInstalls");
        data.Columns.Add("Uninstall");
        data.Columns.Add("DAUS");
        data.Columns.Add("DAUS_Percent");
        data.Columns.Add("NPRevenue");
        data.Columns.Add("NonNPRevenue");
        data.Columns.Add("DealplyRevenue");
        data.Columns.Add("JollywalletRevenue");
        data.Columns.Add("InterYieldRevenue");
        data.Columns.Add("SuperfishRevenue");
        data.Columns.Add("RevizerPopupRevenue");
        data.Columns.Add("IntextRevenue");
        data.Columns.Add("FirstOfferzRevenue");

        data.Columns.Add("Revenue");

        //   data.Columns.Add("TotalRevenue");
        data.Columns.Add("Cost");
        data.Columns.Add("AULT");
        data.Columns.Add("AMUV_NoProblem");
        data.Columns.Add("DUV_NoProblem");
        data.Columns.Add("AMUV");
        data.Columns.Add("DUV");
        data.Columns.Add("ROI");





        foreach (WebReferenceReports.DistributionInstallationReportRowDrillDown dirr in rows.Data)
        {
            DataRow rowDates = data.NewRow();
            rowDates["Origin"] = dirr.OriginName;
            rowDates["Install"] = dirr.Installations;
            rowDates["UniqeInstall"] = dirr.UniqeInstallations;
            rowDates["EffectiveUniqeInstalls"] = dirr.EffectiveUniqueInstalls;
            rowDates["TotalInstalls"] = dirr.TotalUniqueInstallation;
            rowDates["Uninstall"] = dirr.Uninstallation;
            rowDates["DAUS"] = dirr.DAUS;
            rowDates["DAUS_Percent"] = string.Format(NUMBER_FORMAT, dirr.DAUS_TotalInstallation_percent) + "%";
            rowDates["NPRevenue"] = string.Format(NUMBER_FORMAT, dirr.NoProblemRevenue);
            rowDates["NonNPRevenue"] = string.Format(NUMBER_FORMAT, dirr.NonNoProblemRevenue);
            rowDates["DealplyRevenue"] = string.Format(NUMBER_FORMAT, dirr.DealplyRevenue);
            rowDates["JollywalletRevenue"] = string.Format(NUMBER_FORMAT, dirr.JollywalletRevenue);
            rowDates["InterYieldRevenue"] = string.Format(NUMBER_FORMAT, dirr.InterYieldRevenue);
            rowDates["SuperfishRevenue"] = string.Format(NUMBER_FORMAT, dirr.SuperfishRevenue);
            rowDates["RevizerPopupRevenue"] = string.Format(NUMBER_FORMAT, dirr.RevizerPopupRevenue);
            rowDates["IntextRevenue"] = string.Format(NUMBER_FORMAT, dirr.IntextRevenue);
            rowDates["FirstOfferzRevenue"] = string.Format(NUMBER_FORMAT, dirr.FirstOfferzRevenue);
            rowDates["Revenue"] = string.Format(NUMBER_FORMAT, dirr.Revenue);
            rowDates["Cost"] = string.Format(NUMBER_FORMAT, dirr.Cost);
            rowDates["AULT"] = string.Format(NUMBER_FORMAT, dirr.AULT);
            rowDates["AMUV_NoProblem"] = string.Format(NUMBER_FORMAT, dirr.AMUV_NoProblem);
            rowDates["DUV_NoProblem"] = string.Format(NUMBER_FORMAT, dirr.DUV_NoProblem);
            rowDates["AMUV"] = string.Format(NUMBER_FORMAT, dirr.AMUV);
            rowDates["DUV"] = string.Format(NUMBER_FORMAT, dirr.DUV);
            rowDates["ROI"] = string.Format(NUMBER_FORMAT, dirr.ROI) + "%";
            data.Rows.Add(rowDates);
        }
        row_num = rows.Data.Length;
        _GridView.DataSource = data;
        _GridView.DataBind();

    }
}