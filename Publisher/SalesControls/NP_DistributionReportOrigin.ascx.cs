﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_SalesControls_NP_DistributionReportOrigin : System.Web.UI.UserControl
{
    protected const string NUMBER_FORMAT = "{0:#,0.##}";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void LoadData(WebReferenceReports.NP_DistributionReportOriginResponse[] rows)
    {
        DataTable data = new DataTable();
        data.Columns.Add("Date");
        data.Columns.Add("Installs");
        data.Columns.Add("UniqueInstalls");
        data.Columns.Add("EffectiveInstalls");
        data.Columns.Add("CalcCost");
        data.Columns.Add("FixCost");


        foreach (WebReferenceReports.NP_DistributionReportOriginResponse dirr in rows)
        {
            DataRow rowDates = data.NewRow();
            rowDates["Date"] = dirr.Date.ToString("dd/MM/yyyy");
            rowDates["Installs"] = dirr.Installs;
            rowDates["UniqueInstalls"] = dirr.UniqueInstalls;
            rowDates["EffectiveInstalls"] = dirr.EffectiveInstalls;
            rowDates["CalcCost"] = string.Format(NUMBER_FORMAT, dirr.CalcCost);
            rowDates["FixCost"] = string.Format(NUMBER_FORMAT, dirr.FixCost);
            data.Rows.Add(rowDates);
        }
        dataV = data;
        //row_num = rows.Length;
        _GridView.DataSource = data;
        _GridView.DataBind();

    }
    public string CreateExcel()
    {
        ToExcel toexcel = new ToExcel(null, "NP_DistributionReportOrigin");
        DataTable data = GetDataTable.GetDataTableFromDataTableAndGridView(dataV, _GridView);
        return toexcel.CreateExcel(data);
    }
    DataTable dataV
    {
        get { return (Session["NP_DistributionReporOrigintData"] == null) ? null : (DataTable)Session["NP_DistributionReporOrigintData"]; }
        set { Session["NP_DistributionReporOrigintData"] = value; }
    }
}