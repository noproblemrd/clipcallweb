﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PplControl.ascx.cs" Inherits="Publisher_SalesControls_PplControl" %>

    <table class="data-table2" runat="server" id="TablePpl" visible="false">
    <tr>
        <th>
              <asp:Label ID="Label1" runat="server" Text="<%# lbl_Category.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label17" runat="server" Text="<%# lbl_Highest_PPL.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Avergae_PPL.Text %>"></asp:Label>
        </th>
        <th>
             <asp:Label ID="Label5" runat="server" Text="<%# lbl_Lowest_PPL.Text %>"></asp:Label>
        </th>        
    </tr>

    <tr class="-Main-Details">
        <td  class="-date">
           <asp:Label ID="lblCategory" runat="server"></asp:Label> 
            
        </td>
        <td>
             <asp:Label ID="lblHighestPPL" runat="server"></asp:Label>
        </td>
        <td>
             <asp:Label ID="lblAvergaePPL" runat="server"></asp:Label>
        </td>
        <td>
            <asp:Label ID="lblLowestPPL" runat="server"></asp:Label> 
        </td>
        
    </tr>
    <tr class="-tr-details">
        <td colspan="4">
             <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />                
                <FooterStyle CssClass="footer"  />
                <Columns>
            
                    <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# lbl_Buyer.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('Name') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# lbl_Price.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('Price') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# lbl_Interface.Text %>"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('Interface') %>"></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            
            </asp:GridView>
            
        </td>    
    </tr>

    </table>


<asp:HiddenField ID="hf_CountryId" runat="server" />

<asp:Label ID="lbl_Category" runat="server" Text="Category" Visible="false"></asp:Label>
<asp:Label ID="lbl_Highest_PPL" runat="server" Text="Highest PPL" Visible="false"></asp:Label>
<asp:Label ID="lbl_Avergae_PPL" runat="server" Text="Avergae PPL" Visible="false"></asp:Label>
<asp:Label ID="lbl_Lowest_PPL" runat="server" Text="Lowest PPL" Visible="false"></asp:Label>

<asp:Label ID="lbl_Buyer" runat="server" Text="Buyer" Visible="false"></asp:Label>
<asp:Label ID="lbl_Price" runat="server" Text="Price" Visible="false"></asp:Label>
<asp:Label ID="lbl_Interface" runat="server" Text="Interface" Visible="false"></asp:Label>

