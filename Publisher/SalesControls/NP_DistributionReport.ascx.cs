﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_SalesControls_NP_DistributionReport : System.Web.UI.UserControl
{
    protected const string NUMBER_FORMAT = "{0:#,0.##}";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void LoadData(WebReferenceReports.NP_DistributionReportResponse[] rows)
    {
        DataTable data = new DataTable();
        data.Columns.Add("Origin");
        data.Columns.Add("Installs");
        data.Columns.Add("UniqueInstalls");
        data.Columns.Add("EffectiveInstalls");
        data.Columns.Add("Cost");


        foreach (WebReferenceReports.NP_DistributionReportResponse dirr in rows)
        {
            DataRow rowDates = data.NewRow();
            rowDates["Origin"] = dirr.OriginName;
            rowDates["Installs"] = dirr.Installs;
            rowDates["UniqueInstalls"] = dirr.UniqueInstalls;
            rowDates["EffectiveInstalls"] = dirr.EffectiveInstalls;
            rowDates["Cost"] = string.Format(NUMBER_FORMAT, dirr.Cost);
            data.Rows.Add(rowDates);
        }
        dataV = data;
        //row_num = rows.Length;
        _GridView.DataSource = data;
        _GridView.DataBind();

    }
    public string CreateExcel()
    {
        ToExcel toexcel = new ToExcel(null, "NP_DistributionReport");
        DataTable data = GetDataTable.GetDataTableFromDataTableAndGridView(dataV, _GridView);
        return toexcel.CreateExcel(data);
    }
    DataTable dataV
    {
        get { return (Session["NP_DistributionReportData"] == null) ? null : (DataTable)Session["NP_DistributionReportData"]; }
        set { Session["NP_DistributionReportData"] = value; }
    }
}