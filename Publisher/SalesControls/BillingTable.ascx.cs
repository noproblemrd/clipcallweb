﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_SalesControls_BillingTable : System.Web.UI.UserControl
{
    const string SCRAMBEL = "simsim";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void BindReport(WebReferenceReports.BrokersLeadsDataForReportResponse[] datas)
    {
        PageIndex = 0;
        dataV = null;
        if (datas == null)
            return;
        this.EnableViewState = false;
        PageSetting _page = (PageSetting)Page;
        DataTable _data = new DataTable();
        _data.Columns.Add("BrokersId");
        _data.Columns.Add("CreatedOn");
        _data.Columns.Add("Duration");
        _data.Columns.Add("HeadingName");
        _data.Columns.Add("IncidentId");
        _data.Columns.Add("IsMarkedAsBadManually");
        _data.Columns.Add("Notes");
        _data.Columns.Add("Price");
        _data.Columns.Add("Record");
        _data.Columns.Add("Region");
        _data.Columns.Add("TicketNumber");
        _data.Columns.Add("Phone");
        _data.Columns.Add("HasRecord", typeof(bool));
        foreach (WebReferenceReports.BrokersLeadsDataForReportResponse data in datas)
        {
            DataRow row = _data.NewRow();           
            row["BrokersId"] = data.BrokersId;
            row["CreatedOn"] = string.Format(_page.siteSetting.DateFormat, data.CreatedOn) + " " + string.Format(_page.siteSetting.TimeFormat, data.CreatedOn);
            row["Duration"] = data.Duration;
            row["HeadingName"] = data.HeadingName;
            row["IncidentId"] = data.IncidentId.ToString();
            row["IsMarkedAsBadManually"] = (data.IsMarkedAsBadManually ? "Yes" : "No");
            row["Notes"] = data.Notes;
            row["Price"] = string.Format(_page.GetNumberFormat, data.Price);
            row["Region"] = data.Region;
            row["TicketNumber"] = data.TicketNumber;
            if (!string.IsNullOrEmpty(data.RecordingLocation))
            {
                row["HasRecord"] = true;
                string EncodePath = EncryptString.Encrypt_String(data.RecordingLocation, SCRAMBEL);
                row["Record"] = "javascript:Get_Record('" + EncodePath + "');";
            }
            else
                row["HasRecord"] = false;
            row["Phone"] = data.Phone;
            _data.Rows.Add(row);
        }
        
        dataV = _data;
        IEnumerable<DataRow> query = (from x in _data.AsEnumerable()
                                      select x).Take(20);

        _GridView.DataSource = (query.Count() == 0) ? null : query.CopyToDataTable();
        _GridView.DataBind();


    }
    public bool SetDataByPage()
    {
        DataTable data = dataV;
        if (data == null)
            return false;
        PageIndex = PageIndex + 1;
        int _index = PageIndex;
        int _min = (_index * 20);
        IEnumerable<DataRow> data_row = (from x in data.AsEnumerable()
                                         select x).Skip(_min).Take(20);

        if (data_row.Count() == 0)
            return false;
        _GridView.DataSource = data_row.CopyToDataTable();
        _GridView.DataBind();
        return true;
    }
    public string CreateExcel()
    {
        ToExcel toexcel = new ToExcel(null, "Report");
        DataTable data = GetDataTable.GetDataTableFromDataTableAndGridView(dataV, _GridView);
        data.Columns.Remove("Record");
        return toexcel.CreateExcel(data);
    }
   
    DataTable dataV
    {
        get { return (Session["BillingTable"] == null) ? null : (DataTable)Session["BillingTable"]; }
        set { Session["BillingTable"] = value; }
    }
    int PageIndex
    {
        get { return (Session["PageIndexBillingTable"] == null) ? 0 : (int)Session["PageIndexBillingTable"]; }
        set { Session["PageIndexBillingTable"] = value; }
    }
}