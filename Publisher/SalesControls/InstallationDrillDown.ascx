﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InstallationDrillDown.ascx.cs" Inherits="Publisher_SalesControls_InstallationDrillDown" %>


  <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
    CssClass="data-table"  >
<RowStyle CssClass="_even" />
<AlternatingRowStyle CssClass="_odd" />
<Columns>
    <asp:TemplateField SortExpression="Origin">
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Origin.Text %>"></asp:Label> 
                                
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label2" runat="server" Text="<%# Bind('Origin') %>" CssClass="-date-"></asp:Label>        
                                               
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Install">
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_Install.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label2" runat="server" Text="<%# Bind('Install') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="UniqeInstall">
        <HeaderTemplate>
            <asp:Label ID="Label010" runat="server" Text="<%# lbl_UniqeInstall.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label002" runat="server" Text="<%# Bind('UniqeInstall') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="EffectiveUniqeInstalls">
        <HeaderTemplate>
            <asp:Label ID="Label910" runat="server" Text="<%# lbl_EffectiveUniqeInstalls.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label902" runat="server" Text="<%# Bind('EffectiveUniqeInstalls') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="TotalInstalls">
        <HeaderTemplate>
            <asp:Label ID="Label80" runat="server" Text="<%# lbl_TotalInstalls.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label82" runat="server" Text="<%# Bind('TotalInstalls') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Uninstall">
        <HeaderTemplate>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Uninstall.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label4" runat="server" Text="<%# Bind('Uninstall') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="DAUS">
        <HeaderTemplate>
            <asp:Label ID="Label5" runat="server" Text="<%# lbl_DAUS.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('DAUS') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

        <asp:TemplateField SortExpression="DAUS_Percent">
        <HeaderTemplate>
            <asp:Label ID="Label85" runat="server" Text="<%# lbl_DAUS_Percent.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label86" runat="server" Text="<%# Bind('DAUS_Percent') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

                       

    <asp:TemplateField SortExpression="NPRevenue">
        <HeaderTemplate>
            <asp:Label ID="Label9" runat="server" Text="<%# lbl_NPRevenue.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label10" runat="server" Text="<%# Bind('NPRevenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="NonNPRevenue">
        <HeaderTemplate>
                                
            <asp:Label ID="Label89" runat="server" Text="<%# lbl_NonNPRevenue.Text %>"></asp:Label>                                
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label90" runat="server" Text="<%# Bind('NonNPRevenue') %>"></asp:Label>                                 
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="DealplyRevenue">
        <HeaderTemplate>
            <asp:Label ID="Label11" runat="server" Text="<%# lbl_DealplyRevenue.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label12" runat="server" Text="<%# Bind('DealplyRevenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="JollywalletRevenue">
        <HeaderTemplate>
            <asp:Label ID="Label13" runat="server" Text="<%# lbl_JollywalletRevenue.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label14" runat="server" Text="<%# Bind('JollywalletRevenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="InterYieldRevenue">
        <HeaderTemplate>
            <asp:Label ID="Label15" runat="server" Text="<%# lbl_InterYieldRevenue.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label16" runat="server" Text="<%# Bind('InterYieldRevenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="SuperfishRevenue">
        <HeaderTemplate>
            <asp:Label ID="Label17" runat="server" Text="<%# lbl_SuperfishRevenue.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label18" runat="server" Text="<%# Bind('SuperfishRevenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

        <asp:TemplateField SortExpression="RevizerPopupRevenue">
        <HeaderTemplate>
            <asp:Label ID="Label117" runat="server" Text="<%# lbl_RevizerPopupRevenue.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label118" runat="server" Text="<%# Bind('RevizerPopupRevenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="IntextRevenue">
        <HeaderTemplate>
            <asp:Label ID="Label1217" runat="server" Text="<%# lbl_IntextRevenue.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label1218" runat="server" Text="<%# Bind('IntextRevenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="FirstOfferzRevenue">
        <HeaderTemplate>
            <asp:Label ID="Label1267" runat="server" Text="<%# lbl_FirstOfferzRevenue.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label1268" runat="server" Text="<%# Bind('FirstOfferzRevenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="TotalRevenue">
        <HeaderTemplate>
            <asp:Label ID="Label19" runat="server" Text="<%# lbl_TotalRevenue.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label20" runat="server" Text="<%# Bind('Revenue') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Cost">
        <HeaderTemplate>
            <asp:Label ID="Label21" runat="server" Text="<%# lbl_Cost.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label22" runat="server" Text="<%# Bind('Cost') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="AULT">
        <HeaderTemplate>
            <asp:Label ID="Label91" runat="server" Text="<%# lbl_AULT.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label92" runat="server" Text="<%# Bind('AULT') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="AMUV_NoProblem">
        <HeaderTemplate>
            <asp:Label ID="Label93" runat="server" Text="<%# lbl_AMUV_NoProblem.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label94" runat="server" Text="<%# Bind('AMUV_NoProblem') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="DUV_NoProblem">
        <HeaderTemplate>
            <asp:Label ID="Label95" runat="server" Text="<%# lbl_DUV_NoProblem.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label96" runat="server" Text="<%# Bind('DUV_NoProblem') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="AMUV">
        <HeaderTemplate>
            <asp:Label ID="Label97" runat="server" Text="<%# lbl_AMUV.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label98" runat="server" Text="<%# Bind('AMUV') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="DUV">
        <HeaderTemplate>
            <asp:Label ID="Label103" runat="server" Text="<%# lbl_DUV.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label104" runat="server" Text="<%# Bind('DUV') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="ROI">
        <HeaderTemplate>
            <asp:Label ID="Label23" runat="server" Text="<%# lbl_ROI.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label24" runat="server" Text="<%# Bind('ROI') %>"></asp:Label>                             
        </ItemTemplate>
    </asp:TemplateField>
</Columns>
</asp:GridView>


    <asp:Label ID="lbl_Origin" runat="server" Text="Origin" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Install" runat="server" Text="Installs" Visible="false"></asp:Label>
    <asp:Label ID="lbl_UniqeInstall" runat="server" Text="Unique Installs" Visible="false"></asp:Label>
    <asp:Label ID="lbl_EffectiveUniqeInstalls" runat="server" Text="Effective Uniqe Installs" Visible="false"></asp:Label>
    
    <asp:Label ID="lbl_TotalInstalls" runat="server" Text="Total Installs" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Uninstall" runat="server" Text="Uninstalls" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DAUS" runat="server" Text="DAUs" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DAUS_Percent" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lbl_NPRevenue" runat="server" Text="NP Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_NonNPRevenue" runat="server" Text="NonNP Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DealplyRevenue" runat="server" Text="Dealply Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_JollywalletRevenue" runat="server" Text="Jollywallet Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_InterYieldRevenue" runat="server" Text="newtab rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_SuperfishRevenue" runat="server" Text="Superfish Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_RevizerPopupRevenue" runat="server" Text="RevizerPopup Rev" Visible="false"></asp:Label>
     <asp:Label ID="lbl_IntextRevenue" runat="server" Text="Intext Rev" Visible="false"></asp:Label>
     <asp:Label ID="lbl_FirstOfferzRevenue" runat="server" Text="FirstOfferz rev" Visible="false"></asp:Label>
     
    
    <asp:Label ID="lbl_TotalRevenue" runat="server" Text="Total Rev" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Cost" runat="server" Text="Cost" Visible="false"></asp:Label>
    <asp:Label ID="lbl_AULT" runat="server" Text="AULT" Visible="false"></asp:Label>
    <asp:Label ID="lbl_AMUV_NoProblem" runat="server" Text="NP AMUV" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DUV_NoProblem" runat="server" Text="NP DUV" Visible="false"></asp:Label>
    <asp:Label ID="lbl_AMUV" runat="server" Text="Total AMUV" Visible="false"></asp:Label>
    <asp:Label ID="lbl_DUV" runat="server" Text="Total DUV" Visible="false"></asp:Label>
    <asp:Label ID="lbl_ROI" runat="server" Text="ROI" Visible="false"></asp:Label>

