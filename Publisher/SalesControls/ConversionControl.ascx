﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConversionControl.ascx.cs" Inherits="Publisher_SalesControls_ConversionControl" %>
<script type="text/javascript">
    function SetClickOnTable() {
        var _index = 0;
        $('table.data-table2').find('tr.-Main-Details').each(function () {
            var _class = (_index % 2 == 0) ? "even" : "odd";
            $(this).addClass(_class);
            _index++;
            $(this).unbind('click');
            $(this).click(function () {
                if ($(this).next('.-tr-details').is(':visible'))
                    $(this).next('.-tr-details').hide();
                else
                    GetDetails($(this));
            });
        });
    };
    function GetDetails($this) {
        var $tr = $this.next('.-tr-details');
        if ($tr.find('div').length > 0) {//($this.find('input[type="hidden"]').val() == 'true') {
            $tr.show();
            return;
        }
        showDiv();
        var date = $this.find('td.-date > input:hidden').val(); //.split('/');
        var CountryId = $('#<%# hf_CountryId.ClientID %>').val();
  //      $this.find('input[type="hidden"]').val('true');
        $.ajax({
            url: "<%# GetConversionDetails %>",
            data: "{ 'date': '" + date + "', 'Country': '" + CountryId + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if (data.d.length == 0)
                    return;
                
                $tr.find('td').append(data.d);
                $tr.show();
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //              alert(textStatus);
       //         $this.find('input[type="hidden"]').val('');
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                hideDiv();
            }

        });    
    }
</script>
<asp:Repeater ID="_Repeater" runat="server" >

<HeaderTemplate>
    <table class="data-table2">
    <tr>
        <th>
              <asp:Label ID="Label1" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>
        </th>
         <th>
            <asp:Label ID="Label2344" runat="server" Text="<%# lbl_UniqueInstalls.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label17" runat="server" Text="<%# lbl_DAUs.Text %>"></asp:Label>
        </th>
         <th>
            <asp:Label ID="Label23" runat="server" Text="<%# lbl_LogicHits.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_Exposers.Text %>"></asp:Label>
        </th>
        <th>
             <asp:Label ID="Label5" runat="server" Text="<%# lbl_TotalRequests.Text %>"></asp:Label>
        </th>
        <th>
             <asp:Label ID="Label14" runat="server" Text="<%# lbl_Requests.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label25" runat="server" Text="<%# lbl_Upsale.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label89" runat="server" Text="<%# lbl_StoppedRequests.Text %>"></asp:Label>
        </th>
        
         <th>
            <asp:Label ID="Label2301" runat="server" Text="<%# lbl_Hits_KDAUs.Text %>"></asp:Label>
        </th>
        
         <th>
            <asp:Label ID="Label244" runat="server" Text="<%# lbl_Exp_KDAUs.Text %>"></asp:Label>
        </th>       

        <th>
            <asp:Label ID="Label18" runat="server" Text="<%# lbl_LogicHits_DAUs.Text %>"></asp:Label>
        </th> 

        <th>
            <asp:Label ID="Label24" runat="server" Text="<%# lbl_LogicHits_Exposures.Text %>"></asp:Label>
        </th> 

        <th>
            <asp:Label ID="Label2302" runat="server" Text="<%# lbl_Hits_Exposures.Text %>"></asp:Label>
        </th>
        
         <th>
            <asp:Label ID="Label21" runat="server" Text="<%# lbl_Req_KDAUs.Text %>"></asp:Label>
        </th>
         
        <th>
            <asp:Label ID="Label7" runat="server" Text="<%# lbl_TotalRequests_Exposures.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label16" runat="server" Text="<%# lbl_OriginalRequests_Exposures.Text %>"></asp:Label>
        </th>
       
        <th>
            <asp:Label ID="Label9" runat="server" Text="<%# lbl_Calls.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label11" runat="server" Text="<%# lbl_CallsPercent.Text %>"></asp:Label>
        </th>        
        <th>
            <asp:Label ID="Label20" runat="server" Text="<%# lbl_SaleRate.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label22" runat="server" Text="<%# lbl_Dollar_KDAUs.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label30" runat="server" Text="<%# lbl_CPM.Text %>"></asp:Label>
        </th>
        <th>
            <asp:Label ID="Label31" runat="server" Text="<%# lbl_RPM.Text %>"></asp:Label>
        </th>
    </tr>
</HeaderTemplate>                                          
<ItemTemplate>
    <tr class="-Main-Details">
        <td  class="-date">
           <asp:Label ID="Label2" runat="server" Text="<%# Bind('Date') %>"></asp:Label> 
            <asp:HiddenField ID="hf_DateString" runat="server" Value="<%# Bind('DateString') %>"/>
            
        </td>
        <td>
             <asp:Label ID="Label2224" runat="server" Text="<%# Bind('UniqueInstalls') %>"></asp:Label>
        </td>
        <td>
             <asp:Label ID="Label323" runat="server" Text="<%# Bind('DAUS') %>"></asp:Label>
        </td>
        <td>
             <asp:Label ID="Label2074" runat="server" Text="<%# Bind('LogicHits') %>"></asp:Label>
        </td>
        <td>
             <asp:Label ID="Label4" runat="server" Text="<%# Bind('ExposureCount') %>"></asp:Label>
        </td>
        <td>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('RequestCount') %>"></asp:Label> 
        </td>
        <td>
            <asp:Label ID="Label13" runat="server" Text="<%# Bind('NonUpsalesCount') %>"></asp:Label> 
        </td>
        <td>
              <asp:Label ID="Label26" runat="server" Text="<%# Bind('Upsale') %>"></asp:Label>   
        </td>
        <td>
            <asp:Label ID="Label80" runat="server" Text="<%# Bind('StoppedRequests') %>"></asp:Label>                
        </td>

        <td>
            <asp:Label ID="Label2303" runat="server" Text="<%# Bind('Hits_KDAUs') %>"></asp:Label>                
        </td>
       
        <td>
            <asp:Label ID="Label423" runat="server" Text="<%# Bind('Exp_KDAUs') %>"></asp:Label>                
        </td>      

        <td>
            <asp:Label ID="Label247" runat="server" Text="<%# Bind('LogicHits_DAUs') %>"></asp:Label>                
        </td>

        <td>
            <asp:Label ID="Label29" runat="server" Text="<%# Bind('LogicHits_Exposure') %>"></asp:Label>                
        </td>

        <td>
            <asp:Label ID="Label2304" runat="server" Text="<%# Bind('Hits_Exposures') %>"></asp:Label>                
        </td>

        <td>
            <asp:Label ID="Label324" runat="server" Text="<%# Bind('Req_KDAUs') %>"></asp:Label>                
        </td>       
        <td>
            <asp:Label ID="Label8" runat="server" Text="<%# Bind('TotalRequests_Exposures') %>"></asp:Label>                
        </td>
        <td>
            <asp:Label ID="Label15" runat="server" Text="<%# Bind('OriginalRequests_Exposures') %>"></asp:Label>                
        </td>
        
        <td>         
            <asp:Label ID="Label10" runat="server" Text="<%# Bind('CallCount') %>"></asp:Label>               
        </td>
        <td>
           <asp:Label ID="Label12" runat="server" Text="<%# Bind('CallsPercent') %>"></asp:Label> 
                  
        </td>
         <td>
            <asp:Label ID="Label19" runat="server" Text="<%# Bind('SaleRate') %>"></asp:Label>
        </td>
        <td>         
            <asp:Label ID="Label3323" runat="server" Text="<%# Bind('Revenue_KDAUs') %>"></asp:Label>               
        </td>
         <td>
            <asp:Label ID="Label27" runat="server" Text="<%# Bind('CPM') %>"></asp:Label>
        </td>
        <td>         
            <asp:Label ID="Label28" runat="server" Text="<%# Bind('RPM') %>"></asp:Label>               
        </td>
    </tr>
    <tr class="-tr-details" style="display:none;">
        <td colspan="9">
        
        </td>    
    </tr>
</ItemTemplate>
<FooterTemplate>
    </table>
</FooterTemplate>
</asp:Repeater>

<asp:HiddenField ID="hf_CountryId" runat="server" />

<asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Exposers" runat="server" Text="Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_LogicHits" runat="server" Text="Logic hits" Visible="false"></asp:Label>
<asp:Label ID="lbl_UniqueInstalls" runat="server" Text="Live installs" Visible="false"></asp:Label>


<asp:Label ID="lbl_TotalRequests" runat="server" Text="Total requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_Requests" runat="server" Text="Requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_OriginalRequests_Exposures" runat="server" Text="Original requests > Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_TotalRequests_Exposures" runat="server" Text="Total requests > Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_Calls" runat="server" Text="Calls" Visible="false"></asp:Label>
<asp:Label ID="lbl_CallsPercent" runat="server" Text="Calls percent" Visible="false"></asp:Label>
<asp:Label ID="lbl_Upsale" runat="server" Text="Upsold" Visible="false"></asp:Label>
<asp:Label ID="lbl_StoppedRequests" runat="server" Text="Stopped" Visible="false"></asp:Label>
<asp:Label ID="lbl_SaleRate" runat="server" Text="Sale rate" Visible="false"></asp:Label>

<asp:Label ID="lbl_DAUs" runat="server" Text="DAUs" Visible="false"></asp:Label>
<asp:Label ID="lbl_Exp_KDAUs" runat="server" Text="Exp/KDAUs" Visible="false"></asp:Label>
<asp:Label ID="lbl_Req_KDAUs" runat="server" Text="Req/KDAUs" Visible="false"></asp:Label>
<asp:Label ID="lbl_Dollar_KDAUs" runat="server" Text="$/KDAUs" Visible="false"></asp:Label>
<asp:Label ID="lbl_LogicHits_DAUs" runat="server" Text="LogicHits/DAUs" Visible="false"></asp:Label>

<asp:Label ID="lbl_Hits_KDAUs" runat="server" Text="Hits/KDAUs" Visible="false"></asp:Label>
<asp:Label ID="lbl_Hits_Exposures" runat="server" Text="Hits/Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_LogicHits_Exposures" runat="server" Text="LogicHits/Exposures" Visible="false"></asp:Label>

<asp:Label ID="lbl_CPM" runat="server" Text="CPM" Visible="false"></asp:Label>
<asp:Label ID="lbl_RPM" runat="server" Text="RPM" Visible="false"></asp:Label>

