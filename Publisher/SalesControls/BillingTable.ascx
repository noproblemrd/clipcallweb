﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BillingTable.ascx.cs" Inherits="Publisher_SalesControls_BillingTable" %>

<asp:GridView ID="_GridView" runat="server" CssClass="data-table -GridView" AutoGenerateColumns="false">
<RowStyle CssClass="even" />
<AlternatingRowStyle CssClass="odd" />
<Columns>
    <asp:TemplateField SortExpression="BrokersId">
        <HeaderTemplate>
            <asp:Label ID="Label1" runat="server" Text="<%# lbl_BrokersId.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="lbl_LineNumber" runat="server" Text="<%# Bind('BrokersId') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="CreatedOn">
        <HeaderTemplate>
            <asp:Label ID="Label3" runat="server" Text="<%# lbl_CreatedOn.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label4" runat="server" Text="<%# Bind('CreatedOn') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>

     <asp:TemplateField SortExpression="Duration">
        <HeaderTemplate>
            <asp:Label ID="Label5" runat="server" Text="<%# lbl_Duration.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label6" runat="server" Text="<%# Bind('Duration') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="HeadingName">
        <HeaderTemplate>
            <asp:Label ID="Label7" runat="server" Text="<%# lbl_HeadingName.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label8" runat="server" Text="<%# Bind('HeadingName') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="IncidentId">
        <HeaderTemplate>
            <asp:Label ID="Label9" runat="server" Text="<%# lbl_IncidentId.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label10" runat="server" Text="<%# Bind('IncidentId') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="IsMarkedAsBadManually">
        <HeaderTemplate>
            <asp:Label ID="Label11" runat="server" Text="<%# lbl_IsMarkedAsBadManually.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label12" runat="server" Text="<%# Bind('IsMarkedAsBadManually') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Notes">
        <HeaderTemplate>
            <asp:Label ID="Label13" runat="server" Text="<%# lbl_Notes.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label14" runat="server" Text="<%# Bind('Notes') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Price">
        <HeaderTemplate>
            <asp:Label ID="Label15" runat="server" Text="<%# lbl_Price.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label16" runat="server" Text="<%# Bind('Price') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Region">
        <HeaderTemplate>
            <asp:Label ID="Label17" runat="server" Text="<%# lbl_Region.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label18" runat="server" Text="<%# Bind('Region') %>"></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="TicketNumber">
        <HeaderTemplate>
            <asp:Label ID="Label19" runat="server" Text="<%# lbl_TicketNumber.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label20" runat="server" Text="<%# Bind('TicketNumber') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField SortExpression="Phone">
        <HeaderTemplate>
            <asp:Label ID="Label29" runat="server" Text="<%# lbl_Phone.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="Label30" runat="server" Text="<%# Bind('Phone') %>"></asp:Label> 
        </ItemTemplate>
    </asp:TemplateField>
          
    <asp:TemplateField SortExpression="Record">
        <HeaderTemplate>
            <asp:Label ID="Label21" runat="server" Text="<%# lbl_Record.Text %>"></asp:Label> 
        </HeaderTemplate>
        <ItemTemplate>
             <a id="a_recording" runat="server" href="<%# Bind('Record') %>" visible="<%# Bind('HasRecord') %>">
                <asp:Image ID="img_record" runat="server" ImageUrl="../../Management/calls_images/icon_speaker.png" />
            </a>	
        </ItemTemplate>
    </asp:TemplateField>
</Columns>
</asp:GridView>
<asp:Label ID="lbl_BrokersId" runat="server" Text="Dialer call id" Visible="false"></asp:Label>
<asp:Label ID="lbl_CreatedOn" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Duration" runat="server" Text="Duration" Visible="false"></asp:Label>
<asp:Label ID="lbl_HeadingName" runat="server" Text="Heading Name" Visible="false"></asp:Label>
<asp:Label ID="lbl_IncidentId" runat="server" Text="Incident Id" Visible="false"></asp:Label>
<asp:Label ID="lbl_IsMarkedAsBadManually" runat="server" Text="Marked As Bad Manually" Visible="false"></asp:Label>
<asp:Label ID="lbl_Notes" runat="server" Text="Notes" Visible="false"></asp:Label>
 <asp:Label ID="lbl_Price" runat="server" Text="Price" Visible="false"></asp:Label>
  <asp:Label ID="lbl_Region" runat="server" Text="Region" Visible="false"></asp:Label>
   <asp:Label ID="lbl_TicketNumber" runat="server" Text="Ticket Number" Visible="false"></asp:Label>
 <asp:Label ID="lbl_Record" runat="server" Text="Record" Visible="false"></asp:Label>
  <asp:Label ID="lbl_Phone" runat="server" Text="Phone" Visible="false"></asp:Label>
   