﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_SalesControls_ConversionDrillDown : System.Web.UI.UserControl
{
    protected const string NUMBER_FORMAT = "{0:#,0.##}";
    protected const string PERCENT = "%";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void LoadData(WebReferenceReports.ConversionDrillDownRow[] rows)
    {
        DataTable data = new DataTable();
        data.Columns.Add("Origin");
        data.Columns.Add("DAUS");
        data.Columns.Add("ExposureCount");
        data.Columns.Add("TotalRequests");
        data.Columns.Add("RequestCount");
        data.Columns.Add("UpsalesCount");
        data.Columns.Add("Stoped");
        data.Columns.Add("Exp_KDAUs");
        data.Columns.Add("Req_KDAUs");
        data.Columns.Add("TotalRequests_Exposures");
        data.Columns.Add("OriginalRequests_Exposures");
       // data.Columns.Add("Conversion_KDAUs");
        data.Columns.Add("UniqueInstalls");
        data.Columns.Add("CallCount");
        data.Columns.Add("CallsPercent");
        data.Columns.Add("SaleRate");
        data.Columns.Add("Money_KDAUs");
        data.Columns.Add("ExpHits_KDAUs");
        data.Columns.Add("ExpHits_Exposures");

        data.Columns.Add("LogicHits");
        data.Columns.Add("LogicHits_DAUs");

        data.Columns.Add("CPM");
        data.Columns.Add("RPM");
        data.Columns.Add("LogicHits_Exposure");
       
        foreach (WebReferenceReports.ConversionDrillDownRow cddr in rows)
        {
            DataRow row = data.NewRow();
            row["Origin"] = cddr.Origin;
            row["DAUS"] = cddr.DAUS;
            row["UniqueInstalls"] = cddr.UniqueInstalls;
            row["ExposureCount"] = cddr.ExposureCount;
            row["TotalRequests"] = cddr.RequestCount;
            row["RequestCount"] = cddr.NonUpsalesCount;
            row["UpsalesCount"] = cddr.UpsalesCount;
            row["Stoped"] = cddr.StoppedRequests;
            row["Exp_KDAUs"] = GetNumString(cddr.Exposure_KDAUs);
            row["Req_KDAUs"] = GetNumString(cddr.Request_KDAUs);
            row["TotalRequests_Exposures"] = GetNumString(cddr.TotalRequests_Exposures, PERCENT);
            row["OriginalRequests_Exposures"] = GetNumString(cddr.OriginalRequests_Exposures, PERCENT);
          //  row["Conversion_KDAUs"] = string.Format(NUMBER_FORMAT, cddr.Conversion_KDAUs) + "%";
            row["CallCount"] = cddr.CallCount;
            row["CallsPercent"] = GetNumString(cddr.CallsPercent, PERCENT);
            row["SaleRate"] = GetNumString(cddr.SaleRate, PERCENT);
            row["Money_KDAUs"] = GetNumString(cddr.Revenue_KDAUs);
            row["ExpHits_KDAUs"] = GetNumString(cddr.ExpHits_KDAUs);
            row["ExpHits_Exposures"] = GetNumString(cddr.ExpHits_Exposures);
            row["LogicHits"] = cddr.LogicHits;
            row["LogicHits_DAUs"] = GetNumString(cddr.LogicHits_DAUS);
            row["CPM"] = GetNumString(cddr.CPM);
            row["RPM"] = GetNumString(cddr.RPM);
            row["LogicHits_Exposure"] = GetNumString(cddr.LogicHits_Exposure);
            
            data.Rows.Add(row);
        }
        _GridView.DataSource = data;
        _GridView.DataBind();
    }
    string GetNumString(decimal num, string EndWith)
    {
        return SalesUtility.GetNumberForReportNonNegative(num, NUMBER_FORMAT, EndWith);
    }
    string GetNumString(double num, string EndWith)
    {
        return SalesUtility.GetNumberForReportNonNegative(num, NUMBER_FORMAT, EndWith);
    }
    string GetNumString(decimal num)
    {
        return SalesUtility.GetNumberForReportNonNegative(num, NUMBER_FORMAT, null);
    }
    string GetNumString(double num)
    {
        return SalesUtility.GetNumberForReportNonNegative(num, NUMBER_FORMAT, null);
    }
    
    
    
}
 