﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NP_DistributionReport.ascx.cs" Inherits="Publisher_SalesControls_NP_DistributionReport" %>
<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
    <RowStyle CssClass="even" />
    <AlternatingRowStyle CssClass="odd" />
    <Columns>
        <asp:TemplateField SortExpression="Origin">
            <HeaderTemplate>
                <asp:Label ID="Label1" runat="server" Text="Origin"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblOrigin" runat="server" Text="<%# Bind('Origin') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField SortExpression="Installs">
            <HeaderTemplate>
                <asp:Label ID="Label2" runat="server" Text="Installs"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblInstalls" runat="server" Text="<%# Bind('Installs') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

         <asp:TemplateField SortExpression="UniqueInstalls">
            <HeaderTemplate>
                <asp:Label ID="Label3" runat="server" Text="Unique Installs"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblUniqueInstalls" runat="server" Text="<%# Bind('UniqueInstalls') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

         <asp:TemplateField SortExpression="EffectiveInstalls">
            <HeaderTemplate>
                <asp:Label ID="Label4" runat="server" Text="Effective Installs"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblEffectiveInstalls" runat="server" Text="<%# Bind('EffectiveInstalls') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField SortExpression="Cost">
            <HeaderTemplate>
                <asp:Label ID="Label5" runat="server" Text="Cost"></asp:Label> 
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblCost" runat="server" Text="<%# Bind('Cost') %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

    </Columns>
</asp:GridView>
