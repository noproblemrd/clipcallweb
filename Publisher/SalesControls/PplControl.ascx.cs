﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Publisher_SalesControls_PplControl : System.Web.UI.UserControl
{

    //private bool _WithStop = false;
    const string Num_Format = "{0:#,0.##}";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DataBind();
    }
    public void LoadData(WebReferenceReports.PplReportData[] ppldata, string Heading)
    {
        TablePpl.Visible = true;
        lblCategory.Text = Heading;
        if (ppldata == null || ppldata.Length == 0)
        {
            lblAvergaePPL.Text = "0";
            lblHighestPPL.Text = "0";
            lblLowestPPL.Text = "0";
            _GridView.DataSource = null;
            _GridView.DataBind();
            return;
        }

        decimal _max = (from x in ppldata
                        select x.Price).Max();
        decimal _min = (from x in ppldata
                        select x.Price).Min();
        decimal _avg = (from x in ppldata
                        select x.Price).Average();

        lblAvergaePPL.Text = string.Format(Num_Format, _avg);
        lblHighestPPL.Text = string.Format(Num_Format, _max);
        lblLowestPPL.Text = string.Format(Num_Format, _min);
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("Price");
        data.Columns.Add("Interface");
        foreach (WebReferenceReports.PplReportData prd in ppldata)
        {
            DataRow row = data.NewRow();
            row["Name"] = prd.AccountName;
            row["Price"] = string.Format(Num_Format, prd.Price);
            row["Interface"] = prd.LeadBuyerInterface;
            data.Rows.Add(row);
        }
        _GridView.DataSource = data;
        _GridView.DataBind();
       
    }
    
    
}