using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.IO;
using System.Net; 

public partial class Publisher_PublisherSegment : PageSetting//, IPostBackEventHandler
{
    const string REGEX = @"^[^!@>+()*^%#&<>""',]+$";
    const string REGEX_CODE = @"^[\d\w]+$";
    const string EXCEL_NAME = "segment";
    const string TXT_NAME = "segment";
    string Search_Enter;
    PostBackOptions pbo;
    //const string REGEX = @"^[^6]+$";
 //   const string REGEX = @"^[^0-9]+$";
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
     //   ScriptManager1.RegisterAsyncPostBackControl(btn_saveLog);
        div_report.Attributes.Add("class", "popModal_del popModal_del_hide");
   //     lbl_errorFile.Text = "";
        if (!IsPostBack)
        {
            RegularExpressionValidator_NewSegment.ValidationExpression = REGEX;
            SetToolBox();
            LoadSegments(string.Empty);
            SetGridView();
            LoadChannels();
            RangeValidator_MinPrice.MaximumValue = int.MaxValue.ToString();
            lbl_delay.DataBind();
            RangeValidator_delay.DataBind();
        }
        SetToolBoxEvents();
        pbo = new PostBackOptions(this, "search");
   //     ClientScript.RegisterForEventValidation(pbo);
        Search_Enter = Page.ClientScript.GetPostBackEventReference(pbo);
        Header.DataBind();
   //     Page.pa += new EventHandler(Page_PreRender);
    }

    private void LoadChannels()
    {
        foreach (string s in Enum.GetNames(typeof(eChannelCode)))
        {
            ddl_channel.Items.Add(new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, (typeof(eChannelCode)).Name, s),
                                                s));

        }
        ddl_channel.SelectedIndex = 0;
    }

    private void SetToolBox()
    {
        string script = "openPopup(); return false;";
        Toolbox1.SetClientScriptToAdd(script);
        Toolbox1.SetFileUploadType(eFileType.EXCEL);
    //    Toolbox1.RemoveAttach();
   //     script = "return confirm('" + Toolbox1.GetConfirmDelete + "');";
    //    Toolbox1.SetClientScriptToDelete(script);
    }
    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
        Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
        Toolbox1.AttachExec += new EventHandler(Toolbox1_AttachExec);
        ScriptManager1.RegisterAsyncPostBackControl(Toolbox1.GetEditButton);
    }

    

    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        bool IsSelect = false;
        int indx = -1;
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
            {
                if (IsSelect)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ChooseOne", "alert('" + Toolbox1.ChooseOne + "');", true);
                    return;
                }
                IsSelect = true;
                indx = row.RowIndex;
            }
        }
        if (indx > -1)
        {
            GridViewRow row = _GridView.Rows[indx];
            hf_SegmentId.Value = (_GridView.DataKeys[indx].Value == DBNull.Value) ? string.Empty :
                (string)_GridView.DataKeys[indx].Value;
            txt_NewCode.Text = ((Label)row.FindControl("lblCode")).Text;
            txt_NewMinPrice.Text = ((Label)row.FindControl("lblMinPrice")).Text;
            txt_NewMinBrokerBid.Text = ((Label)row.FindControl("lblMinBrokerBid")).Text;
            txt_NewSegment.Text = ((Label)row.FindControl("lblSegment")).Text;
            txt_delay.Text = ((Label)row.FindControl("lblNotificationDelay")).Text;
            cb_NewAuction.Checked = (((Label)row.FindControl("lbl_Is_Auction")).Text == "true");
            cb_NewShowCertificate.Checked = (((Label)row.FindControl("lbl_ShowCertificate")).Text == "true");
            string _val = ((Label)row.FindControl("lblChannel")).Text;
            foreach (ListItem li in ddl_channel.Items)
            {
                li.Selected = (li.Value == _val);
            }
            /*
            if (ddl_channel.SelectedIndex != 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "disable_auction", "disable_auction(true);", true);
            }
             * */
            string script = @"ChangeChannel(document.getElementById('" + ddl_channel.ClientID + "'));";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ChangeChannel", script, true);
            popUpSegment.Attributes["class"] = "popModal_del";
            _mpeSegment.Show();           
        }
        _UpdatePanelEdit.Update();
    }
    

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        List<Guid> list = new List<Guid>();
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            if (cb.Checked)
            {
                Guid _id = new Guid(((Label)row.FindControl("lbl_guid")).Text);                
                list.Add(_id);
            }
        }
        WebReferenceSite.DeleteExpertisesRequest _request = new WebReferenceSite.DeleteExpertisesRequest();
        _request.ExpertiseIds = list.ToArray();
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.DeleteExpertises(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Toolbox1.GetTextBoxSearch.Value = string.Empty;
        LoadSegments(string.Empty);
        SetGridView();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataViewV.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            return;
        }
     //   PrintHelper.PrintWebControl(_GridView, dataViewV);
        Session["data_print"] = dataViewV.Table;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        
  //      DataTable data = dataV;
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordExport + "');", true);
            return;
        }
        
        ToExcel to_excel = new ToExcel(this, "Segments");
        if (!to_excel.ExecExcel(GetDataTableDisplay()))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void Toolbox1_AddExec(object sender, EventArgs e)
    {
        
    }

    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        string _search = Toolbox1.GetTxt.Trim();
        LoadSegments(_search);
        SetGridView();
    }
   
    private void LoadSegments(string str)
    {
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = _site.GetPrimaryExpertise(siteSetting.GetSiteID, str);
        }
        catch (System.Web.Services.Protocols.SoapException ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        /*   NotificationDelay
        System.IO.StringReader sr = new System.IO.StringReader(result);

        DataSet ds = new DataSet();
        ds.ReadXml(sr);
        */
        DataTable data = new DataTable();
        data.Columns.Add("Name");
        data.Columns.Add("ID");
        data.Columns.Add("Code");
        data.Columns.Add("MinPrice");
        data.Columns.Add("IsAuction");
        data.Columns.Add("Is_Auction");
        data.Columns.Add("ShowCertificate");
        data.Columns.Add("Show_Certificate");
        data.Columns.Add("Channel");
        data.Columns.Add("ValueChannel");
        data.Columns.Add("NotificationDelay");
        data.Columns.Add("MinBrokerBid");
       
        XmlDocument xdd = new XmlDocument();
        xdd.LoadXml(result);

        if (xdd["PrimaryExpertise"] == null || xdd["PrimaryExpertise"]["Error"] != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (XmlNode node in xdd["PrimaryExpertise"].ChildNodes)
        {
            DataRow row = data.NewRow();
            row["Name"] = node.Attributes["Name"].Value;
            row["ID"] = node.Attributes["ID"].Value;
            row["Code"] = node.Attributes["Code"].Value;            
            row["MinPrice"] = node.Attributes["MinPrice"].Value;
            row["MinBrokerBid"] = String.Format("{0:0.##}", decimal.Parse(node.Attributes["MinBrokerBid"].Value));
            row["NotificationDelay"] = node.Attributes["NotificationDelay"].Value;
            int _channel = int.Parse(node.Attributes["Channel"].Value);
            row["Channel"] = EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eChannelCode", ((eChannelCode)_channel).ToString());
            row["ValueChannel"] = ((eChannelCode)_channel).ToString();
            if (node.Attributes["IsAuction"].Value.ToLower() == "true" ||
                node.Attributes["IsAuction"].Value == "1")
            {
                row["IsAuction"] = "images/icon-v.png";
                row["Is_Auction"] = "true";
            }
            else
            {
                row["IsAuction"] = "images/icon-x.png";
                row["Is_Auction"] = "false";
            }
            if (node.Attributes["ShowCertificate"].Value.ToLower() == "true" ||
                node.Attributes["ShowCertificate"].Value == "1")
            {
                row["ShowCertificate"] = "images/icon-v.png";
                row["Show_Certificate"] = "true";
            }
            else
            {
                row["ShowCertificate"] = "images/icon-x.png";
                row["Show_Certificate"] = "false";
            }


            data.Rows.Add(row);
        }    
            
  //      _GridView.EditIndex = -1;
   //     _GridView.DataSource = data;
  //      _GridView.DataBind();
       
   //     _updatePanelTable.Update();
        _GridView.PageIndex = 0;
        dataV = data;
        dataViewV = data.AsDataView();
   //     if (data.Rows.Count == 0)
 //           ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + lbl_noResult.Text + "');", true);
        
    }
    protected void btn_Set_Click(object sender, EventArgs e)
    {
        string _id = hf_SegmentId.Value;
        string segment = txt_NewSegment.Text;
        double minPrice = double.Parse(txt_NewMinPrice.Text);
        bool IsAuction = cb_NewAuction.Checked;
        bool ShowCertificate = cb_NewShowCertificate.Checked;
        string code = txt_NewCode.Text;
        string channel = ddl_channel.SelectedValue;
        int delay = int.Parse(txt_delay.Text);
        decimal minBrokerBid;
        if (!decimal.TryParse(txt_NewMinBrokerBid.Text, out minBrokerBid))
            minBrokerBid = 0;
        eChannelCode ecc = eChannelCode.Call;
        foreach (eChannelCode _ecc in Enum.GetValues(typeof(eChannelCode)))
        {
            if (_ecc.ToString() == channel)
            {
                ecc = _ecc;
                break;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.Append("<PrimaryExpertise>");
        sb.Append("<SiteId>" + siteSetting.GetSiteID + "</SiteId>");
        sb.Append("<ID>" + _id + "</ID>");
        sb.Append("<Name>" + segment + "</Name>");
        sb.Append("<MinPrice>" + String.Format("{0:0.##}", minPrice) + "</MinPrice>");
        sb.Append("<IsAuction>" + ((IsAuction) ? "1" : "0") + "</IsAuction>");
        sb.Append("<ShowCertificate>" + ((ShowCertificate) ? "1" : "0") + "</ShowCertificate>");
        sb.Append("<Code>" + code + "</Code>");
        sb.Append("<Channel>" + ((int)ecc) + "</Channel>");
        sb.Append("<NotificationDelay>" + delay + "</NotificationDelay>");
        sb.Append("<MinBrokerBid>").Append(String.Format("{0:0.##}", minBrokerBid)).Append("</MinBrokerBid>");
        sb.Append("</PrimaryExpertise>");

        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = _site.UpsertPrimaryExpertise(sb.ToString());
        }
        catch (Exception ex)
        {
            _mpeSegment.Show();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XmlDataDocument xdd = new XmlDataDocument();
        xdd.LoadXml(result);
        if (xdd["PrimaryExpertiseUpsert"] == null || xdd["PrimaryExpertiseUpsert"]["Error"] != null)
        {
            _mpeSegment.Show();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (xdd["PrimaryExpertiseUpsert"]["Status"] != null && xdd["PrimaryExpertiseUpsert"]["Status"].InnerText == "Exists")
        {
            _mpeSegment.Show();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "exists", "alert('" + lbl_allreadyExists.Text + "');", true);
            return;
        }
        //InNewSegmentV = false;
        _mpeSegment.Hide();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "cleanPopUp", "CleanPopUp();", true);
    //    popUpSegment.Attributes["class"] = "popModal_del popModal_del_hide";
        LoadSegments(Toolbox1.GetTxt);
        SetGridView();
    }
    void SetGridView()
    {
        StringBuilder sb = new StringBuilder();
        DataView data = dataViewV;
       
        _GridView.DataSource = data;
        _GridView.DataBind();
        
        if (data.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noreasultSearch", "alert('" + lbl_noResult.Text + "');", true);
            CBsV = string.Empty;
            _updatePanelTable.Update();
            return;
        }
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }

        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;        
        _updatePanelTable.Update();
    }

    void Toolbox1_AttachExec(object sender, EventArgs e)
    {
       
        string localPath = Toolbox1.GetUrlFile();
        string extension = Path.GetExtension(localPath);
        _updatePanelTable.Update();
        List<WebReferenceSite.PrimaryExpertiseData> list = new List<WebReferenceSite.PrimaryExpertiseData>();

        SortedDictionary<int, string> dicError = new SortedDictionary<int, string>(); 
        
        string sConnectionString = string.Empty;

        if (extension == ".xlsx")
            sConnectionString =
            "Provider=Microsoft.ACE.OLEDB.12.0;Password=\"\";User ID=Admin;Data Source=" + localPath + ";Mode=Share Deny Write;Extended Properties=\"HDR=YES;\";Jet OLEDB:Engine Type=37";
        else if (extension == ".xls")
            sConnectionString =
                "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + localPath + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=YES;""";
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(), "incorrect_file", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_incorrectFile.Text) + "');", true);
            return;
        }
               
        OleDbConnection objConn = new OleDbConnection(sConnectionString);
        try
        {
            objConn.Open();
            DataTable MySchemaTable = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            string sheetName = MySchemaTable.Rows[0].Field<string>("TABLE_NAME");
            if (!sheetName.Contains("$"))
                sheetName += "$";
            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM ["+sheetName+"]", objConn);
            
            OleDbDataReader reader = objCmdSelect.ExecuteReader();
            if (!MuchCulomsNames(reader, "Code", "Segment", "Auction", "Min price", "Show certificate"))
            {
                lbl_report.Text = lbl_incorrectFields.Text;
                _UpdatePanelReport.Update();
                div_report.Attributes["class"] = "popModal_del";
                _mpe.Show();
                return;
            }
            int index = 1;
            Regex reg_code = new Regex(REGEX_CODE);
            Regex reg_name = new Regex(REGEX);
            while (reader.Read())
            {
                string error = string.Empty;
                WebReferenceSite.PrimaryExpertiseData ped = new WebReferenceSite.PrimaryExpertiseData();

                bool IfIgnor = true;
                object o = reader["Code"];
                int _num;
                double _dnum;
                if (o.ToString().Length != 0)//o.GetType() != typeof(double))
                {
                    if (!reg_code.IsMatch(o.ToString()))
                        error += "Code;";
                    else
                        ped.Code = o.ToString();
                    IfIgnor = false;
                    
                }
                else
                    error += "Code;";
                
                o = reader["Segment"];
                if (o.GetType() != typeof(string))
                {
                    error += "Segment;";
                    if (o.ToString().Length != 0)
                        IfIgnor = false;
                }
                else
                {
                    if(!reg_name.IsMatch((string)o))
                        error += "Segment;";
                    else
                        ped.Name = (string)o;
                    IfIgnor = false;
                }
                    
                o = reader["Auction"];
                if (!int.TryParse(o.ToString(), out _num))//o.GetType() != typeof(double))
                {
                    error += "Auction;";
                    if (o.ToString().Length != 0)
                        IfIgnor = false;
                }
                else
                {
                    int _auction = _num;
                    if (_auction == 0 || _auction == 1)
                    {
                        ped.IsAuction = (_auction == 1);
                        IfIgnor = false;
                    }
                    else
                    {
                        if (o.ToString().Length != 0)
                            IfIgnor = false;
                        error += "Auction;";
                    }
                }
                o = reader["Min price"];
                if (!double.TryParse(o.ToString(), out _dnum))//o.GetType() != typeof(double))
                {
                    error += "Min price;";
                    if (o.ToString().Length != 0)
                        IfIgnor = false;
                }
                else
                {
                    double MinPrice = _dnum;
                    if (MinPrice > 0)
                    {
                        ped.MinimalPrice = (decimal)MinPrice;
                        IfIgnor = false;
                    }
                    else
                    {
                        error += "Min price;";
                        if (o.ToString().Length != 0)
                            IfIgnor = false;
                    }
                }
                o = reader["Show certificate"];
                if (!int.TryParse(o.ToString(), out _num))//o.GetType() != typeof(double))
                {
                    error += "Show certificate;";
                    if (o.ToString().Length != 0)
                        IfIgnor = false;
                }
                else
                {
                    int certificate = _num;
                    if (certificate == 0 || certificate == 1)
                    {
                        ped.ShowCertificate = (certificate == 1);
                        IfIgnor = false;
                    }
                    else
                    {
                        error += "Show certificate;";
                        if (o.ToString().Length != 0)
                            IfIgnor = false;
                    }
                }
                if (IfIgnor)
                    continue;
                if (error != string.Empty)
                {
                    error = error.Substring(0, error.Length - 1);
                    dicError.Add(index, error);
                }
                else
                {
                    ped.LineInExcel = index;
                    list.Add(ped);
                }
                index++;
            }
             
            /*
            OleDbDataAdapter adapter = new OleDbDataAdapter(objCmdSelect);
            DataTable data = new DataTable();
            adapter.Fill(data);
             * */
            objConn.Close();
            
            
        }
        catch (Exception ex)
        {
            //debug
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
     //       ScriptManager.RegisterStartupScript(this, this.GetType(), "debug22", "alert('"+ex.Message+"');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        finally
        {
            if (objConn.State != ConnectionState.Closed)
                objConn.Close();
        }
        /*
        if (dicError.Count != 0)
        {
            ShowErrorMessage(dicError, false, 0);
            return;
        }
         * */
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.InsertMultipleExpertisesRequest request = new WebReferenceSite.InsertMultipleExpertisesRequest();
        request.Expertises = list.ToArray();
        WebReferenceSite.ResultOfInsertMultipleExpertisesResponse result = null;
        try
        {
            result = _site.InsertMultipleExpertises(request);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
  //      bool IsSaveOkCrm = true;
        int SaveOk = 0;
        
        foreach (WebReferenceSite.ExpertiseResponseData erd in result.Value.ResponseData)
        {
            if (!erd.SavedOK)
            {
                dicError.Add(erd.LineInExcel, erd.FailureReason);
 //               IsSaveOkCrm = false;
            }
            else
                SaveOk++;
        }
        if (dicError.Count!=0)
        {
            ShowErrorMessage(dicError, true, SaveOk);
            LoadSegments("");
            return;
        }
        btn_saveLog.Visible = false;
        string message = lbl_SavedSuccessfully.Text + "!";
        message += @"<br/>" + SaveOk + " "+lbl_SavedOk.Text+"!";
        lbl_report.Text = message;
        _UpdatePanelReport.Update();
        div_report.Attributes["class"] = "popModal_del";
        _mpe.Show();
   //     string script = "ShowReport();";
    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowReport", script, true);       
        LoadSegments("");
    }
    bool MuchCulomsNames(OleDbDataReader reader, params string[] names)
    {
        foreach (string s in names)
        {        
            bool _find = false;
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (s.ToLower() == reader.GetName(i).ToLower())
                {
                    _find = true;
                    break;
                }
            }
            if (!_find)
                return false;
        }
        return true;
    }
    void ShowErrorMessage(SortedDictionary<int, string> dic, bool IsSaved, int SavedOk)
    {
        StringBuilder sb = new StringBuilder();
        if (IsSaved)
        {
            sb.Append(SavedOk + @" " + lbl_SavedOk.Text + @"!<br/>");
            sb.Append(dic.Count + @" "+lbl_dontSaved.Text+":");            
        }
        else
            sb.Append(lbl_problem.Text+" " + dic.Count + " "+lbl_DocumentNotSaved.Text+"!");
        sb.Append(@"<br/><br/>");
        foreach (KeyValuePair<int, string> kvp in dic)
        {
            string[] errors=kvp.Value.Split(';');
            sb.Append(lbl_errorLines.Text+" " + kvp.Key + ", "+lbl_Fields.Text+": ");
            for(int i=0;i<errors.Length;i++)
            {
                string s = errors[i] + ", ";
                if (i == errors.Length - 1)
                    s = s.Substring(0, s.Length - 2);
                sb.Append(s);
            }
            sb.Append(@"<br/>");
        }
        lbl_report.Text = sb.ToString();
        ErrorMessageV = sb.ToString().Replace(@"<br/>", "\r\n");
        _UpdatePanelReport.Update();
        btn_saveLog.Visible = true;
        //string script = "ShowReport();";
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowReport", script, true);
        div_report.Attributes["class"] = "popModal_del";
        _mpe.Show();
        
    }
    DataTable GetDataTableDisplay()
    {
        DataTable data = dataV;
        DataTable dataExcel = new DataTable();
        dataExcel.Columns.Add("code", typeof(string));
        dataExcel.Columns.Add("Segment", typeof(string));
        dataExcel.Columns.Add("Auction", typeof(string));
        dataExcel.Columns.Add("Min price", typeof(string));
        dataExcel.Columns.Add("Show certificate", typeof(string));
        foreach (DataRow row in data.Rows)
        {
            DataRow rowExcel = dataExcel.NewRow();
            rowExcel["code"] = row["Code"].ToString();
            rowExcel["Segment"] = row["Name"].ToString();
            rowExcel["Auction"] = (row["Is_Auction"].ToString() == "true") ? "1" : "0";
            rowExcel["Min price"] = row["MinPrice"].ToString();
            rowExcel["Show certificate"] = (row["Show_Certificate"].ToString() == "true") ? "1" : "0";
            dataExcel.Rows.Add(rowExcel);
        }
        return dataExcel;
    }
  
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {  
        _GridView.PageIndex = e.NewPageIndex; 
        SetGridView();   
    }
    protected void _btn_closed_click(object sender, EventArgs e)
    {
        string path = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int rnd = new Random().Next();
        path += rnd + EXCEL_NAME + ".txt";
        using (System.IO.StreamWriter _file = new System.IO.StreamWriter(path))
        {
            _file.Write(ErrorMessageV);
        }
        Response.ContentType = "test/plain";
        Response.AppendHeader("Content-disposition", "attachment; filename=Error.txt");
        Response.WriteFile(path);
        Response.End();
        
    }
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
   
    string ErrorMessageV
    {
        get { return (ViewState["ErrorMessage"] == null) ? string.Empty : (string)ViewState["ErrorMessage"]; }
        set { ViewState["ErrorMessage"] = value; }
    }
    private DataView dataViewV
    {
        get { return (Session["dataView"] == null) ? null : (DataView)Session["dataView"]; }
        set { Session["dataView"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
    protected string GetNotificationDelay
    {
        get { return lbl_NotificationDelay.Text; }
    }
    protected string GetValueRange
    {
        get { return lbl_range.Text + " 0-30"; }
    }

   
}
