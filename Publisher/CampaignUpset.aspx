﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CampaignUpset.aspx.cs" Inherits="Publisher_CampaignUpset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link rel="Stylesheet" href="InjectionCampaign.css" type="text/css" />
    <title></title>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        function CloseWin() {
            window.close();
        }
        function CloseReloadWin() {
            window.opener.Reload();
            window.close();
        }
        
        function SaveClick() {
            var val1 = document.getElementById('<%# txt_CostPerRequest.ClientID %>').value;
            var val2 = document.getElementById('<%# txt_CostPerInstall.ClientID %>').value;
            var val3 = document.getElementById('<%# txt_RevenueShare.ClientID %>').value;
            var HasValue = false;
            if (val1.length > 0 && val1 != '0')
                HasValue = true;
            if (val2.length > 0 && val2 != '0') {
                if (HasValue) {
                    document.getElementById('<%# lbl_OnlyOne.ClientID %>').style.visibility = 'visible';
                    return false;
                }
                HasValue = true;
            }
            if (val3.length > 0 && val3 != '0') {
                if (HasValue) {
                    document.getElementById('<%# lbl_OnlyOne.ClientID %>').style.visibility = 'visible';
                    return false;
                }
            }
            document.getElementById('<%# lbl_OnlyOne.ClientID %>').style.visibility = 'hidden';
            $('#<%# lb_Save.ClientID %>').hide();
            $('#a_Save_loader').show();
            return true;
        }
        function MinFactorEmailToSendValidation(source, arguments) {
            if (arguments.Value.length == 0) {
                if (document.getElementById('<%# cb_SendDailyEmail.ClientID %>').checked) {
                    arguments.IsValid = false;
                    EnableButton();
                    return;
                }
                arguments.IsValid = true;
                return;
            }           
            BaseEmailToSendValidation(source, arguments)
        }
        function MaxFactorEmailToSendValidation(source, arguments) {
            if (arguments.Value.length == 0) {
                if (document.getElementById('<%# cb_MaxFactor.ClientID %>').checked) {
                    arguments.IsValid = false;
                    EnableButton();
                    return;
                }
                arguments.IsValid = true;
                return;
            }
            BaseEmailToSendValidation(source, arguments)
        }
        function BaseEmailToSendValidation(source, arguments)
        {
            var emailexp = new RegExp("<%# GetEmailRegExpresionJavaScript %>", 'i');
            
            var mails = arguments.Value.split(';');
            var is_valid = true;
            for (var i = 0; i < mails.length; i++) {
                if (!emailexp.test(mails[i])) {
                    arguments.IsValid = false;
                    EnableButton();
                    return;
                }
                //    }
                arguments.IsValid = true;
            }
        }
        
        
       
        function EnableButton() {
            $('#<%# lb_Save.ClientID %>').show();
            $('#a_Save_loader').hide();
        }
    
    </script>
</head>
<body class="Injection_form">
    <form id="form1" runat="server">
    <div class="Popwin-title">        
         <span><asp:Label ID="lbl_Title" runat="server" Text="Label"></asp:Label>
        distribution campaign</span>
    </div>
    <div>
        <table class="Popwin-table">
            <tr>
                <td>
                    <span>Name</span>
                </td>
                <td>
                    <asp:TextBox ID="txt_Name" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_Name" runat="server" ErrorMessage="Missing" ControlToValidate="txt_Name"
                     CssClass="error-msg" ValidationGroup="Campaign"></asp:RequiredFieldValidator>
                </td>                
            </tr>
            

             <tr>
                <td>
                    <span>Use Same In Upsale</span>
                </td>
                <td>                    
                    <asp:CheckBox ID="cb_UseSameInUpsale" runat="server" />
                </td>
            </tr>

            <tr>
                <td>
                    <span>Include Campaign</span>
                </td>
                <td>                    
                    <asp:CheckBox ID="cb_IncludeCampaign" runat="server" />
                </td>
            </tr>

            <tr>
                <td>
                    <span>Is Api Seller</span>
                </td>
                <td>    
                    <asp:CheckBox ID="cb_IsApiSeller" runat="server" />                
                    
                </td>
            </tr>

            <tr>
                <td>
                    <span>Is In Financial Dashboard</span>
                </td>
                <td>                    
                    <asp:CheckBox ID="cb_IsInFinancialDashboard" runat="server" />  
                </td>
            </tr>

             <tr>
                <td>
                    <span>Show NoProblem In Slider</span>
                </td>
                <td>                    
                    <asp:CheckBox ID="cb_ShowNoProblemInSlider" runat="server" />  
                </td>
            </tr>

             <tr>
                <td>
                    <span>Is Distribution</span>
                </td>
                <td>                    
                   <asp:CheckBox ID="cb_IsDistribution" runat="server" /> 
                </td>
            </tr>

            

              <tr class="Section_mid Section_top">
                <td>
                    <span>Cost Per Request</span>
                </td>
                <td>                    
                    <asp:TextBox ID="txt_CostPerRequest" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="rv_CostPerRequest" runat="server" ErrorMessage="RangeValidator"
                        ControlToValidate="txt_CostPerRequest" CssClass="error-msg" 
                        ValidationGroup="Campaign" MinimumValue="0.00" Type="Double"></asp:RangeValidator>
                </td>
            </tr>

            <tr class="Section_mid">
                <td>
                    <span>Cost Per Install</span>
                </td>
                <td>                    
                    <asp:TextBox ID="txt_CostPerInstall" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="rv_CostPerInstall" runat="server" ErrorMessage="RangeValidator"
                        ControlToValidate="txt_CostPerInstall" CssClass="error-msg"
                        ValidationGroup="Campaign" MinimumValue="0.00" Type="Double"></asp:RangeValidator>
                </td>
            </tr>

            <tr class="Section_mid">
                <td>
                    <span>Revenue Share</span>
                </td>
                <td>                    
                    <asp:TextBox ID="txt_RevenueShare" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="rv_RevenueShare" runat="server" ErrorMessage="RangeValidator"
                        ControlToValidate="txt_RevenueShare" CssClass="error-msg" 
                        ValidationGroup="Campaign" MinimumValue="0.00" Type="Double"></asp:RangeValidator>
                </td>
            </tr>
            <tr class="Section_mid Section_bottom">
                <td>
                    
                </td>
                <td>                   
                    <asp:Label ID="lbl_OnlyOne" runat="server" Text="Please enter one value in this section" CssClass="error-msg" style="visibility:hidden"></asp:Label>
                </td>
            </tr>

            <tr class="Section_mid Section_top">
                <td>
                    <span>Factor</span>
                </td>
                <td>
                    <asp:TextBox ID="txt_Factor" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="rv_Factor" runat="server" ErrorMessage="RangeValidator"
                        ControlToValidate="txt_Factor" CssClass="error-msg" 
                        ValidationGroup="Campaign" MinimumValue="0.00" Type="Double"></asp:RangeValidator>
                </td>
            </tr>
            <tr class="Section_mid">
                <td>
                    <span>Send installation report daily email</span>
                </td>
                <td>
                    <asp:CheckBox ID="cb_SendDailyEmail" runat="server" />                   
                    
                </td>
            </tr>
            <tr class="Section_mid Section_bottom">
                <td>
                    <span>Email to send:</span>
                </td>
                <td>
                    <asp:TextBox ID="txt_EmailToSend" runat="server"></asp:TextBox>
                    <asp:CustomValidator ID="cv_EmailToSend" runat="server" ErrorMessage="Email not valid" 
                         ValidationGroup="Campaign" ControlToValidate="txt_EmailToSend" ValidateEmptyText="true" 
                        ClientValidationFunction="MinFactorEmailToSendValidation" ></asp:CustomValidator>
                </td>
            </tr>

            <tr class="Section_mid Section_top">
                <td>
                    <span>MAX Factor</span>
                </td>
                <td>
                    <asp:TextBox ID="txt_MaxFactor" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="rv_MaxFactor" runat="server" ErrorMessage="RangeValidator"
                        ControlToValidate="txt_MaxFactor" CssClass="error-msg" 
                        ValidationGroup="Campaign" MinimumValue="0.00" Type="Double"></asp:RangeValidator>
                </td>
            </tr>
            <tr class="Section_mid">
                <td>
                    <span>Send max Factor installation report daily email</span>
                </td>
                <td>
                    <asp:CheckBox ID="cb_MaxFactor" runat="server" />
                    
                </td>
            </tr>
            <tr class="Section_mid Section_bottom">
                <td>
                    <span>Email to send:</span>
                </td>
                <td>
                    <asp:TextBox ID="txt_MaxFactorEmail" runat="server"></asp:TextBox>
                    <asp:CustomValidator ID="cv_MaxFactor" runat="server" ErrorMessage="Email not valid"
                         ValidationGroup="Campaign" ControlToValidate="txt_MaxFactorEmail" ValidateEmptyText="true" 
                        ClientValidationFunction="MaxFactorEmailToSendValidation" ></asp:CustomValidator>
                </td>
            </tr>

            <tr>
                <td>
                    <span>Slider Brought To You By Name</span>
                </td>
                <td>                    
                    <asp:TextBox ID="txt_SliderBroughtToYouByName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td><span>Id:</span></td>
                <td><asp:Label ID="lable_originid" runat="server" Text=""></asp:Label></td>
            </tr>

              <tr>
                <td>
                    
                </td>
                <td>
                    <asp:LinkButton ID="lb_Save" runat="server" CssClass="BtnInjection" 
                        onclick="lb_Save_Click" ValidationGroup="Campaign" OnClientClick="return SaveClick();">Save</asp:LinkButton>
                    <span id="a_Save_loader" style="display:none;" class="a__loader a_Save_loader"></span>
                </td>
            </tr>

        </table>
        
    </div>
    </form>
</body>
</html>
