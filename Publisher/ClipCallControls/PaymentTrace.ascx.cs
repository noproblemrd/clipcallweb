﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallControls_PaymentTrace : System.Web.UI.UserControl
{
    const string INVOICE_LINK = @"~/clipcall/Receipt.aspx?prid={0}";
    const string STRIPE_CHARGE_URL = @"payments/";
    protected const string NUMBER_FORMAT = "{0:#,0.##}";
    public int row_num { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private DataTable GetNewTable()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Amount");
        dt.Columns.Add("OperationType");
        dt.Columns.Add("IsDone");
        dt.Columns.Add("ChargeLink");
        dt.Columns.Add("HasChargeLink", typeof(bool));
        return dt;
    }
    private DataTable GetNewTableCustomer()
    {
        DataTable dt = GetNewTable();
        dt.Columns.Add("InvoiceLinkLink");
        dt.Columns.Add("HasInvoiceLink", typeof(bool));
        return dt;
    }
    private void SetDataRow(DataTable dt, ClipCallReport.PaymentTraceResponse ptr)
    {
        DataRow row = _getDataRow(dt, ptr);

        dt.Rows.Add(row);
    }
    private void SetDataRowCustomer(DataTable dt, ClipCallReport.PaymentTraceResponse ptr)
    {
        DataRow row = _getDataRow(dt, ptr);
        row["HasInvoiceLink"] = ptr.PaymentResponseId != Guid.Empty;
        row["InvoiceLinkLink"] = (ptr.PaymentResponseId != Guid.Empty) ? string.Format(INVOICE_LINK, ptr.PaymentResponseId) : null;
        dt.Rows.Add(row);
    }
    private DataRow _getDataRow(DataTable dt, ClipCallReport.PaymentTraceResponse ptr)
    {
        DataRow row = dt.NewRow();
        row["Amount"] = string.Format(NUMBER_FORMAT, ptr.Amount);
        row["OperationType"] = ptr.OperationType;
        row["IsDone"] = ptr.IsDone ? "Done" : string.Empty;
        if (!string.IsNullOrEmpty(ptr.StripeLink))
        {
            row["HasChargeLink"] = true;
            row["ChargeLink"] = PpcSite.GetCurrent().StripeBaseUrl + ptr.StripeLink;
        }
        else
            row["HasChargeLink"] = false;
        return row;
    }
    public void LoadData(ClipCallReport.PaymentTraceResponse[] rows)
    {
        DataTable dataSupplier = GetNewTable();

        DataTable dataCustomer = GetNewTableCustomer();
        
        DataTable dataDeal = GetNewTable();
        

        foreach (ClipCallReport.PaymentTraceResponse ptr in rows)
        {
            if (ptr.Part == ClipCallReport.ePartResponse.DEAL)
            {
                SetDataRow(dataDeal, ptr);
            }
            else if (ptr.Part == ClipCallReport.ePartResponse.SUPPLIER)
            {
                SetDataRow(dataSupplier, ptr);               
            }
            else
            {
                SetDataRow(dataCustomer, ptr);                     
            }
        }
        gv_supplier.DataSource = dataSupplier;
        gv_supplier.DataBind();
        gv_customer.DataSource = dataCustomer;
        gv_customer.DataBind();
        gv_deal.DataSource = dataDeal;
        gv_deal.DataBind();

    }
}