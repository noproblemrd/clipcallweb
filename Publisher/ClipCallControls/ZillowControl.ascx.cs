﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallControls_ZillowControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void LoadData(Guid incidentId)
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfListOfZillowRealEstateResponse _data = null;
        try
        {
            _data = report.GetZillowRealEstate(incidentId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
      //      ClosePopupByError();
            return;
        }
        if (_data.Type == ClipCallReport.eResultType.Failure)
        {
    //        ClosePopupByError();
            return;
        }
        if (_data.Value == null)
            return;
        string DateFormat = ((PublisherPage)Page).siteSetting.DateTimeFormat;

        DataTable dt = new DataTable();
        dt.Columns.Add("Numbered");
        dt.Columns.Add("ZillowId");
        dt.Columns.Add("PropertType");
        dt.Columns.Add("YearOfBuilt");
        dt.Columns.Add("FinishSqFt");
        dt.Columns.Add("LotSizeSqFt");
        dt.Columns.Add("LastSoldPrice");
        dt.Columns.Add("LastSoldDate");
        dt.Columns.Add("EstimatedAmount");
        dt.Columns.Add("EstimatedRent");
        int i = 1;
        foreach(ClipCallReport.ZillowRealEstateResponse data in _data.Value)
        {
            DataRow row = dt.NewRow();
            row["Numbered"] = i;
            row["ZillowId"] = data.ZillowId;
            row["PropertType"] = data.PropertyType;
            row["YearOfBuilt"] = data.YearBuilt == 0 ? "" : data.YearBuilt.ToString();
            row["FinishSqFt"] = data.FinishSqFt == 0 ? "" : data.FinishSqFt.ToString();
            row["LotSizeSqFt"] = data.LotSizeSqFt == 0 ? "" : data.LotSizeSqFt.ToString();
            row["LastSoldPrice"] = data.LastSoldPrice == 0 ? "" : data.LastSoldPrice.ToString("C");
            row["LastSoldDate"] = data.LastSoldDate == DateTime.MinValue ? "" : string.Format(DateFormat, data.LastSoldDate);
            row["EstimatedAmount"] = data.EstimatedAmount == 0 ? "" : data.EstimatedAmount.ToString("C");
            row["EstimatedRent"] = data.EstimatedRent == 0 ? "" : data.EstimatedRent.ToString("C");
            dt.Rows.Add(row);
            i++;

        }
        _dataList.DataSource = dt;
        _dataList.DataBind();
    
        
    }
}