﻿function PagingObject(ControlClass, path) {
    this.ControlClass = ControlClass;
    this.Path = path;
    this.CallServer = function () {
        $(window).unbind('scroll');
        if (typeof showDiv == 'function')
            showDiv();
        var _this = this;
        $.ajax({
            url: this.Path,
            data: null,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                if (data.d.length == 0 || data.d == "done") {
                    $(window).unbind('scroll');
                    return;
                }
                _this.ReciveServerData(data.d);
                _this.Start();

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //              alert(textStatus);
            },
            complete: function (jqXHR, textStatus) {
                //  HasInHeadingRequest = false;
                if (typeof hideDiv == 'function')
                    hideDiv();
                

            }

        });
    };
    
    this.ReciveServerData = function (retValue) {

        $(window).scroll(this.Scroll);
        if (retValue.length == 0)
            return;
        var _trs = $(retValue).get(0).getElementsByTagName('tr');
        var _length = _trs.length;
        for (var i = 1; i < _length; i++) {
            //         _trs[1].className = ((i % 2 == 1) ? "_even" : "_odd");
 //           tr_click(_trs[1]);
            $('.' + this.ControlClass).append(_trs[1]);
        }

    };
    this.Scroll = function () {
        if ($(window).scrollTop() > 0.9 * ($(document).height() - $(window).height())) {
            $(window).unbind('scroll');
            _this.CallServer();
        }
    };
    this.Start = function () {
        $(window).scroll(function () {
            _this.Scroll();
        });
    };
    var _this = this;
    //this.Start();
}

