﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChatControl.ascx.cs" Inherits="Publisher_ClipCallControls_ChatControl" %>
<script type="text/javascript">
    function OpenWin(url) {
        var _url = url;
        var chatWin = window.open(_url, "chatWin_", "resizable=0,width=900,height=900");
        return false;
    }
</script>
<div>
    <asp:GridView ID="_GridView"  Width="580px" CssClass="data-table2 -GridView" runat="server" AutoGenerateColumns="false">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                    <Columns>
                          <asp:TemplateField>
                              <HeaderStyle Width="120" />
                        <HeaderTemplate>
                            <asp:Label ID="Label15" runat="server" Text="Sent At"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label17" runat="server" Text='<%# Eval("SentAt") %>' CssClass="date-time-utc"></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderStyle Width="120" />
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="By"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("By") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Left" CssClass="td_message" />
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="ProMessage"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("ProMessage") %>' Visible='<%# Eval("ShowLable") %>'></asp:Label>
                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("ProMessage") %>' Visible='<%# Eval("ShowLink") %>' OnClientClick='<%# Eval("ProScript") %>'></asp:LinkButton>
                            <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("ProTextLink") %>' Visible='<%# Eval("ProHasLink") %>' NavigateUrl='<%# Eval("ProLink") %>' Target="_blank"></asp:HyperLink>
                            <asp:Image ID="imgProSms" runat="server" Height="15px" Width="15px" ImageUrl="~/Publisher/images/if_SMS.png" style="position:absolute;top:0;right:0px;" Visible='<%# Eval("ifProSms") %>' />
                            <asp:Image ID="imgPro" runat="server" Height="15px" Width="15px" ImageUrl='<%# Eval("imgPro") %>' style="float:right;" />
                        </ItemTemplate>
                        </asp:TemplateField>   
                        
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Left"  CssClass="td_message"/>
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="CustMessage"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("CustMessage") %>' Visible='<%# Eval("ShowLable") %>'></asp:Label>
                            <asp:LinkButton ID="LinkButton2" runat="server" Text='<%# Eval("CustMessage") %>' Visible='<%# Eval("ShowLink") %>' OnClientClick='<%# Eval("CustScript") %>'></asp:LinkButton>
                            <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("CustTextLink") %>' Visible='<%# Eval("CustHasLink") %>' NavigateUrl='<%# Eval("CustLink") %>' Target="_blank"></asp:HyperLink>
                            <asp:Image ID="imgCustSms" runat="server" Height="15px" Width="15px" ImageUrl="~/Publisher/images/if_SMS.png" style="position:absolute;top:0;right:0px;" Visible='<%# Eval("ifCustSms") %>'/>
                            <asp:Image ID="imgCust" runat="server" Height="15px" Width="15px" ImageUrl='<%# Eval("imgCust") %>' style="float:right;"/>
                        </ItemTemplate>
                        </asp:TemplateField>            
                        
                      
                       
                    </Columns>
                    </asp:GridView>
</div>