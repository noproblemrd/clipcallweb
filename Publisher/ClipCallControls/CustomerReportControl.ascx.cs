﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallControls_CustomerReportControl : PagingUserControlBase<ClipCallReport.CustomerRowReport>//System.Web.UI.UserControl
{
    const string SESSION_TABLE_NAME = "CustomerTable";


    public override string SessionTableName
    {
        get
        {
            return SESSION_TABLE_NAME;
        }
    }
    

    public override void BindReport(ClipCallReport.CustomerRowReport[] datas)
    {
        ClearSessions();
        DataTable dt = new DataTable();
        dt.Columns.Add("CustomerId");
        dt.Columns.Add("CreatedOn");
        dt.Columns.Add("Phone");
        dt.Columns.Add("Name");
        dt.Columns.Add("Email");
        dt.Columns.Add("Region");
        dt.Columns.Add("HasFacebook");
        dt.Columns.Add("Credit");
        dt.Columns.Add("Device");
        dt.Columns.Add("ClipCallVer");
        dt.Columns.Add("Status");
        dt.Columns.Add("DisplayFacebbokIcon", typeof(bool));
        dt.Columns.Add("Membership");

        dt.Columns.Add("Origin");
        dt.Columns.Add("OriginalOrigin");
        dt.Columns.Add("TransferOrigin");
        dt.Columns.Add("Carrier");
        dt.Columns.Add("RequestReport");
        dt.Columns.Add("VerizonSkinPopupAppeared");
 //       for (int i = 0; i < 12; i++)
//        {
            foreach (var data in datas)
            {
                DataRow row = dt.NewRow();
                row["CustomerId"] = data.CustomerId;
                row["CreatedOn"] = string.Format(Utilities.DATE_TIME_FORMAT, data.CreatedOn);
                row["Phone"] = data.Phone;
                row["Name"] = data.Name;
                row["Region"] = data.Region;
                row["Email"] = data.Email;
                if (string.IsNullOrEmpty(data.FacebookId))
                {
                    row["DisplayFacebbokIcon"] = false;
                }
                else
                {
                    row["HasFacebook"] = Utilities.V_ICON;
                    row["DisplayFacebbokIcon"] = true;
                }
                row["Credit"] = string.Format(Utilities.NUM_FORMAT, data.Credit);
                row["Device"] = data.Device;
                row["ClipCallVer"] = data.ClipCallVersion;
                row["Status"] = data.Status;
                row["Membership"] = data.Membership;

                row["Origin"] = data.ReportOrigin;
                row["OriginalOrigin"] = data.Origin;
                row["Carrier"] = data.CarrierName;
                row["RequestReport"] = string.Format(Utilities.GET_CUSTOMER_REQUEST_REPORT_URL, data.CustomerId);
                row["VerizonSkinPopupAppeared"] = data.VerizonSkinPopupAppeared ? "Yes" : "No";
                dt.Rows.Add(row);
            }
//        }
        SetDataV(dt);
        IEnumerable<DataRow> query = (from x in dt.AsEnumerable()
                                      select x).Take(20);
        Data_Bind((query.Count() == 0) ? null : query.CopyToDataTable());
     //   _GridView.DataSource = (query.Count() == 0) ? null : query.CopyToDataTable();
        //    lbl_RecordMached.Text = result.Value.Length.ToString();
   //     _GridView.DataBind();
        //       _UpdatePanel.Update();
    }
    protected override void Data_Bind(DataTable dt)
    {
        _GridView.DataSource = dt;
        //    lbl_RecordMached.Text = result.Value.Length.ToString();
        _GridView.DataBind();
    }
    
   

  
}