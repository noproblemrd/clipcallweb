﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ZillowControl.ascx.cs" Inherits="Publisher_ClipCallControls_ZillowControl" %>
<div style="overflow-y:auto; max-height:450px;">
<asp:DataList ID="_dataList" runat="server">
    <ItemStyle CssClass="td_zillow" />
    <ItemTemplate>
        
            <div>
                <asp:Label ID="Label8" runat="server" Text='<%# Bind("Numbered") %>'></asp:Label>
            </div>
            <div class="clear"></div>
             <div class="form-fieldshort">
                <asp:Label ID="lbl_Heading" runat="server" CssClass="label" Text="ZillowId"></asp:Label>
                <asp:TextBox ID="txt_ZillowId" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text='<%# Bind("ZillowId") %>'></asp:TextBox>
            </div>
            <div class="form-fieldshort">
                <asp:Label ID="lbl_Region" runat="server" CssClass="label" Text="Propert type"></asp:Label>
                 <asp:TextBox ID="txt_PropertType" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text='<%# Bind("PropertType") %>'></asp:TextBox>
            </div>
            <div class="form-fieldshort">
                <asp:Label ID="Label5" runat="server" CssClass="label" Text="Year of built"></asp:Label>
                <asp:TextBox ID="txt_YearOfBuilt" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text='<%# Bind("YearOfBuilt") %>'></asp:TextBox>
            </div>
             <div class="form-fieldshort">
                <asp:Label ID="Label1" runat="server" CssClass="label" Text="Finish sq ft"></asp:Label>
                <asp:TextBox ID="txt_FinishSqFt" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text='<%# Bind("FinishSqFt") %>'></asp:TextBox>
            </div>
            <div class="form-fieldshort">
                <asp:Label ID="Label2" runat="server" CssClass="label" Text="Lot size sq ft"></asp:Label>
                 <asp:TextBox ID="txt_LotSizeSqFt" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text='<%# Bind("LotSizeSqFt") %>'></asp:TextBox>
            </div>
            <div class="clear"></div>
            <div class="form-fieldshort">
                <asp:Label ID="Label3" runat="server" CssClass="label" Text="Last sold price"></asp:Label>
                <asp:TextBox ID="txt_LastSoldPrice" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text='<%# Bind("LastSoldPrice") %>'></asp:TextBox>
            </div>
            <div class="form-fieldshort">
                <asp:Label ID="Label4" runat="server" CssClass="label" Text="Last sold date"></asp:Label>
                 <asp:TextBox ID="txt_LastSoldDate" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text='<%# Bind("LastSoldDate") %>'></asp:TextBox>
            </div>
            <div class="clear"></div>

            <div class="form-fieldshort">
                <asp:Label ID="Label6" runat="server" CssClass="label" Text="Estimated amount"></asp:Label>
                 <asp:TextBox ID="txt_EstimatedAmount" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text='<%# Bind("EstimatedAmount") %>'></asp:TextBox>
            </div>
            <div class="form-fieldshort">
                <asp:Label ID="Label7" runat="server" CssClass="label" Text="Estimated rent"></asp:Label>
                 <asp:TextBox ID="txt_EstimatedRent" runat="server" CssClass="form-textshort label_ro" ReadOnly="true" Text='<%# Bind("EstimatedRent") %>'></asp:TextBox>
            </div>
   
        
    </ItemTemplate>
</asp:DataList>
</div>
