﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PaymentTrace.ascx.cs" Inherits="Publisher_ClipCallControls_PaymentTrace" %>
<div>
    <asp:GridView ID="gv_deal" runat="server" CssClass="data-table2" AutoGenerateColumns="false" Caption="Deal">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />                        
                    <Columns>
                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label101" runat="server" Text="Amount"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label102" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label103" runat="server" Text="Operation Type"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label104" runat="server" Text='<%# Bind("OperationType") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label105" runat="server" Text="Is Done"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label106" runat="server" Text='<%# Bind("IsDone") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label107" runat="server" Text="Charge link"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl='<%# Bind("ChargeLink") %>' Visible='<%# Bind("HasChargeLink") %>'>Link</asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
        </asp:GridView>
    <asp:GridView ID="gv_supplier" runat="server" CssClass="data-table2" AutoGenerateColumns="false" Caption="Supplier">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />                        
                    <Columns>
                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label1" runat="server" Text="Amount"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="Operation Type"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("OperationType") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="Is Done"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("IsDone") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label7" runat="server" Text="Charge link"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink2" runat="server" Target="_blank" NavigateUrl='<%# Bind("ChargeLink") %>' Visible='<%# Bind("HasChargeLink") %>'>Link</asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
        </asp:GridView>

     <asp:GridView ID="gv_customer" runat="server" CssClass="data-table2" AutoGenerateColumns="false" Caption="Customer">
                    <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />                        
                    <Columns>
                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label201" runat="server" Text="Amount"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label202" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label203" runat="server" Text="Operation Type"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label204" runat="server" Text='<%# Bind("OperationType") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label205" runat="server" Text="Is Done"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label206" runat="server" Text='<%# Bind("IsDone") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label207" runat="server" Text="Charge link"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink201" runat="server" Target="_blank" NavigateUrl='<%# Bind("ChargeLink") %>' Visible='<%# Bind("HasChargeLink") %>'>Link</asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Label217" runat="server" Text="Invoice"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink211" runat="server" Target="_blank" NavigateUrl='<%# Bind("InvoiceLinkLink") %>' Visible='<%# Bind("HasInvoiceLink") %>'>Invoice</asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
        </asp:GridView>
</div>
