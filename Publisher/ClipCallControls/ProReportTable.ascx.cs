﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallControls_ProReportTable : System.Web.UI.UserControl
{
    const string UNKNOWN = "unknown";
    const string Num_Format = "{0:#,0.##}";
    const string V_ICON = "~/Publisher/images/icon-v.png";
    const string X_ICON = "~/Publisher/images/icon-x.png";

   // protected string TableSessionName { get; set; }
    public string GetNumberFormat
    {
        get { return Num_Format; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void BindReport(ClipCallReport.RegistrationAppReportResponse[] datas, string tableSessionName)
    {
        PageIndex = 0;
        DataTable data = new DataTable();
        data.Columns.Add("LastUpdate");
        data.Columns.Add("CreatedOn");
        data.Columns.Add("Name");
        data.Columns.Add("Categories");
        data.Columns.Add("Address");
        data.Columns.Add("Step");
        data.Columns.Add("Phone");
        data.Columns.Add("Email");
        data.Columns.Add("Device");
        data.Columns.Add("LicenseIcon");
        data.Columns.Add("Insured");
        data.Columns.Add("bbb");
        data.Columns.Add("bbbV", typeof(bool));
        data.Columns.Add("WorkerscompIcon");
        data.Columns.Add("BondedIcon");
        data.Columns.Add("CriminalRecordIcon");
        data.Columns.Add("WebSite");
        data.Columns.Add("WebSiteV", typeof(bool));
        data.Columns.Add("Description");
        data.Columns.Add("PortraitImage");
        data.Columns.Add("Radius");
        data.Columns.Add("FirstName");
        data.Columns.Add("LastName");
        data.Columns.Add("YelpPhone");
        data.Columns.Add("YelpRating");
        data.Columns.Add("YelpReviews");
        data.Columns.Add("YelpName");
        data.Columns.Add("YelpUrl");
        data.Columns.Add("YelpUrlV", typeof(bool));
        data.Columns.Add("GoogleRating");
        data.Columns.Add("GoogleReviews");
        data.Columns.Add("GoogleUrl");
        data.Columns.Add("GoogleUrlV", typeof(bool));
        data.Columns.Add("OldProIcon");
        data.Columns.Add("Status");
        data.Columns.Add("InactiveReason");
        data.Columns.Add("StripeAccount");
        data.Columns.Add("StripeStandaloneAccount");

        data.Columns.Add("LastActive");
        data.Columns.Add("OSversion");
        data.Columns.Add("Rating");

        data.Columns.Add("CanInactive", typeof(bool));
        data.Columns.Add("CanActive", typeof(bool));
        data.Columns.Add("SupplierId");

        data.Columns.Add("IdentityDocUrl");
        data.Columns.Add("HasIdentityDoc", typeof(bool));


        foreach (ClipCallReport.RegistrationAppReportResponse row in datas)
        {
            DataRow dr = data.NewRow();
            dr["SupplierId"] = row.SupplierId;
            dr["CreatedOn"] = string.Format(SiteSetting.GetDateTimeFormat, row.CreatedOn);
            dr["LastUpdate"] = row.RegistrationLastUpdate == DateTime.MinValue ? UNKNOWN : string.Format(SiteSetting.GetDateTimeFormat, row.RegistrationLastUpdate);
            dr["Name"] = row.SupplierName;
            dr["Categories"] = row.Categories;
            dr["Address"] = row.FullAddress;
            dr["Step"] = row.Step.ToString();
            dr["phone"] = row.Phone;
            dr["Email"] = row.Email;
            dr["Device"] = row.OS;
            dr["OSversion"] = row.OSversion;
            dr["LastActive"] = row.LastActive == DateTime.MinValue ? UNKNOWN : string.Format(SiteSetting.GetDateTimeFormat, row.LastActive);

            dr["LicenseIcon"] = string.IsNullOrEmpty(row.LicenseId) ? X_ICON : V_ICON;
            dr["Insured"] = row.Insuredcoverag > 0 ? string.Format(this.GetNumberFormat, row.Insuredcoverag) : string.Empty;

            dr["bbbV"] = !string.IsNullOrEmpty(row.BBBProfileUrl);
            dr["bbb"] = GetUrl(row.BBBProfileUrl, true);

            dr["WorkerscompIcon"] = !row.HasWorkersCompensation ? X_ICON : V_ICON;
            dr["BondedIcon"] = !row.IsBonded ? X_ICON : V_ICON;
            dr["CriminalRecordIcon"] = row.HasCriminalRecords ? X_ICON : V_ICON;
            dr["WebSite"] = GetUrl(row.Website, false);
            dr["WebSiteV"] = !string.IsNullOrEmpty(row.Website);
            dr["Description"] = row.Description;
            dr["PortraitImage"] = row.BusinessImageUrl;

            dr["Radius"] = row.Radius;
            dr["FirstName"] = row.FirstName;
            dr["LastName"] = row.LastName;
            dr["YelpPhone"] = row.YelpPhone;
            dr["YelpRating"] = row.YelpRating < 0 ? "-" : string.Format(Num_Format, row.YelpRating);
            dr["YelpReviews"] = row.YelpReviewCount < 0 ? "-" : row.YelpReviewCount.ToString();
            dr["YelpName"] = row.YelpName;
            dr["YelpUrl"] = GetUrl(row.YelpUrl, true);
            dr["YelpUrlV"] = !string.IsNullOrEmpty(row.YelpUrl);
            dr["GoogleRating"] = row.GoogleRating < 0 ? "-" : string.Format(Num_Format, row.GoogleRating);
            dr["GoogleReviews"] = row.GoogleReviewCount < 0 ? "-" : row.GoogleReviewCount.ToString();
            dr["GoogleUrl"] = GetUrl(row.GoogleUrl, true);
            dr["GoogleUrlV"] = !string.IsNullOrEmpty(row.GoogleUrl);
            dr["OldProIcon"] = row.OldPro == ClipCallReport.eSupplierOldStatus.Old ? V_ICON : X_ICON;

            dr["Status"] = Utilities.GetEnumStringFormat(row.SupplierStatus);
            dr["InactiveReason"] = row.InactiveReason.HasValue ? row.InactiveReason.Value.ToString() : null;
            dr["StripeAccount"] = row.StripeAccountId;
            dr["StripeStandaloneAccount"] = row.StripeStandaloneAccountId;
            dr["Rating"] = row.AccountRating < 0 ? "not supplied" : row.AccountRating.ToString();

            dr["CanInactive"] = row.SupplierStatus == ClipCallReport.eSupplierStatusAppRegistration.Approved;
            dr["CanActive"] = row.SupplierStatus == ClipCallReport.eSupplierStatusAppRegistration.Inactive
                && row.InactiveReason.HasValue &&
                (row.InactiveReason.Value == ClipCallReport.eInactiveReason.Blocked || row.InactiveReason.Value == ClipCallReport.eInactiveReason.NotConfirmedServiceLocation);

            dr["IdentityDocUrl"] = row.IdentityDocUrl;
            dr["HasIdentityDoc"] = !string.IsNullOrEmpty(row.IdentityDocUrl);

            data.Rows.Add(dr);
        }
        //        lbl_RecordMached.Text = response.Value.Length.ToString();
        SetDataV(tableSessionName, data);
        IEnumerable<DataRow> query = (from x in data.AsEnumerable()
                                      select x).Take(20);

        _reapeter.DataSource = (query.Count() == 0) ? null : query.CopyToDataTable();
        _reapeter.DataBind();
        
        
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow row = (HtmlTableRow)(e.Item.FindControl("tr_show"));
        string css_class = (e.Item.ItemIndex % 2 == 0) ? "odd" : "even";
        string exists_class = row.Attributes["class"];
        row.Attributes.Add("class", string.IsNullOrEmpty(exists_class) ?
            css_class : exists_class + " " + css_class);
    }
    public bool SetDataByPage(string tableSessionName)
    {
        DataTable data = GetDataV(tableSessionName);
        if (data == null)
            return false;
        PageIndex = PageIndex + 1;
        int _index = PageIndex;
        int _min = (_index * 20);
        IEnumerable<DataRow> data_row = (from x in data.AsEnumerable()
                                         select x).Skip(_min).Take(20);

        if (data_row.Count() == 0)
            return false;
        _reapeter.DataSource = data_row.CopyToDataTable();
        _reapeter.DataBind();
        return true;
    }
    private string GetUrl(string url, bool isHttps)
    {
        if (string.IsNullOrEmpty(url))
            return null;
        if (url.StartsWith("http"))
            return url;
        return string.Format("http{0}://{1}", (isHttps ? "s" : ""), url);
    }
    DataTable GetDataV(string TableSessionName)
    {
        return (Session[TableSessionName] == null) ? null : (DataTable)Session[TableSessionName];
    }
    void SetDataV(string TableSessionName, DataTable dt)
    {
        Session[TableSessionName] = dt;
    }
        /*
    DataTable 
    {
        get { return (Session[TableSessionName] == null) ? null : (DataTable)Session[TableSessionName]; }
        set { Session[TableSessionName] = value; }
    }
    */
    int PageIndex
    {
        get { return (Session["PageIndexProReportTable"] == null) ? 0 : (int)Session["PageIndexProReportTable"]; }
        set { Session["PageIndexProReportTable"] = value; }
    }
    /*
    protected string TableSessionName 
    {
        get { return (ViewState["TableSessionName"] == null) ? string.Empty : (string)ViewState["TableSessionName"]; }
        set { ViewState["TableSessionName"] = value; }
    }
    */
    
}