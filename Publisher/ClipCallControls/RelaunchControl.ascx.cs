﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class Publisher_ClipCallControls_RelaunchControl : System.Web.UI.UserControl
{
    PageSetting __Page;
    const string CONFIGURATION_KEY = "MaxRegionLevel";
    protected string MyRedirectPath { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        HtmlGenericControl _script = new HtmlGenericControl("script");
        _script.Attributes.Add("src", Page.ResolveUrl("~") + "Controls/Upsale/UpsaleScript.js");
        _script.Attributes.Add("language", "javascript");
        _script.Attributes.Add("type", "text/javascript");
        
         * */


        //  ScriptUpsale.
        __Page = (PageSetting)Page;
      //  LoadTranslate();
        
        a_close.Attributes.Add("onclick", "javascript:ClosePopUpV();");
        a_closeV2.Attributes.Add("onclick", "javascript:ClosePopUpV();");
        txt_City.Attributes.Add("autocomplete", "off");

        if (!IsPostBack)
        {
            RegularExpressionValidator_email.ValidationExpression = __Page.GetEmailRegulareExpresion;
            /*
            if (!string.IsNullOrEmpty(__Page.siteSetting.GetPhoneRegularExpression))
                RegularExpressionValidatorPhone.ValidationExpression = __Page.siteSetting.GetPhoneRegularExpression;
             * */
            if (__Page is IOnLoadControl)
                ((IOnLoadControl)__Page).__OnLoad(this);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "SetScheduleDate", "SetScheduleDate();", true);
        }
        this.DataBind();

    }
    /*
    private void LoadTranslate()
    {
        DBConnection.LoadTranslateToControl(this, "UpsaleControl.ascx", __Page.siteSetting.siteLangId);
    }
     */
    public void LoadDetails(Guid RequestId, string name, string email, string path)
    {
        txt_Name.Text = name;
        txt_email.Text = email;
        MyRedirectPath = path;
        LoadDetails(RequestId);
    }
    public void LoadDetails(Guid RequestId)
    {
        RequestIdV = RequestId;
        LoadExperties();
        LoadNumOfRegionLevel();
        this.DataBind();
    }
    public void LoadDetails(ListItem[] ExpertiseListItem)
    {
        if (ExpertiseListItem != null && ExpertiseListItem.Length > 0)
            LoadExperties(ExpertiseListItem);
        LoadNumOfRegionLevel();
    }
    public string GetLoadDetailsScript(string _phone, string UpsaleId, string ExpertiseId, string _description)
    {
        return "upsale('" + _phone + "', '" + UpsaleId + "', '" +
            ExpertiseId + "', '" + HttpUtility.JavaScriptStringEncode(_description) + "');";
    }
    public string GetLoadDetailsScript(string _phone, string UpsaleId, string ExpertiseCode, string _region, int regionLevel, string _description)
    {
        if (!string.IsNullOrEmpty(_region) && _region.Contains(';'))
            _region = _region.Split(';')[0];
        return "upsaleWithRegion('" + _phone + "', '" + UpsaleId + "', '" +
            ExpertiseCode + "', '" + _region + "', '" + regionLevel + "', '" + HttpUtility.JavaScriptStringEncode(_description) + "');";
    }
    protected string GetPath_SetCitiesItemsUpsale
    {
        get
        {
            return Page.ResolveUrl("~") + "Publisher/AutoComplete.asmx/SetCitiesItemsUpsale";
        }
    }
    private void LoadExperties()
    {
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(__Page);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(__Page.siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, __Page.siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;// nodePrimary.Attributes["Name"].InnerText;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Expertiser.Items.Add(new ListItem(pName, _guid));
        }
    }
    private void LoadExperties(ListItem[] lic)
    {
        ddl_Expertiser.Items.AddRange(lic);
    }
    private void LoadNumOfRegionLevel()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(__Page);
        string result = string.Empty;
        try
        {
            result = _site.GetConfigurationSettings();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, __Page.siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);
        if (xdd.Element("Configurations") == null || xdd.Element("Configurations").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        int _level = 1;
        foreach (XElement node in xdd.Element("Configurations").Elements())
        {
            if (node.Element("Key").Value == CONFIGURATION_KEY)
            {
                _level = int.Parse(node.Element("Value").Value);
                break;
            }
        }
        /*
        for (int i = 0; i < _level; i++)
        {
            string level_name = EditTranslateDB.GetControlTranslate(__Page.siteSetting.siteLangId, "eRegionLevel", ((eRegionLevel)i + 1).ToString());
            ddl_regionLevel.Items.Add(new ListItem(level_name, (i + 1).ToString()));
        }
         * */
        string level_name = "zipcode";
        ddl_regionLevel.Items.Add(new ListItem(level_name, (4).ToString()));
     //   ddl_regionLevel.SelectedIndex = 0;
    }
    public int YPosition
    {
        set { _mpeV.Y = value; }
    }
    public void SetCompleteRequestScript(string _script)
    {

        string script = "function Complete_request() {" + _script + "}";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Complete_request", script, true);

    }
    /*
    string CompleteRequestScriptV
    {
        get { return (ViewState["CompleteRequestScript"] == null) ? "function Complete_request() {}" : (string)ViewState["CompleteRequestScript"]; }
        set { ViewState["CompleteRequestScript"] = value; }
    }
     * */
    protected string NoResult
    {
        get { return HttpUtility.HtmlEncode(lbl_NoResult.Text); }
    }
    protected Guid RequestIdV
    {
        get { return (ViewState["RequestId"] == null) ? Guid.Empty : (Guid)ViewState["RequestId"]; }
        set { ViewState["RequestId"] = value; }
    }
    /*
   protected string createServiceRequestSchedule
   {
       get { return ResolveUrl("~/Publisher/Auction/Auction.asmx/createServiceRequestSchedule"); }
   }
   
   protected string createServiceRequest
   {
       get { return ResolveUrl("~/Publisher/Auction/Auction.asmx/createServiceRequest"); }
   }
    * */
    protected string RelaunchCustonIncident
    {

        get { return ResolveUrl("~/Publisher/ClipCallReportService.asmx/RelaunchCustomIncident"); }
    }
    protected string GetSuplliersService
    {
        get { return ResolveUrl("~/Publisher/MobileAppRequestTicket.aspx/GetSuplliers"); }
    }
}