﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProReportTable.ascx.cs" Inherits="Publisher_ClipCallControls_ProReportTable" %>

<asp:Repeater runat="server" ID="_reapeter" 
    onitemdatabound="_reapeter_ItemDataBound">
    <HeaderTemplate>
        <table class="data-table -GridView">
            <thead>
                <tr>               
                    <th><asp:Label ID="Label49" runat="server" Text="Created on"></asp:Label></th>                 
                    <th><asp:Label ID="Label31" runat="server" Text="Last update"></asp:Label></th>
					<th><asp:Label ID="Label50" runat="server" Text="Name"></asp:Label></th>
                    <th><asp:Label ID="Label19" runat="server" Text="Status"></asp:Label></th>
                    <th><asp:Label ID="Label58" runat="server" Text="Inactive reason"></asp:Label></th>
                    <th><asp:Label ID="Label9" runat="server" Text="Categories"></asp:Label></th>
					<th><asp:Label ID="Label1" runat="server" Text="Address"></asp:Label></th>
					<th><asp:Label ID="Label51" runat="server" Text="Step"></asp:Label></th>
                        <th><asp:Label ID="Label220" runat="server" Text="Phone"></asp:Label></th>
					<th><asp:Label ID="Label32" runat="server" Text="Email"></asp:Label></th>
					<th><asp:Label ID="Label53" runat="server" Text="Device"></asp:Label></th>
                    <th><asp:Label ID="Label45" runat="server" Text="OSversion"></asp:Label></th>
                    <th><asp:Label ID="Label47" runat="server" Text="Last active"></asp:Label></th>
                    <th><asp:Label ID="Label2" runat="server" Text="License Number"></asp:Label></th>
                    <th><asp:Label ID="Label10" runat="server" Text="Insured"></asp:Label></th>
                    <th><asp:Label ID="Label11" runat="server" Text="BBB profile"></asp:Label></th>
                    <th><asp:Label ID="Label12" runat="server" Text="Worker's comp"></asp:Label></th>
                    <th><asp:Label ID="Label13" runat="server" Text="Bonded"></asp:Label></th>
                    <th><asp:Label ID="Label7" runat="server" Text="Doesn't has criminal record"></asp:Label></th>
                    <th><asp:Label ID="Label18" runat="server" Text="Is old pro"></asp:Label></th>
                    <th><asp:Label ID="Label63" runat="server" Text="Driver license"></asp:Label></th>                                
                    <th><asp:Label ID="Label54" runat="server" Text="Rating"></asp:Label></th>
                    <th><asp:Label ID="Label61" runat="server" Text="Active/Inactive"></asp:Label></th>
				</tr>
            </thead>
            <tbody>
    </HeaderTemplate>

    <ItemTemplate>
        <tr runat="server" id="tr_show" class="tr_show" style="cursor:pointer">   
            <td><asp:Label ID="Label52" runat="server" Text='<%# Bind("CreatedOn") %>'></asp:Label></td>                     
			<td><asp:Label ID="Label3" runat="server" Text='<%# Bind("LastUpdate") %>'></asp:Label></td>
			<td><asp:Label ID="Label55" runat="server" Text='<%# Bind("Name") %>'></asp:Label></td>
            <td><asp:Label ID="Label38" runat="server" Text='<%# Bind("Status") %>'></asp:Label></td>
            <td><asp:Label ID="Label59" runat="server" Text='<%# Bind("InactiveReason") %>'></asp:Label></td>
            <td><asp:Label ID="Label8" runat="server" Text='<%# Bind("Categories") %>'></asp:Label></td>
            <td><asp:Label ID="Label221" runat="server" Text='<%# Bind("Address") %>'></asp:Label></td>
			<td><asp:Label ID="Label56" runat="server" Text='<%# Bind("Step") %>'></asp:Label></td>
			<td><asp:Label ID="Label4" runat="server" Text='<%# Bind("Phone") %>'></asp:Label></td>
			<td><asp:Label ID="Label5" runat="server" Text='<%# Bind("Email") %>'></asp:Label></td>
            <td><asp:Label ID="Label6" runat="server" Text='<%# Bind("Device") %>'></asp:Label></td>
            <td><asp:Label ID="Label46" runat="server" Text='<%# Bind("OSversion") %>'></asp:Label></td>
            <td><asp:Label ID="Label48" runat="server" Text='<%# Bind("LastActive") %>'></asp:Label></td>
			<td><asp:Image ID="Image2" runat="server" ImageUrl='<%# Bind("LicenseIcon") %>'></asp:Image></td>
            <td><asp:Label ID="Label15" runat="server" Text='<%# Bind("Insured") %>'></asp:Label></td>
            <td><asp:HyperLink ID="HyperLink1" runat="server" Text="bbb" NavigateUrl='<%# Bind("bbb") %>' Visible='<%# Bind("bbbV") %>' Target="_blank"></asp:HyperLink></td>
            <td><asp:Image ID="Image4" runat="server" ImageUrl='<%# Bind("WorkerscompIcon") %>'></asp:Image></td>
                <td><asp:Image ID="Image5" runat="server" ImageUrl='<%# Bind("BondedIcon") %>'></asp:Image></td>
            <td><asp:Image ID="Image6" runat="server" ImageUrl='<%# Bind("CriminalRecordIcon") %>'></asp:Image></td>
            <td><asp:Image ID="Image10" runat="server" ImageUrl='<%# Bind("OldProIcon") %>'></asp:Image></td>
            <td>
                <div>
                    <a id="a_identityDoc" runat="server" href='<%# Bind("IdentityDocUrl") %>' visible='<%# Bind("HasIdentityDoc") %>' target="_blank">
                        <asp:Label ID="Label64" runat="server" Text="Driver license"></asp:Label>
                    </a>
                </div>
                            
            </td>
            <td><asp:Label ID="Label57" runat="server" Text='<%# Bind("Rating") %>'></asp:Label></td>
            <td>
                    <div runat="server" id="div_ActiveInactive" class="div_ActiveInactive">
                               
                    <a id="a_inactive" runat="server" onclick="inactive(this);" href="javascript:void(0);" visible='<%# Bind("CanInactive") %>'>
                        <asp:Label ID="Label6612" runat="server" Text="Inactive"></asp:Label>
                    </a>
                                
                        <a id="a_active" runat="server" onclick="active(this);" href="javascript:void(0);" visible='<%# Bind("CanActive") %>'>
                        <asp:Label ID="Label62" runat="server" Text="Active"></asp:Label>
                    </a>
                    <div style="text-align:center; display:none;" class="div_active_loader">
                        <asp:Image ID="img_relaunch" runat="server" ImageUrl="~/Publisher/images/ajax-loader.gif" /> 
                    </div>
                    <span class="span_activeResponse" style="color:red;"></span>
                        <asp:HiddenField ID="hf_supplierId" runat="server" Value='<%# Bind("SupplierId") %>' />
                </div>
            </td>
		</tr>
        <tr id="tr_openClose" runat="server" style="display:none" class="tr_openClose">
            <td colspan="14" class="data-table-details">
                <div class="_left">
                    <asp:Label ID="Label007" runat="server" CssClass="label" Text="Description"></asp:Label>
                    <asp:Label ID="Label14" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                </div>
                <div class="clear"></div>
                    <div class="_left">
                    <asp:Label ID="Label37" runat="server" CssClass="label" Text="Website"></asp:Label>
                        <asp:HyperLink ID="HyperLink2" runat="server" Text="Website" NavigateUrl='<%# Bind("Website") %>' Visible='<%# Bind("WebsiteV") %>' Target="_blank"></asp:HyperLink>
                </div>
                                                       
                    <div class="_left">
                    <asp:Label ID="Label16" runat="server" CssClass="label" Text="Cover area radius"></asp:Label>
                    <asp:Label ID="Label17" runat="server" Text='<%# Bind("Radius") %>'></asp:Label>
                </div>                           
                    <div class="_left">
                    <asp:Label ID="Label20" runat="server" CssClass="label" Text="First name"></asp:Label>
                    <asp:Label ID="Label21" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                </div>
                    <div class="_left">
                    <asp:Label ID="Label22" runat="server" CssClass="label" Text="Last name"></asp:Label>
                    <asp:Label ID="Label23" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                </div>
                <div class="_left">
                    <asp:Label ID="Label39" runat="server" CssClass="label" Text="Stripe account"></asp:Label>
                    <asp:Label ID="Label40" runat="server" Text='<%# Bind("StripeAccount") %>'></asp:Label>
                </div>
                    <div class="_left">
                    <asp:Label ID="Label41" runat="server" CssClass="label" Text="Stripe standalone account"></asp:Label>
                    <asp:Label ID="Label42" runat="server" Text='<%# Bind("StripeStandaloneAccount") %>'></asp:Label>
                </div>
                <div class="clear"></div>
                    <div class="_left">
                    <asp:Label ID="Label24" runat="server" CssClass="label" Text="Yelp phone"></asp:Label>
                    <asp:Label ID="Label25" runat="server" Text='<%# Bind("YelpPhone") %>'></asp:Label>
                </div>
                <div class="_left">
                    <asp:Label ID="Label108" runat="server" CssClass="label" Text="Yelp name"></asp:Label>
                    <asp:Label ID="Label109" runat="server" Text='<%# Bind("YelpName") %>'></asp:Label>
                </div>
                    <div class="_left">
                    <asp:Label ID="Label26" runat="server" CssClass="label" Text="Yelp rating"></asp:Label>
                    <asp:Label ID="Label27" runat="server" Text='<%# Bind("YelpRating") %>'></asp:Label>
                </div>
                    <div class="_left">
                    <asp:Label ID="Label28" runat="server" CssClass="label" Text="Yelp reviews"></asp:Label>
                    <asp:Label ID="Label29" runat="server" Text='<%# Bind("YelpReviews") %>'></asp:Label>
                </div>
                <div class="_left">
                    <asp:Label ID="Label107" runat="server" CssClass="label" Text="Yelp url"></asp:Label>
                    <asp:HyperLink ID="HyperLink5" runat="server" Text="YelpUrl" NavigateUrl='<%# Bind("YelpUrl") %>' Visible='<%# Bind("YelpUrlV") %>' Target="_blank"></asp:HyperLink>
                </div>
                <div class="clear"></div>
                    <div class="_left">
                    <asp:Label ID="Label30" runat="server" CssClass="label" Text="Google rating"></asp:Label>
                    <asp:Label ID="Label33" runat="server" Text='<%# Bind("GoogleRating") %>'></asp:Label>
                </div>
                    <div class="_left">
                    <asp:Label ID="Label34" runat="server" CssClass="label" Text="Google reviews"></asp:Label>
                    <asp:Label ID="Label35" runat="server" Text='<%# Bind("GoogleReviews") %>'></asp:Label>
                </div>
                <div class="_left">
                    <asp:Label ID="Label180" runat="server" CssClass="label" Text="Google url"></asp:Label>
                    <asp:HyperLink ID="HyperLink4" runat="server" Text="GoogleUrl" NavigateUrl='<%# Bind("GoogleUrl") %>' Visible='<%# Bind("GoogleUrlV") %>' Target="_blank"></asp:HyperLink>
                </div>
                <div class="clear"></div>
                    <div class="_left">
                    <asp:Label ID="Label36" runat="server" CssClass="label" Text="Portrait image"></asp:Label>
                    <asp:Image ID="Image7" runat="server" ImageUrl='<%# Bind("PortraitImage") %>' ></asp:Image>
                </div> 

            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </tbody>
    </table>
    </FooterTemplate>
</asp:Repeater>
