﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerReportControl.ascx.cs" Inherits="Publisher_ClipCallControls_CustomerReportControl" %>

<asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
				<RowStyle CssClass="even" />
				<AlternatingRowStyle CssClass="odd" />
				<Columns>
					<asp:TemplateField SortExpression="CreatedOn">
						<HeaderTemplate>
							<asp:Label ID="Label1" runat="server" Text="Created on"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lbl2" runat="server" Text='<%# Bind("CreatedOn") %>'></asp:Label>
                            <asp:HiddenField ID="hf_CustomerId" runat="server" Value='<%# Bind("CustomerId") %>' />
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="Phone">
						<HeaderTemplate>
							<asp:Label ID="Label101" runat="server" Text="Phone"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Bind("Phone") %>' Target="_blank" NavigateUrl='<%# Bind("RequestReport") %>'></asp:HyperLink>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Name">
						<HeaderTemplate>
							<asp:Label ID="Label10" runat="server" Text="Name"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
                            <asp:HyperLink ID="HyperLink2" runat="server" Text='<%# Bind("Name") %>' Target="_blank" NavigateUrl='<%# Bind("RequestReport") %>'></asp:HyperLink>
						</ItemTemplate>
					</asp:TemplateField>					

                    <asp:TemplateField SortExpression="Email">
						<HeaderTemplate>
                            <asp:Label ID="Label30" runat="server" Text="Email"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:HyperLink ID="HyperLink3" runat="server" Text='<%# Bind("Email") %>' Target="_blank" NavigateUrl='<%# Bind("RequestReport") %>'></asp:HyperLink>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField SortExpression="Region">
						<HeaderTemplate>
							<asp:Label ID="Label14" runat="server" Text="Region"></asp:Label> 
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label15" runat="server" Text='<%# Bind("Region") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="HasFacebook">
						<HeaderTemplate>
							<asp:Label ID="Label166" runat="server" Text="HasFacebook"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Image ID="Image5" runat="server" ImageUrl='<%# Bind("HasFacebook") %>' Visible='<%# Bind("DisplayFacebbokIcon") %>'/>
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="Credit">
						<HeaderTemplate>
							<asp:Label ID="Label168" runat="server" Text="Credit"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label178" runat="server" Text='<%# Bind("Credit") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="Device">
						<HeaderTemplate>
							<asp:Label ID="Label169" runat="server" Text="Device"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label179" runat="server" Text='<%# Bind("Device") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="ClipCallVer">
						<HeaderTemplate>
							<asp:Label ID="Label166" runat="server" Text="ClipCall version"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label177" runat="server" Text='<%# Bind("ClipCallVer") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="Status">
						<HeaderTemplate>
							<asp:Label ID="Label186" runat="server" Text="Status"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label187" runat="server" Text='<%# Bind("Status") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="Membership">
						<HeaderTemplate>
							<asp:Label ID="Label196" runat="server" Text="Membership"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label197" runat="server" Text='<%# Bind("Membership") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="Origin">
						<HeaderTemplate>
							<asp:Label ID="Label198" runat="server" Text="Origin"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label199" runat="server" Text='<%# Bind("Origin") %>' CssClass="span_origin"></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="OriginalOrigin">
						<HeaderTemplate>
							<asp:Label ID="Label200" runat="server" Text="Original Origin"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label201" runat="server" Text='<%# Bind("OriginalOrigin") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                    <asp:TemplateField SortExpression="Carrier">
						<HeaderTemplate>
							<asp:Label ID="Label210" runat="server" Text="Carrier"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label211" runat="server" Text='<%# Bind("Carrier") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="VerizonSkinPopupAppeared">
						<HeaderTemplate>
							<asp:Label ID="Label250" runat="server" Text="Verizon Skin Appeared"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="Label251" runat="server" Text='<%# Bind("VerizonSkinPopupAppeared") %>'></asp:Label> 
						</ItemTemplate>
					</asp:TemplateField>

                     <asp:TemplateField SortExpression="TransferOrigin">
						<HeaderTemplate>
							<asp:Label ID="Label202" runat="server" Text="Transfer Origin"></asp:Label>
						</HeaderTemplate>
						<ItemTemplate>
							<div runat="server" id="div_TransferOrigin" class="div_TransferOrigin">
                                <a id="a_TransferOrigin" runat="server" onclick="TransferOrigin(this);" href="javascript:void(0);">
                                    <asp:Label ID="Label6612" runat="server" Text="Transfer Origin"></asp:Label>
                                </a>
                                <div style="text-align:center; display:none;" class="div_TransferOrigin_loader">
                                    <asp:Image ID="img_TransferOrigin" runat="server" ImageUrl="/Publisher/images/ajax-loader.gif" /> 
                                </div>
                                <span class="span_TransferOrigin" style="color:red;"></span>
                            </div>
						</ItemTemplate>
					</asp:TemplateField>
                    	
				</Columns>    
				</asp:GridView>
