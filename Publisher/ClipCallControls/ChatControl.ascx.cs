﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallControls_ChatControl : System.Web.UI.UserControl
{
     static readonly string img_seen;
     static readonly string img_delivered;
     static readonly string img_sent;
     static readonly string img_sms;

    static Publisher_ClipCallControls_ChatControl()
    {
       // LoadMessages();
        img_seen = "~/Publisher/images/icons8-DoubleTick_50_seen.png";
        img_delivered = "~/Publisher/images/icons8-DoubleTick_50.png";
        img_sent = "~/Publisher/images/icons8-Checkmark_50.png";

        img_sms = "~/Publisher/images/if_SMS.png";
        
    }
    protected string GetImgSeen
    {
        get { return ResolveUrl(img_seen); }
    }
    protected string GetImgDelivered
    {
        get { return ResolveUrl(img_delivered); }
    }
    protected string GetImgSent
    {
        get { return ResolveUrl(img_sent); }
    }
    protected string GetImgSms
    {
        get { return ResolveUrl(img_sms); }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void LoadData(ClipCallReport.LayerChatReportResponse response)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("SentAt");
        dt.Columns.Add("By");
        dt.Columns.Add("ProMessage");
        dt.Columns.Add("CustMessage");
        dt.Columns.Add("ShowLable", typeof(bool));
        dt.Columns.Add("ShowLink", typeof(bool));
        dt.Columns.Add("ProScript");
        dt.Columns.Add("CustScript");
        dt.Columns.Add("imgPro");
        dt.Columns.Add("imgCust");
        dt.Columns.Add("ifProSms", typeof(bool));
        dt.Columns.Add("ifCustSms", typeof(bool));

        dt.Columns.Add("ProTextLink");
        dt.Columns.Add("ProHasLink", typeof(bool));
        dt.Columns.Add("ProLink");

        dt.Columns.Add("CustTextLink");
        dt.Columns.Add("CustHasLink", typeof(bool));
        dt.Columns.Add("CustLink");

        foreach (ClipCallReport.LayerChatMessageResponse message in response.layerChatResponse)
        {
            DataRow row = dt.NewRow();
            row["SentAt"] = message.SentAt.GetDateForClient();// string.Format(siteSetting.DateTimeFormat, message.SentAt);
            row["By"] = message.UserId == Guid.Empty ? "ClipCall-bot" :
                message.UserId == response.CustomerId ? (string.IsNullOrEmpty(response.CustomerName) ? "Customer" : response.CustomerName) :
                message.UserId == response.SupplierId ? response.SupplierName : "unknown";
            row["ProMessage"] = message.ProMessage;
            row["CustMessage"] = message.CustMessage;
            bool showLink = message.MessageType == ClipCallReport.eLayerChatMessageType.IMAGE || message.MessageType == ClipCallReport.eLayerChatMessageType.QUOTE ||
                message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO || message.MessageType == ClipCallReport.eLayerChatMessageType.AUDIO ||
                message.MessageType == ClipCallReport.eLayerChatMessageType.PHONE_CALL || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO_CHAT;
            row["ShowLink"] = showLink;
            row["ShowLable"] = !showLink;
            row["ProHasLink"] = false;
            row["CustHasLink"] = false;
            string script = null;
            if (message.MessageType == ClipCallReport.eLayerChatMessageType.QUOTE)
                script = "return OpenWin('" + ResolveUrl("~/Publisher/QuoteTicket.aspx?quoteid=") + message.ContentData + "');";
            else if (message.MessageType == ClipCallReport.eLayerChatMessageType.IMAGE)// || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO)
                script = "return OpenWin('" + message.ContentData + "');";
            else if (message.MessageType == ClipCallReport.eLayerChatMessageType.AUDIO || message.MessageType == ClipCallReport.eLayerChatMessageType.PHONE_CALL
                || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO_CHAT || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO)
            {
                string audioUrl;
                bool? isvideo = null;
                if (message.MessageType == ClipCallReport.eLayerChatMessageType.PHONE_CALL || message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO_CHAT)
                {
                    MediaChatBody phoneCallBody = JsonConvert.DeserializeObject<MediaChatBody>(message.ContentData);
                    audioUrl = phoneCallBody.url;
        //            isvideo = true;
                }                
                else
                {
                    audioUrl = message.ContentData;
         //           isvideo = false;
                }
                if (message.MessageType == ClipCallReport.eLayerChatMessageType.VIDEO)
                    isvideo = true;
                GenerateVideoLink gvl = new GenerateVideoLink(audioUrl, null);
                script = "return OpenWin('" + gvl.GenerateFullUrl(isvideo) + "');";
            }
            else if(message.MessageType == ClipCallReport.eLayerChatMessageType.TEXT_LINK)
            {
                if (!string.IsNullOrEmpty(message.ProLink.link))
                {
                    row["ProTextLink"] = string.IsNullOrEmpty(message.ProLink.text) ? message.ProLink.link : message.ProLink.text;
                    row["ProHasLink"] = true;
                   row["ProLink"] = message.ProLink.link;
                }
                if (!string.IsNullOrEmpty(message.CustLink.link))
                {
                    row["CustTextLink"] = string.IsNullOrEmpty(message.CustLink.text) ? message.CustLink.link : message.CustLink.text;
                    row["CustHasLink"] = true;
                    row["CustLink"] = message.CustLink.link;
                }
            }
            row["ProScript"] = script;
            row["CustScript"] = script;

            row["imgPro"] = message.ProRecipientStatus == "read" ? GetImgSeen : message.ProRecipientStatus == "delivered" ? GetImgDelivered : GetImgSent;
            row["imgCust"] = message.CustomerRecipientStatus == "read" ? GetImgSeen : message.CustomerRecipientStatus == "delivered" ? GetImgDelivered : GetImgSent;

            row["ifProSms"] = message.IncludeSms && !string.IsNullOrEmpty(message.ProMessage);
            row["ifCustSms"] = message.IncludeSms && !string.IsNullOrEmpty(message.CustMessage);
            dt.Rows.Add(row);
        }
        _GridView.DataSource = dt;
        _GridView.DataBind();

    }
}