﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="SoldCallReport.aspx.cs" Inherits="Publisher_SoldCallReport" Title="Untitled Page" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<script  type="text/javascript" src="../Calender/Calender.js"></script>
<link href="style.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
<script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>

<script type="text/javascript"  src="../CallService.js"></script>
<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
<!--
<script src="../jquery.min.js" type="text/javascript"></script> 
<script src="../jquery-ui.min.js" type="text/javascript"></script>
-->
<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="../jquery/jquery.colorbox1.3.19.js" type="text/javascript" ></script>	   
<link media="screen" rel="stylesheet" href="../jquery/colorbox.css" />


<script type="text/javascript" >
    var message;
    window.onload = appl_init;
    function appl_init() {
        
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
        
        GrupByChange(document.getElementById("<%# ddl_group.ClientID %>"));
        message = '<%# IfToClose %>';
        
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }  
    
    function _OnError()
    {
    }
    function GrupByChange(elem)
    {
        
        var _val = elem.options[elem.selectedIndex].value;
        
        var div_graph = document.getElementById("div_graph");
        var ToDisabled = (_val == "Without");
        if(ToDisabled)
        {
            var _cb = document.getElementById("<%# cb_charts.ClientID %>");
            var _cbs = _cb.getElementsByTagName("input");
            for(var i=0; i<_cbs.length; i++)
            {
                _cbs[i].checked = false;
            }
        }
        
        
        toggleDisabled(div_graph, ToDisabled);
        div_graph.className = (ToDisabled) ? "Group_By div_disable" : "Group_By";
        
    }
    function OpenRecord(_id)
   {
        var url = "../RecordService.asmx/GetPath";
        var params = "_id="+_id;
       CallWebService(params, url, OnCompleteRecord, _OnError);
   }
   function OnCompleteRecord(arg)
   {
        if(arg.length==0)
        {
            alert(document.getElementById("<%# hf_errorRecord.ClientID %>").value);
            return;
        }
 
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        if(is_chrome)
        {
            oWin = window.open('<%# GetAudioChrome %>?audio='+ arg, "recordWin", "resizable=0,width=500,height=330");     
            setTimeout("CheckPopupBlocker()", 2000);
            
        }
        else
            window.location.href=arg;
   
   }
   function CheckPopupBlocker()
   {
         if(_hasPopupBlocker(oWin))
                alert(document.getElementById('<%#Hidden_AllowPopUp.ClientID  %>').value);
   }
   function _hasPopupBlocker(poppedWindow)
   {   
        if (poppedWindow==null || typeof poppedWindow =="undefined" || !poppedWindow || poppedWindow.closed || poppedWindow.innerHeight==0) 
            return true;
        return false;

   }

   function LoadChart(fileXML, _chart, div_id, parentId) {

       var chart = new FusionCharts(_chart, div_id, '650', '400', '0', '0');
       chart.addParam("WMode", "Transparent");
       chart.setDataURL(fileXML);
       chart.render(parentId);
   }
    //ICallbackEventHandler
    function SetCharts() {
        CallServer("", "ee");
    }
    function ReciveServerData(retValue) {

        var Charts_Area = document.getElementById("<%# Charts_Area.ClientID %>");
        var div_container = document.getElementById("div_container");
        var div_chart_load = document.getElementById("div_chart_load");
        RemoveAllChildNodes(div_container);
        div_container.appendChild(div_chart_load);
        RemoveAllChildNodes(Charts_Area);
        Charts_Area.innerHTML = "";
        var values = retValue.split(';');
        for (var i = 0; i < values.length; i++) {
            var chart1 = document.createElement('div');
            var _id = "div_chart" + i;
            chart1.setAttribute("id", _id);
            var _chart = document.createElement('div');
            var _chart_id = "_chart" + i;
            chart1.appendChild(_chart);
            Charts_Area.appendChild(chart1);
            var _vals = values[i].split(',');
            LoadChart(_vals[0], _vals[1], _chart_id, _id);
        }
      
        

    }
    function SetOnLoadChart() {

        var Charts_Area = document.getElementById("<%# Charts_Area.ClientID %>");
        var div_chart_load = document.getElementById("div_chart_load");
        RemoveAllChildNodes(Charts_Area);
        Charts_Area.appendChild(div_chart_load);
    }
    function openAdvIframe(_id, name) {
        var url = 'framepublisher.aspx?UserId=' + _id + "&UserName=" + name;
        $(document).ready(function () { $.colorbox({ href: url, width: '95%', height: '90%', iframe: true }); });
    }
    
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">          
</cc1:ToolkitScriptManager>       
 <uc1:ToolboxReport runat="server" ID="ToolboxReport1" /> 
 
<div class="page-content minisite-content2">
	<div id="form-analytics">
	    <div class="main-inputs">	
          
           <div id="div_Heading" class="form-field" runat="server">  
                <label for="form-subsegment" class="label" runat="server" id="lbl_heading">Heading</label>
               <asp:DropDownList ID="ddl_Heading" CssClass="form-select" runat="server">
               </asp:DropDownList>
			</div>			
			<div class="form-field">
               <asp:Label ID="lbl_CallsOrigin" runat="server" CssClass="label" Text="Type"></asp:Label>
                <asp:DropDownList ID="ddl_CallsOrigin"  CssClass="form-select" runat="server">
                </asp:DropDownList>
            </div>

            
            <div class="clear"></div>
            <div class="form-field" runat="server" id="div_affiliate">
                <asp:Label ID="lbl_Affiliate" CssClass="label" runat="server" Text="Affiliates"></asp:Label>                                    
                <asp:DropDownList ID="ddl_Affiliate" CssClass="form-select" runat="server"></asp:DropDownList>			
			</div>
            
            <div class="form-field form-group">
			    <span class="label">
                    <asp:Label ID="lbl_GroupBy" runat="server" Text="Group by"></asp:Label>:
                </span>
                <asp:UpdatePanel ID="_UpdatePanelGrupBy" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddl_group" CssClass="form-select" runat="server"
                         onchange="javascript:GrupByChange(this);">
                            
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            
            
           
	    </div>
        <div id="div_graph" class="Group_By">
                <asp:Label ID="lbl_graph" CssClass="label" runat="server" Text="Graph"></asp:Label>
                <div class="checkboxgroup">
                    <asp:CheckBoxList ID="cb_charts" runat="server" 
                    RepeatDirection="Horizontal" CssClass="checkbox">              
                        
                    </asp:CheckBoxList>
                </div>	       
		    </div>
	    <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>	    
		 <div class="clear"></div>
		
	</div>
    <asp:UpdatePanel ID="_UpdatePanelCharts" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div id="Charts_Area" runat="server">
    
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>

    
	 <div class="clear"></div>
	<div id="Table_Report" class="table" runat="server">
	
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional" EnablePageMethods="true" EnablePartialRendering="true">
        <ContentTemplate>    
            
            <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
	        <asp:GridView ID="_GridView" runat="server" CssClass="data-table" AutoGenerateColumns="false">
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />                
            <FooterStyle CssClass="footer"  />
            <Columns>
            
            <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label1" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text="<%# Bind('Date') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label3" runat="server" Text="<%# lbl_RequestId.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text="<%# Bind('CaseNumber') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label5" runat="server" Text="<%# lblHeading.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label6" runat="server" Text="<%# Bind('Expertise') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label7" runat="server" Text="<%# lbl_Advertiser.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <a id="a_Advertiser" runat="server" href="<%# Bind('OnClientClick') %>">
                    <asp:Label ID="Label8" runat="server" Text="<%# Bind('Supplier') %>"></asp:Label>
                </a>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label9" runat="server" Text="<%# lbl_Price.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label10" runat="server" Text="<%# Bind('Price') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label11" runat="server" Text="<%# lbl_PreDefinePrice.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label12" runat="server" Text="<%# Bind('DefaultPrice') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label13" runat="server" Text="<%# lbl_Recording.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <a id="a_recording" runat="server" href="<%# Bind('Recording') %>" visible="<%# Bind('HasRecord') %>">
                    <asp:Image ID="img_record" runat="server" ImageUrl="<%# Bind('ImageUrl') %>" />
                </a>
            </ItemTemplate>
            </asp:TemplateField>
                
             <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label14" runat="server" Text="<%# lbl_SupplierNumber.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label15" runat="server" Text="<%# Bind('SupplierNumber') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label16" runat="server" Text="<%# lbl_Duration.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label17" runat="server" Text="<%# Bind('Duration') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
            <HeaderTemplate>
                <asp:Label ID="Label18" runat="server" Text="<%# lbl_Balance.Text %>"></asp:Label>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label19" runat="server" Text="<%# Bind('Balance') %>"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            </Columns>
            </asp:GridView>
            
            <asp:GridView ID="_GridViewGroupBy" runat="server" CssClass="data-table" >
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />                
            <FooterStyle CssClass="footer"  />
             
            </asp:GridView>
            
            <uc1:TablePaging ID="TablePaging1" runat="server" />
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_RequestId" runat="server" Text="ID" Visible="false"></asp:Label>
<asp:Label ID="lblHeading" runat="server" Text="Heading" Visible="false"></asp:Label>
<asp:Label ID="lbl_Advertiser" runat="server" Text="Advertiser" Visible="false"></asp:Label>
<asp:Label ID="lbl_Price" runat="server" Text="Price" Visible="false"></asp:Label>
<asp:Label ID="lbl_PreDefinePrice" runat="server" Text="Pre define price" Visible="false"></asp:Label>
<asp:Label ID="lbl_Recording" runat="server" Text="Recording" Visible="false"></asp:Label>
<asp:Label ID="lbl_SupplierNumber" runat="server" Text="Supplier number" Visible="false"></asp:Label>
<asp:Label ID="lbl_Duration" runat="server" Text="Duration" Visible="false"></asp:Label>
<asp:Label ID="lbl_Balance" runat="server" Text="Balance" Visible="false"></asp:Label>


<asp:Label ID="lbl_Revenue" runat="server" Text="Revenue" Visible="false"></asp:Label>
<asp:Label ID="lbl_Sum" runat="server" Text="Sum" Visible="false"></asp:Label>

<asp:HiddenField ID="hf_errorRecord" runat="server" Value="There are errors with the record" />
<asp:HiddenField ID="Hidden_AllowPopUp" runat="server" Value="Allow pop-up windows" />
<asp:Label ID="lbl_NoResult" runat="server" Visible="false" Text="There are no results. Please try again"></asp:Label>

<asp:Label ID="lbl_SoldCallReport" runat="server" Visible="false" Text="Sold calls report"></asp:Label>

<asp:Label ID="lbl_Total" runat="server" Visible="false" Text="Total"></asp:Label>
<asp:Label ID="lbl_all" runat="server" Text="All" Visible="false"></asp:Label>


<asp:Label ID="lbl_IfToExite" runat="server" Text="Are you sure you want to exit? Unsaved data will be lost!" Visible="false"></asp:Label>


<div id="div_container" style="display:none;">
<div id="div_chart_load"  class="divLoaderChart">
    <table   >
        <tr>
          <td><img src="../UpdateProgress/ajax-loader.gif" alt="Loading..." style="background-color: #FFFFFF;" /></td>
        </tr>
    </table>
</div>
</div>
</asp:Content>


