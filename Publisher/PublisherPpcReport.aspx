﻿<%@ Page Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="PublisherPpcReport.aspx.cs" Inherits="Publisher_PublisherPpcReport" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link href="../Calender/CalenderDiv.css" rel="stylesheet" type="text/css" />
<link href="../UpdateProgress/updateProgress.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
    <link href="../FromToDate.css" type="text/css" rel="stylesheet" />
<script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>
<script  type="text/javascript" src="../Calender/_Calender.js"></script>

<script type="text/javascript" >
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);        
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler()
    {        
        showDiv();
    }
    function EndHandler()
    {
        hideDiv();
    }   
    function LoadChart(fileXML, _chart)
    {     
        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render("div_chart");
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>
<uc1:ToolboxReport runat="server" ID="ToolboxReport1" />  

<div class="page-content minisite-content2">
    <div id="form-analytics">
        <div class="form-fields clearfix">
            <div class="form-fieldlargeppc">       
                <asp:Label ID="lbl_Heading" runat="server" class="label" Text="Heading"></asp:Label> 
                <asp:DropDownList ID="ddl_Heading" CssClass="form-selectlarge" runat="server">
                </asp:DropDownList>
            </div>
            <div class="callreport"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>
          </div>
        
          
    </div>
     <div class="clear"></div>     
     <div class="table2"> 

        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
           <div class="results3"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
            <div class="clear"></div>
             <div id="div_chart"></div>
            
             <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false"
                CssClass="data-table" AllowPaging="True" 
                onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
            <RowStyle CssClass="odd" />
            <AlternatingRowStyle CssClass="even" />        
            <FooterStyle CssClass="footer"  />
            <PagerStyle CssClass="pager" />
            <Columns>
            
                <asp:TemplateField SortExpression="Date">              
                <HeaderTemplate>
                    <asp:Label ID="Label01" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>                                     
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text="<%# Bind('Date') %>"></asp:Label>       
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField SortExpression="PreDefinedPrice">              
                <HeaderTemplate>
                    <asp:Label ID="Label02" runat="server" Text="<%# lbl_PreDefinePPC.Text %>"></asp:Label>                                     
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text="<%# Bind('PreDefinedPrice') %>"></asp:Label>       
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField SortExpression="ActualPrice">              
                <HeaderTemplate>
                    <asp:Label ID="Label03" runat="server" Text="<%# lbl_ActualPPC.Text %>"></asp:Label>                                     
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text="<%# Bind('ActualPrice') %>"></asp:Label>       
                </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
  
</div>
</div>
<asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_PreDefinePPC" runat="server" Text="Pre define PPC" Visible="false"></asp:Label>
<asp:Label ID="lbl_ActualPPC" runat="server" Text="Actual PPC" Visible="false"></asp:Label>
<asp:Label ID="lbl_PricePerCall" runat="server" Text="Price per call" Visible="false"></asp:Label>

<asp:Label ID="lbl_PPCReport" runat="server" Text="PPC Report" Visible="false"></asp:Label>
<asp:Label ID="lbl_CurrentDot" runat="server" Text="Current dot" Visible="false"></asp:Label>

<asp:Label ID="lbl_ErrorChart" runat="server" Text="There was an error, Can not display the chart" Visible="false"></asp:Label>

</asp:Content>

