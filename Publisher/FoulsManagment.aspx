﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="FoulsManagment.aspx.cs" Inherits="Publisher_FoulsManagment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/Toolbox/Toolbox.ascx" tagname="Toolbox" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"/>
   <style type="text/css">
       td.td_span_block span, div.div_foul_section span
       {
           display:block;
       }
       div.div_foul_section
       {
           margin-top:5px;
           margin-bottom: 5px;
       }
       textarea {
            resize: none;
        }
       .foul_details
       {
            font-size: 14px;
            padding: 15px;
            font-weight: bold;
            margin: 10px auto;
       }
   </style>
    <script src="//code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        var _dialogFoul;
        var _accountFoulId;
        $(document).ready(function () {

            _dialogFoul = $("#div_foul").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                title: "Foul settings",
                buttons: {
                    CANCEL: function () {
                        _dialogFoul.dialog('close');
                    },
                    OK: function () {
                        SetFoul();
                        //       OnRejectedClick();
                    }
                },
                draggable: false,
                beforeClose: function (event, ui) {

                },
            });//.prev(".ui-dialog-titlebar").css("background","green");
            var checkboxes = document.querySelector(".data-table").querySelectorAll('input[type="checkbox"]');
            checkboxes.forEach(function (cb) {
                cb.addEventListener("change", function () {
                    if (!cb.checked)
                        return;
                    for (var i = 0; i < checkboxes.length; i++) {
                        if(cb.isEqualNode(checkboxes[i]))
                            continue;
                        checkboxes[i].checked = false;
                    }
                });
            });
        });
        
        function CloseMeError() {
            alert("There was an error to load the details.");
            window.close();
        }
        function _CreateExcel() {
            var path = '<%# GetCreateExcelUrl %>';
            var _data = '{"SessionDatatableName":"<%# SessionTableName %>","ReportName":"<%# EXCEL_NAME %>"}';
            $.ajax({
                url: path,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    
                    if (data.d.length == 0) {
                        return;
                    }
                    var _iframe = document.createElement('iframe');
                    _iframe.style.display = 'none';
                    _iframe.src = '<%# DownloadExcelUrl %>' + '?fn=' + data.d;
                    document.getElementsByTagName('body')[0].appendChild(_iframe);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }
        function openFoulPopupAdd() {
            document.querySelector("#<%# hf_accountFoulId.ClientID %>").value = '';
            document.querySelector("#<%# txt_description.ClientID %>").value = '';
            document.querySelector("#<%# txt_internalNote.ClientID %>").value = '';
            document.querySelector('#<%# ddl_FoulType.ClientID %>').selectedIndex = 0;
            _dialogFoul.dialog('open');
        }
        function openFoulPopupUpdate() {
            var checkboxes = document.querySelector(".data-table").querySelectorAll('input[type="checkbox"]');
            for (var i = 0; i < checkboxes.length; i++) {
                if (!checkboxes[i].checked)
                    continue;
                var tr = GetParent(checkboxes[i], 'tr');
                var accountFoulId = tr.querySelector('input[type="hidden"]').value;
                var description = tr.querySelector('span._foul_description').innerHTML;
                var projectId = tr.querySelector('span._project_id').innerHTML;
                var note = tr.querySelector('span._foul_note').innerHTML;
                var type = tr.querySelector('span._foul_type').innerHTML;
                var ddl_projects = document.querySelector('#<%# ddl_projects.ClientID %>');
                var ddl_FoulType = document.querySelector('#<%# ddl_FoulType.ClientID %>');
                var txt_description = document.querySelector('#<%# txt_description.ClientID %>');
                var txt_internalNote = document.querySelector('#<%# txt_internalNote.ClientID %>');
                var hf_accountFoulId = document.querySelector('#<%# hf_accountFoulId.ClientID %>');
                SetDropDownList(ddl_projects, projectId);
                SetDropDownList(ddl_FoulType, type);
                txt_description.value = description;
                txt_internalNote.value = note;
                hf_accountFoulId.value = accountFoulId;
                _dialogFoul.dialog('open');
                
                break;
            }           
        }
        function GetParent(elem, node_name) {
            while (elem) {
                if (elem.nodeName.toLowerCase() == node_name)
                    return elem;
                elem = elem.parentNode;
            }
            return null;
        }
        function SetDropDownList(ddl, text) {
            for (var j = 0; j < ddl.length; j++) {
                if (ddl.options[j].text == text) {
                    ddl.options[j].selected = true;
                    break;
                }
            }
        }
        function SetFoul() {
            var foulService = '<%# SetFoulService %>';
            var incidentAccountId = document.querySelector('#<%# ddl_projects.ClientID %>').value;
            var foulType = document.querySelector('#<%# ddl_FoulType.ClientID %>').value;
            var description = document.querySelector('#<%# txt_description.ClientID %>').value;
            var internalNote = document.querySelector('#<%# txt_internalNote.ClientID %>').value;
            var accountFoulId = document.querySelector('#<%# hf_accountFoulId.ClientID %>').value;
            
            var _data = '{"accountFoulId":' + (accountFoulId.length == 0 ? 'null' : '"' + accountFoulId + '"') + ',"incidentAccountId":"' + incidentAccountId + '","type":' + foulType +
                ',"description":"' + description + '","internalNote":"' + internalNote + '"}';
            _dialogFoul.dialog('close');
            showDiv();
            $.ajax({
                url: foulService,
                data: _data,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    var result = eval('(' + data.d + ')');
                    if (!result.IsSucceed) {
                        alert('Failed');
                    }
                    else {
                        
                        window.location.href = window.location.href;
                    }
            
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;

                }

            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
     <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <uc1:Toolbox ID="Toolbox1" runat="server" />
    <div class="page-content minisite-content5">
        <div class="foul_details" >
            <asp:Label ID="Label13" runat="server" Text="Pro:"></asp:Label>
            <asp:Label ID="lbl_supplierName" runat="server"></asp:Label>
            <br />
            <asp:Label ID="Label15" runat="server" Text="Phone number:"></asp:Label>
            <asp:Label ID="lbl_supplierPhoneNumber" runat="server"></asp:Label>
        </div>
			
	    <div id="form-analyticsseg">


            <asp:UpdatePanel ID="_updatePanelTable" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
			
                <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" ShowFooter="false" 
                CssClass="data-table" Width="650">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                 <PagerStyle HorizontalAlign="Center" CssClass="pager" />
                <FooterStyle CssClass="footer"  />


                <Columns>
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label01" runat="server" Text="Select line"></asp:Label>
                    <br />
                    <% /* 
                    <asp:CheckBox ID="cb_all" runat="server" >
                    </asp:CheckBox>  
                    */ %>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cb_choose" runat="server" />
                    
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-CssClass="td_span_block">              
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="In relate of project"></asp:Label> 
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Date") %>'></asp:Label> 
                    <asp:Label ID="Label4" runat="server" CssClass="_project_id" Text='<%# Bind("ProjectId") %>'></asp:Label>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("CustomerName") %>' ></asp:Label>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                    <asp:HiddenField ID="hf_accountFoulId" runat="server" Value='<%# Bind("AccountFoulId") %>' />
                </ItemTemplate>
                
                </asp:TemplateField>
                
                <asp:TemplateField>              
                <HeaderTemplate>
                    <asp:Label ID="Label101" runat="server"  Text="Foul type"></asp:Label> 
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" CssClass="_foul_type" Text='<%# Bind("FoulType") %>'></asp:Label>
                </ItemTemplate>
                
                </asp:TemplateField>

                <asp:TemplateField>              
                <HeaderTemplate>
                    <asp:Label ID="Label131" runat="server" Text="Foul from"></asp:Label> 
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="LabelFrom" runat="server" Text='<%# Bind("From") %>'></asp:Label>
                </ItemTemplate>
                
                </asp:TemplateField>
                

                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label3" runat="server" Text="Description (will be present to the pro)"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDescription" runat="server" CssClass="_foul_description" Text='<%# Bind("Description") %>'></asp:Label>
                </ItemTemplate>
               
                </asp:TemplateField>
                
                <asp:TemplateField >              
                <HeaderTemplate>
                    <asp:Label ID="Label8" runat="server" Text="Internal note"></asp:Label>                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblInternalNote" runat="server" CssClass="_foul_note" Text='<%# Bind("InternalNote") %>'></asp:Label>
                </ItemTemplate>
                
                </asp:TemplateField>
                
                  
                
                </Columns>
             </asp:GridView>
             </ContentTemplate>
             </asp:UpdatePanel>

         </div>
       
 </div>  
<div id="div_foul" title="Foul" class="div_foul">
    <div class="div_foul_section">
        <asp:Label ID="Label9" runat="server" Text="Project"></asp:Label>
        <asp:DropDownList ID="ddl_projects" runat="server"></asp:DropDownList>
    </div>
   <div class="div_foul_section">
        <asp:Label ID="Label10" runat="server" Text="Foul type"></asp:Label>
        <asp:DropDownList ID="ddl_FoulType" runat="server"></asp:DropDownList>
    </div>
   <div class="div_foul_section">
        <asp:Label ID="Label11" runat="server" Text="Description"></asp:Label>
        <asp:TextBox ID="txt_description" runat="server" Rows="5" MaxLength="256" TextMode="MultiLine" Width="260" ></asp:TextBox>
    </div>
    <div class="div_foul_section">
        <asp:Label ID="Label12" runat="server" Text="Internal note"></asp:Label>
        <asp:TextBox ID="txt_internalNote" runat="server" Rows="5" MaxLength="1000" TextMode="MultiLine" Width="260"></asp:TextBox>
    </div>
    <asp:HiddenField ID="hf_accountFoulId" runat="server" />
</div>
</asp:Content>

