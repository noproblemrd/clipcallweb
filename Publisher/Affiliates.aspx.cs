﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;

public partial class Publisher_Affiliates : PageSetting
{
    
    const string ExcelName = "Affiliates";
    protected override void OnLoadComplete(EventArgs e)
    {
        if (CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(btn_Set);
        if (!IsPostBack)
        {
            SetToolBox();
            DataBindTopage();            
            Loadorigins();
            LoadAffiliateUsers();
            __Search();
        }
        SetToolBoxEvents();
        Header.DataBind();
    }

    private void DataBindTopage()
    {
        popUpadd.DataBind();
        RegularExpressionValidator_PhoneNumber.ValidationExpression = siteSetting.GetMobileAndRegularPhoneExpression();
    }

    private void LoadAffiliateUsers()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfAffiliateUserData result = null;
        try
        {
            result = _site.GetAllAffiliateUsers();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        dataV = GetDataTable.GetDataTableFromList(result.Value);

        /*
        result.Value[0].AccountId;
        result.Value[0].AccountNumber;
        result.Value[0].ContactName;
        result.Value[0].Email;
        result.Value[0].Name;
        result.Value[0].OriginId;
        result.Value[0].OriginName;
        result.Value[0].Password;
        result.Value[0].PhoneNumber;
         * */
    }

    private void Loadorigins()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetAllOrigins(false);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            ListItem li = new ListItem(gsp.Name, gsp.Id.ToString());
            ddl_Origin.Items.Add(li);
        }
    }
    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
   //     Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
        Toolbox1.EditExec += new EventHandler(Toolbox1_EditExec);
    }

    void Toolbox1_EditExec(object sender, EventArgs e)
    {
        bool IsSelect = false;
        int indx = -1;
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
            {
                if (IsSelect)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ChooseOne", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.ChooseOne) + "');", true);
                    return;
                }
                IsSelect = true;
                indx = row.RowIndex;
            }
        }
        if (indx > -1)
        {
            
            GridViewRow row = _GridView.Rows[indx];
            hf_guid.Value = ((Label)row.FindControl("lbl_UserId")).Text;
            txt_ID.Text = ((Label)row.FindControl("lbl__AccountNumber")).Text;
            txt_Name.Text = ((Label)row.FindControl("lbl__Name")).Text;
            txt_ContactPerson.Text = ((Label)row.FindControl("lbl__ContactName")).Text;
            txt_Email.Text = ((Label)row.FindControl("lbl__Email")).Text;
            txt_Password.Text = ((Label)row.FindControl("lbl__Password")).Text;
            txt_PhoneNumber.Text = ((Label)row.FindControl("lbl__PhoneNumber")).Text;
            string OriginId = ((Label)row.FindControl("lbl_OriginId")).Text;
            
            for (int i = 0; i < ddl_Origin.Items.Count; i++)
            {
                if (ddl_Origin.Items[i].Value == OriginId)
                {
                    ddl_Origin.SelectedIndex = i;
                    break;
                }
            }
        }
        popUpadd.Attributes["class"] = "popModal_del";
        lbl_PopupTitle.Text = lbl_TitelEdit.Text;
        _mpe.Show();
    }

    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        List<Guid> list = new List<Guid>();
        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            if (cb.Checked)
            {
                Guid _id = new Guid(((Label)row.FindControl("lbl_UserId")).Text);
                list.Add(_id);
            }
        }
        if (list.Count == 0)
            return;
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.DeletePublishersRequest _request = new WebReferenceSite.DeletePublishersRequest();
        _request.PublisherIds = list.ToArray();
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.DeletePublishers(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        Toolbox1.GetTextBoxSearch.Value = string.Empty;
        LoadAffiliateUsers();
        __Search();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataViewV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.NoRecordDisplay) + "');", true);
            return;
        }
        //  PrintHelper.PrintWebControl(_GridView, dataViewV);
        Session["data_print"] = dataViewV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {
        if (dataViewV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(Toolbox1.NoRecordExport) + "');", true);
            return;
        }
        ToExcel to_excel = new ToExcel(this, ExcelName);
        if (!to_excel.ExecExcel(dataViewV, _GridView))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        __Search();
    }
    private void __Search()
    {
        string name = Toolbox1.GetTxt;
        HtmlInputText _tb = Toolbox1.GetTextBoxSearch;
        if (name == Toolbox1.GetWaterTextSearch &&
                    _tb.Attributes["class"].Contains("watermark"))
            name = string.Empty;
        if (string.IsNullOrEmpty(name))
            dataViewV = dataV;
        else
        {
            name = name.ToLower();
            DataTable data = dataV;
            data.TableName = "Search";

            EnumerableRowCollection<DataRow> query = from Search in data.AsEnumerable()
                                                     where ((Search.Field<string>("Name").ToLower().Contains(name))
                                                        || (Search.Field<string>("AccountNumber").ToLower().Contains(name)))
                                                     orderby Search.Field<string>("Name")
                                                     select Search;

            dataViewV = (query.Count() == 0) ? null : query.CopyToDataTable();
        }
        _GridView.PageIndex = 0;
        SetGridView();
    }
    protected void btn_Set_Click(object sender, EventArgs e)
    {
        string _id = hf_guid.Value;
        Guid _guid = Guid.Empty;
        
        WebReferenceSupplier.UpsertAdvertiserRequest _request = new WebReferenceSupplier.UpsertAdvertiserRequest();
        if (!string.IsNullOrEmpty(_id) && Utilities.IsGUID(_id))
        {
            _guid = new Guid(_id);
            _request.Password = txt_Password.Text;
        }
            
        _request.Company = txt_Name.Text;
        _request.ContactPhone = txt_PhoneNumber.Text;
        _request.Email = txt_Email.Text;
        _request.SupplierId = _guid;
        _request.FirstName = txt_ContactPerson.Text;
        _request.AffiliateOriginId = new Guid(ddl_Origin.SelectedValue);
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);

        WebReferenceSupplier.ResultOfUpsertAdvertiserResponse result = null;
        try
        {
            result = supplier.UpsertAdvertiser(_request);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);            
            return;
        }

        if (result.Type == WebReferenceSupplier.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);            
            return;
        }
        if (result.Value.Status != WebReferenceSupplier.UpsertAdvertiserStatus.Success)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "UpsertAdvertiserStatusFailed", "alert('" + 
                HttpUtility.JavaScriptStringEncode(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "UpsertAdvertiserStatus", result.Value.Status.ToString())) + "');", true);
            _updatePanelTable.Update();
            return;
        }
   //     result.Value.Status;
    //    WebReferenceSupplier.UpsertAdvertiserStatus.
        popUpadd.Attributes.Add("class", "popModal_del popModal_del_hide");
        LoadAffiliateUsers();
        __Search();
        _mpe.Hide();
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        SetGridView();
    }
    void SetGridView()
    {
        StringBuilder sb = new StringBuilder();
        DataView data = dataViewV.AsDataView();
        //    _GridView.PageIndex = 0;
        _GridView.DataSource = data;
        _GridView.DataBind();

        if (data.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noreasultSearch", "alert('" + HttpUtility.JavaScriptStringEncode(Master.GetNoResultsMessage) + "');", true);
            CBsV = string.Empty;
            _updatePanelTable.Update();
            return;
        }
        CheckBox cb_all = ((CheckBox)_GridView.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridView.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
        }
        
        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        _updatePanelTable.Update();
    }
    private void SetToolBox()
    {
        Toolbox1.RemoveAttach();
        string script = "openPopup(); return false;";
        Toolbox1.SetClientScriptToAdd(script);
  //      Toolbox1.SetConfirmDelete = lbl_Delete.Text;
    }
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    private DataTable dataViewV
    {
        get { return (Session["dataView"] == null) ? new DataTable() : (DataTable)Session["dataView"]; }
        set { Session["dataView"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
    protected string GetTitleNew
    {
        get { return HttpUtility.HtmlEncode(lbl_TitelNew.Text); }
    }
}