﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Publisher/MasterPagePublisher.master" AutoEventWireup="true" CodeFile="AarDashboard.aspx.cs" Inherits="Publisher_AarDashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="../DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js" type="text/javascript"></script>
    <script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>    
    <script  type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>    
    <link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />    
    <link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />
    <script type="text/javascript">
        var DaysInterval = new Number();
        var ChoosenIntervalOk = new Boolean();
        window.onload = appl_init;
        function appl_init() {
            var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
            pgRegMgr.add_beginRequest(BeginHandler);
            pgRegMgr.add_endRequest(EndHandler);
        }
        function BeginHandler() {
            showDiv();
        }
        function EndHandler() {
            hideDiv();
        }
        function CompareToPast(elm) {
    //        var _div = document.getElementById("div_compareDate");
    //        var _btn = document.getElementById("<%# btn_run.ClientID %>");
            if (elm.checked) {
                Set_Dates_Compars();
                $('#<%# btn_run.ClientID %>').removeAttr('onclick');
                $('#div_compareDate').show();
          //      _div.style.display = "block";
            }
            else {
                $('#<%# btn_run.ClientID %>').attr('onclick', "return CheckCurrentDates();");
                $('#div_compareDate').hide();
       //         _btn.setAttribute("onclick", "return CheckCurrentDates();");
      //          _div.style.display = "none";
            }
        }
        function Set_Dates_Compars() {
            //       var _div = document.getElementById("div_compareDate");
            var _btn = document.getElementById("<%# btn_run.ClientID %>");
            var _format = "<%# GetDateFormat() %>";
            var from_current = document.getElementById("<%# GetFromCurrentId %>");
            var to_current = document.getElementById("<%# GetToCurrentId %>");
            var from_past = document.getElementById("<%# GetFromPastId %>");
            var to_past = document.getElementById("<%# GetToPastId %>");
            var _start = GetDateFormat(from_current.value, _format);
            var _end = GetDateFormat(to_current.value, _format);
            var diff_day = days_between(_start, _end);

            var end_pass = new Date(_start); //.addDays(-1);
            end_pass.addDays(-1);
            var start_pass = new Date(end_pass);
            start_pass.addDays(diff_day * -1);
            from_past.value = GetDateString(new Date(start_pass), _format);
            to_past.value = GetDateString(new Date(end_pass), _format);
            _btn.setAttribute("onclick", "return CheckCurrentAndPastDates();");
        }
        function Set_Dates_Compars_by(_interval) {
            //       var _div = document.getElementById("div_compareDate");
            var _btn = document.getElementById("<%# btn_run.ClientID %>");
            var _format = "<%# GetDateFormat() %>";
            var from_current = document.getElementById("<%# GetFromCurrentId %>");
            var to_current = document.getElementById("<%# GetToCurrentId %>");
            var from_past = document.getElementById("<%# GetFromPastId %>");
            var to_past = document.getElementById("<%# GetToPastId %>");
            var _start = GetDateFormat(from_current.value, _format);
            var _end = GetDateFormat(to_current.value, _format);
            var diff_day = days_between(_start, _end);
            var end_pass = new Date(_start); //.addDays(-1);
            end_pass.addDays(-1);
            var start_pass = new Date(end_pass);
            //    alert(_interval);
            if (_interval == 29)
                start_pass.addMonth(-1);
            else if (_interval == 89)
                start_pass.addMonth(-3);
            else if (_interval == 364)
                start_pass.addFullYear(-1);
            else
                start_pass.addDays(diff_day * -1);
            from_past.value = GetDateString(new Date(start_pass), _format);
            to_past.value = GetDateString(new Date(end_pass), _format);
            _btn.setAttribute("onclick", "return CheckCurrentAndPastDates();");
        }
        function LoadChart(fileXML, _chart) {
            //      alert(fileXML+"----"+_chart);
            var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
            chart.addParam("WMode", "Transparent");
            chart.setDataURL(fileXML);
            chart.render("div_chart");
        }
        function CheckCurrentDates() {
            var elem = document.getElementById("<%# GetDivCurrentId %>");
            var result = Check_DateValidation(elem);
            if (!result)
                alert("<%# GetInvalidDate %>");
            return result;
        }
        function CheckCurrentAndPastDates() {
            if (!CheckCurrentDates())
                return false;
            var elem = document.getElementById("<%# GetDivPastId %>");
            var result = Check_DateValidation(elem);
            if (!result)
                alert("<%# GetInvalidDate %>");
            return result;
        }
        function SetCompareDate(elem_from, parentIdCurrent, mess, elem_maxId) {
            var _days = GetRangeDays(parentIdCurrent);
            var elm_parent = getParentByClassName(elem_from, "div_DatePicker");
            var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;
            var elem_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
            var elem_max = document.getElementById(elem_maxId);

            var _MaxValue = GetDateFormat(elem_max.value, _format);
            var _start = GetDateFormat(elem_from.value, _format);
            var _end = _start + ((24 * 60 * 60 * 1000) * _days); //GetDateFormat(elem_to.value, _format);
            _MaxValue = _MaxValue - (24 * 60 * 60 * 1000);
            //    alert("max= " +elem_max.id+" end= "+elem_from.id);
            //    alert("max= " +_MaxValue+" end= "+_end);
            if (_MaxValue < _end) {
                _end = _MaxValue;
                var start_date = new Date(_end);
                start_date.addDays(-1 * _days);
                //       alert(start_date);
                elem_from.value = GetDateString(start_date, _format);
                elem_to.value = GetDateString(new Date(_MaxValue), _format);
                alert("<%# Lbl_DatesChange %>");
            }
            else {
                elem_to.value = GetDateString(new Date(_end), _format);
            }

        }
        function GetIntervalDates() {
        /*
            var elem = document.getElementById("<# ddl_Interval.ClientID %>");
            var _interval = elem.options[elem.selectedIndex].value;

            if (_interval == "Days")
                return 0;
            if (_interval == "Weeks")
                return 6;
            if (_interval == "Months")
                return 29;
            if (_interval == "Quarters")
                return 89;
            if (_interval == "Years")
                return 364;
                */
            return 0;
        }
        function SetIntervalDatesValidation(elem) {
            /*
            //       var _current = document.getElementById("<%# GetDivCurrentId %>");
            DaysInterval = GetIntervalDates();//elem.options[elem.selectedIndex].value;
        
            var _days = GetRangeDays("<%# GetDivCurrentId %>");
            //     alert('interval= '+DaysInterval+" days= "+_days);
            if(DaysInterval > _days)
            {
            SetDefaultDates_ByDays("<%# GetDivCurrentId %>", DaysInterval);
            Set_Dates_Compars_by(DaysInterval);
            }
            */
            ChoosenIntervalOk = true;
            SetDdlInterval(elem);
            if (!ChoosenIntervalOk)
                alert(document.getElementById('<%# lbl_ChosenInterval.ClientID %>').value);
        }
        function SetDdlInterval(elem) {
            DaysInterval = GetIntervalDates(); //elem.options[elem.selectedIndex].value;
            var _index = elem.selectedIndex;
            var Interval_name = elem.options[_index].value;
            var _days = GetRangeDays("<%# GetDivCurrentId %>");


            if (DaysInterval > _days) {
                _index--;
                ChoosenIntervalOk = false;
                elem.selectedIndex = _index;
                SetDdlInterval(elem);
            }
        }
        function OnBlurByDays(elm) {

            var elm_to;
            var elm_from;
            var elm_parent = getParentByClassName(elm, "div_DatePicker");
            var _format = (getElementsWithMultiClassesByClass("hf_format", elm_parent, "input")[0]).value;

            if (!validate_Date(elm.value, _format)) {
                try { alert(WrongFormatDate); } catch (ex) { }
                elm.focus();
                elm.select();
                return -1;
            }
            if (elm.className.indexOf("_to") == -1) {
                elm_to = getElementsWithMultiClassesByClass("_to", elm_parent, "input")[0];
                elm_from = elm;
            }
            else {
                elm_from = getElementsWithMultiClassesByClass("_from", elm_parent, "input")[0];
                elm_to = elm;
            }
            DaysInterval = GetIntervalDates();
            var _end = GetDateFormat(elm_to.value, _format);
            var _start = GetDateFormat(elm_from.value, _format);
            //    alert("start= "+_start+" end= "+_end);
            //    alert((_start + (24 * 60 * 60 * 1000 * DaysInterval)));
            if ((_start + (24 * 60 * 60 * 1000 * DaysInterval)) > _end) {
                if (elm_from.id == elm.id) {
                    _end = new Date(_start);
                    if (DaysInterval == 29)
                        _end.addMonth(1);
                    else if (DaysInterval == 89)
                        _end.addMonth(3);
                    else if (DaysInterval == 364)
                        _end.addFullYear(1);
                    else {
                        _end = _start + (24 * 60 * 60 * 1000 * DaysInterval);
                        _end = new Date(_end);
                    }
                    elm_to.value = GetDateString(_end, _format);
                }
                else {

                    _start = new Date(_end);
                    if (DaysInterval == 29)
                        _start.addMonth(-1);
                    else if (DaysInterval == 89)
                        _start.addMonth(-3);
                    else if (DaysInterval == 364)
                        _start.addFullYear(-1);
                    else {
                        _start = _end - (24 * 60 * 60 * 1000 * DaysInterval);
                        _start = new Date(_start);
                    }
                    elm_from.value = GetDateString(_start, _format);
                }
                /*
                if(elm_from.id == elm.id)
                {        
                _end = _start + (24 * 60 * 60 * 1000 * DaysInterval);
                elm_to.value = GetDateString(new Date(_end), _format);
                
                }
                else
                {
                _start = _end - (24 * 60 * 60 * 1000 * DaysInterval);        
                elm_from.value = GetDateString(new Date(_start), _format);
                }
                */
                return -10;
            }
            return 1;

        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
</cc1:ToolkitScriptManager>

<div class="page-content minisite-content">
    <h2><asp:Label ID="lbl_Dashboard" runat="server" Text="Business KPI Dashboard"></asp:Label></h2>
 <div id="form-analytics" > 
    <div class="form-fieldDleftAar">
    
        <div class="dash"><uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" /></div>
        <div class="form-field">
            <asp:Label ID="lbl_expertise" CssClass="label" runat="server" Text="Heading"></asp:Label>
            <asp:DropDownList ID="ddl_Heading" CssClass="form-select" runat="server">
               </asp:DropDownList>
        </div>
        <div class="form-fieldDleft33Aar">
            <asp:CheckBox ID="cb_Compare" runat="server" onclick="CompareToPast(this);" Text="Compare to past" />
       
        </div>
         
 
  </div>
    <div class="form-fieldDrightAar">
    <div class="form-field">
        <asp:UpdatePanel ID="_UpdatePanel_Showme" runat="server" UpdateMode="Conditional">
        <ContentTemplate> 
            <asp:Label ID="lbl_ShowMe" runat="server" Cssclass="label" Text="Show me:"></asp:Label>        
            <asp:DropDownList ID="ddl_Showme" CssClass="form-select" runat="server" Width="300">
        </asp:DropDownList>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
   
  
         <div class="compare2">
           <div id="div_compareDate" style="display:none;">
           
         <div class="dash"><uc1:FromToDatePicker ID="FromToDatePicker_compare" runat="server" /></div>
         </div> 
        </div>   
    
     </div>
     
    <div class="clear"><!-- --></div>
      
    
        <div class="clear"><!-- --></div>
    <div class="dashsubmit">
        <asp:Button ID="btn_run" runat="server" Text="Create report"  CssClass="CreateReportSubmit2"
            onclick="btn_run_Click" OnClientClick="return CheckCurrentDates();" />
    
    </div>
 </div>   
    <div class="clear"><!-- --></div>
    <div>
        <asp:UpdatePanel ID="UpdatePanel_chart" runat="server" UpdateMode="Conditional">
        <ContentTemplate>                
            <div id="div_chart"></div>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <div class="data-tabledash">
        <asp:UpdatePanel ID="UpdatePanel_table" runat="server" UpdateMode="Conditional">

        <ContentTemplate> 
            <asp:Repeater ID="_Repeater" runat="server" 
                onitemdatabound="_Repeater_ItemDataBound" >
            
            <HeaderTemplate>
                <table class="data-tabledash">
                <tr class="_TH" runat="server" id="tr_head">
                    <th>
                        <asp:Label ID="Label1" runat="server" Text="<%# lbl_subject.Text %>"></asp:Label>
                    </th>
                    <th>
                        <asp:Label ID="Label3" runat="server" Text="<%# lbl_CurrentPeriod.Text %>"></asp:Label>
                    </th>
                    <th runat="server" id="td_PastPeriod" visible="<%# ShowPast %>">
                        <asp:Label ID="Label9" runat="server" Text="<%# lbl_PastPeriod.Text %>"></asp:Label>
                    </th>
                    
                    <th runat="server" id="td_Gap" visible="<%# ShowPast %>">
                        <asp:Label ID="Label11" runat="server" Text="<%# lbl_Gap.Text %>"></asp:Label>
                    </th>
                </tr>
            </HeaderTemplate>
            
            <ItemTemplate>                            
                <tr runat="server" id="tr_data">
                    <td>
                        <asp:LinkButton ID="lb_row" runat="server" Text="<%# Bind('RowText') %>"
                                CommandArgument="<%# Bind('Row') %>" OnClick="lb_row_click"></asp:LinkButton>                   
                        <asp:Label ID="lbl_row" runat="server" Text="<%# Bind('Row') %>" Visible="false"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="<%# Bind('Total') %>"></asp:Label>
                    </td>         
                    <td runat="server" id="td_PastPeriodIn" visible="<%# Bind('show_PastPeriod') %>">
                        <asp:Label ID="Label10" runat="server" Text="<%# Bind('PastPeriod') %>"></asp:Label>       
                    </td>
                    <td runat="server" id="td_GapIn" visible="<%# Bind('show_Gap') %>">
                        <asp:Label ID="Label12" runat="server" Text="<%# Bind('Gap') %>" ForeColor="<%# Bind('ColorGap') %>"></asp:Label>
                    </td>
                </tr>
            </ItemTemplate>
            
            <FooterTemplate>
                </table>
            </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>




<asp:Label ID="lbl_subject" runat="server" Text="Subject" Visible="false"></asp:Label>
<asp:Label ID="lbl_CurrentPeriod" runat="server" Text="Current period" Visible="false"></asp:Label>
<asp:Label ID="lbl_PastPeriod" runat="server" Text="Past period" Visible="false"></asp:Label>
<asp:Label ID="lbl_Gap" runat="server" Text="Gap" Visible="false"></asp:Label>

<asp:Label ID="lbl_DateError" runat="server" Text="Invalid date" Visible="false"></asp:Label>

<asp:Label ID="lbl_CurrentDot" runat="server" Text="Current dot" Visible="false"></asp:Label>

<asp:Label ID="lbl_Current" runat="server" Text="Current" Visible="false"></asp:Label>

<asp:Label ID="lbl_sooner" runat="server" Visible="false"></asp:Label>
<asp:Label ID="lbl_DatesChange" Text="The date you selected have been changed according to interval requirement" runat="server" Visible="false"></asp:Label>
<asp:HiddenField ID="lbl_ChosenInterval" runat="server"
 Value="The interval you have chosen is not adequate to the date range you selected. Please choose smaller value" />

<asp:Label ID="lbl_activity" Cssclass="label2" runat="server" Text="Activity dates from" Visible="false"></asp:Label>
<asp:Label ID="lbl_activityPast" Cssclass="label2" runat="server" Text="Past Activity dates from" Visible="false"></asp:Label>

</asp:Content>
