using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Linq;

public partial class Publisher_BadWords : PageSetting
{
    const string INSERT_WORD = "InsertWord";
    protected void Page_Load(object sender, EventArgs e)
    {
   //     ScriptManager1.RegisterAsyncPostBackControl(btn_search);
   //     ScriptManager1.RegisterAsyncPostBackControl(btn_Insert);
        if (!IsPostBack)
        {
            dataViewV = null;
            SetToolBox();
            GetBadWords();           
        }
        SetToolBoxEvents();
        Page.Header.DataBind();
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (IsPostBack && CBsV != string.Empty)
            SetCBsList(CBsV);
        base.OnLoadComplete(e);
    }
    void Toolbox1_DeleteExec(object sender, EventArgs e)
    {
        bool IfSuccess = true;
        foreach (GridViewRow row in _GridViewBadWords.Rows)
        {
            Guid BadWordId = new Guid(((Label)row.FindControl("lbl_BadWordId")).Text);
            CheckBox _cb = (CheckBox)row.FindControl("cb_choose");
            if (_cb.Checked)
                if (!Delete_Click(BadWordId))
                    IfSuccess = false;
        }
        if (IfSuccess)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "DeleteError", "alert('" + Toolbox1.DeleteError + "');", true);
        GetBadWords();
        _btn_Search();
    }

    void Toolbox1_PrintExec(object sender, EventArgs e)
    {
        if (dataViewV == null || dataViewV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordDisplay + "');", true);
            return;
        }
        
 //       PrintHelper.PrintWebControl(_GridViewBadWords, dataViewV);
        Session["data_print"] = dataViewV;
        Session["grid_print"] = _GridViewBadWords;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    void Toolbox1_ExcelExec(object sender, EventArgs e)
    {

        if (dataViewV == null || dataViewV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + Toolbox1.NoRecordExport + "');", true);
            return;
        }
        ToExcel to_excel = new ToExcel(this, "BadWords");
        if (!to_excel.ExecExcel(dataViewV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
         
    }
    void Toolbox1_SearchExec(object sender, EventArgs e)
    {
        _btn_Search();
    }
    void Toolbox1_AddExec(object sender, EventArgs e)
    {
        string badWord = Toolbox1.GetTxt.Trim();
        if (string.IsNullOrEmpty(badWord))
            return;
       
        InsertWord(badWord);
        
    }

    private void SetToolBoxEvents()
    {
        Toolbox1.SearchExec += new EventHandler(Toolbox1_SearchExec);
        Toolbox1.AddExec += new EventHandler(Toolbox1_AddExec);
        Toolbox1.ExcelExec += new EventHandler(Toolbox1_ExcelExec);
        Toolbox1.PrintExec += new EventHandler(Toolbox1_PrintExec);        
        Toolbox1.DeleteExec += new EventHandler(Toolbox1_DeleteExec);
    }
    private void SetToolBox()
    {
        Toolbox1.RemoveEdit();
        Toolbox1.RemoveAttach();
    //    Toolbox1.SearchValidation(@"^\\d+$", lbl_invalidPhoneNumber.Text);
        Toolbox1.AddValidationExists(string.Empty, lbl_WordIsNullOrEmpty.Text, string.Empty);
   
    }
    /*
    public string Get_click()
    {
        PostBackOptions pbo = new PostBackOptions(this);
        
        return Page.ClientScript.GetPostBackEventReference(this, INSERT_WORD);
    }
    */
    private void GetBadWords()
    {
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfGetAllBadWordsResponse result = null;
        try
        {
            result = _site.GetAllBadWords();
        }
        catch (Exception exc)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        DataTable data = GetDataTable.GetDataTableFromList(result.Value.BadWords);
        
        
        dataV = data;
    }
    protected void LoadBadWords()
    {
        StringBuilder sb = new StringBuilder();
        
        DataView data = dataViewV.AsDataView();
        _GridViewBadWords.DataSource = data;
        _GridViewBadWords.DataBind();
         if (data.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noreasultSearch", "alert('" + Master.GetNoResultsMessage + "');", true);
            CBsV = string.Empty;
             UpdatePanelSentences.Update();
             return;
        }

        CheckBox cb_all = ((CheckBox)_GridViewBadWords.HeaderRow.FindControl("cb_all"));
        cb_all.Attributes.Add("onclick", "SetAllCBs('" + cb_all.ClientID + "');");
        string _script = "CheckSetAllCBs('" + cb_all.ClientID + "');";

        foreach (GridViewRow row in _GridViewBadWords.Rows)
        {
            CheckBox cb = (CheckBox)row.FindControl("cb_choose");
            sb.Append(cb.ClientID + ";");
            cb.Attributes.Add("onclick", _script);
            /*
            string name = ((Label)row.FindControl("lblBadWord")).Text;
            string message = hf_confirmDelete.Value + " " + name;
            LinkButton lb = (LinkButton)row.FindControl("lb_Delete");
            lb.OnClientClick = "return ConfirmDelete('" + message + "');";
             * */
        }   
        
        string CBs = sb.ToString().Substring(0, sb.Length - 1);
        CBsV = CBs;
        
        UpdatePanelSentences.Update();
    }
    bool Delete_Click(Guid BadWordId)
    {       
       
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.DeleteBadWordRequest _request = new WebReferenceSite.DeleteBadWordRequest();
        _request.BadWordId = BadWordId;
        WebReferenceSite.Result result = null;
        try
        {
            result = _site.DeleteBadWord(_request);
        }
        catch (Exception exc)
        {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return false;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return false;
        }
        return true;
     //   ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
    //    GetBadWords();
   //     _btn_Search();
    }
    /*
    protected void btn_Insert_Click(object sender, EventArgs e)
    {
        InsertWord();
    }
     */
    void InsertWord(string badWord)
    {
        
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.CreateBadWordRequest _request = new WebReferenceSite.CreateBadWordRequest();
        _request.Word = badWord;
        WebReferenceSite.ResultOfCreateBadWordResponse result = null;
        try
        {
            result = _site.CreateBadWord(_request);
        }
        catch (Exception exc)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Value.Status == WebReferenceSite.CreateBadWordStatus.WordAlreadyExists)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "WordAlreadyExists", "alert('" + lbl_WordAlreadyExists.Text +
                "');", true);
        }
        else if (result.Value.Status == WebReferenceSite.CreateBadWordStatus.WordIsNullOrEmpty)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "WordIsNullOrEmpty", "alert('" + lbl_WordIsNullOrEmpty.Text +
                "');", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateSuccess", "top.UpdateSuccess();", true);
        Toolbox1.GetTextBoxSearch.Value = string.Empty;
        GetBadWords();
        _btn_Search();
        
    }
    /*
    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        _btn_Search();
    }

    #endregion
    
    */

    protected void _GridViewBadWords_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridViewBadWords.PageIndex = e.NewPageIndex;
        LoadBadWords();
    }
    private void _btn_Search()
    {
        string name = Toolbox1.GetTxt;
        HtmlInputText _tb = Toolbox1.GetTextBoxSearch;
        if (name == Toolbox1.GetWaterTextSearch &&
                    _tb.Attributes["class"].Contains("watermark"))
            name = string.Empty;
        DataTable data = dataV;
        data.TableName = "Search";
        
        EnumerableRowCollection<DataRow> query = from Search in data.AsEnumerable()
                                                 where Search.Field<string>("Word").ToLower().Contains(name)
                                                 orderby Search.Field<string>("Word")
                                                 select Search;

        dataViewV = (query.Count() == 0) ? null : query.CopyToDataTable();
       
        _GridViewBadWords.PageIndex = 0;
        LoadBadWords();
    }
    /*
    protected void btn_search_Click(object sender, EventArgs e)
    {
        _btn_Search();
    }
     * */
    void SetCBsList(string CBs)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetCBs", "SetCBs('" + CBs + "');", true);
    }
    private DataTable dataV
    {
        get { return (ViewState["data"] == null) ? new DataTable() : (DataTable)ViewState["data"]; }
        set { ViewState["data"] = value; }
    }
    private DataTable dataViewV
    {
        get { return (Session["dataView"] == null) ? new DataTable() : (DataTable)Session["dataView"]; }
        set { Session["dataView"] = value; }
    }
    string CBsV
    {
        get { return (ViewState["CBs"] == null) ? string.Empty : (string)ViewState["CBs"]; }
        set { ViewState["CBs"] = value; }
    }
}


