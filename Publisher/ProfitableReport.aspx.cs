﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using System.Text;
using System.IO;
using System.Data.SqlClient;

public partial class Publisher_ProfitableReport : PageSetting
{
    const int WEEK_FROM_TODAY = 6;
    const string Num_Format = "{0:#,0}";//"{0:#,0.##}";
    const string Num_Float_Format = "{0:#,0.####}";
    const string Num_Float_Percent_Format = "{0:#,0.####%}";
    bool _ShowCompare;
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(btn_run);
        SetToolboxEvents();
        if (!IsPostBack)
        {
            SetToolbox();
            Data_bind();
            setDateTime();
            LoadHeadings(true);
            LoadCountries();
            LoadAffiliates();
            LoadLastUpdate();
            LoadeProfileFilters();
            LoadSliderType();
        }
        else if (HeadingList == null || HeadingList.Count == 0)
            LoadHeadings(false);
        Header.DataBind();
    }

    private void LoadSliderType()
    {
        ddl_SliderType.Items.Clear();
        ddl_SliderType_Compare.Items.Clear();
        ListItem li1 = new ListItem("--ALL--", "-1");
        li1.Selected = true;
        ddl_SliderType.Items.Add(li1);
        ListItem li1Compare = new ListItem("--ALL--", "-1");
        li1Compare.Selected = true;
        ddl_SliderType_Compare.Items.Add(li1Compare);
        foreach (eSliderType est in Enum.GetValues(typeof(eSliderType)))
        {
            ddl_SliderType.Items.Add(new ListItem(est.ToString(), ((int)est).ToString()));
            ddl_SliderType_Compare.Items.Add(new ListItem(est.ToString(), ((int)est).ToString()));
        }
    }

    private void LoadCountries()
    {
        ddl_Country.Items.Clear();
        ddl_Country.Items.Add(new ListItem("ALL", "-1"));
        ddl_CountryCompare.Items.Clear();
        ddl_CountryCompare.Items.Add(new ListItem("ALL", "-1"));
        string command = "EXEC [dbo].[GetCountries]";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string _country = (string)reader["CountryName"];
                    string _id = ((int)reader["Id"]).ToString();
                    ListItem li = new ListItem(_country, _id);
                    ListItem liCompare = new ListItem(_country, _id); 
                    ddl_Country.Items.Add(li);
                    ddl_CountryCompare.Items.Add(liCompare);
                }
            }
            conn.Close();
        }
        ddl_Country.SelectedIndex = 0;
        ddl_CountryCompare.SelectedIndex = 0;
    }

    private void LoadLastUpdate()
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfDateTime result = null;
        try
        {
            result = _report.LastUpdateProfitableReport();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        lblLastUpdate.Text = result.Value.ToString();
    }

    private void LoadeProfileFilters()
    {
        ddl_ProfitableLosing.Items.Clear();
        foreach (WebReferenceReports.eProfitFilter epf in Enum.GetValues(typeof(WebReferenceReports.eProfitFilter)))
        {
            ListItem li = new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eProfitFilter", epf.ToString()), epf.ToString());
            li.Selected = (epf == WebReferenceReports.eProfitFilter.All);
            ddl_ProfitableLosing.Items.Add(li);
        }
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_ProfitableReport.Text);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetButtonPrintAsAsync();
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {

        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, lbl_ProfitableReport.Text);
        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (dataV == null || dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
       PageRenderin page = new PageRenderin();
       page.Controls.Add(_Repeater);
       StringBuilder sb = new StringBuilder();
       using (StringWriter sw = new StringWriter(sb))
       {
           using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
           {
               _Repeater.RenderControl(textWriter);
           }
       }   
        Session["grid_print"] = sb.ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    private void Data_bind()
    {
        _div_compareDate.DataBind();
        dataV = null;
    }
    private void setDateTime()
    {
        FromToDatePicker_current.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
            WEEK_FROM_TODAY);
    }
    private void LoadAffiliates()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfExposuresPicklistsContainer result = null;
        try
        {
            result = _site.GetExposuresPicklists();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        ddl_Partner.Items.Clear();
        ddl_PartnerCompare.Items.Clear();
        ddl_Partner.Items.Add(new ListItem(string.Empty, Guid.Empty.ToString()));
        ddl_PartnerCompare.Items.Add(new ListItem(string.Empty, Guid.Empty.ToString()));
        foreach (WebReferenceSite.GuidStringPair owp in result.Value.Origins)
        {
            ddl_Partner.Items.Add(new ListItem(owp.Name, owp.Id.ToString()));
            ddl_PartnerCompare.Items.Add(new ListItem(owp.Name, owp.Id.ToString()));
        }
        ddl_Partner.SelectedIndex = 0;
        ddl_PartnerCompare.SelectedIndex = 0;
    }

    private void LoadHeadings(bool LoadDropDown)
    {
       
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }

        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        SortedList<Guid, string> list = new SortedList<Guid, string>();
        if (LoadDropDown)
        {
            ddl_Heading.Items.Clear();
            ddl_HeadingCompare.Items.Clear();
            ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
            ddl_HeadingCompare.Items.Add(new ListItem("", Guid.Empty.ToString()));
        }
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {
            
            string pName = nodePrimary.Attribute("Name").Value;
            //    string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            Guid _id;
            if (!Guid.TryParse(_guid, out _id))
                continue;
            if (list.ContainsKey(_id))
                continue;
            list.Add(_id, pName);
            if (LoadDropDown)
            {
                ddl_Heading.Items.Add(new ListItem(pName, _guid));
                ddl_HeadingCompare.Items.Add(new ListItem(pName, _guid));
            }
        }
        if (LoadDropDown)
        {
            ddl_Heading.SelectedIndex = 0;
            ddl_HeadingCompare.SelectedIndex = 0;
        }
        HeadingList = list;
    }
    public override void Update_Faild()
    {
        span_TotalDaus.Visible = false;
        span_MonthlyUser.Visible = false;
        span_TotalDausCompare.Visible = false;
        span_MonthlyUserCompare.Visible = false;
        base.Update_Faild();
    }
    protected void btn_run_Click(object sender, EventArgs e)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ProfitableReportRequest _request = new WebReferenceReports.ProfitableReportRequest();
        _request.From = FromToDatePicker_current.GetDateFrom;
        _request.To = FromToDatePicker_current.GetDateTo;
        _request.HeadingId = new Guid(ddl_Heading.SelectedValue);
        _request.OriginId = new Guid(ddl_Partner.SelectedValue);
        _request.OnlyNonUpsales = cb_OnlyNonUpsales.Checked;
        WebReferenceReports.eProfitFilter epf;
        if (!Enum.TryParse(ddl_ProfitableLosing.SelectedValue, out epf))
            epf = WebReferenceReports.eProfitFilter.All;
        _request.ProfitFilter = epf;
        int _countryid;
        if (!int.TryParse(ddl_Country.SelectedValue, out _countryid))
            _countryid = -1;
        _request.Country = _countryid;
        int slider_type;
        if (!int.TryParse(ddl_SliderType.SelectedValue, out slider_type))
            slider_type = -1;
        _request.SliderType = slider_type;
        if (cb_Compare.Checked)
        {            
            _request.UseCompare = true;
            _request.FromCompare = FromToDatePicker_compare.GetDateFrom;
            _request.ToCompare = FromToDatePicker_compare.GetDateTo;
            _request.HeadingIdCompare = new Guid(ddl_HeadingCompare.SelectedValue);
            _request.OriginIdCompare = new Guid(ddl_PartnerCompare.SelectedValue);
            _request.OnlyNonUpsalesCompare = cb_OnlyNonUpsalesCompare.Checked;
            int _countryidCompare;
            if (!int.TryParse(ddl_CountryCompare.SelectedValue, out _countryidCompare))
                _countryid = -1;
            _request.CountryCompare = _countryidCompare;
            if (!int.TryParse(ddl_SliderType_Compare.SelectedValue, out slider_type))
                slider_type = -1;
            _request.SliderTypeCompare = slider_type;
            
        }
        else
            _request.UseCompare = false;
        _ShowCompare = _request.UseCompare;
        WebReferenceReports.ResultOfProfitableReportResponse result = null;
        try
        {
            result = _report.ProfitableReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
       

        
        DataTable data = new DataTable();
        data.Columns.Add("Heading");
        data.Columns.Add("Requests");
        data.Columns.Add("Exposures");
        data.Columns.Add("Revenue");
        data.Columns.Add("Cost");
        data.Columns.Add("TotalRequest");
        data.Columns.Add("TotalRequestCompare");
        data.Columns.Add("ExposuresCompare");
        data.Columns.Add("HeadingCompare");
        data.Columns.Add("RequestsCompare");
        data.Columns.Add("RevenueCompare");
        data.Columns.Add("CostCompare");
        data.Columns.Add("ShowCompare", typeof(bool));
        data.Columns.Add("Profit");
        data.Columns.Add("ProfitPercent");
        data.Columns.Add("ProfitCompare");
        data.Columns.Add("ProfitPercentCompare");
        data.Columns.Add("ValuePosNeg");
        data.Columns.Add("ComparePosNeg");
        data.Columns.Add("Gap");
        data.Columns.Add("GapPosNeg");
      //  data.Columns.Add("DAUs_Request");
     //   data.Columns.Add("DAUs_RequestCompare");
        data.Columns.Add("Exposure_KDAUs");
        data.Columns.Add("Exposure_KDAUs_Compare");
        data.Columns.Add("Request_KDAUs");
        data.Columns.Add("Request_KDAUs_Compare");
        /*
        data.Columns.Add("Exposure_Request_Compare");
        data.Columns.Add("Exposure_Request");
         * */
        data.Columns.Add("Request_Exposure_Compare");
        data.Columns.Add("Request_Exposure");

        data.Columns.Add("EPPL");
        data.Columns.Add("EPPLCompare");

        data.Columns.Add("CPM");
        data.Columns.Add("RPM");
        data.Columns.Add("CPMCompare");
        data.Columns.Add("RPMCompare");

        data.Columns.Add("Revenue_TotalRequest");
        data.Columns.Add("Revenue_TotalRequest_Compare");

        int _users = (from x in result.Value.MainData
                      select x.Users).FirstOrDefault();
         span_TotalDaus.Visible = true;
         LblTotalDaus.Text = string.Format(Num_Format, _users);

        decimal revenueSum = (from x in result.Value.MainData
                              select x.Revenue).Sum();

        span_MonthlyUser.Visible = true;
        decimal _MonthlyUser = (_users == 0) ? 0m : (revenueSum / (decimal)_users) * 30m * 100m;
        lblMonthlyUser.Text = string.Format(Num_Float_Format, _MonthlyUser) + '\u00A2';

        SortedList<Guid, string> sl = HeadingList;
        List<Guid> list = new List<Guid>();
        DataRow rowSum = data.NewRow();
        rowSum["ShowCompare"] = _ShowCompare;
        rowSum["Heading"] = "TOTAL";
        int TotalSoldRequest = (from x in result.Value.MainData
                                select x.Requests).Sum();
        rowSum["Requests"] = TotalSoldRequest;
        

        rowSum["Revenue"] = siteSetting.CurrencySymbol + string.Format(Num_Format, revenueSum);
        decimal costSum = (from x in result.Value.MainData
                           select x.Cost).Sum();
        rowSum["Cost"] = siteSetting.CurrencySymbol + string.Format(Num_Format, costSum);
        int _TotalRequests = (from x in result.Value.MainData
                              select x.TotalRequest).Sum();
        rowSum["TotalRequest"] = _TotalRequests;
  //      double _daus = (from x in result.Value.MainData
  //                      select x.DAUs_Request).Sum();

    //    rowSum["DAUs_Request"] = string.Format(Num_Float_Format, _daus);// Utilities.ConvertToStringNonCientificNotation(_daus);

        int SumExposures = (from x in result.Value.MainData
                            select x.Exposures).Sum();
        rowSum["Exposures"] = SumExposures;
        //decimal profitSum = revenueSum - costSum;
        rowSum["ValuePosNeg"] = (result.Value.MainProfitTotal > 0) ? "positive" : "negative";
        rowSum["Profit"] = siteSetting.CurrencySymbol + string.Format(Num_Format, result.Value.MainProfitTotal);
        rowSum["ProfitPercent"] = string.Format(Num_Format, result.Value.MainProfitPercentageTotal) + "%";
        
        double Exposure_KDAUs = _users == 0 ? -1d :
            (double)SumExposures / ((double)_users / 1000d);
        rowSum["Exposure_KDAUs"] = SalesUtility.GetNumberForReportNonNegative(Exposure_KDAUs, Num_Float_Format, "");//string.Format(Num_Float_Format, Exposure_KDAUs);
        double Request_KDAUs = _users == 0 ? -1d :
             (double)_TotalRequests / ((double)_users / 1000d);
        rowSum["Request_KDAUs"] = SalesUtility.GetNumberForReportNonNegative(Request_KDAUs, Num_Float_Format, "");
        /*
        double Exposure_Request = SumExposures == 0 ? 0d :
               (double)_TotalRequests / (double)SumExposures;
         * */
        double Request_Exposure = SumExposures == 0 ? -1d :
               (double)_TotalRequests / (double)SumExposures;
        rowSum["Request_Exposure"] = SalesUtility.GetNumberForReportNonNegative(Request_Exposure, Num_Float_Percent_Format, "");

        decimal Revenue_TotalRequest = (_TotalRequests == 0) ? -1m : revenueSum / (decimal)_TotalRequests;
        rowSum["Revenue_TotalRequest"] = SalesUtility.GetNumberForReportNonNegative(Revenue_TotalRequest, Num_Float_Format, "");

        decimal total_EPPL = (TotalSoldRequest == 0) ?-1m : revenueSum / (decimal)TotalSoldRequest;
        rowSum["EPPL"] = SalesUtility.GetNumberForReportNonNegative(total_EPPL, Num_Float_Format, "");

        double total_CPM = (SumExposures == 0) ? -1d :
                (double)costSum / ((double)SumExposures / 1000d);
        rowSum["CPM"] = SalesUtility.GetNumberForReportNonNegative(total_CPM, Num_Float_Format, "");

        double total_RPM = (SumExposures == 0) ? -1d :
                (double)revenueSum / ((double)SumExposures / 1000d);
        rowSum["RPM"] = SalesUtility.GetNumberForReportNonNegative(total_RPM, Num_Float_Format, "");
        
        if (_ShowCompare)
        {
            rowSum["HeadingCompare"] = "SUM";
            int RequestsCompare = (from x in result.Value.CompareData
                                   select x.Requests).Sum();
            rowSum["RequestsCompare"] = RequestsCompare;
            decimal revenueSumCompare = (from x in result.Value.CompareData
                                         select x.Revenue).Sum();
            rowSum["RevenueCompare"] = siteSetting.CurrencySymbol + string.Format(Num_Format, revenueSumCompare);
            decimal costSumCompare = (from x in result.Value.CompareData
                                      select x.Cost).Sum();
            rowSum["CostCompare"] = siteSetting.CurrencySymbol + string.Format(Num_Format, costSumCompare);

            int ExposuresCompare = (from x in result.Value.CompareData
                                    select x.Exposures).Sum();
            rowSum["ExposuresCompare"] = ExposuresCompare;


            //      decimal profitSumCompare = revenueSumCompare - costSumCompare;
            rowSum["ComparePosNeg"] = (result.Value.CompareProfitTotal > 0) ? "positive" : "negative";
            rowSum["ProfitCompare"] = siteSetting.CurrencySymbol + string.Format(Num_Format, result.Value.CompareProfitTotal);
            rowSum["ProfitPercentCompare"] = string.Format(Num_Format, result.Value.CompareProfitPercentageTotal) + "%";
            if (result.Value.CompareProfitTotal != 0m)
            {
                decimal mp = (result.Value.CompareProfitTotal > 0) ? result.Value.CompareProfitTotal : result.Value.CompareProfitTotal * -1;
                decimal _gap = ((result.Value.MainProfitTotal - result.Value.CompareProfitTotal) / mp) * 100m;
                rowSum["GapPosNeg"] = (_gap > 0m) ? "positive" : "negative";
                rowSum["Gap"] = string.Format(Num_Format, _gap) + "%";
            }
            else
            {
                rowSum["GapPosNeg"] = (result.Value.MainProfitTotal > 0m) ? "positive" : "negative";
                rowSum["Gap"] = (result.Value.MainProfitTotal > 0m) ? "∞" : "0%";
            }
            int _TotalRequestCompare = (from x in result.Value.CompareData
                                         select x.TotalRequest).Sum();
            rowSum["TotalRequestCompare"] = _TotalRequestCompare;
            double Request_Exposure_Compare = ExposuresCompare == 0 ? -1d :
                (double)_TotalRequestCompare / (double)ExposuresCompare;
            rowSum["Request_Exposure_Compare"] = SalesUtility.GetNumberForReportNonNegative(Request_Exposure_Compare, Num_Float_Percent_Format, "");

            int _users_Compare = (from x in result.Value.CompareData
                                  select x.Users).FirstOrDefault();

            double Exposure_KDAUs_Compare = _users_Compare == 0 ? -1d :
                (double)ExposuresCompare / ((double)_users_Compare / 1000d);
            rowSum["Exposure_KDAUs_Compare"] = SalesUtility.GetNumberForReportNonNegative(Exposure_KDAUs_Compare, Num_Float_Format, "");

            int _TotalRequests_Compare = (from x in result.Value.CompareData
                                          select x.TotalRequest).Sum();
            double Request_KDAUs_Compare = _users_Compare == 0 ? -1d :
                (double)_TotalRequests_Compare / ((double)_users_Compare / 1000d);
            rowSum["Request_KDAUs_Compare"] = SalesUtility.GetNumberForReportNonNegative(Request_KDAUs_Compare, Num_Float_Format, "");

            decimal Revenue_TotalRequest_Compare = (_TotalRequests_Compare == 0) ? -1m : revenueSumCompare / (decimal)_TotalRequests_Compare;
            rowSum["Revenue_TotalRequest_Compare"] = SalesUtility.GetNumberForReportNonNegative(Revenue_TotalRequest_Compare, Num_Float_Format, "");

            span_TotalDausCompare.Visible = true;
            LblTotalDausCompare.Text = string.Format(Num_Format, _users_Compare);



            span_MonthlyUserCompare.Visible = true;
            decimal _MonthlyUser_Compare = (_users_Compare == 0) ? -1m : (revenueSumCompare / (decimal)_users_Compare) * 30m * 100m;
            lblMonthlyUserCompare.Text = SalesUtility.GetNumberForReportNonNegative(_MonthlyUser_Compare, Num_Float_Format, "" + '\u00A2');

            decimal total_EPPLCompare = (RequestsCompare == 0) ? -1m : revenueSumCompare / (decimal)RequestsCompare;
            rowSum["EPPLCompare"] = SalesUtility.GetNumberForReportNonNegative(total_EPPLCompare, Num_Float_Format, "");

            double total_CPMCompare = (ExposuresCompare == 0) ? -1d :
                (double)costSumCompare / ((double)ExposuresCompare / 1000d);
            rowSum["CPMCompare"] = SalesUtility.GetNumberForReportNonNegative(total_CPMCompare, Num_Float_Format, "");

            double total_RPMCompare = (ExposuresCompare == 0) ? -1d :
                    (double)revenueSumCompare / ((double)ExposuresCompare / 1000d);
            rowSum["RPMCompare"] = SalesUtility.GetNumberForReportNonNegative(total_RPM, Num_Float_Format, "");
        }
        else
        {
            span_TotalDausCompare.Visible = false;
            span_MonthlyUserCompare.Visible = false;
        }
        data.Rows.Add(rowSum);
        foreach (WebReferenceReports.ProfitableReportRow prr in result.Value.MainData)
        {
            DataRow row = data.NewRow();
            
            row["ShowCompare"] = _ShowCompare;
            row["TotalRequest"] = prr.TotalRequest;
            row["Exposures"] = prr.Exposures;
            row["Heading"] = sl.ContainsKey(prr.HeadingId) ? sl[prr.HeadingId] : prr.HeadingId.ToString();
            row["Requests"] = prr.Requests;
            row["Revenue"] = siteSetting.CurrencySymbol + string.Format(Num_Format, prr.Revenue);
            row["Cost"] = siteSetting.CurrencySymbol + string.Format(Num_Format, prr.Cost);
          //  decimal profit = prr.Revenue - prr.Cost;
            row["ValuePosNeg"] = (prr.Profit > 0) ? "positive" : "negative";
            row["Profit"] = siteSetting.CurrencySymbol + string.Format(Num_Format, prr.Profit);
            row["ProfitPercent"] = string.Format(Num_Format, prr.ProfitPercentage) + "%";
       //     row["DAUs_Request"] = string.Format(Num_Float_Format, prr.DAUs_Request);// Utilities.ConvertToStringNonCientificNotation(prr.DAUs_Request);
            row["Exposure_KDAUs"] = SalesUtility.GetNumberForReportNonNegative(prr.Exposure_KDAUs, Num_Float_Format, "");
            row["Request_KDAUs"] = SalesUtility.GetNumberForReportNonNegative(prr.Request_KDAUs, Num_Float_Format, "");
            row["Request_Exposure"] = SalesUtility.GetNumberForReportNonNegative(prr.Request_Exposures, Num_Float_Percent_Format, "");
        //    decimal _Revenue_TotalRequest = (prr.TotalRequest == 0) ? 0m : prr.Revenue / (decimal)prr.TotalRequest;
            row["Revenue_TotalRequest"] = SalesUtility.GetNumberForReportNonNegative(prr.Revenue_TotalRequest, Num_Float_Format, "");
            row["EPPL"] = SalesUtility.GetNumberForReportNonNegative(prr.EPPL, Num_Float_Format, "");
            row["CPM"] = SalesUtility.GetNumberForReportNonNegative(prr.CPM, Num_Float_Format, "");
            row["RPM"] = SalesUtility.GetNumberForReportNonNegative(prr.RPM, Num_Float_Format, "");
            if (_ShowCompare)
            {
                IEnumerable<WebReferenceReports.ProfitableReportRow> query =
                    from x in result.Value.CompareData
                    where x.HeadingId == prr.HeadingId
                    select x;
                if (query.Count() > 0)
                {
                    WebReferenceReports.ProfitableReportRow prrCompare = query.SingleOrDefault();
                    row["HeadingCompare"] = sl.ContainsKey(prrCompare.HeadingId) ? sl[prrCompare.HeadingId] : prrCompare.HeadingId.ToString(); 
                    row["TotalRequestCompare"] = prrCompare.TotalRequest;
                    row["RequestsCompare"] = prrCompare.Requests;
                    row["ExposuresCompare"] = prrCompare.Exposures;
                    row["RevenueCompare"] = siteSetting.CurrencySymbol + string.Format(Num_Format, prrCompare.Revenue);
                    row["CostCompare"] = siteSetting.CurrencySymbol + string.Format(Num_Format, prrCompare.Cost);
                   // decimal profitC = prrCompare.Revenue - prrCompare.Cost;
                    row["ComparePosNeg"] = (prrCompare.Profit > 0) ? "positive" : "negative";
                    row["ProfitCompare"] = siteSetting.CurrencySymbol + string.Format(Num_Format, prrCompare.Profit);
                    row["ProfitPercentCompare"] = string.Format(Num_Format, prrCompare.ProfitPercentage) + "%";
               //     row["DAUs_RequestCompare"] = string.Format(Num_Float_Format, prrCompare.DAUs_Request);// Utilities.ConvertToStringNonCientificNotation(prrCompare.DAUs_Request);
                   
                    if (prrCompare.Profit != 0m)
                    {
                        decimal mp = (prr.Profit > 0) ? prr.Profit : prr.Profit * -1;
                        decimal _gap = ((prr.Profit - prrCompare.Profit) / prrCompare.Profit) * 100m;
                        row["GapPosNeg"] = (_gap > 0m) ? "positive" : "negative";
                        row["Gap"] = string.Format(Num_Format, _gap) + "%";
                    }
                    else
                    {
                        row["GapPosNeg"] = (prr.Profit > 0m) ? "positive" : "negative";
                        row["Gap"] = (prr.Profit > 0m) ? "∞" : "0%";
                    }
                    row["Exposure_KDAUs_Compare"] = SalesUtility.GetNumberForReportNonNegative(prrCompare.Exposure_KDAUs, Num_Float_Format, "");
                    row["Request_KDAUs_Compare"] = SalesUtility.GetNumberForReportNonNegative(prrCompare.Request_KDAUs, Num_Float_Format, "");
                    row["Request_Exposure_Compare"] = SalesUtility.GetNumberForReportNonNegative(prrCompare.Request_Exposures, Num_Float_Percent_Format, "");
                 //   decimal _Revenue_TotalRequest_Compare = (prrCompare.TotalRequest == 0) ? 0m : prrCompare.Revenue / (decimal)prrCompare.TotalRequest;
                    row["Revenue_TotalRequest_Compare"] = SalesUtility.GetNumberForReportNonNegative(prrCompare.Revenue_TotalRequest, Num_Float_Format, "");
                    row["EPPLCompare"] = SalesUtility.GetNumberForReportNonNegative(prrCompare.EPPL, Num_Float_Format, "");
                    row["CPMCompare"] = SalesUtility.GetNumberForReportNonNegative(prrCompare.CPM, Num_Float_Format, "");
                    row["RPMCompare"] = SalesUtility.GetNumberForReportNonNegative(prrCompare.RPM, Num_Float_Format, "");
                }
            }
            list.Add(prr.HeadingId);
            data.Rows.Add(row);
        }
        if (_ShowCompare && result.Value.CompareData != null)
        {
            IEnumerable<WebReferenceReports.ProfitableReportRow> queryCompare =
                        from x in result.Value.CompareData
                        where !list.Contains(x.HeadingId)
                        select x;
            foreach (WebReferenceReports.ProfitableReportRow prr in queryCompare)
            {
                DataRow row = data.NewRow();
                row["ShowCompare"] = _ShowCompare;
                row["TotalRequestCompare"] = prr.TotalRequest;
                row["ExposuresCompare"] = prr.Exposures;
                row["HeadingCompare"] = sl.ContainsKey(prr.HeadingId) ? sl[prr.HeadingId] : prr.HeadingId.ToString();
                row["RequestsCompare"] = prr.Requests;
                row["RevenueCompare"] = siteSetting.CurrencySymbol + string.Format(Num_Format, prr.Revenue);
                row["CostCompare"] = siteSetting.CurrencySymbol + string.Format(Num_Format, prr.Cost);
      //          decimal profitC = prr.Revenue - prr.Cost;
                row["ComparePosNeg"] = (prr.Profit > 0) ? "positive" : "negative";
                row["ProfitCompare"] = siteSetting.CurrencySymbol + string.Format(Num_Format, prr.Profit);
                row["ProfitPercentCompare"] = string.Format(Num_Format, prr.ProfitPercentage) + "%";

                row["Exposure_KDAUs_Compare"] = SalesUtility.GetNumberForReportNonNegative(prr.Exposure_KDAUs, Num_Float_Format, "");
                row["Request_KDAUs_Compare"] = SalesUtility.GetNumberForReportNonNegative(prr.Request_KDAUs, Num_Float_Format, "");
                row["Request_Exposure_Compare"] = SalesUtility.GetNumberForReportNonNegative(prr.Request_Exposures, Num_Float_Percent_Format, "");
                //   decimal _Revenue_TotalRequest_Compare = (prrCompare.TotalRequest == 0) ? 0m : prrCompare.Revenue / (decimal)prrCompare.TotalRequest;
                row["Revenue_TotalRequest_Compare"] = SalesUtility.GetNumberForReportNonNegative(prr.Revenue_TotalRequest, Num_Float_Format, "");
                row["EPPLCompare"] = SalesUtility.GetNumberForReportNonNegative(prr.EPPL, Num_Float_Format, "");
                row["CPMCompare"] = SalesUtility.GetNumberForReportNonNegative(prr.CPM, Num_Float_Format, "");
                row["RPMCompare"] = SalesUtility.GetNumberForReportNonNegative(prr.RPM, Num_Float_Format, "");
                data.Rows.Add(row);
            }
        }
        if (_ShowCompare)
            div_ProfitTable.Attributes["class"] = "div_ProfitTable";
        else
            div_ProfitTable.Attributes.Remove("class");
       
      
        dataV = data;
        _Repeater.DataSource = data;
        _Repeater.DataBind();
        UpdatePanel_table.Update();

    }
  
    protected string GetDivCurrentId
    {
        get { return FromToDatePicker_current.GetParentDivId(); }
    }
    protected string GetDivPastId
    {
        get { return FromToDatePicker_compare.GetParentDivId(); }
    }
    protected string GetDateFormat()
    {
        return ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat);
    }
    protected string GetFromCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxFrom().ClientID; }
    }
    protected string GetToCurrentId
    {
        get { return FromToDatePicker_current.GetTextBoxTo().ClientID; }
    }

    protected string GetFromPastId
    {
        get { return FromToDatePicker_compare.GetTextBoxFrom().ClientID; }
    }
    protected string GetToPastId
    {
        get { return FromToDatePicker_compare.GetTextBoxTo().ClientID; }
    }
    protected string GetInvalidDate
    {
        get { return Server.HtmlEncode(lbl_DateError.Text); }
    }
    protected string Lbl_DatesChange
    {
        get { return lbl_DatesChange.Text; }
    }
    protected SortedList<Guid, string> HeadingList
    {
        get { return (Session["HeadingList"] == null) ? new SortedList<Guid, string>() : (SortedList<Guid, string>)Session["HeadingList"]; }
        set { Session["HeadingList"] = value; }
    }
    DataTable dataV
    {
        get { return (Session["data"] == null) ? null : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    protected bool ShowCompare { get { return _ShowCompare; } set { _ShowCompare = value; } }
    
}