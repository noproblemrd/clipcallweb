﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Publisher_ClipCallRegisterReport : PageSetting
{
    const string Num_Format = "{0:#,0.##}";
    const int ONE_DAY = 1;
    const string V_ICON = "~/Publisher/images/icon-v.png";
    const string X_ICON = "~/Publisher/images/icon-x.png";
    const string REPORT_NAME = "Registration Report";
    protected readonly string SessionTableName = "dataClipCallRegisterReport";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            setDateTime();
            LoadSteps();
            LoadSupplierStatus();
            LoadSupplierOld();
        }
        SetToolboxEvents();
        Header.DataBind();
    }

    private void LoadSupplierOld()
    {

        ddl_oldPro.Items.Clear();
        foreach (ClipCallReport.eSupplierOldStatus sir in Enum.GetValues(typeof(ClipCallReport.eSupplierOldStatus)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(sir), sir.ToString());
            li.Selected = sir == ClipCallReport.eSupplierOldStatus.ALL;
            ddl_oldPro.Items.Add(li);
        }
    }
    private void LoadSteps()
    {
        ddl_Step.Items.Clear();
        foreach (ClipCallReport.eStepInAppRegistration sir in Enum.GetValues(typeof(ClipCallReport.eStepInAppRegistration)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(sir), sir.ToString());
            li.Selected = sir == ClipCallReport.eStepInAppRegistration.ALL;
            ddl_Step.Items.Add(li);
        }
    }
    private void LoadSupplierStatus()
    {
        ddl_SupplierStatus.Items.Clear();
        foreach (ClipCallReport.eSupplierStatusAppRegistration sir in Enum.GetValues(typeof(ClipCallReport.eSupplierStatusAppRegistration)))
        {
            ListItem li = new ListItem(Utilities.GetEnumStringFormat(sir), sir.ToString());
            li.Selected = sir == ClipCallReport.eSupplierStatusAppRegistration.ALL;
            ddl_SupplierStatus.Items.Add(li);
        }
    }
   
    private void setDateTime()
    {
        _FromToDatePicker.SetDefaultDates(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat),
             ONE_DAY);
        //  _FromToDatePicker.SetDefaultDatesMonthRound(ConvertToDateTime.GetDateFormatDisplay(siteSetting.DateFormat));
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.SetTitle(REPORT_NAME);
        ToolboxReport1.RemovePrintButton();
        string _script = "_CreateExcel();";
        ToolboxReport1.SetExcelJavascript(_script);
    }
    protected void lb_RunReport_Click(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        ClipCallReport.ClipCallReport report = WebServiceConfig.GetClipCallReportReference(this);
        ClipCallReport.RegistrationAppReportRequest request = new ClipCallReport.RegistrationAppReportRequest();

        request.From = _FromToDatePicker.GetDateFrom;
        request.To = _FromToDatePicker.GetDateTo;
        ClipCallReport.eStepInAppRegistration _step;
        if(!Enum.TryParse(ddl_Step.SelectedValue, out _step))
            _step = ClipCallReport.eStepInAppRegistration.ALL;
        request.Step = _step;
        ClipCallReport.eSupplierStatusAppRegistration _status;
        if(!Enum.TryParse(ddl_SupplierStatus.SelectedValue, out _status))
            _status = ClipCallReport.eSupplierStatusAppRegistration.ALL;
        request.SupplierStatus = _status;

        ClipCallReport.eSupplierOldStatus old_pro;
        if (!Enum.TryParse(ddl_oldPro.SelectedValue, out old_pro))
            old_pro = ClipCallReport.eSupplierOldStatus.ALL;
        request.SupplierOldStatus = old_pro;

        ClipCallReport.ResultOfListOfRegistrationAppReportResponse response = null;

        try
        {
            response = report.GetRegistrationRepost(request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (response.Type == ClipCallReport.eResultType.Failure)
        {
            Update_Faild();
            return;
        }

        DataTable data = new DataTable();
        data.Columns.Add("LastUpdate");
        data.Columns.Add("Name");
        data.Columns.Add("Categories");
        data.Columns.Add("Address");
        data.Columns.Add("Step");
        data.Columns.Add("Phone");
        data.Columns.Add("Email");
        data.Columns.Add("Device");
        data.Columns.Add("LicenseIcon");
        data.Columns.Add("Insured");
        data.Columns.Add("bbb");
        data.Columns.Add("bbbV", typeof(bool));
        data.Columns.Add("WorkerscompIcon");
        data.Columns.Add("BondedIcon");
        data.Columns.Add("CriminalRecordIcon");
        data.Columns.Add("WebSite");
        data.Columns.Add("WebSiteV", typeof(bool));
        data.Columns.Add("Description");
        data.Columns.Add("PortraitImage");        
        data.Columns.Add("Radius");
        data.Columns.Add("FirstName");
        data.Columns.Add("LastName");
        data.Columns.Add("YelpPhone");
        data.Columns.Add("YelpRating");
        data.Columns.Add("YelpReviews");
        data.Columns.Add("YelpName");
        data.Columns.Add("YelpUrl");
        data.Columns.Add("YelpUrlV", typeof(bool));
        data.Columns.Add("GoogleRating");
        data.Columns.Add("GoogleReviews");
        data.Columns.Add("GoogleUrl");
        data.Columns.Add("GoogleUrlV", typeof(bool));
        data.Columns.Add("OldProIcon");
        data.Columns.Add("Status");
        data.Columns.Add("StripeAccount");
        data.Columns.Add("StripeStandaloneAccount");


        foreach (ClipCallReport.RegistrationAppReportResponse row in response.Value)
        {
            DataRow dr = data.NewRow();
            dr["LastUpdate"] = GetDateTimeStringFormat(row.RegistrationLastUpdate);
            dr["Name"] = row.SupplierName;
            dr["Categories"] = row.Categories;
            dr["Address"] = row.FullAddress;
            dr["Step"] = row.Step.ToString();
            dr["phone"] = row.Phone;
            dr["Email"] = row.Email;
            dr["Device"] = row.OS;

            dr["LicenseIcon"] = string.IsNullOrEmpty(row.LicenseId) ? X_ICON : V_ICON;
            dr["Insured"] = row.Insuredcoverag > 0 ? string.Format(this.GetNumberFormat, row.Insuredcoverag) : string.Empty;
            
            dr["bbbV"] = !string.IsNullOrEmpty(row.BBBProfileUrl);
            dr["bbb"] = GetUrl(row.BBBProfileUrl, true);

            dr["WorkerscompIcon"] = !row.HasWorkersCompensation ? X_ICON : V_ICON;
            dr["BondedIcon"] = !row.IsBonded ? X_ICON : V_ICON;
            dr["CriminalRecordIcon"] = row.HasCriminalRecords ? X_ICON : V_ICON;
            dr["WebSite"] = GetUrl(row.Website, false);
            dr["WebSiteV"] = !string.IsNullOrEmpty(row.Website);
            dr["Description"] = row.Description;
            dr["PortraitImage"] = row.BusinessImageUrl;

            dr["Radius"] = row.Radius;
            dr["FirstName"] = row.FirstName;
            dr["LastName"] = row.LastName;
            dr["YelpPhone"] = row.YelpPhone;
            dr["YelpRating"] = row.YelpRating < 0 ? "-" : string.Format(Num_Format, row.YelpRating);
            dr["YelpReviews"] = row.YelpReviewCount < 0 ? "-" : row.YelpReviewCount.ToString();
            dr["YelpName"] = row.YelpName;
            dr["YelpUrl"] = GetUrl(row.YelpUrl, true);
            dr["YelpUrlV"] = !string.IsNullOrEmpty(row.YelpUrl);
            dr["GoogleRating"] = row.GoogleRating < 0 ? "-" : string.Format(Num_Format, row.GoogleRating);
            dr["GoogleReviews"] = row.GoogleReviewCount < 0 ? "-" : row.GoogleReviewCount.ToString();
            dr["GoogleUrl"] = GetUrl(row.GoogleUrl, true);
            dr["GoogleUrlV"] = !string.IsNullOrEmpty(row.GoogleUrl);
            dr["OldProIcon"] = row.OldPro == ClipCallReport.eSupplierOldStatus.Old ? V_ICON : X_ICON;

            dr["Status"] = Utilities.GetEnumStringFormat(row.SupplierStatus);
            dr["StripeAccount"] = row.StripeAccountId;
            dr["StripeStandaloneAccount"] = row.StripeStandaloneAccountId;

            data.Rows.Add(dr);
        }
        lbl_RecordMached.Text = response.Value.Length.ToString();
        _reapeter.DataSource = data;
        _reapeter.DataBind();
        dataV = data;
    }
    private string GetUrl(string url, bool isHttps)
    {
        if (string.IsNullOrEmpty(url))
            return null;
        if (url.StartsWith("http"))
            return url;
        return string.Format("http{0}://{1}", (isHttps ? "s" : ""), url);
    }
    protected void _reapeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        HtmlTableRow row = (HtmlTableRow)(e.Item.FindControl("tr_show"));
        string css_class = (e.Item.ItemIndex % 2 == 0) ? "odd" : "even";
        string exists_class = row.Attributes["class"];
        row.Attributes.Add("class", string.IsNullOrEmpty(exists_class) ?
            css_class : exists_class + " " + css_class);
    }
    string GetDateTimeStringFormat(DateTime dt)
    {
        return string.Format(siteSetting.DateFormat, dt) + " " +
                    string.Format(siteSetting.TimeFormat, dt);
    }
    DataTable dataV
    {
        get { return (Session[SessionTableName] == null) ? null : (DataTable)Session[SessionTableName]; }
        set { Session[SessionTableName] = value; }
    }
    protected string GetCreateExcel
    {
        get { return ResolveUrl("~/Publisher/PagingService.asmx/CreateExcelByName"); }
    }
    protected string DownloadExcel
    {
        get { return ResolveUrl("~/Publisher/DownloadExcel.ashx"); }
    }
}