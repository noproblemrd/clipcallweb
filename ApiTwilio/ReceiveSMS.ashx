﻿<%@ WebHandler Language="C#" Class="ReceiveSMS" %>

using System;
using System.Web;

public class ReceiveSMS : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        string phone = context.Request.Params["From"];
        string message = context.Request.Params["Body"];
        WebReferenceTwilio.TwilioEntryPoint twillio = WebServiceConfig.GetTwilioReference(PpcSite.GetCurrent().UrlWebReference);
        try
        {
            twillio.GetSmsCallBack(phone, message);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        context.Response.ContentType = "text/plain";
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}