﻿<%@ WebHandler Language="C#" Class="CallBackSms" %>

using System;
using System.Web;

public class CallBackSms : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        /*
        string _log = string.Empty;
        foreach (string key in context.Request.Params)
        {
            _log += key + ";" + context.Request.Params[key] + ";;;" + System.Environment.NewLine;
        }
        dbug_log.MessageLog(_log);        
         * */
        string phone = context.Request.Params["From"];
        string message = context.Request.Params["Body"];
        WebReferenceTwilio.TwilioEntryPoint twillio = WebServiceConfig.GetTwilioReference(PpcSite.GetCurrent().UrlWebReference);
        try
        {
            twillio.sf_GetSmsCallBack(phone, message);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
        context.Response.ContentType = "text/plain";
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}