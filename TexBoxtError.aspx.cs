﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TexBoxtError : System.Web.UI.Page
{
    string _reason;
    protected void Page_Load(object sender, EventArgs e)
    {
        _reason = Request.QueryString["reason"];
        if (string.IsNullOrEmpty(_reason))
            ClientScript.RegisterStartupScript(this.GetType(), "ErrorText", "ErrorText();", true);
        else if (_reason == "cookie")
        {
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
            ClientScript.RegisterStartupScript(this.GetType(), "RedirectBack", "RedirectBack();", true);
        }
        
            
        /*
        string URL = HttpContext.Current.Request.Url.Host;
        string _path = string.Format("{0}://{1}{2}",
               HttpContext.Current.Request.Url.Scheme,
               URL + ((HttpContext.Current.Request["SERVER_PORT"] != "" && URL == "localhost") ? (":" + HttpContext.Current.Request["SERVER_PORT"]) : string.Empty),
               (HttpContext.Current.Request.ApplicationPath.Equals("/")) ? string.Empty : HttpContext.Current.Request.ApplicationPath
               );
        _path += "/TexBoxtError.aspx";
        int t = 0;
         * */
        Header.DataBind();
    }
    protected string GetError
    {
        get { return HttpUtility.HtmlEncode(lbl_Error.Text); }
    }
    /*
    protected string Redirect
    {
        get { return _redirect; }
    }
     * */
}