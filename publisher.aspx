﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPPre.master" AutoEventWireup="true" CodeFile="publisher.aspx.cs" Inherits="publisher" %>

<%@ MasterType VirtualPath="~/Controls/MasterPageNPPre.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div id="homemain">
	<div id="pubmain">	

	<div id="wrapadv">

	 	<div class="subtext">

		<p>Make money and help</p><p>your users find top-notch</p><p>service providers by</p><p>adding No Problem's</p><p>pay-per-call widget to</p><p>your site. </p><p></p> </div>
		
		

		

			

			<div class="factspublisher">

			<h3 class="subclass">Some facts on No Problem's pay-per-call platform: </h3>

        <div class="factsblock01"><p><span class="sloganpublisherleft">An average CPM of</span></p><h4><span class="smalltextpublisherright">$3000 to $5000 USD</span></h4></div>

        <div class="factsblock02"><h4><span class="sloganpublisher">Thousands of advertisers</span></h4><p><span class="slogansmallpublisher">have already signed up</span></p></div>

    	  

        <div class="factsblock04"><h4><span class="sloganpublisher">Full 30% to 40%</span></h4><p><span class="slogansmallpublisher">requests per day</span></p></div>

			<div class="factsblock03"><p><span class="sloganpublisherleft">The average publisher makes</span></p>
			<h4><span class="smalltextpublisherright">$1200 /mon</span></h4></div>

      </div>			

			<div class="clear"> </div> 

			<a name="anchorVideo"></a>

			  <div id="video" style="display:none;">

			    <img src="images/xclose.png" width=31 height=31 class="right" onClick="javascript:showHide();" style="padding-top:24px;padding-right:10px;padding-bottom:5px"/>

                <iframe id="iframeVideo" width="950" height="531" src="" frameborder="0" allowfullscreen></iframe>

              </div>

			  

			<div class="boxleft"> 

		    	<h3 class="subclass">Why you should be using pay-per-call:</h3>

		        <div class="toggle-content">

								<h5>Increases site revenue</h5>

				No Problem's widget generates best-in-market CPM, so the more traffic to your site, the more its revenue will increase. 

				

				<h5>Pay only for actual calls</h5>

				<p>If you don't receive calls, you don't pay a penny.</p>

				

				<h5>Brings added value to your users</h5>

				<p>Add our widget, and your users can start finding quality service providers in their area through your site! And, like no other service out there, all they need to do is lean back and relax and the service providers will call them.</p>

				

				<h5>Extend your range of services/products</h5>

				<p>When you add No Problem's widget to your site, you are adding an accurate and user-friendly tool to help your users find top service providers in their area. </p>

				

				<h5>No setup costs</h5>

				<p>Oh, and it's free to activate! In fact, you get paid for using No Problem's widget&mdash;the better your site performs, the more revenue it generates for you</p>

				</div>

        	

			<div class="publisherslist">

      <div class="publisherleft">

      <a name="publishers"></a>
      <h3>Bloggers</h3>

              Add our widget to your blog and&mdash;cha-ching!&mdash;watch it make more money. Our CPM is way better than any other affiliate program!        

      </div>

      <div class="publisherright">

      <h3>Editorial Sites</h3>

      Help your users refine their searches and add a new source of revenue to your site.

      </div>

      <h3><span class="listcontent">Content</span></h3>

      <div class="publisherleft">

      <h3>Index Sites</h3>

      Help your users refine their searches and add a new source of revenue to your site.

      </div>

      <div class="publisherright">

      <h3>Directories</h3>

      Connect users to quality local service providers and increase site revenue by installing our widget. 

      </div>

      <h3><span class="listlocal">Local</span></h3>

      </div>	

      

      

      

		</div>

				

			<div class="boxright"> 

	            <h3 class="subclass">Here's how it works:</h3>

				

            <div id="phone" style="display:block;">      

                <img src="Formal/images/video.png" width=279 height=158  onclick="javascript:showHide();" />   

            </div>

     

				<div class="adv01orange">You install No Problem's widget on your site.</div>

	      <div class="adv02orange">Your users connect with service providers in their area.</div>

				<div class="adv03orange">You get paid.</div>
      			<a href="javascript:void(0);" onClick="javasceipt:showWizard();" class="getwidgetpartner"></a>	
			</div>

			

			<div class="clear"></div>

			<div class="qa">

            <a name="pubQa"></a>
			<h3 class="subclass">Here are our answers to your questions:</h3>

			

			<div class="aq"><span class="number">1.</span> <span class="questionorange">How and when will I get paid?</span><br />

			<div class="answer">We will pay you by wire transfer each month.

</div></div>



			<div class="aq"><span class="number">2.</span> <span class="questionorange">How easy is it to add the widget to my website? Do I need to know how to code?</span><br />

			<div class="answer">You definitely don't need to know how to code! It's actually quite easy to add the widget to your site.<br /> Just follow the steps in  

    our "Get Widget Wizard" and then let your users know about it. Pretty soon,<br /> you'll start making money!</div></div>

			

			<div class="aq"><div class="number">3.</span> <span class="questionorange">I put the widget on my site and it has started making money. How can I make more money?</span><br />

			<div class="answer">Let your audience know about it. Put it in a more prominent place. Mention it in your mailing list.<br /> Tell your friends about it. The  

    more people using the widget, the more money you'll make.</div></div>



			

			</div>



	</div> <!-- End wrapadv -->
</div>
</div>
</div>



</asp:Content>

