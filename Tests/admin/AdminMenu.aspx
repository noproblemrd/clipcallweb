﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminMenu.aspx.cs" Inherits="admin_AdminMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="Style.css" rel="Stylesheet" type="text/css" />
    
    
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="div_tableMenu">
        <center>
            <table>
            <tr>
            <td>
                
                <asp:HyperLink ID="hl_LoadOriginalTextOfPage" runat="server" NavigateUrl="~/admin/LoadOriginalTextOfPage.aspx">Load Original Text Of Page</asp:HyperLink>
            </td>
            <td>
                
                <asp:HyperLink ID="hl_insertNewSite" runat="server" NavigateUrl="~/admin/InsertNewSite.aspx">Insert new site</asp:HyperLink>
            </td>
            </tr>
            
            <tr>
            <td>
                <asp:HyperLink ID="hl_GoogleAnalytic" runat="server" NavigateUrl="~/admin/LoadGoogleAnalytics.aspx">Load google analytics</asp:HyperLink>
            </td>
            <td>
                <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/admin/LoadGoogleCampain.aspx">Load google campain</asp:HyperLink>
            </td>
            </tr>
            
            <tr>
            <td>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/admin/InsertNewPage.aspx">Insert new page</asp:HyperLink>
            </td>
            <td>
                
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/admin/InsertWizardReport.aspx">Insert wizard report</asp:HyperLink>
            </td>
            </tr>
            
            <tr>
            <td>
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/admin/DeleteWizardReport.aspx">Delete wizard report</asp:HyperLink>
            </td>
            <td>
                
                <asp:HyperLink ID="hl_copyTranslate" runat="server" NavigateUrl="~/admin/CopyTranslate.aspx">Copy translate</asp:HyperLink>
            </td>
            </tr>
            
            </table>
        </center>
        </div>
    </div>
    </form>
</body>
</html>
