﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogInAdmin.aspx.cs" Inherits="admin_LogInAdmin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
        <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="User:"></asp:Label>
            
        </td>
        <td>
            <asp:TextBox ID="txt_user" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ErrorMessage="RequiredFieldValidator" ControlToValidate="txt_user" ValidationGroup="AdminLogin"></asp:RequiredFieldValidator>
        </td>
        </tr>
        <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="password:"></asp:Label>
            
        </td>
        <td>
            <asp:TextBox ID="txt_password" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ErrorMessage="RequiredFieldValidator" ControlToValidate="txt_password" ValidationGroup="AdminLogin"></asp:RequiredFieldValidator>
        </td>
        </tr>
        
        <tr>
        <td colspan="2">
        <center>
            <asp:Label ID="lbl_comment" runat="server" ForeColor="Red"></asp:Label>
        </center>
        </td>        
        </tr>
        
        <tr>
        <td colspan="2">
        <center>
            <asp:Button ID="btn_submit" runat="server" Text="Submit" ValidationGroup="AdminLogin"
                onclick="btn_submit_Click" />
        </center>
        </td>        
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
