﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_DeleteWizardReport : PageAdmin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadIssues();
        }
    }

    private void LoadIssues()
    {
        string command = "EXEC dbo.GetWizardIssues";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        DataTable data = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(data);
        conn.Close();
        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    protected void _GridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int _id = (int)(_GridView.DataKeys[e.RowIndex].Value);
        string command = "EXEC dbo.DeleteWizardIssue @WizardIssueId";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@WizardIssueId", _id);
        int a = cmd.ExecuteNonQuery();
        conn.Close();
        LoadIssues();
    }
}
