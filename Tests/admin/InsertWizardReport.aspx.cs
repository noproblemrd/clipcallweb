﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class admin_InsertWizardReport : PageAdmin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadEnums();
            LoadEnumFilters();
        }
    }

    private void LoadEnumFilters()
    {
        string command = "EXEC dbo.GetWizardFilters";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            cbl_filters.Items.Add(new ListItem((string)reader["FilterName"], (int)reader["FilterId"] + ""));
        }
        conn.Close();
    }

    private void LoadEnums()
    {
        Dictionary<int, Type> dic = new Dictionary<int, Type>();
        SqlConnection conn = DBConnection.GetConnString();
        string command = "EXEC dbo.GetPagesEnum";
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            int PageId = (int)reader["Id"];
            string name = (string)reader["PageName"];
            string Reference = (string)reader["Reference"];
            Type _type = class_enum.GetEnumType(Reference);
            if (_type == null)
                continue;
            ListItem li = new ListItem(name, PageId + "");
           
            ddl_Columns.Items.Add(li);
            dic.Add(PageId, _type);
        }
        conn.Close();
        DicV = dic;
    }   

    protected void btn_add_Click(object sender, EventArgs e)
    {
   //     int FiltersPageId = int.Parse(ddl_filters.SelectedValue);
        int ColumnsPageId = int.Parse(ddl_Columns.SelectedValue);
    //    Type FilterType = DicV[FiltersPageId];
        string filters = string.Empty;
        foreach (ListItem li in cbl_filters.Items)
        {
            if(li.Selected)
                filters += li.Value + ";";
        }
        Type ColumnsType = DicV[ColumnsPageId];
   //     byte[] _byte = Utilities.ObjectToByteArray(_type);
        string command = "EXEC dbo._InsertWizardIssue @Name, @Filters, @ColumnsPageId, @Columns";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@Name", txt_name.Text.Trim());       
        cmd.Parameters.AddWithValue("@ColumnsPageId", ColumnsPageId);
        cmd.Parameters.AddWithValue("@Filters", filters);
        cmd.Parameters.AddWithValue("@Columns", GetStrNames(ColumnsType));
        int IssueId = (int)cmd.ExecuteScalar();
        cmd.Dispose();

        if (IssueId > 0)
        {
            txt_name.Text = string.Empty;
            foreach (ListItem li in cbl_filters.Items)
            {
                li.Selected = false;
            }
            ddl_Columns.SelectedIndex = 0;
            lbl_comment.Text = "SUCCESS";
        }
        else
        {
            lbl_comment.Text = "EXISTS";
            return;
        }

        conn.Close();
       // SetStep2(IssueId);
    }
   
    string GetStrNames(Type _enumType)
    {
        StringBuilder sb = new StringBuilder();
        foreach (string s in Enum.GetNames(_enumType))
        {
            sb.Append(s + ";");
        }
        if (sb.Length == 0)
            return string.Empty;
        return sb.ToString().Substring(0, sb.Length - 1);
    }
   
    int IssueIdV
    {
        get { return (ViewState["IssueId"] == null) ? -1 : (int)ViewState["IssueId"]; }
        set { ViewState["IssueId"] = value; }
    }
    Dictionary<int, Type> DicV
    {
        get { return (ViewState["dic"] == null) ? new Dictionary<int, Type>() : (Dictionary<int, Type>)ViewState["dic"]; }
        set { ViewState["dic"] = value; }
    }  
   

    
}
