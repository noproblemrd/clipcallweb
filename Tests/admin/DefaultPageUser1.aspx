﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DefaultPageUser1.aspx.cs" Inherits="admin_DefaultPageUser1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../UpdateProgress/updateProgress.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Publisher/style.css"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:TemplateField HeaderText="Dashbord as home page">
                <ItemTemplate>
                    <asp:CheckBox ID="_cb" runat="server" Checked=<%# Bind('_cb') %> />
                    <asp:Label ID="lbl_ref" runat="server" Text="<%# Bind('WebReference') %>" Visible="false"></asp:Label>
                    <asp:Label ID="lbl_ID" runat="server" Text="<%# Bind('ID') %>" Visible="false"></asp:Label>
                    <asp:Label ID="lbl_UserId" runat="server" Text="<%# Bind('UserId') %>" Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="SiteID" HeaderText="SiteId" />
            <asp:BoundField DataField="Domain" HeaderText="Domain" />
        </Columns>
        </asp:GridView>
        <asp:Button ID="btn_set" runat="server" Text="SET" onclick="btn_set_Click" />
        <asp:Label ID="lbl_comments" runat="server" ForeColor="Red" Font-Size="Large"></asp:Label>
    </div>
    
    </form>
</body>
</html>
