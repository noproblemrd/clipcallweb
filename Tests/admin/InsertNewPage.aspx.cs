﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;

public partial class admin_InsertNewPage : PageAdmin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadPageTypes();
            tr_reference.Visible = (ddl_type.SelectedItem.Text == "Enum");
        }
    }

    private void LoadPageTypes()
    {
        string command = "EXEC dbo.GetPageTypes";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            ddl_type.Items.Add(new ListItem((string)reader["TypeName"], ((int)reader["ID"]).ToString()));
        }
        conn.Close();
    }

    protected void ddl_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        tr_reference.Visible = (ddl_type.SelectedItem.Text == "Enum");
        //WebReferenceReports.CallsFilters
        //WebReferenceReports.CallColumns
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        string name = txt_pageName.Text.Trim();
        string description = txt_description.Text.Trim();
        string reference = string.Empty;
        Type _type = null;
        if (ddl_type.SelectedItem.Text == "Enum")
        {
            reference = txt_enumType.Text;
            Assembly assembly = typeof(WebReferenceReports.SupplierStatus).Assembly;
            _type = assembly.GetType(reference);
            if (_type == null)
            {
                assembly = typeof(eChartType).Assembly;
                _type = assembly.GetType(reference);
                if (_type == null)
                {
                    lbl_comment.Text = "Enum class not found";
                    return;
                }
            }
            if (!_type.IsEnum)
            {
                lbl_comment.Text = "The class is not enum type";
                return;
            }
        }
        string command = "EXEC dbo._InsertPage @PageName, @description, " +
            "@PageTypeId, @Reference, @HasAudit";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@PageName", name);
        cmd.Parameters.AddWithValue("@description", description);
        cmd.Parameters.AddWithValue("@PageTypeId", int.Parse(ddl_type.SelectedValue));
        if (_type == null)
        {
            cmd.Parameters.AddWithValue("@Reference", DBNull.Value);            
        }
        else
        {           
            cmd.Parameters.AddWithValue("@Reference", reference);
        }
        cmd.Parameters.AddWithValue("@HasAudit", cb_audit.Checked);

        string result = (string)cmd.ExecuteScalar();
        
        conn.Close();
        lbl_comment.Text = result;

        
    }
}
