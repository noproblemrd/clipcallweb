﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetSentenseFromExcell.aspx.cs" Inherits="admin_SetSentenseFromExcell" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">
    function CheckFile()
    {
        var _file = document.getElementById("_FileUpload");
        if(_file.value.length==0)
        {
            alert('no file');
            return false;
        }
        return true;
    }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
        <tr>
        <td>
            file:
        </td>
        <td>
            <asp:FileUpload ID="_FileUpload" runat="server" />
        </td>
        </tr>
        <tr>
        <td colspan="2">
            <asp:Button ID="btn_upload" runat="server" Text="Upload" 
                onclick="btn_upload_Click" OnClientClick="return CheckFile();" />
        </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
