﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class admin_SetFilters : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            LoadFilters();
   //         LoadEnumPages();
        }
    }

    private void LoadFilters()
    {
        List<string> list = new List<string>();
        foreach(string s in Enum.GetNames(typeof(eWizardFilters)))
        {
            list.Add(s);
        }
        Dictionary<int, string> dic = new Dictionary<int, string>();
        DataTable data = new DataTable();
        data.Columns.Add("FilterId");
        data.Columns.Add("FilterName");
        data.Columns.Add("FilterTypeId");
        data.Columns.Add("Reference");
        string command = "EXEC dbo.GetWizardFilters";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            string s = (string)reader["FilterName"];
            if(list.Contains(s))
                list.Remove(s);           
        }
        reader.Dispose();
        reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            DataRow row = data.NewRow();
            row["FilterId"] = (int)reader["FilterId"];
            row["FilterName"] = (string)reader["FilterName"];
            row["FilterTypeId"] = (int)reader["FilterTypeId"];
            row["Reference"] = reader.IsDBNull(4) ? string.Empty : (string)reader["Reference"];
            data.Rows.Add(row);
        }
        reader.Dispose();
        cmd.Dispose();
        while (list.Count > 0)
        {
            string _name = list[0];
            DataRow row = data.NewRow();
            row["FilterId"] = -1;
            row["FilterName"] = _name;
            row["FilterTypeId"] = -1;
            data.Rows.Add(row);
            list.Remove(_name);
        }
        command = "EXEC dbo.GetWizardFilteType";
        cmd = new SqlCommand(command, conn);
        reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            dic.Add((int)reader["Id"], (string)reader["Name"]);
        }
        conn.Close();
        _GridView.DataSource = data;
        _GridView.DataBind();
        foreach (GridViewRow gvr in _GridView.Rows)
        {
            DropDownList ddl = (DropDownList)gvr.FindControl("ddl_types");
            int type_id = int.Parse(((Label)gvr.FindControl("lbl_FilterTypeId")).Text);
            foreach (KeyValuePair<int, string> kvp in dic)
            {
                ListItem li = new ListItem(kvp.Value, kvp.Key + "");
                li.Selected = (kvp.Key == type_id);
                ddl.Items.Add(li);
            }
        }

    }
    protected void btn_set_Click(object sender, EventArgs e)
    {
        string command = "EXEC dbo.SetWizardFilterType @WizardFilterTypeId, @WizardFilterId, @Reference, @Name";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        foreach (GridViewRow gvr in _GridView.Rows)
        {
            string filterName = ((Label)gvr.FindControl("lbl_filterName")).Text;
            int filterId = int.Parse(((Label)gvr.FindControl("lbl_filterId")).Text);
            string _reference = ((TextBox)gvr.FindControl("txt_Reference")).Text.Trim();
            int typeId = int.Parse(((DropDownList)gvr.FindControl("ddl_types")).SelectedValue);
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@WizardFilterId", filterId);
            cmd.Parameters.AddWithValue("@Name", filterName);
            if (string.IsNullOrEmpty(_reference))
                cmd.Parameters.AddWithValue("@Reference", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@Reference", _reference);
            cmd.Parameters.AddWithValue("@WizardFilterTypeId", typeId);
            int a = cmd.ExecuteNonQuery();
            cmd.Dispose();
        }
        conn.Close();
    }
}
