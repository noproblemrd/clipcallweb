﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetFilters.aspx.cs" Inherits="admin_SetFilters" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false">
            <Columns>
            
            <asp:TemplateField HeaderText="Filter name">
            <ItemTemplate>
                <asp:Label ID="lbl_filterName" runat="server" Text="<%# Bind('FilterName') %>"></asp:Label>
                <asp:Label ID="lbl_filterId" runat="server" Text="<%# Bind('FilterId') %>" Visible="false"></asp:Label>
            </ItemTemplate>                
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Set type">
            <ItemTemplate>
                <asp:DropDownList ID="ddl_types" runat="server">
                </asp:DropDownList>
                <asp:Label ID="lbl_FilterTypeId" runat="server" Text="<%# Bind('FilterTypeId') %>" Visible="false"></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Reference">
            <ItemTemplate>
                
                <asp:TextBox ID="txt_Reference" runat="server" Text="<%# Bind('Reference') %>">
                </asp:TextBox>
            </ItemTemplate>                
            </asp:TemplateField>
                
            
            </Columns>
        </asp:GridView>
        
        <asp:Button ID="btn_set" runat="server" Text="set" onclick="btn_set_Click" />
    </div>
    </form>
</body>
</html>
