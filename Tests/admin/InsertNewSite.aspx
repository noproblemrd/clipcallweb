﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InsertNewSite.aspx.cs" Inherits="admin_InsertNewSite" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
        <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Site name id"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txt_siteId" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ErrorMessage="RequiredFieldValidator" ControlToValidate="txt_siteId"
             ValidationGroup="insert_site"></asp:RequiredFieldValidator>
        </td>
        </tr>
        
        <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="Web reference"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txt_webReference" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ErrorMessage="RequiredFieldValidator"
             ControlToValidate="txt_webReference" ValidationGroup="insert_site"></asp:RequiredFieldValidator>
        </td>
        </tr>
        
        <tr>
        <td>
            <asp:Label ID="Label4" runat="server" Text="URL"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txt_URL" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
            ErrorMessage="RequiredFieldValidator"
             ControlToValidate="txt_URL" ValidationGroup="insert_site"></asp:RequiredFieldValidator>
        </td>
        </tr>
        
        <tr>
        <td>
            <asp:Label ID="Label5" runat="server" Text="OrigionId"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txt_OrigionId" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
            ErrorMessage="RequiredFieldValidator"
             ControlToValidate="txt_OrigionId" ValidationGroup="insert_site"></asp:RequiredFieldValidator>
        </td>
        </tr>
        
        <tr>
        <td>
            <asp:Label ID="Label3" runat="server" Text="Default language"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddl_languages" runat="server">
            </asp:DropDownList>
        </td>
        </tr>
        
        <tr>
        <td colspan="2">
            <center>
                <asp:Label ID="lbl_comment" runat="server" ForeColor="Red"></asp:Label>
               
                
            </center>
        </td>
        </tr>
        
        <tr>
        <td colspan="2">
            <center>
                <asp:Button ID="btn_set" runat="server" Text="Insert" 
                    ValidationGroup="insert_site" onclick="btn_set_Click"/>
                
            </center>
        </td>
        </tr>
        </table>
        <div>
            
        </div>
    </div>
    </form>
</body>
</html>
