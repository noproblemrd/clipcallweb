using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Publisher_CopyTranslate : PageAdmin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetSites();
        }
    }
    protected void GetSites()
    {
  
        string command = "EXEC dbo.GetSites";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ddlCopy.Items.Add(new ListItem((string)reader["SiteNameId"], "" + (int)reader["Id"]));
                ddlSource.Items.Add(new ListItem((string)reader["SiteNameId"], "" + (int)reader["Id"]));
            }

            conn.Close();
        }
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        if(ddlCopy.SelectedIndex<0 || ddlSource.SelectedIndex<0)
        {
            lblResult.Text="no site selected!!";
            return;
        }
        int sourceSiteId=int.Parse(ddlSource.SelectedValue);
        int toSiteId=int.Parse(ddlCopy.SelectedValue);
        if (sourceSiteId == toSiteId)
        {
            lblResult.Text = "Same site!!";
            return;
        }
        string command = "EXEC dbo.CopyTranslateLikeSite @SourceSiteId, @ToSiteId";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@SourceSiteId", sourceSiteId);
            cmd.Parameters.AddWithValue("@ToSiteId", toSiteId);
            int a = cmd.ExecuteNonQuery();
            conn.Close();
        }
        lblResult.Text = "OK!!";
    }
}
