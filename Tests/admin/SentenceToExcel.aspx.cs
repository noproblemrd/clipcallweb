﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_SentenceToExcel : PageAdmin
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_toExcel_Click(object sender, EventArgs e)
    {
        string command = "EXEC dbo._GetAllSentences";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        DataTable data = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(data);
        conn.Close();
        ToExcel _excel = new ToExcel(this, "Sentences");
        _excel.ExecExcel(data);
    }
}
