﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_DefaultPageUser1 : PageAdmin
{
    const string USER = "user1@noproblem.co.il";
    const string PASSWORD = "123456";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Response.Buffer = false;
            Response.Write(@"<div id=""divLoader"" class=""divLoader""><table><tr><td><img src=""../UpdateProgress/ajax-loader.gif"" alt=""Loading..."" /></td></tr></table></div>");
            Response.Flush();

            LoadUsers();
        }
    }

    private void LoadUsers()
    {
        DataTable data = new DataTable();
        data.Columns.Add("ID", typeof(int));
        data.Columns.Add("SiteID", typeof(string));
        data.Columns.Add("_cb", typeof(bool));
        data.Columns.Add("Domain", typeof(string));
        data.Columns.Add("UserId", typeof(string));
        data.Columns.Add("WebReference", typeof(string));
        
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;

        SqlConnection conn = DBConnection.GetConnString();
        string command = "EXEC  dbo._GetSitesUser1";
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            string SiteNameId = (string)reader["SiteNameId"];
            int _id = (int)reader["Id"];
            string Domain = string.Empty;
            if(!reader.IsDBNull(1))
                Domain = (string)reader["URL"];
            if (reader.IsDBNull(2))
                continue;
            string WebReference = (string)reader["WebReference"];
            WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(WebReference);
            try
            {
                _response = supplier.UserLogin(USER, PASSWORD);
            }
            catch (Exception exc)
            {
        //        lbl_comments.Text = "Faild 'supplier.UserLogin'";
                continue;
            }
            if (_response.Type != WebReferenceSupplier.eResultType.Success || WebReferenceSupplier.eUserLoginStatus.OK != _response.Value.UserLoginStatus)
            {
 //               lbl_comments.Text = "Faild 'supplier.UserLogin'";
                continue;
            }
            bool _cb = DBConnection.IsDashboardHomePage(_response.Value.UserId, SiteNameId);
            DataRow row = data.NewRow();
            row["SiteID"] = SiteNameId;
            row["UserId"] = _response.Value.UserId.ToString();
            row["ID"] = _id;
            row["Domain"] = Domain;
            row["_cb"] = _cb;
            row["WebReference"] = WebReference;
            data.Rows.Add(row);

        }
        conn.Close();
        _GridView.DataSource = data;
        _GridView.DataBind();
    }
    protected void btn_set_Click(object sender, EventArgs e)
    {
        string command = "EXEC dbo._SetPublisherUserDashbord @UserId, @SiteId, @DashboardAsHomePage";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        foreach (GridViewRow row in _GridView.Rows)
        {
            bool IsDashbord = (((CheckBox)row.FindControl("_cb")).Checked);
            int SiteId = int.Parse(((Label)row.FindControl("lbl_ID")).Text);
            Guid UserId = new Guid(((Label)row.FindControl("lbl_UserId")).Text);
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@SiteId", SiteId);
            cmd.Parameters.AddWithValue("@DashboardAsHomePage", IsDashbord);
            int a = cmd.ExecuteNonQuery();
            cmd.Dispose();
        }
        conn.Close();
        ClientScript.RegisterStartupScript(this.GetType(), "Success", "alert('Success');", true);
    }
}
