using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

public partial class LoadOriginalTextOfPage : PageAdmin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadTypes();
            LoadPages();
            if (_Pages.Items.Count > 0)
                LoadSentences(0);
        }
    }

    private void LoadTypes()
    {
        foreach (string s in Enum.GetNames(typeof(LiteralType)))
        {
            RadioButtonList1.Items.Add(s);
        }
        if (RadioButtonList1.Items.Count > 0)
            RadioButtonList1.SelectedIndex = 0;
    }

    private void LoadSentences(int ind)
    {
        DataTable data = new DataTable();
        int pageId = int.Parse(_Pages.Items[ind].Value);
        string command = "SELECT dbo.GetPageEnumReferenceByPageId(@PageId)";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@pageId", pageId);
        string _ref;
        if (cmd.ExecuteScalar() != DBNull.Value)
        {
            _ref = (string)cmd.ExecuteScalar();
            Type _type = class_enum.GetEnumType(_ref);
            cmd.Dispose();
            List<string> listEnum = new List<string>();
     //       List<string> listDB = new List<string>();
            foreach (string _val in Enum.GetNames(_type))
            {
                listEnum.Add(_val);
            }
            foreach (ListItem li in RadioButtonList1.Items)
            {
                li.Selected = (li.Value == LiteralType.Regular.ToString());
                li.Enabled = (li.Value == LiteralType.Regular.ToString());
            }
            command = "EXEC dbo.GetSentencesByPageId @pageId, @Type";   

            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@PageId", pageId);

            cmd.Parameters.AddWithValue("@Type", RadioButtonList1.SelectedValue);

            SqlDataReader reader = cmd.ExecuteReader();
            List<int> listDB = new List<int>();
            while (reader.Read())
            {
                string _ControlId = (string)reader["ControlId"];
                if (listEnum.Contains(_ControlId))
                    listEnum.Remove(_ControlId);
                else
                    listDB.Add((int)reader["Id"]);
                //delete value
                //         else                
                //             EditTranslateDB.DeleteEnumValue((int)reader["Id"]);           
            }
            foreach (string str in listEnum)
                EditTranslateDB.SetNewEnumValue(pageId, str);
            foreach (int _id in listDB)
                EditTranslateDB.DeleteEnumValue(_id);
            reader.Dispose();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(data);
            lbl_type.Text = PageType.Enum.ToString();
        }
        else
        {
            foreach (ListItem li in RadioButtonList1.Items)
            {
                li.Enabled = true;
            }
            cmd.Dispose();
            command = "EXEC dbo.GetSentencesByPageId @pageId, @Type";
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@pageId", pageId);
            cmd.Parameters.AddWithValue("@Type", RadioButtonList1.SelectedValue);            
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(data);
            lbl_type.Text = PageType.Page.ToString();
        }
        
        conn.Close();
        _GridViewSentence.DataSource = data;
        _GridViewSentence.DataBind();
        lbl_page.Text = dicPagesNamesV[pageId];
    }

    private void LoadPages()
    {
        Dictionary<int, string> dic = new Dictionary<int, string>();
        string command = "EXEC dbo.GetPages";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            int _id = (int)reader["Id"];
            _Pages.Items.Add(new ListItem((string)reader["PageName"], "" + _id));
            dic.Add(_id, (string)reader["description"]);
        }
        conn.Close();
        dicPagesNamesV = dic;        
        
    }
    private Dictionary<int, string> LoadPageDescription()
    {
        Dictionary<int, string> dic = new Dictionary<int, string>();
        string command = "EXEC dbo.GetPages";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            int _id = (int)reader["Id"];           
            dic.Add(_id, (string)reader["description"]);
        }
        conn.Close();
        return dic;       
    }
    protected void _Pages_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSentences(_Pages.SelectedIndex);
    }
    protected void _GridViewSentence_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        int literalId = (int)(_GridViewSentence.DataKeys[e.RowIndex].Value);
        string command = "EXEC dbo.DeleteLiteralEngByLiteralId @LiteralId";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@LiteralId", literalId);
        int a = cmd.ExecuteNonQuery();
        conn.Close();
        LoadSentences(_Pages.SelectedIndex);
    }
    protected void _set_Click(object sender, EventArgs e)
    {
        int pageId = int.Parse(_Pages.SelectedValue);
        string controlId = txt_control.Text.Trim();
        string sentence = txt_sentence.Text.Trim();
        string ControlValue = txt_value.Text.Trim();
        if(string.IsNullOrEmpty(controlId) || string.IsNullOrEmpty(sentence))
            return;
        int indexItem;
        if (!int.TryParse(txt_index.Text.Trim(), out indexItem))
            indexItem = -1;
        if (lbl_type.Text == PageType.Enum.ToString())
        {
            bool IsExists = false;
            int _index = -1;
            for (int i = 0; i < _GridViewSentence.HeaderRow.Cells.Count; i++)
            {
                if (_GridViewSentence.HeaderRow.Cells[i].Text == "ControlId")
                {
                    _index = i;
                    break;
                }
            }
            
            foreach (GridViewRow row in _GridViewSentence.Rows)
            {
                if (row.Cells[_index].Text == controlId)
                {
                    IsExists = true;
                    break;
                }                
            }
            if (!IsExists)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "NotAllowed", "alert('You can not insert new value to Enum');", true);
                return;
            }
        }
        string command = "EXEC dbo.InsertLiteralByPageId @PageId, " +
            "@Sentence, @ControlId, @IndexItem, @LiteralType, @ControlValue";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@PageId", pageId);
        cmd.Parameters.AddWithValue("@Sentence", sentence);
        cmd.Parameters.AddWithValue("@ControlId", controlId);
        if(indexItem > -1)
            cmd.Parameters.AddWithValue("@IndexItem", indexItem);
        else
            cmd.Parameters.AddWithValue("@IndexItem", DBNull.Value);
        cmd.Parameters.AddWithValue("@LiteralType", RadioButtonList1.SelectedValue);
        if (string.IsNullOrEmpty(ControlValue))
            cmd.Parameters.AddWithValue("@ControlValue", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@ControlValue", ControlValue);
        int a=cmd.ExecuteNonQuery();
        conn.Close();
        txt_control.Text = "";
        txt_index.Text = "";
        txt_sentence.Text = "";
        txt_value.Text = "";
        LoadSentences(_Pages.SelectedIndex);
    }
    protected void RadioButtonList1_TextChanged(object sender, EventArgs e)
    {
        LoadSentences(_Pages.SelectedIndex);
    }
    Dictionary<int, string> dicPagesNamesV
    {
        set { ViewState["dicPagesNames"] = value; }
        get
        {
            if (ViewState["dicPagesNames"] == null)
            {
                Dictionary<int, string> dic = LoadPageDescription();
                ViewState["dicPagesNames"] = dic;
                return dic;
            }
            else
                return (Dictionary<int, string>)ViewState["dicPagesNames"];
        }
    }

}
