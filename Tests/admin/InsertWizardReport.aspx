﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InsertWizardReport.aspx.cs" Inherits="admin_InsertWizardReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="Panel_Step1" runat="server">
        
        <table>
        <tr>
        <td>
            Wizard name:
        </td>
        <td>
            <asp:TextBox ID="txt_name" runat="server"></asp:TextBox>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
             ControlToValidate="txt_name" ValidationGroup="NewWizaed"></asp:RequiredFieldValidator>
            
        </td>
        </tr>
        
        <tr>
        <td>
            Enum Filters:
        </td>
            
        <td>
            
            <asp:CheckBoxList ID="cbl_filters" runat="server">
            </asp:CheckBoxList>
            
        </td>
        </tr>
        
        <tr>
        <td>
            Enum Columns Type:
        </td>
        <td>
            
            <asp:DropDownList ID="ddl_Columns" runat="server">
            </asp:DropDownList>
            
        </td>
        </tr>
        
        <tr>
        <td colspan="2">
            <asp:Label ID="lbl_comment" runat="server" ForeColor="Red" ></asp:Label>
        </td>
        </tr>
        
        <tr>
        <td colspan="2">
        <center>
            <asp:Button ID="btn_add" runat="server" Text="Set" onclick="btn_add_Click" ValidationGroup="NewWizaed"/>
        </center>
        </td>
        </tr>
        </table>        
            
        </asp:Panel>
        
    </form>
</body>
</html>
