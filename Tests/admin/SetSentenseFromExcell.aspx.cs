﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.OleDb;
using System.IO;

public partial class admin_SetSentenseFromExcell : PageAdmin
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_upload_Click(object sender, EventArgs e)
    {
        string path = _FileUpload.PostedFile.FileName;

        if (string.IsNullOrEmpty(path))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "noFile", "alert('File error');", true);
            return;
        }
        string extension = Path.GetExtension(path);
        string localPath = System.Configuration.ConfigurationManager.AppSettings["professionalRecord"];
        int rnd = new Random().Next();
        localPath += rnd + "upload" + extension;
        File.Copy(path, localPath);


        string sConnectionString = string.Empty;

        if (extension == ".xlsx")
            sConnectionString =
            "Provider=Microsoft.ACE.OLEDB.12.0;Password=\"\";User ID=Admin;Data Source=" + localPath + ";Mode=Share Deny Write;Extended Properties=\"HDR=YES;\";Jet OLEDB:Engine Type=37";
        else if (extension == ".xls")
            sConnectionString =
                "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + localPath + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=YES;""";
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(), "incorrect_file", "alert('Incorrect file');", true);
            return;
        }

        OleDbConnection objConn = new OleDbConnection(sConnectionString);
        try
        {
            objConn.Open();
            DataTable MySchemaTable = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            string sheetName = MySchemaTable.Rows[0].Field<string>("TABLE_NAME");
            if (!sheetName.Contains("$"))
                sheetName += "$";
            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "]", objConn);

            OleDbDataReader reader = objCmdSelect.ExecuteReader();
            /*
            if (!MuchCulomsNames(reader, "Code", "Segment", "Auction", "Min price", "Show certificate"))
            {
                lbl_report.Text = lbl_incorrectFields.Text;
                _UpdatePanelReport.Update();
                div_report.Attributes["class"] = "popModal_del";
                _mpe.Show();
          //      string _script = "ShowReport();";
          //      ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowReport", _script, true);
                return;
            }
             * */
            string command = "EXEC dbo._SetEngLiteralByLiteralId @LiteralId, @NewSentence";
            SqlConnection conn = DBConnection.GetConnString();
            conn.Open();
            while (reader.Read())
            {
                
                object o = reader["Id"];
                int _id = int.Parse(o.ToString());
                string newSentence = reader["NewSentence"].ToString().Trim();
                if (string.IsNullOrEmpty(newSentence))
                    continue;
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@LiteralId", _id);
                cmd.Parameters.AddWithValue("@NewSentence", newSentence);
                int a = cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            conn.Close();

           
            objConn.Close();


        }
        catch (Exception ex)
        {
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "alert('Faild');", true);
            return;
        }
        finally
        {
            if (objConn.State != ConnectionState.Closed)
                objConn.Close();
        }
    }
        
}
