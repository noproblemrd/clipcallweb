﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class admin_InsertNewSite : PageAdmin
{
    const string DEFAULT_LANGUAGE = "English";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadLanguages();
        }
    }

    private void LoadLanguages()
    {        
        string command = "EXEC dbo.GetLanguages";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            string _lang = (string)reader["LanguageName"];
            string _id = "" + (int)reader["Id"];
            ListItem li = new ListItem(_lang, _id);
            li.Selected = (_lang == DEFAULT_LANGUAGE);
            ddl_languages.Items.Add(li);
        }
        conn.Close();
    }
    protected void btn_set_Click(object sender, EventArgs e)
    {
        string site_id = txt_siteId.Text.Trim();
        string _url = txt_URL.Text.Trim();
        string _reference = txt_webReference.Text.Trim();
        Guid _originId;
        try
        {
            _originId = new Guid(txt_OrigionId.Text.Trim());
        }
        catch (Exception exc)
        {
            lbl_comment.Text = "OriginId not in correct format!";
            return;
        }
        int languageId = int.Parse(ddl_languages.SelectedValue);
        string command = "EXEC dbo._InsertSite @SiteNameId, @URL, @Reference, @OriginId, @LanguageId";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@SiteNameId", site_id);
        cmd.Parameters.AddWithValue("@URL", _url);
        cmd.Parameters.AddWithValue("@Reference", _reference);
        cmd.Parameters.AddWithValue("@OriginId", _originId);
        cmd.Parameters.AddWithValue("@LanguageId", languageId);
        string result = (string)cmd.ExecuteScalar();
        conn.Close();
        if (result == "EXISTS SITE")
            lbl_comment.Text = "SiteNameId already exists";
        else if (result == "EXISTS URL")
            lbl_comment.Text = "URL already exists";
        else
        {
            lbl_comment.Text = "The site '" + site_id + "' insert succesfully";
            txt_siteId.Text = "";
            txt_URL.Text = "";
            txt_webReference.Text = "";
            txt_OrigionId.Text = "";
        }
        
    }
}
