﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_LoadGoogleAnalytics : PageAdmin
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_Click(object sender, EventArgs e)
    {
        string SiteNameId = txt_site.Text.Trim();
        if(string.IsNullOrEmpty(SiteNameId))
            return;
        string google = txt_google.Text;
        string command = "EXEC dbo._SetGoogleAnalytics @SiteNameId, @Google";
       SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
            
        cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
        if(!string.IsNullOrEmpty(google))
            cmd.Parameters.AddWithValue("@Google", google);
        else
            cmd.Parameters.AddWithValue("@Google", DBNull.Value);
        int a = cmd.ExecuteNonQuery();
            
        conn.Close();
        
		
    }
}
