﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class admin_LogInAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        string command = "SELECT dbo._CheckAdminLogin(@UserName, @Password)";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@UserName", txt_user.Text.Trim());
        cmd.Parameters.AddWithValue("@Password", txt_password.Text.Trim());
        bool IsAdmin = (bool)cmd.ExecuteScalar();
        conn.Close();
        if (!IsAdmin)
            lbl_comment.Text = "Login faild!";
        else
        {
            Session["IsAdmin"] = true;
            Response.Redirect("AdminMenu.aspx");
        }
    }
}
