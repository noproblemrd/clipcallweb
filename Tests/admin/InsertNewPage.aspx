﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InsertNewPage.aspx.cs" Inherits="admin_InsertNewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>  
        
        <table>
        <tr>
        <td>
            Page name:
        </td>
        <td>
            <asp:TextBox ID="txt_pageName" runat="server"></asp:TextBox>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pageName"
             ErrorMessage="RequiredFieldValidator" ValidationGroup="NewPage"></asp:RequiredFieldValidator>
        </td>
        </tr>
        
        <tr>
        <td>
            Page description:
        </td>
        <td>
            <asp:TextBox ID="txt_description" runat="server"></asp:TextBox>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_description"
             ErrorMessage="RequiredFieldValidator" ValidationGroup="NewPage"></asp:RequiredFieldValidator>
        </td>
        </tr>
        
        <tr>
        <td>
            Page has audit:
        </td>
        <td>
            <asp:CheckBox ID="cb_audit" runat="server"  />
        </td>
        </tr>
        
        <tr>
        <td>
            Page type:
        </td>
        <td>
            <asp:DropDownList ID="ddl_type" runat="server" AutoPostBack="true" 
                onselectedindexchanged="ddl_type_SelectedIndexChanged">
            </asp:DropDownList>
            
        </td>
        </tr>
        
        <tr runat="server" id="tr_reference">
        <td>
            Enum name:
        </td>
        <td>
            <asp:TextBox ID="txt_enumType" runat="server"></asp:TextBox>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_enumType"
             ErrorMessage="RequiredFieldValidator" ValidationGroup="NewPage"></asp:RequiredFieldValidator>
        </td>
        </tr>
        
        <tr>
        <td colspan="2">
            <asp:Label ID="lbl_comment" runat="server" ForeColor="Red"></asp:Label>
        </td>
        </tr>
        
        <tr>
        <td colspan="2">
            <asp:Button ID="btn_submit" runat="server" Text="Submit" 
                ValidationGroup="NewPage" onclick="btn_submit_Click" />
        </td>
        </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
