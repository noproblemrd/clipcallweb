﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_url : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Type myType = Request.Url.GetType();
        IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
        string str = "";
        foreach (PropertyInfo prop in props)
        {
            str += prop.Name + "=";
            str += prop.GetValue(Request.Url, null).ToString();
            str += "</br>";

            // Do something with propValue
        }
        lbl.Text = str;
        lbl2.Text = HttpRuntime.AppDomainAppVirtualPath;
    }
}