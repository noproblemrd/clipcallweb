﻿(function () {
    function GetOrigin(_src) {
        var _start = _src.indexOf('originid=') + 'originid='.length;
        var _sub = _src.substring(_start, _src.length - 1)
        var _end = _sub.indexOf('&');
        if (_end > -1) {
            _sub = _sub.substring(0, _end - 1);
        }
        return _sub;
    }
    var _scripts = document.getElementsByTagName('script');
    var _origins = [];
    for (var i = 0; i < _scripts.length; i++) {
        if (_scripts[i].src.indexOf('cache.ashx') > -1) {
            var _origin = GetOrigin(_scripts[i].src);
            _origins.push(_origin);
        }
    }
    var main_origin;
    for (var i = 0; i < _scripts.length; i++) {
        if (_scripts[i].src.indexOf('js_cache.js') > -1) {
            main_origin = GetOrigin(_scripts[i].src);
            break;
        }
    }
    var newscript = document.createElement('script');
    newscript.type = 'text/javascript';
    newscript.async = true;
    newscript.src = "cahce.ashx?originid=" + main_origin;
    var s = _scripts[0];
    s.parentNode.insertBefore(newscript, s)

})();