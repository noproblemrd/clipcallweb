﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_Default65 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GeoLocation gl = SalesUtility.GetGeoLocation("74.54.87.190");
        string str = GetShowPhoneUK("08008085458");
        int t = 0;
       
    }
    private string GetShowPhoneUK(string phone)
    {
        if (string.IsNullOrEmpty(phone) || (phone.Length != 10 && phone.Length != 11))
            return string.Empty;
        bool Is10 = (phone.Length == 10);
        string result = ((Is10) ? "0" + phone.Substring(0, 3) : string.Empty + phone.Substring(0, 4)) + " ";
        int StartIndex = (Is10) ? 3 : 4;
        result += phone.Substring(StartIndex, 3) + " " + phone.Substring((StartIndex + 3), 4);// +"-" + phone.Substring(StartIndex + 6);
        return result;
    }
}