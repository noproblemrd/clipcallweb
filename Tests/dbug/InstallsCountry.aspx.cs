﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Tests_dbug_InstallsCountry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        string ConnString = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";
        string command = @"SELECT distinct New_IP
                            FROM [dbo].[New_addonactionExtensionBase]
                            WHERE [new_originid] = @OriginId
								and New_IsInstallation = 1";

        Guid OriginId = new Guid("BCB37A8A-37E0-E311-B491-001517D1792A");
        Dictionary<string, int> dic = new Dictionary<string, int>();
        using (SqlConnection conn = new SqlConnection(ConnString))
        {
            using (SqlCommand cmd = new SqlCommand(command, conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@OriginId", OriginId);
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    string ip = reader["New_IP"] == DBNull.Value ? null : (string)reader["New_IP"];
                    GeoLocation gl = SalesUtility.GetGeoLocation(ip);
                    string country = null;
                    country = (gl == null) ? string.Empty : country = gl.Country;
                    if (!dic.ContainsKey(country))
                        dic.Add(country, 0);
                    dic[country] = dic[country] + 1;

                }
                conn.Close();
            }
        }
        GV.DataSource = dic;
        GV.DataBind();

                                     
    }
}