﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Tests_dbug_Default69 : System.Web.UI.Page
{
    Regex reg;
    protected void Page_Load(object sender, EventArgs e)
    {
        reg = new Regex(@"^\d{5}(-\d{4})?$");
         
    }
    protected void  Button1_Click(object sender, EventArgs e)
    {
        string str = TextBox1.Text;
        Label1.Text =  reg.IsMatch(str).ToString();
    }
}