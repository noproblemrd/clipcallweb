﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_UserAgentParser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpBrowserCapabilities result = GetBrowserDetails(@"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0");
        int t = 0;
    }
    private HttpBrowserCapabilities GetBrowserDetails(string userAgent)
    {
        var browserCapabilities = new HttpBrowserCapabilities
           {
               Capabilities = new Hashtable { { string.Empty, userAgent } }
           };

        var capabilitiesFactory = new BrowserCapabilitiesFactory();

        capabilitiesFactory.ConfigureBrowserCapabilities(new NameValueCollection(), browserCapabilities);

        return browserCapabilities;
    }
}