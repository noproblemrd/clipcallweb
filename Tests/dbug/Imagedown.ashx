﻿<%@ WebHandler Language="C#" Class="Imagedown" %>

using System;
using System.Web;

public class Imagedown : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();

        string RecordPath = AppDomain.CurrentDomain.BaseDirectory + @"images\attachment.png";
        byte[] byteArray = System.IO.File.ReadAllBytes(RecordPath);
        context.Response.ContentType = @"image/png";
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=attachment.png");
        context.Response.OutputStream.Write(byteArray, 0, byteArray.Length);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}