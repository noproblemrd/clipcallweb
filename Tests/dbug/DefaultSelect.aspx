﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DefaultSelect.aspx.cs" Inherits="Tests_dbug_DefaultSelect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link rel="Stylesheet" href="../../scripts/select.css" type="text/css" />
<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../../scripts/Select.js"></script>
    <title></title>
    <script type="text/javascript">
        function GetSelect() {
            alert($('#MySelect').val());
        }
    
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <select width="450" id="MySelect">
            <option value="1">1</option>
             <option value="2" selected="selected">2</option>
        </select>
        <br />
        <br />
        <a href="javascript:GetSelect();">show select</a>
    </div>
    </form>
</body>
</html>
