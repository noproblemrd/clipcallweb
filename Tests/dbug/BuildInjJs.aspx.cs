﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;

public partial class Tests_dbug_BuildInjJs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string js = Build(PpcSite.GetCurrent());
        int t = 0;
    }
    public string Build(PpcSite _cache)
    {
        string line;
        StringBuilder InjectionSB = new StringBuilder();
        string InjectionPath = System.AppDomain.CurrentDomain.BaseDirectory + @"npsb\_inj2.0.js";

        if (File.Exists(InjectionPath))
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(InjectionPath);
                while ((line = file.ReadLine()) != null)
                {
                    InjectionSB.AppendLine(line);
                }
            }
            catch (Exception exc)
            {
                dbug_log.ExceptionLog(exc);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(_cache.UrlWebReference);
        WebReferenceSite.ResultOfListOfInjectionData resultInjectionData = null;
        try
        {
            resultInjectionData = _site.GetInjectionData();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return "";
        }
        if (resultInjectionData.Type == WebReferenceSite.eResultType.Failure)
            return "";
        WebReferenceSite.ResultOfListOfInjectionCampaign resultInstallationCampaign = null;
        try
        {
            resultInstallationCampaign = _site.GetInjectionCampaign();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return "";
        }
        if (resultInstallationCampaign.Type == WebReferenceSite.eResultType.Failure)
            return "";
        StringBuilder InjectioCollectionSB = new StringBuilder();
        StringBuilder CampaignInjectionSB = new StringBuilder();
        Dictionary<Guid, int> IndexCampaign = new Dictionary<Guid, int>();
        InjectioCollectionSB.Append("[");
        //[{Name: 'name', FuncUrl: function(){ return url; }, FuncBefore: function() {}}]
        for (int i = 0; i < resultInjectionData.Value.Length; i++)
        {
            IndexCampaign.Add(resultInjectionData.Value[i].InjectionId, i);
            string ScriptBefore;
            if (string.IsNullOrEmpty(resultInjectionData.Value[i].ScriptBefore))
                ScriptBefore = "null";
            else
                ScriptBefore = "function() {" + resultInjectionData.Value[i].ScriptBefore + "}";
            string ScriptUrl = "function() {" + resultInjectionData.Value[i].ScriptUrl + "}";
            InjectioCollectionSB.Append("{Name: '" + resultInjectionData.Value[i].Name + "',FuncUrl: " + ScriptUrl + ",FuncBefore: " + ScriptBefore + "},");
        }
        InjectioCollectionSB.Remove(InjectioCollectionSB.Length - 1, 1);
        InjectioCollectionSB.Append("]");
        //[{Origin: OriginId, [InjIndex, ...]}]
        CampaignInjectionSB.Append("[");
        Dictionary<Guid, List<int>> Campaigns = new Dictionary<Guid, List<int>>();
        foreach (WebReferenceSite.InjectionCampaign ic in resultInstallationCampaign.Value)
        {
            if (!IndexCampaign.ContainsKey(ic.InjectionId))
                continue;
            if (!Campaigns.ContainsKey(ic.OriginId))
                Campaigns.Add(ic.OriginId, new List<int>());
            Campaigns[ic.OriginId].Add(IndexCampaign[ic.InjectionId]);
        }
        foreach (KeyValuePair<Guid, List<int>> kvp in Campaigns)
        {
            CampaignInjectionSB.Append("{Origin: '" + kvp.Key.ToString() + "', InjIndex: [");
            foreach (int num in kvp.Value)
                CampaignInjectionSB.Append(num + ",");
            CampaignInjectionSB.Remove(CampaignInjectionSB.Length - 1, 1);
            CampaignInjectionSB.Append("]},");
        }
        CampaignInjectionSB.Remove(CampaignInjectionSB.Length - 1, 1);
        CampaignInjectionSB.Append("]");

        InjectionSB.Replace("{InjectionCollection}", InjectioCollectionSB.ToString());
        InjectionSB.Replace("{CampaignInjection}", CampaignInjectionSB.ToString());
        return InjectionSB.ToString();
    }
}