﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_player : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.DataBind();
    }
    protected string _href
    {
        get { return ResolveUrl("~/Management/AudioChrome.aspx?audio=" + HttpUtility.UrlEncode(@"http://gallery.ppcnp.com/Gallery/records/38bad1a46802b88405e2f08c12dea816.mp3")); }
    }
}