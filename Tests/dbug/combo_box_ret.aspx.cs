﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Telerik.Web.UI;

public partial class Tests_dbug_combo_box_ret : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadUsers(); 
        }
        Header.DataBind();
    }

    private void LoadUsers()
    {
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, "1");
        WebReferenceCustomer.ResultOfSearchConsumersResponse result = null;
        WebReferenceCustomer.SearchConsumersRequest request = new WebReferenceCustomer.SearchConsumersRequest();
        request.SearchParameter = string.Empty;
        try
        {
            result = _customer.SearchConsumers(request);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "alert('exc');", true);
            return;
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "alert('faild');", true);
            return;
        }

        DataTable data = new DataTable();
        data.Columns.Add("ContactId", typeof(string));
        data.Columns.Add("Email", typeof(string));
        data.Columns.Add("MobilePhone", typeof(string));
        data.Columns.Add("FirstName", typeof(string));
        //    data.Columns.Add("LastName");
        //     data.Columns.Add("NumberOfRequests");
        ////     data.Columns.Add("Number");
        //    data.Columns.Add("OnClientClick");

        foreach (WebReferenceCustomer.ConsumerMinimalData _counsumer in result.Value.Consumers)
        {
            DataRow row = data.NewRow();
            //         row["Number"] = string.IsNullOrEmpty(_counsumer.ConsumerNumber) ? @"****" : _counsumer.ConsumerNumber;
            row["ContactId"] = _counsumer.ContactId.ToString();
            row["Email"] = (_counsumer.Email == null) ? string.Empty : _counsumer.Email;
            row["MobilePhone"] = (_counsumer.MobilePhone == null) ? string.Empty : _counsumer.MobilePhone;
            row["FirstName"] = (_counsumer.FirstName == null) ? string.Empty : _counsumer.FirstName;
            //          row["LastName"] = _counsumer.LastName;
            //          row["NumberOfRequests"] = _counsumer.NumberOfRequests;
            /*        row["OnClientClick"] = "javascript:$(document).ready(function(){$.colorbox({href:'ConsumerDetails.aspx?Consumer="
                        + _counsumer.ContactId.ToString() +
                        "',width:'95%', height:'90%', iframe:true});});";
             * */
            //          row["OnClientClick"] = "javascript:openIframe('" + _counsumer.ContactId.ToString() + "');";
            data.Rows.Add(row);
        }
        dataV = data;
    }
    protected void RadComboBoxProduct_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {

        DataTable data = dataV;

        foreach (DataRow dataRow in data.Rows)
        {
            RadComboBoxItem item = new RadComboBoxItem();

            item.Text = (string)dataRow["FirstName"];
            item.Value = dataRow["ContactId"].ToString();

            string email = (string)dataRow["Email"];
            string mobile_phone = (string)dataRow["MobilePhone"];

            item.Attributes.Add("Email", email);
            item.Attributes.Add("MobilePhone", mobile_phone);

        //    item.Value += ":" + unitPrice;

            RadComboBoxProduct.Items.Add(item);

            item.DataBind();
        }
    }
    DataTable dataV
    {
        get { return (Session["data_consumer"] == null) ? new DataTable() : (DataTable)Session["data_consumer"]; }
        set { Session["data_consumer"] = value; }
    }
}
