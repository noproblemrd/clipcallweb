﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_CatchPhoneNumber : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string strRegex = @"\d{1}.{0,3}\d{1}.{0,3}\d{1}.{0,3}\d{1}.{0,3}\d{1}.{0,3}\d{1}.{0,3}\d{1}.{0,3}\d{1}.{0,3}\d{1}.{0,3}\d{1}";
        string strRegex2 = @"\d{1}\D{0,3}\d{1}\D{0,3}\d{1}\D{0,3}\d{1}\D{0,3}\d{1}\D{0,3}\d{1}\D{0,3}\d{1}\D{0,3}\d{1}\D{0,3}\d{1}\D{0,3}\d{1}";
        Regex re = new Regex(strRegex2);
        Label1.Text = re.IsMatch(TextBox1.Text).ToString();
    }
}