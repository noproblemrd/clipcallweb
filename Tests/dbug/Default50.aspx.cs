﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Net;

public partial class Tests_dbug_Default50 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<string> list = new List<string>();
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand("EXEC [LogicDB].[dbo].[GetLogicToolbarSumByDate] @date, @EndDate", conn);
        DateTime dt = new DateTime(2013, 5, 1);
        DateTime end_date = new DateTime(2013, 5, 7);
        cmd.Parameters.AddWithValue("@date", dt);
        cmd.Parameters.AddWithValue("@EndDate", end_date);
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            string str = (string)reader["ToolbarId"];
            string ss = Uri.UnescapeDataString(str);//HttpUtility.UrlDecode(str);
            list.Add(ss);
        }
        conn.Close();
        int t = 0;
    }
}