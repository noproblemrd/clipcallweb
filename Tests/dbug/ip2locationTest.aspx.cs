﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class Tests_dbug_ip2locationTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void btn_ip_Click(object sender, EventArgs e)
    {
        string IP;
        if (string.IsNullOrEmpty(txt_ip.Text))
        {
            IP = Utilities.GetIP(Request);
            txt_ip.Text = IP;
            StringBuilder sb = new StringBuilder();
            foreach (string key in Request.ServerVariables) {
                sb.AppendLine(key + "=" + Request.ServerVariables[key] + "<br />");
            }
            lbl_ServerVariables.Text = sb.ToString();
        }
        else
            IP = txt_ip.Text;
        GeoLocation gl = SalesUtility.GetGeoLocation(IP);
        lbl_country.Text = gl.Country;
        lbl_zipcode.Text = gl.ZipCode;
        lbl_state.Text = gl.State;
    }
}