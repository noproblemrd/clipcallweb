﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Permissions;
using System.Web.Configuration;
using System.Security.Principal;

public partial class Tests_dbug_AllowSecurity : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();
        string[] _roles = new string[] { "baby" };
        string authenticationType = windowsIdentity.AuthenticationType;
        string userName = "yoyo";//windowsIdentity.Name;
        GenericIdentity genericIdentity =
            new GenericIdentity(userName, authenticationType);
        HttpContext.Current.User =
            new GenericPrincipal(genericIdentity, _roles);
        
        Header.DataBind();
    }
    protected string GetURL
    {
        get { return "secur/HTMLPage1.htm"; }
    }
}
