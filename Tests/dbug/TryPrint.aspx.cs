﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class dbug_TryPrint : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
   
        
        if (Session["data_print"] == null || Session["data_print"].GetType() != typeof(DataTable))
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "closeWin", "window.close()", true);
            return;
        }
        
 //       GridView _grid = (GridView)Page.PreviousPage.FindControl(ctrlId);
   //     _GridView.DataSource = (DataTable)Session["data_print"];
   //     _GridView.DataBind();
        GridView _grid = (GridView)Session["grid_print"];
        DataTable data = (DataTable)Session["data_print"];
   //     _grid.DataBind();
        PrintHelper.PrintWebControl(_grid, data);
     
        
    }
}
