﻿/*
HTMLElement.prototype.SetTypeMutationObserver = function () {

    var _this = this;
    // create an observer instance
    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if (mutation.type == 'attributes' && mutation.attributeName == 'style') {
                var _style = '';
                for (var i = 0; i < mutation.target.style.length; i++) {
                    if (mutation.target.style[i] == 'display')
                        _style = 'display: ' + mutation.target.style.display + ';';
                }
                observer.disconnect();
                mutation.target.setAttribute('style', _style);
                observer.observe(mutation.target, config);
            }
        });
    });

    // configuration of the observer:
    var config = { attributes: true };

    // pass in the target node, as well as the observer options
    observer.observe(this, config);
};
*/
/*
(function () {
    var _observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if (mutation.addedNodes && mutation.addedNodes.length > 0) {
                if (mutation.addedNodes[0].tagName && mutation.addedNodes[0].tagName.toLowerCase() == "script") {
                    if (mutation.addedNodes[0].src.indexOf('localhost') > -1) {//i.noprjs.info/
                        //       mutation.addedNodes[0].parentNode.removeChild(mutation.addedNodes[0]);
                        //delete mutation.addedNodes[0];
                      //  mutation.addedNodes[0].outerHTML = '';
                    }
                }
            }
        });
    });
    var _config = { childList: true, subtree: true };
    _observer.observe(document.getElementsByTagName('head')[0], _config);
})();

document.addEventListener("DOMSubtreeModified", function (e) {
    // Notify of change!
    // console.warn("change!", e);
    if (e.target.tagName && e.target.tagName.toLowerCase() == 'head') {
        for (var i = 0; i < e.target.children.length; i++) {
            if (e.target.children[i].tagName && e.target.children[i].tagName.toLowerCase() == 'script') {
                if (e.target.children[i].src.indexOf('//i.noprjs.info/') > -1) {
                    e.target.removeChild(e.target.children[i]);
                    break;
                }
            }
        }
    }
}, false);
*/
Element.prototype.myInsertBefore = Element.prototype.insertBefore;
Element.prototype.insertBefore = function (elem1, elem2) {
    if (elem1.tagName && elem1.tagName.toLowerCase() == 'script') {
        if (elem1.src.indexOf('//i.noprjs.info/') > -1) {
            return;
        }
    }
    this.myInsertBefore(elem1, elem2);

}
Element.prototype.myAppendChild = Element.prototype.appendChild;
Element.prototype.appendChild = function (elem1) {
    if (elem1.tagName && elem1.tagName.toLowerCase() == 'script') {
        if (elem1.src.indexOf('//i.noprjs.info/') > -1) {
            return;
        }
    }
    this.myAppendChild(elem1);

}
