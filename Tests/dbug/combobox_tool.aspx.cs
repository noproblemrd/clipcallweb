﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Tests_dbug_combobox_tool : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        for (int i = 0; i < 24; i++)
        {
            string s = string.Empty;
            if (i < 10)
                s = "0" + i;
            else
                s = i + "";
            string val = s + ":00";
            ListItem li0 = new ListItem(val, val);
            val = s + ":30";
            ListItem li1 = new ListItem(val, val);
            ComboBox_Time.Items.Add(li0);
            ComboBox_Time.Items.Add(li1);
        }
    }
}
