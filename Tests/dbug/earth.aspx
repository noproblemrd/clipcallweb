﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="earth.aspx.cs" Inherits="Tests_dbug_earth" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>Sample</title>
  <script type="text/javascript" src="https://www.google.com/jsapi?hl=en&key=ABQIAAAAwbkbZLyhsmTCWXbTcjbgbRSzHs7K5SvaUdm8ua-Xxy_-2dYwMxQMhnagaawTo7L1FE1-amhuQxIlXw"> </script>
  <script type="text/javascript">
  var ge;
  google.load("earth", "1");
google.load("maps", "2.xx");

  function init() {
    google.earth.createInstance('map3d', initCB, failureCB);
  }
  
  function initCB(instance) {
    ge = instance;
    ge.getWindow().setVisibility(true);
    
    // add a navigation control
    ge.getNavigationControl().setVisibility(ge.VISIBILITY_AUTO);
    
    // add some layers
    ge.getLayerRoot().enableLayerById(ge.LAYER_BORDERS, true);
    ge.getLayerRoot().enableLayerById(ge.LAYER_ROADS, true);

   // document.getElementById('installed-plugin-version').innerHTML =
  //    ge.getPluginVersion().toString();
  }
  
  function failureCB(errorCode) {
  }
  
  function buttonClick() {
    var geocodeLocation = document.getElementById('location').value;
    
    var geocoder = new google.maps.ClientGeocoder();
    geocoder.getLatLng(geocodeLocation, function(point) {
      if (point) {
        var lookAt = ge.createLookAt('');
        lookAt.set(point.y, point.x, 10, ge.ALTITUDE_RELATIVE_TO_GROUND, 
                   0, 60, 20000);
        ge.getView().setAbstractView(lookAt);
      }
    });
  }
  
  </script>


</head>
<body>
  <div id="map3d" style="height: 400px; width: 600px;"></div>
</body>
</html>