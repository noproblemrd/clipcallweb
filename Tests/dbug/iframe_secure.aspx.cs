﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_iframe_secure : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DataBind();
    }
    protected string GetPath
    {
        get { return ResolveUrl("~") + @"Publisher/PublisherLogin.aspx"; }
    }
}