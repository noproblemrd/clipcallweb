﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="expreeValidationTest.aspx.cs" Inherits="dbug_expreeValidationTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
    </div>
        <asp:Label ID="lbl" runat="server" Font-Size="Large" ForeColor="Red"></asp:Label>
        <br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator_myPrice" runat="server" 
                ErrorMessage="invalid" ValidationGroup="test" ControlToValidate="TextBox1"
                 ValidationExpression="(^[1-9]\d*$)|(^[1-9]\d*\.\d{1,2}$)|(^[0]\.[1-9]\d?)|(^[0]\.\d[1-9])"></asp:RegularExpressionValidator>
                 <br />
    <asp:Button ID="Button1" runat="server" Text="Button" ValidationGroup="test" 
        onclick="Button1_Click" />
    
    </form>
</body>
</html>
