﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_double_convertor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        double d = 10.0 / 1234567.0;
        string s =string.Format("{0:#.#}", d);
        string[] specifiers = { "C", "E", "e", "F", "G", "N", "P", 
                        "R"};
        List<string> list = new List<string>();
        foreach (string str in specifiers)
        {
            list.Add(d.ToString(str));
        }
        string s2 = String.Format("{0:F20}", d);
        string s3 = d.ToString(".################################################################");
        int t = 0;
    }
}