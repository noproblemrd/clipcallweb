﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Tests_dbug_datetime_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="stylesheet" href="js/jquery.datetimepicker.min.css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.datetimepicker.full.min.js"></script>
    <script type="text/javascript">
        var isPick = false;
        $(function () {
            $('#datetimepicker').datetimepicker({
                step: 30,
                allowBlank: true,
                format: 'm/d/Y H:i',

                /*
                onSelectDate: function () {
                    var d = $('#datetimepicker').datetimepicker('getValue');
                    isPick = true;
                    var i = 0;
                },
                onSelectTime: function () {
                    isPick = true;
                },
                */
                onClose: function () {
            //        if (isPick) {
                    var d = $('#datetimepicker').datetimepicker('getValue');
                    var dd = $('#datetimepicker').val();
                        $('#_span').html(dd);
                        var t = 0;
            //        }
           //         isPick = false;
                    
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <input id="datetimepicker" type="text" value="" />
            <br />
            <br />
            <span id="_span"></span>
        </div>
    </form>
</body>
</html>
