﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_GuidTo64 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Header.DataBind();
    }
    protected void btn_GetBase64_Click(object sender, EventArgs e)
    {
        lbl_Error.Text = string.Empty;
        txt__Base64.Text = string.Empty;
        Guid _id;
        if (!Guid.TryParse(txt_Guid.Text, out _id))
        {
            lbl_Error.Text = "Guid format error!";
            return;
        }
        txt__Base64.Text = "OriginId: new BinData(3, '" + Convert.ToBase64String(_id.ToByteArray()) + "')";

    }
}