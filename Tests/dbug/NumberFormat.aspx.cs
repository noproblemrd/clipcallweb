﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_NumberFormat : System.Web.UI.Page
{
    string numFormat1 = "{0:#,0.##}";
    string numFormat2 = "{0:#,0.#0}";
    protected void Page_Load(object sender, EventArgs e)
    {
        
        decimal num = 12m;
        decimal num2 = 12.3m;
        lbl1.Text = GetNumber(num);
        lbl2.Text = GetNumber(num2);
    }
    string GetNumber(decimal num)
    {
        if((num % 1) == 0)
            return string.Format(numFormat1, num);
        return string.Format(numFormat2, num);
    }
}