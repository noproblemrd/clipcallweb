﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WindowIdentity.aspx.cs" Inherits="Tests_dbug_WindowIdentity" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Main Thread:
        <br />
        <asp:Label ID="lbl_MainThread" runat="server" Text=""></asp:Label>
        <br />
        Main Thread WindowsIdentity:
        <asp:Label ID="lbl_WindowsIdentity" runat="server" Text=""></asp:Label>
        <br />
        <br />
        <br />
        <br />
        ThreadPool:
        <br />
        <asp:Label ID="lbl_ThreadPool" runat="server" Text=""></asp:Label>
        <br />
        ThreadPool WindowsIdentity:
        <br />
        <asp:Label ID="lbl_ThreadPool_WindowsIdentity" runat="server" Text=""></asp:Label>
    
    </div>
    </form>
</body>
</html>
