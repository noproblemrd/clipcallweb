﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class Tests_dbug_WindowIdentity : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        System.Security.Principal.WindowsIdentity _identity = System.Security.Principal.WindowsIdentity.GetCurrent();
   //     System.Security.Principal.GenericIdentity identity = Thread.CurrentPrincipal;
        lbl_MainThread.Text = Thread.CurrentPrincipal.Identity.Name;
        lbl_WindowsIdentity.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        string path = System.AppDomain.CurrentDomain.BaseDirectory + @"App_Data\HeadingInit.xml";
        XDocument xdoc = XDocument.Load(path);
       
        string SiteId = xdoc.Element("Sites").Element("SitePPC").Element("SiteId").Value;
        dbug_log.ExceptionLog(new Exception("Before pool thread: " + SiteId));
        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
       {
           using (System.Security.Principal.WindowsImpersonationContext wi = _identity.Impersonate())
           {
               XDocument xdoc2 = XDocument.Load(path);
               string SiteId2 = xdoc2.Element("Sites").Element("SitePPC").Element("SiteId").Value;
               //    dbug_log.ExceptionLog(new Exception("Start pool thread"));

               //Global.ApplicationIdentity.Impersonate();
         //      lbl_ThreadPool.Text = Thread.CurrentPrincipal.Identity.Name;
         //      lbl_ThreadPool_WindowsIdentity.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
               dbug_log.ExceptionLog(new Exception("End pool thread: " + SiteId2));
           }
       }));
        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
        {
            using (System.Security.Principal.WindowsImpersonationContext wi = PpcSite.GetWindowsImpersonationContext)
            {
                XDocument xdoc2 = XDocument.Load(path);
                string SiteId2 = xdoc2.Element("Sites").Element("SitePPC").Element("SiteId").Value;
                //    dbug_log.ExceptionLog(new Exception("Start pool thread"));
                //         System.Security.Principal.WindowsImpersonationContext wi = _identity.Impersonate();
                //Global.ApplicationIdentity.Impersonate();
                      lbl_ThreadPool.Text = Thread.CurrentPrincipal.Identity.Name;
                        lbl_ThreadPool_WindowsIdentity.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                dbug_log.ExceptionLog(new Exception("End pool thread2: " + SiteId2));
            }
        }));
    }
}