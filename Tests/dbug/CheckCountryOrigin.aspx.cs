﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_CheckCountryOrigin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        PpcSite _cache = PpcSite.GetCurrent();
        Guid OriginId;
        if (!Guid.TryParse(txt_origin.Text, out OriginId))
        {
            lbl_country.Text = "error origin";
            return;
        }
        string country = SalesUtility.GetCountryName(txt_ip.Text, Context.Request.Url.DnsSafeHost);
        lbl_country.Text = country;
        if (!_cache.MonetizationOriginInjection.ContainsKey(OriginId))
        {
            lbl_IsInTheList.Text = "No origin in the list";
            return;
        }
        lbl_IsInTheList.Text = _cache.MonetizationOriginInjection[OriginId].Contains(country).ToString();
    }
}