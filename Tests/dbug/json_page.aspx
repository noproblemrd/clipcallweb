﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="json_page.aspx.cs" Inherits="Tests_dbug_json_page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<script type="text/javascript" src="../../CallService.js"></script>
<script type="text/javascript" src="JScriptJson.js">
</script>
    <title></title>
    <script type="text/javascript">
        var _arr = new Array();
        function CallStam() {
            var url = "json_test.asmx/GetStam";
            CallWebService("", url, function (arg) {
                var func = Function('return ' + arg);
                var jsonObject = func();
                document.getElementById("span_1").innerHTML = jsonObject.name;
                document.getElementById("span_2").innerHTML = jsonObject.age;
               // alert(jsonObject.name);
            },
                function () { });
        }
        function set_stam() {
            var url = "json_test.asmx/SetStam";
            var obj = {
                "name": "ezr'a",
                "age": "2\"5"

            };
            _arr[_arr.length] = obj;
            var obj2 = {
                "name": "ezra2",
                "age": "22"
                };
            _arr[_arr.length] = obj2;
            var _val = JSON.stringify(_arr);
            var params = "str=" + _val;
            CallWebService(params, url, function (arg) {

                alert(arg);
            },
                function () { alert('taa'); });
        }
        function ListObj() {
            var cities = new Array();
            var city = {};
            city.name = "Mumbai";
            city.age = "2000";
            cities[0] = city;
            city = {};
            city.name = "Delhi";
            city.age = "1000";
            cities[1] = city;
            city = {};
            city.name = "Chennai";
            city.age = "3000";
            cities[2] = city;
            var url = "json_test.asmx/SetArrStam";
            var params = "str="+cities;
            CallWebService(params, url, function (arg) {
                alert(arg.length);
            }, function () { alert('taa'); });
        }
    
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <input id="Button1" type="button" value="button" onclick="CallStam();" />
         <input id="Button2" type="button" value="button" onclick="set_stam();" />
         <input id="Button3" type="button" value="button" onclick="ListObj();" />
        <br />
        <span id="span_1"></span>
        <br />
        <span id="span_2"></span>
    </div>
    </form>
</body>
</html>
