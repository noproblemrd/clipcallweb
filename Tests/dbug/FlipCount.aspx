﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlipCount.aspx.cs" Inherits="Tests_dbug_FlipCount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <title></title>
    <script type="text/javascript">
        function Counter() {
            this.nums = [];
            this._divs = [];
            
            this.className = '';
            
        }
        Counter.prototype = {
            constructor: Counter,
            Init: function (num, div_counterId, className) {
                this._div = $('#' + div_counterId);
                this.number = parseInt(num);
                for (var i = 0; i < 7; i++) {
                    var _div = $("<div>");
                    if (className)
                        _div.addClass(className);
                    _div.appendTo(this._div);
                    this._divs.push(_div);
                }
                this.SetNumInDivs(num);

            },
            SetNumInDivs: function (num) {
                for (var i = num.length - 1; i > -1; i--) {
                    this.nums.push(parseInt(num.charAt(i)));
                }
                for (var i = 0; i < this.nums.length; i++) {
                    var _number = (this.nums[i] == 0) ? 9 : this.nums[i] - 1;
                    var _length = this._divs.length - 1;
                    $(this._divs[_length - i]).css('backgroundPosition', (_number * -21) + 'px 0');
                }

            },
            SetNum: function () {
                var duration = Math.floor(Math.random() * 5) * 500;
                var _add = Math.floor(Math.random() * 5);
                this.SetNumInDivs((this.number + _add).toString());
            }
        };

        $(function () {
            var _counter = new Counter();
            _counter.Init('101', '_div', 'div_counter');
        });
    </script>
    <style type="text/css" >
    .div_counter
    {
         width:21px;
        height:27px;
        background:url('FlipCount.png') no-repeat scroll 0 0 transparent;
        background-position: -189px 0;
        float: left;
    }
    
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="_div">
       

    </div>
    </form>
</body>
</html>
