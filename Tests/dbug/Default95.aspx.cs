﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_Default95 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string PAYMENT_FORMAT = "{0:#,0.##}";
        decimal num = new decimal(4.22);
        Label1.Text = string.Format(PAYMENT_FORMAT, num);
    }
}