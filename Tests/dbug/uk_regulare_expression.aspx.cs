﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Tests_dbug_uk_regulare_expression : System.Web.UI.Page
{
    public string POSTAL_CODE;
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
         X1 1XX
X11 1XX
X1X 1XX
XX1 1XX
XX11 1XX
XX1X 1XX
         * */
    //    string _zipcodeReg = @"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) ?[0-9][A-Z-[CIKMOV]]{2})";
      //  string _zipcodeReg = @"^[A-Z]{1}((\d{2})|([A-Z]{2})|([A-Z]{1}\d{1})|(\d{1}[A-Z]{1})) ?\d[A-Z]{2}$";
        string _zipcodeReg = @"^[A-Z]{1}((\d{1})|([A-Z0-9]{2})|([A-Z]{1}[A-Z0-9]{2}))([ -]{1}\d{1}[A-Z]{2})?$";

        POSTAL_CODE = Utilities.RegularExpressionForJavascript(_zipcodeReg);
        Regex reg = new Regex(_zipcodeReg, RegexOptions.IgnoreCase);
        string postalcode = "WC1H";//"W1U 3HS";
        bool is_match = reg.IsMatch(postalcode);
        int t = 0;
        Header.DataBind();
    }
}