﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Tests_dbug_ControlNameConversion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string command = @"INSERT INTO dbo._ControlNameConversion 
                                (Id, ControlName)
                        VALUES (@id, @ControlName)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            foreach (eFlavour flv in Enum.GetValues(typeof(eFlavour)))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@id", (int)flv);
                    cmd.Parameters.AddWithValue("@ControlName", flv.ToString());
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
            conn.Close();
        }
        lbl.Text = "DONE!";
    }
}