﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_Default92 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string phone = "+972543388921";
        string s = GetCleanPhoneFromTwillio(phone);
       
        string source = @"
(function () {
    window['__jBoostRan'] = true;
    window['__np__vbr'] = {
        'partner': { 'id': '2001', 'subid': '1', 'url': 'http://partnerurl.com', 'name': 'Pastaleads' },
        'inimage': { 'active': true, 'auto': true, 'hover': true },
        'coupons': { 'active': true },
        'nt': { 'active': true, 'nti': '10m' },
        'gallery': { 'active': true, 'rt': true, 'rtd': 10, 'rsi': '10m', 'maxSize': 0.6, 'position': 'top' }
    };
    window['__boostConfig'] = window['__np__vbr'];
        
    var _element = document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0];
    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            for (var i = 0; i < mutation.addedNodes.length; i++) {
                if (typeof mutation.addedNodes[i].tagName == 'string' && mutation.addedNodes[i].tagName.toLowerCase() == 'script') {
                    if (typeof window['__boostConfig'] != 'object' || typeof window['__boostConfig']['partner'] != 'object' ||
                        !window['__boostConfig']['partner']['id'] || window['__boostConfig']['partner']['id'] != '2001') {
                        window['__boostConfig'] = window['__np__vbr'];
                        return;
                    }
                }
            }
        });
    });

    var config = { attributes: true, childList: true, subtree: true };

    observer.observe(_element, config);

    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = '//static.boostsaves.com/boost/boost.min.js';    
    _element.appendChild(ga);
   
    
})();";
        string str = new JavaScriptSerializer().Serialize(source);
        int t = 0;
    }
    private static string GetCleanPhoneFromTwillio(string phone)
    {
        if (string.IsNullOrEmpty(phone))
            return null;
        //+17076907398
        if (phone.Length > 10)
            phone = phone.Substring(phone.Length - 10, 10);
        string result = string.Empty;
        foreach (char c in phone)
        {
            int i = (int)c;
            if (i > 47 && i < 58)
                result += c;
        }


        return result;
    }
}