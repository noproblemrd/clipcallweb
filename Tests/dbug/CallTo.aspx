﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CallTo.aspx.cs" Inherits="dbug_CallTo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link type="text/css" rel="Stylesheet" href="../yellowPages/tooltip-general.css" />
<link type="text/css" rel="Stylesheet" href="../yellowPages/style.css" />
<link type="text/css" rel="Stylesheet" href="../yellowPages/clientStyle.css" />
<script type="text/javascript" src="../general.js"></script>

    <title>Untitled Page</title>
    <script type="text/javascript">
    function smsClick(elem)
    {
    
        var div_parent = getParentByClassName(elem, "sms-tooltip");
       
        var _values = getElementsByClass("_hidden", div_parent, "input")[0];
        alert(_values.value);
    }
    
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div >
    <div class='sms-tooltip' style="text-align:center; display:block;" >
        <div class='sms' >
            <a href='javascript:void(0);' class='closeButton' onclick="CloseSms(this)">
                <img width='13px' height='13px' src='../yellowPages/images/tooltip/btn-x2.png' alt='' />
            </a>
            Your Phone number <input type='text' class='_sms_phone' id='Text1' />
            <div class='form-field'>
                Your message:
                <br />
                <input type='text' class='_sms_message' style="width:200px; height:50px;"/>
                
            </div>
        </div>
        <div  >
            <input type='button' class='form-submit' value='Call' onclick="smsClick(this);" />
        </div>
        <input class="_hidden" type="hidden" value="444" />
    </div>
        
    </div>
    </form>
</body>
</html>
