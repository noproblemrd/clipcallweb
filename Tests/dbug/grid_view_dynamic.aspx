﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="grid_view_dynamic.aspx.cs" Inherits="Tests_dbug_grid_view_dynamic" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="_GridView" runat="server" 
            onrowdatabound="_GridView_RowDataBound">
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
