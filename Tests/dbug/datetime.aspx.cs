﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class dbug_datetime : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TimeSpan ts = DateTime.Now - new DateTime(2018, 3, 16, 21, 0, 0);

        double r = ts.TotalHours;

        Response.Write(DateTime.MinValue.ToLongDateString()+"<br/>");
        Response.Write(DateTime.MinValue.ToLongTimeString() + "<br/>");


    }
}
