﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_reg_express_array : System.Web.UI.Page
{
    string REG;
    string[] wordsToBlock = { "remove", "stop", "fuck", "scam", "not", "dont", "suck", "off", "report" };
    protected void Page_Load(object sender, EventArgs e)
    {
        foreach(string str in wordsToBlock)
        {
            REG += str + "|";
        }
        REG = REG.Substring(0, REG.Length - 1);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Regex regex = new Regex(REG, RegexOptions.IgnoreCase);
        lbl_result.Text = regex.IsMatch(null).ToString();
    }
}