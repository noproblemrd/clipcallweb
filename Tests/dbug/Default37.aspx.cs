﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_Default37 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Header.DataBind();
    }
    protected string GetDes
    {
        get { return ResolveUrl("~/main.aspx?keyword=" + HttpUtility.UrlEncode("movers")); }
    }
}