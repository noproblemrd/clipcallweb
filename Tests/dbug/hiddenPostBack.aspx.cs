﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_hiddenPostBack : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(Button1);
        ScriptManager1.RegisterAsyncPostBackControl(Button2);
        if (!IsPostBack)
            HiddenField1.Value = false.ToString();
        else
        {

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        HiddenField1.Value = true.ToString();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {

    }
}