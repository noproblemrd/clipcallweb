﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FocusVsClick.aspx.cs" Inherits="Tests_dbug_FocusVsClick" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <title></title>
    <script type="text/javascript" >
        $(function () {
            $('#Text1').focus(function () {
                console.log('focus');
                $(this).hide();
            });
            $('#Text1').mousedown(function () {
                console.log('click');
               
            });
            $('#Text1').focus(function () {
                console.log('focus2');
                var _this = this;
                setTimeout(function () {
                    $(_this).show();
                }, 1000);

            });

        });
    
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <input id="Text1" type="text" />
    
    </div>
    </form>
    
</body>
</html>
