﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO;

public partial class Tests_dbug_Chart_Line : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(Button1);
        if (!IsPostBack)
        {
            string path = @"D:\svn\Web\NP_Global\trunk\Tests\dbug\chart.chr";
            SaveGraph(path);
            //      StringBuilder sb = new StringBuilder();
            //      sb.Append(@"<div id=""chartdiv2"" align=""center""></div>");
            //       string script = @"var chart = new FusionCharts('FusionCharts/Charts/Line.swf', 'ChartId2', '650', '400', '0', '0');chart.setDataURL('FusionCharts/ChartsXml/"+filename+"');chart.render('chartdiv2');";
            //     string script = "LoadChart('" + ResolveUrl("~") + "Management/FusionCharts/ChartsXml/"
            //          + filename + "','chartdiv2', '" + ResolveUrl("~") + "Management/FusionCharts/Charts/Line.swf');";
            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSArea.swf";
            string _path = ResolveUrl("~") + @"Tests/dbug/chart.chr";
            string script = @"LoadChart(""" + _path + @""",""chartdiv2"", """ + _chart + @""");";

            //     LiteralControl litcon = new LiteralControl(sb.ToString());
            //      PlaceHolder1.Controls.Add(litcon);
            ClientScript.RegisterStartupScript(this.GetType(), "chart", script, true);
        }

    }
    void SaveGraph(string path)
    {
         
        StringBuilder sb=new StringBuilder();
        sb.Append(@"<graph  caption='Revenue from call'");
        sb.Append(@"subCaption='In Thousands'"); 
        sb.Append(@"numdivlines='9'");
        sb.Append(@"lineThickness='19'
                showValues='0' 
                numVDivLines='22'
                formatNumberScale='1' 
                rotateNames='1' 
                anchorRadius='3' 
                anchorBgAlpha='50'
                showAlternateVGridColor='0'
                anchorAlpha='100' 
                animation='1'
                limitsDecimalPrecision='0' 
                divLineDecimalPrecision='1' 
                bgColor='F0F0F0'>");
        sb.Append(@"<categories>
        <category name='7 Mar' toolText ='7 Mar, 7 Aug, yoav'/>
        <category name='8 Mar'/>
        <category name='9 Mar'/>
        <category name='10 Mar'/>
        <category name='11 Mar'/>
        <category name='12 Mar'/>
        <category name='13 Mar'/>
        <category name='14 Mar'/>
        <category name='15 Mar'/>
        <category name='16 Mar'/>
        <category name='17 Mar'/>
        <category name='18 Mar'/>
        <category name='19 Mar'/>
        <category name='20 Mar'/>
        <category name='21 Mar'/>
        <category name='22 Mar'/>
        <category name='23 Mar'/>
        <category name='24 Mar'/>
        <category name='25 Mar'/>
        <category name='26 Mar'/>
        <category name='27 Mar'/>
        <category name='28 Mar'/>
        <category name='29 Mar'/>
        <category name='30 Mar'/>
</categories>");
        sb.Append(@"
<dataset seriesName='Mean' color='0080C0'  anchorBgColor='0080C0' plotBorderThickness='3'>
        <set value='1036'/>
        <set value='1071'/>

        <set value='885'/>
        <set value='892'/>
        <set value='101'/>
        <set value='116'/>
        <set value='164'/>
        <set value='180'/>
        <set value='192'/>
        <set value='262'/>
        <set value='319'/>
        <set value='489'/>
        <set value='633'/>
        <set value='904'/>
        <set value='1215'/>
        <set value='1358'/>
        <set value='1482'/>
        <set value='1666'/>
        <set value='1811'/>
        <set value='2051'/>
        <set value='2138'/>
        <set value='4209'/>
        <set value='7247'/>
        <set value='101'/>
</dataset>

<dataset seriesName='Objective' color='008040' anchorBorderColor='008040' >
        <set value='9023'/>
        <set value='40'/>
        <set value='62'/>
        <set value='118'/>
        <set value='130'/>
        <set value='139'/>
        <set value='158'/>
        <set value='233'/>
        <set value='297'/>
        <set value='379'/>
        <set value='503'/>
        <set value='687'/>
        <set value='746'/>
        <set value='857'/>
        <set value='973'/>
        <set value='1125'/>
        <set value='1320'/>
        <set value='1518'/>
        <set value='1797'/>
        <set value='1893'/>
        <set value='2010'/>
        <set value='2057'/>
        <set value='2166'/>
        <set value='2197'/>
</dataset>

<dataset seriesName='Past' color='808080' anchorBorderColor='808080' >
        <set value='37'/>
        <set value='45'/>
        <set value='70'/>
        <set value='79'/>
        <set value='168'/>
        <set value='337'/>
        <set value='374'/>
        <set value='431'/>
        <set value='543'/>
        <set value='784'/>
        <set value='1117'/>
        <set value='1415'/>
        <set value='2077'/>
        
</dataset>



</graph>");
        
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) { }
         
      //  return sb.ToString();
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        
        string path = @"D:\svn\Web\NP_Global\trunk\Tests\dbug\chart2.chr";
        StringBuilder sb=new StringBuilder();
        sb.Append(@"<graph  caption='Revenue from call'");
        sb.Append(@"subCaption='In Thousands'"); 
        sb.Append(@"numdivlines='9'");
        sb.Append(@"lineThickness='19'
                showValues='0' 
                numVDivLines='22'
                formatNumberScale='1' 
                rotateNames='1' 
                anchorRadius='3' 
                anchorBgAlpha='50'
                showAlternateVGridColor='0'
                anchorAlpha='100' 
                animation='1'
                limitsDecimalPrecision='0' 
                divLineDecimalPrecision='1' 
                bgColor='F0F0F0'>");
        sb.Append(@"<categories>
        <category name='7 Mar'/>
        <category name='8 Mar'/>
        <category name='9 Mar'/>
        <category name='10 Mar'/>
        <category name='11 Mar'/>
        <category name='12 Mar'/>
        <category name='13 Mar'/>
        <category name='14 Mar'/>
        <category name='15 Mar'/>
        <category name='16 Mar'/>
        <category name='17 Mar'/>
        <category name='18 Mar'/>
        <category name='19 Mar'/>
        <category name='20 Mar'/>
        <category name='21 Mar'/>
        <category name='22 Mar'/>
        <category name='23 Mar'/>
        <category name='24 Mar'/>
        <category name='25 Mar'/>
        <category name='26 Mar'/>
        <category name='27 Mar'/>
        <category name='28 Mar'/>
        <category name='29 Mar'/>
        <category name='30 Mar'/>
</categories>");
        sb.Append(@"
<dataset seriesName='Mean' color='0080C0'  anchorBgColor='0080C0' plotBorderThickness='3'>
        <set value='1036'/>
        <set value='1071'/>

        <set value='885'/>
        <set value='892'/>
        <set value='101'/>
        <set value='116'/>
        <set value='164'/>
        <set value='180'/>
        <set value='192'/>
        <set value='262'/>
        <set value='319'/>
        <set value='489'/>
        <set value='633'/>
        <set value='904'/>
        <set value='1215'/>
        <set value='1358'/>
        <set value='1482'/>
        <set value='1666'/>
        <set value='1811'/>
        <set value='2051'/>
        <set value='2138'/>
        <set value='4209'/>
        <set value='7247'/>
        <set value='101'/>
</dataset>





</graph>");
        
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex) { }
        string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSArea.swf";
        string _path = ResolveUrl("~") + @"Tests/dbug/chart2.chr";
        string script = @"LoadChart(""" + _path + @""",""chartdiv2"", """ + _chart + @""");";

        //     LiteralControl litcon = new LiteralControl(sb.ToString());
        //      PlaceHolder1.Controls.Add(litcon);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "chart2", script, true);
        UpdatePanel1.Update();
         
    }
}
