﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tele.aspx.cs" Inherits="dbug_tele" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<script src="../CallService.js" type="text/javascript"></script>
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
     <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <div>
        <asp:DropDownList ID="DropDownList1" runat="server">
        </asp:DropDownList>
    
    </div>
    <telerik:RadFilter ID="RadFilter1" runat="server">
    </telerik:RadFilter>
     <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1">
     <telerik:RadListView ID="RadListView1" DataSourceID="SqlDataSource1" Width="100%"
                    PageSize="6" AllowPaging="True" runat="server" ItemPlaceholderID="ProductsHolder"
                    DataKeyNames="OrderID">
                    
                    <ItemTemplate>
                        <div class="rlvA" style="height: 120px; width: 270px; margin-top: 5px; margin-left: 2px;
                            margin-bottom: 20px; padding-left: 10px; border-style: none">
                            <fieldset style="height: 100%">
                                <table>
                                    <tr>
                                        <td>
                                            <strong>OrderID:</strong>
                                            <asp:Label ID="OrderIDLabel" runat="server" Text='<%#Eval("OrderID") %>' />
                                            <br />
                                            <strong>Order date:</strong>
                                            <asp:Label ID="OrderDateLabel" runat="server" Text='<%# ((DateTime)Eval("OrderDate")).ToShortDateString() %>' />
                                            <br />
                                            <strong>Ship city:</strong>
                                            <asp:Label ID="ShipCityLabel" runat="server" Text='<%#Eval("ShipCity") %>' />
                                            <br />
                                            <strong>Ship country:</strong>
                                            <asp:Label ID="ShipCountryLabel" runat="server" Text='<%# Eval("ShipCountry") %>' />
                                            <br />
                                            <strong>Ship name:</strong>
                                            <asp:Label ID="ShipNameLabel" runat="server" Text='<%#Eval("ShipName") %>' />
                                            <br />
                                        </td>
                                        <td>
                                            <img src="Img/ordertracking.gif" alt="" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </ItemTemplate>
                    
                    <LayoutTemplate>
                        <fieldset>
                            <legend style="margin-left: 20px">Orders</legend>
                            <div class="RadListView RadListViewFloated RadListView_Default">
                                <div class="rlvFloated">
                                    <div id="ProductsHolder" runat="server">
                                    </div>
                                </div>
                                <div style="display: none">
                                    <telerik:RadCalendar ID="rlvSharedCalendar" runat="server" RangeMinDate="<%#new DateTime(1900, 1, 1) %>"
                                        Skin="<%#Container.Skin %>" />
                                </div>
                                <div style="display: none">
                                    <telerik:RadTimeView ID="rlvSharedTimeView" runat="server" Skin="<%# Container.Skin %>" />
                                </div>
                                <div>
                                    <telerik:RadDataPager ID="RadDataPager1" PageSize="6" runat="server">
                                        <Fields>
                                            <telerik:RadDataPagerButtonField FieldType="Numeric" />
                                        </Fields>
                                    </telerik:RadDataPager>
                                </div>
                            </div>
                        </fieldset>
                    </LayoutTemplate>
                </telerik:RadListView>
            </div>
        </telerik:RadAjaxPanel>
    </form>
</body>
</html>
