﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_DynamicOrigin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Guid OriginId;
        if (!Guid.TryParse(txt_OriginId.Text, out OriginId))
            return;
        Header.Title = txt_keyword.Text;
        ClientScript.RegisterStartupScript(this.GetType(), "inject", "inject('" + OriginId.ToString() + "')", true);
    }
}