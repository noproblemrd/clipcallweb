﻿<%@ WebHandler Language="C#" Class="downfile" %>

using System;
using System.Web;

public class downfile : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
       // context.Response.ContentType = "text/plain";
        string path = context.Server.MapPath("~/noproblem.exe");
             System.IO.FileInfo file = new System.IO.FileInfo(path);
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            context.Response.AddHeader("Content-Length", file.Length.ToString());
            context.Response.ContentType = "application/octet-stream";
            context.Response.WriteFile(file.FullName);
            context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}