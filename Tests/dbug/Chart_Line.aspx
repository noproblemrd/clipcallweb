﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Chart_Line.aspx.cs" Inherits="Tests_dbug_Chart_Line" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    
    <link rel="stylesheet" href="../../Management/FusionCharts/Contents/Style.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../../Management/FusionCharts/JSClass/FusionCharts.js"></script>
    
    <script type="text/javascript">
     function LoadChart(fileXML, divId, _chart)
    {
        
//        alert(document.getElementById(divId));
        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
     //   chart.setDataXML(fileXML);
       
        chart.setDataURL(fileXML);
       
        chart.render(divId);
       

    }
    
    </script>


</head>
<body>
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="chartdiv2"></div>
        </ContentTemplate>
        </asp:UpdatePanel>
        <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
        <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
        <asp:Button ID="Button1" runat="server" Text="Button" onclick="Button1_Click" />
        
    </div>
    </form>
    
</body>
</html>
