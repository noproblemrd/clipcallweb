﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_testcookie : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Cookies["_test"].Value = "yes";
        HttpCookie hc = new HttpCookie("_test");
        hc.Value = "yes";
        hc.Expires = DateTime.Now.AddYears(1);
        Response.Cookies.Add(hc);
    }
}