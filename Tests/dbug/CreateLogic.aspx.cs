﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;

public partial class Tests_dbug_CreateLogic : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string path = System.AppDomain.CurrentDomain.BaseDirectory;
        path += "logic.js";
        try
        {
            if (File.Exists(path))
                File.Delete(path);
            using (FileStream fs = File.Create(path))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(PpcSite.GetCurrent().Logic);
                fs.Write(info, 0, info.Length);
            }
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "error", "alert('error');", true);
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "alert('success');", true);
    }
}