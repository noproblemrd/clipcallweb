﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ScriptRemove.aspx.cs" Inherits="Tests_dbug_ScriptRemove" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="ScriptRemove.js"></script>
    <script type="text/javascript">
        function _remove() {
            var scripts = document.getElementsByTagName('script');
            for (var i = 0; i < scripts.length; i++) {
                if(scripts[i].src.indexOf('ScriptRemove.js') > -1)
                    scripts[i].parentElement.removeChild(scripts[i]);
            }
        }
    
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <a href="javascript:_remove();">REMOVE</a>
    </div>
    </form>
</body>
</html>
