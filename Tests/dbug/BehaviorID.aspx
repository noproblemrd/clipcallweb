﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BehaviorID.aspx.cs" Inherits="Tests_dbug_BehaviorID" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ccp" %>

<%@ Register src="WebUserControlDup.ascx" tagname="WebUserControlDup" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../../general.js"></script>
    <script type="text/javascript" language="javascript">
    //    window.onload = _init;
        function _init() {
        /*
            var div2 = document.getElementById("div2");
            var _parent = getElementsByClass("yoav", div2, "input")[0];
            var elem2 = $find(_parent.id);
            */
            //    alert(elem2._id);
            var elem2 = $find("beh");
            for (att in elem2) {
                
                if (att.toLowerCase().indexOf("pop") != -1) {
                    alert("attribute name= " + att);
                    alert("attribute data= " + elem2[att]);
                }
            }
        }
        function bdika(sender, args) {
            var elem2 = $find("beh");
            if (elem2._popupDiv != null) {
                elem2._popupDiv.style.zIndex = "10001";
            }
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ccp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ccp:ToolkitScriptManager>
    
     <asp:Button ID="Button2" runat="server" Text="go" OnClientClick="_init(); return false;" />
     <div id="div1">
    <uc1:WebUserControlDup ID="WebUserControlDup1" runat="server" />
    </div>
    <div id="div2">
    <uc1:WebUserControlDup ID="WebUserControlDup2" runat="server" />
    </div>
    
    
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    <ccp:CalendarExtender ID="CalendarExtender_To" PopupPosition="BottomRight" runat="server"  
        TargetControlID="TextBox1" PopupButtonID="a_toDate" Format="MM/dd/yyyy" 
         OnClientShowing="bdika"
        BehaviorID="beh"></ccp:CalendarExtender>
        </form>
</body>
</html>
