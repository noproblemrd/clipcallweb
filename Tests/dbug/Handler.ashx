﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;

public class Handler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        for (int i = 0; i < 10; i++)
        {
            context.Response.Write(i.ToString());
            context.Response.Flush();
            System.Threading.Thread.Sleep(1000);
        }
    }

    public bool IsReusable
    {
        get {
            return false;
        }
    }

}