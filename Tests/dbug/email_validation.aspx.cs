﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Tests_dbug_email_validation : System.Web.UI.Page
{
    Regex main;
    List<Regex> list;
    protected void Page_Load(object sender, EventArgs e)
    {
        main = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,}([-.]\w+)*$");
        list = new List<Regex>();
        list.Add(new Regex(@"^\w+[-+.']?$"));
        list.Add(new Regex(@"^\w+([-+.']\w+)*[-+.']?$"));
        list.Add(new Regex(@"^\w+([-+.']\w+)*@$"));
        list.Add(new Regex(@"^\w+([-+.']\w+)*@\w+[-.]?$"));
        list.Add(new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*[-.]?$"));
        list.Add(new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.$"));
        list.Add(new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{1,2}$"));
        list.Add(new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,}[-.]?$"));
        list.Add(new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,}([-.]\w+)*[-.]?$"));
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string str = TextBox1.Text;
        if (main.IsMatch(str))
            Label1.Text = "Match";
        else if (chk_byWay(str))
            Label1.Text = "By way";
        else
            Label1.Text = "faild";
    }
    bool chk_byWay(string str)
    {
        foreach (Regex _reg in list)
        {
            if (_reg.IsMatch(str))
                return true;
        }
        return false;
    }
}