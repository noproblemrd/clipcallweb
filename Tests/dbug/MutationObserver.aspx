﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MutationObserver.aspx.cs" Inherits="Tests_dbug_MutationObserver" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
     //   var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

        $("#foo").live("click", function (e) {
            e.preventDefault();
            $(this).append($("<div />").html("new div").attr("id", "bar"));
        });
        $(function () {
            document.addEventListener("DOMNodeInserted",
                function (evt) {
                    console.log("bar was added!");
                    if(evt.target.nodeName == "DIV")
                        console.log("match");

                }
            , false);
        });
        /*
        // define a new observer
        var obs = new MutationObserver(function (mutations, observer) {
        // look through all mutations that just occured
        for (var i = 0; i < mutations.length; ++i) {
        // look through all added nodes of this mutation
        for (var j = 0; j < mutations[i].addedNodes.length; ++j) {
        // was a child added with ID of 'bar'?
        if (mutations[i].addedNodes[j].id == "bar") {
        console.log("bar was added!");
        }
        }
        }
        });
        
        // have the observer observe foo for changes in children
        obs.observe($("#foo").get(0), {
        childList: true
        });
        */
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <a id="foo" href="javascript:void(0);">foo</a>
    </div>
    </form>
</body>
</html>
