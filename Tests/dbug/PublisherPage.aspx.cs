﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Tests_dbug_PublisherPage : PageSetting
{
    protected override void OnPreInit(EventArgs e)
    {

        siteSetting = new SiteSetting("1");
        Session["Site"] = siteSetting;
        userManagement = new UserMangement(Guid.Empty.ToString(), "1", "yoav");
        Session["UserGuid"] = userManagement;
        LinkToSet = new System.Collections.Generic.Dictionary<string, string>();
        LinkToSet.Clear();
        string link_advertiser = DBConnection.GetLinkToSet("a_Advertisers", userManagement.GetSecurityLevel, siteSetting.GetSiteID);
        if (string.IsNullOrEmpty(link_advertiser) || link_advertiser == "#")
            link_advertiser = "PublisherWelcome.aspx";
        LinkToSet.Add("a_Advertisers", link_advertiser);

        LinkToSet.Add("a_AccountSettings",
            DBConnection.GetLinkToSet("a_AccountSettings", userManagement.GetSecurityLevel, siteSetting.GetSiteID));
        LinkToSet.Add("a_Reports",
            DBConnection.GetLinkToSet("a_Reports", userManagement.GetSecurityLevel, siteSetting.GetSiteID));
        LinkToSet.Add("a_consumers",
            DBConnection.GetLinkToSet("a_consumers", userManagement.GetSecurityLevel, siteSetting.GetSiteID));
        SaveLinkToSet();
        base.OnPreInit(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
