﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_ContentSecurityPolicy : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AddHeader("Content-Security-Policy", "script-src 'self' 'unsafe-inline' *.ppcnp.com qa");
        Response.AddHeader("X-WebKit-CSP", "script-src 'self' 'unsafe-inline' *.ppcnp.com qa");
        Response.AddHeader("X-Content-Security-Policy", "script-src 'self' 'unsafe-inline' *.ppcnp.com qa");
    }
}