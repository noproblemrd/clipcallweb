﻿<%@ WebHandler Language="C#" Class="TestCookie" %>

using System;
using System.Web;

public class TestCookie : IHttpHandler, System.Web.SessionState.IRequiresSessionState{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        context.Response.Expires = 0;
        context.Response.CacheControl = "no-cache";
        context.Response.Cache.SetNoStore();
        context.Response.AppendHeader("Pragma", "no-cache");
        context.Response.ContentType = "application/x-javascript";
        context.Response.AddHeader("p3p", "CP=\"IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA\"");
        string _yse = "";
        if (context.Request.Cookies["_test"] == null)
        {
            _yse = "no";
            HttpCookie hc = new HttpCookie("_test");
            hc.Value = "yes";
            hc.Expires = DateTime.Now.AddYears(1);
            context.Response.Cookies.Add(hc);
        }
        else
            _yse = context.Request.Cookies["_test"].Value;
        context.Response.Write("alert('" + _yse + "');");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}