﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DynamicOrigin.aspx.cs" Inherits="Tests_dbug_DynamicOrigin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        INPUT[type="text"]
        {
            width: 300px;
        }
    
    
    </style>
    <title></title>
    <script type="text/javascript">
    
    function inject(origin_id){
	    var s = document.createElement('script');
	    s.type = 'text/javascript';
	    s.async = true;
	    s.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'nps.noproblemppc.com/npsb/logic.js?originid=' + origin_id + '&SiteId=Sales&ToolbarId=';
	    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(s);
	
    }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
    OriginId: <asp:TextBox ID="txt_OriginId" runat="server"></asp:TextBox>
    <br />
    <br />
    Keyword: <asp:TextBox ID="txt_keyword" runat="server"></asp:TextBox>
    <br />
    <br />
        <asp:Button ID="btn" runat="server" Text="GO" />
    </div>
    </form>
    
</body>
</html>
