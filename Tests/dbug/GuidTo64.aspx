﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GuidTo64.aspx.cs" Inherits="Tests_dbug_GuidTo64" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        
        function copyTextToClipboard() {
            var copyFrom = $('<%# txt__Base64.ClientID %>');
          //  copyFrom.text(text);
        //    $('body').append(copyFrom);
            copyFrom.select();
            document.execCommand('copy', true);
      //      copyFrom.remove();

        }
    
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Guid:
        <br />
    <asp:TextBox ID="txt_Guid" runat="server" Width="500"></asp:TextBox>
        <asp:Label ID="lbl_Error" runat="server" Text="" ForeColor="Red"></asp:Label>
        
    <br />
    <br />
        <asp:Button ID="btn_GetBase64" runat="server" Text="Get Base 64" 
            onclick="btn_GetBase64_Click" />
        <br />        
        <br />
       
        <asp:TextBox ID="txt__Base64" runat="server" ReadOnly="true" TextMode="MultiLine" Rows="3" Width="300"></asp:TextBox>
    </div>
    <a onclick="copyTextToClipboard();" href="javascript:void(0);">Copy Text To Clipboard (WORKING ONLY IN IE)</a>
    <br />
    <br />
    <br />
    <br />
    <div>
        MongoDB:
        <br />
        OriginId: new BinData(3, 'utm6T1g4EeOixAAVF9F5Kg==')
    </div>
    </form>
    
</body>
</html>
