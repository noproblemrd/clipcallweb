﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="autocomplete.aspx.cs" Inherits="dbug_autocomplete"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="../../jquery.min.js" type="text/javascript"></script> 
<script src="../../jquery-ui.min.js" type="text/javascript"></script> 
<!--
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js" type="text/javascript"></script> 
-->

    <title>Untitled Page</title>
    <script type="text/javascript">
    $(function() {
    $(".ttr").autocomplete({
        source: function(request, response) {
         //   alert(request.term);
            $.ajax({
                url: "../../Publisher/ReportWizardService.asmx/SuggestList",
                data: "{ 'str': '" + request.term + "', "+
                      " 'classList': 'Advertiser'   }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function(data) { return data; },
                success: function(data) {
                    response($.map(data.d, function(item) {
                        return {
                            value: item
                        }
                    }))
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        },
        minLength: 1
    });
});
	
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="ui-widget">
	<label for="tags">Tags: </label>
	<input id="tags" type="text" class="ttr" />
</div>

    </div>
    </form>
</body>
</html>
