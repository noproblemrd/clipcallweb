﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_encodeBase64 : System.Web.UI.Page
{
    private string _url = "http://www.ask.com";
    protected void Page_Load(object sender, EventArgs e)
    {
        Header.DataBind();
    }
    public string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }



    public string Base64Decode(string base64EncodedData)
    {
        var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    }
    protected string GetUrl
    {
        get 
        {
            return Base64Encode(_url);
        }
    }
}