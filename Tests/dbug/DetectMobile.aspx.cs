﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_DetectMobile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var obj = new { MobileDeviceManufacturer = Request.Browser.MobileDeviceManufacturer,
                        MobileDeviceModel = Request.Browser.MobileDeviceModel,
                        IsMobileDevice = Request.Browser.IsMobileDevice.ToString(),
                        UserAgent = Request.UserAgent
        };

        string str = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(obj);
        _div.InnerText = str;
    }
}