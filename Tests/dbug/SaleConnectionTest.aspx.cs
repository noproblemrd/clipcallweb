﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_SaleConnectionTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        using (SqlConnection connSales = DBConnection.GetSalesConnString())
        {
            string commandSales = @"SELECT o.New_originId
 FROM dbo.New_originBase ob
	INNER JOIN dbo.New_originExtensionBase o on o.New_originId = ob.New_originId
WHERE ob.deletionstatecode = 0
  and ISNULL(o.new_isdistribution, 0) = 0";
            using (SqlCommand cmdSales = new SqlCommand(commandSales, connSales))
            {
                connSales.Open();
                SqlDataReader readerSales = cmdSales.ExecuteReader();
                while (readerSales.Read())
                    Label1.Text += ((Guid)readerSales["New_originId"]).ToString() + ";";
                connSales.Close();
            }
        }
    }
}