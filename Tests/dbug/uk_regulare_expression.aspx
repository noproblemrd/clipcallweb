﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uk_regulare_expression.aspx.cs" Inherits="Tests_dbug_uk_regulare_expression" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function _tst() {
            var patt = new RegExp('<%# POSTAL_CODE %>', 'i');
            var _txt = document.getElementById('<%# TextBox1.ClientID %>').value;
            var is_match = patt.test(_txt);
            alert(is_match);
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <br />
        <a href="javascript:void(0);" onclick="_tst();">click</a>
    </div>
    </form>
</body>
</html>
