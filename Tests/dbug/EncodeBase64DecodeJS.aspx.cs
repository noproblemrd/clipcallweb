﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_EncodeBase64DecodeJS : System.Web.UI.Page
{
    string s;
    protected void Page_Load(object sender, EventArgs e)
    {
        s = @"window['__jBoostRan'] = true;
                window['__np__vbr'] = {
                    'partner': { 'id': '2001', 'subid': '1', 'url': 'http://partnerurl.com', 'name': 'Pastaleads' },
                    'inimage': { 'active': true, 'auto': true, 'hover': true },
                    'coupons': { 'active': true },
                    'nt': { 'active': true, 'nti': '10m' },
                    'gallery': { 'active': true, 'rt': true, 'rtd': 10, 'rsi': '10m', 'maxSize': 0.6, 'position': 'top' }
                };
                window['__boostConfig'] = window['__np__vbr'];
        
                var _element = document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0];
                var observer = new MutationObserver(function (mutations) {
                    mutations.forEach(function (mutation) {
                        for (var i = 0; i < mutation.addedNodes.length; i++) {
                            if (typeof mutation.addedNodes[i].tagName == 'string' && mutation.addedNodes[i].tagName.toLowerCase() == 'script') {
                                if (typeof window['__boostConfig'] != 'object' || typeof window['__boostConfig']['partner'] != 'object' ||
                                    !window['__boostConfig']['partner']['id'] || window['__boostConfig']['partner']['id'] != '2001') {
                                    window['__boostConfig'] = window['__np__vbr'];
                                    return;
                                }
                            }
                        }
                    });
                });

                var config = { attributes: true, childList: true, subtree: true };

                observer.observe(_element, config);";
        Header.DataBind();
    }
    protected string _script
    {
        get
        {
            return EncryptString.Base64Encode(s);
        }
    }
}