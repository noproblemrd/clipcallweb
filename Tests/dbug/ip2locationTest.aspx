﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ip2locationTest.aspx.cs" Inherits="Tests_dbug_ip2locationTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            IP:
        <asp:TextBox ID="txt_ip" runat="server"></asp:TextBox>
        <asp:Button ID="btn_ip" runat="server" Text="Get Geo" OnClick="btn_ip_Click" />
        <br />
        <br />
        Country:
        <asp:Label ID="lbl_country" runat="server" Text=""></asp:Label>
        <br />
         Zipcode:
        <asp:Label ID="lbl_zipcode" runat="server" Text=""></asp:Label>
        <br />
         State:
        <asp:Label ID="lbl_state" runat="server" Text=""></asp:Label>
        <br />
        <asp:Label ID="lbl_ServerVariables" runat="server" Text=""></asp:Label>
    </div>
    </form>
</body>
</html>
