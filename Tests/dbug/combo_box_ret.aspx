﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="combo_box_ret.aspx.cs" Inherits="Tests_dbug_combo_box_ret" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script src="../../jquery.min.js" type="text/javascript"></script> 
<script src="../../jquery-ui.min.js" type="text/javascript"></script> 
    <script type="text/javascript">
    $(function() {
        $("._BadWord").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "telerik_test.asmx/GetEmployees2",
                    data: "{ 'context': '" + request.term + "' }",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function(data) { return data; },
                    success: function(data) {
                        response($.map(data.d, function(item) {
                            return {
                                value: ("<table><tr><td>"+item.FirstName+"</td><td>"+
                                        item.MobilePhone+"</td></tr></table>")
                            }
                        }))
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            },
            minLength: 1
        });
    });
    function onClientDataBound(sender, e) 
    {        /*
        var item = e.get_item();
     //   var context = e.get_context();
        var dataItem = e.get_dataItem();
        //setting the imageUrl property of the item based on its attribute
        var MobilePhone = item.get_attributes().getAttribute("MobilePhone");
        var email = item.get_attributes().getAttribute("Email");
     //   alert("mob= "+MobilePhone+" email= "+email);
    //    alert(dataItem.Email);
        item.get_attributes().setAttribute("MobilePhone", dataItem.MobilePhone);
        item.get_attributes().setAttribute("Email", dataItem.Email);
        item.set_value(dataItem.ContactId);
        item.set_text(dataItem.FirstName);
  //      context["MobilePhone"] = MobilePhone;  
  */  
    }  
    function onItemDataBound(sender, eventArgs)
    {
        //the combo item
        var item = eventArgs.get_item();
        //the data item of type Employee
        var dataItem = eventArgs.get_dataItem();
        //this will be used when selecting an item - its text attribute will go to the input area
        item.get_attributes().setAttribute("text", dataItem.FirstName);
        item.get_attributes().setAttribute("MobilePhone", dataItem.MobilePhone);
        item.get_attributes().setAttribute("Email", dataItem.Email);
        
        item.set_value(dataItem.EmployeeID);
        alert('1');
        //this is a custom property used in the template to alternate the css styles
        dataItem.Index = item.get_index();
alert('2');
        var template = new Sys.UI.Template($get("myTemplate"));
        alert('3');
        template.instantiateIn(item.get_element(), dataItem);
        alert('4');
    }

    function onSelectedIndexChanged(sender, eventArgs)
    {
        var item = eventArgs.get_item();
        sender.set_text(item.get_attributes().getAttribute("text"));
        sender.set_value(item.get_value());
    }
    
    function On_Call_Blur(sender, eventArgs)
    {
        var combo = document.getElementById("<%# RadComboBox1.ClientID %>");
        for(att in combo)
        {
            if(att.toLowerCase().indexOf("en") > -1)
	            alert("attribute name= "+att+"\r\n"+"attribute data= "+combo[att]);
        }
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <div>
    <div>
        <asp:Label ID="Label1" runat="server" AssociatedControlID="RadComboBoxProduct">Product:</asp:Label>
                        <telerik:RadComboBox ID="RadComboBoxProduct" runat="server" Height="200px" Width="200px"
                            DropDownWidth="298px" EmptyMessage="Choose a Product" HighlightTemplatedItems="true"
                            EnableLoadOnDemand="true" Filter="StartsWith" OnItemsRequested="RadComboBoxProduct_ItemsRequested">
                            <HeaderTemplate>
                                <table style="width: 275px" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="width: 177px;">
                                            Name</td>
                                        <td style="width: 60px;">
                                            Mobile Phone</td>
                                        <td style="width: 40px;">
                                            Email</td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table style="width: 275px" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="width: 177px;">
                                            <%# DataBinder.Eval(Container, "Text")%>
                                        </td>
                                        <td style="width: 60px;">
                                            <%# DataBinder.Eval(Container, "Attributes['MobilePhone']")%>
                                        </td>
                                        <td style="width: 40px;">
                                            <%# DataBinder.Eval(Container, "Attributes['Email']")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </telerik:RadComboBox>
<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="RadComboBoxProduct"
ErrorMessage="!" CssClass="validator">
                        </asp:RequiredFieldValidator>
    </div>
    <div>
        <asp:Label ID="Label2" runat="server" AssociatedControlID="RadComboBox1">Product:</asp:Label>
                        <telerik:RadComboBox ID="RadComboBox1" runat="server" Height="200px" Width="200px"
                            DropDownWidth="298px" EmptyMessage="Choose a Product" HighlightTemplatedItems="true"
                            EnableLoadOnDemand="true" Filter="StartsWith" onclientitemdatabound="onClientDataBound" 
                              OnClientBlur="On_Call_Blur">
                            <WebServiceSettings Path="telerik_test.asmx" Method="GetItems" />
                            
                        </telerik:RadComboBox>
<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="RadComboBox1"
ErrorMessage="!" CssClass="validator">
                        </asp:RequiredFieldValidator>
    </div>
    <div>
    <telerik:RadComboBox runat="server" ID="RadComboBox2" 
    Width="300px" 
    Skin="Vista" 
    EnableLoadOnDemand="true"
    OnClientItemDataBound="onItemDataBound"
    OnClientSelectedIndexChanged="onSelectedIndexChanged">
  <WebServiceSettings Method="GetEmployees" Path="telerik_test.asmx" />
  
                        
</telerik:RadComboBox>
    <div id="myTemplate" class="sys-template">
    <!--* if (Index % 2 == 0) { *-->
    <table class="employeeTableAlt">
        <tr>
            <td class="employeeFirstName">
                {{ FirstName }}
            </td>
            <td class="employeeLastName">
                {{ MobilePhone }}
            </td>
            <td>
                {{ Email }}
            </td>
        </tr>
    </table>
    
    </div>
    <div>
        
    
    
    </div>
        
    <asp:TextBox ID="TextBox1" runat="server" CssClass="_BadWord"></asp:TextBox>
    </div>
    </div>
    <a><span><tr><td>hhh</td><td>tgergt</td></tr></span></a>
    </form>
</body>
</html>
