﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_block_injection : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AppendHeader("Content-Security-Policy", "default-src 'none'; script-src 'self'; connect-src: 'self'; img-src: 'self'; style-src: 'self';");
        Response.AppendHeader("X-Frame-Options", "deny");
        Response.AppendHeader("X-Content-Type-Options", "nosniff");
        Response.AppendHeader("X-XSS-Protection", "0");
    }
}