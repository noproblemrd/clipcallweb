<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sleep.aspx.cs" Inherits="dbug_sleep" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">
    var _lbl;
    var _event;
    var flag;
    function sleep(e, milliSeconds){
        _event=e;
        milliSeconds = milliSeconds*1000;
        var lbl = document.getElementById("<%= Label1.ClientID %>");
        lbl.innerHTML="start";
        var startTime = new Date().getTime(); // get the current time

        while (new Date().getTime() < startTime + milliSeconds);
         lbl.innerHTML="out";
        }
    function detectRunning()
    {
        var lbl = document.getElementById("<%= Label2.ClientID %>");
        alert(_event);
        if(_event)lbl.innerHTML="yes";
        else lbl.innerHTML="no";
    }
    
    
    function GotoServer(e){ 
  //      _event=e;
            _lbl = document.getElementById("<%= Label1.ClientID %>");
        flag=true;
        _lbl.innerHTML=flag;
        WebServiceSite.SleepTest(OnComplete, OnError, OnTimeOut);
    }
    
    function OnComplete(arg)
    {       
        flag=false;
        _lbl.innerHTML=flag;
        document.getElementById("<%= Label2.ClientID %>").innerHTML=arg;
    }

    
    function OnTimeOut(arg)
    {
	    alert("timeOut has occured");
    }

    function OnError(arg)
    {
	    alert("error has occured: " + arg._message);
    }
    function onf()
    {
        flag=false;
        _lbl.innerHTML=flag;
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    
    
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services >
            <asp:ServiceReference Path="../WebServiceSite.asmx" />
        </Services>
        </asp:ScriptManager>
    <div>
        <asp:TextBox ID="TextBox1" runat="server" onblur="javascript:GotoServer(event);"
        onfocus="javascript:onf();"></asp:TextBox>
          <br />
        <asp:Button ID="Button1" runat="server" Text="Button" OnClientClick="detectRunning();return false;" />
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        
        
    </div>
    </form>
</body>
</html>
