﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_Default45 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string PageUrl = "http://" + Request.Url.Host + ((Request.Url.Port == 80) ? string.Empty : ":" + Request.Url.Port) + ResolveUrl("~");
        Label1.Text = PageUrl;
    }
}