﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Tests_dbug_dattableSet : System.Web.UI.Page
{
    DataTable data;
    protected void Page_Load(object sender, EventArgs e)
    {
        data = new DataTable();
        data.Columns.Add("name", typeof(string));
        data.Columns.Add("count", typeof(int));

        DataRow row1 = data.NewRow();
        row1["name"] = "yoav";
        data.Rows.Add(row1);

        DataRow row2 = data.NewRow();
        row2["name"] = "nir";
        data.Rows.Add(row2);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        IEnumerable<DataRow> _query = from x in data.AsEnumerable()
                                      where x.Field<string>("name") == "nir"
                                      select x;

        _query.FirstOrDefault()["count"] = 3;
        GridView1.DataSource = data;
        GridView1.DataBind();
    }
}