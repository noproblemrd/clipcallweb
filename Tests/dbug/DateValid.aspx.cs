﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Tests_dbug_DateValid : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        DateTime _date = new DateTime(2012, 2, 31);
        Response.Write(_date.ToString());
         * */
        System.Globalization.CultureInfo culture;
        culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
        culture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
        DateTime _date;
        if (!DateTime.TryParse("31/2/2012", culture, DateTimeStyles.None, out _date))
            ClientScript.RegisterStartupScript(this.GetType(), "dd", "alert('oo');", true);
        Response.Write(_date.ToString());
    }
}