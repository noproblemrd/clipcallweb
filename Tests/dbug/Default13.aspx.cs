﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_dbug_Default13 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string str = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        Response.Write("name= " +str + "</br>");
        Response.Write("id= " + Request.Browser.Id + "</br>");
        Response.Write("LongUserName= " + Request.LogonUserIdentity.Name + "</br>");
        Response.Write("LongUserUser= " + Request.LogonUserIdentity.User + "</br>");
        Response.Write("USER IP= " + Utilities.GetIP(Request) + "</br>");
    }
}