﻿<%@ WebHandler Language="C#" Class="cache" %>

using System;
using System.Web;

public class cache : IHttpHandler {
    private const string GZIP = "gzip";
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();

        context.Response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        context.Response.Expires = 0;
        context.Response.CacheControl = "no-cache";
        context.Response.Cache.SetNoStore();
        context.Response.AppendHeader("Pragma", "no-cache");
        
        HttpRequest Request = context.Request;
        HttpResponse Response = context.Response;

        context.Response.ContentType = "application/json; charset=UTF-8;";
        string _etag = string.Empty; ;
     ///   string ModifiedSince = string.Empty;
        try
        {
            _etag = Request.Headers["If-None-Match"];
    //        ModifiedSince = Request.Headers["If-Modified-Since"];
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
   //     DateTime _date_modified;
    //    if (!DateTime.TryParse(ModifiedSince, out _date_modified))
   //         _date_modified = DateTime.MinValue;
        PpcSite _cache = PpcSite.GetCurrent();
        
        bool IsFromToday = _IsFromToday(_etag);
     //   DateTime DateModified = _cache.DateModified;
   //     bool IsDateModified = DateModified.Date == _date_modified.Date && DateModified.Hour == _date_modified.Hour && DateModified.Minute == _date_modified.Minute;
        if (IsFromToday)
        {
            context.Response.SuppressContent = true;
            context.Response.End();
            return;
        }
        DateTime _expire = DateTime.UtcNow.AddHours(1);
       
        string newEtag = (!IsFromToday) ? SetEtag(context) : _etag;
        Response.Cache.SetETag(newEtag);
        //    CompressResponse(context);
        context.Response.Filter = new System.IO.Compression.GZipStream(context.Response.Filter, System.IO.Compression.CompressionMode.Compress);
        context.Response.AppendHeader("Content-encoding", GZIP);
        Response.StatusCode = 200;
        Response.Write(_cache.Logic);       
    }
    public bool _IsFromToday(string _etag)
    {
        if (string.IsNullOrEmpty(_etag))
            return false;
        _etag = _etag.Replace("\"", "");       

        DateTime etagDate;
        if (!DateTime.TryParseExact(_etag, "MMddyyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out etagDate))
        {
            //dbug_log.ExceptionLog(new ExceptionEtag(_etag, ModifiedSince, "TryParseExact", request.QueryString.ToString()));
            return false;
        }
        if (etagDate == DateTime.Today)
            return true;
        return false;
    }
    string SetEtag(HttpContext context)
    {
       
        return "\"" + SalesUtility.GetEtagDateString(DateTime.UtcNow) + "\"";

    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}