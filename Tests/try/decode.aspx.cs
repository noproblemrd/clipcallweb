﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_try_decode : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string resultTransactionInvoiceInfoURL = Server.UrlDecode("https%3a%2f%2fshoppers.plimus.com%2fjsp%2forder_locator_info.jsp%3frefId%3d19E4852935D63477%26acd%3d9F88F3D548B74926");

        string[] arrUrl;
        arrUrl = resultTransactionInvoiceInfoURL.Split(new String[] { "&", "%3d" }, StringSplitOptions.RemoveEmptyEntries);
        int posrefId = arrUrl[0].IndexOf("refId");
        string refId = arrUrl[0].Substring(posrefId + 6);
        if (arrUrl[0].IndexOf("sandbox") == -1)
            refId = "https://www.plimus.com/jsp/show_invoice.jsp?ref=" + refId;
        Response.Write(resultTransactionInvoiceInfoURL);
        Response.Write("<br>");
        Response.Write(refId);
    }
}