﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="radComboBox.aspx.cs" Inherits="Tests_try_telerik_radComboBox" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="//code.jquery.com/jquery-latest.js"></script>

    <style>
        
        .form .RadComboBox_Black  
        {
            font-size:14px;
            font-weight:600;
        }
        
        .form .RadComboBox_Black .rcbInput
        {
            font-size:14px;
            font-weight:600;
        }        
       
        .form .RadComboBoxDropDown_Black
        {
            font-size:14px;
            font-weight:600;
        }   
        
        /*.RadComboBox_Black, .RadComboBox_Black .rcbInput, .RadComboBoxDropDown_Black*/
              
        .form .rcbList .rcbHovered 
        {
            color:#89C5FD;
            font-size:14px;
            font-weight:600;
        } 
        
        .form .RadComboBox_Black .rcbHovered .rcbInputCell .rcbInput 
        {
            color:#89C5FD;
            font-size:14px;
            font-weight:600;
        } 
        
    </style>

    <script>
        function getvalue() {
            var combo = $find('<%#RadComboBox1.ClientID %>');
            alert(combo.get_selectedItem().get_value());
        }  
    </script>
</head>
<body>
    <form id="form1" runat="server" class="form">
    <div class="container">   
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        </telerik:RadScriptManager>
        <telerik:RadComboBox ID="RadComboBox1" runat="server"   Skin="Black" >
            <Items>
                <telerik:RadComboBoxItem Value="5"  Text="plumber"  />
                <telerik:RadComboBoxItem Value="6"  Text="carpentar" />
            </Items>
        </telerik:RadComboBox>

        <input type="button" value="send" onclick="getvalue();"/>
    </div>
    </form>
</body>
</html>
