﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mask.aspx.cs" Inherits="Tests_try_mask" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <asp:TextBox runat="server" ID="tb" ></asp:TextBox>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="tb" MaskType="Number" Mask="999,999,999" ></asp:MaskedEditExtender>

        
<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
    TargetControlID="TextBox1" />

    </div>
    </form>
</body>
</html>
