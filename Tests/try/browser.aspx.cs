﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

public partial class Tests_try_browser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        NameValueCollection nv = (NameValueCollection)Request.ServerVariables;
        string userAgent=nv["HTTP_USER_AGENT"];
        Response.Write("userAgent:" + userAgent + "<br>");
        string hostIp = nv["REMOTE_HOST"]; //Retrieves the remote host IP Address
        Response.Write("hostIp:" + hostIp + "<br>");
        string clientIp=nv["REMOTE_ADDR"]; //Retrieves the user IP Address
        Response.Write("clientIp:" + clientIp + "<br>");
        string acceptAcceptLanguage = nv["HTTP_ACCEPT_LANGUAGE"];
        Response.Write("acceptAcceptLanguage:" + acceptAcceptLanguage + "<br><br>");

        foreach (string str in nv)
        {
            Response.Write(str + ":" + nv[str] + "<br>");
        }

    }
}