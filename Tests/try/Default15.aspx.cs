﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Tests_try_Default15 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      /*                           
      string pattern = @"/(?:http:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g";
      //pattern = "http";
      string replacement = "<iframe width='420' height='345' src='http://www.youtube.com/embed/$1' /> ";
      Regex rgx = new Regex(pattern);
      string result = rgx.Replace("http://www.youtube.com/watch?v=E6yzQlJeBkQ&list=PLD29F85D637C39F7F", "");
        */


        /*
      const string input = "http://www.youtube.com/watch?v=bSiDLCf5u3s " +
                   "https://www.youtube.com/watch?v=bSiDLCf5u3s " +
                   "http://youtu.be/bSiDLCf5u3s " +
                   "www.youtube.com/watch?v=bSiDLCf5u3s " +
                   "youtu.be/bSiDLCf5u3s " +
                   "http://www.youtube.com/watch?feature=player_embedded&v=bSiDLCf5u3s " +
                   "www.youtube.com/watch?feature=player_embedded&v=bSiDLCf5u3s " +
                   "http://www.youtube.com/watch?v=_-QpUDvTdNY";
        */
      const string input = "http://www.youtube.com/watch?feature=player_embedded&v=bSiDLCf5u3s";
      const string pattern = @"(?:https?:\/\/)?(?:www\.)?(?:(?:(?:youtube.com\/watch\?[^?]*v=|youtu.be\/)([\w\-]+))(?:[^\s?]+)?)";
      //const string replacement = "<iframe title='YouTube video player' width='480' height='390' src='http://www.youtube.com/embed/$1' frameborder='0' allowfullscreen='1'></iframe>";
      const string replacement = "http://www.youtube.com/embed/$1";
      var rgx = new Regex(pattern);
      var result = rgx.Replace(input, replacement);


    }
}