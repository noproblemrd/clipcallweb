﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class Tests_try_arraylist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ArrayList arrList = new ArrayList();
        arrList.Add(1);
	    arrList.Add(8);//Added integer value
	    arrList.Add(10);//Added real value
        arrList.Add(15);//Added real value
        arrList.Add(25);//Added real value

        Response.Write(arrList[1]);

        arrList.RemoveAt(1);

        Response.Write("<br>" + arrList[1]);
    }
}