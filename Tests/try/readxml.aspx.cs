﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

public partial class try_readxml : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string str="";
        str+=@"<?xml version=""1.0"" encoding=""UTF-8""?>";

        str+="<response>";
	        str+="<status>OK</status>";
	        str+="<trans>";
		        str+="<id>130129638</id>";
                str+="<pos_id>85290</pos_id>";
		        str+="<session_id>12345661</session_id>";
		        str+="<order_id></order_id>";
		        str+="<amount>1000</amount>";

		        str+="<status>99</status>";
		        str+="<pay_type>t</pay_type>";
		        str+="<pay_gw_name>pt</pay_gw_name>";
		        str+="<desc>Payment description</desc>";
		        str+="<desc2></desc2>";
		        str+="<create>2011-04-11 15:26:48</create>";
		        str+="<init>2011-04-11 15:26:51</init>";
		        str+="<sent>2011-04-11 15:26:51</sent>";
		        str+="<recv>2011-04-11 15:26:51</recv>";
		        str+="<cancel></cancel>";
		        str+="<auth_fraud>1</auth_fraud>";
                str += "<ts>1302528431346</ts>";
		        str+="<sig>a91db7ee51ea485eeaddf78505d63aeb</sig>";
            str+="</trans>";
        str+="</response>";
         

        XmlDocument xd = new XmlDocument();
        xd.LoadXml(str);

        Response.Write(xd["response"]["status"].InnerText);

        XmlNode xnTrans=xd["response"]["trans"];
        foreach(XmlNode xn in xnTrans)
        {
            Response.Write(xn.InnerText);
        }
    }
}
