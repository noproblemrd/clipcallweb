﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="updatepanel.aspx.cs" Inherits="Tests_try_updatepanel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release">    
</cc1:ToolkitScriptManager>

    
        <asp:Button ID="btn2" Text="time" runat="server"/>
        <hr />
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                
                <div>
                <asp:Button ID="btn" Text="time" runat="server" onclick="btn_Click"/>
                </div>
                <asp:DataList ID="datalist1" runat="server">
                    <HeaderTemplate>
                        <asp:LinkButton ID="lb1" runat="server" onclick="lb1_Click" Text="link button"></asp:LinkButton>
                        <asp:LinkButton ID="lbStatus" runat="server"  
                                            OnClick="lbStatus_Click" >Status</asp:LinkButton>
                        <asp:LinkButton ID="link1" runat="server" Text="kkkk2" onclick="link1_Click"></asp:LinkButton>
                         <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="status" CommandName="sort" 
                                            OnCommand="lbStatus_Command" >Status</asp:LinkButton>  
                    </HeaderTemplate>

                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "color")%>
                        <%# DataBinder.Eval(Container.DataItem, "price")%>
                        <hr />
                    </ItemTemplate>

                </asp:DataList>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="UpdatePanelVirtual" runat="server">
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
