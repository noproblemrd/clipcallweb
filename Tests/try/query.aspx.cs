﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class try_query : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string strRequestForm = "";

        foreach (string strForm in Request.QueryString)
        {
            strRequestForm += "&" + strForm + "=" + Server.UrlEncode(Request.QueryString[strForm]);
        }

        Response.Write(strRequestForm);
    }
}
