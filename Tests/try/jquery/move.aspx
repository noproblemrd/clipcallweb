﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="move.aspx.cs" Inherits="Tests_try_jquery_move" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="//code.jquery.com/jquery-latest.js"></script>
     <style>
        div {
          position:absolute;
          background-color:#abc;
          left:50px;
          width:90px;
          height:90px;
          margin:5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <input type="button" id="left" value="&laquo;" />
    <input type="button" id="right" value="&raquo;" />
    
<div class="block"></div>

<script>
    $("#right").click(function () {
        $(".block").animate({ "left": "+=50px" }, "slow");
    });

    $("#left").click(function () {
        $(".block").animate({ "left": "-=50px" }, "slow");
    });

</script>
    </form>
</body>
</html>
