﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_try_script2 : System.Web.UI.Page
{
    protected string url;
    protected string urlAnythingSliderFolder;
    protected string root;


    protected void Page_Load(object sender, EventArgs e)
    {
        root = ResolveUrl("~");
        url = "asp.aspx";
        urlAnythingSliderFolder = "http://" + Request.Url.Host + ":" + Request.Url.Port + root + "consumer/anythingSlider/";
        
        Page.Header.DataBind();
        //DataBind();
    }
}