﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_try_updatepanel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            car c = new car("red","1000");
            List<car> myCars=new List<car>();
            myCars.Add(c);

            car c2 = new car("blue", "2000");
            myCars.Add(c2);

            datalist1.DataSource = myCars;
            datalist1.DataBind();
        }

        Page.Header.DataBind();

    }

    protected void btn_Click(object sender, EventArgs e)
    {
        btn.Text = DateTime.Now.ToString();
    }

    public class car
    {
        private string _color;
        private string _price;

        public car(string Color, string Price)
        {
            color = Color;
            price = Price;
        }

        public string color
        {
            get { return _color; }
            set { _color = value; }
        }

        public string price
        {
            get { return _price; }
            set { _price = value; }
        }
    }

    protected void lb1_Click(object sender, EventArgs e)
    {
        //Response.Write("baaaaaaa");
    }

    protected void lbStatus_Click(object sender, EventArgs e)
    {
        //Response.Write("fhfgfg");
        UpdatePanel1.Update();
    }

    protected void link1_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        lb.Text = "sdfdfsd";
        lb.Attributes.Add("style", "background-color:red");
    }

    protected void lbStatus_Command(Object sender, CommandEventArgs e)
    {
              
        LinkButton lb = (LinkButton)sender;
        lb.Attributes.Add("style", "background-color:#666666");
        lb.Attributes.Add("style", "color:red;");
        lb.Font.Italic = true;
        lb.Text = "dfgfggdfgdf";
      
    }
   

}