using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class try_script : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string csnameStep = "setStep";
            Type cstypeStep = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager csStep = this.Page.ClientScript;


            if (!csStep.IsStartupScriptRegistered(cstypeStep, csnameStep))
            {
                //string csTextStep = "witchAlreadyStep(" + RegistrationStage + ");";
                string csTextStep = "witchAlreadyStep(6);";
                csStep.RegisterStartupScript(cstypeStep, csnameStep, csTextStep, true);
            }
        }
    }
}
