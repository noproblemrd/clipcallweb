﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_try_isSafari : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool ifSafari = Request.Browser.Browser.ToLower().Contains("safari");

        Response.Write("ifSafari: " + ifSafari);
    }
}