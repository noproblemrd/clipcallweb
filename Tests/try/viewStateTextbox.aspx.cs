﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_try_viewStateTextbox : System.Web.UI.Page
{
    protected TextBox txtDynamic;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToString(ViewState["Generated"]) == "true")
            GenerateDynamicControls();

    }
    protected void btnGDynamicCont_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(ViewState["Generated"]) != "true")
        {
            GenerateDynamicControls();
            ViewState["Generated"] = "true";
        }
        else
        {
            GenerateDynamicControls();
            Response.Write("<h2>Controls are already exist in page</h2>");
        }
    }

    public void GenerateDynamicControls()
    {
        txtDynamic = new TextBox();
        txtDynamic.ID = "txtDynamic";
        txtDynamic.Text = "Dynamic TextBox";
        Page.Form.Controls.Add(txtDynamic);

        LinkButton lnkDynamic = new LinkButton();
        lnkDynamic.ID = "lnkDynamic";
        lnkDynamic.Click += new EventHandler(lnkDynamic_Click);
        lnkDynamic.Text = "Dynamic LinkButton";
        Page.Form.Controls.Add(lnkDynamic);
    }

    void lnkDynamic_Click(object sender, EventArgs e)
    {
        Response.Write("<h2>Dynamic linkbutton clicked</h2>");
    }

}