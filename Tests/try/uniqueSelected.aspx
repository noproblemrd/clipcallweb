﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uniqueSelected.aspx.cs" Inherits="Tests_try_uniqueSelected" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script>
        function getUniqueSelectText(obj, link, waterMark, variable, defaultValue) {
            if (document.getElementById(obj) != null) {
                alert(typeof (variable));
                var returnValue;

                returnValue = document.getElementById(link).innerHTML; //  Firefox does not support innerText property
                returnValue = returnValue.toLowerCase();

                if (typeof (variable) == "boolean") {
                    if (returnValue == document.getElementById(waterMark).value.toLowerCase()) {
                        returnValue = defaultValue;

                    }

                    else if (returnValue == "yes") {
                        returnValue = true;
                    }

                    else {
                        returnValue = false;
                    }
                }
                else {
                    if (returnValue == document.getElementById(waterMark).value.toLowerCase()) {
                        returnValue = defaultValue;
                    }
                }

                return returnValue;

            }



            /*
            var soon;
            if (document.getElementById("expertiseSoon") != null) {

            soon = document.getElementById("soonExpertiseSelected").innerHTML; //  Firefox does not support innerText property
            soon = soon.toLowerCase();

            if (soon == document.getElementById("HiddenWaterMarkSoon").value.toLowerCase()) {
            soon = "Immediately";
            }

            }


            var live = false;
            if (document.getElementById("expertiseLive") != null) {
            live = document.getElementById("liveExpertiseSelected").innerHTML; //  Firefox does not support innerText property
            live = live.toLowerCase();
            if (live == document.getElementById("HiddenWaterMarkLive").value.toLowerCase()) {
            live = true;

            }

            else if (live == "yes") {
            live = true;
            }

            else {
            live = false;
            }


            }
            */


        }

        function init() {
            var soon="";


            soon = getUniqueSelectText("expertiseSoon", "soonExpertiseSelected", "HiddenWaterMarkSoon", soon, true);
            alert(soon);
        }
        </script>
</head>

<body onload="init();">
<form id="form1" runat="server">
   <asp:HiddenField id="HiddenWaterMarkSoon" runat="server" Value="Please select"/>


<div class="expertiseSoon" id="expertiseSoon">                    
                <a tabindex="0" id="linkSoon" title="" style="display: inline-block; -moz-user-select: none;" class="selectBox selectBox-dropdown">
                <span class="selectBox-label" id="soonExpertiseSelected">Please select</span>
                <span class="selectBox-arrow"></span>
                </a>         	
	 

                <ul style="-moz-user-select: none; width: 310px;display:none;" class="selectBox-dropdown-menu selectBox-options" id="ul1">                   
                    <li class="">                        
                        <a href="javascript:void(0);" >Immediately</a>
                    </li>

                     <li class="">                        
                        <a href="javascript:void(0);" >1 to 6 months</a>
                    </li>                

                    <li class="">                        
                        <a href="javascript:void(0);" >6 months and later</a>
                    </li>  

                     <li class="">                        
                        <a href="javascript:void(0);" >We do not want to file</a>
                    </li> 
                </ul>                  
                  
                               
            </div>  

    </form>
</body>
</html>
