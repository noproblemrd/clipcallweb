<%@ Page Language="C#" AutoEventWireup="true" CodeFile="chromeDivHeight.aspx.cs" Inherits="try_chromeDivHeight" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script>
    function hideDiv()
    {
        document.getElementById("div2").style.display='none';
    }
    
    </script>
</head>
<body  style="margin:0px;background-color:green;" >   
	<!--------------------------------- SECTION FORM REQUEST -------------------------------->         
        <table width="100%" cellpadding="0" cellspacing="0" bgcolor="aqua" border=1>   
        <tr>
        <td align="left">
        <input type="button" onclick="hideDiv();" />
            <!--<div class="slogan" >Let the movers call you</div>-->
           <div class="container" >
				<form id="form_request" method="get" action="" runat="server">
				    <!--
					<p>
						<label for="form-search" class="form-search-label">Search Directory</label>
						<select id="form-search" class="form-select">
							<option value="0">Movers</option>
							<option value="1">Carpenters</option>
							<option value="2">Plumbers</option>							
						</select>
					</p>
					-->
					
					<p class="clearfix">
						<label for="form-call" id="lbl_MyPhone" runat="server">My Phone number is</label>
						<input id="form-call" class="form-input"  />
					</p>
					<br class="clear" />
					<p class="clearfix" >			    
						<label for="form-msg" class="form-msg-label" id="lbl_RequestDescription" runat="server" >YOUR REQUEST DESCRIPTION</label>
						<span id="numMmessageChars"></span>						
						<textarea id="form-msg" class="form-textarea" cols="50" rows="5"   onkeypress="checkMeasageLength();" onkeyup="checkMeasageLength();"></textarea>    
					</p>				
					<div>
    <asp:HiddenField ID="HiddenNotCorrectPhoneNum" runat="server" Value="Incorrect phone number format " />
     <asp:HiddenField ID="HiddenMustFulfill" runat="server" Value="Must fulfill your request description" />
     <asp:HiddenField ID="HiddenMaxLength" runat="server" Value="Max length of message is 140 characters" />
     <asp:HiddenField ID="HiddenCharacters" runat="server" Value="Characters" />

</div>
			<asp:HiddenField runat="server" id="hidden_noAvailable" Value="No available advertisers were found for your request please try later or change your search criteria"/>
		
				</form>
				
				<!--------------------------------- SECTION FEEDBACK -------------------------------->

				<table cellspacing="0" class="feedback" border=0>
					<tr>
						<td class="trigger" style="text-align:center;">
						    <div id="div_suppliers" style="position:relative;margin-bottom:20pt;text-align:left;width:100%;font-size:12px;color:#000;">
							<!--Please have <input id="form-suppliers" class="form-input" value="2" /> Movers  call me <strong class="fc-green">NOW</strong>-->
                           <asp:Label ID="lbl_PleaseHave" runat="server" Text="Please have"></asp:Label>
							
							<select id="form-suppliers" class="form-input">
							    <option value="2">2</option>
							    <option value="3" selected="selected">3</option>
							    <option value="4">4</option>
							    <option value="5">5</option>
							    <option value="6">6</option>
							</select>
                            <asp:Label ID="lbl_CallMe" runat="server" Text="suppliers call me"></asp:Label>
							  <strong class="fc-green">
                                  <asp:Label ID="lbl_now" runat="server" Text="NOW"></asp:Label>
							  </strong>

							</div>
						</td>
						<td>
						    <div id="div_initiateCalls" style="display:inline;">
						        <a href="#" class="btn" id="btn_InitiateCalls" runat="server"
						         onclick="javascript:return validation();">Initiate calls</a>
						        
						    </div>							
						</td>								
					</tr>
					<tr>
						<td colspan="2" class="explanation" width="100%">
						<div id="div_explanation" style="display:none;">							
							<table width="100%" height="50px">
							    <tr>			        
							        <td height="50px" style="text-align: center; font-size: 11px; color: #182e71; line-height: 12px; font-variant: small-caps;">
							        		        						       
							            
                                        <asp:Label ID="lbl_callingStatus1" runat="server">
                                        We are calling Trusted and  Available suppliers
                                        </asp:Label>
							            <br />
							            <asp:Label ID="lbl_callingStatus2" runat="server">
                                        Your phone will ring once we have found a matched Mover.
                                        </asp:Label>
							            <br />
							            <asp:Label ID="lbl_callingStatus3" runat="server">
                                        This will take seconds
                                        </asp:Label>
							            <br />
							            <asp:Label ID="lbl_callingStatus4" runat="server">
                                        Please make sure you are available for a call now
                                        </asp:Label>
							            <br />
							            <asp:Label ID="lbl_callingStatus5" runat="server">
                                         Available Movers are listed below, as they respond.
                                        </asp:Label>
							           
							       				        							       
							        </td>
							        
							        <td style="vertical-align:middle;">
							            <div class='iconCallingSmall'>
                                            <div class='iconCallingSmallImg'><img width="68" height="40" src="<%=ResolveUrl("~")%>images/icons/icon-calling-small_clear.gif" alt="calling"/></div>
                                            <div class='iconCallingSmallText'>
                                            <asp:Label ID="lbl_calling" runat="server" Text="CALLING..."></asp:Label>
                                            </div>
                                        </div>
							            
							        </td>							        
							        						        
							    </tr>
							</table>					
							
							
							
						</div>						
						</td>
					</tr>				
					
					<tr>
						<td class="status" id="td_message" colspan="2"  style="display:none;text-align:center;width:100%">
							<div style="width:100%">
                            <asp:Label ID="lbl_ringInSecond" runat="server" 
                            Text="Your Phone will ring in a few seconds. The following movers are willing to talk to you NOW."></asp:Label>
							</div>
						</td>
					</tr>
					
					<tr>
						<td class="status"  id="td_speaking" colspan="2"  style="display:none;text-align:center;width:100%">
							<div style="width:100%">
                                <asp:Label ID="lbl_speakingWith" runat="server" Text="You are speaking with"></asp:Label>
							</div>
						</td>
					</tr>				
					
					
				</table>
				
			</div>
			
			
<!--------------------------------- SECTION RESULTS -------------------------------->
			
			<table cellspacing="0" id="Table1" style="background-color:blue;">
			    <!--
				<thead>
					<tr class="call">
						<th valign="top">SHOWING 1-10 0F 112 LISTINGS</th>
						<th valign="top"><strong>NOW AVAIBLE MOVERS</strong></th>
						<th valign="top">SORT BY: I RATING I A-Z</th>
					</tr>
				</thead>
				-->
				<tbody> 				   
				    <tr>
				        <td colspan="3" style="padding:0px 0px 0px 0px;vertical-align:top;" ><div id="div2" style="width:100%;">baaaaaaa</div></td>				        
				    </tr>				 
				</tbody>
			</table>
			
			
			
			</td>
			
			
			
		  </tr>
        </table>        
<!--------------------------------- Here ends the relevant section -------------------------------->
</body>
</html>
