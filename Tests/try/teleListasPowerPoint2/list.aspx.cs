using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class yellowPages_list : System.Web.UI.Page
{
    protected string root = "";
    protected string SiteId = "";
    protected string ExpertiseCode = "";
    protected string ExpertiseLevel = "";
    protected string RegionCode = "";
    protected string RegionLevel = "";
    protected string MaxResults = "";
    protected string origionId = "";    
    protected string parentDomain = "";
    protected string IfShowMinisite = "";
    protected string regFormatPhone = "";
    protected string style = "";

    public void Page_Load(object sender, EventArgs e)
    {

        root = ResolveUrl("~");
        SiteId = Request.QueryString["SiteId"];        
        ExpertiseCode = Request["ExpertiseCode"];
        ExpertiseLevel = Request["ExpertiseLevel"];
        RegionCode = Request["RegionCode"];
        RegionLevel = Request["RegionLevel"];
        MaxResults = Request["MaxResults"];
        origionId = Request["origionId"];
        parentDomain = Request["parentDomain"];
        style = Request["style"];
        if (style == "" || Request["style"]==null)
            style = "styleList";
        MyStyleSheet.Href = style + ".css?random=" + DateTime.Now.ToString();

        regFormatPhone = DBConnection.GetPhoneRegularExpression(SiteId);
        regFormatPhone = regFormatPhone.Replace("\\", "\\\\");

        IframeSite _iframe = new IframeSite(SiteId);
        IfShowMinisite = _iframe.GetDisplayMinisite.ToString();
        
    }
}
