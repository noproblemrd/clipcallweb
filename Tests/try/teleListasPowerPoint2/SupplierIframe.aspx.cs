﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class teleListasPowerPoint2_SupplierIframe : System.Web.UI.Page
{
    protected string SiteId = "";
    protected string ExpertiseCode = "";
    protected string ExpertiseLevel = "";
    protected string RegionCode = "";
    protected string RegionLevel = "";
    protected string MaxResults = "";
    protected string root = "";
    protected string origionId;
    protected string Description = "";
    protected string parentDomain = "";
    protected string frames = "";
    protected string regFormatPhone = "";
    protected string IfShowMinisite = "";
    protected string waterMarkComment = "";
    protected string waterMarkCommentText = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        root = ResolveUrl("~");
        SiteId = Request["SiteId"];
        regFormatPhone = DBConnection.GetPhoneRegularExpression(SiteId);
        regFormatPhone = regFormatPhone.Replace("\\", "\\\\");
        ExpertiseCode = Request["ExpertiseCode"];
        ExpertiseLevel = Request["ExpertiseLevel"];
        RegionCode = Request["RegionCode"];
        RegionLevel = Request["RegionLevel"];
        MaxResults = Request["MaxResults"];
        origionId = Request["origionId"];
        Description = Request["Description"];
        parentDomain = Request["parentDomain"];
        waterMarkComment = Request["waterMarkComment"];
        frames = String.IsNullOrEmpty(Request["frames"]) ? "1" : Request["frames"];

        IframeSite _iframe = new IframeSite(SiteId);
        int defaultNumAdvertisers = _iframe.GetDefaultNumAdvertisers;
        int getMaximimAdvertisers = _iframe.GetMaximimAdvertisers;
        IfShowMinisite = _iframe.GetDisplayMinisite.ToString();
        ListItem listItem;
        for (int i = 1; i <= getMaximimAdvertisers; i++)
        {
            listItem = new ListItem();
            listItem.Text = i.ToString();
            listItem.Value = i.ToString();
            if (defaultNumAdvertisers == i)
                listItem.Selected = true;
            formSuppliers.Items.Add(listItem);
        }


        //        lbl_TypeOfBusiness.Text = Request["TypeOfBusiness"];


        if (Request["waterMarkComment"] != null)
        {
            formMsg.Text = Request["waterMarkComment"];            
            formMsg.CssClass="waterMark";
            
            waterMarkCommentText = Request["waterMarkComment"];
        } 
        

        formMsg.Attributes["onkeypress"] = "checkMeasageLength();";
        formMsg.Attributes["onkeyup"] = "checkMeasageLength();";

        Page.DataBind();
    }
}
