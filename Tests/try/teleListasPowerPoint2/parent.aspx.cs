﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class teleListasPowerPoint2_parent : System.Web.UI.Page
{
    protected string src = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string _host = Request.ServerVariables["SERVER_NAME"];
            if (string.IsNullOrEmpty(_host))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            //iframeNoProblem.Attributes["src"] = GetSrc(_host);
            src = GetSrc(_host);
        }

    }

    private string GetSrc(string _host)
    {
        string _src = DBConnection.GetSrc(_host);
        if (string.IsNullOrEmpty(_src))
            Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");

        return "supplierIframe.aspx?SiteId=" + _src;

        //return "supplierIframe.aspx?SiteId=1&ExpertiseCode=1&ExpertiseLevel=1&RegionCode=3&RegionLevel=1&MaxResults=-1&frames=2&origionId={CF879946-455C-DF11-80CD-0003FF727321}" +
        //"&TypeOfBusiness=Mudanחas&Description=Mother is somthing else. Can you imagine a perfect world where alll the kids rule the entire world. Punish their  parents and never make mistake";


    }
}
