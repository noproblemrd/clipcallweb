using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text;

public partial class yellowPages_supplierRegular : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Labels
        Panel _panel = new Panel();

        Label lbl_Calls = new Label();
        lbl_Calls.Text = "Calls:";
        lbl_Calls.ID = "lbl_Calls";
        _panel.Controls.Add(lbl_Calls);

        Label lbl_Employees = new Label();
        lbl_Employees.Text = "Employees:";
        lbl_Employees.ID = "lbl_Employees";
        _panel.Controls.Add(lbl_Employees);

        Label lbl_Certificate = new Label();
        lbl_Certificate.Text = "Certificate";
        lbl_Certificate.ID = "lbl_Certificate";
        _panel.Controls.Add(lbl_Certificate);

        Label lbl_CallNow = new Label();
        lbl_CallNow.Text = "CALL NOW";
        lbl_CallNow.ID = "lbl_CallNow";
        _panel.Controls.Add(lbl_CallNow);

        Label lbl_ShowAll = new Label();
        lbl_ShowAll.Text = "Show All List";
        lbl_ShowAll.ID = "lbl_ShowAll";
        _panel.Controls.Add(lbl_ShowAll);





        string SiteId;
        //SiteId = Request["SiteId"].ToString();
        SiteId = Request["SiteId"];
        //Response.Write("SiteId:" + SiteId);
   //     if (!string.IsNullOrEmpty(SiteId))
   //         GetTranslate(SiteId, _panel);

        string ExpertiseCode;
        ExpertiseCode = Request["ExpertiseCode"];

        int ExpertiseLevel;
        ExpertiseLevel = Convert.ToInt32(Request["ExpertiseLevel"]);

        string RegionCode;
        RegionCode = Request["RegionCode"];

        int RegionLevel;
        RegionLevel = Convert.ToInt32(Request["RegionLevel"]);


        int MaxResults;
        if (Request["MaxResults"] != null && Request["MaxResults"].ToString() != "")
        {
            MaxResults = Convert.ToInt16(Request["MaxResults"].ToString());
        }
        else
            MaxResults = -1;

        string origionId = "";
        origionId = Request["origionId"];
        //Response.Write("origionId" + origionId);
        string IfShowMinisite = Request["IfShowMinisite"];
        //Response.Write("IfShowMinisite:" + IfShowMinisite);

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(null,SiteId);
        WebReferenceCustomer.SearchRequest searchRequest = new WebReferenceCustomer.SearchRequest();
        searchRequest.ExpertiseCode = ExpertiseCode;
        searchRequest.ExpertiseLevel = ExpertiseLevel;
        searchRequest.MaxResults = MaxResults;
        searchRequest.RegionCode = RegionCode;
        searchRequest.RegionLevel = RegionLevel;
        searchRequest.SiteId = SiteId;
        searchRequest.OriginId = origionId;
        
        string xmlSuppliers = customer.SearchSiteSuppliers(searchRequest);

        //WebReferenceSite.Site siteWebService = WebServiceConfig.GetSiteReference(this);
        //string xmlSuppliers = siteWebService.SearchSiteSuppliers(SiteId, ExpertiseGUID, CityGuid, ExpertiseType, MaxResults);

        //Response.Write("SiteId:" + SiteId + " ExpertiseGUID:" + ExpertiseGUID + " CityGuid:" + CityGuid + " ExpertiseType:" + ExpertiseType + " MaxResults:" + MaxResults);
        //Response.Write(xmlSuppliers);

        System.Text.UTF8Encoding myEncoder = new System.Text.UTF8Encoding();
        byte[] bytes = myEncoder.GetBytes(xmlSuppliers);
        MemoryStream ms = new MemoryStream(bytes);
        XmlTextReader xmlTextReader = new XmlTextReader(ms);
        string SupplierId = "";
        string SupplierNumber = "";
        string SupplierName = "";
        string SumSurvey = "";
        string SumAssistanceRequests = "";
        string NumberOfEmployees = "";
        string Certificate = "";
        string ShortDescription = "";
        string isAvailable = "";
        string DirectNumber = "";
        bool blnlogo = false;
        string ExtensionNumber = "";
        string professionalLogos = "";
        string professionalLogosWeb = "";
        bool ifError = false;
        
        StringBuilder sb = new StringBuilder();

        /*
         <Suppliers>
            <Supplier>
            <SupplierId />
            <SupplierNumber />
            <SupplierName />
            <SumSurvey />
            <SumAssistanceRequests />
            <NumberOfEmployees />
            <ShortDescription/ >
            <DirectNumber />
            </Supplier>
         </Suppliers>
        */

        sb.Append("<div class='contentwrap'>");
               
            while (xmlTextReader.Read())
            {


                if (xmlTextReader.Name == "Suppliers")
                {

                    if (xmlTextReader.GetAttribute("ExtensionNumber") != null)
                    {
                        ExtensionNumber = xmlTextReader.GetAttribute("ExtensionNumber");
                      
                    }

                    while (xmlTextReader.Read())
                    {
                        if (xmlTextReader.Name == "Error" && xmlTextReader.NodeType == XmlNodeType.Element)
                        {
                            ifError = true;
                            sb.Append("");
                        }

                        if (xmlTextReader.Name == "Error" & xmlTextReader.NodeType == XmlNodeType.EndElement)
                        {
                            break;
                        }

                        if (xmlTextReader.Name == "Supplier")
                        {
                            sb.Append("<div class='content'>");
                            
                            SupplierId = xmlTextReader.GetAttribute("SupplierId");
                            SupplierNumber = xmlTextReader.GetAttribute("SupplierNumber");
                            SupplierName = xmlTextReader.GetAttribute("SupplierName");
                            DirectNumber = xmlTextReader.GetAttribute("DirectNumber");                         

                            string areaCode = "";
                            string newFormatDirectNumber = "";

                            if (DirectNumber != "" && DirectNumber.Length>=10)
                            {
                                areaCode = "(" + DirectNumber.Substring(0, 2) + ")";
                                newFormatDirectNumber = DirectNumber.Substring(2, 4) + " " + DirectNumber.Substring(6, 4);
                                DirectNumber = areaCode + " " + newFormatDirectNumber;
                            }
                            
                            
                            ShortDescription = xmlTextReader.GetAttribute("ShortDescription");
                            //SumSurvey = xmlTextReader.GetAttribute("SumSurvey");
                            SumAssistanceRequests = xmlTextReader.GetAttribute("SumAssistanceRequests");
                            NumberOfEmployees = xmlTextReader.GetAttribute("NumberOfEmployees");
                            Certificate = xmlTextReader.GetAttribute("Certificate");
                            isAvailable = xmlTextReader.GetAttribute("isAvailable");                    
                            
                            
                            professionalLogos = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
                            professionalLogosWeb = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];

                            blnlogo = File.Exists(professionalLogos + SupplierId + ".jpg");

                            if (IfShowMinisite == "True")
                             /*   sb.Append("<div class='title'><a href='../professional.aspx?supplierGUID={" + SupplierId + "}&SiteId=" + SiteId
                                    + "&DirectNumber=" + DirectNumber + "&ExtensionNumber=" + ExtensionNumber
                                    + "' target='_new'>" + SupplierName + "</a></div>");*/
                                 sb.Append("<div class='title'>" + SupplierName + "</div>");
                            else
                                sb.Append("<div class='title'>" + SupplierName + "</div>");

                            sb.Append("<div class='logo'>");

                            if (blnlogo)
                            {                               
                                sb.Append("<img class='image' width=60 height=46 src='" + professionalLogosWeb + SupplierId + @".jpg'>");
                            }
                            else
                            {
                                sb.Append("<img class='image' width=60 height=46 src='images/logox.png'/>");
                            }

                            sb.Append("</div>");

                            if (ShortDescription.Length == 0)
                                ShortDescription = "&nbsp;";



                            else if (ShortDescription.Length>190)
                            {
                                ShortDescription = ShortDescription.Substring(0, 190) + "...";
                            }

                            sb.Append("<div class='mid'>" + ShortDescription + "</div>");
                            sb.Append("<div class='phone'>");
                            
                           
                            //sb.Append("<div class='right'>");

                           
                            if (ExtensionNumber != "")
                            {
                                
                                sb.Append(ExtensionNumber + " " + DirectNumber);
                                
                            }

                            else
                                sb.Append(DirectNumber);

                            //sb.Append("</div>");

                            sb.Append("</div>");
                            
                            sb.Append("<div class='clear'><!----></div>");
                            
                        }

                        if (xmlTextReader.Name == "Suppliers" & xmlTextReader.NodeType == XmlNodeType.EndElement)
                        {
                            break;
                        }
                    }

                    
                    
                }


                
        }

        sb.Append("</div>");
        Response.Write(sb.ToString());

    }

   
}
