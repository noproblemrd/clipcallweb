﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SupplierIframe2.aspx.cs" Inherits="teleListasPowerPoint2_SupplierIframe2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-BR">
<head>
<title>Telelistas</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


<link href="style2.css" rel="stylesheet" type="text/css" />
<!--[if IE 7]>
<link href="ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->
 <script type="text/javascript" src="../../../generalIframe.js"></script>

    <script type="text/javascript">
		//var _areaPhone = new RegExp("^\\d{2}$");
		var _areaPhone = new RegExp("^\\({1}\\d{2}\\){1}$|^\\d{2}$"); // 2 numbers or 2 numbers with parentheses (xx)
		//var _areaPhone = new RegExp("^[0-9]+$");
        //var _phone = new RegExp("^[1-9]{1}\\d{7}$"); // (xxxx-xxxx)
        //var _phone = new RegExp("^\\({1}[1-9]{1}\\d{3}-\\d{4}\\){1}"); // (xxxx-xxxx) first number 1-9
        //var _phone = new RegExp("^[1-9]{1}\\d{3}-\\d{4}$"); // xxxx-xxxx first number 1-9
        var _phone = new RegExp("(^[1-9]{1}\\d{7}$)|(^[1-9]{1}\\d{3}-\\d{4}$)");
		
		
        var intervalRequest;       
        var intervalExplanation;
        var indexExplanations=0;
        var counter_cookies;
        counter_cookies=0;
        
        var RequestDescription;    
        
        function checkMeasageLength()
        {  
           document.getElementById("<%#formMsg.ClientID%>").className='form-textarea'; 
   
           document.getElementById("<%=lbl_RequestDescription.ClientID%>").innerHTML=document.getElementById("<%=HiddenRequestDescription.ClientID%>").value + " <font color='green'>" +  
            document.getElementById("<%#formMsg.ClientID%>").value.length  + "</font>/140";

           if(document.getElementById("<%#formMsg.ClientID%>").value.length>140) 
           {
                alert(document.getElementById("<%=HiddenMaxLength.ClientID%>").value);
                document.getElementById("<%#formMsg.ClientID%>").value=document.getElementById("<%#formMsg.ClientID%>").value.substring(0,document.getElementById("<%#formMsg.ClientID%>").value.length-1); 
                //document.getElementById("numMmessageChars").innerHTML=document.getElementById("<%#formMsg.ClientID%>").value.length + "/140 &nbsp;" + document.getElementById("<%=HiddenCharacters.ClientID%>").value;
                document.getElementById("<%=lbl_RequestDescription.ClientID%>").innerHTML=document.getElementById("<%=HiddenRequestDescription.ClientID%>").value + " <font color='green'>" +  
                    document.getElementById("<%#formMsg.ClientID%>").value.length   + "</font>/140" + document.getElementById("<%=HiddenCharacters.ClientID%>").value;

           }         
           
        }
        
        var counterClear=0;
        
        function makeOnceClear()
        {
            if(counterClear==0)
                document.getElementById('<%#formMsg.ClientID%>').value='';
            counterClear++
        }
        
        function hideExplanations()
        {             
            document.getElementById("div_wrap").style.display='none';
            document.getElementById("div_explanationParent").style.display='none';
            document.getElementById("div_explanation1").style.display='none';
            document.getElementById("div_explanation2").style.display='none';
            document.getElementById("div_explanation3").style.display='none';
        }
        
        function showExplanations()
        {	 
            //alert("showExplanations:"); 
            indexExplanations++;         
            hideExplanations(); 
            document.getElementById("div_wrap").style.display='';
            document.getElementById("div_explanationParent").style.display=''; 	
		    document.getElementById("div_explanation" + indexExplanations).style.display='';
		    if (indexExplanations==3)
		        indexExplanations=0;
		    intervalExplanation=setTimeout("showExplanations()",10000); 

        }
        
        function showDefault()
        {    
            document.getElementById("div_wrap").style.display='';        
            document.getElementById("div_form").style.display='';  
            document.getElementById("div_initiateCalls").style.display='';                           
            
            document.getElementById("form-call").value="";
            document.getElementById("txt_phoneAreaCode").value="";
            document.getElementById("<%#formMsg.ClientID%>").value="";
            document.getElementById("<%=formSuppliers.ClientID%>").selectedIndex=0;
        }      
        
        
        function showBubbleSuppliersList(incidentId)
        {                 
                      
            var oDate = new Date();
	        url="http://" + location.hostname + ":" + location.port + "<%=root%>tests/try/telelistasPowerPoint2/suppliersBubbleBrazilListOriginal.aspx?incidentId=" + incidentId + "&SiteId=<%=SiteId%>&IfShowMinisite=<%=IfShowMinisite%>&randomSeed=" + escape(oDate.toLocaleTimeString());
                       
            var objHttp;
            var ua = navigator.userAgent.toLowerCase(); 
            
            if(ua.indexOf("msie")!= -1)
            {
               // alert("msie");
                objHttp =  new ActiveXObject("Msxml2.XMLHTTP");
               
            }
            else
            {
                //alert("not msie");
                objHttp=new XMLHttpRequest();
            }
            
            if (objHttp == null)
            {
                //alert("Unable to create DOM document!");
                return;
            } 	        
            
            
            objHttp.open("GET", url, true);	       
            
            objHttp.onreadystatechange=function()
            {	  
                          
                if (objHttp.readyState==4)
                {
                    //alert(objHttp.responseText);
                    
                    
                    intervalRequest=setTimeout("showBubbleSuppliersList('" + incidentId + "')",3000); 

                    var posMode;
                    posMode=objHttp.responseText.indexOf("mode=");
                    
                    
                    var posMode2;
                    posMode2=objHttp.responseText.indexOf("mode2");
                   
                    
                    var mode;
                    mode=objHttp.responseText.substring((posMode*1+5),posMode2);                                                   
                    
                    var ifShowIframeStart;
                    ifShowIframeStart=objHttp.responseText.indexOf("IncidentStatus=");
                    
                    var ifShowIframeEnd;
                    ifShowIframeEnd=objHttp.responseText.indexOf("IncidentStatus2");
                    
                    var ifShowIframe;
                    ifShowIframe=objHttp.responseText.substring((ifShowIframeStart*1+15),ifShowIframeEnd);
                     
                    
                    
                    if(ifShowIframe=="Active") // Active means Open or Initiated or Succeed 
                    {
                        if (intervalExplanation!=null && intervalExplanation!="")
                            clearInterval(intervalExplanation);
                        hideExplanations();
                        document.getElementById("div_list").style.display='';
                        document.getElementById("div_list").innerHTML=objHttp.responseText;
                    }
                    
                    else if(ifShowIframe=="Completed")
                    {                        
                        
                        clearInterval(intervalRequest);
                        document.getElementById("div_list").style.display='none'; 
                        showDefault();
                   }                    
                   
                }
             }
             
           objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
           objHttp.send(null);
        }
        
        
        function get_cookie(cookie_name)
        {
          var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

          if (results)
            return ( unescape ( results[2] ) );
          else
            return null;
        }

        function set_cookie(name,value,exp_y,exp_m,exp_d,exp_hh,exp_mm,exp_ss,path,domain,secure)
        {           
          var cookie_string = name + "=" + escape(value);

          if (exp_y)
          {
            var expires = new Date (exp_y,exp_m,exp_d,exp_hh,exp_mm,exp_ss);
            
            cookie_string += ";expires=" + expires.toGMTString();
            
          }

          if (path)
                cookie_string += "; path=" + escape (path);

          if (domain)
                cookie_string += "; domain=" + escape (domain);
      
          
          if (secure)
                cookie_string += "; secure";
          
          
          document.cookie = cookie_string;
        }

        function createServiceRequest(cleanMsg)
        {           
            
    
            var current_date = new Date();
            var cookie_year = current_date.getFullYear();
            var cookie_month = current_date.getMonth();
 
            var cookie_day = current_date.getDate();
           
            var cookie_hh = current_date.getHours();
                  
            var cookie_mm = current_date.getMinutes();
            cookie_mm=cookie_mm+5;
            var cookie_ss = current_date.getSeconds();
           
            
            
            if(!get_cookie('<%=ExpertiseCode%>'))
            {
                counter_cookies++;                

                set_cookie('<%=ExpertiseCode%>',counter_cookies,cookie_year,cookie_month,cookie_day,cookie_hh,cookie_mm,cookie_ss);
                
            }
            
            else
            {                
                alert('<%=HiddenAlreadyRecieved.Value%>');
                clearInterval(intervalExplanation);
                hideExplanations();
                showDefault();
                return false;  
            } 
              
             
	        var phoneNumber=document.getElementById("form-call").value.replace("+","%2b"); // replace + to %2b
	        var phoneAreaCode = document.getElementById("txt_phoneAreaCode").value.replace(/\s/g,"");
            
            phoneNumber=phoneAreaCode+phoneNumber;
 
	        
	        
	        var params;
	        params="ContactPhoneNumber=" + phoneNumber + "&Description=" +
	         cleanMsg  +
	          "&NumOfSuppliers=" + document.getElementById("<%=formSuppliers.ClientID%>").value + 
	          "&SiteId=<%=SiteId%>&ExpertiseCode=<%=ExpertiseCode%>&ExpertiseType=<%=ExpertiseLevel%>&ServiceAreaCode=<%=RegionCode%>&ServiceAreaType=<%=RegionLevel%>&origionId=<%=origionId%>";
	         
        
	        var url;       
	          
	        url="http://" + location.hostname + ":" + location.port + "<%=root%>createServiceRequest.aspx"; 
	            

        	
	        var objHttp;
	        var ua = navigator.userAgent.toLowerCase(); 
	        
	        if(ua.indexOf("msie")!= -1)
	        {
	           // alert("msie");
	            objHttp =  new ActiveXObject("Msxml2.XMLHTTP");
	           
	        }
	        else
	        {
	            //alert("not msie");
	            objHttp=new XMLHttpRequest();
	        }
	      
	       
        	
	        if (objHttp == null)
            {
                //alert("Unable to create DOM document!");
		        return;
	        } 	        
	        
	        
	        objHttp.open("POST", url, true);	        
	        
	        objHttp.onreadystatechange=function()
	        {	  
	                   
		        if (objHttp.readyState==4)
		        {
			       //alert("responseText"+objHttp.responseText);			       
			       
			       if(objHttp.responseText=="unsuccess")
			       {
			            clearInterval(intervalExplanation);
			            alert('<%=hidden_noAvailable.Value%>');                      
                        hideExplanations();
                        showDefault();
			       }
			       
			       else
			       {
			            
			            showBubbleSuppliersList(objHttp.responseText);
			       } 
		        }
            }     
            
            
            objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            objHttp.setRequestHeader("Content-Length", params.length);
            objHttp.setRequestHeader("Connection", "close");
	        
            objHttp.send(params);
            
            
            return true;
        	
        }
        
       
        
        function validation()
        {
       
                var ifValidPhone=true;
                var firstChar;
                
	            var phoneNumber=document.getElementById("form-call").value.replace(/\s/g,"");	           
                var phoneAreaCode = document.getElementById("txt_phoneAreaCode").value.replace(/\s/g,"");
                               
                if (phoneAreaCode.search(_areaPhone)==-1) //if match failed
                {                    
                   ifValidPhone=false;
                }           
                
	            if (!ifValidPhone)
	            {
	                alert(document.getElementById("<%=HiddenNotCorrectPhoneNum.ClientID%>").value);
	                document.getElementById("txt_phoneAreaCode").select();
	                return false;
	            }
	                   
                if (phoneNumber.search(_phone)==-1) //if match failed
                {                    
                   ifValidPhone=false;
                } 
                
	            if (!ifValidPhone)
	            {
	                alert(document.getElementById("<%=HiddenNotCorrectPhoneNum.ClientID%>").value);
	                document.getElementById("form-call").select();
	                return false;
	            }
    	        
   	            
	            phoneAreaCode=phoneAreaCode.replace("(","");
	            phoneAreaCode=phoneAreaCode.replace(")","");
	            
	            
	            phoneNumber=phoneNumber.replace("-","");	                      
	            phoneNumber=phoneAreaCode+phoneNumber;        
	            
	            
	            var msg="";
	            msg=trim(document.getElementById("<%#formMsg.ClientID%>").value);
    	        
	            if(msg=="" || msg=='<%#waterMarkCommentText%>')
	            {
	                alert(document.getElementById("<%=HiddenMustFulfill.ClientID%>").value);
	                document.getElementById("<%#formMsg.ClientID%>").select();
	                return false;
	            } 
    	        
    	        
	            document.getElementById("div_initiateCalls").style.display='none'; 
	            document.getElementById("div_form").style.display='none';   
	            
	            showExplanations();                      
                      
                createServiceRequest(msg);
               
    	       
        }
        
        function init()
        {  
            RequestDescription=document.getElementById("<%=lbl_RequestDescription.ClientID%>").innerHTML;      
        }
        
        //window.onload=init;
        
    </script>
</head>

<body class="page-iframe">
<form id="form1" action="" method="" runat="server">

<div id="div_wrap" class="main-wrap" style="display: block;">
    <div id="div_form" class="box2" style="display: block;">
       <div class="left"> 
            <div class="form-phone">
              <div class="title"><label for="">Meu Telefone</label></div>
              <div class="numbers"><input type="text" id="txt_phoneAreaCode" class="form-text2 form-areacode2" />
              <input type="text" id="form-call" class="form-text2 form-phonenumber2" /></div>
            </div>
       </div>
         <div class="right">
            <div class="form-field form-details">
                <div class="label-wrap">
                    <label for="form_details">Detalhamento do Pedido</label>
                    <span id="lbl_RequestDescription" class="letter-count" runat="server">1/140</span>
                </div>
          
            <asp:TextBox id="formMsg" runat="server" TextMode="MultiLine" onmousedown="makeOnceClear();this.focus();"></asp:TextBox>

      

            </div>
             
            
         
           
        </div>
        <div class="supp">Quero que
            <select id="formSuppliers" class="form-select" runat="server"></select>
            fornecedores liguem para mim <span style="font-size:13px; font-weight:bold;">agora</span>
            </div>
            <!--<input type="submit" class="form-submit" value="Enviar" />-->
        <input id="div_initiateCalls" onclick="javascript:return validation();" type="button" class="form-submit2" value="Enviar" />        
        
    </div>    
    
    <div id="div_explanationParent" class="box2 messages-wrap2" style="display: none;">
        <img src="images/loading2.gif" alt="Loading..." class="loading" width="34" height="30"/>
        <p id="div_explanation1" class="msg msg1" style="display: block;"><strong>Obrigado pela sua solicitação!</strong></p>
        <p id="div_explanation2" class="msg msg2" style="display: none;">Estamos ligando para empresas de <strong>Mudanças</strong> disponíveis e o(a) colocaremos em contato com eles em um minuto.</p>
        <p id="div_explanation3" class="msg msg3">Por favor, certifique-se que você está disponível para receber ligações.</p>
    </div>
    
        
    <div class="links">
    <%-- 
        <a href="#">Instruções de uso</a> |
        <a href="#">Saiba como anunciar</a>
    --%>
    </div>
</div>

<div id="div_list" class="list-results2" style="display: none;">
<%-- 
    <ul>
        <li>
            <div class="title clearfix">
                <h3>Cobrak Express Transportes...</h3>
                <a href="#" class="btn-call" style="display:none;">chamando</a>
            </div>
            <div class="item-details clearfix">
                <p><img src="images/logo.png" alt="Suppliers logo" class="logo" />Coleta, Entregas Rápidas, Armazenagem, Distribuição e Mudanças. Transportes para todo o Brasil</p>
            </div>
        </li>
        <li>
            <div class="title clearfix">
                <h3>Cobrak Express Transportes...</h3>
                <a href="#" class="btn-call">chamando</a>
            </div>
            <div class="item-details clearfix">
                <p><img src="images/logo.png" alt="Suppliers logo" class="logo" />Coleta, Entregas Rápidas, Armazenagem, Distribuição e Mudanças. Transportes para todo o Brasil</p>
            </div>
        </li>
        <li>
            <div class="title clearfix">
                <h3>Cobrak Express Transportes...</h3>
                <a href="#" class="btn-call">chamando</a>
            </div>
            <div class="item-details clearfix">
                <p><img src="images/logo.png" alt="Suppliers logo" class="logo" />Coleta, Entregas Rápidas, Armazenagem, Distribuição e Mudanças. Transportes para todo o Brasil</p>
                
            </div>
        </li>
        <li>
            <div class="title clearfix">
                <h3>Cobrak Express Transportes...</h3>
                <a href="#" class="btn-call">chamando</a>
            </div>
            <div class="item-details clearfix">
                <p><img src="images/logo.png" alt="Suppliers logo" class="logo" />Coleta, Entregas Rápidas, Armazenagem, Distribuição e Mudanças. Transportes para todo o Brasil</p>
               
            </div>
        </li>
        <li>
            <div class="title clearfix">
                <h3>Cobrak Express Transportes...</h3>
                <a href="#" class="btn-call">chamando</a>
            </div>
            <div class="item-details clearfix">
                <p><img src="images/logo.png" alt="Suppliers logo" class="logo" />Coleta, Entregas Rápidas, Armazenagem, Distribuição e Mudanças. Transportes para todo o Brasil</p>
            </div>
        </li>
    </ul>
--%>
</div>

<asp:HiddenField ID="HiddenNotCorrectPhoneNum" runat="server" Value="O número de telefone não está no formato correto" /><!-- Not Correct Format Phone Number -->	
<asp:HiddenField ID="HiddenMustFulfill" runat="server" Value="É necessário preencher a ‘Descrição do Pedido’" /><!-- Must Fulfill Your Request Description -->     
<asp:HiddenField ID="HiddenMaxLength" runat="server" Value="O tamanho máximo da mensagem é de 140 caracteres" /><!-- Max length of message is 140 characters -->
<asp:HiddenField ID="HiddenCharacters" runat="server" Value="" /><!-- Characters -->
<asp:HiddenField id="hidden_noAvailable" runat="server" Value="Não encontramos nenhuma empresa de mudanças disponível para atender sua solicitação neste momento. Pedimos que tente novamente mais tarde. Obrigado."/><!-- No available advertisers were found for your request please try later or change your search criteria -->			
<asp:HiddenField id="HiddenError" runat="server" Value="sorry, we didn’t find advertisers for your request. We welcome you to try again later"/>			        
<asp:HiddenField id="HiddenRequestDescription" runat="server" Value=""/><!-- Detalhamento do Pedidofh -->			
<asp:HiddenField id="HiddenAlreadyRecieved" runat="server" Value="Confirmamos que nós já recebemos esta sua solicitação. Obrigado por utilizar nosso serviço de solicitação de Orçamentos."  /><!-- We have already recieved your request -->			

</form>
</body>
</html>