﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class teleListasPowerPoint2_parentList : System.Web.UI.Page
{
    //  const string SRC = @"&ExpertiseCode=7&ExpertiseLevel=1&RegionCode=gz&RegionLevel=1&MaxResults=-1&frames=2&origionId={CF879946-455C-DF11-80CD-0003FF727321}";
    //protected string src = @"&ExpertiseCode=1&ExpertiseLevel=1&RegionCode=3&RegionLevel=1&MaxResults=-1&frames=2&origionId={35779B9E-759D-DF11-92C8-A4BADB37A26F}";
   
    protected string src2 = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string _host = Request.ServerVariables["SERVER_NAME"];
            if (string.IsNullOrEmpty(_host))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            
            src2 = GetSrc2(_host);
        }
    }   

   
    //ExpertiseCode=7&ExpertiseLevel=1&RegionCode=ny&RegionLevel=1&MaxResults=-1&origionId={35779B9E-759D-DF11-92C8-A4BADB37A26F}";

    private string GetSrc2(string _host)
    {
        string _src = DBConnection.GetSrc(_host);
        if (string.IsNullOrEmpty(_src))
            Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
        return "list.aspx?SiteId=" + _src + "&style=styleList";
    }
}
