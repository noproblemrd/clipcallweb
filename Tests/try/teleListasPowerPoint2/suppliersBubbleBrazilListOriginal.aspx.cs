using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text;


public partial class BrazilOriginal_suppliersBubbleBrazilListOriginal : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Labels
        Panel _panel = new Panel();

        Label lbl_Calls = new Label();
        lbl_Calls.Text = "Chamadas:";
        lbl_Calls.ID = "lbl_Calls";
        _panel.Controls.Add(lbl_Calls);

        Label lbl_Employees = new Label();
        lbl_Employees.Text = "Employees:";
        lbl_Employees.ID = "lbl_Employees";
        _panel.Controls.Add(lbl_Employees);

        Label lbl_Certificate = new Label();
        lbl_Certificate.Text = "Certificate:";
        lbl_Certificate.ID = "lbl_Certificate";
        _panel.Controls.Add(lbl_Certificate);

        Label lbl_CALLING = new Label();
        lbl_CALLING.Text = "CHMADAS..."; // CALLING...
        lbl_CALLING.ID = "lbl_CALLING";
        _panel.Controls.Add(lbl_CALLING);


        Label lbl_CallNow = new Label();
        lbl_CallNow.Text = "LIGUE JÁ!"; // CALL NOW
        lbl_CallNow.ID = "lbl_CallNow";
        _panel.Controls.Add(lbl_CallNow);






        //string incidentId="{26AD9CDC-6599-DF11-9C13-0003FF727321}";
        string SiteId = Request["SiteId"];
        //if (!string.IsNullOrEmpty(SiteId))
            //GetTranslate(SiteId, _panel);
        string incidentId = Request["incidentId"];
        string IfShowMinisite = Request["IfShowMinisite"];

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(this, SiteId);


        string xmlSuppliers = customer.GetSuppliersStatus(incidentId);

        //xmlSuppliers = "<Suppliers IncidentStatus=\"Init\" ><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";
        //Mode="Initiated"
        //xmlSuppliers="<Suppliers IncidentStatus=\"Active\" Mode=\"Open\"><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";
        //xmlSuppliers = "<Suppliers IncidentStatus=\"Active\" Mode=\"Initiated\"><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";
        //xmlSuppliers = "<Suppliers IncidentStatus=\"Completed\" ><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";

        //WebReferenceSite.Site siteWebService = WebServiceConfig.GetSiteReference(this);
        //string xmlSuppliers = siteWebService.SearchSiteSuppliers(SiteId, ExpertiseGUID, CityGuid, ExpertiseType, MaxResults);

        //Response.Write("SiteId:" + SiteId + " ExpertiseGUID:" + ExpertiseGUID + " CityGuid:" + CityGuid + " ExpertiseType:" + ExpertiseType + " MaxResults:" + MaxResults);
        //Response.Write(xmlSuppliers);

        System.Text.UTF8Encoding myEncoder = new System.Text.UTF8Encoding();
        byte[] bytes = myEncoder.GetBytes(xmlSuppliers);
        MemoryStream ms = new MemoryStream(bytes);
        XmlTextReader xmlTextReader = new XmlTextReader(ms);
        string SupplierId = "";
        string SupplierNumber = "";
        string SupplierName = "";
        string SumSurvey = "";
        string SumAssistanceRequests = "";
        string NumberOfEmployees = "";
        string Certificate = "";
        string ShortDescription = "";
        string isAvailable = "";
        string DirectNumber = "";
        bool blnlogo = false;
        int index = 0;
        string classIndex = "";
        string Status = "";
        string ExtensionNumber = "";
        string professionalLogos = "";
        string professionalLogosWeb = "";

        StringBuilder sb = new StringBuilder();

        /*
         <Suppliers>
            <Supplier>
            <SupplierId />
            <SupplierNumber />
            <SupplierName />
            <SumSurvey />
            <SumAssistanceRequests />
            <NumberOfEmployees />
            <ShortDescription/ >
            <DirectNumber />
            </Supplier>
         </Suppliers>
        */


        /*
          <div id="" class="comp_div">
            <div id="" class="comp_header">
                <p class="comp_name">Cobrak Express Transportes...</p>
                <span class="call_button"></span>
                <div id="" class="about">
                <img src="images/logo.png" width="77" height="42" alt="" />
	                <span>Coleta, Entregas Rápidas, Armazenagem, Distribuição e Mudanças. Transportes para todo o Brasil</span>
                </div>
            </div>
          </div>
        */

        while (xmlTextReader.Read())
        {
            if (xmlTextReader.Name == "Suppliers")
            {
                sb.Append("<div style='display:none;'>mode=" +
                    xmlTextReader.GetAttribute("Mode") + "mode2;IncidentStatus=" +
                    xmlTextReader.GetAttribute("IncidentStatus") + "IncidentStatus2</div>");

                LogEdit.SaveLog(SiteId, "Session: " + Session.SessionID + " incidentId:" + incidentId + " Mode:" +
                    xmlTextReader.GetAttribute("Mode") +
                    " IncidentStatus:" + xmlTextReader.GetAttribute("IncidentStatus"));

                sb.Append("<ul>");
                /*
                <ul>
        <li>
            <div class="title clearfix">
                <h3>Cobrak Express Transportes...</h3>
                <a href="#" class="btn-call" style="display:none;">chamando</a>
            </div>
            <div class="item-details clearfix">
                <p><img src="images/logo.png" alt="Suppliers logo" class="logo" />Coleta, Entregas Rápidas, Armazenagem, Distribuição e Mudanças. Transportes para todo o Brasil</p>
            </div>
        </li>
            */

                if (xmlTextReader.GetAttribute("ExtensionNumber") != null)
                {
                    ExtensionNumber = xmlTextReader.GetAttribute("ExtensionNumber");

                    /*
                    if (ExtensionNumber != "")
                    {
                        sb.Append("<span class='phone'>");
                        sb.Append("Call us now :" + ExtensionNumber);
                        sb.Append("</span>");
                    }
                    */
                }

                //sb.Append("<ul class='records'>");

                while (xmlTextReader.Read())
                {
                    index++;                    

                    if (xmlTextReader.Name == "Supplier")
                    {

                        sb.Append("<li>");
                        
                        
                        SupplierId = xmlTextReader.GetAttribute("SupplierId");
                        SupplierNumber = xmlTextReader.GetAttribute("SupplierNumber");
                        SupplierName = xmlTextReader.GetAttribute("SupplierName");
                        DirectNumber = xmlTextReader.GetAttribute("DirectNumber");
                        ShortDescription = xmlTextReader.GetAttribute("ShortDescription");
                        //SumSurvey = xmlTextReader.GetAttribute("SumSurvey");
                        SumAssistanceRequests = xmlTextReader.GetAttribute("SumAssistanceRequests");
                        NumberOfEmployees = xmlTextReader.GetAttribute("NumberOfEmployees");
                        Certificate = xmlTextReader.GetAttribute("Certificate");
                        isAvailable = xmlTextReader.GetAttribute("isAvailable");

                        Status = xmlTextReader.GetAttribute("Status");                       
                       
                        /*
                        if (Status == "Open" || Status == "Initiated" || Status == "Succeed")
                            sb.Append("<li class='active-supplier'>");
                        else
                            sb.Append("<li>");

                        */

                        professionalLogos = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
                        professionalLogosWeb = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];

                        //sb.Append("<p class='comp_name'>" + SupplierName + "...</p>");

                        sb.Append("<div class='title clearfix'>");

                        if (IfShowMinisite == "True")
                        {
                            sb.Append("<h3><a href='../professional.aspx?supplierGUID={" + SupplierId + "}&SiteId=" + SiteId +
                            "' target='_new'>" + SupplierName + "</a></h3>");
                        }
                        else
                            sb.Append("<h3>" + SupplierName + "</h3>");

                        if (Status == "Initiated")
                        {
                            sb.Append("<span class='calling'>");
                            sb.Append("<img src='images/icon-call.gif' class='icon-calling' style='display:block;'>");
                            sb.Append("<span class='text'>chamando</span>");
                            sb.Append("</span>");
                        }
                        sb.Append("</div>");


                       

                        sb.Append("<div class='item-details clearfix'>");

                        blnlogo = File.Exists(professionalLogos + SupplierId + ".jpg");


                        if (blnlogo)
                        {
                            sb.Append("<p><img src='" + professionalLogosWeb + SupplierId + @".jpg' width='77' height='42' alt='Suppliers logo' class='logo' />" +
                                ShortDescription + "</p>");
                        }
                        else
                        {
                            sb.Append("<p><img src='images/logo.png' alt='Suppliers logo' width='77' height='42' class='logo' />" +
                            ShortDescription + "</p>");
                        }

                        
                        sb.Append("</div>");
                        
                        sb.Append("<li>");
                    }

                    if (xmlTextReader.Name == "Suppliers" & xmlTextReader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }
                }

                sb.Append("</ul>");
                //Response.Write(xmlTextReader.Name);
                //Response.Write(xmlTextReader.GetAttribute("Id"));
                //Response.Write(xmlTextReader.GetAttribute("Name"));
                //arrProfessions.Add(new Profession(xmlTextReader.GetAttribute("Id"), xmlTextReader.GetAttribute("Name")));
            }


        }

        Response.Write(sb.ToString());
    }

    /*
    void GetTranslate(string SiteId, Control control)
    {
        string pagename =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        try
        {
            dic = DBConnection.GetTranslate(pagename, SiteId);
        }
        catch (Exception ex)
        {
        }
        InsertTranslate it = new InsertTranslate(control, dic);
        it.StartTo();
    }
    */

}
