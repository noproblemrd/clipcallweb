﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;

public partial class try_regex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        Regex objNotPositivePattern = new Regex("\\.{3,3}$");
        string myString = "....";
        */


        Regex objNotPositivePattern = new Regex("^\\.{3,3}[0-9]{4,4}$");
        string myString = "...5677";
        if (objNotPositivePattern.IsMatch(myString))
            Response.Write("true");

        // eample for just numbers
        Regex objNumbers = new Regex("^[0-9]+$");
        string myString2 = "45464564";
        if (objNumbers.IsMatch(myString2))
            Response.Write("<br>true");

    }
}
