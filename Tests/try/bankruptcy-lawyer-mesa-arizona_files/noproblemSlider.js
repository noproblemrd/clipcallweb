﻿var intervalNoProblem;
var widtDivSliderNoProblem;
var heightDivSliderNoProblem;
var divSliderEndPosNoProblem;
var divSliderStartPosNoProblem;
var newLeftNoProblem;
var newLefPxNoProblem;
var counter_cookiesNoProblem = 0;
var cookie_nameNoProblem = "NoProblemCounterClose2Eng";
//var arrSliderPos = new Array();
var divSliderEndPosBigLeftNoProblem;
var divSliderEndPosBigTopNoProblem;
var ifKeywordValid=true;
var ifKeywordParamExist = true;
var ifExpertisecodeExist = true;

String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
}

function moveSlideNoProblem(mode) {

    document.getElementById("tabNoProblem").style.display = 'none';
    clearInterval(intervalNoProblem);

    if (mode == "show") {
        newLeftNoProblem = parseInt(document.getElementById("divSliderNoProblem").style.left) - 4;
        if (divSliderEndPosNoProblem < newLeftNoProblem) {
            newLefPxNoProblem = newLeftNoProblem + "px";
            document.getElementById("divSliderNoProblem").style.left = newLefPxNoProblem;
            intervalNoProblem = setTimeout("moveSlideNoProblem('show')", 1);
        }
        else {
            clearTimeout(intervalNoProblem);

        }
    }

    else if (mode == "hide") {
        newLeftNoProblem = parseInt(document.getElementById("divSliderNoProblem").style.left) + 4;
        if (divSliderStartPosNoProblem >= newLeftNoProblem) {
            newLefPxNoProblem = newLeftNoProblem + "px";
            document.getElementById("divSliderNoProblem").style.left = newLefPxNoProblem;
            intervalNoProblem = setTimeout("moveSlideNoProblem('hide')", 1);
        }
        else {
            document.getElementById("tabNoProblem").style.display = 'block';
            clearTimeout(intervalNoProblem);

        }
    }

}



function moveSlideJqueryNoProblem(mode) {

    document.getElementById("tabNoProblem").style.display = 'none'; 

    if (mode == "show") {
        jQuery(".sliderNoProblem").animate({ "left": divSliderEndPosNoProblem }, 1000);

    }

    else if (mode == "hide") {
        jQuery(".sliderNoProblem").animate({ "left": divSliderStartPosNoProblem }, 1000, function () { document.getElementById("tabNoProblem").style.display = 'block'; });
    }

}

function moverTabHover() {    
    jQuery("#tabNoProblem").animate({ "left": -46 }, 100);
}

function moverTabOut() {
    jQuery("#tabNoProblem").animate({ "left": -34 }, 100);
}

function makeWiderNoProblem() {
    document.getElementById("flavourANoProblem").style.display = 'block';
    document.getElementById("divSliderNoProblem").style.backgroundColor = '#000';
    jQuery(".sliderNoProblem").animate({ "left": divSliderEndPosBigLeftNoProblem, "top": divSliderEndPosBigTopNoProblem, width: 622, height: 286 }, 500);
    jQuery(".flavourBNoProblem").animate({ "opacity": 0 }, 500, function () { updateSliderPosNoProblem(); document.getElementById("divSliderNoProblem").style.boxShadow = "1px 1px 5px #666666"; document.getElementById("flavourBNoProblem").style.display = 'none'; });
}

function setSliderPosNoProblem() {
    //alert("setSliderPos");
    var myWidth = 0;
    var myHeight = 0;
    var innerWidth = 0;
    var innerHeight = 0;

    if (typeof (window.innerWidth) == 'number') {
        //Non-IE and IE9 strict
        //myWidth = window.innerWidth;
        //innerWidth = myWidth - 12; // minus scroll
        innerHeight = window.innerHeight;
        //alert("1");
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode' strict
        //myWidth = document.documentElement.clientWidth;
        //innerWidth = myWidth;  
        innerHeight = document.documentElement.clientHeight;
        //alert("2");
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        //myWidth = document.body.clientWidth;
        //innerWidth = myWidth;  
        innerHeight = document.body.clientHeight;
        //alert("3");
    }


    innerWidth = document.documentElement.clientWidth;

    widtDivSliderNoProblem = parseInt(document.getElementById("divSliderNoProblem").style.width); // 2 is the border
    heightDivSliderNoProblem = parseInt(document.getElementById("divSliderNoProblem").style.height);

    document.getElementById("divSliderNoProblem").style.left = innerWidth + "px";
    document.getElementById("divSliderNoProblem").style.top = innerHeight - parseInt(heightDivSliderNoProblem) - posYbottomNoProblem + "px";
    divSliderEndPosBigTopNoProblem = innerHeight - 286 - posYbottomNoProblem;

    divSliderStartPosNoProblem = innerWidth;
    divSliderEndPosNoProblem = innerWidth - widtDivSliderNoProblem;
    divSliderEndPosBigLeftNoProblem = innerWidth - 622;

    if (loadAutomaticOnceNoProblem == true && get_cookieNoProblem(cookie_nameNoProblem)) {
        document.getElementById("tabNoProblem").style.display = 'block';
    }
    else {
        moveSlideJqueryNoProblem('show');
    }

}

function updateSliderPosNoProblem() {

    var innerWidth = 0;
    var innerHeight = 0;

    if (typeof (window.innerWidth) == 'number') {
        //Non-IE and IE9 strict
        //myWidth = window.innerWidth;
        //innerWidth = myWidth - 12; // minus scroll
        innerHeight = window.innerHeight;
        //alert("1");
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode' strict
        //myWidth = document.documentElement.clientWidth;
        //innerWidth = myWidth;  
        innerHeight = document.documentElement.clientHeight;
        //alert("2");
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        //myWidth = document.body.clientWidth;
        //innerWidth = myWidth;  
        innerHeight = document.body.clientHeight;
        //alert("3");
    }


    innerWidth = document.documentElement.clientWidth;

    widtDivSliderNoProblem = 622; // 2 is the border
    heightDivSliderNoProblem = 286;

    divSliderStartPosNoProblem = innerWidth;
    divSliderEndPosNoProblem = innerWidth - widtDivSliderNoProblem;

}

function setSliderPosAfterResizeNoProblem() {
    //alert("setSliderPosAfterResize");   
    document.getElementById("tabNoProblem").style.display = 'none';

    var myWidth = 0;
    var myHeight = 0;
    var innerWidth = 0;
    var innerHeight = 0;


    if (typeof (window.innerWidth) == 'number') {
        //Non-IE and IE9 strict
        //myWidth = window.innerWidth;
        //innerWidth = myWidth - 12; // minus scroll
        innerHeight = window.innerHeight;
        //alert("1");
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode' strict
        //myWidth = document.documentElement.clientWidth;
        //innerWidth = myWidth;  
        innerHeight = document.documentElement.clientHeight;
        //alert("2");
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        //myWidth = document.body.clientWidth;
        //innerWidth = myWidth;  
        innerHeight = document.body.clientHeight;
        //alert("3");
    }


    innerWidth = document.documentElement.clientWidth;


    widtDivSliderNoProblem = parseInt(document.getElementById("divSliderNoProblem").style.width); // 2 is the border
    heightDivSliderNoProblem = parseInt(document.getElementById("divSliderNoProblem").style.height);


    if (loadAutomaticOnceNoProblem == true && get_cookieNoProblem(cookie_nameNoProblem)) {

        document.getElementById("tabNoProblem").style.display = 'block';
        document.getElementById("divSliderNoProblem").style.left = innerWidth + "px"; // 2 is the border        
        document.getElementById("divSliderNoProblem").style.top = innerHeight - parseInt(heightDivSliderNoProblem) - posYbottomNoProblem + "px";
    }
    else {

        document.getElementById("divSliderNoProblem").style.left = innerWidth - parseInt(widtDivSliderNoProblem) + "px"; // 2 is the border        
        document.getElementById("divSliderNoProblem").style.top = innerHeight - parseInt(heightDivSliderNoProblem) - posYbottomNoProblem + "px";

        divSliderStartPosNoProblem = innerWidth;
        divSliderEndPosNoProblem = innerWidth - widtDivSliderNoProblem;
    }



}

function closeMeNoProblem() {

    moveSlideJqueryNoProblem('hide');
}

function setProfessionNoProblem(Page, path, expertiseName, expertiseCode) {    
    ExpertiseCodeNoProblem = expertiseCode;
    ExpertiseNameNoProblem = expertiseName;

    params = getParamsNoProblem('link');
    //alert(params);   
    var allPath = path + Page + "?" + params;
    document.getElementById("flavourBNoProblem").style.display = 'none';
    document.getElementById("iframeNoProblem").src = allPath;

    var strTitle = "";
    strTitle = "COMPARE ";
    if (ExpertiseCodeNoProblem != "" && ExpertiseCodeNoProblem != undefined)
        strTitle += ExpertiseNameNoProblem.toUpperCase();
    else
        strTitle += "PROVIDERS";

    if (RegionNameNoProblem != "" && RegionNameNoProblem != undefined)
        strTitle += " IN " + RegionNameNoProblem.toUpperCase();

    document.getElementById("titleFlavourA").innerHTML = strTitle;
}


function setSliderNoProblem(params, ExpertiseName, RegionName, path) {
    //Compare Movers is new yoork;
    
    var strTitle = "";
    strTitle = "COMPARE ";
    
    if (ExpertiseNameNoProblem != "" && ExpertiseNameNoProblem != undefined) {
        strTitle += ExpertiseNameNoProblem.toUpperCase();
    }
    else {
        strTitle += "PROVIDERS";
    }    

    if (RegionNameNoProblem != "" && RegionNameNoProblem != undefined)
        strTitle += " IN " + RegionNameNoProblem.toUpperCase();

    try {
        if (PageNoProblem == undefined || PageNoProblem == "") {
            PageNoProblem = "supplierIframe.aspx";
        }

        if (loadAutomaticOnceNoProblem == undefined || loadAutomaticOnceNoProblem == "") {
            loadAutomaticOnceNoProblem = false;
        }

    }

    catch (e) {
        PageNoProblem = "supplierIframe.aspx";
        loadAutomaticOnceNoProblem = false;
    }    

    arrSplitParams = params.split('&');
    var flavour;

    for (i = 0; i < arrSplitParams.length; i++) {

        arrSplitParam = arrSplitParams[i].split('=');
        if (arrSplitParam[0] == "ControlName") {
            if (arrSplitParam[1].indexOf('flavour a') != -1)
                flavour = 'flavour a';
            else if (arrSplitParam[1].indexOf('flavour b') != -1)
                flavour = 'flavour b';
            else if (arrSplitParam[1].indexOf('flavour c') != -1)
                flavour = 'flavour c';
            else if (arrSplitParam[1].indexOf('flavour d') != -1)
                flavour = 'flavour d';
            else if (arrSplitParam[1].indexOf('flavour e') != -1)
                flavour = 'flavour e';
            else if (arrSplitParam[1].indexOf('flavour f') != -1)
                flavour = 'flavour f';
        }
    }


    if (flavour == 'flavour a') {

        document.getElementById("divSliderNoProblem").style.width = '622px';
        document.getElementById("divSliderNoProblem").style.height = '286px';
        document.getElementById("divSliderNoProblem").style.boxShadow = "1px 1px 5px #666666";

        var str = "<div id=\"flavourANoProblem\" class=\"flavourANoProblem\">";
        str += "<div id=\"titleNoProblem\" class=\"titleNoProblem\">";
        str += "<div id=\"textNoProblem\" class=\"textNoProblem\">" + strTitle + "</div>";
        str += "<div id=\"iconsNoProblem\" class=\"iconsNoProblem\"><span onclick=\"closeMeNoProblem();disableLoadMoveNoProblem();\" class=\"closeNoproblem\">x</span></div>";
        str += "</div>";

        str += "<div id=\"iframe\" class=\"subMain\">";
        str += "<iframe id=\"iframeNoProblem\" class=\"iframeNoProblem\" scrolling=\"no\" frameborder=\"0\" src=\"" + path + PageNoProblem + "?" + params + "\" width=\"614\" height=\"232\"></iframe>";
        str += "</div>";
        str += "</div>";
    }

    else if (flavour == 'flavour b') {
        document.getElementById("divSliderNoProblem").style.width = '421px';
        document.getElementById("divSliderNoProblem").style.height = '200px';
        document.getElementById("divSliderNoProblem").style.backgroundColor = 'transparent';

        var str = "<div id=\"flavourANoProblem\" class=\"flavourANoProblem\" style='display:none;'>";
        str += "<div id=\"title\" class=\"title\">";
        str += "<div id=\"titleFlavourA\" class=\"text\">" + strTitle + "</div>";
        str += "<div id=\"icons\" class=\"icons\"><span onclick=\"closeMeNoProblem();disableLoadMoveNoProblem();\" class=\"closeNoproblem\">x</span></div>";
        str += "</div>";

        str += "<div id=\"iframe\" class=\"subMain\">";
        str += "<iframe id=\"iframeNoProblem\" class=\"iframeNoProblem\" scrolling=\"no\" frameborder=\"0\" src=\"" + path + PageNoProblem + "?" + params + "\" width=\"614\" height=\"232\"></iframe>";
        str += "</div>";
        str += "</div>";

        str += "<div id=\"flavourBNoProblem\" class=\"flavourBNoProblem\" style=\"display:block;\">";
        str += "<div class=\"titleB\" id=\"titleB\">";
        str += "<div id=\"textB\" class=\"textB\" >COMPARE PROVIDERS</div>";
        str += "<div id=\"iconsB\" class=\"iconsB\" ><span class=\"close\" onclick=\"closeMeNoProblem();disableLoadMoveNoProblem();\">X</span></div>";
        str += "</div>";

        str += "<div class=\"main\">";
        str += "<div class=\"left\">";
        str += "<div class=\"leftTitle\">WHO WOULD YOU LIKE TO TALK TO?</div>";

        str += "<div class=\"leftLinks\">";



        try {


            if (ExpertiseArrayNoProblem != undefined && ExpertiseArrayNoProblem != "" && ExpertiseArrayNoProblem.length > 0) {
                var pos;

                for (i = 0; i < ExpertiseArrayNoProblem.length; i++) {
                    if (i % 2 == 0)
                        pos = "Left";
                    else
                        pos = "Right";

                    str += "<div class=\"leftLinks" + pos + " \"><a href='javascript:void(0);' onclick='makeWiderNoProblem();setProfessionNoProblem(\"" + PageNoProblem + "\",\"" + path +
                    "\",\"" + ExpertiseArrayNoProblem[i][0] + "\",\"" + ExpertiseArrayNoProblem[i][1] + "\")'>" + ExpertiseArrayNoProblem[i][0] + "</a></div>";

                    if (i % 2 == 0) {
                    }
                    else
                        str += "<div class=\"clearNoProblem\"></div>";


                }
            }

        }


        catch (e) {

            str += "<div class=\"leftLinksLeft\"><a href='javascript:void(0);' onclick='makeWiderNoProblem();setProfessionNoProblem(\"" + PageNoProblem + "\",\"" + path + "\",\"Architects\",\"2014\")'>Architects</a></div>";
            str += "<div class=\"leftLinksRight\"><a href='javascript:void(0);' onclick='makeWiderNoProblem();setProfessionNoProblem(\"" + PageNoProblem + "\",\"" + path + "\",\"Interior Designers\",\"2109\")'>Interior Designers</a></div>";

            str += "<div class=\"clearNoProblem\"></div>";

            str += "<div class=\"leftLinksLeft\"><a href='javascript:void(0);' onclick='makeWiderNoProblem();setProfessionNoProblem(\"" + PageNoProblem + "\",\"" + path + "\",\"Interior Decorators\",\"2109\")'>Interior Decorators</a></div>";
            str += "<div class=\"leftLinksRight\"><a href='javascript:void(0);' onclick='makeWiderNoProblem();setProfessionNoProblem(\"" + PageNoProblem + "\",\"" + path + "\",\"Painters\",\"2146\")'>Painters</a></div>";

            str += "<div class=\"clearNoProblem\"></div>";

            str += "<div class=\"leftLinksLeft\"><a href='javascript:void(0);' onclick='makeWiderNoProblem();setProfessionNoProblem(\"" + PageNoProblem + "\",\"" + path + "\",\"Movers\",\"2137\")'>Movers</a></div>";
            str += "<div class=\"leftLinksRight\"><a href='javascript:void(0);' onclick='makeWiderNoProblem();setProfessionNoProblem(\"" + PageNoProblem + "\",\"" + path + "\",\"Handymen\",\"2100\")'>Handymen</a></div>";

        }


        str += "</div>";

        str += "</div>";

        str += "</div>";


        str += "<div class=\"footerNoProblem\">";
        str += "<div class=\"textFooter\">";
        str += "Service provider? <a href=\"http://www.noproblemppc.com/adv.html\" class=\"linkFooter\" target=\"_blank\">Advertise with us</a>";

        str += "</div>";

        str += "<div class=\"logoFooter\">";

        str += "</div>";
        str += "</div>";
        str += "</div>";




        str += "</div>";
    }

    else if (flavour == 'flavour c' || flavour == 'flavour d') {

        document.getElementById("divSliderNoProblem").style.width = '421px';
        document.getElementById("divSliderNoProblem").style.height = '200px';
        document.getElementById("divSliderNoProblem").style.backgroundColor = 'transparent';


        var str = "<div id=\"flavourA\" class=\"flavourA\" style='display:none;'>";

        str += "<div id=\"title\" class=\"title\">";
        str += "<div id=\"titleFlavourA\" class=\"text\">" + strTitle + "</div>";
        str += "<div id=\"icons\" class=\"icons\"><span onclick=\"closeMe();disableLoadMove();\" class=\"close\">x</span></div>";
        str += "</div>";



        str += "<div id=\"iframe\" class=\"subMain\">";

        str += "<iframe id=\"iframeNoProblem\" class=\"iframe\" scrolling=\"no\" frameborder=\"0\" src=\"" + path + PageNoProblem + "?" + params + "\" width=\"614\" height=\"232\"></iframe>";

        str += "</div>";

        str += "</div>";


        str += "<div id=\"flavourC\" class=\"flavourC\" style=\"display:block;\">";
        str += "<div class=\"titleB\" id=\"titleB\">";
        str += "<div id=\"textB\" class=\"textB\" >COMPARE PROVIDERS</div>";
        str += "<div id=\"iconsB\" class=\"iconsB\" ><span class=\"close\" onclick=\"closeMe()\">X</span></div>";
        str += "</div>";

        str += "<div class=\"main\">";
        str += "<div class=\"left\">";
        str += "<div class=\"leftTitle\">I NEED...</div>";


        //str += "<form>";

        str += "<div class='message'>";
        str += "<textarea id='msgNoProblem' cols=10 rows=20 ></textarea>";
        str += "</div>";



        str += "<div class=submit>";
        str += "<input id=\"submitNoProblem\" type=\"button\" class=\"button\" value=\"Next\" onclick=\"goToFlavourA(document.getElementById('msgNoProblem').value);\" onmouseover='setBackroundNoProblem(imgSubmitNoProblemHover.src,this.id)' onmouseout='setBackroundNoProblem(imgSubmitNoProblem.src,this.id)' />";
        str += "</div>";

        //str += "</form>";

        str += "</div>";



        str += "<div class=\"right\">";
        str += "<div id=\"rightExplanation\" class=\"rightExplanation\" style=\"display:block;\">";
        str += "<div class=\"ribbon\">";
        str += "<div class=\"textRibbon\">FREE AND SECURE</div>";
        str += "</div>";
        str += "</div>";

        str += "<div class=\"content\">";
        str += "LEAN BACK AND";
        str += "<br />";
        str += "PROVIDERS WILL";
        str += "<br />";
        str += "CALL YOU!";
        str += "</div>";
        str += "</div>";

        str += "</div>";

        str += "<div class=\"footerNoProblem\">";
        str += "<div class=\"textFooter\">";
        str += "Service provider? <a href=\"http://www2.noproblemppc.com/adv.html\" class=\"linkFooter\" target=\"_blank\">Advertise with us</a>";

        str += "</div>";

        str += "<div class=\"logoFooter\">";

        str += "</div>";
        str += "</div>";
        str += "</div>";




        str += "</div>";


    }

    else if (flavour == 'flavour e' || flavour == 'flavour f') {
   
        document.getElementById("divSliderNoProblem").style.width = '619px';
        document.getElementById("divSliderNoProblem").style.height = '329px';
        document.getElementById("divSliderNoProblem").style.backgroundColor = 'transparent';
        document.getElementById("divSliderNoProblem").style.backgroundImage = 'url(images/bg.png)';

        var str = "<div id=\"flavourE\" class=\"flavourE\" style='display:block;'>";
                str += "<div id=\"title\" class=\"title3\">";
                str += "<div id=\"titleFlavourE\" class=\"text\">" + strTitle + "</div>";
                str += "<div id=\"icons\" class=\"icons\"><span onclick=\"closeMeNoProblem();disableLoadMoveNoProblem();\" class=\"closeNoproblem\">x</span></div>";
                str += "</div>";

                var class3;

                if (flavour == 'flavour e')
                    class3 = 'subMain3e';
                else if (flavour == 'flavour f')
                    class3 = 'subMain3f';

                str += "<div id=\"iframe\" class=\"" + class3 + "\">";

                str += "<iframe id=\"iframeNoProblem\" class=\"iframe3\" scrolling=\"no\" frameborder=\"0\" allowTransparency=\"true\" src=\"" + path + "supplierIframe3.aspx?" + params + "\"></iframe>";

                str += "</div>";


            /*
            str += "<div id=\"main\" class=\"main\">";
            str += "<div id=\"left\" class=\"left\">";
                str += "<div class=\"slogan\">";
                    str += ExpertiseNameNoProblem;
                    str += "<br>FIGHT FOR YOU!";
                str += "</div>";
            str += "</div>";
            str += "<div id=\"right\" class=\"right\">";
                str += "<div class=\"ribbon\">";
                    str += "<div class=\"textRibbon\">FREE AND SECURE";
                    str += "</div>";
               str += "</div>";
               str += "<div class=\"content\">";
                str += "Call now to get the best quote:";
                str += "</div>";

                str += "<div class=\"content2\">";
                str += "(575) 219-4781";
                str += "</div>";

            str += "</div>";
            str += "</div>";

            str += "<div class=\"footerNoProblem\">";
                str += "<div class=\"textFooter\">";
                    str += "Service provider? <a href=\"http://www2.noproblemppc.com/adv.html\" class=\"linkFooter\" target=\"_blank\">Advertise with us</a>";

                str += "</div>";

                str += "<div class=\"logoFooter\">";

                str += "</div>";
            str += "</div>";
            */

        str += "</div>";
    }

    if (flavour == 'flavour e' || flavour == 'flavour f')
        str += "<div id=\"tabNoProblem\" class=\"tab3\" onclick=\"moveSlideJqueryNoProblem('show')\" onmouseover=\"moverTabHover();\" onmouseout=\"moverTabOut();\"><img src=\"" + PathNoProblem + "images/compare.png\" /></div>";
    else {
        str += "<div id=\"tabNoProblem\" class=\"tab\" onclick=\"moveSlideJqueryNoProblem('show')\"><img src=\"" + PathNoProblem + "images/tab.png\" /></div>";
    }

    document.getElementById("divSliderNoProblem").innerHTML = str;


}

function getParamsNoProblem(status) {
    var params = "";
    params = "SiteId=" + SiteIdNoProblem;
    params += "&ExpertiseCode=" + ExpertiseCodeNoProblem;
    params += "&ExpertiseLevel=" + ExpertiseLevelNoProblem;

    params += "&ExpertiseName=" + ExpertiseNameNoProblem;

   
    try {
        if (keywordNoProblem != undefined)
            params += "&Keyword=" + keywordNoProblem.toLowerCase().trim();
    }

    catch (e) {
        ifKeywordParamExist = false;
    }
   
    //alert(params);  

    params += "&RegionCode=" + RegionCodeNoProblem;
    params += "&RegionLevel=" + RegionLevelNoProblem;
    params += "&RegionName=" + RegionNameNoProblem;
    params += "&origionId=" + origionIdNoProblem;
    params += "&waterMarkComment=" + waterMarkCommentNoProblem;
    params += "&PageName=" + PageNameNoProblem;
    params += "&PlaceInWebSite=" + PlaceInWebSiteNoProblem;

    if (status == "init") {
        if(ifKeywordParamExist==false) // if there isn't parameter
            ControlNameNoProblem += " flavour b";
        else if (ifKeywordValid == false) // if there is parameter but the it is not valid
            ControlNameNoProblem += " flavour b";

        else if (ExpertiseCodeNoProblem.length > 0) {
            //// var randomnumber = Math.floor(Math.random() * 3);            

            //// if (randomnumber == 0)
            ControlNameNoProblem += " flavour a";
            //// else if (randomnumber == 1)
            ////ControlNameNoProblem += " flavour e";
            ////else if (randomnumber == 2)
            //// ControlNameNoProblem += " flavour f";            

        }

        else if (keywordNoProblem.length > 0) {
            //// var randomnumber = Math.floor(Math.random() * 3);
            //// if (randomnumber == 0)
            ControlNameNoProblem += " flavour a";
            //// else if (randomnumber == 1)
            ////ControlNameNoProblem += " flavour e";
            ////else if (randomnumber == 2)
            ////ControlNameNoProblem += " flavour f";           
        }
        else
            ControlNameNoProblem += " flavour b";
    }

    //alert("ControlNameNoProblem: " + ControlNameNoProblem);
    try {
        if (showNotConnectedNoProblem != undefined)
            params += "&showNotConnected=" + showNotConnectedNoProblem;
    }

    catch (e) {
    }

    params += "&ControlName=" + ControlNameNoProblem;
    params += "&Domain=" + DomainNoProblem;
    params += "&Url=" + encodeURIComponent(UrlNoProblem.substring(0, 200));

    return params;
}

function callbackNoproblem(arg) {
    //alert(arg);
    if (typeof arg == undefined || arg.length == 0) {
        if (keywordNoProblem.length > 0) { // if keyword exist but can't find appropriate expertise or is mistake
            ifKeywordValid = false;
        }
    }
    else {

        
        ExpertiseNameNoProblem = arg;
        //alert("ExpertiseNameNoProblem: " + ExpertiseNameNoProblem);
         
    }

    setSliderNoProblem(getParamsNoProblem('init'), ExpertiseNameNoProblem, RegionNameNoProblem, PathNoProblem);
    setTimeout('setSliderPosNoProblem()', loadSecondsNoProblem);

}

function getExpertiseRemote() {
    

        try {
            if (keywordNoProblem != undefined) {
                ifKeywordParamExist = true;
                var _scr = document.createElement("script");
                _scr.src = PathNoProblem + 'sliderHandler.ashx?callback=callbackNoproblem&Keyword=' + keywordNoProblem.trim();

                /*
                Note: There are several ways an external script can be executed:

                If async="async": The script is executed asynchronously with the rest of the page (the script will be executed while the page continues the parsing)
                If async is not present and defer="defer": The script is executed when the page has finished parsing
                If neither async or defer is present: The script is fetched and executed immediately, before the browser continues parsing the page

                */

                _scr.async = true;
                _scr.setAttribute("type", "text/javascript");
                document.getElementsByTagName("head")[0].appendChild(_scr);  
            }           

        }

        catch (e) {
            
            ifKeywordParamExist = false;
            
            setSliderNoProblem(getParamsNoProblem('init'), ExpertiseNameNoProblem, RegionNameNoProblem, PathNoProblem);
            setTimeout('setSliderPosNoProblem()', loadSecondsNoProblem);
        }

        
    }

    function get_cookieNoProblem(cookie_nameNoProblem) {
    var results = document.cookie.match('(^|;) ?' + cookie_nameNoProblem + '=([^;]*)(;|$)');

    if (results)
    return (unescape(results[2]));
    else
    return null;
    }

    function set_cookieNoProblem(name, value, exp_y, exp_m, exp_d, exp_hh, exp_mm, exp_ss, path, domain, secure) {

    var cookie_string = name + "=" + escape(value);

    if (exp_y) {
    var expires = new Date(exp_y, exp_m, exp_d, exp_hh, exp_mm, exp_ss);

    cookie_string += ";expires=" + expires.toGMTString();

    }

    if (path)
    cookie_string += "; path=" + escape(path);

    if (domain)
    cookie_string += "; domain=" + escape(domain);

    if (secure)
    cookie_string += "; secure";


    document.cookie = cookie_string;

    }



    function disableLoadMoveNoProblem() {
    var current_date = new Date();
    //var cookie_year = current_date.getFullYear();
    var cookie_year = "";
    var cookie_month = current_date.getMonth();

    var cookie_day = current_date.getDate();
    //cookie_day = cookie_day + 30;

    var cookie_hh = current_date.getHours();

    var cookie_mm = current_date.getMinutes();

    var cookie_ss = current_date.getSeconds();

    /* cookies*/


    if (!get_cookieNoProblem(cookie_nameNoProblem)) {
        counter_cookiesNoProblem++;
        set_cookieNoProblem(cookie_nameNoProblem, counter_cookiesNoProblem, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);

    }

    else {
        return false;
    }
}



jQuery.noConflict();






jQuery(document).ready(
   function () {
       
       getExpertiseRemote();
       //setSliderNoProblem(getParamsNoProblem('init'), ExpertiseNameNoProblem, RegionNameNoProblem, PathNoProblem);           

   }
);

jQuery(window).resize(function () {
    setSliderPosAfterResizeNoProblem();
});


