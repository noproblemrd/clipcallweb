﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_try_enumGetValues : System.Web.UI.Page
{
    enum stock
    {
        erbb=1,
        gengr=2,
        eocb=3
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        foreach(stock _stock in Enum.GetValues(typeof(stock)))
        {
            Response.Write(_stock.ToString() + " " + (int)_stock + "<br>");
        }
    }
}