﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="listbox.aspx.cs" Inherits="Tests_try_listbox" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    
    <!--
     <script src="//static0.twilio.com/packages/dashboard.js?48ef379c3b964994dec00e1fbfe69709" type="text/javascript"></script>
    -->
    <!--
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"/>
    <script src="//static0.twilio.com/packages/dashboard.js?48ef379c3b964994dec00e1fbfe69709" type="text/javascript"/>
    -->
    <script>
        /*
        if (typeof twilio == "undefined") var twilio = {}; twilio.realm = "prod"; jQuery.validator.addMethod("phoneUS", function (phone_number, element) { phone_number = phone_number.replace(/\s+/g, ""); return this.optional(element) || phone_number.length > 9 && phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/); }, "Invalid Phone Number"); jQuery.validator.addMethod("phoneEXT", function (extension, element) { extension = extension.replace(/\s+/g, ""); return extension.match(/[0-9*#w ]+/); }, "Please enter a valid extension"); jQuery.validator.addMethod("zipcode", function (zip_code, element) { return this.optional(element) || zip_code.length > 4; }, "Invalid Zipcode"); jQuery.validator.addMethod("url", function (url, element) { url = url.replace(/\s+/g, ""); return this.optional(element) || url.match(/^https?:\/\/(www\.)?([^\.]+)\.(.+)$/i); }, "Invalid URL"); var twilioValidate = { loginform: { rules: { password: { required: true, minlength: 5 }, email: { required: true, email: true} }, messages: { password: { required: "Provide a password", rangelength: jQuery.format("Enter at least {0} characters") }, email: { required: "Please enter a valid email address", minlength: "Please enter a valid email address"} }, errorPlacement: function (error, element) { error.appendTo(element.siblings("div.formerror")); }, highlight: function (element, errorClass) { $(element).addClass("x-form-invalid"); }, unhighlight: function (element, errorClass) { $(element).removeClass("x-form-invalid"); } }, sandboxform: { rules: { SandboxUrl: { required: true, url: true} }, messages: { SandboxUrl: { required: "Please enter a valid URL"} }, errorPlacement: function (error, element) { error.appendTo(element.siblings("div.SandboxUrlError")); }, highlight: function (element, errorClass) { $(element).addClass("x-form-invalid"); }, unhighlight: function (element, errorClass) { $(element).removeClass("x-form-invalid"); } }, callerid: { rules: { PhoneNumber: { required: true, phoneUS: true }, Extension: { phoneEXT: true} }, errorPlacement: function (error, element) { error.appendTo(element.siblings("div.invalid-form")); }, highlight: function (element, errorClass) { $(element).addClass("x-form-invalid"); }, unhighlight: function (element, errorClass) { $(element).removeClass("x-form-invalid"); } }, hurlform: { rules: { HurlUrl: { required: false, url: true }, SmsUrl: { required: false, url: true }, FallbackUrl: { required: false, url: true }, SmsFallbackUrl: { required: false, url: true} }, messages: { HurlUrl: { required: "Please enter a valid URL" }, SmsUrl: { required: "Please enter a valid SMS Url" }, FallbackUrl: { required: "Please enter a valid Fallback Url" }, SmsFallbackUrl: { required: "Please enter a valid Fallback Url"} }, errorPlacement: function (error, element) { error.appendTo(element.siblings("div.invalid-form")); }, highlight: function (element, errorClass) { $(element).addClass("x-form-invalid"); }, unhighlight: function (element, errorClass) { $(element).removeClass("x-form-invalid"); } }, trytwilioform: { rules: { FirstName: { required: true, minlength: 1 }, LastName: { required: true, minlength: 1 }, Passwd: { required: true, minlength: 5 }, Passwd2: { required: true, minlength: 5, equalTo: "#Passwd" }, EmailAddr: { required: true, email: true} }, messages: { FirstName: { required: "Required", minlength: "Required" }, LastName: { required: "Required", minlength: "Required" }, Passwd: { required: "Provide a password", minlength: jQuery.format("Enter at least {0} characters") }, Passwd2: { required: "Provide a password", minlength: jQuery.format("Enter at least {0} characters"), equalTo: "Please enter the same password" }, EmailAddr: { required: "Please enter a valid email address", minlength: "Please enter a valid email address"} }, errorPlacement: function (error, element) { error.appendTo(element.siblings("div")); }, highlight: function (element, errorClass) { $(element).addClass("x-form-invalid"); }, unhighlight: function (element, errorClass) { $(element).removeClass("x-form-invalid"); } }, creditcardformrecurring: { rules: { CardNumber: { required: "#ccNewCard:selected", creditcard: true }, CardholderName: { required: "#ccNewCard:selected" }, ExpMonth: { required: "#ccNewCard:selected", range: [1, 12], digits: true }, ExpYear: { required: "#ccNewCard:selected", range: [2011, 2025], digits: true }, CVV2: { required: "#ccNewCard:selected", minlength: 3, maxlength: 4, digits: true }, StreetAddress: { required: "#ccNewCard:selected" }, Zip: { required: "#ccNewCard:selected", zipcode: true} }, messages: { CardNumber: { required: "Enter a valid credit card number", creditcard: "Enter a valid credit card number" }, ExpMonth: { required: "", range: "", digits: "" }, ExpYear: { required: "Invalid Date", range: "Invalid Date", digits: "Invalid Date" }, Zip: { required: "Invalid Zipcode", minlength: "Invalid Zipcode", maxlength: "Invalid Zipcode", digits: "Invalid Zipcode" }, CVV2: { required: "Invalid CVV", minlength: "Invalid CVV", maxlength: "Invalid CVV", digits: "Invalid CVV"} }, errorPlacement: function (error, element) { error.appendTo(element.siblings("div")); }, highlight: function (element, errorClass) { $(element).addClass("x-form-invalid"); }, unhighlight: function (element, errorClass) { $(element).removeClass("x-form-invalid"); } }, creditcardform: { rules: { CardNumber: { required: true, creditcard: true }, CardholderName: "required", ExpMonth: { required: true, range: [1, 12], digits: true }, ExpYear: { required: true, range: [2011, 2025], digits: true }, CVV2: { required: true, minlength: 3, maxlength: 4, digits: true }, StreetAddress: { required: true }, Zip: { required: true, zipcode: true }, URL: { url: true }, Phone: { phoneUS: true} }, messages: { CardNumber: { required: "Enter a valid credit card number", creditcard: "Enter a valid credit card number" }, ExpMonth: { required: "", range: "", digits: "" }, ExpYear: { required: "Invalid Date", range: "Invalid Date", digits: "Invalid Date" }, Zip: { required: "Invalid Zipcode", minlength: "Invalid Zipcode", maxlength: "Invalid Zipcode", digits: "Invalid Zipcode" }, CVV2: { required: "Invalid CVV", minlength: "Invalid CVV", maxlength: "Invalid CVV", digits: "Invalid CVV"} }, errorPlacement: function (error, element) { error.appendTo(element.siblings("div")); }, highlight: function (element, errorClass) { $(element).addClass("x-form-invalid"); }, unhighlight: function (element, errorClass) { $(element).removeClass("x-form-invalid"); } } }; ; $(document).ready(function () {
            if (!$('#developer-tools #debugger').length) return; var NotificationList = Backbone.Collection.extend({ url: '/user/account/notifications?Log=0&PageSize=5', parse: function (response) { return response.notifications; } }); var NotificationsView = Backbone.View.extend({ initialize: function () { var template = $('#notifications-template').html(); this.template = Handlebars.compile(template); _.bindAll(this, 'render'); this.collection.bind('refresh', this.render); this.collection.view = this; }, render: function (event) {
                var context = {}, collection = this.collection; context.notifications = []; for (var i = 0, l = collection.length; i < l; i++) {
                    var model = collection.models[i].attributes; var date = _.date(new Date(model.message_date)); var messages = model.message_text.split('&'); var message = 'Unknown Error'; for (var j = 0, len = messages.length; j < len; j++) { var pair = decodeURIComponent(messages[j]).split('='); if (pair[0] === 'Msg') { message = pair[1].replace(/\+/g, ' '); } }
                    var dateFormatted = date.format('MMM D'); var timeFormatted = date.format('h:mma'); context.notifications.push({ sid: model.sid, level: 'error', code: model.error_code, message: message, description: message, date: dateFormatted, time: timeFormatted });
                }
                if (context.notifications.length > 0) { this.el.html(this.template(context)); } else { showApiExplorer(); }
                return this;
            } 
            }); var showApiExplorer = function () { $("#debugger").hide(); $("#api-explorer").show(); $("#developer-mode").selectBox("value", "api-explorer-panel"); }
            var notifications = new NotificationList(); var notificationsView = new NotificationsView({ collection: notifications, el: $('#developer-tools .debugger-content') }); notifications.fetch();
        }); ; $(document).ready(function () {
            $('.panel-mode').each(function () { $(this).selectBox(); }); $("#developer-mode").change(function () { if ($("#developer-mode").val() == "debugger-panel") { $("#developer-tools #debugger").show(); $("#developer-tools #api-explorer").hide(); } else { $("#developer-tools #debugger").hide(); $("#developer-tools #api-explorer").show(); } }); var $replace_panel = $('#developer-tools').parent().attr('id') == 'top-left-module' ? $('#top-right-module') : $('#bottom-right-module'); $replace = $replace_panel.children('.panel'); if ($replace.size()) {
                $('#api-credentials-bubble-content').appendTo($replace).hide(); $('#sandbox-bubble-content').appendTo($replace).hide(); $('#sandbox-application-bubble-content').appendTo($replace).hide(); $('.help-bubble-button').click(function (e) {
                    $related = $('#' + this.rel); $('.help-bubble-button').not(this).removeClass('active'); if ($related.is(':visible')) { $(this).removeClass('active'); $related.hide(); $replace.find('.main-content').show(); } else { $(this).addClass('active'); $replace.children('div').not($related).hide(); $related.show(); }
                    e.preventDefault();
                });
            }
            $("#submit-sandbox-panel").attr('disabled', 'disabled'); $("#sandbox-panel-form :input").focusin(function () { $("#submit-sandbox-panel").removeAttr('disabled'); $("#submit-sandbox-panel").addClass("enabled-submit-button"); $("#submit-sandbox-panel").removeClass("disabled-submit-button"); }); $(".show-auth-token").each(function (index, input) { var $input = $(".show-auth-token"); $("#toggle-auth-token, #auth-token-mask[type='password']").live('click', function (e) { e.stopPropagation(); $("#toggle-auth-token").toggleClass('unlocked'); var change = $("#toggle-auth-token").hasClass("unlocked") ? "text" : "password"; var rep = $("<input type='" + change + "' />").attr("id", $input.attr("id")).attr('class', $input.attr('class')).attr("readonly", $input.attr("readonly")).val($input.val()).insertBefore($input); $input.remove(); $input = rep; e.preventDefault(); }); }); $('#sandbox-application-call').click(function () { twilio.client.ui.show(); return false; }); $('#iso-select').change(function () {
                var iso = $(this).val(); if ($('.sandbox-call-number:visible').length) { $('.sandbox-call-number:visible').fadeOut('fast', function () { $('#sandbox-call-number-' + iso).fadeIn('fast'); }); } else { $('#sandbox-call-number-' + iso).show(); }
                if (iso == 'US' || iso == 'CA' || iso == 'GB') { $('#sandbox-sms-fieldset:hidden').fadeIn(); }
                else { $('#sandbox-sms-fieldset:visible').fadeOut(); } 
            }); $('#iso-select').trigger('change'); $('#user-iso-select').bind('iso-changed', function (event, data) { $('#iso-select').selectBox('value', data.value); $('#iso-select').trigger('change'); }); if (Twilio.Device) { Twilio.Device.setup($('#client-capability-token').val()); Twilio.Device.disconnect(function () { twilio.client.ui.hangup(); }); Twilio.Device.cancel(function () { twilio.client.ui.hangup(); }); Twilio.Device.error(function () { twilio.client.ui.hangup(); }); Twilio.Device.offline(function () { twilio.client.ui.hangup(); }); } 
        }); ; $(function () { if ($('#statistics-voice').length) { var endpoint = '/user/account/plots/timeseries.json?range=day'; endpoint += '&services=CALLS_COMPLETED,CALLS_BUSY,CALLS_NOANSWER,CALLS_FAILED'; var labels = [{ label: 'Completed', color: '#009900' }, { label: 'Busy', color: '#3366ff' }, { label: 'No Answer', color: '#000000' }, { label: 'Failed', color: '#ff0000'}]; var flotOptions = analyticsTimeseries.flotOptions; $.extend(flotOptions.xaxis, analyticsTimeseries.xaxisOptions['day']); $.get(endpoint, function (data) { var processed = analyticsTimeseries.processData(data, labels); $.plot($('#phone-call-status-graph'), processed.data, flotOptions); analyticsFlot.legend('#phone-call-status-legend', processed.labels, 'total', 'Total '); analyticsFlot.initTooltip('#phone-call-status-graph'); }); } }); $(function () { if ($('#statistics-sms').length) { var endpoint = '/user/account/plots/timeseries.json?range=day'; endpoint += '&services=SMSS_RECEIVED,SMSS_SENT_API,SMSS_SENT_REPLY,SMSS_SENT_CALL,SMS_SHORTCODE_RECEIVED,SMS_SHORTCODE_SENT_API,SMS_SHORTCODE_SENT_REPLY,SMS_SHORTCODE_SENT_CALL'; var labels = [{ label: 'Incoming', color: '#2CA02C' }, { label: 'Sent API', color: '#1F77B4' }, { label: 'Sent Reply', color: '#9467BD' }, { label: 'Sent Call', color: '#8C564B'}]; var flotOptions = analyticsTimeseries.flotOptions; $.extend(flotOptions.xaxis, analyticsTimeseries.xaxisOptions['day']); $.get(endpoint, function (data) { var summedData = { "SMS_RECEIVED": analyticsTimeseries.sumSets(data.SMSS_RECEIVED, data.SMS_SHORTCODE_RECEIVED), "SMS_SENT_API": analyticsTimeseries.sumSets(data.SMSS_SENT_API, data.SMS_SHORTCODE_SENT_API), "SMS_SENT_REPLY": analyticsTimeseries.sumSets(data.SMSS_SENT_REPLY, data.SMS_SHORTCODE_SENT_REPLY), "SMS_SENT_CALL": analyticsTimeseries.sumSets(data.SMSS_SENT_CALL, data.SMS_SHORTCODE_SENT_CALL) }; var processed = analyticsTimeseries.processData(summedData, labels); $.plot($('#text-message-success-graph'), processed.data, flotOptions); analyticsFlot.legend('#text-message-success-legend', processed.labels, 'total', 'Total '); analyticsFlot.initTooltip('#text-message-success-graph'); }); } });
        */

        /*
         $(document).ready(function () {
            $('.panel-mode').each(function () { $(this).selectBox(); }); $("#developer-mode").change(function () { if ($("#developer-mode").val() == "debugger-panel") { $("#developer-tools #debugger").show(); $("#developer-tools #api-explorer").hide(); } else { $("#developer-tools #debugger").hide(); $("#developer-tools #api-explorer").show(); } }); var $replace_panel = $('#developer-tools').parent().attr('id') == 'top-left-module' ? $('#top-right-module') : $('#bottom-right-module'); $replace = $replace_panel.children('.panel'); if ($replace.size()) {
                $('#api-credentials-bubble-content').appendTo($replace).hide(); $('#sandbox-bubble-content').appendTo($replace).hide(); $('#sandbox-application-bubble-content').appendTo($replace).hide(); $('.help-bubble-button').click(function (e) {
                    $related = $('#' + this.rel); $('.help-bubble-button').not(this).removeClass('active'); if ($related.is(':visible')) { $(this).removeClass('active'); $related.hide(); $replace.find('.main-content').show(); } else { $(this).addClass('active'); $replace.children('div').not($related).hide(); $related.show(); }
                    e.preventDefault();
                
        */

        $(document).ready(function () {

            $(".selectBox-arrow").click(function () {               
                $(".selectBox-dropdown-menu").show();
                $(".selectBox-dropdown").addClass('hhh');                
            }
            );
            //var showApiExplorer = function () { $("#debugger").hide(); $("#api-explorer").show(); $("#developer-mode").selectBox("value", "api-explorer-panel"); }
            //$('.panel-mode').each(function () { $(this).selectBox(); });
        });

        function changeBackground(mode) {
            if (mode == "show") {
                //document.getElementById("container").style.borderWidth = '1px';
                //document.getElementById("<%#fgh.ClientID%>").style.borderWidth = '1px';
                document.getElementById("<%#fgh.ClientID%>").style.border = '1px solid';
            }
            else if (mode == "hide") {
               // document.getElementById("<%#fgh.ClientID%>").style.borderWidth = '0px';
            }
        }  
    </script>
    <style>

        div.RadListBox .rlbText{
        white-space: nowrap;
        }
        
        #developer-tools .selectBox {
            background: none repeat scroll 0 0 transparent;
            border: 1px solid #F5F5F5;
            /*float: right;*/
            margin: 0 10px 0 0;
            width: 90px;
        }
        
        #developer-tools .selectBox-dropdown {
            box-shadow: none;
        }
        
        .selectBox-dropdown {
            background: -moz-linear-gradient(center top , #F8F8F8 1%, #E1E1E1 100%) repeat scroll 0 0 transparent;
            border: 1px solid #BBBBBB;
            border-radius: 6px 6px 6px 6px;
            box-shadow: 0 1px 0 rgba(255, 255, 255, 0.75);
            color: #444444 !important;
            cursor: default;
            display: inline-block;
            font-size: 15px;
            outline: medium none;
            padding: 0 40px 0 4px;
            position: relative;
            text-decoration: none;
            width: 250px;
        }
        
        
  
        .selectBox-dropdown .selectBox-label {
            display: inline-block;
            overflow: hidden;
            padding: 7px 5px;
            white-space: nowrap;
            width: 100%;
        }
        
        #developer-tools .selectBox-arrow {
            border: medium none;
        }
        
        .selectBox-dropdown .selectBox-arrow {
            background: url("https://www.twilio.com//packages/base/images/selectBox-arrow.png") no-repeat scroll 50% center transparent;
            border-left: 1px solid #BBBBBB;
            height: 100%;
            position: absolute;
            right: 0;
            top: 0;
            width: 23px;
        }
        
        #developer-tools .selectBox-arrow {
            border: medium none;
        }
        
        .s
        {
        
        
        border: 0px solid;
       

        }

        a {
            color: #494949;
            outline: medium none;
        }

        .selectBox-dropdown-menu {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #666666;
            border-bottom-left-radius: 6px;
            border-bottom-right-radius: 6px;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2);
            max-height: 200px;
            overflow: auto;
            /*position: absolute;*/
            z-index: 99999;
        }

        .selectBox-options, .selectBox-options li, .selectBox-options li a {
            cursor: default;
            display: block;
            list-style: none outside none;
            margin: 0;
            padding: 0;
        }      
        
        .selectBox-options li a {
            background: none no-repeat scroll 8px center transparent;
            color: #555555;
            font-size: 14px;
            line-height: 2em;
            overflow: hidden;
            padding: 0 0.3em 0 23px;
            white-space: nowrap;
        }

         #developer-tools .selectBox:hover, #developer-tools .selectBox-menuShowing {
            border: 1px solid #777777;            
        }

        .selectBox-dropdown.selectBox-menuShowing {
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .hhh
        {
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        
        .selectBox-options li.selectBox-selected a {
            background-image: url("https://www.twilio.com//packages/base/images/selectBox-tick.png");
        }
        
        .container
        {
            border: 0px solid #FF7733;
            height: 50px;
            overflow: hidden;
            position: relative;
            width: 124px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
   

    <div id="developer-tools">
    <div id="container" class="container">
       <asp:DropDownList ID="fgh" runat="server" CssClass="s"  >
        <asp:ListItem>1</asp:ListItem>
        <asp:ListItem>2</asp:ListItem>
        <asp:ListItem>3</asp:ListItem>
       </asp:DropDownList>   
    </div>
   

    <select id="developer-mode" class="panel-mode selectBox" style="display: none;">
    <option selected="selected" value="debugger-panel">Debugger</option>
    <option value="api-explorer-panel">API Explorer</option>    
    </select>

    <div>
        <a class="selectBox panel-mode selectBox-dropdown" style="display: inline-block; -moz-user-select: none;" title="" tabindex="0">
        <span  id="timeSelected" class="selectBox-label" runat="server">API Explorer</span>
        <span class="selectBox-arrow" ></span>
        </a>

        <ul class="selectBox-dropdown-menu selectBox-options" style="-moz-user-select: none; width: 134px;display:none;">
        <li class="selectBox-selected">
        <!--<a rel="debugger-panel" >Debugger</a>-->
        <asp:LinkButton ID="lbTimeWeek" runat="server" onclick="onClick" Text="Week">Week</asp:LinkButton>
        </li>
        <li class="">
        <!--<a rel="api-explorer-panel">API Explorer</a>-->
         <asp:LinkButton ID="lbTimeMonth" runat="server" onclick="onClick" Text="Month">Month</asp:LinkButton>
        </li>
        </ul>
    </div>

    </div>

    
    </form>
    
</body>
</html>
