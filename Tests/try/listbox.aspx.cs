﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_try_listbox : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fgh.Attributes.Add("onmouseover", "changeBackground('show');");
        fgh.Attributes.Add("onmouseout", "changeBackground('hide');");

        Page.Header.DataBind();
    }

    protected void onClick(object sender, EventArgs e)
    {
        LinkButton lb=(LinkButton) sender;

        timeSelected.InnerText=lb.Text;
    }
}