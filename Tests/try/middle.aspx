<%@ Page Language="C#" AutoEventWireup="true" CodeFile="middle.aspx.cs" Inherits="try_middle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link   href="../yellowPages/style.css" type="text/css"/>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="background-color:Red;text-align:center;">
            <img src="../images/bg-box.png" />
        </div>
    </div>
    
    <div id="primaryIframe" style="text-align:center;">   
            <div id="segmentRegularTitle">
			    <h2>Currently Available Air Conditioning contractors</h2>
			</div>
			 <div id="div_explanation" class="" style="display:block;">
                <div class="comments1" >
                    <asp:Label ID="lbl_callingStatus1" runat="server" >
                    We are Calling Avaliable
                    </asp:Label>
                </div>
                
                <div class="comments1" >
                    <asp:Label ID="lbl_callingStatus2" runat="server">
                    Air Condistioning Contractors
                    </asp:Label>
                </div>
                
                <div class="comments2">
                    <asp:Label ID="lbl_callingStatus3" runat="server">
                    Your Phone will ring once we have found matched contractors.
                    </asp:Label>
                </div>
                
                <div class="comments2">
                    <asp:Label ID="lbl_callingStatus4" runat="server">
                    Availiable Contractors are listed as they respond.
                    </asp:Label>
                </div>    
                    <!--
                    <asp:Label ID="lbl_callingStatus5" runat="server">
                    Available Suppliers are listed below as they respond
                    </asp:Label>
                    -->        
               
                      
                <div id="iconCalling" class="iconCallingSmall">                
                    <img width="110" height="65" src="../images/icon-calling.gif" alt="calling"  class='iconCallingSmallImg' />
                    <span id="iconCallingText" class='iconCallingSmallText'>
                        <asp:Label ID="lbl_calling" runat="server" Text="CALLING..."></asp:Label>
                    </span>
                </div>       
            </div>
        
            <div id="div_openCall" class="clearfix" style="display:none;">
                <!--<asp:Label ID="lbl_ringInSecond" runat="server" Text="Your Phone will ring in a second. The following Suppliers are willing to talk to you NOW."></asp:Label>-->
                 <h2>Your Phone will ring in a second.<br /> The following Contractors are willing to talk to you NOW.</h2>
	        </div>
	    					
            <div id="div_initiateCall" class="clearfix" style="display:none;">				
                <!--<asp:Label ID="lbl_speakingWith" runat="server" Text="You are speaking with"></asp:Label>-->
                <h2>You are speaking with</h2>
	        </div>
    	    
	</div>
    </form>
</body>
</html>
