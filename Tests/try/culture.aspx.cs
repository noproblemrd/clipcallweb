﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Tests_try_culture : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        string s = "09/20/2011";
        DateTime checkDate;
        CultureInfo culture;
        culture = CultureInfo.CreateSpecificCulture("en-US");
        string b;

        //checkDate = DateTime.Parse(s, culture);
        if (DateTime.TryParse(s, culture, DateTimeStyles.None, out checkDate))
            b = "yes
       */

        string dateString;
        CultureInfo culture;
        DateTimeStyles styles;
        DateTime dateResult;
        string b="";
        // Parse a date and time with no styles.
        dateString = "03/01/2009 10:00 AM";
        culture = CultureInfo.CreateSpecificCulture("en-US");
        styles = DateTimeStyles.None;
        if (DateTime.TryParse(dateString, culture, styles, out dateResult))
           b="yes"; 
        else
            b = "no";
    }
}