﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Tests_try_splitDate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string TransDate = "11/28/2011";
        DateTime TransDateParse;

        if (!string.IsNullOrEmpty(TransDate))
        {

            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("en-US");
            culture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";

            if (DateTime.TryParse(TransDate, culture, DateTimeStyles.None, out TransDateParse))
                TransDate = TransDateParse.Day.ToString() + "/" + TransDateParse.Month.ToString() + "/" + TransDateParse.Year.ToString();

        }


    }
}