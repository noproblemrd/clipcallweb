﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default12.aspx.cs" Inherits="Tests_try_Default12" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:DropDownList ID="ddlTime" runat="server" 
                onselectedindexchanged="ddlTime_SelectedIndexChanged" >
                <asp:ListItem Value="month">month</asp:ListItem>
                <asp:ListItem Value="week">week</asp:ListItem>
                <asp:ListItem Value="year">year</asp:ListItem>
                <asp:ListItem Value="all">all</asp:ListItem>
            </asp:DropDownList>
    </div>
    </form>
</body>
</html>
