﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="father.aspx.cs" Inherits="Tests_try_controlGetParent_father" %>

<%@ Register Src="~/Tests/try/controlGetParent/son.ascx" TagPrefix="uc1" TagName="son" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:son runat="server" ID="son" />
    </div>
    </form>
</body>
</html>
