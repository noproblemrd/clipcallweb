using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class try_postback : System.Web.UI.Page
{
    protected string y = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            y = "post";
            Response.Write("y:" + y); 
        }
    }

    protected void bt1_Click(object sender, EventArgs e)
    {
        Response.Write("y:" + y);
        Response.Write("ViewState['Y']" + Y);
    }

    protected string Y
    {
        get
        {
            if (ViewState["Y"] != null)
                return ViewState["Y"].ToString();
            else
                return "start";
        }

        set
        {
            ViewState["Y"] = value;
        }
    }
}
