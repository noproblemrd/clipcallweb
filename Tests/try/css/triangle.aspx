﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="triangle.aspx.cs" Inherits="Tests_try_css_triangle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
    .css-arrow-acute {
    border-color: red red transparent transparent;
    border-style: solid;
    border-width: 30px;
    height: 0;
    width: 0;
}

    .text
    {
        -webkit-transform: rotate(-90deg); 
        -moz-transform: rotate(-90deg);	
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="css-arrow-acute">
        <div class="text">full</div>
    </div>
    </form>
</body>
</html>
