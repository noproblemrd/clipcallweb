﻿function validEmpty2(objId, waterMark, regformatPhone) {

    //alert(objId + " " + waterMark + " " + regformatPhone);
    switch (objId) {

        case "formMsg":
            if (document.getElementById("formMsg").value == "" || document.getElementById("formMsg").value == waterMark) {
                document.getElementById("formMsg").value = waterMark;
                document.getElementById('formMsg').style.color = '#F69100';
                document.getElementById("formMsg").style.borderColor = '#F69100';
            }
            break;


        case "formCall":

            var phoneNumber = document.getElementById("formCall").value.replace(/\s/g, ""); // remove spaces
            //alert(phoneNumber);

            var regFormatPhone = regformatPhone;

            //'^[01]?[- .]?(\\([2-9]\d{2}\\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$'    for cs
            //^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$
            //^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$   for javascript                 

            //var regFormatPhone = '^[01]$';


            if (phoneNumber == "" || phoneNumber == waterMark) {
                document.getElementById("formCall").value = waterMark;
                document.getElementById('formCall').style.color = '#F69100';
                document.getElementById("formCall").style.borderColor = '#F69100';
            }



            else if (phoneNumber.search(regFormatPhone) == -1) //if match failed
            {
                if (document.getElementById("HiddenPhoneDynamicComment") != null &&
                    document.getElementById("HiddenPhoneDynamicComment").value == "yes") {

                    if (document.getElementById("commentZipCode").style.display == 'block')
                        document.getElementById("commentPhone").className = 'comment commentDynamic';

                    else {
                        document.getElementById("commentPhone").className = 'comment';


                    }

                }

                document.getElementById("commentPhone").style.display = 'block';

                document.getElementById("formCall").style.borderColor = '#F69100';
                document.getElementById('formCall').style.color = '#F69100';
            }

            else {

                document.getElementById("commentPhone").style.display = 'none';
                document.getElementById("formCall").style.borderColor = '#CCCCCC';


            }



            break;

        case "formZipCode":

            var zipCodePattern = /^\d{5}$/;
            var zipCode = document.getElementById("formZipCode").value;

            if (zipCode == "" || zipCode == waterMark) {
                document.getElementById("formZipCode").value = waterMark;
                document.getElementById('formZipCode').style.color = '#F69100';
                document.getElementById("formZipCode").style.borderColor = '#F69100';
            }

            else if (!zipCodePattern.test(zipCode)) {
                document.getElementById('formZipCode').style.color = '#F69100';
                document.getElementById("formZipCode").style.borderColor = '#F69100';
                document.getElementById("commentZipCode").style.display = 'block';


                if (document.getElementById("HiddenPhoneDynamicComment") != null &&
                    document.getElementById("HiddenPhoneDynamicComment").value == "yes") {

                    if (document.getElementById("commentPhone").style.display == 'block') {

                        document.getElementById("commentPhone").className = 'comment commentDynamic';
                    }
                    else {

                        document.getElementById("commentPhone").className = 'comment';

                    }

                }
            }

            else {

                document.getElementById("commentZipCode").style.display = 'none';
                document.getElementById("formZipCode").style.borderColor = '#CCCCCC';
            }

            break;

        case "formFromZipCode":

            var zipCodePattern = /^\d{5}$/;
            var zipCode = document.getElementById("formFromZipCode").value;

            if (zipCode == "" || zipCode == waterMark) {
                document.getElementById("formFromZipCode").value = waterMark;
                document.getElementById('formFromZipCode').style.color = '#F69100';
                document.getElementById("formFromZipCode").style.borderColor = '#F69100';
            }

            else if (!zipCodePattern.test(zipCode)) {
                document.getElementById('formFromZipCode').style.color = '#F69100';
                document.getElementById("formFromZipCode").style.borderColor = '#F69100';
                if (document.getElementById("commentToZipCode").style.display == 'block') {
                    document.getElementById("commentFromZipCode").style.display = 'block';
                    document.getElementById("commentToZipCode").style.display = 'none';
                }

                else {
                    document.getElementById("commentFromZipCode").style.display = 'block';
                }

            }

            else {

                ////document.getElementById("commentZipCode").style.display = 'none';
                document.getElementById("formFromZipCode").style.borderColor = '#CCCCCC';
            }

            break;



        case "formToZipCode":

            var zipCodePattern = /^\d{5}$/;
            var zipCode = document.getElementById("formToZipCode").value;

            if (zipCode == "" || zipCode == waterMark) {
                document.getElementById("formToZipCode").value = waterMark;
                document.getElementById('formToZipCode').style.color = '#F69100';
                document.getElementById("formToZipCode").style.borderColor = '#F69100';
            }

            else if (!zipCodePattern.test(zipCode)) {
                document.getElementById("formToZipCode").style.borderColor = '#F69100';
                if (document.getElementById("commentFromZipCode").style.display == 'block') {
                    document.getElementById("commentToZipCode").style.display = 'block';
                    document.getElementById("commentFromZipCode").style.display = 'none';
                }
                else {
                    document.getElementById("commentToZipCode").style.display = 'block';
                }



            }

            else {

                document.getElementById("commentToZipCode").style.display = 'none';
                document.getElementById("formToZipCode").style.borderColor = '#CCCCCC';
            }

            break;

        case "formEmail":

            var email = trim(document.getElementById("formEmail").value);

            if (email == "" || email == waterMark) {
                document.getElementById("formEmail").value = waterMark;
                document.getElementById('formEmail').style.color = '#F69100';
                document.getElementById("formEmail").style.borderColor = '#F69100';
            }

            else if (!checkEmail(email)) {
                document.getElementById('formEmail').style.color = '#F69100';
                document.getElementById("commentEmail").style.display = 'block';
                document.getElementById("formEmail").style.borderColor = '#F69100';
            }

            else {

                document.getElementById("commentEmail").style.display = 'none';
                document.getElementById("formEmail").style.borderColor = '#CCCCCC';
            }

            break;

        case "formName":

            var name = trim(document.getElementById("formName").value);

            if (name == "" || name == waterMark) {
                document.getElementById("formName").value = waterMark;
                document.getElementById('formName').style.color = '#F69100';
                document.getElementById("formName").style.borderColor = '#F69100';
            }


            else {
                arrName = name.split(' ');
                if (arrName.length < 2) {
                    document.getElementById('formName').style.color = '#F69100';
                    document.getElementById("formName").style.borderColor = '#F69100';
                    document.getElementById("commentName").style.display = 'block';

                }

                else {
                    document.getElementById('formName').style.color = 'black';
                    document.getElementById("formName").style.borderColor = '#CCCCCC';
                    document.getElementById("commentName").style.display = 'none';
                }
            }


            break;

        case "formFirstName":

            if (document.getElementById("formFirstName").value == "" || document.getElementById("formFirstName").value == waterMark) {
                document.getElementById("formFirstName").value = waterMark;
                document.getElementById('formFirstName').style.color = '#F69100';
                document.getElementById("formFirstName").style.borderColor = '#F69100';
            }


            else {
                document.getElementById("formFirstName").style.borderColor = '#CCCCCC';
            }


            break;

        case "formLastName":

            if (document.getElementById("formLastName").value == "" || document.getElementById("formLastName").value == waterMark) {
                document.getElementById("formLastName").value = waterMark;
                document.getElementById('formLastName').style.color = '#F69100';
                document.getElementById("formLastName").style.borderColor = '#F69100';

            }


            else {
                document.getElementById("formLastName").style.borderColor = '#CCCCCC';
            }


            break;

        case "formDate":

            if (document.getElementById("formDate").value == "" || document.getElementById("formDate").value == waterMark) {
                document.getElementById("formDate").value = waterMark;
                document.getElementById('formDate').style.color = '#F69100';
                document.getElementById("formDate").style.borderColor = '#F69100';

            }

            else {
                document.getElementById("formDate").style.borderColor = '#CCCCCC';
            }


            break;

        case "subTypeExpertiseSelected":
            /*
            if (document.getElementById("subTypeExpertiseSelected").innerHTML == "" || document.getElementById("subTypeExpertiseSelected").innerHTML == waterMark) {
                
            document.getElementById('linkSubTypeExpertiseSelected').style.color = '#F69100';
            document.getElementById("linkSubTypeExpertiseSelected").style.borderColor = '#F69100';
            document.getElementById("subTypeExpertiseSelected").style.color = '#F69100';

            }
            */

        default:
            break;
    }

}
