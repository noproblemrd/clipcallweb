﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_try_redirect : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ApplicationPath = HttpContext.Current.Request.ApplicationPath;
        Response.Write(ApplicationPath);

        Response.Write("<br>" + ResolveUrl("~"));

        Response.Write("<br>" + System.Web.VirtualPathUtility.ToAbsolute("~"));
        
        //Response.Redirect(Request.Url.Host + ApplicationPath + "/tests/try/101.htm");
    }
}