﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tests_getip : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string _ip = Utilities.GetIP(Request); 
        lbl_ip.Text = _ip;
        lbl_country.Text = SalesUtility.GetCountryName(Utilities.GetIP(Request), Request.Url.Host);

        string str = "";
        foreach(string key in Request.ServerVariables)
        {
            str += key + ": " + Request.ServerVariables[key] + ";;;";
        }
        lbl_header.Text = str;

    }
}