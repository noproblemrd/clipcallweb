﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tryAjaxToolkit.aspx.cs" Inherits="Tests_tryAjaxToolkit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Accordion ID="Accordion1" runat="server"></asp:Accordion>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    </div>
    </form>
</body>
</html>
