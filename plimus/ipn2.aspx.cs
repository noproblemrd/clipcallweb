﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class plimus_ipn2 : System.Web.UI.Page
{
    protected string returnPage = "";

    /*
    plimus: productId=289628&productName=PPC&contractId=2130482&contractName=Full+Version&contractPrice=5.00
     * &overridePrice=5.00&referenceNumber=1002907600&transactionDate=12%2f11%2f2011+06%3a49+AM&
     * untilDate=01%2f11%2f2012+06%3a49+AM&transactionType=CHARGE&quantity=1&
     * currency=USD&addCD=N&referrer=&promoteContractsNum=0&
     * invoiceAmount=5.00&paymentMethod=CC&paymentType=CC&creditCardType=MASTERCARD&
     * shippingMethod=&remoteAddress=194.90.222.218&
     * invoiceInfoURL=https%3a%2f%2fsandbox.plimus.com%2fjsp%2forder_locator_info.jsp%3frefId%3d706B7BDE85CBF9416D511AF1EB718021%26acd%3d895EFA28DC815BBA&
     * testMode=N&contractOwner=389814&invoiceAmountUSD=5.00&invoiceChargeCurrency=USD&
     * invoiceChargeAmount=5.00&subscriptionId=5806970&creditCardLastFourDigits=5589&
     * creditCardExpDate=10%2f2012&targetBalance=PLIMUS_ACCOUNT&invoiceTitle=&
     * invoiceFirstName=%u05e9%u05d9&invoiceLastName=%u05de%u05e8%u05e7%u05d1%u05d9%u05e5&invoiceCompany=&
     * invoiceAddress1=%u05e0%u05d7%u05dc+%u05e6%u05d9%u05df&invoiceAddress2=&
     * invoiceCity=%u05de%u05d5%u05d3%u05d9%u05e2%u05d9%u05df&invoiceState=&
     * invoiceCountry=IL&invoiceZipCode=71701&
     * invoiceEmail=shaim%40noproblem.co.il&invoiceWorkPhone=0545350800&invoiceExtension=&
     * invoiceFaxNumber=&invoiceMobilePhone=&accountId=19329212&title=&
     * firstName=%u05e9%u05d9&lastName=%u05de%u05e8%u05e7%u05d1%u05d9%u05e5&
     * username=1323614889162&company=&address1=%u05e0%u05d7%u05dc+%u05e6%u05d9%u05df&address2=&
     * city=%u05de%u05d5%u05d3%u05d9%u05e2%u05d9%u05df&state=&country=IL&zipCode=71701&
     * email=shaim%40noproblem.co.il&workPhone=0545350800&extension=&
     * faxNumber=&mobilePhone=&homePhone=&shippingFirstName=%u05e9%u05d9&
     * shippingLastName=%u05de%u05e8%u05e7%u05d1%u05d9%u05e5&
     * shippingAddress1=%u05e0%u05d7%u05dc+%u05e6%u05d9%u05df&shippingAddress2=&
     * shippingCity=%u05de%u05d5%u05d3%u05d9%u05e2%u05d9%u05df&shippingState=&
     * shippingCountry=IL&shippingZipCode=71701&authKey=&supplierId=5678&
     * havacookie=%u05de%u05d9%u05e6%u05d9&couponCode=&licenseKey=

    */


    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(Request.UrlReferrer.AbsoluteUri);
        try
        {
            string result = "";
            string token = "";

            string resultTransuctionStatus = "";
            string resultTransuctionNumConfirm = "";
            string resultTransuctionSum = "";
            string resultTransuctionNumberPayments = "";
            string resultTransuctionMyParam = "";            
            string resultTransuctionMyParamIfCharge = "";
            string resultTransuctionCardNumber = "";
            string resultTransuctionCardCompany = "";
            string resultTransuctionExpiration = "";
            string resultTransuctionFiles = "";
            string resultTransuctionKupa = "";
            string resultTransuctionSoderKupa = "";
            string resultTransactionContractId = "";
            string resultTransactionAccountId = "";
            string resultTransactionLast4Digits = "";
            string resultTransactionFirstName="";
            string resultTransactionLastName="";
            string resultTransactionAddress = "";
            string resultTransactionCreditCardCompany = "";
            string resultTransactionInvoiceInfoURL = "";
            string componentType="";
            //http://sandbox.plimus.com/jsp/show_invoice.jsp?ref=5AD37FD100EB1EC12BB5D14C1704EE0C
            string shovar = "";

            int munberPayments = 1;
            string cardCompany = "";
            string transDate;

            string Day;
            Day = DateTime.Now.Day.ToString().Length == 1 ? "0"
                + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();


            string Month;
            Month = DateTime.Now.Month.ToString().Length == 1 ? "0"
                + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();

            string Year;
            Year = DateTime.Now.Year.ToString().Length == 2 ? "20"
                + DateTime.Now.Year.ToString() : DateTime.Now.Year.ToString();

            transDate = Month + @"/" + Day + @"/" + Year;

            result = Request.Form.ToString();

            LogEdit.SaveLog("Accounting_", "Plimus:" + result);

            if (Request.Form["transactionType"] != null)
                resultTransuctionStatus = Request.Form["transactionType"].ToLower();

            if (resultTransuctionStatus == "charge") // success transaction
            {

                resultTransuctionMyParamIfCharge = Request.Form["ifRecharge"];
                resultTransuctionCardNumber = "            " + Request["creditCardLastFourDigits"]; // need to send string of 16 to invoice4you. i recieve 4 last digits and add 12 spaces before                         

                resultTransuctionCardCompany = Request["creditCardType"];

               
                string creditCardExpDate = Request["creditCardExpDate"];
                string[] creditCardExpDateSplit = creditCardExpDate.Split(new String[] { "%2F", "%2f", "/" }, 2, StringSplitOptions.RemoveEmptyEntries);
                resultTransuctionExpiration = creditCardExpDateSplit[0] + creditCardExpDateSplit[1].Substring(2, 2);
               


                decimal amountFloat = Convert.ToDecimal(Request["overridePrice"]);
                int amountInt = Convert.ToInt32(amountFloat);
                resultTransuctionSum = amountInt.ToString();


                /* don't use it because it can repeat. it is not uniqe.
                אפשר לקחת משהו יותר יוניקי שהוא מספר קובץ מספר קופה ומספר סודר בקופה
                    * השיולב של שלושתם פלוס תאריך
                */

                resultTransuctionNumConfirm = Request["referenceNumber"];

                /*
                if (result.Length >= 98)
                {
                    resultTransuctionFiles = result.Substring(95, 2);
                }

                if (result.Length >= 101)
                {
                    resultTransuctionKupa = result.Substring(97, 3);
                }

                if (result.Length >= 104)
                {
                    resultTransuctionSoderKupa = result.Substring(100, 3);
                }           

                shovar = resultTransuctionFiles + resultTransuctionKupa + resultTransuctionSoderKupa;
                */


                resultTransuctionMyParam = Request["noProblemRecordId"]; // פרטים ששלחתי בטקסט חופשי                
                resultTransactionContractId = Request["contractId"]; // plimus contract id
                resultTransactionAccountId = Request["accountId"]; // plimus shopper id
                resultTransactionLast4Digits = Request["creditCardLastFourDigits"];
                resultTransactionCreditCardCompany = Request["creditCardType"];
                resultTransactionFirstName=Request["firstName"];
                resultTransactionLastName=Request["lastName"];
                resultTransactionAddress = Request["address1"];
                resultTransactionInvoiceInfoURL = Request["invoiceInfoURL"];
                /*
                if (Request.Form["Token"] != null)
                {
                    token = Request.Form["Token"];
                }
                */


                /*
                string strGeneral = "";

        
                foreach (string str in Request.Form)
                    strGeneral+=str+ " " + Request.Form[str];
                */

                SupllierPayment sp = new SupllierPayment();

                sp.city = Request["city"];
                sp.state = Request["state"];
                sp.zipCode = Request["zip"];
                //sp.deposit = resultTransuctionSum;
                sp.PaymentAmount = Convert.ToInt32(resultTransuctionSum);
                sp.chargingCompany = "plimus";
                sp.transactionId = resultTransuctionMyParam + "-" + resultTransuctionNumConfirm;
                sp.InvoiceNumber = resultTransuctionNumConfirm;
                sp.creditCardToken = token;
                sp.numberPayments = munberPayments;
                sp.paymentMethod = WebReferenceSupplier.ePaymentMethod.CreditCard;
                sp.creditCard = resultTransactionLast4Digits;
                sp.creditCardCompany = resultTransactionCreditCardCompany;
                sp.creditCardDateMmyy = resultTransuctionExpiration;
                sp.chargingCompanyContractId = resultTransactionContractId;
                sp.chargingCompanyCustomerId = resultTransactionAccountId;
                //sp.isRecharge = true;
                sp.SupplierPricingComponentType = WebReferenceSupplier.eSupplierPricingComponentType.MonthlyFeePayment;
                sp.cardName = resultTransactionFirstName + " " + resultTransactionLastName;
                sp.cardAddress = resultTransactionAddress;

                string paymentDetails;
                string command = "EXEC dbo._SetConfirmGetPayment @auto_id";
                using (SqlConnection conn = DBConnection.GetConnString())
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(command, conn);
                    cmd.Parameters.AddWithValue("@auto_id", resultTransuctionMyParam);
                    SqlDataReader reader = cmd.ExecuteReader();


                    if (reader.Read())
                    {
                        paymentDetails = (string)reader["details"];
                        //RechargeAmount = reader.IsDBNull(1) ? -1 : (int)reader["RechargeAmount"];
                    }
                    else
                        return;
                    reader.Close();
                    conn.Close();
                }
                string[] arrPaymentDetails = { };

                arrPaymentDetails = paymentDetails.Split('&');

                string[] nameValueCustom;

                for (int i = 0; i < arrPaymentDetails.Length; i++)
                {
                    /*
                    SupplierId={ECAD4D81-5FD4-DF11-81D3-001517D1792A}&
                        AdvertiserName=Company Test&
                            isFirstTimeV=False&OldValue=5,334&
                                UserId=a6a59cdf-ba9b-df11-92c8-a4badb37a26f&
                                    UserName=user1&siteLangId=1&
                                    siteId=1015&BonusAmount=72&
                                    NewValue=6306
                    */

                    nameValueCustom = arrPaymentDetails[i].Split('=');
                    if (nameValueCustom[0] == "bonusAmount")
                        sp.discount = nameValueCustom[1];

                    if (nameValueCustom[0] == "bonusType")
                        sp.discountType = nameValueCustom[1];

                    if (nameValueCustom[0] == "bonusExtraAmount")
                        sp.extraBonus = nameValueCustom[1];

                    if (nameValueCustom[0] == "extraReasonBonusId")
                        sp.extraReasonBonusId = nameValueCustom[1];

                    if (nameValueCustom[0] == "supplierGuid")
                        sp.guid = nameValueCustom[1];

                    if (nameValueCustom[0] == "supplierId")
                        sp.id = nameValueCustom[1];

                    if (nameValueCustom[0] == "advertiserName")
                        sp.name = Server.UrlDecode(nameValueCustom[1]);

                    if (nameValueCustom[0] == "fieldId")
                        sp.fieldId = nameValueCustom[1];

                    if (nameValueCustom[0] == "fieldName")
                        sp.fieldName = Server.UrlDecode(nameValueCustom[1]);

                    if (nameValueCustom[0] == "balanceNew")
                        sp.balanceNew = nameValueCustom[1];


                    if (nameValueCustom[0] == "isFirstTimeV")
                        sp.isFirstTimeV = Convert.ToBoolean(nameValueCustom[1]);

                    if (nameValueCustom[0] == "balanceOld")
                        sp.balanceOld = nameValueCustom[1];

                    if (nameValueCustom[0] == "userId")
                        sp.userId = nameValueCustom[1];

                    if (nameValueCustom[0] == "userName")
                        sp.userName = Server.UrlDecode(nameValueCustom[1]);

                    
                    if (nameValueCustom[0] == "siteLangId")
                        sp.siteLangId = Convert.ToInt32(nameValueCustom[1]);

                    if (nameValueCustom[0] == "siteId")
                        sp.siteId = nameValueCustom[1];

                    if (nameValueCustom[0] == "rechargeAmount")
                        sp.RechargeAmount = (String.IsNullOrEmpty(nameValueCustom[1]))? 0 : Convert.ToInt32(nameValueCustom[1]);

                    /*
                    if (nameValueCustom[0] == "isAutoRenew")
                        sp.isAutoRenew = Convert.ToBoolean(nameValueCustom[1]);
                    */


                    if (nameValueCustom[0] == "email")
                        sp.email = Server.UrlDecode(nameValueCustom[1]);

                    if (nameValueCustom[0] == "phoneNumber")
                        sp.tel1 = nameValueCustom[1];

                    if (nameValueCustom[0] == "otherPhone")
                        sp.tel2 = nameValueCustom[1];

                    //if (nameValueCustom[0] == "country")
                    //    sp.state = nameValueCustom[1];

                    //if (nameValueCustom[0] == "city")
                    //    sp.city = nameValueCustom[1];

                    if (nameValueCustom[0] == "streetNumber")
                        sp.address = Server.UrlDecode(nameValueCustom[1]);

                    //if (nameValueCustom[0] == "postalCode")
                    //    sp.zipCode = nameValueCustom[1];

                    if (nameValueCustom[0] == "webSite")
                        sp.website = Server.UrlDecode(nameValueCustom[1]);

                    if (nameValueCustom[0] == "fax")
                        sp.fax = nameValueCustom[1];

                    if (nameValueCustom[0] == "vat")
                        sp.tax = nameValueCustom[1];

                    if (nameValueCustom[0] == "ifAccountingCharging")
                        sp.ifAccountingCharging = Convert.ToBoolean(nameValueCustom[1]);

                    if (nameValueCustom[0] == "ppcPackage")
                        sp.ppcPackage = Server.UrlDecode(nameValueCustom[1]);

                    if (nameValueCustom[0] == "currency")
                        sp.currency = nameValueCustom[1];

                    if (nameValueCustom[0] == "accountNumber")
                        sp.accountNumber = nameValueCustom[1];

                    if (nameValueCustom[0] == "userAgent")
                        sp.userAgent = Server.UrlDecode(nameValueCustom[1]);

                    if (nameValueCustom[0] == "hostIp")
                        sp.hostIp = nameValueCustom[1];

                    if (nameValueCustom[0] == "clientIp")
                        sp.clientIp = nameValueCustom[1];

                    if (nameValueCustom[0] == "acceptLanguage")
                        sp.acceptLanguage = Server.UrlDecode(nameValueCustom[1]);

                    
                    if (nameValueCustom[0] == "isRecharge") // if deal with the crm recharge details
                    {
                        if (nameValueCustom[1].ToLower() == "false")
                            sp.isRecharge = false;
                        else
                            sp.isRecharge = true;
                    }

                    if (nameValueCustom[0] == "componentType")
                    {
                        switch (nameValueCustom[1].ToLower())
                        {
                            case "manual":
                                componentType = "manual";
                                break;

                            case "monthlyfeepayment":
                                componentType = "monthlyfeepayment";
                                break;

                            default :
                                componentType = "monthlyfeepayment";
                                break;
                            
                        }                      

                    }                    


                } // end for

                LogEdit.SaveLog("Accounting_" + sp.siteLangId.ToString(), "Plimus positive :" +
                "\r\nresult: " + result +
                "\r\nconfirm: " + resultTransuctionNumConfirm +
                "\r\navertiser name: " + sp.name +
                "\r\nresultTransuctionSum: " + resultTransuctionSum +
                "\r\nresultTransuctionStatus: " + resultTransuctionStatus +
                "\r\nresultTransuctionNumConfirm: " + resultTransuctionNumConfirm +
                "\r\nresultTransuctionMyParam: " + resultTransuctionMyParam +                
                "\r\nresultTransuctionMyParamIfCharge: " + resultTransuctionMyParamIfCharge 
                //"\r\sp.isRecharge: " + sp.isRecharge.ToString()
                );


                /********** web service to check if it is really ok *************/

                try
                {
                    /*
                    biz.pelecard.ws101.ITrxWebService pelecard = new biz.pelecard.ws101.ITrxWebService();

                    string commandPelecardCompany = "EXEC dbo.GetChargingCompanyDetails @company";
                    //SqlConnection connAccounting = DBConnection.GetConnString();
                    //conn.Open();
                    SqlCommand cmdPelecardCompany = new SqlCommand(commandPelecardCompany, conn);
                    cmdPelecardCompany.Parameters.AddWithValue("@company", "plimus");
                    SqlDataReader readerPelecardCompany = cmdPelecardCompany.ExecuteReader();
                    string pelecardUserName = "";
                    string pelecardPassword = "";
                    string pelecardTermNo = "";

                    if (readerPelecardCompany.HasRows)
                    {
                        while (readerPelecardCompany.Read())
                        {
                            switch ((string)readerPelecardCompany["item"])
                            {
                                case "userName":
                                    pelecardUserName = (string)readerPelecardCompany["value"];
                                    break;

                                case "password":
                                    pelecardPassword = (string)readerPelecardCompany["value"];
                                    break;

                                case "termNo":
                                    pelecardTermNo = (string)readerPelecardCompany["value"];
                                    break;

                                default:
                                    break;
                            }

                        }

                    }

                    readerPelecardCompany.Close();

                    string strParmxExist = pelecard.ChkParmXExist(pelecardUserName, pelecardPassword, pelecardTermNo, resultTransuctionMyParam);

                    LogEdit.SaveLog("Accounting_" + Convert.ToString(sp.siteLangId), "Charging: pelecard validate the transuction " + sp.name + "\r\n" + strParmxExist);
               


                    string[] arrParmxExist = { };
                    arrParmxExist = strParmxExist.Split(';');
                    string resultTransuctionStatus2 = arrParmxExist[0];
                    if (resultTransuctionStatus2 != "000")
                    {
                        LogEdit.SaveLog("Accounting_" + Convert.ToString(sp.siteLangId), "Charging: pelecard validate the transuction. and it is not confirm it " + sp.name + "\r\n" + strParmxExist);
                        return;

                    }
                 
                    */
                }

                catch (Exception eParmX)
                {
                    LogEdit.SaveLog("Accounting_" + Convert.ToString(sp.siteLangId), "Charging: plimus ChkParmXExist server problem " + sp.name + "\r\n" + eParmX.Message);

                }

                if (resultTransuctionMyParamIfCharge == "false") // come from the web and not from crm recurring
                {
                    try
                    {

                        //   WebServiceCharging wsCharging = new WebServiceCharging();
                        List<WebReferenceSupplier.SupplierPricingComponent> list_spc=new List<WebReferenceSupplier.SupplierPricingComponent>();

                        //
                        
                        if (componentType == "manual")
                            list_spc.Add(new WebReferenceSupplier.SupplierPricingComponent() { Type = WebReferenceSupplier.eSupplierPricingComponentType.Manual, AmountPayed = Convert.ToDecimal(resultTransuctionSum), BaseAmount = Convert.ToDecimal(resultTransuctionSum) });
                        else
                            list_spc.Add(new WebReferenceSupplier.SupplierPricingComponent() { Type = WebReferenceSupplier.eSupplierPricingComponentType.MonthlyFeePayment, AmountPayed = Convert.ToDecimal(resultTransuctionSum), BaseAmount = Convert.ToDecimal(resultTransuctionSum) });

                        //get invoice url reference
                        string refId = ExtractInvoiceReferenceUrl(resultTransactionInvoiceInfoURL);
                        sp.InvoiceUrlReference = refId;

                        ResultCharging resultCharging = DpzUtilities.ChargingSupplier(sp, list_spc, Session);//wsCharging.ChargingGeneral(sp);

                        if (resultCharging.Result == ResultCharging.eResult.success && resultCharging.DepositStatus == eDepositStatusResponse.OK && sp.ifAccountingCharging == true)
                        {

                            LogEdit.SaveLog("Accounting_" + Convert.ToString(sp.siteLangId), "Accounting: There is right permission to send invoice " + sp.name);                            

                        }

                        else if (resultCharging.Result == ResultCharging.eResult.unsuccess)
                        {
                            LogEdit.SaveLog("Accounting_" + Convert.ToString(sp.siteLangId), "Accounting: There is charging problem " +
                                "\r\n" + sp.name + " charging unsuccess");

                        }

                        else
                        {
                            LogEdit.SaveLog("Accounting_" + Convert.ToString(sp.siteLangId), "Accounting: There is charging problem " +
                               "\r\n" + sp.name + " charging yaanoo success but " + resultCharging.DepositStatus.ToString());
                        }

                    }

                    catch (Exception exPricing)
                    {
                        LogEdit.SaveLog("Accounting_" + Convert.ToString(sp.siteLangId), "Pricing server problem: " + sp.name + "\r\n" + exPricing.Message);

                    }

                }


            } // if (resultTransuctionStatus=="charge")

        }

        catch (Exception eAll)
        {
            LogEdit.SaveLog("Accounting_", "problem plimus: " + eAll.Message + "\r\n" + eAll.StackTrace);

        }

    } // close page load

    private static string ExtractInvoiceReferenceUrl(string resultTransactionInvoiceInfoURL)
    {
        string[] arrUrl;
        arrUrl = resultTransactionInvoiceInfoURL.Split(new String[] { "&", "%3d" }, StringSplitOptions.RemoveEmptyEntries);
        int posrefId = arrUrl[0].IndexOf("refId");
        string refId = arrUrl[0].Substring(posrefId + 6);
        if (arrUrl[0].IndexOf("sandbox") == -1)
            refId = "https://www.plimus.com/jsp/show_invoice.jsp?ref=" + refId;
        else
            refId = "http://sandbox.plimus.com/jsp/show_invoice.jsp?ref=" + refId;
        return refId;
    }
}