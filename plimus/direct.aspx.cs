﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;

public partial class plimus_direct : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

        /*
        string strWebRquest = "http://track.right-ads.com/tags_conv/?n=156&t=img&ta_creative_id=28496&ta_camp_id=4071&px_c=" + Url +
                        "&li_id=" + PageName + "&vurlid=" + PlaceInWebSite +
                        "&pub_id=" + Domain + "&amount=0&commission=0&curency=USD&nconv_id=" + serviceResponse.ServiceRequestId + "&data=" +
                        ContactPhoneNumber + "&d_vchar_1=&d_vchar_2=" + Utilities.GetIP(Request);
        */
 
        /*

        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(strWebRquest);

        webRequest.Method = "POST";
        
        Stream webStream = webRequest.GetRequestStream();
        webStream.Close();


        WebResponse webResponse = webRequest.GetResponse();
        webStream = webResponse.GetResponseStream();


        TextReader reader = new StreamReader(webStream);
        string strResponse = reader.ReadToEnd();                     
        */
        
        
            /*
        string xmlPlimus=
            "<order xmlns="http://ws.plimus.com">
            <soft-descriptor>DescTest</soft-descriptor>
            <ordering-shopper>
            <credit-card>
            <card-last-four-digits>1111</card-last-four-digits>
            <card-type>VISA</card-type>
            </credit-card>
            <shopper-id>19287562</shopper-id>
            <web-info>
            <ip>192.168.1.123</ip>";
*/
        StringBuilder sb = new StringBuilder();
        //sb.Append("<order xmlns=\"http://ws.plimus.com\">");
        sb.Append("<order xmlns=\"http://sandbox.plimus.com\">");
            sb.Append("<soft-descriptor>DescTest</soft-descriptor>");
            sb.Append("<ordering-shopper>");
                sb.Append("<credit-card>");
                    sb.Append("<card-last-four-digits>1111</card-last-four-digits>");
                    sb.Append("<card-type>VISA</card-type>");
                sb.Append("</credit-card>");
                sb.Append("<shopper-id>19287562</shopper-id>");
                sb.Append("<web-info>");
                sb.Append("<ip>192.168.1.123</ip>");
                sb.Append("<remote-host>bzq-219-121-253.static.bezeqint.net.reinventhosting.com</remote-host>");
                sb.Append("<user-agent>Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; GTB6.3; .NET CLR 2.0.50727)</user-agent>");
                sb.Append("<accept-language>en-us</accept-language>");
                sb.Append("</web-info>");
            sb.Append("</ordering-shopper>");

            sb.Append("<cart>");
                sb.Append("<cart-item>");
                    sb.Append("<sku>");
                        sb.Append("<sku-id>2117474</sku-id>");
                    sb.Append("</sku>");
                sb.Append("<quantity>1</quantity>");
                sb.Append("</cart-item>");
            sb.Append("</cart>");

            sb.Append("<expected-total-price>");
                sb.Append("<amount>99.99</amount>");
                sb.Append("<currency>USD</currency>");
            sb.Append("</expected-total-price>");
        sb.Append("</order>");

        XmlDocument xmlDocPlimus=new XmlDocument();
        xmlDocPlimus.LoadXml(sb.ToString());

        PostXMLTransaction("https://sandbox.plimus.com/services/2/orders", xmlDocPlimus);


    }

    public static XmlDocument PostXMLTransaction(string v_strURL, XmlDocument v_objXMLDoc)
    {
        //Declare XMLResponse document
        XmlDocument XMLResponse = null;

        //Declare an HTTP-specific implementation of the WebRequest class.
        HttpWebRequest objHttpWebRequest;

        //Declare an HTTP-specific implementation of the WebResponse class
        HttpWebResponse objHttpWebResponse = null;

        //Declare a generic view of a sequence of bytes
        Stream objRequestStream = null;
        Stream objResponseStream = null;

        //Declare XMLReader
        XmlTextReader objXMLReader;

        //Creates an HttpWebRequest for the specified URL.
        objHttpWebRequest = (HttpWebRequest)WebRequest.Create(v_strURL);

        try
        {
            //---------- Start HttpRequest
            string username = "darkblue";
            string password = "eff0bba1e3";

            //Set HttpWebRequest properties
            byte[] bytes;
            //bytes = System.Text.Encoding.ASCII.GetBytes(v_objXMLDoc.InnerXml);
            bytes = System.Text.Encoding.ASCII.GetBytes(v_objXMLDoc.InnerXml);
            objHttpWebRequest.Method = "POST";
            objHttpWebRequest.ContentLength = bytes.Length;
            objHttpWebRequest.ContentType = "text/xml;encoding='utf-8'";
            //objHttpWebRequest.ContentType = "application/xml; charset=utf-8";
            objHttpWebRequest.Credentials = new NetworkCredential(username, password);
            objHttpWebRequest.PreAuthenticate = true;
            //objHttpWebRequest.Headers.Add("Content-Type", "text/xml;charset=UTF-8");
            
            //Get Stream object
            objRequestStream = objHttpWebRequest.GetRequestStream();

            //Writes a sequence of bytes to the current stream
            objRequestStream.Write(bytes, 0, bytes.Length);

            //Close stream
            objRequestStream.Close();

            //---------- End HttpRequest

            //Sends the HttpWebRequest, and waits for a response.
            objHttpWebResponse = (HttpWebResponse)objHttpWebRequest.GetResponse();

            //---------- Start HttpResponse
            if (objHttpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                //Get response stream
                objResponseStream = objHttpWebResponse.GetResponseStream();

                //Load response stream into XMLReader
                objXMLReader = new XmlTextReader(objResponseStream);

                //Declare XMLDocument
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(objXMLReader);

                //Set XMLResponse object returned from XMLReader
                XMLResponse = xmldoc;

                //Close XMLReader
                objXMLReader.Close();
            }

            //Close HttpWebResponse
            objHttpWebResponse.Close();
        }
        catch (WebException we)
        {
            //TODO: Add custom exception handling
            throw new Exception(we.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            //Close connections
            objRequestStream.Close();
            objResponseStream.Close();
            objHttpWebResponse.Close();

            //Release objects
            objXMLReader = null;
            objRequestStream = null;
            objResponseStream = null;
            objHttpWebResponse = null;
            objHttpWebRequest = null;
        }

        //Return
        return XMLResponse;
    }

   
}