﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="professional.aspx.cs" Inherits="professional" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>My mini site</title>
    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />
    
    <script type="text/javascript">
    function showVideo()
    {         
        if(document.getElementById('<%# divVideo.ClientID %>').style.display=='none')
            document.getElementById('<%# divVideo.ClientID %>').style.display='';
        else
            document.getElementById('<%# divVideo.ClientID %>').style.display='none';
            
        return true;
    }
    function IsVideoOk(elem) {
        var i = elem.getElementsByTagName('embed');
     //   alert(elem.title);
        /*
        var oo = document.frames['yoav'];
        var ooo = oo.document.getElementsByTagName('embed');
        alert(ooo.length);
        */
    }
    </script>
</head>
<body>
<form id="form1" action="" method="post" runat="server">
<div style="height:20px;"></div>
<div class="wrapclient">
<div class="container">
    <div  id="supplierLogo1">
	<div class="logoclient"> <img src="" alt="logo" id="ds_logo" runat="server" width="106" height="81" /></div>
	</div>
		<div class="client">

        	<div class="nameclient">
        	<asp:Label ID="lblCompany" CssClass="nameclient" runat="server" ></asp:Label>
            </div>
           	 <div class="vnumber"><asp:Label ID="lblDirectPhone2" runat="server"></asp:Label></div>
           		 <div class="clear"></div>
           		    <div class="clientaddress">
           		    <asp:Label ID="lblAddress" runat="server" ></asp:Label><br />
           	        <asp:Label ID="lblDirectPhone" runat="server"></asp:Label>
           	        </div>
           	        <div class="clear"><p>&nbsp;</p></div>
           			 <div class="tools">
                      <div class="calls">&nbsp;</div>
                        <div class="callstxtgen">
                         <asp:Label ID="lbl_Requests" runat="server" CssClass="callstxt" Text="Calls:"></asp:Label>
                         <asp:Label ID="lblRequests" runat="server" CssClass="callstxt"></asp:Label>                            
                            
                       
                        </div>
                            <div class="employees">&nbsp;</div>
                            
                                <div class="employeestxtgen">
                                    <asp:Label ID="lbl_Employees" runat="server" CssClass="callstxt" Text="Employees:"></asp:Label>
                                    <asp:Label ID="lblEmployees" runat="server" ></asp:Label>
                                </div>
                                
                                <div class="handup">                                   
                                    
                                </div>
                                <div class="handuptxtgen">
                                <asp:Label ID="lbl_handup" runat="server"></asp:Label> 
                                   
                                          
                                </div>
                                <asp:HiddenField ID="_HiddenField_LoadFailed" runat="server" Value="There was an error, please enter info again!!!" />
                                <div class="handown">
                                   
                                </div>
                                <div class="handuptxtgen"> <asp:Label ID="lbl_handown" runat="server"></asp:Label></div>
                                <div class="comments">&nbsp;</div>
                                <div class="commentstxtgen"><a href='<%=_pathReview%>' target='_blank' class="clientLinks">Read/Add Reviews</a></div>
                 </div>
           			 
           		
           				 <div class="sep"><!-- --></div>
        					<div class="clientdesc">
           						 <asp:Label ID="lbl_Desc" runat="server" CssClass="titlesmall" Text="Description:"></asp:Label>
           						 <br />
                                 <asp:Label ID="lblDesc" runat="server" ></asp:Label>
            				</div>
            				
                            <div >
                              <asp:Label ID="lbl_Expertise" runat="server" CssClass="titlesmall" Text="Services:" ></asp:Label>
                              <br />
                              <asp:Label ID="lblExpertise" runat="server" ></asp:Label>  
                            </div>
    <div class="sep"><!-- --></div>
             
              <div id="divVideo" class="video" runat="server">
            <asp:Label ID="lbl_video" runat="server" CssClass="titlegen" Text="My Video"></asp:Label><br /><br />
            
            <iframe title="YouTube video player" width="560" height="340" src="<%=VideoUrl%>" 
            frameborder="0" allowfullscreen  onload="IsVideoOk(this);" name="yoav"></iframe>
            
        </div>

        
    <div class="sep"><!-- --></div>
        <div class="flash" id="_flash" runat="server">  
            <!--ifFlash = File.Exists(Server.MapPath(ResolveUrl("~") + "images/Professionals/" + supplierGUID + "/shai_Gallery.html"));-->
            <asp:Label ID="lbl_MyPhotos" runat="server" CssClass="titlegen" Text="My Photos"></asp:Label><br />
            <iframe  frameborder="0" width="600"  scrolling ="no"  height="250" src="<%=professionalImagesPrefixWeb%>/shai_Gallery.html"/>
            
        </div>
         
         <div class="clear"></div>
        <div class="sep"><!-- --></div>
          <div class="clear"></div>
 
 	</div>
 	          <div class="clear"></div>
        
    <asp:Label ID="lbl_DirectPhone" runat="server" Text="Direct phone"></asp:Label>
</div>
</div>
</form>
</body>
</html>









