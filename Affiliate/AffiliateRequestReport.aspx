﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Affiliate/MasterPageAffiliate.master" AutoEventWireup="true" CodeFile="AffiliateRequestReport.aspx.cs" Inherits="Affiliate_AffiliateRequestReport" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>

<%@ MasterType VirtualPath="~/Affiliate/MasterPageAffiliate.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">

<link href="../StyleAutoComplete.css" rel="stylesheet" type="text/css"/>
<link href="../AutoCompleteCity/StyleAutoComplete.css" rel="stylesheet" type="text/css"/>
 
<script  type="text/javascript" src="../Calender/Calender.js"></script>

<script type="text/javascript" >
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);

    }
    function BeginHandler() {
        
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
</cc1:ToolkitScriptManager> 
          
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
    	
	    <div id="form-analytics" >
    	       
               <div class="main-inputs" runat="server" id="PublisherInput">					
	                <asp:UpdatePanel id="_UpdatePanelSearch" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>   
                        <div id="div_experties" class="form-field" runat="server">
					            <label for="form-subsegment" class="label" runat="server" id="lblExperties" >Heading</label>
                            <asp:DropDownList ID="ddl_Heading" runat="server" CssClass="form-select">
                            </asp:DropDownList>
					            
			            </div>
            			 
			            <div class="form-field">
                          <label for="form-subsegment" class="label" runat="server" id="lbl_RequestId">Request Id</label>
			              <asp:TextBox ID="txtRequestId" CssClass="form-text" runat="server" ></asp:TextBox>										
                          <asp:CompareValidator ID="CompareValidator1" runat="server" Type="Integer"  Operator="DataTypeCheck" ControlToValidate="txtRequestId" Display="Dynamic">
                             <asp:Label ID="lbl_RequestIdError" runat="server" Text="Must be number" Visible="true"></asp:Label>
                          </asp:CompareValidator> 
                       </div>            
                    
                    </ContentTemplate>
		            </asp:UpdatePanel>
		            <div class="clear"></div>
		            <div class="form-field form-group">
		             <asp:Label ID="lbl_type" CssClass="label" runat="server" Text="Type"></asp:Label> 
                        <asp:DropDownList ID="ddl_Type"  CssClass="form-select" runat="server">
                        </asp:DropDownList>
        		    
        		    </div>
                     <div class="form-field">
                          <label for="form-subsegment" class="label" runat="server" id="l_Toolbar">Toolbar ID</label>
			             <asp:DropDownList ID="ddl_ToolbarId"  CssClass="form-select" runat="server">
                        </asp:DropDownList>
                       </div>  
                     
		           </div> 
        		   
                    <div class="callreportaf"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>        
                 
		  
    		
    	
    	</div>
    	<div class="clear"></div>
    	
	    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	    <ContentTemplate>	
        <div class="results">
            <asp:Label ID="lbl_RecordMached" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lbl_RecordNotToPay" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lbl_RecordToPay" runat="server"></asp:Label>
        </div>
        <div id="Table_Report" class="table" runat="server" >
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                CssClass="data-table">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                 
                
                <Columns>                     
                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label01" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text="<%# Bind('Date') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField Visible="false">              
                    <HeaderTemplate>
                        <asp:Label ID="Label02" runat="server" Text="<%# lbl_Id.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('CaseNumber') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label03" runat="server" Text="<%# lbl_Region.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text="<%# Bind('RegionName') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label04" runat="server" Text="<%# lbl_Heading.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text="<%# Bind('ExpertiseName') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label05" runat="server" Text="<%# lbl_Consumer.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text="<%# Bind('ConsumerName') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label06" runat="server" Text="<%# lbl_Payment.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Image ID="Image6" runat="server" ImageUrl="<%# Bind('IsToPay') %>" />
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label07" runat="server" Text="<%# lbl_Status.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text="<%# Bind('PayStatusReason') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                </asp:GridView>
                <uc1:TablePaging ID="TablePaging1" runat="server" />
            </div>
        </ContentTemplate>
        </asp:UpdatePanel>
    </div>

<asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Id" runat="server" Text="Id" Visible="false"></asp:Label>
<asp:Label ID="lbl_Region" runat="server" Text="Region" Visible="false"></asp:Label>
<asp:Label ID="lbl_Heading" runat="server" Text="Heading" Visible="false"></asp:Label>
<asp:Label ID="lbl_Consumer" runat="server" Text="Consumer" Visible="false"></asp:Label>
<asp:Label ID="lbl_Payment" runat="server" Text="Payment" Visible="false"></asp:Label>
<asp:Label ID="lbl_Status" runat="server" Text="Status" Visible="false"></asp:Label>

    
<asp:Label ID="lbl_PageTitle" runat="server" Text="Request reports" Visible="false"></asp:Label>
<asp:Label ID="lbl_NoToPayment" runat="server" Text="not to pay" Visible="false"></asp:Label>
<asp:Label ID="lbl_ToPay" runat="server" Text="valid requests" Visible="false"></asp:Label>


</asp:Content>

