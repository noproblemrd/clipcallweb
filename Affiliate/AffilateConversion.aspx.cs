﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using System.Text;
using System.Configuration;
using System.IO;

public partial class Affiliate_AffilateConversion : PageSetting
{
    const string ALL_VALUE = "ALL_VALUE";
    const string FILE_NAME = "CoversionReport.xml";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dataV = null;
            SetToolbox();
            SetDatePickerSentences();
         //   LoadTypes();
            LoadExpertises();
      //      LoadConversionDetails();
            FromToDatePicker_current.SetDefaultDates(siteSetting.DateFormatClean);
        }
        SetToolboxEvents();
        Header.DataBind();
    }
    /*
    private void LoadConversionDetails()
    {
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfExposuresPicklistsContainer result = null;
        try
        {
            result = _site.GetExposuresPicklists();
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        WebReferenceSite.ExposuresPicklistsContainer Exposures = result.Value;

        ddl_Website.Items.Add(new ListItem(string.Empty, Guid.Empty.ToString()));
        foreach (WebReferenceSite.OriginWebsitesPair owp in Exposures.OriginsAndWebsites)
        {
            if (owp.Origin.Id == userManagement.GetAffiliateId)
            {
                foreach (WebReferenceSite.GuidStringPair gsp in owp.Websites)
                {
                    ddl_Website.Items.Add(new ListItem(gsp.Name, gsp.Id.ToString()));
                }
                break;
            }

        }
    }
  */
    private void SetDatePickerSentences()
    {
        FromToDatePicker_current.SetFromSentence = lbl_CreationDate.Text;
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
        if (!string.IsNullOrEmpty(PathV))
        {
            string _chart = ResolveUrl("~") + "Management/FusionCharts/Charts/MSLine.swf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadChart",
                "LoadChart('" + PathV + "','" + _chart + "');", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CleanChart",
                "CleanChart();", true);

    }
    private void SetToolbox()
    {
        _Toolbox.SetTitle(lbl_ConversionReport.Text);
    }
    private void LoadExpertises()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);
        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {
            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;
    }
    /*
    private void LoadTypes()
    {
        ddl_Type.Items.Clear();
        foreach (string it in Enum.GetNames(typeof(WebReferenceReports.eIframeType)))
        {
            ListItem li = new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "eIframeType", it), it);
            li.Selected = (it == WebReferenceReports.eIframeType.All.ToString());
            ddl_Type.Items.Add(li);
        }
    }
     * */
    protected void btn_Run_Click(object sender, EventArgs e)
    {
        WebReferenceReports.Reports _reports = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ConversionReportRequest _request = new WebReferenceReports.ConversionReportRequest();
      
        _request.ExpertiseId = new Guid(ddl_Heading.SelectedValue);
        _request.FromDate = FromToDatePicker_current.GetDateFrom;
        _request.ToDate = FromToDatePicker_current.GetDateTo;
        _request.OriginId = userManagement.GetAffiliateId;
        _request.CountryId = -1;
   //     _request.Domain = txt_Domain.Text.Trim();
        WebReferenceReports.ResultOfConversionReportResponse result = null;
        try
        {
            result = _reports.ConversionReport2(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        //     DataTable data = GetDataTable.GetDataTableFromListCorrectNumberDateTimeFormat(result.Value, siteSetting.DateFormat);
        DataTable data = new DataTable();        
        data.Columns.Add("Date", typeof(string));
        data.Columns.Add("ExposureCount", typeof(string));
        data.Columns.Add("RequestCount", typeof(string));
        data.Columns.Add("RequestPercent", typeof(string));

        foreach (WebReferenceReports.ConversionReportRow crr in result.Value.DataList)
        {
            DataRow row = data.NewRow();           
            row["Date"] = string.Format(siteSetting.DateFormat, crr.Date);
            row["ExposureCount"] = crr.ExposureCount + "";
            row["RequestCount"] = crr.RequestCount + "";
            row["RequestPercent"] = string.Format(NUMBER_FORMAT, crr.TotalRequests_Exposures) + "%";
            data.Rows.Add(row);
        }
        _GridView.DataSource = data;
        _GridView.DataBind();
        lbl_RecordMached.Text = _Toolbox.GetRecordMaches(data.Rows.Count);
        if (data.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecordDisplay", "alert('" + HttpUtility.JavaScriptStringEncode(_Toolbox.NoRecordToPrint) + "');", true);
            PathV = string.Empty;
        }
        else
            LoadChart(data);
        _UpdatePanel.Update();
        dataV = data;

        /*
        result.Value[0].CallCount;
        result.Value[0].CallsPercent;
        result.Value[0].Date;
        result.Value[0].ExposureCount;
        result.Value[0].RequestCount;
        result.Value[0].RequestPercent;
        */

    }
    private void SetToolboxEvents()
    {

        _Toolbox.PrintExec += new EventHandler(_Toolbox_PrintExec);
        _Toolbox.ExcelExec += new EventHandler(_Toolbox_ExcelExec);
    }

    void _Toolbox_ExcelExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordExport + "');", true);
            return;
        }
        ToExcel te = new ToExcel(this, lbl_ConversionReport.Text);

        if (!te.ExecExcel(dataV))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
    }

    void _Toolbox_PrintExec(object sender, EventArgs e)
    {
        if (dataV.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + _Toolbox.NoRecordToPrint + "');", true);
            return;
        }
        Session["data_print"] = dataV;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }
    protected void _GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _GridView.PageIndex = e.NewPageIndex;
        _GridView.DataSource = dataV;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }


    #region  chart
    bool LoadChart(DataTable data)
    {

        //    string SymboleValue = CriteriaSetting.GetSymbolCriteria(ceriteria, siteSetting.CurrencySymbol);
        StringBuilder sb = new StringBuilder();
        sb.Append(@"<graph  caption='" + lbl_ConversionReport.Text + @"' ");
        //    sb.Append(@"subCaption='In Thousands' ");
        sb.Append(@"labelDisplay='ROTATE' ");
        sb.Append(@"numdivlines='2' ");
        sb.Append(@"lineThickness='3' ");
        sb.Append(@"showValues='0' ");
        sb.Append(@"numVDivLines='2' ");
        sb.Append(@"formatNumberScale='1' ");
        sb.Append(@"numberSuffix='%' ");
        /*
        if (SymboleValue == "%")
            sb.Append(@"numberSuffix='%' ");
        else if (!string.IsNullOrEmpty(SymboleValue))
            sb.Append(@"numberprefix='" + SymboleValue + @"' ");
        */
        //    sb.Append(@"rotateNames='1' ");
        //     sb.Append(@"slantLabels='1' ");

        sb.Append(@"staggerLines='2' ");
        sb.Append(@"anchorRadius='3' ");
        sb.Append(@"anchorBgAlpha='60' ");
        sb.Append(@"showAlternateVGridColor='0' ");
        sb.Append(@"anchorAlpha='100' ");
        sb.Append(@"animation='1' ");
        sb.Append(@"limitsDecimalPrecision='0' ");
        sb.Append(@"divLineDecimalPrecision='1' ");

        sb.Append(@"seriesNameInToolTip='0' ");
        sb.Append(@"toolTipSepChar=' ' ");

        sb.Append(@"bgColor='FFFFFF'>");



        int modulu = data.Rows.Count / 15;
        modulu++;

        StringBuilder sbRequests = new StringBuilder();
   //     StringBuilder sbCalls = new StringBuilder();
        sb.Append(@"<categories>");
        sbRequests.Append(@"<dataset seriesName='" + lbl_RequestsPercent.Text + @"' color='808080' >");// anchorBgColor='808080' plotBorderColor='#808080' plotBorderThickness='3'
    //    sbCalls.Append(@"<dataset seriesName='" + lbl_CallsPercent.Text + @"' color='0080C0'  >");//anchorBgColor='0080C0' plotBorderColor='#0080C0' plotBorderThickness='3'
        /*
         * result.Value.DataList[0].PercentApprovedFromCalls;       
        result.Value.DataList[0].PercentRefundRequestsOfCalls;
         */
        foreach (DataRow row in data.Rows)
        {
            string RequestsPercent = row["RequestPercent"].ToString();
     //       string CallsPercent = row["CallsPercent"].ToString();
            string _title = lbl_RequestsPercent.Text + "=" + RequestsPercent;// +"\r\n" +
         //       lbl_CallsPercent.Text + "=" + CallsPercent + "\r\n";
            sb.Append(@"<category name='" + row["Date"].ToString() + @"' toolText='" + _title + "'/>");

            sbRequests.Append(@"<set value='" + RequestsPercent.Replace("%", "") + "'/>");
  //          sbCalls.Append(@"<set value='" + CallsPercent.Replace("%", "") + "'/>");
        }
        sb.Append("</categories>");
        sbRequests.Append(@"</dataset>");
//        sbCalls.Append(@"</dataset>");

        sb.Append(sbRequests.ToString());
     //   sb.Append(sbCalls.ToString());
        sb.Append(@"</graph>");


        string path = //HttpContext.Current.Request.PhysicalApplicationPath + 
            ConfigurationManager.AppSettings["professionalRecord"];
        int ToFile = 0;
        do
        {
            ToFile = new Random().Next();
        } while (File.Exists(path + ToFile + FILE_NAME));
        string _path = ToFile + FILE_NAME;

        try
        {
            using (FileStream fs = new FileStream(path + _path, FileMode.Create))
            {
                using (StreamWriter tw = new StreamWriter(fs, Encoding.UTF8))
                {
                    tw.WriteLine(sb.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting.GetSiteID);
            return false;
        }
        string pathWeb = ConfigurationManager.AppSettings["professionalRecordWeb"] + _path;
        PathV = pathWeb;
        return true;
    }

    #endregion
    
    DataTable dataV
    {
        get { return (Session["data"] == null) ? new DataTable() : (DataTable)Session["data"]; }
        set { Session["data"] = value; }
    }
    string PathV
    {
        get { return (ViewState["XmlGraph"] == null) ? "" : (string)ViewState["XmlGraph"]; }
        set { ViewState["XmlGraph"] = value; }
    }

}
