﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Affiliate_AffiliateLogin : PageSetting
{
    const string SCRAMBEL = "*5106sp";
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security- for the browser not save in the cash memory
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");
        ////

        if (!IsPostBack)
        {

            if (!string.IsNullOrEmpty(siteSetting.GetSiteID))
            {

                setTextLogin();
            }
            if (Request.Cookies["AffiliateLogin"] != null)
            {
                string _password = Request.Cookies["AffiliateLogin"].Values["AffiliateLogin"];
                if (!string.IsNullOrEmpty(_password))
                {
                    try
                    {
                        _password = EncryptString.Decrypt_String(_password, SCRAMBEL);
                    }
                    catch (Exception exc) { }

                    string[] newPassword = _password.Split(';');
                    if (newPassword.Length == 2)
                    {
                        txt_professionalPassword.Attributes.Add("value", newPassword[0]);
                        txt_professionalEmail.Text = newPassword[1];
                    }
                }
            }
            if (Request.Cookies["AffiliateRememberme"] != null && Request.Cookies["AffiliateRememberme"].Value == "false")
                ifRememberMe.Checked = false;
        }
    }

    private void setTextLogin()
    {
        string command = "SELECT dbo.GetTextLoginBySiteNameId(@siteNameId)";
        using (SqlConnection conn = DBConnection.GetConnString())
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@siteNameId", siteSetting.GetSiteID);
            lbl_LogInText.Text = (string)cmd.ExecuteScalar();
            conn.Close();
        }
    }


    protected void btnCheckCode_Click(object sender, EventArgs e)
    {

        if (siteSetting == null || string.IsNullOrEmpty(siteSetting.GetSiteID))
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            if (!DBConnection.LoadSiteId(host, this))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
        }
        string email = txt_professionalEmail.Text;
        string password = txt_professionalPassword.Text;

        //Check if try to access multi times
        bool IsApprove = DBConnection.IsLoginApproved(email, siteSetting.GetSiteID);
                
        if (!IsApprove)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "AccountLocked", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_AccountLocked.Text) + "');", true);
            return;
        }
        /**********/

        
        WebReferenceSupplier.Supplier supplier = WebServiceConfig.GetSupplierReference(this);
        WebReferenceSupplier.ResultOfUserLoginResponse _response = null;
        try
        {
            _response = supplier.UserLoginForAffiliates(email, password);
        }
        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            lblComment.Text = "Failed";
            return;
        }
        if (_response.Type != WebReferenceSupplier.eResultType.Success)
        {
            lblComment.Text = "Failed";
            return;
        }
        if (_response.Value.UserLoginStatus == WebReferenceSupplier.eUserLoginStatus.NotFound)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Affiliate);
            ClientScript.RegisterStartupScript(this.GetType(), "NotFound", "alert('" + lbl_NotFound.Text + "');", true);
            return;
        }

        if (_response.Value.AffiliateOrigin == Guid.Empty || _response.Value.UserId == Guid.Empty)
        {
            DBConnection.InsertLogin(email, Guid.Empty, siteSetting.GetSiteID, eUserType.Affiliate);
            if (_response.Value.SecurityLevel > 0)
                ClientScript.RegisterStartupScript(this.GetType(), "PublisherUser", "WrongLogin('" + HttpUtility.JavaScriptStringEncode(lbl_PublisherUser.Text) + "', '" + ResolveUrl("~") + @"Publisher/PublisherLogin.aspx" + "');", true);
            else
                ClientScript.RegisterStartupScript(this.GetType(), "AdvertiserUser", "WrongLogin('" + HttpUtility.JavaScriptStringEncode(lbl_AdvertiserUser.Text) + "', '" + ResolveUrl("~") + @"Management/ProfessionLogin.aspx" + "');", true);
            return;
        }
        DBConnection.InsertLogin(email, _response.Value.UserId, siteSetting.GetSiteID, eUserType.Affiliate);
        userManagement = new UserMangement(_response.Value.UserId.ToString(), _response.Value.Name, _response.Value.AffiliateOrigin);
        userManagement.SetUserObject(this);
        int sitelangid = DBConnection.GetSiteLangIdByUserId(new Guid(userManagement.GetGuid), siteSetting.GetSiteID);

        if (sitelangid > 0)
        {
            siteSetting.siteLangId = sitelangid;
            Response.Cookies["language"].Value = sitelangid.ToString();
            Response.Cookies["language"].Expires = DateTime.Now.AddMonths(3);
        }


       
        if (ifRememberMe.Checked)
        {
            string newPassword = password + ";" + email;
            newPassword = EncryptString.Encrypt_String(newPassword, SCRAMBEL);
            Response.Cookies["AffiliateLogin"]["AffiliateLogin"] = newPassword;
            Response.Cookies["AffiliateLogin"].Expires = DateTime.Now.AddMonths(3);
            Response.Cookies["AffiliateRememberme"].Values.Clear();
        }
        else
        {
            Response.Cookies["AffiliateLogin"].Values.Clear();
            Response.Cookies["AffiliateRememberme"].Value = "false";
            Response.Cookies["AffiliateRememberme"].Expires = DateTime.Now.AddMonths(3);
        }


        Response.Redirect("AffiliateRequestReport.aspx");
    }
    /*
    void InsertLogin(string email, Guid _id)
    {
        
        string command = "EXEC dbo.InsertLogin @UserName, @SiteNameId, @UserId, @IP, @Success";
        SqlConnection conn = DBConnection.GetConnString();
        conn.Open();
        SqlCommand cmd = new SqlCommand(command, conn);
        cmd.Parameters.AddWithValue("@UserName", email);
        cmd.Parameters.AddWithValue("@SiteNameId", siteSetting.GetSiteID);
        cmd.Parameters.AddWithValue("@IP", Utilities.GetIP(Request));
        if (_id == Guid.Empty)
        {
            cmd.Parameters.AddWithValue("@UserId", DBNull.Value);
            cmd.Parameters.AddWithValue("@Success", false);
        }
        else
        {
            cmd.Parameters.AddWithValue("@UserId", _id);
            cmd.Parameters.AddWithValue("@Success", true);
        }
        int a = cmd.ExecuteNonQuery();
        conn.Close();
        
    }
     * */
}