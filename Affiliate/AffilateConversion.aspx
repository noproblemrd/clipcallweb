﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Affiliate/MasterPageAffiliate.master" AutoEventWireup="true" CodeFile="AffilateConversion.aspx.cs" Inherits="Affiliate_AffilateConversion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/DatePick/FromToDatePicker.ascx" tagname="FromToDatePicker" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<link rel="stylesheet" href="../Management/FusionCharts/Contents/Style.css" type="text/css" />
<link rel="stylesheet" href="../StyleDatePicker/jquery-ui-1.8.16.custom.css" type="text/css" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="../scripts/jquery-ui-1.8.17.custom.datepicker.min.js"></script>

<script type="text/javascript" src="../Management/FusionCharts/JSClass/FusionCharts.js"></script>
<script type="text/javascript">
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);
    }
    function BeginHandler() {
        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }

    function LoadChart(fileXML, _chart) {

        var chart = new FusionCharts(_chart, 'ChartId2', '650', '400', '0', '0');
        chart.addParam("WMode", "Transparent");
        chart.setDataURL(fileXML);
        chart.render("div_chart");
    }
    function CleanChart() {
        document.getElementById("div_chart").innerHTML = "";
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" >
    
</cc1:ToolkitScriptManager>
<uc1:ToolboxReport ID="_Toolbox" runat="server" />
<div class="page-content minisite-content2"> 
  <div id="form-analytics">
      <div class="main-inputs">	
               
            <div class="form-field">
                <asp:Label ID="lbl_Heading" runat="server" CssClass="label" Text="Heading"></asp:Label>          
	            <asp:DropDownList ID="ddl_Heading" CssClass="form-select" runat="server">
               </asp:DropDownList>
            </div>
            
            
        </div>
        
       
        <div class="callreport">
              
             <uc1:FromToDatePicker ID="FromToDatePicker_current" runat="server" />
        </div>

        <div class="clear"></div>
        
        </div>   
           
              <div class="clear"></div>
            <div class="headingConversionAffiliate">
                <asp:Button ID="btn_Run" runat="server" CssClass="CreateReportSubmit2" Text="Create report" 
                    onclick="btn_Run_Click"  />
            </div>
            
           
            
            <div class="TblConversionAffilate">
            
                <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                 <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
                   <div id="div_chart"></div> 
                    <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false"
                            CssClass="data-table2" AllowPaging="True" 
                            onpageindexchanging="_GridView_PageIndexChanging" PageSize="20">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />        
                        <FooterStyle CssClass="footer"  />
                        <PagerStyle CssClass="pager" />
                        <Columns>
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="<%# lbl_Date.Text %>"></asp:Label>
                                                   
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text="<%# Bind('Date') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label3" runat="server" Text="<%# lbl_Exposers.Text %>"></asp:Label>
                                                   
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text="<%# Bind('ExposureCount') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label5" runat="server" Text="<%# lbl_Requests.Text %>"></asp:Label>
                                                   
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text="<%# Bind('RequestCount') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField >              
                            <HeaderTemplate>
                                <asp:Label ID="Label7" runat="server" Text="<%# lbl_RequestsPercent.Text %>"></asp:Label>
                                                   
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text="<%# Bind('RequestPercent') %>"></asp:Label>                  
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                           
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            
            
        </div>


    
<asp:Label ID="lbl_ConversionReport" runat="server" Text="Conversion Report" Visible="false"></asp:Label>


<asp:Label ID="lbl_Date" runat="server" Text="Date" Visible="false"></asp:Label>
<asp:Label ID="lbl_Exposers" runat="server" Text="Exposures" Visible="false"></asp:Label>
<asp:Label ID="lbl_Requests" runat="server" Text="Requests" Visible="false"></asp:Label>
<asp:Label ID="lbl_RequestsPercent" runat="server" Text="Requests percent" Visible="false"></asp:Label>

<asp:Label ID="lbl_All" runat="server" Text="All" Visible="false"></asp:Label>

<asp:Label ID="lbl_CreationDate" runat="server" Text="Creation Date from" Visible="false"></asp:Label>

    
</asp:Content>

