﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;

public partial class Affiliate_aff_RootUrlExposureFullUrl : AffiliatePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        if (!IsPostBack)
        {
            string _url = Request.QueryString["url"];
            string _from = Request.QueryString["from"];
            string _to = Request.QueryString["to"];
            string _keyword = Request.QueryString["keyword"];
            if (string.IsNullOrEmpty(_url) || string.IsNullOrEmpty(_from) || string.IsNullOrEmpty(_to) || string.IsNullOrEmpty(_keyword))
            {
                closeTheWindow();
                return;
            }
            LoadDetails(_url, _from, _to, _keyword);
        }
        Header.DataBind();
    }

    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (RequestV == null)
            return;
        WebReferenceReports.RootUrlExposuresFullUrlRequest _request = RequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        DataResult dt = GetDataReport(_request);
        BindData(dt.data);
        LoadGeneralData(dt.TotalPages, dt.TotalRows, dt.CurrentPage);
    }

    private void LoadDetails(string _url, string _from, string _to, string _keyword)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.RootUrlExposuresFullUrlRequest _request = new WebReferenceReports.RootUrlExposuresFullUrlRequest();
        _request.OriginId = userManagement.GetAffiliateId;
        _request.FromDate = ConvertToDateTime.CalanderToDateTime(_from, siteSetting.CrmDateFormat);
        _request.RootUrl = _url;
        _request.ToDate = ConvertToDateTime.CalanderToDateTime(_to, siteSetting.CrmDateFormat);
        _request.PageSize = 20;
        _request.PageNumber = 1;
        _request.Keyword = _keyword;
        lbl_KeywordTitleVal.Text = _keyword;
        lbl_UrlTitleVal.Text = _url;
        RequestV = _request;
        DataResult dr = GetDataReport(_request);

        if (dr == null)
        {
            Update_Faild();
            ClearTable();
            return;
        }
        if (dr.data.Rows.Count == 0)
        {
            ClearTable();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
            return;
        }
        BindData(dr.data);
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);

    }
    private DataResult GetDataReport(WebReferenceReports.RootUrlExposuresFullUrlRequest _request)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfRootUrlExposuresReportResponse result = null;
        try
        {
            result = _report.RootUrlExposureFullUrlDrillDown(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return null;
        }
        DataTable data = GetDataTable.GetDataTableFromListCorrectNumber(result.Value.Rows);
        DataResult dr = new DataResult() { data = data, CurrentPage = result.Value.CurrentPage, TotalRows = result.Value.TotalRows, TotalPages = result.Value.TotalPages };
        return dr;
        
        /*
        result.Value.Rows[0].CTR
        result.Value.Rows[0].Exposures;
        result.Value.Rows[0].Requests;
        result.Value.Rows[0].UrlRoot;
         * */
    }
    protected void BindData(DataTable data)
    {
        _GridView.DataSource = data;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    void ClearTable()
    {
        _GridView.DataSource = null;
        _GridView.DataBind();
        LoadGeneralData(0, 0, 0);
        _UpdatePanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = _TotalRows + " " + lbl_RecordMatch.Text;
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    protected void _GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        /*
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;
        HtmlAnchor ha = (HtmlAnchor)e.Row.FindControl("a_UrlRoot");
        ha.Attributes["onclick"] = "OpenRootDomain(this, '" + string.Format(siteSetting.CrmDateFormat, RequestV.FromDate) + "', '" + string.Format(siteSetting.CrmDateFormat, RequestV.ToDate) + "');";
         * */
    }
    void closeTheWindow()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "CloseWindow", "CloseWindow();", true);
    }
    protected string GetLogin
    {
        get { return "LogOut.aspx"; }
    }
    WebReferenceReports.RootUrlExposuresFullUrlRequest RequestV
    {
        get { return (ViewState["RequestV"] == null) ? null : (WebReferenceReports.RootUrlExposuresFullUrlRequest)ViewState["RequestV"]; }
        set { ViewState["RequestV"] = value; }
    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
}