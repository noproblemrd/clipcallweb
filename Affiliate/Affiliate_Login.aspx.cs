﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Affiliate_Affiliate_Login : PageSetting
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security- for the browser not save in the cash memory
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1); // set expiry date in the past 
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");
        ////
        /*****************/
        Response.Redirect("AffiliateLogin.aspx");
        return;
        /*****************/

       
    }
   
}