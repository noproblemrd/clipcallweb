﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Affiliate/MasterPageAffiliate.master" AutoEventWireup="true" CodeFile="Aff_RootDomain.aspx.cs" Inherits="Affiliate_Aff_RootDomain_" %>

<%@ Register Src="~/Controls/FromToDate.ascx" TagName="FromToDate" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Toolbox/ToolboxReport.ascx" TagName="ToolboxReport" TagPrefix="uc1" %>
<%@ Register src="~/Controls/TablePaging.ascx" tagname="TablePaging" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script  type="text/javascript" src="../Calender/Calender.js"></script>

<script type="text/javascript" >
    window.onload = appl_init;
    function appl_init() {
        var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
        pgRegMgr.add_beginRequest(BeginHandler);
        pgRegMgr.add_endRequest(EndHandler);

    }
    function BeginHandler() {

        showDiv();
    }
    function EndHandler() {
        hideDiv();
    }
    function OpenRootDomain(elem, _from, _to) {

        var _url = encodeURIComponent($(elem).find('span').html());
        var _opener = window.open('aff_RootUrlExposureKeyword.aspx?url=' + _url + '&from=' + _from + '&to=' + _to, 'RootDomain', 'width=750,height=800,resizable=yes');
        _opener.focus();
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
  <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">              
</cc1:ToolkitScriptManager> 
    <uc1:ToolboxReport runat="server" ID="ToolboxReport1" />               
    <div class="page-content minisite-content2" >
   
       
	    <div id="form-analytics" >
             <div class="form-field div_exposureRight">
                <asp:Label ID="lblExposures" runat="server" CssClass="label" Text="<%# lbl_Exposures.Text %>"></asp:Label>          
                <div class="div_exposureLeft">
	                <asp:DropDownList ID="ddl_Operators" CssClass="form-select" runat="server">
                    </asp:DropDownList>
                </div>
                <div>
                    <asp:TextBox ID="txt_Exposures" runat="server" CssClass="form-text"></asp:TextBox>
                    <asp:RangeValidator ID="rv_Exposure" runat="server" ErrorMessage="Number value bigger then zero" ControlToValidate="txt_Exposures"
                     CssClass="error-msg" Type="Integer" MinimumValue="0"></asp:RangeValidator>
                 </div>
            </div>
        
            
                    <div class="callreportaf Two-lines"><uc1:FromToDate ID="FromToDate1" runat="server" /></div>        
            <div class="form-field Line2">
                <asp:Label ID="lbl_TextUrl" runat="server" CssClass="label" Text="URL"></asp:Label>
                <asp:TextBox ID="txt_TextUrl" runat="server" CssClass="form-text"></asp:TextBox>
            </div>
    	</div>
    	<div class="clear"></div>
    	
	    <asp:UpdatePanel ID="_UpdatePanel" runat="server" UpdateMode="Conditional">
	    <ContentTemplate>	
        <div class="results"><asp:Label ID="lbl_RecordMached" runat="server"></asp:Label></div>
        <div id="Table_Report" class="table" runat="server" >
            <asp:GridView ID="_GridView" runat="server" AutoGenerateColumns="false" 
                CssClass="data-table" onrowdatabound="_GridView_RowDataBound">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <Columns>                     
                    <asp:TemplateField ControlStyle-Width="400" ControlStyle-Font-Overline="false">              
                    <HeaderTemplate>
                        <asp:Label ID="Label01" runat="server" Text="<%# lbl_UrlRoot.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a id="a_UrlRoot" runat="server" href="javascript:void(0);">
                            <asp:Label ID="Label1" runat="server" Text="<%# Bind('UrlRoot') %>"></asp:Label>          
                        </a>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label02" runat="server" Text="<%# lbl_Exposures.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text="<%# Bind('Exposures') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label03" runat="server" Text="<%# lbl_Requests.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text="<%# Bind('Requests') %>"></asp:Label>          
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField >              
                    <HeaderTemplate>
                        <asp:Label ID="Label04" runat="server" Text="<%# lbl_CTR.Text %>"></asp:Label>                                           
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text="<%# Bind('CTR') %>"></asp:Label>          
                        <asp:Label ID="Label5" runat="server" Text="%"></asp:Label>    
                    </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                </asp:GridView>
                <uc1:TablePaging ID="TablePaging1" runat="server" />
            </div>
        </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <asp:Label ID="lbl_UrlRoot" runat="server" Text="Root URL" Visible="false"></asp:Label>
     <asp:Label ID="lbl_Exposures" runat="server" Text="Exposures" Visible="false"></asp:Label>
    <asp:Label ID="lbl_Requests" runat="server" Text="Requests" Visible="false"></asp:Label>
        <asp:Label ID="lbl_CTR" runat="server" Text="CTR" Visible="false"></asp:Label>

   
   <asp:Label ID="lbl_NoResult" runat="server" Visible="false" Text="There are not results"></asp:Label>    

   <asp:Label ID="lbl_RootUrlName" runat="server" Visible="false" Text="Root URL Report"></asp:Label>    

</asp:Content>

