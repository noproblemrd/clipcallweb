﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;

public partial class Affiliate_AffiliateRequestReport : PageSetting
{
    const int DAYS_INTERVAL = 2;
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    const string EXCEL_NAME = "RequestReport";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec +=new EventHandler(FromToDate1_ReportExec);
        TablePaging1._lnkPage_Click += new EventHandler(_Paging_click);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
    //    TablePagingConfiguration();
        if (!IsPostBack)
        {
            RequestsReportRequestV = null;
            SetToolbox();
            SetDateInterval();
            LoadExperties();
            LoadRequestTypes();
            LoadToolbars();
            
         //   Clearreports();
            ExecRequestReport();
        }
        
        SetToolboxEvents();
        
    }

    private void LoadToolbars()
    {
        ddl_ToolbarId.Items.Clear();
        ddl_ToolbarId.Items.Add(new ListItem("", Guid.Empty.ToString()));
        WebReferenceSite.Site _site = WebServiceConfig.GetSiteReference(this);
        WebReferenceSite.ResultOfListOfGuidStringPair result = null;
        try
        {
            result = _site.GetToolbarsId(userManagement.GetAffiliateId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, userManagement);
            Update_Faild();
            return;
        }
        if (result.Type == WebReferenceSite.eResultType.Failure)
        {
            Update_Faild();
            return;
        }
        foreach (WebReferenceSite.GuidStringPair gsp in result.Value)
        {
            ddl_ToolbarId.Items.Add(new ListItem(gsp.Name, gsp.Id.ToString()));
        }
        ddl_ToolbarId.SelectedIndex = 0;
    }
    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
    /*
    #region IOnLoadControl Members

    public void __OnLoad(UserControl ControlSender)
    {
        TablePaging _tp = (TablePaging)ControlSender;
        _tp._lnkPage_Click += new EventHandler(_Paging_click);
        _tp.PagesInPaging = PAGE_PAGES;
        _tp.ItemInPage = ITEM_PAGE;
    }

    #endregion
   */
    protected void _Paging_click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (RequestsReportRequestV == null)
            return;
        WebReferenceReports.RequestsReportRequest _request = RequestsReportRequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        DataResultRequest drr = GetDataResult(_request);
        SetGeneralDetails(drr);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (TotalCountRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        WebReferenceReports.RequestsReportRequest requestReportRequest = RequestsReportRequestV;
        requestReportRequest.PageSize = -1;
        requestReportRequest.PageNumber = -1;

        DataResultRequest drr = GetDataResult(requestReportRequest);
        if (drr == null)
        {
            Update_Faild();
            return;
        }
        ToExcel te = new ToExcel(this, EXCEL_NAME);
        if (!te.ExecExcel(drr.data))
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
        
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        
        if (TotalCountRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        WebReferenceReports.RequestsReportRequest requestReportRequest = RequestsReportRequestV;
        requestReportRequest.PageSize = -1;
        requestReportRequest.PageNumber = -1;

        DataResultRequest drr = GetDataResult(requestReportRequest);
        if (drr == null)
        {
            Update_Faild();
            return;
        }

        Session["data_print"] = drr.data;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
       
    }
    void  FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecRequestReport();
    }
    void ExecRequestReport()
    {
        Clearreports();

        DateTime _to = FromToDate1.GetDateTo;
        DateTime _from = FromToDate1.GetDateFrom;

        if (_to == DateTime.MinValue)
        {
            _to = DateTime.Now;

        }

        if (_from == DateTime.MinValue)
        {
            _from = _to.AddMonths(-1);

        }
        else
        {
            if (_from > _to)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + FromToDate1.GetDateError + "');", true);

                return;
            }

        }
        WebReferenceReports.RequestsReportRequest requestReportRequest = new WebReferenceReports.RequestsReportRequest();
        requestReportRequest.PageSize = ITEM_PAGE;
        requestReportRequest.FromDate = _from;
        requestReportRequest.ToDate = _to;
        Guid Toolbar_id;
        if (!Guid.TryParse(ddl_ToolbarId.SelectedValue, out Toolbar_id))
            Toolbar_id = Guid.Empty;
        requestReportRequest.ToolbarId = Toolbar_id;

        string RequestId = txtRequestId.Text.Trim();
        requestReportRequest.Type = (WebReferenceReports.CaseType)class_enum.GetValue(typeof(WebReferenceReports.CaseType), ddl_Type.SelectedValue);
        requestReportRequest.ExpertiseId = new Guid(ddl_Heading.SelectedValue);
        requestReportRequest.CaseNumber = RequestId;


        requestReportRequest.AffiliateOrigin = userManagement.GetAffiliateId;
        requestReportRequest.PageNumber = 1;
        //    LoadRequestTable(requestReportRequest, 1);
        DataResultRequest drr = GetDataResult(requestReportRequest);

        RequestsReportRequestV = requestReportRequest;
        SetGeneralDetails(drr);     
    }
    void SetGeneralDetails(DataResultRequest drr)
    {
        if (drr == null)
        {
            Update_Faild();
            ClearAllDetails();
            return;
        }
        TablePaging1.Current_Page = drr.CurrentPage;
        TablePaging1.Pages_Count = drr.TotalPages;
        TotalCountRowsV = drr.TotalRows;
        lbl_RecordMached.Text = drr.TotalRows + " " + ToolboxReport1.RecordMached;
        lbl_RecordToPay.Text = drr.RecordToPay + " " + lbl_ToPay.Text;
        lbl_RecordNotToPay.Text = drr.RecordNotToPay + " " + lbl_NoToPayment.Text;
        TablePaging1.LoadPages();

        _GridView.DataSource = drr.data;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    void ClearAllDetails()
    {
        TablePaging1.Current_Page = 0;
        TablePaging1.Pages_Count = 0;
        TotalCountRowsV = 0;
        lbl_RecordMached.Text = string.Empty;
        lbl_RecordToPay.Text = string.Empty;
        lbl_RecordNotToPay.Text = string.Empty;
        TablePaging1.LoadPages();
        _GridView.DataSource = null;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    
    DataResultRequest GetDataResult(WebReferenceReports.RequestsReportRequest requestReportRequest)
    {
        WebReferenceReports.Reports _site = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfRequestsReportResponse resultRequestReport;
        try
        {
            resultRequestReport = _site.RequestsReport(requestReportRequest);
            if (resultRequestReport.Type != WebReferenceReports.eResultType.Success)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
                return null;

            }

        }

        catch (Exception ex)
        {
            dbug_log.ExceptionLog(ex, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return null;
        }
       
        DataTable data = new DataTable();
        data.Columns.Add("Date", typeof(string));
        data.Columns.Add("CaseNumber", typeof(string));
        data.Columns.Add("RegionName", typeof(string));
        data.Columns.Add("ExpertiseName", typeof(string));
        data.Columns.Add("ConsumerName", typeof(string));
        data.Columns.Add("IsToPay", typeof(string));
        data.Columns.Add("PayStatusReason", typeof(string));
       
        foreach (WebReferenceReports.IncidentData id in resultRequestReport.Value.Incidents)
        {
            DataRow row = data.NewRow();
            row["Date"] = string.Format(siteSetting.DateFormat, id.CreatedOn) + " " +
                string.Format(siteSetting.TimeFormat, id.CreatedOn);
            row["CaseNumber"] = id.CaseNumber;
            row["RegionName"] = id.RegionName;
            row["ExpertiseName"] = id.ExpertiseName;
            row["ConsumerName"] = id.ConsumerName;
            row["IsToPay"] = (id.IsToPay) ? "images/icon-v.png" : "images/icon-x.png";
            row["PayStatusReason"] = id.PayStatusReason;
            data.Rows.Add(row);
           
        }
        DataResultRequest dr = new DataResultRequest()
        {
            data = data,
            CurrentPage = resultRequestReport.Value.CurrentPage,
            TotalPages = resultRequestReport.Value.TotalPages,
            TotalRows = resultRequestReport.Value.TotalRows,
            RecordNotToPay = resultRequestReport.Value.NotToPay,
            RecordToPay = resultRequestReport.Value.ToPay
        };
        return dr;
    }
    private void Clearreports()
    {
 	    _GridView.DataSource = null;
        _GridView.DataBind();
        _UpdatePanel.Update();
    }
    private void LoadRequestTypes()
    {
        ddl_Type.Items.Clear();
        foreach (WebReferenceReports.CaseType ct in Enum.GetValues(typeof(WebReferenceReports.CaseType)))
        {
            ListItem li = new ListItem(EditTranslateDB.GetControlTranslate(siteSetting.siteLangId, "CaseType", ct.ToString()), ct.ToString());
            li.Selected = (ct == WebReferenceReports.CaseType.All);
            ddl_Type.Items.Add(li);
        }
    }
    private void LoadExperties()
    {
        ddl_Heading.Items.Clear();
        WebReferenceSite.Site sit = WebServiceConfig.GetSiteReference(this);
        string result = string.Empty;
        try
        {
            result = sit.GetPrimaryExpertise(siteSetting.GetSiteID, "");
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        XDocument xdd = XDocument.Parse(result);

        if (xdd.Element("PrimaryExpertise") == null || xdd.Element("PrimaryExpertise").Element("Error") != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Failed", "top.UpdateFailed();", true);
            return;
        }
        ddl_Heading.Items.Add(new ListItem("", Guid.Empty.ToString()));
        foreach (XElement nodePrimary in xdd.Element("PrimaryExpertise").Elements())
        {

            string pName = nodePrimary.Attribute("Name").Value;
            string _id = nodePrimary.Attribute("Code").Value;
            string _guid = nodePrimary.Attribute("ID").Value;
            ddl_Heading.Items.Add(new ListItem(pName, _guid));
        }
        ddl_Heading.SelectedIndex = 0;
    }
    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_PageTitle.Text);
    }
    
    WebReferenceReports.RequestsReportRequest RequestsReportRequestV
    {
        get
        {
            return (Session["RequestsReportRequest"] == null) ? new WebReferenceReports.RequestsReportRequest() : (WebReferenceReports.RequestsReportRequest)Session["RequestsReportRequest"];
        }
        set { Session["RequestsReportRequest"] = value; }
    }
    int TotalCountRowsV
    {
        get { return (ViewState["TotalCountRows"] == null) ? 0 : (int)ViewState["TotalCountRows"]; }
        set { ViewState["TotalCountRows"] = value; }
    }


    
}