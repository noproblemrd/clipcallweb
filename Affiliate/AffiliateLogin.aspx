﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Affiliate/MasterPagePreAffiliate.master" AutoEventWireup="true" CodeFile="AffiliateLogin.aspx.cs" Inherits="Affiliate_AffiliateLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../Management/stylelogin.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" language="javascript">
        function WrongLogin(_message, _direct) {
            alert(_message);
            window.location = _direct;
        }
    
    
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="primary-content">
		<div class="form-login">
				<h2>
                <asp:Label ID="lbl_title" runat="server">Welcome to Affiliate Portal</asp:Label>
				</h2>
				
				<asp:Label runat="server" ID="lblComment" Font-Size="14pt" ForeColor="red" Font-Bold="true"></asp:Label>
				<div class="form-field-wrap">
				 <div class="form-field">
					<label for="form-email" runat="server" id="lbl_email">Your E-mail</label>
						<asp:TextBox runat="server" CssClass="form-input" text="" id="txt_professionalEmail" 
						textmode="SingleLine" TabIndex="0" autocomplete="off"></asp:TextBox>
						
						<asp:Image ID="img_email" runat="server" ImageUrl="../Management/imageslogin/icon-q.png" AlternateText="Insert your email" />
						<span class="form-error">
						<asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" Display="Dynamic"
						     runat="server" ErrorMessage="Missing email" ControlToValidate="txt_professionalEmail"></asp:RequiredFieldValidator>
						    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" Display="Dynamic"
						     runat="server" ErrorMessage="Invalid email" ControlToValidate="txt_professionalEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
						 </span>
						</div>    
						
						<div class="form-field">
					<label for="form-password" runat="server" id="lbl_password">Your password</label>
						<asp:TextBox runat="server"  CssClass="form-input"  id="txt_professionalPassword"  
						 TabIndex="0"  textmode="Password" OnTextChanged="btnCheckCode_Click"></asp:TextBox>
                        
                        
						<asp:Image ID="img_password" runat="server" ImageUrl="../Management/imageslogin/icon-q.png" AlternateText="Insert password" 
                        style=" padding: 0px 0 0 0px; bottom:0px; margin-bottom:-3px; " />
						<span class="form-error">
						    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ErrorMessage="Missing password" 
						    ControlToValidate="txt_professionalPassword" Display="Dynamic"></asp:RequiredFieldValidator>
                        </span>
                       </div>
                       </div>
						
                
                 
					
					
                 <asp:Button  runat="server" Text="Login" id="btnCheckCode" CssClass="form-submit" TabIndex="0" 
                 OnClick="btnCheckCode_Click"   />
					<p class="service">
						 <input id="ifRememberMe" style="vertical-align: middle;" class="form-checkbox" type="checkbox" 
						checked="checked" runat="server" dir="rtl" />
						<label for="<%= ifRememberMe.ClientID %>" style="width:auto;">
						<span class="fc-red">
                        <asp:Label ID="lbl_rememberMe" runat="server" Text="Remember me"></asp:Label>
						</span> 
						</label>
						<asp:Image ID="img_RememberMe" runat="server" ImageUrl="../Management/imageslogin/icon-q.png" AlternateText="Remember me" 
                        style=" padding: 0px 0 0 0px; bottom:0px; margin-bottom:-3px; " />
						<a class="link-help" href='<%=ResolveUrl("~")%>ResetPassword.aspx?IsPublisher=true'>
                         <asp:Label ID="lbl_cantGetInto" runat="server" cssClass="fc-red" Text="Can't get into your account?"></asp:Label>
						 </a>
						
						<a href="javascript:void(0)" target="_blank">
                        <asp:Label ID="lbl_support" runat="server" Text="Support"></asp:Label>
						</a>
						<br />
                        <asp:Label ID="lbl_version" runat="server" Text="Version 2.9.2" CssClass="version_num" ></asp:Label>
						
					</p>
					
			
			</div>
		</div>
		<div id="sidebar">
		
			<div class="add">
		
				<p>
				
            <asp:Label ID="lbl_LogInText" runat="server"></asp:Label>
            </p>
			</div>
			
			
		</div>
	

<asp:hiddenfield ID="Hidden_url" runat="server"></asp:hiddenfield>
<asp:Label ID="lbl_NotFound" runat="server" Text="User or Password is not correct"
         Visible="false"></asp:Label>

<asp:Label ID="lbl_AccountLocked" runat="server" Text="Your account is locked due to many login attempts" Visible="false"></asp:Label>

<asp:Label ID="lbl_PublisherUser" runat="server" Text="You need to login by publisher portal" Visible="false"></asp:Label>

<asp:Label ID="lbl_AdvertiserUser" runat="server" Text="You need to login by Advertiser portal" Visible="false"></asp:Label>

</asp:Content>

