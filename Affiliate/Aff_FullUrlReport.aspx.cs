﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Affiliate_Aff_FullUrlReport :PageSetting
{
    const int DAYS_INTERVAL = 2;
    protected const int ITEM_PAGE = 20;
    protected const int PAGE_PAGES = 10;
    const string EXCEL_NAME = "Full URL Report";
    protected void Page_Load(object sender, EventArgs e)
    {
        FromToDate1.ReportExec += new EventHandler(FromToDate1_ReportExec);
        TablePaging1._lnkPage_Click += new EventHandler(TablePaging1__lnkPage_Click);
        ScriptManager1.RegisterAsyncPostBackControl(FromToDate1.GetBtnSubmit());
        if (!IsPostBack)
        {
            SetToolbox();
            SetDateInterval();
            ExecReport();
        }
        SetToolboxEvents();
    }
    private void SetDateInterval()
    {
        FromToDate1.SetDateIntervalInServer(DAYS_INTERVAL);
    }
    private void SetToolboxEvents()
    {
        ToolboxReport1.PrintExec += new EventHandler(ToolboxReport1_PrintExec);
        ToolboxReport1.ExcelExec += new EventHandler(ToolboxReport1_ExcelExec);
    }

    void ToolboxReport1_ExcelExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordExport) + "');", true);
            return;
        }
        WebReferenceReports.FullUrlExposureReportRequest _request = RequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataTable data = GetDataReport(_request).data;
        ToExcel te = new ToExcel(this, "Report");
        if (!te.ExecExcel(data))
            Update_Faild();
    }

    void ToolboxReport1_PrintExec(object sender, EventArgs e)
    {
        if (TotalRowsV == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoRecords", "alert('" + HttpUtility.JavaScriptStringEncode(ToolboxReport1.NoRecordToPrint) + "');", true);
            return;
        }
        WebReferenceReports.FullUrlExposureReportRequest _request = RequestV;
        _request.PageSize = -1;
        _request.PageNumber = -1;
        DataTable data = GetDataReport(_request).data;
        Session["data_print"] = data;
        Session["grid_print"] = _GridView;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "WinPrint", "window.open('" + ResolveUrl("~") + "ToPrint.aspx', '_blank');", true);
    }

    private void SetToolbox()
    {
        ToolboxReport1.SetTitle(lbl_ReportName.Text);

    }

    void TablePaging1__lnkPage_Click(object sender, EventArgs e)
    {
        int _pageNum = ((EventArgsPageIndex)e).PageIndex;
        if (RequestV == null)
            return;
        WebReferenceReports.FullUrlExposureReportRequest _request = RequestV;
        _request.PageNumber = _pageNum;
        _request.PageSize = TablePaging1.ItemInPage;
        DataResult dt = GetDataReport(_request);
        BindData(dt.data);
        LoadGeneralData(dt.TotalPages, dt.TotalRows, dt.CurrentPage);
    }

    void FromToDate1_ReportExec(object sender, EventArgs e)
    {
        ExecReport();
    }

    private void ExecReport()
    {
        DateTime _to = FromToDate1.GetDateTo;
        DateTime _from = FromToDate1.GetDateFrom;

        if (_to == DateTime.MinValue)
            _to = DateTime.Now;
        if (_from == DateTime.MinValue)
            _from = _to.AddMonths(-1);
        else
        {
            if (_from > _to)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DateFromBefore",
                    "alert('" + FromToDate1.GetDateError + "');", true);
                return;
            }
        }

        WebReferenceReports.FullUrlExposureReportRequest _request = new WebReferenceReports.FullUrlExposureReportRequest();        
        _request.FromDate = _from;
        _request.ToDate = _to;
        _request.OriginId = userManagement.GetAffiliateId;
        _request.PageNumber = 1;
        _request.PageSize = TablePaging1.ItemInPage;
        RequestV = _request;
        DataResult dr = GetDataReport(_request);

        if (dr == null)
        {
            Update_Faild();
            ClearTable();
            return;
        }
        if (dr.data.Rows.Count == 0)
        {
            ClearTable();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "NoResult", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_NoResult.Text) + "');", true);
            return;
        }
        BindData(dr.data);
        LoadGeneralData(dr.TotalPages, dr.TotalRows, dr.CurrentPage);
    }

    private DataResult GetDataReport(WebReferenceReports.FullUrlExposureReportRequest _request)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(this);
        WebReferenceReports.ResultOfFullUrlExposureReportResponse result = null;
        try
        {
            result = _report.FullUrlExposureReport(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting);
            Update_Faild();
            return null;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            Update_Faild();
            return null;
        }
        DataTable data = GetDataTable.GetDataTableFromListDateTimeFormat(result.Value.Rows, siteSetting.DateFormat, siteSetting.TimeFormat);
        DataResult dr = new DataResult() { data = data, CurrentPage = result.Value.CurrentPage, TotalRows = result.Value.TotalRows, TotalPages = result.Value.TotalPages };
        return dr;
        /*
        result.Value.Rows[0].CreatedOn;
        result.Value.Rows[0].Url;
        */
         
    }
    void ClearTable()
    {
        _GridView.DataSource = null;
        _GridView.DataBind();
        LoadGeneralData(0, 0, 0);
        _UpdatePanel.Update();
    }
    void LoadGeneralData(int _TotalPages, int _TotalRows, int _CurrentPage)
    {
        lbl_RecordMached.Text = _TotalRows + " " + ToolboxReport1.RecordMached;
        TotalRowsV = _TotalRows;
        TablePaging1.Current_Page = _CurrentPage;
        TablePaging1.Pages_Count = _TotalPages;
        TablePaging1.LoadPages();
    }
    protected void BindData(DataTable data)
    {
        // sb_Script = new StringBuilder();
        _GridView.DataSource = data;
        _GridView.DataBind();
        /*
        if (sb_Script != null && sb_Script.Length > 0)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "DisabledBlackList", sb_Script.ToString(), true);
         * */
        _UpdatePanel.Update();
    }
    WebReferenceReports.FullUrlExposureReportRequest RequestV
    {
        get { return (ViewState["Request"] == null) ? null : (WebReferenceReports.FullUrlExposureReportRequest)ViewState["Request"]; }
        set { ViewState["Request"] = value; }
    }
    int TotalRowsV
    {
        get { return (ViewState["TotalRows"] == null) ? 0 : (int)ViewState["TotalRows"]; }
        set { ViewState["TotalRows"] = value; }
    }
}