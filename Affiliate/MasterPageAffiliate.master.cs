﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;

public partial class Affiliate_MasterPageAffiliate : System.Web.UI.MasterPage
{
    PageSetting ps;
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (Page.GetType().IsSubclassOf(typeof(PageSetting)))
            ps = (PageSetting)Page;

        if (Session.IsNewSession || ps == null || ps.siteSetting == null ||
            string.IsNullOrEmpty(ps.siteSetting.GetSiteID) || !ps.userManagement.IsAffiliate())
        {
            Response.Redirect(ResolveUrl("~") + "Affiliate/LogOut.aspx");
        }        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(ps.siteSetting.LogoPath))
                setLogo(ps.siteSetting.LogoPath);

            GetTranslate(ps.siteSetting.siteLangId);

            lblUserName.Text = " " + ps.userManagement.User_Name + " ";
            SetMenu();

        }
        
        Page.Header.DataBind();
        if (sm == null || !sm.IsInAsyncPostBack)
            ps.SetNoIframe();
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LoadDetails", "LoadDetails('" + GetLogOutMessage + "', '" + GetLogOutPath + "');", true);
    }
    protected void UnrecognizedClient()
    {
        if (IsPostBack)
        {
            string script = @"<script type=""text/javascript"">alert('" +
                HttpUtility.JavaScriptStringEncode(lbl_logOutMes.Text) + "'); window.location='logout.aspx';</script>";
            Response.Write(script);
            Response.Flush();
            Response.End();
        }
        else
            Response.Redirect("LogOut.aspx");

    }
    public void setLogo(string name)
    {
        string LogoServerPath = ConfigurationManager.AppSettings["PublisherLogo"] + @"aspjpeg\" + name;
        if (File.Exists(LogoServerPath))
            ImageLogo.ImageUrl = ConfigurationManager.AppSettings["PublisherLogoWeb"] + @"aspjpeg/" + name;
    }
    void GetTranslate(int siteLangId)
    {
        DBConnection.LoadTranslateToControl(this, "MasterPageAffiliate.master", siteLangId);
    }
    private void SetMenu()
    {
        string pageName =
            System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
        if (pageName == Path.GetFileName(a_RequestReport.HRef) ||
            pageName == a_RootDomain.HRef ||
            pageName == a_FullUrl.HRef ||
            pageName == a_Conversion.HRef)
        {
            a_Reports.Attributes.Add("class", "btn-ads active");
            btn_reports.Attributes.Add("class", "_btn_ads active");
            a_ReportCenter.Attributes.Add("class", "active");
            if (pageName == Path.GetFileName(a_RequestReport.HRef))
                a_RequestReport.Attributes.Add("class", "active");
            else if(pageName == a_Conversion.HRef)
                a_Conversion.Attributes.Add("class", "active");
            else if(pageName == a_RootDomain.HRef)
                a_RootDomain.Attributes.Add("class", "active");
            else if(pageName == a_FullUrl.HRef)
                a_FullUrl.Attributes.Add("class", "active");
        }
    }
    
    protected void lbLogout_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetLogOutPath);
    }
    public string GetLogOutMessage
    {
        get
        {
            return HttpUtility.HtmlEncode(lbl_logOutMes.Text);
        }
    }
    public string GetLogOutPath
    {
        get
        {
            return ResolveUrl("~") + "Affiliate/LogOut.aspx";
        }
    }
    protected string GetUpdateFaild
    {
        get { return HttpUtility.HtmlEncode(lbl_UpdateFailed.Text); }
    }
    public string GetNoResultAlert
    {
        get { return HttpUtility.JavaScriptStringEncode(lbl__noResults.Text); }
    }
}
