﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="payment.aspx.cs" Inherits="deskApp_payment" MasterPageFile="~/deskApp/MasterPageDeskUp.master"  ClientIDMode="Static"%>
<asp:Content  runat="server" ContentPlaceHolderID="head">
    <link rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/shieldui-all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/light/all.min.css" />
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <link href="/Ladda-master/dist/ladda.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Ladda-master/dist/spin.min.js"></script>
	<script type="text/javascript" src="/Ladda-master/dist/ladda.min.js"></script>
   <script>

       /*

       The pusher event is sent to a channel called [ContactId] (guid).

        The method called in the javascript is "customerPaymentResult(data)”

        data is:

        {
	        “Success”:true (or false)
        }


       */

       var ContactId;

       if (typeof noProblemDesktopBinder != "undefined") {
           ContactId = noProblemDesktopBinder.getContactId();
           if (!ContactId || ContactId.length == 0)
               window.location.href = 'UserRegister.aspx';
           noProblemDesktopBinder.setSize(452, 635);
       }


       function validation()
       {
           return true;

           var blnValid = true;

           if ($.trim($('#firstName').val()) == "")
           {
               $('#firstName').css('border-color', 'red');
               blnValid = false;
           }

           else
           {
               $('#firstName').css('border-color', '#ccc');
           }
           
           if ($.trim($('#lastName').val()) == "") {
               $('#lastName').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#lastName').css('border-color', '#ccc');
           }

           if ($.trim($('#email').val()) == "") {
               $('#email').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#email').css('border-color', '#ccc');
           }

           if ($.trim($('#email').val()) == "") {
               $('#email').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#email').css('border-color', '#ccc');
           }

           if ($.trim($('#address').val()) == "") {
               $('#address').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#address').css('border-color', '#ccc');
           }

           if ($.trim($('#city').val()) == "") {
               $('#city').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#city').css('border-color', '#ccc');
           }

           if ($.trim($('#zipCode').val()) == "") {
               $('#zipCode').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#zipCode').css('border-color', '#ccc');
           }

           if ($.trim($('#zipCode').val()) == "") {
               $('#zipCode').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#zipCode').css('border-color', '#ccc');
           }


           if ($.trim($('#phone').val()) == "") {
               $('#phone').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#phone').css('border-color', '#ccc');
           }

           if ($.trim($('#creditCard').val()) == "") {
               $('#creditCard').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#creditCard').css('border-color', '#ccc');
           }

           if ($.trim($('#cvv').val()) == "") {
               $('#cvv').css('border-color', 'red');
               blnValid = false;
           }

           else {
               $('#cvv').css('border-color', '#ccc');
           }
           

           var lengthCreditCard;
           lengthCreditCard = $('#creditCard').val().length;

           var last4 = $('#creditCard').val().substring(lengthCreditCard - 4, lengthCreditCard);
           $('#hdn_last4').val(last4);


           //alert($('#hdn_last4').val());
           return false;
       }


       /*
        var ContactId;
        

        if (typeof noProblemDesktopBinder != "undefined") {           
            ContactId = noProblemDesktopBinder.getContactId();
            if (!ContactId || ContactId.length == 0)
                window.location.href = 'UserRegister.aspx';
            noProblemDesktopBinder.setSize(446, 680);
        }


       */
       function ifExplorer() {
           var boolExplorer = false;
           if (navigator.userAgent.search("MSIE") >= 0 || navigator.userAgent.search("Trident/") >= 0) {
               boolExplorer = true;
           }

           return boolExplorer;
       }

       function stopActiveButtonAnim() {

           if (!ifExplorer()) {
               if (l) {
                   clearInterval(interval);
                   l.stop();
               }
           }
       }


       function activeButtonAnim(obj, intervalProgressTime) {
           if (!ifExplorer()) {
               //  var l = Ladda.create(document.querySelector());
               l = Ladda.create(document.querySelector(obj));
               // Start loading
               l.start();

               var progress = 0;
               interval = setInterval(function () {
                   progress = Math.min(progress + Math.random() * 0.1, 1);
                   l.setProgress(progress);

                   if (progress === 1) {
                       progress = 0;
                       l.start();
                       //clearInterval(interval);
                   }
               }, intervalProgressTime);
           }
       }

       function preCheckRegistrationStep(str)
       {
           activeButtonAnim('.progress-demo #lbSend', 200);
       }

       function setUniqueSelectetBox(container, linkSpanSelected, linkSelected, arrow) {

           $("." + container + " .selectBox-dropdown-menu li").each(function (evt) {

               if ($(this).find('A').text() == $(this).parent().siblings('A').find("#" + linkSpanSelected).text())
                   $(this).addClass('selectBox-selected');

           });

           $("." + container + " .selectBox-dropdown").click(function (evt) {
               if ($(evt.target).hasClass("selectBox-arrow")) {

               }

               else {
                   $("." + container + " .selectBox-dropdown-menu").toggle();
                   $("." + container + " .selectBox-dropdown").toggleClass('selectBox-dropdown-clicked');
               }
           }
       );

           $("." + container + " .selectBox-arrow").click(function (evt) {
               //alert("asdas " + container);
               $("." + container + " .selectBox-dropdown-menu").toggle();
               $("." + container + " .selectBox-dropdown").toggleClass('selectBox-dropdown-clicked');

           }
       );


           $("." + container + " .selectBox-dropdown").mouseover(function () {
               //if ($(".selectBox-dropdown-menu").is(":hidden"))
               //.selectBox-menuShowing
               $("." + container + " .selectBox-dropdown").addClass('selectBox-menuShowing');
               if ($("." + container + " .selectBox-dropdown-menu").is(":visible"))
                   $("." + container + " .selectBox-dropdown").addClass('selectBox-dropdown-clicked');
           });

           $("." + container + " .selectBox-dropdown").mouseout(function () {
               if ($("." + container + " .selectBox-dropdown-menu").is(":hidden"))
                   $("." + container + " .selectBox-dropdown").removeClass('selectBox-menuShowing');

               //$(".selectBox-dropdown").removeClass('selectBox-dropdown-clicked');


           });

           $("." + container + " .selectBox-dropdown").blur(function () {
               if ($("." + container + " .selectBox-dropdown-menu").is(":visible")) {

                   //$(".selectBox-dropdown-menu").hide();
                   //$(".selectBox-dropdown").removeClass('selectBox-dropdown-clicked');
               }
               //$(".selectBox-dropdown").toggleClass('selectBox-dropdown-clicked'); 
           });

           $("." + container + " .selectBox-dropdown-menu").click(function (evt) {

               $(".selectBox-dropdown-menu").hide();

               //$(".selectBox-dropdown").addClass('selectBox-menuShowing');

           });

           $("." + container + " .selectBox-dropdown-menu li a").click(function (evt) {
               //alert(evt.target.id);
               $("#" + linkSpanSelected).text($(this).text()).css('color', 'black');
               $("#" + linkSelected).css('borderColor', '#CCCCCC');

               $(this).parents('li').siblings().removeClass('selectBox-selected');
               $(this).parents('li').addClass('selectBox-selected');
               ////cleanErrors();
               //$(evt.target.id).css('color','blue');


           });

           $("." + container + " .selectBox-dropdown-menu").mouseout(function () {
               //$(".selectBox-dropdown-menu").hide();


           });


           $("." + container + " .selectBox-options").mouseleave(function (evt) {
               $("." + container + " .selectBox-dropdown-menu").hide();
               $("." + container + " .selectBox-dropdown").removeClass('selectBox-menuShowing');
               $("." + container + " .selectBox-dropdown").removeClass('selectBox-dropdown-clicked');
               //mouseout fires when the pointer moves out of the child element as well, while mouseleave fires only when the pointer moves out of the bound element.
           });


           /*
           $(".selectBox-dropdown").mouseleave(function (evt) {
           $(".selectBox-dropdown-menu").hide();
           $(".selectBox-dropdown").removeClass('selectBox-menuShowing');
           //mouseout fires when the pointer moves out of the child element as well, while mouseleave fires only when the pointer moves out of the bound element.
           }
           );
           */

           $("body").bind('click', function (ev) {

               var myID = ev.target.id;
               //alert(myID);
               if (myID.indexOf(linkSpanSelected) == -1 && myID.indexOf(arrow) == -1) {
                   $("." + container + " .selectBox-dropdown-menu").hide();
                   $("." + container + " .selectBox-dropdown").removeClass('selectBox-menuShowing');
                   $("." + container + " .selectBox-dropdown").removeClass('selectBox-dropdown-clicked');
               }
               ///alert(myID);
               // timeSelected

           });

       }

      


       $(document).ready(function()
       {           

           if (typeof noProblemDesktopBinder != "undefined") {                          
               $('#hdn_contactId').val(ContactId);               
               
           }


           setUniqueSelectetBox("paymentStates", "paymentStatesSelected", "linkPaymentStatesSelected", "paymentStatesArrow");
           setUniqueSelectetBox("paymentCardType", "paymentCardTypeSelected", "linkPaymentCardTypeSelected", "paymentCardTypeArrow");
           setUniqueSelectetBox("paymentMonth", "paymentMonthSelected", "linkPaymentMonthSelected", "paymentMonthArrow");
           setUniqueSelectetBox("paymentYear", "paymentYearSelected", "linkPaymentYearSelected", "paymentYearArrow");
          
       }
       )
   </script>

</asp:Content>

<asp:Content  runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <div class="payment">


        <div class="mainTitle">
            <span runat="server" id="spn_PlanName"></span>
        </div>

        <div class="content">
            <div class="content1">Your package: <span runat="server" id="spn_PlanMins"></span> mins for $<span runat="server" id="spn_PlanPrice"></span></div>
            <div class="content2">We just need your credit and details</div>
        </div>

        <div class="inputs">
            <div class="inputsRow inputsRow1">
                
                <div class="inputsRowLeft">
                    <input type="text" name="firstName" id="firstName"  runat="server"  value="First Name"/>   
                </div>
                
                <div class="inputsRowRight">
                    <input type="text" name="lastName" id="lastName" runat="server"  value="Last Name"/>
                </div>             
                

            </div>

            <div class="clear"></div>
           
            <div class="inputsRow inputsRow2">  
                  <div class="inputsRowLeft">
                     <input type="text" name="address" id="address" runat="server" value="Address"/>
                 </div>

                 <div class="inputsRowRight">
                    <input type="text" name="city" id="city" runat="server"  value="City"/>
                </div>             

            </div>

          <div class="clear"></div>

          <div class="inputsRow inputsRow3"> 
              <div class="inputsRowLeft">
                     <input type="text" name="zipCode" id="zipCode" runat="server" value="Zip Code"/>
              </div>

              <div class="inputsRowRight">           
                    <div class="paymentStates" id="paymentStates">                    
                        <a tabindex="0" id="linkPaymentStatesSelected" title="" style="display: inline-block; -moz-user-select: none;" class="selectBox selectBox-dropdown">
                        <span class="selectBox-label" id="paymentStatesSelected" runat="server">State</span>
                        <span class="selectBox-arrow" id="paymentStatesArrow"></span>
                        </a>         	
	 

                        <ul runat="server" style="-moz-user-select: none; width: 190px;display:none;" class="selectBox-dropdown-menu selectBox-options" id="ulPaymentStates">                 
                   
                            <!--
                            <li class="">                        
                                <a href="javascript:void(0);" >Yes</a>
                            </li>
                            -->
                                       

                    
                       </ul>                  
                               
                    </div>  
              </div>
              

          </div>

        <div class="clear"></div>

        <div class="inputsRow inputsRow4">
            <div class="inputsRowLeft">
                     <input type="text" name="phone" id="phone"  runat="server" value="Phone"/>
              </div> 
           
            <div class="inputsRowRight">
                <input type="text" name="email" id="email" runat="server" value="Email"/>

                <div class="inputComment" id="div_EmailComment">Please enter a valid email address                      
                  <div class="inputCommentArrow"></div>
                </div>

            </div>
        </div>

        <div class="inputsRow inputsRow5">
            <div class="inputsRowLeft">
                                <div class="paymentCardType" id="paymentCardType">                    
                        <a tabindex="0" id="linkPaymentCardTypeSelected" title="" style="display: inline-block; -moz-user-select: none;" class="selectBox selectBox-dropdown">
                        <span class="selectBox-label" id="paymentCardTypeSelected" runat="server">Card Type</span>
                        <span class="selectBox-arrow" id="paymentCardTypeArrow"></span>
                        </a>         	
	 

                        <ul runat="server" style="-moz-user-select: none; width: 190px;display:none;" class="selectBox-dropdown-menu selectBox-options" id="ulPaymentCardType">                 

                            <li class="">                        
                                <a href="javascript:void(0);" >VISA</a>
                            </li>

                            <li class="">                        
                                <a href="javascript:void(0);" >MASTERCARD</a>
                            </li>

                             <li class="">                        
                                <a href="javascript:void(0);" >AMEX</a>
                            </li>
                             
                            <li class="">                        
                                <a href="javascript:void(0);" >CARTE_BLEUE</a>
                            </li>
             
                            <li class="">                        
                                <a href="javascript:void(0);" >DINERS</a>
                            </li>

                            <li class="">                        
                                <a href="javascript:void(0);" >DISCOVER</a>
                            </li>
             
                             <li class="">                        
                                <a href="javascript:void(0);" >JCB</a>
                            </li>

                             <li class="">                        
                                <a href="javascript:void(0);" >MAESTR_UK</a>
                            </li>
            
                             <li class="">                        
                                <a href="javascript:void(0);" >SOLO</a>
                            </li>                    

                    
                       </ul>                  
                               
                    </div>

            </div>

            <div class="inputsRowRight">
                
                
                <input type="text" name="creditCard" id="creditCard" data-bluesnap="encryptedCreditCard"  value="Credit Card"/>
 
                
              </div> 
            
            
        </div>

        <div class="clear"></div>
            
        <div class="inputsRow inputsRow6">
            <div class="inputsRowLeft">
                
                  <div class="paymentMonth" id="paymentMonth">                    
                        <a tabindex="0" id="linkPaymentMonthSelected" title="" style="display: inline-block; -moz-user-select: none;" class="selectBox selectBox-dropdown">
                        <span class="selectBox-label" id="paymentMonthSelected" runat="server">mm</span>
                        <span class="selectBox-arrow" id="paymentMonthArrow"></span>
                        </a>         	
	 

                        <ul runat="server" style="-moz-user-select: none; width: 112px;display:none;" class="selectBox-dropdown-menu selectBox-options" id="ulPaymentMonth">                 

                            <li class="">                        
                                <a href="javascript:void(0);" >1</a>
                            </li>

                            <li class="">                        
                                <a href="javascript:void(0);" >2</a>
                            </li>

                             <li class="">                        
                                <a href="javascript:void(0);" >3</a>
                            </li>
                             
                            <li class="">                        
                                <a href="javascript:void(0);" >4</a>
                            </li>
             
                            <li class="">                        
                                <a href="javascript:void(0);" >5</a>
                            </li>

                            <li class="">                        
                                <a href="javascript:void(0);" >6</a>
                            </li>
             
                             <li class="">                        
                                <a href="javascript:void(0);" >7</a>
                            </li>

                             <li class="">                        
                                <a href="javascript:void(0);" >8</a>
                            </li>
            
                             <li class="">                        
                                <a href="javascript:void(0);" >9</a>
                            </li>                    

                             <li class="">                        
                                <a href="javascript:void(0);" >10</a>
                            </li>   

                             <li class="">                        
                                <a href="javascript:void(0);" >11</a>
                            </li>   

                             <li class="">                        
                                <a href="javascript:void(0);" >12</a>
                            </li>   

                       </ul>                  
                               
                    </div> 

                  <div class="paymentYear" id="paymentYear">                    
                        <a tabindex="0" id="linkPaymentYearSelected" title="" style="display: inline-block; -moz-user-select: none;" class="selectBox selectBox-dropdown">
                        <span class="selectBox-label" id="paymentYearSelected" runat="server">yy</span>
                        <span class="selectBox-arrow" id="paymentYearArrow"></span>
                        </a>         	
	 

                        <ul runat="server" style="-moz-user-select: none; width: 112px;display:none;" class="selectBox-dropdown-menu selectBox-options" id="ulPaymentYear">                 

                            <li class="">                        
                                <a href="javascript:void(0);" >1015</a>
                            </li>

                            <li class="">                        
                                <a href="javascript:void(0);" >1016</a>
                            </li>

                             <li class="">                        
                                <a href="javascript:void(0);" >1017</a>
                            </li>
                             
                            <li class="">                        
                                <a href="javascript:void(0);" >1018</a>
                            </li>
             
                            <li class="">                        
                                <a href="javascript:void(0);" >1019</a>
                            </li>

                            <li class="">                        
                                <a href="javascript:void(0);" >1020</a>
                            </li>
             
                             <li class="">                        
                                <a href="javascript:void(0);" >1021</a>
                            </li>

                             <li class="">                        
                                <a href="javascript:void(0);" >1022</a>
                            </li>
            
                             <li class="">                        
                                <a href="javascript:void(0);" >1023</a>
                            </li>                    

                             <li class="">                        
                                <a href="javascript:void(0);" >1024</a>
                            </li> 
                    
                       </ul>                  
                               
                    </div> 

                  <input type="text" name="cvv" id="cvv" data-bluesnap="encryptedCvv" value="Security Code" />
            </div>
            
        </div> 
        
        <div class="clear"></div> 

        
         <!--
        

            <div class="inputsRow">
                <label for="cvv" class="inputsRowLeft">
                    Security code
                    <br />                    
                    <span class="cvvDesc">3 numbers in the back of your card</span>
                </label>
                <input type="text" name="cvv" id="cvv" data-bluesnap="encryptedCvv"  />
            </div>

           <div>
               <input type="hidden" runat="server" id="hdn_last4"/>                            
               <input type="hidden" runat="server" id="hdn_contactId"/>
           </div>
            -->

           <div class="inputsRow">
               <div class="inputsRowLeft">&nbsp;</div>
               <input type="submit"  value="AUTHORIZE" runat="server"  onclick="return validation();" onserverclick="btnSubmit_Click" class="btn" />

               
             <section class="progress-demo">                   
                  
                  <asp:LinkButton ID="lbSend" runat="server" data-style="expand-right" data-color="black" class="ladda-button" OnClientClick="javascript:return preCheckRegistrationStep('regularRegister');" OnClick="btnSubmit_Click" PostBackUrl="" >
                    <span class="ladda-label">AUTHORIZE</span><span class="ladda-spinner">
                    </span><div class="ladda-progress" style="width: 138px;"></div>
                  </asp:LinkButton>
                 
                 
                 <asp:HyperLink runat="server"  ID="lbSend2"   NavigateUrl="payment.aspx" Target="">
                      <span class="ladda-label">AUTHORIZE</span><span class="ladda-spinner">
                    </span><div class="ladda-progress" style="width: 138px;"></div>
                 </asp:HyperLink> 
             </section> 
            

           </div>
           
            <div class="inputsRow">
                <div class="inputsRowLeft">&nbsp;</div>
                <div>Confirm to use my details</div>
            </div>

       
    </div>
    </div>
     <script src="https://gateway.bluesnap.com/js/cse/v1.0.1/bluesnap.js"></script>

    <script>
        BlueSnap.publicKey = "<%#blueSnapPublicId%>";
        BlueSnap.setTargetFormId("<%#formId%>");
    </script>

</asp:Content>