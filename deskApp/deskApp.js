﻿function checkEmail(strEmail) {
    var ifValidEmail = true;
    //var regFormatEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    var regFormatEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (strEmail.search(regFormatEmail) == -1) //if match failed
    {
        ifValidEmail = false;
    }

    return ifValidEmail;
}



