﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchProvider.aspx.cs" Inherits="deskApp_SearchProvider" MasterPageFile="~/deskApp/MasterPageDeskUp.master"%>

<asp:Content  runat="server" ContentPlaceHolderID="head">
     <link type="text/css" href="StyleSheet.css" rel="stylesheet" />
    <link href="/Formal/jquery-ui-1.8.16.custom.css" type="text/css" rel="Stylesheet" />   
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../../../scripts/jquery-ui-1.8.17.custom.autocomplete.min.js"></script>
    <script type="text/javascript" src="../../../scripts/jquery-ui-1.8.18.effectslide.custom.min.js"></script>
    <script type="text/javascript" src="../../../PlaceHolder.js"></script>

    <script type="text/javascript">
        var _noproblem = _noproblem || {};
        _noproblem.IsSelected = false;
        $(function () {
            $("#<%# txt_Provider.ClientID  %>").autocomplete({

                source: function (request, response) {
                    var _txt = $("#<%# txt_Provider.ClientID  %>").val();
                    SourceText = request.term;
                    $.ajax({
                        url: "<%# GetHeadingServiceList %>",
                        data: "{ 'TagPrefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {

                            response($.map(data.d, function (item) {
                                return {
                                    value: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            //              alert(textStatus);
                        },
                        complete: function (jqXHR, textStatus) {
                            //  HasInHeadingRequest = false;
                        }

                    });
                },
                minLength: 1,
                close: function (event, ui) {
                    if (!_noproblem.IsSelected && event.target.value.length != 0) {
                        //             SlideElement("div_ServiceComment", true); // document.getElementById("div_ServiceComment").style.display = "block";
                        //               $('#div_ServiceError').show();
                    }
                    _noproblem.IsSelected = false;
                },
                /*
                open: function (event, ui) {
                SlideElement("div_ServiceComment", false); // document.getElementById("div_ServiceComment").style.display = "none";
                $('#div_ServiceError').hide();
                },
                */
                select: function (event, ui) {
                    //                SlideElement("div_ServiceComment", false); // document.getElementById("div_ServiceComment").style.display = "none";
                    //                 $("#div_ServiceSmile").show();
                    //                 $('#div_ServiceError').hide();
                    _noproblem.IsSelected = true;
                }
            });
            $('#a_GetStart').click(function () {
                var _name = document.getElementById('<%# txt_Provider.ClientID %>').value;
                if (_name.length == 0) {
                    ShowProviderError();
                    return;
                }
                GetSlider(_name);
            });
            $("#<%# txt_Provider.ClientID  %>").focus(function () {
                HideProviderError();
            });
            $("#<%# txt_Provider.ClientID  %>").keyup(function(e){
                var code;
                if (!e) var e = window.event;
                if (e.keyCode) code = e.keyCode;
                else if (e.which) code = e.which;
                if(code != 13){
              //      HideProviderError();
                    return;
                }
                var _name = e.target.value;
                if (_name.length == 0)                     
                    return;
                
                GetSlider(_name);
            });
            
            
        });
        function GetSlider(_name){
           
            ShowLoader();
            $.ajax({
                url: "<%# GetIsHeadingByName %>",
                data: "{ 'name': '" + _name + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                //         dataFilter: function (data) { return data; },
                success: function (data) {
                    var _heading = data.d;
                    if (_heading.length == 0) {
                        ShowProviderError();
                        HideLoader();
                        return;
                    }

                    if(typeof noProblemDesktopBinder!="undefined")
                         window.location.href = '<%# SliderFrame %>' + "OriginId=" + noProblemDesktopBinder.getOriginId() + "&ExpertiseCode=" + _heading;
                    else
                        window.location.href = '<%# SliderFrame %>' + "OriginId=&ExpertiseCode=" + _heading;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //              alert(textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;
                }

            });
        }
        function ShowProviderError() {
            $('#div_ProviderError').show();
            $('#div_ProviderComment').show();
        }
        function HideProviderError() {
            $('#div_ProviderError').hide();
            $('#div_ProviderComment').hide();
        }
        function ShowLoader() {
            $('#a_GetStart').hide();
            $('#a_GetStart_loader').show();
        }
        function HideLoader() {
            $('#a_GetStart').show();
            $('#a_GetStart_loader').hide();
        }

        if(typeof noProblemDesktopBinder!="undefined" )
            noProblemDesktopBinder.setSize(<%# WinWidth %>, <%# WinHeight %>);
    </script>
</asp:Content>
<asp:Content  runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <div>
        <div>Hire the right professional to move your project forward.</div>
        <div>
            <span>What kind of pro do you need?</span>
            <asp:TextBox ID="txt_Provider" runat="server" place_holder="Find a Plumber, Lawyer, Mover…"
                 holder-position="none" HolderClass="place-holder"></asp:TextBox>             
            <div class="inputError" style="display:none;"  id="div_ProviderError">
               
            </div>
            <div class="inputComment" style="display:none;" id="div_ProviderComment">
                    <%= NoMatchCategory %>
                <div class="chat-bubble-arrow-border"></div>
                <div class="chat-bubble-arrow"></div>
            </div>
        </div>

        <div>
            <a href="javascript:void(0);" id="a_GetStart">GET STARTED</a>
            <a href="javascript:void(0);" id="a_GetStart_loader" style="display:none;">loading...</a>
        </div>
    
    </div>
</asp:Content>
