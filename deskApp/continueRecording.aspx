﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="continueRecording.aspx.cs" Inherits="deskApp_continueRecording" MasterPageFile="~/deskApp/MasterPageDeskUp.master" %>
<asp:Content  runat="server" ContentPlaceHolderID="head">
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="continueRecording">
        <div class="mainTitle">
            Call recording enabled
        </div>

        <div>
            You’re good to go!
            <br />
            Keep talking. We’ll make sure every word gets recorded.
        </div>

        <div>
            image
        </div>
    </div>
</asp:Content>
