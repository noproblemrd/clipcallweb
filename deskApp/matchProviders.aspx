﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="matchProviders.aspx.cs" Inherits="deskApp_matchProviders"  MasterPageFile="~/deskApp/MasterPageDeskUp.master" ClientIDMode="Static" %>

<asp:Content  runat="server" ContentPlaceHolderID="head">   

    <style>
        
    </style>
    <script type="text/javascript"  src="http://maps.googleapis.com/maps/api/js?sensor=false&language=en&libraries=places"></script>
    <script src="deskApp.js"></script>
    <link rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/shieldui-all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/light/all.min.css" />
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="../../../PlaceHolder.js"></script>
    
    <script type="text/javascript">
        
        var map;          

        function setLatLot(address, index) {
            //alert(address);
            var geocoder = new google.maps.Geocoder();

            var infowindow = new google.maps.InfoWindow();

            var marker;

            geocoder.geocode({ 'address': address }, function (results, status) {
                //alert(status + ' ' + address);
                if (window.console)
                    console.log(status + ' ' + address);
                if (status == google.maps.GeocoderStatus.OK) {
                    var latLng = results[0].geometry.location;
                    //alert(latLng);

                    //var newLat = -25.363882 + (Math.random() - .5) / 1500; // * (Math.random() * (max - min) + min);
                    //var newLng = 131.044922 + (Math.random() - .5) / 1500; // * (Math.random() * (max - min) + min);
                    myLatlng = new google.maps.LatLng(latLng.lat(), latLng.lng());

                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(latLng.lat(), latLng.lng()),
                        map: map,
                        icon: "<%#root%>Consumer/images/mapPins/mapPin" + (index + 1) + ".png"
                        //icon: "<%#root%>npsb/flavours/images/animEyes.gif"
                    });

                    //alert(arraySupplierDetails[index].imgSrc);

                    /*
                    var imgStr;
                    if (arraySupplierDetails[index].imgSrc == '' || !arraySupplierDetails[index].imgSrc)
                        imgStr = "";
                    else
                        imgStr = '<img src="' + arraySupplierDetails[index].imgSrc + '" width="34" height="34">';

                    google.maps.event.addListener(marker, 'click', (function (marker, index) {
                        return function () {
                            infowindow.setContent('<div class="containerGoogleSupplier">' +
                                '<div class="GoogleSupplierImg">' + imgStr + '</div>' +
                                '<div class="GoogleSupplierContent">' +
                            //'<div class="GoogleSupplierName">' + arraySupplierDetails[index].name + '</div>' +
                                    '<div class="GoogleSupplierName"><a href="' + arraySupplierDetails[index].link + '" target="_blank">' + arraySupplierDetails[index].name + '</a></div>' +
                                    '<div class="GoogleSupplierPhone"><a href="' + arraySupplierDetails[index].link + '" target="_blank">' + arraySupplierDetails[index].phone + '</a></div>' +
                                '<div>' +

                            '</div>');

                            infowindow.open(map, marker);
                        }
                    })(marker, index));
                    */

                } else {
                    //alert("Geocode failed. Reason: " + status);                       
                }
            });

        }

        function setLatLot2(address) {
            //alert(address);
            var geocoder = new google.maps.Geocoder();

            var infowindow = new google.maps.InfoWindow();

            var marker;

            geocoder.geocode({ 'address': address }, function (results, status) {
                //alert(status + ' ' + address);
                if (window.console)
                    console.log(status + ' ' + address);
                if (status == google.maps.GeocoderStatus.OK) {
                    var latLng = results[0].geometry.location;
                    //alert(latLng);

                    //var newLat = -25.363882 + (Math.random() - .5) / 1500; // * (Math.random() * (max - min) + min);
                    //var newLng = 131.044922 + (Math.random() - .5) / 1500; // * (Math.random() * (max - min) + min);
                    myLatlng = new google.maps.LatLng(latLng.lat(), latLng.lng());

                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(latLng.lat(), latLng.lng()),
                        map: map,
                        icon: "<%#root%>deskapp/images/blue-smiley.png"
                    });

                 

                } else {
                    //alert("Geocode failed. Reason: " + status);                       
                }
            });

        }

        function LatLotSetTimeout(index) {
            //setLatLot(arraySupplierDetails[index].address, index);
            setTimeout(function () { setLatLot(arraySupplierDetails[index].address, index); }, (index + 1) * 200);
        }

        function initializeMap() {

            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({ 'address': '<%#googleAdrress%>' }, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    var latLng = results[0].geometry.location;
                    myLatlng = new google.maps.LatLng(latLng.lat(), latLng.lng());

                    map = new google.maps.Map(document.getElementById('map_canvas'), {
                        zoom: <%#zoom%>,
                        center: new google.maps.LatLng(latLng.lat(), latLng.lng()),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: false,
                        mapTypeControl: false,
                        streetViewControl: false

                    });

                } else {
                    //alert("Geocode failed. Reason: " + status);                        
                }
            });

           
        }       

        function findSupplierTipDetails(supplierId,tipId)
        {
            var supplierTipDetails;

            for(var i=0;i<arraySupplierDetails.length;i++)
            {
                if(arraySupplierDetails[i].supplierId==supplierId)
                {
                    for(var x=0;i<arraySupplierDetails[i][x].length;x++)
                    {
                        if(arraySupplierDetails[i][x].TipId==tipId)
                            supplierTipDetails=arraySupplierDetails[i][x];
                    }
                }
            }

            return supplierTipDetails;
        }

        
        function findSupplierTipDetails2(tipId)
        {
            var tipDetails;

            for(var i=0;i<currentSupplier.arrTips.length;i++)
            {
                if(currentSupplier.arrTips[i].TipId==tipId)
                {
                    tipDetails=currentSupplier.arrTips[i];
                    break;
                }
                    
            }

            return tipDetails;
        }

        function SupplierDetails(supplierId,name,address,payWith,index,recordingCall,yelpRating,yelpReview,yelpLink,arrTips, imgSrc, phone, link) {
            this.supplierId=supplierId;
            this.address = address;
            this.name = name;
            this.payWith=payWith;
            this.index=index;
            //alert("init: " + recordingCall);
            this.recordingCall=recordingCall;
            //this.recordingCall=true; // from server. decision of the supplier
            this.recordingCallClient=true; // at runtime decision of client. default is true
            this.yelpRating=yelpRating;
            //this.yelpRating="http://s3-media3.fl.yelpcdn.com/assets/2/www/img/34bc8086841c/ico/stars/v1/stars_3.png";
            this.yelpReview=yelpReview;
            //this.yelpReview="Okay so I'm definitely not from the area so finding a moving company was really hard at first for all of my stuff. I had a lot of valuables and previously";
            this.yelpLink=yelpLink;
            this.arrTips=arrTips; // example [Object { TipId="edf256d8-20d6-e411-befd-001517d10f6e", Percent=0, Show=false}, Object { TipId="35f7e9e9-20d6-e411-befd-001517d10f6e", Percent=0, Show=false}]
            this.imgSrc = imgSrc;
            this.phone = phone;
            this.link = link;
            
        }

        var arraySupplierDetails = new Array();

        function loadSuppliersMap() {
            /*
            $(".suppliersDetails").each(function () {
                var supplierRegion = $(this).find('.region').text();
                var supplierName = $(this).find('.heading A').text();
                var supplierImg = $(this).find('.supplierImage IMG').attr('src');
                //alert(supplierImg);
                var supplierPhone = $(this).find('#linkPhone').text();
                var supplierLink = $(this).find('.heading A').attr('href');
                //alert(supplierLink);
                //alert($(this).find('.region').text());
                //alert($(this).find('.heading A').text());
                //alert(supplierImg);
                var supplierDetails = new SupplierDetails(supplierRegion, supplierName, supplierImg, supplierPhone, supplierLink);
                arraySupplierDetails.push(supplierDetails);

                
            })
            */

            /*
            var supplierDetails = new SupplierDetails("albany", "cookie", "http://10.0.0.30:80/gallery/Images/logos/9968aec1-35fd-43e9-a325-530c3001e03f/melech hamekarerim baam_logo.jpg", "0545350800", "/business/melech hamekarerim baam/9968aec1-35fd-43e9-a325-530c3001e03f");
            arraySupplierDetails.push(supplierDetails);

            var supplierDetails2 = new SupplierDetails("38801", "cookie2", "http://10.0.0.30:80/gallery/Images/logos/9968aec1-35fd-43e9-a325-530c3001e03f/melech hamekarerim baam_logo.jpg", "0545350800", "/business/melech hamekarerim baam/9968aec1-35fd-43e9-a325-530c3001e03f");
            arraySupplierDetails.push(supplierDetails2);            
            */


            for (var indexDetails = 0; indexDetails < arraySupplierDetails.length; indexDetails++) {
                //for (var indexDetails = 0; indexDetails < 3; indexDetails++) {
                LatLotSetTimeout(indexDetails);
            }
          

        }

        


        //window.onload = initializeMap;
    </script>

    <script type="text/javascript">    
                       
        var maxSeconds = 10; 
        var remaintMinutes = maxSeconds; 
        var timer;

        function updateProgress() {                                
            var remaining = maxSeconds - (Date.now() - startTime) / 1000; // seconds                                
            progress.value(remaining);
            if (remaining <= 0) {
                clearInterval(timer);
                nextCustomer();
            }
        }
        
        function initTimer()
        {
            clearInterval(timer);
            startTime = Date.now();
            //timer = setInterval(updateProgress, 100);
            timer = setInterval(updateProgress, 100);
        }

        jQuery(function ($) {
               timer = null,
                startTime = null,
                progress = $("#progress").shieldProgressBar({
                    min: 0,
                    max: maxSeconds,
                    value: remaintMinutes,
                    layout: "circular",
                    layoutOptions: {
                        circular: {
                            width: 7,
                            borderWidth: 2,
                            borderColor: "#cccccc",
                            color: "#fff",
                            colorDisabled: "#C4C4C4",
                            backgroundColor: "#1E98E4"
                        }
                    },
                    text: {
                        enabled: true,
                        template: '{0:n0}'
                    },
                    reversed: true
                }).swidget();
        
           

            //initTimer();             

                           
        });

    </script>

    <script>
        

        

        function MyMethod(obj)
        {
            //alert(obj.id);
            //alert(obj.advs[0].name);
            
        }

        function suppliersMatches(matches)
        {
            $('.numberMatches').text(matches);
        }

        function supplierDetails(obj) // include ifRecord
        {

        }      

        function tips(obj)
        {

        }

        function matchingAvailable()
        {
            
            ////getCaseDataForDesktopApp();
            $('.containerMapDiv').css('display','block'); 
            initializeMap();
            $('.matchProviders').show();
            $('.foundServiceProvider').hide();
            $('.tipsPrepared').hide();
            $('.ProviderCalling1').hide();
            $('.thankYou').hide();
            $('.modalWrapper').hide();
            $('.CallInSession1').hide();
            $('.noOneAnswered').hide();
            menuCurrentSearch();
            
            
        }

        function foundServiceProvider()
        {            
            $('.containerMapDiv').show();
            $('.matchProviders').hide();
            $('.foundServiceProvider').show();
            $('.radar').hide();
            $('.tipsPrepared').hide();
            $('.ProviderCalling1').hide();
            $('.thankYou').hide();
            $('.modalWrapper').hide();
            $('.CallInSession1').hide();
        }

        function tips()
        {
            $('.containerMapDiv').hide();
            $('.matchProviders').hide();
            $('.foundServiceProvider').hide();
            $('.tipsPrepared').show();
            $('.ProviderCalling1').hide();
            $('.thankYou').hide();
            $('.modalWrapper').hide();
            $('.CallInSession1').hide();
        }

        function providerCalling1() {
            $('.containerMapDiv').hide();
            $('.matchProviders').hide();
            $('.foundServiceProvider').hide();
            $('.tipsPrepared').hide();
            $('.ProviderCalling1').show();
            $('.ProviderCalling1 #recordIcon').removeClass('recordingImage');
            $('.ProviderCalling1 #recordIcon').addClass('recordImage');
            $('.ProviderCalling1MiddleRight1').show();
            $('.ProviderCalling1MiddleRight2').hide();
            $('.ProviderCalling1Bottom').show();
            $('.thankYou').hide();
            $('.modalWrapper').hide();
            $('.CallInSession1').hide();
        }       

       

        function providerCalling1Tips()
        {
            $('.containerMapDiv').hide();
            $('.matchProviders').hide();
            $('.foundServiceProvider').hide();
            $('.tipsPrepared').hide();
            $('.ProviderCalling1').show();
            $('.ProviderCalling1 #recordIcon').removeClass('recordImage');
            $('.ProviderCalling1 #recordIcon').addClass('recordingImage');
            $('.ProviderCalling1 .mainTitle').html('WE ARE CONNECTING YOU <br> TO YOUR PRO');
            $('.ProviderCalling1 .subTitle').html('For best results, ask the pro this questions...');
            
            $('.ProviderCalling1MiddleRight1').hide();
            $('.ProviderCalling1MiddleRight2').show();
            $('.ProviderCalling1Bottom').hide();            
            $('.thankYou').hide();
            $('.modalWrapper').hide();
            $('.CallInSession1').hide();

           

        }

        function callInSession1() {
            initTimer(); 
            $('.containerMapDiv').hide();
            $('.matchProviders').hide();
            $('.foundServiceProvider').hide();
            $('.tipsPrepared').hide();
            $('.ProviderCalling1').hide();
            $('.thankYou').hide();
            $('.modalWrapper').show();
            $('.CallInSession1').show(); 


        }

        function closeCallInSession1() {
            clearInterval(timer);
            thankYou();

        }

        function thankYou()
        {
            $('.containerMapDiv').hide();
            $('.matchProviders').hide();
            $('.foundServiceProvider').hide();
            $('.tipsPrepared').hide();
            $('.ProviderCalling1').hide();
            $('.thankYou').show();
            $('.modalWrapper').hide();
            $('.CallInSession1').hide();
            menuCurrentThanks();
        }

        function noOneAnswers()
        {            
            $('.containerMapDiv').hide();
            $('.matchProviders').hide();
            $('.foundServiceProvider').hide();
            $('.tipsPrepared').hide();
            $('.ProviderCalling1').hide();
            $('.thankYou').hide();
            $('.modalWrapper').show();
            $('.CallInSession1').hide();            
            $('.noOneAnswered').show();            
        }

        function closeNoOneAnswers()
        {
            matchingAvailable();
        }
        

        function setSupplierDetailsHardCode()
        {
            $('.providerName .providerItemText').text('shai');
            $('.providerAddress .providerItemText').text('nachal dalya');
            $('.providerPayments .providerItemText').text('shekel');
            $('.lnk_supplier').attr('href', 'http://www.a.co.il');
            $('.lnk_supplier').text('shai');
            $('.yelpReviewAvg img').attr('src', 'http://s3-media3.fl.yelpcdn.com/assets/2/www/img/34bc8086841c/ico/stars/v1/stars_2.png');
            $('.yelpContent2 .reviewText').text('michi mici');
            $('.yelpContent2 A').attr('href', 'http://www.globes.co.il');
        }

        function showReviews(ifShow)
        {
            if (ifShow)
                $('.reviews').show();
            else
                $('.reviews').hide();
        }

        function sendEmail()
        {     
            var strEmail=$('#txt_email').val();
            var validEmail;
            validEmail=checkEmail(strEmail);

            //alert(validEmail);

            if(validEmail)
            {
                sendEmailWebService(strEmail);

                $('#btn_email').prop('disabled',true);
                $('#btn_email').css('background-color','#ccc');
                
            }                
            
            else
                alert('email is not valid');

            return false;
        }      

        function initDetails()
        {
            getHeadingTips();         
            

            //initTips();
            
        }


        $(document).ready(function () {
            $('.reviews .yelpOpenerObj').click(function () {

                if ($(this).closest('.reviews').find('.yelpContent').css('display') == 'none') {
                    $(this).closest('.reviews').find('.yelpContent').css('display', 'block');
                    $(this).css('background-position', '0px 0px');
                }

                else {
                    $(this).closest('.reviews').find('.yelpContent').css('display', 'none');
                    $(this).css('background-position', '0px -16px');
                }

            }
            )
            
            
            $('.CallInSession1 .Continue').click(function()
            {
                clearInterval(timer);
                nextCustomer();
            }
            )

            if (typeof noProblemDesktopBinder != "undefined")
            {                
                //noProblemDesktopBinder.setSize(462, 720);
                noProblemDesktopBinder.setSize(452, 635); // the inside is 446 614 like the design
                //noProblemDesktopBinder.connectToPusher("aaa");
            }
                

            //loadSuppliersMap();
            
            //$('.containerMapDiv').hide();
            initDetails();

            
        }
        );
    </script>    

    <script>
        // tips
        var headingTips;
        var headingTips2;

        function HeadingTips(heading,arrTips,objContainer,objQuetion, objAnswer,objProgressBar,progressBarCircleId,objNextTip,objPreviousTip,objEvaluationNumber)
        {
            this.heading=heading;
            this.tips=arrTips;            
            this.currentTip=0;
            this.objContainer=objContainer;
            this.objQuetion=objQuetion;
            this.objAnswer=objAnswer;
            this.objProgressBar=objProgressBar;
            this.progressBarCircleId=progressBarCircleId;
            this.objNextTip=objNextTip;
            this.objPreviousTip=objPreviousTip;
            this.objEvaluationNumber=objEvaluationNumber;            
        }
        
        HeadingTips.prototype.initFirstTip=function()
        {
            var _this=this;
            $(this.objContainer).find(this.objQuetion).text(_this.tips[0]['question']);
            $(this.objContainer).find(this.objAnswer).text(_this.tips[0]['answer']);
            //alert(this.objEvaluationNumber);

            /*
            if(this.objEvaluationNumber)
            {                
                $(this.objEvaluationNumber).text("qwewqwqe");
            }
            */
                
        }

        HeadingTips.prototype.initPaging=function()
        {
            var _this=this;

            for(var i=0;i<_this.tips.length;i++)
            {
                var tipCirclePrepared = document.createElement('div');

                if(i==0)
                    tipCirclePrepared.className='circle circleActive'; 
                else
                    tipCirclePrepared.className='circle'; 

                tipCirclePrepared.id=_this.progressBarCircleId + i;
                
                //alert(_this.objProgressBar + ' ' + tipCirclePrepared);
                $(_this.objProgressBar).append(tipCirclePrepared);
                
            }
        }

        HeadingTips.prototype.resetTips=function()
        {   
            this.currentTip=0;
            $(this.objQuetion).text(this.tips[this.currentTip]['question']);
            $(this.objAnswer).text(this.tips[this.currentTip]['answer']);

            for(var i=1;i<this.tips.length;i++)
                $('#' + this.progressBarCircleId + i).removeClass('circleActive');

            //$('#' + this.progressBarCircleId + this.currentTip).addClass('circleActive'); 
            

        }

        
        HeadingTips.prototype.setEvaluationDetails=function()
        {
            tipId=this.tips[this.currentTip]['id'];
            //alert(tipId);

            var tipSupplierDetails=findSupplierTipDetails2(tipId);
            //{ TipId="edf256d8-20d6-e411-befd-001517d10f6e", Percent=0, Show=false
            //alert(tipSupplierDetails['TipId']);

            //currentSupplier.arrTips[headingTips2.currentTip]["alreadyClickedEvaluation"]

            var alreadyClickedEvaluation=tipSupplierDetails['alreadyClickedEvaluation'];
            //alert("alreadyClickedEvaluation:" + alreadyClickedEvaluation);
            var tipPercent=tipSupplierDetails['Percent'];
            var tipShow=tipSupplierDetails['Show'];

            $(this.objEvaluationNumber).text(tipPercent.toFixed(2));                      
            

            if(alreadyClickedEvaluation=="good")
            {
                $('.answerGoodBackground').css('background-position','-348px -945px'); 
                $('.answerBadBackground').css('background-position','-256px -2004px');
            }
                 
            else if(alreadyClickedEvaluation=="bad")
            {
                $('.answerGoodBackground').css('background-position','-386px -945px'); 
                $('.answerBadBackground').css('background-position','-293px -2004px');
            }
                
            else
            {
                $('.answerGoodBackground').css('background-position','-386px -945px');  
                $('.answerBadBackground').css('background-position','-256px -2004px');
            }

        }
        


        HeadingTips.prototype.init=function()
        {            
            var _this=this;
            this.initFirstTip();            
            this.initPaging();                   


            $(_this.objNextTip).click(function()
            {              
                
                _this.currentTip++;
                if(_this.currentTip==_this.tips.length)
                {

                    _this.currentTip=0;

                    for(var i=1;i<_this.tips.length;i++)
                        $('#' + _this.progressBarCircleId + i).removeClass('circleActive');
                }                    

                $(_this.objQuetion).text(_this.tips[_this.currentTip]['question']);
                $(_this.objAnswer).text(_this.tips[_this.currentTip]['answer']);
                $('#' + _this.progressBarCircleId + _this.currentTip).addClass('circleActive');
                
                if(_this.objEvaluationNumber)
                {
                    _this.setEvaluationDetails();
                }                    
               
                
            })
            
            $(_this.objPreviousTip).click(function()
            {                
                _this.currentTip--;
                if(_this.currentTip<0)
                    _this.currentTip=0;
                 
                $(_this.objQuetion).text(_this.tips[_this.currentTip]['question']);
                $(_this.objAnswer).text(_this.tips[_this.currentTip]['answer']);
                $('#' + _this.progressBarCircleId + (_this.currentTip+1)).removeClass('circleActive');               
                
                if(_this.objEvaluationNumber)
                {
                    _this.setEvaluationDetails();
                }  
            })
           
        };

        /*
        var arrTips=new Array();
        var objTip1=new Object;
        objTip1['question']="1 sdfsdf sdf df sdfsdfd sdfdf?";
        objTip1['answer']="answer 1 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";

        var objTip2=new Object;
        objTip2['question']="2 sdfsdf sdf df sdfsdfd sdfdf  sdfsdf sdf df sdfsdfd sdfdf sdfsdf sdf df sdfsdfd sdfdf sdfsdf sdf df sdfsdfd sdfdf?";
        objTip2['answer']="answer 2 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";

        
        var objTip3=new Object;
        objTip3['question']="3 sdfsdf sdf df sdfsdfd sdfdf?";
        objTip3['answer']="answer 3 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";

        var objTip4=new Object;
        objTip4['question']="4 sdfsdf sdf df sdfsdfd sdfdf?";
        objTip4['answer']="answer 4 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";

        var objTip5=new Object;
        objTip5['question']="5 sdfsdf sdf df sdfsdfd sdfdf?";
        objTip5['answer']="answer 5 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";

        var objTip6=new Object;
        objTip6['question']="6 sdfsdf sdf df sdfsdfd sdfdf?";
        objTip6['answer']="answer 6 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";

        var objTip7=new Object;
        objTip7['question']="7 sdfsdf sdf df sdfsdfd sdfdf?";
        objTip7['answer']="answer 7 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";

        var objTip8=new Object;
        objTip8['question']="8 sdfsdf sdf df sdfsdfd sdfdf?";
        objTip8['answer']="answer 8 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";

        var objTip9=new Object;
        objTip9['question']="9 sdfsdf sdf df sdfsdfd sdfdf?";
        objTip9['answer']="answer 9 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";
        
        arrTips[0]=objTip1;
        arrTips[1]=objTip2;        
        arrTips[2]=objTip3;
        arrTips[3]=objTip4;
        arrTips[4]=objTip5;
        arrTips[5]=objTip6;
        arrTips[6]=objTip7;
        arrTips[7]=objTip8;
        arrTips[8]=objTip9;       
        */

        function hideEvaluation()
        {            
            currentSupplier.arrTips[headingTips2.currentTip]["alreadyClickedEvaluation"]="yes";          

            headingTips2.setEvaluationDetails();
        }

        function tipEvaluation(isPositiveFeedBack)
        {
            //alert(isPositiveFeedBack + ' ' + headingTips2.currentTip);
            var ifAlreadyClicked;
            //alert(currentSupplier.arrTips[headingTips2.currentTip]["alreadyClickedEvaluation"]);
            if(currentSupplier.arrTips[headingTips2.currentTip]["alreadyClickedEvaluation"]!="good" && currentSupplier.arrTips[headingTips2.currentTip]["alreadyClickedEvaluation"]!="bad")
            {
                //alert("not clicked");

                if(isPositiveFeedBack)
                {
                    $('.answerGoodBackground').css('background-position','-348px -945px');
                    currentSupplier.arrTips[headingTips2.currentTip]["alreadyClickedEvaluation"]="good";
                }
                    
                else
                {
                    $('.answerBadBackground').css('background-position','-293px -2004px');
                    currentSupplier.arrTips[headingTips2.currentTip]["alreadyClickedEvaluation"]="bad";
                }
                    

                var tipId=headingTips2.tips[headingTips2.currentTip]['id'];
                //alert("tipId: "+ tipId );

                var supplierId=currentSupplier.supplierId;
                //alert("supplierId: "+ supplierId );         
                   
               
                    
                //hideEvaluation();
            
                setCustomerTipFeedback(tipId, supplierId, isPositiveFeedBack)
            }
            else
            {
                //alert("already clicked");
            }
        }
    </script>

    <script>
        function initInstansesTips()
        {
            headingTips=new HeadingTips("<%#ExpertiseName%>",arrTips,'.tipsContent','.questionText','.answerText','.progressTipsPreparedInner','circleTipPrepared','.nextTipPrepared','.previousTipPrepared',null);
            headingTips.init();

            
            headingTips2=new HeadingTips("<%#ExpertiseName%>",arrTips,'.tips2','.questionText','.answerText','.progressTipsInner','circleTip','.nextTip','.previousTip','.evaluationAllNumber');
            headingTips2.init();

        }

        var arrTips=new Array();

        function fillArrayTips(arrTipsFromServer)
        {            
            

            for(var i=0;i<arrTipsFromServer.length;i++)
            {
                //alert(arrTipsFromServer[i]['Question']);
                //alert(arrTipsFromServer[i]['Answer']);
                var objTip=new Object;
                objTip['question']=arrTipsFromServer[i]['Question'];
                objTip['answer']=arrTipsFromServer[i]['Answer']; 
                objTip['id']=arrTipsFromServer[i]['TipId'];                
                arrTips[i]=objTip;
            }

            initInstansesTips()
          
            /*
            var arrTips=new Array();
            var objTip1=new Object;
            objTip1['question']="1 sdfsdf sdf df sdfsdfd sdfdf?";
            objTip1['answer']="answer 1 sdfsdf sdf df sdfsdfd sdfdf adas dsd sdas d asd asd adsdasd sd sad asd asd sa aasd asd asd asdasdasdasdasdsadasd sd sa sad asd asd sad sad sa asdsad sad sad asdsa";
            */
        }

        function getHeadingTips()
        {
            //alert("getHeadingTips");
            var _data = "{incidentId: '<%#ServiceRequestId%>'}";
            ////var _data = "{incidentId: '4B3319A5-D0E5-42FD-B9B1-528E5937E3AA'}";

            $.ajax({
                url: "DesktopAppWebService.asmx/getDeskAppHeadingTips",
                //url: ROOT + "ppc/inviteMorePro.ashx",
                data: _data,
                dataType: "json",
                type: "POST",
                async: true,
                contentType: "application/json; charset=utf-8",
                //dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    //alert(_data.status);
                    //alert(_data.value);

                    if(_data.status=="success")
                        fillArrayTips(_data.value)
                    else
                        alert("Problem with server. Try later.");
                    //[Object { Question="Do you provide a written contract?", Answer="Never trust a contractor...u would be responsible.", CategoryId="decfa4f6-37aa-e111-9280-001517d10f6e"}, Object { Question="Is there a deposit? If so, how much?", Answer="If possible avoid paying...f the value of the job.", CategoryId="decfa4f6-37aa-e111-9280-001517d10f6e"}]

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("error" + errorThrown);
                    return;
                },
                complete: function () {
                
                }
            });
        }

       
        function startDesktopAppCase()
        {
            //alert("startDesktopAppCase");
            var _data = "{incidentId: '<%#ServiceRequestId%>'}";
            //var _data = "{incidentId: '4B3319A5-D0E5-42FD-B9B1-528E5937E3AA'}";

            $.ajax({
                url: "DesktopAppWebService.asmx/startDesktopAppCase",                
                data: _data,
                dataType: "json",
                type: "POST",
                async: true,
                contentType: "application/json; charset=utf-8",                
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    //alert(_data.status);
                    if(_data.status=="success")
                    {
                        ////getCaseDataForDesktopApp();

                        if(noProblemDesktopBinder!="undefined")
                            noProblemDesktopBinder.connectToPusher('<%#ServiceRequestId%>');
                    }

                    else
                        alert("Problem with server. Try later.");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("error" + errorThrown);
                    return;
                },
                complete: function () {
                
                }
            });
        }        

        function setCaseDataForDesktopApp(objCase)
        {
            // exaple of objCase :  //{"d":"{\"status\":\"success\",\"value\":{\"Advertisers\":[{\"Name\":\"RegisQA 29\",\"Address\":\"Albany, NY 12201, United States\",\"YelpRating\":null,\"YelpReview\":null,\"PayWith\":null,\"Index\":0,\"SupplierId\":\"fd422d1c-9049-4f72-bb00-ddf528936646\",\"RecordingCall\":false}],\"InterestedAdvertisers\":1}}"}
            //alert("setCaseDataForDesktopApp");
            if(objCase.Advertisers.length>0)
            { 

                foundServiceProvider();
                suppliersMatches(objCase.InterestedAdvertisers);                  

                for(var i=0;i<objCase.Advertisers.length;i++)
                {
                    var supplierDetails=new SupplierDetails(objCase.Advertisers[i].SupplierId,objCase.Advertisers[i].Name,objCase.Advertisers[i].Address,
                        objCase.Advertisers[i].PayWith,objCase.Advertisers[i].Index,objCase.Advertisers[i].RecordingCall,
                        objCase.Advertisers[i].YelpRating,objCase.Advertisers[i].YelpReview,objCase.Advertisers[i].YelpLink,
                        objCase.Advertisers[i].TipsPercentages,"","","");
                    arraySupplierDetails.push(supplierDetails);
                } 
                
                loadSuppliersMap();

            }          

            else
            {
                //alert("There aren't suppliers");
                noOneAnswers()
            }
                

        }

        function getCaseDataForDesktopApp()
        {
            //alert("getCaseDataForDesktopApp");
            var _data = "{incidentId: '<%#ServiceRequestId%>'}";
            //var _data = "{incidentId: '4B3319A5-D0E5-42FD-B9B1-528E5937E3AA'}";

            $.ajax({
                url: "DesktopAppWebService.asmx/getCaseDataForDesktopApp",               
                data: _data,
                dataType: "json",
                type: "POST",
                async: true,
                contentType: "application/json; charset=utf-8",
                //dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    //alert(_data.status);

                    if(_data.status=="success")
                        setCaseDataForDesktopApp(_data.value)
                    else
                        alert("Problem with server. Try later.");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("error" + errorThrown);
                    return;
                },
                complete: function () {
                
                }
            });
        }

        function continueDesktopAppCase(wantsToContinue)
        {            
            var _data = "{incidentId: '<%#ServiceRequestId%>',wantsToContinue:" + wantsToContinue + "}";

            $.ajax({
                url: "DesktopAppWebService.asmx/continueDesktopAppCase",               
                data: _data,
                dataType: "json",
                type: "POST",
                async: true,
                contentType: "application/json; charset=utf-8",
                //dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    //alert(_data.status);

                    if(_data.status=="success")
                    {

                    }
                        
                    else
                        alert("Problem with server. Try later.");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("error" + errorThrown);
                    return;
                },
                complete: function () {
                
                }
            });
        }
            
        function setCustomerTipFeedback(tipId, supplierId, isPositiveFeedBack)
        {
            var _data = "{incidentId: '<%#ServiceRequestId%>',tipId:'" + tipId + "',supplierId:'" + supplierId + "',isPositiveFeedBack:" + isPositiveFeedBack+ "}";

            $.ajax({
                url: "DesktopAppWebService.asmx/setCustomerTipFeedback",               
                data: _data,
                dataType: "json",
                type: "POST",
                async: true,
                contentType: "application/json; charset=utf-8",
                //dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    //alert(_data.status);

                    if(_data.status=="success")
                    {
                        //alert("success");
                    }
                        
                    else
                        alert("Problem with server. Try later.");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("error" + errorThrown);
                    return;
                },
                complete: function () {
                
                }
            });
        }

        function wsCancelRecord(supplierId)
        {
            var _data = "{incidentId: '<%#ServiceRequestId%>', supplierId: '" + supplierId + "'}";

            $.ajax({
                url: "DesktopAppWebService.asmx/cancelRecordForSupplier",               
                data: _data,
                dataType: "json",
                type: "POST",
                async: true,
                contentType: "application/json; charset=utf-8",
                //dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    //alert(_data.status);

                    if(_data.status=="success")
                    {
                        //alert("success");
                    }
                        
                    else
                        alert("Problem with server. Try later.");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("error" + errorThrown);
                    return;
                },
                complete: function () {
                
                }
            });
        }

        function sendEmailWebService(email)
        {
            var _data = "{incidentId: '<%#ServiceRequestId%>',email:'" + email + "'}";

            $.ajax({
                url: "DesktopAppWebService.asmx/sendEmailToCustomerAfterCase",               
                data: _data,
                dataType: "json",
                type: "POST",
                async: true,
                contentType: "application/json; charset=utf-8",
                //dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    //alert(_data.status);

                    if(_data.status=="success")
                    {
                        //alert("success");
                    }
                        
                    else
                        alert("Problem with server. Try later.");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("error" + errorThrown);
                    return;
                },
                complete: function () {
                
                }
            });
        }


        var currentSupplier;

        function findSupplierById(supplierId)
        {            

            for(var i=0;i<arraySupplierDetails.length;i++)
            {
                if(arraySupplierDetails[i].supplierId==supplierId)
                {
                    currentSupplier=arraySupplierDetails[i];                    
                }
            }

            return currentSupplier;
        }

        function findSupplierByIndex(index)
        {            

            for(var i=0;i<arraySupplierDetails.length;i++)
            {
                if(arraySupplierDetails[i].index==index)
                {
                    currentSupplier=arraySupplierDetails[i];                    
                }
            }

            return currentSupplier;
        }

        function getShortReview(review)
        {
            if(review.length>100)
            {               
                var i;
                for(i=100;i<review.length;i++)
                {                   
                    if(review.charAt(i)==' ')
                    {                        
                        break;
                    }                        
                }
                var tempReview=review.substring(0,i) + "...";

                return tempReview;
            }

            return review;
        }

        function setBeforeSupplierArrow(index)
        {
            $('.providerIndex' + index).css('background-color','#dedede');
            $('.providerIndex' + index).css('border-top-width','1px');
            $('.providerIndex' + index).css('border-left-width','1px');
            $('.providerIndex' + index).css('border-right-width','1px');
            $('.containerProviderIndex' + index).css('background-image',"url('')");
        }

        function setCurrentSupplierArrow(index)
        {            
            $('.providerIndex' + index).css('background-color','transparent');
            $('.providerIndex' + index).css('border-width','0px');
            $('.containerProviderIndex' + index).css('background-image','url("images/sprites/DeskApp_icons.png")');         
                
        }

        function setCurrentSupplierArrowPhone(index)
        {            
            $('.providerIndexPhone' + index).css('background-position','-448px -1909px');              
        }

        function setBeforeSupplierArrowPhone(index)
        {            
            $('.providerIndexPhone' + index).css('background-position','0 -2004px');              
        }


        function setRecordIconWhenConnected(objSupplier)
        {
            //alert("RecordingCall: " + objSupplier.recordingCall + ' recordingCallClient: ' + objSupplier.recordingCallClient);

            if(objSupplier.recordingCall)
            {
                $('.recordImage2').css('background-position','-218px -2004px');
                $('.recordImage2').attr('title','My call is being recorded');
            }
                                  
            else  
            {
                $('.recordImage2').css('background-position','-181px -2004px');
                $('.recordImage2').attr('title','Record my calls is disabled');
            }
        }

        function setSupplierDetails(objSupplier)
        {
            if(objSupplier!="undefined")
            {
                //alert(callingCustomer.name);
                providerCalling1();
                $('.providerName .providerItemText').text(objSupplier.name);
                $('.providerAddress .providerItemText').text(objSupplier.address);
                setLatLot2(objSupplier.address);

                var strPayWith="Pay with";

                for(var i=0;i<objSupplier.payWith.length;i++)
                {
                    if(i==objSupplier.payWith.length-1)
                        strPayWith+=" " + objSupplier.payWith[i] + ".";
                    else if(i==objSupplier.payWith.length-2)
                        strPayWith+=" " + objSupplier.payWith[i] + " and";
                    else
                        strPayWith+=" " + objSupplier.payWith[i]+",";
                }

                $('.providerPayments .providerItemText').text(strPayWith);
                $('.providerLink .providerItemText A').text(objSupplier.name);
                $('.providerLink .providerItemText A').attr('href',$('.providerLink .providerItemText A').attr('href') + "business/" + objSupplier.name + "/" + objSupplier.supplierId);

                if(objSupplier.yelpReview && objSupplier.yelpReview.length>0)
                {                    
                    $('.ProviderCalling1Bottom').show();

                    //var tempStr="Okay so I'm definitely not from the area so finding a moving company was really hard at first for all of my stuff. I had a lot of valuables and previously";
                   
                    //$('.reviewText').text(getShortReview(tempStr));
                    //alert(objSupplier.yelpReview);
                    //alert(getShortReview(objSupplier.yelpReview));
                    $('.reviewText').text(getShortReview(objSupplier.yelpReview));
                    $('.yelpReviewAvg img').attr('src',objSupplier.yelpRating);
                    $('.yelpContent2 A').attr('href',objSupplier.yelpLink);

                    /* for real yelp supplier
                    $('.providerPayments').hide();
                    $('.providerLink .providerItemText A').attr('href',objSupplier.yelpLink);
                    $('.providerNameYelpContainerLogo').show();
                    */

                }                    
                else // not yelp
                {
                    $('.ProviderCalling1Bottom').hide();                    
                    
                }
                    

                if(objSupplier.recordingCall)
                {
                    $('.recordImage').css('background-position','-143px -2004px');
                    $('.recordImage').attr('title','Record my calls');
                }
                                  
                else  
                {
                    $('.recordImage').css('background-position','-181px -2004px');
                    $('.recordImage').attr('title','Record my calls is disabled');
                }
                   
                
                $('.answerGoodBackground').css('background-position','-386px -945px');  
                $('.answerBadBackground').css('background-position','-256px -2004px');

                //alert(objSupplier.index);
                $('.providerIndex').removeClass('providerIndexCurrent');
                $('.providerIndex' +  objSupplier.index).addClass('providerIndexCurrent');                

                switch(objSupplier.index)
                {
                    case 0:   
                        setCurrentSupplierArrow(0);
                        break;
                    case 1: 
                        setBeforeSupplierArrow(0);
                        setBeforeSupplierArrowPhone(0);
                        setCurrentSupplierArrow(1);
                        break;
                    case 2:
                        setBeforeSupplierArrow(1);
                        setBeforeSupplierArrowPhone(1);
                        setCurrentSupplierArrow(2);
                        break;
                    default:
                        break;
                }

                
            }
        }

        function cancelRecord()
        {
            //alert("cancel record");

            if(currentSupplier.recordingCallClient)
            {                
                var supplierId=currentSupplier.supplierId;
                //alert("supplierId: "+ supplierId );
                //alert("before:" + currentSupplier.recordingCallClient);
                currentSupplier.recordingCallClient=false;
                //alert("after:" + currentSupplier.recordingCallClient);
                $('.recordImage2').css('background-position','-181px -2004px');
                wsCancelRecord(supplierId);
            }

            else
            {
                //alert("second Time");
            }

        }

        function openAndSendmail(heading) {            
            var subject = "NoProblem.Me App, FIND A PRO " + heading;
            var body = "Hello NoProblem.Me:\r\n\r\n";
            body += "I have other important questions to ask the pro\r\n\r\n";
            body += "Here's my questions:";
            var uri = "mailto:service@noproblem.me?subject=";
            uri += encodeURIComponent(subject);
            uri += "&body=";
            uri += encodeURIComponent(body);
            //window.open(uri);
            //location.href = uri;
            //noProblemDesktopBinder.BrowseTo(uri);
            if(noProblemDesktopBinder!="undefined")
                noProblemDesktopBinder.browseTo(uri);
        }

       

        function nextCustomer()
        {
            //  alert(currentSupplier.index);
            var currentSupplierIndex=currentSupplier.index + 1;
            //alert(findSupplierByIndex(currentSupplierIndex));
            var continueSupplier=findSupplierByIndex(currentSupplierIndex);
            setSupplierDetails(continueSupplier);
            continueDesktopAppCase(true);
        }


       
        /***************  functions from pusher ****************************/

        function caseDataReady()
        {
            //alert("caseDataReady");
            // from pusher
            getCaseDataForDesktopApp();
        }

        function callingCustomer(objData)
        {
            //alert("callingCustomer");
            // from pusher
            // When the supplier answers and presses one
            // the data is {’SupplierId’:’00000000-0000-0000-0000-000000000000'}

            var callingSupplierId;
            callingSupplierId=objData.SupplierId;

            var callingSupplier=findSupplierById(callingSupplierId);
            setSupplierDetails(callingSupplier);
            menuCurrentInSession();
           
        }

        function setSupplierTipEvaluation(connectedSupplier)
        {
            //alert(connectedSupplier.arrTips[0].TipId + " " + connectedSupplier.arrTips[0].Percent);
            $(headingTips2.objEvaluationNumber).text(connectedSupplier.arrTips[0].Percent.toFixed(2));
        }

        function customerConnected(objData)
        {
            //alert("customerConnected");
            // from pusher
            // When the customer answers
            // the data is {’SupplierId’:’00000000-0000-0000-0000-000000000000'}

            var connectedSupplierId;
            connectedSupplierId=objData.SupplierId;

            var connectedSupplier=findSupplierById(connectedSupplierId);
            providerCalling1Tips();
            headingTips2.resetTips();
            setSupplierTipEvaluation(connectedSupplier);
            setCurrentSupplierArrowPhone(connectedSupplier.index);
            setRecordIconWhenConnected(connectedSupplier);
            
        }      

        function callEnded(data)
        {
            //alert("callEnded");
            // from pusher
            //var data = {“HasMoreSuppliers”:true};

            if(data.HasMoreSuppliers)
                callInSession1();
            else
            {
                if(noProblemDesktopBinder!="undefined")
                    noProblemDesktopBinder.disconnectFromPusher();
                thankYou();
            }
                
        }

        


    </script>
</asp:Content>


<asp:Content  runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    

    <div class="mainWrapperInner">  
           
        <div class="tipsPrepared">
         
            <div class="tipsPreparedTop">
                <div class="mainTitle">PREPARE FOR SUCCESS</div>
                <div class="subTitle">For best results, make sure to ask the professionals the following questions:</div>
            </div>

            <div class="containerTips">
                            <div class="tipsContent">
                                <div class="previousTipPrepared"></div>
                                <div class="question">
                                    <div class="questionIcon"></div>
                                    <div class="questionText"></div>
                                </div>

                                <div class="clear"></div>

                                <div class="answer">
                                    <div class="answerIcon"></div>
                                    <div class="answerText">
                                   
                                    </div>
                                
                                </div>

                                <div class="clear"></div>
                                <div class="shadow"></div>
                                <div class="progressTipsPrepared">
                                    <div class="progressTipsPreparedInner">
                                        <!--
                                        <div id="circleTipPrepared0" class="circle circleActive"></div>
                                        <div id="circleTipPrepared1" class="circle"></div>
                                        <div id="circleTipPrepared2" class="circle"></div>
                                        <div id="circleTipPrepared3" class="circle"></div>
                                        <div id="circleTipPrepared4" class="circle"></div>
                                        <div id="circleTipPrepared5" class="circle"></div>
                                        <div id="circleTipPrepared6" class="circle"></div>
                                        <div id="circleTipPrepared7" class="circle"></div>
                                        <div id="circleTipPrepared8" class="circle"></div>
                                        -->
                                    </div>
                                </div>


                                <div class="nextTipPrepared"></div>
                            </div>


                        

                        </div>

            <div class="tipsPreparedBottom">

                <div class="tipsPreparedBottomInner">
                    <div class="dontWorry">
                        Don't worry. We'll remind you of this during the call                     
                    </div>
                    
                    <div class="skipQuestions btn">
                        <a href="javascript:void(0);" onclick="matchingAvailable();startDesktopAppCase();">I AM READY! CONNECT ME NOW</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="containerMapDiv" style="display:none">       
                 <div class="MapDiv" style="width:100%;height:100%;">             
                    <div id="map_canvas" ></div>                     
                 </div>        

                 <div class="mapBottomTranparent"></div>
                 <div class="radar"></div>     
         </div>    

        <div class="matchProviders" style="display:none">
        

            <div class="matchProvidersContent">    

                <div class="matchProvidersContentText">
                    <div class="mainTitle">HOLD ON!</div>
                    <div class="matchProvidersContentText1">
                        <div>We’re reaching out to available</div>
                        <div>pros interested in this job.</div>                    
                    </div>

                    <div class="matchProvidersContentText2">
                        <div>This may take a few minutes.</div>
                        <div class="Closing">Closing this window won’t stop you from getting calls</div>
                    </div>

                    <div class="matchProvidersContentText3">
                        Please make sure your phone is on and available.
                    </div>
                </div>           

            </div>
        
        </div>            

        <div class="foundServiceProvider" style="display:none">
        
            <div class="mainTitle">GREAT!</div>
        
            <div class="foundServiceProviderText">
                <div class="foundServiceProviderText1">We found <span class="numberMatches">3</span> matches</div>
                <div class="foundServiceProviderText2">that want speak with.</div>
                <div class="foundServiceProviderText3">
                    Please make sure your phone is on and available.
                </div>
            </div>         

            <div class="foundServiceProviderImg"><img alt="animEyes" src="/npsb/flavours/images/animEyes.gif" /></div>        


            <div class="clear"></div>

        
        
       

        </div>

        <div class="ProviderCalling1" style="display:none">

            <div class="ProviderCalling1Top">
                <div class="mainTitle">Meet your pros</div>
                <div class="subTitle">We think you’re going to like them.</div>            
            </div>        

            <div class="clear"></div>

            <div class="ProviderCalling1Middle">
                <div class="ProviderCalling1MiddleLeft">
                    <div class="containerProviderIndex containerProviderIndex0">
                        <div class="providerIndex providerIndex0">
                            <div class="providerIndexPhone providerIndexPhone0">
                                <div class="providerIndexPhoneText">1</div>
                            </div>
                        </div>
                    </div>
                    <div class="containerProviderIndex containerProviderIndex1">
                        <div class="providerIndex providerIndex1">
                            <div class="providerIndexPhone providerIndexPhone1">
                                <div class="providerIndexPhoneText">2</div>
                            </div>
                        </div>
                    </div>

                    <div class="containerProviderIndex containerProviderIndex2">
                        <div class="providerIndex providerIndex2">
                            <div class="providerIndexPhone providerIndexPhone2">
                                <div class="providerIndexPhoneText">3</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ProviderCalling1MiddleRight">

                    <div class="ProviderCalling1MiddleRight1" style="display:none;">
                   
                        <div class="providerName">
                            <div class="providerNameInner">                   
                                <span class="providerItemText">“House of Decor”</span><span class="providerNameYelpContainerLogo"><img src="images/yelpMiniMapLogo.png" /></span>
                                <br /> is calling you now...
                            </div> 
                            <div id="recordIcon" class="recordImage" ></div>

                        </div>

                        <div class="clear"></div>

                        <div class="conatinerImage">
                            <img width="333" src="/npsb/flavours/images/phoneAnim.gif"/>
                        </div>

                        <div class="providerItem providerAddress">
                            <div class="providerIcon providerIconAddress">
                                <span class="fa-stack fa-lg">
                                  <i class="fa fa-circle fa-stack-2x"></i>
                                  <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                                </span>                        
                            </div>
                            <div class="providerItemText">7701 Sepulveda Blvd, Los Angeles,CA 91405, USA</div>

                        </div>

                        <div class="clear"></div>

                        <div class="providerItem providerPayments">
                            <div class="providerIcon providerIconPayments">                       
                                 <span class="fa-stack fa-lg">
                                  <i class="fa fa-circle fa-stack-2x"></i>
                                  <i class="fa fa-usd fa-stack-1x fa-inverse"></i>
                                </span>        
                            </div>
                            <div class="providerItemText">Pay with AMEX, MASTER CARD,VISA and CASH.</div>

                        </div>

                        <div class="clear"></div>

                        <div class="providerItem providerLink">
                            <div class="providerIcon providerIconLink">
                                 <span class="fa-stack fa-lg">
                                  <i class="fa fa-circle fa-stack-2x"></i>
                                  <i class="fa fa-globe fa-stack-1x fa-inverse"></i>
                                </span>
                            </div>
                            <div class="providerItemText">
                                <!--
                                <a class="lnk_supplier" href="http://localhost:59764/business/melech%20hamekarerim%20baam/9968aec1-35fd-43e9-a325-530c3001e03f">House of decor</a>
                                -->
                                <a class="lnk_supplier" href="<%#url%>" target="_blank">House of decor</a>


                            </div>

                        </div>

                        <div class="ProviderCalling1Bottom">
                
                    <div class="reviews">
                        <div class="innerReviews">
                        <div class="yelpContent">
                            <!--
                            <div class="yelpContent1">
                                <div class="yelpUserImage"><img width="40px" height="40px" src="http://media1.fl.yelpcdn.com/upthumb/3Cr4xQ49kHHPFAN0P98xFQ/ss"></div>
                                <div class="yelpReviewDetails">
                                    <div class="yelpReviewStar"><img src="http://s3-media1.fl.yelpcdn.com/assets/2/www/img/f1def11e4e79/ico/stars/v1/stars_5.png"></div>
                                    <div class="yelpReviewDate">2014-09-22</div><div class="yelpReviewUser">Kevin M.</div>

                                </div>

                            </div>
                            -->

                            <div class="yelpContent2"><span class="reviewText"></span> <a target="_blank" href="http://www.yelp.com/biz/divine-moving-and-storage-new-york-2?hrid=KCxX-_LER18RRZrnZOu3sw">read more</a></div>
                            <div class="yelpContent3"><img src="http://qa.ppcnp.com/consumer/images/reviewsFromYelpWHT.gif"></div>

                           </div>

                
                            <div class="yelpContent0">
                            <div class="yelpPreview">
                                <div class="yelpOpener">
                                    <div class="yelpOpenerObj"></div>
                                    <!--<div class="yelpOpenerText">Recent review for melech hame+karerim baam</div>-->
                                    <div class="yelpContainerMiniLogo"><img src="images/yelpMiniMapLogo.png" /></div>
                                </div>
                                <!--
                                <div class="yelpReviewCounts">Based on <a target="_blank" href="http://www.yelp.com/biz/divine-moving-and-storage-new-york-2">24 reviews</a></div>
                                -->
                                <div class="yelpReviewAvg"><img src="http://s3-media3.fl.yelpcdn.com/assets/2/www/img/34bc8086841c/ico/stars/v1/stars_3.png"></div>
                            </div>
                            </div>
                        </div>
                

                    </div>
                </div>

                    </div>

                    <div class="ProviderCalling1MiddleRight2" style="display:block;">
                         <div class="providerName">                    
                            <span class="providerItemText">“House of Decor”</span>

                            <div class="providerExplain">
                                Call in session and recorded
                            </div>
                            
                            <div class="providerRecordCancel">
                                <a href="javascript:void(0);" onclick="javascript:cancelRecord();">Don't record this call</a>
                            </div>

                            <div id="recordIcon2" class="recordImage2"></div>
                        </div>
                    
                        <div class="containerTips2">
                            <div class="tips2">
                                
                                <div class="question">
                                    <div class="questionIcon"></div>
                                    <div class="questionText">Will there be a written contract?</div>
                                </div>

                                <div class="clear"></div>

                                <div class="answer">
                                    <div class="answerIcon"></div>
                                    <div class="answerText">
                                        sd fsdf sdsf dsd fsdfdf d fsdf sdfsd d
                                        d f dsfsdfsdfsdsd sfsdsf sd fsdf sd dsd
                                        sdfsd f sdf sdfsdfsdf sdf sd fsdf d 
                                        fsdfsd sd fsdfsddfsdf sdf dsfds fsdfdsf
                                    </div>
                                
                                </div>
                                

                                <div class="clear"></div>

                                <div class="containerEvaluation">
                                    <div class="containerEvaluationInner">
                                        <div class="evaluationTitle">How was the answer? (Click)
                                        <div class="evaluationInputs">
                                        <div class="evaluationInputsGood">                                    
                                            <div class="answerBackground answerGoodBackground" onclick="tipEvaluation(true);"></div>
                                        </div>
                                
                                        <div class="evaluationInputsBad">                                    
                                            <div class="answerBackground answerBadBackground" onclick="tipEvaluation(false);"></div>  
                                        </div>
                                                              
                                    </div>

                                        <div class="clear"></div>

                                        <div class="evaluationAll">
                                        <span class="evaluationAllNumber"></span>% like the answer
                                    </div>
                                    </div>
                                </div>

                            </div>


                                <div class="progressTips">

                                    <div class="previousTip"></div>

                                    <div class="progressTipsInner">
                                        <!--
                                        <div id="circleTip0" class="circle circleActive"></div>
                                        <div id="circleTip1" class="circle"></div>
                                        <div id="circleTip2" class="circle"></div>
                                        <div id="circleTip3" class="circle"></div>
                                        <div id="circleTip4" class="circle"></div>
                                        <div id="circleTip5" class="circle"></div>
                                        <div id="circleTip6" class="circle"></div>
                                        <div id="circleTip7" class="circle"></div>
                                        <div id="circleTip8" class="circle"></div>
                                        -->
                                    </div>

                                    <div class="nextTip"></div>

                                </div>

                                                      

                        </div>

                            <div class="shadow"></div>  
                    </div>

                

                
                   
                
                </div>
            
                </div>
            </div>
            <div class="clear"></div>

        
        </div>       

        <div class="CallInSession1" style="display:none">
            <!--
            <div class="close" onclick="closeCallInSession1();"></div>
            -->
            <div class="mainTitle">
                <div class="mainTitle1">SPEAK TO MORE</div>
                <div class="mainTitle2">PROFESSIONALS.</div>
            </div>
            <div class="compare">For best results, speak to several pros.</div>
        
            <div class="containerBtn">
                <div class="btn">
                    <div class="btnText">
                        <div class="Continue">CONTINUE</div>
                        <div class="container">
                            <div id="progress" style="width: 38px; height: 38px; margin-bottom: 10px;background-color:#000;"></div>                             
                        </div>

                    
                    </div>
                    <div id="progressMoreProfessionals" style="width: 80px; height: 80px; margin-bottom: 10px;"></div>
                </div>
                <div class="noThanks"><a href="javascript:void(0);" onclick="closeCallInSession1();">No thanks, i'm done.</a></div>
                <div class="clear"></div>
            </div>        
         </div>

        <div class="clear"></div>

        <div class="thankYou" style="display:none">
            <div class="thankyouContent">
                <div class="mainTitle">THANKS FOR USING NOPROBLEM.ME</div>

                <div class="subTitle">We got every word</div>

                <div class="containerEmail">
                    Enter your email to receive address to recieve
                    <br />
                    the recording and the pro's contact info.
                    <div class="emailInputs">
                        <div class="emailIcon"></div>
                        <asp:TextBox runat="server" ID="txt_email" CssClass="txt_input" place_holder="Enter your email address"
                                  HolderClass="place-holder" ></asp:TextBox>
                        <asp:Button  runat="server" ID="btn_email" OnClick="btn_email_Click" Text="Send" CssClass="btn" OnClientClick="return sendEmail();"/>
                    </div>
                
                </div>

                <div class="clear"></div>

                <div class="doYou">
                    Are there other important questions to ask the pro?
                    <a href="#" onclick="openAndSendmail('<%#ExpertiseName%>');">HELP US IMPROVE</a>                    
                    <div class="doYouImage">

                    </div>
                </div>                

                <div class="searchAgain">
                    <a href="SearchProvider.aspx" >NEW SEARCH</a>
                </div>
            </div>
        </div>

        <div class="noOneAnswered" style="display:none">            

            <div class="mainTitle">
                <div class="mainTitle1">Darn it!</div>
                <div class="mainTitle2">No one answered.</div>            
            </div>

            <div class="sorry">
                    Sorry about that.
                    <br />
                    Please try it later.
                </div>

                <div class="containerBtn">
                    <div class="btn" onclick="closeNoOneAnswers();">OK</div>
             </div>
        </div>

      
        <div style="display:none">
            <input type="button" onclick="tips();" value="tips"/> 

            <input type="button" onclick="matchingAvailable();getCaseDataForDesktopApp();" value="Matching available"/>
        
            <input type="button" onclick="foundServiceProvider();" value="Found service provider"/>         
       
            <input type="button" onclick="providerCalling1();" value="Provider calling - 1"/>
            <input type="button" onclick="providerCalling1Tips();" value="Provider calling - 1 tips"/>

            <input type="button" onclick="callInSession1();" value="Call in session - 1"/>
       
            <input type="button" onclick="thankYou();" value="Thank you"/>
            <input type="button" onclick="suppliersMatches(1);" value="number matches"/>
            <input type="button" onclick="setSupplierDetailsHardCode();" value="set Supplier Details"/>
            <input type="button" onclick="showReviews(true);" value="show reviews"/>
            <input type="button" onclick="showReviews(false);" value="hide reviews"/>

            <input type="button" onclick="var obj=new Object();obj.SupplierId='fd422d1c-9049-4f72-bb00-ddf528936646';callingCustomer(obj);" value="calling Customer"/>
            <input type="button" onclick="var obj=new Object();obj.SupplierId='fd422d1c-9049-4f72-bb00-ddf528936646';customerConnected(obj);" value="customer Connected"/>
            <input type="button" onclick="var obj=new Object();obj.HasMoreSuppliers=true;callEnded(obj);" value="call end. there is more suppliers"/>
            <input type="button" onclick="var obj=new Object();obj.HasMoreSuppliers=false;callEnded(obj);" value="call end. there is'nt more suppliers"/>
            <input type="button" onclick="headingTips2.resetTips();" value="reset tips"/>
        
            <input type="button" onclick="noOneAnswers();" value="no one answers"/>
            <input type="button" onclick="menuCurrentSearch();" value="menu current search"/>
            <input type="button" onclick="menuCurrentInSession();" value="menu current in session"/>
            <input type="button" onclick="menuCurrentThanks();" value="menu current thanks"/>
            <input type="button" onclick="setCurrentSupplierArrow(0);" value="set arrow supllier first"/>
            <input type="button" onclick="setCurrentSupplierArrow(1);setBeforeSupplierArrow(0);setBeforeSupplierArrowPhone(0);" value="set arrow supllier second"/>
            <input type="button" onclick="setCurrentSupplierArrow(2);setBeforeSupplierArrow(1);setBeforeSupplierArrowPhone(1);" value="set arrow supllier third"/>

            <br />
            <input type="button" onclick="setCurrentSupplierArrowPhone(0);" value="set arrow connected first phone"/>
            <input type="button" onclick="setCurrentSupplierArrowPhone(1);" value="set arrow connected second phone"/>
            <input type="button" onclick="setCurrentSupplierArrowPhone(2);" value="set arrow connected third phone"/>
        </div>
     
    </div>
</asp:Content>
