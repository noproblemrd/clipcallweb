﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class deskApp_UserRegister : System.Web.UI.Page
{
    bool HasSearch = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            SliderData sd = new SliderData(Context, eAppType.DeskApp);
            HasSearch = !string.IsNullOrEmpty(sd.ExpertiseCode);
            txt_Mobile.Attributes.Add("place_holder", PhonePlaceHolder);
            txt_digits.Attributes.Add("place_holder", PINcode);
        }
        Header.DataBind();
    }
    protected string PhonePlaceHolder
    {
        get { return "My Phone number"; }
    }
    protected string PhoneRegExpression
    {
        get { return Utilities.RegularExpressionForJavascript(PpcSite.GetCurrent().PhoneReg); }
    }
    protected string PINcode
    {
        get { return "PIN code"; }
    }
    protected string _4digitsRegulareExpresion
    {
        get { return "^[0-9]{4}$"; }
    }
    protected string RegisterUser
    {
        get { return "DesktopAppWebService.asmx/RegisterConsumer"; }
    }
    protected string SendPinCode
    {
        get { return "DesktopAppWebService.asmx/RegisterConsumerPinCode"; }
    }
    protected string RedirectPage
    {
        get
        {
            if (HasSearch)
                return ResolveUrl("~/deskApp/SliderFrame.aspx?" + Request.QueryString.ToString());
            return ResolveUrl("~/deskApp/SearchProvider.aspx");
        }
    }
    
}