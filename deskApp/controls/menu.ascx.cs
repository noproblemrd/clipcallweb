﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class deskApp_controls_menu : System.Web.UI.UserControl
{
    // links
    
    protected const string findAPro = "<a href='SearchProvider.aspx'>Find a pro</a>";
    protected const string project = "Project";
    protected const string searching = "Searching";
    protected const string inSession = "In session";
    protected const string thanks = "Thanks";
    protected const string callRecording = "Call recording";
    protected const string connect = "Connect";
    protected const string myInfo = "My Info";
    protected const string signUp = "Sign up as a pro";
    protected const string byMore = "<a href='buyMore.aspx'>Buy more</a>";
    protected const string payment = "<span class='menu2_item_text_paymentLogo'></span>Payment";
    protected const string paymentCurrent = "<span class='menu2_item_text_paymentLogoCurrent'></span>Payment";

    protected void Page_Load(object sender, EventArgs e)
    {
        //if(!IsPostBack)
        //{
            setMenuStyle();
        //}

        Page.DataBind();
    }

    public void setMenuCurrentFindAPro()
    {
        List<DeskAppMenu> deskAppMenuArr;        

        deskAppMenuArr = new List<DeskAppMenu>();
        DeskAppMenu deskAppMenu1 = new DeskAppMenu(findAPro, true, true);
        deskAppMenuArr.Add(deskAppMenu1);
        DeskAppMenu deskAppMenu2 = new DeskAppMenu(project, false, false);
        deskAppMenuArr.Add(deskAppMenu2);
        DeskAppMenu deskAppMenu3 = new DeskAppMenu(searching, false, true);
        deskAppMenuArr.Add(deskAppMenu3);
        DeskAppMenu deskAppMenu4 = new DeskAppMenu(inSession, false, false);
        deskAppMenuArr.Add(deskAppMenu4);
        DeskAppMenu deskAppMenu5 = new DeskAppMenu(thanks, false, false);
        deskAppMenuArr.Add(deskAppMenu5);

        setMenu2BoldNotBold(deskAppMenuArr);
    }

    public void setMenuCurrentSearcing()
    {
        List<DeskAppMenu> deskAppMenuArr;

        deskAppMenuArr = new List<DeskAppMenu>();
        DeskAppMenu deskAppMenu1 = new DeskAppMenu(findAPro, true, false);
        deskAppMenuArr.Add(deskAppMenu1);
        DeskAppMenu deskAppMenu2 = new DeskAppMenu(project, true, false);
        deskAppMenuArr.Add(deskAppMenu2);
        DeskAppMenu deskAppMenu3 = new DeskAppMenu(searching, true, true);
        deskAppMenuArr.Add(deskAppMenu3);
        DeskAppMenu deskAppMenu4 = new DeskAppMenu(inSession, false, false);
        deskAppMenuArr.Add(deskAppMenu4);
        DeskAppMenu deskAppMenu5 = new DeskAppMenu(thanks, false, false);
        deskAppMenuArr.Add(deskAppMenu5);

        setMenu2BoldNotBold(deskAppMenuArr);
    }

    public void setMenuCurrentRequest()
    {
        List<DeskAppMenu> deskAppMenuArr;

        deskAppMenuArr = new List<DeskAppMenu>();
        DeskAppMenu deskAppMenu1 = new DeskAppMenu(findAPro, true, false);
        deskAppMenuArr.Add(deskAppMenu1);
        DeskAppMenu deskAppMenu2 = new DeskAppMenu(project, true, true);
        deskAppMenuArr.Add(deskAppMenu2);
        DeskAppMenu deskAppMenu3 = new DeskAppMenu(searching, false, false);
        deskAppMenuArr.Add(deskAppMenu3);
        DeskAppMenu deskAppMenu4 = new DeskAppMenu(inSession, false, false);
        deskAppMenuArr.Add(deskAppMenu4);
        DeskAppMenu deskAppMenu5 = new DeskAppMenu(thanks, false, false);
        deskAppMenuArr.Add(deskAppMenu5);

        setMenu2BoldNotBold(deskAppMenuArr);
    }

    public void setMenuRecorderCurrentCallRecording()
    {
        List<DeskAppMenu> deskAppMenuArr;

        deskAppMenuArr = new List<DeskAppMenu>();
        DeskAppMenu deskAppMenu1 = new DeskAppMenu(callRecording, true, true);
        deskAppMenuArr.Add(deskAppMenu1);
        DeskAppMenu deskAppMenu2 = new DeskAppMenu(connect, false, false);
        deskAppMenuArr.Add(deskAppMenu2);        
        DeskAppMenu deskAppMenu3 = new DeskAppMenu(inSession, false, false);
        deskAppMenuArr.Add(deskAppMenu3);
        DeskAppMenu deskAppMenu4 = new DeskAppMenu(thanks, false, false);
        deskAppMenuArr.Add(deskAppMenu4);

        setMenu2BoldNotBold(deskAppMenuArr);
    }

    public void setMenuCurrentMyInfo()
    {
        List<DeskAppMenu> deskAppMenuArr;

        deskAppMenuArr = new List<DeskAppMenu>();
        DeskAppMenu deskAppMenu1 = new DeskAppMenu(myInfo, true, true);
        deskAppMenuArr.Add(deskAppMenu1);        

        setMenu2BoldNotBold(deskAppMenuArr);
    }

    public void setMenuCurrentBecomeProvider()
    {
        List<DeskAppMenu> deskAppMenuArr;

        deskAppMenuArr = new List<DeskAppMenu>();
        DeskAppMenu deskAppMenu1 = new DeskAppMenu(signUp, true, true);
        deskAppMenuArr.Add(deskAppMenu1);

        setMenu2BoldNotBold(deskAppMenuArr);
    }


    public void setMenuCurrentBuy()
    {
        List<DeskAppMenu> deskAppMenuArr;

        deskAppMenuArr = new List<DeskAppMenu>();
        DeskAppMenu deskAppMenu1 = new DeskAppMenu(byMore, true, true);
        deskAppMenuArr.Add(deskAppMenu1);
        DeskAppMenu deskAppMenu2 = new DeskAppMenu(payment, false, false);
        deskAppMenuArr.Add(deskAppMenu2);        

        setMenu2BoldNotBold(deskAppMenuArr);
    }

    public void setMenuCurrentPayment()
    {
        List<DeskAppMenu> deskAppMenuArr;

        deskAppMenuArr = new List<DeskAppMenu>();
        DeskAppMenu deskAppMenu1 = new DeskAppMenu(byMore, true, false);
        deskAppMenuArr.Add(deskAppMenu1);
        DeskAppMenu deskAppMenu2 = new DeskAppMenu(paymentCurrent, true, true);
        deskAppMenuArr.Add(deskAppMenu2);

        setMenu2BoldNotBold(deskAppMenuArr);
    }


    public void setMenuStyle()
    {
        string urlPage = this.Request.Url.AbsolutePath;
        FileInfo fileInfo = new FileInfo(urlPage);
        

        


        switch (fileInfo.Name.ToLower())
        {
            case "settings.aspx":
                menu_setting.Attributes.Add("class", menu_setting.Attributes["class"] + " menuItemActive");  
                
                break;
            case "searchprovider.aspx":
                menu_servicePro.Attributes.Add("class", menu_servicePro.Attributes["class"] + " menuItemActive");
                menu_servicePro_link.Attributes.Add("class", "active");
                menu_containerLink_settings.Attributes.Add("class", menu_containerLink_settings.Attributes["class"] + " menu_containerLink_blue");
                //setMenu(true, false); 
                addSlogan();
                setMenuCurrentFindAPro();
                
                              
                break;
            
            case "form.aspx":
                menu_servicePro.Attributes.Add("class", menu_servicePro.Attributes["class"] + " menuItemActive");
                menu_servicePro_link.Attributes.Add("class", "active");
                menu_containerLink_settings.Attributes.Add("class", menu_containerLink_settings.Attributes["class"] + " menu_containerLink_blue");
                //setMenu(true, false); 
                addSlogan();
                setMenu2("Find a pro","Form");
                break;

            case "sliderframe.aspx":
                menu_servicePro.Attributes.Add("class", menu_servicePro.Attributes["class"] + " menuItemActive");
                menu_servicePro_link.Attributes.Add("class", "active");
                menu_containerLink_settings.Attributes.Add("class", menu_containerLink_settings.Attributes["class"] + " menu_containerLink_blue");
                   
                addSlogan();

                setMenuCurrentRequest();

                break;
            case "matchproviders.aspx":
                menu_servicePro.Attributes.Add("class", menu_servicePro.Attributes["class"] + " menuItemActive");
                menu_servicePro_link.Attributes.Add("class", "active");
                menu_containerLink_settings.Attributes.Add("class", menu_containerLink_settings.Attributes["class"] + " menu_containerLink_blue");
                  
                addSlogan();

                setMenuCurrentRequest();

                break;

            case "mytime.aspx":
                menu_smartRecorder.Attributes.Add("class", menu_smartRecorder.Attributes["class"] + " menuItemActive");
                menu_smartRecorder_link.Attributes.Add("class", "active");
                menu_containerLink_servicePro.Attributes.Add("class", menu_containerLink_servicePro.Attributes["class"] + " menu_containerLink_blue");
                addTimer();
                setMenu2("My time");              
                break;

            case "smartrecorder.aspx":
                menu_smartRecorder.Attributes.Add("class", menu_smartRecorder.Attributes["class"] + " menuItemActive");
                menu_smartRecorder_link.Attributes.Add("class", "active");
                menu_containerLink_servicePro.Attributes.Add("class", menu_containerLink_servicePro.Attributes["class"] + " menu_containerLink_blue");               
                addTimer();
                setMenuRecorderCurrentCallRecording();

                break;

            case "addtime.aspx":
                menu_smartRecorder.Attributes.Add("class", menu_smartRecorder.Attributes["class"] + " menuItemActive");
                menu_smartRecorder_link.Attributes.Add("class", "active");
                menu_containerLink_servicePro.Attributes.Add("class", menu_containerLink_servicePro.Attributes["class"] + " menu_containerLink_blue");
                addTimer();
                setMenuCurrentBuy();
                break;

            case "buymore.aspx":
                menu_smartRecorder.Attributes.Add("class", menu_smartRecorder.Attributes["class"] + " menuItemActive");
                menu_smartRecorder_link.Attributes.Add("class", "active");
                menu_containerLink_servicePro.Attributes.Add("class", menu_containerLink_servicePro.Attributes["class"] + " menu_containerLink_blue");
                addTimer();
                setMenuCurrentBuy();
                break;
            case "payment.aspx":
                menu_smartRecorder.Attributes.Add("class", menu_smartRecorder.Attributes["class"] + " menuItemActive");
                menu_smartRecorder_link.Attributes.Add("class", "active");
                menu_containerLink_servicePro.Attributes.Add("class", menu_containerLink_servicePro.Attributes["class"] + " menu_containerLink_blue");
                addTimer();
                setMenuCurrentPayment();
                break;

            case "continuerecording.aspx":
                menu_smartRecorder.Attributes.Add("class", menu_smartRecorder.Attributes["class"] + " menuItemActive");
                menu_smartRecorder_link.Attributes.Add("class", "active");
                menu_containerLink_servicePro.Attributes.Add("class", menu_containerLink_servicePro.Attributes["class"] + " menu_containerLink_blue");               

                //setMenu(false, true);
                setMenu2("New Call","Your call");
                break;
                

            case "becomeprovider.aspx":
                menu_becomeProvider.Attributes.Add("class", menu_becomeProvider.Attributes["class"] + " menuItemActive");
                menu_becomeProvider_link.Attributes.Add("class", "active");
                menu_containerLink_smartRecordero.Attributes.Add("class", menu_containerLink_smartRecordero.Attributes["class"] + " menu_containerLink_blue");

                addSlogan2();
                //setMenu(true, false);
                setMenuCurrentBecomeProvider();               
                break;

            
                
            case "changemynumber.aspx":
                
            case "recordcall.aspx":
                menu_setting.Attributes.Add("class", menu_setting.Attributes["class"] + " menuItemActive");
                //menu_settings_link.Attributes.Add("class", "active");
                break;

            case "myinfo.aspx":
                menu_setting.Attributes.Add("class", menu_setting.Attributes["class"] + " menuItemActive");
                //menu_settings_link.Attributes.Add("class", "active");
                addSlogan();
                setMenuCurrentMyInfo();
                break;  

            default:
                menu_setting.Attributes.Add("class", "menuItem menuItemActive");
                //menu_settings_link.Attributes.Add("class", "active");
                
                break;

        }
    }


    

    protected void setMenu(bool ifCallFree, bool ifTimer)
    {
        /*
        menu_callFree.Visible = ifCallFree;
        menu_Timer.Visible = ifTimer;
        */
    }

    public void setMenu2(params string[] items)
    {
        for(int i=0;i<items.Length;i++)
        {
            HtmlGenericControl menu2_item = (HtmlGenericControl)menu2.FindControl("menu2_item_" + (i + 1) );
            menu2_item.Attributes.Add("style", "display:block");

            HtmlGenericControl menu2_item_1_text = (HtmlGenericControl)menu2.FindControl("menu2_item_" + (i + 1) + "_text");

            if (menu2_item_1_text!=null)
                menu2_item_1_text.InnerHtml = items[i];
            
        }
    }

    public void setMenu2BoldNotBold(List<DeskAppMenu> deskAppMenuArr)
    {
        for (int i = 0; i < deskAppMenuArr.Count;i++ )
        {
            HtmlGenericControl menu2_item = (HtmlGenericControl)menu2.FindControl("menu2_item_" + (i + 1));
            menu2_item.Attributes.Add("style", "display:block");

            HtmlGenericControl menu2_item_1_text = (HtmlGenericControl)menu2.FindControl("menu2_item_" + (i + 1) + "_text");
            HtmlGenericControl menu2_item_1_arrow = (HtmlGenericControl)menu2.FindControl("menu2_item_" + (i + 1) + "_arrow");

            if (menu2_item_1_text != null)
            {
                menu2_item_1_text.InnerHtml = deskAppMenuArr[i].value;

                if(deskAppMenuArr[i].already)
                {
                    //menu2_item_1_text.Attributes.Add("style", "color:black");
                    menu2_item_1_text.Attributes.Add("class", menu2_item_1_text.Attributes["class"] + " menu2_item_text_already");
                    menu2_item_1_arrow.Attributes.Add("class", menu2_item_1_arrow.Attributes["class"] + " menuArrowAllready");
                    
                    if(deskAppMenuArr[i].current)
                    {
                        menu2_item_1_text.Attributes.Add("class", menu2_item_1_text.Attributes["class"] + " menu2_item_text_current");
                        menu2_item_1_arrow.Attributes.Add("class", menu2_item_1_arrow.Attributes["class"] + " menuArrowCurrent");
                    }
                                                
                    
                }               

            }
                


        }
    }

    public void addTimer()
    {
        deskApp_controls_timer ctlTimer = (deskApp_controls_timer)Page.LoadControl("~/deskApp/controls/timer.ascx");
        menu_dynamicImage.Controls.Add(ctlTimer);
    }

    private void addSlogan()
    {
        deskApp_controls_ribbon ctlRibbon = (deskApp_controls_ribbon)Page.LoadControl("~/deskApp/controls/ribbon.ascx");
        menu_dynamicImage.Controls.Add(ctlRibbon);
    }

    private void addSlogan2()
    {
 //       deskApp_controls_ribbon2 ctlRibbon = (deskApp_controls_ribbon2)Page.LoadControl("~/deskApp/controls/ribbon2.ascx");
 //       menu_dynamicImage.Controls.Add(ctlRibbon);
    }

    public class DeskAppMenu
    {
        public string value { get; set; }
        public bool already { get; set; }
        public bool current { get; set; }

        public DeskAppMenu()
        {

        }

        public DeskAppMenu(string value, bool already, bool current)
        {
            this.value = value;
            this.already = already;
            this.current = current;
        }

        
    }
}