﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="timer.ascx.cs" Inherits="deskApp_controls_timer" %>

<div class="container" onclick="javascript:addTime();">
    <div id="progress" style="height: 60px;
    margin-bottom: 10px;
    margin-left: 14px;
    margin-top: 2px;
    width: 60px;"></div>
    <!--
    <input type="button" id="start" value="Start Timer"/>
    <input type="button" id="stop" value="Start Timer"/> 
    -->   
    <div class="addTime" ></div>      
</div>

<script type="text/javascript">
    /*
    var ContactId;
    if (typeof noProblemDesktopBinder != "undefined") {
        ContactId = noProblemDesktopBinder.getContactId();
        //alert(ContactId);
    }
    */
var maxMinutes = 30;
var maxSeconds = maxMinutes*60; // must be equel to maxMinutes
var remaintMinutes = 25; // in the initizlize
var remaining; // in runTime
var timer = null;
var startTime = null;
var progress = null;
var blnRemain5min = false;
var countRemain5min = 0;

function StartProgress() {
    //alert("StartProgress");
    //alert(Date.now() - startTime);
    //var remaining = 60 - (Date.now() - startTime) / 1000; // seconds
    startTime = Date.now();
    timer = setInterval(_progressing, 1000);
    
}

function _progressing() {
    remaining = remaintMinutes - (((Date.now() - startTime) / 1000.0) / 60.0); // convert seconds to minutes

    if (remaining < 5 )
    {        
        //remain5Minutes();
        blnRemain5min = true;

        if (stepCalling == "OtherIsAnswered" && countRemain5min==0)
        {            
            connectBeingRecorded();
            countRemain5min = 1;
        }
            
    }
     
    else
        blnRemain5min = false;

    //alert("_progressing:" + remaining);
    progress.value(remaining);
    $('.timerTextDigit').text(minTommss(remaining)); // my addition to make the decimal number to timer

    if (remaining <= 0) {
        clearInterval(timer);
    }
}
function SetProgress(_min) {
    //alert("SetProgress:" + _min);
    remaintMinutes = _min;
    progress.value(_min);
    //alert(minTommss(_min));
    $('.timerTextDigit').html(minTommss(_min)); // my addition to make the decimal number to timer
}

function StopProgress() {
    //alert("stop");
    //alert(timer);
    clearInterval(timer);
    ////SetSecondRecordLeft();
}

function addTime() {
    location.href = "buyMore.aspx";
}

function minTommss(minutes) {
    var sign = minutes < 0 ? "-" : "";
    var min = Math.floor(Math.abs(minutes))
    var sec = Math.floor((Math.abs(minutes) * 60) % 60);
    return sign + (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
}


function initCircleProgressBar()
{
    progress = $("#progress").shieldProgressBar({
        min: 0,
        max: maxMinutes,
        value: remaintMinutes,
        layout: "circular",
        layoutOptions: {
            circular: {
                width: 5,
                borderWidth: 1,
                borderColor: "transparent",
                color: "#EBEFF2",
                colorDisabled: "#C4C4C4",
                backgroundColor: "#7998B4"
            }
        },
        text: {
            enabled: true,
            template: '<div class="timerTextDigit" >{0:n1}</div> <div class="timerTextMinute">M</div> <div class="clear"></div>'
            //template: '<div class="timerTextDigit" >' + minTommss(remaining) + '</div> <div class="timerTextMinute">M</div> <div class="clear"></div>'
            //template: '<div class="timerTextDigit" >' + progress .value() + '</div> <div class="timerTextMinute">M</div> <div class="clear"></div>'
        },
        reversed: true
    }).swidget();
}

function SetSecondRecordLeft() {
    //alert("SetSecondRecordLeft");
    if (!ContactId || ContactId.length == 0)
        return;
    $.ajax({
        url: "<%# GetSecondRecordLeft %>",
        data: "{ 'ContactId': '" + ContactId + "'}",
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        //         dataFilter: function (data) { return data; },
        success: function (data) {
            var _data = eval('(' + data.d + ')');
            //alert(_data.status);
            switch (_data.status) {
                case ('Success'):
                    var _min = parseFloat(_data.duration) / 60.0;
                    if (_min > 30)
                        maxMinutes = _min;
                    else
                        maxMinutes = 30;

                    initCircleProgressBar();                   

                    SetProgress(_min);
                    //SetProgress("240");
                    break;
                case ('ServerFaild'):
                    break;
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ' ' + errorThrown);
        },
        complete: function (jqXHR, textStatus) {
            //  HasInHeadingRequest = false;
            //       noProblemDesktopBinder.disconnectFromPusher(ContactId);
        }

    });
}
jQuery(function ($) {        
       

    if (typeof noProblemDesktopBinder != "undefined")
    {        
        SetSecondRecordLeft();        
        
    }
            
        else // for developer use
        {
            maxMinutes = 30;
            initCircleProgressBar();
        }

    /*
    layoutOptions: {
            circular: {
                width: 20,
                color: "#197BB5",
                colorDisabled: "#C4C4C4",
                borderColor: "#C4C4C4",
                borderWidth: 1,
                backgroundColor: "white"
            }
        }

    */

    /*
    $("#start").shieldButton({
        events: {
            click: function () {
                clearInterval(timer);
                startTime = Date.now();
                //timer = setInterval(updateProgress, 100);
                timer = setInterval(updateProgress, 100);
            }
        }
    });
    $("#stop").shieldButton({
        events: {
            click: function () {
                clearInterval(timer);
            }
        }
    });

        */
    
        

    /*
    function updateProgress() {
        var remaining = 60 - (Date.now() - startTime) / 1000*60;
        progress.value(remaining);
        if (remaining <= 0) {
            clearInterval(timer);
        }
    }
    */
    
});
</script>

     
