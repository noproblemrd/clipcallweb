﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="menu.ascx.cs" Inherits="deskApp_controls_menu"  ClientIDMode="Static"%>
<%@ Register Src="~/deskApp/controls/timer.ascx" TagPrefix="uc1" TagName="timer" %>
<%@ Register Src="~/deskApp/controls/ribbon.ascx" TagPrefix="uc1" TagName="ribbon" %>


<script>

    /*
    public void setMenu2(params string[] items)
    {
        for(int i=0;i<items.Length;i++)
    {
        HtmlGenericControl menu2_item = (HtmlGenericControl)menu2.FindControl("menu2_item_" + (i + 1) );
        menu2_item.Attributes.Add("style", "display:block");

        HtmlGenericControl menu2_item_1_text = (HtmlGenericControl)menu2.FindControl("menu2_item_" + (i + 1) + "_text");

        if (menu2_item_1_text!=null)
            menu2_item_1_text.InnerHtml = items[i];
            
    }
    }
    */

    function setMenu2()
    {
       
        for (var i = 0; i < arguments.length; i++) {

            alert(arguments[i]);
            
            var $menu2_item = $('#menu2').find('#menu2_item_' + (i + 1));
            $menu2_item.css('display', 'block');

            var $menu2_item_text = $menu2_item.find("#menu2_item_" + (i + 1) + "_text");

            if ($menu2_item_text)
                $menu2_item_text.html(arguments[i]);
          
        }       

        
    }

    function setMenu2BoldNotBold(arrMenu)
    {
        for (var i = 0; i < arrMenu.length;i++)
        {
            var $menu2_item = $('#menu2').find('#menu2_item_' + (i + 1));
            $menu2_item.css('display', 'block');

            var $menu2_item_text = $menu2_item.find("#menu2_item_" + (i + 1) + "_text");
            var $menu2_item_arrow = $menu2_item.find("#menu2_item_" + (i + 1) + "_arrow");

            if ($menu2_item_text)
            {
                $menu2_item_text.html(arrMenu[i].value);

                if (arrMenu[i].already)
                {
                    $menu2_item_text.addClass("menu2_item_text_already");
                    $menu2_item_arrow.addClass("menuArrowAllready");

                    if(arrMenu[i].current)
                    {
                        $menu2_item_text.addClass("menu2_item_text_current");
                        $menu2_item_arrow.addClass("menuArrowCurrent");
                    }

                    else
                    {
                        $menu2_item_text.removeClass("menu2_item_text_current");
                        $menu2_item_arrow.removeClass("menuArrowCurrent");
                    }
                   
                }
                    
                else
                {
                    $menu2_item_text.removeClass("menu2_item_text_already");
                    $menu2_item_text.removeClass("menu2_item_text_current");
                    $menu2_item_arrow.removeClass("menuArrowCurrent");
                    $menu2_item_arrow.removeClass("menuArrowAllready");
                }
                    
            }
                
        }
    }

    /***************** class menu and relarted functions ************************/

    function Menu(value, already, current) {
        this.value = value;
        this.already = already;
        this.current = current;
    }

    var findAPro = "<%#findAPro%>";
    var project = "<%#project%>";
    var search = "<%#searching%>";
    var inSession = "<%#inSession%>";
    var thanks = "<%#thanks%>";    
    var callRecording = "<%#callRecording%>";
    var connect = "<%#connect%>";

    function menuCurrentSearch() {
        var arrMenu = new Array();

        var menu1 = new Menu();
        menu1.value = findAPro;
        menu1.already = true;
        menu1.current = false;

        arrMenu.push(menu1);

        var menu2 = new Menu();
        menu2.value = project;
        menu2.already = true;
        menu2.current = false;

        arrMenu.push(menu2);

        var menu3 = new Menu();
        menu3.value = search;
        menu3.already = true;
        menu3.current = true;

        arrMenu.push(menu3);

        var menu4 = new Menu();
        menu4.value = inSession;
        menu4.already = false;
        menu4.current = false;

        arrMenu.push(menu4);

        var menu5 = new Menu();
        menu5.value = thanks;
        menu5.already = false;
        menu5.current = false;

        arrMenu.push(menu5);

        setMenu2BoldNotBold(arrMenu);
    }

    function menuCurrentInSession() {
        var arrMenu = new Array();

        var menu1 = new Menu();
        menu1.value = findAPro;
        menu1.already = true;
        menu1.current = false;

        arrMenu.push(menu1);

        var menu2 = new Menu();
        menu2.value = project;
        menu2.already = true;
        menu2.current = false;

        arrMenu.push(menu2);

        var menu3 = new Menu();
        menu3.value = search;
        menu3.already = true;
        menu3.current = false;

        arrMenu.push(menu3);

        var menu4 = new Menu();
        menu4.value = inSession;
        menu4.already = true;
        menu4.current = true;

        arrMenu.push(menu4);

        var menu5 = new Menu();
        menu5.value = thanks;
        menu5.already = false;
        menu5.current = false;

        arrMenu.push(menu5);

        setMenu2BoldNotBold(arrMenu);
    }

    function menuCurrentThanks() {
        var arrMenu = new Array();

        var menu1 = new Menu();
        menu1.value = findAPro;
        menu1.already = true;
        menu1.current = false;

        arrMenu.push(menu1);

        var menu2 = new Menu();
        menu2.value = project;
        menu2.already = true;
        menu2.current = false;

        arrMenu.push(menu2);

        var menu3 = new Menu();
        menu3.value = search;
        menu3.already = true;
        menu3.current = false;

        arrMenu.push(menu3);

        var menu4 = new Menu();
        menu4.value = inSession;
        menu4.already = true;
        menu4.current = false;

        arrMenu.push(menu4);

        var menu5 = new Menu();
        menu5.value = thanks;
        menu5.already = true;
        menu5.current = true;

        arrMenu.push(menu5);

        setMenu2BoldNotBold(arrMenu);
    }


    function menuRecorderCurrentCallRecording() {
        var arrMenu = new Array();

        var menu1 = new Menu();
        menu1.value = callRecording;
        menu1.already = true;
        menu1.current = false;

        arrMenu.push(menu1);

        var menu2 = new Menu();
        menu2.value = connect;
        menu2.already = true;
        menu2.current = true;

        arrMenu.push(menu2);      

        var menu3 = new Menu();
        menu3.value = inSession;
        menu3.already = false;
        menu3.current = false;

        arrMenu.push(menu3);

        var menu4 = new Menu();
        menu4.value = thanks;
        menu4.already = false;
        menu4.current = false;

        arrMenu.push(menu4);

        setMenu2BoldNotBold(arrMenu);
    }

    function menuRecorderCurrentBeingRecorded() {
        var arrMenu = new Array();

        var menu1 = new Menu();
        menu1.value = callRecording;
        menu1.already = true;
        menu1.current = false;

        arrMenu.push(menu1);

        var menu2 = new Menu();
        menu2.value = connect;
        menu2.already = true;
        menu2.current = false;

        arrMenu.push(menu2);

        var menu3 = new Menu();
        menu3.value = inSession;
        menu3.already = true;
        menu3.current = true;

        arrMenu.push(menu3);

        var menu4 = new Menu();
        menu4.value = thanks;
        menu4.already = false;
        menu4.current = false;

        arrMenu.push(menu4);

        setMenu2BoldNotBold(arrMenu);
    }

    function menuRecorderCurrentThanks() {
        var arrMenu = new Array();

        var menu1 = new Menu();
        menu1.value = callRecording;
        menu1.already = true;
        menu1.current = false;

        arrMenu.push(menu1);

        var menu2 = new Menu();
        menu2.value = connect;
        menu2.already = true;
        menu2.current = false;

        arrMenu.push(menu2);

        var menu3 = new Menu();
        menu3.value = inSession;
        menu3.already = true;
        menu3.current = false;

        arrMenu.push(menu3);

        var menu4 = new Menu();
        menu4.value = thanks;
        menu4.already = true;
        menu4.current = true;

        arrMenu.push(menu4);

        setMenu2BoldNotBold(arrMenu);
    }

    var gender="male";
    var genderOther = "female";

    function setGender()
    {

        var ifSmartRecorderPage;
        ifSmartRecorderPage = false;
        
        if (location.href.toLowerCase().indexOf("smartrecorder.aspx")!=-1)
            ifSmartRecorderPage = true;           


        if (gender == "male")
        {            
            gender = "female";
            $('.menu_settings_gender').css("background-position", "-122px -685px");

            if (ifSmartRecorderPage)            
                $('.you').css("background-position", "-190px -685px");
            
        }

        else
        {
            gender = "male";
            $('.menu_settings_gender').css("background-position", "-241px -1223px");

            if (ifSmartRecorderPage)
                $('.you').css("background-position", "-283px -1223px");
        }
    }


    function setGenderOther()
    {
        if(genderOther=="male")
        {
            genderOther = "female";
            $('.other').css("background-position", "-190px -685px");
        }

        else
        {
            genderOther = "male";
            $('.other').css("background-position", "-283px -1223px");
        }
    }

</script>

<div class="menu">
    <div id="menu_setting" runat="server" class="menuItem menu_setting">
        <div id="menu_containerLink_settings" runat="server" class="menu_containerLink" >           
            <div class="menu_settings_gender" onclick="setGender();" title="Click to change your avatar"></div>
        </div>       
    </div>
    <div id="menu_servicePro" runat="server" class="menuItem menu_servicePro">
        <div id="menu_containerLink_servicePro" runat="server" class="menu_containerLink" >
            <a id="menu_servicePro_link" runat="server" href="~/deskApp/SearchProvider.aspx">Find a pro</a>
        </div>
    </div>
    <div id="menu_smartRecorder" runat="server" class="menuItem menu_smartRecorder">
        <div id="menu_containerLink_smartRecordero" runat="server" class="menu_containerLink" >
            <a id="menu_smartRecorder_link" runat="server" href="~/deskApp/smartRecorder.aspx">FREE call recorder</a>
        </div>
    </div>
    <div id="menu_becomeProvider" runat="server" class="menuItem menu_becomeProvider">
        <div id="menu_containerLink_becomeProvider" runat="server" class="menu_containerLink" >
            <a id="menu_becomeProvider_link" runat="server" href="~/deskApp/becomeProvider.aspx">Sign up as a pro</a>
        </div>
        
    </div>
    <div id="menu_dynamicImage" runat="server" class="menuItem menu_dynamicImage">
        &nbsp;
    </div>

    <!--
    <div id="menu_callFree" runat="server"  class="menu_callFree" visible="false">free call image</div>
    <div id="menu_Timer" runat="server"  class="menu_Timer" visible="false">timer image</div>
    -->
</div>

<div class="clear"></div>

<!--
 <div class="menuSub">
    <div class="menuSubItem menu_myTime" ><a href="myTime.aspx">My time</a></div>
    <div class="menuSubItemStripe"></div>
    <div class="menuSubItem menu_changemyNumber"><a href="changeMyNumber.aspx">Change my number</a></div>
    <div class="menuSubItemStripe"></div>
    <div class="menuSubItem menu_RecordCall"><a href="recordCall.aspx">Record call</a></div>
 </div>
-->

<div id="menu2" class="menu2" runat="server">
    <div runat="server" id="menu2_item_1" class="menuItem2 menuItem2_1">
        <div runat="server" id="menu2_item_1_text" class="menu2_item_text"></div>
        <div runat="server" id="menu2_item_1_arrow" class="menuArrow menuArrow_1" ></div>
    </div>
    <div runat="server" id="menu2_item_2" class="menuItem2 menuItem2_2">
        <div runat="server" id="menu2_item_2_text" class="menu2_item_text"></div>
        <div runat="server" id="menu2_item_2_arrow" class="menuArrow menuArrow_2"></div>
    </div>
    <div runat="server" id="menu2_item_3" class="menuItem2 menuItem2_3">
        <div runat="server" id="menu2_item_3_text" class="menu2_item_text"></div>
        <div runat="server" id="menu2_item_3_arrow" class="menuArrow menuArrow_3"></div>
    </div>
    <div runat="server" id="menu2_item_4" class="menuItem2 menuItem2_4">
        <div runat="server" id="menu2_item_4_text" class="menu2_item_text"></div>
        <div runat="server" id="menu2_item_4_arrow" class="menuArrow menuArrow_4"></div>
    </div>
    <div runat="server" id="menu2_item_5" class="menuItem2 menuItem2_5">
         <div runat="server" id="menu2_item_5_text" class="menu2_item_text"></div>
        <div runat="server" id="menu2_item_5_arrow" class="menuArrow menuArrow_5"></div>
    </div>
</div>

<div class="clear"></div>
