﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class deskApp_matchProviders : System.Web.UI.Page
{
    protected string root;
    protected string url;
    protected string googleAdrress;
    protected string ExpertiseCode;
    protected string ServiceRequestId;
    protected string ZipCode;
    protected string ExpertiseName="";
    protected int zoom;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        root = ResolveUrl("~");

        if (Request.Url.Host == "localhost")
        {
            url = "http://" + Request.Url.Host + ":" + Request.Url.Port + root;
        }

        else
        {
            url = "http://" + Request.Url.Host + root;
        }

        


        ServiceRequestId= Request.QueryString["ServiceRequestId"];

        if(string.IsNullOrEmpty(ServiceRequestId) && Request.Url.Host=="localhost")
        {
            //ServiceRequestId="4B3319A5-D0E5-42FD-B9B1-528E5937E3AA";
            ServiceRequestId="133D7C2D-8C3A-4F26-8B25-59E94347F131";
            
        }
            

        ExpertiseCode = Request.QueryString["ExpertiseCode"];
        ZipCode = Request.QueryString["ZipCode"];

        if (!string.IsNullOrEmpty(ZipCode))
        {
            googleAdrress = ZipCode;
            zoom = 11;
        }
            
        else
        {
            googleAdrress = "12201";
            zoom = 3;
        }
            

        //Response.Write("ServiceRequestId: " + ServiceRequestId);
        
        HeadingCode headingCode = PpcSite.GetCurrent().GetHeadingDetails(ExpertiseCode);

        if (headingCode!=null)
        { 
            ExpertiseName = (string.IsNullOrEmpty(headingCode.SingularName)) ? headingCode.heading : headingCode.SingularName;
            ExpertiseName = ExpertiseName.ToLower();
        }

        //Response.Write("<br>ExpertiseName:" + ExpertiseName);
        //Response.Write("<br>ZipCode:" + ZipCode);

        Page.DataBind();

        //supplierItem.InnerHtml = getYelpDatails("2122444011", "Divine Moving");
    }

    protected string getYelpDatails(string phone, string supplierName)
    {

        /*
          documentation http://www.yelp.com/developers/documentation/v1/phone_api#sampleResponse
        */

        /* important comments
        1. i use this service wich is by v1. the new one is v2.
        2. When will access to the API v1.0 be turned off?

            The Yelp v1.0 API is deprecated and it is strongly recommended that developers 
             not use it. There are no current plans to turn it off but using it is at your own 
        risk.

        3. What's the best way to match a specific business?

            Our phone number API matches a business's phone number to the data we have in our 
            database. This is the best way to find data on a specific business.


        4. At this juncture, we only offer the phone match API via the deprecated V1 
           service.
           best
           Yelp API Team
        */

        StringBuilder sbYelp = new StringBuilder();

        try
        {
            var request = WebRequest.Create("http://api.yelp.com/phone_search?phone=" + phone + "&ywsid=J4sSbZsr63xaLAYJPRqQ5Q");  // example with 1 business returned 

            //var request = WebRequest.Create("http://api.yelp.com/phone_search?phone=2122444011&ywsid=J4sSbZsr63xaLAYJPRqQ5Q"); // example with 2 business returned
            //var request = WebRequest.Create("http://api.yelp.com/phone_search?phone=2122931393&ywsid=J4sSbZsr63xaLAYJPRqQ5Q");  // example with 1 business returned          
            //var request = WebRequest.Create("http://api.yelp.com/phone_search?phone=0543388912&ywsid=J4sSbZsr63xaLAYJPRqQ5Q"); // example with irrelevant number                    

            string text;
            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
            }

            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();

            dynamic item = jss.Deserialize<object>(text);

            string status = item["message"]["text"];

            //Response.Write("<br>" + item["businesses"].Length);

            if (status == "OK" && item["businesses"] != null && item["businesses"].Length > 0)
            {

                string rating_img_url = item["businesses"][0]["rating_img_url"];
                string business_url = item["businesses"][0]["url"];
                int review_count = item["businesses"][0]["review_count"];
                string rating_img_url2 = item["businesses"][0]["reviews"][0]["rating_img_url"];
                string user_photo_url_small = item["businesses"][0]["reviews"][0]["user_photo_url_small"];
                string user_photo_url = item["businesses"][0]["reviews"][0]["user_photo_url"];
                string user_url = item["businesses"][0]["reviews"][0]["user_url"];
                string review_url = item["businesses"][0]["reviews"][0]["url"];
                string text_excerpt = item["businesses"][0]["reviews"][0]["text_excerpt"];
                string review_date = item["businesses"][0]["reviews"][0]["date"];
                string review_user_name = item["businesses"][0]["reviews"][0]["user_name"];

                sbYelp.Append("<div class='yelpContent0'>");
                sbYelp.Append("<div class='yelpPreview'>");

                sbYelp.Append("<div class='yelpOpener'>");
                sbYelp.Append("<div class='yelpOpenerObj' ></div><div class='yelpOpenerText'>Recent review for " + supplierName + "</div>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewCounts'>");

                if (review_count == 1)
                    sbYelp.Append("Based on <a href='" + business_url + "' target='_blank' >1 review</a>");
                else
                    sbYelp.Append("Based on <a href='" + business_url + "' target='_blank' >" + review_count + " reviews</a>");

                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewAvg'>");
                sbYelp.Append("<img src='" + rating_img_url + "'>");
                sbYelp.Append("</div>");

                sbYelp.Append("</div>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpContent'>");

                sbYelp.Append("<div class='yelpContent1'>");
                sbYelp.Append("<div class='yelpUserImage'>");
                sbYelp.Append("<img src='" + user_photo_url_small + "' width='40px' height='40px'>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewDetails'>");
                sbYelp.Append("<div class='yelpReviewStar'>");
                sbYelp.Append("<img src='" + rating_img_url2 + "'>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewDate'>");
                sbYelp.Append(review_date);
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpReviewUser'>");
                sbYelp.Append(review_user_name);
                sbYelp.Append("</div>");

                sbYelp.Append("</div>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpContent2'>");
                sbYelp.Append(text_excerpt + " <a href='" + review_url + "' target='_blank'>read more</a>");
                sbYelp.Append("</div>");

                sbYelp.Append("<div class='yelpContent3'>");
                sbYelp.Append("<img src='" + url + "consumer/images/reviewsFromYelpWHT.gif'>");
                sbYelp.Append("</div>");

                sbYelp.Append("</div>");
            }

            else
            {
                sbYelp.Append("&nbsp;");
            }

        }

        catch (Exception exc)
        {
            ////dbug_log.ExceptionLog(exc, _cache2.SiteId);
            sbYelp.Append("&nbsp;");
        }


        return sbYelp.ToString();


        /*
        
         * example of stracture
        phone:2122444011 mover in new york "Divine Moving"        
        
        {"message": {"text": "OK", "code": 0, "version": "1.1.1"},                 "businesses": 
        [{"rating_img_url": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/f1def11e4e79/ico/stars/v1/stars_5.png", 
        "country_code": "US", "id": "7U0CPySKcB9JNUqcsOWZuQ", "is_closed": false, "city": 
        "New York", "mobile_url": "http://m.yelp.com/biz/divine-moving-and-storage-ltd-
        new-york-3", "review_count": 1, "zip": "10022", "state": "NY", 
        "rating_img_url_small": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/c7623205d5cd/ico/stars/v1/stars_small_5.pn
        g", "address1": "845 3rd Ave", "address2": "6th Fl", "address3": "", "phone": 
        "2122444011", "state_code": "NY", "categories": [{"category_filter": "movers", 
        "search_url": "http://www.yelp.com/search?cflt=movers\u0026find_desc=
        \u0026find_loc=845+3rd+Ave%2C+New+York+10022", "name": "Movers"}], "photo_url": 
        "http://media4.fl.yelpcdn.com/bpthumb/PVcN75tT13Ic1LXZrWG1Bw/ms", "distance": 
        0.0, "name": "Divine Moving \u0026 Storage Ltd", "neighborhoods": [{"url": 
        "http://www.yelp.com/search?exclude_start=True\u0026find_desc=
        \u0026find_loc=Midtown+East%2C+Manhattan%2C+NY", "name": "Midtown East"}], "url": 
        "http://www.yelp.com/biz/divine-moving-and-storage-ltd-new-york-3", "country": 
        "USA", "avg_rating": 5.0, "nearby_url": "http://www.yelp.com/search?find_desc=
        \u0026find_loc=845+3rd+Ave%2C+New+York+10022", "reviews": 
        [{"rating_img_url_small": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/c7623205d5cd/ico/stars/v1/stars_small_5.pn
        g", "user_photo_url_small": 
        "http://media1.fl.yelpcdn.com/upthumb/ZwtU0FeNTeV2OEePBg3Nqg/ss", 
        "rating_img_url": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/f1def11e4e79/ico/stars/v1/stars_5.png", 
        "rating": 5, "user_url": "http://www.yelp.com/user_details?
        userid=ens7LiIE4Cs4RjW5Mz65Mw", "url": "http://www.yelp.com/biz/divine-moving-
        and-storage-ltd-new-york-3?hrid=gTg13yHduBos9yOrybuuuA", "mobile_uri": 
        "/biz/divine-moving-and-storage-ltd-new-york-3?full=True
        \u0026hrid=gTg13yHduBos9yOrybuuuA", "text_excerpt": "After three misfires with 
        three very shady moving companies, one of the passerby neighbors overhearing my 
        conversation with Jakop the doorman, chimed-in and...", "user_photo_url": 
        "http://media1.fl.yelpcdn.com/upthumb/ZwtU0FeNTeV2OEePBg3Nqg/ms", "date": "2014-
        03-09", "user_name": "George M.", "id": "gTg13yHduBos9yOrybuuuA"}], 
        "photo_url_small": 
        "http://media4.fl.yelpcdn.com/bpthumb/PVcN75tT13Ic1LXZrWG1Bw/ss"}, 
        {"rating_img_url": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/5ef3eb3cb162/ico/stars/v1/stars_3_half.png
        ", "country_code": "US", "id": "SwA8bsQ4WxOXi1z84QJhgQ", "is_closed": false, 
        "city": "New York", "mobile_url": "http://m.yelp.com/biz/divine-moving-and-
        storage-new-york-2", "review_count": 28, "zip": "10022", "state": "NY", 
        "rating_img_url_small": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/2e909d5d3536/ico/stars/v1/stars_small_3_ha
        lf.png", "address1": "845 3rd Ave", "address2": "6Fl", "address3": "", "phone": 
        "2122444011", "state_code": "NY", "categories": [{"category_filter": "movers", 
        "search_url": "http://www.yelp.com/search?cflt=movers\u0026find_desc=
        \u0026find_loc=845+3rd+Ave%2C+New+York+10022", "name": "Movers"}, 
        {"category_filter": "selfstorage", "search_url": "http://www.yelp.com/search?
        cflt=selfstorage\u0026find_desc=\u0026find_loc=845+3rd+Ave%2C+New+York+10022", 
        "name": "Self Storage"}], "photo_url": 
        "http://media4.fl.yelpcdn.com/bpthumb/C4x0I5v-WPzhSWUzcfY6EQ/ms", "distance": 
        0.0, "name": "Divine Moving \u0026 Storage", "neighborhoods": [{"url": 
        "http://www.yelp.com/search?exclude_start=True\u0026find_desc=
        \u0026find_loc=Midtown+East%2C+Manhattan%2C+NY", "name": "Midtown East"}], "url": 
        "http://www.yelp.com/biz/divine-moving-and-storage-new-york-2", "country": "USA", 
        "avg_rating": 3.5, "nearby_url": "http://www.yelp.com/search?find_desc=
        \u0026find_loc=845+3rd+Ave%2C+New+York+10022", "reviews": 
        [{"rating_img_url_small": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/c7623205d5cd/ico/stars/v1/stars_small_5.pn
        g", "user_photo_url_small": 
        "http://media1.fl.yelpcdn.com/upthumb/8HUCxwxBV3UCmNik_SQk-w/ss", 
        "rating_img_url": "http://s3-
        media1.fl.yelpcdn.com/assets/2/www/img/f1def11e4e79/ico/stars/v1/stars_5.png", 
        "rating": 5, "user_url": "http://www.yelp.com/user_details?
        userid=QIxQ4IvK8DsvmCsABMAzFA", "url": "http://www.yelp.com/biz/divine-moving-
        and-storage-new-york-2?hrid=CiI0yfcU3AEvAMd7cV3veg", "mobile_uri": "/biz/divine-
        moving-and-storage-new-york-2?full=True\u0026hrid=CiI0yfcU3AEvAMd7cV3veg", 
        "text_excerpt": "I have been with Divine Moving and Storage for 5 months now and 
        it was a great experience working with them. I moved to a small apartment last 
        February and...", "user_photo_url": 
        "http://media1.fl.yelpcdn.com/upthumb/8HUCxwxBV3UCmNik_SQk-w/ms", "date": "2014-
        08-13", "user_name": "Diane B.", "id": "CiI0yfcU3AEvAMd7cV3veg"}], 
        "photo_url_small": "http://media4.fl.yelpcdn.com/bpthumb/C4x0I5v-
        WPzhSWUzcfY6EQ/ss"}]}
        
        */


        /*
         Response Codes:

        This section outlines the response codes that can be expected from a "BusinessReviewSearch" operation.
        Response Code 	Response Message 	Description
        0 	OK 	Indicates the request completed without error.
        1 	Server error 	Indicates that a system error occurred and that the request was unable to be processed.
        2 	Invalid YWSID 	Returned when an invalid YWSID is supplied with the API request.
        3 	Missing YWSID 	Indicates that  the required YWSID parameter was not supplied with the request.
        4 	Exceed daily API request limit 	Returned when the number of requests executed for a particular YWSID on a particular day exceeds the specified limit.
        5 	API not available 	Indicates that the Yelp API is currently not available
        6 	Did not understand query 	Indicates an invalid API request was sent
        300 	Invalid phone number 	Indicates that the value of the "phone" parameter was not a valid 10-digit telephone number 
         
        */

    }

    protected void btn_email_Click(object sender, EventArgs e)
    {
        string email=txt_email.Text;
    }
}