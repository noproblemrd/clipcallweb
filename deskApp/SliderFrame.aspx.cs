﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class deskApp_SliderFrame : System.Web.UI.Page
{
    const int ADD_TO_WIDTH = 20;
    const int ADD_TO_HEIGHT = 115;
    const int MIN_WIDTH = 460;
    SliderData sd;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            sd = new SliderData(Context, eAppType.DeskApp);
            SliderViewData svd = SalesUtility.GetSliderViewData(sd);
            if (svd == null)
                return;
            sd.ControlName = svd.FlavorName;
            _iframe.Attributes["width"] = svd.width.ToString();
            _iframe.Attributes["height"] = svd.height.ToString();
     //       return
            _iframe.Attributes["src"] = sd.GetFullSliderPath(svd);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setSize", "setSize(" + svd.width + ", " + svd.height + ");", true);
            sd.InsertExposure();            
        }
        Header.DataBind();
        
    }
    protected string OriginMessage
    {
        get { return sd.GetServerDomainWithSchemaForMessage(Context); }
    }
    protected string AddToWidth
    {
        get { return ADD_TO_WIDTH.ToString(); }
    }
    protected string AddToHeight
    {
        get { return ADD_TO_HEIGHT.ToString(); }
    }
    protected string MinWidth
    {
        get { return MIN_WIDTH.ToString(); }
    }
}
/*
  Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("Keyword", keyword);
            dic.Add("ok", this.OriginalKeyword);
            dic.Add("ToolbarId", this.ToolbarId);
            dic.Add("OriginId", this.OriginId.ToString());
            dic.Add("ExpertiseCode", this.ExpertiseCode);
            if (!string.IsNullOrEmpty(this.ProductName))
                dic.Add("ProductName", this.ProductName);
            dic.Add("AppType", AppType.ToString());
            
*/