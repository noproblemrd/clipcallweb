﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SliderFrame.aspx.cs"  MasterPageFile="~/deskApp/MasterPageDeskUp.master" 
    Inherits="deskApp_SliderFrame" %>

<asp:Content  runat="server" ContentPlaceHolderID="head">
    <script src="//js.pusher.com/2.2/pusher.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var AddToWidth = new Number(<%# AddToWidth %>);
        var AddToHeight = new Number(<%# AddToHeight %>);
        var MinWidth = new Number(<%# MinWidth %>);
        var CurrentWidth;
        var CurrentHeigth;
        function GetSliderWidth(_width) {
            var WidthResult = _width + AddToWidth;
            return WidthResult > MinWidth ? WidthResult : MinWidth.valueOf();
        }
        function setSize(_width, _height){
            CurrentWidth = _width;
            CurrentHeigth = _height;

            if (typeof noProblemDesktopBinder!="undefined")
                noProblemDesktopBinder.setSize(GetSliderWidth(_width), (_height + AddToHeight));
        }
        if (window.addEventListener) {
            window.addEventListener("message",
                function (event) {
                    GetMessage(event);
                }
            , false);
        }
        else if (window.attachEvent) {
            window.attachEvent("onmessage",
                function (event) {
                    GetMessage(event);
                }
            );
        }
        function GetMessage(e) {

            if (e.origin != '<%# OriginMessage %>')
                return;
            var data = eval('(' + e.data + ')');
            if (data.selector == 'Resize' || data.selector == 'Resize3') {

            //    this.IframeRezise(data.width, data.heigth, data.IframeWidth, data.IframeHeigth, data.CssClass);
                var _iframe = document.getElementById('<%#_iframe.ClientID%>');
                _iframe.setAttribute("width", data.IframeWidth);
                _iframe.setAttribute("height", data.IframeHeigth);
                setSize(data.width, data.heigth);
               
                if (e.preventDefault)
                    e.preventDefault();
                // otherwise set the returnValue property of the original event to false (IE)
                e.returnValue = false;

            }
            else if (data.selector == "ResizeLight") {
                setSize(CurrentWidth, data.heigth);
                if (e.preventDefault)
                    e.preventDefault();
                // otherwise set the returnValue property of the original event to false (IE)
                e.returnValue = false;
            }
            else if (data.selector == "RemoveSlider") {
             //   this.CloseSlider();
                if (e.preventDefault)
                    e.preventDefault();
                // otherwise set the returnValue property of the original event to false (IE)
                e.returnValue = false;
            }
            else if (data.selector == "Title") {
                /*
                var _message;
                if (data.message.length == 0)
                    _message = 'COMPARE SERVICE PROVIDERS';
                else
                    _message = data.message.toUpperCase();
                    */
               
                if (e.preventDefault)
                    e.preventDefault();
                // otherwise set the returnValue property of the original event to false (IE)
                e.returnValue = false;

            }
           
            else if (data.selector == "TransparentBackground") {

                if (e.preventDefault)
                    e.preventDefault();
                // otherwise set the returnValue property of the original event to false (IE)
                e.returnValue = false;
            }
            else if (data.selector == "setFailedRequestMessage") {
                /*
                if (document.querySelector) {
                    if (document.querySelector('.__NP_backWhite') == null) {
                        this.TransparentBackground(data.phone);
                    }
                    document.querySelector('.__NP_backWhite').style.height = '292px';
                    document.querySelector('.__NP_backTitle').innerHTML = 'Thank you!';
                    var strbackTitle = "No " + data.expertisePlural + "  are currently<br>" +
                               "available. Your phone will ring once they<br>" +
                               "become available again. Expect a call<br>" +
                               "from: " + data.phone + ".";
                    var __NP_backContent = document.querySelector('.__NP_backContent');
                    __NP_backContent.innerHTML = '';
                    var _backContentTextFaild = document.createElement('div');
                    _backContentTextFaild.className = '__NP_backContentTextFailed';
                    _backContentTextFaild.innerHTML = strbackTitle;

                    __NP_backContent.appendChild(_backContentTextFaild);

                    var _backContentTitleServices = document.createElement('div');
                    _backContentTitleServices.className = '__NP_backContentTitleServices';
                    _backContentTitleServices.innerHTML = "Try related services:";

                    __NP_backContent.appendChild(_backContentTitleServices);
                    var elem = document.createElement('div');
                    elem.innerHTML = unescape(data.relatedheadings);
                    __NP_backContent.appendChild(elem);
                   
                }
                 */
            }
        };
    </script>
</asp:Content>
<asp:Content  runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div>
        <iframe runat="server" id="_iframe" scrolling="no" frameborder="0" allowTransparency="true" ></iframe>
        
    </div>
</asp:Content>