﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="myTime.aspx.cs" Inherits="deskApp_myTime"  MasterPageFile="~/deskApp/MasterPageDeskUp.master" ClientIDMode="Static"%>

<asp:Content  runat="server" ContentPlaceHolderID="head">
    <link rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/shieldui-all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/light/all.min.css" />
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

    <style>
        
    </style>
</asp:Content>

<asp:Content  runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
   <div class="myTime">
    <div class="mainTitle">Time Remaining</div>      

    <div class="myTimeContent">
        <div class="myTimeContentTimer">
            <div class="container">
                <div id="progress2" style="width: 80px; height: 80px; margin-bottom: 10px;"></div>
                <!--
                <input type="button" id="start" value="Start Timer"/>
                <input type="button" id="stop" value="Start Timer"/> 
                -->           
            </div>

            <script type="text/javascript">

            // based on http://demos.shieldui.com/web/progressbar/circular
        var maxMinutes = 30;
        var maxSeconds = maxMinutes*60; // must be equel to maxMinutes
        var remaintMinutes = 25; // reamian

        jQuery(function ($) {
            var timer = null,
                startTime = null,
                progress = $("#progress2").shieldProgressBar({
                    min: 0,
                    max: maxMinutes,
                    value: remaintMinutes,
                    layout: "circular",
                    layoutOptions: {
                        circular: {
                            width: 10,
                            borderWidth: 2,
                            borderColor: "#cccccc",
                            color: "#1E98E4",
                            colorDisabled: "#C4C4C4",
                            backgroundColor: "#000000"
                        }
                    },
                    text: {
                        enabled: true,
                        template: '<span style="font-size:10px;">{0:n1}</span> min'
                    },
                    reversed: true
                }).swidget();

            /*
            layoutOptions: {
                    circular: {
                        width: 20,
                        color: "#197BB5",
                        colorDisabled: "#C4C4C4",
                        borderColor: "#C4C4C4",
                        borderWidth: 1,
                        backgroundColor: "white"
                    }
                }

            */


            $("#start").shieldButton({
                events: {
                    click: function () {
                        clearInterval(timer);
                        startTime = Date.now();
                        //timer = setInterval(updateProgress, 100);
                        timer = setInterval(updateProgress, 100);
                    }
                }
            });
            $("#stop").shieldButton({
                events: {
                    click: function () {
                        clearInterval(timer);
                    }
                }
            });

        
            function updateProgress() {
                //alert(Date.now() - startTime);
                //var remaining = 60 - (Date.now() - startTime) / 1000; // seconds
                var remaining = (maxSeconds - ((Date.now() - startTime) / 1000)) / 60; // convert seconds to minutes
                //alert(remaining);
                progress.value(remaining);
                if (remaining <= 0) {
                    clearInterval(timer);
                }
            }
        

            /*
            function updateProgress() {
                var remaining = 60 - (Date.now() - startTime) / 1000*60;
                progress.value(remaining);
                if (remaining <= 0) {
                    clearInterval(timer);
                }
            }
            */
        });
        </script>

        </div>

        <div>
            <div>Used: 5 mins</div>
            <div>Remaining: 25 mins</div>
        </div>

        <div class="clear"></div>

    </div>

    <div>
        <input type="button"  value="Add time" class="btn"/>
    </div>

    </div>

</asp:Content>