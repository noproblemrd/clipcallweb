﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class deskApp_smartRecorder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Header.DataBind();
    }
    protected string GetCallRecordService
    {
        get { return ResolveUrl("~/deskApp/DesktopAppWebService.asmx/CallRecord"); }
    }
    
}