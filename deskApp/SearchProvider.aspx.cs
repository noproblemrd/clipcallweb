﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class deskApp_SearchProvider : Page
{
    const int Width = 452;
    const int Height = 635;
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        Header.DataBind();
    }    
    protected string GetHeadingServiceList
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/GetHeadingListIframe"); }
    }
    protected string GetIsHeadingByName
    {
        get { return ResolveUrl("~/PPC/WebServiceSales.asmx/GetHeadingCodeByNameKeyword"); }
    }
    protected string SliderFrame
    {
        get { return ResolveUrl("~/deskApp/SliderFrame.aspx?camefrom=3&"); }
    }
    protected string NoMatchCategory
    {
        get { return "No match category"; }
    }
    protected string WinWidth
    {
        get { return Width.ToString(); }
    }
    protected string WinHeight
    {
        get { return Height.ToString(); }
    }
}