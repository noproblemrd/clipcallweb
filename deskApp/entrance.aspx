﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="entrance.aspx.cs" Inherits="deskApp_entrance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
    <script type="text/javascript">        try { Typekit.load(); } catch (e) { }</script>
    <link href="StyleDeskApp.css" rel="stylesheet" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>    <script>
        $(document).ready(function()
        {
            
            $('.navigationBtn').mouseenter(function () {               
                var newTextLeft = parseInt($('.navigationBtnText').css('left')) + 4 + "px";                
                $('.navigationBtnText').css('left', newTextLeft);

                
                var newArrowLeft = parseInt($('.navigationBtnArrow').css('left')) + 4 + "px";
                $('.navigationBtnArrow').css('left', newArrowLeft);
                
            })

            $('.navigationBtn').mouseleave(function () {
                var newTextLeft = parseInt($('.navigationBtnText').css('left')) - 4 + "px";
                $('.navigationBtnText').css('left', newTextLeft);

                
                var newArrowLeft = parseInt($('.navigationBtnArrow').css('left')) - 4 + "px";
                $('.navigationBtnArrow').css('left', newArrowLeft);
                
            })
            
            $('.navigationItem').click(function ()
            {
                location.href = "tutorial.aspx";
            }
            )

            if (typeof noProblemDesktopBinder != "undefined") {               
                noProblemDesktopBinder.setSize(452, 635);
            }

        }
        )
        
    </script>
    <title></title>
</head>
<body class="entranceBody">
    <form id="form1" runat="server">
    <div class="entrance">
        <div class="entranceLogo">

        </div>

        <div class="content">

            <div class="backgroundNavigation">
                <div class="navigationItem navigationRecorder"></div>
                <div class="navigationItem navigationFindAPro"></div>
                <div class="navigationItem navigationAddTime"></div>
                <div class="navigationItem navigationSignUp"></div>

                <div class="navigationBtn">
                    <div class="navigationBtnText">
                       <a href="tutorial.aspx">LET'S GET<br />STARTED</a>                        
                    </div>
                    <div class="navigationBtnArrow"></div>
                </div>
            </div>

            <div class="navigationItem navigationRecorderStandAlone">
                <div class="navigationStandAloneIcon navigationRecorderStandAloneIcon"></div>
                <div class="navigationStandAloneMainText navigationRecorderStandAloneMainText"><a href="tutorial.aspx">FREE CALL <br /> RECORDER</a></div>
                <div class="navigationStandAloneSubMainText navigationRecorderStandAloneSubMainText"><a href="recordCall.aspx">Record & listen back <br />to your personal calls.</a> </div>
            </div>

            <div class="navigationItem navigationFindAProStandAlone">
                <div class="navigationStandAloneIcon navigationFindAProStandAloneIcon"></div>
                <div class="navigationStandAloneMainText navigationFindAProStandAloneMainText"><a href="tutorial.aspx">FIND A PRO</a></div>
                <div class="navigationStandAloneSubMainText navigationFindAProStandAloneSubMainText"><a href="recordCall.aspx">Hire the right professional <br />for your job.</a> </div>
            </div>

            <div class="navigationItem navigationSignUpStandAlone">
                <div class="navigationStandAloneIcon navigationSignUpStandAloneIcon"></div>
                <div class="navigationStandAloneMainText navigationSignUpStandAloneMainText"><a href="tutorial.aspx">SIGN UP AS A <br />SERVICE PRO</a></div>
                <div class="navigationStandAloneSubMainText navigationSignUpStandAloneSubMainText"><a href="tutorial.aspx">Speak to customers within<br />seconds of their online search.</a> </div>
            </div>

            <div class="navigationItem navigationByMoreStandAlone">
                <div class="navigationStandAloneIcon navigationByMoreStandAloneIcon"></div>
                <div class="navigationStandAloneMainText navigationByMoreStandAloneMainText"><a href="tutorial.aspx">BUY MORE TIME</a></div>
                <div class="navigationStandAloneSubMainText navigationByMoreStandAloneSubMainText"><a href="tutorial.aspx">Get social to earn <br />call recording minutes.</a> </div>
            </div>          
            

        </div>

    </div>
    </form>
</body>
</html>
