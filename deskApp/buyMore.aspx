﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="buyMore.aspx.cs" Inherits="deskApp_buyMore" MasterPageFile="~/deskApp/MasterPageDeskUp.master" %>
<asp:Content  runat="server" ContentPlaceHolderID="head">
    <link rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/shieldui-all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/light/all.min.css" />
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

    <script>

        var ContactId;

        if (typeof noProblemDesktopBinder != "undefined") {
            ContactId = noProblemDesktopBinder.getContactId();
            if (!ContactId || ContactId.length == 0)
                window.location.href = 'UserRegister.aspx';
            noProblemDesktopBinder.setSize(452, 635);
        }


        function setPrice(price, planName, duration)
        {
            location.href = "payment.aspx?planPrice=" + price + "&planName=" + planName + "&planDuration=" + duration;
        }
    </script>
</asp:Content>

<asp:Content  runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
   <div class="buyMore">
       <div class="innerBuyMore">
            <div class="mainTitle">BUY MORE RECORDING TIME</div>

            <div class="content">
                Choose your package
            </div>

            <div class="plans">
                <div class="planLeftContainer">
                    <div class="plan planLeft">
                        <div class="planTitle">BASIC</div>
                        <div class="planTerms">
                           10 mins
                            <br />
                           for $10
                        </div>
                        <div class="planSelect">
                            <a href="javascript:void(0);" onclick="setPrice(10,'BASIC' ,10);">SELECT</a>
                        </div>
                    </div>
                </div>
                <div class="planMiddleContainer">
                    <div class="plan planMiddle">
                        <div class="planTitle">PREMIUM</div>
                        <div class="planTerms">
                           20 mins
                            <br />
                           for $15
                        </div>
                        <div class="planSelect">
                            <a href="javascript:void(0);" onclick="setPrice(15,'PREMIUM' , 20);">SELECT</a>
                        </div>
                    </div>
                </div>
                <div class="planRightContainer">
                    <div class="plan planRight">
                          <div class="planTitle">BEST<br /> VALUE</div>
                        <div class="planTerms">
                           30 mins
                            <br />
                           for $20
                        </div>
                        <div class="planSelect">
                            <a href="javascript:void(0);" onclick="setPrice(20,'BEST VALUE', 30);">SELECT</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="myTimeComment">
                <!--
                Keep up open until 03/03/2015 +30 min FREE
                -->
            </div>
       </div>
  </div>
</asp:Content>
