﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserRegister.aspx.cs" Inherits="deskApp_UserRegister" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" href="StyleSheet.css" rel="stylesheet" />
    <title></title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../scripts/US-Phone.js"></script>
    <script type="text/javascript" src="../PlaceHolder.js"></script>
    <script type="text/javascript">
        var _phone = new np_phone('<%# PhoneRegExpression %>');
        _phone.init();
        _phone.OnKeyUpEmpty = function (txtId) {
            ClearPhoneValidate();
        };
        _phone.OnMatch = function (txtId) {
            ClearPhoneValidate();
            $('#div_MobileSmile').show();
            $('#<%# txt_Mobile.ClientID %>').addClass('textValid');
        };
        _phone.OnKeyUpOnTheWay = function (txtId) {
            ClearPhoneValidate();
        };
        _phone.OnFaild = function (txtId) {
            ClearPhoneValidate();
            $('#div_MobileComment').show();
            $('#div_MobileError').show();
            $('#<%# txt_Mobile.ClientID %>').addClass('textError');
        };
        _phone.OnClickFaild = function (txtId) {
            _phone.OnFaild(txtId);
        };
        _phone.OnEmpty = function (txtId) {
            ClearPhoneValidate();

        };
        _phone.OnFocus = function (txtId) {
            ClearPhoneValidate();
        };
        _phone.OnClickMatch = function (txtId) {
            a_next_click(txtId);
        };
        function ClearPhoneValidate() {
            $('#div_MobileSmile').hide();
            $('#div_MobileError').hide();
            $('#div_MobileComment').hide();
            $('#<%# txt_Mobile.ClientID %>').removeClass('textError');
            $('#<%# txt_Mobile.ClientID %>').removeClass('textValid');
        };
        
        
        
        function a_next_click(txtId) {
            if (!document.getElementById("cb_privacy").checked) {
                ShowDivAgree();
                return;
            }
            RegisterUser();
        };
        function ShowDivAgree() {
            $('.LoaderCenter').show();
            $('#CoverDiv').show();
        };
        function HideDivAgree() {
            $('.LoaderCenter').hide();
            $('#CoverDiv').hide();
        };
        function RegisterUser() {
            var phoneNum = $('#<%# txt_Mobile.ClientID %>').val();
            $.ajax({
                url: "<%# RegisterUser %>",
                data: "{'phone': '" + phoneNum + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    switch (_data.status) {
                        case ('OK'):
                            $('#<%# hf_phone.ClientID %>').val(_data.phone);
                            ToStep2();
                            break;
                        default:
                            alert(_data.message);
                            break;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    /*
                    $('span.btn_next').hide();
                    $('a.btn_next').show();
                    GeneralServerError();
                    */
                }
            });
        }
        function ToStep2() {
            $('#step1').hide();
            $('#step2').show();
        };
        $(function () {
            var _4digitReg = new RegExp('<%# _4digitsRegulareExpresion %>');
            $('#a_agree').click(function () {
                HideDivAgree();
                RegisterUser();
            });
            $('#<%# txt_digits.ClientID%>').blur(function () {
                if (this.value.length == 0) {
                    Clear4DigitsValidation();
                    return;
                }
                if (!_4digitReg.test(this.value)) {
                    $('#div_DigitsSmile').hide();
                    $('#div_DigitsError').show();
                    $('#div_DigitsComment').show();
                    $('#div_DigitsCommentAgain').removeClass('vshidden');
                    return;
                }
                $('#div_DigitsSmile').show();
                $('#div_DigitsError').hide();
                $('#div_DigitsComment').hide();
                $('#div_DigitsCommentAgain').addClass('vshidden');
            });
            $('#<%# txt_digits.ClientID%>').focus(function () {
                Clear4DigitsValidation();
            });
            $('#a_nextDigits').click(function () {
                var phoneNum = $('#<%# hf_phone.ClientID %>').val();
                var PinCode = $('#<%# txt_digits.ClientID %>').val();
                $.ajax({
                    url: "<%# SendPinCode %>",
                    data: "{'phone': '" + phoneNum + "', 'PinCode': '" + PinCode + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        var _data = eval("(" + data.d + ")");
                        switch (_data.status) {
                            case ('OK'):
                                noProblemDesktopBinder.userRegistration(_data.account);
                                window.location.href = '<%# RedirectPage %>';//'SearchProvider.aspx';
                                break;
                            default:
                                alert(_data.status);
                                break;
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        /*
                        $('span.btn_next').hide();
                        $('a.btn_next').show();
                        GeneralServerError();
                        */
                    }
                });
            });

        });
        function Clear4DigitsValidation(){
            $('#div_MobileSmile').hide();
            $('#div_DigitsError').hide();
            $('#div_DigitsComment').hide();
    //        $('#div_DigitsCommentAgain').removeClass('vsvisible');
            $('#div_DigitsCommentAgain').addClass('vshidden');

        };
        /*
        $(function () {
            ToStep2();

        });
        */
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <div class="Register">
        <div>
            NoProblem.me
        </div>
        <div id="step1">
            <div>
                <span>US unlimited FREE call recording to the provide</span>
            </div>
            <div class="div_lines">
                <span>Listen back to conversations to:</span>
                <span class="">✓ Remember important details</span>
                <span class="">✓ Protect your rights</span>
                <span class="">✓ Preserve treasured conversations</span>
            </div>
            <div class="div_phone">
                <div class="titleMobilePhone"><asp:Label ID="lbl_Mobile" runat="server" Text="Enter your mobile to get started."></asp:Label></div>
                <div class="inputParent"> 
                    <div class="inputSun">
                        <asp:TextBox ID="txt_Mobile" runat="server" CssClass="textActive"  HolderClass="place-holder" text-mode="phone" ></asp:TextBox>
                    </div>

                    <div class="inputValid" style="display:none;"  id="div_MobileSmile">
                
                    </div>
                    <div class="inputError" style="display:none;"  id="div_MobileError">
               
                    </div>
                    <div class="inputComment" style="display:none;" id="div_MobileComment">
                         <%= PhonePlaceHolder %>
                      <div class="chat-bubble-arrow-border"></div>
                      <div class="chat-bubble-arrow"></div>
                    </div>
                </div>
            </div>
            <div class="div_next">
                <a href="javascript:void(0);" id="a_next" button-mode="phone">Next</a>
            </div>
            <div class="div_privacy">
                <input id="cb_privacy" type="checkbox" />
                <span>Agree</span>
                <a href="http://www.noproblem.me" target="_blank">Privacy and Terms.</a>
            </div>
        </div>
        <div id="step2" style="display:none;">
            <div>
                <span>US unlimited FREE call recording to the provide</span>
            </div>
            <div>
                <span>We’ve sent you a 4 digit PIN code via SMS or automated call.</span>
            </div>
            <div class="div_digits">
                <div class="title"><span>Please enter it below.</span></div>
                <div class="inputParent"> 
                    <div class="inputSun">
                        <asp:TextBox ID="txt_digits" runat="server" CssClass="textActive"  HolderClass="place-holder"  ></asp:TextBox>
                    </div>

                    <div class="inputValid" style="display:none;"  id="div_DigitsSmile">
                
                    </div>
                    <div class="inputError" style="display:none;"  id="div_DigitsError">
               
                    </div>
                    <div class="inputComment" style="display:none;" id="div_DigitsComment">
                         <%= PINcode %>
                      <div class="chat-bubble-arrow-border"></div>
                      <div class="chat-bubble-arrow"></div>
                    </div>
                    <div class="inputCommentAgain vshidden" id="div_DigitsCommentAgain">
                        <span>Please send it again</span>
                    </div>
                </div>
            </div>
            <div class="div_lines">
                <span>I can’t receive SMS.</span>
                <span>Please <a href="javascript:void(0);">call me</a> with the PIN.</span>
            </div>
            <div class="div_next">
                <a href="javascript:void(0);" id="a_nextDigits">Next</a>
            </div>
        </div>
        <div id="step3" style="display:none;">
            <div>
                <span>GREAT!</span>
            </div>
            <div>
                <span>Let’s move ahead.</span>
            </div>
        </div>
    </div>
    <div id="CoverDiv" class="CoverDiv">   
        
    </div>
    <div class="LoaderCenter">
        <span>Please agree to the</span>
        <span><a href="http://www.noproblem.me" target="_blank">Privacy and Terms.</a></span>
        <div>
            <a href="javascript:void(0);" id="a_agree">Agree</a>
        </div>
    </div>
        <asp:HiddenField ID="hf_phone" runat="server" />
    </form>
</body>
</html>
