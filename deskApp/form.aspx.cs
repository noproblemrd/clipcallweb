﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class deskApp_form : System.Web.UI.Page
{
    protected string ROOT;
    protected string url;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            ROOT = ResolveUrl("~");

            if (Request.Url.Host == "localhost")
            {
                url = "http://" + Request.Url.Host + ":" + Request.Url.Port;
                searchIframe.Attributes["src"] = url + ROOT + "npsb/flavours/flavourHalfControlsTopPersonalInjuryAttorney/supplierIframe.aspx?ExposureId=54fc263152a4ba22b43eeffa&ControlName=Flavor_Half_Controls_Top_PersonalInjuryAttorney&SliderType=1&type=1&OriginId=35779b9e-759d-df11-92c8-a4badb37a26f&ExpertiseCode=2220&Keyword=personalinjury&IsDidPhoneAvailable=False&SiteId=1&ToolbarId=&ClientDomain=localhost&TimeZone=2&Country=ISRAEL&ClientScheme=http&ProductName=YoavProduct";

            }
                
            else // for qa
            {
                url = "http://" + Request.Url.Host;
                searchIframe.Attributes["src"] = url + ROOT + "npsb/flavours/flavourHalfControlsTopPersonalInjuryAttorney/supplierIframe.aspx?ExposureId=54fc263152a4ba22b43eeffa&ControlName=Flavor_Half_Controls_Top_PersonalInjuryAttorney&SliderType=1&type=1&OriginId=35779b9e-759d-df11-92c8-a4badb37a26f&ExpertiseCode=2220&Keyword=personalinjury&IsDidPhoneAvailable=False&SiteId=1015&ToolbarId=&ClientDomain=app.ppcnp.com&TimeZone=2&Country=ISRAEL&ClientScheme=http&ProductName=YoavProduct";

            }
                

            //width:353px;height:419px
            //searchIframe.Attributes["src"] = url + ROOT + "npsb/flavours/flavourHalfControlsTopHomeImprovement/supplierIframe.aspx?ExposureId=54fc1f9d52a4ba22b4b4eae9&ControlName=Flavor_Half_Controls_Top_HomeImprovement&SliderType=1&type=1&OriginId=35779b9e-759d-df11-92c8-a4badb37a26f&ExpertiseCode=2158&Keyword=plumber&IsDidPhoneAvailable=False&SiteId=1&ToolbarId=&ClientDomain=localhost&TimeZone=2&Country=ISRAEL&ClientScheme=http&ProductName=YoavProduct";
            //searchIframe.Attributes["src"] = url + ROOT + "npsb/flavours/flavourHalfControlsTopPersonalInjuryAttorney/supplierIframe.aspx?ExposureId=54fc263152a4ba22b43eeffa&ControlName=Flavor_Half_Controls_Top_PersonalInjuryAttorney&SliderType=1&type=1&OriginId=35779b9e-759d-df11-92c8-a4badb37a26f&ExpertiseCode=2220&Keyword=personalinjury&IsDidPhoneAvailable=False&SiteId=1&ToolbarId=&ClientDomain=localhost&TimeZone=2&Country=ISRAEL&ClientScheme=http&ProductName=YoavProduct";
            searchIframe.Attributes["style"] = "width:353px;height:419px";
        }
    }
}