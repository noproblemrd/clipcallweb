﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="becomeProvider.aspx.cs" Inherits="deskApp_becomeProvider"  MasterPageFile="~/deskApp/MasterPageDeskUp.master"%>
<asp:Content  runat="server" ContentPlaceHolderID="head">
    <script>

        $(document).ready(function()
        {            
            if (typeof noProblemDesktopBinder != "undefined") {
                noProblemDesktopBinder.setSize(452, 635);
                //noProblemDesktopBinder.connectToPusher("aaa");
            }
        }
        )
        
    </script>
</asp:Content>

<asp:Content  runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="becomeProvider">
        <div class="mainTitle">
            ARE YOU A PROFESSIONAL
            <br />
            LOOKING TO GROW YOUR BUSINESS?</div>
        <div>
            
        </div>

        <div class="content1">
            Speak to interested local customers
            <br />
            within seconds of their online search.         
        </div>

        <div class="content2">
            Sign up now and receive
            <br />
            $30 in FREE phone leads. 
        </div>

        <div class="containerBtn">            
            <a href="http://app.noproblemppc.com/PPC/Register.aspx" class="btn" target="_blank">TRY US OUT!</a>
        </div>

        <div class="containerImage">

        </div>
    </div>

</asp:Content>