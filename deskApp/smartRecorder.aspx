﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="smartRecorder.aspx.cs" Inherits="deskApp_smartRecorder" MasterPageFile="~/deskApp/MasterPageDeskUp.master"  ClientIDMode="Static"%>
<asp:Content  runat="server" ContentPlaceHolderID="head">
    <link rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/shieldui-all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/light/all.min.css" />
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="../../../PlaceHolder.js"></script>
    <script src="deskApp.js"></script>
    <script type="text/javascript">

        var stepCalling;
        var ContactId;
        

        if (typeof noProblemDesktopBinder != "undefined") {           
            ContactId = noProblemDesktopBinder.getContactId();
            if (!ContactId || ContactId.length == 0)
                window.location.href = 'UserRegister.aspx';
            noProblemDesktopBinder.setSize(452, 635);
        }

        function newCallRecording()
        {
            $('.callRecording').show();
            $('.calling').hide();
            $('.online').hide();
            $('.recorded').hide();
            $('.thanks').hide();
        }
        
        function weAreCallingYouNow()
        {
            ////alert("weAreCallingYouNow");
            //      noProblemDesktopBinder.disconnectFromPusher()

            stepCalling = "weAreCallingYouNow";

            $('.callRecording').hide();
            $('.calling').show();
            $('.calling .mainTitleText').html(' WE ARE CALLING <br> YOUR PHONE..');
            $('.calling .content').text('Please pick up to connect your call.');
            $('.calling .content').css('margin-top', '18px');

            if (gender == "male")
                $('.calling .you').css('background-position', '-283px -1223px');
            else
                $('.calling .you').css('background-position', '-190px -685px');
            
            if (genderOther == "male")
                $('.calling .other').css('background-position', '-283px -1223px');
            else
                $('.calling .other').css('background-position', '-190px -685px');

            $('.imageRecord').css('background-position', '-143px -2004px');
            $('.containerAdvertisment').hide();
            $('.containerAlert').hide();
            $('.thanks').hide();
            menuRecorderCurrentCallRecording();

        }

        function YouAreAnswered() {
            ////alert('YouAreAnswered');

            stepCalling = "YouAreAnswered";

            StartProgress();

            $('.callRecording').hide();
            $('.calling').show();
            $('.calling .mainTitleText').text('WE HAVE YOU ON THE LINE');
            $('.calling .content').text('Please hold while we are connect your call');
            $('.calling .content').css('margin-top', '9px');

            if (gender == "male")
                $('.calling .you').css('background-position', '0 -1483px');
            else
                $('.calling .you').css('background-position', '-314px -685px');

            if (genderOther == "male")
                $('.calling .other').css('background-position', '-283px -1223px');
            else
                $('.calling .other').css('background-position', '-190px -685px');
            $('.arrowYou').show();
            $('.arrowOther').show();
            $('.imageRecord').css('background-position', '-143px -2004px');
            $('.containerAdvertisment').hide();
            $('.containerAlert').hide();
            $('.thanks').hide();

            
        }

        function weAreCallingToBob() { // the other 
            //alert('weAreCallingToBob - the other');
            stepCalling = "weAreCallingToOther";
        }

        function connectToTheDial() { // made connection to bob , the other
            //alert("connectToTheDial");

            stepCalling = "OtherIsAnswered";
            connectBeingRecorded();
            //_connectToTheDial();
            //StartProgress();
            //alert('connectToTheDial');

        }

        function HangUp(Reason)
        {
            
            //alert('HangUp duration: ' + Reason);
           
            StopProgress();
            

            if (typeof noProblemDesktopBinder != "undefined")
                noProblemDesktopBinder.disconnectFromPusher();
            switch(Reason)
            {
                case ('EndOfCall')://End of regular call
                    thankYou();
                    break;
                case ('NoOneAnswer'):// the other didn't answer and doesn't have automatic secretary
                    noOnePickedUp();
                    break;
                case ('TimeOver')://
                    timeRunOutRunTime()
                    break;
                case ('Redirect'): //Move to another page
                    break;
            }           
            
        }

        function _connectToTheDial() {
            
        
        }
        
        function noOnePickedUp()
        {
            $('.callRecording').hide();
            $('.calling').show();
            $('.calling .mainTitleText').text('SORRY, NO ONE PICKED UP');
            $('.calling .content').text('try again later');
            $('.calling .content').css('margin-top', '9px');

            if (gender == "male")
                $('.calling .you').css('background-position', '0 -1483px');
            else
                $('.calling .you').css('background-position', '-314px -685px');

            if (genderOther == "male")
                $('.calling .other').css('background-position', '-283px -1223px');
            else
                $('.calling .other').css('background-position', '-190px -685px');

            $('.containerAdvertisment').hide();
            $('.containerAlert').hide();
            $('.thanks').hide();
        }

        function connectBeingRecorded() {
            //alert("connectBeingRecorded");
            $('.callRecording').hide();
            $('.calling').show();
            $('.calling .mainTitleText').text('YOUR CALL IS BEING RECORDED');
            $('.calling .content').text('');
            $('.imageRecord').css('background-position', '-218px -2004px');

            if (gender == "male")
                $('.calling .you').css('background-position', '0 -1483px');
            else
                $('.calling .you').css('background-position', '-314px -685px');

            if (genderOther == "male")
                $('.calling .other').css('background-position', '0 -1483px');
            else
                $('.calling .other').css('background-position', '-314px -685px');
            
            if (blnRemain5min)
            {
                $('.containerAdvertisment').hide();
                $('.containerAlert').show();
            }

            else
            {
                $('.containerAdvertisment').show();
                $('.containerAlert').hide();
            }

            $('.thanks').hide();
        
            menuRecorderCurrentBeingRecorded();
        }

        function remain5Minutes()
        {
            if (stepCalling == "OtherIsAnswered")
            {            
                $('.callRecording').hide();
                $('.calling').show();
                $('.calling .mainTitleText').text('YOUR CALL IS BEING RECORDED');
                $('.calling .content').text('');
                $('.imageRecord').css('background-position', '-218px -2004px');

                $('.containerAdvertisment').hide();
                $('.containerAlert').show();
                $('.thanks').hide();

                blnRemain5min = true;

                menuRecorderCurrentBeingRecorded();
            }
        }


        function timeRunOutInit()
        {
            $('.callRecording .content').text('Your time has run out');
            $('.callRecording .content2').html('Please <a href="buyMore.aspx">ADD TIME</a> to cintinue recording');
            $('#txt_callTo').prop('disabled', true);
            $('.content3 .containerCallTo .place-holder').prop('disabled', true);
            $('.content3 .btn').css('background-color', '#939393');
            $('.content3 .btn .btnText A').attr('onclick', null);
            $('.content3 .btn .btnText A').css('cursor', 'auto');
            $('.content4 #chk_callerId').attr('disabled',true);
            
        }

        function timeRunOutRunTime() {
            $('.callRecording').hide();
            $('.calling').hide();
            $('.thanks').show();
            $('.modalWrapper').show();
            $('.runTimeOutOfTime').show();

            usedRemainMinutes()

        }

        function hideTimeRunOutRunTime()
        {
            $('.runTimeOutOfTime').hide();
            $('.modalWrapper').hide();
        }
        

        function usedRemainMinutes()
        {
            // remaintMinutes  remaining : global variables from timer.ascx
            //$('.thanksUsed').text((remaintMinutes - remaining).toFixed(1));
            //$('.thanksRemain').text(remaining.toFixed(1));
            $('.thanksUsed').text((minTommss(remaintMinutes - remaining)));
            $('.thanksRemain').text(minTommss(remaining));
            
        }
            

        function thankYou()
        {
            $('.callRecording').hide();
            $('.calling').hide();           
            $('.thanks').show();

            usedRemainMinutes();
            menuRecorderCurrentThanks();
        }



        function sendEmailWebService(email) {
            var _data = "{callId: '35C00602-45F4-4D2E-838D-F301AB78BA96',email:'" + email + "'}";

            $.ajax({
                url: "DesktopAppWebService.asmx/sendEmailAfterCallAndRecordDesktopApp",
                data: _data,
                dataType: "json",
                type: "POST",
                async: true,
                contentType: "application/json; charset=utf-8",
                //dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval("(" + data.d + ")");
                    //alert(_data.status);

                    if (_data.status == "success") {
                        //alert("success");
                    }

                    else
                        alert("Problem with server. Try later.");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("error" + errorThrown);
                    return;
                },
                complete: function () {

                }
            });
        }


        function sendEmail() {
            var strEmail = $('#txt_email').val();
            var validEmail;
            validEmail = checkEmail(strEmail);

            //alert(validEmail);

            if (validEmail) {
                sendEmailWebService(strEmail);

                $('#btn_email').prop('disabled', true);
                $('#btn_email').css('background-color', '#ccc');

            }

            else
                alert('email is not valid');

            return false;
        }

        function closeAdvertisement()
        {
            $('.containerAlert').hide();
        }

        

        function CallAndRecord()
        {
            var phone = $('#<%# txt_callTo.ClientID %>').val();
            var TimezoneOffset = new Date().getTimezoneOffset();
            var BlockCallerId = document.getElementById('<%# chk_callerId.ClientID %>').checked;
        //    var ContactId = '74a6aaa3-ae6a-45cd-8f27-f4932967c54d';//noProblemDesktopBinder.getContactId();
            
         //   noProblemDesktopBinder.connectToPusher(ContactId);
            $.ajax({
                url: "<%# GetCallRecordService %>",
                data: "{ 'ContactId': '" + ContactId + "', 'ToPhoneNumber': '" + phone + "', 'TimezoneOffset': " + TimezoneOffset + ", 'BlockCallerId': " + BlockCallerId + "}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                //         dataFilter: function (data) { return data; },
                success: function (data) {
                    var _data = eval('(' + data.d + ')');
                    //alert(_data.status);
                    switch (_data.status)
                    {
                        case ('StartSuccessfully'):
                            //alert(_data.callid);
                            noProblemDesktopBinder.connectToPusher(_data.callid);
                            weAreCallingYouNow();
                            break;
                        case ('ServerFaild'):
                            break;                        
                        case ('NoSecondRecordLeft'):                            
                            timeRunOutInit();
                            break;
                        case('ContactIdNotFound'):
                            break;
                        case ('ThereIsNotContactPhoneNumber'):
                            break;
                        case ('PhoneNotValid'):
                            break;
                        case ('Faild'):
                            break;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                  alert("see error: " + textStatus);
                },
                complete: function (jqXHR, textStatus) {
                    //  HasInHeadingRequest = false;
             //       noProblemDesktopBinder.disconnectFromPusher(ContactId);
                }

            });
        }
        window.addEventListener("beforeunload", function () {
            HangUp('Redirect');
        }, false);
        
    </script>
</asp:Content>

<asp:Content  runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="smartRecorder" >
       
        <div class="callRecording">
            <div class="mainTitle">CALL RECORDING</div>
            <div class="content">Ready to record a call?</div>
            <div class="content2">Enter the ten-digit phone number you want to dial:</div>            


            <div class="content3">            
                <div class="containerCallTo"><asp:TextBox runat="server" ID="txt_callTo" CssClass="txt_input" place_holder="Enter phone number" HolderClass="place-holder"></asp:TextBox></div>
                <div class="btn">
                    <div class="btnText">
                        <a href="javascript:void(0)" onclick="CallAndRecord();">Call and Record</a>
                    </div>
                    <div class="backgroundRecord">

                    </div>
                </div>
            </div>

            <div class="content4">
                <div class="">
                    *
                        The call will appear as coming
                       <br />
                     from your phone number.
                </div>

                <div>
                    <asp:CheckBox runat="server"  ID="chk_callerId"/> Block Caller ID
                </div>

                <div class="backgroundOctopus"></div>

            </div>
            
        </div>

        <div class="calling" style="display:none;">
            <div class="containerContent">     

                <div class="mainTitle">
                    <div class="mainTitleText">
                     WE ARE CALLING 
                     <br />               
                     YOUR PHONE...                    
                     
                    </div>

                    <div class="imageRecord"></div>
                </div> 

                <div class="clear"></div>

                <div class="content">
                    Please pick up to connect your call.
                </div>            
               
                <div class="containerAlert">
                    <div class="alert"></div>
                    <div class="alertContent">
                        You have less than 5 min
                        <br />
                        to wrap it up!
                    </div>
                    <div class="alertContainerBtn">
                        <!--
                        <input  type="button" value="ADD MINUTES NOW!" class="aletBtn"/>
                        -->
                        <a href="buyMore.aspx" class="alertBtn">ADD MINUTES NOW!</a>
                    </div>
                    <div class="alertClose" onclick="closeAdvertisement();">

                    </div>
                </div>

                <div class="containerAdvertisment">
                    <div class="advertisement">
                        <a href="http://app.noproblemppc.com/PPC/Register.aspx" target="_blank"><img src="images/AD_banner.png" width="383"  height="161"/></a>
                    </div>

                </div>

            </div> 
                
            <div class="containerImages">
                <div class="you"></div>
                <div class="arrowYou"></div>
                <div class="logo2"></div>
                <div class="other" title="Click to change avatar" onclick="setGenderOther();"></div>
                <div class="arrowOther"></div>
            </div>
        </div>
       

        <div class="thanks" style="display:none;">
            <div class="mainTitle">
                THANKS FOR USING NOPROBLEM.ME!
            </div>

            <div class="content">
                 We got every word.
            </div>   
            
            <div class="content2">
                Enter your email address
                <br />
                to recieve your call recording.
            </div>
            
            <div class="emailInputs">
                <div class="emailIcon"></div>
                <asp:TextBox runat="server" ID="txt_email" CssClass="txt_input" place_holder="Enter your email address"
                            HolderClass="place-holder" ></asp:TextBox>
                <input type="button"  id="btn_email" value="Send" class="btn" onclick="return sendEmail();"/>
               
            </div> 

             <div class="content3">
                 <div class="used">Used: <span class="thanksUsed"></span><span class="thanksUsedSymbol">M</span></div>
                 <div class="remaining">Remaining: <span class="thanksRemain"></span><span class="thanksRemainSymbol">M</span></div>                
            </div>

            <div class="content4">
                <a href="buyMore.aspx" >Need more minutes?</a>
                <div class="backgroundOctopus"></div>
            </div>
        </div>

        <div class="runTimeOutOfTime" style="display:none;">
            <div class="content">
                SORRY,
                <br />
                YOU'RE OUT OF TIME!
            </div>

            <div class="btn" onclick="hideTimeRunOutRunTime();">
                CLOSE
            </div>

        </div>


         <div style="display:none;">
            <input type="button" onclick="newCallRecording();" value="New call recording"/>
            <input type="button" onclick="weAreCallingYouNow();" value="We are calling you now"/>
            <input type="button" onclick="YouAreAnswered();" value="You Are Answered"/>            
            <input type="button" onclick="connectToTheDial();" value="Being recorded"/>
            <input type="button" onclick="thankYou();" value="thanks"/>

            <input type="button" onclick="noOnePickedUp();" value="No one picked up"/>
            <input type="button" onclick="remain5Minutes();" value="Remain 5 minutes"/> 
            <input type="button" onclick="timeRunOutInit();" value="Time run out initialize"/> 
            <input type="button" onclick="timeRunOutRunTime();" value="Time run out runt time"/> 
        </div>
    </div> 
</asp:Content>
