﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tutorial.aspx.cs" Inherits="deskApp_tutorial" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="http://use.typekit.com/kxs8umo.js"></script>
    <script type="text/javascript">        try { Typekit.load(); } catch (e) { }</script>
    <link href="StyleDeskApp.css" rel="stylesheet" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <title></title>

    <script>
        /********************** pre load images ***********************/
        var images = new Array()

        function preload() {
            for (i = 0; i < preload.arguments.length; i++) {
                images[i] = new Image()
                images[i].src = preload.arguments[i]
            }
        }

        preload(
            "images/tutorial/tutorial1.PNG",
            "images/tutorial/tutorial2.PNG",
            "images/tutorial/tutorial3.PNG",
            "images/tutorial/tutorial4.PNG",
            "images/tutorial/tutorial5.PNG",
            "images/tutorial/tutorial6.PNG",
            "images/tutorial/tutorial7.PNG"
        )

        var imageBack = new Image();
        imageBack.src = "images/tutorial/back.png";      

        function setWindowSize()
        {
            if (typeof noProblemDesktopBinder != "undefined") {
                noProblemDesktopBinder.setSize(452, 635); 
                //noProblemDesktopBinder.connectToPusher("aaa");
            }
        }

        $(document).ready(function()
        {          

            var index = 0;
            
            //$('.previous').css('background', 'red');

            $('.next').click(function()
            {

                index += 1;
                $('#circle' + (index - 1)).removeClass('circleActive');
                if (index == $('.circle').length)
                {
                    index = 0;
                    location.href = "SearchProvider.aspx";
                }
                
                else if (index == ($('.circle').length-1))
                {
                    $('.contentImage').css('cursor','pointer');
                    $('.contentImage').bind("click", function () {                        
                        location.href = "SearchProvider.aspx";
                        
                    });                    
                }

                else
                {
                    $('.contentImage').unbind("click");
                }

                if (index == 0)
                {
                    $('.previous').css('background-image', 'url(images/tutorial/backDisabled.png)');
                }                    
                else
                {                    
                    $('.previous').css('background-image', 'url(' + imageBack.src + ')');
                }
                    
                $('#circle' + index).addClass('circleActive');
                
                var imageToturial = images[index].src;
                
                $('.contentImage').css('background-image', 'url(' + imageToturial + ')');
                

            }
            )


            $('.contentImage').click(function()
            {
                //location.href = "SearchProvider.aspx";
                alert("fdgdfgdf");
                //$(this).unbind();
            }
            )

            

            $('.previous').click(function () {

                index -= 1;
                $('#circle' + (index + 1)).removeClass('circleActive');
                if (index <0)
                    index = 0;
                
                if (index == 0)
                {                    
                    $('.previous').css('background-image', 'url(images/tutorial/backDisabled.png)');
                }
                    
                else
                {

                }
                   
                $('.contentImage').css('cursor', 'auto');
                $('.contentImage').unbind("click");
                $('#circle' + index).addClass('circleActive');

                var imageToturial = images[index].src;
                $('.contentImage').css('background-image', 'url(' + imageToturial + ')');
               

            }
            )

            setWindowSize();
        }
        )
    </script>

</head>
<body class="tutorialBody">
    <form id="form1" runat="server">
    <div class="tutorial">
        <div class="container">
            
            <div class="mainTitle">
                How it works
            </div>

            <div class="content">
                <div class="previous"></div>
                <div class="contentImage"></div>
                <div class="next"></div>
            </div>

            <div class="progressBar">
                <div id="circle0" class="circle circleActive"></div>
                <div id="circle1" class="circle"></div>
                <div id="circle2" class="circle"></div>
                <div id="circle3" class="circle"></div>
                <div id="circle4" class="circle"></div>
                <div id="circle5" class="circle"></div>
                <div id="circle6" class="circle"></div>
            </div>

            <div class="skip">
                <a href="SearchProvider.aspx">SKIP TUTORIAL & START EXPLORING</a>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
