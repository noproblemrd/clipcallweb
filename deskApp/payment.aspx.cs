﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class deskApp_payment : System.Web.UI.Page
{
    protected string formId;
    protected string encryptedCreditCard;
    protected string encryptedCvv;
    protected string hostName;
    protected string blueSnapPublicId;

    protected void Page_Load(object sender, EventArgs e)
    {

       hostName = Request.Url.Host;
       blueSnapPublicId = ConfigurationManager.AppSettings["blueSnapPublickey"];

       deskApp_MasterPageDeskUp masterPage = (deskApp_MasterPageDeskUp)Page.Master;
       //Response.Write("cookie:" + masterPage.getFormClientId());
       formId = masterPage.getFormClientId();


       if(!IsPostBack)
       {
           planPrice = Request["planPrice"];
           planName = Request["planName"];
           planDuration = Request["planDuration"];



           HtmlGenericControl li;
           foreach(string st in states)
           {               
               li=new HtmlGenericControl("li");
               li.Attributes.Add("class", "");
               li.InnerHtml = "<a href='javascript:void(0);'>" + st + "</a>";
               ulPaymentStates.Controls.Add(li);
           }
           

           foreach (string item in Request.Form)
               Response.Write(item + " " + Request.Form[item] + "<br>************");
       }

       else
       {
           //Response.Write("first Name :" + firstName.Value + "<br>");
           //Response.Write("card last 4 :" + hdn_last4.Value + "<br>");           
           foreach (string item in Request.Form)
               Response.Write(item + " " + Request.Form[item] + "<br>************");

           Response.Write("<br>QueryString");

           foreach (string item in Request.QueryString)
               Response.Write(item + " " + Request.Form[item] + "<br>************");

           encryptedCreditCard = Request["encryptedCreditCard"];
           Response.Write("card encrypted:" + encryptedCreditCard + "<br>");

           encryptedCvv = Request["encryptedCvv"];
           Response.Write("cvv encrypted:" + encryptedCvv + "<br>");
       }

       

       spn_PlanName.InnerText = planName;
       spn_PlanMins.InnerText = planDuration;
       spn_PlanPrice.InnerText = planPrice;

       Page.DataBind();
    }

    

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        for (long i = 0; i < 1000000000; i++)
        {

        }

        /*
        WebReferenceCustomer.ResultOfBoolean _response = null;
        WebReferenceCustomer.CustomersServicePaymentInDesktopApp customerPaymentInDesktopApp=new WebReferenceCustomer.CustomersServicePaymentInDesktopApp();

        customerPaymentInDesktopApp.AmountDollars = Convert.ToDecimal(planPrice);
        customerPaymentInDesktopApp.BillingAddress = address.Value;
        customerPaymentInDesktopApp.BillingCity = city.Value;
        customerPaymentInDesktopApp.BillingFirstName = firstName.Value;
        customerPaymentInDesktopApp.BillingLastName = lastName.Value;
        customerPaymentInDesktopApp.BillingPhone = phone.Value;
        customerPaymentInDesktopApp.BillingState = ddlState.SelectedItem.Text;
        customerPaymentInDesktopApp.BillingZip = zipCode.Value;
        customerPaymentInDesktopApp.BillingEmail = email.Value;
        customerPaymentInDesktopApp.ContactId = new Guid("081A61EC-6A2F-E011-9E12-001517D10F6E");
        //customerPaymentInDesktopApp.ContactId = new Guid(hdn_contactId.Value);        

        customerPaymentInDesktopApp.CreditCardType = ddlCartType.Text;
        customerPaymentInDesktopApp.EcryptedCvv = encryptedCvv;
        customerPaymentInDesktopApp.EncryptedCreditCardNumber = encryptedCreditCard;
        customerPaymentInDesktopApp.Last4Digits = hdn_last4.Value;

        customerPaymentInDesktopApp.ExpirationMonth = Convert.ToInt16(ddlMonth.SelectedItem.Text);
        customerPaymentInDesktopApp.ExpirationYear = Convert.ToInt16(ddlYear.SelectedItem.Text);        

        customerPaymentInDesktopApp.UserAgent = Request.UserAgent;
        customerPaymentInDesktopApp.UserHost = Request.UserHostName;


        if (hostName == "localhost") // must send ip
            customerPaymentInDesktopApp.UserIP ="212.143.223.100"; 
        else
            customerPaymentInDesktopApp.UserIP = Utilities.GetIP(Request);
        

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(PpcSite.GetCurrent().UrlWebReference);
        string status="";

        try
        {
            
            _response=customer.CustomerCallRecorderPayemnt(customerPaymentInDesktopApp);

            if(_response.Type== WebReferenceCustomer.eResultType.Success)
            {
                
                if(_response.Value==true)
                {
                    status = "success";
                }

                else
                    status = "failure";
            }
                
            else
            {
                status = "failure";
                dbug_log.ExceptionLog(new Exception("deskapp payment failed " + _response.Messages[0].ToString()));
            }
               
 
            
        }

        catch(Exception exc)
        {
            status = "failure";
            dbug_log.ExceptionLog(exc);
        }       
        */
    }

    private static readonly List<string> states = new List<string>()
        {
            "AL",
            "AK",
            "AZ",
            "AR",
            "CA",
            "CO",
            "CT",
            "DE",
            "FL",
            "GA",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NV",
            "NH",
            "NJ",
            "NM",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "WA",
            "WV",
            "WI",
            "WY",
            "AS",
            "DC",
            "FM",
            "GU",
            "MH",
            "MP",
            "PW",
            "PR",
            "VI",
            "AE",
            "AA",
            "AE",
            "AE",
            "AE",
            "AP",

        };

    string planName
    {
        get
        {
            return (ViewState["planName"] == null) ? "" : (string)ViewState["planName"];
        }

        set
        {
            ViewState["planName"] = value;
        }

    }

    string planPrice
    {
        get
        {
            return (ViewState["planPrice"] == null) ? "" : (string)ViewState["planPrice"];
        }

        set
        {
            ViewState["planPrice"] = value;
        }

    }

    string planDuration
    {
        get
        {
            return (ViewState["planDuration"] == null) ? "" : (string)ViewState["planDuration"];
        }

        set
        {
            ViewState["planDuration"] = value;
        }

    }
    

}