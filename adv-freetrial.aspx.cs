﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adv_freetrial : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Master.SetTitle1 = "Advertising just got better.";
            Master.SetTitle2 = "Meet pay-per-call.";
        }
        Header.DataBind();
    }
    protected const string place_holder = "e.g. me@awesomesite.com";
    protected string Site_Id
    {
        get { return siteSetting.GetSiteID; }
    }

    protected string GetPath
    {
        get { return ResolveUrl("~") + "Formal/SalesNewRegistration.asmx/NewUser"; }
    }
    protected string GetPathLogIn
    {
        get { return ResolveUrl("~") + "Formal/SalesNewRegistration.asmx/LogIn"; }
    }
    protected string GetEmailExpresion
    {
        get { return Utilities.RegularExpressionForJavascript(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,}([-.]\w+)*"); }
    }
    protected string GetPhoneExpresion
    {
   //     get { return Utilities.RegularExpressionForJavascript(@"^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"); }
        //      get { return Utilities.RegularExpressionForJavascript(@"^\d+$"); }
        get { return Utilities.RegularExpressionForJavascript(@"^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"); }
    }

    protected string GetEmailExist
    {
        get
        {
            string _mes = "E-mail is already in the system";
            string _link = @"<a href='" + ResolveUrl("~") + @"ResetPassword.aspx' target='_blank'>Reset password</a>";
            return _mes + " " + _link;
        }
    }
    protected string GetServerError
    {
        get
        {
            return "The code could not be sent, please try again later";
        }
    }
    protected string PasswordNotFound
    {
        get { return "Wrong password"; }

    }
}