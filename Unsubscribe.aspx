﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Unsubscribe.aspx.cs" Inherits="Unsubscribe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="success" runat="server" >
        <p>You have been successfully unsubscribed.</p>
        <p>Email: <%= Email %></p>
    </div>
    <div id="fail" runat="server">
        <p>The unsubscription request failed.</p>
    </div>
</body>
</html>
