﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ApiLogIn : PageSetting
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Guid _id;
        try
        {
            _id = new Guid(Request.QueryString["login"]);
        }
        catch (Exception exc)
        {
            AccessDenied();
            return;
        }
        _SetSiteSetting();
        SetUserDetails(_id);
        Response.Redirect(ResolveUrl("~") + "Management/frame.aspx");
    }
    void SetUserDetails(Guid _id)
    {
        string SiteNameId = "";
        Guid _guid = Guid.Empty;
        string email = "";
        int SiteLangId = -1;
        using (SqlConnection conn = DBConnection.GetConnString())
        {

            string command = "dbo.ConfirmedLogIn";
            conn.Open();

            SqlCommand cmd = new SqlCommand(command, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter RetVal = cmd.Parameters.Add("RetVal", SqlDbType.Int);
            RetVal.Direction = ParameterDirection.ReturnValue;
            SqlParameter p_id = new SqlParameter("@Id", _id);
            p_id.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(p_id);
            SqlDataReader reader = cmd.ExecuteReader();


            
            if (reader.Read())
            {
                decimal balance = (decimal)reader["Balance"];
                bool IscompleteRegistration = ((int)reader["StageInRegistration"] > 5);
                string userName = (string)reader["UserName"];
                SiteNameId = (string)reader["SiteNameId"];
                _guid = ((Guid)reader["UserId"]);
                email = (string)reader["Email"];
                userManagement = new UserMangement(_guid.ToString(), userName, (double)balance, IscompleteRegistration);
                userManagement.SetUserObject(this);
            }
            reader.Dispose();
            cmd.Dispose();
            int result = (int)RetVal.Value;

            if (result < 0)
            {
                conn.Close();
                AccessDenied();
                return;
            }
            DBConnection.InsertLogin(email, _guid, siteSetting.GetSiteID, eUserType.Supplier);
            command = "EXEC dbo.GetSiteLangIdByUserId @UserId, @SiteNameId";
            cmd = new SqlCommand(command, conn);
            cmd.Parameters.AddWithValue("@UserId", _guid);
            cmd.Parameters.AddWithValue("@SiteNameId", SiteNameId);
            SiteLangId = (int)cmd.ExecuteScalar();
            conn.Close();
        }
        siteSetting = new SiteSetting(SiteNameId, SiteLangId);
        siteSetting.SetSiteSetting(this);
        
    //   Utilities.GetIP(HttpContext.Current.Request)
    }
    void AccessDenied()
    {
        Response.Redirect(ResolveUrl("~")+ "AccessDenied.aspx");
    }
    
    protected void _SetSiteSetting()
    {
        if (siteSetting==null || string.IsNullOrEmpty(siteSetting.GetSiteID))
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            if (!DBConnection.LoadSiteId(host, this))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");

            if (Request.Cookies["language"] != null)
            {
                int SiteLangId = int.Parse(Request.Cookies["language"].Value);
                if (DBConnection.IfSiteLangIdExists(SiteLangId, siteSetting.GetSiteID))
                    siteSetting.siteLangId = SiteLangId;
            }
        }
    }
}
