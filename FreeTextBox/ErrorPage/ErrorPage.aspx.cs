﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;

public partial class ErrorPage_ErrorPage : PageSetting
{
    string _path;
    protected void Page_Load(object sender, EventArgs e)
    {
        _path = Request.QueryString["aspxerrorpath"];
        if (string.IsNullOrEmpty(_path))
            _Redirect();
        else
        {
            int start = _path.LastIndexOf('.');
            string _extension = (start > -1) ? _path.Substring(start) : string.Empty;
            if ((_extension != ".aspx"))
                _Redirect();
        }
        
        Header.DataBind();
        
    }
    void _Redirect()
    {
        if (userManagement == null || !userManagement.IsAuthenticated())
        {
            _path = ResolveUrl("~") + "Publisher/LogOut.aspx";
            Header.DataBind();
            return;
        }
        if (userManagement.IsPublisher())
        {
            _path = ResolveUrl("~") + "Publisher/LogOut.aspx";
            Header.DataBind();
            return;
        }
        if (userManagement.IsAffiliate())
        {
            _path = ResolveUrl("~") + "Affiliate/LogOut.aspx";
            Header.DataBind();
            return;
        }
        _path = ResolveUrl("~") + "Management/LogOut.aspx";
    }
    protected string GetLocation
    {
        get { return _path; }
    }
    protected string GetErrorMessage
    {
        get { return HttpUtility.JavaScriptStringEncode("Error occur"); }
    }
    
}