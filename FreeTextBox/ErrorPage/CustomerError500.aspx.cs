﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ErrorPage_CustomerError500 : PageSetting
{
    string _path;
    protected void Page_Load(object sender, EventArgs e)
    {
        string _siteId;
        if (string.IsNullOrEmpty(siteSetting.GetSiteID))
        {
            string _host = Request.ServerVariables["SERVER_NAME"];
            _siteId = DBConnection.GetSiteId(_host);
        }
        else
            _siteId = siteSetting.GetSiteID;
        if (userManagement == null || !userManagement.IsAuthenticated())
        {
            if (_siteId == PpcSite.GetCurrent().SiteId)
                _path = ResolveUrl("http://www.noproblem.me");
            else
                _path = ResolveUrl("~") + "Publisher/LogOut.aspx";
            Header.DataBind();
            return;
        }
        if (userManagement.IsPublisher())
        {
            _path = ResolveUrl("~") + "Publisher/LogOut.aspx";
            Header.DataBind();
            return;
        }
        if (userManagement.IsAffiliate())
        {
            _path = ResolveUrl("~") + "Affiliate/LogOut.aspx";
            Header.DataBind();
            return;
        }
        _path = ResolveUrl("~") + "Management/LogOut.aspx";
        Header.DataBind();
    }
    protected string GetLocation
    {
        get { return _path; }
    }
    protected string GetErrorMessage
    {
        get { return HttpUtility.JavaScriptStringEncode("Error occur"); }
    }
}