﻿<%@ WebHandler Language="C#" Class="GetVideo" %>

using System;
using System.Web;
using System.IO;

public class GetVideo : IHttpHandler {
    const string CONTENT_FILE = 
@"#EXTM3U
#EXTINF:5000,Super Awesome Example Video Title
";
    const string FILE_NAME = "ClipCallVideo.m3u";
    public void ProcessRequest (HttpContext context) {
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.Clear();
        GenerateVideoLink gvl = GenerateVideoLink.GetVideoLinkPrams(context.Request.QueryString);        
            
        if (string.IsNullOrEmpty(gvl.GetVideoUrl))
        {
            context.Response.SuppressContent = true;
            context.Response.End();
            return;
        }
        MemoryStream memoryStream = new MemoryStream();
        TextWriter textWriter = new StreamWriter(memoryStream);
        textWriter.WriteLine(CONTENT_FILE + gvl.GetVideoUrl);
        textWriter.Flush(); // added this line
        byte[] bytesInStream = memoryStream.ToArray(); // simpler way of converting to array
        memoryStream.Close();

        context.Response.ContentType = "application/x-mpegURL";
        context.Response.AppendHeader("Content-disposition", "attachment; filename=" + FILE_NAME);
        context.Response.BinaryWrite(bytesInStream);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}