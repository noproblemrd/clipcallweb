﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml;
using System.IO;

public partial class yellowBook_supplierBubble : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //string incidentId="{26AD9CDC-6599-DF11-9C13-0003FF727321}";
        string SiteId = Request["SiteId"];
        //     if (!string.IsNullOrEmpty(SiteId))
        //         GetTranslate(SiteId, _panel);
        string incidentId = Request["incidentId"];
        string IfShowMinisite = Request["IfShowMinisite"];

        WebReferenceCustomer.CustomersService customer = WebServiceConfig.GetCustomerReference(this, SiteId);


        string xmlSuppliers = customer.GetSuppliersStatus(incidentId);
        //LogEdit.SaveLog(SiteId, "customer.GetSuppliersStatus:\r\n" + xmlSuppliers);
        //xmlSuppliers = "<Suppliers IncidentStatus=\"Init\" ><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";
        //Mode="Initiated"
        //xmlSuppliers="<Suppliers IncidentStatus=\"Active\" Mode=\"Open\"><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";
        //xmlSuppliers = "<Suppliers IncidentStatus=\"Active\" Mode=\"Initiated\"><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";
        //xmlSuppliers = "<Suppliers IncidentStatus=\"Completed\" ><Supplier SupplierId=\"f9af163b-7c74-df11-9319-0003ff727321\" SupplierNumber=\"123456\" SupplierName=\"eran_test1\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Init\" /><Supplier SupplierId=\"43444f8e-6d69-df11-9319-0003ff727321\" SupplierNumber=\"\" SupplierName=\"Effect\" SumSurvey=\"\" SumAssistanceRequests=\"1\" NumberOfEmployees=\"\" ShortDescription=\"\" Status=\"Initiated\" /></Suppliers>";

        //WebReferenceSite.Site siteWebService = WebServiceConfig.GetSiteReference(this);
        //string xmlSuppliers = siteWebService.SearchSiteSuppliers(SiteId, ExpertiseGUID, CityGuid, ExpertiseType, MaxResults);

        //Response.Write("SiteId:" + SiteId + " ExpertiseGUID:" + ExpertiseGUID + " CityGuid:" + CityGuid + " ExpertiseType:" + ExpertiseType + " MaxResults:" + MaxResults);
        //Response.Write(xmlSuppliers);

        System.Text.UTF8Encoding myEncoder = new System.Text.UTF8Encoding();
        byte[] bytes = myEncoder.GetBytes(xmlSuppliers);
        MemoryStream ms = new MemoryStream(bytes);
        XmlTextReader xmlTextReader = new XmlTextReader(ms);
        string SupplierId = "";
        string SupplierNumber = "";
        string SupplierName = "";
        string SumSurvey = "";
        string SumAssistanceRequests = "";
        string NumberOfEmployees = "";
        string Certificate = "";
        string ShortDescription = "";
        string isAvailable = "";
        string DirectNumber = "";
        bool blnlogo = false;
        int index = 0;
        string classIndex = "";
        string Status = "";
        string ExtensionNumber = "";
        string professionalLogos = "";
        string professionalLogosWeb = "";
        string _like = "";
        string _dislike = "";
        string City;
        string Street;
        string StreetNumber;
        string ZipCode;
        string District;

        StringBuilder sb = new StringBuilder();

        /*
         <Suppliers>
            <Supplier>
            <SupplierId />
            <SupplierNumber />
            <SupplierName />
            <SumSurvey />
            <SumAssistanceRequests />
            <NumberOfEmployees />
            <ShortDescription/ >
            <DirectNumber />
            </Supplier>
         </Suppliers>
        */

        sb.Append("<div class=\"title\">");
        sb.Append("<div id=\"listRegularText\" class=\"text\">Currently available:</div>");
        sb.Append("</div>");


        while (xmlTextReader.Read())
        {
            if (xmlTextReader.Name == "Suppliers")
            {

                sb.Append("<div style='display:none;'>mode=" +
                    xmlTextReader.GetAttribute("Mode") + "mode2;IncidentStatus=" +
                    xmlTextReader.GetAttribute("IncidentStatus") + "IncidentStatus2</div>");

                LogEdit.SaveLog(SiteId, "Session: " + Session.SessionID + " incidentId:" + incidentId + " Mode:" +
                    xmlTextReader.GetAttribute("Mode") +
                    " IncidentStatus:" + xmlTextReader.GetAttribute("IncidentStatus"));


                if (xmlTextReader.GetAttribute("ExtensionNumber") != null)
                {
                    ExtensionNumber = xmlTextReader.GetAttribute("ExtensionNumber");

                }

                while (xmlTextReader.Read())
                {
                    index++;
                    if (xmlTextReader.Name == "Supplier")
                    {

                        SupplierId = xmlTextReader.GetAttribute("SupplierId");
                        SupplierNumber = xmlTextReader.GetAttribute("SupplierNumber");
                        SupplierName = xmlTextReader.GetAttribute("SupplierName");
                        DirectNumber = xmlTextReader.GetAttribute("DirectNumber");
                        

                        Status = xmlTextReader.GetAttribute("Status");

                        City = xmlTextReader.GetAttribute("City");
                        Street = xmlTextReader.GetAttribute("Street");
                        StreetNumber = xmlTextReader.GetAttribute("StreetNumber");
                        ZipCode = xmlTextReader.GetAttribute("ZipCode");
                        District = xmlTextReader.GetAttribute("District");

                        string address = StreetNumber + " " + Street + " " + City;

                        sb.Append("<hr class=\"hr\"/>");

                        sb.Append("<div class=\"details\">");
                        if (Status == "Initiated")
                        {
                            sb.Append("<div class=\"arrow\"><img src=\"images/trangle.png\" /></div>");
                            sb.Append("<div class=\"name initiated\">" + SupplierName + "</div><div class=\"phone\">" + DirectNumber + "</div>");
                            sb.Append("<div class=\"address initiated\">" + address + "</div>");
                        }

                        else
                        {
                            sb.Append("<div class=\"name\">" + SupplierName + "</div><div class=\"phone\">" + DirectNumber + "</div>");
                            sb.Append("<div class=\"address\">" + address + "</div>");
                        }

                        sb.Append("<div class=\"clear\"></div>");
                        sb.Append("</div>");                       

                        
                    }

                    if (xmlTextReader.Name == "Suppliers" & xmlTextReader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }
                }

            }


        }


        //sb.Append("<div style='height:30px'>&nbsp;<div>");
        Response.Write(sb.ToString());


    }
}