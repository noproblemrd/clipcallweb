﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="parent2.aspx.cs" Inherits="yellowBook_parent2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<link href="Style.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-latest.js"></script>
<script src="noproblemSlider.js" type="text/javascript"></script>
   
    <title></title>
</head>
<body class="parent2">
    <form id="form1" runat="server">
        <div id="divSlider" class="slider" style="width:622px;height:275px;">            
            <div id="title" class="title">
                <div id="text" class="text">Compare Providers in Moving Companies</div>
                <div id="icons" class="icons"><span class="question">?</span> <span onclick="closeMe()" class="close">x</span></div>
            </div>
            <div id="iframe" class="subMain">
                <iframe id="iframeNoProblem"  scrolling="no" frameborder="0" src="<%=src%>" width="614" height="232"></iframe>
            </div>
            <div id="tab" class="tab" onclick="moveSlideJquery('show');"><img src="images/tab.png" /></div>
        </div>
        
    </form>
</body>
</html>