﻿var interval;
var widtDivSlider;
var heightDivSlider;
var divSliderEndPos;
var divSliderStartPos;
var newLeft;
var newLefPx;



function moveSlide(mode) {
   
    document.getElementById("tab").style.display = 'none';
    clearInterval(interval);
    
    if (mode == "show") {
        newLeft = parseInt(document.getElementById("divSlider").style.left) - 4;
        if (divSliderEndPos < newLeft) {            
            newLefPx = newLeft + "px";
            document.getElementById("divSlider").style.left = newLefPx;            
            interval = setTimeout("moveSlide('show')", 1);
        }
        else {            
            clearTimeout(interval);

        }
    }

    else if (mode == "hide") {
        newLeft = parseInt(document.getElementById("divSlider").style.left) + 4;
        if (divSliderStartPos >= newLeft) {            
            newLefPx = newLeft + "px";
            document.getElementById("divSlider").style.left = newLefPx;            
            interval = setTimeout("moveSlide('hide')", 1);
        }
        else {            
            document.getElementById("tab").style.display = 'block';
            clearTimeout(interval);

        }
    }
    
}

function moveSlideJquery(mode) {
    document.getElementById("tab").style.display = 'none';

    if (mode == "show") {
        $(".slider").animate({ "left": divSliderEndPos, opacity: 1 }, 1000);
    }

    else if (mode == "hide") {
    $(".slider").animate({ "left": divSliderStartPos }, 1000, function () { document.getElementById("tab").style.display = 'block'; });        
    }

}
function setSliderPos() {
    //document.getElementById("divSlider").style.top = '100px';
    //document.getElementById("divSlider").style.left = '300px';


    var myWidth = 0;
    var myHeight = 0;

    if (typeof (window.innerWidth) == 'number') {
        //Non-IE and IE9 strict
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
        //alert("1");
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode' strict
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
        //alert("2");
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
        //alert("3");
    }


    var innerWidth = myWidth - 10;
    var innerHeight = myHeight;
    //alert(innerHeight);

    //var divSliderTop = availHeight - 200;

    //alert(availHeight + " " + innerHeight + " " + innerWidth);
    //alert(divSliderTop);
    //document.getElementById("divSlider").style.top = divSliderTop + "px";
    //document.getElementById("divSlider").style.left = '300px';
    //alert("baaa");
    
    //var availWidth = window.screen.availWidth - 10; // minus the scroller
    var clientWidth = document.body.clientWidth;
    var clientHeight = document.body.clientHeight;
    //alert(clientHeight);

    /*
    widtDivSlider = parseInt(document.getElementById("divSlider").style.width);
    heightDivSlider = parseInt(document.getElementById("divSlider").style.height);
    */

    widtDivSlider = parseInt(document.getElementById("divSlider").style.width) + 2; // 2 is the border
    heightDivSlider = parseInt(document.getElementById("divSlider").style.height) + 2;

    //alert(width + " " + availWidth);
    //alert(document.getElementById("divSlider").style.left);
    document.getElementById("divSlider").style.left = clientWidth + "px";
    //alert(document.getElementById("divSlider").style.left);
    //document.getElementById("divSlider").style.top = availHeight - 274 + "px";
    document.getElementById("divSlider").style.top = innerHeight - parseInt(heightDivSlider) - 30 + "px";
    //alert(document.getElementById("divSlider").style.top);
    //alert(document.getElementById("divSlider").style.left);


    divSliderStartPos = clientWidth;
    divSliderEndPos = clientWidth - widtDivSlider;
    //alert(divSliderEndPos);
    moveSlideJquery('show');
}

function setSliderPosAfterResize() {     
    document.getElementById("tab").style.display = 'none';
    //if (document.getElementById("tab").style.display == 'none') {
        var innerHeight = window.innerHeight;
        var innerWidth = window.innerWidth - 10;

        var clientWidth = document.body.clientWidth;
        var clientHeight = document.body.clientHeight;
        //alert(clientHeight);

        widtDivSlider = parseInt(document.getElementById("divSlider").style.width) + 2; // 2 is the border
        heightDivSlider = parseInt(document.getElementById("divSlider").style.height) + 2;

        //alert(width + " " + availWidth);
        //alert(document.getElementById("divSlider").style.left);
        document.getElementById("divSlider").style.left = clientWidth - parseInt(widtDivSlider) + "px"; // 2 is the border
        //document.getElementById("divSlider").style.top = availHeight - 274 + "px";
        document.getElementById("divSlider").style.top = innerHeight - parseInt(heightDivSlider) - 30 + "px";
        
        divSliderStartPos = clientWidth;
        divSliderEndPos = clientWidth - widtDivSlider;
    //}
}

function closeMe() {

    moveSlideJquery('hide');
}

$("#tab").click(function () {
    
});

$(document).ready(
   function () { setSliderPos(); }
);
//window.onload = setSliderPos;
window.onresize = setSliderPosAfterResize;
    