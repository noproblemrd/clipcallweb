﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class yellowBook_parent2 : System.Web.UI.Page
{
    protected string src = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string _host = Request.ServerVariables["SERVER_NAME"];
            if (string.IsNullOrEmpty(_host))
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
            //iframeNoProblem.Attributes["src"] = GetSrc(_host);
            src = GetSrc(_host);
        }

    }

    private string GetSrc(string _host)
    {
        string _src = DBConnection.GetSrc(_host);
        if (string.IsNullOrEmpty(_src))
            Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");

        return "supplierIframe.aspx?SiteId=" + _src;


    }
}