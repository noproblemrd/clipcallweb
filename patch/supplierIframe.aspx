﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="supplierIframe.aspx.cs" Inherits="yellowBook_supplierIframe" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../general.js"></script>
    <script type="text/javascript">
        var RequestDescription;

        function checkMeasageLength() {
            document.getElementById("<%=lbl_RequestDescription.ClientID%>").style.display = '';
            document.getElementById("<%=lbl_RequestDescription.ClientID%>").innerHTML = RequestDescription + "&nbsp;<font color='#666666'>" +
           document.getElementById("<%#formMsg.ClientID%>").value.length + "</font>/140";

            if (document.getElementById("<%#formMsg.ClientID%>").value.length > 140) {
                alert(document.getElementById("<%=HiddenMaxLength.ClientID%>").value);
                document.getElementById("<%#formMsg.ClientID%>").value = document.getElementById("<%#formMsg.ClientID%>").value.substring(0, document.getElementById("<%#formMsg.ClientID%>").value.length - 1);
                document.getElementById("<%=lbl_RequestDescription.ClientID%>").innerHTML = RequestDescription + "&nbsp;<font color='#666666'>" +
                document.getElementById("<%#formMsg.ClientID%>").value.length + "</font>/140";
            }

        }

        function setClearMsg() {
            document.getElementById('<%#formMsg.ClientID%>').style.color = 'black';

            if (document.getElementById('<%#formMsg.ClientID%>').value == "<%#HiddenWaterMarkComment.Value%>") {
                document.getElementById('<%#formMsg.ClientID%>').value = '';
            }
        }

        function setClearPhone() {
            document.getElementById('<%#formCall.ClientID%>').style.color = 'black';

            if (document.getElementById('<%#formCall.ClientID%>').value == '<%#HiddenWaterMarkPhone.Value%>') {
                document.getElementById('<%#formCall.ClientID%>').value = '';
            }
        }

        

       
             

        function showRegulrSuppliersList() {

            var url;

            var oDate = new Date();
            url = "http://" + location.hostname + ":" + location.port + "<%=root%>elocal/supplierRegular.aspx?SiteId=<%=SiteId%>&origionId=<%=origionId%>&ExpertiseCode=<%=ExpertiseCode%>&ExpertiseLevel=<%=ExpertiseLevel%>&RegionCode=<%=RegionCode%>&RegionLevel=<%=RegionLevel%>&MaxResults=<%=MaxResults%>&IfShowMinisite=<%=IfShowMinisite%>&Domain=<%#Server.UrlEncode(Domain)%>&Url=<%#Server.UrlEncode(Url)%>&PageName=<%#PageName%>&PlaceInWebSite=<%#PlaceInWebSite%>&ControlName=<%#ControlName%>&SessionId=<%#Session.SessionID%>&randomSeed=" + oDate.getMilliseconds();

            //alert(url);


            var objHttp;
            var ua = navigator.userAgent.toLowerCase();

            if (ua.indexOf("msie") != -1) {
                // alert("msie");
                objHttp = new ActiveXObject("Msxml2.XMLHTTP");
            }
            else {
                //alert("not msie");
                objHttp = new XMLHttpRequest();
            }

            if (objHttp == null) {
                //alert("Unable to create DOM document!");
                return;
            }


            objHttp.open("GET", url, true);

            objHttp.onreadystatechange = function () {
                //alert("objHttp.readyState "+objHttp.readyState);          
                if (objHttp.readyState == 4) {
                    //alert(objHttp.responseText); 

                    document.getElementById("right").innerHTML = objHttp.responseText;
                    //setIframeSize('iframeNoProblem');
                }
            }

            objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            objHttp.send(null);
        }
               
        var intervalRequest="";
        var index=0;

        function showBubbleSuppliersList(incidentId)
        {
                    
            var oDate = new Date();
	        url="http://" + location.hostname + ":" + location.port + "<%=root%>elocal/supplierBubble.aspx?incidentId=" + incidentId + "&SiteId=<%=SiteId%>&IfShowMinisite=<%=IfShowMinisite%>&randomSeed=" + escape(oDate.toLocaleTimeString());    
          
            
            var objHttp;
            var ua = navigator.userAgent.toLowerCase(); 
            
            if(ua.indexOf("msie")!= -1)
            {
               // alert("msie");
                objHttp =  new ActiveXObject("Msxml2.XMLHTTP");       
            }
            else
            {
                //alert("not msie");
                objHttp=new XMLHttpRequest();
            }
            
            if (objHttp == null)
            {
                //alert("Unable to create DOM document!");
                return;
            } 	        
            
            
            objHttp.open("GET", url, true);	       
            
            objHttp.onreadystatechange=function()
            { 
                        
                if (objHttp.readyState==4)
                {
                    //alert(objHttp.responseText);
                    
                    if(intervalRequest!="")
                        clearInterval(intervalRequest);
                    intervalRequest=setTimeout("showBubbleSuppliersList('" + incidentId + "')",1000); 
                    
                    var posMode;
                    posMode=objHttp.responseText.indexOf("mode=");
                    
                    
                    var posMode2;
                    posMode2=objHttp.responseText.indexOf("mode2");
                    
                    
                    var mode;
                    mode=objHttp.responseText.substring((posMode*1+5),posMode2);  
                                
                    
                    var ifShowIframeStart;
                    ifShowIframeStart=objHttp.responseText.indexOf("IncidentStatus=");
                    
                    var ifShowIframeEnd;
                    ifShowIframeEnd=objHttp.responseText.indexOf("IncidentStatus2");
                    
                    var ifShowIframe;
                    ifShowIframe=objHttp.responseText.substring((ifShowIframeStart*1+15),ifShowIframeEnd);
                    
                    

                    if(mode=="Open") // ifShowIframe is active
                    {      
                        document.getElementById("messages").style.display = 'none';

                        document.getElementById("left").style.display = 'block';
                        document.getElementById("right").style.display = 'inline-block';                        
                                          
                        document.getElementById("left").style.backgroundColor='#FFFF99';
                        document.getElementById("leftContent").style.display = 'none';
                        document.getElementById("leftContentWaitRing").style.display = 'block';
                        document.getElementById("wait").style.display = 'block'; 
                        document.getElementById("connect").style.display = 'none';
                        document.getElementById("right").style.paddingLeft='27px';                       
                        
                    }    
                     
                    else if(mode=="Initiated") // ifShowIframe is active
                    {
                        document.getElementById("left").style.backgroundColor='#FFFF99';
                        document.getElementById("leftContent").style.display = 'none';
                        document.getElementById("leftContentWaitRing").style.display = 'block';
                        document.getElementById("wait").style.display = 'none'; 
                        document.getElementById("connect").style.display = 'block';
                        document.getElementById("right").style.paddingLeft='27px';  
                    }
                    
                    
                        
                    if(ifShowIframe=="Active")
                    {
                        //document.getElementById("div_loading").style.display='none';
                        //document.getElementById("div_list").style.display='block';                        
                        
                        /*
                        document.getElementById("div_right2").style.display='none';
                        document.getElementById("div_list").style.display='block';  
                        document.getElementById("div_list").innerHTML=objHttp.responseText;
                        */
                        document.getElementById("right").innerHTML=objHttp.responseText;
                    }
                    
                    
                    else if(ifShowIframe=="Completed")
                    {                
                        
                        clearInterval(intervalRequest);                
                        
                        //document.getElementById("div_list").style.display='none';                   
                        //document.getElementById("div_form").style.display='block'; 
                        showRegulrSuppliersList();
                        
                        document.getElementById("left").style.backgroundColor='#FFFFFF';
                        document.getElementById("leftComplete").style.display='block';
                        document.getElementById("leftContent").style.display='none';
                        document.getElementById("leftContentWaitRing").style.display='none';
                        document.getElementById("right").style.paddingLeft='17px';
                        
                                  
                    }             
                   
        	        //setIframeSize('iframeNoProblem');
                }
             }
             
            objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            
            objHttp.send(null);
            
        }

         function get_cookie(cookie_name)
        {
          var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

          if (results)
            return ( unescape ( results[2] ) );
          else
            return null;
        }

        function set_cookie(name,value,exp_y,exp_m,exp_d,exp_hh,exp_mm,exp_ss,path,domain,secure)
        {           
          
          var cookie_string = name + "=" + escape(value);

          if (exp_y)
          {
            var expires = new Date (exp_y,exp_m,exp_d,exp_hh,exp_mm,exp_ss);
           
            cookie_string += ";expires=" + expires.toGMTString();
            
          }

          if (path)
                cookie_string += "; path=" + escape (path);

          if (domain)
                cookie_string += "; domain=" + escape (domain);
          
          if (secure)
                cookie_string += "; secure";
          
          
          document.cookie = cookie_string;
          
        }

        var counter_cookies;
        counter_cookies=0;

        function createServiceRequest(msg) {           

            var phoneNumber = document.getElementById("<%#formCall.ClientID%>").value.replace("+", "%2b"); // replace + to %2b

            var current_date = new Date();
            var cookie_year = current_date.getFullYear();
            var cookie_month = current_date.getMonth();
            var cookie_day = current_date.getDate();
            var cookie_hh = current_date.getHours();
            var cookie_mm = current_date.getMinutes();
            cookie_mm = cookie_mm + 5;
            var cookie_ss = current_date.getSeconds();

            /* cookies*/

            /*
            if (!get_cookie('<%=ExpertiseCode%>')) {
                counter_cookies++;
                set_cookie('<%=ExpertiseCode%>', counter_cookies, cookie_year, cookie_month, cookie_day, cookie_hh, cookie_mm, cookie_ss);

            }

            else {
                alert("<%#HiddenHasCookie.Value%>");
                return false;
            }
            */


            document.getElementById("btnCall").style.display = 'none';
            //document.getElementById("div_form").style.display='none'; 
            //document.getElementById("div_loading").style.display='block';  

            var params;
            params = "ContactPhoneNumber=" + phoneNumber + "&Description=" +
             msg +
              "&NumOfSuppliers=" + document.getElementById("<%=formSuppliers.ClientID%>").value +
              "&SiteId=<%=SiteId%>&ExpertiseCode=<%=ExpertiseCode%>&ExpertiseType=<%=ExpertiseLevel%>&ServiceAreaCode=<%=RegionCode%>&ServiceAreaType=<%=RegionLevel%>&origionId=<%=origionId%>&ControlName=<%#ControlName%>&Domain=<%#Domain%>&Url=" + escape('<%#Url%>') + "&PageName=<%#PageName%>&PlaceInWebSite=<%#PlaceInWebSite%>";

            //alert(params);

            var url;

            url = "http://" + location.hostname + ":" + location.port + "<%=root%>createServiceRequest.aspx";


            var objHttp;
            var ua = navigator.userAgent.toLowerCase();

            if (ua.indexOf("msie") != -1) {
                // alert("msie");
                objHttp = new ActiveXObject("Msxml2.XMLHTTP");

            }
            else {
                //alert("not msie");
                objHttp = new XMLHttpRequest();
            }



            if (objHttp == null) {
                //alert("Unable to create DOM document!");
                return;
            }

            document.getElementById("btnCall").style.display = 'none';
            document.getElementById("left").style.display = 'none';
            document.getElementById("right").style.display = 'none';
            document.getElementById("messages").style.display = 'block';

            objHttp.open("POST", url, true);

            objHttp.onreadystatechange = function () {

                if (objHttp.readyState == 4) {
                    //alert("responseText "+objHttp.responseText);


                    if (objHttp.responseText == "unsuccess") {                        

                    }

                    else {
                        //alert(objHttp.responseText);
                       
                        showBubbleSuppliersList(objHttp.responseText);
                   }
                }
            }


            objHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            objHttp.setRequestHeader("Content-Length", params.length);
            objHttp.setRequestHeader("Connection", "close");

            objHttp.send(params);


            return true;

        }

        function validation() {
        <%--
        /*
            var ifValidPhone = true;
            var firstChar;

            var phoneNumber = document.getElementById("<%#formCall.ClientID%>").value.replace(/\s/g, ""); // remove spaces

            //alert(phoneNumber);

            var regFormatPhone = '<%=regFormatPhone%>';

            if (phoneNumber.search(regFormatPhone) == -1) //if match failed
            {

                ifValidPhone = false;
            }

            if (!ifValidPhone) {
                //alert(document.getElementById("<%=HiddenNotCorrectPhoneNum.ClientID%>").value);
                document.getElementById("summaryError").style.display = 'block';
                document.getElementById("phoneTitle").style.color = 'red';
                document.getElementById("<%#formCall.ClientID%>").style.borderColor = 'red';
                document.getElementById("<%#formCall.ClientID%>").style.borderWidth = '2px';
                document.getElementById("<%#formCall.ClientID%>").style.borderStyle = 'solid';
                document.getElementById("summaryError").innerHTML = document.getElementById("<%=HiddenSummary.ClientID%>").value;

                document.getElementById("<%#formCall.ClientID%>").select();
                return false;
            }

            else {
                document.getElementById("phoneTitle").style.color = 'black';
                document.getElementById("<%#formCall.ClientID%>").style.borderColor = '';
                document.getElementById("<%#formCall.ClientID%>").style.borderWidth = '';
                document.getElementById("<%#formCall.ClientID%>").style.borderStyle = '';
            }

            var msg = "";
            msg = trim(document.getElementById("<%#formMsg.ClientID%>").value);

            //alert(msg);

            if (msg == "" || msg == "<%#HiddenWaterMarkComment.Value%>") {
                document.getElementById("summaryError").style.display = 'block';
                document.getElementById("summaryError").innerHTML = document.getElementById("<%=HiddenSummary.ClientID%>").value;

                document.getElementById("msgTitle").style.color = 'red';
                document.getElementById("<%#formMsg.ClientID%>").style.borderColor = 'red';
                //document.getElementById("<%#formMsg.ClientID%>").style.borderWidth = '1px';
                document.getElementById("<%#formMsg.ClientID%>").style.borderStyle = 'solid';

                document.getElementById("<%#formMsg.ClientID%>").focus();
                return false;
            }

            else {
                document.getElementById("msgTitle").style.color = 'black';
                document.getElementById("summaryError").style.display = 'block';
                document.getElementById("<%#formMsg.ClientID%>").style.borderColor = '';
                document.getElementById("<%#formMsg.ClientID%>").style.borderWidth = '';
                document.getElementById("<%#formMsg.ClientID%>").style.borderStyle = '';
            }

            var zipCode;
            zipCode = document.getElementById("<%#formZipcode.ClientID%>").value;

            var zipCodePattern = /^\d{5}$/;

            if (!zipCodePattern.test(zipCode) && zipCode != "<%#HiddenWaterMarkZipcode.Value%>") {
                document.getElementById("summaryError").style.display = 'block';
                document.getElementById("summaryError").innerHTML = document.getElementById("<%=HiddenSummary.ClientID%>").value;

                document.getElementById("msgTitle").style.color = 'red';
                document.getElementById("<%#formZipcode.ClientID%>").style.borderColor = 'red';
                document.getElementById("<%#formZipcode.ClientID%>").style.borderStyle = 'solid';

                document.getElementById("<%#formZipcode.ClientID%>").focus();
                return false;
            }

            else {
                document.getElementById("msgTitle").style.color = 'black';
                document.getElementById("summaryError").style.display = 'block';
                document.getElementById("<%#formZipcode.ClientID%>").style.borderColor = '';
                document.getElementById("<%#formZipcode.ClientID%>").style.borderWidth = '';
                document.getElementById("<%#formZipcode.ClientID%>").style.borderStyle = '';
            }

            var mode = "<%#Mode%>";
            if (mode == "2") {
                var ExpertiseCode = document.getElementById("<%#ddl_codes.ClientID%>").value;

                if (ExpertiseCode == "") {
                    document.getElementById("summaryError").style.display = 'block';
                    document.getElementById("summaryError").innerHTML = document.getElementById("<%=HiddenSummary.ClientID%>").value;

                    document.getElementById("<%#ddl_codes.ClientID%>").style.borderColor = 'red';
                    document.getElementById("<%#ddl_codes.ClientID%>").style.borderStyle = 'solid';

                    return false;
                }

                else {
                    document.getElementById("<%#ddl_codes.ClientID%>").style.borderColor = '';
                    document.getElementById("<%#ddl_codes.ClientID%>").style.borderStyle = '';
                }

            }
            */
            --%>
            
            var msg = "";
            msg = trim(document.getElementById("<%#formMsg.ClientID%>").value);
            
            createServiceRequest(msg);


        }

        function initFields()
        {       
            document.getElementById("<%#formCall.ClientID%>").value="<%#HiddenWaterMarkPhone.Value%>";
            document.getElementById("<%#formMsg.ClientID%>").value = "<%#HiddenWaterMarkComment.Value%>";            
            document.getElementById('<%#formCall.ClientID%>').style.color = '#CCCCCC';
            document.getElementById('<%#formMsg.ClientID%>').style.color = '#CCCCCC';                        

            document.getElementById("<%#lbl_RequestDescription.ClientID%>").style.display = 'none';            
            
            document.getElementById("leftContent").style.display = 'block';
            document.getElementById("leftContentWaitRing").style.display = 'none';
            document.getElementById("leftComplete").style.display='none';
                        
            document.getElementById("wait").style.display = 'none';
            document.getElementById("connect").style.display = 'none';            
            document.getElementById("btnCall").style.display='block';             
        }

        function init() {
            showRegulrSuppliersList();
            RequestDescription = document.getElementById("<%=lbl_RequestDescription.ClientID%>").innerHTML;
        }

        window.onload = init;    
    </script>
</head>
<body class="iframe">
    <form id="form1" runat="server">
    <div id="main" class="main"> 


       <div id="left" class="left" style="display:block;">
            
            <div id="leftContent" style="display:block;">

                <div class="title">
                    <div class="text">The service I request</div>
                    <label  id="lbl_RequestDescription"  runat="server" style="display:none;font-size:8pt;position:relative;left:0px;color:black;text-indent:0px;top:0px;float:right;padding-right:15px;display:inline;"></label>
                </div>

                <div class="msgTextArea">      
                    <asp:TextBox id="formMsg"  runat="server" CssClass="waterMarkMessage" TextMode="MultiLine" onkeyup="javascript:checkMeasageLength();"  onkeypress="checkMeasageLength();"  onfocus="setClearMsg();" ></asp:TextBox>
                </div>

                <div class="categoryLocation">
                    <div class="text">
                        Category: <b>Moving</b>   <span class="location">Location: <b>San Mateo, CA</b></span> 
                    </div>

                    <div class="edit">
                        Edit
                    </div>

                </div>

                <div class="numSuppliers">
                    <div class="text">I'd like <select id="formSuppliers" runat="server" name="drop" class="fielddrop2"></select> providers to call me at</div>  <div class="call"><asp:TextBox  ID="formCall" runat="server" CssClass="waterMarkPhone" onfocus="setClearPhone();" ></asp:TextBox></div>
                </div>

                <div class="clear"></div>

                <div class="call">
                    <input type="button" id="btnCall" onclick="validation();" style="cursor:pointer;"  value="CALL ME NOW"  class="submit"/>  
                </div>
                </div>

            <div id="leftContentWaitRing" class="ring" style="display:none;">
                <div id="wait" class="wait" style="display:block;">
                    <div class="image"><img src="images/ringing_animated02.gif" /></div>
                    <div class="text">Your phone will ring now...</div>
                </div>

                <div id="connect" class="connect" style="display:none;">
                    <div class="image"><img src="images/session.gif" /></div>
                    <div class="text">Call in session...</div>
                </div>

            </div>

            <div id="leftComplete" class="leftComplete" style="display:none;">

                <div class="content">
                    Thank you for using our
                    
                    comparison service.
                </div>

                <div class="startOver">
                    You can now <a href="javascript:void(0);" onclick="initFields();" class="link">start over</a> a new comparision
                </div>
            
            </div>
       </div>
       
       

       <div id="right" class="right" style="display:inline-block;"> 
            <!--                        
            <div class="title">
                <div id="listRegularText" class="text">Currently available:</div>                              
            </div>
            
            <hr class="hr"/>

            <div class="details">
                  <div class="name">ABC Movers</div><div class="phone">(212) 333 452</div>
                  <div class="clear"></div>  
            </div>
            
            

             <hr class="hr"/>

            <div class="details">
                  <div class="name">ABC Movers</div><div class="phone">(212) 333 452</div> 
                  <div class="clear"></div> 
            </div>  
            
            <div class="clear"></div>

             <hr class="hr"/>

            <div class="details">
                  <div class="name">ABC Movers</div><div class="phone">(212) 333 452</div>
                  <div class="clear"></div>  
            </div>  
            
            <div class="clear"></div>
            -->  
            
            <!--
            <div class="title">
                <div id="listRegularText" class="text">Matched providers:</div>                              
            </div>
            
            <hr class="hr"/>

            <div class="details">
                  <div class="arrow"><img src="images/trangle.png" /></div> 
                  <div class="name">ABC Movers</div><div class="phone">(212) 333 452</div>
                  <div class="address">123 23rd st. Manhattan</div>                   
                  <div class="clear"></div>  
            </div>
            
            

             <hr class="hr"/>

            <div class="details">
                  <div class="name initiated">ABC Movers</div><div class="phone">(212) 333 452</div>
                  <div class="address initiated">123 23rd st. Manhattan</div> 
                  <div class="clear"></div> 
            </div>  
            
            <div class="clear"></div>

             <hr class="hr"/>

            <div class="details">
                  <div class="name">ABC Movers</div><div class="phone">(212) 333 452</div>
                  <div class="address">123 23rd st. Manhattan</div>
                  <div class="clear"></div>  
            </div>  
            
            <div class="clear"></div>
            -->        
        </div>

        <div id="messages" class="messages" style="display:none;">
            <div class="finding">
                 <div class="title">We are matching available Moving companies.</div>

                 <div class="content">Your phone will ring once we find matches</div>

                 <div class="anim"><img src="images/ajax-loader.gif" /></div>
            </div>          
        </div>
        
    </div>

    <asp:HiddenField id="HiddenWaterMarkComment" runat="server" Value="i.e: moving my apt from San Mateo to NY" />
    <asp:HiddenField ID="HiddenMaxLength" runat="server" Value="Max length of message is 140 characters" />
    <asp:HiddenField id="HiddenWaterMarkPhone" runat="server" Value="Mobile Number" />
    <asp:HiddenField id="HiddenHasCookie" runat="server" Value="We have already received your request. "/>
    </form>
</body>
</html>
