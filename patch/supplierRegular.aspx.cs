﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class yellowBook_supplierRegular : iframeParent
{
    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);

        /*
         <Suppliers>
            <Supplier>
            <SupplierId />
            <SupplierNumber />
            <SupplierName />
            <SumSurvey />
            <SumAssistanceRequests />
            <NumberOfEmployees />
            <ShortDescription/ >
            <DirectNumber />
            </Supplier>
         </Suppliers>
        */

        //Likes="1" Dislikes="0"

        /*
        <div class="title">
                <div id="listRegularText" class="text">Currently available:</div>                              
            </div>
            
            <hr class="hr"/>

            <div class="details">
                  <div class="name">ABC Movers</div><div class="phone">(212) 333 452</div>
                  <div class="clear"></div>  
            </div>
        */
        sb.Append("<div class=\"title\">");
        sb.Append("<div id=\"listRegularText\" class=\"text\">Currently available:</div>");                              
        sb.Append("</div>");

        while (xmlTextReader.Read())
        {


            if (xmlTextReader.Name == "Suppliers")
            {



                if (xmlTextReader.GetAttribute("ExtensionNumber") != null)
                {
                    ExtensionNumber = xmlTextReader.GetAttribute("ExtensionNumber");

                }




                while (xmlTextReader.Read())
                {
                    if (xmlTextReader.Name == "Error" && xmlTextReader.NodeType == XmlNodeType.Element)
                    {
                        ifError = true;
                        sb.Append("");
                    }

                    if (xmlTextReader.Name == "Error" & xmlTextReader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }

                    if (xmlTextReader.Name == "Supplier")
                    {
                        SupplierId = xmlTextReader.GetAttribute("SupplierId");
                        SupplierNumber = xmlTextReader.GetAttribute("SupplierNumber");
                        SupplierName = xmlTextReader.GetAttribute("SupplierName");
                        
                        /*
                        if (SupplierName.Length > 19)
                            SupplierName = SupplierName.Substring(0, 19) + "...";
                        */

                        DirectNumber = xmlTextReader.GetAttribute("DirectNumber");
                        if (DirectNumber.Length == 10)
                            DirectNumber = "(" + DirectNumber.Substring(0, 3) + ")" + " " +
                                DirectNumber.Substring(3, 3) + " " + DirectNumber.Substring(6, 4);

                        /*
                        City = xmlTextReader.GetAttribute("City");
                        Street = xmlTextReader.GetAttribute("Street");
                        StreetNumber = xmlTextReader.GetAttribute("StreetNumber");
                        ZipCode = xmlTextReader.GetAttribute("ZipCode");
                        District = xmlTextReader.GetAttribute("District");

                        string address = StreetNumber + " " + Street + " " + City;
                        */

                        //sb.Append("<hr class=\"hr\" noshade=\"noshade\" />");
                        sb.Append("<hr class=\"hr\" />");

                        sb.Append("<div class=\"details\">");
                        sb.Append("<div class=\"name\">" + SupplierName + "</div><div class=\"phone\">" + DirectNumber + "</div>");
                        //sb.Append("<div class=\"address\">" + address + "</div>");
                        sb.Append("<div class=\"clear\"></div>");
                        sb.Append("</div>");                       

                        /*
                        if (IfShowMinisite == "True")
                            sb.Append("<a href='../professional.aspx?supplierGUID={" + SupplierId + "}&SiteId=" + SiteId
                                + "&DirectNumber=" + DirectNumber + "&ExtensionNumber=" + ExtensionNumber
                                + "' target='_new'>" + SupplierName + "</a>");
                        else

                            sb.Append(SupplierName);

                        */                       

                        
                    }

                    if (xmlTextReader.Name == "Suppliers" & xmlTextReader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }
                }

            }


        }


        /*
        if (Request["MaxResults"] != null && !ifError)
        {
           
            sb.Append("<a class='showall' target='_new' href='supplierRegular.aspx?SiteId=" + SiteId +
                "&ExpertiseCode=" + ExpertiseCode + "&ExpertiseLevel=" +
                ExpertiseLevel + "&RegionCode=" + RegionCode +
                "&RegionLevel=" + RegionLevel + "'>" + lbl_ShowAll.Text + "</a>");
           
        }
        */
        //sb.Append("<div style='height:20px'>&nbsp;<div>");
        Response.Write(sb.ToString());

    }
}