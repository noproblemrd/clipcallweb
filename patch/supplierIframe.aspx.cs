﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class yellowBook_supplierIframe : System.Web.UI.Page
{
    protected string SiteId = "";
    protected string ExpertiseCode = "";
    protected string ExpertiseLevel = "";
    protected string RegionCode = "";
    protected string RegionLevel = "";
    protected string RegionName = "";
    protected string MaxResults = "";
    protected string root = "";

    protected string origionId = "";
    protected string regFormatPhone = "";
    protected string IfShowMinisite = "";

    protected string Domain = "";
    protected string Url = "";
    protected string Channel = "";
    protected string ExposureType = "";
    protected string PageName = "";
    protected string PlaceInWebSite = "";
    protected string ControlName = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            root = ResolveUrl("~");
            SiteId = Request["SiteId"];
            regFormatPhone = DBConnection.GetPhoneRegularExpression(SiteId);
            regFormatPhone = regFormatPhone.Replace("\\", "\\\\");
            ExpertiseCode = Request["ExpertiseCode"];
            ExpertiseLevel = Request["ExpertiseLevel"];
            RegionCode = Request["RegionCode"];
            RegionLevel = Request["RegionLevel"];
            MaxResults = Request["MaxResults"];
            origionId = Request["origionId"];

            Domain = Request["Domain"];
            if (String.IsNullOrEmpty(Domain))
            {
                Domain = Request.Url.Host;
                //Domain = Utilities.GetIP(Request); // the ip of the client
            }

            Url = Request["Url"];
            if (String.IsNullOrEmpty(Url))
                Url = Request.Url.AbsoluteUri;

            Channel = "";

            ExposureType = Request["ExposureType"];
            if (String.IsNullOrEmpty(ExposureType))
                ExposureType = "widget";

            PageName = Request["PageName"];
            if (String.IsNullOrEmpty(PageName))
                PageName = "plumbers";

            PlaceInWebSite = Request["PlaceInWebSite"];
            if (String.IsNullOrEmpty(PlaceInWebSite))
                PlaceInWebSite = "right";

            ControlName = Request["ControlName"];
            if (String.IsNullOrEmpty(ControlName))
                ControlName = "widget1";

            formMsg.Text = HiddenWaterMarkComment.Value;
            formCall.Text = HiddenWaterMarkPhone.Value;

            IframeSite _iframe = new IframeSite(SiteId);
            int defaultNumAdvertisers = _iframe.GetDefaultNumAdvertisers;
            int getMaximimAdvertisers = _iframe.GetMaximimAdvertisers;
            IfShowMinisite = _iframe.GetDisplayMinisite.ToString();
            //Response.Write("bool: " + _iframe.GetDisplayMinisite.ToString());

            ListItem listItem;

            for (int i = 1; i <= getMaximimAdvertisers; i++)
            {
                listItem = new ListItem();
                listItem.Text = i.ToString();
                listItem.Value = i.ToString();
                if (defaultNumAdvertisers == i)
                    listItem.Selected = true;
                formSuppliers.Items.Add(listItem);
            }        

            Page.Header.DataBind();
        }
    }
}