﻿// JScript File

    if (window.addEventListener)
        window.addEventListener('load', LoadLoader, false);
    else if (window.attachEvent)
        window.attachEvent('onload', LoadLoader);
    function LoadLoader() {        
       if (typeof (Sys) == 'undefined')
            return;
       var manager = Sys.WebForms.PageRequestManager.getInstance();
       manager.add_endRequest(endRequest);
       manager.add_beginRequest(OnBeginRequest);
    }
    function OnBeginRequest(sender, args) {
        //alert("begin");
        document.getElementById('divLoader').style.display = 'block';
    }
    function endRequest(sender, args) {
        document.getElementById('divLoader').style.display = 'none';
    }  
    /*
    function LoadUpdateProgress()
    {
        var manager = Sys.WebForms.PageRequestManager.getInstance();
       manager.add_endRequest(endRequest);
       manager.add_beginRequest(OnBeginRequest);
    }
    */
