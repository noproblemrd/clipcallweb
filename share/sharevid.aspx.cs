﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class share_sharevid : PageClipCallLandingPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
         Guid _id;
         if (!Guid.TryParse(Request.QueryString["shareid"], out _id))
        {
            Response.Redirect(GENERAL_REDIRECT);
            return;
        }
         shareVideoId = _id;
        
    }
    protected Guid shareVideoId { get; set; }

    [WebMethod(MessageName = "GetShareVideoDetails")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ClipCallReport.VideoShareDataResponse GetShareVideoDetails(Guid shareVideoId)
    {
        ClipCallReport.ClipCallReport ccr = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        ClipCallReport.ResultOfVideoShareDataResponse data = null;
        try
        {
            data = ccr.GetShareVideoData(shareVideoId);
        }
        catch(Exception exc)
        {
            dbug_log.ExceptionLog(exc);
            return null;
        }
        if (data.Type == ClipCallReport.eResultType.Failure)
            return null;
        return data.Value;
    }

    [WebMethod(MessageName = "UpdateRequestMetadata")]
    public static void UpdateRequestMetadata(Guid shareId, bool isRobot)
    {
        ClipCallReport.ClipCallReport ccr = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        var context = HttpContext.Current;
        ClipCallReport.VideoSharePageRequestData videoSharePageRequest = new ClipCallReport.VideoSharePageRequestData
        {
            ShareVideoId = shareId,
            Browser = context.Request.Browser.Browser,
            Platform = context.Request.Browser.Platform,
            UserAgent = context.Request.UserAgent,
            IP = Utilities.GetIP(context.Request),
            isRobot = isRobot
        };
     //   AarService.AarService aarService = WebServiceConfig.GetAarService();
        try
        {
            ccr.UpdateCameToLandingPage(videoSharePageRequest);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
    }
    [WebMethod(MessageName = "FireEvent")]
    public static void FireEvent(string eventName, Guid shareId)
    {
        ClipCallReport.ClipCallReport ccr = WebServiceConfig.GetClipCallReportReference(PpcSite.GetCurrent().UrlWebReference);
        try
        {
            ccr.FireEvent(eventName, shareId);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc);
        }
    }
}