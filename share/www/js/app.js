(function (app) {
    app.appModule = angular.module('shareApp', ['ionic', 'ngSanitize']);

    app.appModule
        .run(function($ionicPlatform) {
            $ionicPlatform.ready(function() {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
        })
        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
            $ionicConfigProvider.views.maxCache(0);

            $stateProvider.state('viewVideo', {
                url: '/',

                views: {
                    '': {
                        templateUrl: 'www/templates/view-video.html',
                        controller: 'videoController'
                    },
                    'video@viewVideo':
                        {
                            templateUrl: window.app.useJwPlayer ? 'www/templates/jw-player.html' : 'www/templates/video.html',
                            controller: window.app.useJwPlayer ? 'jwVideoController' : 'html5VideoController'
                        }
                      
                }
            });
            
           

            $stateProvider.state('getTheApp', {
                url: '/app',
                templateUrl: 'www/templates/gettheapp.html',
                controller: 'getTheAppController'
            });
           
// if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('/');
        })
        .config(function($httpProvider) {

            $httpProvider.defaults.headers.post = {};
            $httpProvider.defaults.headers.post["Content-Type"] = "application/json; charset=utf-8";

        })
    .config(function ($ionicConfigProvider) {
        $ionicConfigProvider.backButton.previousTitleText(false);
        $ionicConfigProvider.backButton.text('');
        $ionicConfigProvider.backButton.icon('ion-chevron-left');
        
    })

    .run(function (shareService, $timeout) {
        $timeout(function () {
            shareService.updateRequestMetadata(window.app.shareId, window.app.isRobot);
        }, 1000);
    })
      
    ;

})(window.app)