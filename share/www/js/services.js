﻿(function(app) {
    app.appModule.factory('shareService', function ($http, $ionicModal, $ionicLoading, $state, $ionicPopup, $ionicHistory, $window) {

        var modalDialog = undefined;

        var share = null;
        var isPlaying = false;
        var _toShowThumbnail = true;
        var phoneDetails = null;
        function updateRequestMetadata(shareId, isRobot) {
            return $http.post('sharevid.aspx/UpdateRequestMetadata', { shareId: shareId, isRobot: isRobot });
        }



        function fireEvent(eventName, shareId) {
            return $http.post('sharevid.aspx/FireEvent', { eventName: eventName, shareId: shareId });
        }

        function openModal(templateUrl,$scope) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                modalDialog = modal;
                modalDialog.show();
            });           
        }
        
        function closeModal() {
            modalDialog.hide();
            modalDialog.remove();
            modalDialog = undefined;
        }

        function getShareVideoData(callId) {
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
            });
            return $http.post('sharevid.aspx/GetShareVideoDetails', { shareVideoId: window.app.shareId })
                .success(function (data, status, headers, config) {                       
                    $ionicLoading.hide();
                    if (data.d == null) {
                        setTimeout(function () {
                            window.location.href = window.app.GeneralRedirect;
                        }, 500);
                        return;
                    }
                    share = data.d;
                    console.log(share);
                })
                .error(function (data, status, headers, config) {
                    $ionicLoading.hide();                   
                });
        }
        function showGeneralErrorMessage() {
            var myPopup = $ionicPopup.show({
                title: 'Oops...',
                subTitle: 'Something went wrong. Please try again late',
                buttons: [
                  {
                      text: '<b>Ok</b>',
                      type: 'button-positive',
                      onTap: function (e) {
                        
                      }
                  }
                ]
            });
            myPopup.then(function (res) {

            });
        }

        function getTheApp() {
            /*
            if (!lead || !lead.Incident)
                return;            
                */
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
            });

            fireEvent('GetApp', window.app.shareId).then(getTheAppCompleted, getTheAppCompleted);
        }

        function getTheAppCompleted() {            
            $ionicLoading.hide();
            $window.location.href = "../tm/DownloadApp.aspx?shareId=" + window.app.shareId;
        }
        function loadVideo(VideoUrl, ThumbnailUrl) {
            var clipcallVideoNode = document.getElementById("ClipcallVideo");
            while (clipcallVideoNode.firstChild) {
                clipcallVideoNode.removeChild(clipcallVideoNode.firstChild);
            }
            var playerInstance = jwplayer("ClipcallVideo");
            var divParent = document.querySelector('.video-container');

            playerInstance.setup({
                file: VideoUrl,
                image: ThumbnailUrl,
                width: divParent.offsetWidth,
                height: (divParent.offsetHeight)
                //    aspectratio: "16:9"//,
                //    autostart: true               
            });
            jwplayer().onComplete(function () {
                isPlaying = false;
                fireEvent('leadViewed', window.app.shareId);
                //       $ionicHistory.goBack();
            });
            jwplayer().onPlay(function () {
                isPlaying = true;
                fireEvent('viewLead', window.app.shareId);

                try {
                    if (typeof sessionStorage != 'undefined')
                        sessionStorage.setItem('isViewedVideo', 'true');
                }
                catch (exc) { }
            })
        }
        function loadVideoHtml5(thumbnailUrl) {
            var divParent = document.querySelector('.video-container');
            var _video = document.querySelector('video');            

            var observer = new MutationObserver(function (mutations) {
                mutations.forEach(function (mutation) {
                    if (mutation.type == 'attributes' && mutation.attributeName == 'src') {
                        _video.load();
                    }
                });
            });

            var config = { attributes: true, childList: true, subtree: true };

            observer.observe(_video, config);

            //   _video.style.width = divParent.offsetWidth + 'px';
            _video.setAttribute("width", divParent.offsetWidth);
            //   _video.style.height = (divParent.offsetHeight - 5) + 'px';
            _video.setAttribute("height", (divParent.offsetHeight - 5));
       //     _video.style.background = "transparent url('" + thumbnailUrl + "') no-repeat 0 0";
            _video.addEventListener('play', function () {
                //_toShowThumbnail = false;
                HideThumbnail();
                if (isPlaying)
                    return;
                isPlaying = true;
                
                fireEvent('viewLead', window.app.shareId);

                try {
                    if (typeof sessionStorage != 'undefined')
                        sessionStorage.setItem('isViewedVideo', 'true');
                }
                catch (exc) { }
            }, false);
            _video.addEventListener('ended', function () {
                isPlaying = false;
                fireEvent('leadViewed', window.app.shareId);
            }, false);           
        }
        function HideThumbnail() {
            document.querySelector('#img_thumbnail').style.display = "none";
        }
        function runVideo(VideoUrl, ThumbnailUrl) {
           
            
            ///    jwplayer().onReady(function () {
            if (window.app.useJwPlayer == true)
                jwplayer().play();
            else {
                var _video = document.querySelector('video');
       //         _video.load();
                _video.play();
               
            }
            //       });
            //      playerInstance.play();

        }
        function IsVideoPlaying() {
            return isPlaying;
        }
        function ToShowThumbnail() {
            return _toShowThumbnail;
        }
        
        return {
            fireEvent:fireEvent,
            getShareVideoData: getShareVideoData,            
            openModal: openModal,
            closeModal: closeModal,
            updateRequestMetadata: updateRequestMetadata,
            showGeneralErrorMessage: showGeneralErrorMessage,            
            getTheApp: getTheApp,
            runVideo: runVideo,
            loadVideo: loadVideo,
            loadVideoHtml5: loadVideoHtml5,
            IsVideoPlaying: IsVideoPlaying,
            ToShowThumbnail: ToShowThumbnail
        };
    });
})(window.app);