﻿(function (app) {


    app.appModule
        .controller('videoController', function ($scope, shareService, $state, $sce, $location) {
                       

            $scope.getTheApp = function () {
                shareService.getTheApp();
            };
            $scope.playVideo = function () {
               
                shareService.runVideo($scope.vm.VideoUrl, $scope.vm.PicPreviewUrl);
            }
            $scope.isViewedVideo = function () {

                if (typeof sessionStorage != 'undefined' && $scope.isSupportSessionStorage()) {
                    if (typeof sessionStorage.isViewedVideo == 'undefined')
                        return false;
                    return sessionStorage.isViewedVideo == 'true';
                }

                return true;
            }
            $scope.isSupportSessionStorage = function () {

                try {
                    var testKey = 'test', storage = window.sessionStorage;
                    storage.setItem(testKey, '1');
                    storage.removeItem(testKey);
                    return true;
                }
                catch (error) {
                    return false;
                }
            }
            $scope.thumbnailWidth = function () {
                return document.querySelector('div.video-container').offsetWidth;
            }
            $scope.thumbnailHeight = function () {
                return document.querySelector('div.video-container').offsetHeight;
            }            
            function init() {
                $scope.vm = {};
                $scope.vm.getThumbnail = function () {
                  //  return '';
                    if ($scope.vm.Incident && $scope.vm.PicPreviewUrl)
                        return $scope.vm.PicPreviewUrl.$$unwrapTrustedValue();
                    else
                        return null;
                };

                shareService.getShareVideoData().success(function (data) {
                    if (!data.d) {
                        shareService.showGeneralErrorMessage();

                        return;
                    }

                    angular.extend($scope.vm, data.d);
                    //     $scope.vm.videoState = window.app.useJwPlayer == true ? "jwvideo" : "video";
                    if (window.app.useJwPlayer == true)
                        shareService.loadVideo($scope.vm.VideoUrl, $scope.vm.PicPreviewUrl);
                    else {
                        shareService.loadVideoHtml5($scope.vm.PicPreviewUrl);
                        if ($scope.vm && $scope.vm.PicPreviewUrl)
                            $scope.vm.PicPreviewUrl = $sce.trustAsResourceUrl($scope.vm.PicPreviewUrl);

                        if ($scope.vm && $scope.vm.VideoUrl)
                            $scope.vm.VideoUrl = $sce.trustAsResourceUrl($scope.vm.VideoUrl);
                        
                    }
                    

                }).error(function (error) {
                    $scope.error = error;
                });
            }


            init();
            

        })        
        .controller('getTheAppController', function ($scope, shareService) {
            $scope.getTheApp = function () {
                shareService.getTheApp();
            };
        })
        
        .controller('jwVideoController', function ($scope, $stateParams, shareService) {
            
        })
        .controller('html5VideoController', function ($scope, $stateParams, shareService) {
            $scope.showPlayIcon = function()
            {
                return !shareService.IsVideoPlaying();
            }
            $scope.showThumbnail = function () {
                return shareService.ToShowThumbnail();
            }
            
        });        

    

})(window.app);