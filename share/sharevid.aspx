﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sharevid.aspx.cs" Inherits="share_sharevid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
      <meta name="robots" content="nofollow"><meta name="googlebot" content="noindex"/>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width"/>
    <title></title>

    <link href="www/lib/ionic/css/ionic.css" rel="stylesheet"/>
    <link href="www/css/style.css" rel="stylesheet"/>
    <link href="www/css/font-awesome.css" rel="stylesheet" />

   <script src="//use.typekit.net/lbv4vuj.js"></script>
   <script>try{Typekit.load({ async: true });}catch(e){}</script>
      <script src="../jwplayer8/jwplayer.js" type="text/javascript"></script>
     <script type="text/javascript">jwplayer.key = "y0rIShXb57vnSkTfeDGVEdJRxWxvMmdar4NZxr6O60U=";</script>
<!--    <script type="text/javascript">jwplayer.key = "YiTTzdj7vuYrnKLzmlEHBtKK4FOLklGGehNIf6rsjWg=";</script>-->
    <script type="text/javascript">
        window.app = {};
        window.app.shareId = <%=Guid.Empty == this.shareVideoId ? "''" : "'"+this.shareVideoId.ToString()+"'" %>;
        window.app.isRobot = <%=IsRobot()%>;
        window.app.useJwPlayer = <%= useJwPlayer()%>;
        window.app.GeneralRedirect = '<%= GENERAL_REDIRECT%>';
    </script>
     <script src="www/lib/ionic/js/ionic.bundle.min.js" type="text/javascript"></script>

    <script src="www/js/app.js" type="text/javascript"></script>
    <script src="www/js/services.js" type="text/javascript"></script>
    <script src="www/js/controllers.js" type="text/javascript"></script>
</head>
<body ng-app="shareApp">     
    <ion-nav-view></ion-nav-view>
       <script type="text/javascript">
           (function (d, s, i, r) {
               if (d.getElementById(i)) { return; }
               var n = d.createElement(s), e = d.getElementsByTagName(s)[0];
               n.id = i; n.src = '//js.hs-analytics.net/analytics/' + (Math.ceil(new Date() / r) * r) + '/2156669.js';
               e.parentNode.insertBefore(n, e);
           })(document, "script", "hs-analytics", 300000);
    </script>
  </body>
</html>
