﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPPre.master" AutoEventWireup="true" CodeFile="partners.aspx.cs" Inherits="partners" %>

<%@ MasterType VirtualPath="~/Controls/MasterPageNPPre.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript">
    $(document).ready(function () {
        $('#slider1').bxSlider();
    });
    
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="partnermain">	
	<div id="wrappar">
	 	<div class="subtext">
    <p>Five years of activity in the</p><p>worldwide market have proven</p><p>beyond a doubt that pay-per-call is</p><p>the best way to bring value to</p><p>advertisers.</p></div>
		
		<div class="boxpartners">
			<h3 class="subclass">Voice leads are the new clicks.</h3>
			<div class="boxpartnerstext">If you have a database of advertisers and would like to introduce them to a better way to get leads, or if you're considering establishing such an operation, you are in the right place, at the right time! Join No Problem's affiliate program and start reaping the benefits of pay-per-call. We're always looking for partners to help us extend our global reach and bring quality advertisers on board.</div> 
			
		</div>
	
			<div class="clear"></div> 

		
		<div class="boxpartner">
		
			<h3><a href="partners-marketing.aspx">Marketing Agencies</a></h3>
			<div class="boxparnettwotext">
			  <p>Help your clients gain greater ROI and improve lead quality by signing up for a pay-per-call campaign.</p>
			  <p>&nbsp;</p>
			  <p><a href="partners-marketing.aspx" class="partnersReadMore">Read more</a><a href="#"></a></p>
			</div>
			<div class="clear"></div>
				<!--div class="pmore"><br /><a href="">Learn More</a></div-->
		</div>
		
		<div class="boxpartner">
			<h3><a href="partners-franchisees.aspx">Franchisees</a></h3>			
			<div class="boxparnettwotext">
			  <p>No Problem is expanding globally. Become a franchisee in your country.</p>
			  <p>&nbsp;</p>
			  <p>&nbsp;</p>
			  <p><a href="partners-franchisees.aspx" class="partnersReadMore">Read more</a></p>
			</div>
			<div class="clear"></div>
			<!--div class="pmore"><br /><br /><a href="">Learn More</a></div-->
		</div>
	<div class="clear"></div>
    <!--
		<div class="icons">
		</div>			
        -->
			
<div id="Icons_container">
	    
	    <span class="_partners">Partners we have mileage with</span>
	    
	    <div class="_slider">
		    <div id="slider1">          
              <div class="icons1">
                <span style="left:1115px;">Finland</span>
                <span style="left:1308px;">Poland</span>
                <span style="left:1515px;">Israel</span>
                <span style="left:1680px;">Estonia Latvia Lithuani</span>
              </div>
              <div class="icons2">
                <span style="left:2065px;">Brazil</span>
                <span style="left:2272px;">Israel</span>
                <span style="left:2455px;">Russia</span>
                <span style="left:2680px;">Hungary</span>
              </div>
            </div>
        </div>
    </div>
  </div>
	</div> <!-- End wrapadv -->
</asp:Content>

