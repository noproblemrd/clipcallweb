﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Reviews : PageSetting//, IPostBackEventHandler
{
    const string SEND = "_send";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string _id = Request.QueryString["Id"];
            string SiteNameId = Request.QueryString["SiteId"];
            if (string.IsNullOrEmpty(_id) || !Utilities.IsGUID(_id))
            {
                 Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
                return;
            }
            if (string.IsNullOrEmpty(SiteNameId))
            {
                string host = Request.Url.Host;
                SiteNameId = DBConnection.GetSiteId(host);
            }
            if (!SiteSetting.TryGetSiteSetting(SiteNameId, out siteSetting))
            {
                Response.Redirect(ResolveUrl("~") + "AccessDenied.aspx");
                return;
            }
        //    siteSetting = new SiteSetting(SiteNameId);
            siteSetting.SetSiteSetting(this);
            MyStyleSheet.Attributes["href"] = ResolveUrl("~") + "Management/" + siteSetting.GetStyle;// SiteSetting.GetStyle(SiteNameId);
            
            RegularExpressionValidator_phone.ValidationExpression = siteSetting.GetMobileAndRegularPhoneExpression();
            Guid _guid;
            try
            {
                _guid = new Guid(_id);
            }
            catch (Exception exc)
            {
                ErrorOccurred();
                return;
            }
            //set default rating
            hf_IsLike.Value = "Like";
            LoadReviews(siteSetting.GetUrlWebReference, _guid);
           
            IdV = _guid;
        }
         
        Header.DataBind();
    }
    
    private void LoadReviews(string UrlWebReference, Guid _id)
    {
        WebReferenceReports.Reports _report = WebServiceConfig.GetReportsReference(UrlWebReference);
        WebReferenceReports.GetReviewsRequest _request = new WebReferenceReports.GetReviewsRequest();
        
        _request.SupplierId = _id;
        _request.ReviewStatus = WebReferenceReports.ReviewStatus.Approved;
        WebReferenceReports.ResultOfGetReviewsResponse result = null;
        try
        {
            result = _report.GetReviews(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            ErrorOccurred();
            return;
        }
        if (result.Type == WebReferenceReports.eResultType.Failure)
        {
            ErrorOccurred();
            return;
        }
        
        DataTable data = new DataTable();
        //GetDataTable.GetDataTableFromList(result.Value.Reviews);
        data.Columns.Add("CreatedOn");
        data.Columns.Add("Name");
        data.Columns.Add("Description");
        data.Columns.Add("Like");
        foreach (WebReferenceReports.ReviewData rd in result.Value.Reviews)
        {
            DataRow row = data.NewRow();
            row["CreatedOn"] =  rd.CreatedOn;
            row["Name"] = rd.Name;
            row["Description"] = rd.Description;
            row["Like"] = (rd.IsLike) ? ResolveUrl("~") + "images/up1.png" :
                ResolveUrl("~") + "images/down1.png";
            data.Rows.Add(row);
        }

        _Repeater.DataSource = data;
        _Repeater.DataBind();

       
    }
    void ErrorOccurred()
    {
        string script = "alert('" + lbl_errorClose.Text + "');";
        script += "window.close();";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "CloseWindow", script, true);
    }
    void _send()
    {
        if (!_RadCaptcha.IsValid)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MissingCaptcha", "alert('" + HttpUtility.JavaScriptStringEncode(lbl_MissingCaptcha.Text) + "');", true);
            return;
        }
        WebReferenceCustomer.CustomersService _customer = WebServiceConfig.GetCustomerReference(null, siteSetting.GetSiteID);
        WebReferenceCustomer.InsertReviewRequest _request = new WebReferenceCustomer.InsertReviewRequest();
        _request.Description = txt_yourReview.Text;
        _request.IsLike = (hf_IsLike.Value == "Like");
        _request.Name = txt_name.Text;
        _request.Phone = txt_phone.Text;
        _request.SupplierId = IdV;
        WebReferenceCustomer.ResultOfInsertReviewResponse result = null;
        try
        {
            result = _customer.InsertReview(_request);
        }
        catch (Exception exc)
        {
            dbug_log.ExceptionLog(exc, siteSetting.GetSiteID);
            string script = "alert('" + lbl_error.Text + "');";
            ClientScript.RegisterStartupScript(this.GetType(), "error", script, true);
            return;
        }
        if (result.Type == WebReferenceCustomer.eResultType.Failure)
        {
            string script = "alert('" + lbl_error.Text + "');";
            ClientScript.RegisterStartupScript(this.GetType(), "error", script, true);
            return;
        }
        string _script = "";

        switch (result.Value.InsertionStatus)
        {
            case (WebReferenceCustomer.InsertReviewStatus.NoCaseFound):
                _script = lbl_NoCaseFound.Text;
                break;
            case (WebReferenceCustomer.InsertReviewStatus.AlreadyExists):
                _script = lbl_AllreadyExists.Text;
                break;
            case (WebReferenceCustomer.InsertReviewStatus.OK):
                _script = lbl_ReviewReceive.Text;
                break;
            default: _script = HttpUtility.HtmlEncode(hf_validation.Text);
                break;
        }

        _script = "alert('" + _script + "');";
        ClientScript.RegisterStartupScript(this.GetType(), "success", _script, true);
        txt_yourReview.Text = "";
        txt_name.Text = "";
        txt_phone.Text = "";
        hf_IsLike.Value = "Like";
        ClientScript.RegisterStartupScript(this.GetType(), "ClearLike_Dislike", "ClearLike_Dislike();", true);
    }
    protected void btn_send_Click(object sender, EventArgs e)
    {
        _send();
    }
    /*
    CustomersService.asmx:
[WebMethod]
ublic Result<DataModel.Reviews.InsertReviewResponse> InsertReview(DataModel.Reviews.InsertReviewRequest request)
*/
    /*
    string SiteIdV
    {
        get { return (ViewState["SiteNameId"] == null) ? string.Empty : (string)ViewState["SiteNameId"]; }
        set { ViewState["SiteNameId"] = value; }
    }
     * */
    Guid IdV
    {
        get { return (ViewState["UserId"] == null) ? new Guid() : (Guid)ViewState["UserId"]; }
        set { ViewState["UserId"] = value; }
    }
    protected string GetValidationMissing
    {
        get { return HttpUtility.HtmlEncode(hf_validation.Text); }
    }
    protected string GetReviewLength
    {
        get { return HttpUtility.HtmlEncode(hf_reviewLength.Text); }
    }
}
