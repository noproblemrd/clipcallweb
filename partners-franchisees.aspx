﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Controls/MasterPageNPPartner.master" AutoEventWireup="true" CodeFile="partners-franchisees.aspx.cs" Inherits="partners_franchisees" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">

<div class="subtext_partners">
    		<p>If you're here, you must be considering</p><p>
			opening a No Problem franchise. Did you know,</p><p> 
			pay-per-call campaigns convert 10X the rate of search campaigns? </p><p>
			Want to hear more?</p>
			</div>
		
			<div class="factspartners">
				<h3 class="subclass">Take a look at these numbers:</h3>

        		<div class="partnersfactsblock01"><p><span class="sloganpartnersleft">An average CPM of</span></p><h4><span class="smalltextpartnersright">$3000 to $5000 USD</span></h4></div>

        <div class="partnersfactsblock02"><p><span class="sloganpartnersleft">The number of No Problem advertisers increased more than</span></p><h4><span class="smalltextpartnersright">400% in three months</span></h4></div>

    	  

        <div class="partnersfactsblock03"><h4><span class="sloganpartners">Full 30% to 40%</span></h4><p><span class="slogansmallpartners">conversion rate from calls to real jobs</span></p></div>

        <div class="partnersfactsblock04"><h4><span class="sloganpartners">OVER 95%</span></h4><p><span class="slogansmallpartners">of our customers are loyal and increasing their exposure</span></p></div>

      </div>
	
			<div class="clear"></div> 

		
		<div class="boxleft"> 

		    	<h3 class="subclass">Why pay-per-call is a smart way to advertise:</h3>

		        <div class="toggle-content">

				
					<h5>Connect by phone immediately to motivated customers</h5>
					<p>Consumers who reach out by phone are typically more ready to spend. Our pay-per-call system brings them right to the advertiser.</p>
					
					<h5>Pay only for actual calls </h5>
					<p>If the advertiser doesn't receive calls, she doesn't pay a penny.</p> 
					
					<h5>Decide the price only after evaluating the lead</h5>
					<p>The advertiser listens to the customer request and decides whether to accept the call and how much he is willing to pay.</p>
					
					<h5>Track calls, manage budget, calculate ROI and more</h5>
					<p>Our Advertiser Dashboard lets advertisers completely control their total budgets, decide where they do business and when, and track the amount of calls they receive, the details of each call and how many calls they've lost. </p>
					
					<h5>No set up fees, contracts or monthly fees</h5>
					<p>We believe advertisers should pay only for results. No calls? They don't pay.</p>

				
				
				<br /><br /><br />				

				

				<h3 class="subclass">Have we convinced you yet?</h3>
				
				<p>If you're interested in launching No Problem in your country, we want to partner with you and will gladly guide you along the way. Let's talk!</p>
				
				<a class="contact_us" href="contact.aspx"></a>

				</div>
				
				</div>


</asp:Content>

