﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="aar_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
      <meta name="robots" content="nofollow"/><meta name="googlebot" content="noindex"/>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width"/>
    <title></title>
     <link href="www/lib/ionic/css/ionic.css" rel="stylesheet"/>
    <link href="www/css/style.css" rel="stylesheet"/>
    <link href="www/css/font-awesome.css" rel="stylesheet" />
   
      <script src="../jwplayer8/jwplayer.js" type="text/javascript"></script>
    <script type="text/javascript">jwplayer.key = "y0rIShXb57vnSkTfeDGVEdJRxWxvMmdar4NZxr6O60U=";</script>
<!--    <script type="text/javascript">jwplayer.key = "YiTTzdj7vuYrnKLzmlEHBtKK4FOLklGGehNIf6rsjWg=";</script>-->
    <!-- IF using Sass (run gulp sass first), then uncomment below and remove the CSS includes above
    <link href="css/ionic.app.css" rel="stylesheet">
    -->
       <script type="text/javascript">
           window.app = {};
           window.app.callId = <%=Guid.Empty == this.AarCallId ? "''" : "'"+this.AarCallId.ToString()+"'" %>;
           window.app.isRobot = <%=IsRobot()%>;
           window.app.useJwPlayer = <%= useJwPlayer()%>;
           window.app.GeneralRedirect = '<%= GENERAL_REDIRECT%>';
    </script>
      

    <script src="www/lib/ionic/js/ionic.bundle.min.js"></script>


    <!-- your app's js -->
    <script src="www/js/app.js"></script>
    <script src="www/js/services.js"></script>
    <script src="www/js/controllers.js"></script>
</head>
<body ng-app="aarApp">     
    <ion-nav-view></ion-nav-view>       
  </body>
</html>
