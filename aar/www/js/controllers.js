﻿(function (app) {


    app.appModule
        .controller('videoController', function ($scope, aarService, $state, $sce, $location) {


            /*
            $scope.connectToCustomer = function (callId, incidentId) {
                //     aarService.updateRequestMetadata(window.app.callId);
                aarService.fireEvent('GetLead', $scope.vm.AarCallId, $scope.vm.Incident.Id);

                if (!$scope.vm.Incident.IsOpen) {//when lead is closed just show closed message
                    $state.go('closedLead', { incidentId: $scope.vm.Incident.Id, callId: $scope.vm.AarCallId });
                    return;
                }

                aarService.connectToCustomer(callId, incidentId).success(function (data) {
                       
                    
                }).error(function (error) {
                    $state.go('closedLead', { incidentId: incidentId, callId: callId });
                });
            }
            */


            $scope.reportLead = function (callId, incidentId) {
                $state.go('reportLead', { incidentId: incidentId, callId: callId });
            };
            /*
            $scope.getTheApp = function () {
                aarService.getTheApp();
            };
            */
            $scope.clickGetTheApp = function () {
                aarService.clickGetTheApp();
            };
            $scope.playVideo = function () {
                /*
                //    aarService.updateRequestMetadata(window.app.callId);
                aarService.fireEvent('viewLead', $scope.vm.AarCallId, $scope.vm.Incident.Id);
                
                try {
                    if (typeof sessionStorage != 'undefined')
                        sessionStorage.setItem('isViewedVideo', 'true');
                }
                catch (exc) { }
                    
                $state.go('video', { vm: $scope.vm });
                //aarService.openModal('www/templates/video-popover.html', $scope);
                */
                aarService.runVideo($scope.vm.Incident.VideoUrl, $scope.vm.Incident.ThumbnailUrl);
            }
            $scope.isViewedVideo = function () {

                if (typeof sessionStorage != 'undefined' && $scope.isSupportSessionStorage()) {
                    if (typeof sessionStorage.isViewedVideo == 'undefined')
                        return false;
                    return sessionStorage.isViewedVideo == 'true';
                }

                return true;
            }
            $scope.isSupportSessionStorage = function () {

                try {
                    var testKey = 'test', storage = window.sessionStorage;
                    storage.setItem(testKey, '1');
                    storage.removeItem(testKey);
                    return true;
                }
                catch (error) {
                    return false;
                }
            }
            $scope.thumbnailWidth = function () {
                return document.querySelector('div.video-container').offsetWidth;
            }
            $scope.thumbnailHeight = function () {
                return document.querySelector('div.video-container').offsetHeight;
            }
            /*
            $scope.callClick = function () {
                aarService.fireEvent('Call', $scope.vm.AarCallId, $scope.vm.Incident.Id);
            }
            */
            function init() {
                $scope.vm = {};
                $scope.vm.getThumbnail = function () {
                  //  return '';
                    if ($scope.vm.Incident && $scope.vm.Incident.ThumbnailUrl)
                        return $scope.vm.Incident.ThumbnailUrl.$$unwrapTrustedValue();
                    else
                        return null;
                };

                aarService.getIncidentData().success(function (data) {
                    if (!data.d) {
                        aarService.showGeneralErrorMessage();

                        return;
                    }

                    angular.extend($scope.vm, data.d);
                    //     $scope.vm.videoState = window.app.useJwPlayer == true ? "jwvideo" : "video";
                    if (window.app.useJwPlayer == true)
                        aarService.loadVideo($scope.vm.Incident.VideoUrl, $scope.vm.Incident.ThumbnailUrl);
                    else {
                        aarService.loadVideoHtml5($scope.vm.Incident.ThumbnailUrl);
                        if ($scope.vm.Incident && $scope.vm.Incident.ThumbnailUrl)
                            $scope.vm.Incident.ThumbnailUrl = $sce.trustAsResourceUrl($scope.vm.Incident.ThumbnailUrl);

                        if ($scope.vm.Incident && $scope.vm.Incident.VideoUrl)
                            $scope.vm.Incident.VideoUrl = $sce.trustAsResourceUrl($scope.vm.Incident.VideoUrl);
                        
                    }
                    

                }).error(function (error) {
                    $scope.error = error;
                });
            }


            init();
            

        })
        .controller('connectToCustomerController', function ($scope, aarService, $stateParams, $state) {
            $scope.vm = {};

            if (!$stateParams.phone.phone || $stateParams.phone.phone.length == 0) {
       //         aarService.showGeneralErrorMessage();
                $state.go('viewVideo');
                return;
            }
            $scope.vm.phoneDetails = $stateParams.phone;
            $scope.vm.clickedCall = false;
            
            $scope.callClick = function () {
                $scope.vm.clickedCall = true;
                aarService.fireEvent('Call', $stateParams.callId, $stateParams.incidentId);
                //   $state.go('getTheApp', { incidentId: $stateParams.incidentId, callId: $stateParams.callId });
                
            };
            $scope.isCalled = function () {
                return $scope.vm.clickedCall;
            };
            $scope.getTheApp = function () {
                aarService.getTheApp();
            };
            
           
        })
        .controller('getTheAppController', function ($scope, aarService) {
            $scope.getTheApp = function () {
                aarService.getTheApp();
            };
        })
        .controller('leadClosedController', function ($scope, aarService) {
            $scope.getTheApp = function () {
                aarService.getTheApp();
            };
        })
        .controller('leadReportController', function ($scope, $stateParams, aarService) {

            $scope.vm = {
                callId: $stateParams.callId,
                incidentId: $stateParams.incidentId,
                commentText: null,
                reportType: 9
            };

            //Out of my area - code 9
            //Out of my hours of operation - code 2
            // Inappropriate content - code 11

            $scope.report = function () {
                var request = {
                    callId: $scope.vm.callId,
                    incidentId: $scope.vm.incidentId,
                    commentText: $scope.vm.commentText,
                    reportType: $scope.vm.reportType
                };

                aarService.reportLead(request).then(function (response) {

                });
            };


        })
        .controller('jwVideoController', function ($scope, $stateParams, aarService) {
            /*
            function init() {
                $scope.vm = $stateParams.vm;
                aarService.loadVideo($scope.vm.Incident.VideoUrl, $scope.vm.Incident.ThumbnailUrl);
            }
            init();
            */
        })
        .controller('html5VideoController', function ($scope, $stateParams, aarService) {
            $scope.showPlayIcon = function()
            {
                return !aarService.IsVideoPlaying();
            }
            $scope.showThumbnail = function () {
                return aarService.ToShowThumbnail();
            }
            /*
            function init() {
                $scope.vm = $stateParams.vm;

            }
            init();
            */
        });
        /*
        .directive('autoPlayVideo', function ($ionicHistory, $timeout, aarService) {
            return {
                restrict: 'A', //E = element, A = attribute, C = class, M = comment                        
                link: function ($scope, element, attrs, controller) {
                    var video = element[0];
                    video.play();

                    $scope.visible = false;

                    $timeout(function() {
                        $scope.visible = true;
                    }, 3000);

                    video.addEventListener('ended', function () {
                        aarService.fireEvent('leadViewed', $scope.vm.AarCallId, $scope.vm.Incident.Id);                        
                        $ionicHistory.goBack();
                    }, false);

                } //DOM manipulation
            };
        });
        */


    

})(window.app);