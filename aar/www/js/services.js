﻿(function(app) {
    app.appModule.factory('aarService', function ($http, $ionicModal, $ionicLoading, $state, $ionicPopup, $ionicHistory, $window) {

        var modalDialog = undefined;

        var lead = null;
        var isPlaying = false;
        var _toShowThumbnail = true;
        var phoneDetails = null;
        function updateRequestMetadata(callId, isRobot) {
            return $http.post('default.aspx/UpdateRequestMetadata', { callId: callId, isRobot: isRobot });
        }



        function fireEvent(eventName,callId, incidentId) {
            return $http.post('default.aspx/FireEvent', { eventName:eventName, callId:callId, incidentId:incidentId });
        }

        function openModal(templateUrl,$scope) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                modalDialog = modal;
                modalDialog.show();
            });           
        }
        
        function closeModal() {
            modalDialog.hide();
            modalDialog.remove();
            modalDialog = undefined;
        }

        function getIncidentData(callId) {
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
            });
            return $http.post('default.aspx/GetIncidentDetails', { callId: window.app.callId })
                .success(function (data, status, headers, config) {                       
                    $ionicLoading.hide();
                    if (data.d == null) {
                        setTimeout(function () {
                            window.location.href = window.app.GeneralRedirect;
                        }, 500);
                        return;
                    }
                    lead = data.d;
                    console.log(lead);
                })
                .error(function (data, status, headers, config) {
                    $ionicLoading.hide();                   
                });
        }


        function connectToCustomer(callId, incidentId) {
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
            });
            return $http.post('callback1.aspx/ConnectToCustomer', {callId: callId, incidentId: incidentId})
              .success(function (data, status, headers, config) {

                  if (data.d && data.d.IsSuccess) {
                      phoneDetails = data.d.phone;
                      $ionicLoading.hide();
                      $state.go('connectToCustomer', { phone: data.d.phone, callId: callId, incidentId: incidentId });
                  } else {
                      $ionicLoading.hide();
                      $state.go('closedLead');
                  }

                  
              })
              .error(function (data, status, headers, config) {
                  $ionicLoading.hide();
              });
        }

        function showGeneralErrorMessage() {
            var myPopup = $ionicPopup.show({
                title: 'Oops...',
                subTitle: 'Something went wrong. Please try again late',
                buttons: [
                  {
                      text: '<b>Ok</b>',
                      type: 'button-positive',
                      onTap: function (e) {
                        
                      }
                  }
                ]
            });
            myPopup.then(function (res) {

            });
        }

        function reportLead(request) {
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
            });
            return $http.post('callback1.aspx/reportLead', request)
              .success(function (data, status, headers, config) {
                  $ionicLoading.hide();
                  var alertPopup = $ionicPopup.alert({
                      title: 'Success',
                      template: 'Reported lead successfully'
                  });
                  alertPopup.then(function (res) {
                      $ionicHistory.goBack();
                  });
              })
              .error(function (data, status, headers, config) {
                  $ionicLoading.hide();
                  var alertPopup = $ionicPopup.alert({
                      title: 'Oops...',
                      template: 'Failed to report this video. Please try again later'
                  });
                  alertPopup.then(function (res) {
                      console.log('Thank you for not eating my delicious ice cream cone');
                  });
              });
        }
        function clickGetTheApp() {
            fireEvent('getLeadEvent', lead.AarCallId, lead.Incident.Id);
            $state.go('getTheApp');
        }
        function getTheApp() {

            if (!lead || !lead.Incident)
                return;            
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles" class="spinner-stable"></ion-spinner>'
            });

            fireEvent('GetApp', lead.AarCallId, lead.Incident.Id).then(getTheAppCompleted, getTheAppCompleted);
        }

        function getTheAppCompleted() {
            var call_id = (!lead || !lead.AarCallId) ? '' : lead.AarCallId;
            $ionicLoading.hide();
            $window.location.href = "/tm/DownloadApp.aspx?callid=" + call_id;
        }
        function loadVideo(VideoUrl, ThumbnailUrl) {
            var clipcallVideoNode = document.getElementById("ClipcallVideo");
            while (clipcallVideoNode.firstChild) {
                clipcallVideoNode.removeChild(clipcallVideoNode.firstChild);
            }
            var playerInstance = jwplayer("ClipcallVideo");
            var divParent = document.querySelector('.video-container');

            playerInstance.setup({
                file: VideoUrl,
                image: ThumbnailUrl,
                width: divParent.offsetWidth,
                height: (divParent.offsetHeight)
                //    aspectratio: "16:9"//,
                //    autostart: true               
            });
            jwplayer().onComplete(function () {
                isPlaying = false;
                fireEvent('leadViewed', lead.AarCallId, lead.Incident.Id);
                //       $ionicHistory.goBack();
            });
            jwplayer().onPlay(function () {
                isPlaying = true;
                fireEvent('viewLead', lead.AarCallId, lead.Incident.Id);

                try {
                    if (typeof sessionStorage != 'undefined')
                        sessionStorage.setItem('isViewedVideo', 'true');
                }
                catch (exc) { }
            })
        }
        function loadVideoHtml5(thumbnailUrl) {
            var divParent = document.querySelector('.video-container');
            var _video = document.querySelector('video');            

            var observer = new MutationObserver(function (mutations) {
                mutations.forEach(function (mutation) {
                    if (mutation.type == 'attributes' && mutation.attributeName == 'src') {
                        _video.load();
                    }
                });
            });

            var config = { attributes: true, childList: true, subtree: true };

            observer.observe(_video, config);

            //   _video.style.width = divParent.offsetWidth + 'px';
            _video.setAttribute("width", divParent.offsetWidth);
            //   _video.style.height = (divParent.offsetHeight - 5) + 'px';
            _video.setAttribute("height", (divParent.offsetHeight - 5));
       //     _video.style.background = "transparent url('" + thumbnailUrl + "') no-repeat 0 0";
            _video.addEventListener('play', function () {
                //_toShowThumbnail = false;
                HideThumbnail();
                if (isPlaying)
                    return;
                isPlaying = true;
                
                fireEvent('viewLead', lead.AarCallId, lead.Incident.Id);

                try {
                    if (typeof sessionStorage != 'undefined')
                        sessionStorage.setItem('isViewedVideo', 'true');
                }
                catch (exc) { }
            }, false);
            _video.addEventListener('ended', function () {
                isPlaying = false;
                fireEvent('leadViewed', lead.AarCallId, lead.Incident.Id);
            }, false);           
        }
        function HideThumbnail() {
            document.querySelector('#img_thumbnail').style.display = "none";
        }
        function runVideo(VideoUrl, ThumbnailUrl) {
           
            
            ///    jwplayer().onReady(function () {
            if (window.app.useJwPlayer == true)
                jwplayer().play();
            else {
                var _video = document.querySelector('video');
       //         _video.load();
                _video.play();
               
            }
            //       });
            //      playerInstance.play();

        }
        function IsVideoPlaying() {
            return isPlaying;
        }
        function ToShowThumbnail() {
            return _toShowThumbnail;
        }
        
        return {
            fireEvent:fireEvent,
            getIncidentData: getIncidentData,
            connectToCustomer: connectToCustomer,
            openModal: openModal,
            closeModal: closeModal,
            updateRequestMetadata: updateRequestMetadata,
            showGeneralErrorMessage: showGeneralErrorMessage,
            reportLead: reportLead,
            getTheApp: getTheApp,
            runVideo: runVideo,
            loadVideo: loadVideo,
            loadVideoHtml5: loadVideoHtml5,
            IsVideoPlaying: IsVideoPlaying,
            ToShowThumbnail: ToShowThumbnail,
            clickGetTheApp: clickGetTheApp
        };
    });
})(window.app);